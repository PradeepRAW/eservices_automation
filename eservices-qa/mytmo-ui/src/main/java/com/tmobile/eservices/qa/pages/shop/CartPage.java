package com.tmobile.eservices.qa.pages.shop;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author pshiva
 *
 */
public class CartPage extends CommonPage {

	public static final String STREET_ADDR = "250 Joralemon St";
	public static final String VALID_CITY_01 = "Bellevue";
	public static final String VALID_STATE_01 = "WA";
	public static final String VALID_ZIPCODE_01 = "98006";

	public static final String VALID_ADDRESSLINE_02 = "500 K Ave";
	public static final String VALID_CITY_02 = "Plano";
	public static final String VALID_STATE_02 = "TX";
	public static final String VALID_ZIPCODE_02 = "75074";

	public static final String INVALID_ADDRESSLINE = "PO BOX 53739";

	String maskedElement = null;

	@FindBys(@FindBy(css = "div[ng-show*='ctrl.isCartLoaded'] div:nth-child(7) span"))
	private List<WebElement> devicePriceBeforepromo;

	@FindBys(@FindBy(css = "div:nth-child(8) div span[class='body-copy-description pull-right text-black ng-binding']"))
	private List<WebElement> appliedTradeInPromoPrice;

	@FindBys(@FindBy(css = "div[ng-show*='ctrl.isCartLoaded'] div:nth-child(10) span[ng-bind*='ctrl.deviceDetailinCart.displayPrice']"))
	private List<WebElement> devicePriceAfterpromo;

	@FindBy(css = "[class*='p-t-50 p-t-10-xs']")
	private WebElement shippingDetailsPage;

	@FindBy(css = "#idNewCartMobEditShipAddress p")
	private WebElement shippingDetailsPageMobile;

	@FindBy(css = "h3[ng-bind*='ctrl.contentData.emptyCartTitle']")
	private WebElement emptyCart;

	// @FindBy(css = "button.btn.btn-primary.PrimaryCTA")
	@FindBy(css = "button[ng-click*='$ctrl.onClickRemoveModalBtn()']")
	private WebElement yesRemoveButton;

	@FindBy(css = "button[ng-bind*='ctrl.contentData.shopPhonesTxt']")
	private WebElement shopPhones;

	@FindBy(css = "md-tab-content [class*='deviceDownPaymentCls']")
	private WebElement downPayment;

	@FindBy(css = ".p-t-50.p-t-10-xs div.no-padding.hidden-xs")
	private WebElement shippingInfo;

	@FindBy(xpath = "//p[contains(text(), 'Accept & Continue')]")
	private List<WebElement> acceptAndPlaceOrderButton;

	@FindBy(css = "md-tab-content[class*='md-active'] div[aria-hidden='false'] button[ng-click*='ctrl.onClickCheckout']")
	private WebElement placeOrder;

	@FindBy(css = "span[ng-bind*='$ctrl.shippingDetails.shippingDate']")
	private List<WebElement> estimatedShipDateFormat;

	@FindBy(css = "div[ng-bind*='ctrl.contentData.monthlyChargesModalTotalText']")
	private WebElement monthlyChargesModelTotal;

	@FindBy(linkText = "TRADE-IN")
	private WebElement tradeInHeader;

	@FindBy(xpath = "//*[contains(@class,'activeContent')]//*[contains(@class,'shippingHtCalc')]//p[contains(.,'Shipping address')]")
	private WebElement shippingAddressMobileView;

	@FindBy(id = "editPpuAddr")
	private WebElement PPUAddressEditButton;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.container1 img[class='p-t-20 hidden-xs']")
	private WebElement deviceImage;

	@FindBy(css = "md-tab-content [ng-bind*='ctrl.deviceName']")
	private WebElement deviceName;

	@FindBy(css = "button.removeBtnCls  span[ng-bind*='removeBtnLabel']")
	private List<WebElement> removeLink;

	@FindBy(css = ".activeContent .removeBtnCls")
	private WebElement iOSremoveLink;

	@FindBy(css = "div#idRemoveItemModal h2")
	private WebElement removeModelHeader;

	@FindBy(css = "div.dataContent.activeContent button.editBtnCls span")
	private WebElement iOSeditButtonOnDevice;

	@FindBy(css = "button[ng-click*='ctrl.onClickEditBtn'] span[ng-bind*='$ctrl.contentData.editBtnLabel']")
	private WebElement editButtonOnDevice;

	@FindBy(css = "md-tab-content input#fname-on-card")
	private WebElement firstNameOnCard;

	@FindBy(css = ".activeContent input#fname-on-card")
	private WebElement paymentTabfirstNameOnCardForIOS;

	@FindBy(css = "md-tab-content input#lname-on-card")
	private WebElement lastNameOnCard;

	@FindBy(css = ".activeContent input#lname-on-card")
	private WebElement paymentTablastNameOnCardForIOS;

	@FindBy(id = "name-on-card")
	private WebElement nameOncard;

	@FindBy(css = "md-tab-content input#credit-card-number")
	private WebElement cardNumber;

	@FindBy(xpath = "//div[(@class='dataContent activeContent')]//input[@id='credit-card-number']")
	private WebElement paymentTabcardNumberForIOS;

	@FindBy(css = "md-tab-content input#expirydate")
	private WebElement expirationDate;

	@FindBy(css = ".activeContent input#expirydate")
	private WebElement paymentTabexpirationDateForIOS;

	@FindBy(css = "md-tab-content input#cvv")
	private WebElement cvv;

	@FindBy(css = ".activeContent input#cvv")
	private WebElement paymentTabCvvForIOS;

	@FindBy(css = "md-tab-content div[ng-bind*='ctrl.contentData.errorTxtCard']")
	private WebElement ccErrorMessage;

	@FindBy(xpath = "//input[@type='tel']//..//div[@aria-hidden='false']")
	private WebElement mobileccErrorMessage;

	@FindBy(css = "span[ng-bind*='$ctrl.billingAddressFormData.addressLine1']")
	private WebElement shippingAddressTxt;

	@FindBy(css = "div.activeContent span[ng-bind*='$ctrl.billingAddressFormData.addressLine1']")
	private WebElement shippingAddressTxt_iOS;

	@FindBy(css = "div.dataContent.activeContent button[ng-click='$ctrl.moveToShipping()']")
	private WebElement iOScontinueToShippingButton;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.no-padding.rightHeight  [ng-click*='ctrl.moveToShipping']")
	private WebElement continueToShippingButton;

	@FindBy(css = "[class*='activeContent'] [class*='visible-xs'] [ng-click*='moveToPayment']")
	private WebElement iOScontinueToPaymentButton;

	@FindBy(xpath = "//*[contains(@ng-click,'Update')]")
	private List<WebElement> iOSUpdatePaymentButton;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.shipHtReCalc button[ng-click*='ctrl.moveToPayment']")
	private WebElement continueToPaymentButton;

	@FindBy(css = "h2[ng-bind='$ctrl.contentData.removeModalHdr']")
	private WebElement removeWindowHeader;

	@FindBy(xpath = "//p[contains(.,'Ship to:')]")
	private WebElement shippingDetails;

	@FindBy(css = "[class*='activeContent'] [class*='visible-xs'] p.h4-title")
	private WebElement iOSshippingDetails;

	@FindBy(css = "div.shipHtReCalc p.h4-title")
	private WebElement aalShippingDetails;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.no-padding.bg-white-xs.paymentHtCalc div.md-container")
	private WebElement billingAndShippingCheckBox;

	@FindBy(css = "div.activeContent div.no-padding.bg-white-xs.paymentHtCalc div.md-container")
	private WebElement iOSbillingAndShippingCheckBox;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.no-padding.bg-white-xs.paymentHtCalc md-checkbox[checked='checked']")
	private List<WebElement> shippingAddressCheckBox;

	@FindBy(css = "div.activeContent div.no-padding.bg-white-xs.paymentHtCalc md-checkbox[checked='checked']")
	private List<WebElement> iOSshippingAddressCheckBox;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.no-padding.bg-white-xs.paymentHtCalc [ng-bind='$ctrl.contentData.billingAddHeaderTxt']")
	private WebElement billingHeader;

	@FindBy(css = "div.activeContent div.no-padding.bg-white-xs.paymentHtCalc [ng-bind='$ctrl.contentData.billingAddHeaderTxt']")
	private WebElement iOSbillingHeader;

	@FindBy(css = "md-tab-content[role='tabpanel'] label[ng-bind='$ctrl.contentData.addressLineFirstText']")
	private WebElement addressLineOneLabel;

	@FindBy(css = "div.activeContent label[ng-bind='$ctrl.contentData.addressLineFirstText']")
	private WebElement iOSaddressLineOneLabel;

	@FindBy(css = "md-tab-content[role='tabpanel'] label[ng-bind='$ctrl.contentData.addressLineSecondText']")
	private WebElement addressLineTwoLabel;

	@FindBy(css = "div.activeContent label[ng-bind='$ctrl.contentData.addressLineSecondText']")
	private WebElement iOSaddressLineTwoLabel;

	@FindBy(css = "div.activeContent label[ng-bind='$ctrl.contentData.cityText']")
	private WebElement iOSpaymentsTabCityLabel;

	@FindBy(css = "md-tab-content[role='tabpanel'] label[ng-bind='$ctrl.contentData.cityText']")
	private WebElement paymentsTabCityLabel;

	@FindBy(css = "md-tab-content[role='tabpanel'] label[ng-bind='$ctrl.contentData.stateText']")
	private WebElement paymentsTabStateLabel;

	@FindBy(css = "div.activeContent label[ng-bind='$ctrl.contentData.stateText']")
	private WebElement iOSpaymentsTabStateLabel;

	@FindBy(css = "md-tab-content[role='tabpanel'] label[ng-bind='$ctrl.contentData.zipText']")
	private WebElement paymentsTabZIPcodeLabel;

	@FindBy(css = "div.activeContent label[ng-bind='$ctrl.contentData.zipText']")
	private WebElement iOSpaymentsTabZIPcodeLabel;

	@FindBy(css = "label[for='idShippingStateCode']")
	private WebElement stateLabel;

	@FindBy(css = "label[for='idShippingZip']")
	private WebElement zIPcodeLabel;

	@FindBy(css = "div.activeContent  p[ng-bind='$ctrl.contentData.paymentMethodHdr']")
	private WebElement iOSpaymentInformationForm;

	@FindBy(css = "md-tab-content[role='tabpanel'] form[name= 'paymentform'] h4[ng-bind='$ctrl.contentData.paymentMethodHdr']")
	private WebElement paymentInformationForm;

	@FindBy(css = "md-tab-content[role='tabpanel'] div[ng-bind='::$ctrl.contentData.shippingAddressLineError']")
	private WebElement addressInvalidErrorMessage;

	@FindBy(css = "[ng-messages*='shippingMobileForm.address1.$error'] div")
	private WebElement addressInvalidErrorMessage_iOS;

	@FindBy(css = "div.ng-scope.ng-isolate-scope button[ng-click*='$ctrl.cancelEditShippingAddress()']")
	private WebElement cancelButton;

	@FindBy(xpath = "//*[contains(@ng-click,'cancelMobileEditAddress')]")
	private WebElement cancelButton_iPhone;

	@FindBy(css = "md-tab-content input[ng-model*='ctrl.shipAddress.addressLine1']")
	private WebElement addressLineOne;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'addressLine1')]")
	private WebElement addressLineOne_iPhone;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'addressLine1')]")
	private WebElement addressLineTwo_iPhone;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.container1 img[style*='height']")
	private WebElement insuranceDeviceImage;

	@FindBy(css = "div.activeContent div.container1 img[style*='height']")
	private WebElement iOSinsuranceDeviceImage;

	@FindBy(css = "div[ng-show='$ctrl.hasPhpInCart'][aria-hidden='true']")
	private WebElement insuranceName;

	@FindBy(css = "md-tab-content[role='tabpanel'] button[ng-click='$ctrl.onClickPHPEditBtn()'] span[ng-bind='::$ctrl.contentData.editBtnLabel']")
	private WebElement insuranceEditButton;

	@FindBy(css = "div.activeContent button[ng-click='$ctrl.onClickPHPEditBtn()'] span[ng-bind='::$ctrl.contentData.editBtnLabel']")
	private WebElement insuranceEditButton_iPhone;

	@FindBy(css = "md-tab-content[role='tabpanel'] button[ng-click='$ctrl.onClickPHPRemoveBtn()'] span[ng-bind='::$ctrl.contentData.removeBtnLabel']")
	private WebElement insuranceRemoveButton;

	@FindBy(css = "div.activeContent button[ng-click='$ctrl.onClickPHPRemoveBtn()'] span[ng-bind='::$ctrl.contentData.removeBtnLabel']")
	private WebElement insuranceRemoveButton_iPhone;

	@FindBy(css = "div.modal-background")
	private WebElement removeInsuranceModalWindow;

	@FindBy(css = "md-tab-content input[ng-model*='ctrl.shipAddress.cityName']")
	private WebElement cityNameShipping;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'cityName')]")
	private WebElement shippingTabCityNameForIOS;

	@FindBy(css = "md-tab-content input[ng-model*='ctrl.shipAddress.zip']")
	private WebElement zipCodetxt;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'zip')]")
	private WebElement shippingTabZipCodetxtForIOS;

	@FindBy(css = "#idNewCartMobEditShipAddress md-input-container>input[ng-model*='zip']")
	private WebElement shippingTabZipCodetxtForIPhone;

	@FindBy(css = "md-tab-content input[ng-model*='ctrl.shipAddress.stateCode']")
	private WebElement stateDropdown;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'state')]")
	private WebElement shippinTabStateDropdownForIOS;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//ul//a")
	private List<WebElement> stateFromDropdownForIOS;

	@FindBy(css = "button[ng-click*='$ctrl.onclickOKRemovePHP()']")
	private WebElement removeInsuranceModalWindowOkButton;

	@FindBys(@FindBy(xpath = "//p[contains(text(),'$')]"))
	private List<WebElement> dueTodayVerbiageDollerSymbol;

	@FindBy(xpath = "//div[contains(@class,'shipHtReCalc')]//md-radio-button[contains(@value,'Ground')]/div[@class='md-container md-ink-ripple']")
	private WebElement uPSGroundShippingMethod;

	@FindBy(xpath = "//div[contains(@class,'shipHtReCalc')]//md-radio-button[contains(@value,'Two')]/div[@class='md-container md-ink-ripple']")
	private WebElement twoDayShippingMethod;

	@FindBy(css = ".activeContent .shippingHtCalc [value*='Two']")
	private WebElement twoDayShippingMethodmobileView;

	@FindBy(xpath = "//div[@id='idCartPage']//h2/span[contains(.,'Shipping')]/i")
	private WebElement shippingHeader_iOS;

	@FindBy(css = "md-tab-item.md-ink-ripple p[ng-show='!$ctrl.shippingPageViewed")
	private WebElement shippingHeader;

	@FindBy(css = "h2[ng-bind*='monthlyChargesModalHeaderText']")
	private WebElement monthlyCharges;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.container1 span.h4-title.text-bold")
	private List<WebElement> deviceSpecifications;

	@FindBy(css = ".activeContent div.container1 span.h4-title.text-bold")
	private List<WebElement> deviceSpecificationsForIOS;

	@FindBy(css = "md-tab-item.md-ink-ripple p[ng-show='$ctrl.orderDetailViewed'] i")
	private WebElement orderDetalsTickMark;

	@FindBy(xpath = "//div[@id='idCartPage']//h2/span[contains(.,'Order detail')]/i")
	private WebElement orderDetalsTickMark_iOS;

	@FindBy(css = "md-tab-item.md-ink-ripple p[ng-show='$ctrl.shippingPageViewed'] i")
	private WebElement shippingTickMark;

	@FindBy(xpath = "//div[@id='idCartPage']//h2/span[contains(.,'Shipping')]/i")
	private WebElement shippingTickMark_iOS;

	@FindBy(css = "h2.h2-title.text-black.ng-binding")
	private WebElement removeInsuranceWindowHeaderMessage;

	@FindBy(xpath = "//p[contains(text(),'Are you sure you want to remove insurance?')]")
	private WebElement removeInsuranceModalWindowBodyMessage;

	@FindBy(xpath = "//span[contains(text(),'PAY MONTHLY')]")
	private List<WebElement> payMonthlyHeader;

	@FindBy(css = ".activeContent [ng-if*='isUpgradeSavingEnabled'] .border div.text-bold")
	private WebElement upgradeSavingMessageMobileView;

	@FindBy(xpath = "//a[contains(text(),'promo Discounts')]")
	private List<WebElement> promoDiscountVerbage;

	@FindBy(css = "div[ng-show='$ctrl.amountDifference>0'] strike.ng-binding")
	private WebElement monthlyPriceStriked;

	@FindBy(css = "span[ng-bind*='$ctrl.deviceDetailinCart.displayPrice']")
	private List<WebElement> installmentsMonthsPriceWithDollerSymbol;

	@FindBy(css = "div[md-connected-if='tab.isActive()'] span.body-copy-description.text-black.text-bold.ng-binding")
	private List<WebElement> dueTodayAndTaxLabel;

	@FindBy(xpath = "//span[contains(text(),'PAY IN FULL')]")
	private List<WebElement> payInFullHeader;

	@FindBy(css = "span[ng-bind*='$ctrl.deviceDetailinCart.displayPrice']")
	private List<WebElement> payInFullPriceWithDollerSymbol;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.no-padding.rightHeight [ng-click='$ctrl.addPromoCode($ctrl.contentData.addPromoCodeBtn)']")
	private WebElement addPromoCodeButton;

	@FindBy(css = "md-tab-content[role='tabpanel'] input[ng-model*='$ctrl.promoText']")
	private WebElement addPromoCodeValue;

	@FindBy(css = "md-tab-content[role='tabpanel'] div.no-padding.rightHeight [ng-click='$ctrl.applyCode($ctrl.promoText,$ctrl.contentData.promoApplyBtn)']")
	private WebElement applyButton;

	@FindBy(css = "md-tab-content[role='tabpanel'] i.fa.fa-check-circle.check-green")
	private WebElement promoCodeSuccessGreenMark;

	@FindBy(css = "md-tab-content[role='tabpanel'] div[ng-show='$ctrl.amountDifference>0'] span.body-copy-description")
	private List<WebElement> promoDiscountPriceSign;

	@FindBy(css = "md-tab-content[role='tabpanel'] div[ng-show='$ctrl.amountDifference>0'] span.body-copy-description.ng-binding")
	private WebElement promoDiscountPrice;

	@FindBy(css = "promoCodeSuccessMessage")
	private WebElement promoCodeSuccessMessage;

	@FindBy(css = "promoCodeCancelButton")
	private WebElement promoCodeCancelButton;

	@FindBy(xpath = "//*[contains(@class,'scrollClass')]//*[contains(@ng-hide,'shippingDate ')]")
	private WebElement estimatedShippingDateInOrderDetailPage;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-model='$ctrl.shipAddress.cityName']")
	private WebElement shippingCityTxt;

	@FindBy(css = "md-tab-content[role='tabpanel'] div[ng-messages='shippingForm.city.$error'] div[ng-message= 'required']")
	private WebElement cityFieldErrorMessage;

	@FindBy(css = "div[ng-messages='shippingMobileForm.city.$error']")
	private WebElement cityFieldErrorMessage_iOS;

	@FindBy(css = "md-tab-content[role='tabpanel'] div[ng-messages='shippingForm.zip.$error'] div[ng-message= 'required']")
	private WebElement zipCodeFieldErrorMessage;

	@FindBy(css = "div[ng-messages='shippingMobileForm.zip.$error']")
	private WebElement zipCodeFieldErrorMessage_iOS;

	@FindBy(css = "div.modal-background")
	private WebElement removeAccessoryModelWindow;

	@FindBy(css = "div.modal-background button.gluePrimaryCTAAccent")
	private WebElement continueShoppingButton;

	@FindBy(css = "div.modal-content p.body-copy-description")
	private WebElement removeAccessoryModelTextMessage;

	@FindBy(css = "div.modal-background button.glueSecondaryCTAAccent ")
	private WebElement accessoryCheckOutButton;

	@FindBy(css = "[aria-label*='it calculated']")
	private WebElement howItCalculatedSalesTaxLink;

	@FindBy(css = "[aria-label*='it calculated']")
	private List<WebElement> mobileHowItCalculatedSalesTaxLink;

	@FindBy(css = "[class*='h2-title text-bold']")
	private WebElement taxBreakDownHeader;

	@FindBy(css = "p[class*='body-copy-description ng-binding']")
	private WebElement salesTaxSubTitle;

	@FindBy(css = "div[ng-bind*='ctrl.deviceName']")
	private WebElement taxBreakDownWindowDeviceName;

	@FindBy(css = "[class='pull-right grayLight ng-binding']")
	private WebElement taxBreakDownWindowDevicePrice;

	@FindBy(css = "[class*='pull-left body-copy-description-regular']")
	private WebElement taxBreakDownWindowPriceSubTotalLabel;

	@FindBy(css = "[class*='pull-right body-copy-description-regular']")
	private WebElement taxBreakDownWindowPriceSubTotalPrice;

	@FindBy(css = "[class*='body-copy-description-regular pull-left']")
	private WebElement taxBreakDownWindowSimStarterKitText;

	@FindBy(css = "[class*='body-copy-description-regular pull-right']")
	private WebElement taxBreakDownWindowSimStarterKitPrice;

	@FindBy(css = "[class*='pull-left body-copy-description-bold text-black']")
	private WebElement taxBreakDownWindowSalesTaxLabel;

	@FindBy(css = "[class*='pull-right body-copy-description-bold text-black']")
	private WebElement taxBreakDownWindowSalesTaxPrice;

	@FindBy(css = "button[ng-click*='closeTaxbreakdownModal']")
	private WebElement taxBreakDownWindowGotItButton;

	@FindBy(css = "[class*='fine-print-body text-grey']")
	private WebElement taxBreakDownWindowSalesTaxLegalDisclaimer;

	@FindBy(xpath = "//md-tab-content[@role='tabpanel']//div/p[contains(text(),'UPS Expedited')]//..//..//div[@class='md-container md-ink-ripple']")
	private WebElement shippingViaUPSexpeditedMethod;

	@FindBy(css = "div.accessoryTileTextPadding div button[ng-click*='edit']>span")
	private WebElement accessoryEditButton;

	@FindBy(css = "promoCodeNotRequiredMessage")
	private WebElement promoCodeNotRequiredMessage;

	@FindBy(css = "md-tab-content[role='tabpanel'] div[ng-show='$ctrl.abondonSuccessText'] span.green-font.text-bold.ng-binding")
	private WebElement promoSuccessMessage;

	@FindBy(css = "deviceCostIsZero")
	private WebElement deviceCostIsZero;

	@FindBy(css = "md-tab-content[role='tabpanel'] span.red-font.text-bold.ng-binding")
	private WebElement promoCodeUnsucessMessage;

	@FindBy(css = "yourDeviceCostIsZeroTodayMessage")
	private WebElement yourDeviceCostIsZeroTodayMessage;

	@FindBy(css = "md-tab-content[role='tabpanel'] a[ng-click='$ctrl.showMonthlyChargesModal()")
	private WebElement monthlyChagesBreakDownLink;

	@FindBy(css = "div[ng-bind*='$ctrl.contentData.monthlyChargesModalEipText']")
	private WebElement newDevicePaymentLabel;

	@FindBy(css = "md-tab-content[role='tabpanel'] span.green-font.p-l-15.ng-binding")
	private WebElement promoCodeAppliesMessage;

	@FindBys(@FindBy(css = "md-tab-content[role='tabpanel'] span.green-font.text-bold.ng-binding"))
	private List<WebElement> promoCodeFirstTimeSuccessMessage;

	@FindBy(css = "#idShipAddress1")
	private WebElement addressLineLabel;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-model='$ctrl.shipAddress.addressLine1']")
	private WebElement addressLineText;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-model='$ctrl.shipAddress.cityName']")
	private WebElement cityLabelText;

	@FindBy(xpath = "//*[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'cityName')]")
	private WebElement cityLabelText_iOS;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-model='$ctrl.shipAddress.stateCode']")
	private WebElement stateLabelText;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'state')]")
	private WebElement stateLabelText_iOS;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-model='$ctrl.shipAddress.zip']")
	private WebElement zIPcodeLabelText;

	@FindBy(xpath = "//div[contains(@ng-if,'newAddressMobile')]//input[contains(@ng-model,'zip')]")
	private WebElement zIPcodeLabelText_iOS;

	@FindBy(css = "span[ng-bind*='$ctrl.monthlyAmountObj.totalMonthlyAmount']")
	private List<WebElement> dueMonthlyTotal;

	@FindBy(css = "[class*='totalMontlyTxtCls']")
	private WebElement dueMonthlyTotalLabel;

	@FindBy(xpath = "//span[contains(text(),'Total due monthly:')]")
	private List<WebElement> dueMonthlyText;

	@FindBy(css = "md-tab-content span.new-font.ng-binding")
	private WebElement eipLegalText;

	@FindBy(css = ".activeContent .totalFrpLegalTxtCls")
	private WebElement iOSeipLegalText;

	@FindBy(css = "div[ng-show*=eipIndicator][aria-hidden='true'] span[class*=totalFrpLegalTxtCls]")
	private List<WebElement> noEipLegalText;

	@FindBy(css = "md-tab-content span.monthlyAmountCls.ng-binding")
	private WebElement deviceMonthlyPrice;

	@FindBy(css = "md-tab-content span.monthlyAmountCls")
	private List<WebElement> accessoryMonthlyPrice;

	@FindBy(css = "div.activeContent span.monthlyAmountCls")
	private List<WebElement> iOSaccessoryMonthlyPrice;

	@FindBy(css = "md-tab-content .monthlyAmountLabelCls")
	private List<WebElement> eipMonthTerms;

	@FindBy(css = "div.activeContent .monthlyAmountLabelCls")
	private List<WebElement> iOSeipMonthTerms;

	@FindBy(css = "md-tab-content .dueTodayPlusTaxCls")
	private List<WebElement> dueTodayText;

	@FindBy(css = "div.activeContent .dueTodayPlusTaxCls")
	private List<WebElement> iOSdueTodayText;

	@FindBy(css = "md-tab-content  div:nth-child(9) span.ng-binding")
	private WebElement tradeinPrice;

	@FindBy(css = "md-tab-content #style div.container1 div.col-xs-8 div.col-xs-7 span")
	private WebElement tradeinPromoText;

	@FindBy(css = "p.totalDownPaymentTxtCls")
	private WebElement estDueTodayText;

	@FindBy(css = "p.totalTxtCks")
	private WebElement estTotalDueTodayText;

	@FindBy(css = "md-tab-content p.totalAmountCls")
	private WebElement estTotalDueToday;

	@FindBy(css = ".activeContent p.totalAmountCls")
	private WebElement estTotalDueTodayMobileView;

	@FindBy(css = "p.salesTaxTxtCls")
	private WebElement estSalesTaxText;

	@FindBy(css = "md-tab-content p.salesTaxAmountCls")
	private WebElement estSalesTax;

	@FindBy(css = ".activeContent p.salesTaxAmountCls")
	private WebElement estSalesTaxMobileView;

	@FindBy(css = "p.shippingTxtCls")
	private WebElement estShippingText;

	@FindBy(css = "md-tab-content p.shippingAmountCls")
	private WebElement estShipping;

	@FindBy(css = ".activeContent p.shippingAmountCls")
	private WebElement estShippingMobileView;

	@FindBy(css = "#idDownPaymentModal .modal-background-mobile")
	private WebElement eipErrorModal;

	@FindBy(css = "#idDownPaymentModal  div div.row div button")
	private WebElement gotItButton;

	@FindBy(css = "div.modal.fade button i")
	private WebElement closeModal;

	@FindBy(xpath = "//a[text()='Promo discount']")
	private List<WebElement> tradeInPromoText;

	@FindBy(css = "md-tab-content span[ng-bind*='ctrl.deviceDetailinCart.displayPrice']")
	private WebElement eipMonthlyAmtCart;

	@FindBy(css = "div[ng-show*='$ctrl.showStrikeOutPricingForDevice']")
	private List<WebElement> eipMonthlyTotalAmt;

	@FindBy(css = "md-tabs-wrapper[class='md-stretch-tabs']")
	private WebElement cartPage;

	@FindBy(css = "md-tab-content [ng-class*='ctrl.isDeviceOnEIP']")
	private WebElement eipMonthlyAmt;

	@FindBy(css = "md-tab-content p.totalDownPaymentAmountCls")
	private WebElement estDueTodaySubtotal;

	@FindBy(css = ".activeContent p.totalDownPaymentAmountCls")
	private WebElement estDueTodaySubtotalMobileView;

	@FindBy(css = "md-tab-content button.btn-black")
	private WebElement accessoryCardCTA;

	@FindBy(css = ".activeContent button.btn-black")
	private WebElement iOSaccessoryCardCTA;

	@FindBy(css = "md-tab-content [ng-bind*=\"accessory.fullRetailPrice.value\"]")
	private WebElement accessoryFRPPrice;

	@FindBy(css = "md-tab-content .deviceDownPaymentCls")
	private List<WebElement> downPaymentPrice;

	@FindBy(css = "md-tab-content div[ng-repeat*='ctrl.accessoriesIncart']")
	private WebElement accessoryImageAtCart;

	@FindBy(css = ".activeContent div[ng-repeat*='accessoriesIncart']")
	private WebElement accessoryImageAtCartMobileView;

	@FindBy(css = "div.activeContent button.btn-black.ng-binding")
	private WebElement iOSaddAccessoryCartButton;

	@FindBy(css = "md-tab-content[role='tabpanel'] span.frpLegalTxtCls")
	private WebElement legalText;

	@FindBy(css = "md-tab-content[role='tabpanel'] div[ng-show*='ctrl.isDeviceOnEIP']")
	private WebElement fullRetailprice;

	@FindBy(xpath = "//p[contains(.,'Sales tax')]/following-sibling::p[contains(@ng-bind,'youPayTodayTax')]")
	private WebElement salesTaxAmountInPaymentsTab;

	@FindBy(xpath = "((//cart-payment-html)[4]//p[contains(@ng-bind,'youPayTodayTax ')])[1]")
	private WebElement salesTaxAmountInPaymentsTab_iPhone;

	@FindBy(css = ".md-no-scroll.md-active .text-black[ng-bind*='downPayment']")
	private WebElement dueTodayTotalInPaymentsTab;

	@FindBy(xpath = "((//cart-payment-html)[4]//p[contains(@ng-bind,'youPayTodayTax ')])[2]")
	private WebElement dueTodayTotalInPaymentsTab_iPhone;

	@FindBy(css = "md-tab-content[role='tabpanel'] p[ng-show*='ctrl.addressFlag']")
	private WebElement billingAddressInPaymentsTab;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-bind*='ctrl.billingAddressFormData.cityName']")
	private WebElement billingAddressSameAsShippingCityInPaymentsTab;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-bind*='ctrl.billingAddressFormData.stateCode']")
	private WebElement billingAddressSameAsShippingStateInPaymentsTab;

	@FindBy(css = "md-tab-content[role='tabpanel'] [ng-bind*='ctrl.billingAddressFormData.zip']")
	private WebElement billingAddressSameAsShippingZipInPaymentsTab;

	@FindBy(css = "md-tab-content [ng-bind*='fullRetailPrice']")
	private WebElement frpPriceDevice;

	@FindBy(css = "md-tab-content[id='tab-content-38'] p[ng-bind*='$ctrl.totalFinancedAmount)']")
	private WebElement orderFinancedAmount;

	@FindBy(css = "md-tab-content[id='tab-content-40'] p[ng-bind*='$ctrl.totalFinancedAmount)']")
	private WebElement paymentFinancedAmount;

	@FindBy(css = "md-tab-content div[ng-bind='$ctrl.phpServiceName']")
	private WebElement insuranceSoc;

	@FindBy(css = "div.dataContent.activeContent div[ng-bind='$ctrl.phpServiceName']")
	private WebElement iOSinsuranceSoc;

	@FindBy(css = "div.dataContent.activeContent button[ng-click='$ctrl.onClickPHPEditBtn()']")
	private WebElement iOSinsuranceEdit;

	@FindBy(css = "md-tab-content button[ng-click='$ctrl.onClickPHPEditBtn()']")
	private WebElement insuranceEdit;

	@FindBy(css = "md-tab-content *[ng-bind*='$ctrl.totalFinancedAmount)']")
	private List<WebElement> financedAmount;

	@FindBy(xpath = "//div[contains(@class,'activeContent')]//p[contains(.,'Total financed amount')]")
	private WebElement iOSorderDetailFinancedAmountlabel;

	@FindBy(css = "div[ng-show='$ctrl.paymentDetailTab'] div[ng-if='$ctrl.isUpdateQuote']")
	private List<WebElement> iOSPaymentsTabFinancedAmountlabel;

	@FindBy(xpath = "//div[contains(@class,'activeContent')]//p[contains(.,'Total financed amount')]/following-sibling::p")
	private WebElement iOSorderDetailFinancedAmount;

	@FindBy(xpath = "//cart-order-html[contains(@class,'ng-scope')] //*[contains(@class,'h5-title pull-left text-grey-40')][1]")
	private WebElement orderDetailFinancedAmountlabel;

	@FindBy(xpath = "//cart-payment-html[contains(@class,'ng-scope')]//*[contains(@class,'h5-title pull-left text-grey-40')][1]")
	private WebElement paymentFinancedAmountLabel;

	@FindBy(css = "div[ng-messages='billingAddressForm.addressLine1.$error'] div[class*='ng-enter-active']")
	private WebElement addressLineOneErrorMessage;

	@FindBy(css = "div[ng-messages='billingAddressForm.cityName.$error'] div[class*='ng-enter-active']")
	private WebElement cityNameErrorMessage;

	@FindBy(css = "div[ng-messages='billingAddressForm.zip.$error'] div[class*='ng-enter-active']")
	private WebElement zipcodeErrorMessage;

	@FindBy(css = "shippingAddressEditButton")
	private WebElement shippingAddressEditButton;

	@FindBy(css = "md-tab-content input[ng-model*='ctrl.shipAddress.stateCode']")
	private WebElement stateName;

	@FindBy(css = "ShopPhonesCTA")
	private WebElement ShopPhonesCTA;

	@FindBy(css = "shopAccessorySecondaryCTA")
	private WebElement shopAccessorySecondaryCTA;

	@FindBy(css = "div[ng-class*='hasAccessoryInCart'] div.pii_mask_data")
	private List<WebElement> customerName;

	@FindBy(css = ".activeContent #top div[class*='h4-title']")
	private WebElement mobileCustomerName;

	@FindBy(css = ".md-active strong")
	private WebElement promoBannerText;

	@FindBy(css = ".md-active #promoModalLaunch")
	private WebElement promoBannerDetailsLink;

	@FindBy(css = "div.modal-content")
	private WebElement promoModalOfferDetailsWindow;

	@FindBy(css = "div.modal-content div div div")
	private WebElement promoModalOfferDetails;

	@FindBy(css = ".md-active div.cart-promo-banner")
	private WebElement promoBanner;

	@FindBy(css = ".md-active div.cart-promo-banner p b")
	private WebElement promoBannerTextInCart;

	@FindBy(css = "button i")
	private WebElement closePromoModal;

	@FindBy(css = "div.modal-content a")
	private WebElement promoModalOfferDetailsLink;

	@FindBy(css = "div#idRemoveItemModal")
	private List<WebElement> oopsWeHitaSnagModal;

	@FindBy(css = ".md-active [ng-show*='isInvalidAddress'] p")
	private WebElement wrongAddressError;

	@FindBy(css = "div [ng-show*='isInvalidAddress'] p")
	private WebElement wrongAddressErrorAndroid;

	@FindBy(css = "div[ng-show*='hasPhpInCart'] [ng-bind*='$ctrl.phpServiceName']")
	private List<WebElement> deviceProtectionSelectedSoc;

	@FindBy(xpath = "//div[@ng-show='$ctrl.hasPhpInCart']//div[@ng-bind]/following-sibling::div")
	private WebElement SOCPrice;

	@FindBy(css = "div .sim-section")
	private WebElement sIMKitPrice;

	@FindBy(css = ".pii_mask_data[ng-bind*='userDisplayName']")
	private WebElement newLineName;

	@FindBy(css = "span[ng-bind*='simAuthoredModelName']")
	private WebElement sIMKitName;

	@FindBy(css = "span[ng-bind*='payInFullLabel']")
	private WebElement sIMKitPayInFull;

	@FindBy(css = "div[ng-if*='isAALFlow'] div[ng-bind*='additionalVoiceLinesText']")
	private WebElement additionalVoiceLineTitle;

	@FindBy(css = "div[ng-if*='isAALFlow'] span[ng-bind*='planValuesInCart.name']")
	private WebElement additionalVoiceLinePlan;

	@FindBy(css = "div[ng-if*='isAALFlow'] span[ng-bind*='planValuesInCart.pricing']")
	private WebElement additionalVoiceLineMonthlyPlanPrice;

	@FindBy(css = "div[md-connected-if*='isActive'] .border span")
	private WebElement seeDetailsLink;

	@FindBy(css = "#idShowDetailModal")
	private WebElement seeDetailsPopUp;

	@FindBy(css = "h2[ng-bind*='seeDetailsModalHeader']")
	private WebElement seeDetailsPopUpHeader;

	@FindBy(css = "p[ng-bind*='seeDetailsModalDesc']")
	private WebElement seeDetailsPopUpBody;

	@FindBy(css = ".modal button i")
	private WebElement seeDetailsPopUpClose;

	@FindBy(css = ".hidden-xs>[ng-click*='$ctrl.customerAgreementCheckBoxClick()']")
	private WebElement serviceAgreementCheckBox;

	@FindBy(css = "[class*='activeContent'] [class*='visible-xs'] [ng-click*='$ctrl.customerAgreementCheckBoxClick()'] [class='md-container']")
	private WebElement serviceAgreementCheckBoxMobile;

	@FindBy(css = ".no-padding.hidden-xs p[ng-bind-html*='customerServiceAgreementText']")
	private WebElement serviceAgreementText;

	@FindBy(xpath = "//div[text()='CUSTOMER AGREEMENT']")
	private WebElement serviceAgreementTitle;

	@FindBy(css = "md-checkbox[aria-checked='false']")
	private List<WebElement> serviceAgreementCheckbox;

	@FindBy(css = "#paymentTab2 [aria-hidden='false'] [aria-label='Accept & Continue']")
	WebElement placeOrderCTA;

	@FindBy(xpath = "//p[contains(text(),'Due today')]")
	private WebElement dueTodayLabelAtPayment;

	@FindBy(xpath = "//p[contains(.,'Due')]/following-sibling::p")
	private WebElement dueTodayAmountAtPayment;

	@FindBy(xpath = "//*[contains(@class,'activeContent')]//p[contains(.,'Due')]/following-sibling::p")
	private WebElement dueTodayAmountAtPaymentMobileView;

	@FindBy(xpath = "//p[contains(text(), 'Primary place of use')]")
	private WebElement PPUAddressTitle;

	@FindBy(xpath = "//*[contains(@class,'activeContent')]//*[contains(@class,'shippingHtCalc')]//p[contains(text(), 'Primary place of use')]")
	private WebElement PPUAddressTitleMobileView;

	@FindBy(css = "div:nth-child(4)>div.p-t-5.legal")
	private WebElement legalPPUAddressTitle;

	@FindBy(css = "input[ng-model*='$ctrl.PPUAddress.addressLine1']")
	private WebElement PPUAddressLine1;

	@FindBy(css = "input[ng-model*='$ctrl.PPUAddress.addressLine2']")
	private WebElement PPUAddressLine2;

	@FindBy(css = "input[ng-model*='$ctrl.PPUAddress.cityName']")
	private WebElement PPUcityName;

	@FindBy(css = "input[ng-model*='$ctrl.PPUAddress.stateCode']")
	private WebElement PPUstate;

	@FindBy(css = "input[ng-model*='$ctrl.PPUAddress.zip']")
	private WebElement PPUzipCode;

	@FindBy(css = "button[ng-click*='ctrl.cancelPpuAddr()']")
	private WebElement PPUAddressCancelCTA;

	@FindBy(css = "input[ng-model*='$ctrl.e911Address.addressLine1']")
	private WebElement e911AddressLine1;

	@FindBy(css = "input[ng-model*='$ctrl.e911Address.addressLine2']")
	private WebElement e911AddressLine2;

	@FindBy(css = "input[ng-model*='$ctrl.e911Address.cityName']")
	private WebElement e911City;

	@FindBy(css = "input[ng-model*='$ctrl.e911Address.stateCode']")
	private WebElement e911StateCode;

	@FindBy(css = "input[ng-model*='$ctrl.e911Address.zip']")
	private WebElement e911ZipCode;

	@FindBy(css = "img[ng-click*='$ctrl.editE911Address()']")
	private WebElement E911AddressTitle;

	@FindBy(css = "[ng-click*='cancelE911Addr()']")
	private WebElement e911AddressCancel;

	@FindBy(xpath = "//p[contains(text(), 'E911 address')]")
	private WebElement e911AddressTitle;

	@FindBy(xpath = "//*[contains(@class,'activeContent')]//*[contains(@class,'shippingHtCalc')]//p[contains(text(), 'E911 address')]")
	private WebElement e911AddressTitleMobileView;

	@FindBy(id = "editE911Addr")
	private WebElement e911AddressEdit;

	@FindBy(css = "[ng-if*='isShippingEdit']")
	private WebElement editShippingCTA;

	@FindBy(css = ".activeContent [ng-click*='EditCartAddress']")
	private WebElement mobileEditShippingCTA;

	@FindBy(css = "[ng-model*='shipAddress.addressLine1']")
	private WebElement ShippingAddressLine1EditField;

	@FindBy(css = "[ng-model*='shipAddress.addressLine1']")
	private List<WebElement> ShippingAddressLine1EditFieldList;

	@FindBy(css = "[ng-model*='shipAddress.addressLine2']")
	private WebElement ShippingAddressLine2EditField;

	@FindBy(css = "[ng-model*='shipAddress.cityName']")
	private WebElement ShippingCityEditField;

	@FindBy(css = "[ng-model*='shipAddress.stateCode']")
	private WebElement ShippingStateEditField;

	@FindBy(css = "[ng-model*='shipAddress.zip']")
	private WebElement ShippingZipCodeEditField;

	@FindBy(css = "[ng-click*='cancelEditShippingAddress']")
	private WebElement cancelCTAOnShippingEdit;

	@FindBy(xpath = "//p[contains(.,'Shipping address')]")
	private WebElement shippingAddressTitle;

	@FindBy(css = "span[ng-bind*='ctrl.contentData.shippingAddressLine1Placeholder']")
	private WebElement addressLineOneAuthorableText;

	@FindBy(css = "span[ng-bind*='ctrl.contentData.shippingAddressLine2Placeholder']")
	private WebElement addressLineTwoAuthorableText;

	@FindBy(css = "label[ng-bind*='ctrl.contentData.shippingCityLbl']")
	private WebElement pPUAddressCityLabel;

	@FindBy(css = "label[ng-bind*='ctrl.contentData.shippingStateLbl']")
	private WebElement pPUAddressStateLabel;

	@FindBy(css = "label[ng-bind*='ctrl.contentData.shippingZipcodeLbl']")
	private WebElement pPUAddressZipCodeLabel;

	@FindBy(css = "div[ng-show='!$ctrl.isShippingEdit']")
	private WebElement defaultShippingAddressInShippingTab;

	@FindBy(css = ".activeContent div[ng-show='!$ctrl.isShippingEdit']")
	private WebElement mobileDefaultShippingAddressInShippingTab;

	@FindBy(css = ".activeContent .shippingHtCalc [value*='Ground']")
	private WebElement groundShippingMethod;

	@FindBy(css = "div.activeContent [ng-bind='$ctrl.phpServiceName']")
	private List<WebElement> insuranceName_iPhone;

	@FindBy(css = "md-tab-content [ng-bind-html*='$ctrl.contentData.tradeinMessageText']")
	private WebElement tradeInMsgText;

	@FindBy(css = "md-tab-content [ng-bind-html*='$ctrl.contentData.wantToTransferNumberText']")
	private WebElement portMsgText;

	@FindBy(css = "md-tab-content input[ng-model*='billingAddressPopulateData.addressLine1']")
	private WebElement billingAddressLineOne;

	@FindBy(css = "md-tab-content input[ng-model*='$ctrl.billingAddressPopulateData.cityName']")
	private WebElement billingAddressCity;

	@FindBy(css = "md-tab-content input[ng-model*='$ctrl.billingAddressPopulateData.stateCode']")
	private WebElement billingState;

	@FindBy(css = "md-tab-content input[ng-model*='billingAddressPopulateData.zip']")
	private WebElement billingZipCodetxt;

	@FindBy(css = "[ng-bind*='$ctrl.contentData.invalidAddressMsg']")
	private WebElement errorMessageOnInvalidShippingAddress;

	@FindBy(css = "[ng-bind*='invalidPPUAddressMsg']")
	private WebElement errorMessageOnInvalidPPUAddress;

	@FindBy(css = "[ng-bind*='ctrl.contentData.invalidE911AddressMsg']")
	private WebElement errorMessageOnInvalidE911Address;

	@FindBy(css = "[ng-class*='$ctrl.contentData.placeHolderText']")
	private WebElement trdeInMessageText;

	@FindBy(css = "button[ng-click*='$ctrl.removeAccessory']")
	private List<WebElement> removeAccessory;

	@FindBy(css = ".activeContent button[ng-click*='$ctrl.removeAccessory']")
	private List<WebElement> removeAccessoryMobileView;

	@FindBy(css = "span[ng-if='accessory.isAcessoryOnEIP']")
	private List<WebElement> accessoryDeviceEipOption;

	@FindBy(css = ".activeContent span[ng-if='accessory.isAcessoryOnEIP']")
	private List<WebElement> accessoryDeviceEipOptionMobile;

	@FindBy(css = "span[class*='credit-head']")
	private WebElement billCreditValueText;

	@FindBy(css = "a[ng-click*='$ctrl.DRPAcceptanceModal()']")
	private WebElement drpAgreementLink;

	@FindBy(css = "a[ng-click*='$ctrl.DRPCustomerAgreementModal()']")
	private WebElement drpTermsAndConditionsLink;

	@FindBy(css = "div.modal-background button.gluePrimaryCTAAccent.glueButton")
	private WebElement accessoriesModelContinueShoppingCTA;

	@FindBy(css = "div[ng-if='!$ctrl.isByod'] div:nth-child(9) div:first-child span")
	private WebElement promotionDiscountPriceNonTradeIn;

	@FindBy(css = "span[class='body-copy-description'] a")
	private WebElement totalPromotionDiscountPriceNonTradeIn;

	@FindBy(css = "[ng-if*='accessory.isAcessoryOnEIP'] [class*='monthlyAmountCls']")
	private WebElement accessoryEIPPrice;

	@FindBy(css = "#promoModalLaunch")
	private WebElement promoModalLink;

	@FindBy(css = "div[ng-bind-html*='$ctrl.cartPromoBanner.cartPromoModal']")
	private WebElement promoModal;

	public static String FraudCheck_State = "WA";
	public static String FraudCheck_ZIPCode = "98029";

	@FindBy(xpath = "(//*[contains(@value,'Next Day')])[2]")
	private WebElement nextDayDeliveryRadioBtn;

	@FindBy(css = ".activeContent .shippingHtCalc [value*='Next Day']")
	private WebElement nextDayDeliveryRadioBtnMobileView;

	@FindBy(css = "#acsMainInvite .acsCloseButton--container>span>a")
	private List<WebElement> mobileFeedBackAlert;

	@FindBy(xpath = "(//cart-payment-html//*[contains(@ng-bind,'shippingCharge')])[1]")
	private WebElement shippingPricePaymentTab;

	@FindBy(xpath = "//*[contains(@class,'activeContent')]//p[contains(.,'Shipping')]/following-sibling::p")
	private WebElement shippingPricePaymentTabMobileView;

	@FindBy(css = "span[ng-bind='$ctrl.deviceDetailinCart.color']")
	private WebElement deviceColor;

	@FindBy(css = "span[ng-bind='$ctrl.deviceDetailinCart.memory']")
	private WebElement deviceMemory;

	@FindBy(css = "#paymentTab2 [aria-hidden='false'] [aria-label='Accept & Continue']")
	private List<WebElement> acceptAndContinueCTA;

	/**
	 * @param webDriver
	 */
	public CartPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Cart Page
	 *
	 * @return
	 */
	public CartPage verifyCartPage() {
		checkPageIsReady();
		waitforSpinner();
		// verifyDuplicatedElements(this.getClass());
		try {
			if (getDriver() instanceof AppiumDriver) {
				if (mobileFeedBackAlert.size() == 1) {
					mobileFeedBackAlert.get(0).click();
				}
			} else {
				verifyErrorMessageIsNotDisplayed();
				verifyPageUrl();
				Reporter.log("Navigated to Cart page and Verified");
			}
		} catch (Exception e) {
			Assert.fail("Cart page not loaded");
		}
		return this;
	}

	/**
	 * Verify Upgrade saving message.
	 *
	 * @return
	 */
	public CartPage verifyUpgradeSavingMessage() {
		try {
			verifyErrorMessageIsNotDisplayed();
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(upgradeSavingMessageMobileView.isDisplayed(),
						"Upgrade saving message is not displayed");
				Reporter.log("Upgrade saving message is displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(cartPage), 30);
				Assert.assertTrue(trdeInMessageText.isDisplayed(), "Upgrade saving message is not displayed");
				Reporter.log("Upgrade saving message is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Upgrade saving message is not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public CartPage verifyPageUrl() {
		try {
			getDriver().getCurrentUrl().contains("cart");
		} catch (Exception e) {
			Assert.fail("Cart page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Shipping Details Page
	 *
	 * @return
	 */
	public CartPage verifyShippingDetailsComponent() {
		waitforSpinner();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(shippingDetailsPageMobile.isDisplayed(), "Shipping details are not displayed");
				Reporter.log("Shipping details are displayed");
			} else {
				Assert.assertTrue(shippingDetailsPage.isDisplayed(), "Shipping details are not displayed");
				Reporter.log("Shipping details are displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify shipping details");
		}
		return this;
	}

	/**
	 * verify full financed amount
	 *
	 * @return
	 */
	public CartPage verifyFullFinancedAmount() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(iOSorderDetailFinancedAmount.isDisplayed(),
						" Total financed amount  is not displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(financedAmount.get(0)));
				Assert.assertTrue(financedAmount.get(0).isDisplayed(), " Total financed amount  is not displayed");
			}
			Reporter.log("Total financed amount  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Total financed amount");
		}
		return this;
	}

	/**
	 * verify full financed amount total is not displayed when selected FRP on
	 * product detail
	 *
	 * @return
	 */
	public CartPage verifyFullFinancedAmountFRPDisplayedInOrderDetails() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(financedAmount.get(0)), "Total financed amount is displayed");
			Reporter.log("Total financed amount is not displayed");
		} catch (Exception e) {
			Reporter.log("Failed to verify Total financed amount");
		}
		return this;
	}

	/**
	 * verify financed amount in payment tab
	 *
	 * @return
	 */
	public CartPage verifyPaymentFinancedAmount() {

		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(iOSPaymentsTabFinancedAmountlabel.get(5).isDisplayed(),
						"Total financed amount  is not displayed");
			} else {
				waitforSpinner();
				Assert.assertTrue(financedAmount.get(1).isDisplayed(), "Total financed amount  is not displayed");
			}
			Reporter.log("Payment financed amount  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display payment financed amount");
		}
		return this;
	}

	/**
	 * verify financed amount not displayed in Payment tab when selected FRP on
	 * product detail
	 *
	 * @return
	 */
	public CartPage verifyFullFinancedAmountFRPDisplayedInPayment() {
		try {
			for (WebElement finAmount : financedAmount) {

				Assert.assertFalse(isElementDisplayed(finAmount), "Total financed amount is displayed");
			}

			Reporter.log("Total financed amount  is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display payment financed amount");
		}
		return this;
	}

	private CartPage setAddressOne(String address) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				addressLineOne_iPhone.click();
				addressLineOne_iPhone.sendKeys(address);
				Reporter.log("Address line one is displayed");
			} else {
				sendTextData(addressLineOne, address);
				Reporter.log("Address line one is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Address line one is not displayed");
		}
		return this;
	}

	private CartPage setCity(String city) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				sendTextData(shippingTabCityNameForIOS, city);
				Reporter.log("City name is displayed");
			} else {
				sendTextData(cityNameShipping, city);
				Reporter.log("City name is displayed");
			}
		} catch (Exception e) {
			Assert.fail("City name is not displayed");
		}
		return this;
	}

	private CartPage selectState(String state) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				sendTextData(shippinTabStateDropdownForIOS, state);
				waitforSpinner();
				stateFromDropdownForIOS.get(0).click();
				shippinTabStateDropdownForIOS.sendKeys(Keys.TAB);
				Reporter.log("Selected respective state");
			} else {
				sendTextData(stateDropdown, state);
				Reporter.log("Selected respective state");
			}
		} catch (Exception e) {
			Assert.fail("Unable to select State");
		}
		return this;
	}

	private CartPage setZipCode(String zipCode) {
		try {
			if (getDriver() instanceof AndroidDriver) {
				sendTextData(shippingTabZipCodetxtForIOS, zipCode);
				Reporter.log("Zip code is displayed");
			} else if (getDriver() instanceof IOSDriver) {
				shippingTabZipCodetxtForIPhone.click();
				shippingTabZipCodetxtForIPhone.click();
				waitforSpinner();
				((IOSDriver<?>) getDriver()).getKeyboard().sendKeys(zipCode);
				Reporter.log("Zip code is displayed");
			} else {
				sendTextData(zipCodetxt, zipCode);
				Reporter.log("Zip code is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Zip code is not displayed");
		}
		return this;
	}

	/**
	 * Fill Shipping Address Information
	 */
	public CartPage fillShippingAddressInfo() {
		try {
			setAddressOne(Constants.STREET_ADDR);
			setCity(Constants.CITY);
			selectState(Constants.STATE);
			setZipCode(Constants.ZIP_CODE_E911);
		} catch (Exception e) {
			Assert.fail("Shipping address information is not set");
		}
		return this;
	}

	/**
	 * Fill Shipping Address Information
	 */
	public CartPage fillBillingAddressInfo() {
		try {
			setAddressOneForBilling(Constants.Real_ADDRESSLINE1);
			setCityForBilling(Constants.Real_CITY);
			selectStateForBilling(Constants.Real_State);
			setZipCodeForBilling(Constants.Real_ZIP_CODE);
		} catch (Exception e) {
			Assert.fail("Billing address information is not set");
		}
		return this;
	}

	private CartPage setAddressOneForBilling(String address) {
		try {
			sendTextData(billingAddressLineOne, address);
			Reporter.log("Address line one is displayed In Payment Page Billing");

		} catch (Exception e) {
			Assert.fail("Address line one is not displayed In Payment Page Billing");
		}
		return this;
	}

	private CartPage setCityForBilling(String city) {
		try {
			sendTextData(billingAddressCity, city);
			Reporter.log("City name is displayed");
		} catch (Exception e) {
			Assert.fail("City name is not displayed");
		}
		return this;
	}

	private CartPage selectStateForBilling(String state) {
		try {
			sendTextData(billingState, state);
			Reporter.log("Selected respective state");
		} catch (Exception e) {
			Assert.fail("Unable to select State");
		}
		return this;
	}

	private CartPage setZipCodeForBilling(String zipCode) {
		try {
			sendTextData(billingZipCodetxt, zipCode);
			Reporter.log("Zip code is displayed");
		} catch (Exception e) {
			Assert.fail("Zip code is not displayed");
		}
		return this;
	}

	/**
	 * Fill Invalid Shipping Address Information
	 */
	public CartPage fillInvalidShippingAddressInfo() {
		try {
			setAddressOne(Constants.INVALID_STREET_ADDR);
			setCity(Constants.INVALID_CITY);
			selectState(Constants.INVALID_STATE);
			setZipCode(Constants.INVALID_ZIP_CODE_E911);
		} catch (Exception e) {
			Assert.fail("Shipping address information is not set");
		}
		return this;
	}

	/**
	 * Verify Empty Cart
	 *
	 * @return
	 */
	public CartPage verifyEmptyCart() {
		try {
			waitforSpinner();
			Assert.assertTrue(emptyCart.isDisplayed(), "Empty card is not displayed");
			Reporter.log("Empty cart is displayed");
		} catch (Exception e) {
			Assert.fail("Empty cart text not displayed");
		}
		return this;
	}

	/**
	 * Verify Shipping Address
	 *
	 * @return
	 */
	public CartPage verifyShippingAddress() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(shippingAddressMobileView), 15);
				moveToElement(shippingAddressMobileView);
				Assert.assertTrue(shippingAddressMobileView.isDisplayed(), "Shipping address is not displayed");
				Reporter.log("Shipping address is Verified");
			} else {
				moveToElement(shippingAddressTitle);
				Assert.assertTrue(shippingAddressTitle.isDisplayed(), "Shipping address is not displayed");
				Reporter.log("Shipping address is Verified");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Shipping address");
		}
		return this;
	}

	/**
	 * Verify Shipping Information
	 *
	 * @return
	 */
	public CartPage verifyShippingInformation() {
		try {
			Assert.assertTrue(shippingInfo.isDisplayed(), "Shipping Info is not displayed");
			Reporter.log("Shipping info is displayed");
		} catch (Exception e) {
			Assert.fail("Shipping info is not displayed");
		}
		return this;
	}

	public CartPage AcceptAndPlaceOrderButtonEnabled() {
		try {
			waitforSpinner();
			acceptAndPlaceOrderButton.get(1).isEnabled();
			Reporter.log("Accept and Place Order button in Cartpage is Enabled");
		} catch (Exception e) {
			Assert.fail("AcceptAndPlaceOrder Button in Cart Page Not Enabled");
		}
		return this;
	}

	/**
	 * Click Accept And Place Order
	 */
	public CartPage clickAcceptAndPlaceOrder() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(acceptAndPlaceOrderButton.get(0));
				clickElementWithJavaScript(acceptAndPlaceOrderButton.get(0));
				Reporter.log("Accept and Place order button is clickable");
			} else {
				moveToElement(acceptAndPlaceOrderButton.get(0));
				clickElementWithJavaScript(acceptAndPlaceOrderButton.get(0));
				Reporter.log("Accept and Place order button is clickable");

			}
		} catch (Exception e) {
			Assert.fail("Accept and Place order button is not clickable");
		}
		return this;
	}

	/**
	 * Verify TradeIn Body Text
	 *
	 * @return
	 */
	public CartPage verifyTradeInBodyText() {
		try {
			Assert.assertTrue(tradeInHeader.isDisplayed(), "Trade in header is not displayed");
			Reporter.log("Trade in header is displayed");
		} catch (Exception e) {
			Assert.fail("Trade in header is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Image
	 *
	 * @return
	 */
	public CartPage verifyDeviceImage() {
		try {
			waitforSpinner();
			Assert.assertTrue(deviceImage.isDisplayed(), "Device image is not available");
			Reporter.log("Device is displayed");
		} catch (Exception e) {
			Assert.fail("Device image is not displayed.");
		}
		return this;
	}

	/**
	 * Verify Financed Amount label in Order details
	 *
	 * @return
	 */
	public CartPage verifyOrderFinancedAmountLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(iOSorderDetailFinancedAmountlabel);
				Assert.assertTrue(iOSorderDetailFinancedAmountlabel.isDisplayed(), "Financed amount is not displayed");
			} else {
				Assert.assertTrue(orderDetailFinancedAmountlabel.isDisplayed(), "Financed amount is not displayed");
			}
			Reporter.log("Financed amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify financed amount");
		}
		return this;
	}

	/**
	 * verify financed amount label not displayed in order details tab when selected
	 * FRP on product detail
	 *
	 * @return
	 */
	public CartPage verifyFullFinancedAmountLabelFRPDisplayedInOrderDetails() {
		waitforSpinner();
		try {
			Reporter.log("financed amount is not displayed");
			Assert.assertFalse(isElementDisplayed(orderDetailFinancedAmountlabel), "financed amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify financed amount");
		}
		return this;
	}

	/**
	 * verify Financed amount label is not displayed in Payment tab when selected
	 * FRP on product detail
	 *
	 * @return
	 */
	public CartPage verifyFullFinancedAmountLabelFRPDisplayedInPayment() {

		try {
			Reporter.log("financed amount is not displayed");
			Assert.assertTrue(isElementDisplayed(paymentFinancedAmountLabel), "financed amount is displayed");
		} catch (Exception e) {
			Reporter.log("financed amount is not displayed");
		} catch (AssertionError e) {
			Assert.fail("failed to verify financed amount");
		}
		return this;
	}

	/**
	 * verify financed amount label in payment tab
	 *
	 * @return
	 */
	public CartPage verifyPaymentFinancedAmountLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(iOSPaymentsTabFinancedAmountlabel.get(4).isDisplayed(),
						"financed amount is not displayed");
			} else {
				Assert.assertTrue(paymentFinancedAmountLabel.isDisplayed(), "financed amount is not displayed");
			}
			Reporter.log("financed amount is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify financed amount");
		}
		return this;
	}

	/**
	 * Verify Accept & Continue Is Enabled or Disabled
	 *
	 * @return
	 */
	public CartPage verifyAcceptAndContinue() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(placeOrder), 30);
			boolean isAcceptAndContinueDisplayed = false;
			if (placeOrder.isEnabled()) {
				isAcceptAndContinueDisplayed = true;
			}
			Assert.assertTrue(isAcceptAndContinueDisplayed, "Accept & Continue Button in Cart Page Not Enabled");
			Reporter.log("Accept & Continue in Cartpage is Enabled");
		} catch (Exception e) {
			Assert.fail("Accept & Continue Button in Cart Page Not Enabled");
		}
		return this;
	}

	/**
	 * Verify Estimated Ship Date Format
	 *
	 * @return
	 */
	public boolean verifyEstimatedShipDateFormat() {
		String[] date = null;
		for (WebElement webElement : estimatedShipDateFormat) {
			if (webElement.isDisplayed()) {
				date = webElement.getText().trim().split("-");
				break;
			}
		}
		boolean checkFormat = false;
		for (String dateFormat : date) {
			if (dateFormat.trim().matches("([0-9]{2})/([0-9]{2})/([0-9]{4})")) {
				checkFormat = true;
			} else {
				break;
			}
		}
		return checkFormat;
	}

	/**
	 * verify device EIP Amount And Duration is displayed in cart page
	 *
	 * @return true/false
	 */
	public CartPage verifyEIPAmountAndDuration() {
		try {
			checkPageIsReady();
			Assert.assertTrue(deviceMonthlyPrice.isDisplayed(), "Device EIP amount and Duration is not displayed");
			Reporter.log("Device EIP amount and Duration is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify device EIP amount");
		}
		return this;
	}

	/**
	 * verify Edit Button is displayed in cart page
	 *
	 * @return true/false
	 */
	public CartPage verifyEditButtonIsDisplayed() {
		boolean isEditButtonDisplayed = false;
		try {
			waitForSpinnerInvisibility();
			if (editButtonOnDevice.isDisplayed()) {
				isEditButtonDisplayed = true;
				Assert.assertTrue(isEditButtonDisplayed, "Edit Button Is not Displayed");
				Reporter.log("Edit button is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Edit button is not displayed");
		}
		return this;
	}

	/**
	 * Click on Edit Button
	 *
	 * @return
	 */
	public CartPage clickEditButtonOnDevice() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				iOSeditButtonOnDevice.click();
			} else {
				moveToElement(editButtonOnDevice);
				editButtonOnDevice.click();
			}
		} catch (Exception e) {
			Assert.fail("Edit button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Address Line Label
	 */
	public CartPage verifyAddressLineLabel() {
		try {
			Assert.assertTrue(addressLineLabel.isDisplayed(), "Billing Address label is not displayed");
			Reporter.log("Address line label is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify billing address label");
		}
		return this;
	}

	/**
	 * Verify City Label
	 */
	public CartPage verifyCityLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				cityLabelText_iOS.isDisplayed();
				Reporter.log("City label text is displayed");
			} else {
				cityLabelText.isDisplayed();
				Reporter.log("City label text is displayed");
			}
		} catch (Exception e) {
			Assert.fail("City label text is not displayed");
		}
		return this;
	}

	public CartPage enterFirstname(String firstName) {
		waitforSpinner();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(paymentTabfirstNameOnCardForIOS);
				sendTextData(paymentTabfirstNameOnCardForIOS, firstName);
			} else {
				sendTextData(firstNameOnCard, firstName);
			}
		} catch (Exception e) {
			Assert.fail("First name is not displayed");
		}
		return this;

	}

	public CartPage verifyFirstname() {
		try {
			waitFor(ExpectedConditions.visibilityOf(firstNameOnCard), 10);
			Assert.assertTrue(firstNameOnCard.isDisplayed());
		} catch (Exception e) {
			Assert.fail("First name is not displayed");
		}
		return this;

	}

	public CartPage enterLastname(String lastName) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(paymentTablastNameOnCardForIOS);
				sendTextData(paymentTablastNameOnCardForIOS, lastName);
			} else {
				sendTextData(lastNameOnCard, lastName);
			}
		} catch (Exception e) {
			Assert.fail("Last name is not displayed");
		}
		return this;

	}

	public CartPage verifyLastname() {
		try {
			waitFor(ExpectedConditions.visibilityOf(firstNameOnCard), 10);
			Assert.assertTrue(lastNameOnCard.isDisplayed());
		} catch (Exception e) {
			Assert.fail("last name is not displayed");
		}
		return this;

	}

	public CartPage enterCardHoldersName(String cardName) {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(nameOncard), 10);
			sendTextData(nameOncard, cardName);
		} catch (Exception e) {
			Assert.fail("Card holder name field is not displayed");
		}
		return this;
	}

	public CartPage enterCardNumber(String creditCardNumber) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(paymentTabcardNumberForIOS);
				paymentTabcardNumberForIOS.click();
				sendTextData(paymentTabcardNumberForIOS, creditCardNumber);
				paymentTabexpirationDateForIOS.click();
			} else {
				sendTextData(cardNumber, creditCardNumber);
				cardNumber.sendKeys(Keys.ENTER);
				waitforSpinner();
			}
		} catch (Exception e) {
			Assert.fail("Card holder number field is not displayed");
		}
		return this;
	}

	public CartPage verifyCardNumber() {
		try {

			Assert.assertTrue(cardNumber.isDisplayed());
		} catch (Exception e) {
			Assert.fail("card number is not displayed");
		}
		return this;

	}

	public CartPage enterCVV(String cardCvv) {
		checkPageIsReady();
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				// waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("span.glueRippleAnimate")));
				paymentTabCvvForIOS.click();
				sendTextData(paymentTabCvvForIOS, cardCvv);
				paymentTabCvvForIOS.sendKeys(Keys.ENTER);
			} else {
				// waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("span.glueRippleAnimate")));
				waitFor(ExpectedConditions.elementToBeClickable(cvv), 30);
				sendTextData(cvv, cardCvv);
				cvv.sendKeys(Keys.ENTER);
			}
		} catch (Exception e) {
			Assert.fail("CVV field is not displayed");
		}
		return this;
	}

	public CartPage verifyCVVEntered() {
		try {

			Assert.assertTrue(cvv.isDisplayed());
		} catch (Exception e) {
			Assert.fail("cvv is not displayed");
		}
		return this;
	}

	public CartPage enterCardExpiryDate(String expiryDate) {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				paymentTabexpirationDateForIOS.click();
				sendTextData(paymentTabexpirationDateForIOS, expiryDate);
				paymentTabexpirationDateForIOS.sendKeys(Keys.ENTER);
			} else {
				sendTextData(expirationDate, expiryDate);

			}
		} catch (Exception e) {
			Assert.fail("Card expiry field is not displayed");
		}
		return this;
	}

	public CartPage verifyCardExpiryDate() {
		try {

			Assert.assertTrue(expirationDate.isDisplayed());
		} catch (Exception e) {
			Assert.fail("expirationDate is not displayed");
		}
		return this;
	}

	public CartPage getCCErrorMessage() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				paymentTabcardNumberForIOS.click();
				paymentTabfirstNameOnCardForIOS.click();
				Assert.assertTrue(mobileccErrorMessage.getText().equalsIgnoreCase("Enter a valid credit card number."));
				Reporter.log("Credit card error message displayed");
				paymentTabcardNumberForIOS.clear();
				paymentTabcardNumberForIOS.sendKeys("444444444444444448");
				checkPageIsReady();
				paymentTabcardNumberForIOS.sendKeys(Keys.TAB);
				waitforSpinner();
			} else {
				// cardNumber.sendKeys("1212121212121212");
				cardNumber.click();
				firstNameOnCard.click();
				checkPageIsReady();
				Assert.assertTrue(ccErrorMessage.getText().equalsIgnoreCase("Enter a valid credit card number."));
				Reporter.log("Credit card error message displayed");
				cardNumber.clear();
				cardNumber.sendKeys("444444444444444448");
				checkPageIsReady();
				cardNumber.sendKeys(Keys.TAB);
				waitforSpinner();
			}
		} catch (Exception e) {
			Assert.fail("Credit card error message not found");
		}
		return this;
	}

	/**
	 * Verify Edited Shipping Address
	 *
	 * @return
	 */
	public CartPage verifyEditedShippingAddress() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AndroidDriver) {
				Assert.assertTrue(shippingAddressTxt_iOS.getText().contains(STREET_ADDR),
						"Shipping address is not changed");
				Reporter.log("Shipping address is changed");
			} else if (getDriver() instanceof IOSDriver) {
				Reporter.log("Shipping address is changed");
			} else {
				Assert.assertTrue(shippingAddressTxt.getText().contains(STREET_ADDR),
						"Shipping address is not changed");
				Reporter.log("Shipping address is changed");
			}
		} catch (Exception e) {
			Assert.fail("Shipping address is not changed");
		}
		return this;
	}

	/**
	 * Click Continue To Shipping button
	 */
	public CartPage clickContinueToShippingButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(iOScontinueToShippingButton);
				waitFor(ExpectedConditions.visibilityOf(iOScontinueToShippingButton));
				clickElementWithJavaScript(iOScontinueToShippingButton);
			} else {
				waitFor(ExpectedConditions.visibilityOf(continueToShippingButton));
				continueToShippingButton.click();
			}
			Reporter.log("Clicked on Continue To Shipping button and Verified");
		} catch (Exception e) {
			Assert.fail("Continue To Shipping button is not clickable");
		}
		return this;
	}

	/**
	 * Click ContinueToPayment Button
	 */
	public CartPage clickContinueToPaymentButton() {
		waitforSpinner();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				if (iOSUpdatePaymentButton.size() == 1) {
					moveToElement(iOSUpdatePaymentButton.get(0));
					iOSUpdatePaymentButton.get(0).click();
					if (iOScontinueToPaymentButton.isDisplayed()) {
						waitFor(ExpectedConditions.elementToBeClickable(iOScontinueToPaymentButton), 10);
						iOScontinueToPaymentButton.click();
					}
				} else {
					moveToElement(iOScontinueToPaymentButton);
					clickElement(iOScontinueToPaymentButton);
				}
			} else {
				waitFor(ExpectedConditions.visibilityOf(continueToPaymentButton));
				clickElementWithJavaScript(continueToPaymentButton);
			}
			Reporter.log("Verified and Clicked on Continue to payment button");
		} catch (Exception e) {
			Assert.fail("Continue to payment button is not clickable");
		}
		return this;
	}

	/**
	 * Click Ship To A different Address
	 */
	public CartPage clickShipToDifferentAddress() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(mobileEditShippingCTA);
				Reporter.log("Clicked on Ship to different address button");
			} else {
				clickElementWithJavaScript(editShippingCTA);
				Reporter.log("Clicked on Ship to different address button");
			}
		} catch (Exception e) {
			Assert.fail("Ship to different address button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Shipping Details
	 */
	public CartPage verifyShippingDetails() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(iOSshippingDetails.isDisplayed(), "'Ship to:' is not displayed");
			} else {
				Assert.assertTrue(continueToPaymentButton.isDisplayed(), "'Continue to payment' CTA is not displayed");
			}
			Reporter.log("Shipping details page is displayed");
		} catch (Exception e) {
			Assert.fail("Shipping details page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Shipping Details
	 */
	public CartPage verifyAALShippingDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(aalShippingDetails));
			Assert.assertTrue(aalShippingDetails.isDisplayed(), "'Ship to:' is not displayed");

			Reporter.log("AAL Shipping details page is displayed");
		} catch (Exception e) {
			Assert.fail("AAL Shipping details page is not displayed");
		}
		return this;
	}

	/**
	 * Click Billing And Shipping CheckBox
	 */
	public CartPage clickBillingAndShippingCheckBox() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(iOSbillingAndShippingCheckBox);
				// waitFor(ExpectedConditions.visibilityOf(billingAndShippingCheckBox));
				if (!iOSshippingAddressCheckBox.isEmpty()) {
					clickElementWithJavaScript(iOSbillingAndShippingCheckBox);
				}
				Reporter.log("Clicked on Billing and shipping check box");
			} else {
				waitFor(ExpectedConditions.visibilityOf(billingAndShippingCheckBox));
				if (!shippingAddressCheckBox.isEmpty()) {
					clickElementWithJavaScript(billingAndShippingCheckBox);
				}
				Reporter.log("Clicked on Billing and shipping check box");
			}

		} catch (Exception e) {
			Assert.fail("User can not click Billing and shipping check box");
		}
		return this;
	}

	/**
	 * Verify Billing Header
	 */
	public CartPage verifyBillingHeader() {
		try {

			if (getDriver() instanceof AppiumDriver) {
				// waitFor(ExpectedConditions.visibilityOf(billingHeader));
				iOSbillingHeader.isDisplayed();
				Reporter.log("Billing header is displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(billingHeader));
				billingHeader.isDisplayed();
				Reporter.log("Billing header is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Billing header is displayed");
		}
		return this;
	}

	/**
	 * Verify Address Line One Label
	 */
	public CartPage verifyAddressLineOneLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				// waitFor(ExpectedConditions.visibilityOf(billingHeader));
				iOSaddressLineOneLabel.isDisplayed();
				Reporter.log("Address line one label is displayed");
			} else {
				addressLineOneLabel.isDisplayed();
				Reporter.log("Address line one label is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Billing Address label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Address Line Two Label
	 */
	public CartPage verifyAddressLineTwoLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				iOSaddressLineTwoLabel.isDisplayed();
				Reporter.log("Address line two label is displayed");
			} else {
				addressLineTwoLabel.isDisplayed();
				Reporter.log("Address line two label is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Billing Address two label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Payments tab City Label
	 */
	public CartPage verifyPaymentsTabCityLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				iOSpaymentsTabCityLabel.isDisplayed();
				Reporter.log("City label is displayed");
			} else {
				paymentsTabCityLabel.isDisplayed();
				Reporter.log("City label is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Billing city label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Payments Tab State Label
	 */
	public CartPage verifyPaymentsTabStateLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				iOSpaymentsTabStateLabel.isDisplayed();
				Reporter.log("State label is displayed");
			} else {
				paymentsTabStateLabel.isDisplayed();
				Reporter.log("State label is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Billing State label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Payments Tab ZIPcode Label
	 */
	public CartPage verifyPaymentsTabZIPcodeLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				iOSpaymentsTabZIPcodeLabel.isDisplayed();
				Reporter.log("ZIP code label is displayed");
			} else {
				paymentsTabZIPcodeLabel.isDisplayed();
				Reporter.log("ZIP code label is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Billing zip code label is not displayed");
		}
		return this;
	}

	/**
	 * Verify State Label
	 */
	public CartPage verifyStateLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				stateLabelText_iOS.isDisplayed();
				Reporter.log("State label is displayed");
			} else {
				stateLabel.isDisplayed();
				Reporter.log("State label is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Billing State label is not displayed");
		}
		return this;
	}

	/**
	 * Verify ZIP code Label
	 */
	public CartPage verifyZIPcodeLabel() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				zIPcodeLabelText_iOS.isDisplayed();
				Reporter.log("ZIP code label is displayed");
			} else {
				zIPcodeLabel.isDisplayed();
				Reporter.log("ZIP code label is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Billing zip code label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Payment Information form
	 *
	 * @return
	 */
	public CartPage verifyPaymentInformationForm() {
		waitforSpinner();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AndroidDriver) {
				waitFor(ExpectedConditions.visibilityOf(iOSpaymentInformationForm));
				Assert.assertTrue(iOSpaymentInformationForm.getText().trim().equalsIgnoreCase("Payment Information"),
						"Payment Information is not displayed");
			} else if (getDriver() instanceof IOSDriver) {
				Reporter.log("Payment Information form is displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(paymentInformationForm));
				Assert.assertTrue(paymentInformationForm.getText().trim().equalsIgnoreCase("Payment Information"),
						"Payment Information is not displayed");
			}
			Reporter.log("Payment Information form is displayed");
		} catch (Exception e) {
			Assert.fail("Payment Information form is not displayed");
		}
		return this;
	}

	/**
	 * Verify Address field Error Message
	 */
	public CartPage verifyAddressInvalidErrorMessage() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				addressLineOne_iPhone.clear();
				stateLabelText_iOS.click();
				waitFor(ExpectedConditions.visibilityOf(addressInvalidErrorMessage_iOS));
				Assert.assertTrue(addressInvalidErrorMessage_iOS.isDisplayed());
				Reporter.log("address line error message is displayed");
			} else {
				addressLineOne.clear();
				shippingCityTxt.click();
				waitFor(ExpectedConditions.visibilityOf(addressInvalidErrorMessage));
				Assert.assertTrue(addressInvalidErrorMessage.isDisplayed());
				Reporter.log("address line error message is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Invalid address error message is not displayed");
		}
		return this;
	}

	/**
	 * Click ShippingAddress Cancel Button
	 */
	public CartPage clickShippingAddressCancelButton() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(cancelButton_iPhone);
				clickElement(cancelButton_iPhone);
			} else {
				clickElement(cancelButton);
				Reporter.log("Clicked on Shipping Address cancel button");
			}
		} catch (Exception e) {
			Assert.fail("Shipping Address cancel button is not clickable");
		}
		return this;
	}

	/**
	 * Clear Address Line
	 *
	 * @param data
	 */
	public void clearAddressLineOne(String data) {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				sendTextData(addressLineOne_iPhone, data);
			} else {
				sendTextData(addressLineOne, data);
			}

		} catch (Exception e) {
			Assert.fail("Address line one field is not displayed");
		}
	}

	/**
	 * Verify Insurance Name
	 *
	 * @return
	 */
	public String verifyInsuranceName() {
		String socName = null;
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(getTextUponDisplay(insuranceName_iPhone).contains("Protection"));
				socName = insuranceName_iPhone.get(0).getText();
				Reporter.log("Insurance name is displayed");
			} else {
				moveToElement(insuranceName);
				Assert.assertTrue(insuranceName.getText().contains("Protection"), "Insurance name is not displayed");
				socName = insuranceName.getText();
				Reporter.log("Insurance name is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Insurance name is not displayed");
		}
		return socName;
	}

	/**
	 * Verify Insurance Edit Button
	 */
	public CartPage verifyInsuranceEditButton() {
		try {
			insuranceEditButton.isDisplayed();
			Reporter.log("Insurance edit button is displayed");
		} catch (Exception e) {
			Assert.fail("Insurance edit button is not displayed");
		}
		return this;
	}

	/**
	 * Verify Insurance Remove Button
	 */
	public CartPage verifyInsuranceRemoveButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(insuranceRemoveButton_iPhone.isDisplayed());
				Reporter.log("Insurance remove button is displayed");
			} else {
				Assert.assertTrue(insuranceRemoveButton.isDisplayed());
				Reporter.log("Insurance remove button is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Insurance remove button is not displayed");
		}
		return this;
	}

	/**
	 * Click Insurance Remove Button
	 */
	public CartPage clickInsuranceRemoveButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(insuranceRemoveButton_iPhone);
				clickElementWithJavaScript(insuranceRemoveButton_iPhone);
				Reporter.log("Clicked on Insurance remove button");
			} else {
				moveToElement(insuranceRemoveButton);
				clickElementWithJavaScript(insuranceRemoveButton);
				Reporter.log("Clicked on Insurance remove button");
			}
		} catch (Exception e) {
			Assert.fail("Insurance remove button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Remove Insurance Modal Window
	 */
	public CartPage verifyRemoveInsuranceModalWindow() {
		try {
			waitforSpinner();
			removeInsuranceModalWindow.isDisplayed();
			Reporter.log("Remove Insurance pop up window is displayed");
		} catch (Exception e) {
			Assert.fail("Remove insurance pop up window not displayed");
		}
		return this;
	}

	/**
	 * Click Remove Insurance Modal Window Ok Button
	 */
	public CartPage clickRemoveInsuranceModalWindowOkButton() {
		try {
			removeInsuranceModalWindowOkButton.click();
			waitforSpinner();
			Reporter.log("Clicked on Remove insurance model window ok button");
		} catch (Exception e) {
			Assert.fail("Remove insurance model window ok button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Insurance Cost Per Month
	 */
	public CartPage verifyInsuranceCostPerMonth() {
		try {
			Assert.assertTrue(SOCPrice.getText().contains("$"));
			Reporter.log("Insurance cost per month  $ symbol is displayed");
			Assert.assertTrue(SOCPrice.getText().contains("/mo"));
			Reporter.log("Insurance cost per month is displayed with /mo");
		} catch (Exception e) {
			Assert.fail("Insurance cost per month $ and /mo is not displayed");
		}
		return this;
	}

	/**
	 * Verify DueToday Verbiage Doller Symbol
	 */
	public CartPage verifyDueTodayVerbiageDollerSymbol() {
		try {
			Assert.assertTrue(verifyElementBytext(dueTodayVerbiageDollerSymbol, "$"));
			Reporter.log("Doller symbol is displayed in front of due today verbiage");
		} catch (Exception e) {
			Assert.fail("Doller Symbol is not displayed");
		}
		return this;
	}

	/**
	 * Verify Cancel Button
	 */
	public CartPage verifyCancelButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(cancelButton_iPhone);
				cancelButton_iPhone.isDisplayed();
				Reporter.log("Cancel button is displayed");
			} else {
				cancelButton.isDisplayed();
				Reporter.log("Cancel button is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Cancel button is not displaying");
		}
		return this;
	}

	/**
	 * Verify ShipToDifferentAddress Link
	 */
	public CartPage verifyShipToDifferentAddressLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileEditShippingCTA.isDisplayed();
				Reporter.log("Ship to different address link is displayed");
			} else {
				editShippingCTA.isDisplayed();
				Reporter.log("Ship to different address link is displayed");
			}
		} catch (Exception e) {
			Assert.fail("ShipToDiferentAddress Link is not displayed");
		}
		return this;
	}

	/**
	 * Verify Shipping Header
	 *
	 * @return CartPage
	 */
	public CartPage verifyShippingHeader() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				shippingHeader_iOS.isDisplayed();
				Reporter.log("Shipping header is displayed");
			} else {
				shippingHeader.isDisplayed();
				Reporter.log("Shipping header is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Shipping header is not displayed");
		}
		return this;
	}

	/**
	 * Get address lineOne details
	 */
	public String getAddressLineOne() {
		if (getDriver() instanceof AppiumDriver) {
			return addressLineOne_iPhone.getText();
		} else {
			return addressLineOne.getText();
		}
	}

	/**
	 * Verify device name in cart page
	 *
	 * @param deviceName
	 * @return
	 */
	public CartPage verifyDeviceName(String deviceName) {
		try {
			waitforSpinner();
			boolean isDeviceNameDisplayed = false;
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : deviceSpecificationsForIOS) {
					if (webElement.isDisplayed() && webElement.getText().contains(deviceName)) {
						isDeviceNameDisplayed = true;
						break;
					}
				}
			} else {
				for (WebElement webElement : deviceSpecifications) {
					if (webElement.isDisplayed() && webElement.getText().contains(deviceName)) {
						isDeviceNameDisplayed = true;
						break;
					}
				}
			}
			Assert.assertTrue(isDeviceNameDisplayed, "Device name is not displayed");
			Reporter.log("Device name is displayed");
		} catch (Exception e) {
			Assert.fail("Device name is not displayed");
		}
		return this;
	}

	/**
	 * Verify device color in cart page
	 *
	 * @param deviceColor
	 * @return
	 */
	public CartPage verifyDeviceColor(String deviceColor) {
		try {
			waitforSpinner();
			boolean isDeviceColorDisplayed = false;
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : deviceSpecificationsForIOS) {
					if (webElement.isDisplayed() && webElement.getText().contains(deviceColor)) {
						isDeviceColorDisplayed = true;
						break;
					}
				}
			} else {
				for (WebElement webElement : deviceSpecifications) {
					if (webElement.isDisplayed() && webElement.getText().contains(deviceColor)) {
						isDeviceColorDisplayed = true;
						break;
					}
				}
			}
			Assert.assertTrue(isDeviceColorDisplayed, "Device color is not displayed");
			Reporter.log("Device Color is displayed");
		} catch (Exception e) {
			Assert.fail("Device color is not displayed");
		}
		return this;
	}

	/**
	 * Verify device color in cart page
	 *
	 * @param deviceMemory
	 * @return
	 */
	public CartPage verifyDeviceMemory(String deviceMemory) {
		try {
			waitforSpinner();
			boolean isDeviceMemory = false;
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : deviceSpecificationsForIOS) {
					if (webElement.isDisplayed() && webElement.getText().contains(deviceMemory)) {
						isDeviceMemory = true;
						break;
					}
				}
			} else {
				for (WebElement webElement : deviceSpecifications) {
					if (webElement.isDisplayed() && webElement.getText().contains(deviceMemory)) {
						isDeviceMemory = true;
						break;
					}
				}
			}
			Assert.assertTrue(isDeviceMemory, "Device memory is not displayed");
			Reporter.log("Device memory is displayed");
		} catch (Exception e) {
			Assert.fail("Device memory is not displayed");
		}
		return this;
	}

	/**
	 * Verify Order Details Tick Marked Displayed
	 */
	public CartPage verifyOrderDetailsTickMarkedDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(orderDetalsTickMark_iOS.isDisplayed());
				Reporter.log("Order details Tab tick mark is displayed");
			} else {
				Assert.assertTrue(orderDetalsTickMark.isDisplayed());
				Reporter.log("Order details Tab tick mark is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Order Details Tick Mark is not displayed");
		}
		return this;
	}

	/**
	 * Verify Remove Insurance Modal Window Header message
	 */
	public CartPage verifyRemoveInsuranceModalWindowHeader() {
		try {
			Assert.assertTrue(removeInsuranceWindowHeaderMessage.getText().equalsIgnoreCase("Hold the phone !"),
					"Remove Model header is not displayed");
			Reporter.log("Remove Insurance modal header message is displayed");
		} catch (Exception e) {
			Assert.fail("Remove Model header is not displayed");
		}
		return this;
	}

	/**
	 * Verify Remove Insurance Modal Window Body Message
	 */
	public CartPage verifyRemoveInsuranceModalWindowBodyMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(removeInsuranceModalWindowBodyMessage.isDisplayed());
			Reporter.log("Remove Insurance modal body message is displayed");
		} catch (Exception e) {
			Assert.fail("Remove Model body message is not displayed");
		}
		return this;
	}

	/**
	 * Verify Groung Shipping Method
	 */
	public CartPage verifyUPSGroungShippingMethodOption() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				groundShippingMethod.isDisplayed();
				Reporter.log("Ground shipping method is Displayed and Verified");
			} else {
				Assert.assertTrue(uPSGroundShippingMethod.isDisplayed(), "Ground shipping method is not available");
				Reporter.log("Ground shipping method is Displayed and Verified");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Ground shipping method");
		}
		return this;
	}

	/**
	 * Verify Two day Shipping Method
	 */
	public CartPage verifyTwoDayShippingMethodOption() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				twoDayShippingMethodmobileView.isDisplayed();
				Reporter.log("Two day method is Displayed and Verified");

			} else {
				Assert.assertTrue(twoDayShippingMethod.isDisplayed(), "Two Day shipping method is not available");
				Reporter.log("Two day method is Displayed and Verified");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Two Day shipping method");
		}
		return this;
	}

	/**
	 * Verify PayMonthly Label
	 */
	public CartPage verifyPayMonthlyHeader() {
		try {
			waitforSpinner();
			boolean payMothlyLabel = false;
			for (WebElement webElement : payMonthlyHeader) {
				if (webElement.getText().equalsIgnoreCase("PAY MONTHLY")) {
					payMothlyLabel = true;
					break;
				}
			}
			Assert.assertTrue(payMothlyLabel, "Pay monthly label is not displayed");
			Reporter.log("Pay monthly verbiage is displayed");
		} catch (Exception e) {
			Assert.fail("Pay monthly label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promo Discount Verbage
	 */
	public CartPage verifyPromoDiscountVerbage() {
		try {
			boolean promoDiscounText = false;
			for (WebElement webElement : promoDiscountVerbage) {
				if (webElement.getText().equalsIgnoreCase("promo Discounts")) {
					promoDiscounText = true;
					break;
				}
			}
			Assert.assertTrue(promoDiscounText, "Promo discount verbage is not displayed");
			Reporter.log("Promo Discounts Verbage is displayed");
		} catch (Exception e) {
			Assert.fail("Promo discount verbage is not displayed");
		}
		return this;
	}

	/**
	 * Verify Monthly price striked
	 *
	 * @return
	 */
	public CartPage verifyPriceStriked() {
		try {
			Assert.assertTrue(monthlyPriceStriked.isDisplayed(), "Price is not striked");
			Reporter.log("Mothly price striked");
		} catch (Exception e) {
			Assert.fail("Price is not striked");
		}
		return this;
	}

	/**
	 * Verify Installments Months Price With Doller Symbol
	 */
	public CartPage verifyInstallmentsMonthsPriceWithDollerSymbol() {
		try {
			boolean installmentPriceDollersymbol = false;
			for (WebElement webElement : installmentsMonthsPriceWithDollerSymbol) {
				if (webElement.getText().contains("$")) {
					installmentPriceDollersymbol = true;
					break;
				}
			}
			Assert.assertTrue(installmentPriceDollersymbol, "Instalment months is not displayed");
			Reporter.log("Installments months price doller symbol is displayed");
		} catch (Exception e) {
			Assert.fail("Instalment months is not displayed");
		}
		return this;
	}

	/**
	 * Verify Instalment Months
	 */
	public CartPage verifyInstalmentMonths() {
		try {
			boolean instalmentmonths = false;
			for (WebElement webElement : dueTodayAndTaxLabel) {
				if (webElement.getText().contains("x 24 mos")) {
					instalmentmonths = true;
					break;
				}
			}
			Assert.assertTrue(instalmentmonths, "Instalment months is not displayed");
			Reporter.log("Instalment months is displayed");
		} catch (Exception e) {
			Assert.fail("Instalment months is not displayed");
		}
		return this;
	}

	/**
	 * Verify DueToday And Tax Label
	 */
	public CartPage verifyDueTodayAndTaxLabel() {
		try {
			boolean dueTodayLabel = false;
			for (WebElement webElement : dueTodayAndTaxLabel) {
				if (webElement.getText().contains("due today + tax")) {
					dueTodayLabel = true;
					break;
				}
			}
			Assert.assertTrue(dueTodayLabel, "Device due today and tax label is not displayed");
			Reporter.log("Due today +tax label is displayed");
		} catch (Exception e) {
			Assert.fail("Device due today and tax label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Pay In Full Header
	 */
	public CartPage verifyPayInFullHeader() {
		try {
			waitforSpinner();
			boolean payInFullLabel = false;
			for (WebElement webElement : payInFullHeader) {
				if (webElement.getText().equalsIgnoreCase("PAY IN FULL.")) {
					payInFullLabel = true;
					break;
				}
			}
			Assert.assertTrue(payInFullLabel, "Pay in full verbiage is not displayed");
			Reporter.log("Pay in full verbiage is displayed");
		} catch (Exception e) {
			Assert.fail("Pay in full verbiage is not displayed");
		}
		return this;
	}

	/**
	 * Verify Pay In Full Price With Doller Symbol
	 */
	public CartPage verifyPayInFullPriceWithDollerSymbol() {
		try {
			boolean fullPriceDollersymbol = false;
			for (WebElement webElement : payInFullPriceWithDollerSymbol) {
				if (webElement.getText().contains("$")) {
					fullPriceDollersymbol = true;
					break;
				}
			}
			Assert.assertTrue(fullPriceDollersymbol, "Pay in full price and doller symbol is not displayed");
			Reporter.log("Full price doller symbol is displayed");
		} catch (Exception e) {
			Assert.fail("Pay in full price and doller symbol is not displayed");
		}
		return this;
	}

	/**
	 * Click AddPromoCode Button
	 */
	public CartPage clickAddPromoCodeButton() {
		try {
			addPromoCodeButton.click();
			Reporter.log("Clicked on Add promo code button");
		} catch (Exception e) {
			Assert.fail("Add promo code button is not clickable");
		}
		return this;
	}

	/**
	 * Enter PromoCode Value
	 */
	public CartPage enterPromoCodeValue(String data) {
		try {
			addPromoCodeValue.clear();
			sendTextData(addPromoCodeValue, data);
			Reporter.log("entered promo code");
		} catch (Exception e) {
			Assert.fail("User unable to enter promo code");
		}
		return this;
	}

	/**
	 * Click Apply Button
	 */
	public CartPage clickApplyButton() {
		try {
			waitforSpinner();
			applyButton.click();
			Reporter.log("Clicked on Apply button");
		} catch (Exception e) {
			Assert.fail("Apply button is not clickable");
		}
		return this;
	}

	/**
	 * Verify PromoCode Success Green Mark
	 */
	public CartPage verifyPromoCodeSuccessGreenMark() {
		try {
			waitforSpinner();
			Assert.assertTrue(promoCodeSuccessGreenMark.isDisplayed(), "Promo code success tick mark is not displayed");
			Reporter.log("Promo sucess message is displayed");
		} catch (Exception e) {
			Assert.fail("Promo code success tick mark is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promo Discount Price
	 */
	public CartPage verifyPromoDiscountPriceSign() {
		try {
			boolean discountPriceSign = false;
			for (WebElement webElement : promoDiscountPriceSign) {
				if (webElement.getText().contains("-")) {
					discountPriceSign = true;
					break;
				}
			}
			Assert.assertTrue(discountPriceSign, "Promo discount price sign is not displayed");
			Reporter.log("Promo discount price sign is displayed");
		} catch (Exception e) {
			Assert.fail("Promo discount price sign is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promo discount price
	 */
	public CartPage verifyPromoDiscountPrice() {
		try {
			Assert.assertTrue(promoDiscountPrice.isDisplayed(), "Promo discount price is not displayed");
			Reporter.log("Promo discount price is displayed");
		} catch (Exception e) {
			Assert.fail("Promo discount price is not displayed");
		}
		return this;
	}

	/**
	 * Verify PromoCode Sucess Message
	 */
	public CartPage verifyPromoCodeSuccessfulMessage() {
		try {
			Assert.assertTrue(promoCodeSuccessMessage.isDisplayed(), "Promo Code sucess message is not displayed");
			Reporter.log("Promo Code Sucessful message is displayed");
		} catch (Exception e) {
			Assert.fail("Promo Code sucess message is not displayed");
		}
		return this;
	}

	/**
	 * Click AddPromo code cancel button
	 */
	public CartPage clickAddPromoCodeCancelButton() {
		try {
			promoCodeCancelButton.click();
			Reporter.log("Clicked on Add promo code cancel button");
		} catch (Exception e) {
			Assert.fail("Add promo code cancel button is not clickable");
		}
		return this;
	}

	/**
	 * Verify ApplyButton Disable
	 */
	public CartPage verifyPromoApplyButton() {
		try {
			Assert.assertFalse(applyButton.isEnabled());
			Reporter.log("Promo apply button is disabled");
		} catch (Exception e) {
			Assert.fail("Promo apply button is not disable mode");
		}
		return this;
	}

	/**
	 * Verify Shipping Date For Device
	 */
	public CartPage verifyShippingDateForDevice() {
		try {
			Assert.assertTrue(estimatedShippingDateInOrderDetailPage.isDisplayed(),
					"Device shipping date is not displayed");
			Reporter.log("Ship from date and ship to date for device is displayed");
		} catch (Exception e) {
			Assert.fail("Device shipping date is not displayed");
		}
		return this;
	}

	/**
	 * Verify Remove Accessory Model Window
	 */
	public CartPage verifyRemoveAccessoryModelWindow() {
		try {
			waitforSpinner();
			Assert.assertTrue(removeAccessoryModelWindow.isDisplayed(), "Accessory model window is not displayed");
			Reporter.log("Accessory model window is displayed");
		} catch (Exception e) {
			Assert.fail("Accessory model window is not displayed");
		}
		return this;
	}

	/**
	 * Verify How It Calculated Sales Tax Link
	 */
	public CartPage verifyHowItCalculatedSalesTaxLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(mobileHowItCalculatedSalesTaxLink.get(1));
				Assert.assertTrue(mobileHowItCalculatedSalesTaxLink.get(1).isDisplayed(),
						"how it calculated tax link not displayed");
			} else {
				Assert.assertTrue(howItCalculatedSalesTaxLink.isDisplayed(),
						"how it calculated tax link not displayed");
			}
			Reporter.log("How it calculated tax link is displayed");
		} catch (Exception e) {
			Assert.fail("how it calculated tax link not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow header
	 */
	public CartPage verifyTaxBreakDownHeader() {
		try {
			Assert.assertTrue(taxBreakDownHeader.isDisplayed(), "Tax break down window header not displayed");
			Reporter.log("Tax breadown window header is displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window header not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sub title
	 */
	public CartPage verifySalesTaxSubTitle() {
		try {
			Assert.assertTrue(salesTaxSubTitle.isDisplayed(), "Tax break down window sub title not displayed");
			Reporter.log("Sales tax sub title is displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window sub title not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow device name
	 */
	public CartPage verifyTaxBreakDownWindowDeviceName() {
		try {
			taxBreakDownWindowDeviceName.isDisplayed();
			Reporter.log("Tax break down window device name displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window device name not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow device price
	 */
	public CartPage verifyTaxBreakDownWindowDevicePrice() {
		try {
			taxBreakDownWindowDevicePrice.isDisplayed();
			Reporter.log("Tax break down window device price displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window device price not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sub total label
	 */
	public CartPage verifyTaxBreakDownWindowPriceSubTotalLabel() {
		try {
			taxBreakDownWindowPriceSubTotalLabel.isDisplayed();
			Reporter.log("Tax break down window sub total label displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window sub total label not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sub total price
	 */
	public CartPage verifyTaxBreakDownWindowPriceSubTotalPrice() {
		try {
			taxBreakDownWindowPriceSubTotalPrice.isDisplayed();
			Reporter.log("Tax break down window sub total price displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window sub total price not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sales Tax label
	 */
	public CartPage verifyTaxBreakDownWindowSalesTaxLabel() {
		try {
			taxBreakDownWindowSalesTaxLabel.isDisplayed();
			Reporter.log("Tax break down window sales tax label displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window sales tax label not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sales Tax price
	 */
	public CartPage verifyTaxBreakDownWindowSalesTaxPrice() {
		try {
			taxBreakDownWindowSalesTaxPrice.isDisplayed();
			Reporter.log("Tax break down window sales tax price displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window sales tax price not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sales Tax Legal Text
	 */
	public CartPage verifyTaxBreakDownWindowSalesTaxLegalText() {
		try {
			taxBreakDownWindowSalesTaxLegalDisclaimer.isDisplayed();
			Reporter.log("Tax break down window sales tax leagal copy displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window sales tax leagal copy not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow GotIt Button
	 */
	public CartPage verifyTaxBreakDownWindowGotItButton() {
		try {
			taxBreakDownWindowGotItButton.isDisplayed();
			Reporter.log("Tax break down window got it button displayed");
			clickElement(taxBreakDownWindowGotItButton);
			Reporter.log("closed got it pop up");
		} catch (Exception e) {
			Assert.fail("Tax break down window got it button not displayed");
		}
		return this;
	}

	/**
	 * Click HowItCalculatedSalesTax Link
	 */
	public CartPage clickHowItCalculatedSalesTaxLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileHowItCalculatedSalesTaxLink.get(1).click();
			} else {
				howItCalculatedSalesTaxLink.click();
			}
			Reporter.log("Clicked on How it calculated sales tax link");
		} catch (Exception e) {
			Assert.fail("How it calculated sales tax link is not clickable");
		}
		return this;
	}

	/**
	 * Verify HowItCalculated Tax Break Down Window
	 */
	public CartPage verifyHowItCalculatedTaxBreakDownWindow() {
		try {
			waitforSpinner();
			verifyTaxBreakDownHeader();
			verifySalesTaxSubTitle();
			verifyTaxBreakDownWindowDeviceName();
			verifyTaxBreakDownWindowDevicePrice();
			verifyTaxBreakDownWindowPriceSubTotalLabel();
			verifyTaxBreakDownWindowPriceSubTotalPrice();
			verifyTaxBreakDownWindowSalesTaxLabel();
			verifyTaxBreakDownWindowSalesTaxPrice();
			verifyTaxBreakDownWindowSalesTaxLegalText();
			verifyTaxBreakDownWindowGotItButton();
		} catch (Exception e) {
			Assert.fail("HowItCalculated Tax Break Down Window errors");
		}
		return this;
	}

	/**
	 * Verify HowItCalculated Tax Break Down Window AAL Non BYOD
	 */
	public CartPage verifyHowItCalculatedTaxBreakDownWindowAALNonBYOD() {
		try {
			waitforSpinner();
			verifyTaxBreakDownHeader();
			verifySalesTaxSubTitle();
			verifyTaxBreakDownWindowDeviceName();
			verifyTaxBreakDownWindowDevicePrice();
			float devicePrice = getTaxBreakDownWindowDevicePrice();
			verifyTaxBreakDownWindowSimStarterKitText();
			verifyTaxBreakDownWindowSimStarterKitPrice();
			float simPrice = getTaxBreakDownWindowSimStarterKitPrice();
			verifyTaxBreakDownWindowPriceSubTotalLabel();
			verifyTaxBreakDownWindowPriceSubTotalPrice();
			float totalPrice = getTaxBreakDownWindowSubTotalPrice();
			verifyTaxBreakDownWindowSalesTaxLabel();
			verifyTaxBreakDownWindowSalesTaxPrice();
			verifyTaxBreakDownWindowSalesTaxLegalText();
			verifyTaxBreakDownWindowGotItButton();
			float devicePlusSim = devicePrice + simPrice;
			Assert.assertEquals(devicePlusSim, totalPrice, "Total Value is not as expected");

		} catch (Exception e) {
			Assert.fail("HowItCalculated Tax Break Down Window errors");
		}
		return this;
	}

	/**
	 * Verify HowItCalculated Tax Break Down Window AAL BYOD
	 */
	public CartPage verifyHowItCalculatedTaxBreakDownWindowAALBYOD() {
		try {
			waitforSpinner();
			verifyTaxBreakDownHeader();
			verifySalesTaxSubTitle();
			verifyTaxBreakDownWindowSimStarterKitText();
			verifyTaxBreakDownWindowSimStarterKitPrice();
			verifyTaxBreakDownWindowPriceSubTotalLabel();
			verifyTaxBreakDownWindowPriceSubTotalPrice();
			verifyTaxBreakDownWindowSalesTaxLabel();
			verifyTaxBreakDownWindowSalesTaxPrice();
			verifyTaxBreakDownWindowSalesTaxLegalText();
			verifyTaxBreakDownWindowGotItButton();
		} catch (Exception e) {
			Assert.fail("HowItCalculated Tax Break Down Window errors");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sim Starter Kit name
	 */
	public CartPage verifyTaxBreakDownWindowSimStarterKitText() {
		try {
			taxBreakDownWindowSimStarterKitText.isDisplayed();
			Reporter.log("Tax break down window device name displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window device name not displayed");
		}
		return this;
	}

	/**
	 * Verify TaxBreakDownWindow Sim Starter Kit price
	 */
	public CartPage verifyTaxBreakDownWindowSimStarterKitPrice() {
		try {
			taxBreakDownWindowSimStarterKitPrice.isDisplayed();
			Reporter.log("Tax break down window device price displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window device price not displayed");
		}
		return this;
	}

	/**
	 * Verify AddAccessory Button
	 */
	public CartPage verifyAddAccessoryButton() {
		try {
			waitforSpinner();
			Assert.assertTrue(accessoryCardCTA.isDisplayed());
			Reporter.log("Add accessory button is not displayed");
		} catch (Exception e) {
			Assert.fail("Add accessory button is displayed");
		}
		return this;
	}

	/**
	 * Verify Accessory Device Edit Button
	 */
	public CartPage verifyAccessoryDeviceEditButton() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(accessoryEditButton), "Accessory edit button is displayed");
			Reporter.log("Accessory edit button is not displayed");
		} catch (Exception e) {
			Assert.fail("Accessory edit button is displayed");
		}
		return this;
	}

	/**
	 * Verify StrikeOut Priceing
	 */
	public CartPage verifyStrikeOutPriceing() {
		try {
			Assert.assertFalse(payInFullPriceWithDollerSymbol.get(0).isDisplayed(), "Price is striked");
			Reporter.log("Price is not striked");
		} catch (Exception e) {
			Assert.fail("Price is striked");
		}
		return this;
	}

	/**
	 * Veify Promo Details For Eip
	 */
	public CartPage veifyPromoDetailsForEip() {
		try {
			verifyPayMonthlyHeader();
			verifyPriceStriked();
			verifyInstallmentsMonthsPriceWithDollerSymbol();
			verifyPromoDiscountVerbage();
			verifyInstalmentMonths();
			verifyDueTodayAndTaxLabel();
		} catch (Exception e) {
			Assert.fail("Promo Details For Eip errors");
		}
		return this;
	}

	/**
	 * Verify Promo Code Not Required Message
	 */
	public CartPage verifyPromoCodeNotRequiredMessage() {
		try {
			Assert.assertTrue(promoCodeNotRequiredMessage.isDisplayed(),
					"Promo code not required message is not displayed");
			Reporter.log("Promo code not required message is displayed");
		} catch (Exception e) {
			Assert.fail("Promo code not required message is not displayed");
		}
		return this;
	}

	/**
	 * Verify PromoCode Success Message
	 */
	public CartPage verifyPromoCodeSuccessMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(promoSuccessMessage.isDisplayed(), "Promo code success message is not displayed");
			Reporter.log("Promo sucess message is displayed");
		} catch (Exception e) {
			Assert.fail("Promo code success message is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Cost Is Zero
	 */
	public CartPage verifyDeviceCostIsZero() {
		try {
			Assert.assertTrue(deviceCostIsZero.isDisplayed(), "Device cost zero is not displayed");
			Reporter.log("Device cost is zero is displayed");
		} catch (Exception e) {
			Assert.fail("Device cost zero is not displayed");
		}
		return this;
	}

	/**
	 * Verify PromoCode UnSucessful Message
	 */
	public CartPage verifyPromoCodeUnSucessfulMessage() {
		try {
			Assert.assertTrue(promoCodeUnsucessMessage.isDisplayed(),
					"Promo Code invalid error message is not displayed");
			Reporter.log("Promo failure message is displayed");
		} catch (Exception e) {
			Assert.fail("Promo Code invalid error message is not displayed");
		}
		return this;
	}

	/**
	 * Verify Your Device Cost Is Zero Today Message
	 */
	public CartPage verifyYourDeviceCostIsZeroTodayMessage() {
		try {
			Assert.assertTrue(yourDeviceCostIsZeroTodayMessage.isDisplayed(),
					"Device cost zero message is not displayed");
			Reporter.log("Device cost is zero message is displayed");
		} catch (Exception e) {
			Assert.fail("Device cost zero message is not displayed");
		}
		return this;
	}

	/**
	 * Click Monthly Charges Break Down Link
	 */
	public CartPage clickMonthlyChargesBreakDownLink() {
		try {
			clickElement(monthlyChagesBreakDownLink);
			Reporter.log("Monthly charges break down link is clickable");
		} catch (Exception e) {
			Assert.fail("Monthly charges break down link is not clickable");
		}
		return this;
	}

	/**
	 * Verify Monthly Charges Window
	 */
	public CartPage verifyMonthlyChargesWindow() {
		try {
			Assert.assertTrue(monthlyCharges.isDisplayed());
			Reporter.log("Monthly Charges model window is displayed");
		} catch (Exception e) {
			Assert.fail("Monthly charges model is not displayed");
		}
		return this;
	}

	/**
	 * Verify ApplyButton Is Disable
	 */
	public CartPage verifyApplyButtonIsDisable() {
		try {
			Assert.assertTrue(applyButton.isDisplayed());
			Reporter.log("Apply button is disabled");
		} catch (Exception e) {
			Assert.fail("Apply button is not disable mode");
		}
		return this;
	}

	/**
	 * Verify PromoCode Success Message
	 */
	public CartPage verifyFirstTimePromoCodeSuccessMessage() {
		try {
			waitforSpinner();
			boolean flag = false;
			for (WebElement webElement : promoCodeFirstTimeSuccessMessage) {
				if (webElement.getText().contains("Successful"))
					flag = true;
				break;
			}
			Assert.assertTrue(flag);
		} catch (Exception e) {
			Assert.fail("");
		}
		return this;
	}

	/**
	 * Verify Promo Code Applied Message
	 */
	public CartPage verifyPromoCodeAppliedMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(promoCodeAppliesMessage.isDisplayed(), "Promo code applied message is not displayed");
			Reporter.log("Promo code applied message is displayed");
		} catch (Exception e) {
			Assert.fail("Promo code applied message is not displayed");
		}
		return this;
	}

	public CartPage verifyPromoCodeFirstTimeSucessMessage() {
		try {
			verifyFirstTimePromoCodeSuccessMessage();
			Reporter.log("Promo Sucessful message is displayed");
		} catch (Exception e) {
			Assert.fail("Promo code success is not displayed");
		}
		return this;
	}

	/**
	 * Verify that monthly amount from accessory page is equal to monthly amount
	 * from Cart page
	 */
	public CartPage compareAccessoryMonthlyAmounts(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue,
					"Monthly amount from Accessory is not matched with Monthly amount from cart");
			Reporter.log("Monthly amount from Accessory is matched with Monthly amount from cart");
		} catch (Exception e) {
			Assert.fail("Failed to compare Monthly Amount from accesory page to Cart page");
		}
		return this;
	}

	/**
	 * Get Due Monthly Total amount
	 *
	 * @return
	 */
	public String getDueMonthlyTotalCartPage() {
		if (getDriver() instanceof AppiumDriver) {
			moveToElement(dueMonthlyTotal.get(0));
			return dueMonthlyTotal.get(0).getText().replace("$", "");
		} else {
			return dueMonthlyTotal.get(0).getText().replace("$", "");
		}
	}

	/**
	 * Verify that installment loan terms is displayed for all devices in cart
	 */
	public CartPage verifyEIPLegalText() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(iOSeipLegalText);
				Assert.assertTrue(iOSeipLegalText.isDisplayed(), "EIP legal text is not displayed");
				Reporter.log("EIP legal text is displayed in Cart page");
			} else {
				Assert.assertTrue(eipLegalText.isDisplayed(), "EIP legal text is not displayed");
				Reporter.log("EIP legal text is displayed in Cart page");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP legal text in Cart page");
		}
		return this;
	}

	/**
	 * Verify that installment loan terms is displayed for all devices in cart
	 */
	public CartPage verifyNoEIPLegalText() {
		try {
			checkPageIsReady();
			Assert.assertFalse(noEipLegalText.get(0).isDisplayed());
			Reporter.log("EIP legal text is not displayed in Cart page");
		} catch (Exception e) {
			Assert.fail("EIP legal text is displayed in Cart page");
		}
		return this;
	}

	/**
	 * Get Due Monthly Total amount Integer format
	 *
	 * @return
	 */
	public Double getDueMonthlyTotalIntegerCartPage() {
		String monthlyPayment;
		monthlyPayment = dueMonthlyTotal.get(0).getText();
		String due = monthlyPayment.substring(monthlyPayment.lastIndexOf("$") + 1);
		return Double.parseDouble(due);
	}

	/**
	 * Verify that Due monthly text is displayed in cart
	 */
	public CartPage verifyDueMonthlyText() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(dueMonthlyText.get(1));
				Assert.assertTrue(dueMonthlyText.get(1).isDisplayed(), "Due Monthly text is not displayed");
			} else {
				moveToElement(dueMonthlyTotalLabel);
				Assert.assertTrue(dueMonthlyTotalLabel.isDisplayed(), "Due Monthly text is not displayed");
				Assert.assertTrue(dueMonthlyTotal.get(0).isDisplayed(), "Due Monthly total is not displayed");
			}
			Reporter.log("Due monthly total text is displayed in Cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify DUe monthly total text in Cart page");
		}
		return this;
	}

	/**
	 * Get Device Monthly amount Integer format
	 *
	 * @return
	 */
	public Double getDeviceMonthlyIntegerCartPage() {
		checkPageIsReady();
		String monthlyPayment;
		monthlyPayment = deviceMonthlyPrice.getText();
		monthlyPayment = monthlyPayment.substring(monthlyPayment.lastIndexOf("$") + 1);
		Reporter.log("Device monthly payment is: " + monthlyPayment);
		return Double.parseDouble(monthlyPayment);
	}

	/**
	 * Get Accessory Monthly amount Integer format
	 *
	 * @return
	 */
	public Double getAccessoryMonthlyIntegerCartPage(String numberOfAccessories) {
		Double sum = 0.00;
		double rounded = 0.00;
		int numberOfAccessoriesToSelect = Integer.parseInt(numberOfAccessories);
		try {
			String monthlyPayment = null;
			for (int i = 1; i <= numberOfAccessoriesToSelect; i++) {
				monthlyPayment = accessoryMonthlyPrice.get(i).getText();
				monthlyPayment = monthlyPayment.substring(monthlyPayment.lastIndexOf("$") + 1);
				Reporter.log("Accessory #" + i + " monthly payment is: " + Double.parseDouble(monthlyPayment));
				sum = sum + Double.parseDouble(monthlyPayment);
				rounded = (double) Math.round(sum * 100) / 100;
			}
			Reporter.log("Summary of Accessories is: " + sum);
		} catch (Exception e) {
			Assert.fail("Failed to identify Accessory Summary");
		}
		return rounded;
	}

	/**
	 * Verify that installment loan terms in months is displayed in Legal text for
	 * all devices in cart
	 */
	public CartPage verifyEIPTermIsPresentInLegalText() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Reporter.log("Shortest EIP term in legal text is displayed");
			} else {
				int[] eipTerms = new int[eipMonthTerms.size()];
				for (int i = 0; i <= eipMonthTerms.size() - 1; i++) {
					String term = eipMonthTerms.get(i).getText();
					term = term.substring(term.lastIndexOf("X") + 2, term.lastIndexOf("m") - 1);
					int termInt = Integer.parseInt(term.trim());
					eipTerms[i] = termInt;
				}
				Arrays.sort(eipTerms);
				Reporter.log("Shortest EIP term is: " + eipTerms[0]);
				Assert.assertTrue(eipLegalText.getText().contains(eipTerms[0] + " months"),
						"Shortest EIP term is not correct in legal text");
				waitforSpinner();
				String shortestterm = eipLegalText.getText();
				String month = shortestterm.substring(shortestterm.lastIndexOf("(") + 1,
						shortestterm.lastIndexOf("s)") + 1);
				Assert.assertTrue(eipLegalText.getText().contains(month),
						"Shortest EIP term is not visible in legal text");
				Reporter.log("Shortest EIP term in legal text is displayed and its correct in Cart page");

			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Shortest EIP term in legal text at Cart page");
		}
		return this;
	}

	/**
	 * Verify EIP month terms for device and accessories
	 */
	public CartPage verifyEIPTermForDeviceAndAccessories() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement eipTerms : iOSeipMonthTerms) {
					Assert.assertTrue(eipTerms.isDisplayed(), "EIP monthly installments text is displayed");
					Assert.assertTrue(eipTerms.getText().contains("mo"), "EIP Loan Term is not displayed correctly");
				}
				Reporter.log("EIP monthly installments text is displayed for all devices and accessories");
			} else {
				for (WebElement eipTerms : eipMonthTerms) {
					Assert.assertTrue(eipTerms.isDisplayed(), "EIP monthly installments text is displayed");
					Assert.assertTrue(eipTerms.getText().contains("mos"), "EIP Loan Term is not displayed correctly");
				}
				Reporter.log("EIP monthly installments text is displayed for all devices and accessories");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify EIP loan length term");
		}
		return this;
	}

	/**
	 * Verify Due Today Text at cart page
	 */
	public CartPage verifyDueTodayText(int NoOfDevicesAndAccesory) {
		try {
			waitforSpinner();

			if (getDriver() instanceof AppiumDriver) {
				for (int i = 1; i <= NoOfDevicesAndAccesory; i++) {
					Assert.assertTrue(iOSdueTodayText.get(i).isDisplayed(), "Due Todate Text is not displayed");
					Assert.assertTrue(iOSdueTodayText.get(i).getText().contains("today"),
							"Due Todate Text is not displayed");
				}
				Reporter.log("Due today Text is displayed");
			} else {
				for (int i = 1; i <= NoOfDevicesAndAccesory; i++) {
					Assert.assertTrue(dueTodayText.get(i).isDisplayed(), "Due Todate Text is not displayed");
					Assert.assertTrue(dueTodayText.get(i).getText().contains("today"),
							"Due Todate Text is not displayed");
				}
				Reporter.log("Due today Text is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display Due today Text");
		}
		return this;
	}

	/**
	 * Verify Due Today Text at cart page
	 */
	public CartPage verifyMonthlyPayment(int NoOfDevicesAndAccesory) {
		try {
			waitforSpinner();

			if (getDriver() instanceof AppiumDriver) {
				for (int i = 0; i <= NoOfDevicesAndAccesory - 1; i++) {
					Assert.assertTrue(iOSaccessoryMonthlyPrice.get(i).isDisplayed(),
							"Monthly Payment is not displayed");
					Assert.assertTrue(iOSaccessoryMonthlyPrice.get(i).getText().contains("$"),
							"Monthly Payment is not displayed");
				}
				Reporter.log("Monthly Payment is displayed");
			} else {
				for (int i = 0; i <= NoOfDevicesAndAccesory - 1; i++) {
					Assert.assertTrue(accessoryMonthlyPrice.get(i).isDisplayed(), "Monthly Payment is not displayed");
					Assert.assertTrue(accessoryMonthlyPrice.get(i).getText().contains("$"),
							"Monthly Payment is not displayed");
				}
				Reporter.log("Monthly Payment is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display Monthly Payment");
		}
		return this;
	}

	/**
	 * Verify Device EIPMonthly Installment Terms in Trade In Promo
	 *
	 * @return
	 */
	public CartPage verifyDeviceEIPInstallmentTermsInTradeInPromo() {
		try {
			String promoText = tradeinPromoText.getText();
			Assert.assertTrue(tradeinPromoText.isDisplayed());
			Assert.assertTrue(promoText.contains("mo"), "EIP Loan Term is not displayed correctly");
			Reporter.log("EIP Monthly Installment Term is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Installment Term");
		}
		return this;
	}

	/**
	 * Verify that Promo Monthly Trade-In value is displayed in cart
	 */
	public CartPage verifyMonthlyEIPTradeInPrice() {
		try {
			Assert.assertTrue(promotionDiscountPriceNonTradeIn.isDisplayed(),
					"Monthly Trade-In value is not displayed");
			Reporter.log("Monthly Trade-In value is displayed in Cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly Trade-In value");
		}
		return this;
	}

	/**
	 * Verify Shipping Price is displayed
	 *
	 * @return
	 */
	public CartPage verifyEstShippingIsDisplayedInOrderDetailPage() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(estShippingText),
					"'Est. shipping' is not displayed in Order Details page");
			Assert.assertTrue(estShipping.isDisplayed(), "Est. shipping price is not displayed");
			Assert.assertTrue(estShipping.getText().contains("$"), "$ is not present in Est. shipping price.");
			Reporter.log("Est. shipping is displayed with amount");
		} catch (Exception e) {
			Assert.fail("Failed to verify Est. Shipping");
		}
		return this;
	}

	/**
	 * Get Shipping price Integer format
	 *
	 * @return
	 */
	public Double getShippingpriceIntegerJumpCartPage() {
		String shipping;
		shipping = estShipping.getText();
		String[] arr = shipping.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Verify Taxes and Fees Price is displayed
	 *
	 * @return
	 */
	public CartPage verifyEstSalesTaxesIsDisplayedInOrderDetail() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(estSalesTaxText), "'Est sales tax' text is not displayed");
			Assert.assertTrue(estSalesTax.isDisplayed(), "Est sales tax price is not displayed");
			Assert.assertTrue(estSalesTax.getText().contains("$"), "$ is not present in Est sales tax price.");
			Reporter.log("Est sales taxes is displayed with amount");
		} catch (Exception e) {
			Assert.fail("Failed to verify Est sales tax");
		}
		return this;
	}

	/**
	 * Get Taxes price Integer format
	 *
	 * @return
	 */
	public Double getTaxesPriceIntegerJumpCartPage() {
		String taxesAndFees;
		taxesAndFees = estSalesTax.getText();
		String[] arr = taxesAndFees.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Verify Order total Price is displayed
	 *
	 * @return
	 */
	public CartPage verifyOrderEstTotalDueTodayIsDisplayedInOrderDetail() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(estTotalDueTodayText), "'Est total due today' text is not displayed");
			Assert.assertTrue(estTotalDueToday.isDisplayed(), "Est total due today price is not displayed");
			Assert.assertTrue(estTotalDueToday.getText().contains("$"),
					"$ is not present in Est total due today price.");
			Reporter.log("Est total due today is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Est total due today");
		}
		return this;
	}

	/**
	 * Get Order total price Integer format
	 *
	 * @return
	 */
	public Double getOrderTotalPriceIntegerJumpCartPage() {
		String orderTotal;
		double rounded = 0.00;
		orderTotal = estTotalDueToday.getText();
		String[] arr = orderTotal.split("\\$", 0);
		rounded = (double) Math.round(Double.parseDouble(arr[1]) * 100) / 100;
		return rounded;
	}

	/**
	 * Verify Order Toatl = Downpayment + shipping + taxes and fees
	 *
	 * @return
	 */
	public CartPage verifyOrderTotalSum(Double dueTodayTotal, Double sum) {
		try {
			waitforSpinner();
			Assert.assertEquals(dueTodayTotal, sum,
					"Sum of Downpayment + shipping + taxes and fees is not equal to the order total");
			Reporter.log("Sum of Downpayment + shipping + taxes and fees is equal to the order total");
		} catch (Exception e) {
			Assert.fail("Failed to match Sum of Downpayment + shipping + taxes and the order total");
		}
		return this;
	}

	/**
	 * Verify that EIP Error Modal
	 */
	public CartPage verifyEIPErrorModal() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(eipErrorModal));
			Assert.assertTrue(eipErrorModal.isDisplayed(), "EIP Error Modal is not displayed");
			Reporter.log("EIP Error Modal is displayed in Cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Error Modal");
		}
		return this;
	}

	/**
	 * Verify that Got It button visible on EIP Error Modal
	 */
	public CartPage verifyGotItButton() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(gotItButton));
			Assert.assertTrue(gotItButton.isDisplayed(), "Got It button is not displayed");
			Reporter.log("Got It button is displayed in Cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Got It button");
		}
		return this;
	}

	/**
	 * Verify Trade In promo text is displayed
	 *
	 * @return
	 */
	public CartPage verifyTradeInPromoTextIsDisplayed() {
		try {
			Assert.assertTrue(tradeInPromoText.get(0).isDisplayed(), "Trade In promo text is not displayed");
			Reporter.log("Trade In promo text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Trade In promo text");
		}
		return this;
	}

	/**
	 * Verify Trade In promo text is displayed
	 *
	 * @return
	 */
	public CartPage verifyTradeInPromoValueIsDisplayed() {
		try {
			Assert.assertTrue(appliedTradeInPromoPrice.get(0).isDisplayed(), "Trade In promo Value is not displayed");
			Reporter.log("Trade In promo Value is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade In promo Value" + e);
		}
		return this;
	}

	/**
	 * Verify EIP Price before promo applied
	 *
	 * @return
	 */
	public CartPage verifyeipPricingBeforepromo() {
		try {
			Assert.assertTrue(devicePriceBeforepromo.get(0).isDisplayed(),
					"Price is not displayed before promo applied");
			Reporter.log("Price is not displayed before promo applied");
		} catch (Exception e) {
			Assert.fail("Price is not displayed before promo applied" + e);
		}
		return this;
	}

	/**
	 * Verify EIP Price After promo applied
	 *
	 * @return
	 */
	public CartPage verifyeipPricingAfterpromo() {
		try {
			Assert.assertTrue(devicePriceAfterpromo.get(0).isDisplayed(), "Price is not displayed After promo applied");
			Reporter.log("Price is not displayed After promo applied");
		} catch (Exception e) {
			Assert.fail("Price is not displayed After promo applied" + e);
		}
		return this;
	}

	/**
	 * Verify EIP Price After promo applied
	 *
	 * @return
	 */
	public CartPage VerifyFinalEip() {
		try {
			Assert.assertFalse(devicePriceBeforepromo.get(0).equals(devicePriceAfterpromo.get(0)),
					"Final EIP price is differed with regular Eip price");
			Reporter.log("Final EIP price is differed with regular Eip price");
		} catch (Exception e) {
			Assert.fail("Final EIP price is differed with regular Eip price" + e);
		}
		return this;
	}

	/**
	 * Verify Due monthly total Price is displayed
	 *
	 * @return
	 */
	public CartPage verifyDueMonthlyTotalIsDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(dueMonthlyTotal.get(1).isDisplayed(), "Due monthly total is not displayed");
			} else {
				Assert.assertTrue(dueMonthlyTotal.get(0).isDisplayed(), "Due monthly total is not displayed");
				Assert.assertTrue(dueMonthlyTotal.get(0).getText().contains("$"),
						"Due monthly total Label is not displayed");
			}
			Reporter.log("Due monthly total is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Due monthly total");
		}
		return this;
	}

	/**
	 * Get EIP Promo amount Integer format
	 *
	 * @return
	 */
	public Double geteipPromoAmtIntegerCartPage() {
		String promoAmt;
		promoAmt = promotionDiscountPriceNonTradeIn.getText();
		String promoAmtIntegar = promoAmt.substring(promoAmt.lastIndexOf("$") + 1);
		return Double.parseDouble(promoAmtIntegar);
	}

	/**
	 * Get EIP MONTHLY amount Integer format
	 *
	 * @return
	 */
	public Double geteipMonthlyAmtTotalIntegerCartPage() {
		String eipMonthlyAmtTotal;
		eipMonthlyAmtTotal = eipMonthlyAmt.getText();
		String monthlyAmt = eipMonthlyAmtTotal.substring(eipMonthlyAmtTotal.lastIndexOf("$") + 1);
		return Double.parseDouble(monthlyAmt);
	}

	/**
	 * Verify that Promo value and EIP pricing is equal from Cart page
	 */
	public CartPage comparePromoValueEipPricing(double todayValue1, double todayValue2) {
		try {
			Assert.assertEquals(todayValue1, todayValue2,
					" EIP and promo price is not matched with Final EIP price. from cart page.");
			Reporter.log("Difference between EIP and promo prices is equal to Final EIP price.");
		} catch (Exception e) {
			Assert.fail("Failed to compare Final EIP price with EIP and promo price cart page.");
		}
		return this;
	}

	/**
	 * Get EIP MONTHLY amount Integer format
	 *
	 * @return
	 */
	public Double geteipMonthlyIntegerCartPage() {
		String eipMonthly;
		eipMonthly = eipMonthlyTotalAmt.get(0).getText();
		String eipMonthlyAmt = eipMonthly.substring(eipMonthly.lastIndexOf("$") + 1);
		return Double.parseDouble(eipMonthlyAmt);
	}

	/**
	 * verify EIP Monthly amount
	 */
	public CartPage verifyeipMonthlyAmtCartPage() {
		try {
			Assert.assertTrue(eipMonthlyAmt.isDisplayed(), "EIP monthly amount is not displayed");
			Reporter.log("EIP monthly amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP monthly amount");
		}
		return this;
	}

	/**
	 * Verify DownPayment Price is displayed
	 *
	 * @return
	 */
	public CartPage verifyEstDueTodayIsDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(estDueTodayText), "'Est. due today' text is displayed");
			Assert.assertTrue(estDueTodaySubtotal.isDisplayed(), "Estimated Due Today price is not displayed");
			Assert.assertTrue(estDueTodaySubtotal.getText().contains("$"),
					"$ is not present in Estimated Due Today price.");
			Reporter.log("Est. due today is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Est. due today");
		}
		return this;
	}

	/**
	 * Click on Accessories Card CTA
	 */
	public CartPage clickOnAccessoriesCardCTA() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(iOSaccessoryCardCTA);
				Reporter.log("Accessories Card CTA is clikable");
			} else {
				clickElementWithJavaScript(accessoryCardCTA);
				Reporter.log("Accessories Card CTA is clikable");
			}
		} catch (Exception e) {
			Assert.fail("Accessories Card CTA is not available to click");
		}
		return this;
	}

	/**
	 * Verify Accessory FRP is displayed
	 *
	 * @return
	 */
	public CartPage verifyAccessoryFRPisDisplayed() {
		try {
			Assert.assertTrue(accessoryFRPPrice.isDisplayed(), "Accessory FRP is not displayed");
			Assert.assertTrue(accessoryFRPPrice.getText().contains("$"), "Accessory FRP value is present");
			Reporter.log("Accessory FRP is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Accessory FRP");
		}
		return this;
	}

	/**
	 * Get Accessory FRP is displayed
	 *
	 * @return
	 */
	public Double getAccessoryFRP() {

		String fRPText = accessoryFRPPrice.getText();
		String[] arr = fRPText.split("\\$");
		String[] arr2 = arr[1].split(" ");
		return Double.parseDouble(arr2[0]);
	}

	/**
	 * Verify Accessory DownPayment is displayed
	 *
	 * @return
	 */
	public CartPage verifyAccessoryDownPaymentisDisplayed() {
		try {
			Assert.assertTrue(downPaymentPrice.get(0).isDisplayed(), "Accessory DownPayment is not displayed");
			Reporter.log("Accessory DownPayment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Accessory DownPayment");
		}
		return this;
	}

	/**
	 * Get Accessory DownPayment is displayed
	 *
	 * @return
	 */
	public Double getAccessoryDownPayment() {

		String downPayment = downPaymentPrice.get(1).getText();
		downPayment = downPayment.substring(1);

		return Double.parseDouble(downPayment);
	}

	/**
	 * Get Accessory EIP Term is displayed
	 *
	 * @return
	 */
	public Double getAccessoryEIPTerm() {

		String eipTerm = eipMonthTerms.get(1).getText();
		String[] arr = eipTerm.split(" ");

		return Double.parseDouble(arr[1]);
	}

	/**
	 * Verify that Promo FRP value on Accessory PDP and Cart Page are equal
	 */
	public CartPage compareAccessoryFRP(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo FRP values PDP and Cart are not equal");
			Reporter.log("Promo FRP values onPDP and Cart  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo FRP values on PDP and Cart  page");
		}
		return this;
	}

	/**
	 * Verify that Promo DownPayment value on Accessory PDP and Cart Page are equal
	 */
	public CartPage compareAccessoryDownPayment(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "Promo EIP values on PDP and Cart are not equal");
			Reporter.log("Promo EIP values on PDP and Cart are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo EIP values on PDP and Cart page");
		}
		return this;
	}

	/**
	 * Verify that Promo EIP Term value on Accessory PDP and Cart Page are equal
	 */
	public CartPage compareAccessoryEIPTerm(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo EIP Term values on PDP and Cart are not equal");
			Reporter.log("Promo EIP Term values on PDP and Cart are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo EIP Term values on PDP and Cart page");
		}
		return this;
	}

	/**
	 * Verify that monthly amount from accessory Cart page is equal to calculated
	 * Accessory
	 */
	public CartPage compareAccessoryDueMonthlyAmounts(double firstValue, double secondValue) {
		try {

			DecimalFormat df = new DecimalFormat("0.00");
			String formate = df.format(secondValue);
			double finalValue = Double.parseDouble(formate);

			Assert.assertEquals(firstValue, finalValue,
					"EIP value on Accessory Cart page and Calculated EIP are equal");
			Reporter.log("EIP Monthly amount from Accessory Cart is matched with Calculated EIP");
		} catch (Exception e) {
			Assert.fail("Failed to compare EIP price and calculated Cart price from accesory PDP page.");
		}
		return this;
	}

	/**
	 * Verify Accessory Image
	 */
	public CartPage verifyAccessoryImage() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(accessoryImageAtCartMobileView.isDisplayed(), "Accessory Image is not displayed");
			} else {
				Assert.assertTrue(accessoryImageAtCart.isDisplayed(), "Accessory Image is not displayed");
				Reporter.log("Accessory Image is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Accessory Image is not displayed");
		}
		return this;
	}

	/**
	 * Verify Add Accessory Button
	 */
	public CartPage verifyAddAccessoryCartButton() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(iOSaddAccessoryCartButton.isDisplayed(), "Add an Accessory Button is not displayed");
			} else {
				Assert.assertTrue(accessoryCardCTA.isDisplayed(), "Add an Accessory Button is not displayed");
			}
			Reporter.log("Add an Accessory Button is displayed");
		} catch (Exception e) {
			Assert.fail("Add an Accessory Button is not displayed");
		}
		return this;
	}

	/**
	 * Click Add Accessory Button
	 */
	public CartPage clickOnAddAccessoryCartButton() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(iOSaddAccessoryCartButton);
				iOSaddAccessoryCartButton.click();
			} else {
				moveToElement(accessoryCardCTA);
				accessoryCardCTA.click();
			}
			Reporter.log("Add an Accessory Button is clickable");
		} catch (Exception e) {
			Assert.fail("Add an Accessory Button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Accessory Sub Header
	 */
	public CartPage verifyFRPseperatedfromLegalText() {
		try {
			waitforSpinner();

			Assert.assertFalse(legalText.getText().contains("full retail price of"),
					"FRP not seperated from legal text");
			Reporter.log("FRP is seperated from legal text");
		} catch (Exception e) {
			Assert.fail("FRP not seperated from legal text");
		}
		return this;
	}

	/**
	 * Verify Accessory Sub Header
	 */
	public CartPage verifyFullRetailPrice() {
		try {
			waitforSpinner();
			fullRetailprice.isDisplayed();
			Reporter.log("FRP is displayed");
		} catch (Exception e) {
			Assert.fail("FRP is not displayed");
		}
		return this;
	}

	/**
	 * Verify Sales tax Amount in Payments Tab
	 */
	public CartPage verifySalesTaxAmountInPaymentsTab() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AndroidDriver) {
				Assert.assertTrue(salesTaxAmountInPaymentsTab_iPhone.isDisplayed(),
						"Sales tax amount is not displayed");
			} else if (getDriver() instanceof IOSDriver) {
			} else {
				Assert.assertTrue(salesTaxAmountInPaymentsTab.isDisplayed(), "Sales tax amount is not displayed");
			}
			Reporter.log("Sales tax amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sales tax amount");
		}
		return this;
	}

	/**
	 * Verify Sales tax Amount in Payments Tab
	 */
	public CartPage verifySalesTaxAmountForORInPaymentsTab() {
		waitforSpinner();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(salesTaxAmountInPaymentsTab_iPhone.isDisplayed(),
						"Sales tax amount is not displayed");
				Reporter.log("Sales tax amount is displayed");
			} else {
				Assert.assertTrue(salesTaxAmountInPaymentsTab.getText().contains("$0.00"),
						"Sales tax amount for OR state is not displayed");
				Reporter.log("Sales tax amount is $0 for OR state is Verified");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Sales tax amount");
		}
		return this;
	}

	/**
	 * Verify Due today total Amount in Payments Tab
	 */
	public CartPage verifyDueTodayTotalInPaymentsTab() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AndroidDriver) {
				Assert.assertTrue(dueTodayTotalInPaymentsTab_iPhone.isDisplayed());
			} else if (getDriver() instanceof IOSDriver) {
			} else {
				Assert.assertTrue(dueTodayTotalInPaymentsTab.isDisplayed());
			}
			Reporter.log("Due today total amount is displayed");
		} catch (Exception e) {
			Assert.fail("Due today total amount is not displayed");
		}
		return this;
	}

	/**
	 * Get Sales tax amount in Payments tab
	 *
	 * @return
	 */
	public String getSalesTaxAmountInPaymetsTab() {
		String salesTaxPaymentTab = null;
		try {
			if (getDriver() instanceof AndroidDriver) {
				salesTaxPaymentTab = salesTaxAmountInPaymentsTab_iPhone.getText();
			} else if (getDriver() instanceof IOSDriver) {
			}else {
				salesTaxPaymentTab = salesTaxAmountInPaymentsTab.getText();
			}
		} catch (Exception e) {
			Assert.fail("Sales tax amount is not displayed");
		}
		return salesTaxPaymentTab;
	}

	/**
	 * Get Due today total amount in Payments tab
	 *
	 * @return
	 */
	public String getDueTodayTotalAmountInPaymetsTab() {
		String dueTodayTotalPaymentTab = null;
		try {
			if (getDriver() instanceof AndroidDriver) {
				dueTodayTotalPaymentTab = dueTodayTotalInPaymentsTab_iPhone.getText();
			} else if (getDriver() instanceof IOSDriver) {
			}else {
				dueTodayTotalPaymentTab = dueTodayTotalInPaymentsTab.getText();
			}
		} catch (Exception e) {
			Assert.fail("Due today total amount is not displayed");
		}
		return dueTodayTotalPaymentTab;
	}
	// shippingTickMark

	/**
	 * Click Shipping tab
	 */
	public CartPage clickShippingTab() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				clickElement(shippingTickMark_iOS);
			} else {
				shippingTickMark.click();
			}
		} catch (Exception e) {
			Assert.fail("Shipping tab is not clickabe");
		}
		return this;
	}

	/**
	 * Clear City and Update New City
	 *
	 * @param data
	 */
	public void clearCity(String data) {
		try {
			// waitFor(ExpectedConditions.visibilityOf(cityNameShipping));
			// cityNameShipping.clear();
			// waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				sendTextData(shippingTabCityNameForIOS, data);
			} else {
				sendTextData(cityNameShipping, data);
			}
		} catch (Exception e) {
			Assert.fail("City name field is not displayed");
		}
	}

	/**
	 * Clear State and Update New State
	 *
	 * @param data
	 */
	public void clearState(String data) {
		try {
			// waitFor(ExpectedConditions.visibilityOf(stateLabelText));
			// stateLabelText.clear();
			// waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				sendTextData(stateLabelText_iOS, data);
				stateFromDropdownForIOS.get(0).click();
				shippinTabStateDropdownForIOS.sendKeys(Keys.TAB);
			} else {
				sendTextData(stateLabelText, data);
			}
		} catch (Exception e) {
			Assert.fail("State name field is not displayed");
		}
	}

	/**
	 * Clear Zip code and Update New Zip code
	 *
	 * @param data
	 */
	public void clearZipCode(String data) {
		try {
			// waitFor(ExpectedConditions.visibilityOf(zipCodetxt));
			// zipCodetxt.clear();
			// waitforSpinner();
			if (getDriver() instanceof AndroidDriver) {
				sendTextData(shippingTabZipCodetxtForIOS, data);
			}
			if (getDriver() instanceof IOSDriver) {
				shippingTabZipCodetxtForIPhone.click();
				// shippingTabZipCodetxtForIPhone.click();
				clickElementWithJavaScript(shippingTabZipCodetxtForIPhone);
				shippingTabZipCodetxtForIPhone.sendKeys(data);

			} else {
				sendTextData(zipCodetxt, data);
			}
		} catch (Exception e) {
			Assert.fail("Zip code field is not displayed");
		}
	}

	/**
	 * Compare Sales tax amount
	 */
	public CartPage compareSalesTaxAmount(String SalesTaxCharges, String NewSalesTaxCharges) {
		try {
			if (getDriver() instanceof IOSDriver) {
				Reporter.log("Sales tax amount is not equal");
			} else {
				Assert.assertFalse(SalesTaxCharges.contentEquals(NewSalesTaxCharges), "Sales tax amount is equal");
				Reporter.log("Sales tax amount is not equal");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Sales tax amount");
		}
		return this;
	}

	/**
	 * Compare Due today total amount
	 */
	public CartPage compareDueTodayTotalAmount(String DueTodayTotalCharges, String NewDueTodayTotalCharges) {
		try {
			if (getDriver() instanceof IOSDriver) {
				Reporter.log("Due today total amount is not equal");
			} else {
				Assert.assertFalse(DueTodayTotalCharges.contentEquals(NewDueTodayTotalCharges),
						"Due today total amount is equal");
				Reporter.log("Due today total amount is not equal");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Due today total amount");
		}
		return this;
	}

	/**
	 * Provide Shipping Address with PO Box
	 */
	public void provideShippingAddressWithPO() {
		try {
			clearAddressLineOne(INVALID_ADDRESSLINE);
			clearCity(VALID_CITY_01);
			clearState(VALID_STATE_01);
			clearZipCode(VALID_ZIPCODE_01);
			Reporter.log("Shipping Address with PO box is provided");
		} catch (Exception e) {
			Assert.fail("Failed to provide Shipping Address");
		}
	}

	/**
	 * Provide Shipping Address
	 */
	public void provideShippingAddressSecond() {
		try {
			checkPageIsReady();
			clearAddressLineOne(VALID_ADDRESSLINE_02);
			clearCity(VALID_CITY_02);
			clearState(VALID_STATE_02);
			clearZipCode(VALID_ZIPCODE_02);
			Reporter.log("Second Shipping Address is provided");
		} catch (Exception e) {
			Assert.fail("Failed to provide Second Shipping Address");
		}
	}

	/**
	 * Verify Device Name from TMO
	 */
	public CartPage verifyTMOPhonePrice(String downPayment, String eipPrice, String frpPRice) {
		try {
			String eipCartPrice = geteipMonthlyAmtStringCartPage();
			String downPaymentCartPrice = getDeviceDownPayment();
			String frpCartPrice = frpPriceDevice.getText().replace("$", "");
			Assert.assertEquals(downPayment, downPaymentCartPrice,
					"DownPayment Price on Cart Page and TMO PDP are not equal");
			Assert.assertEquals(eipPrice, eipCartPrice, "Eip Price on Cart Page and TMO PDP are not equal");
			Assert.assertEquals(frpPRice, frpCartPrice, "FRP Price on Cart Page and TMO PDP are not equal");
			Reporter.log("Device Name on CartPage and TMO PDP are equal");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Name from TMO");
		}
		return this;
	}

	/**
	 * Get Device Downpayment amount String format
	 *
	 * @return
	 */
	public String getDeviceDownPayment() {
		checkPageIsReady();
		String deviceDownPayment = downPayment.getText();
		deviceDownPayment = deviceDownPayment.substring(deviceDownPayment.indexOf("$") + 1);
		return deviceDownPayment;
	}

	/**
	 * Get Device EIP MONTHLY amount String format
	 *
	 * @return
	 */
	public String geteipMonthlyAmtStringCartPage() {
		String eipMonthlyAmt;
		eipMonthlyAmt = eipMonthlyAmtCart.getText();
		String MonthlyAmt = eipMonthlyAmt.substring(eipMonthlyAmt.lastIndexOf("$") + 1);
		return MonthlyAmt;
	}

	/**
	 * Click On Edit Insurance SOC
	 *
	 * @return
	 */
	public CartPage clickEditInsuranceSoc() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(iOSinsuranceEdit);
				iOSinsuranceEdit.click();
			} else {
				waitforSpinner();
				waitFor(ExpectedConditions.elementToBeClickable(insuranceEdit));
				clickElementWithJavaScript(insuranceEdit);
			}
			Reporter.log("insurance soc is displayed");
		} catch (Exception e) {
			Assert.fail("insurance soc is not displayed");
		}
		return this;
	}

	/**
	 * verify Protection 360 SOC not displayed
	 */
	public CartPage verifyProtectionSOCNotDisplayed() {
		try {
			Assert.assertFalse(insuranceSoc.isDisplayed(), "Protection SOC displayed");
			Reporter.log("Protection SOC not displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Protection SOC not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify protection SOC");
		}
		return this;
	}

	/**
	 * Get Est. Due Today Integer format
	 *
	 * @return
	 */
	public Double getEstDueTodayIntegerUpgradeCart() {
		String downPayment;
		downPayment = estDueTodaySubtotal.getText();
		String[] arr = downPayment.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Verify Strike Through Price
	 */
	public CartPage verifyStrikeThroughPrice() {
		try {
			Assert.assertTrue(frpPriceDevice.isDisplayed(), "Strike Through Price is not displayed");
			Reporter.log("Strike Through Price is displyed");
		} catch (Exception e) {
			Assert.fail("Failed to display displayed Strike Through Price");
		}
		return this;
	}

	/**
	 * Get New FRP after Promo
	 *
	 * @return
	 */
	public Double getNewFRPAfterPromoCart() {
		String downPayment;
		downPayment = frpPriceDevice.getText();
		String str = downPayment.replaceAll("[$-+^:,]", "");
		return Double.parseDouble(str);
	}

	/**
	 * Verify that Promo FRP value on PDP and Cart Page are equal
	 */
	public CartPage compareFRPonPDPwithCart(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo FRP values PDP and Cart are not equal");
			Reporter.log("Promo FRP values onPDP and Cart  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo FRP values on PDP and Cart  page");
		}
		return this;
	}

	/**
	 * Get New Down Payment after Promo
	 *
	 * @return
	 */
	public Double getDownPaymentCart() {
		String downPayment1;
		downPayment1 = downPayment.getText();
		String[] arr = downPayment1.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Verify that Promo DP value on PDP and Cart Page are equal
	 */
	public CartPage compareDPonPDPwithCart(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "Promo DP values PDP and Cart are not equal");
			Reporter.log("Promo DP values on PDP and Cart  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo DP values on PDP and Cart  page");
		}
		return this;
	}

	/**
	 * Get New monthly term price after promo
	 */
	public Double getEipTermCDevice() {
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(eipMonthTerms.get(0)));
		String[] xyz = eipMonthTerms.get(0).getText().split(" ");
		Reporter.log("Get term monthly term of catelog promo");
		return Double.parseDouble(xyz[1]);
	}

	/**
	 * Verify that Promo EIPTerm value on PDP and Cart Page are equal
	 */
	public CartPage compareEIPTermonPDPwithCart(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "Promo EIPTerm values PDP and Cart are not equal");
			Reporter.log("Promo EIPTerm values on PDP and Cart  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo EIPTerm values on PDP and Cart  page");
		}
		return this;
	}

	/**
	 * Verify that Promo EIPTerm value on PDP and Cart Page are equal
	 */
	public CartPage compareEIPonPDPwithCart(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "Promo EIP values PDP and Cart are not equal");
			Reporter.log("Promo EIP values on PDP and Cart  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo EIP values on PDP and Cart  page");
		}
		return this;
	}

	/**
	 * Verify that Promo FRP value on PDP and PLP Page are equal
	 */
	public CartPage compareFRPwithTotal(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo FRP values PDP and PLP are not equal");
			Reporter.log("Promo FRP values onPDP and PLP  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo FRP values on PDP and PLP  page");
		}
		return this;
	}

	/**
	 * Verify Masked Element for Customer Name line
	 */
	public CartPage verifyMaskedElementForCustomerName(String mask) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(mobileCustomerName));
				Assert.assertTrue(mobileCustomerName.isDisplayed(), "Customer name is displayed on Cart page");
			} else {
				waitFor(ExpectedConditions.visibilityOf(customerName.get(0)));
				for (int i = 0; i < customerName.size(); i++) {
					if (customerName.get(i).isDisplayed()) {
						maskedElement = getAttribute(customerName.get(i), "class");
						Assert.assertTrue(maskedElement.contains(mask),
								"Class for Customer Name number " + i + " element doesnt contain Masked value");
					}
				}
				Reporter.log("Customer Name contain masked parameter");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Customer Name Masked element");
		}
		return this;
	}

	/**
	 * Verify Promo Banner Text in cart page
	 */
	public CartPage verifyPromoBannerText() {
		try {
			Assert.assertTrue(promoBannerText.isDisplayed(), "Promo banner text is not displayed");
			Reporter.log("Promo banner text is displyed");
		} catch (Exception e) {
			Assert.fail("Promo banner text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promo Detils Link in cart page
	 */
	public CartPage verifyPromoDetilsLink() {
		try {
			Assert.assertTrue(promoBannerDetailsLink.isDisplayed(), "Promo banner details link is not displayed");
			Reporter.log("Promo banner details link is displyed");
		} catch (Exception e) {
			Assert.fail("Promo banner details link is not displayed");
		}
		return this;
	}

	/**
	 * Click Promo Details Link
	 */
	public CartPage clickPromoDetailsLink() {
		try {
			promoBannerDetailsLink.click();
			Reporter.log("Promo details link is clickable");
		} catch (Exception e) {
			Assert.fail("Promo details link is not clickable");
		}
		return this;
	}

	/**
	 * Verify Promo Model Offer Details Window
	 */
	public CartPage verifyPromoModalOfferDetailsWindow() {
		try {
			Assert.assertTrue(promoModalOfferDetailsWindow.isDisplayed(),
					"Promo Model offer details window is not displayed");
			Reporter.log("Promo Model offer details window is displayed");
		} catch (Exception e) {
			Assert.fail("Promo Model offer details window is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promotion Offer Details
	 */
	public CartPage verifyPromotionOfferDetails() {
		try {
			Assert.assertTrue(promoModalOfferDetails.isDisplayed(), "Promo Model offer details is not displayed");
			Reporter.log("Promo Model offer details is displayed");
		} catch (Exception e) {
			Assert.fail("Promo Model offer details window is displayed");
		}
		return this;
	}

	/**
	 * Click on Close Modal
	 */
	public CartPage clickClosePromoModal() {
		try {
			clickElementWithJavaScript(closePromoModal);
			Reporter.log("Clicked on Close Modal icon");
		} catch (Exception e) {
			Assert.fail("Promo Model offer details window is displayed");
		}
		return this;
	}

	/**
	 * Verify Promo Banner is displayed
	 */
	public CartPage verifyPromoBannerIsDisplayed() {
		try {
			Assert.assertTrue(isElementDisplayed(promoBanner), "Promo banner is not displayed in cart page");
			Reporter.log("Promo banner is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo banner in cart page");
		}
		return this;
	}

	/**
	 * Verify Promo Banner is displayed
	 */
	public CartPage verifyPromoBannerWithPFAMTextIsDisplayed() {
		try {
			Assert.assertTrue((promoBannerTextInCart.getText().contains("iPhone XS and iPhone XS Max")),
					"Promo banner is not displayed in cart page");
			Reporter.log("Promo banner is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo banner in cart page");
		}
		return this;
	}

	/**
	 * Verify Promo Banner is displayed
	 */
	public CartPage verifyPromoBannerWithPFAMAALTextIsDisplayed() {
		try {
			Assert.assertTrue((promoBannerTextInCart.getText().contains("AAL")),
					"Promo banner is not displayed in cart page");
			Reporter.log("Promo banner is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo banner in cart page");
		}
		return this;
	}

	/**
	 * Verify Promo Banner is displayed
	 */
	public CartPage verifyPromoBannerIsDisplayedWithNoPFAM() {
		try {
			Assert.assertTrue(isElementDisplayed(promoBanner), "Promo banner is not displayed in cart page");
			Assert.assertTrue((promoBannerTextInCart.getText().contains("for any device. ")),
					"Promo banner is not displayed in cart page");
			Reporter.log("Promo banner is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo banner in cart page");
		}
		return this;
	}

	/**
	 * Verify Promo Banner is not displayed
	 */
	public CartPage verifyPromoBannerIsNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(promoBanner), "Promo banner is displayed in cart page");
			Reporter.log("Promo banner is not displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo banner in cart page");
		}
		return this;
	}

	/**
	 * Verify Get More Details link is displayed
	 */
	public CartPage verifyGetMoreDetailsLinkInModal() {
		try {
			Assert.assertTrue(isElementDisplayed(promoModalOfferDetailsLink), "Link is not displayed in Offers Modal");
			Reporter.log("Link is displayed in Offers Modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify Link in Offers Modal");
		}
		return this;
	}

	/**
	 * Verify Get More Details link is functional
	 */
	public CartPage verifyLinkInModalIsFunctional() {
		try {
			if (isElementDisplayed(promoModalOfferDetailsLink)) {
				clickElementWithJavaScript(promoModalOfferDetailsLink);
				Reporter.log("Clicked on Link in Offers Modal");
				switchToSecondWindow();
				String currentUrl = getDriver().getCurrentUrl();
				Assert.assertFalse(currentUrl.contains("cart"), "Cart page is still displayed");
				Reporter.log("Link is redirecting to new tab");
			} else {
				Reporter.log("Link is not displayed in Offers Modal");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Link in Offers Modal");
		}
		return this;
	}

	/**
	 * Verify Empty Cart modal is not displayed
	 */
	public CartPage verifyErrorMessageIsNotDisplayed() {
		try {
			Assert.assertFalse(!oopsWeHitaSnagModal.isEmpty(), "'OOPS! WE HIT A SNAG' error is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'OOPS! WE HIT A SNAG' error in cart page");
		}
		return this;
	}

	/**
	 * Verify Empty Cart modal is not displayed
	 */
	public CartPage verifyErrorMessageAddressWithPOBox() {
		try {
			if (getDriver() instanceof AndroidDriver) {
				Assert.assertTrue(wrongAddressErrorAndroid.getText().toLowerCase().contains("po box"),
						"Error message for address with PO box is not contain PO Box");
				Reporter.log("Error message is displayed and contain PO Box");
			} else {
				if (getDriver() instanceof IOSDriver) {
					Reporter.log("Error message is displayed and contain PO Box");
				} else {
					Assert.assertTrue(wrongAddressError.getText().toLowerCase().contains("po box"),
							"Error message for address with PO box is not contain PO Box");
					Reporter.log("Error message is displayed and contain PO Box");
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Error message for address in cart page");
		}
		return this;
	}

	/**
	 * Verify device Protection selected soc
	 */
	public CartPage verifyDeviceProtectionSelectedSoc() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(iOSinsuranceSoc), 15);
				Assert.assertTrue(iOSinsuranceSoc.isDisplayed(), "Device Protection Selected Soc is not displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(insuranceSoc), 15);
				Assert.assertTrue(insuranceSoc.isDisplayed(), "Device Protection Selected Soc is not displayed");
			}
			Reporter.log("Device Protection Selected Soc is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Protection Selected Soc in Cart page");
		}
		return this;
	}

	/**
	 * Verify device Protection no soc selected
	 */
	public CartPage verifyNoDeviceProtection() {
		try {
			// Assert.assertTrue(deviceProtectionSelectedSoc.isEmpty());
			Assert.assertTrue(deviceProtectionSelectedSoc.get(0).getText().equals(""), "SOC is displayed on cart Page");
			Reporter.log("No Device Protection soc displayed");
		} catch (Exception e) {
			Assert.fail("Device Protection  Soc is displayed in Cart page");
		}
		return this;
	}

	/**
	 * Getting SIM kit price from Cart page
	 *
	 * @return price of SIM kit
	 */
	public Double getSIMKitPrice() {
		String downPayment = null;
		downPayment = estDueTodaySubtotal.getText();
		String[] arr = downPayment.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Verify and getting estimate shipping price from Cart page
	 *
	 * @return Estimated Shipping Price
	 */
	public Double verifyAndGetEstimateShippingPrice() {
		String estimateShippingprice;
		String[] arr = new String[0];
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(estShippingMobileView.isDisplayed());
				estimateShippingprice = estShippingMobileView.getText();
				arr = estimateShippingprice.split("\\$", 0);
			} else {
				Assert.assertTrue(estShipping.isDisplayed());
				estimateShippingprice = estShipping.getText();
				arr = estimateShippingprice.split("\\$", 0);
			}
		} catch (Exception e) {
			Assert.fail("Estimated Shipping price is not available");
		}
		return Double.parseDouble(String.valueOf(arr[1]));
	}

	/**
	 * Verify and getting estimate Sales tax price from Cart page
	 *
	 * @return Estimate Sales tax price
	 */
	public Double verifyAndGetEstimateSalexTaxPrice() {
		String estimateSalestaxPrice;
		String[] arr = new String[0];
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(estSalesTaxMobileView.isDisplayed());
				estimateSalestaxPrice = estSalesTaxMobileView.getText();
				arr = estimateSalestaxPrice.split("\\$", 0);
			} else {
				Assert.assertTrue(estSalesTax.isDisplayed());
				estimateSalestaxPrice = estSalesTax.getText();
				arr = estimateSalestaxPrice.split("\\$", 0);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Estimated Sales tax price");
		}
		return Double.parseDouble(String.valueOf(arr[1]));
	}

	/**
	 * Verify Phone Details section
	 */
	public CartPage verifyPhoneSectionDetails() {
		try {
			String lineName = newLineName.getText();
			Assert.assertTrue(lineName.toLowerCase().contentEquals("new line"), "Name of new line is not 'New Line'");
			Assert.assertTrue(estimatedShippingDateInOrderDetailPage.isDisplayed(),
					"Estimated shipping date is not displayed");
		} catch (Exception e) {
			Assert.fail("Phone section details are incorrect");
		}
		return this;
	}

	/**
	 * Verify SIM Details section
	 */
	public CartPage verifySIMSection() {
		try {
			Assert.assertTrue(sIMKitName.isDisplayed(), "Sim Starter Kit title is not displayed");
			Assert.assertTrue(sIMKitPayInFull.isDisplayed(), "'Pay in full' label is not displayed");
			Assert.assertTrue(sIMKitPrice.isDisplayed(), "Sim kit price is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify SIM Kit details");
		}
		return this;
	}

	/**
	 * Verify Additional Voice Line Details section
	 */
	public CartPage verifyAdditionalVoiceLineSection() {
		verifyAdditionalVoiceLineTitle();
		verifyAdditionalVoiceLinePlan();
		verifyAdditionalVoiceLineMonthlyPriceDetails();
		return this;
	}

	/**
	 * Verify Additional Voice Line Details section
	 */
	public void verifyAdditionalVoiceLineMonthlyPriceDetails() {
		try {
			Assert.assertTrue(additionalVoiceLineMonthlyPlanPrice.isDisplayed(),
					"Additional Voice Line monthly price is not displayed");
			Reporter.log("Additional Voice Line monthly price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Additional Voice Line monthly price");
		}
	}

	/**
	 * Verify Additional Voice Line Details section
	 */
	public void verifyAdditionalVoiceLinePlan() {
		try {
			Assert.assertTrue(additionalVoiceLinePlan.getText().contains("Magenta"),
					"Additional Voice Line Plan is not Magenta");
			Reporter.log("Additional Voice Line Plan is Magenta");
		} catch (Exception e) {
			Assert.fail("Failed to verify Additional Voice Line Plan is Magenta");
		}
	}

	/**
	 * Verify Additional Voice Line Details section
	 */
	public void verifyAdditionalVoiceLineTitle() {
		try {
			Assert.assertTrue(additionalVoiceLineTitle.getText().equalsIgnoreCase("Additional voice line"),
					"Additional voice line title is not displayed");
			Reporter.log("Additional Voice Line title is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Additional Voice Line title");
		}
	}

	/**
	 * Click on See Details Link
	 */
	public CartPage clickSeeDetailsLink() {
		try {
			clickElementWithJavaScript(seeDetailsLink);
			Reporter.log("Clicked on See Details link");
		} catch (Exception e) {
			Assert.fail("Failed to click on see details link");
		}
		return this;
	}

	/**
	 * Verify on popup after clicking on See Details link
	 */
	public CartPage verifySeeDetailsPopUp() {
		checkPageIsReady();
		try {
			Assert.assertTrue(seeDetailsPopUp.isDisplayed(), "See Details pop up modal in cart page is not displayed");
			Reporter.log("See Details pop up modal in cart page is displayed");
			Assert.assertTrue(seeDetailsPopUpHeader.isDisplayed(),
					"See Details pop up modal Header in cart page is not displayed");
			Reporter.log("See Details pop up modal header in cart pgae is displayed");
			Assert.assertTrue(seeDetailsPopUpBody.isDisplayed(),
					"See Details pop up modal Body in cart page is not displayed");
			Reporter.log("See Details pop up modal Body in cart page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify See Details modal");
		}
		return this;
	}

	/**
	 * Click on Close 'X' on modal
	 */
	public CartPage verifyModalCloseCTA() {
		try {
			clickElementWithJavaScript(seeDetailsPopUpClose);
			Reporter.log("Close CTA in modal pop up is clickable");
		} catch (Exception e) {
			Assert.fail("Close CTA in modal pop up is not clickable");
		}
		return this;
	}

	/**
	 * Verify PPU Address Edit button
	 */
	public CartPage verifyPPUAddressEditButton() {
		try {
			Assert.assertTrue(PPUAddressEditButton.isDisplayed());
			Reporter.log("PPU Address Edit Button is displayed");
		} catch (Exception e) {
			Assert.fail("PPU Address Edit Button is not displayed");
		}
		return this;
	}

	/**
	 * Click PPU Address Edit button
	 */
	public CartPage clickPPUAddressEditButton() {
		waitforSpinner();
		try {
			clickElement(PPUAddressEditButton);
			Reporter.log("PPU Address Edit Button is clicked");
		} catch (Exception e) {
			Assert.fail("PPU Address Edit Button is not clicked");
		}
		return this;
	}

	/**
	 * Verify By default Service Agreement - Check Box Displayed
	 */
	public CartPage serviceAgreementCheckBoxDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(serviceAgreementCheckBox));
			Assert.assertTrue(serviceAgreementCheckBox.isDisplayed(), "Service Agreement Check Box is displayed");
			Reporter.log("Service Agreement Check Box is displayed");
		} catch (Exception e) {
			Assert.fail("Service Agreement Check Box is not displayed");
		}
		return this;
	}

	/**
	 * Verify By default Service Agreement - Check Box unchecked
	 */
	public CartPage verifyServiceAgreementCheckBox() {
		try {
			boolean isServiceAgreementCheckBoxSelected = false;
			if (serviceAgreementCheckbox.get(0).isDisplayed()) {
				isServiceAgreementCheckBoxSelected = true;
			}
			Assert.assertTrue(isServiceAgreementCheckBoxSelected, "Service Agreement checkbox is selected");
		} catch (Exception e) {
			Assert.fail("By default check box is Not Unchecked");
		}
		return this;
	}

	/**
	 * Verify Service Agreement Text
	 *
	 * @return
	 */
	public CartPage verifyServiceAgreementText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(serviceAgreementText));
			Assert.assertTrue(serviceAgreementText.isDisplayed(), "Service Agreement Text is not displayed");
			Reporter.log("Service Agreement Text is displayed");
		} catch (Exception e) {
			Assert.fail("Service Agreement Text is not displayed");
		}
		return this;
	}

	/**
	 * Verify By default Service Agreement - Title Displayed
	 */
	public CartPage serviceAgreementTitleDisplayed() {
		try {
			Assert.assertTrue(serviceAgreementTitle.isDisplayed(), "Service Agreement Title is displayed");
		} catch (Exception e) {
			Assert.fail("Service Agreement Title is not displayed");

		}
		return this;
	}

	/**
	 * Click on Service customer agreement Check Box
	 */
	public CartPage clickServiceCustomerAgreementCheckBox() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(serviceAgreementCheckBoxMobile);
				Reporter.log("Service customer agreement Check Box is Displayed and Clickable");
			} else {
				clickElementWithJavaScript(serviceAgreementCheckBox);
				Reporter.log("Service customer agreement Check Box is Displayed and Clickable");
			}
		} catch (Exception e) {
			Assert.fail("Service customer agreement Check Box is not clickable");
		}
		return this;
	}

	/**
	 * Verify By default 'Place order CTA' is Disabled
	 */
	public CartPage placeOrderCTADisabled() {
		try {
			if (!placeOrderCTA.isEnabled()) {
			}
			Reporter.log("By default 'Place order CTA' is Disabled");
		} catch (Exception e) {
			Assert.fail("By default 'Place order CTA' is Not Disabled");
		}
		return this;
	}

	/**
	 * Verify By default 'Place order CTA' is Enabled
	 */
	public CartPage placeOrderCTAEnabled() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(acceptAndContinueCTA.get(3)));
				if (acceptAndContinueCTA.get(3).isEnabled()) {
				}
			} else {
				waitFor(ExpectedConditions.visibilityOf(placeOrderCTA));
				if (placeOrderCTA.isEnabled()) {
				}
			}
			Reporter.log("By default 'Place order CTA' is Enabled");
		} catch (Exception e) {
			Assert.fail("By default 'Place order CTA' is Not Enabled");
		}
		return this;
	}

	/**
	 * Click on place Order CTA
	 */
	public CartPage clickplaceOrderCTA() {
		try {
			clickElementWithJavaScript(placeOrderCTA);
			Reporter.log("'Place order CTA'is clickable");
		} catch (Exception e) {
			Assert.fail("'Place order CTA'is not clickable");
		}
		return this;
	}

	/**
	 * Verify edit shipping CTA is displayed
	 */
	public CartPage verifyEditShippingCTADisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(mobileEditShippingCTA.isDisplayed(), "edit shipping CTA is Not displayed");
			} else {
				Assert.assertTrue(editShippingCTA.isDisplayed(), "edit shipping CTA is Not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Edit shipping CTA is displayed");
		}
		return this;
	}

	/**
	 * Verify due today price label at payment page is displayed
	 */
	public CartPage verifyDueTodayLabelAtPayment() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dueTodayLabelAtPayment));
			Assert.assertTrue(dueTodayLabelAtPayment.isDisplayed(), "due today payemnt label is displayed");
		} catch (Exception e) {
			Assert.fail("due today payemnt label is not displayed");
		}
		return this;
	}

	/**
	 * Verify due today price at payment page is displayed
	 */
	public CartPage verifyDueTodayAmountAtPayment() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dueTodayAmountAtPayment));
			Assert.assertTrue(dueTodayAmountAtPayment.isDisplayed(), "due today payemnt label is displayed");
		} catch (Exception e) {
			Assert.fail("due today payemnt label is not displayed");
		}
		return this;
	}

	public CartPage verifyPPUAddressTitle() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(PPUAddressTitleMobileView.isDisplayed());
				Reporter.log("PPU address is displayed and Verified");
			} else {
				Assert.assertTrue(PPUAddressTitle.isDisplayed());
				Reporter.log("PPU address is displayed");
			}

		} catch (Exception e) {
			Assert.fail("PPU address is not displayed");
		}
		return this;
	}

	/**
	 * Verify PPU Address Legal Text
	 *
	 * @return
	 */
	public CartPage verifyPPUAddressLegalText() {
		try {
			waitforSpinner();
			Assert.assertTrue(legalPPUAddressTitle.isDisplayed(), "PPU address title legal text  is not displayed");
			Reporter.log("PPU address title legal text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display PPU address title legal text");
		}
		return this;
	}

	public CartPage clickEditE911Address() {
		try {
			waitforSpinner();
			e911AddressEdit.click();
			Reporter.log("Clicked on E 911 Address");
		} catch (Exception e) {
			Assert.fail("Failed to Click on E 911 Address");
		}
		return this;
	}

	public CartPage verifyE911AddressTitle() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(e911AddressTitleMobileView);
				Assert.assertTrue(e911AddressTitleMobileView.isDisplayed(), "E911 address title is not displayed");
				Reporter.log("E911 address is displayed and Verified");
			} else {
				Assert.assertTrue(e911AddressTitle.isDisplayed(), "E911 address title is not displayed");
				Reporter.log("E911 address is displayed and Verified");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display E911 address title.");
		}
		return this;
	}

	/**
	 * Fill Invalid E911 Address Info
	 */
	public CartPage fillInvalidE911AddressInfo() {
		try {
			sendTextData(e911AddressLine1, Constants.INVALID_STREET_ADDR);
			sendTextData(e911City, Constants.INVALID_CITY);
			sendTextData(e911StateCode, Constants.INVALID_STATE);
			sendTextData(e911ZipCode, Constants.INVALID_ZIP_CODE_E911);
		} catch (Exception e) {
			Assert.fail("E911 address information is not set");
		}
		return this;
	}

	public CartPage clickE911AddressCancel() {
		try {
			waitforSpinner();
			e911AddressCancel.click();
			Reporter.log("E911 address cancel is clickable");
		} catch (Exception e) {
			Assert.fail("E911 address cancel is not  clickable");
		}
		return this;
	}

	/**
	 * Verify E911 Address check is not visible
	 */
	public CartPage verifyE911AddressFieldsCollapse() {
		try {
			waitforSpinner();
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("#idE911Address1")).size());
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("[name='e911address2']")).size());
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("[name='e911city']")).size());
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("[name='e911stateCode']")).size());
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("[name='e911zip']")).size());

			Reporter.log("E911 Address fields are not present");
		} catch (Exception e) {
			Assert.fail("Failed to verify E911 Address fields");
		}
		return this;
	}

	/**
	 * Verify Shipping Address Title
	 */
	public CartPage verifyShippingAddressTitle() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(iOSshippingDetails.isDisplayed(), "Shipping address title is not displayed");
				Reporter.log("Shipping address title is displayed");
			} else {
				Assert.assertTrue(shippingAddressTitle.isDisplayed(), "Shipping address title is not displayed");
				Reporter.log("Shipping address title is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Shipping address title.");
		}
		return this;
	}

	/**
	 * Edit Shipping Address
	 */
	public CartPage clickEditShippingAddress() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileEditShippingCTA.click();
				Reporter.log("Shipping address is editable");
			} else {
				clickElementWithJavaScript(editShippingCTA);
				Reporter.log("Shipping address is editable");
			}

		} catch (Exception e) {
			Assert.fail("Shipping address is not editable");
		}
		return this;
	}

	/**
	 * Fill Invalid PPU Address Info
	 */

	public CartPage fillInvalidPPUAddressInfo() {
		try {
			sendTextData(PPUAddressLine1, Constants.INVALID_STREET_ADDR);
			sendTextData(PPUcityName, Constants.INVALID_CITY);
			sendTextData(PPUstate, Constants.INVALID_STATE);
			sendTextData(PPUzipCode, Constants.INVALID_ZIP_CODE_E911);
		} catch (Exception e) {
			Assert.fail("PPU address information is not set");
		}
		return this;
	}

	/**
	 * Click PPU Address Cancel CTA
	 */
	public CartPage clickPPUAddressCancelCTA() {
		try {
			PPUAddressCancelCTA.click();
			Reporter.log("PPU Address Cancel CTA is clicked");
		} catch (Exception e) {
			Assert.fail("PPU Address Cancel CTA is not clicked");
		}
		return this;
	}

	/**
	 * Verify PPU Edit Address fields Collapse
	 *
	 * @return
	 */

	public CartPage verifyPPUAddressFieldsCollapse() {
		try {
			waitforSpinner();
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("input#idPPUAddress1")).size());
			Assert.assertEquals(0, getDriver()
					.findElements(By.cssSelector("input[ng-model*='$ctrl.PPUAddress.addressLine2']")).size());
			Assert.assertEquals(0,
					getDriver().findElements(By.cssSelector("input[ng-model*='$ctrl.PPUAddress.cityName']")).size());
			Assert.assertEquals(0,
					getDriver().findElements(By.cssSelector("input[ng-model*='$ctrl.PPUAddress.stateCode']")).size());
			Assert.assertEquals(0,
					getDriver().findElements(By.cssSelector("input[ng-model*='$ctrl.PPUAddress.zip']")).size());

			Reporter.log("PPU Address fields are not present");
		} catch (Exception e) {
			Assert.fail("Failed to verify PPU Address fields");
		}
		return this;
	}

	/**
	 * Verify edit shipping CTA when clicked expands edit shipping options
	 */
	public CartPage verifyShippingEditCollapseOnCancel() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(editShippingCTA));
			Assert.assertEquals(0, ShippingAddressLine1EditFieldList.size(),
					"Shipping edit section didn't got collapsed");
			Reporter.log("Shipping edit section got collapsed");
		} catch (Exception e) {
			Assert.fail("failed to Collapse Shipping edit section");
		}
		return this;
	}

	/**
	 * Verify edit shipping CTA when clicked expands edit shipping options
	 */
	public CartPage verifyShippingEditExpand() {
		try {
			waitFor(ExpectedConditions.visibilityOf(ShippingAddressLine1EditField));
			Assert.assertTrue(ShippingAddressLine1EditField.isDisplayed(), "Address Line 1 is Not displayed");
			Assert.assertTrue(ShippingAddressLine2EditField.isDisplayed(), "Address Line 2 is Not displayed");
			Assert.assertTrue(ShippingCityEditField.isDisplayed(), "City is Not displayed");
			Assert.assertTrue(ShippingStateEditField.isDisplayed(), "State is Not displayed");
			Assert.assertTrue(ShippingZipCodeEditField.isDisplayed(), "Zip is Not displayed");
			Reporter.log("All text Fields are displayed");
		} catch (Exception e) {
			Assert.fail("Edit shipping CTA is displayed");
		}
		return this;
	}

	/**
	 * Verify Cancel CTA on Shipping Edit Option
	 */
	public CartPage verifyCancelCTAOnShippingEdit() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cancelCTAOnShippingEdit));
			Assert.assertTrue(cancelCTAOnShippingEdit.isDisplayed(), "Cancel CTA is Not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Cancel CTA");
		}
		return this;
	}

	/**
	 * Click Cancel CTA on Shipping Edit Option
	 */
	public CartPage clickCancelCTAOnShippingEdit() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(cancelButton_iPhone));
				cancelButton_iPhone.click();
				Reporter.log("cancel CTA clicked");
			} else {
				waitFor(ExpectedConditions.visibilityOf(cancelCTAOnShippingEdit));
				cancelCTAOnShippingEdit.click();
				Reporter.log("cancel CTA clicked");
			}

		} catch (Exception e) {
			Assert.fail("Failed to click on Cancel CTA");
		}
		return this;
	}

	/**
	 * Verify Address Line One Authorable Text
	 */
	public CartPage verifyAddressLineOneAuthorableText() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(addressLineOne_iPhone.isDisplayed(),
						"Address Line One authorable text is not displayed");
				Reporter.log("Address Line One authorable text is displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(addressLineOneAuthorableText));
				Assert.assertTrue(addressLineOneAuthorableText.isDisplayed(),
						"Address Line One authorable text is not displayed");
				Reporter.log("Address Line One authorable text is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Address Line One authorable text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Address Line Two Authorable Text
	 */
	public CartPage verifyAddressLineTwoAuthorableText() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(addressLineTwo_iPhone.isDisplayed(),
						"Address Line Two authorable text is not displayed");
				Reporter.log("Address Line Two authorable text is displayed");
			} else {
				Assert.assertTrue(addressLineTwoAuthorableText.isDisplayed(),
						"Address Line Two authorable text is not displayed");
				Reporter.log("Address Line Two authorable text is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Address Line Two authorable text is not displayed");
		}
		return this;
	}

	/**
	 * Verify PPU Address City Label
	 */
	public CartPage verifyPPUAddressCityLabel() {
		try {
			Assert.assertTrue(pPUAddressCityLabel.isDisplayed(), "PPU Address City label is not displayed");
			Reporter.log("PPU Address City label is displayed");
		} catch (Exception e) {
			Assert.fail("PPU Address City label is not displayed");
		}
		return this;
	}

	/**
	 * Verify PPU Address State Label
	 */
	public CartPage verifyPPUAddressStateLabel() {
		try {
			Assert.assertTrue(pPUAddressStateLabel.isDisplayed(), "PPU Address State label is not displayed");
			Reporter.log("PPU Address State label is displayed");
		} catch (Exception e) {
			Assert.fail("PPU Address State label is not displayed");
		}
		return this;
	}

	/**
	 * Verify PPU Address Zip code Label
	 */
	public CartPage verifyPPUAddressZipCodeLabel() {
		try {
			Assert.assertTrue(pPUAddressZipCodeLabel.isDisplayed(), "PPU Address Zip code label is not displayed");
			Reporter.log("PPU Address Zip Code label is displayed");
		} catch (Exception e) {
			Assert.fail("PPU Address Zip Code label is not displayed");
		}
		return this;
	}

	/**
	 * Verify Default Shipping Address in shipping tab
	 */
	public CartPage verifyDefaultShippingAddressInShippingTab() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(mobileDefaultShippingAddressInShippingTab.isDisplayed(),
						"Default Shipping address is not displayed in shipping tab");
				Reporter.log("Default Shipping address is displayed in shipping tab");
			} else {
				Assert.assertTrue(defaultShippingAddressInShippingTab.isDisplayed(),
						"Default Shipping address is not displayed in shipping tab");
				Reporter.log("Default Shipping address is displayed in shipping tab");
			}
		} catch (Exception e) {
			Assert.fail("Default Shipping address is not displayed in shipping tab");
		}
		return this;
	}

	/**
	 * verify InValid CreditCard Error Message()
	 */
	public CartPage verifyInValidCreditCardErrorMessage() {
		try {
			waitforSpinner();
			expirationDate.click();
			waitFor(ExpectedConditions.visibilityOf(ccErrorMessage));
			Assert.assertTrue(ccErrorMessage.getText().equalsIgnoreCase("Enter a valid credit card number."));
			Reporter.log("Credit card error message displayed");
		} catch (Exception e) {
			Assert.fail("Credit card error message not found");
		}
		return this;
	}

	/**
	 * verify InValid Credit Card Number()
	 */
	public CartPage enterInvalidCardNumber() {
		try {
			waitforSpinner();
			sendTextData(cardNumber, Constants.INVALID_CARDNUMBER);
		} catch (Exception e) {
			Assert.fail("Invalid Card Number is not set");
		}
		return this;
	}

	/**
	 * Verify SIM Starter Kit on Cart Page
	 */
	public CartPage verifySimStarterKit() {
		try {
			waitFor(ExpectedConditions.visibilityOf(sIMKitName));
			Assert.assertTrue(sIMKitName.isDisplayed(), "Sim Starter Kit is not visible");
			Reporter.log("Sim Starter Kit is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sim Starter Kit");
		}
		return this;
	}

	/**
	 * Verify SIM Starter Kit on Cart Page
	 */
	public CartPage verifySimStarterKitPriceNotDisplayed() {
		try {
			// waitFor(ExpectedConditions.visibilityOf(sIMKitPrice));
			Assert.assertFalse(isElementDisplayed(sIMKitPrice), "Sim kit price is displayed");
			Assert.assertFalse(isElementDisplayed(sIMKitName), "Sim kit name is displayed");
			Reporter.log("Sim Starter Kit name and price not displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sim Starter Kit name and price");
		}
		return this;
	}

	/**
	 * Verify tradeIn msg text
	 */
	public CartPage verifyTradeInMsgText() {
		try {
			Assert.assertTrue(tradeInMsgText.getText().contains("If you would like to trade in a device"),
					"Trade In Message Text is not displayed");
			Reporter.log("Trade in message text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display trade in message text");
		}
		return this;
	}

	/**
	 * Verify port msg text
	 */
	public CartPage verifyPortMsgText() {
		try {
			Assert.assertTrue(portMsgText.getText().contains("Want to transfer your number"),
					"Port Message Text is not displayed");
			Reporter.log("Port message text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display port message text");
		}
		return this;
	}

	/**
	 * Verify tradeIn msg text Not Displayed
	 */
	public CartPage verifyTradeInMsgTextNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(tradeInMsgText), "Trade in message is still displayed on Cart Page");
			Reporter.log("Trade in message text is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to hide trade in message text");
		}
		return this;
	}

	/**
	 * Click Accept And Place Order For FRP flow
	 */
	public CartPage clickAcceptAndPlaceOrderForFRPFlow() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(acceptAndPlaceOrderButton.get(1));
			Reporter.log("Accept and Place order button is clickable");
		} catch (Exception e) {
			Assert.fail("Accept and Place order button is not clickable");
		}
		return this;
	}

	/**
	 * Get Device Name in String Format
	 *
	 * @return
	 */
	public String getSimKitName() {

		String deviceNametext = sIMKitName.getText();
		return deviceNametext;
	}

	/**
	 * Get Device Name in String Format
	 *
	 * @return
	 */
	public String getDeviceName() {

		String deviceNametext = deviceName.getText();
		return deviceNametext;
	}

	/**
	 * Verify Device Name is Same as That on PDP Page
	 */
	public CartPage compareDeviceName(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue,
					"Device Name on Cart Page does not match with device name on PDP Page");
			Reporter.log("Device Name on Cart Page matched with device name on PDP Page");
		} catch (Exception e) {
			Assert.fail("Failed to compare Device Name on Cart Page and device Name on PDP page");
		}
		return this;
	}

	/**
	 * Verify Sim Kit Name is Same as That on PDP Page
	 */
	public CartPage compareSimKitName(String firstValue, String secondValue) {
		try {
			Assert.assertTrue(firstValue.contains(secondValue),
					"Sim Name on Cart Page does not contain Sim name on PDP Page");
			Reporter.log("Sim Name on Cart Page contains Sim name on PDP Page");
		} catch (Exception e) {
			Assert.fail("Failed to compare Sim Name on Cart Page and Sim Name on PDP page");
		}
		return this;
	}

	/**
	 * Fill Credit Card Information
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage fillCreditCardInfoAndClickOnUpdate(MyTmoData myTmoData) {
		try {
			enterFirstname(myTmoData.getPayment().getNameOnCard());
			enterLastname(myTmoData.getPayment().getNameOnCard());
			enterCardNumber(myTmoData.getPayment().getCardNumber());
			enterCVV(myTmoData.getPayment().getCvv());
			enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			Reporter.log("Credit card information entered");
		} catch (Exception e) {
			Assert.fail("Credit cart information component missing");
		}
		return this;
	}

	/**
	 * Verify Error Message on Invalid Shipping Address
	 */
	public CartPage verifyErrorMessageOnInvalidShippingAddress() {
		waitforSpinner();
		try {
			waitFor(ExpectedConditions.visibilityOf(errorMessageOnInvalidShippingAddress));
			Assert.assertTrue(errorMessageOnInvalidShippingAddress.isDisplayed(),
					"Error Message on Invalid Shipping Address is Not displayed");
			Reporter.log("Error message on Invalid Shipping address is dispayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Error Message on Invalid Shipping Address");
		}
		return this;
	}

	/**
	 * Verify Error Message on Invalid Shipping Address
	 */
	public CartPage verifyErrorMessageOnInvalidShippingAddressNotDisplayed() {
		try {
			Assert.assertFalse(errorMessageOnInvalidShippingAddress.isDisplayed(),
					"Error Message on Invalid Shipping Address is displayed");
			Reporter.log("Error message on Invalid Shipping address is not dispayed");
		} catch (Exception e) {
			Assert.fail("Failed not to Verify Error Message on Invalid Shipping Address");
		}
		return this;
	}

	/**
	 * Verify Error Message on Invalid E911 Address
	 */
	public CartPage verifyErrorMessageOnInvalidE911Address() {
		try {
			waitFor(ExpectedConditions.visibilityOf(errorMessageOnInvalidE911Address));
			Assert.assertTrue(errorMessageOnInvalidE911Address.isDisplayed(),
					"Error Message on Invalid E911 Address is Not displayed");
			Reporter.log("error message on Invalid 911 address is dispayed");

		} catch (Exception e) {
			Assert.fail("Failed to Verify Error Message on Invalid E911 Address");
		}
		return this;
	}

	/**
	 * Verify Error Message on Invalid PPU Address
	 */
	public CartPage verifyErrorMessageOnInvalidPPUAddress() {
		try {
			waitFor(ExpectedConditions.visibilityOf(errorMessageOnInvalidPPUAddress));
			Assert.assertTrue(errorMessageOnInvalidPPUAddress.isDisplayed(),
					"Error Message on Invalid PPU Address is Not displayed");
			Reporter.log("error message on Invalid PPU address is dispayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Error Message on Invalid PPU Address");
		}
		return this;
	}

	/**
	 * Fill Shipping Address Information
	 */
	public CartPage fillShippingAddressInfoWithLowerCaseState() {
		try {
			setAddressOne(Constants.STREET_ADDR);
			setCity(Constants.CITY);
			selectState(Constants.LCASE_STATE);
			setZipCode(Constants.ZIP_CODE_E911);
		} catch (Exception e) {
			Assert.fail("Shipping address information is not set");
		}
		return this;
	}

	/**
	 * Click on Remove Accessory Button in CartPage
	 */
	public CartPage clickOnRemoveButtonForAccessory() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				removeAccessoryMobileView.get(0).click();
				Reporter.log("Clicked on Remove Accessory Button");
			} else {
				removeAccessory.get(0).click();
				Reporter.log("Clicked on Remove Accessory Button");
			}

		} catch (Exception e) {
			Assert.fail("Remove Accessory Button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Accesory Device Eip Option
	 */
	public CartPage verifyAccesoryDeviceEipOption() {
		boolean isEipDisplayed = false;
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : accessoryDeviceEipOptionMobile) {
					if (webElement.isDisplayed())
						isEipDisplayed = true;
				}
				Assert.assertTrue(isEipDisplayed, "Not All accessories have EIP option selected");
				Reporter.log("EIP Option is displayed for accessory devices");
			} else {
				for (WebElement webElement : accessoryDeviceEipOption) {
					if (webElement.isDisplayed())
						isEipDisplayed = true;
				}
				Assert.assertTrue(isEipDisplayed, "Not All accessories have EIP option selected");
				Reporter.log("EIP Option is displayed for accessory devices");
			}

		} catch (Exception e) {
			Assert.fail("EIP option is not displayed accessories");
		}
		return this;
	}

	/**
	 * Verify State contains value in Upper case
	 *
	 * @return
	 */
	public CartPage verifyUpperCaseValueInStateField() {
		try {
			checkPageIsReady();
			Assert.assertTrue(stateDropdown.getAttribute("value").contains(Constants.LCASE_STATE.toUpperCase()),
					"Auto update failed from Lower case to Upper Case");
			Reporter.log("Auto update successful from Lower case to Upper Case");
		} catch (Exception e) {
			Assert.fail("Failed to Auto update from Lower case to Upper Case");
		}
		return this;
	}

	/**
	 * Verify Bill Credit Value Text
	 *
	 * @return
	 */
	public CartPage verifyBillCreditValueText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(billCreditValueText), 60);
			Assert.assertTrue(billCreditValueText.isDisplayed(), "Bill Credit Value header is not displayed");
			// Assert.assertTrue(billCreditValueText.getText().toLowerCase().contains("bill
			// credit value"),"'Bill Credit Value' Text is not displayed");
			Reporter.log("Bill Credit Value Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Bill Credit Value Text");
		}
		return this;
	}

	/**
	 * Verify DRP Agreement Text
	 *
	 * @return
	 */
	public CartPage verifyDrpAgreementLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(drpAgreementLink));
			Assert.assertTrue(isElementDisplayed(drpAgreementLink), "DRP Agreement Text is not displayed");
			Reporter.log("DRP Agreement Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify DRP Agreement Text");
		}
		return this;
	}

	/**
	 * Verify DRP Agreement Text
	 *
	 * @return
	 */
	public CartPage verifyDrptermsAndConditionsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(drpTermsAndConditionsLink));
			Assert.assertTrue(isElementDisplayed(drpTermsAndConditionsLink),
					"drp Terms And Conditions Link is not displayed");
			Reporter.log("drp Terms And Conditions Link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify drp Terms And Conditions Link");
		}
		return this;
	}

	/**
	 * Verify that Device Price is equal for FRP and EIP payments for Device amount
	 * from Cart page
	 */
	public CartPage compareDevicePrice(Double firstValue, Double secondValue) {
		try {
			if (firstValue.equals(secondValue)) {
				Reporter.log("Device Price is equal");
			} else {
				Assert.fail("Device Price is not equal");
			}
		} catch (Exception e) {
			Assert.fail("Failed to compare Device Price is equal to DownPayment+EIP*Term");
		}
		return this;
	}

	/**
	 * Get Est. Due Today Integer format for AAL Cart
	 *
	 * @return
	 */
	public Double getEstDueTodayCart() {
		String downPayment = null;
		try {
			if (getDriver() instanceof AppiumDriver) {
				downPayment = estDueTodaySubtotalMobileView.getText();
				String[] arr = downPayment.split("\\$", 0);
				return Double.parseDouble(arr[1]);
			} else {
				downPayment = estDueTodaySubtotal.getText();
				String[] arr = downPayment.split("\\$", 0);
				return Double.parseDouble(arr[1]);
			}
		} catch (Exception e) {
			Reporter.log("Unable to get estimate due date");
		}
		return 0.00;
	}

	/**
	 * Get Shipping price Integer format for AAL Cart
	 *
	 * @return
	 */
	public Double getShippingpriceIntegerAALCartPage() {
		String shipping;
		shipping = estShipping.getText();
		String[] arr = shipping.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Taxes price Integer format for AAL Cart
	 *
	 * @return
	 */
	public Double getTaxesPriceIntegerAALCartPage() {
		String taxesAndFees;
		taxesAndFees = estSalesTax.getText();
		String[] arr = taxesAndFees.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Order total price Integer format for AAL Cart
	 *
	 * @return
	 */
	public Double getEstTotalDueTodayPriceOrderDetailsCartPage() {
		String orderTotal;
		try {
			if (getDriver() instanceof AppiumDriver) {
				orderTotal = estTotalDueTodayMobileView.getText();
				if (orderTotal.contains(",")) {
					orderTotal = orderTotal.replace(",", "");
				}
				String[] arr = orderTotal.split("\\$", 0);
				return Double.parseDouble(arr[1]);
			} else {
				orderTotal = estTotalDueToday.getText();
				if (orderTotal.contains(",")) {
					orderTotal = orderTotal.replace(",", "");
				}
				String[] arr = orderTotal.split("\\$", 0);
				return Double.parseDouble(arr[1]);
			}
		} catch (Exception e) {
			Assert.fail("Unable to get total due price");
		}
		return 0.00;
	}

	/**
	 * Verify Bill Credit message
	 *
	 * @return
	 */
	public CartPage verifyBillCreditMessage() {
		try {
			Assert.assertFalse(billCreditValueText.isDisplayed(), "Bill Credit message is displayed");
			Reporter.log("Bill Credit Value message is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Bill Credit Value message");
		}
		return this;
	}

	/**
	 * Fill Fraud Check Shipping Address Information
	 */
	public CartPage fillFraudCheckShippingAddressInfo(String FraudCheck_Street, String FraudCheck_City) {
		try {
			setAddressOne(FraudCheck_Street);
			setCity(FraudCheck_City);
			selectState(FraudCheck_State);
			setZipCode(FraudCheck_ZIPCode);
		} catch (Exception e) {
			Assert.fail("Fraud check Shipping address information is not set");
		}
		return this;
	}

	/**
	 * Verify Remove link is present
	 */
	public CartPage verifyRemoveCtaIsPresent() {
		try {
			Assert.assertTrue(isElementDisplayed(removeLink.get(0)), "Remove link is not present");
			Reporter.log("'Remove' link is present for first tile in Order Details page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Remove CTA");
		}
		return this;
	}

	/**
	 * Click Accessories Model Continue Shopping CTA
	 */
	public CartPage clickAccessoriesModelContinueShoppingCTA() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(accessoriesModelContinueShoppingCTA);
			Reporter.log("Accessories Model Continue Shopping CTA button is clickable");
		} catch (Exception e) {
			Assert.fail("Accessories Model Continue Shopping CTA button is not clickable");
		}
		return this;
	}

	/**
	 * Verify that Due Today amount
	 */
	public CartPage compareDueTodayPrice(double dueTodayPriceAfterDeletingAccessory,
			double dueTodayPriceBeforeDeletingAccessory, double payInFullPrice) {
		try {
			Double total = (dueTodayPriceBeforeDeletingAccessory + payInFullPrice);
			Assert.assertEquals(dueTodayPriceAfterDeletingAccessory, total, " Due Today pries are not equal");
			Reporter.log("Due today prices are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Due today prices");
		}
		return this;
	}

	/**
	 * Get TaxBreakDownWindow device price
	 */
	public float getTaxBreakDownWindowDevicePrice() {
		float price1 = 0;
		try {
			taxBreakDownWindowDevicePrice.isDisplayed();
			String price = taxBreakDownWindowDevicePrice.getText().replace("$", "").replace(",", "");
			price1 = Float.parseFloat(price);
			Reporter.log("Tax break down window device price displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window device price not displayed");
		}
		return price1;
	}

	/**
	 * Get TaxBreakDownWindow Sim Starter Kit price
	 */
	public float getTaxBreakDownWindowSimStarterKitPrice() {
		float price1 = 0;
		try {
			taxBreakDownWindowSimStarterKitPrice.isDisplayed();
			String price = taxBreakDownWindowSimStarterKitPrice.getText().replace("$", "").replace(" ", "").replace(",",
					"");
			price1 = Float.parseFloat(price);
			Reporter.log("Tax break down window device price displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window device price not displayed");
		}
		return price1;
	}

	/**
	 * Get TaxBreakDownWindow Sub total price
	 */
	public float getTaxBreakDownWindowSubTotalPrice() {
		float price1 = 0;
		try {
			taxBreakDownWindowPriceSubTotalPrice.isDisplayed();
			String price = taxBreakDownWindowPriceSubTotalPrice.getText().replace("$", "").replace(",", "");
			price1 = Float.parseFloat(price);
			Reporter.log("Tax break down window sub total price displayed");
		} catch (Exception e) {
			Assert.fail("Tax break down window sub total price not displayed");
		}
		return price1;
	}

	/**
	 * Get estimated Ship date in cart page
	 */
	public String getEstimatedShipDate() {
		String estdate = null;
		for (WebElement webElement : estimatedShipDateFormat) {
			if (webElement.isDisplayed()) {
				estdate = webElement.getText().replace(" ", "");
				break;
			}
		}
		return estdate;
	}

	/**
	 * Compare Estimated ship date in cart page
	 */
	public CartPage compareEstimatedShipDate(String pdpPageEstimatedShipDate, String cartPageEstimatedShipDate) {
		try {
			Assert.assertEquals(pdpPageEstimatedShipDate, cartPageEstimatedShipDate,
					"Estimated ship date are not matched");
			Reporter.log("Estimated ship dates are equal");
		} catch (Exception e) {
			Assert.fail("Failed to Estimated ship dates");
		}
		return this;

	}

	/**
	 * Get SOC Price info
	 *
	 * @return CartPage object
	 */
	public String getPHPSelectedSocPrice() {
		waitforSpinner();
		String price = null;
		String price1[] = new String[0];
		try {
			Assert.assertTrue(SOCPrice.isDisplayed());
			price = SOCPrice.getText();
			price = price.replace(" ", "");
			price1 = price.split("/");
			Reporter.log("SOC Price info is displayed");
		} catch (Exception e) {
			Assert.fail("SOC Price info is not displayed");
		}
		return price1[0];
	}

	/**
	 * Get SOC name info
	 *
	 * @return CartPage object
	 */
	public String getPHPSelectedSocName() {
		waitforSpinner();
		String name = null;
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(iOSinsuranceSoc.isDisplayed());
				name = iOSinsuranceSoc.getText();
			} else {
				Assert.assertTrue(insuranceSoc.isDisplayed());
				name = insuranceSoc.getText();
			}
			Reporter.log("SOC Name info is displayed");
		} catch (Exception e) {
			Assert.fail("SOC Name info is not displayed");
		}
		return name;
	}

	/**
	 * Compare any 2 string values
	 */
	public CartPage compareTwoStrings(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "String Comparison failed");
			Reporter.log("Two strings match");
		} catch (Exception e) {
			Assert.fail("Failed to compare both strings");
		}
		return this;
	}

	/**
	 * Compare any 2 string values not Equal
	 */
	public CartPage compareTwoStringsNotEqual(String firstValue, String secondValue) {
		try {
			Assert.assertNotEquals(firstValue, secondValue, "String Comparison failed - They are Equal");
			Reporter.log("Two strings does not match");
		} catch (Exception e) {
			Assert.fail("Failed to compare both strings");
		}
		return this;
	}

	/**
	 * Verify promotion details
	 */
	public CartPage verifyPromotionDetailsNonTradeIn() {
		try {
			Assert.assertTrue(isElementDisplayed(eipMonthlyTotalAmt.get(0)),
					"promotion details monthly actual price is not present");
			Assert.assertTrue(isElementDisplayed(eipMonthlyTotalAmt.get(1)),
					"promotion details monthly discount details is not present");
			Reporter.log("promotion details monthly actual price,discount details are present");
		} catch (Exception e) {
			Assert.fail("Failed to verify promotion details");
		}
		return this;
	}

	/**
	 * Verify Remove Model Header
	 *
	 * @return
	 */
	public CartPage verifyRemoveModelHeader() {
		try {
			removeModelHeader.isDisplayed();
			Reporter.log("Remove model header is displayed");
		} catch (Exception e) {
			Assert.fail("Remove model is not displayed in cart page");
		}
		return this;
	}

	/**
	 * Click Yes Remove Button
	 */
	public CartPage clickYesRemoveButton() {
		try {
			yesRemoveButton.click();
			Reporter.log("Yes button is clickable");
		} catch (Exception e) {
			Assert.fail("Yes button is not clickable");
		}
		return this;
	}

	/**
	 * Click Shop Phones
	 */
	public CartPage clickShopPhones() {
		try {
			shopPhones.click();
			Reporter.log("Shop phones is clickable");
		} catch (Exception e) {
			Assert.fail("Shop phones is not clickable");
		}
		return this;
	}

	/**
	 * Verify promotion Discount Price
	 */
	public CartPage verifyPromotionDiscountPriceNonTradeIn() {
		try {
			Assert.assertTrue(promotionDiscountPriceNonTradeIn.isDisplayed(),
					"promotion Discount Price is not displayed");
			Reporter.log("promotion Discount Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify promotion Discount Price");
		}
		return this;

	}

	/**
	 * Verify promotion Discount Price
	 */
	public CartPage verifyTotalPromotionDiscountPriceNonTradeIn() {
		try {
			Assert.assertTrue(totalPromotionDiscountPriceNonTradeIn.isDisplayed(),
					"Total promotion Discount Price is not displayed");
			Reporter.log("Total promotion Discount Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Total promotion Discount Price");
		}
		return this;

	}

	/**
	 * Verify Due monthly total Label is displayed
	 *
	 * @return
	 */
	public CartPage verifyDueMonthlyTotalLabelIsDisplayed() {
		try {
			Assert.assertTrue(dueMonthlyTotalLabel.isDisplayed(), "Due monthly total Label is not displayed");
			Reporter.log("Due monthly total Label is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Due monthly total Label");
		}
		return this;

	}

	/**
	 * Verify Monthly Breakdown details are correct
	 *
	 * @return
	 */
	public CartPage verifyMonthlyBreakdownDetailsAreCorrect() {
		try {
			Double monthlyPriceAfterPromo = getDeviceMonthlyIntegerCartPage();
			Double deviceDownPaymentValue = Double.parseDouble(getDeviceDownPayment());
			Double deviceLoanTerm = getEipTermCDevice();
			Double deviceFRP = getNewFRPAfterPromoCart();
			Double devicePromo = getDevicePromoDiscount();
			Double calcFRP = deviceDownPaymentValue + (deviceLoanTerm * monthlyPriceAfterPromo) + devicePromo;
			Double scalcFRP = (double) Math.round(calcFRP);
			Double sdeviceFRP = (double) Math.round(deviceFRP);
			Assert.assertEquals(scalcFRP, sdeviceFRP, "Monthly Breakdown is not correct");
			Reporter.log("Monthly Breakdown is correct");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly breakdown correct");
		}
		return this;

	}

	/**
	 * Get Device Promo Discount
	 *
	 * @return
	 */
	public Double getDevicePromoDiscount() {
		checkPageIsReady();
		String monthlyPayment;
		monthlyPayment = totalPromotionDiscountPriceNonTradeIn.getText();
		monthlyPayment = monthlyPayment.substring(monthlyPayment.lastIndexOf("$") + 1, monthlyPayment.lastIndexOf("&"));
		Reporter.log("Device monthly payment is: " + monthlyPayment);
		return Double.parseDouble(monthlyPayment);
	}

	/**
	 * Verify Accessory EIP Price is displayed
	 *
	 * @return
	 */
	public CartPage verifyAccessoryEIPPriceIsDisplayed() {
		try {
			Assert.assertTrue(accessoryEIPPrice.isDisplayed(), "Accessory EIP Price is not displayed");
			Reporter.log("Accessory EIP Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Accessory EIP Price");
		}
		return this;

	}

	/**
	 * Click Promo Modal link on Promo Banner
	 */
	public CartPage clickPromoModalLinkOnPromoBanner() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(promoModalLink);
			Reporter.log("Promo Modal link is clicked");
		} catch (Exception e) {
			Assert.fail("Promo Modal link is not clickable");
		}
		return this;
	}

	/**
	 * Verify promo modal for cart is displayed
	 *
	 * @return
	 */
	public CartPage verifyPromoModalIsDisplayed() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(promoModal.isDisplayed(), "Promo modal for cart is not displayed");
			Reporter.log("Promo modal for cart is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify promo modal for cart");
		}
		return this;
	}

	/**
	 * Verifies due today total prices
	 *
	 * @param dueTodayTotal
	 * @param totalPrice
	 * @return
	 */
	public CartPage verifyTotalDueTodayPrices(Double dueTodayTotal, Double totalPrice) {
		try {
			String dueToday = dueTodayTotal.toString();
			String totalTodayPrice = totalPrice.toString();
			Assert.assertTrue(totalTodayPrice.contains(dueToday), "Today due today prices are not matching");
			Reporter.log("Today due today prices are verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Due today total on cart");
		}
		return this;
	}

	/**
	 * Verifies and click on Next day delivery
	 *
	 * @return
	 */
	public CartPage verifyNextDayDeliveryBtn() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(nextDayDeliveryRadioBtnMobileView), 10);
				Assert.assertTrue(nextDayDeliveryRadioBtnMobileView.isDisplayed(),
						"Next day delivery option is not available");
				Reporter.log("Next day delivery option is Displayed and Verified");
			} else {
				waitFor(ExpectedConditions.visibilityOf(nextDayDeliveryRadioBtn), 10);
				Assert.assertTrue(nextDayDeliveryRadioBtn.isDisplayed(), "Next day delivery option is not available");
				Reporter.log("Next day delivery option is available");
			}
		} catch (Exception e) {
			Assert.fail("Failed to validate Next day delivery option");
		}
		return this;
	}

	/**
	 * Verifies and click on Next day delivery
	 *
	 * @return
	 */
	public CartPage clickNextDayDeliveryBtn() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.elementToBeClickable(nextDayDeliveryRadioBtnMobileView), 10);
				nextDayDeliveryRadioBtnMobileView.click();
				Reporter.log("Clicked on Next day delivery option");
			} else {
				waitFor(ExpectedConditions.elementToBeClickable(nextDayDeliveryRadioBtn), 10);
				nextDayDeliveryRadioBtn.click();
				Reporter.log("Clicked on Next day delivery option");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Next day delivery option ");
		}
		return this;
	}

	/**
	 * Fill OR Address Info
	 */
	public CartPage fillWAAddressInfo() {
		try {
			setAddressOne(Constants.WA_ADDRESSLINE1);
			setCity(Constants.WA_CITY);
			selectState(Constants.WA_State);
			setZipCode(Constants.WA_ZIP_CODE);
			Reporter.log("WA state details are filled and Verified");
		} catch (Exception e) {
			Assert.fail("Shipping address information is not set");
		}
		return this;
	}

	/**
	 * Fill Billing Address ib Cart Payment Page
	 */
	public CartPage fillBillingAddress() {
		try {
			setAddressOne(Constants.WA_ADDRESSLINE1);
			setCity(Constants.WA_CITY);
			selectState(Constants.WA_State);
			setZipCode(Constants.WA_ZIP_CODE);
			Reporter.log("WA state details are filled and Verified");
		} catch (Exception e) {
			Assert.fail("Billing address information is not set");
		}
		return this;
	}

	/**
	 * Retrieves due today price from cart payment page
	 *
	 * @return
	 */
	public Double getDueTodayCartPaymentPage() {
		checkPageIsReady();
		String dueTodayPaymentAmount;
		String[] arr = new String[0];
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(dueTodayAmountAtPaymentMobileView), 10);
				Assert.assertTrue(dueTodayAmountAtPaymentMobileView.isDisplayed(), "Due today amount is not displayed");
				Reporter.log("Due Today amount is Displayed and Verified on Payment Tab");
				dueTodayPaymentAmount = dueTodayAmountAtPaymentMobileView.getText();
				arr = dueTodayPaymentAmount.split("\\$", 0);
			} else {
				waitFor(ExpectedConditions.visibilityOf(dueTodayAmountAtPayment), 10);
				Assert.assertTrue(dueTodayAmountAtPayment.isDisplayed(), "Due today amount is not displayed");
				Reporter.log("Due Today amount is Displayed and Verified on Payment Tab");
				dueTodayPaymentAmount = dueTodayAmountAtPayment.getText();
				arr = dueTodayPaymentAmount.split("\\$", 0);
			}

		} catch (Exception e) {
			Assert.fail("Due today amount is not available");
		}
		return Double.parseDouble(String.valueOf(arr[1]));
	}

	/**
	 * Verify and getting estimate shipping price from Cart page
	 *
	 * @return Estimated Shipping Price
	 */
	public Double verifyAndGetEstimateShippingPricePaymentPage() {
		String estimateShippingprice;
		String[] arr = new String[0];
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(shippingPricePaymentTabMobileView), 10);
				Assert.assertTrue(shippingPricePaymentTabMobileView.isDisplayed(), "Shipping price is not displayed");
				Reporter.log("Shipping Price amount is Displayed and Verified on Payment Tab");
				estimateShippingprice = shippingPricePaymentTabMobileView.getText();
				arr = estimateShippingprice.split("\\$", 0);
			} else {
				waitFor(ExpectedConditions.visibilityOf(shippingPricePaymentTab), 10);
				Assert.assertTrue(shippingPricePaymentTab.isDisplayed(), "Shipping price is not displayed");
				Reporter.log("Shipping Price amount is Displayed and Verified on Payment Tab");
				estimateShippingprice = shippingPricePaymentTab.getText();
				arr = estimateShippingprice.split("\\$", 0);
			}
		} catch (Exception e) {
			Assert.fail("Estimated Shipping price is not available");
		}
		return Double.parseDouble(String.valueOf(arr[1]));
	}

	/**
	 * Verifies and get Estimate Sale Tax Price on Cart Payment page
	 *
	 * @return
	 */
	public Double verifyAndGetEstimateSalexTaxPricePaymentPage() {
		String estimateSalesTaxprice;
		String[] arr = new String[0];
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(salesTaxAmountInPaymentsTab_iPhone.isDisplayed(), "Sales tax price is not displayed");
				estimateSalesTaxprice = salesTaxAmountInPaymentsTab_iPhone.getText();
				arr = estimateSalesTaxprice.split("\\$", 0);
				Reporter.log("Sales Tax amount is Displayed and Verified on Payment Tab");
			} else {
				Assert.assertTrue(salesTaxAmountInPaymentsTab.isDisplayed(), "Sales tax price is not displayed");
				estimateSalesTaxprice = salesTaxAmountInPaymentsTab.getText();
				arr = estimateSalesTaxprice.split("\\$", 0);
				Reporter.log("Sales Tax amount is Displayed and Verified on Payment Tab");
			}
		} catch (Exception e) {
			Assert.fail("Sales tax price is not available");
		}
		return Double.parseDouble(String.valueOf(arr[1]));
	}

	/**
	 * Verifies and get Total due price on Cart Payment page
	 *
	 * @return
	 */
	public Double verifyAndGetDueTodayTotalPaymentPage() {
		String estimateTotalDueTodayPrice;
		String[] arr = new String[0];
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(dueTodayTotalInPaymentsTab_iPhone.isDisplayed(),
						"Total due today price is not displayed");
				estimateTotalDueTodayPrice = dueTodayTotalInPaymentsTab_iPhone.getText();
				if (estimateTotalDueTodayPrice.contains(",")) {
					estimateTotalDueTodayPrice = estimateTotalDueTodayPrice.replace(",", "");
				}
				arr = estimateTotalDueTodayPrice.split("\\$", 0);
			} else {
				Assert.assertTrue(dueTodayTotalInPaymentsTab.isDisplayed(), "Total due today price is not displayed");
				estimateTotalDueTodayPrice = dueTodayTotalInPaymentsTab.getText();
				if (estimateTotalDueTodayPrice.contains(",")) {
					estimateTotalDueTodayPrice = estimateTotalDueTodayPrice.replace(",", "");
				}
				arr = estimateTotalDueTodayPrice.split("\\$", 0);
			}
		} catch (Exception e) {
			Assert.fail("Total due today price is not available");
		}
		return Double.parseDouble(String.valueOf(arr[1]));
	}

	public void verifyPageElements() {
		try {
			Map<WebElement, String> sree = new HashMap<WebElement, String>();
			// Field[] fields = objClass.getDeclaredFields();
			Field[] fields = cartPage.getClass().getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				String name = field.getName();
				// Object value = field.get(this);

				if (field.getGenericType().toString().contains("WebElement")) {
					/* if (field.getType().toString().contains("WebElement")) { */
					WebElement w = getelement((WebElement) field.get(this));
					if (w != null) {
						// String xpath= getxpathfromelement(w);
						if (!sree.containsKey(w)) {
							sree.put(w, name);
						} else {
							System.out.println("duplicates are :" + name + ":  " + sree.get(w));
						}
					}
				}

			}

		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}

	}

	public WebElement getelement(WebElement w) {
		try {
			w.isDisplayed();
		} catch (Exception e) {
			return null;
		}
		return w;
	}

	/**
	 * Click Remove Link
	 */
	public CartPage clickRemoveLink() {
		waitforSpinner();
		try {

			if (getDriver() instanceof AppiumDriver) {
				moveToElement(iOSremoveLink);
				iOSremoveLink.click();
				Reporter.log("Clicked on Remove link");
			} else {
				checkPageIsReady();
				removeLink.get(0).click();
				Reporter.log("Clicked on Remove link");
			}
		} catch (Exception e) {
			Assert.fail("Remove link is not clickable");
		}
		return this;
	}

	/**
	 * getDeviceColor
	 */
	public String getDeviceColor() {
		return deviceColor.getText();
	}

	/**
	 * getDeviceColor
	 */
	public String getDeviceMemory() {
		return deviceMemory.getText();
	}

	/**
	 * Get total Device FRP on CartPage
	 * 
	 * @return
	 */
	public String getDeviceFRP() {
		String frp = "";
		try {
			frp = frpPriceDevice.getText().replace("$", "");
			Reporter.log(" Got total Device FRP on CartPage");
		} catch (Exception e) {
			Assert.fail("Failed to get total Device FRP on CartPage");
		}
		return frp;
	}

	/**
	 * Compare Device color with UNOPDPPage
	 */
	public CartPage compareDeviceColor(String deviceColorInCartPage, String deviceColorInUNOPDPPage) {
		try {
			String DeviceColorFromUNOPDPPage = deviceColorInUNOPDPPage.toLowerCase();
			String DeviceColorFromCartPage = deviceColorInCartPage.toLowerCase();
			Assert.assertTrue(DeviceColorFromUNOPDPPage.contains(DeviceColorFromCartPage), "Device color not matched");
			Reporter.log("Device color is matched");
		} catch (Exception e) {
			Assert.fail("Failed to display different color for device");
		}
		return this;
	}

	/**
	 * Compare Device memory with UNOPDPPage
	 */
	public CartPage compareDeviceMemory(String deviceMemoryInCartPage, String deviceMemoryInUNOPDPPage) {
		try {
			String DeviceMemoryFromUNOPDPPage = deviceMemoryInUNOPDPPage.trim();
			String DeviceMemoryFromCartPage = deviceMemoryInCartPage.trim();
			Assert.assertEquals(DeviceMemoryFromCartPage, DeviceMemoryFromUNOPDPPage, "Device memory not matched");
			Reporter.log("Device memory is matched");
		} catch (Exception e) {
			Assert.fail("Failed to display different memory for device");
		}
		return this;
	}

	/**
	 * Compare Device FRP Price with UNOPDPPage
	 */
	public CartPage compareDeviceFRPPrice(String deviceFRPPriceInCartPage, String deviceFRPPriceInUNOPDPPage) {
		try {
			Assert.assertEquals(deviceFRPPriceInCartPage, deviceFRPPriceInUNOPDPPage, "Device price not matched");
			Reporter.log("Device price is matched");
		} catch (Exception e) {
			Assert.fail("Failed to display different price for device");
		}
		return this;
	}

	/**
	 * Compare Device color When user edited SKU information
	 */
	public CartPage deviceColorUpdatedAfterEditingSKUInformation(String deviceColorInCartPageAfterEditing,
			String deviceColorInCartPageBeforeEditing) {
		try {
			Assert.assertNotSame(deviceColorInCartPageAfterEditing, deviceColorInCartPageBeforeEditing,
					"Device color matched");
			Reporter.log("Device color is not matched");
		} catch (Exception e) {
			Assert.fail("Failed to display same color for device");
		}
		return this;
	}

	/**
	 * Compare Device memory When user edited SKU information
	 */
	public CartPage deviceMemoryUpdatedAfterEditingSKUInformation(String deviceMemoryInCartPageAfterEditing,
			String deviceMemoryInCartPageBeforeEditing) {
		try {
			Assert.assertNotSame(deviceMemoryInCartPageAfterEditing, deviceMemoryInCartPageBeforeEditing,
					"Device memory matched");
			Reporter.log("Device memory is not matched");
		} catch (Exception e) {
			Assert.fail("Failed to display same memory for device");
		}
		return this;
	}
}