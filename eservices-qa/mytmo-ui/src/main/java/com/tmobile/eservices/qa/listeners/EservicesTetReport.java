package com.tmobile.eservices.qa.listeners;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

public class EservicesTetReport implements IReporter {
	private static final Logger LOGGER = LoggerFactory.getLogger(EservicesTetReport.class);
	String testSuiteName = "";
	int totalTestCount = 0, totalPassCount = 0, totalFailCount = 0, totalSkipCount = 0;
	float passPercentage = 0, failPercentage = 0, skippedPercentage = 0;
	IResultMap testFailedResult = null, testSkippedResult = null, testPassedResult = null;
	ITestContext testObj;
	Set<String> classSet = new HashSet<String>();
	StringBuilder pwtdBuf = new StringBuilder();

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		String reportTemplate = initReportTemplate();

		getTestSuiteSummary(suites);
		reportTemplate = reportTemplate.replace("$suiteName", String.format("%s", testSuiteName));
		reportTemplate = reportTemplate.replace("$totalTests", String.format("%s", totalTestCount));
		reportTemplate = reportTemplate.replace("$passCount", String.format("%s", totalPassCount));
		reportTemplate = reportTemplate.replace("$failCount", String.format("%s", totalFailCount));
		reportTemplate = reportTemplate.replace("$skipCount", String.format("%s", totalSkipCount));
		reportTemplate = reportTemplate.replace("$passRate", String.format("%s", passPercentage));
		reportTemplate = reportTemplate.replace("$failRate", String.format("%s", failPercentage));
		reportTemplate = reportTemplate.replace("$skipRate", String.format("%s", skippedPercentage));
		System.out.println("Started writing ESERVICES reporter....");
		for (String className : getClassSet(suites)) {
			pwtdBuf.append(getPagewiseTestsDetails(className));
		}
		String failedMessages=Matcher.quoteReplacement(getFailedTestDetails());
		reportTemplate = reportTemplate.replaceFirst("<em></em>", String.format("%s<em></em>", pwtdBuf));

		reportTemplate = reportTemplate.replaceFirst("<i></i>", String.format("%s<i></i>",failedMessages ));

		saveReportTemplate(outputDirectory, reportTemplate);
		System.out.println("Ending writing ESERVICES reporter....");
	}

	private void getTestSuiteSummary(List<ISuite> suites) {
		try {
			for (ISuite tempSuite : suites) {
				testSuiteName = tempSuite.getName();

				Map<String, ISuiteResult> testResults = tempSuite.getResults();
				for (ISuiteResult result : testResults.values()) {

					testObj = result.getTestContext();

					totalPassCount = testObj.getPassedTests().getAllMethods().size();
					totalSkipCount = testObj.getSkippedTests().getAllMethods().size();
					totalFailCount = testObj.getFailedTests().getAllMethods().size();

					totalTestCount = totalPassCount + totalFailCount + totalSkipCount;

					if (totalTestCount != 0) {
						passPercentage = (float) totalPassCount / totalTestCount * 100;
						passPercentage = Math.round(passPercentage);

						failPercentage = (float) totalFailCount / totalTestCount * 100;
						failPercentage = Math.round(failPercentage);

						skippedPercentage = (float) totalSkipCount / totalTestCount * 100;
						skippedPercentage = Math.round(skippedPercentage);
					} else
						System.out.println("No tests in the TestNG context, check if you writen test correctly");

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Set<String> getClassSet(List<ISuite> suites) {
		try {
			for (ISuite tempSuite : suites) {
				testSuiteName = tempSuite.getName();

				Map<String, ISuiteResult> testResults = tempSuite.getResults();
				for (ISuiteResult result : testResults.values()) {

					ITestContext testContext = result.getTestContext();

					testFailedResult = testContext.getFailedTests();
					testPassedResult = testContext.getPassedTests();
					testSkippedResult = testContext.getSkippedTests();

					if (testFailedResult != null)
						for (ITestResult testResult : testFailedResult.getAllResults()) {
							String testClassName = "";
							testClassName = testResult.getTestClass().getName();
							classSet.add(testClassName);
						}
					if (testPassedResult != null)
						for (ITestResult testResult : testPassedResult.getAllResults()) {
							String testClassName = "";
							testClassName = testResult.getTestClass().getName();
							classSet.add(testClassName);
						}
					if (testSkippedResult != null)
						for (ITestResult testResult : testSkippedResult.getAllResults()) {
							String testClassName = "";
							testClassName = testResult.getTestClass().getName();
							classSet.add(testClassName);
						}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return classSet;
	}

	private String getPagewiseTestsDetails(String className) {
		int failedMethods = 0, passMethods = 0, skippedMethods = 0, totalMethods = 0;

		if (testFailedResult != null)
			for (ITestResult classFailedName : testFailedResult.getAllResults())
				if (classFailedName.getTestClass().getName().equalsIgnoreCase(className))
					failedMethods++;

		if (testPassedResult != null)
			for (ITestResult classPassedName : testPassedResult.getAllResults())
				if (classPassedName.getTestClass().getName().equalsIgnoreCase(className))
					passMethods++;

		if (testSkippedResult != null)
			for (ITestResult classSkippedName : testSkippedResult.getAllResults())
				if (classSkippedName.getTestClass().getName().equalsIgnoreCase(className))
					skippedMethods++;

		totalMethods = skippedMethods + passMethods + failedMethods;
		className = className.substring(25, className.length());
		return "<tr><td>" + className + "</td><td style=\"text-align: center;\"><span style=\"color: #0000ff;\">"
				+ totalMethods + "</span></td><td style=\"text-align: center;\"><span style=\"color: #339966;\">"
				+ passMethods + "</span></td><td style=\"text-align: center;\"><span style=\"color: #ff0000;\">"
				+ failedMethods + "</span></td><td style=\"text-align: center;\"><span style=\"color: #ff9900;\">"
				+ skippedMethods + "</td></tr>";
	}

	private String getFailedTestDetails(){
		String erroredTestName = null, errorMessage = null, failedClass = null;
		StringBuilder ftd = new StringBuilder();
	try {
			if (testFailedResult != null)
				for (ITestResult classFailedName : testFailedResult.getAllResults()) {
					failedClass = classFailedName.getTestClass().getName();
					erroredTestName = classFailedName.getMethod().getMethodName();
					if (ITestResult.FAILURE == classFailedName.getStatus()) {
						Throwable throwable = classFailedName.getThrowable();
							if (throwable.getMessage().toString() != null) {
								String originalMessage = throwable.getMessage();
//								System.out.println("##" + originalMessage +"##");
								if (originalMessage.contains("(Session info:"))
									originalMessage = originalMessage.substring(0, originalMessage.indexOf("(Session"));
								if (originalMessage
										.contains("org.openqa.selenium.NoSuchElementException: no such element:"))
									originalMessage = originalMessage.replaceAll(
											"org.openqa.selenium.NoSuchElementException: no such element:.*",
											"Element not found");
								if (originalMessage
										.contains("java.lang.AssertionError:"))
									originalMessage = originalMessage.replaceAll("java.lang.AssertionError:", "-");
								errorMessage = originalMessage;
							} else if (throwable.getMessage().toString() == null) {
								errorMessage = "Please set some Assert message";
							} else
								errorMessage = throwable.toString();
					}
					failedClass = failedClass.substring(25, failedClass.length());
					ftd.append("<tr><td>" + failedClass + "</td><td>" + erroredTestName + "</td><td>" + errorMessage
							+ "</td></tr>");
				}
			else
				ftd.append("<tr><td>" + failedClass + "</td><td>" + erroredTestName + "</td><td>" + errorMessage
						+ "</td></tr>");

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("==>" + e);
		}
		return ftd.toString();
	}
	
	private String initReportTemplate() {
		String template = null;
		byte[] reportTemplate;
		try {
			reportTemplate = Files.readAllBytes(Paths.get("src/test/resources/eservicesReportTemplate.html"));
			template = new String(reportTemplate, "UTF-8");
		} catch (IOException e) {
			LOGGER.error("Problem initializing template", e);
		}
		return template;
	}

	private void saveReportTemplate(String outputDirectory, String reportTemplate) {
		new File(outputDirectory).mkdirs();
		try {
			PrintWriter reportWriter = new PrintWriter(
					new BufferedWriter(new FileWriter(new File(outputDirectory, "eservicesTestReport.html"))));
			reportWriter.println(reportTemplate);
			reportWriter.flush();
			reportWriter.close();
		} catch (IOException e) {
			LOGGER.error("Problem saving template", e);
		}
	}

}
