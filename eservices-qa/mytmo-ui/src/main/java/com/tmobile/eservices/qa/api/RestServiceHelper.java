package com.tmobile.eservices.qa.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tmobile.eservices.qa.api.unlockdata.Constants;
import com.tmobile.eservices.qa.api.unlockdata.RequestData;
import com.tmobile.eservices.qa.api.unlockdata.Response;
import com.tmobile.eservices.qa.api.unlockdata.ServiceException;
import com.tmobile.eservices.qa.api.unlockdata.RestServiceImpl;
import com.tmobile.eservices.qa.api.unlockdata.CommonUtils;

/**
 * This class handles to perform operation for rest service ex: unlockAccount
 * 
 * @author blakshminarayana
 *
 */
public class RestServiceHelper {
	private static final Logger logger = LoggerFactory.getLogger(RestServiceHelper.class);
	private RestServiceImpl restService;
	private static final String TMO_PROFILE_TYPE = "TMO";
	private static final String IAM_PROFILE_TYPE = "IAM";

	/**
	 * This method is used to unlock account
	 * 
	 * @param requestData
	 */
	public boolean unlockAccount(RequestData requestData) {
		logger.info("unlockAccount method called in RestServiceHelper");
		restService = new RestServiceImpl();
		String accessToken = restService.getAccessToken(requestData);
		System.out.println("Access Token="+accessToken);
		StringBuilder builder = new StringBuilder();
		builder.append("Bearer").append(" ").append(accessToken);
		requestData.setToken(builder.toString());
		return restService.unlockAccount(requestData);
	}

	/**
	 * This method delete the profile from IAM
	 * 
	 * @param requestData
	 */
	public boolean deleteProfile(RequestData requestData) {
		logger.info("deleteProfile method called in RestServiceHelper");
		boolean deleteProfileBoolean;
		restService = new RestServiceImpl();
		String accessToken = restService.getAccessToken(requestData);
		requestData.setToken(accessToken);
		requestData.setProfileType("IAM");
		Response deleteProfile = restService.deleteProfile(requestData);
		deleteProfileBoolean = true;
		if (deleteProfile.getStatusCode() != 200) {
			logger.error("profile not deletd {}", deleteProfileBoolean);

		}
		return deleteProfileBoolean;
	}

	/**
	 * This method is used to sendPinToMsisdn
	 * 
	 * @param requestData
	 */
	public boolean sendPinToMsisdn(RequestData requestData) {
		//logger.info("sendPinToMsisdn method called in RestServiceHelper");
		restService = new RestServiceImpl();
		String accessToken = restService.getAccessToken(requestData);
		StringBuilder builder = new StringBuilder();
		builder.append("Bearer").append(" ").append(accessToken);
		requestData.setToken(builder.toString());
		return restService.sendPinToMsisdn(requestData);
	}

	/**
	 * This method is used to sendPinToMsisdn
	 * 
	 * @param requestData
	 */
	public String getTempPassword(RequestData requestData) {
		//logger.info("getTempPassword method called in RestServiceHelper");
		String decryptedCode = null;
		restService = new RestServiceImpl();
		boolean sendPinToMsisdn = sendPinToMsisdn(requestData);
		if (sendPinToMsisdn) {
			String token = requestData.getToken();
			String[] splitToken = token.split(" ");
			requestData.setProfileType(IAM_PROFILE_TYPE);
			requestData.setToken(splitToken[1]);
			Response response = restService.getProfile(requestData);
			if (response.getStatusCode() == 200) {
				String tempPassword = response.getTempPassword();
				if (StringUtils.isNotEmpty(tempPassword)) {
					requestData.setTempPassword(tempPassword);
					decryptedCode = restService.getDecryption(requestData);
				}
			}
		}
		return decryptedCode;
	}

	/**
	 * 
	 * @param requestData
	 * @return
	 */
	public String getPinProfileDetails(RequestData requestData) {
		restService = new RestServiceImpl();
		String accessToken = restService.getAccessToken(requestData);
		requestData.setToken(accessToken);
		requestData.setProfileType(TMO_PROFILE_TYPE);
		Response response = restService.getProfile(requestData);
		return response.getPinCode();
	}

	/**
	 * 
	 * @param requestData
	 * @return
	 */
	public String getZipCode(RequestData requestData) {
		restService = new RestServiceImpl();
		String accessToken = restService.getAccessToken(requestData);
		requestData.setToken(accessToken);
		requestData.setProfileType(TMO_PROFILE_TYPE);
		Response response = restService.getProfile(requestData);
		return response.getZipCode();
	}
	
	public static String getProfilePin(RequestData requestData) {
		String pinCode = null;
		try{
		RestServiceImpl restService = new RestServiceImpl();
		RequestData requestData1 = new RequestData();
		String accessToken = restService.getAccessToken(requestData1);
//		System.out.println("Access Token :::: " +accessToken);
		requestData1.setToken(accessToken);
		requestData1.setMsisdn(requestData.getMsisdn());
		requestData1.setProfileType("TMO");
		boolean result = restService.sendPinToMsisdn(requestData1);
//		System.out.println("PIN Result :::: " + result);
		if (result) {
			com.tmobile.eservices.qa.api.unlockdata.Response response = restService.getProfile(requestData1);
//			System.out.println("PIN :::: " + response.getPinCode());
			pinCode = response.getPinCode();
		}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return pinCode;
	}
	
	public static String getAuthCode(String environment){
		String authCode = null;
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.AUTHORIZATION);
		authCode =  loadPropertyFile.getProperty(envBuilder.toString());
		
		return authCode;
		
	}
	
	/**
	 * This method is load the property file from resources
	 * 
	 * @return property file
	 */
	private static Properties loadPropertyFile() {
		logger.info("called loadPropertyFile method in CommonUtils");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = CommonUtils.class.getClassLoader();
			InputStream inputStream = classLoader.getResourceAsStream(Constants.APPLICATION_PROPERTY_FILE);
			properties.load(inputStream);
		} catch (IOException e) {
			logger.error("Fail to load property file{}", e);
			throw new ServiceException("Fail to load property file{}", e);
		}
		return properties;
	}
	
	public boolean changeProfilePassword(RequestData requestData) {
//		logger.info("changeProfilePassword method called in RestServiceHelper");
		restService = new RestServiceImpl();
		String accessToken = restService.getAccessToken(requestData);
		//System.out.println("Access Token="+accessToken);
		StringBuilder builder = new StringBuilder();
		builder.append("Bearer").append(" ").append(accessToken);
		requestData.setToken(builder.toString());
		return restService.changePassword(requestData);
	}

	/**
	 * This method is used to generatetemppin
	 * 
	 * @param requestData
	 */
	public String getSecondAuthPassword(RequestData requestData) {
		return generateTempPin(requestData);
	}

	/**
	 * This method is used to generatetemppin
	 * 
	 * @param requestData
	 */
	public String generateTempPin(RequestData requestData) {
		restService = new RestServiceImpl();
		String accessToken = restService.getAccessToken(requestData);
		StringBuilder builder = new StringBuilder();
		builder.append("Bearer").append(" ").append(accessToken);
		requestData.setToken(builder.toString());
		return restService.generateTempPin(requestData);
	}
	
	public String getTextMessagePin(RequestData requestData) {
		String pinCode = null;
		try {
			RestServiceImpl restService = new RestServiceImpl();
			String accessToken = restService.getAccessToken(requestData);
			
			requestData.setToken(accessToken);
			requestData.setMsisdn(requestData.getMsisdn());
			pinCode = restService.generateTempPin(requestData);

		} catch (Exception ex) {
			System.out.println("Unable to get Text message pin");
		}
		return pinCode;
	}

}
