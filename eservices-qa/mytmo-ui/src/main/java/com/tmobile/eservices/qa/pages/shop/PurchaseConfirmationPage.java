package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.CommonPage;



/**
 * 
 * @author Sudheerc
 *
 */
public class PurchaseConfirmationPage extends CommonPage {

	public PurchaseConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	@FindBy(css = "h2[class*='h2-title']")
	private WebElement yourOrderIsComplete;
	
	/**
	 * Verify your Order Is Complete
	 * 
	 * @return
	 */
	public PurchaseConfirmationPage verifyYourOrderIsComplete() {
		try {
			checkPageIsReady();
			yourOrderIsComplete.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Purchase Confirmation Page is not Displayed");
		}
		return this;
	}
	
}



