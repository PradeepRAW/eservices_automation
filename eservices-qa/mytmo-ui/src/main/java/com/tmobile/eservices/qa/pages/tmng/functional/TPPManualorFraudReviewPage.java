package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class TPPManualorFraudReviewPage extends TmngCommonPage {

	@FindBy(css = "tmo-pre-screen-manual-review>div>h2")
	private WebElement manualReviewPageHeaderText;

	@FindBy(css = "a[href='tel:1-800-T-Mobile']")
	private WebElement callTMobileCta;

	@FindBy(css = "a[href='/store-locator']")
	private WebElement scheduleStoreVisitCta;

	@FindBy(css = "")
	private WebElement dialerOnMobile;

	public TPPManualorFraudReviewPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * verify Mutual Review or Fraud Detection Page
	 *
	 */

	public TPPManualorFraudReviewPage verifyManualRevieworFraudDetectionPage() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(manualReviewPageHeaderText), 60);
			waitFor(ExpectedConditions.urlContains("manual-review"), 60);
			Assert.assertTrue(manualReviewPageHeaderText.isDisplayed(), "Manual Review Page is not displayed");
			Reporter.log("Manual Review Page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Manual Review or Fraud Detection Page");
		}
		return this;
	}

	/**
	 * Verify Call TMobile 1800 CTA
	 * 
	 * @return
	 */
	public TPPManualorFraudReviewPage verifyCallTMobileCTA() {
		try {
			Assert.assertTrue(callTMobileCta.isDisplayed(), "Call TMobile Cta is not displayed");
			Reporter.log("Call TMobile Cta is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Call TMobile Cta");
		}
		return this;
	}

	/**
	 * Click Call TMobile 1800 CTA
	 * 
	 * @return
	 */
	public TPPManualorFraudReviewPage clickCallTMobileCTA() {
		try {
			callTMobileCta.click();
			Reporter.log("Click on Call TMobile Cta");
		} catch (Exception e) {
			Assert.fail("Failed to click Call TMobile Cta");
		}
		return this;
	}

	/**
	 * Verify Schedule Store visit cta
	 *
	 * @return
	 */
	public TPPManualorFraudReviewPage verifyscheduleStoreVisitCta() {
		try {
			Assert.assertTrue(scheduleStoreVisitCta.isDisplayed(), "Schedule Store Visit Cta is not displayed");
			Reporter.log("Schedule Store Visit Cta is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Schedule Store Visit Cta");
		}
		return this;
	}

	/**
	 * Click Schedule Store visit cta
	 *
	 * @return
	 */
	public TPPManualorFraudReviewPage clickscheduleStoreVisitCta() {
		try {
			scheduleStoreVisitCta.click();
			Reporter.log("Click on Schedule Store Visit Cta");
		} catch (Exception e) {
			Assert.fail("Failed to click Schedule Store Visit Cta");
		}
		return this;
	}

	/**
	 * Verify Dialer on Mobile popup
	 *
	 * @return
	 */
	public TPPManualorFraudReviewPage verifyDialerOnMobile() {
		try {
			Assert.assertTrue(dialerOnMobile.isDisplayed(), "Dialer On Mobile pop up is not displayed");
			Reporter.log("Dialer On Mobile pop up is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Dialer On Mobile pop up");
		}
		return this;
	}
}
