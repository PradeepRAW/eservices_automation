package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class PlansPage extends TmngCommonPage {
	private int timeout = 30;

	private final String pageLoadedText = "On all T-Mobile plans, during congestion, the small fraction of customers using &gt;50GB/month may notice reduced speeds until next bill cycle due to data prioritization";
	private final String pageUrl = "/cell-phone-plans";

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	private WebElement about;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/accessibility-policy']")
	private WebElement accessibility;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	private WebElement accessories;

	@FindBy(css = "a[href='//prepaid-phones.t-mobile.com/prepaid-activate']")
	private WebElement activateYourPrepaidPhoneOrDevice;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	private WebElement business2;

	@FindBy(css = "#c8c4ad5c1b4a424b08aff7c380c9deba6ab5eb77 div:nth-of-type(1) div:nth-of-type(2) div.cta. a:nth-of-type(2)")
	private WebElement call1800tmobile1;

	@FindBy(css = "#6ff66dced29300d9f730c198e61a0262aafa40f0 div:nth-of-type(1) div:nth-of-type(2) div.cta. a:nth-of-type(3)")
	private WebElement call1800tmobile2;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	private WebElement callUs;

	@FindBy(css = "a[href='//www.t-mobile.com/careers']")
	private WebElement careers;

	@FindBy(css = "a[href='/offers/t-mobile-one-unlimited-55?icid=WMM_TM_FIDELIS_W9A4FQCPKYG13113']")
	private WebElement checkItOut1;

	@FindBy(css = "a[href='/offers/military-phone-plans?icid=WMM_TM_FIDELIS_3QO120AGKH13112']")
	private WebElement checkItOut2;

	@FindBy(css = "a[href='//www.t-mobile.com/no-credit-check?icid=WMD_TMNG_Q118NCCLP_0NLV7J6CPFI12694']")
	private WebElement checkItOut3;

	@FindBy(css = "a[href='http://www.t-mobile.com/orderstatus/default.aspx']")
	private WebElement checkOrderStatus;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	private WebElement checkOutTheCoverage;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling#check-rates']")
	private WebElement clickHere;

	@FindBy(css = "#5139ea84ba84cf4a17290c04ec9a8d02b383da22 div:nth-of-type(2) button.close")
	private WebElement closeVideo;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info']")
	private WebElement consumerInformation;

	@FindBy(css = "a[href='http://www.t-mobile.com/contact-us.html']")
	private WebElement contactInformation;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	private WebElement deals2;

	@FindBy(css = "a[href='//www.telekom.com/']")
	private WebElement deutscheTelekom;

	@FindBy(css = "a[href='//support.t-mobile.com/community/phones-tablets-devices']")
	private WebElement deviceSupport;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	private WebElement espaol;

	@FindBy(css = "a.lang-toggle-wrapper")
	private WebElement espaolEspaol;

	@FindBy(css = "a[href='http://www.t-mobile.com/store-locator.html']")
	private WebElement findAStore;

	@FindBy(css = "a[href='http://www.t-mobile.com/promotions']")
	private WebElement getARebate;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div.panel-collapse.collapse.anchorID-650970 div.panel-body p a")
	private WebElement here1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) div.panel-collapse.collapse.anchorID-650970 div.panel-body p a")
	private WebElement here2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) button.accordion-toggle.collapsed.")
	private WebElement howDoIGetNetflix1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement howDoIGetNetflix2;

	@FindBy(css = "#e2e62437acc588a82bf7ff23089ebbb7bb57fb22 div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(1)")
	private WebElement httpswwwNetflixComtermsofuse;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx#choicesaboutadvertising']")
	private WebElement interestbasedAds;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	private WebElement internationalCalling;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global.html']")
	private WebElement internationalRates;

	@FindBy(css = "a[href='http://iot.t-mobile.com/']")
	private WebElement internetOfThings;

	@FindBy(css = "a[href='http://investor.t-mobile.com/CorporateProfile.aspx?iid=4091145']")
	private WebElement investorRelations;

	@FindBy(css = "a.btn-primary.btn-brand.btn")
	private WebElement joinUsNow;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	private WebElement logIn1;

	@FindBy(css = "a[href='//my.t-mobile.com/plans.html?icid=WMM_TM_Q417PLANSP_FO240JD100611068']")
	private WebElement logIn2;

	@FindBy(css = "a.btn-secondary.btn")
	private WebElement makeTheSwitch;

	@FindBy(css = "button.hamburger.hamburger-border")
	private WebElement menuMenu;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) button.accordion-toggle.collapsed.")
	private WebElement needToStreamToMore1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement needToStreamToMore2;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/policies/internet-service']")
	private WebElement openInternet;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	private WebElement phones;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	private WebElement plans;

	@FindBy(css = "a[href='//support.t-mobile.com/community/plans-services']")
	private WebElement plansServices;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	private WebElement prepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/news']")
	private WebElement press;

	@FindBy(css = "input.slider-range.ng-untouched.ng-valid.ng-not-empty.ng-dirty.ng-valid-parse")
	private WebElement priceSlider;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/privacy']")
	private WebElement privacyCenter;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx']")
	private WebElement privacyStatement;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/safety/9-1-1']")
	private WebElement publicSafety911;

	@FindBy(css = "a[href='http://www.t-mobilepr.com/']")
	private WebElement puertoRico;

	@FindBy(css = "a[href='//support.t-mobile.com/community/billing']")
	private WebElement questionsAboutYourBill;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-refill-api-bridging']")
	private WebElement refillYourPrepaidAccount;

	@FindBy(css = ".marketing-page.ng-scope div:nth-of-type(8) button")
	private WebElement scrollToTop;

	@FindBy(id = "searchText")
	private WebElement search1;

	@FindBy(id = "searchText")
	private WebElement search2;

	@FindBy(css = "a[href='/offers/tmo_one_faqs']")
	private WebElement seeFullFaqs;

	@FindBy(css = "#e2e62437acc588a82bf7ff23089ebbb7bb57fb22 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) p.section-legal.col-xs-12 span a.tmo-modal-link")
	private WebElement seeFullTerms1;

	@FindBy(css = "#8766fefbb9b7114ea946b0b1e0dc00397e769cf7 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms2;

	@FindBy(css = "a[href='javascript:void(0)']")
	private WebElement seeFullTerms3;

	@FindBy(css = "#458c2dab5712d955a84dc48a50f007d6fd38cb22 div:nth-of-type(1) div.section-content div:nth-of-type(4) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms4;

	@FindBy(css = "#85ef3e61dc876f372ae50b07a250d041f40845c7 div:nth-of-type(1) div.section-content div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms5;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	private WebElement shopPhones1;

	@FindBy(css = "a[href='//www.t-mobile.com/cart?icid=WMM_TM_Q417PLANSP_A94C7S14M111072']")
	private WebElement shopPhones2;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-plans?icid=WMD_TMNG_Q417TMONCC_BPOJNQV2YLQ11511']")
	private WebElement shopPrepaidPlans;

	@FindBy(css = "a[href='#divfootermain']")
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	private WebElement smartDevices;

	@FindBy(css = "a[href='//www.t-mobile.com/cart?icid=WMM_TM_Q417PLANSP_X7IY89V2Q9U11067']")
	private WebElement startShopping;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	private WebElement stores;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/student-teacher-smartphone-tablet-discounts?icid=WMM_TM_CAMPUSEXCL_UHTZZ7GR1RT11028']")
	private WebElement studentteacherDiscount;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch2;

	@FindBy(css = "a[href='//support.t-mobile.com/']")
	private WebElement supportHome;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	private WebElement termsConditions;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true']")
	private WebElement termsOfUse;

	@FindBy(css = "a[href='tel:1-800-266-2453']")
	private WebElement tmobile11800;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) div.panel-collapse.collapse.anchorID-639395 div.panel-body a.tmo_tfn_number")
	private WebElement tmobile21800;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) div.panel-collapse.collapse.anchorID-639395 div.panel-body a.tmo_tfn_number")
	private WebElement tmobile31800;

	@FindBy(css = "#e2e62437acc588a82bf7ff23089ebbb7bb57fb22 div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(2)")
	private WebElement tmobileComopeninternet1;

	@FindBy(css = "#85ef3e61dc876f372ae50b07a250d041f40845c7 div:nth-of-type(1) div.section-content div:nth-of-type(4) p a")
	private WebElement tmobileComopeninternet2;

	@FindBy(css = "a[href='https://www.t-mobile.com/company/company-info/consumer/internet-services.html']")
	private WebElement tmobileComopeninternet3;

	@FindBy(css = "a[href='https://business.t-mobile.com']")
	private WebElement tmobileForBusiness;

	@FindBy(css = "a[href='http://www.t-mobile.com/cell-phone-trade-in.html']")
	private WebElement tradeInProgram;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "a[href='http://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_ReturnPolicy&print=true']")
	private WebElement viewReturnPolicy;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement wantToLearnMoreAbout1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement wantToLearnMoreAbout2;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	private WebElement wellHelpYouJoin;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button.accordion-toggle.collapsed.")
	private WebElement whatIfIDontHave1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement whatIfIDontHave2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement whatIfINeedMore1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement whatIfINeedMore2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement whatIfIWantMore1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement whatIfIWantMore2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whatIsNetflixOnUs1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whatIsNetflixOnUs2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) button.accordion-toggle.collapsed.")
	private WebElement whatVersionOfNetflixDo1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement whatVersionOfNetflixDo2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement whoCanGetIt1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement whoCanGetIt2;

	@FindBy(css = "#8766fefbb9b7114ea946b0b1e0dc00397e769cf7 div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(3) a")
	private WebElement wwwNetflixComtermsofuse1;

	@FindBy(css = "#plans div:nth-of-type(3) p:nth-of-type(2) a")
	private WebElement wwwNetflixComtermsofuse2;

	public PlansPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the PlansPage class instance.
	 */
	public PlansPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Plans page loaded");
		} catch (NoSuchElementException e) {
			Assert.fail("Plans Page not loaded");
		}

		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the PlansPage class instance.
	 */
	public PlansPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

	/**
	 * Click on About Link.	 
	 */
	public PlansPage clickAboutLink() {
		about.click();
		return this;
	}

	/**
	 * Click on Accessibility Link.	 
	 */
	public PlansPage clickAccessibilityLink() {
		accessibility.click();
		return this;
	}

	/**
	 * Click on Accessories Link.	
	 */
	public PlansPage clickAccessoriesLink() {
		accessories.click();
		return this;
	}

	/**
	 * Click on Activate Your Prepaid Phone Or Device Link.
	 *
	 * @return the PlansPage class instance.
	 */
	public PlansPage clickActivateYourPrepaidPhoneOrDeviceLink() {
		activateYourPrepaidPhoneOrDevice.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.	 
	 */
	public PlansPage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.	 
	 */
	public PlansPage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 */
	public PlansPage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on Call 1800tmobile Link.	
	 */
	public PlansPage clickCall1800tmobile1Link() {
		call1800tmobile1.click();
		return this;
	}

	/**
	 * Click on Call 1800tmobile Link.
	 */
	public PlansPage clickCall1800tmobile2Link() {
		call1800tmobile2.click();
		return this;
	}

	/**
	 * Click on Call Us Link.	 
	 */
	public PlansPage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Careers Link.	
	 */
	public PlansPage clickCareersLink() {
		careers.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.	 
	 */
	public PlansPage clickCheckItOut1Link() {
		checkItOut1.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.	 
	 */
	public PlansPage clickCheckItOut2Link() {
		checkItOut2.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.	 
	 */
	public PlansPage clickCheckItOut3Link() {
		checkItOut3.click();
		return this;
	}

	/**
	 * Click on Check Order Status Link.	
	 */
	public PlansPage clickCheckOrderStatusLink() {
		checkOrderStatus.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.	
	 */
	public PlansPage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Click Here Link.	 
	 */
	public PlansPage clickClickHereLink() {
		clickHere.click();
		return this;
	}

	/**
	 * Click on Close Video Button.	
	 */
	public PlansPage clickCloseVideoButton() {
		closeVideo.click();
		return this;
	}

	/**
	 * Click on Consumer Information Link.	
	 */
	public PlansPage clickConsumerInformationLink() {
		consumerInformation.click();
		return this;
	}

	/**
	 * Click on Contact Information Link.
	 */
	public PlansPage clickContactInformationLink() {
		contactInformation.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 */
	public PlansPage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.	
	 */
	public PlansPage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Deutsche Telekom Link.	
	 */
	public PlansPage clickDeutscheTelekomLink() {
		deutscheTelekom.click();
		return this;
	}

	/**
	 * Click on Device Support Link.	
	 */
	public PlansPage clickDeviceSupportLink() {
		deviceSupport.click();
		return this;
	}

	/**
	 * Click on Espanol Link.	 
	 */
	public PlansPage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Espaol Link.	
	 */
	public PlansPage clickEspaolEspaolLink() {
		espaolEspaol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.	
	 */
	public PlansPage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.	
	 */
	public PlansPage clickFindAStoreLink() {
		findAStore.click();
		return this;
	}

	/**
	 * Click on Get A Rebate Link.	 
	 */
	public PlansPage clickGetARebateLink() {
		getARebate.click();
		return this;
	}

	/**
	 * Click on Here Link.
	 */
	public PlansPage clickHere1Link() {
		here1.click();
		return this;
	}

	/**
	 * Click on Here Link.	
	 */
	public PlansPage clickHere2Link() {
		here2.click();
		return this;
	}

	/**
	 * Click on How Do I Get Netflix At No Additional Charge How Does It Work
	 * Button.	
	 */
	public PlansPage clickHowDoIGetNetflix1Button() {
		howDoIGetNetflix1.click();
		return this;
	}

	/**
	 * Click on How Do I Get Netflix At No Additional Charge How Does It Work
	 * Button.	
	 */
	public PlansPage clickHowDoIGetNetflix2Button() {
		howDoIGetNetflix2.click();
		return this;
	}

	/**
	 * Click on Httpswww.netflix.comtermsofuse Link.	 
	 */
	public PlansPage clickHttpswwwNetflixComtermsofuseLink() {
		httpswwwNetflixComtermsofuse.click();
		return this;
	}

	/**
	 * Click on Interestbased Ads Link.
	 */
	public PlansPage clickInterestbasedAdsLink() {
		interestbasedAds.click();
		return this;
	}

	/**
	 * Click on International Calling Link.	
	 */
	public PlansPage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on International Rates Link.	
	 */
	public PlansPage clickInternationalRatesLink() {
		internationalRates.click();
		return this;
	}

	/**
	 * Click on Internet Of Things Link.	
	 */
	public PlansPage clickInternetOfThingsLink() {
		internetOfThings.click();
		return this;
	}

	/**
	 * Click on Investor Relations Link.	
	 */
	public PlansPage clickInvestorRelationsLink() {
		investorRelations.click();
		return this;
	}

	/**
	 * Click on Join Us Now Link.	
	 */
	public PlansPage clickJoinUsNowLink() {
		joinUsNow.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.	
	 */
	public PlansPage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.	
	 */
	public PlansPage clickLogIn1Link() {
		logIn1.click();
		return this;
	}

	/**
	 * Click on Log In Link.	
	 */
	public PlansPage clickLogIn2Link() {
		logIn2.click();
		return this;
	}

	/**
	 * Click on Make The Switch Link.	 
	 */
	public PlansPage clickMakeTheSwitchLink() {
		makeTheSwitch.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.	
	 */
	public PlansPage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.	 
	 */
	public PlansPage clickMyTmobileLink() {
		myTmobile.click();
		return this;
	}

	/**
	 * Click on Need To Stream To More Devices Button.	
	 */
	public PlansPage clickNeedToStreamToMore1Button() {
		needToStreamToMore1.click();
		return this;
	}

	/**
	 * Click on Need To Stream To More Devices Button.	
	 */
	public PlansPage clickNeedToStreamToMore2Button() {
		needToStreamToMore2.click();
		return this;
	}

	/**
	 * Click on Open Internet Link.
	 */
	public PlansPage clickOpenInternetLink() {
		openInternet.click();
		return this;
	}

	/**
	 * Click on Phones Link.	
	 */
	public PlansPage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click on Plans Link.	
	 */
	public PlansPage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Plans Services Link.
	 */
	public PlansPage clickPlansServicesLink() {
		plansServices.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.	
	 */
	public PlansPage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 */
	public PlansPage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Press Link.	
	 */
	public PlansPage clickPressLink() {
		press.click();
		return this;
	}

	/**
	 * Click on Privacy Center Link.	
	 */
	public PlansPage clickPrivacyCenterLink() {
		privacyCenter.click();
		return this;
	}

	/**
	 * Click on Privacy Statement Link.
	 */
	public PlansPage clickPrivacyStatementLink() {
		privacyStatement.click();
		return this;
	}

	/**
	 * Click on Public Safety911 Link.
	 */
	public PlansPage clickPublicSafety911Link() {
		publicSafety911.click();
		return this;
	}

	/**
	 * Click on Puerto Rico Link.	
	 */
	public PlansPage clickPuertoRicoLink() {
		puertoRico.click();
		return this;
	}

	/**
	 * Click on Questions About Your Bill Link.
	 */
	public PlansPage clickQuestionsAboutYourBillLink() {
		questionsAboutYourBill.click();
		return this;
	}

	/**
	 * Click on Refill Your Prepaid Account Link.
	 */
	public PlansPage clickRefillYourPrepaidAccountLink() {
		refillYourPrepaidAccount.click();
		return this;
	}

	/**
	 * Click on Scroll To Top Button.
	 */
	public PlansPage clickScrollToTopButton() {
		scrollToTop.click();
		return this;
	}

	/**
	 * Click on See Full Faqs Link.
	 */
	public PlansPage clickSeeFullFaqsLink() {
		seeFullFaqs.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 */
	public PlansPage clickSeeFullTerms1Link() {
		seeFullTerms1.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 */
	public PlansPage clickSeeFullTerms2Link() {
		seeFullTerms2.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 */
	public PlansPage clickSeeFullTerms3Link() {
		seeFullTerms3.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 */
	public PlansPage clickSeeFullTerms4Link() {
		seeFullTerms4.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 */
	public PlansPage clickSeeFullTerms5Link() {
		seeFullTerms5.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 */
	public PlansPage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 */
	public PlansPage clickShopPhones1Link() {
		shopPhones1.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 */
	public PlansPage clickShopPhones2Link() {
		shopPhones2.click();
		return this;
	}

	/**
	 * Click on Shop Prepaid Plans Link.
	 */
	public PlansPage clickShopPrepaidPlansLink() {
		shopPrepaidPlans.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 */
	public PlansPage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 */
	public PlansPage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 */
	public PlansPage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Start Shopping Link.
	 */
	public PlansPage clickStartShoppingLink() {
		startShopping.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 */
	public PlansPage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 */
	public PlansPage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Studentteacher Discount Link.
	 */
	public PlansPage clickStudentteacherDiscountLink() {
		studentteacherDiscount.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 */
	public PlansPage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 */
	public PlansPage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Support Home Link.
	 */
	public PlansPage clickSupportHomeLink() {
		supportHome.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 */
	public PlansPage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Terms Of Use Link.
	 */
	public PlansPage clickTermsOfUseLink() {
		termsOfUse.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 */
	public PlansPage clickTmobile1Link1800() {
		tmobile11800.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 */
	public PlansPage clickTmobile2Link1800() {
		tmobile21800.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 */
	public PlansPage clickTmobile3Link1800() {
		tmobile31800.click();
		return this;
	}

	/**
	 * Click on Tmobile.comopeninternet Link.
	 */
	public PlansPage clickTmobileComopeninternet1Link() {
		tmobileComopeninternet1.click();
		return this;
	}

	/**
	 * Click on Tmobile.comopeninternet Link.
	 */
	public PlansPage clickTmobileComopeninternet2Link() {
		tmobileComopeninternet2.click();
		return this;
	}

	/**
	 * Click on Tmobile.comopeninternet Link.
	 */
	public PlansPage clickTmobileComopeninternet3Link() {
		tmobileComopeninternet3.click();
		return this;
	}

	/**
	 * Click on Tmobile For Business Link.
	 */
	public PlansPage clickTmobileForBusinessLink() {
		tmobileForBusiness.click();
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 */
	public PlansPage clickTradeInProgramLink() {
		tradeInProgram.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 */
	public PlansPage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 */
	public PlansPage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 */
	public PlansPage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on View Return Policy Link.
	 */
	public PlansPage clickViewReturnPolicyLink() {
		viewReturnPolicy.click();
		return this;
	}

	/**
	 * Click on Want To Learn More About Our Unlimited Talk Text Only Plan Go To The
	 * Link Below For Details. Button.
	 */
	public PlansPage clickWantToLearnMoreAbout1Button() {
		wantToLearnMoreAbout1.click();
		return this;
	}

	/**
	 * Click on Want To Learn More About Our Unlimited Talk Text Only Plan Go To The
	 * Link Below For Details Button.
	 */
	public PlansPage clickWantToLearnMoreAbout2Button() {
		wantToLearnMoreAbout2.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 */
	public PlansPage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 */
	public PlansPage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Click on What If I Dont Have A Netflix Account Button.
	 */
	public PlansPage clickWhatIfIDontHave1Button() {
		whatIfIDontHave1.click();
		return this;
	}

	/**
	 * Click on What If I Dont Have A Netflix Account Button.
	 */
	public PlansPage clickWhatIfIDontHave2Button() {
		whatIfIDontHave2.click();
		return this;
	}

	/**
	 * Click on What If I Need More Than 12 Lines Can I Get More On The Same Account
	 * Button.
	 */
	public PlansPage clickWhatIfINeedMore1Button() {
		whatIfINeedMore1.click();
		return this;
	}

	/**
	 * Click on What If I Need More Than 12 Lines Can I Get More On The Same Account
	 * Button.
	 */
	public PlansPage clickWhatIfINeedMore2Button() {
		whatIfINeedMore2.click();
		return this;
	}

	/**
	 * Click on What If I Want More Than Four Voice Lines What Would The Price Be
	 * Button.
	 */
	public PlansPage clickWhatIfIWantMore1Button() {
		whatIfIWantMore1.click();
		return this;
	}

	/**
	 * Click on What If I Want More Than Four Voice Lines What Would The Price Be
	 * Button.
	 */
	public PlansPage clickWhatIfIWantMore2Button() {
		whatIfIWantMore2.click();
		return this;
	}

	/**
	 * Click on What Is Netflix On Us Button.
	 */
	public PlansPage clickWhatIsNetflixOnUs1Button() {
		whatIsNetflixOnUs1.click();
		return this;
	}

	/**
	 * Click on What Is Netflix On Us Button.
	 */
	public PlansPage clickWhatIsNetflixOnUs2Button() {
		whatIsNetflixOnUs2.click();
		return this;
	}

	/**
	 * Click on What Version Of Netflix Do I Get Button.
	 */
	public PlansPage clickWhatVersionOfNetflixDo1Button() {
		whatVersionOfNetflixDo1.click();
		return this;
	}

	/**
	 * Click on What Version Of Netflix Do I Get Button.
	 */
	public PlansPage clickWhatVersionOfNetflixDo2Button() {
		whatVersionOfNetflixDo2.click();
		return this;
	}

	/**
	 * Click on Who Can Get It Button.
	 */
	public PlansPage clickWhoCanGetIt1Button() {
		whoCanGetIt1.click();
		return this;
	}

	/**
	 * Click on Who Can Get It Button.
	 */
	public PlansPage clickWhoCanGetIt2Button() {
		whoCanGetIt2.click();
		return this;
	}

	/**
	 * Click on Www.netflix.comtermsofuse Link.
	 */
	public PlansPage clickWwwNetflixComtermsofuse1Link() {
		wwwNetflixComtermsofuse1.click();
		return this;
	}

	/**
	 * Click on Www.netflix.comtermsofuse Link.
	 */
	public PlansPage clickWwwNetflixComtermsofuse2Link() {
		wwwNetflixComtermsofuse2.click();
		return this;
	}

	/**
	 * Set Price Slider Range field.
	 */
	public PlansPage setPriceSliderRangeField() {
		return this;
	}

	/**
	 * Set value to Search Text field.
	 */
	public PlansPage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set value to Search Text field.
	 */
	public PlansPage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}

}
