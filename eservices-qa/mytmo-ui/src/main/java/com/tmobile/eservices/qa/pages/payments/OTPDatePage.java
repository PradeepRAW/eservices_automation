package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class OTPDatePage extends CommonPage {
	
	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//div[contains(text(),'Date')]")
	private WebElement dateBlade;

    @FindBy(css = "otp-payment-date p.button-title")
    private WebElement datePageHeader;

    @FindBy(css = "i[class='fa fa-circle'] span.text-black.p-l-5.p-b-5")
    private WebElement selectedDate;

    @FindBy(css = "i[class='fa fa-circle-o p-l-15 ng-scope'] span.text-black.p-l-5.p-b-5")
    private WebElement dueDate;

    @FindBy(css = "otp-payment-date button.btn-secondary")
    private WebElement dateBackBtn;

    @FindBy(css = ".row.overlay-text-height div p")
    private WebElement immediatePaymentsMsg;

    
    @FindBy(xpath = "//span[@class='day ng-binding ng-scope' and @ng-repeat='dateObj in week']")
	private List<WebElement> activeDates;

    @FindBy(xpath = "//p[text()='Update']/ancestor::button")
    private WebElement btnupdate;
  
  
    
    private By otpSpinner = By.cssSelector("i.fa.fa-spinner");
	private final String pageUrl = "/date";
	private By otpNewSpinner = By.cssSelector(".loader.spinner-image");

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public OTPDatePage(WebDriver webDriver) {
		super(webDriver);
	}
	/**
     * Verify that current page URL matches the expected URL.
     */
    public OTPDatePage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public OTPDatePage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
    		waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
	    	waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("OTP Date page not loaded");
    	}
        return this;
    }

	public String selectdateforFDP() {
		String selecteddate=null;
		int elementsdate=activeDates.size();
		if(elementsdate>0) {
			String strdate=activeDates.get(elementsdate-1).getText();
			activeDates.get(elementsdate-1).click();
			return strdate;
		}
		return selecteddate;
	}
    
    
    public OTPDatePage clickDateIcon() {
		try {
			clickElementWithJavaScript(dateBlade);
			Reporter.log("Clicked on Date Icon");
		} catch (Exception e) {
			Assert.fail("Failed to click on Date icon ");
		}
		return this;
	}

	/**
	 * verify the date page header
	 *
	 * @return true/false
	 */
	public OTPDatePage verifyDatePageHeader() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpNewSpinner));
			waitFor(ExpectedConditions.urlContains("date"));
			datePageHeader.isDisplayed();
			Reporter.log("Verified date page header");
		} catch (Exception e) {
			Assert.fail("Date page missing");
		}
		return this;
	}

    /**
     * verify date calendar is displayed
     *
     * @return true/false
     */
    public OTPDatePage verifycalendar() {
        try {
            selectedDate.isDisplayed();
            selectedDate.getText().equals("Selected date");
            dueDate.isDisplayed();
            dueDate.getText().equals("Due date");
            Reporter.log("Verified calender");
        } catch (Exception e) {
        	if (immediatePaymentsMsg.isDisplayed()) {
        		immediatePaymentsMsg.getText().equals("Your account is eligible for Immediate payments only");
        		Reporter.log("Verified Calendar, This user is ineligible date for selection at this moment");
			}else {
				Assert.fail("Calender not found");
			}
        }
        return this;
    }

    public OTPDatePage clickDateBackBtn() {
        try {
            dateBackBtn.click();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
            Reporter.log("Clicked on date back button");
        } catch (Exception e) {
            Assert.fail("Date back button not found");
        }
        return this;
    }

    public OTPDatePage verifyInelgibleDateSelectionMsg() {
        try {
            waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
            waitFor(ExpectedConditions.invisibilityOfElementLocated(otpNewSpinner));
            waitFor(ExpectedConditions.urlContains("date"));
            immediatePaymentsMsg.isDisplayed();
            immediatePaymentsMsg.getText().equals("Your account is eligible for Immediate payments only");
            Reporter.log("Verified ineligible date selection");
        } catch (Exception e) {
            Assert.fail("Ineligible date selection filed not found");
        }
        return this;
    }

public OTPDatePage Clickupdatebutton() {
	try {
		btnupdate.click();
		Reporter.log("Update button clicked");
	}catch(Exception e) {
		Assert.fail("Update button is not located");
	}
	return this;
}
    
}
