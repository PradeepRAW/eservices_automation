package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class TPPPreScreenIntro extends TmngCommonPage {

	private final String pageUrl = "/pre-screen";

	@FindBy(css = ".pre-screen-container h2")
	private WebElement preScreenIntroPageHeader;

	@FindBy(css = ".actions-container>button[data-analytics-value*='Let']")
	private WebElement letsGoCTAOnpreScreen;

	@FindBy(css = "button[data-analytics-value*='No thanks']")
	private WebElement noThanksCTAOnpreScreen;

	@FindBy(css = "*[href*='shop']")
	private WebElement existingCustomerLink;

	@FindBy(css = "*[aria-label*='Sprint login']")
	private WebElement sprintCustomerLink;

	public TPPPreScreenIntro(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Pre-screen Intro page
	 *
	 */
	public TPPPreScreenIntro verifyPreScreenIntroPage() {
		try {
			checkPageIsReady();
			verifyTPPPreScreenPageUrl();
			Assert.assertTrue(preScreenIntroPageHeader.isDisplayed(), "Pre screen is not loaded");
			Reporter.log("Pre screen into page loaded successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pre-screen Intropage.");
		}
		return this;
	}

	/**
	 * Click Let's Go CTA on Pre-screen Intro page
	 *
	 */
	public TPPPreScreenIntro clickLetsGoCTAOnPreScreenIntroPage() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(letsGoCTAOnpreScreen), 60);
			clickElementWithJavaScript(letsGoCTAOnpreScreen);
			Reporter.log("Clicked on Let's go CTA on Pre screen into page");
		} catch (Exception e) {
			Assert.fail("Failed to Click Let's Go CTA on Pre-screen Intro page");
		}
		return this;
	}

	/**
	 * Verify Let's Go CTA on Pre-screen Intro page
	 *
	 */
	public TPPPreScreenIntro verifyLetsGoCTAOnPreScreenIntroPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(letsGoCTAOnpreScreen), 60);
			Assert.assertTrue(letsGoCTAOnpreScreen.isDisplayed(), "Let's Go CTA is not displayed on Pre screen.");
			Reporter.log("Let's Go CTA is displayed on Pre screen");
		} catch (Exception e) {
			Assert.fail("Failed to verify Let's Go CTA on Pre-screen Intro page");
		}
		return this;
	}

	/**
	 * Verify No Thanks cta on Pre-screen Intro page
	 * 
	 * @return
	 */
	public TPPPreScreenIntro verifyNoThanksCtaOnPrescreenIntroPage() {
		try {
			Assert.assertTrue(noThanksCTAOnpreScreen.isDisplayed(), "No thanks CTA is not displayed on Pre screen.");
			Reporter.log("No Thanks CTA is displayed on Pre screen");
		} catch (Exception e) {
			Assert.fail("Failed to verify No Thanks CTA on Pre-screen Intro page");
		}
		return this;
	}

	/**
	 * Verify existing customer link
	 * 
	 * @return
	 */
	public TPPPreScreenIntro verifyExistingCustomerLink() {
		try {
			Assert.assertTrue(existingCustomerLink.isDisplayed(),
					"Existing Customer Link is not displayed on Pre screen.");
			Reporter.log("Existing Customer Link is displayed on Pre screen");
		} catch (Exception e) {
			Assert.fail("Failed to verify Existing Customer Link on Pre-screen Intro page");
		}
		return this;
	}

	/**
	 * Verify Sprint customer link
	 * 
	 * @return
	 */
	public TPPPreScreenIntro verifySprintCusotmerLink() {
		try {
			Assert.assertTrue(sprintCustomerLink.isDisplayed(), "Sprint Customer Link is not displayed on Pre screen.");
			Reporter.log("Sprint Customer Link is displayed on Pre screen");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sprint Customer Link on Pre-screen Intro page");
		}
		return this;
	}

	/**
	 * Click on Existing customer link
	 * 
	 * @return
	 */
	public TPPPreScreenIntro clickExistingCusotmerLink() {
		try {
			existingCustomerLink.click();
			Reporter.log("Clicked on Existing Customer Link on Pre screen");
		} catch (Exception e) {
			Assert.fail("Failed to click on Existing Customer Link on Pre-screen");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TPPPreScreenIntro verifyTPPPreScreenPageUrl() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains(pageUrl), "TPP Pre-screen Page not loaded");
		} catch (Exception e) {
			Assert.fail("Failed to verify cart URL");
		}
		return this;
	}

	/**
	 * Click No Thanks on Pre-screen Intro page
	 *
	 */
	public TPPPreScreenIntro clickNoThanksOnPreScreenIntroPage() {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			clickElementWithJavaScript(noThanksCTAOnpreScreen);
			Reporter.log("Clicked on No Thanks on Pre screen into page");
		} catch (Exception e) {
			Assert.fail("Failed to Click No Thanks on Pre-screen Intro page");
		}
		return this;
	}
}
