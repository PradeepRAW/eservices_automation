package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class NewOTPPage extends CommonPage {

	private final String pageUrl = "payYourBill";

	@FindBy(xpath = "span.Display3")
	private WebElement otpheader;

	@FindBy(css = "[routerlink='termsAndCondition']")
	private WebElement termsAndConditionsLink;

	@FindBy(css = "div.padding-vertical-small.cursor.active")
	private List<WebElement> otpBlades;

	@FindBy(css = "button.PrimaryCTA.full-btn-width")
	private WebElement agreeAndSubmitbtn;

	@FindBy(xpath = "//span[contains(@class,'Card')]/following-sibling::p")
	private WebElement storedCardNumber;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement cancelBtn;

	@FindBy(xpath = "//span[text()='payment arrangement']")
	private WebElement paymentArrangementLink;

	public NewOTPPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewOTPPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewOTPPage verifyPageLoaded() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				getDriver().navigate().refresh();
				Thread.sleep(2000);
				verifyPageUrl();
				verifyOTPPageHeader();
			} else {
				checkPageIsReady();

				verifyPageUrl();
				Reporter.log("OTP page displayed");
			}
		} catch (Exception e) {
			Assert.fail("OTP page not displayed");
		}
		return this;
	}

	public NewOTPPage verifyOTPPageHeader() {
		try {
			otpheader.isDisplayed();
		} catch (Exception e) {
			Assert.fail("OTP page header not displayed");
		}
		return this;
	}

	/**
	 * Click edit button for amount
	 */
	public NewOTPPage clickamountIcon() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(otpBlades.get(0)));
			clickElementWithJavaScript(otpBlades.get(0));
			Reporter.log("Clicked on Amount icon");
		} catch (Exception e) {
			Assert.fail("Amount icon not found");
		}
		return this;
	}

	/**
	 * click add payment method blade
	 */
	public NewOTPPage clickPaymentMethodBlade() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("screen-lock")));
			waitFor(ExpectedConditions.elementToBeClickable(otpBlades.get(1)));
			checkPageIsReady();
			otpBlades.get(1).click();
			Reporter.log("Clicked on payment method blade");
		} catch (Exception e) {
			Assert.fail("Payment method blade not found");
		}
		return this;
	}

	/**
	 * click date blade
	 */
	public NewOTPPage clickDateBlade() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("screen-lock")));
			waitFor(ExpectedConditions.elementToBeClickable(otpBlades.get(2)));
			checkPageIsReady();
			otpBlades.get(2).click();
			Reporter.log("Clicked on date blade");
		} catch (Exception e) {
			Assert.fail("date blade not found");
		}
		return this;
	}

	/**
	 * Verify Agree And Submit Button is displayed and enabled
	 * 
	 * @return boolean
	 */
	public NewOTPPage verifyAgreeAndSubmitButtonEnabledOTPPage() {
		try {

			waitFor(ExpectedConditions.visibilityOf(agreeAndSubmitbtn));
			agreeAndSubmitbtn.isEnabled();
			Reporter.log("Verified Agree and submit button");
		} catch (Exception e) {
			Verify.fail("Agree and submit button not found");
		}
		return this;
	}

	/**
	 * get payment card number on blade
	 * 
	 * @return boolean
	 */
	public String getStoredCardNumber() {
		String storedCard = null;
		try {
			storedCard = storedCardNumber.getText();
			Reporter.log("Stored card number " + storedCard);
		} catch (Exception e) {
			Assert.fail("Stored card number not found");
		}
		return storedCard;
	}

	/**
	 * click cancel otp button
	 */
	public NewOTPPage clickCancelBtn() {
		try {
			clickElementWithJavaScript(cancelBtn);

			Reporter.log("Clicked on Cancel button");
		} catch (Exception e) {
			Verify.fail("Cancel button not found");
		}
		return this;
	}

	/**
	 * Click payment Arrangement link
	 */
	public NewOTPPage clickPaymentArrangementLink() {
		try {

			waitFor(ExpectedConditions.visibilityOf(paymentArrangementLink));
			paymentArrangementLink.click();
			Reporter.log("Clicked on PaymentArrangement link");
		} catch (Exception e) {
			Assert.fail("PaymentArrangement link not found");
		}
		return this;
	}

	public NewOTPPage clickTermsAndConditions() {
		try {

			termsAndConditionsLink.click();
			Reporter.log("Clicked on Terms and Conditions link");
		} catch (Exception e) {
			Verify.fail("Terms and Conditions link not found");
		}
		return this;
	}

}
