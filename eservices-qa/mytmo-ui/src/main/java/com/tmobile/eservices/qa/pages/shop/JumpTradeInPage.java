/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class JumpTradeInPage extends CommonPage {
	
	public static final String JUMP_TRADEIN_PAGEURL = "purchase-jump/tradein";
	
	@FindBy(xpath = "//div[contains(text(), 'LCD and Display?')]//..//i")
	private WebElement lcdAndDisplay;
	
	@FindBy(xpath = "//div[contains(text(), 'Power on?')]//..//i")
	private WebElement powerOnchkBox;
	
	@FindBy(xpath = "//div[contains(text(), 'Find My')]//..//i")
	private WebElement disableFindMyPhoneChkBox;
	
	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement continueButton;
	
		
	public JumpTradeInPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Jump PLP Page
	 * 
	 * @return
	 */
	public JumpTradeInPage verifyPageUrl() {
		
		waitforSpinner();
		checkPageIsReady();
		try{
			getDriver().getCurrentUrl().contains(JUMP_TRADEIN_PAGEURL);
		}catch(Exception e){
			Assert.fail("Jump TradeIn page is not displayed");
		}
		return this;
	}
	
	/**
	 * Select LCD Display check box
	 * @return
	 */
	public JumpTradeInPage clickLCDDisplayCheckBox() {
		try {
			lcdAndDisplay.click();
			Reporter.log("LCDDisplayCheckBox is displayed");
		} catch (Exception e) {
			Assert.fail("LCDDisplayCheckBox is not displayed");
		}
		return this;
	}
	
	/**
	 * Select Device Power on check box
	 * @return
	 */
	public JumpTradeInPage clickPowerOnCheckBox() {
		try {
			waitforSpinner();
			powerOnchkBox.click();
		} catch (Exception e) {
			Assert.fail("PowerOnCheckBox is not available to click");
		}
		return this;
	}
	
	/**
	 * Select Device Power on check box
	 * @return
	 */
	public JumpTradeInPage clickDisableFindMyPhoneCheckBox() {
		try {
			waitforSpinner();
			disableFindMyPhoneChkBox.click();
		} catch (Exception e) {
			Assert.fail("Disable Find My Phone checkbox is not available to click");
		}
		return this;
	}
	
	/**
	 * Click Continu button
	 * @return
	 */
	public JumpTradeInPage clickContinueButton() {
		try {
			continueButton.click();
			Reporter.log("ContinueButton is displayed");
		} catch (Exception e) {
			Assert.fail("ContinueButton is not displayed to click");
		}
		return this;
	}
	
	
}
