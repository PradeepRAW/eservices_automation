package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;

import io.appium.java_client.AppiumDriver;

public class WatchPlanPage extends CommonPage {

	@FindBy(css = "[ng-bind*='continueCTAText']")
	private WebElement addPlanCTAWatchPlanPage;

	@FindBy(css = "img[class='phoneImage']")
	private List<WebElement> watchPlanImg;

	@FindBy(css = "span[ng-bind='$ctrl.contentData.dueMonthlyText']")
	private WebElement dueMonthlyTextWatchPlan;

	@FindBy(css = "a[ng-bind='$ctrl.contentData.depositText']")
	private WebElement watchPlanDepositText;

	@FindBy(css = "span[ng-bind*='$ctrl.contentData.dueTodayText']")
	private WebElement watchPlanDueTodayText;

	@FindBy(xpath = "//span[text()= 'DUE TODAY']/../..//*[contains(@class,'Display')]")
	private List<WebElement> watchPlanDueTodayPrice;

	@FindBy(css = "span[class='Display2']")
	private WebElement watchPlanNameHeader;

	@FindBy(css = "span[ng-bind*='consolidatedWatchPlanText']")
	private WebElement watchPlanText;

	@FindBy(css = "span[ng-bind*='consolidatedWatchAALTitle']")
	private WebElement watchPlanGetCallText;

	@FindBy(css = "#planlink")
	private WebElement watchPlanLink;

	@FindBy(css = "[class*='black select-label']")
	private WebElement lineSelectionSection;

	@FindBy(css = "[for*='radio']")
	private List<WebElement> radioButtonForLine;

	@FindBy(css = "[ng-bind*='phone.name']")
	private List<WebElement> nameForLine;

	@FindBy(css = "[ng-bind*='phone.msisdn']")
	private List<WebElement> phoneNumberForLine;

	@FindBy(xpath = "//span[text()= 'DUE MONTHLY']/../..//*[contains(@class,'Display')]")
	private List<WebElement> dueMonthlyPriceWatchPlan;
	
	@FindBy(css = "div[class*='glueAnimationIn'] div[class*='dialogPaddingLCModal']")
	private WebElement depositModal;
	
	@FindBy(css = "span[ng-bind*='depositModalHeader']")
	private WebElement depositModalHeaderText;	
	
	@FindBy(css = "span[ng-bind='$ctrl.contentData.depositModalContent']")
	private WebElement depositModalBodyText;	
	
	@FindBy(css = "i.icox.ico-closeIcon")
	private WebElement depositModalCloseButton;
	
	@FindBy(css = "[ng-bind*='consolidatedWatchAutopay']")
	private WebElement dueMonthlyAutoPayText;
	
	
	
	/**
	 * 
	 * @param webDriver
	 */
	public WatchPlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public WatchPlanPage verifyPageUrl() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("plan"),
					"URL for Consolidated Charges Page is not correct");
		} catch (Exception e) {
			Assert.fail("Failed to verify consolidated Charges Page url");
		}
		return this;
	}

	/**
	 * Verify watch plan Page
	 * 
	 * @return
	 */
	public WatchPlanPage verifyWatchPlanPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(addPlanCTAWatchPlanPage), 10);
			verifyPageUrl();
			Reporter.log("Navigated to watch Plan  page");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to watch Plan  page");
		}
		return this;
	}

	/**
	 * Verify watch plan image displayed
	 */
	public WatchPlanPage verifyWatchPlanImgDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(watchPlanImg.get(1).isDisplayed(), "Watch plan image is not displayed on mobile");
				Reporter.log("Watch plan image is displayed on mobile");
			} else {
				Assert.assertTrue(watchPlanImg.get(0).isDisplayed(), "Watch plan image is not displayed on desktop");
				Reporter.log("Watch plan image is displayed on desktop");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display Watch plan image");
		}
		return this;
	}

	/**
	 * Verify watch plan due monthly text displayed
	 */
	public WatchPlanPage verifyWatchPlanDueMonthlyTextDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(dueMonthlyTextWatchPlan.isDisplayed(), "Watch plan due monthly text is not displayed");
			Reporter.log("Watch plan due monthly text is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display Watch plan due monthly text");
		}
		return this;
	}

	/**
	 * Verify watch plan deposit text displayed
	 */
	public WatchPlanPage verifyWatchPlanDepositTextDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(watchPlanDepositText.isDisplayed(), "Watch plan deposit text is not displayed");
			Reporter.log("Watch plan deposit text is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display Watch plan deposit text");
		}
		return this;
	}

	/**
	 * Verify watch plan due today text displayed
	 */
	public WatchPlanPage verifyWatchPlanDueTodayTextDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(watchPlanDueTodayText.isDisplayed(), "Watch plan due today text is not displayed");
			Reporter.log("Watch plan due today text is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display Watch plan due today text");
		}
		return this;
	}

	/**
	 * Verify watch plan due today price displayed
	 */
	public WatchPlanPage verifyWatchPlanDueTodayPriceDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(watchPlanDueTodayPrice.get(0).isDisplayed(),
					"$ is not displayed in watch plan due today price");
			Assert.assertTrue(watchPlanDueTodayPrice.get(1).isDisplayed(),
					"Subscript amount is not displayed in watch plan due today price");
			Assert.assertTrue(watchPlanDueTodayPrice.get(2).isDisplayed(),
					"Superscript amount is not displayed in watch plan due today price");
			Reporter.log("Watch plan due today price is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display Watch plan due today price");
		}
		return this;
	}

	/**
	 * Verify Add Plan and link number CTA
	 */
	public WatchPlanPage verifyAddPlanLinkNumberCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addPlanCTAWatchPlanPage), 5);
			Assert.assertTrue(addPlanCTAWatchPlanPage.isDisplayed(), "Add Plan Link Number CTA is not displayed");
			Reporter.log("Add Plan Link Number CTA is displayed.");

		} catch (Exception e) {
			Assert.fail("Failed to display Add Plan Link Number CTA");
		}
		return this;
	}

	/**
	 * Verify watch plan rate plan section displayed
	 */
	public WatchPlanPage verifyWatchPlanRatePlanSection() {
		try {
			waitforSpinner();
			Assert.assertTrue(watchPlanNameHeader.isDisplayed(), "rate plan name is not displayed");
			Assert.assertTrue(watchPlanText.isDisplayed(), "Plan text is not displayed");
			Assert.assertTrue(watchPlanGetCallText.isDisplayed(), "get call text is not displayed");
			Assert.assertTrue(watchPlanLink.isDisplayed(), "rate plan link not displayed");
			Reporter.log("Rate plan section is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display the rate plan section");
		}
		return this;
	}

	/**
	 * Verify Line Selection Section on watch Plan Page
	 *
	 * @return
	 */
	public WatchPlanPage verifyLineSectionForWearables() {
		try {
			checkPageIsReady();
			Assert.assertTrue(lineSelectionSection.isDisplayed(), "Line Selection Section not displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Selection Section");
		}
		return this;
	}

	/**
	 * Verify Line Section Authorable Text on watch Plan Page
	 *
	 * @return
	 */
	public WatchPlanPage verifyLineSectionAuthorableText() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(lineSelectionSection));
			Assert.assertTrue(lineSelectionSection.isDisplayed(), "Line Section Authorable Text not displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Section Authorable Text");
		}
		return this;
	}

	/**
	 * Verify Line Section verify Line Section Radio Btn, Name And PhoneNumber on
	 * watch Plan Page
	 *
	 * @return
	 */
	public WatchPlanPage verifyLineSectionRadioBtnNameAndPhoneNumber() {
		try {
			checkPageIsReady();
			for (WebElement radio : radioButtonForLine) {
				Assert.assertTrue(radio.isDisplayed(), "Radio Button is not displayed");
			}

			for (WebElement name : nameForLine) {
				Assert.assertTrue(name.isDisplayed(), "Line Name is not displayed");
			}

			for (WebElement phoneNumber : phoneNumberForLine) {
				Assert.assertTrue(phoneNumber.isDisplayed(), "Phone Number for Line is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Section Radio Btn, Name And PhoneNumber");
		}
		return this;
	}

	/**
	 * Verify Line Section Authorable Text on watch Plan Page
	 *
	 * @return
	 */
	public WatchPlanPage verifyRadioButtonForLoggerUserSelected(String input) {
		try {
			checkPageIsReady();
			String number = input.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
			WebElement Element = getDriver().findElement(
					By.xpath("//span[contains(text(),'" + number + "')]/../..//input[@checked='checked']/../div"));
			Assert.assertTrue(isElementDisplayed(Element), "Loggen In number is not selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify line number for logged in user");
		}
		return this;
	}

	/**
	 * Verify watch plan due monthly price displayed
	 */
	public WatchPlanPage verifyWatchPlanDueMonthlyPriceDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(dueMonthlyPriceWatchPlan.get(0).isDisplayed(),
					"$ is not displayed in watch plan due monthly price");
			Assert.assertTrue(dueMonthlyPriceWatchPlan.get(1).isDisplayed(),
					"Subscript amount is not displayed in watch plan due monthly price");
			Assert.assertTrue(dueMonthlyPriceWatchPlan.get(2).isDisplayed(),
					"Superscript amount is not displayed in watch plan due monthly price");
			Reporter.log("Watch plan due monthly price is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display Watch plan due monthly price");
		}
		return this;
	}

	/**
	 * Verify Line Section Authorable Text on watch Plan Page
	 *
	 * @return
	 */
	public WatchPlanPage verifyUserNameForLoggedUser(String name) {
		try {
			checkPageIsReady();
			Assert.assertTrue(getDriver()
					.findElement(By
							.xpath("//span[contains(text(), '" + name + "')]/../..//input[@checked='checked']/../div"))
					.isDisplayed(), "User name is not diplayed for selected User");
			Reporter.log("User name for Logged User is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to verify User name for Logged User");
		}
		return this;
	}
	
	
	/**
	 * Click Deposit Hyperlink
	 * 
	 */
	public WatchPlanPage clickDepositHyperlink() {
		try {
			watchPlanDepositText.click();
			Reporter.log("Deposit Hyperlink is clicked");
		} catch (Exception ex) {
			Assert.fail("Failed to click Deposit Hyperlink");

		}
		return this;
	}

	/**
	 * Verify Deposit Modal
	 *
	 * @return
	 */
	public WatchPlanPage verifyDepositModal() {
		try {
			checkPageIsReady();
			Assert.assertTrue(depositModal.isDisplayed(), "Deposit modal is not displayed");
			Reporter.log("Deposit modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Deposit modal");
		}
		return this;
	}
	
	
	
	/**
	 * Verify Deposit Modal Header Text
	 *
	 * @return
	 */
	public WatchPlanPage verifyDepositModalHeaderText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(depositModalHeaderText.isDisplayed(), "Deposit modal header text not displayed");
			Reporter.log("Deposit modal header text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Deposit modal header text");
		}
		return this;
	}
	
	/**
	 * Verify Deposit Modal Body Text
	 *
	 * @return
	 */
	public WatchPlanPage verifyDepositModalBodyText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(depositModalBodyText.isDisplayed(), "Deposit modal body text is not displayed");
			Reporter.log("Deposit modal body text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Deposit modal body text");
		}
		return this;
	}
	
	
	/**
	 * Verify Deposit Modal Close Button
	 *
	 * @return
	 */
	public WatchPlanPage verifyDepositModalCloseButton() {
		try {
			checkPageIsReady();
			Assert.assertTrue(depositModalCloseButton.isDisplayed(), "Deposit modal close button not displayed");
			Reporter.log("Deposit modal close button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Deposit modal close button");
		}
		return this;
	}
	
	/**
	 * Click modal window close button
	 * 
	 * @return
	 */
	public WatchPlanPage clickModalWindowCloseBtn() {
		try {
			depositModalCloseButton.click();
		} catch (Exception e) {
			Assert.fail("Click on close button failed ");
		}
		return this;
	}
	
	/**
	 * Verify Deposit Modal is Closed
	 *
	 * @return
	 */
	public WatchPlanPage verifyDepositModalNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(depositModal), "Deposit modal is not closed");
			Reporter.log("Deposit modal is closed");
		} catch (Exception e) {
			Assert.fail("Failed to close Deposit modal");
		}
		return this;
	}
	
	/**
	 * Verify AutoPay Text below Due Monthly 
	 *
	 * @return
	 */
	public WatchPlanPage verifyAutoPayTextBelowDueMonthlyDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(dueMonthlyAutoPayText.isDisplayed(), "AutoPay Text below due monthly is not displayed");
			Reporter.log("AutoPay Text below due monthly is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display AutoPay Text below due monthly");
		}
		return this;
	}
	
}
