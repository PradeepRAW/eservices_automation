package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * @author blakshminarayana
 *
 */
public class ChangePasswordPage extends CommonPage {

	public static final String pageUrl = "/oauth2/v1/forgotpassword";
	public static final String pageText = "Reset Your Password";

	@FindBy(id = "confirmcode")
	private WebElement confirmcode;

	@FindBy(css = "div.ui_subhead.ng-binding")
	private WebElement checkMail;

	@FindBy(id = "password")
	private WebElement passwordText;

	@FindBy(id = "reEnterNewPassword")
	private WebElement confirmPasswordText;

	@FindBy(css = "input[value='Next']")
	private WebElement nextButton;
	@FindBy(css = "div.ui_subhead.ng-binding")
	private WebElement changePasswordText;

	@FindAll({ @FindBy(css = "a.lmenudelall"), @FindBy(css = "a.igif.enva") })
	private WebElement deleteAll;

	@FindBy(css = "div#delmenu li a.lmen_all")
	private WebElement emptyInbox;

	@FindBy(css = "table.barremail td a[href*='javascript:suppr_sel()']")
	private WebElement mobileEmptyInbox;

	@FindBys(@FindBy(css = "span.lmfd span.lmf"))
	private List<WebElement> inboxMail;

	@FindBy(id = "login")
	private WebElement mailTextBox;

	@FindAll({ @FindBy(css = "input[value='Check Inbox']"), @FindBy(css = "input.sbut") })
	private WebElement checkInboxButton;

	@FindBy(xpath = "//div[@id='mailmillieu']//div[2]")
	private WebElement mailContext;

	@FindBy(id = "ifinbox")
	private WebElement inboxFrame;

	@FindBy(id = "ifmail")
	private WebElement mailContextFrame;

	@FindBy(css = "span#nbmail")
	private WebElement mails;

	/**
	 * 
	 * @param webDriver
	 */
	public ChangePasswordPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * verify change password text
	 * 
	 * @return value
	 */
	public ChangePasswordPage verifyChangePasswordText() {
		checkPageIsReady();
		try {
			changePasswordText.getText();
			Reporter.log("Able to fetch Change Password text.");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to fetch Change Password text.");
		}
		return this;
	}

	/**
	 * this method is to perform change password
	 * 
	 * @param passwordAndConfirmPassword
	 */
	public ChangePasswordPage performChangePassword(String passwordAndConfirmPassword) {
		try {
			sendTextData(passwordText, passwordAndConfirmPassword);
			sendTextData(confirmPasswordText, passwordAndConfirmPassword);
			nextButton.click();
			Reporter.log("Successfully change the password and navigated to HomePage");
		} catch (NoSuchElementException e) {
			Assert.fail("Password Change was unsuccessful");
		}
		return this;
	}

	/**
	 * @param confirmCode
	 */
	public ChangePasswordPage enterConfirmCode(String confirmCode) {
		checkPageIsReady();
		try {
			confirmcode.sendKeys(confirmCode);
			Reporter.log("Successfully sent Confirmation Code.");
		} catch (NoSuchElementException e) {
			Assert.fail("Sending Confirmation Code was unsuccessful.");
		}
		return this;
	}

	/**
	 * click on Submit next button
	 */
	public ChangePasswordPage submitNextButton() {
		try {
			nextButton.click();
			Reporter.log("Successfully clicked next button.");
		} catch (NoSuchElementException e) {
			Assert.fail("Error: Next Button is not clickable.");
		}
		return this;
	}

	/**
	 * verify check mail is displayed or not
	 * 
	 * @return boolean
	 */
	public ChangePasswordPage checkMail() {
		checkPageIsReady();
		try {
			Assert.assertTrue(checkMail.isDisplayed());
			Reporter.log("Check your email text is visible.");
		} catch (NoSuchElementException e) {
			Assert.fail("Error: Check your email text is unavailable.");
		}
		return this;
	}

	/**
	 * Delete Mails
	 */
	public ChangePasswordPage clickDelete() {
		checkPageIsReady();
		try {
			if (!mails.getText().contains("0")) {
				switchToFrame(inboxFrame);
				deleteAll.click();
				if (getDriver() instanceof AppiumDriver) {
					switchToDefaultContent();
					checkPageIsReady();
					mobileEmptyInbox.click();
				} else {
					emptyInbox.click();
					switchToDefaultContent();
				}
			}
			Reporter.log("Successfully clicked next button.");
		} catch (NoSuchElementException e) {
			Assert.fail("Error: Delete Mail Action was Unsuccessful.");
		}

		return this;
	}

	/**
	 * enter email value
	 * 
	 * @param mail
	 */
	public ChangePasswordPage enterMail(String mail) {
		try {
			sendTextData(mailTextBox, mail);
			Reporter.log("Successfully entered email value");
		} catch (NoSuchElementException e) {
			Assert.fail("Email Value is invalid");
		}
		return this;
	}

	/**
	 * click on check inbox button
	 */
	public ChangePasswordPage clickCheckInboxButton() {
		try {
			if (getDriver() instanceof IOSDriver) {
				getDriver().findElement(By.cssSelector("input.sbut")).click();
			} else {
				checkInboxButton.click();
			}
			Reporter.log("Successfully clicked on check inbox button.");
		} catch (NoSuchElementException e) {
			Assert.fail("Check inbox button is not clickable.");
		}
		return this;
	}

	/**
	 * Get Forgot Password URL
	 * 
	 * @param myTmoData
	 * @return
	 */
	public String getYopMailURL(MyTmoData myTmoData) {
		String passwordUrl;
		getDriver().get(myTmoData.getUrl());
		enterMail(myTmoData.getEmailid());
		clickCheckInboxButton();
		switchToFrame(inboxFrame);
		clickMail();
		switchToDefaultContent();
		if (!(getDriver() instanceof AppiumDriver)) {
			switchToFrame(mailContextFrame);
		}
		String mailContent = mailContext.getText();
		passwordUrl = mailContent.substring(mailContent.indexOf("https"), mailContent.lastIndexOf("=="));
		if (!(getDriver() instanceof AppiumDriver)) {
			switchToDefaultContent();
		}
		return passwordUrl;
	}

	/**
	 * Click Mail
	 */
	public ChangePasswordPage clickMail() {
		try {
			for (WebElement webElement : inboxMail) {
				webElement.click();
				break;
			}
			Reporter.log("Successfully clicked Inbox Mail.");
		} catch (NoSuchElementException e) {
			Assert.fail("Inbox Mail is not clickable.");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the Change Password class instance.
	 */
	public ChangePasswordPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			getDriver().getPageSource().contains(pageText);
		} catch (Exception e) {
			Assert.fail("May not be correct page");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Change Password class instance.
	 */
	public ChangePasswordPage verifyPageUrl() {
		try {
			checkPageIsReady();
			getDriver().getCurrentUrl().contains(pageUrl);
		} catch (Exception e) {
			Assert.fail("May not be correct page");
		}
		return this;
	}

}
