package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSAALEligibilityCheckApiV1 extends EOSCommonLib {

	/***
	 * Summary: Check the AAL Eligibilty for the given BAN/MSISDN
	 * Headers:Authorization, -transactionId, -transactionType, -applicationId,
	 * -channelId,(This indicates whether the original request came from Mobile /
	 * web / retail / mw etc) -correlationId, -clientId,Unique identifier for the
	 * client (could be e-servicesUI ,MWSAPConsumer etc ) -transactionBusinessKey,
	 * -transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU
	 * , inventoryfeed etc) -phoneNumber,required=true -Content-Type -usn
	 * -phoneNumber
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getResponseeligibilityCheckAAL(String alist[]) throws Exception {

		Response response = null;

		if (alist != null) {
			JSONObject rootval = new JSONObject();

			rootval.put("msisdn", alist[0]);
			rootval.put("ban", alist[3]);
			rootval.put("userRole", alist[4].trim());

			RestService restService = new RestService(rootval.toString(), eep.get(environment));

			restService.addHeader("Authorization", alist[1]);
			restService.addHeader("transactionType", "AAL");
			restService.addHeader("applicationId", "MYTMO");
			restService.addHeader("cache-control", "no-cache");
			restService.addHeader("channelId", "WEB");
			restService.addHeader("correlationId", "xasdf1234asa4444");
			restService.addHeader("X-Auth-Originator", alist[2]);
			restService.addHeader("transactionBusinessKey", "BAN");
			restService.addHeader("transactionBusinessKeyType", alist[3]);
			restService.addHeader("phoneNumber", alist[0]);
			restService.addHeader("Content-Type", "application/json");
			restService.addHeader("interactionid", "xasdf1234asa4444");
			restService.addHeader("usn", "testUSN");
			restService.addHeader("accept", "application/json");
			restService.addHeader("transactionid", "xasdf1234asa4444");

			response = restService.callService("aal/v1/eligibility", RestCallType.POST);

		}

		return response;
	}

	public String isAALEligible(Response response) {
		String isAALEligible = "false";

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("errorCode") != null
					&& "100".equalsIgnoreCase(jsonPathEvaluator.get("errorCode"))) {
				isAALEligible = "true";
			}
		}

		return isAALEligible;
	}
	
	public Map<String, String> aalMsisdnStatusType(Response response) {
		String pendingRatePlanSoc = "false";
		String eligibleLinesMbbLinesZero = "false";
		String nonMatchingRatePlanSoc = "false";
		String maxLimitExceeded = "false";
		String voiceLinesMaxedout = "false";
		Map<String, String> aalMsisdnStatusType = new HashMap<String, String>();
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("errorCode") != null) {
				if ("10013".equalsIgnoreCase(jsonPathEvaluator.get("errorCode"))) {
					pendingRatePlanSoc = "true";
				}
				if ("10009".equalsIgnoreCase(jsonPathEvaluator.get("errorCode"))) {
					eligibleLinesMbbLinesZero = "true";
				}
				if ("10010".equalsIgnoreCase(jsonPathEvaluator.get("errorCode"))) {
					nonMatchingRatePlanSoc = "true";
				}
				if ("10011".equalsIgnoreCase(jsonPathEvaluator.get("errorCode"))) {
					maxLimitExceeded = "true";
				}
				if ("10012".equalsIgnoreCase(jsonPathEvaluator.get("errorCode"))) {
					voiceLinesMaxedout = "true";
				}
			}
		}
		aalMsisdnStatusType.put("pendingRatePlanSoc", pendingRatePlanSoc);
		aalMsisdnStatusType.put("eligibleLinesMbbLinesZero", eligibleLinesMbbLinesZero);
		aalMsisdnStatusType.put("nonMatchingRatePlanSoc", nonMatchingRatePlanSoc);
		aalMsisdnStatusType.put("maxLimitExceeded", maxLimitExceeded);
		aalMsisdnStatusType.put("voiceLinesMaxedout", voiceLinesMaxedout);

		return aalMsisdnStatusType;
	}

	public Response eligibilityCheckAAL(String[] getjwt, String updatedRequest) {
		// TODO Auto-generated method stub
		return null;
	}

}
