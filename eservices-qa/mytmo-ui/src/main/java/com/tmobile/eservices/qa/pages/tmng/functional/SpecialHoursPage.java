/**
 * 
 */
package com.tmobile.eservices.qa.pages.tmng.functional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;


import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author ksrivani
 *
 */
public class SpecialHoursPage extends TmngCommonPage {

	public SpecialHoursPage(WebDriver webDriver) {
		super(webDriver);
	}	
	
	@FindBy(xpath = "//div[contains(@class,'ui-dialog-content')]/div/p[contains(text(),'A special hours event on')]")
	private WebElement popUpMessage;

	@FindBy(xpath = "(//span[contains(text(),'Reason')]/../following-sibling::*/span)[1]")
	private WebElement reasonValue;
	
	@FindBy(xpath = "((//span[contains(text(),'Special hours')])[2]/../following-sibling::*/span)[1]")
	private WebElement startAndEndDate;
	
	@FindBy(xpath = "((//span[contains(text(),'Special hours')])[2]/../following-sibling::*/span)[2]")
	private WebElement openingTimeInConfirmationPage;
	
	@FindBy(xpath = "((//span[contains(text(),'Special hours')])[2]/../following-sibling::*/span)[3]")
	private WebElement closingTimeInConfirmationPage;
	
	@FindBy(xpath = "(//span[contains(text(),'Description')]/../following-sibling::*/span)[1]")
	private WebElement descriptionValue;
	
	@FindBy(xpath = "//span[contains(text(),'Special hours saved')]")
	private WebElement specialHoursSavedMessage;
	
	@FindBy(xpath = "(//span[contains(text(),'Store(s)')]/../following-sibling::*/span)[1]")
	private WebElement storesSelectValues;
	
	@FindBy(xpath="//span[@class='ng-arrow']")
	private WebElement reasonDropDown;
	
	@FindBy(xpath="//div[contains(text(),'Select Reason')]/../div/input[@type='text']")
	private WebElement selectReason;
	
	@FindBy(css="div[class*='ng-option ng-star-inserted'] span")
	private List<WebElement> reasonOptions;
	
	@FindBy(xpath="//label[contains(text(),'Description')]/../input[@type='text']")
	private WebElement description;
	
	@FindBy(xpath="//div[contains(text(),'Opening early or late')]/../div/input[@role='combobox']")
	private WebElement openingEarlyDropdown;
	
	@FindBy(xpath="//div[contains(text(),'Closing early or late')]/../div/input[@role='combobox']")
	private WebElement closingEarlyDropdown;
	
	@FindBy(xpath="//div[contains(text(),'Opening time')]/../div/input[@role='combobox']")
	private WebElement openingDropdown;
	
	@FindBy(xpath="//div[contains(text(),'Closing time')]/../div/input[@role='combobox']")
	private WebElement closingDropdown;
	
	@FindBy(xpath="//span[contains(text(),'Closing time cannot be before opening time')]")
	private WebElement errorMssageForOpeningclosingtime;
	
	@FindBy(css="div[class*='ui-datepicker ui-widget']")
	private WebElement calendar;
	
	@FindBy(css="p[aria-label*='Closed']")
	private WebElement ClosedAllDayRadio;
	
	@FindBy(css="p[aria-label*='Exact time']")
	private WebElement ExactTimesRadio;
	
	@FindBy(css="p[aria-label*='Early']")
	private WebElement EarlyRadio;
	
	@FindBy(xpath="//p[contains(text(),'Store hours')]")
	private WebElement storeHours;
	
	@FindBy(css="span[class*='ui-button-icon']")
	private WebElement calendarLogo;
	
	@FindBy(css = "span[aria-label='Special hours']")
	private WebElement specialHoursTitle;

	@FindBy(xpath = "//span[text()='Log in']")
	private WebElement logIn;

	@FindBy(xpath = "//span[text()='You must have permissions to create special store hours. ']")
	private WebElement permissionsMessage;

	@FindBy(xpath = "//a[text()='For access, please contact:']")
	private WebElement accessContactMessage;

	@FindBy(xpath = "//a[text()='SpecialStoreHoursAdmin@t-mobile.com']")
	private WebElement contactLink;

	@FindBy(css = "input[type='email']")
	private WebElement tMobileEmail;
	
	@FindBy(css = "input[value='Next']")
	private WebElement nextButtonInLoginPage;

	@FindBy(css = "input[type='password']")
	private WebElement ntPassword;

	@FindBy(id = "submitButton")
	private WebElement signInButton;

	@FindBy(xpath = "//button[contains(text(),'Cancel')]")
	private WebElement cancelButton;

	@FindBy(css = "span[aria-label='Create special hours']")
	private WebElement createSpecialHours;

	@FindBy(css = "p[aria-label='Press enter to select Retail']")
	private WebElement retailLabel;

	@FindBy(css = "p[aria-label='Press enter to select TPR']")
	private WebElement tPRLabel;

	@FindBy(css = "input[aria-label='retail']")
	private WebElement retailRadioButton;

	@FindBy(css = "input[aria-label='TPR']")
	private WebElement tPRRadioButton;

	@FindBy(css = "span[aria-label*='search for stores']")
	private WebElement selectSearchForStoreText;

	@FindBy(css = "p[aria-label*='Press enter to select City, state, or ZIP']")
	private WebElement cityStateZipLabel;

	@FindBy(css = "p[aria-label*='Press enter to select SAP ID(s)']")
	private WebElement sapIdLabel;

	@FindBy(css = "input[value='address']")
	private WebElement cityStateZipRadioButton;

	@FindBy(css = "input[aria-label='City, state, or ZIP']")
	private WebElement cityStateZipTextBox;

	@FindBy(css = "label[for='inputValue']")
	private WebElement cityStateZipTextBoxMessage;

	@FindBy(css = "input[value='SAP']")
	private WebElement sapIdRadioButton;

	@FindBy(css = "input[aria-label='SAP ID(s) (separated by comma)']")
	private WebElement sapIdTextbox;

	@FindBy(css = "label[for='inputValue']")
	private WebElement sapIdTextBoxMessage;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	private WebElement continueButton;

	@FindBy(xpath = "//span[text()='Cancel']")
	private WebElement cancelButtonForSpecialHours;

	private final String pageUrl = "/special-hours";

	@FindBy(xpath = "//span[contains(text(),'Please enter City, State or ZIP.')]")
	private WebElement errorMessageForBlankCityStateZipcode;

	@FindBy(xpath = "//span[contains(text(),'Please enter in the format of City, State or ZIP Code.')]")
	private WebElement errorMessageForInvalidCityStateZipcode;

	@FindBy(xpath = "//span[contains(text(),'Please enter SAP ID(s).')]")
	private WebElement errorMessageForBlankSapID;

	@FindBy(xpath = "//span[contains(text(),'Please enter the SAP ID(s), separated by commas.')]")
	private WebElement errorMessageForInvalidSapID;

	@FindBy(css = "span[aria-label='Selected stores']")
	private WebElement selectedStores;

	@FindBy(css = "span[aria-label='Results']")
	private WebElement resultsHeader;

	@FindBy(css = "input[aria-label='Checkbox to select all']")
	private WebElement allCheckBox;

	@FindBy(css = "input[role='Checkbox']")
	private List<WebElement> storeCheckbox;

	@FindBy(xpath = "//span[contains(text(),'Select a reason and times')]")
	private WebElement reasonAndTimeSelectionPage;

	@FindBy(css = "#startDateFltngLbl input.ui-state-default")
	private WebElement startDate;

	@FindBy(css = "#endDateFltngLbl input.ui-state-default")
	private WebElement endDate;

	@FindBy(xpath = "//span[contains(text(),'Please select a start date that is with in one year from today.')]")
	private WebElement errorMessageForStartDate;

	@FindBy(xpath = "//span[contains(text(),'Please select an end date that is within 31 days from the start date.')]")
	private WebElement errorMessageForEndDate;
	
	@FindBy(xpath = "//div[contains(@class,'page-list')]/div/a")
	private List<WebElement> pagination;
	
	@FindBy(xpath = "//p[contains(@class,'page-list first-page')]")
	private WebElement firstPageNumberDisable;
	
	@FindBy(xpath = "(//a[contains(@class,'star')])[5]")
	private WebElement lastPageNumberEnable;
	
	@FindBy(xpath = "//i[contains(@class,'angle-left disabled')]")
	private WebElement lessThanCaretLinkDisable;
	
	@FindBy(xpath = "//i[contains(@class,'angle-right')]")
	private WebElement greaterThanCaretLinkEnable;
	
	@FindBy(css = "span[aria-label='Special hours saved.']")
	private WebElement specialHoursSaved;

	@FindBy(css = "span[aria-label='Saved special hours']")
	private WebElement savedSpecialHours;
	
	@FindBy(xpath = "//div/div/h6/span")
	private List<WebElement> reasonsInSavedSpecialHours;
	
	@FindBy(xpath = "//div/div/h6/span/../following-sibling::p/span")
	private List<WebElement> storeHoursInSavedSpecialHours;
	
	@FindBy(css = "i[class*='angle-down']")
	private List<WebElement> reasonCollapsedAccordion;
	
	@FindBy(css = "i[class*='angle-up']")
	private List<WebElement> reasonExpandedAccordion;
	
	@FindBy(css = "span[aria-label='Special hours']")
	private List<WebElement> specialHoursLabelForEventDate;
	
	@FindBy(css = "span[aria-label='Store(s) selected']")
	private List<WebElement> storesSelectedLabelForEventDate;
	
	@FindBy(xpath = "//span[@aria-label='Store(s) selected']/../following-sibling::p[1]/span")
	private List<WebElement> sapIdInSavedSpecialHours;
	
	@FindBy(css = "span[aria-label='Reason']")
	private List<WebElement> reasonHeaderInSavedSpecialHours;
	
	@FindBy(xpath = "//span[@aria-label='Reason']/../following-sibling::p[1]/span")
	private List<WebElement> reasonValueInSavedSpecialHours;

	@FindBy(css = "span[aria-label='Description']")
	private List<WebElement> descriptionHeaderInSavedSpecialHours;
	
	@FindBy(xpath = "//span[@aria-label='Description']/../following-sibling::p[1]/span")
	private List<WebElement> descriptionValueInSavedSpecialHours;

	@FindBy(css = "span[aria-label='Special hours']")
	private List<WebElement> specialHoursHeaderInSavedSpecialHours;
	
	@FindBy(xpath = "//span[@aria-label='Special hours']/../following-sibling::p[1]/span")
	private List<WebElement> specialHoursEventDate;
	
	@FindBy(xpath = "//span[@aria-label='Special hours']/../following-sibling::p[2]/span")
	private List<WebElement> storeHoursInSavedSpecialHoursUnderEvent;
	
	@FindBy(css = "p[class='hours-txt']")
	private WebElement createSpecialHoursInSavedSpecialHours;
	
	@FindBy(css = "i[class*='plus']")
	private WebElement createSpecialHoursIcon;
	
	@FindBy(xpath = "//a[contains(.,'Log out')]")
	private WebElement logOut;
	
	@FindBy(css = "span[class*='times']")
	private WebElement removeIcon;
	
	@FindBy(xpath = "//h3[contains(.,'Confirmation')]")
	private WebElement removeModal;
	
	@FindBy(xpath = "//p[contains(.,'Are you sure that you want to delete this event?')]")
	private WebElement removeModalText;
	
	@FindBy(id = "btnCancel")
	private WebElement cancelInRemoveModal;
	
	@FindBy(id = "btnConfirm")
	private WebElement confirmInRemoveModal;
	
	@FindBy(css = "span[title='Clear all'] span[class*='clear']")
	private WebElement reasonClear;
	
	@FindBy(css = "a[aria-label='Saved events']")
	private WebElement savedEventsLink;
	
	@FindBy(css = "i[class*='angle-right']")
	private WebElement savedEventsCaret;
	
	private final String savedEventsPageUrl = "/saved-events";
	
	private final String loginPageUrl = "/login";
	
	@FindBy(css = "button[aria-label='Logout']")
	private WebElement logOutInSavedEvents;
	
	@FindBy(xpath = "//div[@class='wrap-content'][contains(.,'Which account do you want to sign out')]")
	private WebElement whichAccountToSignOut;

	@FindBy(xpath = "//small[contains(.,'Signed in')]/../preceding-sibling::div/small[contains(.,'@T-Mobile.com')]")
	private WebElement logInEmail;
	
	@FindBy(css = "span[aria-label='Confirmation']")
	private WebElement confirmationPage;
	
	@FindBy(xpath = "//p[contains(.,' Please contact SpecialStoreHoursAdmin@t-mobile.com for access.')]")
	private WebElement errorMessageForInvalidUser;
	
	@FindBy(xpath = "//button[contains(.,'Close')]")
	private WebElement closeButtonInModalPopUpForInvalidUser;
	
	@FindBy(css = "input[value*='Yes']")
	private WebElement confirmationYesBtn;
	
	@FindBy(css = "input[value='hierarchy']")
	private WebElement storeHierarchyRadioButton;
	
	@FindBy(css = ".error-text")
	private WebElement warningMessageforNonStoreSelection;
	
	@FindBy(css = "p-dropdown[name='storeArea']")
	private WebElement areaDropDownSelector;
	
	@FindBy(css = "p-dropdown[name='storeArea'] .fa-caret-down")
	private WebElement areaDropDownSelectorUpArrow;
	
	@FindBy(xpath = "//span[text()='Central']")
	private WebElement centralAreaMenu;
	
	@FindBy(css = "p-dropdown[name='storeMarket']")
	private WebElement marketDropDownSelector;
	
	@FindBy(css = "p-dropdown[name='storeMarket'] .fa-caret-down")
	private WebElement marketDropDownSelectorUpArrow;
	
	@FindBy(xpath = "//span[text()='Central Markets']")
	private WebElement centralMarketsAreaMenu;
	
	@FindBy(css = "p-dropdown[name='storeDistrict']")
	private WebElement districtDropDownSelector;
	
	@FindBy(css = "p-dropdown[name='storeDistrict'] .fa-caret-down")
	private WebElement districtDropDownSelectorUpArrow;
	
	@FindBy(xpath = "//span[text()='Michigan/Indiana']")
	private WebElement michiganDistrictMenu;
	
	@FindBy(css = "p-dropdown[name='storeTerritory']")
	private WebElement territoryDropDownSelector;
	
	@FindBy(css = "p-dropdown[name='storeTerritory'] .fa-caret-down")
	private WebElement territoryDropDownSelectorUpArrow;
	
	@FindBy(xpath = "//span[text()='West Michigan']")
	private WebElement westMichiganTeritoryMenu;
	
	@FindBy(css = "span i")
	private WebElement storeSelectorResults;
	

	/***
	 * verify Special Hours page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySpecialHoursPage() {
		try {
			waitFor(ExpectedConditions.urlContains(pageUrl),60);
			Reporter.log("Special hours page is displayed");
			return this;
		} catch (Exception e) {
			Assert.fail("Special hours page is not displayed");
		}
		return this;
	}

	/***
	 * verify Special Hours page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySpecialHoursTitle() {
		try {
			Assert.assertTrue(isElementDisplayed(specialHoursTitle), "Special hours title is not displayed");
			Reporter.log("Special hours title is displayed");
		} catch (Exception e) {
			Assert.fail("Special hours title is not displayed");
		}
		return this;
	}

	/***
	 * verify permissions text
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyPermissionsText() {
		try {
			Assert.assertEquals(permissionsMessage.getText(),
					"You must have permissions to create special store hours. ");
			Reporter.log("Message informing about permissions is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the message informing about permissions");
		}
		return this;
	}

	/***
	 * verify Please contact text
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyPleaseContactText() {
		try {
			Assert.assertEquals(accessContactMessage.getText(), "For access, please contact:");
			Reporter.log("Please contact message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Please contact message");
		}
		return this;
	}

	/***
	 * verify Please contact text
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyAccessContactLink() {
		try {
			Assert.assertTrue(contactLink.isDisplayed());
			Reporter.log("Verified successfully the contact link");
		} catch (Exception e) {
			Assert.fail("Failed to verify Contact link");
		}
		return this;
	}

	/***
	 * inputs value in the Email and Password text box
	 * 
	 * @param searchValue
	 * @return
	 */
	public SpecialHoursPage signInWithTMobileEmailAndPassword(String email, String ntpassword) {
		try {
			checkPageIsReady();
			 waitFor(ExpectedConditions.visibilityOf(tMobileEmail));
		    sendTextData(tMobileEmail, email);
			Reporter.log("Entering email details into the email text box is success");
			 waitFor(ExpectedConditions.visibilityOf(nextButtonInLoginPage));
			 nextButtonInLoginPage.click();
		    Reporter.log("Click on next button is success");
		    Thread.sleep(10000);
		    waitFor(ExpectedConditions.visibilityOf(ntPassword));
		    ntPassword.clear();
			sendTextData(ntPassword, ntpassword);
			Reporter.log("Entering ntpassword into the password text box is success");
			 waitFor(ExpectedConditions.visibilityOf(signInButton));
			elementClick(signInButton);
			Thread.sleep(10000);
			if(isElementDisplayed(confirmationYesBtn))
			{
				confirmationYesBtn.click();
			}
			Reporter.log("Click on sign in button is success");
		} catch (Exception e) {
			Assert.fail("failed to enter email and password in the text box");
		}
		return this;
	}

/***
	 * click on Sign in button
	 * 
	 * @return
	 */
	public SpecialHoursPage clickSignInButton() {
		try {
			checkPageIsReady();
			elementClick(signInButton);
			Reporter.log("Clicked on Sign In button");
		} catch (Exception e) {
			Assert.fail("Unable to click on Sign In button");
		}
		return this;
	}

	/***
	 * verify create special hours label
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyCreateSpecialHoursTitle() {
		try {
			checkPageIsReady();
			Assert.assertEquals(createSpecialHours.getText(), "Create special hours");
			Reporter.log("Create special hours title is displayed");
		} catch (Exception e) {
			Assert.fail("Create special hours title is not displayed");
		}
		return this;
	}

	/***
	 * verify Retail and TPR radio button labels
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyRetailTprRadioButtonLabels() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(retailLabel), "Failed to display Retail radio button label");
			Assert.assertTrue(isElementDisplayed(tPRLabel), "Failed to display TPR radio button label");
			Reporter.log("Verified successfully the radio button labels under Create special hours");
		} catch (Exception e) {
			Assert.fail("Failed to verify the radio button labels under Create special hours");
		}
		return this;
	}

	/***
	 * select Retail button
	 * 
	 * @return
	 */
	public SpecialHoursPage selectRetailButton() {
		try {
			checkPageIsReady();
			elementClick(retailRadioButton);
			Reporter.log("Able to Select Retail radio button");
		} catch (Exception e) {
			Assert.fail("Unable to select Retail radio button");
		}
		return this;
	}

	/***
	 * select TPR button
	 * 
	 * @return
	 */
	public SpecialHoursPage selectTPRButton() {
		try {
			checkPageIsReady();
			elementClick(tPRRadioButton);
			Reporter.log("Unable to Select TPR radio button");
		} catch (Exception e) {
			Assert.fail("Unable to select TPR radio button");
		}
		return this;
	}

	/***
	 * verify Search for stores text
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySearchForStoresText() {
		try {
			checkPageIsReady();
			Assert.assertEquals(selectSearchForStoreText.getText(), "Select how you'd like to search for stores");
			Reporter.log("'Select how you'd like to search for stores' text is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify the text 'Select how you'd like to search for stores'");
		}
		return this;
	}

	/***
	 * select City,State,Zip button
	 * 
	 * @return
	 */
	public SpecialHoursPage selectCityStateZipButton() {
		try {
			checkPageIsReady();
			elementClick(cityStateZipRadioButton);
			Reporter.log("Selected City,State,Zip radio button");
		} catch (Exception e) {
			Assert.fail("Unable to select City,State,Zip radio button");
		}
		return this;
	}

	/***
	 * verify CityStateZip text box
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyCityStateZipTextBox() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(cityStateZipTextBox), "City, state, Zip text box is not displayed");
			Reporter.log("City,state,Zip text box is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to display City,state,Zip text box");
		}
		return this;
	}

	/***
	 * verify text in SAP ID text box
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyTextInCityStateZipTextBox() {
		try {
			checkPageIsReady();
			elementClick(cityStateZipTextBox);
			Assert.assertEquals(cityStateZipTextBoxMessage.getText(), "(ex. Bellevue, WA or 98008)");
			Reporter.log("Text shown in City,State,Zip text box is verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify text shown in City,State,Zip text box");
		}
		return this;
	}

	/***
	 * select SAP ID button
	 * 
	 * @return
	 */
	public SpecialHoursPage selectSapIdButton() {
		try {
			checkPageIsReady();
			elementClick(sapIdRadioButton);
			Reporter.log("Selected SAP ID radio button");
		} catch (Exception e) {
			Assert.fail("Unable to select SAP ID radio button");
		}
		return this;
	}

	/***
	 * verify SAP ID text boox
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySapIdTextBox() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(sapIdTextbox), "SAP ID text box is not displayed");
			Reporter.log("SAP ID text box is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to display SAP ID text box");
		}
		return this;
	}

	/***
	 * verify text in SAP ID text boox
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyTextInSapIdTextBox() {
		try {
			checkPageIsReady();
			elementClick(sapIdTextbox);
			Assert.assertEquals(sapIdTextBoxMessage, "SAP ID(s) (separated by comma)");
			Reporter.log("Text shown in SAP ID text box is verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify text shown in SAP ID text box");
		}
		return this;
	}

	/***
	 * enter city,state in the search text box
	 * 
	 * @param searchValue
	 * @return
	 */
	public SpecialHoursPage setCityStateIntoTheSearchTextBox(String city, String state) {
		try {
			checkPageIsReady();
			sendTextData(cityStateZipTextBox, city+","+state);
			Reporter.log("Entered city, state as: " + city +"," +state+ " in the search text box");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + city +"," +state+" into the search text box");
		}
		return this;
	}
	
	/***
	 * enter zip in the search text box
	 * 
	 * @param searchValue
	 * @return
	 */
	public SpecialHoursPage setZipcodeIntoTheSearchTextBox(String zip) {
		try {
			checkPageIsReady();
			sendTextData(cityStateZipTextBox, zip);
			Reporter.log("Entered Zipcode: " + zip + " in the search text box");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + zip +" into the search text box");
		}
		return this;
	}

	/***
	 * validate City
	 * 
	 * @return
	 */
	public SpecialHoursPage validateCity(String searchValue) {
		try {
			checkPageIsReady();
			Assert.assertTrue(searchValue.matches("[a-zA-Z]+"));
			Reporter.log("Validated successfully City entering has only Alphabets");
		} catch (Exception e) {
			Assert.fail("Unable to validate City");
		}
		return this;
	}

	/***
	 * validate State
	 * 
	 * @return
	 */
	public SpecialHoursPage validateState(String searchValue) {
		try {
			checkPageIsReady();
			Assert.assertTrue(searchValue.matches("[a-zA-Z]+") && searchValue.length() == 2);
			Reporter.log("Validated successfully State entering has two characters");
		} catch (Exception e) {
			Assert.fail("Unable to validate State");
		}
		return this;
	}

	/***
	 * validate Zipcode
	 * 
	 * @return
	 */
	public SpecialHoursPage validateZipcode(String searchValue) {
		try {
			checkPageIsReady();
			Assert.assertTrue(searchValue.matches("[0-9]") && searchValue.length() == 5);
			Reporter.log("Validated successfully Zipcode entering is Numeric which has 5 digits");
		} catch (Exception e) {
			Assert.fail("Unable to validate Zipcode");
		}
		return this;
	}

	/***
	 * enter SAP ID in the search text box
	 * 
	 * @param searchValue
	 * @return
	 */
	public SpecialHoursPage setSapIdIntoTheSearchTextBox(String searchValue) {
		try {
			checkPageIsReady();
			sendTextData(sapIdTextbox, searchValue);
			Reporter.log("Entered value " + searchValue + " in the search text box");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + searchValue + " into the search text box");
		}
		return this;
	}


	/***
	 * Click on Continue button
	 * 
	 * @return
	 */
	public SpecialHoursPage clickOnContinueButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(continueButton));
			continueButton.click();
		} catch (Exception e) {
			Assert.fail("Click on continue button failed");
		}
		return this;
	}
	
	/**
	 * 
	 * verify PopUP for conflicting date
	 * @return
	 */
	public SpecialHoursPage verifyPopUpForConflictingEvent() {
		try {
			checkPageIsReady();
			Assert.assertTrue(popUpMessage.isDisplayed());
			Reporter.log("Saved Special hours pop up message is  displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate pop up message");
		}
		return this;
	}
	
	/**
	 * Click on Reason drop down
	 * 
	 * @return
	 */
	public SpecialHoursPage ClickOnReasonDropDown() {
		try {
			checkPageIsReady();
			elementClick(reasonDropDown);
			Reporter.log("Clicked on Reason drop down");
		} catch (Exception e) {
			Assert.fail("Unable to click on Reason drop down");
		}
		return this;
	}
	
	/***
	 * Verify Reason options for Emergency user
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyReasonOptionsForEmergencyUser() {
		try {
			checkPageIsReady();
			ArrayList<String> expectedResult = new ArrayList<String>();
			expectedResult.add("Fire");
			expectedResult.add("Flood");
			expectedResult.add("Hurricane");
			expectedResult.add("Snow");
			expectedResult.add("Smoke");
			expectedResult.add("Storm"); 
			expectedResult.add("Tornado"); 
			expectedResult.add("Earthquake"); 
			expectedResult.add("Other weather"); 
			expectedResult.add("Other emergency"); 
			ArrayList<String> actualResult = new ArrayList<String>();
			for(int i=0; i<reasonOptions.size(); i++) {
				actualResult.add(reasonOptions.get(i).getAttribute("innerHTML"));				
			}
			for(int i=0;i<expectedResult.size();i++) {
		       if(actualResult.get(i).contains(expectedResult.get(i))) {
			    }
		       else {
			        Reporter.log("Unable to get the Reason options for Emergency User");
			    }
			}
			Reporter.log("User should be able to see the list of emergency reasons consisting of 'Fire, Flood, Hurricane, Snow ,Smoke ,Storm ,Other weather ,Other emergency'");		
		} catch (Exception e) {
			Assert.fail("Unable to verify Reason options for Emergency User");
		}																								
		return this;
	}
	
	/***
	 * Verify Reason options for Holiday NPI user
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyReasonOptionsForHolidayNPIUser() {
		try {
			checkPageIsReady();
			ArrayList<String> expectedResult = new ArrayList<String>();
			expectedResult.add("Holiday");
			expectedResult.add("New Product Introduction");
			ArrayList<String> actualResult = new ArrayList<String>();
			for(int i=0; i<reasonOptions.size(); i++) {
				actualResult.add(reasonOptions.get(i).getAttribute("innerHTML"));				
			}
			for(int i=0;i<expectedResult.size();i++) {
		       if(actualResult.get(i).contains(expectedResult.get(i))) {
			    }
		       else {
			        Reporter.log("Unable to get the Reason options for Holiday NPI User");
			    }
			}
			Reporter.log("User should be able to see the list of reasons consisting of 'Holiday, New Product Introduction'");		
		} catch (Exception e) {
			Assert.fail("Unable to verify Reason options for Holiday NPI User");
		}																								
		return this;
	}
	
	/***
	 * Verify Reason options for Super user
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyReasonOptionsForSuperUser() {
		try {
			checkPageIsReady();
			ArrayList<String> expectedResult = new ArrayList<String>();
			expectedResult.add("Fire");
			expectedResult.add("Flood");
			expectedResult.add("Hurricane");
			expectedResult.add("Snow");
			expectedResult.add("Smoke");
			expectedResult.add("Storm"); 
			expectedResult.add("Tornado"); 
			expectedResult.add("Earthquake"); 
			expectedResult.add("Other weather"); 
			expectedResult.add("Other emergency"); 
			expectedResult.add("Holiday");
			expectedResult.add("New Product Introduction");
			ArrayList<String> actualResult = new ArrayList<String>();
			for(int i=0; i<reasonOptions.size(); i++) {
				actualResult.add(reasonOptions.get(i).getAttribute("innerHTML"));				
			}
			for(int i=0;i<expectedResult.size();i++) {
		       if(actualResult.get(i).contains(expectedResult.get(i))) {
			    }
		       else {
			        Reporter.log("Unable to get the Reason options for Super User");
			    }
			}
			Reporter.log("User should be able to see the list of reasons consisting of 'Fire, Flood, Hurricane, Snow ,Smoke ,Storm ,Other weather ,Other emergency, Holiday, New Product Introduction'");		
		} catch (Exception e) {
			Assert.fail("Unable to verify Reason options for Super User");
		}																								
		return this;
	}
	
	/**
	 * Enters the reason in reason field
	 * @param selectReson
	 * @return
	 */
	public SpecialHoursPage setReason(String selectReson) {
		try {
			checkPageIsReady();
			sendTextData(selectReason, selectReson);
			Reporter.log("Selected Reason as: " + selectReson + " from the Reason drop down");
			reasonOptions.get(0).click();
		} catch (Exception e) {
			Assert.fail("failed to select Reason: " + selectReson + " from the Reason drop down");
		}
		return this;
	}
	
	/***
	 * Click on Cancel button
	 * 
	 * @return
	 */
	public SpecialHoursPage clickOnCancelButton() {	
		try {
			checkPageIsReady();
			elementClick(cancelButton);
			Reporter.log("Clicked on Cancel button");
		} catch (Exception e) {
			Assert.fail("Unable to click on Cancel button");
		}
		return this;
	}

	/***
	 * Verify Error Message For Blank City State Zipcode
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyErrorMessageForBlankCityStateZipcode() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(errorMessageForBlankCityStateZipcode));
			Reporter.log("Error message 'Please enter City, State or ZIP.' is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to verify error message when there is no data entered in search text box");
		}
		return this;
	}

	/***
	 * Verify Error Message For Invalid City State Zipcode
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyErrorMessageForInvalidCityStateZipcode() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(errorMessageForInvalidCityStateZipcode));
			Reporter.log(
					"Error message 'Please enter in the format of City, State or ZIP Code.' is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to verify error message when entered invalid data in search text box");
		}
		return this;
	}

	/***
	 * Verify Error Message For Blank SAP ID
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyErrorMessageForBlankSapID() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(errorMessageForBlankSapID));
			Reporter.log("Error message 'Please enter SAP ID(s).' is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to verify error message when there is no data entered in search text box");
		}
		return this;
	}

	/***
	 * Verify Error Message For Invalid SAP ID
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyErrorMessageForInvalidSapID() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(errorMessageForInvalidSapID));
			Reporter.log("Error message 'Please enter the SAP ID(s), separated by commas.' is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to verify error message when entered invalid data in search text box");
		}
		return this;
	}

	/***
	 * Verify Continue And Cancel Buttons Upon Page Loading
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyContinueAndCancelButtonsUponPageLoading() {
		try {
			checkPageIsReady();
			Assert.assertFalse(continueButton.isEnabled());
			Reporter.log("Continue button is disabled while page loading");
			Assert.assertFalse(cancelButton.isEnabled());
			Reporter.log("Cancel button is disabled while page loading");
		} catch (Exception e) {
			Assert.fail("Unable to verify the state of Continue and Cancel button while page loading");
		}
		return this;
	}

	/***
	 * Verify Selected stores header
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySelectedStoresPage() {
		try {
			checkPageIsReady();
			 waitFor(ExpectedConditions.visibilityOf(selectedStores),60);
			Assert.assertTrue(isElementDisplayed(selectedStores));
			Reporter.log("Selected Stores page is displayed with its header");
		} catch (Exception e) {
			Assert.fail("Unable to display Selected stores page");
		}
		return this;
	}

	/***
	 * Verify Results sub header
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyResultsSubheader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(resultsHeader));
			Reporter.log("Results Sub header is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display Results sub header");
		}
		return this;
	}

	/***
	 * Verify default state of All Checkbox
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyDefaultAllCheckboxChecked() {
		try {
			checkPageIsReady();
			String actualCheckedState = allCheckBox.getAttribute("value").toString();
			Assert.assertTrue(actualCheckedState.contains("true"));
			Reporter.log("All checkbox should be pre checked");
		} catch (Exception e) {
			Assert.fail("Unable to verify 'All' checkbox default state");
		}
		return this;
	}

	/*
	 * Verify Selecting 'All' checkbox, automatically selects all stores checkboxes
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyEveryStorePrechecked() {
		try {
			checkPageIsReady();
			if (allCheckBox.getAttribute("value").toString().contains("true")) {
				for (WebElement ele : storeCheckbox) {
					Assert.assertTrue(ele.getAttribute("value").toString().contains("true"));				
				}
				Reporter.log("Every store is prechecked, If not selecting 'All' checkbox, automatically selects all stores checkboxes");
			}
				else {
					elementClick(allCheckBox);
					for (WebElement ele : storeCheckbox) {
						Assert.assertTrue(ele.getAttribute("value").toString().contains("true"));
					}
					Reporter.log("Selecting 'All' checkbox, automatically selects all stores checkboxes");
				}
			} catch (Exception e) {
			Assert.fail("Selecting 'All' checkbox, is not selecting all stores checkboxes automatically");
		}
		return this;
	}

	/*
	 * Verify deselecting a store checkbox, automatically deselects All Checkbox
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyDeselectingAStore() {
		try {
			checkPageIsReady();
			if (storeCheckbox.get(0).getAttribute("value").toString().contains("true")) {
				elementClick(storeCheckbox.get(0));
				Assert.assertTrue(allCheckBox.getAttribute("value").toString().contains("false"));
				Reporter.log("Deselecting any of the store checkbox, automatically deselects 'All' checkbox");
			}
			else{
		Reporter.log("Deselecting any of the store checkbox, automatically deselects 'All' checkbox");
			}
		} catch (Exception e) {
			Assert.fail("Deselecting any of the store checkbox, is not deselecting 'All' checkbox automatically");
		}
		return this;
	}


	/*
	 * Verify deselecting a store checkbox, automatically deselects All Checkbox
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySelectingAllStores() {
		try {
			checkPageIsReady();
			for (WebElement ele : storeCheckbox) {
				if (ele.getAttribute("value").toString().contains("false")) {
					elementClick(ele);
				}
				else{
					Reporter.log("Selecting all stores checkboxes, automatically selects 'All' checkbox");
				}
			}
			Assert.assertTrue(allCheckBox.getAttribute("value").toString().contains("true"));
			Reporter.log("Selecting all the stores checkboxes, automatically selects 'All' checkbox");
	} catch (Exception e) {
			Assert.fail("Selecting all stores checkboxes, is not selecting 'All' checkbox automatically");
		}
		return this;
	}

	/*
	 * Verify Pagination is not displayed when less than or equal to 10 stores in the result
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyNoPaginationForLessThan10Stores() {
		try {
			checkPageIsReady();
			if (storeCheckbox.size() <= 10) {
				List<WebElement> paginationSize = getDriver().findElements(By.xpath("//div[contains(@class,'page-list')]/div/a"));
				Assert.assertEquals(paginationSize.size(),0);
				Reporter.log(
						"Pagination is not displayed when less than or equal to 10 stores in the result successfully");
			}
		} catch (Exception e) {
			Assert.fail(
					"Unable to verify the display of Pagination for less than or equal to 10 stores in the result successfully");
		}
		return this;
	}
	
		
	/*
	 * Verify Pagination and Stores displayed in a page for more than 10 stores
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyPaginationAndStoresDisplayInAPageForMoreThan10Stores() {
		try {
			checkPageIsReady();
			String count = resultsHeader.getText().substring(9, 11);
			int storesCount = Integer.parseInt(count);
				Reporter.log("Stores count displayed successfully");
				JavascriptExecutor js = (JavascriptExecutor) getDriver();
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			if (storesCount > 10) {
				Assert.assertTrue(!pagination.isEmpty());
				Reporter.log("Pagination exits, as number of stores are greater than 10");
				Assert.assertTrue(firstPageNumberDisable.isDisplayed());
				Reporter.log("Page number 1 is selected successfully, and thus is disabled");
				Assert.assertEquals(storeCheckbox.size(),10);
				Reporter.log("At a time only"+storeCheckbox.size()+" stores displayed per page successfully");	
			}
	} catch (Exception e) {
			Assert.fail("Unable to verify the display of stores in a page");
		}
		return this;
	}

	
	/*
	 * Verify Less than caret link, when first page is selected
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyLessThanCaretLinkDisabledWhenSelectedFirstPage() {
		try {
			checkPageIsReady();
			if (firstPageNumberDisable.isDisplayed())
			{
				Assert.assertTrue(lessThanCaretLinkDisable.isDisplayed());
				Reporter.log("Less than caret link is disabled when First page is selected");
			}
			} catch (Exception e) {
			Assert.fail("Unable to verify Less than caret link, when first page is selected");
		}
		return this;
	}
	
	/*
	 * Verify Greater than caret link, when first page is selected
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyGreaterThanCaretLinkIsEnabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(greaterThanCaretLinkEnable.isDisplayed());
			Reporter.log("Greater than caret link is enabled when First page is selected");
			} catch (Exception e) {
			Assert.fail("Unable to display Greater than Caret link");
		}
		return this;
	}
	
		
	/*
	 * Verify the display of Continue and Cancel CTA
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyContinueAndCancelCTA() {
		try {
			checkPageIsReady();
			Assert.assertTrue(continueButton.isDisplayed());
			Assert.assertTrue(cancelButton.isDisplayed());
			Reporter.log("Continue or Cancel CTA's displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display Continue and Cancel CTA");
		}
		return this;
	}

	/*
	 * Verify Continue button is enabled upon selecting a store
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyContinueButtonEnabledUponSelectingAStore() {
		int count = 0;
		try {
			checkPageIsReady();
			for (WebElement ele : storeCheckbox) {
				if (ele.getAttribute("value").toString().contains("on")) {
					count++;
				}
			}if (count > 0) {
				Assert.assertTrue(continueButton.isEnabled());
				Reporter.log("Continue Button enabled when only one Store selected");
			} else {
				elementClick(storeCheckbox.get(0));
				Reporter.log("Selected first store checkbox inorder to enable Continue Button");
			}
		} catch (Exception e) {
			Assert.fail("Continue Button is not enabled when any of the store is selected");
		}
		return this;
	}

	/*
	 * Verify the display of Reason And Time Selection Page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyReasonAndTimeSelectionPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(reasonAndTimeSelectionPage.isDisplayed());
			Reporter.log("'Reason and Time Selection' page is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to display 'Reason and Time Selection' page");
		}
		return this;
	}
	

	/***
	 * Verify Special Hours Start date field
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyStartDateField() {
		try {
			checkPageIsReady();
			Assert.assertTrue(startDate.isDisplayed());
			Reporter.log("'Special hours start date' text field is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display 'Special hours start date' text field");
		}
		return this; 
	}

	/***
	 * set Start date
	 * 
	 * @param searchValue
	 * @return
	 */
	public SpecialHoursPage setStartDate(String searchValue) {
		try {
			checkPageIsReady();
			sendTextData(startDate, searchValue);
			Reporter.log("Entered date: " + searchValue + " in the Start Date");
			startDate.sendKeys(Keys.TAB);
		} catch (Exception e) {
			Assert.fail("failed to enter date" + searchValue + " into the Start Date");
		}
		return this;
	}

	/***
	 * set End date
	 * 
	 * @param searchValue
	 * @return
	 */
	public SpecialHoursPage setEndDate(String searchValue) {
		try {
			checkPageIsReady();
			sendTextData(endDate, searchValue);
			Reporter.log("Entered date: " + searchValue + " in the End Date");
//			endDate.sendKeys(Keys.TAB);
		} catch (Exception e) {
			Assert.fail("failed to enter date" + searchValue + " into the End Date");
		}
		return this;
	}


	/***
	 * Verify Error Message If Start Date Exceeds One Year from Current date
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyErrorMessageIfStartDateExceedsOneYearFromCurrentDate(String startDate) {
		try {
			checkPageIsReady();
			Calendar date = Calendar.getInstance();
			date.setTime(new Date());
			SimpleDateFormat sdf = new SimpleDateFormat("MM/DD/YYYY");
			date.add(Calendar.YEAR, 1);
			String oneYearFromCurrentDate = sdf.format(date.getTime());
			Date date1 = sdf.parse(startDate);
			Date date2 = sdf.parse(oneYearFromCurrentDate);
			if (date1.after(date2)) {
				Assert.assertTrue(isElementDisplayed(errorMessageForStartDate));
				Reporter.log(
						"Error message 'Please select a start date that is within one year from today.' is displayed");
			}
		} catch (Exception e) {
			Assert.fail(
					"Unable to display error message when entered start data is greater than one year from current date");
		}
		return this;
	}

	/***
	 * Verify Error Message If End Date Exceeds 31 days from Start date
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyErrorMessageIfEndDateExceeds31DaysFromStartDate(String startDate) {
		try {
			checkPageIsReady();
			
			Calendar date = Calendar.getInstance();
			date.setTime(new Date());
			SimpleDateFormat sdf = new SimpleDateFormat("MM/DD/YYYY");
			date.add(Calendar.DATE, 31);
			String oneYearFromStartDate = sdf.format(date.getTime());
			Date date1 = sdf.parse(startDate);
			Date date2 = sdf.parse(oneYearFromStartDate);
			if (date1.after(date2)) {
				Assert.assertTrue(isElementDisplayed(errorMessageForEndDate));
				Reporter.log(
						"Error message 'Please select an end date that is within 31 days from the start date.' is displayed");
			}
		} catch (Exception e) {
			Assert.fail(
					"Unable to display error message when entered end data is greater than 31 days from start date");
		}
		return this;
	}
	
	/**
	 * 
	 * clicks on Closed All day radio button
	 * @return
	 */
	public SpecialHoursPage clickonClosedAllRadio() {
		try {
			checkPageIsReady();
			clickElement(ClosedAllDayRadio);
			Reporter.log("clicked on Closed All day radio button");
		} catch (Exception e) {
			Assert.fail("Unable to click on Closed All day radio button");
		}
		return this;
	}
	
	/**
	 * 
	 * clicks on select date field
	 * @return
	 */
	public SpecialHoursPage clickOnSelectDateField() {
		try {
			checkPageIsReady();
			clickElement(startDate);
			Reporter.log("Clicked on select date field");
		} catch (Exception e) {
			Assert.fail("Unable to validate select date field");
		}
		return this;
	}
	
	/**
	 * verifies if calendar is displayed or not
	 * @return
	 */
	public SpecialHoursPage verifyCalendarIsDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(calendar.isDisplayed());
			Reporter.log("Calendar is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate Calendar");
		}
		return this;
	}
	
	
	/**
	 * veridies closed all day radio is displayed
	 * @return
	 */
	public SpecialHoursPage validateClosedAllDayRadio() {
		try {
			checkPageIsReady();
			Assert.assertTrue(ClosedAllDayRadio.isDisplayed());
			Reporter.log("Closed all day radio button is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate cloased all day  radio button");
		}
		return this;
	}
	
//	/**
//	 * clicks nno end date radio
//	 * @return
//	 */
//	public SpecialHoursPage clickonNoEndDateRadio() {
//		try {
//			checkPageIsReady();
//			clickElement(noEndDateRadio);
//			Reporter.log("clicked on noEndDateRadio radio button");
//		} catch (Exception e) {
//			Assert.fail("Unable to validate no end date radio");
//		}
//		return this;
//	}
	
	/**
	 * It will enter data in opening dropdown
	 * @param openingTime
	 * @return
	 */
	public SpecialHoursPage setOpeningTimeinOpeningDropDown(String openingTime) {
		try {
			checkPageIsReady();
			openingDropdown.click();
			sendTextData(openingDropdown, openingTime);
			openingDropdown.sendKeys(Keys.ENTER);
			openingDropdown.sendKeys(Keys.TAB);
			Reporter.log("Entered value " +openingTime+ " in the opening dropdown ");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + openingTime+" in the opening dropdown ");
		}
		return this;
	}
	
	/**
	 * verifies store seelcted field values  are separated by ','
	 * @return
	 */
	public SpecialHoursPage verifyStoreSelctedFieldValue() {
		try {
			checkPageIsReady();
			String sapIds=storesSelectValues.getAttribute("aria-label");
			Assert.assertTrue(sapIds.contains(","));
			Reporter.log("sapIds are separated by ','");
		} catch (Exception e) {
			Assert.fail("Unable to validate sapIds");
		}
		return this;
	}
	
	/**
	 * Enters the description in description field
	 * @param descrption
	 * @return
	 */
	public SpecialHoursPage setDescription(String descrption) {
		try {
			checkPageIsReady();
			sendTextData(description, descrption);
			Reporter.log("Entered description: " + descrption + " in the description field");
		} catch (Exception e) {
			Assert.fail("Failed to enter description: " + descrption + " in the description");
		}
		return this;
	}
	
	/**
	 * verifies success message "special hours saved"
	 * @return
	 */
	public SpecialHoursPage verifySpecialHoursSavedMessage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(specialHoursSavedMessage),60);
			Assert.assertTrue(specialHoursSavedMessage.isDisplayed());
			Reporter.log("success message 'special hours saved' is verified");
		} catch (Exception e) {
			Assert.fail("Unable to validate success message");
		}
		return this;
	}
	
	/***
	 * validate reason value
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyReasonValue(String resonValue) {
		try {
			checkPageIsReady();
			Assert.assertEquals(reasonValue.getText(), resonValue);
			Reporter.log("Reason value is verified");
		} catch (Exception e) {
			Assert.fail("Unable to validate reason");
		}
		return this;
	}
	
	/***
	 * validate description Value
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyDescriptionValue(String descrptionValue) {
		try {
			checkPageIsReady();
			Assert.assertEquals(descriptionValue.getText(), descrptionValue);
			Reporter.log("descriptionValue is verified");
		} catch (Exception e) {
			Assert.fail("Unable to validate descriptionValue");
		}
		return this;
	}
	
	/***
	 * validate start and end date  Value in confirmation page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyStartDateAndEndDateInConfirmationPage(String startDate,String EndDate) {
		try {
			checkPageIsReady();
			String startAndEndDte[]=startAndEndDate.getText().toString().split("-");
			Assert.assertEquals(startAndEndDte[0].trim(),startDate);
			Assert.assertEquals(startAndEndDte[1].trim(),EndDate);
			Reporter.log("Verified start and end date successfully in confirmation page");
		} catch (Exception e) {
			Assert.fail("Unable to validate start and end date in confirmation page");
		}
		return this;
	}
	
	/***
	 * validate opening and closing time  in confirmation page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyOpeningAndClosingTimeInConfirmationPage(String openingTime,String closingTime) {
		try {
			checkPageIsReady();
			Assert.assertTrue(openingTimeInConfirmationPage.getText().toString().contains(openingTime));
			Assert.assertTrue(closingTimeInConfirmationPage.getText().toString().contains(closingTime));
			Reporter.log("verified opening and closing time in confirmation page succcessfully");
		} catch (Exception e) {
			Assert.fail("Unable to validate start and end date in confirmation page");
		}
		return this;
	}
	
	/**
	 * It will enter data in opening dropdown
	 * @param closingTime
	 * @return
	 */
	public SpecialHoursPage setClosingTimeinclosingDropDown(String closingTime) {
		try {
			checkPageIsReady();
			closingDropdown.click();
			sendTextData(closingDropdown, closingTime);
			closingDropdown.sendKeys(Keys.ENTER);
			closingDropdown.sendKeys(Keys.TAB);
			Reporter.log("Entered value " +closingTime+ " in the closing dropdown ");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + closingTime+" in the closing dropdown ");
		}
		return this;
	}
	
	/**
	 * verifies opening and closing  early dropdown is displayed
	 * @return
	 */
	public SpecialHoursPage verifyOpeningAndClosingEarlyDropDownIsDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(openingEarlyDropdown.isDisplayed());
			Assert.assertTrue(closingEarlyDropdown.isDisplayed());
			Reporter.log("opening and closing early dropdown is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate opening and closing early dropdown");
		}
		return this;
	}
	
	/**
	 * verifies opening and closing dropdown is displayed
	 * @return
	 */
	public SpecialHoursPage verifyOpeningAndClosingDropDownIsDisplayed() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(openingDropdown),2000);
			waitFor(ExpectedConditions.visibilityOf(closingDropdown),2000);
			Assert.assertTrue(openingDropdown.isDisplayed());
			Assert.assertTrue(closingDropdown.isDisplayed());
			Reporter.log("opening and closing  dropdown is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate opening and closing dropdown");
		}
		return this;
	}
	
	/**
	 * selects value from openig and closing early dropdown
	 * @return
	 */
	public SpecialHoursPage selectValueFromOpeningAndClosingEarlyDropDown() {
		try {
			checkPageIsReady();
			openingEarlyDropdown.click();
			openingEarlyDropdown.sendKeys(Keys.DOWN);
			openingEarlyDropdown.sendKeys(Keys.ENTER);
			openingEarlyDropdown.sendKeys(Keys.TAB);
			closingEarlyDropdown.click();
			closingEarlyDropdown.sendKeys(Keys.DOWN);
			closingEarlyDropdown.sendKeys(Keys.ENTER);
			openingEarlyDropdown.sendKeys(Keys.TAB);
			Reporter.log("successfully selected value from opening and closing drop down menu");
		} catch (Exception e) {
			Assert.fail("Unable to validate opening and closing earlydropdown");
		}
		return this;
	}
	
	/**
	 * 
	 * selects value from opening and closing dropdown
	 * @return
	 */
	public SpecialHoursPage selectValueFromOpeningAndClosingDropDown() {
		try {
			checkPageIsReady();
			openingDropdown.click();
			openingDropdown.sendKeys(Keys.DOWN);
			openingDropdown.sendKeys(Keys.ENTER);
			closingDropdown.click();
			closingDropdown.sendKeys(Keys.DOWN);
			closingDropdown.sendKeys(Keys.ENTER);
			Reporter.log("successfully selected value from opening and closing drop down menu");
		} catch (Exception e) {
			Assert.fail("Unable to validate opening and closing dropdown");
		}
		return this;
	}
	
	/**
	 * verifies if closing time is before opening time
	 * @param openingtime
	 * @param closingTime
	 * @return
	 */
	public SpecialHoursPage validateclosingTimeBeforeOpeningTime(String openingtime,String closingTime) {
		try {
			checkPageIsReady();
			openingDropdown.click();
			openingDropdown.sendKeys(openingtime);
			openingDropdown.sendKeys(Keys.ARROW_DOWN);
			openingDropdown.sendKeys(Keys.ENTER);
			openingDropdown.sendKeys(Keys.TAB);
			closingDropdown.click();
			closingDropdown.sendKeys(closingTime);
			openingDropdown.sendKeys(Keys.ARROW_DOWN);
			openingDropdown.sendKeys(Keys.ENTER);
			closingDropdown.sendKeys(Keys.TAB);
			clickOnContinueButton();
			Assert.assertTrue(errorMssageForOpeningclosingtime.isDisplayed());
			Reporter.log("successfully validated error message when closing time is before opening time");
		} catch (Exception e) {
			Assert.fail("Unable to validate error ");
		}
		return this;
	}
	
	/**
	 * validates error message if closing  time falls in next day
	 * @param openingtime
	 * @param closingTime
	 * @return
	 */
	public SpecialHoursPage validateclosingTimeNextDay(String openingtime,String closingTime) {
		try {
			checkPageIsReady();
			openingEarlyDropdown.click();
			openingEarlyDropdown.sendKeys(openingtime);
			//openingEarlyDropdown.sendKeys(Keys.ARROW_DOWN);
			openingEarlyDropdown.sendKeys(Keys.ENTER);
			openingEarlyDropdown.sendKeys(Keys.TAB);
			closingEarlyDropdown.click();
			closingEarlyDropdown.sendKeys(closingTime);
			openingEarlyDropdown.sendKeys(Keys.ARROW_DOWN);
			closingEarlyDropdown.sendKeys(Keys.ENTER);
			closingEarlyDropdown.sendKeys(Keys.TAB);
			clickOnContinueButton();
			Assert.assertTrue(errorMssageForOpeningclosingtime.isDisplayed());
			Reporter.log("successfully selected value from opening and closing drop down menu");
		} catch (Exception e) {
			Assert.fail("Unable to validate opening and closing dropdown");
		}
		return this;
	}
	
	/**
	 *  clicks on early radio button
	 * @return
	 */
	public SpecialHoursPage clickonEarlyRadio() {
		try {
			checkPageIsReady();
			clickElement(EarlyRadio);
			Reporter.log("clicked on EarlyRadio radio button");
		} catch (Exception e) {
			Assert.fail("Unable to validate early radio");
		}
		return this;
	}
	/**
	 *  verifies exact time radio button is displayed 
	 * @return
	 */
	public SpecialHoursPage validateExactTimesRadio() {
		try {
			checkPageIsReady();
			Assert.assertTrue(ExactTimesRadio.isDisplayed());
			Reporter.log("exact times radio button is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate  exact time radio button");
		}
		return this;
	}
	
	/**
	 * verifies early late radio button is displayed 
	 * @return
	 */
	public SpecialHoursPage validateEarlyLateRadio() {
		try {
			checkPageIsReady();
			Assert.assertTrue(EarlyRadio.isDisplayed());
			Reporter.log("Early radio button is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate early late radio button");
		}
		return this;
	}
	
	/**
	 * validates special header is displayed
	 * @return
	 */
	public SpecialHoursPage validateSpecialHoursHeader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(storeHours.isDisplayed());
			Reporter.log("store hours header is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate store hours");
		}
		return this;
	}
	
	/**
	 * clicks on calendar logo
	 * @return
	 */
	public SpecialHoursPage clickonCalendarLogo() {
		try {
			checkPageIsReady();
			clickElement(calendarLogo);
			Reporter.log("clicked on calendar logo successfully");
		} catch (Exception e) {
			Assert.fail("Unable to validate Calendar logo");
		}
		return this;
	}
	
	/**
	 * 
	 * clicks on exact time radio button
	 * @return
	 */
	public SpecialHoursPage clickonExactTimesRadio() {
		try {
			checkPageIsReady();
			clickElement(ExactTimesRadio);
			Reporter.log("clicked on exact time radio button");
		} catch (Exception e) {
			Assert.fail("Unable to validate radio");
		}
		return this;
	}
	
	/**
	 * 
	 * verify Special Hours saved
	 * @return
	 */
	public SpecialHoursPage verifySpecialHoursSaved() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(specialHoursSaved),40000);
			Assert.assertTrue(specialHoursSaved.isDisplayed());
			Reporter.log("Special hours saved header is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate 'Special hours saved' header");
		}
		return this;
	}
	
	/**
	 * 
	 * verify Saved special hours header
	 * @return
	 */
	public SpecialHoursPage verifySavedSpecialHoursHeader() {
		try {
			checkPageIsReady();
			 waitFor(ExpectedConditions.urlContains(savedEventsPageUrl),30000);
			 waitFor(ExpectedConditions.visibilityOf(savedSpecialHours));
			Assert.assertTrue(savedSpecialHours.isDisplayed());
			Reporter.log("Saved Special hours header is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate 'Saved special hours' header");
		}
		return this;
	}
	
	/**
	 * It will retrieve the number of events before Remove event is performed
	 * 
	 * @return
	 */
	public int numberOfReasonEventsBeforeRemove() {
		return reasonsInSavedSpecialHours.size();
	}
	
	/**
	 * 
	 * verify Special Hours saved event with reason on collapsed accordion
	 * @return
	 */
	public SpecialHoursPage verifySpecialHoursEventReasons() {
		try {
			checkPageIsReady();
			for(int i=0;i<reasonsInSavedSpecialHours.size();i++) {
			Assert.assertTrue(reasonsInSavedSpecialHours.get(i).isDisplayed());
			Assert.assertTrue(storeHoursInSavedSpecialHours.get(i).isDisplayed());
			Assert.assertTrue(reasonCollapsedAccordion.get(i).isDisplayed());
			}
			Reporter.log("Special hours events are created with the reason on collapsed accordion header");
		}catch (Exception e) {
			Assert.fail("Unable to create special hours event with the reason on collapsed accordion header");
		}
		return this;
	}
	
	/**
	 * 
	 * verify Special Hours saved event with reason on collapsed accordion
	 * @return
	 */
	public SpecialHoursPage verifyAccordionExpandsUponClick() {
		try {
			checkPageIsReady();
			clickElement(reasonCollapsedAccordion.get(0));
			Assert.assertTrue(reasonExpandedAccordion.get(0).isDisplayed());
			Reporter.log("Accordion is expanded and SH event details are displayed");
		}catch (Exception e) {
			Assert.fail("Unable to expand Accordion and display SH event details");
		}
		return this;
	}
	
	/**
	 * 
	 * verify SH event date
	 * @return
	 */
	public SpecialHoursPage verifySpecialHoursEventDate() {
		try {
			checkPageIsReady();
//			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/DD/YYYY");
//			Date date = new Date();
//			String date1 = dateFormat.format(date);
//			Date currentDate = dateFormat.parse(date1);
//			String specialHoursSavedEventDate = dateFormat.format(specialHoursEventDate.get(0).getText());
//			Date reasonDate = dateFormat.parse(specialHoursSavedEventDate);
//			if (reasonDate.after(currentDate)) {	
//				Reporter.log("Special hour event date is today or future date");
//			}
			Assert.assertTrue(specialHoursLabelForEventDate.get(0).isDisplayed());	
			Assert.assertTrue(specialHoursEventDate.get(0).isDisplayed());	
			Reporter.log("Special hour event date is verified in Saved events page");
		} catch (Exception e) {
			Assert.fail("Unable to display Special hour event date");
		}
		return this;
	}
	
	/**
	 * 
	 * verify Store(s) selected in comma separated list of SAP IDs
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyStoresWithSapIdInsavedSpecialHours() {
		try {
			checkPageIsReady();
			Assert.assertTrue(storesSelectedLabelForEventDate.get(0).isDisplayed());	
			Assert.assertTrue(sapIdInSavedSpecialHours.get(0).isDisplayed());
			Reporter.log("Store(s) selected in comma separated list of SAP IDs is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display Store(s) selected in comma separated list of SAP IDs");
		}
		return this;
	}
	
	/**
	 * 
	 * verify Reason header with reason value
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyReasonHeaderWithReasonValue() {
		try {
			checkPageIsReady();
			Assert.assertTrue(reasonHeaderInSavedSpecialHours.get(0).isDisplayed());
			Assert.assertTrue(reasonValueInSavedSpecialHours.get(0).isDisplayed());
			Reporter.log("Reason header with selected reason value: "+reasonValueInSavedSpecialHours.get(0).getText()+" is displayed, thus verified");
		} catch (Exception e) {
			Assert.fail("Unable to display Reason header with reason value");
		}
		return this;
	}
	
	/**
	 * 
	 * verify Description header with Description value
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyDescriptionHeaderWithDescriptionValue() {
		try {
			checkPageIsReady();
			Assert.assertTrue(descriptionHeaderInSavedSpecialHours.get(0).isDisplayed());
			Assert.assertTrue(descriptionValueInSavedSpecialHours.get(0).isDisplayed());
			Reporter.log("Description header with entered Description value: "+descriptionValueInSavedSpecialHours.get(0).getText()+" is displayed, thus verified");
		} catch (Exception e) {
			Assert.fail("Unable to display Description header with Description value");
		}
		return this;
	}
	
	/**
	 * 
	 * verify Special Hours header with Special Hours details
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySpecialHoursHeaderWithEventDate() {
		try {
			checkPageIsReady();
			Assert.assertTrue(specialHoursHeaderInSavedSpecialHours.get(0).isDisplayed());
			Assert.assertTrue(specialHoursEventDate.get(0).isDisplayed());	
			Assert.assertTrue(storeHoursInSavedSpecialHoursUnderEvent.get(0).isDisplayed());
			Reporter.log("Special Hours header with Event date: "+specialHoursEventDate.get(0).getText()+", Store hours: "+storeHoursInSavedSpecialHoursUnderEvent.get(0).getText()+" are displayed, thus verified");
		} catch (Exception e) {
			Assert.fail("Unable to display Special Hours header with Special Hours details");
		}
		return this;
	}
	
	/**
	 * 
	 * Click Create Special Hours Icon, navigates to create special hours page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyAndClickCreateSpecialHoursIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(createSpecialHoursInSavedSpecialHours.isDisplayed());
			Assert.assertTrue(createSpecialHoursIcon.isDisplayed());
			clickElement(createSpecialHoursIcon);
			verifySpecialHoursPage();
      Reporter.log("Clicking on Create special hours icon, thus takes to Create special hours page");
		} catch (Exception e) {
			Assert.fail("Unable to verify and click on Create special hours icon");
		}
		return this;
	}
	
	/**
	 * 
	 * Verify Log Out Icon
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyLogOut() {
		try {
			checkPageIsReady();
			Assert.assertTrue(logOut.isDisplayed());
      Reporter.log("Log out button is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to verify Log out button");
		}
		return this;
	}
	
	/**
	 * 
	 * Click Remove Icon, navigates to create special hours page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyAndClickRemoveIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(removeIcon.isDisplayed());
			clickElement(removeIcon);
			Assert.assertTrue(removeModal.isDisplayed());
      Reporter.log("Clicked on Remove icon, displays a Modal");
		} catch (Exception e) {
			Assert.fail("Unable to click on Remove icon");
		}
		return this;
	}
	
	/**
	 * 
	 * Verify Remove Modal
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyRemoveModal() {
		try {
			checkPageIsReady();
			Assert.assertTrue(removeModalText.isDisplayed());
      Reporter.log("Are you sure that you want to delete this event?' text is displayed in the Modal");
		} catch (Exception e) {
			Assert.fail("Unable to display the text in Remove modal");
		}
		return this;
	}
	
	/**
	 * 
	 * Verify Remove Modal
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyRemoveModalCTA() {
		try {
			checkPageIsReady();
			Assert.assertTrue(cancelInRemoveModal.isDisplayed());
			Assert.assertTrue(confirmInRemoveModal.isDisplayed());
      Reporter.log("2 CTAs 'Cancel' and 'Confirm' are displayed in Remove Modal");
		} catch (Exception e) {
			Assert.fail("Unable to display 2 CTAs 'Cancel' and 'Confirm' in Remove modal");
		}
		return this;
	}
	
	/**
	 * click on Cancel CTA
	 * 
	 * @return
	 */
	public SpecialHoursPage clickOnCancelCTA() {
		try {
			checkPageIsReady();
			clickElement(cancelInRemoveModal);
			Assert.assertTrue(!removeModal.isDisplayed());
      Reporter.log("Modal is closed, when clicked on cancel CTA");
		} catch (Exception e) {
			Assert.fail("Unable to click on cancel CTA");
		}
		return this;
	}
	
	/**
	 * click on Confirm CTA
	 * 
	 * @return
	 */
	public SpecialHoursPage clickOnConfirmCTA() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(confirmInRemoveModal));
			clickElement(confirmInRemoveModal);
      Reporter.log("Clicked on Confirm CTA in Remove Modal");
		} catch (Exception e) {
			Assert.fail("Unable to click on confirm CTA");
		}
		return this;
	}
	
	/**
	 * Verify SH event deleted
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyShEventDeleted(int eventsCountBeforeRemoval) {
		try {
			checkPageIsReady();
			Assert.assertEquals(reasonsInSavedSpecialHours.size(),eventsCountBeforeRemoval-1);
      Reporter.log("SH event is deleted from Saved events");
		} catch (Exception e) {
			Assert.fail("Unable to delete SH event from Saved events");
		}
		return this;
	}
	
	/**
	 * Verify clearing selected reason
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyClearingReason() {
		try {
			checkPageIsReady();
			clickElement(reasonClear);
      Reporter.log("Able to clear selected reason");
		} catch (Exception e) {
			Assert.fail("Unable to clear selected reason");
		}
		return this;
	}
	
	/**
	 * 
	 * Verify Saved Events Link
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySavedEventsLink() {
		try {
			checkPageIsReady();
			Assert.assertTrue(savedEventsLink.isDisplayed());
			Assert.assertTrue(savedEventsCaret.isDisplayed());
      Reporter.log("Saved events link along with its caret is displayed in 'Select store' page");
		} catch (Exception e) {
			Assert.fail("Unable to display Saved events link along with its caret in 'Select store' page");
		}
		return this;
	}
	
	/**
	 * click on Saved events link
	 * 
	 * @return
	 */
	public SpecialHoursPage clickOnSavedEventslink() {
		try {
			checkPageIsReady();
			clickElement(savedEventsLink);
      Reporter.log("Clicked on Saved events link");
		} catch (Exception e) {
			Assert.fail("Unable to click on confirm CTA");
		}
		return this;
	}
	

	/***
	 * verify Saved Events Page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySavedEventsPage() {
		try {
			waitFor(ExpectedConditions.urlContains(savedEventsPageUrl),60);
			Reporter.log("Navigated to Saved events page");
			return this;
		} catch (Exception e) {
			Assert.fail("Unable to navigate to Saved events page");
		}
		return this;
	}
	
	/**
	 * click on Log out button
	 * 
	 * @return
	 */
	public SpecialHoursPage clickOnLogOut() {
		try {
			checkPageIsReady();
			clickElement(logOut);
      Reporter.log("Clicked on Log out button");
		} catch (Exception e) {
			Assert.fail("Unable to click on confirm CTA");
		}
		return this;
	}
	
	/***
	 * verify Special Hours Login page
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySHLoginPage() {
		try {
			waitFor(ExpectedConditions.urlContains(loginPageUrl),60);
			Reporter.log("Special hours Login page is displayed");
			return this;
		} catch (Exception e) {
			Assert.fail("Unable to display Special hours Login page");
		}
		return this;
	}
	
	/***
	 * verify Special Hours Login page  after log out
	 * 
	 * @return
	 */
	public SpecialHoursPage verifySHLoginPageAfterLogout() {
		checkPageIsReady();
		try {
			if (whichAccountToSignOut.isDisplayed()) {
				clickElement(logInEmail);
			      Reporter.log("Selected Logged in email to sign out");
			      waitFor(ExpectedConditions.urlContains(loginPageUrl),60);
					Reporter.log("Special hours Login page is displayed");
					return this;
			}
			else {	
			waitFor(ExpectedConditions.urlContains(loginPageUrl),60);
			Reporter.log("Special hours Login page is displayed");
			return this;
			}
		} catch (Exception e) {
			Assert.fail("Unable to display Special hours Login page");
		}
		return this;
	}
	
	/**
	 * 
	 * Verify Log Out Icon in Saved Events
	 * 
	 * @return
	 */
	public SpecialHoursPage verifyLogOutInSavedEvents() {
		try {
			checkPageIsReady();
			Assert.assertTrue(logOutInSavedEvents.isDisplayed());
      Reporter.log("Log out button is displayed in Saved events pages");
		} catch (Exception e) {
			Assert.fail("Unable to verify Log out button in Saved events page");
		}
		return this;
	}
	
	/**
	 * click on Log out button in Saved events page
	 * 
	 * @return
	 */
	public SpecialHoursPage clickOnLogOutInSavedEvents() {
		try {
			checkPageIsReady();
			clickElement(logOutInSavedEvents);
      Reporter.log("Clicked on Log out button in Saved events page");
		} catch (Exception e) {
			Assert.fail("Unable to click on log out button in Saved events page");
		}
		return this;
	}
	
	/**
	 * Verify confirmation page is displayed
	 * @return
	 */
	public SpecialHoursPage verifyConfirmationPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(confirmationPage.isDisplayed());
			Reporter.log("Special hours event confirmation page is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display Special hours confirmation page");
		}
		return this;
	}
	
	/**
	 * Verify Error message in Modal pop up for invalid user 
	 * @return
	 */
	public SpecialHoursPage verifyErrorMessageInModalPopUp() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(errorMessageForInvalidUser));
			Reporter.log("A modal of error message 'Please contact SpecialStoreHoursAdmin@t-mobile.com for access.' should be displayed for invalid user");
		} catch (Exception e) {
			Assert.fail("Unable to display Error message for invalid user");
		}
		return this;
	}
	
	/**
	 * Verify close button in Modal pop up for invalid User
	 * @return
	 */
	public SpecialHoursPage verifycloseButtonInModalPopUp() {
		try {
			checkPageIsReady();
			Assert.assertTrue(closeButtonInModalPopUpForInvalidUser.isDisplayed());
			Reporter.log("Close button is displayed in Modal pop up");
		} catch (Exception e) {
			Assert.fail("Unable to display close button in Modl pop up for invalid user");
		}
		return this;
	}
	
	/**
	 * click on close button in modal pop up for invalid user
	 * @return
	 */
	public SpecialHoursPage clickOncloseButtonInModalPopUp() {
		try {
			checkPageIsReady();
			clickElement(closeButtonInModalPopUpForInvalidUser);
		    Reporter.log("Clicked on Close button in Modal pop up");
		} catch (Exception e) {
			Assert.fail("Unable to click on Close button in Modal pop up for invalid user");
		}
		return this;
	}
	
	
	/**
	 * Verify Entering Landing page url in the browser 
	 * @return
	 */
	public SpecialHoursPage verifyRedirectingToLandingPage() {
		try {
			checkPageIsReady();
			getDriver().navigate().to("https://dev.specialhours.t-mobile.com/special-hours");
			Reporter.log("Entered Landing page URL in the browser");
		} catch (Exception e) {
			Assert.fail("Unable to enter Landing page url in the browser");
		}
		return this;
	}
	
	/***
	 * select store hierarchy radio button
	 * 
	 * @return
	 */
	public SpecialHoursPage selectStoreHierarchyRadioButton() {
		try {
			checkPageIsReady();
			elementClick(storeHierarchyRadioButton);
		} catch (Exception e) {
			Reporter.log("Unable to select store hierarchy radio button");
			Assert.fail("Unable to select store hierarchy radio button");
		}
		return this;
	}
	
	/**
	 * verify warning message for store hierarchy non selection
	 * @return
	 */
	public SpecialHoursPage verifyWarningMessageForNonSelection() {
		try {
			Assert.assertTrue(isElementDisplayed(warningMessageforNonStoreSelection));
			Reporter.log("'Please select store area' warning message displayed");
		} catch (Exception e) {
			Reporter.log("'Please select store area' warning message not displayed");
			Assert.fail("'Please select store area' warning message not displayed");
		}
		return this;
	}
	
	/***
	 * select central in area drop down
	 * 
	 * @return
	 */
	public SpecialHoursPage selectCentralInAreaDropDown() {
		try {
			waitFor(ExpectedConditions.visibilityOf(areaDropDownSelector));
			areaDropDownSelector.click();
			waitFor(ExpectedConditions.visibilityOf(centralAreaMenu));
			centralAreaMenu.click();
			waitFor(ExpectedConditions.visibilityOf(areaDropDownSelector));
			areaDropDownSelector.click();
			waitFor(ExpectedConditions.visibilityOf(areaDropDownSelectorUpArrow));
			areaDropDownSelectorUpArrow.click();
			Assert.assertTrue(isElementDisplayed(marketDropDownSelector));
			Reporter.log("Market dropdown displayed");
		} catch (Exception e) {
			Reporter.log("Market dropdown not displayed");
			Assert.fail("Market dropdown not displayed");
		}
		return this;
	}
	
	/***
	 * select central markets in market drop down
	 * 
	 * @return
	 */
	public SpecialHoursPage selectCentralMarketsInMarketDropDown() {
		try {
			marketDropDownSelector.click();
			waitFor(ExpectedConditions.visibilityOf(centralMarketsAreaMenu));
			centralMarketsAreaMenu.click();
			Assert.assertTrue(isElementDisplayed(districtDropDownSelector));
			Reporter.log("District dropdown displayed");
		} catch (Exception e) {
			Reporter.log("District dropdown not displayed");
			Assert.fail("District dropdown not displayed");
		}
		return this;
	}
	
	/***
	 * select michigan in district drop down
	 * 
	 * @return
	 */
	public SpecialHoursPage selectMichiganInDistrictDropDown() {
		try {
			districtDropDownSelector.click();
			waitFor(ExpectedConditions.visibilityOf(michiganDistrictMenu));
			michiganDistrictMenu.click();
			Assert.assertTrue(isElementDisplayed(territoryDropDownSelector));
			waitFor(ExpectedConditions.visibilityOf(territoryDropDownSelectorUpArrow));
			territoryDropDownSelectorUpArrow.click();
			Reporter.log("Territory dropdown displayed");
		} catch (Exception e) {
			Reporter.log("Territory dropdown not displayed");
			Assert.fail("Territory dropdown not displayed");
		}
		return this;
	}
	
	/***
	 * select west michigan in territory drop down
	 * 
	 * @return
	 */
	public SpecialHoursPage selectWestMichiganInTerritoryDropDown() {
		try {
			territoryDropDownSelector.click();
			waitFor(ExpectedConditions.visibilityOf(westMichiganTeritoryMenu));
			westMichiganTeritoryMenu.click();
		} catch (Exception e) {
			Assert.fail("Click on west michigan menu failed");
		}
		return this;
	}
	
	/**
	 * verify store selector results
	 * @return
	 */
	public SpecialHoursPage verifyStoreSelectorResults() {
		try {
			waitFor(ExpectedConditions
					.invisibilityOfElementLocated(By.cssSelector(".fa.fa-spinner.fa-pulse.spinner-icon")));
			Assert.assertTrue(isElementDisplayed(storeSelectorResults));
			Reporter.log("Store selector results displayed");
		} catch (Exception e) {
			Reporter.log("Store selector results not displayed");
			Assert.fail("Store selector results not displayed");
		}
		return this;
	}
	
}

