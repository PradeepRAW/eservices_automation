package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class IdentityReviewQuestionPage extends CommonPage {

	@FindBy(css = "[ng-value*='option.choiceid']")
	private List<WebElement> questionsRadioButton;
	
	@FindBy(css = "[ng-click*='ctrl.submitAnswer()']")
	private WebElement submitButton;
	
	@FindBy(css = "span[ng-bind*='cancelCTAText']")
    private WebElement reviewQuestionCancelCTA;
	
	@FindBy(css = "button[ng-click*='redirectToReject()']")
	   private WebElement warningModalCancelCTA;
	
	@FindBy(css = "[ng-click*='ctrl.goToback']")
	private WebElement backButton;
	
	@FindBy(css = "[ng-if*='ctrl.isCustomHeader']")
	private WebElement sedonaHeader;
	
	/**
	 * @param webDriver
	 */
	public IdentityReviewQuestionPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify Identity Review Introduction Page
	 * 
	 * @return
	 */
	public IdentityReviewQuestionPage verifyReviewQuestionPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
			Reporter.log("Fraud Question reviw page is visible");
		} catch (Exception e) {
			Assert.fail(" Failed to Fraud Question reviw page Page not loaded");
		}
		return this;
	}
	
	
	/**
	 * Verify that page is review question page
	 */
	public IdentityReviewQuestionPage verifyPageUrl() {
		try{
			getDriver().getCurrentUrl().contains("identityreviewquestionnaire");
		}catch(Exception e){
			Assert.fail("Identity review questionnaire page is not displayed");
		}
		return this;
	}
	
	/**
	 * click first answer on Identity Review question
	 */
	public IdentityReviewQuestionPage clickFirstOption() {
		try {
			clickElementWithJavaScript(questionsRadioButton.get(0));
			Reporter.log("first answer is clicked");
		} catch (Exception e) {
			Reporter.log("Failed to click on first answer");
		}
		return this;
	}
	
	/**
	 * click on submit Identity Review question
	 */
	public IdentityReviewQuestionPage clickSubmit() {
		try {
			waitFor(ExpectedConditions.visibilityOf(submitButton));
			clickElementWithJavaScript(submitButton);
			Reporter.log("submit Identity Review question is clicked");
		} catch (Exception e) {
			Reporter.log("Failed to click on submit Identity Review question");
		}
		return this;
	}
	/**
	 * Verify Identity Review Introduction Page
	 * 
	 * @return
	 */
	public IdentityReviewQuestionPage verifyReviewQuestionCancelCTA() {

		try {
			Assert.assertTrue(reviewQuestionCancelCTA.isDisplayed(), "review question cancel cta is not displayed");
			Reporter.log("review question cancel cta  is Displayed");
		} catch (Exception e) {
			Assert.fail(" Failed to review question cancel cta  not loaded");

		}
		return this;
	}
	
	/**
	 * verify Submit CTA is disabled
	 */
	public IdentityReviewQuestionPage verifySubmitDisabled() {
		try {
			Assert.assertFalse(submitButton.isEnabled(), "Submit CTA is not disabled");
			Reporter.log("submit CTA is disabled");
		} catch (Exception e) {
			Reporter.log("Failed to verify if submit CTA is disabled");
		}
		return this;
	}
	
	/**
	 * verify Submit CTA is enabled
	 */
	public IdentityReviewQuestionPage verifySubmitEnabled() {
		try {
			Assert.assertTrue(submitButton.isEnabled(), "Submit CTA is not enabled");
			Reporter.log("submit CTA is enabled");
		} catch (Exception e) {
			Reporter.log("Failed to verify if submit CTA is enabled");
		}
		return this;
	}
    /*
	 * Verify Identity Review Introduction Page
	 * 
	 * @return
	 */
	public IdentityReviewQuestionPage clickReviewQuestionCancelCTA() {

		try {
			reviewQuestionCancelCTA.click();
			Reporter.log("review question cancel cta  is clicked");
		} catch (Exception e) {
			Assert.fail(" Failed to review question cancel cta  not loaded");

		}
		return this;
	}
	
	/**
	 * click on submit Identity Review question
	 */
	public IdentityReviewQuestionPage clickWarningCancelCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(warningModalCancelCTA));
			waitFor(ExpectedConditions.elementToBeClickable(warningModalCancelCTA));
			clickElementWithJavaScript(warningModalCancelCTA);
			Reporter.log("warningModalCancelCTA  is clicked");
		} catch (Exception e) {
			Reporter.log("Failed to click on warningModalCancelCTA");
		}
		return this;
	}
	
	
	/**
	 * Verify Back Button not displayed
	 */
	public IdentityReviewQuestionPage verifyBackButtonNotDisplayed() {
		try {
			Assert.assertFalse(backButton.isDisplayed(), "Back Button is displayed");
			Reporter.log("Back Button is not displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Back Button");
		}
		return this;
	}
	
	
	
	/**
	 * Verify Identity Review Introduction Page Header
	 */
	public IdentityReviewQuestionPage verifySedonaHeaderDisplayed() {
		try {
			Assert.assertTrue(sedonaHeader.isDisplayed(), "Header is not displayed");
			Reporter.log("Sedona Header is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Sedona Header on Introduction Page");
		}
		return this;
	}
	
}
