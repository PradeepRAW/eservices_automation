package com.tmobile.eservices.qa.pages.payments.api;
import org.json.JSONArray;
import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSservicequotefilter extends  EOSCommonLib {

	



/***********************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 ***********************************************************************************/

public Response getResponsesedonaquote(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	    	JSONObject rootval = new JSONObject();
	    	
	    	JSONObject salesinfo = new JSONObject();
	    	salesinfo.put("originalDealerCode", "123456");
	    	salesinfo.put("salesChannel", "WEB");  	
	    	
	    	JSONArray quoteitems=new JSONArray();   	
	    
	    	JSONObject requestParams = new JSONObject();	    	
	    	
	 		requestParams.put("transactionType", "BILLPAY");
	 		requestParams.put("transactionSubType", "AUTORESTORE");
	 		quoteitems.put(requestParams);
	 		rootval.put("salesInfo", salesinfo);
	 		rootval.put("accountNumber", alist[3]);
	 		rootval.put("quoteItems", quoteitems);
	 		
	 		
	 		
	 	RestService restService = new RestService(rootval.toString(), eep.get(environment));
	    restService.setContentType("application/json");
	    
	    restService.addHeader("Authorization", alist[2]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("Accept", "application/json");
	    restService.addHeader("interactionId", "543219876");
	    restService.addHeader("workflowId", "CHANGEMSISDN");
	    
	    response = restService.callService("v1/generatequote",RestCallType.POST);
	    
	}

	    return response;
}


public String checkissedona(Response response) {
	
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("quoteId")!=null) return "true";
		else return "false";
	}
	return "false";



}




public String getquoteid(Response response) {
	String quoteid="";
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("quoteId")!=null) return jsonPathEvaluator.get("quoteId").toString();
		else return "";
	}
	return quoteid;



}






/***********************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 ***********************************************************************************/

public Response getResponsepaquote(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	    	JSONObject rootval = new JSONObject();
	    	
	    	JSONObject salesinfo = new JSONObject();
	    	salesinfo.put("originalDealerCode", "123456");
	    	salesinfo.put("salesChannel", "WEB");  	
	    	
	    	JSONArray quoteitems=new JSONArray();   	
	    
	    	JSONObject requestParams = new JSONObject();	    	
	    	
	 		requestParams.put("transactionType", "MANAGEPAYMENTARRANGEMENT");
	 		requestParams.put("effectiveStartDate", "2017-04-27 03:38:00");
	 		quoteitems.put(requestParams);
	 		rootval.put("salesInfo", salesinfo);
	 		rootval.put("accountNumber", alist[3]);
	 		rootval.put("quoteItems", quoteitems);
	 		
	 		
	 		
	 	RestService restService = new RestService(rootval.toString(), eep.get(environment));
	    restService.setContentType("application/json");
	    
	    restService.addHeader("Authorization", alist[2]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("Accept", "application/json");
	    restService.addHeader("interactionId", "543219876");
	    restService.addHeader("workflowId", "CHANGEMSISDN");
	    
	    response = restService.callService("v1/generatequote",RestCallType.POST);
	    
	}

	    return response;
}




}

