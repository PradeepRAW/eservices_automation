package com.tmobile.eservices.qa.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

/**
 * @author rnallamilli
 * 
 */
public class MultipleDevicesPage extends CommonPage {

	private static final Logger logger = LoggerFactory.getLogger(MultipleDevicesPage.class);

	private static final String pageUrl = "profile/multiple_devices";

	@FindBy(xpath = "//p[contains(text(),'DIGITS')]")
	private WebElement digitsHeader;

	@FindBy(xpath = "//p[contains(text(),'Devices Status')]")
	private WebElement deviceStatusHeader;

	@FindBy(xpath = "//a[contains(text(),'Manage')]")
	private WebElement manageSettings;

	@FindBy(xpath = "//p[contains(text(),'Devices')]/../../div/span/label")
	private WebElement deviceStatusToggle;

	@FindBy(xpath = "//p[contains(text(),'Devices')]/../../div/span//a")
	private WebElement deviceStatusMorebtn;

	@FindBy(xpath = "//p[contains(text(),'It may take up to 2 hours for changes to take effect.')]")
	private WebElement showMoreTxt;

	public MultipleDevicesPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify MultipleDevices page.
	 *
	 * @return the MultipleDevicesPage class instance.
	 */
	public MultipleDevicesPage verifyMultipleDevicesPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("MultipleDevices page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MultipleDevices page not displayed");
		}
		return this;

	}

	/**
	 * Verify Digits header.
	 */
	public MultipleDevicesPage verifyDigitsHeader() {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOf(digitsHeader));
		try {
			digitsHeader.isDisplayed();
			Reporter.log("Digits header displayed");
		} catch (Exception e) {
			Assert.fail("Digits header not displayed");
		}
		return this;
	}

	/**
	 * Verify DeviceStatus header.
	 */
	public MultipleDevicesPage verifyDeviceStatusHeader() {
		waitforSpinnerinProfilePage();
		try {
			deviceStatusHeader.isDisplayed();
			Reporter.log("Device status header displayed");
		} catch (Exception e) {
			Assert.fail("Device status header displayed");
		}
		return this;
	}

	/**
	 * Click Digits Manage Settings link.
	 */
	public MultipleDevicesPage clickManageSettingsLink() {
		waitforSpinnerinProfilePage();
		try {
			manageSettings.click();
			Reporter.log("Clicked on manage settings link");
		} catch (Exception e) {
			Assert.fail("Click on manage settings link failed");
		}
		return this;
	}

	/**
	 * Click Device Status toggle.
	 */
	public MultipleDevicesPage clickDeviceStatusToggle() {
		waitforSpinnerinProfilePage();
		try {
			if (!isElementDisplayed(deviceStatusMorebtn)) {
				deviceStatusToggle.click();
				Reporter.log("Clicked device status toggle");
			}

		} catch (Exception e) {
			Assert.fail("Click on device status toggle failed");
		}
		return this;
	}

	/**
	 * Click DeviceStatus more button.
	 */
	public MultipleDevicesPage clickDeviceStatusMorebtn() {
		waitforSpinnerinProfilePage();
		try {
			deviceStatusMorebtn.click();
			showMoreTxt.isDisplayed();
			deviceStatusMorebtn.click();
			Reporter.log("Show more btn is displayed");

		} catch (Exception e) {
			Assert.fail("Click on show more button failed");
		}
		return this;
	}

}
