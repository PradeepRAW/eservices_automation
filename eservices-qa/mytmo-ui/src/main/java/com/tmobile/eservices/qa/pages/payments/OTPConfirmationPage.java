package com.tmobile.eservices.qa.pages.payments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class OTPConfirmationPage extends CommonPage {

	@FindBy(css = "div#ajax_waitCursor div b")
	private WebElement partialPaymetnconfirmationMessage;

	@FindBy(css = "div.ui_headline.comfirmation-message")
	private WebElement fullPaymentConfirmationMessage;

	@FindBy(linkText = "shopping")
	private WebElement shoppingLink;

	@FindBy(css = ".ui_subhead b")
	private WebElement signUpAutoPayHeader;

	@FindBy(css = "autopay-pitch-page button")
	private WebElement signUpAutoPayButton;

	@FindBy(css = "h4.h4-title-mobile")
	private WebElement thanksHeader;

	@FindAll({ @FindBy(css = "div.row div.col-12.col-md-10 p"),
			@FindBy(css = "span.fine-print-body.text-black.alert-align-text.ng-binding") })
	private WebElement otpConfirmationAlerts;

	@FindBy(css = "div.fine-print-body.text-black.ng-binding.text-center")
	private WebElement otpPaymentDelayAlert;

	@FindBy(xpath = "//a[contains(text(),'enrolling in AutoPay')]")
	private WebElement enrollAutopay;

	@FindBy(xpath = "//span[contains(text(),'Show details')]")
	private WebElement showDetails;

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	@FindBy(css = ".fine-print-body.pull-left.ng-binding")
	private List<WebElement> paymentSection;

	@FindBy(css = ".fine-print-body.pull-right.ng-binding")
	private List<WebElement> paymentSectionValues;

	@FindBy(css = ".fine-print-body.pull-right.ng-scope span")
	private WebElement paymentMethod;
	
	@FindBy(css = "a[ng-click='vm.showDetails(true)']")
	private WebElement showPaymentDetails;

	@FindBy(css = "a[ng-click='vm.showDetails(false)']")
	private WebElement hidePaymentDetails;

	@FindBy(css = "*[ng-click='vm.redirectToHome()']")
	private WebElement returnToHome;

	@FindBy(css = "autopay-pitch-page div.text-center.tele-groteskult p")
	private WebElement autopaySetupDiv;

	@FindBy(css = "div.text-center.body-copy-description-regular.text-black.ng-binding")
	private WebElement otpPaymentTxt;
	
	@FindBy(css = "div.text-center.body-copy-description-regular.text-black.ng-binding span")
	private WebElement otpPaymentMethodInAlert;
	
	@FindBy(css = ".text-center.fine-print-body.legal-txt-clr.ng-binding")
	private WebElement FirstPaymentDateOnAutopayLandingPage;

	@FindBy(css = "div#addPayment div.body-copy-description-bold")
	private WebElement paymentMethodBlade;

	@FindBy(css = "a[ng-click='vm.goToFAQ()']")
	private WebElement autoPayFAQsLink;

	@FindBy(css = "a[ng-click='vm.goToTnC()']")
	public WebElement termsNconditions;

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindBy(css = "*[ng-click='vm.redirectToHome()']")
	private WebElement pageLoadElement;

	private final String pageUrl = "/confirmation";

	@FindBy(linkText = "View payment schedule")
	private WebElement paLinkOnNotificaiton;

	@FindBy(linkText = "Review previous payments")
	private WebElement reviewPreviouspayemntsLink;
	
	
	@FindBy(css="span.alert-align-text")
	private WebElement paMessageNotification;

	@FindBy(css="")
	private WebElement continueShoppingBtn;

	/**
	 * 
	 * @param webDriver
	 */
	public OTPConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public OTPConfirmationPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */

	public OTPConfirmationPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			thanksHeader.isDisplayed();
			Reporter.log("Verified OTP page header");
		} catch (Exception e) {
			Assert.fail("OTP page header not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OTPConfirmationPage verifyPAClosingAlert() {
		try {
			scrollToElement(otpConfirmationAlerts);
			waitFor(ExpectedConditions.visibilityOf(otpConfirmationAlerts));
			otpConfirmationAlerts.isDisplayed();
			otpConfirmationAlerts.getText().replaceAll("[0-9,/,$,.]", "").contains(
					"Your Payment Arrangement is now complete! Save time & avoid missing payments in the future by");
			Reporter.log("Verified PA closing alert");
		} catch (Exception e) {
			Assert.fail("PA closing alert not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OTPConfirmationPage verifyAlertForAmountGreaterThanInstallement() {
		try {
			scrollToElement(otpConfirmationAlerts);
			waitFor(ExpectedConditions.visibilityOf(otpConfirmationAlerts));
			otpConfirmationAlerts.isDisplayed();
			otpConfirmationAlerts.getText().replaceAll("[0-9,/,$,.]", "")
					.contains("This payment covers your payment arrangement amount due");
			Reporter.log("Verified alert amount greater than installment");
		} catch (Exception e) {
			Assert.fail("Alert component is missing");
		}
		return this;
	}

	public OTPConfirmationPage clickonEnrollAutopay() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(enrollAutopay));
			clickElementWithJavaScript(enrollAutopay);
			Reporter.log("Clicked on Enroll Autopay");
		} catch (Exception e) {
			Assert.fail("Enroll autopay component not found");
		}
		return this;
	}

	/**
	 * verify alert for amountless than installment otp confirmation page inside
	 * blacout period
	 * 
	 * @return boolean
	 */
	public OTPConfirmationPage verifyAlertForAmountLessThanInstallementInsideBlackoutperiod() {
		try {
			scrollToElement(otpConfirmationAlerts);
			waitFor(ExpectedConditions.visibilityOf(otpConfirmationAlerts));
			otpConfirmationAlerts.isDisplayed();
			otpConfirmationAlerts.getText().replaceAll("[0-9,/,$,.]", "").contains(
					"You need to have paid toward your Payment Arrangement by today to avoid suspension & fees Increase amount or review previous payments");
			Reporter.log("Verified Blackout period alert ");
		} catch (Exception e) {
			Assert.fail("Blackout period alert not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OTPConfirmationPage verifyAlertForAmountLessThanInstallement() {
		try {
			scrollToElement(otpConfirmationAlerts);
			waitFor(ExpectedConditions.visibilityOf(otpConfirmationAlerts));
			otpConfirmationAlerts.isDisplayed();
			otpConfirmationAlerts.getText().replaceAll("[0-9,/,$,.]", "").contains(
					"You need to have paid  toward your Payment Arrangement by  to avoid suspension & fees Increase amount or review payments made");
			Reporter.log("Verified Alert for less than installment");
		} catch (Exception e) {
			Assert.fail("Alert for less than installment component not found");
		}
		return this;
	}

	/**
	 * verify payment transaction lines below Thanks header
	 * 
	 * @return true/false
	 */
	public OTPConfirmationPage verifyOtppaymentMessages() {
		try {
			otpPaymentTxt.isDisplayed();
			Reporter.log("Verified OTP payment messages");
		} catch (Exception e) {
			Assert.fail("OTP payment messages not found");
		}
		return this;
	}

	/**
	 * verify card details are displayed
	 * 
	 * @return
	 */
	public OTPConfirmationPage verifyPaymentCardDetails() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			showPaymentDetails.isDisplayed();
			showPaymentDetails.click();
			for (WebElement webElement : paymentSection) {
				webElement.isDisplayed();
			}
			hidePaymentDetails.click();
			Reporter.log("Verified payment card details");
		} catch (Exception e) {
			Assert.fail("Payment card details component not found");
		}
		return this;
	}

	/**
	 * verify OTP confirmation page elements
	 * 
	 * @return
	 */
	public OTPConfirmationPage verifyOTPConfirmationPage() {
		try {
			verifyPageLoaded();
			verifyOtppaymentMessages();
			verifyReturnToHome();
		} catch (Exception e) {
			Assert.fail("No thanks button not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OTPConfirmationPage verifyAutoPayProcessingAlert() {
		try {
			scrollToElement(otpConfirmationAlerts);
			otpConfirmationAlerts.isDisplayed();
			otpConfirmationAlerts.getText().contains("Autopay will process your remaining account balance of $");
			Reporter.log("Verified auto pay alert message and balance");
		} catch (Exception e) {
			Assert.fail("Auto pay alert component missing");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OTPConfirmationPage verifyAutoPaywillnotProcessAlert() {
		try {
			otpConfirmationAlerts.isDisplayed();
			Assert.assertTrue(otpConfirmationAlerts.getText()
					.contains("Your account balance has been paid in full. AutoPay will no longer process"));
			Reporter.log("Autopay not Processing alert is displayed");
		} catch (Exception e) {
			Assert.fail("Auto pay alert component not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OTPConfirmationPage verifyAutoPaysubAlert() {
		try {
			otpPaymentDelayAlert.isDisplayed();
			otpPaymentDelayAlert.getText().contains("Allow up to two hours for this payment to post on your account");
			Reporter.log("Autopay sub alert is displayed");
		} catch (Exception e) {
			Assert.fail("Autopay sub alert not displayed");
		}
		return this;
	}

	/**
	 * 
	 *
	 * @return the OTPConfirmationPage class instance.
	 */
	public OTPConfirmationPage verifyAutoPayAlerts() {
		try {
			verifyAutoPayProcessingAlert();
			Reporter.log("Autopay Processing alert is displayed");
			verifyAutoPaysubAlert();
			Reporter.log("Autopay sub alert is displayed");
		} catch (Exception e) {
			Assert.fail("Autopay alerts not displayed");
		}
		return this;
	}

	public OTPConfirmationPage verifyReturnToHome() {
		try {
			returnToHome.isDisplayed();
			Reporter.log("Retrurn to home button displayed");
		} catch (Exception e) {
			Assert.fail("Return to home button not found");
		}
		return this;
	}

	public OTPConfirmationPage clickReturnToHome() {
		try {
			returnToHome.click();
			Reporter.log("Clicked on Return to home");
		} catch (Exception e) {
			Assert.fail("Return to home button not found");
		}
		return this;
	}

	public OTPConfirmationPage verifysetupAutoPay() {
		try {
			autopaySetupDiv.isDisplayed();
			Reporter.log("AutoPay setup verified");
		} catch (Exception e) {
			Assert.fail("Autopay setup not found");
		}
		return this;
	}

	/*
	 * validate AutoPay block in OTP page
	 */
	public OTPConfirmationPage verifySetupAutoPayBlock() {
		try {
			verifysetupAutoPay();
			Reporter.log("Autopay Setup header is displayed");
			Assert.assertTrue(FirstPaymentDateOnAutopayLandingPage.getText().contains("First Autopay payment date: "),
					"First payment date content not dispalyed");
			Reporter.log("First payment date content is dispalyed");
			Assert.assertTrue(paymentMethodBlade.isDisplayed(), "Payment method blade is not dispalyed");
			Reporter.log("Payment method blade is dispalyed");
			Assert.assertTrue(autoPayFAQsLink.isDisplayed(), "See FAQs link is not displayed");
			Reporter.log("See FAQs link is not displayed");
			Assert.assertTrue(termsNconditions.isDisplayed(), "Terms & Conditions link is not displayed.");
			Reporter.log("Terms & Conditions link is displayed");
			Assert.assertTrue(signUpAutoPayButton.getText().contains("Sign up for AutoPay"),
					"Sign up for AutoPay button is not dispalyed");
			Reporter.log("Sign up for AutoPay button is dispalyed");
		} catch (Exception e) {
			Assert.fail("Set up Auto pay block not displayed");
		}
		return this;
	}

	/**
	 * 
	 *
	 * @return the OTPConfirmationPage class instance.
	 */
	public OTPConfirmationPage verifyAutoPayNotProcessAlerts() {
		try {
			verifyAutoPaywillnotProcessAlert();
			verifyAutoPaysubAlert();
		} catch (Exception e) {
			Assert.fail("Auto pay Alerts not present");
		}
		return this;
	}

	public OTPConfirmationPage clickOnShowdetails() {
		try {
			showDetails.click();
			Reporter.log("Clicked 'Show Details' link");
		} catch (Exception e) {
			Assert.fail("'Show details' link not found");
		}
		return this;
	}

	/**
	 * Verify otp confirmation Page
	 * 
	 * @return
	 */
	public OTPConfirmationPage verifyOtpConfirmationPage() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			thanksHeader.isDisplayed();
			thanksHeader.getText().contains("THANKS FOR YOUR PAYMENT");
			Reporter.log("Verified OTP Confirmation page");
		} catch (Exception e) {
			Assert.fail("OTP Confirmation page not loaded");
		}
		return this;
	}

	/**
	 * verify card details are displayed
	 * 
	 * @param routingNo
	 * @param accNo
	 * 
	 * @return
	 */
	public OTPConfirmationPage verifyPaymentBankDetails(String accNo, String routingNo) {
		boolean paymentDetailsDisplayed = false;
		try {
			showPaymentDetails.click();
			List<String> paymentdetails = new ArrayList<>(Arrays.asList("Name on account", "T-Mobile account no.",
					"Reference no.:", "Name on bank", "Payment method:", "Routing no:", "Transaction type:"));

			for (String detail : paymentdetails) {
				for (int i = 0; i < paymentSection.size(); i++) {

					if (paymentSection.get(i).isDisplayed() && detail.equals(paymentSection.get(i).getText())) {
						paymentDetailsDisplayed = true;
					}
					if (paymentSection.get(i).getText().equals("Payment method:")) {
						Assert.assertTrue(paymentSectionValues.get(i).getText()
								.contains("Checking ****" + accNo.substring(accNo.length() - 4)));
					}
					if (paymentSection.get(i).getText().equals("Routing no:")) {
						Assert.assertTrue(paymentSectionValues.get(i).getText()
								.contains("****" + routingNo.substring(routingNo.length() - 4)));
					}
				}
				if (!paymentDetailsDisplayed) {
					break;
				}
			}
			hidePaymentDetails.click();
			Reporter.log("Verified payment bank details");
		} catch (Exception e) {
			Assert.fail("Payment bank details not found ");
		}
		return this;
	}

	/**
	 * click Auto Pay signup button
	 */
	public OTPConfirmationPage clickAutoPaySignUpBtn() {
		try {
			signUpAutoPayButton.click();
			Reporter.log("Clicked on AutoPay SignUP button");
		} catch (Exception e) {
			Assert.fail("Autopay signUP button not found");
		}
		return this;
	}

	/**
	 * This method is to verify PII masking is done for all payment information
	 */
/*	public void verifyPIIMaskingOld() {
		try {
			boolean isMasked = false;
			List<String> pidDetails = new ArrayList<>(
					Arrays.asList("cust_name", "cust_account", "cust_crd", "cust_account"));
			showPaymentDetails.click();
			for (int i = 0; i < paymentSectionValues.size(); i++) {
				for (String pid : pidDetails) {
					String pidAttr = paymentSectionValues.get(i).getAttribute("pid");
					if (!pidAttr.isEmpty() && pidAttr.contains(pid) && pidAttr.matches(pid + "\\d+")) {
						isMasked = true;
						break;
					}
				}
				if (!isMasked) {
					Assert.fail("Pii masking for Payment bank details not found");
				}
			}
			Reporter.log("Verified PII masking for Payment bank details");
			hidePaymentDetails.click();
		} catch (Exception e) {
			Assert.fail("Pii masking for Payment bank details not found");
		}
	}*/

	/**
	 * This method is to verify PII masking is done for all payment information
	 */
	public void verifyPIIMasking() {
		try {
			String alertPIDAttr = otpPaymentMethodInAlert.getAttribute("pid");
			Boolean isMasked = !alertPIDAttr.isEmpty() && alertPIDAttr.contains("cust_crd")
					&& alertPIDAttr.matches("cust_crd\\d+");
			Assert.assertTrue(isMasked, "PII Masking is not applied for notification text");
			showPaymentDetails.click();
			for (int i = 0; i < paymentSection.size(); i++) {
				switch (paymentSection.get(i).getText()) {
				case "Name on account":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i), "cust_name"));
					break;
				case "T-Mobile account no.":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i), "cust_account"));
					break;
				case "Payment method:":
					Assert.assertTrue(checkElementisPIIMasked(paymentMethod, "cust_crd"));
					break;
				case "Routing no:":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i-1), "cust_crd"));
					break;
				case "Zip:":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i-1), "cust_zip"));
					break;	
				default:
					break;
				}
			}
			Reporter.log("Verified PII masking for Payment details");
			hidePaymentDetails.click();
		} catch (Exception e) {
			Assert.fail("Pii masking for Payment details not found");
		}
	}

	public OTPConfirmationPage verifyPANotification(String paNotification) {
		try {
			waitFor(ExpectedConditions.visibilityOf(otpConfirmationAlerts));
			otpConfirmationAlerts.isDisplayed();
			otpConfirmationAlerts.getText().replaceAll("[0-9,/,$,.]", "").contains(paNotification);
			Reporter.log("Verified PA closing alert");
		} catch (Exception e) {
			Assert.fail("PA closing alert not found");
		}
		return this;
	}

	/**
	 * verify PA link on notification
	 */
	public void verifyPALink() {
		try {
			paLinkOnNotificaiton.isDisplayed();
			Reporter.log("PA link is displayed on notification");
		} catch (Exception e) {
			Assert.fail("PA link is not displayed on notification");
		}
	}
	
	/**
	 * click PA link on notification
	 */
	public void clickPALink() {
		try {
			paLinkOnNotificaiton.click();
			Reporter.log("PA link is clicked on notification");
		} catch (Exception e) {
			Assert.fail("PA link is not found");
		}
	}

	/**
	 * click review previous payments link
	 */
	public void clickReviewPreviousPayments() {
		try {
			reviewPreviouspayemntsLink.click();
			Reporter.log("Clicked on Review Previous Payments link");
		} catch (Exception e) {
			Assert.fail("Review Previous Payments link is not found");
		}
	}
	
	public void verifyPAConfirmationAlert(String ConfirmationAlert) {
		try {
			
			waitFor(ExpectedConditions.visibilityOf(paMessageNotification));
			Assert.assertTrue(paMessageNotification.getText().contains(ConfirmationAlert));
			Reporter.log(paMessageNotification.getText() + "PA Notification Message is displayed on OTP Confirmation Page");
		} catch (Exception e) {
			Assert.fail("PA Notification Message is not  displayed on OTP Confirmation Page");
		}
		
	}
	
	
	/**
	 * Verify review previous payments link
	 */
	public void verifyReviewPreviousPayments() {
		try {
			reviewPreviouspayemntsLink.isDisplayed();
			Reporter.log(" Review Previous Payments link is displayed on Payment confirmation Page");
		} catch (Exception e) {
			Assert.fail("Review Previous Payments link is not found on Payment confirmation Page");
		}
	}

	/**
	 * verify continue shopping button for Shop OTP flow
	 */
	public void verifyContinueShoppingBtn() {
		try {
			continueShoppingBtn.isDisplayed();
			Reporter.log("Continue shopping button is displayed");
		} catch (Exception e) {
			Assert.fail("Continue shopping button is not found");
		}
	}

	/**
	 * click on continue shopping button
	 */
	public void clickContinueShoppingBtn() {
		try {
			continueShoppingBtn.click();
			Reporter.log("Clicked on Continue shopping button");
		} catch (Exception e) {
			Assert.fail("Continue shopping button is not found");
		}
	}
	
}
