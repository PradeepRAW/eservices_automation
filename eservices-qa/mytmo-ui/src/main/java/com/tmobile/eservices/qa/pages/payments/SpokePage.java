package com.tmobile.eservices.qa.pages.payments;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author pshiva
 *
 */
public class SpokePage extends CommonPage {

	private final String pageUrl = "spoke";
	private final String newPageUrl = "paymentcollection";

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");
	@FindAll({ @FindBy(xpath = "//h4[contains(text(),'Select Payment Method')]"),
			@FindBy(xpath = "//h4[contains(text(),'Select Payment method')]") })

	private WebElement paymentHeader;
	@FindAll({ @FindBy(css = "div.col-xs-12 h4"), @FindBy(css = "div.Display3"),
			@FindBy(css = "span[role='heading']") })
	private WebElement pageHeader;

	@FindBy(css = "div:not([class*='padding-horizontal-medium'])>span.col-12")
	private WebElement deleteCardModelHeader;

	@FindBy(css = "div.radio-container")
	private List<WebElement> radioBtn;

	@FindAll({ @FindBy(css = "div:not(.no-padding)>div.row.padding-vertical-medium"),
			@FindBy(css = "div.TMO-RADIO-PAYMENT-BLADE-PAGE") })
	private List<WebElement> paymentMethodBlades;

	@FindBy(css = "div.pt-lg-1>span span:first-of-type")
	private List<WebElement> paymentMethodIcon;

	@FindAll({ @FindBy(css = "span[class^='Display6']"),
			@FindBy(css = "div.glueRadio-container>input:not([value='option4'])"),
			@FindBy(css = "input[name='radiobutton']"), @FindBy(css = "p[class^='Display6']>span[pid*=cust_crd]") })
	private List<WebElement> paymentNumber;

	@FindBy(css = "a.black")
	private List<WebElement> deleteLinks;

	@FindBy(css = "div:not([class*='padding-horizontal-medium'])>span.col-12")
	private WebElement deleteBankModelHeader;

	@FindBy(css = "div[class$='text-center'] div[class$='body'] ")
	private WebElement deleteModelSubHeader;

	@FindBy(css = "div.padding-top-small span.black ")
	private WebElement cardNumberOnModel;

	@FindBy(css = "div.px-md-0>button.SecondaryCTA-accent")
	private WebElement deleteModelNoBtn;

	@FindBy(css = "div.px-md-0>button.PrimaryCTA-accent")
	private WebElement deleteModelYesBtn;

	@FindBy(xpath = "//span[contains(@class,'icon')]/following-sibling::span[contains(@class,'align-top')]")
	private List<WebElement> storedCards;

	@FindBy(xpath = "//span[contains(@class,'Bank')]/following-sibling::span[contains(@class,'align-top')]")
	private List<WebElement> storedBanks;

	@FindBy(xpath = "//span[contains(@class,'icon')]/ancestor::span/following-sibling::span/a")
	private List<WebElement> storedCardsDeleteLinks;

	@FindBy(xpath = "//span[contains(@class,'Bank')]/ancestor::span/following-sibling::span/a")
	private List<WebElement> storedBanksDeleteLinks;

	@FindAll({ @FindBy(css = "div#bank p.body-copy-description-bold"),
			@FindBy(css = "div[routerlink='/payments/addbank']>span.black"),
			@FindBy(css = "div[class*='PAYMENT']>div:first-of-type>div>span.black"),
			@FindBy(xpath = "//span[contains(text(),'Add a bank')]"),
			@FindBy(xpath = " //span[contains(text(),'Add Bank')]") })
	private WebElement addBank;

	@FindAll({ @FindBy(css = "div[ng-show='$ctrl.paymentMethodSpokeService.isAccValidForCard']"),
			@FindBy(css = "div.TMO-PAYMENT-BLADE-PAGE>div+div>div.cursor"),
			@FindBy(css = "span.pading-blade-top-xxs.pull-left.Display6.black"),
			@FindBy(xpath = "//span[contains(text(),'Add Card')]") })
	private WebElement addCard;

	@FindBy(xpath = "//span[contains(@class,'icon-echeckAccount')]/following-sibling::p")
	private WebElement storedPaymentCheckingNumber;

	@FindAll({ @FindBy(xpath = "//span[contains(@class,'Card')]/following-sibling::p"),
			@FindBy(xpath = "//span[contains(@class,'Card')]/../..//div/p[@class='card-blade-title']/span") })
	private WebElement storedCardNumber;

	@FindBy(xpath = "//span[contains(@class,'icon')]/../../div/p/span[@pid]")
	private WebElement storedCardNoAngular;

	@FindBy(xpath = "//span[contains(@class,'icon')]/../../div/p/span[@pid or text()='Edit']")
	private List<WebElement> storedPaymentsCard;

	@FindBy(xpath = "//span[contains(@class,'Bank')]/../../div/p/span[@pid or text()='Edit']")
	private List<WebElement> storedPaymentsBank;

	@FindBy(id = "storedCard")
	private List<WebElement> storedCardRadioBtn;

	@FindBy(css = "div#storedCard div input")
	private WebElement storedCardMobileAttr;

	@FindBy(css = "div#storedCard div span.radio-ht")
	private WebElement storedCardRadioBtnMobile;

	@FindBy(css = "div#storedBank div span.radio-ht")
	private WebElement storedBankRadioBtnMobile;

	@FindBy(id = "storedBank")
	private List<WebElement> storedBankRadioBtn;

	@FindBy(css = "div#storedBank i")
	private WebElement removeCheckingAccountOption;

	@FindBy(xpath = "//span[contains(text(), 'Remove method?')]")
	private List<WebElement> removeStoredCardLink;

	@FindBy(xpath = "//i[contains(@class,'fa-trash') and contains(@ng-class,'isDeleteBank')]")
	private WebElement removeStoredBankLink;

	@FindBy(xpath = "//i[contains(@class,'fa-trash') and contains(@ng-class,'isDelete')]")
	private List<WebElement> removeStoredCardIcon;

	@FindBy(id = "storedCard")
	private WebElement removeStoredCardConf;

	@FindBy(id = "storedBank")
	private WebElement removeStoredBankConf;

	@FindBy(css = "div#bankaccount_add span.sprite.mob_next-cross-icon")
	private WebElement removeCheckingAccountOptionMobile;

	@FindBy(css = "span.padding-top-xsmall.invalid.text-error")
	private List<WebElement> errorTextForCard;

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button[class*='float-lg-right']") })
	private WebElement backBtn;

	@FindBy(css = "input[ng-click*='storedBank']")
	private List<WebElement> storedBankAccountRadioBtn;

	@FindBy(css = "input[name='selector']")
	private List<WebElement> paymentMethodRadioBtns;

	@FindAll({ @FindBy(xpath = "//div[contains(@ng-if,'showChoosePaymentLaterBlade')]//span"),
			@FindBy(xpath = "//p[contains(text(),'payment method later')]//../..//label") })
	private WebElement choosePaymentMethodLater;

	@FindBy(css = "button.btn.btn-secondary.SecondaryCTA")
	private WebElement cancelBtnAmountSpokePage;

	@FindBy(css = "span.invalid.text-error")
	private List<WebElement> errorMsgs;

	@FindAll({ @FindBy(css = "p.legal.color-red.text-left.errormessage"),
			@FindBy(xpath = "//div[contains(text(),'Enter a valid card number.')]") })
	private WebElement cardNumberError;

	@FindBy(css = "div.label-copy span.sprite")
	private List<WebElement> storedPaymentTypes;

	@FindAll({ @FindBy(css = "div.col-xs-3 p.card-blade-title span"), @FindBy(css = "div.col-4 p.Display6 span") })
	private List<WebElement> storedPaymentNos;

	/*
	 * @FindBy(css = "div.pull-right.show span.pull-left button.miniCTA") private
	 * WebElement removeYesBtn;
	 */
	@FindBy(xpath = "//button[text()='Yes']")
	private WebElement removeYesBtn;

	@FindBy(css = "div.pull-right.show span button.miniCTA_black")
	private WebElement removeNoBtn;

	/*
	 * @FindAll({ @FindBy(xpath =
	 * "//div[contains(@class,'padding-vertical-large')]//i[contains(@class,'fa-trash')"
	 * ), @FindBy(xpath = "//img[@class='cursor-pointer']")}) private
	 * List<WebElement> removeStoredPaymentBtns;
	 */

	/*
	 * @FindBy(xpath =
	 * "//div[contains(@class,'padding-vertical-large')]//i[contains(@class,'fa-trash')")
	 * private List<WebElement> removeStoredPaymentBtns;
	 */

	@FindBy(xpath = "//div/span[contains(text(),'Add Bank')]")
	private WebElement addBankBlade;

	@FindBy(css = "div#bank")
	private WebElement addBankpaymentBlade;

	@FindAll({ @FindBy(xpath = "//div/span[contains(text(),'Add Card')]"), @FindBy(css = "div#card") })
	private WebElement addCardBlade;

	@FindAll({ @FindBy(css = "div#continue button"), @FindBy(css = "button[class*='float-lg-left']") })
	private WebElement continueAddCard;

	@FindBy(css = "p.text-error.fine-print-body.padding-top-xsmall.pWidth.ng-binding")
	private WebElement invalidBankAccMsg;

	@FindBy(css = "span.padding-horizontal-xsmall.body.mt-lg-1")
	private WebElement invalidCardMsg;

	@FindBy(css = "p.text-error.fine-print-body")
	private List<WebElement> prohibitedPayments;

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindAll({ @FindBy(css = "div.payment-content"), @FindBy(css = "p>span[role=heading]"),
			@FindBy(css = "div.Display4"), @FindBy(css = "div.paymentProcessing") })
	private WebElement pageLoadElement;

	@FindBy(css = "img.cursor-pointer.deleteIconImg")
	private List<WebElement> removeStoredPaymentBtns;

	@FindBy(xpath = "//div/span[contains(text(),'In Session')]")
	private WebElement insessionPayment;

	@FindBy(xpath = "//button[text()='Continue ']")
	private WebElement Continuebutton;

	@FindBy(xpath = "//div[contains(@ng-repeat,'storedPaymentMethods')]")
	private List<WebElement> allpaymentmenthodcheckboxes;

	@FindBy(xpath = "//span[text()= 'In use']")
	private WebElement inUseBlade;

	@FindBy(css = "div.d-flex span.padding-horizontal-xsmall")
	private WebElement paymentVerificationMsgAngular;

	@FindBy(css = "div.d-flex span.padding-horizontal-xsmall")
	private WebElement disabledPaymentMethodChevronAngular;

	@FindBy(css = "span.Display6.disable-div")
	private WebElement disabledStoredPaymentAngular;

	@FindAll({ @FindBy(css = "p.card-blade-title.ng-scope>span.ng-binding"),
			@FindBy(css = "p.card-blade-title>span[pid*=cust_nickname]"),
			@FindBy(css = "p.Display6>span[pid*=cust_nickname]"), @FindBy(css = "span.Display6") })
	private WebElement nameAngular;

	@FindAll({ @FindBy(xpath = "//span[contains(text(),'Default')]"),
			@FindBy(xpath = "//p[contains(text(),'Default')]") })
	private WebElement defaultLabelAngular;

	@FindAll({ @FindBy(xpath = "//span[contains(text(),'/')]"), @FindBy(css = "p.d-md-none") })
	private WebElement expirationDateAngular;

	@FindBy(css = "span.arrow-right")
	private WebElement storedPaymentMethodChevronAngular;

	@FindBy(css = "p.legal.text-red")
	private WebElement paymentVerificationMsg;

	@FindBy(xpath = "//p[contains(@class,'text-red')]//..//..//..//i")
	private WebElement disabledPaymentMethodChevron;

	@FindBy(css = "input[disabled='disabled'] + label")
	private WebElement disabledStoredPayment;

	@FindAll({
			@FindBy(css = "div[style='display:grid'] span.body-copy-description.font-size-bodycopy.ng-binding.black"),
			@FindBy(css = "") })
	private WebElement name;

	@FindAll({ @FindBy(xpath = "//p[contains(text(),'Default')]"),
			@FindBy(xpath = "//span[contains(text(),'Default')]") })
	private WebElement defaultLabel;

	@FindAll({ @FindBy(xpath = "//span[contains(text(),'/')]"), @FindBy(css = "p.d-md-none") })
	private WebElement expirationDate;

	@FindAll({ @FindBy(css = "span.Display6.disable-div+span+span"),
			@FindBy(css = "span[ng-if='paymentInstrument.expirationStatus']"),
			@FindBy(css = "span.card-blade-error-message"), @FindBy(css = "p.d-md-none"),
			@FindBy(css = "span.d-md-block>span") })
	private List<WebElement> expirationStatus;

	@FindBy(css = "i.fa-rightIcon")
	private WebElement storedPaymentMethodChevron;

	@FindAll({ @FindBy(css = "span#moreInfoLink>span"), @FindBy(linkText = "showNudge") })
	private WebElement showNudgelink;

	@FindAll({ @FindBy(css = "div#goToWalletModal"), @FindBy(css = "div.dialog.modal_landscapeht") })
	private WebElement nudgeModalPopUp;

	@FindAll({ @FindBy(xpath = "//span[contains(text(),'New:')]"),
			@FindBy(css = "div.main-content>div.TMO-RADIO-PAYMENT-BLADE-PAGE span.d-inline-block.align-top") })
	private WebElement msgOnNudge;

	@FindAll({ @FindBy(css = "p.ellipsis-text>span"),
			@FindBy(css = "div.main-content>div.TMO-RADIO-PAYMENT-BLADE-PAGE") })
	private WebElement nudge;

	@FindBy(css = "div.modal-button-div-xs button.PrimaryCTA-accent")
	private WebElement closeBtnOnNudgeModal;

	@FindBy(css = "")
	private WebElement linkOnGreyedBlade;

	@FindBy(css = "[ng-if='$ctrl.isWalletFull'] span")
	private WebElement walletFullMsg;

	@FindBy(css = "div.black.text-center span")
	private WebElement walletFullMsgAngular;

	@FindBy(css = "div.glueRadio-container")
	private WebElement savedPayments;

	@FindAll({ @FindBy(css = "p.card-blade-title.ng-scope>span.ng-binding"),
			@FindBy(css = "p.card-blade-title>span[pid*=cust_nickname]"),
			@FindBy(css = "p.Display6>span[pid*=cust_nickname]") })
	private List<WebElement> nickName;

	@FindAll({ @FindBy(xpath = "//span[contains(text(),'Confirm/Delete')]"),
			@FindBy(xpath = "//span/a[contains(text(),'Confirm/Delete')]"),
			@FindBy(xpath = "//p[@class='edit-link']/span"), @FindBy(xpath = "//p/span[@role='link']") })
	private List<WebElement> ConfirmorDeleteorEditLink;

	@FindAll({ @FindBy(xpath = "//span[@class='ng-binding' and contains(text(),'****')]"),
			@FindBy(xpath = "//span[contains(@pid,'cust_crd') and contains(text(),'****')]") })
	private List<WebElement> accountNumberLastFourDigits;

	@FindAll({ @FindBy(xpath = "//span[contains(@class,'Bank')]/../../div/p/span[@pid]"),
			@FindBy(xpath = "//span[contains(@aria-label,'Bank')]/../../../div/p/span[@pid]") })
	private WebElement storedBankNumberAngular;

	@FindBy(xpath = "//span[contains(@class,'check')]/../..//div/p[@class='card-blade-title']/span")
	private WebElement storedBankNumber;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public SpokePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public SpokePage verifyPageUrl() {
		if (getDriver().getCurrentUrl().contains(pageUrl) || getDriver().getCurrentUrl().contains(newPageUrl)) {
			Reporter.log("Navigated to " + getDriver().getCurrentUrl() + "");
		}
		waitFor(ExpectedConditions.or(ExpectedConditions.urlContains(pageUrl),
				ExpectedConditions.urlContains(newPageUrl)));
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public SpokePage verifyNewPageUrl() {
		waitFor(ExpectedConditions.urlContains(newPageUrl));
		return this;
	}

	/**
	 * Verify Payment Spoke Page
	 * 
	 * @return
	 */
	public SpokePage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement), 5);
			Reporter.log("Payment spoke page is loaded");
		} catch (Exception e) {
			Assert.fail("Failed to load payment spoke page");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public SpokePage verifyNewPageLoaded() {
		try {
			checkPageIsReady();
			// waitforSpinner();
			verifyNewPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			// pageHeader.isDisplayed();
			Reporter.log("Payment spoke page is loaded and header is verified ");
		} catch (Exception e) {
			Assert.fail("Failed to load payment spoke page");
		}
		return this;
	}

	/**
	 * 
	 * click on add bank
	 */
	public SpokePage clickAddBank() {
		try {
				addBank.click();
				Reporter.log("Clicked on add bank");
		} catch (Exception e) {
			Assert.fail("Add Bank Blade is disabled | Msisdn is Not eligible for Bank payments");
		}
		return this;
	}

	/**
	 * 
	 * verify add bank blade is displayed or not
	 */
	public SpokePage verifyAddBankBlade() {
		try {
			addBank.isDisplayed();
			Reporter.log("add a bank blade is diaplayed");
		} catch (Exception e) {
			Assert.fail("failed to verify add a bank blade");
		}
		return this;
	}

	/**
	 * 
	 * verify add card blade is displayed or not
	 */
	public SpokePage verifyAddCardBlade() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addCard));
			addCard.isDisplayed();
			Reporter.log("add a card blade is diaplayed");
		} catch (Exception e) {
			Assert.fail("failed to verify add a card blade");
		}
		return this;
	}

	/**
	 * click on add card
	 */
	public SpokePage clickAddCard() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(addCard));
			clickElementWithJavaScript(addCard);
			Reporter.log("Add card button clicked");
		} catch (Exception e) {
			Assert.fail("Add card not found");
		}
		return this;
	}

	/***
	 * Select stored card
	 * 
	 * @return
	 */

	public SpokePage selectStoredCard() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				waitFor(ExpectedConditions.elementToBeClickable(storedCardRadioBtnMobile));
				String storedCardAttr = storedCardMobileAttr.getAttribute("checked");
				if (storedCardAttr == null)
					storedCardRadioBtnMobile.click();
			} else {
				if (!storedCardRadioBtn.get(0).isSelected()) {
					storedCardRadioBtn.get(0).click();
				}
			}
			Reporter.log("Stored card radio button selected");
		} catch (Exception e) {
			Assert.fail("Stored card component missing");
		}
		return this;
	}

	/**
	 * verify if stored card payments radio button is disabled
	 * 
	 * @return true/false
	 */
	public SpokePage isStoredCardEnabled() {
		try {
			storedCardRadioBtn.get(0).isEnabled();
			Reporter.log("Stored card enabled");
		} catch (Exception e) {
			Assert.fail("Stored card component not found");
		}
		return this;
	}

	/**
	 * verify if stored card payments radio button is disabled
	 * 
	 * @return true/false
	 */
	public SpokePage isStoredCheckingAccountEnabled() {
		try {
			storedBankRadioBtn.get(0).isEnabled();
			Reporter.log("Stored checking account enabled");
		} catch (Exception e) {
			Assert.fail("Stored card radio button not found");
		}
		return this;
	}

	/**
	 * select stored checking account
	 */
	/*
	 * public void selectStoredCheckingAccount() {
	 * if(!storedBankRadioBtn.get(0).isSelected()){
	 * clickElement(storedBankRadioBtn); }
	 * 
	 * }
	 */

	public SpokePage selectStoredCheckingAccount() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				waitFor(ExpectedConditions.elementToBeClickable(storedBankRadioBtnMobile));
				try {
					storedBankRadioBtnMobile.getAttribute("checked").equals("checked");
				} catch (Exception e) {
					storedBankRadioBtnMobile.click();
				}

			} else {
				if (!storedBankRadioBtn.get(0).isSelected()) {
					storedBankRadioBtn.get(0).click();
				}
			}
		} catch (Exception e) {
			Assert.fail("Stored checking account component missing");
		}
		return this;
	}

	/**
	 * verify remove link for stored checking account
	 * 
	 * @return boolean
	 */
	public SpokePage verifyRemovecheckingAccountlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(removeCheckingAccountOption));
			removeCheckingAccountOption.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Remove checking account component missing");
		}
		return this;
	}

	/**
	 * verify remove link for stored checking account
	 * 
	 * @return boolean
	 */
	public SpokePage removeStoredBankAccount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(removeCheckingAccountOption));
			removeCheckingAccountOption.isDisplayed();
			removeCheckingAccountOption.click();
			waitFor(ExpectedConditions.visibilityOf(removeYesBtn));
			removeYesBtn.click();
			Reporter.log("Removed Stored payment type: Bank");
		} catch (Exception e) {
			Assert.fail("Remove checking account component missing");
		}
		return this;
	}

	/**
	 * verify error messages for empty fields
	 * 
	 * @param message
	 * @return boolean
	 */
	public SpokePage verifyErrorTextMessage(String message) {
		try {
			for (WebElement error : errorTextForCard) {
				if (error.isDisplayed() && error.getText().contains(message)) {
					Reporter.log("Error Text message verified: " + message);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Error text message component not found");
		}
		return this;
	}

	/**
	 * click add bank icon
	 */
	public SpokePage clickBackButton() {
		try {
			backBtn.click();
			Reporter.log("Clicked on button");
		} catch (Exception e) {
			Assert.fail("Back button not found");
		}
		return this;
	}

	/**
	 * Verify Stored Card Remove link
	 * 
	 * @return boolean
	 */
	public SpokePage verifyRemoveStoredCardLink() {
		try {
			checkPageIsReady();
			for (WebElement element : removeStoredCardLink) {

				if (element.isDisplayed()) {
					Reporter.log("Remove Stored Card button is verified");
					break;

				}

			}

		} catch (Exception e) {
			Verify.fail("Remove stored card link missing");
		}
		return this;
	}

	public SpokePage clickRemoveStoredBankLink() {
		checkPageIsReady();
		try {
			clickElementWithJavaScript(removeStoredBankLink);
			Reporter.log("Clicked on Remove Stored Card Link.");
		} catch (NoSuchElementException elementNotFound) {
			Reporter.log("Remove Stored Card Link not found");
		}
		return this;
	}

	/**
	 * verify if stored bank account payment radio button is disabled
	 * 
	 * @return true/false
	 */
	public SpokePage isStoredBankAccountEnabled() {
		try {
			storedBankAccountRadioBtn.get(0).isEnabled();
			Verify.fail("Stored bank account component missing");
			Reporter.log("Stored bank account enabled");
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Verify Expiration date Error Msg
	 * 
	 * @return true/false
	 */
	public SpokePage verifyExpirationdateErrorMsg(String errorMsg) {
		try {
			errorMsgs.get(0).getText().contains(errorMsg);
			Reporter.log("Error message exists");
		} catch (Exception e) {
			Assert.fail("Expiration error message component not found");
		}
		return this;
	}

	/**
	 * verify invalid CC error msg for autopay
	 * 
	 * @return boolean
	 */
	public SpokePage verifyInvalidCCErrorMsgAutoPay() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cardNumberError));
			cardNumberError.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Continue CTA component missing");
		}
		return this;
	}

	/**
	 * generate a unique amount value
	 * 
	 * @return double - amount
	 */
	private SpokePage generateRandomAccNumber() {
		try {
			Integer min = 1000;
			Integer max = 9999;
			Random r = new Random();
			int randomNumber = r.nextInt() * (max - min) + min;
			System.out.println("random number : " + randomNumber);
		} catch (Exception e) {
			Assert.fail("Continue CTA component missing");
		}
		return this;
	}

	public String getStoredCardNumber(int count) {
		String storedPaymentNumber = null;
		try {
			storedCards.get(count).isDisplayed();

			storedPaymentNumber = storedCards.get(count).getText().replace("*", "");

		} catch (NoSuchElementException elementNotFound) {
			Assert.fail("failed to get stored card number");
		}

		return storedPaymentNumber;
	}

	public String getStoredBankNumber(int count) {
		String storedPaymentNumber = null;
		try {
			storedBanks.get(count).isDisplayed();

			storedPaymentNumber = storedBanks.get(count).getText().replace("*", "");

		} catch (NoSuchElementException elementNotFound) {
			Assert.fail("failed to get stored bank number");
		}

		return storedPaymentNumber;
	}

	/**
	 * verify stored payment types are displayed
	 * 
	 * @return
	 */
	public SpokePage verifyStoredPaymentType() {
		try {
			for (WebElement webElement : storedPaymentTypes) {
				if (webElement.isDisplayed())
					Reporter.log("Stored payment type is displayed");
			}
		} catch (Exception e) {
			Assert.fail("No Stored payment types are displayed");
		}
		return this;
	}

	/**
	 * verify Back CTA is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyBackCTA() {
		try {
			backBtn.isDisplayed();
			Reporter.log("Verified BackCTA");
		} catch (Exception e) {
			Assert.fail("Continue CTA component missing");
		}
		return this;
	}

	/**
	 * verify delete link is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyDeleteLink(int i) {
		try {
			deleteLinks.get(i).isDisplayed();
			Reporter.log("Verified delete link");
		} catch (Exception e) {
			Assert.fail("failed to verify delete link");
		}
		return this;
	}

	/**
	 * verify payment number is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyPaymentNumber(int i) {
		try {
			paymentNumber.get(i).isDisplayed();
			Reporter.log("Verified payment number");
		} catch (Exception e) {
			Assert.fail("failed to verify payment number");
		}
		return this;
	}

	/**
	 * verify radio button is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyRadioBtn(int i) {
		try {
			radioBtn.get(i).isDisplayed();
			Reporter.log("Verified radio button");
		} catch (Exception e) {
			Assert.fail("failed to verify radio button");
		}
		return this;
	}

	/**
	 * verify payment method icon is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyCardAndBankIcon(int i) {
		try {
			paymentMethodIcon.get(i).isDisplayed();
			Reporter.log("Verified payment method icon");
		} catch (Exception e) {
			Assert.fail("failed to verify payment method icon");
		}
		return this;
	}

	/**
	 * verify payment method blade elements is displayed or not
	 * 
	 * @return boolean
	 */
	public SpokePage verifyPaymentMethodBladeElements() {
		try {
			for (int i = 0; i < paymentMethodBlades.size(); i++) {

				verifyPaymentNumber(i);
				verifyDeleteLink(i);
				verifyCardAndBankIcon(i);
				verifyRadioBtn(i);
			}

		} catch (Exception e) {
			Assert.fail("failed to verify payment method icon");
		}
		return this;
	}

	/**
	 * remove all saved payments
	 * 
	 * @param paymentType
	 */
	public SpokePage verifyAndDeleteAllOtherStoredPayments(String paymentType) {
		try {
			String xpath = "//span[contains(@class,'" + paymentType
					+ "')]//..//..//..//..//..//..//div[contains(@class,'padding-vertical-large')]//i[contains(@class,'fa-trash')]";
			List<WebElement> removeStoredPaymentBtns = getDriver().findElements(By.xpath(xpath));
			for (WebElement removeBtn : removeStoredPaymentBtns) {
				if (removeBtn.isDisplayed()) {
					removeBtn.click();
					waitFor(ExpectedConditions.visibilityOf(removeYesBtn));
					removeYesBtn.click();
					waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
					waitforSpinner();
				}
			}
		} catch (Exception e) {
			Assert.fail("Continue CTA component missing");
		}
		return this;
	}

	/**
	 * remove all saved payments
	 *
	 */
	public SpokePage verifyRemovingStoredPayments() {
		try {
			for (WebElement removeBtn : removeStoredPaymentBtns) {
				if (removeBtn.isDisplayed()) {
					removeBtn.click();
					waitFor(ExpectedConditions.visibilityOf(removeYesBtn));
					removeYesBtn.isDisplayed();
					removeNoBtn.isDisplayed();
				}
			}
		} catch (Exception e) {
			Assert.fail("Remove buttons component missing.");
		}
		return this;
	}

	public SpokePage verifyAddBankDisabled() {
		try {
			addBankpaymentBlade.getAttribute("class").contains("backgroundDisabled");
			Reporter.log("Verified add bank disabled");
		} catch (Exception e) {
			Assert.fail("Add bank component missing");
		}
		return this;
	}

	public SpokePage verifyAddCardDisabled() {
		try {
			addCardBlade.getAttribute("class").contains("disable-div");
			Reporter.log("Verified add Card disabled");
		} catch (Exception e) {
			Assert.fail("Add card component missing");
		}
		return this;
	}

	public SpokePage verifyInvalidBankErrorMsg() {
		try {
			Assert.assertTrue(invalidBankAccMsg.isDisplayed(), "Invalid Bank message is not displayed");
			Assert.assertTrue(
					invalidBankAccMsg.getText()
							.equals("We are unable to accept bank account payments on your account."),
					"Invalid Bank message link text is incorrect");
			Reporter.log("Verified Invalid Bank Error message");
		} catch (Exception e) {
			Assert.fail("Invalid bank message component missing");
		}
		return this;
	}

	public SpokePage verifyInvalidCardErrorMsg() {
		try {
			invalidCardMsg.isDisplayed();
			invalidCardMsg.getText().equals("We cannot accept card payments on your account");
			Reporter.log("Verified invalid card error message");
		} catch (Exception e) {
			Assert.fail("Invalid card message component missing");
		}
		return this;
	}

	/**
	 * click on stored card Delete link
	 * 
	 * 
	 */
	public SpokePage clickOnStoredCardDeleteLink(int count) {
		try {
			storedCardsDeleteLinks.get(count).isDisplayed();
			storedCardsDeleteLinks.get(count).click();
			Reporter.log("clicked on stored card delete link");
		} catch (Exception e) {
			Assert.fail("failed to click stored card delete link");
		}
		return this;
	}

	/**
	 * click on stored bank Delete link
	 * 
	 * 
	 */
	public SpokePage clickOnStoredBankDeleteLink(int count) {
		try {
			storedBanksDeleteLinks.get(count).isDisplayed();
			storedBanksDeleteLinks.get(count).click();
			Reporter.log("clicked on stored bank delete link");
		} catch (Exception e) {
			Assert.fail("failed to click stored bank delete link");
		}
		return this;
	}

	/**
	 * click on Delete link
	 * 
	 * 
	 */
	public SpokePage clickDeleteLinkAndVerifyModel(String header, String subHeader) {
		try {
			for (WebElement deleteLink : deleteLinks) {
				deleteLink.isDisplayed();
				deleteLink.click();
				verifyDeletModelSubHeader(subHeader);

				verifyDeletModelYesCTA();
				verifyDeleteModelNoCTA();
				clickDeleteModelNoCTA();
			}
			Reporter.log("clicked on  delete link");
		} catch (Exception e) {
			Assert.fail("failed to click  delete link");
		}
		return this;
	}

	/**
	 * click continue button for add Card
	 */
	public SpokePage clickContinueButton() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("di_modal_message_loading")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
			waitFor(ExpectedConditions.elementToBeClickable(continueAddCard));
			clickElementWithJavaScript(continueAddCard);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("di_modal_message_loading")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
		} catch (Exception e) {
			Assert.fail("Add card continue button missing");
		}
		return this;
	}

	/**
	 * verify Continue CTA is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyContinueCTA() {
		try {
			continueAddCard.isDisplayed();
			Reporter.log("Contact CTA displayed");
		} catch (Exception e) {
			Assert.fail("Continue CTA component missing");
		}
		return this;
	}

	/**
	 * verify model yes CTA is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyDeletModelYesCTA() {
		try {
			deleteModelYesBtn.isDisplayed();
			Reporter.log("yes CTA displayed");
		} catch (Exception e) {
			Assert.fail("failed to found yes button");
		}
		return this;
	}

	/**
	 * verify model no CTA is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyDeleteModelNoCTA() {
		try {
			deleteModelNoBtn.isDisplayed();
			Reporter.log("no CTA displayed");
		} catch (Exception e) {
			Assert.fail("failed to found NO button");
		}
		return this;
	}

	/**
	 * verify click on NO CTA
	 * 
	 * @return boolean
	 */
	public SpokePage clickDeleteModelNoCTA() {
		try {
			deleteModelNoBtn.click();
			Reporter.log("clicked on NO CTA");
		} catch (Exception e) {
			Assert.fail("failed to found NO CTA");
		}
		return this;
	}

	/**
	 * verify Bank delete model header is displayed or not
	 * 
	 * @return boolean
	 */
	public SpokePage verifyBankDeletModelHeader(String bankHeader) {
		try {
			deleteBankModelHeader.isDisplayed();
			Assert.assertTrue(deleteBankModelHeader.getText().equals(bankHeader),
					"failed to verify delete bank model header");
			Reporter.log("delete bank model header is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify Bank model header");
		}
		return this;
	}

	/**
	 * verify card delete model header is displayed or not
	 * 
	 * @return boolean
	 */
	public SpokePage verifyCardDeletModelHeader(String cardHeader) {
		try {
			deleteCardModelHeader.isDisplayed();
			Assert.assertTrue(deleteCardModelHeader.getText().equals(cardHeader),
					"failed to verify delete card model header");
			Reporter.log("delete card model header is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify card model header");
		}
		return this;
	}

	/**
	 * verify model sub header is displayed or not
	 * 
	 * @return boolean
	 */
	public SpokePage verifyDeletModelSubHeader(String subHeader) {
		try {
			deleteModelSubHeader.isDisplayed();
			Assert.assertTrue(deleteModelSubHeader.getText().equals(subHeader),
					"failed to verify delete bank model header");
			Reporter.log("delete bank model header is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify Bank model header");
		}
		return this;
	}

	public SpokePage clickDeletLinkAndVerifyBankDeleteModel(String bankHeader, String subHeader) {

		try {
			for (int i = 0; i < storedBanks.size(); i++) {
				String storedBankNum = getStoredBankNumber(i);
				clickOnStoredBankDeleteLink(i);
				verifyBankDeletModelHeader(bankHeader);
				verifyDeletModelSubHeader(subHeader);
				verifyCardNumberOnModel(storedBankNum);
				verifyDeletModelYesCTA();
				verifyDeleteModelNoCTA();
				clickDeleteModelNoCTA();
				Reporter.log("bank delete model is displayed");
			}

		} catch (Exception e) {
			Assert.fail("failed to verify bank delete model");

		}
		return this;
	}

	public SpokePage clickDeletLinkAndVerifyCardDeleteModel(String cardHeader, String subHeader) {

		try {
			for (int i = 0; i < storedCards.size(); i++) {
				String storedCardNum = getStoredCardNumber(i);
				clickOnStoredCardDeleteLink(i);
				verifyCardDeletModelHeader(cardHeader);
				verifyDeletModelSubHeader(subHeader);
				verifyCardNumberOnModel(storedCardNum);
				verifyDeletModelYesCTA();
				verifyDeleteModelNoCTA();
				clickDeleteModelNoCTA();
				Reporter.log("card delete model is displayed");
			}

		} catch (Exception e) {
			Assert.fail("failed to verify card delete model");

		}
		return this;
	}

	/**
	 * verify card number is displayed on model or not
	 * 
	 * @return boolean
	 */
	public SpokePage verifyCardNumberOnModel(String cardNumber) {
		try {
			cardNumberOnModel.isDisplayed();
			Assert.assertTrue(cardNumberOnModel.getText().contains(cardNumber), "failed to verify card number");
			Reporter.log("card number is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify card number");
		}
		return this;
	}

	/**
	 * verify page header is displayed
	 * 
	 * @return boolean
	 */
	public SpokePage verifyPageHeader() {
		try {
			pageHeader.isDisplayed();
			Assert.assertTrue(pageHeader.getText().equals("Edit payment method"), "page header is not displayed");
			Reporter.log("page header is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify page header");
		}
		return this;
	}

	/*
	 * To verify the count of stored payment remove buttons In put parameter
	 * :Expected number of remove buttons
	 */

	public SpokePage verifyRemovebuttonscount(int btnRMVcnt) {
		try {
			if (removeStoredPaymentBtns.size() == btnRMVcnt)
				Reporter.log("Stored payment are:" + btnRMVcnt);
			else
				Assert.fail("Test script is not matching with pre condition stored payments are"
						+ removeStoredPaymentBtns.size());

		} catch (Exception e) {
			Assert.fail("Stored payment remove buttons are not found");
		}
		return this;
	}

	public int gettotalremovebuttons() {
		try {
			return removeStoredPaymentBtns.size();

		} catch (Exception e) {
			Assert.fail("Stored payment remove buttons are not found");
		}
		return 0;
	}

	public SpokePage clickonselectedremovebutton(WebElement ele) {
		try {
			ele.click();
			Reporter.log("remove button clicked");
		} catch (Exception e) {
			Assert.fail("Stored payment remove buttons are not found");
		}
		return this;
	}

	public SpokePage verifyOneradioBtnIsSelected() {
		try {

			int count = 0;
			for (WebElement radioBtnn : radioBtn) {
				if (radioBtnn.isSelected()) {
					count++;
				}
			}
			if (count > 2) {
				Assert.fail("more than one toggle button is selected");
			}
			Reporter.log("one radio button is selected");
		} catch (Exception e) {
			Assert.fail("Stored payment remove buttons are not found");
		}
		return this;
	}

	public SpokePage clickcorrespondingYesbutton(WebElement ele) {
		try {
			ele.findElement(
					By.xpath("./ancestor::div[contains(@ng-repeat,'storedPaymentMethods')]//button[text()='Yes']"))
					.click();
			Reporter.log("Yes button clicked");
		} catch (Exception e) {
			Assert.fail("Stored payment remove buttons are not found");
		}
		return this;
	}

	public WebElement identifypaymentmethodtoberemoved() {
		try {

			return removeStoredPaymentBtns.get(0);

		} catch (Exception e) {
			Assert.fail("Stored payment remove buttons are not found");
		}
		return null;
	}

	/**
	 * verify if the stored card is prohibited for payment
	 * 
	 * @return true/false
	 */
	public SpokePage verifyProhibitedCardPayments() {
		try {
			for (WebElement inlinePaymentError : prohibitedPayments) {

				if (inlinePaymentError.isDisplayed()
						&& (inlinePaymentError.getText().equals(Constants.INLINE_PAYMENT_ERRORS)
								|| inlinePaymentError.getText().equals(Constants.INLINE_CARD_PAYMENT_ERRORS))) {
					Reporter.log("Verified Prohibited Card payments");
				}
			}
		} catch (Exception e) {
			Verify.fail("Prohibited payments not found");
		}
		return this;
	}

	public SpokePage verifyInsessionPayment() {
		try {
			waitFor(ExpectedConditions.visibilityOf(insessionPayment));
			Reporter.log("In Session payment is selected");
		} catch (Exception e) {
			Assert.fail("In Session payment is not selected");
		}
		return this;
	}

	public SpokePage checkdisabledpaymentmethods(ArrayList<String> text) {
		try {

			for (WebElement chkbox : allpaymentmenthodcheckboxes) {
				if (!chkbox.findElement(By.tagName("input")).isEnabled()) {
					if (chkbox.getText().contains(text.get(0)) || chkbox.getText().contains(text.get(1)))
						Reporter.log("Error text is displaying for expired payment options");
					else
						Assert.fail("Error text is not displaying for expired payment options");
				} else {

					if (chkbox.getText().contains(text.get(0)) || chkbox.getText().contains(text.get(1)))
						Assert.fail("Error text is displaying for Active payment options");
					else
						Reporter.log("Error text is not displaying for Active payment options");

				}
			}

		} catch (Exception e) {
			Assert.fail("Stored payment remove buttons are not found");
		}
		return this;
	}

	public void choosePaymentMethodLater() {
		try {
			choosePaymentMethodLater.click();
			Reporter.log("Choose payment method later radio button is selected");
		} catch (Exception e) {
			Assert.fail("Failed to modify payment method in PA");
		}
	}

	/**
	 * This method is to verify PII masking is done for all payment information
	 * 
	 * @param custPaymentPID
	 */
	public void verifyPIIMasking(String custPaymentPID) {
		Boolean isMasked = false;
		try {
			if (!storedPaymentNos.isEmpty()) {
				for (WebElement payment : storedPaymentNos) {
					if (payment.isDisplayed()) {
						isMasked = checkElementisPIIMasked(payment, custPaymentPID);
						if (!isMasked)
							break;
					}
				}
				Reporter.log("Verified PII masking for Stored Payment method");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Verify: PII masking for Stored Payment method");
		}
	}

	public void verifyDefaultSelectionofCardorBankInspokePage(String paymentNum) {
		try {
			for (int i = 0; i < paymentMethodBlades.size(); i++) {
				if (radioBtn.get(i).isSelected()) {
					Assert.assertTrue(paymentNumber.get(i).getText().contains(paymentNum));

					break;
				}

			}

		} catch (Exception e) {
			Assert.fail("Failed to Verify card selected default in spoke page");

		}

	}

	public void clickRemoveStoredCardIcon() {
		try {
			checkPageIsReady();
			for (WebElement element : removeStoredCardIcon) {
				if (element.isDisplayed()) {
					clickElementWithJavaScript(element);
				}
			}
			// removeStoredCardIcon.click();
			Reporter.log("RemoveStoredCardIcon is selected");

		} catch (Exception e) {
			Verify.fail("Failed to click RemoveStoredCardIcon");
		}
	}

	public void clickInSessionBlade() {
		try {
			checkPageIsReady();
			inUseBlade.click();
			Reporter.log("In Use blade is clicked");

		} catch (Exception e) {
			Verify.fail("Failed to click In use blade");
		}
	}

	/**
	 * Verify invalid error message on BAN
	 * 
	 * @return
	 */
	public SpokePage verifyInvalidErrorMsgOnBAN() {
		try {
			invalidCardMsg.isDisplayed();
			invalidCardMsg.getText().equals("We cannot accept online payments for your account");
			Reporter.log("Verified invalid card error message");
		} catch (Exception e) {
			Assert.fail("Invalid card message component missing");
		}
		return this;
	}

	public SpokePage Removestoredpaymentmethods() {
		try {
			for (WebElement removeBtn : removeStoredPaymentBtns) {
				if (removeBtn.isDisplayed()) {
					removeBtn.click();
					waitFor(ExpectedConditions.visibilityOf(removeYesBtn));
					removeYesBtn.click();

				}
			}
		} catch (Exception e) {
			Assert.fail("Remove buttons component missing.");
		}
		return this;
	}

	public SpokePage ClickContinuebutton() {
		try {
			int cnt = 0;
			while (!Continuebutton.isEnabled()) {
				Thread.sleep(1000);
				cnt++;
				if (cnt == 30)
					Assert.fail("Continue button is not enabled");
			}

			if (Continuebutton.isEnabled())
				Continuebutton.click();
			else
				Assert.fail("Continue button is not enabled");

		} catch (Exception e) {
			Assert.fail("Continue button is not existing");
		}
		return this;
	}

	public void verifyStoredPaymentMethodDisabledAngular() {
		try {
			disabledStoredPaymentAngular.isDisplayed();
			Assert.assertTrue(disabledStoredPaymentAngular.getAttribute("class").contains("disable"));
			Reporter.log("Verified Stored Payment method is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Stored Payment method is disabled");
		}
	}

	public void verifyPaymentVerificationmessageAngular() {
		try {
			paymentVerificationMsgAngular.isDisplayed();
			Assert.assertTrue(paymentVerificationMsgAngular.getText().equals("Re-enter payment info"));
			Reporter.log("Verified Payment verification message");
		} catch (Exception e) {
			Assert.fail("Failed to verify Payment verification message");
		}
	}

	public void verifyChevronForDisabledStoredPaymentMethodAngular() {
		try {
			disabledPaymentMethodChevronAngular.isDisplayed();
			Reporter.log("Verified chevron is displayed for disabledPaymentMethod");
		} catch (Exception e) {
			Assert.fail("Failed to verify Chevron");
		}
	}

	/**
	 * verify nickname, default, and expiration date on stored payment blade
	 */
	public void verifyModifiedStoredPaymentMethodBladeAngular() {
		try {

			nameAngular.isDisplayed();
			defaultLabelAngular.isDisplayed();
			defaultLabelAngular.isDisplayed();
			// storedPaymentMethodChevronAngular.isDisplayed();
			Reporter.log("Verified Modified Stored Payment Method Blade");
		} catch (Exception e) {
			Assert.fail("Failed to verify Modified Stored Payment Method Blade");
		}
	}

	public void verifyStoredPaymentMethodDisabled() {
		try {
			Assert.assertTrue(disabledStoredPayment.isDisplayed());
			Reporter.log("Verified Stored Payment method is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Stored Payment method is disabled");
		}
	}

	public void verifyPaymentVerificationmessage() {
		try {
			paymentVerificationMsg.isDisplayed();
			Assert.assertTrue(paymentVerificationMsg.getText().equals("Re-enter payment info"));
			Reporter.log("Verified Payment verification message");
		} catch (Exception e) {
			Assert.fail("Failed to verify Payment verification message");
		}
	}

	public void verifyChevronForDisabledStoredPaymentMethod() {
		try {
			disabledPaymentMethodChevron.isDisplayed();
			Reporter.log("Verified chevron is displayed for disabledPaymentMethod");
		} catch (Exception e) {
			Assert.fail("Failed to verify Chevron");
		}
	}

	/**
	 * verify nickname, default, and expiration date on stored payment blade
	 */
	public void verifyModifiedStoredPaymentMethodBlade() {
		try {
			for (WebElement Namedisplay : nickName) {
				Namedisplay.isDisplayed();
			}
			defaultLabel.isDisplayed();
			expirationDate.isDisplayed();
			// storedPaymentMethodChevron.isDisplayed();
			Reporter.log("Verified Modified Stored Payment Method Blade");
		} catch (Exception e) {
			Assert.fail("Failed to verify Modified Stored Payment Method Blade");
		}
	}

	/**
	 * Method to verify expiration warning messages are displayed on PCM spoke page
	 * blades
	 * 
	 * @param warningText
	 */
	public void verifyExpirationMsgStoredPaymentMethod(String warningText) {
		try {
			waitFor(ExpectedConditions.visibilityOf(addBank));
			for (WebElement status : expirationStatus) {
				if (status.getText().trim().contains(warningText)) {
					Assert.assertTrue(status.getText().trim().contains(warningText));
					Reporter.log(warningText + ": notification text is verified");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify " + warningText + " notification");
		}
	}

	/**
	 * verify nudge
	 */
	public void verifyNudge() {
		try {
			nudge.isDisplayed();
			Reporter.log("Nudge is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Nudge");
		}
	}

	/**
	 * verify and click link on nudge
	 */
	public void verifyandClickLinkOnNudge() {
		try {
			showNudgelink.isDisplayed();
			Reporter.log("Nudge link is displayed");
			showNudgelink.click();
			Reporter.log("Clicked on the link displayed in Nudge");
		} catch (Exception e) {
			Assert.fail("Failed to verify link on nudge");
		}
	}

	/**
	 * verify message displayed on nudge
	 */
	public void verifyMsgOnNudge() {
		try {
			msgOnNudge.isDisplayed();
			Reporter.log("verified message on nudge is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify message on nudge");
		}
	}

	/**
	 * verify modal pop up after clicking link on nudge
	 */
	public void verifyNudgeModalPopUp() {
		try {
			nudgeModalPopUp.isDisplayed();
			Reporter.log("Nudge Modal Pop Up is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify modal pop up that occurs after link click on nudge");
		}
	}

	/**
	 * verify Close CTA on modal after clicking link on nudge
	 */
	public void verifyCloseCTAOnNudgePopUp() {
		try {
			closeBtnOnNudgeModal.isDisplayed();
			Reporter.log("Verified Close button on nudge modal pop up is displayed");
			closeBtnOnNudgeModal.click();
			Reporter.log("Clicked Close button on nudge modal pop up");

		} catch (Exception e) {
			Assert.fail("Failed to verify Close CTA on modal pop up that occurs after link click on nudge");
		}
	}

	/**
	 * Getting Total No of Payment Methods
	 */

	public int gettotalPaymentbuttons() {
		try {
			return paymentNumber.size();
		} catch (Exception e) {
			Assert.fail("Payment Button are not displayed");
		}
		return paymentNumber.size();
	}

	/**
	 * verify text on stored payment blade
	 */
	public void verifyDefaultPaymentMethodAngular() {

		try {
			waitFor(ExpectedConditions.visibilityOf(addBank));
			waitFor(ExpectedConditions.visibilityOf(defaultLabelAngular));
			defaultLabelAngular.isDisplayed();
			Reporter.log("Verified Default for Modified or  Stored Payment Method Blade ");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Default' on Modified or Stored Payment Method Blade");
		}
	}

	/**
	 * click link on greyed out blade
	 */
	public void clickLinkOnGreyedOutBlade() {
		try {
			linkOnGreyedBlade.click();
			Reporter.log("CLicked on link of Greyed out blade 'Non Claimed payment method'");
		} catch (Exception e) {
			Assert.fail("Failed to click on link of Greyed out blade 'Non Claimed payment method'");
		}
	}

	/**
	 * click link on greyed out blade Angular
	 */
	public void clickLinkOnGreyedOutBladeAngular() {
		try {
			linkOnGreyedBlade.click();
			Reporter.log("CLicked on link of Greyed out blade 'Non Claimed payment method'");
		} catch (Exception e) {
			Assert.fail("Failed to click on link of Greyed out blade 'Non Claimed payment method'");
		}
	}

	/**
	 * verify wallet full message is displayed for 10 saved methods displayed
	 */
	public void verifyWalletFullMsg(String walletFullText) {
		try {
			waitFor(ExpectedConditions.visibilityOf(walletFullMsg));
			walletFullMsg.isDisplayed();
			Assert.assertTrue(walletFullMsg.getText().equals(walletFullText));
			Reporter.log("Wallet full message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify the wallet full message");
		}
	}

	/**
	 * Angular 6: verify wallet full message is displayed for 10 saved methods
	 * displayed
	 */
	public void verifyAngular6WalletFullMsg(String walletFullText) {
		try {
			waitFor(ExpectedConditions.visibilityOf(walletFullMsgAngular));
			walletFullMsgAngular.isDisplayed();
			Assert.assertTrue(walletFullMsgAngular.getText().equals(walletFullText));
			Reporter.log("Wallet full message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify the wallet full message");
		}
	}

	/**
	 * verify saved payments suppressed for Non Master user
	 */
	public void verifySavedPaymentsSuppressed(MyTmoData myTmoData) {
		try {

			if (!savedPayments.isDisplayed()) {
				Reporter.log("saved payments suppressed for" + myTmoData.getPayment().getUserType());
			} else {
				Assert.fail(
						"Failed to verify saved payments suppressed for Non Master user. saved payments should NOT be present for "
								+ myTmoData.getPayment().getUserType());
			}
		} catch (Exception e) {
			if (e.toString().contains("NoSuchElementException")) {
				Reporter.log(
						"saved payments suppressed for Non Master user for " + myTmoData.getPayment().getUserType());
			} else {
				Assert.fail(
						"Failed to verify saved payments suppressed for Non Master user. saved payments should NOT be present for "
								+ myTmoData.getPayment().getUserType());
			}
		}

	}

	/**
	 * Verify Saved Payment Method NickName.
	 */
	public boolean verifySavedPaymentMethodNickName(String NickName) {
		boolean bflag = false;

		try {
			for (WebElement nickNametag : nickName) {
				if (nickNametag.getText().contains(NickName)) {
					bflag = true;
					Reporter.log("Saved Payment Method nickName is displayed as " + nickNametag.getText());
				}
			}
		} catch (Exception e) {
			Verify.fail("Saved Payment Method Nick Name not displayed");
		}
		return bflag;
	}

	/**
	 * Click Saved Payment Method NickName.
	 */
	public SpokePage ClickSavedPaymentMethodNickName(String NickName) {
		try {
			for (WebElement nickNametag : nickName) {
				if (nickNametag.getText().contains(NickName)) {
					nickNametag.click();
					Reporter.log("Clicked Payment Method nickName is displayed as " + nickNametag.getText());
				}
			}
		} catch (Exception e) {
			Verify.fail("Saved Payment Method Nick Name not displayed");
		}
		return this;
	}

	/**
	 * Click Confirm or Delete Link by Sending AccountNo
	 */
	public SpokePage ClickConfirmorDeleteLink(String AccountNo) {
		try {
			waitFor(ExpectedConditions.visibilityOf(addBank));
			for (int i = 0; i <= accountNumberLastFourDigits.size() - 1; i++) {
				if (AccountNo.contains(accountNumberLastFourDigits.get(i).getText().replace("|", "").replace(" ", "")
						.substring(4, 8))) {
					ConfirmorDeleteorEditLink.get(i + 1).click();
					Reporter.log("Clicked on Confirm or Delete Link ");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Confirm or Delete Link not displayed");
		}
		return this;
	}

	/**
	 * Verify Saved Payment Method by AccountNo.
	 */
	public boolean verifySavedPaymentMethodAccountNo(String AccountNo) {
		boolean bflag = false;
		try {
			for (int i = 0; i <= accountNumberLastFourDigits.size() - 1; i++) {
				if (AccountNo.contains(accountNumberLastFourDigits.get(i).getText().replace("|", "").replace(" ", "")
						.substring(4, 8))) {
					bflag = true;
					Reporter.log("Account no is not deleted ");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Saved Payment Method Account No not found");
		}
		return bflag;
	}

	/**
	 * method to click any saved card for validating edit card page
	 */
	public String verifyAndClickStoredCardAngular() {
		try {
			checkPageIsReady();
			String storedCardNo = storedCardNoAngular.getText().replaceAll("[^\\d.]", "");
			// storedCardNumber.click();
			WebElement editLink = getDriver()
					.findElement(By.xpath("//span[contains(@class,'icon')]/../../div/p/span[@class='body-link ']"));
			editLink.click();
			Reporter.log("Clicked on Stored card number: " + storedCardNo);
			return storedCardNo;
		} catch (Exception e) {
			Assert.fail("Failed to click on saved card");
		}
		return null;
	}

	/**
	 * method to click any saved bank for validating edit card page
	 */
	public String verifyAndClickStoredBankAngular() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(storedBankNumberAngular));
			String storedBankNo = storedBankNumberAngular.getText();
			// storedBankNumber.click();
			WebElement editLink = getDriver().findElement(
					By.xpath("//span[contains(@aria-label,'Bank')]/../../div/p/span[@class='body-link ']"));
			waitFor(ExpectedConditions.visibilityOf(editLink));
			editLink.click();
			Reporter.log("Clicked on Stored Bank number: " + storedBankNo);
			return storedBankNo;
		} catch (Exception e) {
			Assert.fail("Failed to click on saved bank");
		}
		return null;
	}

	/**
	 * method to click any saved card for validating edit card page
	 */
	public String verifyAndClickStoredCard() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(storedCardNumber));
			String storedCardNo = storedCardNumber.getText().replaceAll("[^\\d.]", "");
			WebElement editLink = getDriver().findElement(
					By.xpath("//span[contains(@class,'Card')]/../../div/p/span[contains(@class,'edit-link-text')]"));
			waitFor(ExpectedConditions.visibilityOf(editLink));
			editLink.click();
			Reporter.log("Clicked on Stored card number: " + storedCardNo);
			return storedCardNo;
		} catch (Exception e) {
			Assert.fail("Failed to click on saved card");
		}
		return null;
	}

	/**
	 * method to click any saved bank for validating edit card page
	 */
	public String verifyAndClickStoredBank() {
		try {
			checkPageIsReady();
			String storedBankNo = storedBankNumber.getText();
			WebElement editLink = getDriver().findElement(By.xpath(
					"//span[contains(@class,'check')]/../..//div/p[@class='card-blade-title']//following-sibling::p/span[contains(@ng-click,'edit')]"));
			editLink.click();
			Reporter.log("Clicked on Stored Bank number: " + storedBankNo);
			return storedBankNo;
		} catch (Exception e) {
			Assert.fail("Failed to click on saved bank");
		}
		return null;
	}

	public void clickUpdatedStoredCardAngular(String storedCardNo) {
		try {
			if (storedCardNoAngular.getText().contains(storedCardNo)) {
				String pid = storedCardNoAngular.getAttribute("pid");
				pid = pid.substring(pid.length() - 1);
				WebElement editLink = getDriver().findElement(
						By.xpath("//span[contains(@class,'Card')]/../../div/p/span[@id='edit" + pid + "']"));
				editLink.click();
				Reporter.log("Clicked on Stored card number: " + storedCardNo);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void clickUpdatedStoredCard(String storedCardNo) {
		try {
			if (storedCardNumber.getText().contains(storedCardNo)) {
				WebElement editLink = getDriver().findElement(By.xpath(
						"//span[contains(@class,'Card')]/../..//div/p[@class='card-blade-title']/span//..//../p[@class='card-blade-title']//following-sibling::p/span[contains(@ng-click,'edit')]"));
				editLink.click();
				Reporter.log("Clicked on Stored card number: " + storedCardNo);
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on saved card Edit link");
		}
	}

	public void clickUpdatedStoredBankAngular(String storedBankNo) {
		try {
			if (storedBankNumberAngular.getText().contains(storedBankNo)) {
				String pid = storedBankNumberAngular.getAttribute("pid");
				pid = pid.substring(pid.length() - 1);
				WebElement editLink = getDriver().findElement(
						By.xpath("//span[contains(@class,'Bank')]/../../div/p/span[@id='edit" + pid + "']"));
				editLink.click();
				Reporter.log("Clicked on Stored bank number: " + storedBankNo);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void clickUpdatedStoredBank(String storedBankNo) {
		try {
			if (storedBankNumber.getText().contains(storedBankNo)) {
				WebElement editLink = getDriver().findElement(By.xpath(
						"//span[contains(@class,'check')]/../..//div/p[@class='card-blade-title']//following-sibling::p/span[contains(@ng-click,'edit')]"));
				editLink.click();
				Reporter.log("Clicked on Stored bank number: " + storedBankNo);
			}
		} catch (Exception e) {
			Assert.fail("Failed to click saved bank Edit link");
		}
	}

	public void verifyDeletedBankIsNotDisplayed(String storedBankNo) {
		try {
			for (WebElement storedPayment : storedPaymentsBank) {
				if (storedPayment.getText().contains(storedBankNo)) {
					Assert.fail("Deleted Stored payment method is still displayed on Spoke page " + storedBankNo);
				}
			}
			Reporter.log("Verified that the deleted Stored payment method is not displayed on Spoke page");
		} catch (Exception e) {
			Assert.fail("Failed to verify if Deleted Stored payment method is still displayed on Spoke page "
					+ storedBankNo);
		}
	}

	public void verifyDeletedCardIsNotDisplayed(String storedCardNo) {
		try {
			for (WebElement storedPayment : storedPaymentsCard) {
				if (storedPayment.getText().contains(storedCardNo)) {
					Assert.fail("Deleted Stored payment method is still displayed on Spoke page " + storedCardNo);
				}
			}
			Reporter.log("Verified that the deleted Stored payment method is not displayed on Spoke page");
		} catch (Exception e) {
			Assert.fail("Failed to verify if Deleted Stored payment method is still displayed on Spoke page "
					+ storedCardNo);
		}
	}

	/**
	 * click on saved card
	 */
	public void clickSavedCard() {
		try {
			savedPayments.click();
			Reporter.log("Clicked on saved card");
		} catch (Exception e) {
			Assert.fail("Failed to click on saved card blade");
		}
	}

	/**
	 * click on saved bank
	 */
	public void clickSavedBank() {
		try {
			savedPayments.click();
			Reporter.log("Clicked on saved bank");
		} catch (Exception e) {
			Assert.fail("Failed to click on saved bank blade");
		}
	}

	/**
	 * verify default on stored payment blade
	 */
	public void verifyDefaultPaymentMethod() {
		try {
			waitFor(ExpectedConditions.visibilityOf(defaultLabel));
			defaultLabel.isDisplayed();
			Reporter.log("Verified Default for Modified or  Stored Payment Method Blade ");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Default' on Modified or Stored Payment Method Blade");
		}
	}
}