package com.tmobile.eservices.qa.listeners;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.annotations.ITestAnnotation;

public class FailTestRetryListener implements IAnnotationTransformer {

	@Override
	@SuppressWarnings("rawtypes")
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		 IRetryAnalyzer retry = annotation.getRetryAnalyzer();
	        if (retry == null) {
	            annotation.setRetryAnalyzer(FailedTestRetry.class);
	        }

	}

}
