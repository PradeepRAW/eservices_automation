package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class EmployeeLineDesignationPage extends CommonPage {

	private static final String pageUrl = "profile/employee";

	// Headers
	@FindBy(css = "p.Display3")
	private WebElement pageHeader;

	@FindBy(css = "span.arrow-right")
	private WebElement employeeLineDetailsArrow;

	@FindBys(@FindBy(css = "span.arrow-right"))
	private List<WebElement> employeeLineDetails;

	@FindBys(@FindBy(css = ".body.d-inline-block"))
	private List<WebElement> employeeDependantStatus;

	// Buttons

	@FindBy(css = "button.PrimaryCTA")
	private WebElement saveButton;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement backButton;

	@FindBy(xpath = "//label[contains(text(),'Employee/Spouse')]/../input")
	private WebElement employeeDependantRadioBtn;

	@FindBy(xpath = "//label[text()='Non-Dependent']/../input")
	private WebElement nonDependantRadioBtn;

	@FindBy(xpath = "//label[contains(text(),'Employee/Spouse')]")
	private WebElement employeeDependantBtn;

	@FindBy(xpath = "//label[text()='Non-Dependent']")
	private WebElement nonDependantBtn;

	public EmployeeLineDesignationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify EmployeeLineDesignation page.
	 *
	 * @return the EmployeeLineDesignation page class instance.
	 */
	public EmployeeLineDesignationPage verifyEmployeeLineDesignationPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("EmployeeLineDesignation page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("EmployeeLineDesignation page not displayed");
		}
		return this;
	}

	/**
	 * Click Profile link
	 */
	public EmployeeLineDesignationPage clickonEmployeeLineDetails() {
		waitforSpinnerinProfilePage();
		try {
			employeeLineDetailsArrow.click();
			Reporter.log("clicked on employee line details");

		} catch (Exception e) {
			Assert.fail("employee line details not found");
		}
		return this;
	}

	/**
	 * Click Profile link
	 */
	public EmployeeLineDesignationPage verifySaveButtonEnabled() {
		waitforSpinnerinProfilePage();
		try {
			saveButton.isEnabled();
			Reporter.log("save button is enabled");
		} catch (Exception e) {
			Assert.fail("save button is not enabled");
		}
		return this;
	}

	/**
	 * Click First Employee Line Details link
	 */
	public EmployeeLineDesignationPage clickFirstEmployeeLineDetailsLink() {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOfAllElements(employeeLineDetails));
		try {
			for (WebElement webElement : employeeLineDetails) {
				clickElementWithJavaScript(webElement);
				Reporter.log("Clicked on first employee line details");
				break;
			}
		} catch (Exception e) {
			Assert.fail("Click on first employee line details failed");
		}
		return this;
	}

	/**
	 * Click on EmployeeDependantRadioBtn.
	 */
	public EmployeeLineDesignationPage clickEmployeeDependantRadioBtn() {
		waitforSpinnerinProfilePage();
		try {
			if (!employeeDependantRadioBtn.isSelected()) {
				clickElementWithJavaScript(employeeDependantBtn);
				saveButton.click();
				Reporter.log("Clicked on employee dependent radio button");
			} else {
				backButton.click();
			}
		} catch (Exception e) {
			Assert.fail("Click on employee dependent radio button failed");
		}
		return this;
	}

	/**
	 * Click on nonDependantRadioBtn.
	 */
	public EmployeeLineDesignationPage clickNonDependantRadioBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (!nonDependantRadioBtn.isSelected()) {
				clickElementWithJavaScript(nonDependantBtn);
				saveButton.click();
				Reporter.log("Clicked on non dependent radio button");
			} else {
				backButton.click();
			}
		} catch (Exception e) {
			Assert.fail("Click on  non dependent radio button failed");
		}
		return this;
	}

	/**
	 * Click employeeDependantStatus for Non dependant
	 */
	public EmployeeLineDesignationPage verifyStatusForNonDependant() {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOfAllElements(employeeDependantStatus));
		Assert.assertTrue(employeeDependantStatus.get(0).getText().contains("Non-Dependent"),
				"Status not updated to Non depenedant");
		Reporter.log("Employee non depenedant updated on EmployeeLineDesignation page");
		return this;
	}

	/**
	 * Click employeeDependantStatus
	 */
	public EmployeeLineDesignationPage verifyStatusForDependant() {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOfAllElements(employeeDependantStatus));
		Assert.assertTrue(employeeDependantStatus.get(0).getText().contains("Employee/Spouse"),
				"Status not updated to depenedant");
		Reporter.log("Employee dependent status updated on EmployeeLineDesignation page");
		return this;
	}
}
