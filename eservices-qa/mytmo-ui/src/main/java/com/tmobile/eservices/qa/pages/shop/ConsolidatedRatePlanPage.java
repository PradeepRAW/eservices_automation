package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class ConsolidatedRatePlanPage extends CommonPage {

	@FindBy(css = "")
	private WebElement consolidatedRatePlanPage;

	@FindBy(css = "button.btn.btn-primary.PrimaryCTA-Normal")
	private WebElement continueCTA;

	@FindBy(css = "sub[ng-bind*='$ctrl.consolidatedAALModel.todayTotal']")
	private WebElement dueTodayPrices;

	@FindBy(xpath = "//div[text()= 'DUE TODAY']//..//*[contains(@class,'Display')]")
	private WebElement dueTodayAmount;

	@FindBy(css = "a[ng-bind*='$ctrl.consolidatedAALModel.deposit']")
	private WebElement SIMStarterKITCTA;

	@FindBy(css = "span[ng-bind*='$ctrl.consolidatedAALModel.promoText']")
	private WebElement promotionalText;

	@FindBy(css = "img.tag-icon.m-r-4")
	private WebElement promotionIcon;

	@FindBy(css = "div.no-padding.ng-scope")
	private WebElement promotionalModel;

	@FindBy(css = "button.close.pull-right i")
	private WebElement promotionalModelCloseButton;

	@FindBy(css = "h4.text-black.text-center")
	private List<WebElement> planDetailsModelHeader;

	@FindBy(css = "p.body-copy-description-regular")
	private List<WebElement> planDetailsModelHeaderDescription;

	@FindBy(css = "span[ng-bind-html='$ctrl.contentData.depositModalBodyContent  | displayHTML']")
	private WebElement depositDescription;

	@FindBy(css = "span[ng-bind-html='$ctrl.contentData.simkitModalBodyContent  | displayHTML']")
	private WebElement simStarterKitDescription;

	@FindBy(css = "")
	private WebElement planDetailsModelBottomHeader;

	@FindBy(css = "")
	private WebElement planDetailsModelBottomHeaderDescription;

	@FindBy(xpath = "//div[contains(text(), 'DUE MONTHLY')]")
	private WebElement dueMonthlyText;

	@FindBy(xpath = "//div[contains(text(), 'DUE TODAY')]")
	private WebElement dueTodayText;

	@FindBy(xpath = "//div[text()= 'DUE MONTHLY']//..//*[contains(@class,'Display')]")
	private WebElement dueMonthlyAmount;

	@FindBy(xpath = "//span/a[contains(@ng-click,'planModal')]")
	private WebElement friendlyPlanName;

	@FindBy(css = "div[ng-if='$ctrl.consolidatedAALModel.autoPayIndicator']")
	private WebElement autoPayAuthrobleText;

	@FindBy(xpath = "//div[contains(text(), 'Taxes and fees')]")
	private WebElement taxesAndFeesText;

	@FindBy(css = "h4.f-24.text-black.text-center")
	private WebElement planModelWindowHeader;

	@FindBy(css = "span.body-copy-description-regular")
	private WebElement planModelWindowDescription;

	@FindBy(css = "i.icox.ico-closeIcon")
	private WebElement planModelCloseIcon;

	@FindBy(xpath = "//a[contains(text(), 'T-Mobile ONE')]")
	private List<WebElement> seeFullPlanTermsLink;

	@FindBy(css = "titleOnLegalPage")
	private WebElement titleOnLegalPage;

	@FindBy(css = "span[ng-bind*='$ctrl.consolidatedAALModel.deposit']")
	private WebElement depositAmount;

	@FindBy(css = ".close-icon i")
	private WebElement tradeInDetailsDropDownOrStickyCloseBtn;

	@FindBy(css = "span[class='strike ng-binding ng-scope']")
	private WebElement simStarterKitStrikePrice;

	// @FindBy (css = "span[class='strike ng-binding ng-scope']+span")"
	@FindBy(css = "span[ng-bind='$ctrl.consolidatedAALModel.simStartOfferPrice | currency']")
	private WebElement simStarterKitAfterDiscountPrice;

	@FindBy(css = "span[ng-bind*='$ctrl.consolidatedAALModel.deposit']")
	private WebElement depositAmountText;

	@FindBy(css = "span[ng-bind*='tradeInWarningStickyHeader']")
	private WebElement tradeInDetailsDropDown;

	@FindBy(css = ".tmo_tfn_number")
	private WebElement tradeInStickyNumber;

	@FindBy(css = "")
	private WebElement instantDiscountText;

	@FindBy(css = "")
	private WebElement instantDiscountPrice;

	@FindBy(css = "simPrice")
	private WebElement simPrice;

	@FindBy(css = "[ng-if*='tradeInStickyHeaderTxt']")
	private WebElement wantToTradeInADeviceText;

	@FindBy(css = "[ng-if*='tradeInStickyHeaderTxt']")
	private List<WebElement> wantToTradeInADeviceTxt;

	@FindBy(css = "div.footer-nav-container")
	private WebElement footerContainer;

	@FindBy(css = "a[ng-bind*='$ctrl.aalWarningModalContent.ctaLabel']")
	private WebElement aalIneligibilityModalPrimaryCTA;

	@FindBy(css = "h2[ng-bind='$ctrl.aalWarningModalContent.title']")
	private WebElement aalIneligibilityModal;

	@FindBy(css = ".modal button.close")
	private WebElement aalIneligibilityModalCloseIcon;

	@FindBy(css = "div[ng-hide*='$ctrl.consolidatedAALModel.deposit']")
	private List<WebElement> dueTodayTotalSection;

	@FindBy(css = "[class='col-xs-12 no-padding ng-scope']")
	private WebElement depositModal;

	/**
	 * 
	 * /**
	 * 
	 * @param webDriver
	 */
	public ConsolidatedRatePlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Cart Page
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyConsolidatedRatePlanPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(continueCTA), 10);
			verifyPageUrl();
			Reporter.log("Navigated to consolidated Rate Plan  page");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to consolidated Rate Plan  page");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ConsolidatedRatePlanPage verifyPageUrl() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("plan"),
					"URL for Consolidated Charges Page is not correct");
		} catch (Exception e) {
			Assert.fail("Failed to verify consolidated Charges Page url");
		}
		return this;
	}

	/**
	 * Click on continue button
	 */
	public ConsolidatedRatePlanPage clickOnContinueCTA() {
		waitforSpinner();
		try {
			checkPageIsReady();
			clickElement(continueCTA);
			Reporter.log("Continue button was clicked on rate plan page");
		} catch (Exception e) {
			Assert.fail("Continue button was not clickable on rate plan page");
		}
		return this;
	}

	/**
	 * Verify Continue CTA
	 */
	public ConsolidatedRatePlanPage verifyContinueCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(continueCTA), 5);
			Assert.assertTrue(continueCTA.isDisplayed(), "Continue button is not displayed");
			Reporter.log("Continue button is displayed.");

		} catch (Exception e) {
			Assert.fail("Continue button was not displayed.");
		}
		return this;
	}

	/**
	 * Verify Sim Starter kit Strike Price
	 */
	public ConsolidatedRatePlanPage verifySimStarterKitStrikePrice() {
		try {

			Assert.assertTrue(simStarterKitStrikePrice.isDisplayed(), "Sim Starter Kit Strike Price is not displayed");
			Reporter.log("Sim Starter Kit Strike Price is displayed");

		} catch (Exception e) {
			Assert.fail("Sim Starter Kit Strike Price is not displayed");
		}
		return this;
	}

	/**
	 * Verify Sim Starter Kit After Discount Price
	 */
	public ConsolidatedRatePlanPage verifySimStarterKitAfterDiscountPrice() {
		try {
			Assert.assertTrue(simStarterKitAfterDiscountPrice.isDisplayed(),
					"Sim Starter Kit After Discount Price is displayed");
			Reporter.log("Sim Starter Kit After discount Price is displayed");
		} catch (Exception e) {
			Assert.fail("Sim Starter Kit After Discount Price is displayed");
		}
		return this;
	}

	/**
	 * Verify Due Today Amount
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyDueTodayAmount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dueTodayAmount));
			Assert.assertTrue(dueTodayAmount.isDisplayed(), "Due today amount is not displayed");
			Reporter.log("Due today amount is displayed");
		} catch (Exception e) {
			Assert.fail("Due today amount is not displayed");
		}
		return this;
	}

	/**
	 * Verify SIM Starter KIT CTA
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifySIMStarterKitCTA() {
		try {
			Assert.assertTrue(SIMStarterKITCTA.isDisplayed(), "SIM starter KIT CTA is not displayed");
		} catch (Exception e) {
			Assert.fail("SIM starter KIT CTA is not displayed");
		}
		return this;
	}

	/**
	 * Verify Sim Price
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifySimPrice() {
		try {
			Assert.assertTrue(simPrice.isDisplayed(), "Sim price not displayed");
			Reporter.log("Sim price is displayed");
		} catch (Exception e) {
			Assert.fail("Sim price not displayed");
		}
		return this;
	}

	/**
	 * Verify Promotional TextDetails
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyPromotionalText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(promotionalText.getText().replaceAll("[0-9]", "")
					.contains("TODAY ONLY SAVE $ on Samsung instantly"), "Promotional text is not displayed");
		} catch (Exception e) {
			Assert.fail("Promotional text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promotional Icon
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyPromotionIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(promotionIcon.isDisplayed(), "Promotional Icon is not displayed");
		} catch (Exception e) {
			Assert.fail("Promotional Icon is not displayed");
		}
		return this;
	}

	/**
	 * Click Promotional Text
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage clickPromotionalLink() {
		try {
			promotionalText.click();
		} catch (Exception e) {
			Assert.fail("Promotional text is not displayed");
		}
		return this;
	}

	/**
	 * Click Promotional Text
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage clickPromotionalModelCloseButton() {
		try {
			checkPageIsReady();
			promotionalModelCloseButton.click();
		} catch (Exception e) {
			Assert.fail("Promotional model close is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promotional Model
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyPromotionalModel() {
		try {
			Assert.assertTrue(promotionalModel.isDisplayed(), "Promotional model is not displayed");
		} catch (Exception e) {
			Assert.fail("Promotional model is not displayed");
		}
		return this;
	}

	/**
	 * Verify Deposit Header
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyDepositHeaderonPlanDetailsModel() {
		try {
			checkPageIsReady();
			Assert.assertTrue(planDetailsModelHeader.get(0).getText().contains("Deposit"),
					"Deposit header is not displayed");
		} catch (Exception e) {
			Assert.fail("Deposit header is not displayed");
		}
		return this;
	}

	/**
	 * Verify Deposit Header Description
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyDescriptionforDeposit() {
		try {

			Assert.assertTrue(depositDescription.isDisplayed());
			Reporter.log("Deposit description is displayed");

		} catch (Exception e) {
			Assert.fail("Deposit Description is not displayed");
		}
		return this;
	}

	/**
	 * Verify 3-in-1 SIM starter kit Header
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verify3in1SIMStarterkitHeaderonPlanDetailsModel() {
		try {
			Assert.assertTrue(planDetailsModelHeader.get(1).getText().contains("SIM Starter Kit"),
					"SIM Starter Kit header is not displayed");

		} catch (Exception e) {
			Assert.fail("SIM Starter Kit header is not displayed");
		}
		return this;
	}

	/**
	 * Verify 3-in-1 SIM starter kit Description
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verify3in1SIMStarterkitDescription() {

		try {
			Assert.assertTrue(simStarterKitDescription.isDisplayed());
			Reporter.log("Sim stareter kit descriptin is displayed");
		} catch (Exception e) {
			Assert.fail("3-in-1 SIM starter kit Description is not displayed");
		}
		return this;
	}

	/**
	 * Verify DueMonthly Text
	 */
	public ConsolidatedRatePlanPage verifyDueMonthlyText() {
		try {
			Assert.assertTrue(dueMonthlyText.isDisplayed(), "Due Monthly text is not displayed");
			Reporter.log("Due Monthly text is displayued");
		} catch (Exception e) {
			Assert.fail("Due Monthly text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Due Today Text
	 */
	public ConsolidatedRatePlanPage verifyDueTodayText() {
		try {
			Assert.assertTrue(dueTodayText.isDisplayed(), "Due Today text is not displayed");
			Reporter.log("Due Today text is displayued");
		} catch (Exception e) {
			Assert.fail("Due Today text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Due Today Text not displayed
	 */
	public ConsolidatedRatePlanPage verifyDueTodayTextNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertFalse(dueTodayTotalSection.get(0).isDisplayed(), "Due Today section is displayed");
			Assert.assertFalse(isElementDisplayed(dueTodayText), "Due Today text is displayed");
			Assert.assertFalse(isElementDisplayed(dueTodayPrices), "Due Today price is displayed");
			Reporter.log("Due Today text&price section not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Due Today section");
		}
		return this;
	}

	/**
	 * Verify DueMonthly Amount
	 */
	public ConsolidatedRatePlanPage verifyDueMonthlyAmount() {
		try {
			Assert.assertTrue(dueMonthlyAmount.getText().contains("$"), "Due Monthly Amount is not displayed");
			Reporter.log("Due Monthly Amount is displayued");
		} catch (Exception e) {
			Assert.fail("Due Monthly Amount is not displayed");
		}
		return this;
	}

	/**
	 * Verify FriendlyPlanName
	 */
	public ConsolidatedRatePlanPage verifyFriendlyPlanName() {
		try {
			waitforSpinner();
			Assert.assertTrue(friendlyPlanName.isDisplayed(), "FriendlyPlan Name is not displayed");
			Reporter.log("FriendlyPlan Name is displayued");
		} catch (Exception e) {
			Assert.fail("FriendlyPlan Name is not displayed");
		}
		return this;
	}

	/**
	 * Verify FriendlyPlanName
	 */
	public ConsolidatedRatePlanPage verifyFriendlyPlanNameEssentials() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(friendlyPlanName));
			Assert.assertTrue(friendlyPlanName.isDisplayed(), "FriendlyPlan Name is not displayed");
			Assert.assertTrue(friendlyPlanName.getText().contains("T-Mobile Essentials"),
					"FriendlyPlan Name is not displayed");
			Reporter.log("FriendlyPlan Name is displayued");
		} catch (Exception e) {
			Assert.fail("FriendlyPlan Name is not displayed");
		}
		return this;
	}

	/**
	 * Verify FriendlyPlanName
	 */
	public ConsolidatedRatePlanPage verifyFriendlySimpleChoicePlanName() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(friendlyPlanName));
			Assert.assertTrue(friendlyPlanName.isDisplayed(), "Simple choice Plan Name is not displayed");
			Assert.assertTrue(friendlyPlanName.getText().contains("Simple Choice"),
					"Simple choice Plan Name is not displayed");
			Reporter.log("Simple choice Plan Name is displayued");
		} catch (Exception e) {
			Assert.fail("Simple choice Plan Name is not displayed");
		}
		return this;
	}

	/**
	 * Verify AutoPayAuthorable Text
	 */
	public ConsolidatedRatePlanPage verifyAutoPayAuthorableText() {
		try {
			Assert.assertTrue(autoPayAuthrobleText.isDisplayed(), "AutoPay Authorable Text is not displayed");
			Assert.assertTrue(autoPayAuthrobleText.getText().equalsIgnoreCase("While using AutoPay."),
					"AutoPay text not matched");
			Reporter.log("AutoPay Authorable Text is displayed");
		} catch (Exception e) {
			Assert.fail("AutoPay Authorable Text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Taxes And Fees Included Text For TI customers
	 */
	public ConsolidatedRatePlanPage verifyTaxesAndFeesIncludedTextForTIcustomers() {
		try {
			Assert.assertTrue(taxesAndFeesText.isDisplayed(),
					"Taxes And Fees Included Text For TI customers is not displayed");
			Assert.assertTrue(taxesAndFeesText.getText().equalsIgnoreCase("Taxes and fees included."),
					"TI customers taxes text matched");
			Reporter.log("Taxes And Fees Included Text For TI customers is displayed");
		} catch (Exception e) {
			Assert.fail("Taxes And Fees Included Text For TI customers is not displayed");
		}
		return this;
	}

	/**
	 * Click Friendly Plan Name
	 */
	public ConsolidatedRatePlanPage clickFriendlyPlanName() {
		try {
			friendlyPlanName.click();
			Reporter.log("Friendly plan name should be clickable");
		} catch (Exception e) {
			Assert.fail("Friendly plan name should not be clickable");
		}
		return this;
	}

	/**
	 * Verify Model Header
	 */
	public ConsolidatedRatePlanPage verifyModelHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(planModelWindowHeader), 10);
			Assert.assertTrue(planModelWindowHeader.isDisplayed(), "Plan model window header is not displayed");
			Reporter.log("Plan mopdel window header is displayed");
		} catch (Exception e) {
			Assert.fail("Plan model window header is not displayed");
		}
		return this;
	}

	/**
	 * Verify Model Description
	 */
	public ConsolidatedRatePlanPage verifyModelDescription() {
		try {
			waitFor(ExpectedConditions.visibilityOf(planModelWindowHeader), 10);
			Assert.assertTrue(planModelWindowDescription.isDisplayed(),
					"Plan model window descriptin is not displayed");
			Reporter.log("Plan mopdel window description is displayed");
		} catch (Exception e) {
			Assert.fail("Plan model window description is not displayed");
		}
		return this;
	}

	/**
	 * Click Model Close Icon
	 */
	public ConsolidatedRatePlanPage clickModelCloseIcon() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(planModelCloseIcon), 10);
			planModelCloseIcon.click();
			Reporter.log("Plan model window close icon should be clickable");
		} catch (Exception e) {
			Assert.fail("Plan model window close icon should be not clickable");
		}
		return this;
	}

	/**
	 * Verify Plan Model Window Not Displayed
	 */
	public ConsolidatedRatePlanPage verifyPlanModelWindowHeaderNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(planModelWindowHeader), "Plan model Header is displayed");
			Reporter.log("Plan model header is not displayed");
		} catch (Exception e) {
			Assert.fail("Plan model header is displayed");
		}
		return this;
	}

	/**
	 * Verify Taxes And Fees Included Text For TE customers
	 */
	public ConsolidatedRatePlanPage verifyTaxesAndFeesIncludedTextForTEcustomers() {
		try {
			Assert.assertTrue(taxesAndFeesText.isDisplayed(),
					"Taxes And Fees Included Text For TE customers is not displayed");
			Assert.assertTrue(taxesAndFeesText.getText().equalsIgnoreCase("Plus taxes and fees."),
					"TE customers taxes text matched");
			Reporter.log("Taxes And Fees Included Text For TE customers is displayed");
		} catch (Exception e) {
			Assert.fail("Taxes And Fees Included Text For TE customers is not displayed");
		}
		return this;
	}

	/**
	 * Verify See Full Plan Terms Link in RatePlan page
	 */
	public ConsolidatedRatePlanPage verifySeeFullPlanTermsLink() {
		try {
			Assert.assertTrue(seeFullPlanTermsLink.get(0).isDisplayed(), "See full plan terms link is not displayed");
			Reporter.log("See full plan terms link is displayed");
		} catch (Exception e) {
			Assert.fail("See full plan terms link is not displayed");
		}
		return this;
	}

	/**
	 * Click SeeFullPlanTerms Link
	 */
	public ConsolidatedRatePlanPage clickSeeFullPlanTermsLink() {
		try {
			seeFullPlanTermsLink.get(0).click();
			Reporter.log("See full plan terms link is clickable");
		} catch (Exception e) {
			Assert.fail("See full plan terms link is not clickable");
		}
		return this;
	}

	/**
	 * Verify Legal Text Page
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyLegalTextPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(titleOnLegalPage.getText().contains("Legal"));
			Reporter.log("Title was displayed on legal page.");
		} catch (NoSuchElementException e) {
			Assert.fail("Title was not displayed on legal page.");
		}
		return this;
	}

	/**
	 * Trade-in details components are verified
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyTradeInDetailsDropDownOrSticky() {
		try {
			waitforSpinner();
			Assert.assertTrue(tradeInDetailsDropDown.isDisplayed());
			Reporter.log("trade In Details Drop Down is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("trade In Details Drop Down is not displaying");
		}
		return this;
	}

	/**
	 * Click on Call CTA
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyOnCallCta() {
		try {
			Assert.assertTrue(tradeInStickyNumber.isDisplayed());
			Reporter.log("trade In Sticky Number is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("trade In Details Drop down number is not displaying");
		}
		return this;
	}

	/**
	 * Verify plan details CTA
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyPlanDetailsCTA() {
		try {
			SIMStarterKITCTA.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Plan details CTA is not displayed");
		}
		return this;
	}

	/**
	 * Verify depsoit amount
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyAuthorableDepositAmount() {
		try {
			depositAmount.isDisplayed();
			Reporter.log("Authorable deposit amount displayed");
		} catch (Exception e) {
			Assert.fail("Authorable deposit amount not displayed");
		}
		return this;
	}

	/**
	 * Verify depsoit amount should not be displayed
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyAuthorableDepositAmountNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(depositAmount), "deposit amount is displayed");
			Reporter.log("Authorable deposit amount not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify deposit amount");
		}
		return this;
	}

	/**
	 * Click modal window close btn CTA
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage clickModalWindowCloseBtn() {
		try {
			tradeInDetailsDropDownOrStickyCloseBtn.click();
		} catch (Exception e) {
			Assert.fail("Click on close btn failed ");
		}
		return this;
	}

	/**
	 * Verify that Due Today Price equal to (Deposit price + Sim StarterKit After
	 * discount price)
	 */
	public ConsolidatedRatePlanPage compareDueTodayAmount(double todayValue1, double todayValue2) {
		try {
			Assert.assertEquals(todayValue1, todayValue2,
					" Due Today amount is not matched with (Deposit price + Sim Starter kit price).");
			Reporter.log("Due Today amount is matched with (Deposit price + Sim Starter kit price)");
		} catch (Exception e) {
			Assert.fail("Failed to compare Due Today Total Amount with (Deposit price + Sim Starter kit price)");
		}
		return this;
	}

	/**
	 * Get DueToday price in Rate Plan page
	 * 
	 * @return
	 */
	public Long getDueTodayPrice() {
		String dueTodayPrice;
		dueTodayPrice = dueTodayPrices.getText();
		return Long.valueOf(dueTodayPrice);
	}

	/**
	 * Get Deposit Amount Under Due Today Price
	 */
	public Double getDepositAmountUnderDueTodayPrice() {
		String depositPrice;
		depositPrice = depositAmount.getText();
		String depositPrices = depositPrice.substring(depositPrice.lastIndexOf("$") + 1);
		return Double.parseDouble(depositPrices);
	}

	/**
	 * Get Sim stater Kit After Discount Price Under DueToday Price
	 */
	public Double getSimstaterKitAfterDiscountPriceUnderDueTodayPrice() {
		String simstaterKitAfterDiscountPrice;
		simstaterKitAfterDiscountPrice = simStarterKitAfterDiscountPrice.getText();
		String simstaterKitAfterDiscountAmount = simstaterKitAfterDiscountPrice
				.substring(simstaterKitAfterDiscountPrice.lastIndexOf("$") + 1);
		return Double.parseDouble(simstaterKitAfterDiscountAmount);
	}

	/**
	 * Verify instant discount text
	 */
	public ConsolidatedRatePlanPage verifyInstantDiscountTextDisplayed() {
		try {

			Assert.assertFalse(instantDiscountText.isDisplayed(), "Instant discount text is displayed");
			Reporter.log("Instant discount text is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display instant discount text");
		}
		return this;
	}

	/**
	 * Verify instant discount price
	 */
	public ConsolidatedRatePlanPage verifyInstantDiscountPriceDisplayed() {
		try {

			Assert.assertFalse(instantDiscountPrice.isDisplayed(), "Instant discount price is displayed");
			Reporter.log("Instant discount price is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display instant discount price");
		}
		return this;
	}

	/**
	 * Verify Consolidated Rate Plan page footer Not displayed
	 */
	public ConsolidatedRatePlanPage verifyRatePlanPageFooterNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(footerContainer), "Rate Plan Page footer is displayed");
			Reporter.log("Rate Plan Page footer is  not displayedd");
		} catch (Exception e) {
			Assert.fail("Failed not to display Rate Plan Page footer");
		}
		return this;
	}

	/**
	 * verify Want to Trade In a device text
	 */
	public ConsolidatedRatePlanPage verifyWantToTradeInADeviceTextNotDisplayed() {
		try {
			Assert.assertEquals(wantToTradeInADeviceTxt.size(), 0, "Want To Trade In A Device text is displayed");
			Reporter.log("Want To Trade In A Device text is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Want To Trade In A Device text ");
		}
		return this;
	}

	/**
	 * Verify AAL ineligibility modal displayed
	 */
	public ConsolidatedRatePlanPage verifyAalIneligibilityModalDisplayed() {
		try {

			waitforSpinner();
			Assert.assertTrue(aalIneligibilityModal.isDisplayed(), "AAL Ineligibility Modal is not displayed");
			Reporter.log("AAL Ineligibility Modal is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display AAL Ineligibility Modal");
		}
		return this;
	}

	/**
	 * Click on AAL ineligibility modal cta
	 */
	public ConsolidatedRatePlanPage clickOnAALIneligibilityModalCTA() {
		try {
			waitforSpinner();
			aalIneligibilityModalPrimaryCTA.click();
			Reporter.log("AAL Ineligibility Modal CTA is clicked");

		} catch (Exception e) {
			Assert.fail("Failed to click AAL Ineligibility Modal CTA");
		}
		return this;
	}

	/**
	 * Click on AAL ineligibility modal Close icon
	 */
	public ConsolidatedRatePlanPage clickOnAALIneligibilityModalCloseIcon() {
		try {
			waitforSpinner();
			aalIneligibilityModalCloseIcon.click();
			Reporter.log("AAL Ineligibility Modal Close icon is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click close icon for AAL Ineligibility Modal");
		}
		return this;
	}

	/**
	 * Click SIM Starter KIT CTA
	 * 
	 * @return
	 */
	public ConsolidatedRatePlanPage clickSIMStarterKitCTA() {
		try {
			SIMStarterKITCTA.click();
			Reporter.log("SIM starter KIT CTA is clicked");
		} catch (Exception e) {
			Assert.fail("SIM starter KIT CTA is not clickable");
		}
		return this;
	}

	/**
	 * Verify Deposit Modal
	 *
	 * @return
	 */
	public ConsolidatedRatePlanPage verifyDepositModal() {
		try {
			checkPageIsReady();
			Assert.assertTrue(depositModal.isDisplayed(), "Deposit modal not displayed ");
		} catch (Exception e) {
			Assert.fail("Deposit header is not displayed");
		}
		return this;
	}

}
