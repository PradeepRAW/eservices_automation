package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class NewSpokePage extends CommonPage {

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button.SecondaryCTA") })
	private WebElement pageLoadElement;

	@FindBy(css = "div.Display3")
	private WebElement pageHeader;

	@FindBy(xpath = "//span[contains(text(),'Add a card')]")
	private WebElement addCard;

	@FindBy(xpath = "//span[contains(text(),'Add a bank')]")
	private WebElement addBank;

	private final String newPageUrl = "payments/paymentcollection";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public NewSpokePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * 
	 * 
	 * /** Verify that current page URL matches the expected URL.
	 */
	public NewSpokePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(newPageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewSpokePage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyNewPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			pageHeader.isDisplayed();
			Reporter.log("Payment spoke page is loaded and header is verified ");
		} catch (Exception e) {
			Assert.fail("Failed to load payment spoke page");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewSpokePage verifyNewPageUrl() {
		waitFor(ExpectedConditions.urlContains(newPageUrl));
		return this;
	}

	/**
	 * verify page header is displayed
	 * 
	 * @return boolean
	 */
	public NewSpokePage verifyPageHeader() {
		try {
			pageHeader.isDisplayed();
			Assert.assertTrue(pageHeader.getText().equals("Edit payment method"), "page header is not displayed");
			Reporter.log("page header is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify page header");
		}
		return this;
	}

	/**
	 * 
	 * click on add bank
	 */
	public NewSpokePage clickAddBank() {
		try {
			addBank.click();
			Reporter.log("Clicked on add bank");
		} catch (Exception e) {
			Assert.fail("Add bank button not found");
		}
		return this;
	}

	/**
	 * 
	 * verify add bank blade is displayed or not
	 */
	public NewSpokePage verifyAddBankBlade() {
		try {
			addBank.isDisplayed();
			Reporter.log("add a bank blade is diaplayed");
		} catch (Exception e) {
			Assert.fail("failed to verify add a bank blade");
		}
		return this;
	}

	/**
	 * 
	 * verify add card blade is displayed or not
	 */
	public NewSpokePage verifyAddCardBlade() {
		try {
			addCard.isDisplayed();
			Reporter.log("add a card blade is diaplayed");
		} catch (Exception e) {
			Assert.fail("failed to verify add a card blade");
		}
		return this;
	}

	/**
	 * click on add card
	 */
	public NewSpokePage clickAddCard() {
		try {
			addCard.click();
			Reporter.log("Add card button clicked");
		} catch (Exception e) {
			Assert.fail("Add card not found");
		}
		return this;
	}

}