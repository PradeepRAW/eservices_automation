package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class DocuSignPage extends CommonPage {
	private final String pageUrl = "Signing/?insession";

	@FindBy(css = "h1.title")
	private WebElement pageHeader;

	@FindBy(css = "button#action-bar-btn-continue")
	private WebElement continueButton;

	@FindBy(css = "label.cb_label")
	private WebElement disclosureAcceptedCheckbox;

	@FindBy(css = "svg.tab-image-for-signature")
	private WebElement SignImageIcon;

	@FindBy(css = "canvas[aria-describedby='canvas-helpfulText']")
	private WebElement Signature;

	@FindBy(css = "button[data-qa='adopt-submit']")
	private WebElement AdoptSignButton;

	@FindBy(css = "div#finish-button-callout>button.btn-main")
	private WebElement SubmitOrderButton;

	@FindBy(css = "div.signature-draw.signature")
	private WebElement signatureCoordinatesReference;

	public DocuSignPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public DocuSignPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public DocuSignPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Docsign Page is loaded.");
		} catch (Exception e) {
			Assert.fail("Docsign page is not loaded");
		}
		return this;
	}

	/**
	 * Click DOCU sign Continue Button
	 * 
	 * 
	 */
	public void clickDisclosureCheckBox() {
		try {
			disclosureAcceptedCheckbox.click();
			Reporter.log("DisclosureCheckbox is Clicked");
		} catch (Exception e) {
			Assert.fail("Disclosure Checkbox not found in DocuSign Page");
		}
	}

	/**
	 * Click Discloser Checkbox
	 * 
	 * 
	 */
	public void clickContinueButton() {
		try {
			continueButton.click();
			Reporter.log("Continue Button is Clicked");
		} catch (Exception e) {
			Assert.fail("Continue Button is not found in DocuSign Page");
		}
	}

	/**
	 * Click on Sign Image Icon
	 * 
	 * 
	 */
	public void clickSignImageIcon() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.elementToBeClickable(SignImageIcon));
			SignImageIcon.click();
			Reporter.log("SignImage Icon is Clicked");
		} catch (Exception e) {
			Assert.fail("SignImage Icon is not found in DocuSign Page");
		}
	}

	/**
	 * Click on AdoptSign Button
	 * 
	 * 
	 */
	public void clickAdoptSignButton() {
		try {
			AdoptSignButton.click();
			Reporter.log("AdoptSign Button is Clicked");
		} catch (Exception e) {
			Assert.fail("AdoptSign Button is not found in DocuSign Page");
		}
	}

	/**
	 * Click on SubmitOrder Button
	 * 
	 * 
	 */
	public void clickSubmitOrderButton() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.elementToBeClickable(SubmitOrderButton));
			SubmitOrderButton.click();
			Reporter.log("SubmitOrder Button is Clicked");
		} catch (Exception e) {
			Assert.fail("SubmitOrder Button is not found in DocuSign Page");
		}
	}

	/**
	 * Click on SubmitOrder Button
	 * 
	 * 
	 */
	public void editSignature() {
		try {
			waitforSpinner();
			Point cordinates = signatureCoordinatesReference.getLocation();
			cordinates.getX();
			cordinates.getY();
			System.out.println("x :" + cordinates.getX() + "y :" + cordinates.getY());
			waitFor(ExpectedConditions.elementToBeClickable(Signature));
			Actions builder = new Actions(getDriver());
			builder.moveToElement(Signature);
			builder.clickAndHold(Signature).moveByOffset(cordinates.getX() + 30, cordinates.getY() + 5)
					.moveByOffset(cordinates.getX() + 50, cordinates.getY() + 5).release().build().perform();
			Reporter.log("Signature is Edited");
		} catch (Exception e) {
			Assert.fail("Signature Element is not found in DocuSign Page");
		}
	}

}
