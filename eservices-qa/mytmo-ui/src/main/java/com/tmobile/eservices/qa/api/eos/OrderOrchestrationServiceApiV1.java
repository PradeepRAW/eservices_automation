package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class OrderOrchestrationServiceApiV1 extends ApiCommonLib{
	
    public Response orders(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildOrderOrchestrationHeader(apiTestData);
		String resourceURL = "v1/orders";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    }
    
    private Map<String, String> buildOrderOrchestrationHeader(ApiTestData apiTestData) throws Exception {
    	final String oauth = getAccessToken();
    	
		Map<String, String> headers = new HashMap<String, String>();
		
		headers.put("Authorization",oauth);
		/*headers.put("transactionId","transactionId");
		headers.put("transactionType","jump");
		headers.put("applicationId","CSM");
		headers.put("channelId","web");
		headers.put("correlationId",oauth);
		headers.put("clientId","TMO");
		headers.put("transactionBusinessKey","msisdn");
		headers.put("transactionBusinessKeyType",apiTestData.getMsisdn());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("Content-Type","application/json");
		headers.put("dealerCode","000002");*/
		return headers;
	}

}
