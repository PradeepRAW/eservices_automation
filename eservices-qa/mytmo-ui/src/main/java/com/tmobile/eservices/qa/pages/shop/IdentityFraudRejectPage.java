package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class IdentityFraudRejectPage extends CommonPage {

	@FindBy(css = "button[ng-click*='rejectSecondaryCTAAction']")
	private WebElement rejectSecondaryBackShoppingCTA;

	@FindBy(css = "span[ng-bind*='findStoreCTAText']")
	private WebElement rejectPrimaryFindStoreCTA;

	@FindBy(css = "span[ng-bind*='rejectionHeader']")
	private WebElement rejectScreenHeader;

	@FindBy(css = "span[ng-bind*='rejectionBody']")
	private WebElement rejectScreenTextBody;

	@FindBy(css = "[ng-class*='isHideHeader']")
	private WebElement introductionPageHeader;

	@FindBy(css = "[ng-class*='isHideFooter']")
	private WebElement introductionPageFooter;

	@FindBy(css = "[ng-click*='ctrl.goToback']")
	private WebElement backButton;

	@FindBy(css = "[ng-if*='ctrl.isCustomHeader']")
	private WebElement sedonaHeader;

	/**
	 * @param webDriver
	 */
	public IdentityFraudRejectPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Identity Review Introduction Page
	 * 
	 * @return
	 */
	public IdentityFraudRejectPage verifyFraudRejectPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
			Reporter.log("Fraud reject page is visible");
		} catch (Exception e) {
			Assert.fail("failed to load reject screen");
		}
		return this;
	}

	/**
	 * Verify that page is review question page
	 */
	public IdentityFraudRejectPage verifyPageUrl() {
		try {
			getDriver().getCurrentUrl().contains("identityreviewfail");
		} catch (Exception e) {
			Assert.fail("Identity review fail page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Fraud Reject Page Header
	 */
	public IdentityFraudRejectPage verifyRejectFraudScreenPageHeaderText() {
		try {
			Assert.assertTrue(rejectScreenHeader.isDisplayed(), "Header is not displayed on fraud reject screen");
			Reporter.log("Header is Displayed on fraud reject screen");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Header on fraud reject screen");
		}
		return this;
	}

	/**
	 * Verify Fraud Reject Page Body
	 */
	public IdentityFraudRejectPage verifyRejectFraudScreenIntroductionPagebodyText() {
		try {
			Assert.assertTrue(rejectScreenTextBody.isDisplayed(), "Text is not displayed on fraud reject screen");
			Reporter.log("text is Displayed on fraud reject  screen");
		} catch (Exception e) {
			Reporter.log("Failed to Verify text on fraud reject screen");
		}
		return this;
	}

	/**
	 * Verify Fraud Reject Page Body
	 */
	public IdentityFraudRejectPage verifyPrimaryRejectFraudScreen() {
		try {
			Assert.assertTrue(rejectPrimaryFindStoreCTA.isDisplayed(),
					"Primary CTA is not displayed on fraud reject screen");
			Reporter.log("Primary CTA is Displayed on fraud reject  screen");
		} catch (Exception e) {
			Reporter.log("Failed to Verify text on Primary CTA");
		}
		return this;
	}

	/**
	 * Verify Fraud Reject Page Body
	 */
	public IdentityFraudRejectPage verifySecondaryRejectFraudScreenCTA() {
		try {
			Assert.assertTrue(rejectSecondaryBackShoppingCTA.isDisplayed(),
					"Secondary CTA is not displayed on fraud reject screen");
			Reporter.log("Secondary CTA is Displayed on fraud reject  screen");
		} catch (Exception e) {
			Reporter.log("Failed to Verify text on Secondary CTA");
		}
		return this;
	}

	/**
	 * Verify Identity Review Introduction Page Header & Footer Not Displayed
	 */
	public IdentityFraudRejectPage verifyIdentityFraudRejectheaderAndFooter() {
		try {
			Assert.assertFalse(introductionPageHeader.isDisplayed(), "Header is displayed");
			Assert.assertFalse(introductionPageFooter.isDisplayed(), "Footer is displayed");
			Reporter.log("Header & footer are not Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Header & footer on Reject Page");
		}
		return this;
	}

	/**
	 * Click on Primary CTA
	 */
	public IdentityFraudRejectPage clickPrimaryRejectFraudCTA() {
		try {
			rejectPrimaryFindStoreCTA.click();
			Reporter.log("Primary CTA is Clicked");
		} catch (Exception e) {
			Reporter.log("Failed to Click on Primary CTA");
		}
		return this;
	}

	/**
	 * Click on Secondary CTA
	 */
	public IdentityFraudRejectPage clickSecondaryRejectFraudCTA() {
		try {
			rejectSecondaryBackShoppingCTA.click();
			Reporter.log("Secondary CTA is Clicked");
		} catch (Exception e) {
			Reporter.log("Failed to Click on Secondary CTA");
		}
		return this;
	}

	/**
	 * Verify Back Button not displayed
	 */
	public IdentityFraudRejectPage verifyBackButtonNotDisplayed() {
		try {
			Assert.assertFalse(backButton.isDisplayed(), "Back Button is displayed");
			Reporter.log("Back Button is not displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Back Button");
		}
		return this;
	}

	/**
	 * Verify Identity Review Introduction Page Header
	 */
	public IdentityFraudRejectPage verifySedonaHeaderDisplayed() {
		try {
			Assert.assertTrue(sedonaHeader.isDisplayed(), "Header is not displayed");
			Reporter.log("Sedona Header is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Sedona Header on Introduction Page");
		}
		return this;
	}

	/**
	 * Verify Fraud reject text
	 */
	public IdentityFraudRejectPage verifyTextForFraudRejectModel() {
		try {
			Assert.assertTrue(
					rejectScreenHeader.getText().contains("Sorry -- We weren’t able to verify your information"),
					"Fraud check text not displayed");
			Reporter.log("Verified fraud reject text");
		} catch (Exception e) {
			Reporter.log("Failed to Verify reject etxt");
		}
		return this;
	}

}
