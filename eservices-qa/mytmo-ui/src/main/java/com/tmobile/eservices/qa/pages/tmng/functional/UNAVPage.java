package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author sputti
 *
 */
public class UNAVPage extends TmngCommonPage {
	private final String pageUrl = "http://dmo-tmo-publisher.corporate.t-mobile.com/content/t-mobile/test_unav.html";
	private final String ibapageUrl = "http://dmo-tmo-publisher.corporate.t-mobile.com/iba?tid=WlEuipbMit/9aQ==";

	@FindBy(css = "#search > .search-form > .icon")
	private WebElement searchIcon;

	@FindBy(xpath = "//input[@id='searchText']")
	private WebElement searchTextBox;

	@FindBy(xpath = "//button[@class='search-submit']")
	private WebElement searchSubmit;

	@FindBy(xpath = "//div[@class='resultsTabDiv device firstTab activeTab']")
	private WebElement searchResultSection;

	@FindBy(xpath = "//a[@href='http://www.t-mobile.com']")
	private WebElement logo;

	// ------Header-------------
	@FindBy(xpath = "//a[contains(text(),'5G VISION')]")
	private WebElement vision;

	@FindBy(xpath = "//a[contains(text(),'Phones')]")
	private WebElement phones;

	@FindBy(xpath = "//a[contains(text(),'Plans')]")
	private WebElement plans;

	@FindBy(xpath = "//a[contains(text(),'My T-Mobile')]")
	private WebElement myTmobile;

	@FindBy(css = ".hamburger")
	private WebElement menu;

	// ------------Footer----------
	@FindBy(xpath = "//a[contains(text(),'Contact information')]")
	private WebElement contactUs;
	@FindBy(xpath = "//a[contains(text(),'Check order status')]")
	private WebElement checkOrder;
	@FindBy(xpath = "//a[contains(text(),'View Return Policy')]")
	private WebElement viewPolicy;
	@FindBy(xpath = "//a[contains(text(),'Get a rebate')]")
	private WebElement getRebate;
	@FindBy(xpath = "//a[contains(text(),'Find a store')]")
	private WebElement findStore;
	@FindBy(xpath = "//a[contains(text(),'Trade in program')]")
	private WebElement tradeProgram;
	@FindBy(xpath = "//a[contains(text(),'Support home')]")
	private WebElement support;
	@FindBy(xpath = "//a[contains(text(),'Device support')]")
	private WebElement deviceSupport;
	@FindBy(xpath = "//a[contains(text(),'Questions about your bill')]")
	private WebElement questionBill;
	@FindBy(xpath = "//a[contains(text(),'Plans & services')]")
	private WebElement planService;
	@FindBy(xpath = "//a[contains(text(),'Activate your Prepaid phone or device')]")
	private WebElement prepaidPhone;
	@FindBy(xpath = "//a[contains(text(),'Refill your Prepaid account')]")
	private WebElement refillAccount;
	@FindBy(xpath = "//a[contains(text(),'International rates')]")
	private WebElement InterRate;
	@FindBy(xpath = "//a[contains(text(),'Student/Teacher Discount')]")
	private WebElement studentDiscount;
	@FindBy(xpath = "//a[contains(text(),'T-Mobile for Business')]")
	private WebElement business;
	@FindBy(xpath = "//a[contains(text(),'Internet of Things')]")
	private WebElement internet;
	@FindBy(xpath = "//a[contains(text(),'ABOUT')]")
	private WebElement aboutus;
	@FindBy(xpath = "//a[contains(text(),'INVESTOR RELATIONS')]")
	private WebElement investor;
	@FindBy(xpath = "//a[contains(text(),'PRESS')]")
	private WebElement press;
	@FindBy(xpath = "//a[contains(text(),'CAREERS')]")
	private WebElement careers;
	@FindBy(xpath = "//a[contains(text(),'DEUTSCHE TELEKOM')]")
	private WebElement deutsche;
	@FindBy(xpath = "//a[contains(text(),'PUERTO RICO')]")
	private WebElement puertorico;
	
	// ------------IBA----------
	@FindBy(xpath = "//button[@class='btn btn-primary btn-lg text-center ng-binding']")
	private WebElement ibaTurnOff;
	
	@FindBy(xpath = "//a[@class='btn btn-primary m-t-20 m-r-20 m-l-20 ng-binding']")
	private WebElement ibaModalTurnOff;

	public UNAVPage(WebDriver webDriver) {
		super(webDriver);
	}

//  ------------------------------------------------------- UNAV Page Load Verification --------------------------------
	/**
	 * Verify that the page loaded completely.
	 */
	public UNAVPage verifyUNAVPageLoaded() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			if (getDriver().getCurrentUrl().contains(pageUrl)) {
				verifyUNAVPageUrl();
				Reporter.log("UNAV page loaded");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("UNAV page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public UNAVPage verifyUNAVPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		Reporter.log("UNAV page URL verified");
		return this;
	}

	/***
	 * verifying the presence of search icon for the search text box
	 * 
	 * @return
	 */
	public UNAVPage verifySearchIconIsDisplayed() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(searchIcon),2);
			moveToElement(searchIcon);
			Assert.assertTrue(isElementDisplayed(searchIcon));
			Reporter.log("Search icon is displayed");
		} catch (Exception e) {
			Assert.fail("Search icon is displayed");
		}
		return this;
	}

	/***
	 * click on the search icon button
	 * 
	 * @return
	 */
	public UNAVPage clickSearchIcon() {
		try {
			elementClick(searchIcon);
			Reporter.log("Clicked on Search icon button");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Search icon button");
		}
		return this;
	}

	/***
	 * verifying the presence of search text box
	 * 
	 * @return
	 */
	public UNAVPage verifySearchTextBoxIsset() {
		try {
			checkPageIsReady();
			moveToElement(searchTextBox);
			Assert.assertTrue(isElementDisplayed(searchTextBox));
			searchTextBox.click();
			sendTextData(searchTextBox, "Apple");
			Reporter.log("Search text box is displayed and entered value");
		} catch (Exception e) {
			Assert.fail("Search text box is displayed");
		}
		return this;
	}

	/***
	 * click on the search icon button after input of store locator field value
	 * 
	 * @return
	 */
	public UNAVPage clickSearchIcon1() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchSubmit),2);
			elementClick(searchSubmit);
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on Search icon button");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Search icon button");
		}
		return this;
	}

	/***
	 * UNAV Header Links Testing
	 * 
	 * @return
	 */
	public UNAVPage testHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(phones),2);
			elementClick(phones);
			verifyPageUrl("www.t-mobile.com/cell-phones");
			Reporter.log("Clicked on phones Link and verified to navigate the phones page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(plans),2);
			elementClick(plans);
			verifyPageUrl("https://www.t-mobile.com/cell-phone-plans");
			Reporter.log("Clicked on plans Link and verified to navigate the plans page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(myTmobile),2);
			elementClick(myTmobile);
			verifyPageUrl("www.t-mobile.com");
			Reporter.log("Clicked on My-TMobile link and verified to navigate the t-mobile page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(menu),2);
			elementClick(menu);
			Reporter.log("Clicked on Menu Button");
			navigateBack();
		} catch (Exception e) {
			Assert.fail("Unable to click on the Header Links");
		}
		return this;
	}

	/***
	 * UNAV Footer Links Testing
	 * 
	 * @return
	 */
	public UNAVPage testFooter() {
		try {
			waitFor(ExpectedConditions.visibilityOf(contactUs),2);
			elementClick(contactUs);
			verifyPageUrl("www.t-mobile.com/contact-us");
			Reporter.log("Clicked on contactUs and verified to navigate the t-mobile contactUs page");
			navigateBack();
			elementClick(checkOrder);
			verifyPageUrl("https://secure-checkout.t-mobile.com/webapp/wcs/stores");
			Reporter.log("Clicked on checkOrder Link and verified to navigate the checkOrder stores page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(viewPolicy),2);
			elementClick(viewPolicy);
			verifyPageUrl("https://www.t-mobile.com/responsibility/legal/return-policy");
			Reporter.log("Clicked on viewPolicy Link and verified to navigate the viewPolicy page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(findStore),2);
			elementClick(findStore);
			verifyPageUrl("https://www.t-mobile.com/store-locator");
			Reporter.log("Clicked on findStore link and verified to navigate the t-mobile store-locator page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(tradeProgram),2);
			elementClick(tradeProgram);
			verifyPageUrl("https://www.t-mobile.com/cell-phone-trade-in.html");
			Reporter.log("Clicked on tradeProgram link and verified to navigate the t-mobile tradeProgram page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(support),2);
			elementClick(support);
			verifyPageUrl("https://support.t-mobile.com/welcome");
			Reporter.log("Clicked on support link and verified to navigate the t-mobile support page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(deviceSupport),2);
			elementClick(deviceSupport);
			verifyPageUrl("https://support.t-mobile.com/community/phones-tablets-devices");
			Reporter.log("Clicked on deviceSupport link and verified to navigate the t-mobile deviceSupport page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(questionBill),2);
			elementClick(questionBill);
			verifyPageUrl("https://support.t-mobile.com/community/account");
			Reporter.log("Clicked on questionBill link and verified to navigate the t-mobile questionBill page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(planService),2);
			elementClick(planService);
			verifyPageUrl("https://support.t-mobile.com/community/plans-services");
			Reporter.log("Clicked on planService link and verified to navigate the t-mobile planService page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(prepaidPhone),2);
			elementClick(prepaidPhone);
			verifyPageUrl("https://prepaid.t-mobile.com/bring-your-own-device");
			Reporter.log("Clicked on prepaidPhone link and verified to navigate the t-mobile prepaidPhone page");
			navigateBack();
			waitFor(ExpectedConditions.visibilityOf(business),2);
			elementClick(business);
			verifyPageUrl("https://www.t-mobile.com/business");
			Reporter.log("Clicked on business link and verified to navigate the t-mobile business page");
			navigateBack();
			explicitWait(internet, 5000);
			elementClick(internet);
			verifyPageUrl("https://iot.t-mobile.com");
			Reporter.log("Clicked on internet link and verified to navigate the t-mobile internet page");
			navigateBack();
			explicitWait(aboutus, 5000);
			elementClick(aboutus);
			verifyPageUrl("https://www.t-mobile.com/about-us");
			Reporter.log("Clicked on aboutus link and verified to navigate the t-mobile aboutus page");
			navigateBack();
			explicitWait(investor, 5000);
			elementClick(investor);
			verifyPageUrl("https://investor.t-mobile.com/investors/default.aspx");
			Reporter.log("Clicked on investor link and verified to navigate the t-mobile investor page");
			navigateBack();
			explicitWait(press, 5000);
			elementClick(press);
			verifyPageUrl("https://www.t-mobile.com/news");
			Reporter.log("Clicked on press link and verified to navigate the t-mobile news page");
			navigateBack();
			explicitWait(careers, 5000);
			elementClick(careers);
			verifyPageUrl("https://www.t-mobile.com/careers");
			Reporter.log("Clicked on careers link and verified to navigate the t-mobile careers page");
			navigateBack();
			explicitWait(deutsche, 5000);
			elementClick(deutsche);
			verifyPageUrl("https://www.telekom.com");
			Reporter.log("Clicked on deutsche link and verified to navigate the t-mobile page");
			navigateBack();
			explicitWait(puertorico, 5000);
			elementClick(puertorico);
			verifyPageUrl("https://www.t-mobilepr.com/");
			Reporter.log("Clicked on puertorico link and verified to navigate the t-mobile puertorico page");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Footer Links");
		}
		return this;
	}
	
//  ------------------------------------------------------- IBA Page Load Verification --------------------------------
	/**
	 * Verify that the page loaded completely.
	 */
	public UNAVPage verifyIBAPageLoaded() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			if (getDriver().getCurrentUrl().contains(ibapageUrl)) {
				verifyIBAPageUrl();
				Reporter.log("IBA page loaded");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("IBA page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public UNAVPage verifyIBAPageUrl() {
		waitFor(ExpectedConditions.urlContains(ibapageUrl),60);
		Reporter.log("IBA page URL verified");
		return this;
	}
	/***
	 * click on the Turn Off button
	 * 
	 * @return
	 */
	public UNAVPage clickTurnOff() {
		try {
			elementClick(ibaTurnOff);
			Reporter.log("Clicked on Turn Off button");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Turn Off button");
		}
		return this;
	}
	
	/***
	 * click on the Turn Off button in Modal
	 * 
	 * @return
	 */
	public UNAVPage clickModalTurnOff() {
		try {
			elementClick(ibaModalTurnOff);
			Reporter.log("Clicked on Turn Off button in Modal");
			Reporter.log("Verified to navigate the t-mobile /iba/turnoff page");
			waitFor(ExpectedConditions.urlContains("/iba/turnoff"));
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/iba/turnoff"),"Unable to navigate to /iba/turnoff when we click on Turn Off CTA ");
			Reporter.log("Navigated to /iba/turnoff when we click on Turn Off CTA");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Turn Off button in Modal");
		}
		return this;
	}

}