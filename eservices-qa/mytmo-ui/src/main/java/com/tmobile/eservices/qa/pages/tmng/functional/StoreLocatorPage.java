/**
 * 
 */
package com.tmobile.eservices.qa.pages.tmng.functional;

import static org.testng.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.google.common.collect.Ordering;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author csudheer
 * 
 *
 */
public class StoreLocatorPage extends TmngCommonPage {

	public StoreLocatorPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(xpath = "//p[contains(text(),'About the store')]//../div/div/div/p")
	private WebElement sprintRepairCenterBlurb;

	@FindBy(xpath = "(//p[contains(text(),'About the store')]/../div/p)[2]")
	private WebElement addNoSupportSprintBlurb;

	@FindBy(xpath = "//*[text()='Distance']")
	private WebElement selectDistanceFromDropDown;

	@FindBy(xpath = "//div[contains(text(),'Wait time')]")
	private WebElement selectWaitTimeFromDropDown;

	@FindBy(css = "button[ng-click*='FILTER']")
	private List<WebElement> filterButtn;

	@FindBy(xpath = "//img[contains(@role,'button')]")
	private List<WebElement> imagesList;

	@FindBy(xpath = "//li[@id='storesCarouselIndicators'][1]")
	private WebElement carousalIndicator;

	@FindBy(xpath = "//li[contains(@id,'storesCarouselIndicators')]")
	private List<WebElement> CarouselIndicators;

	@FindBy(xpath = "//a[text()='Store Locator']")
	private WebElement storeLocator;

	@FindBy(xpath = "//a[text()='Stores']")
	private WebElement mobileStoreLocator;

	@FindBy(css = ".store-locator-search-box")
	private WebElement searchTextBox;

	@FindBy(css = ".store-locator-search-submit")
	private WebElement searchIcon;

	@FindBy(xpath = "//img[@ng-click='vm.openModal($index)'][1]")
	private WebElement picturePreview;

	@FindBy(xpath = "//span[contains(@id,'storesLeftCarousel')]")
	private WebElement leftArrowIcon;

	@FindBy(xpath = "//span[contains(@id,'storesRightCarousel')]")
	private WebElement rightArrowIcon;

	@FindBy(xpath = "(//img[contains(@id,'storesCarousel')])[2]")
	private WebElement secondImageCaurosel;

	@FindBy(xpath = "(//img[contains(@id,'storesCarousel')])[1]")
	private WebElement firstImageCaurosel;

	@FindBy(xpath = "(//p[@id='store-address'])[1]")
	private WebElement firstStoreAddress;

	@FindBy(css = ".store-locator-results-store-item")
	private List<WebElement> searchResultSection;

	@FindBy(xpath = "//p[contains(text(),'We couldn')]")
	private WebElement noSprintRepairCenter;

	@FindBy(css = ".store-list-message div p")
	private WebElement loadedSearchSection;

	private String loadedSearchResultMsg = "Change sorting or filter by store type.";
	private String thirdPartyRetailers = "Third Party Retailers";
	private String tmobileStoremessage = "Showing T-Mobile branded stores";
	private String tPRTextOnStoreDetailPage = "T-MOBILE BRANDED STORE";
	private String tprTextOnMobileCarousel = "TPR STORE";
	private String expectedErrorMessage = "We couldn’t find a T-Mobile location in that area. Try widening your search and give it another shot.";
	private String noSprintRepairCenterMesage = "We couldn't find a Sprint Repair Center in that area.";

	@FindBy(css = "button[ng-click*='FILTER']")
	private WebElement filterButton;

	@FindBy(xpath = "//label[@aria-label='T-Mobile store']/../input[@type='checkbox']")
	private WebElement tMobileStoreInputCB;

	@FindBy(xpath = "//label[@aria-label='T-Mobile store']/../label")
	private WebElement tMobileStoreLabelCB;

	@FindBy(xpath = "//label[@aria-label='Authorized dealer']/../input[@type='checkbox']")
	private WebElement authorizedDealerInputCB;

	@FindBy(xpath = "//label[@aria-label='Authorized dealer']/../label")
	private WebElement authorizedDealerLabelCB;

	@FindBy(xpath = "//label[@aria-label='T-Mobile branded store']/../input[@type='checkbox']")
	private WebElement thirdPartyRetailerInputCB;

	@FindBy(xpath = "//label[@aria-label='T-Mobile branded store']/../label")
	private WebElement thirdPartyRetailerLabelCB;

	@FindBy(css = "button[aria-label='Apply']")
	private WebElement applyButton;

	@FindBy(css = "//span[contains(text(),'Authorized dealer')]")
	private List<WebElement> authorizedTagInResultBlades;

	private String tPRStoreTagName = "T-Mobile branded store";

	private String authorizedStoreTagName = "Authorized dealer";

	private String todaysHours = "Today’s Hours";

	private String inStoreWaitTimeInSearchBlades = "In-Store wait";

	private String appointmentsAvailable = "Appointments available";

	@FindBy(css = ".sl-search-pagination p")
	private WebElement paginationResult;

	@FindBy(css = ".store-locator-results-store-item")
	private List<WebElement> tprSearchBlade;

	@FindBy(id = "storeType")
	private WebElement tprStoreTypeOnStoreDetailPage;

	@FindBy(xpath = "//button[contains(text(),'Go back')]")
	private WebElement goBackButton;

	@FindBy(xpath = "//p[text()='T-Mobile branded store']")
	private WebElement thirdPartyFilterStore;

	@FindBy(xpath = "//a[contains(text(),'Stores')]")
	private WebElement storesOnOverlay;

	@FindBy(css = "button[class*='close-button']")
	private WebElement closeButton;

	@FindBy(xpath = "//p[text()='T-Mobile branded store']")
	private List<WebElement> tprStoreTypeOnMobileCarousel;

	private String type3Url = "filterBy=type3";

	@FindBy(xpath = "//p[contains(text(), 'We couldn’t find a T-Mobile location in that area. Try widening your search and give it another shot.')]")
	private WebElement actualErrorMessage;

	@FindBy(xpath = "//div[@class='ng-scope']//search-results-item[1]//div[1]")
	private WebElement clickTStore;

	@FindBy(xpath = "//div[@class='store-locator-details-thumbnail store-img m-t-20 ng-scope']")
	private WebElement storeLocatorThumnail;

	private final String pageUrl = "/store-locator";

	private final String sprintRepairCenterBlurbText = "Have a Sprint device that needs repair? Sprint Repair Centers can help resolve a variety of issues, with or without a device protection plan. T-Mobile devices are not currently supported at this service repair center.";

	private final String addNoSupportForSprintBlurb = "This store doesn’t currently support Sprint account management or transactions.";

	@FindBy(id = "storeName")
	private WebElement storeNameInSelectedSearchResult;

	@FindBy(xpath = "//span[contains(@class,'body--bold magenta')]/following-sibling::span")
	private List<WebElement> searchBladeStoreDistance;
	
	@FindBy(css = "[ng-if*='StoreWaitToggle'] span")
	private List<WebElement> searchBladeStoreWaitTime;

	@FindBy(css = "span[ng-if*='Distance']")
	private List<WebElement> searchBladeStoreWaitTime1;

	@FindBy(xpath = "//div[contains(text(),'Product Repair Center')]")
	private WebElement productRepairCenterOption;

	@FindBy(xpath = "//span[contains(text(),'Product Repair Center Inside')]/../../..")
	private List<WebElement> productRepairCenterResultBlades;

	@FindBy(xpath = "//span[contains(text(),'Product Repair Center Inside')]")
	private List<WebElement> productRepairCenterTextInResultBlade;

	@FindBy(xpath = "//span[contains(text(),'Product Repair Center Inside')]/../../p/b/following-sibling::span")
	private List<WebElement> distanceInProductRepairCenter;

	@FindBy(css = "img[src*='/repair_icon']")
	private List<WebElement> productRepairCenterIcon;

	@FindBy(xpath = "//span[contains(text(),'Product Repair Center Inside')]")
	private WebElement productRepairCenterInStoreDetails;

	@FindBy(id = "firstName")
	private WebElement firstName;

	@FindBy(id = "lastName")
	private WebElement lastName;

	@FindBy(id = "reason")
	private WebElement reasonTextBox;

	@FindBy(css = "select[aria-label='Reason for your visit'] option")
	private List<WebElement> reasonListOfValues;

	@FindBy(css = "label[for='additionalReason']")
	private WebElement additionalReasonTextBox;

	@FindBy(css = "select[aria-label='Additional reason for your visit'] option")
	private List<WebElement> additionalReasonListOfValues;

	@FindBy(id = "phoneNumber")
	private WebElement phoneNumber;

	@FindBy(css = "p[aria-label='Enter your first name']")
	private WebElement firstNameErrorMessage;

	@FindBy(css = "p[aria-label='Enter your last name']")
	private WebElement lastNameErrorMessage;

	@FindBy(css = "p[aria-label='Please select a reason for your visit']")
	private WebElement reasonForVisitErrorMessage;

	@FindBy(xpath = "//p[contains(text(),'Enter your email address')]")
	private WebElement emailErrorMessage;

	@FindBy(xpath = "//p[contains(text(),'Enter a valid email address')]")
	private WebElement invalidEmailErrorMessage;

	@FindBy(css = "p[aria-label*='valid phone number']")
	private WebElement phoneNumberErrorMessage;

	@FindBy(css = "p[aria-label='Enter a valid phone number']")
	private WebElement invalidPhoneNumberErrorMessage;

	@FindBy(xpath = "//input[@name='notifyTurn']")
	private WebElement notifyCheckbox;

	@FindBy(id = "getInLineFormNotifyLabel")
	private WebElement notifyCheckboxText;

	@FindBy(xpath = "//label[@id='getInLineFormNotifyLabel']/following-sibling::p/span")
	private WebElement notifyCheckboxContent;

	private final String notifyValue = "(We’ll also remind you to reschedule if you miss your appointment. Provide your email or phone number to agree we can notify you via email or automated text message. Agreement is not required for purchase.)";

	@FindBy(id = "notifyMethod")
	private WebElement notifyDropDown;

	@FindBy(id = "phone")
	private WebElement phoneNumberTextBox;

	@FindBy(id = "email")
	private WebElement emailTextBox;

	@FindBy(xpath = "//select[@id='notifyMethod']/option")
	private List<WebElement> notifyDropDownValues;

	@FindBy(id = "getInLine")
	private WebElement getInLineCTAInStoreDetails;

	@FindBy(xpath = "//span[@ng-if='vm.item.todayHours']/span[@class='ng-binding']")
	private WebElement storeTimings;

	@FindBy(id = "getInLineSubmit")
	private WebElement getInLineCTA;

	@FindBy(xpath = "//h1/span")
	private WebElement getInLineSuccessModal;

	@FindBy(id = "storeName")
	private WebElement storeNameInGetInLineSuccessModal;

	@FindBy(css = "div.sl-form-group.ng-scope [for*='English']")
	private WebElement englishRadioButton;

	@FindBy(id = "Yes")
	private WebElement yesRadioButton;

	@FindBy(css = "[for*='userConsent'] p")
	private WebElement legalTermContentCheckbox;

	@FindBy(xpath = "//span[contains(text(),'Indicates required field')]")
	private WebElement requiredFieldContent;

	@FindBy(xpath = "//span[contains(text(),'Standard text messaging rates may apply')]")
	private WebElement textMessagingContent;

	@FindBy(css = "#storeDescription")
	private WebElement aboutTheStoreDescription;

	@FindBy(css = "#map")
	private WebElement storeLocatorMap;

	@FindBy(css = "p[ng-if*='inStoreAppointment']")
	private List<WebElement> changeStoreWhichHasAppointments;

	@FindBy(css = "button[aria-label='Zoom in']")
	private WebElement zoomInButtonOnMap;

	@FindBy(css = "#store-name")
	private List<WebElement> nearbyStoresList;

	@FindBy(css = "#inStoreWaitSection")
	private WebElement inStoreWait;

	@FindBy(css = "#directions")
	private WebElement directionsCta;

	@FindBy(css = "#appointment")
	private WebElement appointmentCta;

	@FindBy(css = "div.store-locator-reserve")
	private WebElement selectDataAndTimeTable;

	@FindBy(css = "button[ng-click*='vm.onClickTime']")
	private List<WebElement> selectTimeSlot;

	@FindBy(css = "button[disabled='disabled'][ng-click='vm.submitSelectedAppointment()']")
	private WebElement disabledNextCTAOnTimeandDateModal;

	@FindBy(css = "button[ng-click='vm.submitSelectedAppointment()']")
	private WebElement nextCTAOnTimeandDateModal;

	@FindBy(css = ".mat-paginator-navigation-next")
	private WebElement nextStoresPageButton;

	@FindBy(css = ".mat-paginator-navigation-previous")
	private WebElement previousStoresPageButton;

	@FindBy(css = "input[placeholder='Find a store']")
	private WebElement searchInputHintText;

	@FindBy(css = "input[placeholder='Enter city/state or zip code']")
	private WebElement searchInputHintTextAfterFocus;

	@FindBy(css = ".sl-search-input b")
	private WebElement SortingResultsByDropDown;

	@FindBy(xpath = "//span[contains(text(),'T-Mobile branded store')]")
	private List<WebElement> tprTagInSearchResults;

	@FindBy(xpath = "//span[contains(@class,'body--bold magenta')]")
	private List<WebElement> storeNameInSearchBlades;

	@FindBy(xpath = "//span[contains(@class,'body--bold magenta')]/../following-sibling::p/span[contains(@aria-label,'Today’s Hours')]")
	private List<WebElement> todaysHoursInSearchBlades;

	@FindBy(xpath = "//span[contains(text(),'In-Store wait')]/../following-sibling::p/span[contains(text(),'Appointments available')]")
	private List<WebElement> appointmentsAvailableInSearchBlades;

	@FindBy(xpath = "//p[contains(@class,'body mat-line')]/span")
	private List<WebElement> storeAddress;
	
	@FindBy(css = "[ng-click*='next']")
	private WebElement nextBtn;
	
	@FindBy(css = "#aboutStoreHeader")
	private WebElement storeHeader;

	/***
	 * click on store locator link
	 * 
	 * @return
	 */
	public StoreLocatorPage clickStoreLocaterLink() {
		try {
			storeLocator.click();
			waitForSpinnerInvisibility();
			Reporter.log("Navigated to Store Locator Page");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Store locator link");
		}
		return this;
	}

	/***
	 * click on store locator link in mobile mode
	 * 
	 * @return
	 */
	public StoreLocatorPage clickMobileStoreLocaterLink() {
		try {
			checkPageIsReady();
			mobileStoreLocator.click();
			waitForSpinnerInvisibility();
			Reporter.log("Navigated to Store Locator Page");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Store locator link");
		}
		return this;
	}

	/***
	 * verifying the presence of search text box on store locator page
	 * 
	 * @return
	 */
	public StoreLocatorPage verifySearchTextBoxIsDisplayed() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(searchTextBox),30);
			moveToElement(searchTextBox);
			Assert.assertTrue(isElementDisplayed(searchTextBox), "Search text box is not displayed");
			Reporter.log("Search text box is displayed on store locator page");
		} catch (Exception e) {
			Assert.fail("Search text box is not displayed");
		}
		return this;
	}

	/***
	 * verifying the presence of search icon for the search text box
	 * 
	 * @return
	 */
	public StoreLocatorPage verifySearchIconIsDisplayed() {
		try {
			checkPageIsReady();
			moveToElement(searchIcon);
			Assert.assertTrue(isElementDisplayed(searchIcon), "Search icon is not displayed");
			Reporter.log("Search icon is displayed for the search text box");
		} catch (Exception e) {
			Assert.fail("Failed to verify Search icon");
		}
		return this;
	}

	/***
	 * click on the search icon button after input of store locator field value
	 * 
	 * @return
	 */
	public StoreLocatorPage clickSearchIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchIcon));
			searchIcon.click();
			Reporter.log("Clicked on Search icon button");
		} catch (Exception e) {
			Reporter.log("Click on search icon failed");
			Assert.fail("Click on search icon failed");
		}
		return this;
	}

	/***
	 * click on the photo
	 * 
	 * @return
	 */
	public StoreLocatorPage clickphotoPreview() {
		try {
			checkPageIsReady();
			picturePreview.click();
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on picture");
		} catch (Exception e) {
			Assert.fail("Unable to click on the picture icon");
		}
		return this;
	}

	/***
	 * verified next image successfully
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyRightNavigateToNextImage() {
		try {
			checkPageIsReady();
			clickElement(rightArrowIcon);
			checkPageIsReady();
			Assert.assertTrue(secondImageCaurosel.isDisplayed());
			Reporter.log("next picture is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to validate photo caursoel navigation");
		}
		return this;
	}

	/***
	 * verified previous image successfully
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyLeftNavigateToNextImage() {
		try {
			checkPageIsReady();
			clickElement(leftArrowIcon);
			checkPageIsReady();
			Assert.assertTrue(firstImageCaurosel.isDisplayed());
			Reporter.log("previous  picture is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to validate photo caursoel navigation");
		}
		return this;
	}

	/***
	 * click on the left arrow in photo caurosel
	 * 
	 * @return
	 */
	public StoreLocatorPage clickLeftArrow() {
		try {
			checkPageIsReady();
			clickElement(leftArrowIcon);
			Reporter.log("Clicked on left arrow in photo caurosel");
		} catch (Exception e) {
			Assert.fail("Unable to click on the left arrow ");
		}
		return this;
	}

	/***
	 * click on the right arrow in photo caurosel
	 * 
	 * @return
	 */
	public StoreLocatorPage clickRightArrow() {
		try {
			checkPageIsReady();
			clickElement(rightArrowIcon);
			Reporter.log("Clicked on right arrow in photo caurosel");
		} catch (Exception e) {
			Assert.fail("Unable to click on the right arrow ");
		}
		return this;
	}

	/***
	 * verifies images are displayed
	 * 
	 * @return
	 */
	public int verifyImageIsDisplayed() {
		try {
			checkPageIsReady();
			firstStoreAddress.click();
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOfAllElements(imagesList),30);
			for (WebElement image : imagesList) {
				Assert.assertTrue(image.isDisplayed());

			}
			Reporter.log("Image is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to validate picture");
		}
		return imagesList.size();
	}

	/***
	 * verifies number of images and dots
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyDotsEqualsImages(int numberOfImages) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(carousalIndicator));
			Assert.assertEquals(numberOfImages, CarouselIndicators.size());
			Reporter.log("Number of images equals to number of Dots");
		} catch (Exception e) {
			Assert.fail("Unable to validate picture");
		}
		return this;
	}

	/***
	 * verify if the image src is coming from CDN
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyImagesForCdn() {
		try {
			checkPageIsReady();
			for (WebElement image : imagesList) {
				Assert.assertTrue(image.getAttribute("src").contains("mktgcdn.com"));
			}
			Reporter.log("image source is  coming from CDN");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Search icon button");
		}
		return this;
	}

	/***
	 * inputs value in the store locator search text box
	 * 
	 * @param searchValue
	 * @return
	 */
	public StoreLocatorPage setTextIntoTheSearchTextBox(String searchValue) {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchTextBox));
			sendTextData(searchTextBox, searchValue);
			Reporter.log("Entering value " + searchValue + " in the search text box is success");
		} catch (Exception e) {
			Assert.fail("Entering value " + searchValue + " in the search text box is failed");
		}
		return this;
	}

	/***
	 * verification of search results section upon search
	 * 
	 * @return
	 */
	public StoreLocatorPage verifySearchResultSectionIsDisplayed() {
		try {
			checkPageIsReady();
			waitforSpinner();
			Assert.assertTrue(searchResultSection.size() > 0,
					"Failed to load the search results with the given search criteria");
			Reporter.log("Search results are loaded for the given search criteria");
		} catch (Exception e) {
			Assert.fail("Failed to load the search results with the given search criteria");
		}
		return this;
	}

	/***
	 * verification of sprint repair center blurb
	 * 
	 * @return
	 */
	public StoreLocatorPage verifySprintRepairCenterBlurb() {
		try {
			checkPageIsReady();
			waitforSpinner();
			for (WebElement searchBlade : searchResultSection) {
				if (searchBlade.getText().contains("Product Repair Center")) {
					searchBlade.click();
					waitFor(ExpectedConditions.visibilityOf(sprintRepairCenterBlurb), 60);
					Assert.assertTrue(sprintRepairCenterBlurb.isDisplayed());
					break;
				}
			}

			Reporter.log("center blurb is displayed successfully");
		} catch (Exception e) {
			Assert.fail("failed to validate sprint repair center blurb");
		}
		return this;
	}

	/***
	 * verification of add no support for sprint blurb
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyAddNoSupportForSprintBlurb() {
		try {
			checkPageIsReady();
			waitforSpinner();
			for (WebElement searchBlade : searchResultSection) {
				if ((searchBlade.getText().contains("Product Repair Center"))) {
					searchBlade.click();
					waitFor(ExpectedConditions.visibilityOf(addNoSupportSprintBlurb), 60);
					Assert.assertTrue(addNoSupportSprintBlurb.isDisplayed());
					break;
				}
			}

			Reporter.log("center blurb for add no support is displayed successfully");
		} catch (Exception e) {
			Assert.fail("failed to validate add sprint repair center blurb");
		}
		return this;
	}

	/***
	 * verification of no sprint repair center message text
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyMessageForNoProductSprintRepairCentre() {
		try {
			int count = 0;
			checkPageIsReady();
			waitforSpinner();
			for (WebElement searchBlade : searchResultSection) {
				if ((searchBlade.getText().contains("Product Repair Center"))) {
					count++;
				}
			}
			if (count == 0) {
				Assert.assertEquals(noSprintRepairCenter.getText(), noSprintRepairCenterMesage);
			}
			Reporter.log("'No product repair center' text is displayed");
		} catch (Exception e) {
			Reporter.log("'No product repair center' text not displayed");
			Assert.fail("'No product repair center' text not displayed");
		}
		return this;
	}

	/***
	 * verification of store types display section
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyLoadedSearchSectionIsDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementByText(loadedSearchSection, loadedSearchResultMsg),
					"Failed to verify the section that displays the store types loaded");
			Reporter.log("'" + loadedSearchResultMsg + "' Section is displayed with store type results fetched");
		} catch (Exception e) {
			Assert.fail("Failed to verify the section that displays the store types loaded");
		}
		return this;
	}

	/***
	 * click filter button
	 * 
	 * @return
	 */
	public StoreLocatorPage clickFilterButton() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(filterButton), "Unable to click on the Filter button");
			Reporter.log("Filter button is displayed");
			filterButton.click();
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on Filter button");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Filter button");
		}
		return this;
	}

	/***
	 * method to check or uncheck the T-Mobile Store check box
	 * 
	 */
	public StoreLocatorPage checkOrUncheckTMobileStoreFilterOption(String expectedCheckState) {
		try {
			checkPageIsReady();
			String actualCheckedState = tMobileStoreInputCB.getAttribute("aria-checked").toString();
			if (!actualCheckedState.equals(expectedCheckState)) {
				tMobileStoreLabelCB.click();
				waitForSpinnerInvisibility();
				Reporter.log("T-Mobile Store check box is set to " + expectedCheckState);
			} else
				Reporter.log("T-Mobile Store check box is already set to " + expectedCheckState);
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for Tmobile Store check box");
		}
		return this;
	}

	/***
	 * method to check un check the Authorized dealer check box
	 * 
	 */
	public StoreLocatorPage checkOrUncheckAuthorizedDealerFilterOption(String expectedCheckState) {
		try {
			checkPageIsReady();
			String actualCheckedState = authorizedDealerInputCB.getAttribute("aria-checked").toString();
			if (!actualCheckedState.equals(expectedCheckState)) {
				authorizedDealerLabelCB.click();
				waitForSpinnerInvisibility();
				Reporter.log("Authorized dealer check box is set to " + expectedCheckState);
			} else
				Reporter.log("Authorized dealer check box is already set to " + expectedCheckState);
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for Authorized dealer check box");
		}
		return this;
	}

	/***
	 * method to check uncheck the Third Party Retailer check box
	 * 
	 */
	public StoreLocatorPage checkOrUncheckThirdPartyRetailerFilterOption(String expectedCheckState) {
		try {
			checkPageIsReady();
			String actualCheckedState = thirdPartyRetailerInputCB.getAttribute("aria-checked").toString();
			if (!actualCheckedState.equals(expectedCheckState)) {
				thirdPartyRetailerLabelCB.click();
				waitForSpinnerInvisibility();
				Reporter.log("Third Party Retailer check box is set to " + expectedCheckState);
			} else
				Reporter.log("Third Party Retailer check box is already set to " + expectedCheckState);
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for Third Party Retailer check box");
		}
		return this;
	}

	/***
	 * click on the Apply button after the filter selection
	 */
	public StoreLocatorPage clickApplyButton() {
		try {
			checkPageIsReady();
			applyButton.click();
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on Apply button");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Apply button");
		}
		return this;
	}

	/***
	 * verify result header section refined to Third party retailers
	 */
	public StoreLocatorPage verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementByText(loadedSearchSection, tmobileStoremessage),
					"Failed to verify the section that displays the store types refined");
			Reporter.log("The loaded search result section contains header with results refined for only "
					+ thirdPartyRetailers);
		} catch (Exception e) {
			Assert.fail("Failed to verify the section that displays the store types refined");
		}
		return this;
	}

	/***
	 * validation of Third party retailer store tags in the search result blades
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyThirdPartyRetailersTagInSearchResultBlades() {
		try {
			checkPageIsReady();
			int counterForTagNameText = 0;
			for (WebElement ele : authorizedTagInResultBlades) {
				if (ele.getText().toString().equals(tPRStoreTagName))
					counterForTagNameText++;
			}
			String paginationText = paginationResult.getText().toString();
			String[] resAfterSplit1 = paginationText.split("-");
			String[] resAfterSplit2 = resAfterSplit1[1].split(" ");
			Assert.assertEquals(String.valueOf(counterForTagNameText), resAfterSplit2[0],
					"Failed to verify the presence of third party retailers tag in the search result blades");
			Reporter.log("All the search result blades contain the tag Third Party Retailers");

		} catch (Exception e) {
			Assert.fail("Failed to verify the presence of third party retailers tag in the search result blades");
		}
		return this;
	}

	/***
	 * click on Third Party Retailer Search Blade
	 * 
	 * @return
	 */
	public StoreLocatorPage clickOnTprSearchBlade() {
		try {
			checkPageIsReady();
			boolean isTPR = false;
			for (WebElement ele : authorizedTagInResultBlades) {
				if (ele.getText().toString().equals(tPRStoreTagName)) {
					clickElement(ele);
					Reporter.log("Clicked on the store type as TPR");
					isTPR = true;
					break;
				}
			}
			if (!isTPR) {
				Assert.fail("No TPR stores are available");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify the presence of third party retailers tag in the search result blades");
		}
		return this;
	}

	/**
	 * verify Third Party Retailer store type on store detail page
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyStoreTypeOnStoreDetailPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementByText(tprStoreTypeOnStoreDetailPage, tPRTextOnStoreDetailPage),
					"'Third Party Retailer' Store type is not displayed on Store Detail page for TPR search blade");
			Reporter.log("'Third Party Retailer' Store type is displayed on Store Detail page for TPR search blade");
		} catch (Exception e) {
			Assert.fail("'Third Party Retailer' Store type is not displayed on Store Detail page for TPR search blade");
		}
		return this;
	}

	/***
	 * click on Go Back button on Store Detail Page
	 * 
	 * @return
	 */
	public StoreLocatorPage clickOnGoBackButton() {
		try {
			checkPageIsReady();
			goBackButton.click();
			Reporter.log("Clicked on Go Back button on Store Detail Page");
		} catch (Exception e) {
			Assert.fail("Unable to click on Go Back button");
		}
		return this;
	}

	/**
	 * verifies third party store after clicking filter
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyThirdPartyFilterStore() {
		try {
			checkPageIsReady();
			Assert.assertTrue(thirdPartyFilterStore.isDisplayed(),
					"Third party Retailer filter store is not displayed on Filter screen");
			Reporter.log("Third party Retailer filter store is displayed on Filter screen");

		} catch (Exception e) {
			Assert.fail("Third party Retailer filter store is not displayed on Filter screen");
		}
		return this;
	}

	/***
	 * verify result header section not refined to Third party retailers
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyThirdPartyRetailerTextNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(!verifyElementByText(loadedSearchSection, tmobileStoremessage),
					"Failed to verify the section that displays the store types refined");
			Reporter.log("The loaded search result section does not contains header with results refined for only "
					+ thirdPartyRetailers);
		} catch (Exception e) {
			Assert.fail("Failed to verify the section that displays the store types refined");
		}
		return this;
	}

	/**
	 * verify text 'type 3' is present in URL after clicking third party retailer
	 * checkbox in filter tab
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyType3InUrl() {
		try {
			checkPageIsReady();
			String uRL = getDriver().getCurrentUrl();
			verifyCurrentPageURL(uRL, type3Url);
			Reporter.log(
					"URL contains text 'filterBy=type3' after selecting third party retailer check box  " + type3Url);
		} catch (Exception e) {
			Assert.fail("URL contains text 'filterBy=type3' after selecting third party retailer check box");
		}
		return this;
	}

	/***
	 * click on Stores link on overlay
	 * 
	 * @return
	 */
	public StoreLocatorPage clickOnStoresLinkOnOverlay() {
		try {
			checkPageIsReady();
			storesOnOverlay.click();
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on Stores link on Overlay Menu page");
			Reporter.log("Navigated to Store Locator Page");
		} catch (Exception e) {
			Assert.fail("Unable to click on the Store locator link");
		}
		return this;
	}

	/***
	 * click on Close button
	 * 
	 * @return
	 */
	public StoreLocatorPage clickOnCloseButton() {
		try {
			checkPageIsReady();
			closeButton.click();
			Reporter.log("Clicked on Close button");
		} catch (Exception e) {
			Assert.fail("Unable to click on Close button");
		}
		return this;
	}

	/**
	 * verify count of Third Party Retailer search blades
	 * 
	 * @return
	 */
	String[] resAfterSplit2;

	public StoreLocatorPage verifyTotalCountOfTprSearchBlades() {
		try {
			checkPageIsReady();
			int counterForTagNameText = 0;
			for (WebElement ele : tprStoreTypeOnMobileCarousel) {
				if (ele.getText().toString().equals(tprTextOnMobileCarousel))
					counterForTagNameText++;
			}
			String paginationText = paginationResult.getText().toString();
			String[] resAfterSplit1 = paginationText.split("-");
			resAfterSplit2 = resAfterSplit1[1].split(" ");
			Reporter.log("Total count of TPR search blades: " + resAfterSplit2[2]);
		} catch (Exception e) {
			Assert.fail("Failed to retrieve search results of Third Party Retailer");
		}
		return this;
	}

	/***
	 * validation of Third party retailer store type on Mobile Carousel
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyStoreTypeOnMobileCarousel() {
		try {
			checkPageIsReady();
			Assert.assertEquals(tprStoreTypeOnMobileCarousel.size(), Integer.parseInt(resAfterSplit2[2]),
					"Failed to verify the presence of third party retailers store type on Mobile Carousel");
			Reporter.log("All TPR search results on Mobile Carousel contains 'Third Party Retailers' store type");
		} catch (Exception e) {
			Assert.fail("Failed to verify the presence of third party retailers store type on Mobile Carousel");
		}
		return this;
	}

	/***
	 * click on City Search Blade
	 *
	 * @return
	 */
	public StoreLocatorPage clickOnCitySearchBlade(String cityName) {
		try {
			checkPageIsReady();
			String xpathForCity = "//button[contains(@class,'city')]/b[text()='" + cityName + "']";
			getDriver().findElement(By.xpath(xpathForCity)).click();
			Reporter.log("Clicked on a Third Party Retailer Search Blade with " + cityName);

		} catch (Exception e) {
			Assert.fail("Unable to click on Third Party Retailer Search Blade");
		}
		return this;
	}

	/***
	 * verification of error message display when no search results fetched
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyErrorMessageDisplayForNoResults() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementByText(actualErrorMessage, expectedErrorMessage),
					"Failed to display an error message when no search results are fetched ");
			Reporter.log("'" + expectedErrorMessage + "' message is displayed when no search results are fetched");
			Reporter.log("Error message has been verified for no search results of TPR store");
		} catch (Exception e) {
			Assert.fail("Failed to display an error message when no search results are fetched ");
		}
		return this;
	}

	/***
	 * click on T-Mobile Store
	 */
	public StoreLocatorPage clickTMobileStore() {
		try {
			checkPageIsReady();
			clickTStore.click();
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on T-Mobile Store");
			waitFor(ExpectedConditions.visibilityOf(storeLocatorThumnail), 60);
			storeLocatorThumnail.isDisplayed();
			Reporter.log("Thumnail Image is getting displayed for T-Mobile Store");
		} catch (Exception e) {
			Assert.fail("Unable to click on T-Mobile Store");
		}
		return this;
	}

	/**
	 * verify store locator default page is loaded for invalid stores deeplink page
	 * 
	 * @return
	 */
	public StoreLocatorPage verifystorelocatorUrl() {
		try {
			checkPageIsReady();
			String uRL = getDriver().getCurrentUrl();
			getDriver().navigate().to(System.getProperty("environment") + "/store-locator/invalidstore");
			verifyCurrentPageURL(uRL, System.getProperty("environment") + "/store-locator");
			Reporter.log("Store Locator default link is loaded for invalid store deeplink");
		} catch (Exception e) {
			Assert.fail("Store Locator default link is not loaded for invalid store deeplink" + e.getMessage());
		}
		return this;
	}

	/***
	 * Verify the display of store locator page
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyStoreLocatorPage() {
		waitForSpinnerInvisibility();
		waitFor(ExpectedConditions.urlContains(pageUrl), 60);
		getDriver().switchTo().frame("pccIFrame");
		waitFor(ExpectedConditions.visibilityOf(storeLocatorMap), 60);
		getDriver().switchTo().parentFrame();
		scrollDownToelement(directionsCta);
		for (WebElement availableStoe : changeStoreWhichHasAppointments) {
			if (isElementDisplayed(availableStoe)) {
				clickElementWithJavaScript(availableStoe);
				break;
			}
		}
		Reporter.log("Store Locator page is displayed");
		return this;
	}

	/**
	 * Select second store in search results
	 */
	public StoreLocatorPage selectSecondStoreInSearchResults() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(searchResultSection), 60);
			searchResultSection.get(0).click();
			Reporter.log("Store selected successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to select Store from the list");
		}
		return this;
	}

	/**
	 * Verify store name in Store Details page
	 */
	public StoreLocatorPage verifyStoreNameInStoreDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(storeNameInSelectedSearchResult), 30);
			Assert.assertTrue(storeNameInSelectedSearchResult.isDisplayed());
			Reporter.log("Store name is displayed successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to display Store name");
		}
		return this;
	}

	/**
	 * Verify Store name in GetInLine form
	 *
	 */
	public StoreLocatorPage verifyStoreNameInGetInLineForm(String storeName) {
		try {
			checkPageIsReady();
			Assert.assertTrue(storeNameInSelectedSearchResult.getText().equalsIgnoreCase(storeName),
					"Selected store is not displaying");
			Reporter.log("Store name value successfully verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify Store name value");
		}
		return this;
	}

	/***
	 * validation of First party retailer store tags in the search result blades
	 *
	 * @return
	 */
	public StoreLocatorPage verifyThirdPartyRetailersTagInCitySearchResultBlades() {
		try {
			checkPageIsReady();
			boolean isTPR = false;
			for (WebElement ele : tprTagInSearchResults) {
				if (ele.getText().toString().equals(tPRStoreTagName)) {
					Reporter.log("TPR search result blades contain the tag Third Party Retailers");
					isTPR = true;
					break;
				}
			}
			if (!isTPR) {
				Assert.fail("No TPR stores are available for the used search criteria");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify the presence of third party retailers tag in the search result blades");
		}
		return this;
	}

	/**
	 * Verify Product Repair Center in the drop down
	 *
	 */
	public StoreLocatorPage verifyProductRepairCenterInSortDropDown() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(productRepairCenterOption));
			Reporter.log("Product Repair center is displayed successfully in the drop down");
		} catch (Exception e) {
			Assert.fail("Unable to display Product Repair center in the drop down");
		}
		return this;
	}

	/**
	 * Select Product Repair Center from the drop down
	 *
	 */
	public StoreLocatorPage selectProductRepairCenterFromDropDown() {
		try {
			checkPageIsReady();
			productRepairCenterOption.click();
			Reporter.log("Selecting  product repair center menu from sort dropdown is success");
		} catch (Exception e) {
			Reporter.log("Selecting  product repair center menu from sort dropdown failed");
			Assert.fail("Selecting  product repair center menu from sort dropdown failed");
		}
		return this;
	}

	/***
	 * verify search blades are sorted by Product repair center which in turn are
	 * sorted by distance
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyProductRepairCenterInSearchBlades() {
		try {
			checkPageIsReady();
			Assert.assertTrue(searchResultSection.size() > 0,
					"Failed to load the search results with the given search criteria");
			Reporter.log("Search results are loaded for the respective sort");
			ArrayList<String> prcWithDistance = new ArrayList<String>();
			for (int i = 0; i < searchResultSection.size(); i++) {
				if (searchResultSection.get(i).getText().contains("Product")) {
					if (searchResultSection.get(i).getText().contains("miles")) {
						String miles = searchBladeStoreDistance.get(i).getText();
						String milesOfAllBlades[] = miles.split(" ");
						prcWithDistance.add(milesOfAllBlades[2]);
					} else {
						Reporter.log("No distance value shown in search blade");
					}
				}
			}
			for (int i = 0; i < prcWithDistance.size() - 1; i++) {
				if (prcWithDistance.get(i).compareTo(prcWithDistance.get(i + 1)) > 0) {
					break;
				}
			}
			Reporter.log("Distance shown in search blade are in sorted order for Product repair center stores");
		} catch (Exception e) {
			Assert.fail("Failed to load the search results with sorting by Product Repair Center");
		}
		return this;
	}

	/***
	 * verify other stores followed by stores with Product repair center are sorted
	 * by distance
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyOtherSearchBladesSortedByDistance() {
		try {
			checkPageIsReady();
			ArrayList<String> othersWithDistance = new ArrayList<String>();
			for (int i = 0; i < searchResultSection.size(); i++) {
				if (!searchResultSection.get(i).getText().contains("Product")) {
					String miles = searchBladeStoreDistance.get(i).getText();
					String milesOfAllBlades[] = miles.split(" ");
					othersWithDistance.add(milesOfAllBlades[2]);
				}
			}
			for (int i = 0; i < othersWithDistance.size() - 1; i++) {
				if (othersWithDistance.get(i).compareTo(othersWithDistance.get(i + 1)) > 0) {
					break;
				}
			}
			Reporter.log("After sorting with Product repair center, remaining blades are sorted by distance");
		} catch (Exception e) {
			Assert.fail("Failed to load the search results with sorting by Product Repair Center");
		}
		return this;
	}

	/**
	 * Verify Repair icon
	 *
	 */
	public StoreLocatorPage verifyRepairIcon() {
		try {
			checkPageIsReady();
			for (int i = 0; i < productRepairCenterTextInResultBlade.size(); i++) {
				if (productRepairCenterTextInResultBlade.get(i).isDisplayed()) {
					Assert.assertTrue(productRepairCenterIcon.get(i).isDisplayed());
				}
			}
			Reporter.log("Repair icon is displayed in the search blades for Product Repair center");
		} catch (Exception e) {
			Assert.fail("Unable to display Repair icon");
		}
		return this;
	}

	/**
	 * Click on search blade with Product Repair Center
	 *
	 */
	public StoreLocatorPage clickOnSearchBladeWithPRC() {
		try {
			checkPageIsReady();
			Assert.assertTrue(productRepairCenterTextInResultBlade.get(0).isDisplayed(),
					"Search blade with Product Repair Center is not displayed");
			productRepairCenterTextInResultBlade.get(0).click();
			Reporter.log("Store detail page is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to click on Search blade with Product Repair center and display Store details page");
		}
		return this;
	}

	/**
	 * Verify Product Repair Center Inside In Store Details page
	 *
	 */
	public StoreLocatorPage verifyProductRepairCenterInStoreDetails() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(productRepairCenterInStoreDetails));
			Reporter.log("'Product Repair center Inside' text is displayed in Store details page");
		} catch (Exception e) {
			Assert.fail("Unable to display 'Product Repair center Inside' text in Store details page");
		}
		return this;
	}

	/***
	 * verify filter not available
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyFilterNotVisible() {
		try {
			checkPageIsReady();
			Assert.assertEquals(filterButtn.size(), 0);
			Reporter.log("Filter button is not available");
		} catch (Exception e) {
			Assert.fail("Unable to validate Filter button");
		}
		return this;
	}

	/**
	 * To get Store name from store details page
	 *
	 */
	public String getStoreNameInStoreDetailsPage() {
		return storeNameInSelectedSearchResult.getText();
	}

	/**
	 * Click on Get In Line Button in store Details
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyAndclickOnGetInLineCTAInStoreDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(getInLineCTAInStoreDetails));
			getInLineCTAInStoreDetails.isEnabled();
			clickElementWithJavaScript(getInLineCTAInStoreDetails);
			Reporter.log("'Get in line' CTA was highlighted and clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Get in line' CTA");
		}
		return this;
	}

	/**
	 * Verify field names in Get in line Modal window
	 */
	public StoreLocatorPage verifyFieldNamesInGetInLine() {
		checkPageIsReady();
		try {
			firstName.isDisplayed();
			lastName.isDisplayed();
			reasonTextBox.isDisplayed();
			additionalReasonTextBox.isDisplayed();
			Reporter.log("Verified text fields successfully in Get In Line form.");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify text fields in Get In Line form");
		}
		return this;
	}

	/**
	 * Enter first name
	 */
	public StoreLocatorPage enterFirstNameInGetInLine(TMNGData tMNGData) {
		checkPageIsReady();
		try {
			firstName.clear();
			firstName.sendKeys(tMNGData.getFirstName());
			Reporter.log("Entered first name in Get in line form");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to enter first name ");
		}
		return this;
	}

	public String retrieveFirstName() {
		return firstName.getAttribute("value");
	}

	/**
	 * Verify First Name Maximum Limit
	 *
	 */
	public StoreLocatorPage verifyFirstNameMaxLimit() {
		try {
			Assert.assertEquals(firstName.getAttribute("value").length(), 40);
			Reporter.log("First name allows maximum 40 characters");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to validate the maximum length of First name");
		}
		return this;
	}

	/**
	 * Enter last name
	 */
	public StoreLocatorPage enterLastNameInGetInLine(TMNGData tMNGData) {
		checkPageIsReady();
		try {
			lastName.clear();
			lastName.sendKeys(tMNGData.getLastName());
			Reporter.log("Entered last name in Get in line form");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to enter last name ");
		}
		return this;
	}

	/**
	 * Verify Last Name Maximum Limit
	 *
	 */
	public StoreLocatorPage verifyLastNameMaxLimit() {
		try {
			Assert.assertEquals(lastName.getAttribute("value").length(), 40);
			Reporter.log("Last name allows maximum 40 characters");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to validate the maximum length of Last name");
		}
		return this;
	}

	/**
	 * Select Reason for your visit
	 */
	public StoreLocatorPage selectReasonForYourVisit() {
		checkPageIsReady();
		try {
			reasonTextBox.click();
			reasonListOfValues.get(1).click();
			Reporter.log("Selected Reason for your visit in Get in line form");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to select Reason for your visit");
		}
		return this;
	}

	/**
	 * Select Additional Reason for your visit
	 */
	public StoreLocatorPage selectAdditionalReasonForYourVisit() {
		checkPageIsReady();
		try {
			additionalReasonTextBox.click();
			additionalReasonListOfValues.get(3).click();
			Reporter.log("Selected Additional reason for your visit in Get in line form");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to select Additional Reason for your visit");
		}
		return this;
	}

	/**
	 * Verify error message For First name
	 */
	public StoreLocatorPage verifyErrorMessageForFirstName() {
		try {
			firstName.clear();
			firstName.sendKeys(Keys.TAB);
			Assert.assertEquals(firstNameErrorMessage.getText(), "Enter your first name");
			Reporter.log("'Enter your first name' error message is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify error message for first name");
		}
		return this;
	}

	/**
	 * Verify error message For Last name
	 */
	public StoreLocatorPage verifyErrorMessageForLastName() {
		try {
			lastName.clear();
			lastName.sendKeys(Keys.TAB);
			Assert.assertEquals(lastNameErrorMessage.getText(), "Enter your last name");
			Reporter.log("'Enter your last name' error message is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify error message for last name");
		}
		return this;
	}

	/**
	 * Verify error message For Reason for your visit
	 */
	public StoreLocatorPage verifyErrorMessageForReasonForVisit() {
		checkPageIsReady();
		try {
			reasonTextBox.click();
			reasonTextBox.sendKeys(Keys.TAB);
			reasonTextBox.sendKeys(Keys.TAB);
			Assert.assertEquals(reasonForVisitErrorMessage.getText(), "Please select a reason for your visit");
			Reporter.log("'Please select a reason for your visit' error message is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify error message for Reason for your visit");
		}
		return this;
	}

	/**
	 * Verify Notify check box default state
	 *
	 * @return the CartPage class instance.
	 */
	public StoreLocatorPage verifyNotifyCheckboxDefaultState() {
		checkPageIsReady();
		try {
			Assert.assertTrue(notifyCheckboxText.isDisplayed(), "Notify checkbox text is not validated");
			String actualCheckedState = notifyCheckbox.getAttribute("value").toString();
			Assert.assertTrue(actualCheckedState.contains("on"), "Notify checkbox is checked by default");
			Reporter.log("Notify checkbox is unchecked by default");
		} catch (Exception e) {
			Assert.fail("Unable to validate Notify check box default state");
		}
		return this;
	}

	/**
	 * Check or uncheck Notify Check box
	 *
	 * @return the CartPage class instance.
	 */
	public StoreLocatorPage clickNotifyCheckbox() {
		checkPageIsReady();
		try {
			notifyCheckbox.click();
			Reporter.log("Clicked on Notify checkbox");
			Reporter.log("Notify checkbox is checked");
		} catch (Exception e) {
			Assert.fail("Unable to check Notify check box");
		}
		return this;
	}

	/**
	 * Enter Additional Reason for your visit
	 */
	public StoreLocatorPage validateContentBelowNotifyCheckbox() {
		try {
			Assert.assertEquals(notifyCheckboxContent.getText(), notifyValue);
			Reporter.log("Validated notify content successfully.");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to validate notify content");
		}
		return this;
	}

	/**
	 * Click on English Radio Button
	 *
	 */
	public StoreLocatorPage clickOnEnglishRadioButton() {
		try {
			englishRadioButton.click();
			Reporter.log("Successfully changed user language preference.");
		} catch (Exception e) {
			Assert.fail("Failed to click English radio button");
		}
		return this;
	}

	/**
	 * Click on English Radio Button
	 *
	 */
	public StoreLocatorPage selectYesInAsTMobileCustomer() {
		try {
			yesRadioButton.click();
			Reporter.log("Clicked yes radio button successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to click Yes radio button");
		}
		return this;
	}

	/**
	 * Click on User Consent Checkbox
	 *
	 */
	public StoreLocatorPage clickOnLegalTermContentCheckbox() {
		try {
			legalTermContentCheckbox.click();
			Reporter.log("Validated Legal term content checkbox clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to Legal term content Checkbox.");
		}
		return this;
	}

	/**
	 * Validate additional text content checkbox.
	 *
	 */
	public StoreLocatorPage validateRequiredFieldAndTextMessagingContent() {
		try {
			Assert.assertEquals(requiredFieldContent.getText(), "Indicates required field");
			textMessagingContent.isDisplayed();
			Reporter.log("Validated required field and text messaging content successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to Validate required field and text messaging content");
		}
		return this;
	}

	/**
	 * Click on Get In Line Button
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyAndclickOnGetInLineCTA() {
		checkPageIsReady();
		try {
			getInLineCTA.isEnabled();
			getInLineCTA.click();
			Reporter.log("'Get in line' CTA is highlighted and clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Get in line' CTA");
		}
		return this;
	}

	/**
	 * Verify Get In Line success modal
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyGetInLineSuccessModal(String firstName) {
		checkPageIsReady();
		try {
			String[] getName = getInLineSuccessModal.getText().split(",");
			Assert.assertEquals(getName[0], firstName);
			Reporter.log("'Get in line' success modal is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display 'Get in line' success modal");
		}
		return this;
	}

	/**
	 * verify Store name in Get in line success modal
	 */
	public StoreLocatorPage verifyStoreNameInGetInLineSuccessModal(String storeName) {
		checkPageIsReady();
		try {
			Assert.assertTrue(storeNameInGetInLineSuccessModal.getText().equals(storeName),
					"Selected store is not displaying");
			Reporter.log("Store name in Get in line success modal is successfully verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Store name in Get in line success Modal");
		}
		return this;
	}

	/**
	 * Verify notify drop down selector
	 *
	 */
	public StoreLocatorPage verifyNotifyDropDown() {
		try {
			Assert.assertTrue(notifyDropDown.isDisplayed(), "Notify drop down is not displayed");
			Reporter.log("Drop down selector is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display notify drop down selector");
		}
		return this;
	}

	/**
	 * Click notify drop down
	 *
	 */
	public StoreLocatorPage clickNotifyDropDown() {
		checkPageIsReady();
		try {
			notifyDropDown.click();
			Reporter.log("Clicked on Notify drop down");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to click on notify drop down");
		}
		return this;
	}

	/**
	 * Verify PhoneNumber text box
	 *
	 */
	public StoreLocatorPage verifyPhoneNumberTextBox() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(phoneNumberTextBox), 30);
			Assert.assertTrue(phoneNumberTextBox.isDisplayed(), "Notify text box is not displayed");
			Reporter.log("Phone number text box is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Phone number text box");
		}
		return this;
	}

	/**
	 * Verify Email address text box
	 *
	 */
	public StoreLocatorPage verifyEmailTextBox() {
		checkPageIsReady();
		try {
			Assert.assertTrue(emailTextBox.isDisplayed(), "Notify text box is not displayed");
			Reporter.log("Email text box is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Email text box");
		}
		return this;
	}

	/**
	 * Verify notify drop down default value
	 *
	 */
	public StoreLocatorPage verifyNotifyDropDownDefaultValue() {
		checkPageIsReady();
		try {
			Assert.assertEquals(notifyDropDownValues.get(0).getText(), "Text message");
			Reporter.log("Drop down selector is defaulted to Text message");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display the default value in notify drop down selector");
		}
		return this;
	}

	/**
	 * Select TextMessage In Notify DropDown
	 *
	 */
	public StoreLocatorPage selectTextMessageInNotifyDropDown() {
		checkPageIsReady();
		try {
			Assert.assertTrue(notifyDropDownValues.get(0).isDisplayed(),
					"Text Message is not displayed in the drop down");
			Reporter.log("Text messages is displayed in the drop down");
			notifyDropDownValues.get(0).click();
			Reporter.log("Selected 'Text messages' from drop down");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to select Text message from drop down");
		}
		return this;
	}

	/**
	 * Select Email In Notify DropDown
	 *
	 */
	public StoreLocatorPage selectEmailInNotifyDropDown() {
		checkPageIsReady();
		try {
			Assert.assertTrue(notifyDropDownValues.get(1).isDisplayed(), "Email is not displayed in the drop down");
			Reporter.log("Email is displayed in the drop down");
			notifyDropDownValues.get(1).click();
			Reporter.log(" Selected 'Email' from drop down");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to select Email from drop down");
		}
		return this;
	}

	/**
	 * Set value In Phone Number to notify
	 *
	 */
	public StoreLocatorPage setPhoneNumber(TMNGData tMNGData) {
		checkPageIsReady();
		try {
			phoneNumberTextBox.clear();
			phoneNumberTextBox.sendKeys(tMNGData.getPhoneNumber());
			Reporter.log("Entered '" + tMNGData.getPhoneNumber() + "' in the text box");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to enter '" + tMNGData.getPhoneNumber() + "' in the text box");
		}
		return this;
	}

	/**
	 * Set value In Email address to notify
	 *
	 */
	public StoreLocatorPage setEmail(TMNGData tMNGData) {
		checkPageIsReady();
		try {
			emailTextBox.clear();
			emailTextBox.sendKeys(tMNGData.getEmail());
			Reporter.log("Entered '" + tMNGData.getEmail() + "' in the text box");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to enter '" + tMNGData.getEmail() + "' in the text box");
		}
		return this;
	}

	/**
	 * Verify error message for Phone number
	 *
	 */
	public StoreLocatorPage verifyErrorMessageForPhoneNumber() {
		checkPageIsReady();
		try {
			phoneNumberTextBox.clear();
			phoneNumberTextBox.sendKeys(Keys.TAB);
			Assert.assertTrue(isElementDisplayed(phoneNumberErrorMessage),
					"Error message for Phone number is not displayed");
			Reporter.log("'Enter your phone number' error message is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify error message for phone number");
		}
		return this;
	}

	/**
	 * Verify error message for Phone number is not displayed for valid data
	 *
	 */
	public StoreLocatorPage verifyErrorMessageForPhoneNumberNotDisplayedForValidData() {
		try {
			Assert.assertFalse(isElementDisplayed(phoneNumberErrorMessage),
					"Error message for Phone number is  displaying for valid data");
			Reporter.log("'Enter your phone number' error message is not displayed for valid data");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify error message for Phone number is not displayed for valid data");
		}
		return this;
	}

	/**
	 * Verify error message for Phone number
	 *
	 */
	public StoreLocatorPage verifyErrorMessageForInvalidPhoneNumber() {
		checkPageIsReady();
		try {
			phoneNumberTextBox.clear();
			phoneNumberTextBox.sendKeys("12345");
			Assert.assertEquals(invalidPhoneNumberErrorMessage.getText(), "Enter a valid phone number");
			Reporter.log("'Enter a valid phone number' error message is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify error message for invalid phone number");
		}
		return this;
	}

	/**
	 * Verify error message for email
	 *
	 */
	public StoreLocatorPage verifyErrorMessageForEmail() {
		checkPageIsReady();
		try {
			emailTextBox.clear();
			emailTextBox.sendKeys(Keys.TAB);
			Assert.assertEquals(emailErrorMessage.getText(), "Enter your email address");
			Reporter.log("'Enter your email address' error message is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to enter verify error message for email");
		}
		return this;
	}

	/**
	 * Verify error message for email
	 *
	 */
	public StoreLocatorPage verifyErrorMessageForInvalidEmail() {
		checkPageIsReady();
		try {
			emailTextBox.clear();
			emailTextBox.sendKeys("www.mytmobile");
			Assert.assertEquals(invalidEmailErrorMessage.getText(), "Enter a valid email address");
			Reporter.log("'Enter a valid email address' error message is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to enter verify error message for invalid email");
		}
		return this;
	}

	/**
	 * Verify 'About the store' description text On Store Locator Page
	 *
	 */
	public StoreLocatorPage verifyAboutTheStoreDescription() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(aboutTheStoreDescription), 60);
			Assert.assertTrue(aboutTheStoreDescription.isDisplayed(),
					"'About the store' description text On Store Locator Page is not displayed");
			Reporter.log("'About the store' description text On Store Locator Page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'About the store' description text On Store Locator Page");
		}
		return this;
	}

	/**
	 * Validate Store map page in Store locator page
	 *
	 */
	public StoreLocatorPage verifyMapDisplayed() {
		try {
			getDriver().switchTo().frame("pccIFrame");
			Assert.assertTrue(storeLocatorMap.isDisplayed(), "Map is not displaying beside store list");
			getDriver().switchTo().parentFrame();
			Reporter.log("Map is displaying beside store list");
		} catch (Exception e) {
			Assert.fail("Failed to verify map");
		}
		return this;
	}

	/**
	 * Click Zoom In Button on Map
	 *
	 */
	public StoreLocatorPage clickZoomInButtonOnMap() {
		try {
			waitForSpinnerInvisibility();
			getDriver().switchTo().frame("pccIFrame");
			zoomInButtonOnMap.click();
			getDriver().switchTo().parentFrame();
			Reporter.log("Map Zoom In button is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Map Zoom In button");
		}
		return this;
	}

	/**
	 * Verify Store Locator list page
	 *
	 */
	public String selectFirstStore() {
		String storeName = null;
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			for (WebElement store : nearbyStoresList) {
				moveToElement(store);
				if (store.isDisplayed()) {
					storeName = store.getText();
					clickElementWithJavaScript(store);
					break;
				}
			}
			Reporter.log("First Store is selected");
		} catch (Exception e) {
			Assert.fail("First Store not selected");
		}
		return storeName;
	}

	/**
	 * Verify Store Locator list page
	 *
	 */
	public StoreLocatorPage selectStoreWhichHasAppointments() {
		try {
			waitForSpinnerInvisibility();
			for (WebElement availableStoe : changeStoreWhichHasAppointments) {
				if (isElementDisplayed(availableStoe)) {
					clickElementWithJavaScript(availableStoe);
					break;
				}
			}
			Reporter.log("Store is not selected");
		} catch (Exception e) {
			Assert.fail("Store is not selected");
		}
		return this;
	}

	/**
	 * Verify Selected store locator name
	 *
	 */
	public StoreLocatorPage verifySelectedStoreLocatorName(String storeLocatorName) {
		try {
			String displayedStoreLocator = getStoreNameInStoreDetailsPage();
			assertTrue(displayedStoreLocator.equals(storeLocatorName), "Store locator list is not displayed");
			Reporter.log("Selected Store locator name is not displayed");
		} catch (Exception e) {
			Assert.fail("Selected Store locator name is not displayed");
		}
		return this;
	}

	/**
	 * Verify get In Line Cta
	 *
	 */
	public StoreLocatorPage verifyGetInLineCTA() {
		try {
			if (isElementDisplayed(getInLineCTAInStoreDetails)) {
				Assert.assertTrue(getInLineCTAInStoreDetails.isDisplayed(), "Get In Line Cta is not displaying");
				Reporter.log("Verified Get In Line Cta");
			} else {

				Calendar now = Calendar.getInstance(TimeZone.getTimeZone("PST"));
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
				sdf.setTimeZone(TimeZone.getTimeZone("PST"));
				String currentTime = sdf.format(now.getTime());

				String storeTimigs = storeTimings.getText();
				String storeTimigsArray[] = storeTimigs.split("-");

				String startTime = storeTimigsArray[0].replaceAll("[^0-9?!\\.-:]", "");
				String endTime = storeTimigsArray[1].replaceAll("[^0-9?!\\.-:]", "");
				String currentTimestr = currentTime.replaceAll("[^0-9?!\\.]", "");

				startTime = manageStoreStartTime(startTime);
				endTime = manageStoreClosingTime(endTime);

				int storeOpeningTime = Integer.parseInt(startTime);
				int storeClosingTime = Integer.parseInt(endTime);
				int localTime = Integer.parseInt(currentTimestr);

				if (localTime >= storeOpeningTime && localTime <= storeClosingTime) {
					Assert.assertTrue(getInLineCTAInStoreDetails.isDisplayed(), "Get In Line Cta is not displaying");
					Reporter.log("Verified Get In Line Cta");
				} else {
					Reporter.log("Store is closed and Get In Line CTA is disabled.");
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Get In Line Cta");
		}
		return this;
	}

	/**
	 * Verify Directions Cta
	 *
	 */
	public StoreLocatorPage verifyDirectionsCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(directionsCta), 60);
			Assert.assertTrue(directionsCta.isDisplayed(), "'Directions' Cta is not displayed");
			Reporter.log("Verified 'Directions' Cta");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Directions' Cta");
		}
		return this;
	}

	/**
	 * Verify Appointment Cta
	 *
	 */
	public StoreLocatorPage verifyAppointmentCTA() {
		try {
			Assert.assertTrue(appointmentCta.isDisplayed(), "'Appointment' Cta is not displayed");
			Reporter.log("Verified 'Appointment' Cta");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Appointment' Cta");
		}
		return this;
	}

	/**
	 * Click Appointment Cta
	 *
	 */
	public StoreLocatorPage clickAppointmentCTA() {
		try {
			appointmentCta.click();
			Reporter.log("Clicked on 'Appointment' Cta");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Appointment' Cta");
		}
		return this;
	}

	/**
	 * Verify Time And Date Table
	 *
	 */
	public StoreLocatorPage verifyTimeAndDateTable() {
		try {
			waitFor(ExpectedConditions.visibilityOf(selectDataAndTimeTable), 60);
			Assert.assertTrue(selectDataAndTimeTable.isDisplayed(), "Time And Date Table is not displayed");
			Reporter.log("Verified Time And Date Table");
		} catch (Exception e) {
			Assert.fail("Failed to verify Time And Date Table");
		}
		return this;
	}

	/**
	 * Select Time slot for Appointment
	 *
	 */
	public StoreLocatorPage selectTimeSlotforAppointment() {
		try {
			waitFor(ExpectedConditions.visibilityOf(disabledNextCTAOnTimeandDateModal), 60);
			waitforSpinner();
			Thread.sleep(5000);
			for (WebElement timeSlot : selectTimeSlot) {
				waitFor(ExpectedConditions.visibilityOf(timeSlot), 60);
				if(isElementDisplayed(timeSlot)) {
					clickElementWithJavaScript(timeSlot);
				}
				if (!isElementDisplayed(disabledNextCTAOnTimeandDateModal)) {
					break;
				}

			}
			Reporter.log("Date and slot is selected");
		} catch (Exception e) {
			Assert.fail("Failed to select Time and Date");
		}
		return this;
	}

	/**
	 * Click Next CTA On Time and DateTable
	 *
	 */
	public StoreLocatorPage clickNextCTAOnTimeAndDateTable() {
		try {
			if(isElementEnabled(nextCTAOnTimeandDateModal)) {
				nextCTAOnTimeandDateModal.click();
			}
			Reporter.log("Clicked on 'Next' Cta");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Next' Cta on time and date table");
		}
		return this;
	}

	/**
	 * Verify the Maximum Length of the name in succcess modal
	 *
	 */
	public StoreLocatorPage verifyFirstNameLengthInSuccessModal() {
		try {
			String[] getName = getInLineSuccessModal.getText().split(",");
			Assert.assertEquals(getName[0].length(), 40);
			Reporter.log("Verified successfully the length of the Name displayed in Success modal");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Name length in Success modal");
		}
		return this;
	}

	/**
	 * Verify the Pagination is displayed in correct format on store locator results
	 * page
	 *
	 */
	public StoreLocatorPage verifyPaginationFormatDisplayedCorrectly() {
		try {
			String getName = paginationResult.getText();
			//replace string
			Assert.assertTrue(getName.matches("(.*)[0-9]*-[0-9]* of [0-9](.*)"));
			Reporter.log("Verified successfully that pagination is correctly displayed on store locator result page");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify pagination is correctly displayed on store locator result page");
		}
		return this;
	}

	/**
	 * Verify Next page button is displayed or not on store locator results page
	 *
	 */
	public StoreLocatorPage verifyNextStoresPageButtonIsDisplayedOrNot(String disabledOrNot) {
		checkPageIsReady();
		switch (disabledOrNot) {
		case "Enabled":
			try {
				Assert.assertTrue(nextStoresPageButton.isEnabled(), "Next stores page button is not enabled");
				Reporter.log("Next stores page button is enabled");
			} catch (NoSuchElementException e) {
				Assert.fail("Failed to see next stores page button enabled");
			}
			break;
		case "Disabled":
			try {
				Assert.assertFalse(nextStoresPageButton.isEnabled(), "Next stores page button is not disabled");
				Reporter.log("Next stores page button is disabled");
			} catch (NoSuchElementException e) {
				Assert.fail("Failed to see next stores page button disabled");
			}
			break;
		}
		return this;
	}

	/**
	 * Verify Previous page button is displayed or not on store locator results page
	 *
	 */
	public StoreLocatorPage verifyPreviousStoresPageButtonIsDisabledOrNot(String disabledOrNot) {
		checkPageIsReady();
		switch (disabledOrNot) {
		case "Enabled":
			try {
				Assert.assertTrue(previousStoresPageButton.isEnabled(), "Previous stores page button is not enabled");
				Reporter.log("Previous stores page button is enabled");
			} catch (NoSuchElementException e) {
				Assert.fail("Failed to see previous stores page button enabled");
			}
			return this;
		case "Disabled":
			try {
				Assert.assertFalse(previousStoresPageButton.isEnabled(), "Previous stores page button is not disabled");
				Reporter.log("Previous stores page button is disabled");
			} catch (NoSuchElementException e) {
				Assert.fail("Failed to see previous stores page button disabled");
			}
		}
		return this;
	}

	/**
	 * Click next page button on store locator results page
	 *
	 */
	public StoreLocatorPage goToNextStoresPage() {
		checkPageIsReady();
		try {
			nextStoresPageButton.click();
			Reporter.log("Clicked on next page button");
			Reporter.log("next page button is clicked");
		} catch (Exception e) {
			Assert.fail("Unable to click next page button");
		}
		return this;
	}

	/**
	 * Verify distances are displayed up to 2 decimal places only
	 *
	 */
	public StoreLocatorPage verifyDistancesAreDisplayedUpto2DecimalPlacesOnly() {
		checkPageIsReady();
		int counter = 0;
		try {
			for (WebElement distance : searchBladeStoreDistance) {
				String miles = distance.getText();
				String num = miles.split(" ")[2];
				int i = num.lastIndexOf('.');
				if (i == -1 || num.substring(i + 1).length() == 2 || num.substring(i + 1).length() == 1) {
					System.out.println("The number " + num + " has up to two digits after dot");
				} else {
					counter++;
					System.out.println("The number " + num + " does not has up to two digits after dot");
				}
			}
			Assert.assertTrue(counter == 0);
			Reporter.log("All distances are displayed upto 2 decimal places only");
		} catch (Exception e) {
			Assert.fail("All distances are not displayed upto 2 decimal places only");
		}
		return this;
	}

	/***
	 * Verify the presence of search input hint text
	 * 
	 * @return
	 */
	public StoreLocatorPage verifySearchInputHintText() {
		try {
			checkPageIsReady();
			moveToElement(searchTextBox);
			Assert.assertTrue(isElementDisplayed(searchInputHintText), "Search input hint text is not displayed");
			Reporter.log("Search input hint text is displayed");
		} catch (Exception e) {
			Assert.fail("Search input hint text is not displayed");
		}
		return this;
	}

	/***
	 * Verify the presence of search input hint text after focus
	 * 
	 * @return
	 */
	public StoreLocatorPage verifySearchInputTextAfterFocus() {
		try {
			checkPageIsReady();
			searchTextBox.click();
			Assert.assertTrue(isElementDisplayed(searchInputHintTextAfterFocus),
					"Search input hint text after focus is not displayed");
			Reporter.log("Search input hint text after focus is displayed");
		} catch (Exception e) {
			Assert.fail("Search input hint text after focus is not displayed");
		}
		return this;
	}

	/***
	 * Verify search icon is enabled after entering value
	 * 
	 * @return
	 */
	public StoreLocatorPage verifySearchIconIsEnabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(searchIcon.isEnabled(), "Search icon is not enabled");
			Reporter.log("Search icon is enabled, once entered a value in saerch text box");
		} catch (Exception e) {
			Assert.fail("Failed to verify Search icon is enabled");
		}
		return this;
	}

	/***
	 * validation of First party retailer stores in the search result blades
	 *
	 * @return
	 */
	public StoreLocatorPage verifyFPRSearchResultBlades() {
		try {
			checkPageIsReady();
			for (int i = 0; i < searchResultSection.size(); i++) {
				if (searchResultSection.get(i).getText().toString().contains(tPRStoreTagName)
						|| searchResultSection.get(i).getText().toString().contains(authorizedStoreTagName)) {
				} else {
					Assert.assertTrue(storeNameInSearchBlades.get(i).isDisplayed(),
							"Failed to verify store name in FPR search results");
					String miles = searchBladeStoreDistance.get(i).getText();
					String milesOfAllBlades[] = miles.split(" ");
					Assert.assertTrue((!milesOfAllBlades[1].isEmpty()) && (!milesOfAllBlades[2].isEmpty()),
							"Failed to verify Distance in FPR search results");
					Assert.assertTrue(storeAddress.get(i).isDisplayed(),
							"Failed to verify store address in FPR search results");
					Assert.assertTrue(searchResultSection.get(i).getText().toString().contains(todaysHours),
							"Failed to verify Today's hours with its value in FPR search results");
					Assert.assertTrue(
							searchResultSection.get(i).getText().toString().contains(inStoreWaitTimeInSearchBlades),
							"Failed to verify In store wait time in FPR search results");
					Assert.assertTrue(searchResultSection.get(i).getText().toString().contains(appointmentsAvailable),
							"Failed to verify Appointments Available text in FPR search results");
				}
			}
			Reporter.log("FPR search result blades are displayed successfully in search results");
			Reporter.log(
					"Successfully verified Store name data, Distance of store, Store address data, Today's hours, In-Store wait, Appointments available data for FPR search results");
		} catch (Exception e) {
			Assert.fail("Failed to verify the FPR search results with store details");
		}
		return this;
	}

	/***
	 * validation of Third party retailer stores in the search result blades
	 *
	 * @return
	 */
	public StoreLocatorPage verifyTPRSearchResultBlades() {
		try {
			checkPageIsReady();
			for (int i = 0; i < searchResultSection.size(); i++) {
				if (searchResultSection.get(i).getText().toString().contains(tPRStoreTagName)) {
					Assert.assertTrue(searchResultSection.get(i).getText().toString().contains(tPRStoreTagName),
							"Failed to display TPR search results with tag Third Party Retailers");
					Assert.assertTrue(storeNameInSearchBlades.get(i).isDisplayed(),
							"Failed to verify store name in TPR search results");
					String miles = searchBladeStoreDistance.get(i).getText();
					String milesOfAllBlades[] = miles.split(" ");
					Assert.assertTrue((!milesOfAllBlades[0].isEmpty()) && (!milesOfAllBlades[1].isEmpty()),
							"Failed to verify Distance in TPR search results");
					Assert.assertTrue(storeAddress.get(i).isDisplayed(),
							"Failed to verify store address in TPR search results");
					Assert.assertTrue(searchResultSection.get(i).getText().toString().contains(todaysHours),
							"Failed to verify Today's hours with its value in TPR search results");
					Assert.assertTrue(
							searchResultSection.get(i).getText().toString().contains(inStoreWaitTimeInSearchBlades),
							"Failed to verify In store wait time in TPR search results");
					Assert.assertTrue(searchResultSection.get(i).getText().toString().contains(appointmentsAvailable),
							"Failed to verify Appointments Available text in TPR search results");
				}
			}
			Reporter.log(
					"TPR search result blades are displayed successfully in search results, with tag Third Party Retailers");
			Reporter.log(
					"Successfully verified Store name data, Distance of store, Store address data, Today's hours, In-Store wait, Appointments available data for TPR search results");
		} catch (Exception e) {
			Assert.fail("Failed to verify the TPR search results with store details");
		}
		return this;
	}

	/***
	 * validation of Authorized dealer stores in the search result blades
	 *
	 * @return
	 */
	public StoreLocatorPage verifyAuthorizedDealerSearchResultBlades() {
		try {
			checkPageIsReady();
			for (int i = 0; i < searchResultSection.size(); i++) {
				if (searchResultSection.get(i).getText().toString().contains(authorizedStoreTagName)) {
					Assert.assertTrue(searchResultSection.get(i).getText().toString().contains(authorizedStoreTagName),
							"Failed to display Authorized dealer search results with tag Authorized dealer");
					Assert.assertTrue(storeNameInSearchBlades.get(i).isDisplayed(),
							"Failed to verify store name in Authorized dealer search results");
					String miles = searchBladeStoreDistance.get(i).getText();
					String milesOfAllBlades[] = miles.split(" ");
					Assert.assertTrue((!milesOfAllBlades[0].isEmpty()) && (!milesOfAllBlades[1].isEmpty()),
							"Failed to verify Distance in Authorized dealer search results");
					Assert.assertTrue(storeAddress.get(i).isDisplayed(),
							"Failed to verify store address in Authorized dealer search results");
				}
			}
			Reporter.log(
					"Authorized search result blades are displayed successfully in search results, with tag Authorized dealer");
			Reporter.log(
					"Successfully verified Store name data, Distance of store, Store address data for Authorized dealer search results");
			for (int i = 0; i < searchResultSection.size(); i++) {
				if (searchResultSection.get(i).getText().toString().contains(authorizedStoreTagName)) {
					if (searchResultSection.get(i).getText().toString().contains(todaysHours)) {
						Reporter.log("Verified today's Hours successfully for this Authorized dealer store");
					} else {
						Reporter.log("No Today's Hours exist for this Authorized dealer store");
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify the Authorized dealer search results with store details");
		}
		return this;
	}

	/***
	 * click on Sort drop down(Sorting results by)
	 * 
	 * @return
	 */
	public StoreLocatorPage clickOnSortDropDown() {
		try {
			Assert.assertTrue(isElementDisplayed(SortingResultsByDropDown));
			SortingResultsByDropDown.click();
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on drop down menu successfully");
		} catch (Exception e) {
			Reporter.log("Click on sort drop down failed");
			Assert.fail("Click on sort drop down failed");
		}
		return this;
	}

	/***
	 * select distance from drop down
	 * 
	 * @return
	 */
	public StoreLocatorPage selectDistanceFromDropDown() {
		checkPageIsReady();
		try {
			selectDistanceFromDropDown.click();
			Reporter.log("Selected distance from drop down menu successfully");
		} catch (Exception e) {
			Assert.fail("Unable to select distance in drop down");
		}
		return this;
	}

	/***
	 * select distance from drop down
	 * 
	 * @return
	 */
	public StoreLocatorPage selectWaitTimeFromDropDown() {
		checkPageIsReady();
		try {
			selectWaitTimeFromDropDown.click();
			Reporter.log("Selected wait time from drop down menu successfully");
		} catch (Exception e) {
			Assert.fail("Unable to select wait time in drop down");
		}
		return this;
	}

	/**
	 * Verify search blades are sorted by distance
	 */
	public StoreLocatorPage verifySearchResultSortedByDistance() {
		checkPageIsReady();
		ArrayList<String> distanceValue = new ArrayList<String>();
		try {
			for (WebElement distance : searchBladeStoreDistance) {
				String miles = distance.getText();
				String milesOfAllBlades[] = miles.split(" ");
				distanceValue.add(milesOfAllBlades[2]);
			}
			Assert.assertTrue(Ordering.natural().isOrdered(distanceValue));
			Reporter.log("Search results are sorted by Distance");
		} catch (Exception e) {
			Assert.fail("unable to sort search results by Distance");
		}
		return this;
	}

	/**
	 * Verify search blades are sorted by wait time
	 */
	public StoreLocatorPage verifySearchResultSortedByWaitTime() {
		waitFor(ExpectedConditions.visibilityOfAllElements(searchBladeStoreWaitTime));
		ArrayList<String> waitTimeValue = new ArrayList<String>();
		try {
			do {
				for (WebElement waitTime : searchBladeStoreWaitTime) {
					String miles = waitTime.getText();
					String milesOfAllBlades[] = miles.split(":");
					String waitTimeWithminutes = milesOfAllBlades[1].trim();
					String waitTimeWithOutminutes = waitTimeWithminutes.replaceAll("[^\\d.]", "");
					waitTimeValue.add(waitTimeWithOutminutes);
				}
				if (isElementDisplayed(nextBtn)) {
					nextBtn.click();
				} else {
					break;
				}
			} while (isElementDisplayed(nextBtn));
			Assert.assertTrue(Ordering.natural().isOrdered(waitTimeValue));
			Reporter.log("Search results are sorted by Wait time");
			Reporter.log(waitTimeValue.toString());
		} catch (Exception e) {
			Assert.fail("unable to sort search results by Wait time");
		}
		return this;
	}
	
	/***
	 * Verify the display of store locator page
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyStoreDetailsPage() {
		try {
			//checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(storeHeader));
			Reporter.log("Store Details page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Store Details page is displayed");
		}
		return this;
	}

}