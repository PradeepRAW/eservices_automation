package com.tmobile.eservices.qa.pages.payments.api;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSBilling extends  ApiCommonLib {

	
public Response getResponsedataset(String alist[]) throws Exception {
	Response response=null;   
	//String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
	    if(alist!=null) {
	    	
	    RestService restService = new RestService("", "https://stage2.eos.corporate.t-mobile.com");
	   // RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("userRole", alist[4]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("msisdn", alist[0]);
	    restService.addHeader("unBilledCycleStartDate", "2017-10-01");
	    restService.addHeader("Authorization","Bearer "+alist[2]);
	    restService.addHeader("access_token", "Bearer "+alist[1]);
	  
	    response = restService.callService("v1/billing/dataset",RestCallType.GET);
	    
	}

	    return response;
}







public String geteipdetails(Response response) {
	String eipdetails=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("eipDetailsSection.isEIPDetailsExists")!=null) {
        	return jsonPathEvaluator.get("eipDetailsSection.isEIPDetailsExists").toString();
	}}
		
	
	return eipdetails;
}


public String getpaeligibility(Response response) {
	String paeligibility=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("paymentArrangementDetails.paymentArrangementIndicator")!=null) {
        	return jsonPathEvaluator.get("paymentArrangementDetails.paymentArrangementIndicator").toString();
	}}
		
	
	return paeligibility;
}


public String getautopaystatus(Response response) {
	String autopay=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("autoPay.easyPayStatus")!=null) {
        	return jsonPathEvaluator.get("autoPay.easyPayStatus").toString();
	}}
		
	
	return autopay;
}



public String getIspastdueAmount(Response response) {
	String pastdue=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("pastdueAmountDetails.pastdueAmount")!=null) {
		
        float amt=Float.parseFloat(jsonPathEvaluator.get("pastdueAmountDetails.pastdueAmount").toString());
        if(amt>0) {
        	return "true";
        	}
        else {
        	return "false";
        }
			
	}}
		
	
	return pastdue;
}


public String getARbalance(Response response) {
	String arbalance=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("arBalance.balanceDue")!=null) {
		
        float amt=Float.parseFloat(jsonPathEvaluator.get("arBalance.balanceDue").toString());
        if(amt>0) {
        	return "true";
        	}
        else {
        	return "false";
        }
			
	}}
		
	
	return arbalance;
}

//pastdueAmountDetails.pastdueAmount

/***********************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 ***********************************************************************************/

public Response getResponsebillList(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization", "Bearer "+alist[2]);
	    restService.addHeader("ban", alist[3]);
	    response = restService.callService("v1/billing/cycles?ban="+alist[3],RestCallType.GET);
	    
	}

	    return response;
}









public String getbilltype(Response response) {
	String billtype=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("statusMessage")!=null) {
	    if(jsonPathEvaluator.get("statusMessage").toString().equalsIgnoreCase("Bill list found")) {
		
		if(jsonPathEvaluator.get("BillList[0].documentId")!=null) {
        	String doc=jsonPathEvaluator.get("BillList[0].documentId").toString();
        	if(doc.trim().length()>0) {
        		return "bbill";
        	}
        	else {
        		return "ebill";
        	}
	    }
		
	}
	    return "ebill";
		}
		
	}
	
	return billtype;
}






public String checkpreviousbills(Response response) {
	String billtype=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("statusMessage")!=null) {
	    if(jsonPathEvaluator.get("statusMessage").toString().equalsIgnoreCase("Bill list found")) {
		
		if(jsonPathEvaluator.get("BillList.size()")!=null) {
        	if(Integer.parseInt(jsonPathEvaluator.get("BillList.size()").toString())>2) {
        		return "true";
        	}
        	else {
        		return "false";
        	}
        	
	    }
		
	}
	    return billtype;
		}
		
	}
	
	return billtype;
}




	private Map<String, String> buildEOSBiilingAPIHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("userRole", "PAH");
		headers.put("ban", apiTestData.getBan());
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("Authorization", "Bearer " + checkAndGetAccessToken());
		headers.put("unBilledCycleStartDate", "2017-10-01");
		headers.put("easyPayStatus", "true");

		return headers;
	}

	public Response getDataset(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildEOSBiilingAPIHeader(apiTestData);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);

		Response response = service.callService("v1/billing/dataset", RestCallType.GET);
		return response;
	}



}
