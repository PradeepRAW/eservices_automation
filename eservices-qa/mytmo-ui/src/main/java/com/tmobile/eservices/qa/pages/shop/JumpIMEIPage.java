package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class JumpIMEIPage extends CommonPage {

	@FindBy(css = "p.h3-title")
	private WebElement jumpIMEI;

	@FindBy(xpath = "//div[contains(text(), 'Power on?')]//..//i")
	private WebElement powerOnchkBox;

	@FindBy(xpath = "//div[contains(text(), 'LCD and Display?')]//..//i")
	private WebElement lcdAndDisplay;

	@FindBy(xpath = "//div[contains(text(), 'liquid damage?')]//..//i")
	private WebElement liquidDamage;

	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement continueButton;

	@FindBy(css = "div.text-center.header-box-shadow")
	private WebElement sedonaHeader;

	@FindBy(xpath = "//div[contains(text(), 'Have you disabled')]//..//i")
	private WebElement haveYouDisabled;

	public JumpIMEIPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Jump IMEI Page
	 * 
	 * @return
	 */
	public boolean verifyJumpIMEIPage() {
		try {
			checkPageIsReady();
			waitforSpinner();
			Assert.assertTrue(jumpIMEI.isDisplayed(), "JumpIMEI Page is not displayed");
			Reporter.log("JumpIMEI Page is displayed");
		} catch (Exception e) {
			Assert.fail("JumpIMEI Page is not displayed");
		}
		return this != null;
	}

	public JumpIMEIPage clickPowerOnCheckBox() {
		try {
			waitforSpinner();
			powerOnchkBox.click();
		} catch (Exception e) {
			Assert.fail("PowerOnCheckBox is not available to click");
		}
		return this;
	}

	public JumpIMEIPage clickLCDDisplayCheckBox() {
		try {
			lcdAndDisplay.click();
			Reporter.log("LCDDisplayCheckBox is displayed");
		} catch (Exception e) {
			Assert.fail("LCDDisplayCheckBox is not displayed");
		}
		return this;
	}

	public JumpIMEIPage clickContinueButton() {
		try {
			continueButton.click();
			Reporter.log("ContinueButton is displayed");
		} catch (Exception e) {
			Assert.fail("ContinueButton is not displayed to click");
		}
		return this;
	}

}
