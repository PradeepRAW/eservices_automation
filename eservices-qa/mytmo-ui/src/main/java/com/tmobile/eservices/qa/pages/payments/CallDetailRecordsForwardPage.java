package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author charshavardhana
 *
 */
public class CallDetailRecordsForwardPage extends CommonPage {

	@FindBy(css = "#di_voicetab")
	private WebElement voiceTab;

	@FindBy(css = "#di_messagetab")
	private WebElement messageTab;

	@FindBy(css = "#di_datatab")
	private WebElement dataTab;

	@FindBy(id = "di_premiumtab")
	private WebElement tmobilePurchasesTab;

	@FindBy(id = "di_thirdPartytab")
	private WebElement thirdPartyTab;

	@FindBy(id = "allvoice")
	private WebElement allVoiceTab;

	@FindBy(css = "tr[id*=datarow]")
	private List<WebElement> dataPassUsage;

	@FindBy(css = "tr[id*='voicerow']")
	private List<WebElement> voiceUsage;

	@FindBy(css = "tr[id*='messagerow']")
	private List<WebElement> messageUsage;

	@FindBy(css = "table#voicePicker0 tr:nth-child(2) td:nth-child(1)")
	private WebElement filterByDateTime;

	@FindBy(css = "table#voicePicker0 tr:nth-child(2) td:nth-child(2)")
	private WebElement filterByDestination;

	@FindBy(css = "table#voicePicker0 tr:nth-child(2) td:nth-child(3)")
	private WebElement filterByNumber;

	@FindBy(css = "table#voicePicker0 tr:nth-child(2) td:nth-child(4)")
	private WebElement filterByMin;

	@FindBy(css = "table#voicePicker0 tr:nth-child(2) td:nth-child(5)")
	private WebElement filterByType;

	@FindBy(id = "downloadCSV")
	private WebElement downloadUsageRecordsLink;

	@FindBy(css = "div.pagetitle_txt span.ui_headline")
	private WebElement verifyUsageDetails;

	@FindBys(@FindBy(css = "ul#di_allTabs li a"))
	private List<WebElement> verifyTabs;

	@FindBy(id = "alldata")
	private WebElement allDataTab;

	@FindBy(id = "allmessagetab")
	private WebElement allMessageTab;

	@FindBy(css = "#downloadCSV")
	private WebElement downloadUsageRecords;

	@FindBy(id = "filtervoice")
	private WebElement voiceFilter;

	@FindBy(css = ".active tr[id*='row'] td:nth-child(3)")
	private List<WebElement> filteredNumbersInTable;

	@FindBy(css = ".active td[id$='data02']")
	private WebElement numberForFiltering;

	@FindBy(css = "#tabSelected")
	private WebElement verifyVoiceMobile;

	@FindBy(css = "div#pdp-dropdown1 a#voiceAllLink")
	private WebElement verifyVoiceTab;

	@FindBy(css = "div#pdp-dropdown1 a#messageAllLink")
	private WebElement verifyMessagingTab;

	@FindBy(css = "div#pdp-dropdown1 a#dataAllLink")
	private WebElement verifyDataTab;

	@FindBy(css = "div#pdp-dropdown1 div.thirdPartyClass a#premiumAllLink")
	private WebElement verifyTmobilePurchasesTab;

	@FindBy(css = "div#pdp-dropdown1 div.thirdPartyClass a#thirdPartyAllLink")
	private WebElement verifyThirdPartyPurchasesTab;

	@FindBy(css = "div#ui_filterdropdown .DropArwPadding")
	private WebElement clickToggleSelector;

	protected By deviceWaitCursor = By.id("device_waitCursor");

	@FindAll({ @FindBy(id = "img_DropDownIcon"),
			@FindBy(css = "span.pull-right.ui_mobile_primary_link.cdrpassdropdetail") })
	private WebElement usageDetailDropDown;

	@FindAll({ @FindBy(css = "span#di_CDRbillCycle"), @FindBy(id = "di_CDRbillCycle") })
	private WebElement billCycleSelectedOption;

	@FindBys(@FindBy(css = "ul#di_billCycleDropDown li a span"))
	private List<WebElement> billCycleList;

	@FindBy(css = "span#di_changeline")
	private WebElement changeLineSelectedOption;

	@FindAll({ @FindBy(id = "di_msisdnDropDown"), @FindBy(id = "pdp-dropdown") })
	private List<WebElement> msisdnDropDownList;

	@FindAll({ @FindBy(id = "voicedata00"), @FindBy(id = "di_noRecordsError") })
	private WebElement voiceData;

	@FindBys(@FindBy(css = "div#pdp-dropdown1 ul li div"))
	private List<WebElement> mobileTabsDropDown;
	
	@FindBy(css = "#filtervoice")
	private WebElement filterInVoiceTab;
	
	@FindBy(css = "#filtermessage")
	private WebElement filterInMessageTab;
	
	@FindBy(css = "#tabfilterdata")
	private WebElement filterInDataTab;
	
	@FindBy(css = "#voice span#newDate")
	private WebElement voiceDateAndTime;
	
	@FindBy(css = "#voice span#callDestination")
	private WebElement voiceDestination;
	
	@FindBy(css = "#voice span#description")
	private WebElement voiceNumber;
	
	@FindBy(css = "#voice span#minutes")
	private WebElement voiceMin;	

	@FindBy(css = "#voice span#Type")
	private WebElement voiceType;
	
	@FindBy(css = "#voiceHelperText")
	private WebElement voiceFilterHelper;
	
	@FindBy(css = "#messaging #newDateHeading")
	private WebElement messagingDateAndTime;
	
	@FindBy(css = "#messaging #callDestinationHeading")
	private WebElement messagingDestination;
	
	@FindBy(css = "#messaging #descriptionHeading")
	private WebElement messagingNumber;
	
	@FindBy(css = "#messaging #directionCodeHeading")
	private WebElement messagingDirection;
	
	@FindBy(css = "#messaging #contentTypeHeading")
	private WebElement messagingType;
	
	@FindBy(css = "#data #startDateHeading")
	private WebElement dataDate;
	
	@FindBy(css = "#data #featureHeading")
	private WebElement dateService;
	
	@FindBy(css = "#data #subtitleHeading")
	private WebElement dataType;
	
	@FindBy(css = "#data #volumeHeading")
	private WebElement dataUsage;
	
	@FindBy(css = "#di_CDRMsisdn1")
	private WebElement lineSelectorMSISDNUsagepage;
	
	@FindBy(css = ".active span[id$='FilterName']")
	private WebElement filterName;

	@FindBy(css = "#voicerow0")
	private WebElement pageLoadElement;

	@FindBy(css = ".ajax_loader")
	private WebElement pageSpinner;

	private final String pageUrl = "/calldetailrecords.forward.html";

	/**
	 * 
	 * @param webDriver
	 */
	public CallDetailRecordsForwardPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public CallDetailRecordsForwardPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public CallDetailRecordsForwardPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
			//waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
	    	//waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			Reporter.log("CallDetailRecordsForward Page is verified");
    	}catch(Exception e){
    		Assert.fail("Fail to load CallDetailRecordsForward Page");
    	}
        return this;
    }

	/**
	 * Verify Tabs
	 * 
	 * @param tab
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyTabs(String tab) {
		try {
			Boolean isTabDisplayed = false;
			waitFor(ExpectedConditions.invisibilityOfElementLocated(deviceWaitCursor));
			for (WebElement webElement : verifyTabs) {
				if (webElement.isDisplayed() && tab.equalsIgnoreCase(webElement.getText())) {
					isTabDisplayed = true;
					Reporter.log("Verified tab: " + tab);
					break;
				}
			}
			Assert.assertTrue(isTabDisplayed, "Tab: " + tab + " not found");
		} catch (Exception e) {
			Assert.fail("Tabs not found");
		}
		return this;
	}

	/**
	 * Click Voice Tab
	 */
	public CallDetailRecordsForwardPage clickVoiceTab() {
		try {
			clickElementWithJavaScript(voiceTab);
			Reporter.log("Clicked on Voice tab");
		} catch (Exception e) {
			Assert.fail("Voice tab not found");
		}
		return this;
	}

	/**
	 * Click Messaging Tab
	 */
	public CallDetailRecordsForwardPage clickMessagingtab() {
		try {
			clickElementWithJavaScript(messageTab);
			Reporter.log("Clicked on Messaging tab");
		} catch (Exception e) {
			Assert.fail("Messaging tab not found");
		}
		return this;
	}

	/**
	 * Click Data Tab
	 */
	public CallDetailRecordsForwardPage clickDatatab() {
		try {
			clickElementWithJavaScript(dataTab);
			Reporter.log("Clicked on Data tab");
		} catch (Exception e) {
			Assert.fail("Data tab not found");
		}
		return this;
	}

	/**
	 * Verify Data Pass Usage
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyDataPassUsage() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(dataPassUsage));
			for (WebElement datapassUsageRow : dataPassUsage) {
				if (!datapassUsageRow.getText().contains("There are no records at this time")) {
					break;
				} else {
					Reporter.log("Data pass usage table is empty");
				}
			}
			Reporter.log("Verified data pass usage");
		} catch (Exception e) {
			Assert.fail("Data pass usage not found");
		}
		return this;
	}

	/**
	 * Verify Voice Pass Usage
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyVoiceUsage() {
		try {
			if (!voiceUsage.isEmpty())
				Reporter.log("Voice records are verified");
			else
				Assert.fail("No Voice records are found");
		} catch (Exception e) {
			Assert.fail("Voice record not found");
		}
		return this;
	}

	/**
	 * Verify Message Pass Usage
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyMessageUsage() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(messageUsage));
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			if (!messageUsage.isEmpty())
				Reporter.log("Message usage verified");
			else
				Assert.fail("Message usage not found");
		} catch (Exception e) {
			Assert.fail("Message usage not found");
		}
		return this;
	}

	/**
	 * Verify Voice Filter
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyFilteringByNumber() {
		try {
			String filterNumber = numberForFiltering.getText();
			numberForFiltering.click();
			waitFor(ExpectedConditions.visibilityOf(filterName));
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			for (WebElement webElement : filteredNumbersInTable) {
				Assert.assertTrue(filterNumber.equalsIgnoreCase(webElement.getText()), "Some rows dont contain filtered parameter");
			}
			Reporter.log("Data is filtered by Number correctly");
		} catch (Exception e) {
			Assert.fail("Filtering by Number was not successfull");
		}
		return this;
	}

	/**
	 * Verify Download Usage Records Link
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyDownloadUsageRecordsLink() {
		try {
			downloadUsageRecords.isDisplayed();
			Assert.assertTrue(downloadUsageRecords.isDisplayed(), "Download usage records not displayed");
			Reporter.log("Download usage records link displayed");
		} catch (Exception e) {
			Assert.fail("Download usage records not found");
		}
		return this;
	}

	/**
	 * verify voice tab on mobile
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyVoiceMobile() {
		try {
			// return getText(verifyVoiceMobile);
			verifyVoiceMobile.isDisplayed();
			Reporter.log("Verified voice mobile tab");
		} catch (Exception e) {
			Assert.fail("Voice mobile tab not found");
		}
		return this;
	}

	/**
	 * verify voice tab on desktop
	 * 
	 * @param tab
	 * @return boolean
	 */
	public CallDetailRecordsForwardPage verifyVoiceTab(String tab) {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(deviceWaitCursor));
			waitFor(ExpectedConditions.visibilityOf(verifyVoiceTab));
			Assert.assertTrue(tab.equalsIgnoreCase(verifyVoiceTab.getText()), "Voice tab not found");
			Reporter.log("Verified voice tab");
		} catch (Exception e) {
			Assert.fail("Voice tab not found");
		}
		return this;
	}

	/**
	 * verify messaging tab
	 * 
	 * @param tab
	 * @return boolean
	 */
	public CallDetailRecordsForwardPage verifyMessagingTab(String tab) {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(deviceWaitCursor));
			waitFor(ExpectedConditions.visibilityOf(verifyMessagingTab));
			Assert.assertTrue(tab.equalsIgnoreCase(verifyMessagingTab.getText()), "Messaging tab not found");
			Reporter.log("Verified messaging tab");
		} catch (Exception e) {
			Assert.fail("Messaging tab not found");
		}
		return this;
	}

	/**
	 * verify data tab
	 * 
	 * @param tab
	 * @return boolean
	 */
	public CallDetailRecordsForwardPage verifyDataTab(String tab) {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(deviceWaitCursor));
			waitFor(ExpectedConditions.visibilityOf(verifyDataTab));
			Assert.assertTrue(tab.equalsIgnoreCase(verifyDataTab.getText()), "Data tab not found");
			Reporter.log("Verified Data tab");
		} catch (Exception e) {
			Assert.fail("Data tab not found");
		}
		return this;
	}

	/**
	 * verify T-Mobile purchases tab
	 * 
	 * @param tab
	 * @return boolean
	 */
	public CallDetailRecordsForwardPage verifyTmobilePurchasesTab(String tab) {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(deviceWaitCursor));
			waitFor(ExpectedConditions.visibilityOf(verifyTmobilePurchasesTab));
			Assert.assertTrue(tab.equalsIgnoreCase(verifyTmobilePurchasesTab.getText()),
					"T-mobile purchases tab not displayed");
			Reporter.log("Verified T-mobile purchases tab");
		} catch (Exception e) {
			Assert.fail("T-mobile purchases tab not displayed");
		}
		return this;
	}

	/**
	 * verify Third party purchases tab
	 * 
	 * @param tab
	 * @return boolean
	 */
	public CallDetailRecordsForwardPage verifyThirdPartyPurchasesTab(String tab) {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(deviceWaitCursor));
			waitFor(ExpectedConditions.visibilityOf(verifyThirdPartyPurchasesTab));
			Assert.assertTrue(tab.equalsIgnoreCase(verifyThirdPartyPurchasesTab.getText()),
					"Third party purchase tabs not found");
			Reporter.log("Verified third party purchases tab");
		} catch (Exception e) {
			Assert.fail("Third party purchase tabs not found");
		}
		return this;
	}

	/**
	 * click voice toggle
	 */
	public CallDetailRecordsForwardPage clickTabToggleSelector() {
		try {
			clickElementWithJavaScript(clickToggleSelector);
			Reporter.log("Clicked on tab toggle selector");
		} catch (Exception e) {
			Assert.fail("Tab toggle selector not found");
		}
		return this;
	}

	/**
	 * Click Usage details Drop Down Icon
	 */
	public CallDetailRecordsForwardPage clickDropDownIcon() {
		try {
			usageDetailDropDown.click();
			Reporter.log("Clicked on dropdown icon");
		} catch (Exception e) {
			Assert.fail("Dropdown icon not found");
		}
		return this;
	}

	/**
	 * Select Past Usage Bill Cycle
	 */

	public CallDetailRecordsForwardPage selectPastbillcycle() {
		try {
			for (WebElement webElement : billCycleList) {
				if (!webElement.getText().contains("Current")) {
					webElement.click();
					break;
				}
			}
			Reporter.log("Selected past bill cycle");
		} catch (Exception e) {
			Assert.fail("Past bill cycle selection field not found");
		}
		return this;
	}

	public String getBillCycleSelectedOption() {
		try {
			checkPageIsReady();
			billCycleSelectedOption.getText();
			Reporter.log(billCycleSelectedOption.getText() + " is selected");
		} catch (Exception e) {
			Assert.fail("Bill cycle dropdown not found");
		}
		return billCycleSelectedOption.getText();
	}

	/**
	 * Verify Bill Cycle Selected option
	 * 
	 * @param option
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyBillCycleSelectedOption(String option) {
		try {
			if (!option.equalsIgnoreCase(billCycleSelectedOption.getText())) {
				Reporter.log("Bill Cycle displayed");
			} else
				Assert.fail("Bill cycle not selected");
		} catch (Exception e) {
			Assert.fail("Bill cycle option is not changed");
		}
		return this;
	}

	/**
	 * Click Change line Drop Down
	 */
	public CallDetailRecordsForwardPage clickChangeLineDropDown() {
		try {
			changeLineSelectedOption.click();
			Reporter.log("Clicked on change line dropdown");
		} catch (Exception e) {
			Assert.fail("Change line dropdown not found");
		}
		return this;
	}

	/**
	 * Select MSISDN Line from DropDown and verify MSISDN Data
	 */
	public CallDetailRecordsForwardPage selectLineFromDropDown() {
		try {
			for (WebElement msisdnLine : msisdnDropDownList) {
				msisdnLine.click();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("device_waitCursor")));
				if (voiceData.isDisplayed() || voiceData.getText().contains("There are no records at this time")) {
					Reporter.log("Voice data displayed");
				}
			}
			Reporter.log("Line selected from dropdown");
		} catch (Exception e) {
			Assert.fail("Line selection dropdown not found");
		}
		return this;
	}

	/**
	 * select tab from mobile dropdown
	 * 
	 * @param tab
	 */
	public CallDetailRecordsForwardPage selectTabMobileDropdown(String tab) {
		try {
			checkPageIsReady();
			for (WebElement mobileTab : mobileTabsDropDown) {
				if (mobileTab.isDisplayed() && mobileTab.getText().contains(tab)) {
					mobileTab.click();
					break;
				}
			}
			Reporter.log("Selected tab mobile dropdown");
		} catch (Exception e) {
			Assert.fail("Mobile dropdown not found");
		}
		return this;
	}
	
	/**
	 * Click FilterVoice Option
	 */
	public CallDetailRecordsForwardPage clickFilterInVoiceTab() {
		try {
			clickElementWithJavaScript(filterInVoiceTab);
			Reporter.log("Clicked on Filter CTA in Voice tab");
		} catch (Exception e) {
			Assert.fail("Filter in Vvoice tab not found");
		}
		return this;
	}
	
	/**
	 * Verify Filter Voice Date Option
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyVoiceTableAllRowsAreDisplayed() {
		try {
			Assert.assertTrue(voiceDateAndTime.isDisplayed(), "Voice Date and Time row is not displayed");
			Assert.assertTrue(voiceDestination.isDisplayed(), "Voice Destination row is not displayed");
			Assert.assertTrue(voiceNumber.isDisplayed(), "Voice Number row is not displayed");
			Assert.assertTrue(voiceMin.isDisplayed(), "Voice Min row is not displayed");
			Assert.assertTrue(voiceType.isDisplayed(), "Voice Type row is not displayed");
			Reporter.log("All rows inside Voice table are displayed");
		} catch (Exception e) {
			Assert.fail("Voice table rows not found");
		}
		return this;
	}
	
	/**
	 * Click FilterMessage Option
	 */
	public void clickFilterInMessageTab() {
		try {
			clickElementWithJavaScript(filterInMessageTab);
			Reporter.log("Clicked on Filter CTA in Message tab");
		} catch (Exception e) {
			Assert.fail("Filter in message tab not found");
		}
	}
	
	/**
	 * Verify Filter Message Date Option
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyMessagingTableAllRowsAreDisplayed() {
		try {
			Assert.assertTrue(messagingDateAndTime.isDisplayed(), "Messaging Date and Time row is not displayed");
			Assert.assertTrue(messagingDestination.isDisplayed(), "Messaging Destination row is not displayed");
			Assert.assertTrue(messagingNumber.isDisplayed(), "Messaging Number row is not displayed");
			Assert.assertTrue(messagingDirection.isDisplayed(), "Messaging Direction row is not displayed");
			Assert.assertTrue(messagingType.isDisplayed(), "Messaging Type row is not displayed");
			Reporter.log("All rows inside Messaging table are displayed");
		} catch (Exception e) {
			Assert.fail("Messaging table not found");
		}
		return this;
	}
	
	/**
	 * Click FilterData Option
	 */
	public void clickFilterInDataTab() {
		try {
			if (!filterInDataTab.isDisplayed()) {
				dataTab.click();
			}
			waitFor(ExpectedConditions.visibilityOf(filterInDataTab));
			clickElementWithJavaScript(filterInDataTab);
			Reporter.log("Clicked on Filter CTA in Data tab");
		} catch (Exception e) {
			Assert.fail("Filter in data tab not found");
		}
	}
	
	/**
	 * Verify Filter Data Date Option
	 * 
	 * @return
	 */
	public CallDetailRecordsForwardPage verifyDataTableAllRowsAreDisplayed() {
		try {
			Assert.assertTrue(dataDate.isDisplayed(), "Data Date row is not displayed");
			Assert.assertTrue(dateService.isDisplayed(), "Data Service row is not displayed");
			Assert.assertTrue(dataType.isDisplayed(), "Data Type row is not displayed");
			Assert.assertTrue(dataUsage.isDisplayed(), "Data Usage row is not displayed");
			Reporter.log("All rows inside Data table are displayed");
		} catch (Exception e) {
			Assert.fail("Data Date table not found");
		}
		return this;
	}
	
	/**
	 * Select virtual line in the line selector drop down
	 */
	public CallDetailRecordsForwardPage selectVirtuallinLineselector() {
		try {
			changeLineSelectedOption.click();
			lineSelectorMSISDNUsagepage.click();
			Reporter.log("Virtual line selected");
		} catch (Exception e) {
			Assert.fail("Virtual line selector not found");
		}
		return this;
	}


	/**
	 * Click Third-Party Purchases Tab
	 */
	public CallDetailRecordsForwardPage clickThirdPartyPurchasestab() {
		try {
			clickElementWithJavaScript(thirdPartyTab);
			Reporter.log("Clicked on ThirdParty Purchases tab");
		} catch (Exception e) {
			Assert.fail("Third Party Purchases tab not found");
		}
		return this;
	}
	
	
}
