/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;

/**
 * @author csudheer
 *
 */

public class MilitaryVerificationPage extends CommonPage {

	//
	@FindBy(css = "span[aria-label='clickable Edit verification form']")
	private WebElement editVerificationForm;

	@FindBy(css = "p.Display3")
	private WebElement headerOnUploadReceivedAndUnderReview;

	@FindBy(css = "label[for='firstName']")
	private WebElement textFieldFirstNameOfMilitaryMember;

	@FindBy(id = "firstName")
	private WebElement fieldFirstNameOfMilitaryMember;

	@FindBy(xpath = "//span[text()='Active Duty']//..//..//label")
	private WebElement radioButtonForActiveDuty;

	@FindBy(xpath = "//span[text()='National Guard or Reserve']//..//..//label")
	private WebElement radioButtonForNationalGuardOrReserve;

	@FindBy(xpath = "//span[text()='Gold Star']//..//..//label")
	private WebElement radioButtonForGoldStar;

	// Military verified elements

	@FindBy(css = "div i.d-md-inline-block")
	private WebElement iconOnVerifiedStatusPage;

	@FindBy(css = ".H5-heading")
	private WebElement verifiedSubTitleOnVerifiedStatusPage;

	@FindBy(css = "p.body")
	private List<WebElement> successMessageForMilitaryPlanUserOnVerifiedStatusPage;

	@FindBy(css = "p.Display6")
	private List<WebElement> otherHeaders;

	// ================================================

	@FindBy(css = ".H5-heading.text-center")
	private WebElement stepNameOnMilitaryVerificationPage;

	@FindBy(css = ".body.text-center.padding-vertical-xsmall")
	private WebElement descriptionCopyOnMilitaryVerificationPage;

	@FindBy(css = ".black.text-decoration-underline.no-wrap")
	private WebElement faqLinkOnMilitaryVerificationPage;

	@FindBy(xpath = "(//input[@type='radio'])[1]")
	private WebElement radioButtonForVeteranOrRetiree;

	@FindBy(xpath = "((//*[@class='body pl-1'])[1])")
	private WebElement labelOfVeteranOrRetiree;

	@FindBy(xpath = "((//*[@class='legal pl-1 padding-top-base'])[1])")
	private WebElement subLabelOfVeteranOrRetiree;

	@FindBy(xpath = "((//*[@class='body pl-1'])[2])")
	private WebElement labelOfActiveDuty;

	@FindBy(xpath = "((//*[@class='legal pl-1 padding-top-base'])[2])")
	private WebElement subLabelOfActiveDuty;

	@FindBy(xpath = "((//*[@class='body pl-1'])[3])")
	private WebElement labelOfNationalGuardOrReserve;

	@FindBy(xpath = "((//*[@class='legal pl-1 padding-top-base'])[3])")
	private WebElement subLabelOfNationalGuardOrReserve;

	@FindBy(xpath = "((//*[@class='body pl-1'])[4])")
	private WebElement labelOfGoldStar;

	@FindBy(xpath = "((//*[@class='legal pl-1 padding-top-base'])[4])")
	private WebElement subLabelOfGoldStar;

	@FindBy(xpath = "(//*[contains(@class,'body padding-top-xl')])")
	private WebElement labelOfAllFieldsRequired;

	@FindBy(xpath = "(//*[@id='lastName'])")
	private WebElement textFieldLastNameOfMilitaryMember;

	@FindBy(xpath = "(//*[@id='email'])")
	private WebElement textFieldEmailAddress;

	@FindBy(xpath = "(//*[@id='confirmemail'])")
	private WebElement textFieldConfirmEmailAddress;

	@FindBy(xpath = "(//*[@id='msisdn'])")
	private WebElement textFieldPhoneNumber;

	@FindBy(css = "button.PrimaryCTA.full-btn-width")
	private WebElement submitInformationCTA;

	@FindBy(xpath = "(//*[@id='model'])")
	private WebElement branchOfServiceDropDown;

	private String optionsOfBranchOfServiceDropdown = "(//*[@id='branchOfService']/ul/li/a)";

	@FindBy(xpath = "(//*[@id='model1'])")
	private WebElement dischargeDateDropDown;

	private String optionsOfDischargeDateDropdown = "(//*[@id='dischargeDate']/ul/li/a)";

	@FindBy(xpath = "(//*[@id='monthddl' and @class='dropdown-toggle'])")
	private WebElement monthDropDown;

	private String optionsOfMonthDropdown = "(//*[@id='monthddl']/ul/li/a)";

	@FindBy(xpath = "(//*[@id='dayddl' and @class='dropdown-toggle'])")
	private WebElement dayDropDown;

	private String optionsOfDayDropdown = "(//*[@id='dayddl']/ul/li/a)";

	@FindBy(xpath = "(//*[@id='yearddl' and @class='dropdown-toggle'])")
	private WebElement yearDropDown;

	private String optionsOfYearDropdown = "(//*[@id='yearddl']/ul/li/a)";

	@FindBy(xpath = "(.//*[@id='relationship'])")
	private WebElement relationshipDropDown;

	@FindBy(css = "ul[aria-labelledby='relationship'] a")
	private List<WebElement> optionsOfRelationshipDropdown;
	
	@FindBy(xpath = "(//*[text()='Branch of service'])")
	private WebElement labelBranchOfService;

	@FindBy(xpath = "(//*[text()='Discharge Date'])")
	private WebElement labelDischargeDate;

	@FindBy(xpath = "(//*[text()='Date of birth'])")
	private WebElement labelDateOfBirth;

	@FindBy(id = "relationDropdownLabel")
	private WebElement labelRelationshipToMilitaryMember;

	@FindBy(css = "div.legal")
	private List<WebElement> legalText;

	@FindBy(xpath = "(//*[contains(text(),'powered by SheerID')])")
	private WebElement legalTextForSheerID;

	@FindBy(xpath = "//*[contains(@class,'legal color-red text')]")
	private WebElement errorMessage;

	@FindBy(xpath = "(//*[contains(@class,'warning-red-outline')])")
	private WebElement errorMessageIcon;

	@FindBy(css = ".exclamation-error-64px.d-inline-block")
	private WebElement iconOnContactPAHScreen;

	@FindBy(xpath = "(//*[contains(text(),'Please contact an authorized')])")
	private WebElement messageOnContactPAHScreen;

	@FindBy(css = ".PrimaryCTA.full-btn-width.float-md-none")
	private WebElement okCTAOnContactPAHScreen;

	@FindBy(xpath = "(//p[@class='Display3 padding-bottom-small'])")
	private WebElement headerTitleOnContactPAHScreen;

	@FindBy(xpath = "(//*[contains(text(),'Military Verification')])")
	private WebElement militaryVerificationTabInProfilePage;

	@FindBy(css = ".clock-64px.d-none.d-md-inline-block")
	private WebElement iconOnUploadReceivedAndUnderReview;

	@FindBy(css = ".H5-heading.text-center")
	private WebElement subTitleForUploadReceivedAndUnderReview;

	@FindBy(xpath = "//p[(@class='body text-center') and (contains(text(),'documentation'))]")
	private WebElement messageOnUploadReceivedAndUnderReview;

	@FindBy(xpath = "//p[(@class='body text-center') and (contains(text(),'Note:'))]")
	private WebElement messageNoteOnUploadReceivedAndUnderReview;

	@FindBy(css = ".PrimaryCTA.full-btn-width.text-center")
	private WebElement doneCTAOnUploadReceivedAndUnderReview;

	@FindBy(css = ".col-md-8.offset-md-2.padding-top-medium.body.text-center")
	private WebElement thankYouMessageOnVerifiedStatusPage;

	@FindBy(xpath = "(//*[contains(@class,'body padding-top-xsmall-xs')])[1]")
	private WebElement nameFieldOnVerifiedStatusPage;

	@FindBy(xpath = "(//*[contains(@class,'body padding-top-xsmall-xs')])[2]")
	private WebElement emailFieldOnVerifiedStatusPage;

	@FindBy(css = ".PrimaryCTA.full-btn-width.text-center")
	private WebElement doneCTAOnVerifiedStatusPage;

	@FindBy(css = ".warning-red-outline.d-inline-block")
	private WebElement iconOnTooManyFailedAttemptPage;

	@FindBy(css = ".Display3.padding-bottom-small")
	private WebElement headerOnTooManyFailedAttemptPage;

	@FindBy(css = ".H5-heading")
	private WebElement subTitleOnTooManyFailedAttemptPage;

	@FindBy(css = ".body.padding-top-xsmall")
	private WebElement messageOnTooManyFailedAttemptPage;

	@FindBy(css = ".SecondaryCTA.full-btn-width.float-md-right.float-lg-right.float-xl-right.blackCTA")
	private WebElement restartVerificationCTAOnTooManyFailedAttemptPage;

	@FindBy(css = ".PrimaryCTA.full-btn-width.float-md-left")
	private WebElement contactCustomerCareCTAOnTooManyFailedAttemptPage;

	@FindBy(xpath = "//*[contains(@class,'col-md-10 offset-md-1 Display3')]")
	private WebElement headerOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "//*[contains(@class,'H5-heading')]")
	private WebElement subtitleOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='col-md-8 offset-md-2 text-center body'])")
	private WebElement message1OnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='padding-top-large padding-bottom-xsmall'])")
	private WebElement message2OnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='col-6 d-inline text-center brand-magenta body'])")
	private WebElement editVerificationFormLinkOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//span[@class='cursor'])")
	private WebElement contactCustomerCareLinkOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='col-md-6 offset-md-3 Display6'])[1]")
	private WebElement labelConfirmYourInformationOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[contains(@class,'padding-top-small-sm padding-top-medium body-disabled')])[1]")
	private WebElement labelNameOfMilitaryMemberOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='border-E8E8E8 pt-1 pb-3 pt-md-2 pb-md-4 body black'])[1]")
	private WebElement fieldNameOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[contains(@class,'padding-top-small-sm padding-top-medium body-disabled')])[2]")
	private WebElement labelDOBOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='border-E8E8E8 pt-1 pb-3 pt-md-2 pb-md-4 body black'])[2]")
	private WebElement fieldDOBOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[contains(@class,'padding-top-small-sm padding-top-medium body-disabled')])[2]")
	private WebElement labelStatusOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='border-E8E8E8 pt-1 pb-3 pt-md-2 pb-md-4 body black'])[2]")
	private WebElement fieldStatusOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[contains(@class,'padding-top-small-sm padding-top-medium body-disabled')])[3]")
	private WebElement labelBranchOfServiceOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='border-E8E8E8 pt-1 pb-3 pt-md-2 pb-md-4 body black'])[3]")
	private WebElement fieldBranchOfServiceOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='col-md-6 offset-md-3 Display6'])[2]")
	private WebElement documentsMustIncludeOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//img[contains(@class,'checkImage')])[1]")
	private WebElement firstCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//img[contains(@class,'checkImage')])[1]")
	private WebElement secondCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//img[contains(@class,'checkImage')])[1]")
	private WebElement thirdCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//img[contains(@class,'checkImage')])[1]")
	private WebElement fourthCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='align-self-center']/div)[1]")
	private WebElement labelFirstNameUnderDocumentMustOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='align-self-center']/div)[3]")
	private WebElement labelLastNameUnderDocumentMustOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='align-self-center']/div)[5]")
	private WebElement labelDOBUnderDocumentMustOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='align-self-center']/div)[5]")
	private WebElement labelCurrentMilitaryStatusOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[@class='align-self-center']/div)[6]")
	private WebElement labelExampleOnUploadDocumentPage;

	@FindBy(xpath = "(//*[@class='col-md-6 offset-md-3 legal padding-bottom-small'])")
	private WebElement notificationTextOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[contains(text(),'Acceptable')])")
	private WebElement acceptableFileFormatTextOnUploadDocumentFirstAttemptPage;

	@FindBy(xpath = "(//*[contains(text(),'Max file size')])")
	private WebElement maxFileSizeTextOnUploadDocumentFirstAttemptPage;

	@FindBy(css = ".SecondaryCTA.full-btn-width.full-btn-width.float-md-right.float-lg-right.float-xl-right")
	private WebElement chooseFileToUploadCTAOnUploadDocumentFirstAttemptPage;

	@FindBy(css = ".PrimaryCTA.full-btn-width.float-md-left.float-lg-left.float-xl-left")
	private WebElement submitDocumentationOnUploadDocumentFirstAttemptPage;

	@FindBy(css = ".col-md-6.offset-md-3.text-center.padding-bottom-small.legal")
	private WebElement faqTextOnUploadDocumentFirstAttemptPage;

	@FindBy(css = ".black.text-decoration-underline.no-wrap")
	private WebElement faqLinkOnUploadDocumentFirstAttemptPage;

	@FindBy(css = "span.arrow-back")
	private WebElement backArrowOnMilitaryVerificationPage;

	@FindBy(css = ".fa.fa-phone.pull-right.phone-size")
	private WebElement contactUSIconOnMilitaryVerificationPage;

	@FindBy(css = ".icon-size")
	private WebElement logoTMobileIcon;

	@FindBy(css = ".exclamation-error-64px.d-inline-block")
	private WebElement iconOnDocumentNotAcceptedPage;

	@FindBy(css = ".Display3.padding-bottom-small")
	private WebElement headerOnDocumentNotAcceptedPage;

	@FindBy(css = ".H5-heading")
	private WebElement subTitleOnDocumentNotAcceptedPage;

	@FindBy(xpath = "(//*[contains(@class,'col-12 col-md-10 offset-md-1')]/p)[1]")
	private WebElement message1OnDocumentNotAcceptedPage;

	@FindBy(xpath = "(//*[contains(@class,'col-12 col-md-10 offset-md-1')]/p)[2]")
	private WebElement message2OnDocumentNotAcceptedPage;

	@FindBy(xpath = "(//*[contains(@class,'col-12 col-md-10 offset-md-1')])[3]")
	private WebElement errorMessageOnDocumentNotAcceptedPage;

	@FindBy(css = ".PrimaryCTA.full-btn-width.float-md-none")
	private WebElement uploadNewDocumentCTAOnDocumentNotAcceptedPage;

	public MilitaryVerificationPage(WebDriver webDriver) {
		super(webDriver);
	}

	public MilitaryVerificationPage clickOnMillitaryStatusTabOnProfilePage() {
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.clickProfilePageLink("Military Verification");
		return this;
	}

	public MilitaryVerificationPage clickOnMillitaryVerificationTab() {
		militaryVerificationTabInProfilePage.isDisplayed();

		try {
			militaryVerificationTabInProfilePage.isDisplayed();
			Reporter.log("Military verification tab is displayed on Profile page.");
			militaryVerificationTabInProfilePage.click();
			Reporter.log("Military verification tab is clicked on Profile page.");
			verifyMilitaryVerificationPage();
		} catch (Exception e) {
			Assert.fail("Military verification tab is not displayed on Profile page.");
		}
		return this;
	}

	/**
	 * Verify Millitary Verification Page loaded or not
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public MilitaryVerificationPage verifyMilitaryVerificationPage() {
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		try {
			// Assert.assertTrue(verifyElementText(upgradeYourServices, "Manage
			// your data
			// and Add-ons"),"Manage your data and AddOns page is loaded");
			Reporter.log("Military verification page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Military verification page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Millitary Upload Documents Page loaded or not
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public MilitaryVerificationPage verifyUploadDocumentsPage() {
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		checkPageIsReady();
		try {
			// Assert.assertTrue(verifyElementText(upgradeYourServices, "Manage
			// your data
			// and Add-ons"),"Manage your data and AddOns page is loaded");
			Reporter.log("Upload Document page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Upload Document page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Too Many failed attempt Page loaded or not
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public MilitaryVerificationPage verifyTooManyFailedAttemptPage() {
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		checkPageIsReady();
		try {
			// Assert.assertTrue(verifyElementText(upgradeYourServices, "Manage
			// your data
			// and Add-ons"),"Manage your data and AddOns page is loaded");
			Reporter.log("Too many failed attempt page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Too Many failed attempt page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Verified status page.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public MilitaryVerificationPage verifyVerifiedStatusPage() {
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		checkPageIsReady();
		try {
			Reporter.log("Verified status page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Verified status page is not displayed.");
		}
		return this;
	}

	/**
	 * Verify Too Many failed attempt Page loaded or not
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public MilitaryVerificationPage waitToLoadContactUSPage() {
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		try {
			// Assert.assertTrue(verifyElementText(upgradeYourServices, "Manage
			// your data
			// and Add-ons"),"Manage your data and AddOns page is loaded");
			Reporter.log("Contact US page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Contact US page is not displayed");
		}
		return this;
	}

	public MilitaryVerificationPage verifyWhetherProfilePageLoadedOrNot() {
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		return this;
	}

	public MilitaryVerificationPage verifyWhetherHomePageLoadedOrNot() {
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		return this;
	}

	/**
	 * Navigate to Military Verification page
	 */
	public MilitaryVerificationPage navigateToMilitaryVerificationPage() {
		String currentURL[];
		currentURL = getDriver().getCurrentUrl().split("home");
		currentURL[0] = currentURL[0] + "military";
		getDriver().get(currentURL[0]);
		verifyMilitaryVerificationPage();
		return this;
	}

	public MilitaryVerificationPage verifyEachComponentOfVeteranOrRetireeOnMilitaryVerificationPage() {
		// clickOnMillitaryStatusTabOnProfilePage();
		/*
		 * navigateToMilitaryVerificationPage();
		 * 
		 * checkHeaderTextOnMilitaryVerificationPage();
		 * checkStepNameOnMilitaryVerificationPage();
		 * checkDescriptionOnMilitaryVerificationPage();
		 * 
		 * checkRadioButtonForVeteranOrRetireeSelectedByDefault();
		 * checkLabelForVeteranOrRetiree(); checkSubLabelForVeteranOrRetiree();
		 * 
		 * checkRadioButtonForActiveDuty(); checkLabelForActiveDuty();
		 * checkSubLabelForActiveDuty();
		 * 
		 * checkRadioButtonForNationalGuardOrReserve();
		 * checkLabelForNationalGuardOrReserve();
		 * checkSubLabelForNationalGuradOrReserve();
		 * 
		 * checkRadioButtonForGoldStar(); checkLabelForGoldStar();
		 * checkSubLabelForGoldStar();
		 * 
		 * checkLabelAllFieldsAreRequired();
		 * checkFieldFirstNameOfMilitaryMember();
		 * checkFieldLastNameOfMilitaryMember();
		 * checkFieldEmailAddressOfMilitaryMember();
		 * checkFieldConfirmEmailAddressOfMilitaryMember();
		 * checkFieldPhoneNumberOnMilitaryMember();
		 * 
		 * checkBranchOfServiceDropdown(); optionFromBranchOfServiceDropdown();
		 * 
		 * checkDischargeDateServiceDropdown();
		 * optionsFromDischargeDateDropdown();
		 * 
		 * checkMonthDropdown(); optionsFromDischargeDateDropdown();
		 * 
		 * checkDayDropdown(); optionsOfDayDropdown();
		 * 
		 * checkYearDropdown(); optionsOfYearDropdown();
		 */

		// getAndAddOnHitURLWithIneligibleSOC()
		// verifyUpgradeYourServicesPage();
		// verifyErrorMessageForIneligibleSOC();
		// checkErrorMessageForIneligibleSOCsInURL();
		// verifyContinueCTAForIneligibleSOC();
		// clickOnContinueCTAForIneligibleSOC();
		// verifyUpgradeYourServicesPage();
		return this;
	}

	// Check Header SubHeader and Description section
	public MilitaryVerificationPage checkHeaderSubHeaderAndDescriptionOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		checkHeaderTextOnMilitaryVerificationPage();
		checkStepNameOnMilitaryVerificationPage();
		checkDescriptionOnMilitaryVerificationPage();
		return this;
	}

	// Check Redirection of FAQ link
	public MilitaryVerificationPage checkRedirectionOfFAQURLOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		checkFAQLink();
		checkFAQLinkRedirection(faqLinkOnMilitaryVerificationPage);
		return this;
	}

	// Check each components for Veteran and Retiree
	public MilitaryVerificationPage verifyEachComponentForVeteranOrRetireeOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		// navigateToMilitaryVerificationPage();

		checkRadioButtonForVeteranOrRetireeSelectedByDefault();
		checkLabelForVeteranOrRetiree();
		checkSubLabelForVeteranOrRetiree();

		checkLabelAllFieldsAreRequired();

		checkWhetherFirstNameFieldIsDisabled();
		checkWhetherLastNameFieldIsDisabledOrNot();

		checkBranchOfServiceDropdown();
		// optionFromBranchOfServiceDropdown();
		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		checkSubmitCTAIsDisabledOrNot();

		checkDischargeDateServiceDropdown();
		selectOptionFromDischargeDate("1995");
		checkSubmitCTAIsDisabledOrNot();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		// optionsFromDischargeDateDropdown();
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();
		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();

		checkSubmitCTAIsEnabledOrNot();

		checkLegalTextForSheeeID();

		return this;
	}

	// Check each components for Active Duty
	public MilitaryVerificationPage verifyEachComponentForActiveDutyOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		// navigateToMilitaryVerificationPage();

		clickOnActiveDutyRadioButton();

		checkSubmitCTAIsDisabledOrNot();

		checkLabelForActiveDuty();
		checkSubLabelForActiveDuty();
		checkSubmitCTAIsDisabledOrNot();

		checkLabelAllFieldsAreRequired();
		checkSubmitCTAIsDisabledOrNot();

		checkBranchOfServiceDropdown();
		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		checkSubmitCTAIsDisabledOrNot();

		verifyDischargeDateServiceDropdownNotDisplayed();

		checkFieldFirstNameOfMilitaryMember();
		checkWhetherFirstNameFieldIsEnabled();
		enterValidFirstName("Test");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldLastNameOfMilitaryMember();
		checkWhetherLastNameFieldIsEnabledOrNot();
		enterValidLastName("User");
		checkSubmitCTAIsDisabledOrNot();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		// optionsFromDischargeDateDropdown();
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();
		checkSubmitCTAIsDisabledOrNot();

		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkSubmitCTAIsEnabledOrNot();

		checkLegalTextForSheeeID();
		return this;
	}

	// Check each components for National Guard or Reserve
	public MilitaryVerificationPage verifyEachComponentForNationalGuardOrReserveOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnNationalGuardOrReserveRadioButton();

		checkLabelForNationalGuardOrReserve();
		checkSubLabelForNationalGuradOrReserve();

		checkLabelAllFieldsAreRequired();
		checkSubmitCTAIsDisabledOrNot();

		checkWhetherFirstNameFieldIsDisabled();
		checkWhetherLastNameFieldIsDisabledOrNot();
		checkSubmitCTAIsDisabledOrNot();

		checkBranchOfServiceDropdown();
		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		checkSubmitCTAIsDisabledOrNot();

		verifyDischargeDateServiceDropdownNotDisplayed();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();
		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();

		checkSubmitCTAIsEnabledOrNot();

		checkLegalTextForSheeeID();

		return this;
	}

	// Check each components for Gold Star
	public MilitaryVerificationPage verifyEachComponentForGoldStarOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnGoldStarRadioButton();

		checkLabelForGoldStar();
		checkSubLabelForGoldStar();

		checkLabelAllFieldsAreRequired();
		checkSubmitCTAIsDisabledOrNot();

		checkRelationshipToMilitaryMemberDropdown();
		selectOptionFromRelatioshipDropdown("Child");
		checkSubmitCTAIsDisabledOrNot();

		verifyBranchOfServiceDropdownNotDisplayed();
		verifyDischargeDateServiceDropdownNotDisplayed();

		checkWhetherFirstNameFieldIsDisabled();
		checkWhetherLastNameFieldIsDisabledOrNot();
		checkSubmitCTAIsDisabledOrNot();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();
		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();

		checkSubmitCTAIsEnabledOrNot();

		checkLegalTextForSheeeID();

		return this;
	}

	// Error validation
	public MilitaryVerificationPage checkBranChOfServiceAndDischargeDateFieldValidationForVeteran() {
		clickOnMillitaryStatusTabOnProfilePage();

		checkBranchOfServiceDropdown();
		selectOptionFromBranchOfServiceDropdown("Select branch of service");
		checkErrorMessageForFieldValiadation("Please select a branch of service");
		checkErrorIconForAllErrorMessagesForFieldValiadation();
		selectOptionFromBranchOfServiceDropdown("Army National Guard");

		checkDischargeDateServiceDropdown();
		selectOptionFromDischargeDate("Select discharge date");
		checkErrorMessageForFieldValiadation("Please select discharge date");
		checkErrorIconForAllErrorMessagesForFieldValiadation();
		selectOptionFromDischargeDate("1995");
		return this;
	}

	// Error validation
	public MilitaryVerificationPage checkFirstNameAndLastNameFieldValidationsOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnActiveDutyRadioButton();

		enterInValidFirstName();
		checkErrorMessageForFieldValiadation("Enter a valid name");
		checkErrorIconForAllErrorMessagesForFieldValiadation();
		enterValidFirstName("PAH");

		enterInValidLastName();
		checkErrorMessageForFieldValiadation("Enter a valid name");
		checkErrorIconForAllErrorMessagesForFieldValiadation();
		enterValidLastName("User");

		return this;
	}

	// Error validation
	public MilitaryVerificationPage checkRelationshipAndEmailAddressFieldValidationOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnGoldStarRadioButton();
		selectOptionFromRelatioshipDropdown("Select relationship");
		checkErrorMessageForFieldValiadation("Please select relationship status");
		checkErrorIconForAllErrorMessagesForFieldValiadation();
		selectOptionFromRelatioshipDropdown("Child");

		checkFieldEmailAddressOfMilitaryMember();
		enterWrongEmailAddressInConfirmEmailAddressField();
		checkErrorMessageForFieldValiadation("Email does not match");
		checkErrorIconForAllErrorMessagesForFieldValiadation();
		enterMatchingEmailAddressInConfirmEmailAddresseField();

		return this;
	}

	// Check each components for Veteran and Retiree for Business User
	public MilitaryVerificationPage verifyEachComponentForVeteranOrRetireeForBusinessUserOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		checkHeaderTextOnMilitaryVerificationPage();
		checkStepNameOnMilitaryVerificationPage();
		checkDescriptionOnMilitaryVerificationPage();

		checkRadioButtonForVeteranOrRetireeSelectedByDefault();
		checkLabelForVeteranOrRetiree();
		checkSubLabelForVeteranOrRetiree();

		checkLabelAllFieldsAreRequired();

		checkWhetherFirstNameFieldIsEnabled();
		enterValidFirstName("Business");

		checkWhetherLastNameFieldIsEnabledOrNot();
		enterValidLastName("User");

		checkBranchOfServiceDropdown();
		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		checkSubmitCTAIsDisabledOrNot();

		checkDischargeDateServiceDropdown();
		selectOptionFromDischargeDate("1995");
		checkSubmitCTAIsDisabledOrNot();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		// optionsFromDischargeDateDropdown();
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();
		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();

		checkSubmitCTAIsEnabledOrNot();
		checkLegalTextForSheeeID();

		return this;
	}

	// Check each components for Active Duty For Business Users
	public MilitaryVerificationPage verifyEachComponentForActiveDutyForBusinessUserOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnActiveDutyRadioButton();

		checkHeaderTextOnMilitaryVerificationPage();
		checkStepNameOnMilitaryVerificationPage();
		// checkDescriptionOnMilitaryVerificationPage();

		checkSubmitCTAIsDisabledOrNot();

		checkLabelForActiveDuty();
		checkSubLabelForActiveDuty();
		checkSubmitCTAIsDisabledOrNot();

		checkLabelAllFieldsAreRequired();
		checkSubmitCTAIsDisabledOrNot();

		checkBranchOfServiceDropdown();
		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		checkSubmitCTAIsDisabledOrNot();

		verifyDischargeDateServiceDropdownNotDisplayed();

		checkFieldFirstNameOfMilitaryMember();
		checkWhetherFirstNameFieldIsEnabled();
		enterValidFirstName("Business");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldLastNameOfMilitaryMember();
		checkWhetherLastNameFieldIsEnabledOrNot();
		enterValidLastName("User");
		checkSubmitCTAIsDisabledOrNot();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		// optionsFromDischargeDateDropdown();
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();
		checkSubmitCTAIsDisabledOrNot();

		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkSubmitCTAIsEnabledOrNot();

		checkLegalTextForSheeeID();
		return this;
	}

	// Check each components for National Guard or Reserve For Business Users
	public MilitaryVerificationPage verifyEachComponentForNationalGuardOrReserveForBusinessUserOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnNationalGuardOrReserveRadioButton();

		checkHeaderTextOnMilitaryVerificationPage();
		checkStepNameOnMilitaryVerificationPage();
		checkDescriptionOnMilitaryVerificationPage();

		checkLabelForNationalGuardOrReserve();
		checkSubLabelForNationalGuradOrReserve();

		checkLabelAllFieldsAreRequired();
		checkSubmitCTAIsDisabledOrNot();

		checkBranchOfServiceDropdown();
		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		checkSubmitCTAIsDisabledOrNot();

		verifyDischargeDateServiceDropdownNotDisplayed();

		checkFieldFirstNameOfMilitaryMember();
		checkWhetherFirstNameFieldIsEnabled();
		enterValidFirstName("Business");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldLastNameOfMilitaryMember();
		checkWhetherLastNameFieldIsEnabledOrNot();
		enterValidLastName("User");
		checkSubmitCTAIsDisabledOrNot();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();
		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();

		checkSubmitCTAIsEnabledOrNot();

		checkLegalTextForSheeeID();

		return this;
	}

	// Check each components for Gold Star for Business users
	public MilitaryVerificationPage verifyEachComponentForGoldStartForBusinessUserOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnGoldStarRadioButton();

		checkHeaderTextOnMilitaryVerificationPage();
		checkStepNameOnMilitaryVerificationPage();
		checkDescriptionOnMilitaryVerificationPage();

		checkLabelForGoldStar();
		checkSubLabelForGoldStar();

		checkLabelAllFieldsAreRequired();
		checkSubmitCTAIsDisabledOrNot();

		checkRelationshipToMilitaryMemberDropdown();
		selectOptionFromRelatioshipDropdown("Child");
		checkSubmitCTAIsDisabledOrNot();

		verifyBranchOfServiceDropdownNotDisplayed();
		verifyDischargeDateServiceDropdownNotDisplayed();

		checkFieldFirstNameOfMilitaryMember();
		checkWhetherFirstNameFieldIsEnabled();
		enterValidFirstName("Business");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldLastNameOfMilitaryMember();
		checkWhetherLastNameFieldIsEnabledOrNot();
		enterValidLastName("User");
		checkSubmitCTAIsDisabledOrNot();

		checkMonthDropdownFromDateOfBirthSection();
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		checkSubmitCTAIsDisabledOrNot();

		checkDayDropdownFromDateOfBirthSection();
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		checkSubmitCTAIsDisabledOrNot();

		checkYearDropdownFromDateOfBirthSection();
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");
		checkSubmitCTAIsDisabledOrNot();

		checkFieldEmailAddressOfMilitaryMember();
		checkFieldConfirmEmailAddressOfMilitaryMember();
		enterMatchingEmailAddressInConfirmEmailAddresseField();

		checkFieldPhoneNumberOnMilitaryMember();
		checkWhetherPhoneNumberFieldIsDisabledOrNot();

		checkSubmitCTAIsEnabledOrNot();
		checkLegalTextForSheeeID();

		return this;
	}

	// Check each components for Gold Star for Business users
	public MilitaryVerificationPage verifyLegalCopyForRegularUserOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		// navigateToMilitaryVerificationPage();

		verifyLegalTextOnVerificationPage();

		clickOnActiveDutyRadioButton();
		verifyLegalTextOnVerificationPage();

		clickOnNationalGuardOrReserveRadioButton();
		verifyLegalTextOnVerificationPage();

		clickOnGoldStarRadioButton();
		verifyLegalTextOnVerificationPage();

		return this;
	}

	// Check each components for Gold Star for Business users
	public MilitaryVerificationPage verifyLegalCopyForBusinessUserOnMilitaryVerificationPage() {

		clickOnMillitaryStatusTabOnProfilePage();
		verifyLegalTextOnVerificationPage();

		clickOnEditVerificationForm();

		clickOnActiveDutyRadioButton();
		checkFieldFirstNameOfMilitaryMember();

		clickOnNationalGuardOrReserveRadioButton();
		checkWhetherFirstNameFieldIsDisabled();

		clickOnGoldStarRadioButton();
		checkWhetherFirstNameFieldIsDisabled();

		return this;
	}

	// Check each components for Gold Star for Business users
	public MilitaryVerificationPage checkContactToPAHMessageOnMilitaryVerificationPageWhenStandardOrRestrictedOrNonMasterLoggedIn() {
		// clickOnMillitaryStatusTabOnProfilePage();

		clickOnMillitaryStatusTabOnProfilePage();
		checkHeaderTitleTextOnContactPAHMessageScreen();
		checkIconOnContactPAHMessageScreen();
		checkErrorMessageOnContactPAHMessageScreen();
		checkOKCTAOnContactPAHMessageScreen();
		clickOKCTAOnContactPAHMessageScreen();
		checkRedirectionToProfilePage();
		return this;
	}

	// Check Upload received and under review
	public MilitaryVerificationPage checkUploadReceivedAndUnderReviewMessageOnMilitaryVerificationPage() {
		// clickOnMillitaryStatusTabOnProfilePage();

		clickOnMillitaryStatusTabOnProfilePage();
		checkHeaderTextOnMilitaryVerificationPage();
		checkIconOnUploadReceivedAndUnderReviewPage();
		checkSubtitleForUploadReceivedAndUnderReview();
		checkMessageBelowSubtitleOnUploadReceivedAndUnderReview();
		checkNoteForUploadReceivedAndUnderReview();
		checkNameOnUploadReceivedAndUnderReview();
		checkEmailFieldOnUploadReceivedAndUnderReview();
		checkDoneCTAOnContactPAHMessageScreen();
		clickOnDoneCTAOnUploadReceivedAndUnderProcess();
		checkRedirectionToProfilePage();
		return this;
	}

	// Check Verified status page for Military plan user.
	public MilitaryVerificationPage checkVerifiedStatusPageWhenUserOnMilitaryPlan() {
		// clickOnMillitaryStatusTabOnProfilePage();

		clickOnMillitaryStatusTabOnProfilePage();
		checkHeaderTextOnMilitaryVerificationPage();
		checkIconOnVerifiedStatusPage();
		checkSubtitleOnVerifiedStatusPage();
		checkSuccessMessageForMilitaryPlanUserOnVerifiedStatusPage();
		checkThankYouForYourServiceMessageOnVerifiedStatusPage();
		checkNameOnUploadReceivedAndUnderReview();
		checkEmailFieldOnUploadReceivedAndUnderReview();
		checkApprovalDateFieldOnVerifiedStatusPage();
		return this;
	}

	// Check verified status page when user nto on a Military plan.
	public MilitaryVerificationPage checkVerifiedStatusPageWhenUserNotOnMilitaryPlan() {

		clickOnMillitaryStatusTabOnProfilePage();
		checkHeaderTextOnMilitaryVerificationPage();

		checkIconOnVerifiedStatusPage();
		checkSubtitleOnVerifiedStatusPage();

		checkSuccessMessageOnVerifiedStatusPageWhenUserNotOnMilitaryPlan();
		checkThankYouForYourServiceMessageOnVerifiedStatusPage();

		checkNameOnUploadReceivedAndUnderReview();
		checkEmailFieldOnUploadReceivedAndUnderReview();
		checkApprovalDateFieldOnVerifiedStatusPage();
		return this;
	}

	// Check Too many failed attempt page
	public MilitaryVerificationPage checkTooManyFailedAttemptsPage() {
		// clickOnMillitaryStatusTabOnProfilePage();

		clickOnMillitaryStatusTabOnProfilePage();
		verifyTooManyFailedAttemptPage();

		checkIconOnTooManyFailedAttemptsPage();
		checkHeaderTextOnTooManyFailedAttemptsPage();
		checkSubtitleOnTooManyFailedAttemptsPage();
		checkMessageOnTooManyFailedAttemptsPage();
		checkRestartVerificationCTAOnTooManyFailedAttemptsPage();
		checkContactCustomerCareCTAOnTooManyFailedAttemptsPage();

		clickOnContactCustomerCareCTAOnTooManyFailedAttemptsPage();
		waitToLoadContactUSPage();
		verifyContactUsPage();
		clickOnBrowserBack();

		verifyTooManyFailedAttemptPage();

		clickOnRestartVerificationCTAOnTooManyFailedAttemptsPage();

		return this;

	}

	// Check Upload Document First attempt page
	public MilitaryVerificationPage checkUploadDocumentFirstAttemptPage() {
		// clickOnMillitaryStatusTabOnProfilePage();

		clickOnMillitaryStatusTabOnProfilePage();

		checkHeaderTextOnUploadDocumentPage();
		checkSubtitleOnUploadDocumentPage();
		checkMessage1OnUploadDocumentPage();
		checkMessage2OnUploadDocumentPage();
		checkLinkEditVerificationFormOnUploadDocumentPage();
		checkContactCustomerCareCTAOnTooManyFailedAttemptsPage();
		checkFieldsUnderConfirmYourInformationSectionOnUploadDocumentPage();
		checkAllFieldsUnderDocumentsMustIncludeTheFollowingSectionOnUploadDocumentFirstAttemptPage();
		return this;

	}

	// Check Header and Message on Upload Document First attempt page
	public MilitaryVerificationPage checkHeaderAndMessageOnUploadDocumentPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		verifyUploadDocumentsPage();
		checkHeaderTextOnUploadDocumentPage();
		checkSubtitleOnUploadDocumentPage();
		checkMessage1OnUploadDocumentPage();
		checkMessage2OnUploadDocumentPage();
		checkMessage3OnUploadDocumentPage();
		checkLinkEditVerificationFormOnUploadDocumentPage();
		checkContactCustomerCareLinkFormOnUploadDocumentPage();
		return this;

	}

	// Check Confirm your Information on Upload Document First attempt page
	public MilitaryVerificationPage checkConfirmYourInformationSectionOnUploadDocumentPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		verifyUploadDocumentsPage();
		checkHeaderTextOnUploadDocumentPage();
		checkFieldsUnderConfirmYourInformationSectionOnUploadDocumentPage();
		return this;

	}

	// Check Document must include section on Upload Document First attempt page
	public MilitaryVerificationPage checkDocumentMustIncludeSectionOnUploadDocumentPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		verifyUploadDocumentsPage();
		checkHeaderTextOnUploadDocumentPage();
		checkAllFieldsUnderDocumentsMustIncludeTheFollowingSectionOnUploadDocumentFirstAttemptPage();
		return this;

	}

	/**
	 * Check Notification and File Size text on Upload Document page
	 */
	public MilitaryVerificationPage checkNotificationAndFileSizeMessageOnUploadDocumentPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		verifyUploadDocumentsPage();
		checkHeaderTextOnUploadDocumentPage();

		checkNotificationUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();

		checkAcceptableFileFormatsUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkTextMaxFileSizeUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		return this;
	}

	/**
	 * Check Notification and File Size text on Upload Document page
	 */
	public MilitaryVerificationPage checkCTAsAndFAQTextOnUploadDocumentFirstAttemptPage() {

		clickOnMillitaryStatusTabOnProfilePage();

		verifyUploadDocumentsPage();

		checkChooseFileToUploadCTAIsEnabledOrNot();
		checkSubmitDocumentationCTAOnUploadDocumentFirstAttemptPage();
		checkSubmitDocumentationCTAIsDisabledOrNot();
		checkFAQTextUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkFAQLinkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		return this;
	}

	/**
	 * Check Notification and File Size text on Upload Document page
	 */
	public MilitaryVerificationPage checkFAQTextAndRedirectionOfFAQLinkOnUploadDocumentFirstAttemptPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		verifyUploadDocumentsPage();
		checkFAQTextUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkFAQLinkRedirection(faqLinkOnUploadDocumentFirstAttemptPage);
		return this;
	}

	// Check Reset functionality for First name and last name
	public MilitaryVerificationPage checkResetFunctionalityForFirstNameAndLastNameForMilitaryVerificationPage() {
		// clickOnMillitaryStatusTabOnProfilePage();

		clickOnMillitaryStatusTabOnProfilePage();
		String firstNameBeforeReset = getFirstNameOfMilitaryMemberBeforeReset();
		String lastNameBeforeReset = getLastNameOfMilitaryMemberBeforeReset();

		clickOnActiveDutyRadioButton();
		enterFirstNameAndLastName();

		String firstNameAfterReset = getFirstNameOfMilitaryMemberBeforeReset();
		String lastNameAfterReset = getLastNameOfMilitaryMemberBeforeReset();

		clickOnNationalGuardOrReserveRadioButton();

		validateFirstNameOfMilitaryMemberWithTheAlreadyEnteredName(firstNameAfterReset, firstNameBeforeReset);
		validateLastNameOfMilitaryMemberWithTheAlreadyEnteredName(lastNameAfterReset, lastNameBeforeReset);

		return this;

	}

	// Check Edit Verification link redirection
	public MilitaryVerificationPage checkRedirectionOfEditVerificationLinkOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();

		verifyUploadDocumentsPage();
		checkRedirectionOfEditVerificationLink();
		return this;

	}

	// Click on Back navigation icon
	public MilitaryVerificationPage checkRedirectionOfBackArrowOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		checkBackArrowOnMilitaryVerificationPage();
		clickOnBackArrowOnMilitaryVerificationPage();
		verifyWhetherProfilePageLoadedOrNot();
		return this;
	}

	// Click on Back navigation Arrow
	public MilitaryVerificationPage checkRedirectionOfBackArrowOnMilitaryVerificationStatusPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		checkBackArrowOnMilitaryVerificationPage();
		clickOnBackArrowOnMilitaryVerificationStatusPage();
		verifyWhetherProfilePageLoadedOrNot();
		return this;
	}

	// Click on Back navigation Arrow on Document Not accepted page
	public MilitaryVerificationPage checkRedirectionOfBackArrowOnDocumentNotAcceptedPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		checkBackArrowOnMilitaryVerificationPage();
		clickOnBackArrowOnMilitaryVerificationStatusPage();
		verifyWhetherProfilePageLoadedOrNot();
		return this;
	}

	// Click on T-Mobile logo
	public MilitaryVerificationPage checkRedirectionOfTMobileLogoOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		checkTMobileLogoOnMilitaryVerificationPage();
		clickOnTMobileLogoOnMilitaryVerificationStatusPage();
		verifyWhetherHomePageLoadedOrNot();
		return this;
	}

	// Redirection of Contact us logo
	public MilitaryVerificationPage checkRedirectionOfContactUsLogoOnMilitaryVerificationPage() {
		clickOnMillitaryStatusTabOnProfilePage();
		checkContactUsLogoOnMilitaryVerificationPage();
		clickOnContactUsLogoOnMilitaryVerificationStatusPage();
		verifyContactUsPage();
		return this;
	}

	// Check Document not accepted page
	public MilitaryVerificationPage checkDocumentNotAcceptedPage() {
		// clickOnMillitaryStatusTabOnProfilePage();

		clickOnMillitaryStatusTabOnProfilePage();
		checkIconOnDocumentNotAcceptedPage();

		checkHeaderOnDocumentNotAcceptedPage();
		checkSubtitleOnDocumentNotAcceptedPage();
		checkMessage1OnDocumentOnAcceptedPage();
		checkMessage2OnDocumentOnAcceptedPage();
		checkErrorReasonsOnDocumentNotAcceptedPage();
		// checkErrorMessageOnDocumentNotAcceptedPage();
		checkUploadNewDocumentCTAOnDocumentNotAcceptedPage();
		return this;

	}

	// Check Submission flow for Veteran and Retiree
	public MilitaryVerificationPage verifySubmissionFlowForVeteranOrRetiree() {
		clickOnMillitaryStatusTabOnProfilePage();

		selectOptionFromBranchOfServiceDropdown("Army National Guard");

		selectOptionFromDischargeDate("1995");
		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");

		selectOptionFromYearDropdownFromDateOfBirthSection("1950");

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();
		String firstName = getFirstName();
		String lastName = getLastName();

		clickOnSubmitCTA();

		verifyVerifiedStatusPage();
		checkHeaderTextOnMilitaryVerificationPage();
		checkIconOnVerifiedStatusPage();
		checkSubtitleOnVerifiedStatusPage();
		checkSuccessMessageOnVerifiedStatusPageWhenUserNotOnMilitaryPlan();
		checkThankYouForYourServiceMessageOnVerifiedStatusPage();

		checkNameOnVerifiedStatusPage();
		verifyFirstNameOnVerificationPageAndVerifiedPage(firstName);
		verifyLastNameOnVerificationPageAndVerifiedPage(lastName);

		verifyEmailAddressOnMilitaryVerificationPageAndVerifiedStatusPage(emailAddressOnVerificationPage);

		checkApprovalDateFieldOnVerifiedStatusPage();
		verifyApprovalDateOnVerifiedStatusPage();

		return this;
	}

	// Check Submission flow for Veteran and Retiree
	public MilitaryVerificationPage verifySubmissionFlowForActiveDuty() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnActiveDutyRadioButton();

		selectOptionFromBranchOfServiceDropdown("Army National Guard");

		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();
		String firstName = getFirstName();
		String lastName = getLastName();

		clickOnSubmitCTA();

		verifyVerifiedStatusPage();
		checkHeaderTextOnMilitaryVerificationPage();
		checkIconOnVerifiedStatusPage();
		checkSubtitleOnVerifiedStatusPage();
		checkSuccessMessageOnVerifiedStatusPageWhenUserNotOnMilitaryPlan();
		checkThankYouForYourServiceMessageOnVerifiedStatusPage();

		checkNameOnVerifiedStatusPage();
		verifyFirstNameOnVerificationPageAndVerifiedPage(firstName);
		verifyLastNameOnVerificationPageAndVerifiedPage(lastName);

		verifyEmailAddressOnMilitaryVerificationPageAndVerifiedStatusPage(emailAddressOnVerificationPage);

		checkApprovalDateFieldOnVerifiedStatusPage();
		verifyApprovalDateOnVerifiedStatusPage();

		return this;
	}

	// Check Submission flow for National Guard or Reserve
	public MilitaryVerificationPage verifySubmissionFlowForNationalGuardOrReserve() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnNationalGuardOrReserveRadioButton();

		selectOptionFromBranchOfServiceDropdown("Army National Guard");

		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();
		String firstName = getFirstName();
		String lastName = getLastName();

		clickOnSubmitCTA();

		verifyVerifiedStatusPage();
		checkHeaderTextOnMilitaryVerificationPage();
		checkIconOnVerifiedStatusPage();
		checkSubtitleOnVerifiedStatusPage();
		checkSuccessMessageOnVerifiedStatusPageWhenUserNotOnMilitaryPlan();
		checkThankYouForYourServiceMessageOnVerifiedStatusPage();

		checkNameOnVerifiedStatusPage();
		verifyFirstNameOnVerificationPageAndVerifiedPage(firstName);
		verifyLastNameOnVerificationPageAndVerifiedPage(lastName);

		verifyEmailAddressOnMilitaryVerificationPageAndVerifiedStatusPage(emailAddressOnVerificationPage);

		checkApprovalDateFieldOnVerifiedStatusPage();
		verifyApprovalDateOnVerifiedStatusPage();

		return this;
	}

	// Check Submission flow for Gold Start
	public MilitaryVerificationPage verifySubmissionFlowForGoldStar() {
		clickOnMillitaryStatusTabOnProfilePage();

		clickOnGoldStarRadioButton();

		selectOptionFromRelatioshipDropdown("Child");

		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		selectOptionFromYearDropdownFromDateOfBirthSection("1950");

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();
		String firstName = getFirstName();
		String lastName = getLastName();

		clickOnSubmitCTA();

		verifyVerifiedStatusPage();
		checkHeaderTextOnMilitaryVerificationPage();
		checkIconOnVerifiedStatusPage();
		checkSubtitleOnVerifiedStatusPage();
		checkSuccessMessageOnVerifiedStatusPageWhenUserNotOnMilitaryPlan();
		checkThankYouForYourServiceMessageOnVerifiedStatusPage();

		checkNameOnVerifiedStatusPage();
		verifyFirstNameOnVerificationPageAndVerifiedPage(firstName);
		verifyLastNameOnVerificationPageAndVerifiedPage(lastName);

		verifyEmailAddressOnMilitaryVerificationPageAndVerifiedStatusPage(emailAddressOnVerificationPage);

		checkApprovalDateFieldOnVerifiedStatusPage();
		verifyApprovalDateOnVerifiedStatusPage();

		return this;
	}

	// Check Upload flow for Veteran and Retiree
	public MilitaryVerificationPage checkE2EFlowForUploadPageForVeteranOrRetiree() {
		String branchOfServiceOnVerificationPage = null;
		String dischargeDateOnVerificationPage = null;
		String enteredNameOnVerificationPage = null;
		String nameOnUploadPage = null;
		String dobOnVerificationPage = null;

		clickOnMillitaryStatusTabOnProfilePage();

		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		branchOfServiceOnVerificationPage = getSelectedOptionFromBranchOfServiceDropdown();

		selectOptionFromDischargeDate("1995");
		dischargeDateOnVerificationPage = getDischargeDateFromDischargeDateDropdown();

		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		selectOptionFromYearDropdownFromDateOfBirthSection("1981");
		dobOnVerificationPage = convertDateIntoMMDDYYYFormat();

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();

		enteredNameOnVerificationPage = combinedFirstAndLastName();

		clickOnSubmitCTA();

		verifyUploadDocumentsPage();

		checkHeaderTextOnUploadDocumentPage();
		checkFirstNameAndLastNameOnUploadDocumentFirstAttemptPage();
		// verifyNameOnVerificationPageAndOnUploadPage(enteredNameOnVerificationPage);
		verifyDOBOnUploadDocumentAndOnVerificationPage(dobOnVerificationPage);
		verifyStatusOnUploadDocumentPageForVeteranOrRetiree();
		verifyBranchOfServiceOnUploadAndVerificationPage(branchOfServiceOnVerificationPage);
		return this;
	}

	// Check Upload flow for Active Duty
	public MilitaryVerificationPage checkE2EFlowForUploadPageForActiveDuty() {
		String branchOfServiceOnVerificationPage = null;
		String enteredNameOnVerificationPage = null;
		String dobOnVerificationPage = null;

		clickOnMillitaryStatusTabOnProfilePage();

		clickOnActiveDutyRadioButton();

		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		branchOfServiceOnVerificationPage = getSelectedOptionFromBranchOfServiceDropdown();

		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		selectOptionFromYearDropdownFromDateOfBirthSection("1981");
		dobOnVerificationPage = convertDateIntoMMDDYYYFormat();

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();

		enteredNameOnVerificationPage = combinedFirstAndLastName();

		clickOnSubmitCTA();

		verifyUploadDocumentsPage();

		checkHeaderTextOnUploadDocumentPage();
		checkFirstNameAndLastNameOnUploadDocumentFirstAttemptPage();
		// verifyNameOnVerificationPageAndOnUploadPage(enteredNameOnVerificationPage);
		verifyDOBOnUploadDocumentAndOnVerificationPage(dobOnVerificationPage);
		verifyStatusOnUploadDocumentPageForActiveDuty();
		verifyBranchOfServiceOnUploadAndVerificationPage(branchOfServiceOnVerificationPage);
		return this;
	}

	// Check Upload flow for Active Duty
	public MilitaryVerificationPage checkE2EFlowForUploadPageForNationalGuardOrReserve() {
		String branchOfServiceOnVerificationPage = null;
		String dischargeDateOnVerificationPage = null;
		String enteredNameOnVerificationPage = null;
		String nameOnUploadPage = null;
		String dobOnVerificationPage = null;

		clickOnMillitaryStatusTabOnProfilePage();

		clickOnNationalGuardOrReserveRadioButton();

		selectOptionFromBranchOfServiceDropdown("Army National Guard");
		branchOfServiceOnVerificationPage = getSelectedOptionFromBranchOfServiceDropdown();

		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		selectOptionFromYearDropdownFromDateOfBirthSection("1981");
		dobOnVerificationPage = convertDateIntoMMDDYYYFormat();

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();

		enteredNameOnVerificationPage = combinedFirstAndLastName();

		clickOnSubmitCTA();

		verifyUploadDocumentsPage();

		checkHeaderTextOnUploadDocumentPage();
		checkFirstNameAndLastNameOnUploadDocumentFirstAttemptPage();
		// verifyNameOnVerificationPageAndOnUploadPage(enteredNameOnVerificationPage);
		verifyDOBOnUploadDocumentAndOnVerificationPage(dobOnVerificationPage);
		verifyStatusOnUploadDocumentPageForNationalGuardOrReserve();
		verifyBranchOfServiceOnUploadAndVerificationPage(branchOfServiceOnVerificationPage);
		return this;
	}

	// Check Upload flow for Gold star
	public MilitaryVerificationPage checkE2EFlowForUploadPageForGoldStar() {
		String relationshipOnVerificationPage = null;
		String dischargeDateOnVerificationPage = null;
		String enteredNameOnVerificationPage = null;
		String nameOnUploadPage = null;
		String dobOnVerificationPage = null;

		clickOnMillitaryStatusTabOnProfilePage();

		clickOnGoldStarRadioButton();

		selectOptionFromRelatioshipDropdown("Spouse");
		relationshipOnVerificationPage = getSelectedOptionFromRelationshipDropdown();

		selectOptionFromMonthDropdownFromDateOfBirthSection("February");
		selectOptionFromDayDropdownFromDateOfBirthSection("3");
		selectOptionFromYearDropdownFromDateOfBirthSection("1981");
		dobOnVerificationPage = convertDateIntoMMDDYYYFormat();

		enterMatchingEmailAddressInConfirmEmailAddresseField();
		String emailAddressOnVerificationPage = getConfirmEmailAddressOfMilitaryMember();

		enteredNameOnVerificationPage = combinedFirstAndLastName();

		clickOnSubmitCTA();

		verifyUploadDocumentsPage();

		checkHeaderTextOnUploadDocumentPage();
		checkFirstNameAndLastNameOnUploadDocumentFirstAttemptPage();
		verifyDOBOnUploadDocumentAndOnVerificationPage(dobOnVerificationPage);
		verifyRelationshipStatusOnUploadAndVerificationPage(relationshipOnVerificationPage);
		return this;
	}

	/**
	 * Check Header on Military Verification page
	 */
	public MilitaryVerificationPage checkHeaderTextOnMilitaryVerificationPage() {
		try {
			waitforSpinner();
			Assert.assertTrue(headerOnUploadReceivedAndUnderReview.isDisplayed(),
					"Header is not displayed on Millitary Verification page");
			Reporter.log("Header is displayed on Millitary Verification page");

			Assert.assertEquals(headerOnUploadReceivedAndUnderReview.getText(), "Military verification",
					" Header text on Military Verification page is mismatched");
			Reporter.log("Header on Millitary Verification page is matched");

		} catch (Exception e) {
			Assert.fail("Header is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Step Name on Military Verification page
	 */
	public MilitaryVerificationPage checkStepNameOnMilitaryVerificationPage() {
		try {
			stepNameOnMilitaryVerificationPage.isDisplayed();
			Reporter.log("Step Name is displayed on Millitary Verification page");
			String actualText = stepNameOnMilitaryVerificationPage.getText();
			Assert.assertEquals(actualText, "Submit information",
					" Step Name text on Military Verification page is mismatched");
			Reporter.log("Step Name Text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("Step Name is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Description Copy text on Military Verification page
	 */
	public MilitaryVerificationPage checkDescriptionOnMilitaryVerificationPage() {
		try {
			String descriptionCopy = "Please select current military affiliation and enter your information below";
			descriptionCopyOnMilitaryVerificationPage.isDisplayed();
			Reporter.log("Step Name is displayed on Millitary Verification page");
			String actualText = descriptionCopyOnMilitaryVerificationPage.getText();
			Assert.assertTrue(actualText.contains(descriptionCopy),
					"Description copy text on Military Verification page is mismatched");
			Reporter.log("Description copy text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("Description copy text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check FAQ link
	 */
	public MilitaryVerificationPage checkFAQLink() {
		try {
			faqLinkOnMilitaryVerificationPage.isDisplayed();
			Reporter.log("FAQ link is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("FAQ link is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check FAQ link redirection on Military Verification page
	 */
	public MilitaryVerificationPage checkFAQLinkRedirection(WebElement FAQLink) {
		try {
			String currentPageHandle = getDriver().getWindowHandle();
			FAQLink.click();
			waitForDataPassSpinnerInvisibility();
			checkPageIsReady();

			ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());

			String pageTitle = "Privacy Policy & Personal Information | T-Mobile";
			for (String eachHandle : tabs) {
				getDriver().switchTo().window(eachHandle);
				// Check Your Page Title
				if (getDriver().getTitle().equalsIgnoreCase(pageTitle)) {
					String currentURL = getDriver().getCurrentUrl();
					if (currentURL.contains("https://support.t-mobile.com/docs/DOC-37061#FAQs"))
						Reporter.log("FAQ link redirection is successful");
					else
						Assert.fail("FAQ link redirection on Military verification page is unsuccessful");
					getDriver().close();
					getDriver().switchTo().window(currentPageHandle);

				}
			}
		} catch (Exception e) {
			Assert.fail("FAQ link on Military verification page is not clicked");
		}
		return this;
	}

	// Checking all Radio Buttons, Labels and Sub labels
	/**
	 * Check Radio button for Veteran Or Retiree on Military Verification page
	 */
	public MilitaryVerificationPage checkRadioButtonForVeteranOrRetireeSelectedByDefault() {
		try {

			radioButtonForVeteranOrRetiree.isDisplayed();
			Reporter.log("Radio Button for Veteran Or Retiree is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Radio Button for Veteran Or Retiree is not displayed on Millitary Verification page");
		}

		try {
			boolean isRadioButtonChecked = false;
			isRadioButtonChecked = radioButtonForVeteranOrRetiree.isSelected();
			if (isRadioButtonChecked == true)
				Reporter.log("Radio Button for Veteran Or Retiree is checked/selected");
			else
				Assert.fail("Radio Button for Veteran Or Retiree is not checked/selected");
		} catch (Exception e) {
			Assert.fail("Radio Button for Veteran Or Retiree is not checked/selected");
		}

		return this;
	}

	/**
	 * Check Label for Veteran Or Retiree on Military Verification page
	 */
	public MilitaryVerificationPage checkLabelForVeteranOrRetiree() {
		try {
			labelOfVeteranOrRetiree.isDisplayed();
			Reporter.log("Label for Veteran Or Retiree is displayed on Millitary Verification page");
			String actualText = labelOfVeteranOrRetiree.getText();
			Assert.assertEquals(actualText, "Veteran or Retiree",
					" Label for Veteran Or Retiree text on Military Verification page is mismatched");
			Reporter.log("Label for Veteran Or Retiree text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("Label for Veteran Or Retiree text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check SubLabel for Veteran Or Retiree on Military Verification page
	 */
	public MilitaryVerificationPage checkSubLabelForVeteranOrRetiree() {
		try {
			subLabelOfVeteranOrRetiree.isDisplayed();
			Reporter.log("SubLabel for Veteran Or Retiree is displayed on Millitary Verification page");
			String actualText = subLabelOfVeteranOrRetiree.getText();
			Assert.assertEquals(actualText, "Those honorably discharged after serving in the U.S. Military.",
					" SubLabel for Veteran Or Retiree text on Military Verification page is mismatched");
			Reporter.log("SubLabel for Veteran Or Retiree text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("SubLabel for Veteran Or Retiree text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Radio button for Active Duty on Military Verification page
	 */
	public MilitaryVerificationPage checkRadioButtonForActiveDuty() {
		try {
			radioButtonForActiveDuty.isDisplayed();
			Reporter.log("Radio Button for Active Duty is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Radio Button for Active Duty is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Label for Active Duty on Military Verification page
	 */
	public MilitaryVerificationPage checkLabelForActiveDuty() {
		try {
			labelOfActiveDuty.isDisplayed();
			Reporter.log("Label for Active Duty is displayed on Millitary Verification page");
			String actualText = labelOfActiveDuty.getText();
			Assert.assertEquals(actualText, "Active Duty",
					" Label for Active Duty text on Military Verification page is mismatched");
			Reporter.log("Label for Active Duty text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("Label for Active Duty text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check SubLabel for Active duty on Military Verification page
	 */
	public MilitaryVerificationPage checkSubLabelForActiveDuty() {
		try {
			subLabelOfActiveDuty.isDisplayed();
			Reporter.log("SubLabel for Active duty is displayed on Millitary Verification page");
			String actualText = subLabelOfActiveDuty.getText();
			Assert.assertEquals(actualText, "Serving in the U.S. Military under Title 10 orders.",
					" SubLabel for Active duty text on Military Verification page is mismatched");
			Reporter.log("SubLabel for Active duty text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("SubLabel for Active duty text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Radio button for National Guard Or Reserve on Military Verification
	 * page
	 */
	public MilitaryVerificationPage checkRadioButtonForNationalGuardOrReserve() {
		try {
			radioButtonForNationalGuardOrReserve.isDisplayed();
			Reporter.log("Radio Button for National Guard Or Reserve is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Radio Button for National Guard Or Reserve is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Label for National Guard Or Reserve on Military Verification page
	 */
	public MilitaryVerificationPage checkLabelForNationalGuardOrReserve() {
		try {
			labelOfNationalGuardOrReserve.isDisplayed();
			Reporter.log("Label for National Guard Or Reserve is displayed on Millitary Verification page");
			String actualText = labelOfNationalGuardOrReserve.getText();
			Assert.assertEquals(actualText, "National Guard or Reserve",
					" Label for National Guard Or Reserve text on Military Verification page is mismatched");
			Reporter.log("Label for National Guard Or Reserve text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("Label for National Guard Or Reserve text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check SubLabel for National Guard Or Reserve on Military Verification
	 * page
	 */
	public MilitaryVerificationPage checkSubLabelForNationalGuradOrReserve() {
		try {
			subLabelOfNationalGuardOrReserve.isDisplayed();
			Reporter.log("SubLabel for National Guard Or Reserve is displayed on Millitary Verification page");
			String actualText = subLabelOfNationalGuardOrReserve.getText();
			Assert.assertEquals(actualText, "Currently serving in the U.S. Military under Title 32 orders.",
					" SubLabel for National Guard Or Reserve text on Military Verification page is mismatched");
			Reporter.log("SubLabel for National Guard Or Reserve text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("SubLabel for Active duty text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Radio button for Gold Star on Military Verification page
	 */
	public MilitaryVerificationPage checkRadioButtonForGoldStar() {
		try {
			radioButtonForGoldStar.isDisplayed();
			Reporter.log("Radio Button for Gold Star is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Radio Button for Gold Star is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Label for Gold Star on Military Verification page
	 */
	public MilitaryVerificationPage checkLabelForGoldStar() {
		try {
			labelOfGoldStar.isDisplayed();
			Reporter.log("Label for Gold Star is displayed on Millitary Verification page");
			String actualText = labelOfGoldStar.getText();
			Assert.assertEquals(actualText, "Gold Star",
					" Label for Gold Star text on Military Verification page is mismatched");
			Reporter.log("Label for Gold Star text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("Label for Gold Star text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check SubLabel for Gold Star on Military Verification page
	 */
	public MilitaryVerificationPage checkSubLabelForGoldStar() {
		try {
			subLabelOfGoldStar.isDisplayed();
			Reporter.log("SubLabel for Gold Star is displayed on Millitary Verification page");
			
			Assert.assertEquals(subLabelOfGoldStar.getText(), "A family member who has lost a loved one in service to the nation.",
					" SubLabel for Gold Star text on Military Verification page is mismatched");
			
			Reporter.log("SubLabel for Gold Star text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("SubLabel for Active duty text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Click on Veteran Or Retiree radio button
	 */
	public MilitaryVerificationPage clickOnVeteranOrRetireeRadioButton() {
		radioButtonForVeteranOrRetiree.click();
		verifyMilitaryVerificationPage();
		return this;
	}

	/**
	 * Click on Active Duty radio button
	 */
	public MilitaryVerificationPage clickOnActiveDutyRadioButton() {
		waitforSpinner();
		radioButtonForActiveDuty.click();
		return this;
	}

	/**
	 * Click on National Guard Or Reserve radio button
	 */
	public MilitaryVerificationPage clickOnNationalGuardOrReserveRadioButton() {
		radioButtonForNationalGuardOrReserve.click();
		return this;
	}

	/**
	 * Click on Gold Start radio button
	 */
	public MilitaryVerificationPage clickOnGoldStarRadioButton() {
		waitforSpinner();
		checkPageIsReady();
		radioButtonForGoldStar.click();
		return this;
	}

	/**
	 * Check Label All Fields Are Required on Military Verification page
	 */
	public MilitaryVerificationPage checkLabelAllFieldsAreRequired() {
		try {
			labelOfAllFieldsRequired.isDisplayed();
			Reporter.log("Label All fields are required is displayed on Millitary Verification page");
			String actualText = labelOfAllFieldsRequired.getText();
			Assert.assertEquals(actualText, "All fields are required.",
					" Label All fields are required text on Military Verification page is mismatched");
			Reporter.log("Label for All fields are required text on Millitary Verification page is matched");
		} catch (Exception e) {
			Assert.fail("Label for All fields are required text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Text field First Name of Military member on Military Verification
	 * page
	 */
	public MilitaryVerificationPage checkFieldFirstNameOfMilitaryMember() {
		try {
			Assert.assertTrue(textFieldFirstNameOfMilitaryMember.isDisplayed(),
					"Text field First Name of Military member is not displayed on Millitary Verification page");
			Reporter.log("Text field First Name of Military member is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Text field First Name of Military member is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check whether First name field is disabled or not
	 */
	public MilitaryVerificationPage checkWhetherFirstNameFieldIsEnabled() {
		try {
			Assert.assertTrue(fieldFirstNameOfMilitaryMember.getAttribute("disabled").contains("false"),
					"First name field is enabled");
			Reporter.log("First name field is enabled");

		} catch (Exception e) {
			Assert.fail("First name field is disabled");
		}
		return this;
	}

	/**
	 * Check whether First name field is disabled or not
	 */
	public MilitaryVerificationPage checkWhetherFirstNameFieldIsDisabled() {
		try {

			Assert.assertTrue(fieldFirstNameOfMilitaryMember.getAttribute("disabled").contains("true"),
					"First name field is disabled");
			Reporter.log("First name field is disabled");

		} catch (Exception e) {
			Assert.fail("First name field is enabled");
		}
		return this;
	}

	/**
	 * Enter First name
	 */
	public MilitaryVerificationPage enterValidFirstName(String firstName) {
		try {
			textFieldFirstNameOfMilitaryMember.clear();
			textFieldFirstNameOfMilitaryMember.sendKeys(firstName);
			Reporter.log("First name is entered");
		} catch (Exception e) {
			Assert.fail("First name is not entered");
		}
		return this;
	}

	/**
	 * Get First name
	 */
	public String getFirstName() {
		String firstName = null;
		try {
			firstName = textFieldFirstNameOfMilitaryMember.getText();
			Reporter.log("First name is read");
		} catch (Exception e) {
			Reporter.log("First name not able to read");
		}
		return firstName;
	}

	/**
	 * Enter Invalid First name
	 */
	public MilitaryVerificationPage enterInValidFirstName() {
		String actualFirstName = null;

		try {
			actualFirstName = textFieldFirstNameOfMilitaryMember.getText();
			textFieldFirstNameOfMilitaryMember.clear();
			textFieldFirstNameOfMilitaryMember.sendKeys(actualFirstName + "@#$#@$");
			Reporter.log("Wrong First name is entered");
		} catch (Exception e) {
			Assert.fail("First name is not entered");
		}
		return this;
	}

	/**
	 * Check Text field Last Name of Military member on Military Verification
	 * page
	 */
	public MilitaryVerificationPage checkFieldLastNameOfMilitaryMember() {
		try {
			textFieldLastNameOfMilitaryMember.isDisplayed();
			Reporter.log("Text field Last Name of Military member is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Text field Last Name of Military member is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check whether Last name field is disabled or not
	 */
	public MilitaryVerificationPage checkWhetherLastNameFieldIsEnabledOrNot() {
		try {
			boolean lastNameFieldStatus = textFieldLastNameOfMilitaryMember.isEnabled();
			if (lastNameFieldStatus == true)
				Reporter.log("Last name field is enabled");
			else
				Assert.fail("Last name field is disabled");
		} catch (Exception e) {
			Assert.fail("Last name field is disabled");
		}
		return this;
	}

	/**
	 * Check whether Last name field is disabled or not
	 */
	public MilitaryVerificationPage checkWhetherLastNameFieldIsDisabledOrNot() {
		try {
			boolean lastNameFieldStatus = textFieldLastNameOfMilitaryMember.isEnabled();
			if (lastNameFieldStatus == true)
				Assert.fail("Last name field is enabled");
			else
				Reporter.log("Last name field is disabled");
		} catch (Exception e) {
			Reporter.log("Last name field is disabled");
		}
		return this;
	}

	/**
	 * Enter First name
	 */
	public MilitaryVerificationPage enterValidLastName(String lastName) {
		try {
			textFieldLastNameOfMilitaryMember.clear();
			textFieldLastNameOfMilitaryMember.sendKeys(lastName);
			Reporter.log("Last name is entered");
		} catch (Exception e) {
			Assert.fail("Last name is not entered");
		}
		return this;
	}

	/**
	 * Enter Invalid First name
	 */
	public MilitaryVerificationPage enterInValidLastName() {
		String actualLastName = null;

		try {
			actualLastName = textFieldLastNameOfMilitaryMember.getText();
			textFieldLastNameOfMilitaryMember.clear();
			textFieldLastNameOfMilitaryMember.sendKeys(actualLastName + "@#$#@$");
			Reporter.log("Wrong Last name is entered");
		} catch (Exception e) {
			Assert.fail("Last name is not entered");
		}
		return this;
	}

	/**
	 * Get Last name
	 */
	public String getLastName() {
		String firstName = null;
		try {
			firstName = textFieldLastNameOfMilitaryMember.getText();
			Reporter.log("Last name is read");
		} catch (Exception e) {
			Reporter.log("Last name not able to read");
		}
		return firstName;
	}

	/**
	 * Combine First Name and Last name
	 */
	public String combinedFirstAndLastName() {
		String firstName = null;
		String lastName = null;
		String combinedName = null;
		try {
			firstName = getFirstName();
			lastName = getLastName();
			combinedName = firstName + " " + lastName;
			Reporter.log("Able to combine the first name and last name");
		} catch (Exception e) {
			Reporter.log("Not able to combine first name and last names");
		}
		return combinedName;
	}

	/**
	 * Check Text field Email Address of Military member on Military
	 * Verification page
	 */
	public MilitaryVerificationPage checkFieldEmailAddressOfMilitaryMember() {
		try {
			textFieldEmailAddress.isDisplayed();
			Reporter.log("Text field Email Address is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Text field Email Address is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * get current Email Address page
	 */
	public String getCurrentEmailAddressOfMilitaryMember() {
		String actualCurrentEmailAddress = null;
		try {
			textFieldEmailAddress.isDisplayed();
			Reporter.log("Text field Email Address is displayed on Millitary Verification page");
			actualCurrentEmailAddress = textFieldEmailAddress.getText();

		} catch (Exception e) {
			Assert.fail("Text field Email Address is not displayed on Millitary Verification page");
		}
		return actualCurrentEmailAddress;
	}

	/**
	 * get current Email Address page
	 */
	public String enterWrongEmailAddressInConfirmEmailAddressField() {
		WebElement emailaddress;
		String actualCurrentEmailAddress = null;
		String confirmEmailAddress[];
		try {
			emailaddress = getDriver().findElement(By.id("email"));
			actualCurrentEmailAddress = emailaddress.getAttribute("value");
			actualCurrentEmailAddress = getCurrentEmailAddressOfMilitaryMember();
			textFieldEmailAddress.isDisplayed();
			Reporter.log("Text field Email Address is displayed on Millitary Verification page");
			actualCurrentEmailAddress = textFieldEmailAddress.getText();
			confirmEmailAddress = actualCurrentEmailAddress.split(".");
			textFieldConfirmEmailAddress.sendKeys(confirmEmailAddress[0] + "org");
		} catch (Exception e) {
			Assert.fail("Text field Email Address is not displayed on Millitary Verification page");
		}
		return actualCurrentEmailAddress;
	}

	/**
	 * Check Text field Confirm Email Address of Military member on Military
	 * Verification page
	 */
	public MilitaryVerificationPage checkFieldConfirmEmailAddressOfMilitaryMember() {
		try {
			textFieldConfirmEmailAddress.isDisplayed();
			Reporter.log("Text field Confirm Email Address is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Text field Confirm Email Address is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Text field Confirm Email Address of Military member on Military
	 * Verification page
	 */
	public MilitaryVerificationPage enterMatchingEmailAddressInConfirmEmailAddresseField() {
		WebElement emailaddress;
		String actualCurrentEmailAddress = null;
		String confirmEmailAddress[];
		try {
			emailaddress = getDriver().findElement(By.id("email"));
			actualCurrentEmailAddress = emailaddress.getAttribute("value");
			textFieldConfirmEmailAddress.sendKeys(actualCurrentEmailAddress);
			Reporter.log("Email Address entered in Confirm Email Address field");
		} catch (Exception e) {
			Assert.fail("Email Address is not entered in Email Address / Confirm Email Address field");
		}
		return this;
	}

	/**
	 * get current Email Address page
	 */
	public String getConfirmEmailAddressOfMilitaryMember() {
		String actualConfirmEmailAddress = null;
		try {
			actualConfirmEmailAddress = textFieldConfirmEmailAddress.getText();
			Reporter.log("Able to read the Email address");
		} catch (Exception e) {
			Assert.fail("Text field Email Address is not displayed on Millitary Verification page");
		}
		return actualConfirmEmailAddress;
	}

	/**
	 * Check Text field Phone number on Military Verification page
	 */
	public MilitaryVerificationPage checkFieldPhoneNumberOnMilitaryMember() {
		try {
			textFieldPhoneNumber.isDisplayed();
			Reporter.log("Text field Phone Number is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Text field Phone Number is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Text field Phone number on Military Verification page
	 */
	public MilitaryVerificationPage checkWhetherPhoneNumberFieldIsDisabledOrNot() {
		try {
			boolean firstNameFieldStatus = textFieldPhoneNumber.isEnabled();
			if (firstNameFieldStatus == true)
				Assert.fail("Phone Number field is enabled");
			else {
				Reporter.log("Phone number field is enabled");
			}
		} catch (Exception e) {
			Reporter.log("Phone number field is disabled");
		}
		return this;
	}

	/**
	 * Check Submit Information CTA on Military Verification page
	 */
	public MilitaryVerificationPage checkSubmitInformationCTA() {
		try {
			submitInformationCTA.isDisplayed();
			Reporter.log("CTA Submit Information Phone Number is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("CTA Submit Information is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Legal Text on Military Verification page
	 */
	public MilitaryVerificationPage checkLegalTextForSheeeID() {
		try {
			legalTextForSheerID.isDisplayed();
			Reporter.log("Legal Text for Sheer ID is displayed on Millitary Verification page");
			String expectedSheerIDText = "Verification services powered by SheerID.";
			String actualSheerIDText = legalTextForSheerID.getText();
			Assert.assertEquals(expectedSheerIDText, actualSheerIDText, "Sheer ID text mismatch");
		} catch (Exception e) {
			Assert.fail("Legal Text for Sheer ID is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Branch of Service dropdown
	 */
	public MilitaryVerificationPage checkBranchOfServiceDropdown() {
		try {
			branchOfServiceDropDown.isDisplayed();
			Reporter.log("Branch of Service Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Branch of Service Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check options of Branch of Service dropdown
	 */
	public MilitaryVerificationPage optionFromBranchOfServiceDropdown() {

		branchOfServiceDropDown.click();

		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfBranchOfServiceDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
		}
		labelBranchOfService.click();
		return this;
	}

	/**
	 * Select a particular options from Branch of Service dropdown
	 */
	public MilitaryVerificationPage selectOptionFromBranchOfServiceDropdown(String option) {

		branchOfServiceDropDown.click();

		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfBranchOfServiceDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			
			if (innerElement.equals(option)) {
				element.click();
			}
		}
		labelBranchOfService.click();
		return this;
	}

	/**
	 * Get selected option from Branch of Service dropdown
	 */
	public String getSelectedOptionFromBranchOfServiceDropdown() {
		String selectedBranchOfServiceOption = null;
		try {
			selectedBranchOfServiceOption = branchOfServiceDropDown.getText();
			Reporter.log("Branch of service is read");
		} catch (Exception e) {
			Reporter.log("Not able to read Branch of service value");

		}
		return selectedBranchOfServiceOption;
	}

	/**
	 * Check all options from Branch of Service dropdown
	 */
	public MilitaryVerificationPage checkAllOptionsFromBranchOfServiceDropdown() {

		branchOfServiceDropDown.click();
		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfBranchOfServiceDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
		}
		return this;
	}

	/**
	 * Check all options from month dropdown
	 */
	public MilitaryVerificationPage checkAllOptionsFromMonthDropdown() {

		monthDropDown.click();
		List<WebElement> monthDropdown = getDriver().findElements(By.xpath(optionsOfMonthDropdown));
		String expectedMonthDropdownValues[] = { "January", "February", "March", "April", "May", "June", "July",
				"August", "September", "October", "November", "December" };

		int count = monthDropdown.size();
		String actualMonthDropDown[] = new String[count];

		for (int i = 0; i < monthDropdown.size(); i++) {
			WebElement element = monthDropdown.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			actualMonthDropDown[i] = innerElement;
		}

		// Compare actual and expected values of Month dropdown
		for (int i = 0; i < monthDropdown.size(); i++) {
			if (actualMonthDropDown[i].equals(expectedMonthDropdownValues[i]))
				Reporter.log("Month is matched and month is  = " + actualMonthDropDown[i]);
		}
		return this;
	}

	/**
	 * Check all options from day dropdown
	 */
	public MilitaryVerificationPage checkAllOptionsFromDayDropdown() {

		dayDropDown.click();
		List<WebElement> dayDropdown = getDriver().findElements(By.xpath(optionsOfDayDropdown));
		String expectedDayDropdownValues[] = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
				"13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
				"30", "31" };

		int count = dayDropdown.size();
		String actualDayDropDown[] = new String[count];

		for (int i = 0; i < dayDropdown.size(); i++) {
			WebElement element = dayDropdown.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			actualDayDropDown[i] = innerElement;
		}

		// Compare actual and expected values of Month dropdown
		for (int i = 0; i < dayDropdown.size(); i++) {
			if (actualDayDropDown[i].equals(expectedDayDropdownValues[i]))
				Reporter.log("Month is matched and month is  = " + actualDayDropDown[i]);
		}
		return this;
	}

	/**
	 * Check all options from month dropdown
	 */
	public MilitaryVerificationPage checkAllOptionsFromYearDropdown() {

		dayDropDown.click();

		List<WebElement> yearDropdown = getDriver().findElements(By.xpath(optionsOfDayDropdown));
		// String expectedYearDropdownValues[] =
		// {"1901","1902","1903","1904","1905","1906","1907","1908","1909","1910","1911","1912","1913","1914","1915","1916","1917","1918","1919","1920","1921","1922","1923",1924","1925","1926","1927","1928","1929","1930","1931","1932","1933","1934","1935","1936","1937","1938","1939","1940","1941","1942","1943","1944","1945","1946","1947","1948","1949","1950","1951","1952","1953","1954","1955","1956","1957","1958","1959","1960","1961","1962","1963","1964","1965","1966","1967","1968","1969","1970","1971","1972","1973","1974","1975","1976","1977","1978","1979","1980","1981","1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018"};

		String expectedYearDropdownValues[] = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
				"13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
				"30", "31" };

		int count = yearDropdown.size();
		String actualYearDropDown[] = new String[count];

		for (int i = 0; i < yearDropdown.size(); i++) {
			WebElement element = yearDropdown.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			actualYearDropDown[i] = innerElement;
			
		}

		// Compare actual and expected values of year dropdown
		for (int i = 0; i < yearDropdown.size(); i++) {
			if (actualYearDropDown[i].equals(expectedYearDropdownValues[i]))
				Reporter.log("Month is matched and month is  = " + actualYearDropDown[i]);
		}
		return this;
	}

	/**
	 * Check Branch Of Service dropdown is displayed or not. Expected is to
	 * check Dropdown should not be displayed
	 */
	public MilitaryVerificationPage verifyBranchOfServiceDropdownNotDisplayed() {
		try {
			if (!(branchOfServiceDropDown.isDisplayed()))
				Reporter.log("Branch of service Dropdown is not displayed on Millitary Verification page");
			else
				Assert.fail("Branch of service Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Reporter.log("Branch of service Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Discharge Date dropdown
	 */
	public MilitaryVerificationPage checkDischargeDateServiceDropdown() {
		try {
			dischargeDateDropDown.isDisplayed();
			Reporter.log("Discharge Date Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Discharge Date Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Discharge Date dropdown is displayed or not. Expected is to check
	 * Dropdown should not be displayed
	 */
	public MilitaryVerificationPage verifyDischargeDateServiceDropdownNotDisplayed() {
		try {
			if (!(dischargeDateDropDown.isDisplayed()))
				Reporter.log("Discharge Date Dropdown is not displayed on Millitary Verification page");
			else
				Assert.fail("Discharge Date Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Reporter.log("Discharge Date Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check options of Discharge Date dropdown
	 */
	public MilitaryVerificationPage optionsFromDischargeDateDropdown() {

		dischargeDateDropDown.click();

		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfDischargeDateDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
		}
		labelDischargeDate.click();
		return this;
	}

	/**
	 * Select a particular options from Discharge Of Date dropdown
	 */
	public MilitaryVerificationPage selectOptionFromDischargeDate(String option) {
		dischargeDateDropDown.click();
		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfDischargeDateDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			if (innerElement.equals(option)) {
				element.click();
			}
		}
		// labelDischargeDate.click();
		return this;
	}

	/**
	 * Get Discharge date from Discharge date dropdown
	 */
	public String getDischargeDateFromDischargeDateDropdown() {
		String dischargeDate = null;
		try {
			dischargeDate = dischargeDateDropDown.getText();
			Reporter.log("Branch of service is read");
		} catch (Exception e) {
			Reporter.log("Not able to read Branch of service value");

		}
		return dischargeDate;
	}

	/**
	 * Check month dropdown
	 */
	public MilitaryVerificationPage checkMonthDropdownFromDateOfBirthSection() {
		try {
			monthDropDown.isDisplayed();
			Reporter.log("Month Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Month Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check options of Month dropdown
	 */
	public MilitaryVerificationPage optionsOfMonthDropdownFromDateOfBirthSection() {

		monthDropDown.click();

		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfMonthDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
		}
		labelDateOfBirth.click();
		return this;
	}

	/**
	 * Select a particular Month from Month dropdown
	 */
	public MilitaryVerificationPage selectOptionFromMonthDropdownFromDateOfBirthSection(String option) {

		monthDropDown.click();

		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfMonthDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			if (innerElement.equals(option)) {
				element.click();
			}
		}
		labelDateOfBirth.click();
		return this;
	}

	/**
	 * Get Month value from Month Dropdown
	 */
	public String getMonthValueFromMonthDropdown() {
		String selectedMonthValue = null;
		String selectedMonthIntoNumber = null;
		try {
			selectedMonthValue = monthDropDown.getText();
			Reporter.log("Selected Month is read");
		} catch (Exception e) {
			Reporter.log("Not able to read Selected Month value");
		}
		if (selectedMonthValue.equals("January"))
			selectedMonthIntoNumber = "01";
		else if (selectedMonthValue.equals("February"))
			selectedMonthIntoNumber = "02";
		else if (selectedMonthValue.equals("March"))
			selectedMonthIntoNumber = "03";
		else if (selectedMonthValue.equals("April"))
			selectedMonthIntoNumber = "04";
		else if (selectedMonthValue.equals("May"))
			selectedMonthIntoNumber = "05";
		else if (selectedMonthValue.equals("June"))
			selectedMonthIntoNumber = "06";
		else if (selectedMonthValue.equals("July"))
			selectedMonthIntoNumber = "07";
		else if (selectedMonthValue.equals("August"))
			selectedMonthIntoNumber = "08";
		else if (selectedMonthValue.equals("September"))
			selectedMonthIntoNumber = "09";
		else if (selectedMonthValue.equals("October"))
			selectedMonthIntoNumber = "10";
		else if (selectedMonthValue.equals("November"))
			selectedMonthIntoNumber = "11";
		else if (selectedMonthValue.equals("December"))
			selectedMonthIntoNumber = "12";
		return selectedMonthIntoNumber;
	}

	/**
	 * Check Day dropdown
	 */
	public MilitaryVerificationPage checkDayDropdownFromDateOfBirthSection() {
		try {
			dayDropDown.isDisplayed();
			Reporter.log("Day Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Day Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check options of Day dropdown
	 */
	public MilitaryVerificationPage optionsOfDayDropdownFromDateOfBirthSection() {
		dayDropDown.click();
		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfDayDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
		}
		labelDateOfBirth.click();
		return this;
	}

	/**
	 * Select a particular Month from Month dropdown
	 */
	public MilitaryVerificationPage selectOptionFromDayDropdownFromDateOfBirthSection(String option) {

		dayDropDown.click();

		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfDayDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			if (innerElement.equals(option)) {
				element.click();
			}
		}
		labelDateOfBirth.click();
		return this;
	}

	/**
	 * Get Day from Day dropdown
	 */
	public String getDayFromDayDropdown() {
		String dayDate = null;
		try {
			dayDate = dayDropDown.getText();
			if (dayDate.equals("1"))
				dayDate = "01";
			else if (dayDate.equals("2"))
				dayDate = "02";
			else if (dayDate.equals("3"))
				dayDate = "03";
			else if (dayDate.equals("4"))
				dayDate = "04";
			else if (dayDate.equals("5"))
				dayDate = "05";
			else if (dayDate.equals("6"))
				dayDate = "06";
			else if (dayDate.equals("7"))
				dayDate = "07";
			else if (dayDate.equals("8"))
				dayDate = "08";
			else if (dayDate.equals("9"))
				dayDate = "09";
			Reporter.log("Able to read Day from Day dropdown");
		} catch (Exception e) {
			Reporter.log("Not able to read Day from Day dropdown.");

		}
		return dayDate;
	}

	/**
	 * Check Year dropdown
	 */
	public MilitaryVerificationPage checkYearDropdownFromDateOfBirthSection() {
		try {
			yearDropDown.isDisplayed();
			Reporter.log("Year Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Year Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check options of Year dropdown
	 */
	public MilitaryVerificationPage optionsOfYearDropdownFromDateOfBirthSection() {
		yearDropDown.click();
		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfYearDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
		}
		labelDateOfBirth.click();
		return this;
	}

	/**
	 * Select a particular Year from Year dropdown
	 */
	public MilitaryVerificationPage selectOptionFromYearDropdownFromDateOfBirthSection(String option) {

		yearDropDown.click();

		List<WebElement> branchOfService = getDriver().findElements(By.xpath(optionsOfYearDropdown));

		for (int i = 0; i < branchOfService.size(); i++) {
			WebElement element = branchOfService.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			if (innerElement.equals(option)) {
				element.click();
			}
		}
		labelDateOfBirth.click();
		return this;
	}

	/**
	 * Get year from Year dropdown
	 */
	public String getYearFromYearDropdown() {
		String YearDate = null;
		try {
			YearDate = yearDropDown.getText();
			Reporter.log("Able to read Day from Year dropdown");
		} catch (Exception e) {
			Reporter.log("Not able to read Day from Year dropdown.");

		}
		return YearDate;
	}

	/**
	 * Format date to MM/DD/YYYY
	 */
	public String convertDateIntoMMDDYYYFormat() {
		String monthValueOnVerificationPage = null;
		String dayValueOnVerificationPage = null;
		String yearValueOnVerificationPage = null;
		String dateInFormatedValue = null;
		try {
			monthValueOnVerificationPage = getMonthValueFromMonthDropdown();
			dayValueOnVerificationPage = getDayFromDayDropdown();
			yearValueOnVerificationPage = getYearFromYearDropdown();
			dateInFormatedValue = monthValueOnVerificationPage + "/" + dayValueOnVerificationPage + "/"
					+ yearValueOnVerificationPage;
		} catch (Exception e) {
		}
		return dateInFormatedValue;
	}

	/**
	 * Check Relationship to military member dropdown
	 */
	public MilitaryVerificationPage checkRelationshipToMilitaryMemberDropdown() {
		try {
			Assert.assertTrue(relationshipDropDown.isDisplayed(),"Relationship to Military Member Dropdown is not displayed on Millitary Verification page");
			Reporter.log("Relationship to Military Member Dropdown is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Relationship to Military Member Dropdown is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check options of Relationship to military member dropdown
	 */
	public MilitaryVerificationPage optionsOfRelatioshipDropdown() {
		relationshipDropDown.click();

		for (int i = 0; i < optionsOfRelationshipDropdown.size(); i++) {
			WebElement element = optionsOfRelationshipDropdown.get(i);
			String innerElement = element.getAttribute("innerHTML");
		}
		labelRelationshipToMilitaryMember.click();
		return this;
	}

	/**
	 * Select a particular option from Relationship to military member dropdown
	 */
	public MilitaryVerificationPage selectOptionFromRelatioshipDropdown(String option) {

		relationshipDropDown.click();

		for (int i = 0; i < optionsOfRelationshipDropdown.size(); i++) {
			WebElement element = optionsOfRelationshipDropdown.get(i);
			String innerElement = element.getAttribute("innerHTML");
			innerElement = innerElement.trim();
			if (innerElement.equals(option)) {
				element.click();
				Reporter.log("Relationship selected option is " + innerElement);
			}
		}
		labelRelationshipToMilitaryMember.click();
		return this;
	}

	/**
	 * Get selected option from Relationship dropdown
	 */
	public String getSelectedOptionFromRelationshipDropdown() {
		String selectedOptionFromRelationshipDropdown = null;
		try {
			selectedOptionFromRelationshipDropdown = relationshipDropDown.getText();
			Reporter.log("Relationship status is read");
		} catch (Exception e) {
			Reporter.log("Not able to read Relationship status value");

		}
		return selectedOptionFromRelationshipDropdown;
	}

	/**
	 * Check Submit CTA is disabled or not on Military Verification page
	 */
	public MilitaryVerificationPage checkSubmitCTAIsDisabledOrNot() {
		try {
			if (submitInformationCTA.isEnabled())
				Assert.fail("Submit CTA is enabled");
			else {
				Reporter.log("Submit CTA is disabled");
			}
		} catch (Exception e) {
			Reporter.log("Submit CTA is disabled");
		}
		return this;
	}

	/**
	 * Check Submit CTA is Enabled or not on Military Verification page
	 */
	public MilitaryVerificationPage checkSubmitCTAIsEnabledOrNot() {
		try {
			boolean submitCTAStatus = submitInformationCTA.isEnabled();
			if (submitCTAStatus == true)
				Reporter.log("Submit CTA is enabled");
			else {
				Assert.fail("Submit CTA is disabled");
			}
		} catch (Exception e) {
			Reporter.log("Submit CTA is disabled");
		}
		return this;
	}

	/**
	 * Click on Submit CTA
	 */
	public MilitaryVerificationPage clickOnSubmitCTA() {
		try {
			submitInformationCTA.click();
			Reporter.log("Submit CTA is clicked");
		} catch (Exception e) {
			Reporter.log("Submit CTA is not clicked");
		}
		return this;
	}

	/**
	 * Check Error Message
	 */
	public MilitaryVerificationPage checkErrorMessageForFieldValiadation(String expectedErrorMessage) {
		try {
			waitFor(ExpectedConditions.visibilityOf(errorMessage));
			errorMessage.isDisplayed();
			Reporter.log("Error Message is displayed");
			String actualErrorMessage = errorMessage.getText();
			Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message mismatch");
		} catch (Exception e) {
			Assert.fail("Error Message is not displayed");
		}
		return this;
	}

	/**
	 * Check Error Icon
	 */
	public MilitaryVerificationPage checkErrorIconForAllErrorMessagesForFieldValiadation() {
		try {
			errorMessageIcon.isDisplayed();
			Reporter.log("Error Message Icon is displayed");
		} catch (Exception e) {
			Assert.fail("Error Message Icon is not displayed");
		}
		return this;
	}

	/**
	 * Check Legal Text
	 */
	public MilitaryVerificationPage verifyLegalTextOnVerificationPage() {
		try {
			verifyElementBytext(legalText,
					"Please omit or black out any sensitive information such as Social Security number, or Military ID#.");
			Reporter.log("Legal Text is displayed on Millitary Verification page");
		} catch (Exception e) {
			Assert.fail("Legal Text is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check Icon on Contact PAH screen
	 */
	public MilitaryVerificationPage checkIconOnContactPAHMessageScreen() {
		try {
			iconOnContactPAHScreen.isDisplayed();
			Reporter.log("Icon is displayed on Contact PAH user message screen.");
		} catch (Exception e) {
			Reporter.log("Icon is not displayed on Contact PAH user message screen.");
		}
		return this;
	}

	/**
	 * Check Header Title on Contact PAH screen
	 */
	public MilitaryVerificationPage checkHeaderTitleTextOnContactPAHMessageScreen() {
		try {
			headerTitleOnContactPAHScreen.isDisplayed();
			Reporter.log("Header title is displayed on Contact PAH user message screen.");
		} catch (Exception e) {
			Reporter.log("Header Title is not displayed on Contact PAH user message screen.");
		}
		return this;
	}

	/**
	 * Check Error message on Contact PAH screen
	 */
	public MilitaryVerificationPage checkErrorMessageOnContactPAHMessageScreen() {
		try {
			String errorMessage = "You do not have permission to perform this verification. Please contact an authorized user for assistance.";
			messageOnContactPAHScreen.isDisplayed();
			Reporter.log("Error message is displayed on Contact PAH user screenrification page");
			String actualText = messageOnContactPAHScreen.getText();
			Assert.assertEquals(actualText, errorMessage, " Error message to Contact PAH is mismatched");
			Reporter.log("Error message to Contact PAH is matched");
		} catch (Exception e) {
			Assert.fail("Error message to contact PAH is not displayed on Millitary Verification page");
		}
		return this;
	}

	/**
	 * Check OK CTA on Contact PAH screen
	 */
	public MilitaryVerificationPage checkOKCTAOnContactPAHMessageScreen() {
		try {
			okCTAOnContactPAHScreen.isDisplayed();
			Reporter.log("OK CTA is displayed on Contact PAH user verification page");
		} catch (Exception e) {
			Assert.fail("OK CTA is not displayed on contact PAH verification page");
		}
		return this;
	}

	/**
	 * Click OK CTA on Contact PAH screen
	 */
	public MilitaryVerificationPage clickOKCTAOnContactPAHMessageScreen() {
		try {
			okCTAOnContactPAHScreen.click();
			Reporter.log("OK CTA is clicked on Contact PAH user verification page");
		} catch (Exception e) {
			Assert.fail("OK CTA is not clicked on contact PAH verification page");
		}
		return this;
	}

	/**
	 * Check redirection to Profile page.
	 */
	public MilitaryVerificationPage checkRedirectionToProfilePage() {
		ProfilePage profilepage = new ProfilePage(getDriver());
		profilepage.verifyProfilePage();
		return this;
	}

	/**
	 * Check Icon on Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkIconOnUploadReceivedAndUnderReviewPage() {
		try {
			iconOnUploadReceivedAndUnderReview.isDisplayed();
			Reporter.log("Icon is displayed on Upload Received and Under Review screen.");
		} catch (Exception e) {
			Reporter.log("Icon is not displayed on Upload Received and Under Review screen.");
		}
		return this;
	}

	/**
	 * Check Subtitle for Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkSubtitleForUploadReceivedAndUnderReview() {
		try {
			// String messageNote = "Upload received and under review";
			subTitleForUploadReceivedAndUnderReview.isDisplayed();
			Reporter.log("Subtitle is displayed on Upload received and under review page");
			String actualText = subTitleForUploadReceivedAndUnderReview.getText();
			Assert.assertEquals(actualText, "Upload received and under review",
					" Subtitle message on Upload received and under review is mismatched");
			Reporter.log("Subtitle message on Upload received and under review is matched");
		} catch (Exception e) {
			Assert.fail("Subtitle is not displayed on Upload received and under review page");
		}
		return this;
	}

	/**
	 * Check Subtitle for Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkMessageBelowSubtitleOnUploadReceivedAndUnderReview() {
		try {
			String messageNote = "We received your documentation. You will receive an email from verify@sheerid.com within 24 hours regarding the status of your verification request.";
			messageOnUploadReceivedAndUnderReview.isDisplayed();
			Reporter.log("Message below Subtitle is displayed on Upload received and under review page");
			String actualText = messageOnUploadReceivedAndUnderReview.getText();
			Assert.assertEquals(actualText, messageNote,
					" Message below Subtitle on Upload received and under review is mismatched");
			Reporter.log("Message below Subtitle on Upload received and under review is matched");
		} catch (Exception e) {
			Assert.fail("Message below Subtitle is not displayed on Upload received and under review page");
		}
		return this;
	}

	/**
	 * Check Note for Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkNoteForUploadReceivedAndUnderReview() {
		try {
			String messageNote = "Note: If you don't receive an email, check your junk/spam folder. For additional help, please visit our Frequently Asked Questions or dial 611 from your T-Mobile phone.";
			messageNoteOnUploadReceivedAndUnderReview.isDisplayed();
			Reporter.log("Message note is displayed on Upload received and under review page");
			String actualText = messageNoteOnUploadReceivedAndUnderReview.getText();
			Assert.assertEquals(actualText, messageNote,
					"Message note message on Upload received and under review is mismatched");
			Reporter.log("Message note message on Upload received and under review is matched");
		} catch (Exception e) {
			Assert.fail("Message note message is not displayed on Upload received and under review page");
		}
		return this;
	}

	/**
	 * Check Name field on Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkNameOnUploadReceivedAndUnderReview() {
		try {
			Assert.assertTrue(verifyElementBytext(otherHeaders, "Name"), "Name field is not displayed");
			Reporter.log("Name Field is displayed on Upload received and under review page");
		} catch (Exception e) {
			Assert.fail("Name field is not displayed on Upload received and under review page");
		}
		return this;
	}

	/**
	 * Check Email field on Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkEmailFieldOnUploadReceivedAndUnderReview() {
		try {
			Assert.assertTrue(verifyElementBytext(successMessageForMilitaryPlanUserOnVerifiedStatusPage, "yopmail.com"),
					"Email field is not displayed");
			Reporter.log("Email Field is displayed on Upload received and under review page");
		} catch (Exception e) {
			Assert.fail("Email field is not displayed on Upload received and under review page");
		}
		return this;
	}

	/**
	 * Check Done CTA on Upload Received and under review
	 */
	public MilitaryVerificationPage checkDoneCTAOnContactPAHMessageScreen() {
		try {
			doneCTAOnUploadReceivedAndUnderReview.isDisplayed();
			Reporter.log("Done CTA is displayed on Upload received and under Review page");
		} catch (Exception e) {
			Assert.fail("Done CTA is not displayed on Upload received and under Review page");
		}
		return this;
	}

	/**
	 * Click on Done CTA on upload received and under review
	 */
	public MilitaryVerificationPage clickOnDoneCTAOnUploadReceivedAndUnderProcess() {
		try {
			doneCTAOnUploadReceivedAndUnderReview.click();
			Reporter.log("Done CTA is clicked on Upload received and under review page");
		} catch (Exception e) {
			Assert.fail("Done CTA is not clicked Upload received and under review page");
		}
		return this;
	}

	/**
	 * Check Icon on Verified Status Page
	 */
	public MilitaryVerificationPage checkIconOnVerifiedStatusPage() {
		try {
			Assert.assertTrue(iconOnVerifiedStatusPage.isDisplayed(), "Icon is not displayed on Verified status page");
			Reporter.log("Icon is displayed on Verified status page.");
		} catch (Exception e) {
			Reporter.log("Icon is not displayed on Verified status page.");
		}
		return this;
	}

	/**
	 * Check Subtitle on Verified Status page
	 */
	public MilitaryVerificationPage checkSubtitleOnVerifiedStatusPage() {
		try {
			Assert.assertTrue(verifiedSubTitleOnVerifiedStatusPage.isDisplayed(),
					"Subtitle is not displayed on Verified status page");
			Reporter.log("Subtitle is displayed on Upload received and under review page");

			Assert.assertTrue(verifyElementByText(verifiedSubTitleOnVerifiedStatusPage, "Verified"),
					"Subtitle is not displayed on Verified status page");
			Reporter.log("Subtitle on Verified status is matched");

		} catch (Exception e) {
			Assert.fail("Subtitle is not displayed on Verified status page");
		}
		return this;
	}

	/**
	 * Check Subtitle for Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkSuccessMessageForMilitaryPlanUserOnVerifiedStatusPage() {
		try {
			String messageNote = "Congratulations, your military affiliation has been verified.";
			Assert.assertTrue(verifyElementBytext(successMessageForMilitaryPlanUserOnVerifiedStatusPage, messageNote));
			Reporter.log("Success Message is matched on Verified Status page");

		} catch (Exception e) {
			Assert.fail("Success Message is not displayed on Verified Status page");
		}
		return this;
	}

	/**
	 * Check Subtitle for Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkSuccessMessageOnVerifiedStatusPageWhenUserNotOnMilitaryPlan() {
		try {
			String messageNote = "Congratulations, your military affiliation has been verified. You can now take benefit of the T-Mobile ONE Military plan.";

			Assert.assertTrue(verifyElementBytext(successMessageForMilitaryPlanUserOnVerifiedStatusPage, messageNote),
					"Success Message is not displayed on Verified Status page");
			Reporter.log("Success Message is matched on Verified Status page");

		} catch (Exception e) {
			Assert.fail("Success Message is not displayed on Verified Status page");
		}
		return this;
	}

	/**
	 * Check Thank you for your service message.
	 */
	public MilitaryVerificationPage checkThankYouForYourServiceMessageOnVerifiedStatusPage() {
		try {
			String messageNote = "Thank you for your service.";

			String actualText = thankYouMessageOnVerifiedStatusPage.getText();
			Assert.assertEquals(actualText, messageNote, " Thank you Message is mismatched on Verified Status page");
			Reporter.log("Thank you Message is matched on Verified Status page");
		} catch (Exception e) {
			Assert.fail("Thank you Message is not displayed on Verified Status page");
		}
		return this;
	}

	/**
	 * Check Name field on Verified status page.
	 */
	public MilitaryVerificationPage checkNameOnVerifiedStatusPage() {
		try {
			nameFieldOnVerifiedStatusPage.isDisplayed();
			Reporter.log("Name Field is displayed on Veerified Status page.");
		} catch (Exception e) {
			Assert.fail("Name Field is not displayed on Verified Status page.");
		}
		return this;
	}

	/**
	 * Check Name field on Verified status page.
	 */
	public MilitaryVerificationPage verifyFirstNameOnVerificationPageAndVerifiedPage(String firstName) {
		String firstNameOnVerifiedStatusPage = null;
		try {
			firstNameOnVerifiedStatusPage = nameFieldOnVerifiedStatusPage.getText();
			if (firstNameOnVerifiedStatusPage.contains(firstName))
				Reporter.log("Name on Verification page and verified status page matched.");
		} catch (Exception e) {
			Assert.fail("Name on Verification page and verified status page mismatched.");
		}
		return this;
	}

	/**
	 * Check Last name field on Verified status page.
	 */
	public MilitaryVerificationPage verifyLastNameOnVerificationPageAndVerifiedPage(String lastName) {
		String lastNameOnVerifiedStatusPage = null;
		try {
			lastNameOnVerifiedStatusPage = nameFieldOnVerifiedStatusPage.getText();
			if (lastNameOnVerifiedStatusPage.contains(lastName))
				Reporter.log("Last Name on Verification page and verified status page matched.");
		} catch (Exception e) {
			Assert.fail("Last Name on Verification page and verified status page mismatched.");
		}
		return this;
	}

	/**
	 * Check Email field on Verified status page.
	 */
	public MilitaryVerificationPage checkEmailFieldOnVerifiedStatusPage() {
		try {
			emailFieldOnVerifiedStatusPage.isDisplayed();
			Reporter.log("Email Field is displayed on Veerified Status page.");
		} catch (Exception e) {
			Assert.fail("Email Field is not displayed on Veerified Status page.");
		}
		return this;
	}

	/**
	 * Check Email on Verification page and Verified status page
	 */
	public MilitaryVerificationPage verifyEmailAddressOnMilitaryVerificationPageAndVerifiedStatusPage(
			String emailAddressOnVerifyPage) {
		try {
			String emailAddressOnVerifiedStatusPage = null;
			emailAddressOnVerifiedStatusPage = emailFieldOnVerifiedStatusPage.getText();
			if (emailAddressOnVerifiedStatusPage.equals(emailAddressOnVerifyPage))
				Reporter.log("Email on Verify page and Verified status page is matched.");
		} catch (Exception e) {
			Assert.fail("Email on Verify page and Verified status page is mismatched.");
		}
		return this;
	}

	/**
	 * Check Email field on Verified status page.
	 */
	public MilitaryVerificationPage checkApprovalDateFieldOnVerifiedStatusPage() {
		try {
			Assert.assertTrue(verifyElementBytext(otherHeaders, "Approval date"),
					"Approval date Field is not displayed");
			Reporter.log("Approval date Field is displayed on Veerified Status page.");
		} catch (Exception e) {
			Assert.fail("Approval date Field is not displayed on Veerified Status page.");
		}
		return this;
	}

	/**
	 * Check Email field on Verified status page.
	 */
	public MilitaryVerificationPage verifyApprovalDateOnVerifiedStatusPage() {
		String expectedDate = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			expectedDate = dateFormat.format(date);

			verifyElementBytext(successMessageForMilitaryPlanUserOnVerifiedStatusPage, expectedDate);
			Reporter.log("Approval date is matched on Verified Status page.");
		} catch (Exception e) {
			Assert.fail("Approval date Field is not displayed on Veerified Status page.");
		}
		return this;
	}

	/**
	 * Check Icon on Too Many Failed Attempts Page
	 */
	public MilitaryVerificationPage checkIconOnTooManyFailedAttemptsPage() {
		try {
			iconOnTooManyFailedAttemptPage.isDisplayed();
			Reporter.log("Icon is displayed on Too Many failed attempts page.");
		} catch (Exception e) {
			Reporter.log("Icon is not displayed on Too Many failed attempts page.");
		}
		return this;
	}

	/**
	 * Check Header on Too many failed attempts page
	 */
	public MilitaryVerificationPage checkHeaderTextOnTooManyFailedAttemptsPage() {
		try {
			headerOnTooManyFailedAttemptPage.isDisplayed();
			Reporter.log("Header is displayed on Millitary Verification page");
			String actualText = headerOnTooManyFailedAttemptPage.getText();
			Assert.assertEquals(actualText, "Military verification",
					" Header text on Too Many Failed attempts page is mismatched");
			Reporter.log("Header on Too Many Failed attempts page is matched");
		} catch (Exception e) {
			Assert.fail("Header is not displayed on Too Many Failed attempts page");
		}
		return this;
	}

	/**
	 * Check subtitle on Too many failed attempts page
	 */
	public MilitaryVerificationPage checkSubtitleOnTooManyFailedAttemptsPage() {
		try {
			subTitleOnTooManyFailedAttemptPage.isDisplayed();
			Reporter.log("Subtitle is displayed on Millitary Verification page");
			String actualText = subTitleOnTooManyFailedAttemptPage.getText();
			Assert.assertEquals(actualText, "Too many failed attempts",
					"Subtitle text on Too Many Failed attempts page is mismatched");
			Reporter.log("Subtitle on Too Many Failed attempts page is matched");
		} catch (Exception e) {
			Assert.fail("Subtitle is not displayed on Too Many Failed attempts page");
		}
		return this;
	}

	/**
	 * Check Subtitle for Upload Received and Under Review
	 */
	public MilitaryVerificationPage checkMessageOnTooManyFailedAttemptsPage() {
		try {
			String messageNote = "You've reached the maximum number of attempts to upload your documentation. Please restart verification or contact Customer Care for further assistance.";
			messageOnTooManyFailedAttemptPage.isDisplayed();
			Reporter.log("Message is displayed on Too many failed attempts page");
			String actualText = messageOnTooManyFailedAttemptPage.getText();
			Assert.assertEquals(actualText, messageNote, "Message is mismatched on Too many failed attempts page");
			Reporter.log("Message is matched on Too many failed attempts page");
		} catch (Exception e) {
			Assert.fail("Message is not displayed on Too many failed attempts page");
		}
		return this;
	}

	/**
	 * Check Restart Verification CTA Too Many Failed Attempts page
	 */
	public MilitaryVerificationPage checkRestartVerificationCTAOnTooManyFailedAttemptsPage() {
		try {
			restartVerificationCTAOnTooManyFailedAttemptPage.isDisplayed();
			Reporter.log("CTA Restart Verification is displayed on Too many failed attempts page");
		} catch (Exception e) {
			Assert.fail("CTA Restart Verification is not displayed on Too many failed attempts page");
		}
		return this;
	}

	/**
	 * Check Contact Customer Care CTA Too Many Failed Attempts page
	 */
	public MilitaryVerificationPage checkContactCustomerCareCTAOnTooManyFailedAttemptsPage() {
		try {
			contactCustomerCareCTAOnTooManyFailedAttemptPage.isDisplayed();
			Reporter.log("CTA Contact Customer Care is displayed on Too many failed attempts page");
		} catch (Exception e) {
			Assert.fail("CTA Contact Customer Care is not displayed on Too many failed attempts page");
		}
		return this;
	}

	/**
	 * Click on Restart Verification CTA Too Many Failed Attempts page
	 */
	public MilitaryVerificationPage clickOnRestartVerificationCTAOnTooManyFailedAttemptsPage() {
		restartVerificationCTAOnTooManyFailedAttemptPage.click();
		verifyMilitaryVerificationPage();
		return this;
	}

	/**
	 * Click on Browser Back
	 */
	public MilitaryVerificationPage clickOnBrowserBack() {
		getDriver().navigate().back();
		verifyMilitaryVerificationPage();
		return this;
	}

	/**
	 * Click on Restart Verification CTA Too Many Failed Attempts page
	 */
	public MilitaryVerificationPage clickOnContactCustomerCareCTAOnTooManyFailedAttemptsPage() {
		contactCustomerCareCTAOnTooManyFailedAttemptPage.click();
		return this;
	}

	/**
	 * Verify Contact us page
	 */
	public MilitaryVerificationPage verifyContactUsPage() {
		String getcurrentURL = null;
		getcurrentURL = getDriver().getCurrentUrl();
		if (getcurrentURL.contains("contact-us"))
			Reporter.log("Clicking on CTA Contact Customer Care redirects to Contact us page.");
		else
			Assert.fail("Clicking on CTA Contact Customer Care is not redirecting to Contact us page.");
		return this;
	}

	/**
	 * Check Header on Too many failed attempts page
	 */
	public MilitaryVerificationPage checkHeaderTextOnUploadDocumentPage() {
		try {
			headerOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Header is displayed on Upload Document First Attempt page");
			String actualText = headerOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Military verification",
					" Header text on Upload Document First Attempt page is mismatched");
			Reporter.log("Header on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail("Header is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check subtitle on Too many failed attempts page
	 */
	public MilitaryVerificationPage checkSubtitleOnUploadDocumentPage() {
		try {
			subtitleOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Subtitle is displayed on Upload Document First Attempt page");
			String actualText = subtitleOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Upload documentation",
					"Subtitle text on Upload Document First Attempt page is mismatched");
			Reporter.log("Subtitle on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail("Subtitle is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Message1 on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkMessage1OnUploadDocumentPage() {
		try {
			String messageNote = "We need documented proof of your current military affiliation to verify eligibility. Confirm the information below is correct and then upload a document that confirms your current military affiliation.";
			message1OnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Message is displayed on Upload Document First Attempt page");
			String actualText = message1OnUploadDocumentFirstAttemptPage.getText();
			if (actualText.contains(messageNote))
				Reporter.log("Message is matched on Upload Document First Attempt page");
			else
				Assert.fail("Message is mismatched on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("Message is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Message2 on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkMessage2OnUploadDocumentPage() {
		try {
			String messageNote = "If the information is incorrect or doesn't match what's on your documentation, please go back and edit your verification form or contact Customer Care for assistance.";
			message1OnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Message is displayed on Upload Document First Attempt page");
			String actualText = message2OnUploadDocumentFirstAttemptPage.getText();
			if (actualText.contains(messageNote))
				Reporter.log("Message is matched on Upload Document First Attempt page");
			else
				Assert.fail("Message is mismatched on Upload Document First Attempt page");

		} catch (Exception e) {
			Assert.fail("Message is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Message3 on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkMessage3OnUploadDocumentPage() {
		try {
			String messageNote = "Note: If the name has changed, please upload documentation that verifies the name change.";
			message1OnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Message is displayed on Upload Document First Attempt page");
			String actualText = message2OnUploadDocumentFirstAttemptPage.getText();
			if (actualText.contains(messageNote))
				Reporter.log("Message is matched on Upload Document First Attempt page");
			else
				Assert.fail("Message is mismatched on Upload Document First Attempt page");

		} catch (Exception e) {
			Assert.fail("Message is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Edit Verification Form Link on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkLinkEditVerificationFormOnUploadDocumentPage() {
		try {
			editVerificationFormLinkOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Edit Verification Form Link is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("Edit Verification Form Link is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Contact Customer care Form Link on Upload Document First Attempt
	 * page
	 */
	public MilitaryVerificationPage checkContactCustomerCareLinkFormOnUploadDocumentPage() {
		try {
			contactCustomerCareLinkOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Contact Customer Care Link is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("Contact Customer Care Link is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check All fields under Confirm your information section
	 */
	public MilitaryVerificationPage checkFieldsUnderConfirmYourInformationSectionOnUploadDocumentPage() {
		checkLabelConfirmYourInformationOnUploadDocumentPage();
		checkLabelNameOfMilitaryMemberOnUploadDocumentFirstAttemptPage();
		checkFirstNameAndLastNameOnUploadDocumentFirstAttemptPage();
		// checkLabelDOBOnUploadDocumentFirstAttemptPage();
		// checkDOBOnUploadDocumentFirstAttemptPage();
		checkLabelStatusOnUploadDocumentFirstAttemptPage();
		checkStatusOnUploadDocumentFirstAttemptPage();
		checkLabelBranchOfServiceOnUploadDocumentFirstAttemptPage();
		checkBranchOfServiceOnUploadDocumentFirstAttemptPage();
		return this;
	}

	/**
	 * Check Label Confirm your information on Upload Document First Attempt
	 * page
	 */
	public MilitaryVerificationPage checkLabelConfirmYourInformationOnUploadDocumentPage() {
		try {
			labelConfirmYourInformationOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Label Confirm your information is displayed on Upload Document First Attempt page");
			String actualText = labelConfirmYourInformationOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Confirm your information:",
					"Label Confirm your information on Upload Document First Attempt page is mismatched");
			Reporter.log("Label Confirm your information on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail("Label Confirm your information is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Label Name of Military member on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkLabelNameOfMilitaryMemberOnUploadDocumentFirstAttemptPage() {
		try {
			labelNameOfMilitaryMemberOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Label Name of Military member is displayed on Upload Document First Attempt page");
			String actualText = labelNameOfMilitaryMemberOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Name of military member",
					"Label Name of Military member on Upload Document First Attempt page is mismatched");
			Reporter.log("Label Name of Military member on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail("Label Name of Military member is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check First name and last name is displayed on Upload Document First
	 * Attempt page or not
	 */
	public String checkFirstNameAndLastNameOnUploadDocumentFirstAttemptPage() {
		String name = null;
		try {
			fieldNameOnUploadDocumentFirstAttemptPage.isDisplayed();
			name = fieldNameOnUploadDocumentFirstAttemptPage.getText();
			if (name == null)
				Assert.fail("First name and last names are blank on Upload Document First Attempt page");
			else
				Reporter.log("First name and last names are displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("First name and last names are not displayed on Upload Document First Attempt page");
		}
		return name;
	}

	/**
	 * Verify Name on Verification page and on Upload Page.
	 */
	public MilitaryVerificationPage verifyNameOnVerificationPageAndOnUploadPage(String nameOnVerificationPage) {
		String nameOnUploadPage = null;
		try {
			nameOnUploadPage = checkFirstNameAndLastNameOnUploadDocumentFirstAttemptPage();
			if (nameOnUploadPage.equals(nameOnVerificationPage))
				Reporter.log("First name and last names matched on Veriffication and upload pages");
			else
				Assert.fail("First name and last names are not matched on Veriffication and upload pages");
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Check Label DOB on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkLabelDOBOnUploadDocumentFirstAttemptPage() {
		try {
			labelDOBOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Label DOB  is displayed on Upload Document First Attempt page");
			String actualText = labelDOBOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "DOB", "Label DOB on Upload Document First Attempt page is mismatched");
			Reporter.log("Label DOB on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail("Label DOB is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check DOB is displayed on Upload Document First Attempt page or not
	 */
	public MilitaryVerificationPage checkDOBOnUploadDocumentFirstAttemptPage() {
		try {
			fieldDOBOnUploadDocumentFirstAttemptPage.isDisplayed();
			String name = null;
			name = fieldDOBOnUploadDocumentFirstAttemptPage.getText();
			if (name == null)
				Assert.fail("DOB is blank on Upload Document First Attempt page");
			else
				Reporter.log("DOB is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("DOB is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Cross verify DOB on Upload Page and on Verification page.
	 */
	public MilitaryVerificationPage verifyDOBOnUploadDocumentAndOnVerificationPage(String DOBOnVerificationPage) {
		String dobOnUploadPage = null;
		try {
			dobOnUploadPage = fieldDOBOnUploadDocumentFirstAttemptPage.getText();
			if (dobOnUploadPage.equals(DOBOnVerificationPage))
				Reporter.log("DOB on Verification page and Upload page is matched");
			else
				Assert.fail("DOB on Verification page and Upload page is mismatched");
		} catch (Exception e) {
		}
		return this;
	}

	/**
	 * Check Label Status on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkLabelStatusOnUploadDocumentFirstAttemptPage() {
		try {
			labelStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Label Status  is displayed on Upload Document First Attempt page");
			String actualText = labelStatusOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Status",
					"Label Status on Upload Document First Attempt page is mismatched");
			Reporter.log("Label Status on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail("Label Status is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Status is displayed on Upload Document First Attempt page or not
	 */
	public MilitaryVerificationPage checkStatusOnUploadDocumentFirstAttemptPage() {
		try {
			fieldStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			String status = null;
			status = fieldStatusOnUploadDocumentFirstAttemptPage.getText();
			if (status == null)
				Assert.fail("Status is blank on Upload Document First Attempt page");
			else
				Reporter.log("Status is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("Status is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Status is displayed on Upload Document First Attempt page or not
	 */
	public MilitaryVerificationPage verifyStatusOnUploadDocumentPageForActiveDuty() {
		try {
			fieldStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			String status = null;
			status = fieldStatusOnUploadDocumentFirstAttemptPage.getText();

			if (status.equals("Active duty"))
				Reporter.log("Correct status displayed on Upload page for Active Duty user");
			else
				Assert.fail("Correct status not displayed on Upload page for Active Duty user");
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Check Status is displayed on Upload Document First Attempt page or not
	 * for Veteran or retiree
	 */
	public MilitaryVerificationPage verifyStatusOnUploadDocumentPageForVeteranOrRetiree() {
		try {
			fieldStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			String status = null;
			status = fieldStatusOnUploadDocumentFirstAttemptPage.getText();

			if (status.equals("Veteran or retiree"))
				Reporter.log("Correct status displayed on Upload page for Veteran or retiree user");
			else
				Assert.fail("Correct status not displayed on Upload page for Veteran or retiree user");
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Check Status is displayed on Upload Document First Attempt page or not
	 * for National Guard or Reserve
	 */
	public MilitaryVerificationPage verifyStatusOnUploadDocumentPageForNationalGuardOrReserve() {
		try {
			fieldStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			String status = null;
			status = fieldStatusOnUploadDocumentFirstAttemptPage.getText();

			if (status.equals("National Guard or Reserve"))
				Reporter.log("Correct status displayed on Upload page for National Guard or Reserve user");
			else
				Assert.fail("Correct status not displayed on Upload page for National Guard or Reserve user");
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Check Label Branch of Service on Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkLabelBranchOfServiceOnUploadDocumentFirstAttemptPage() {
		try {
			labelBranchOfServiceOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Label Branch of Service  is displayed on Upload Document First Attempt page");
			String actualText = labelBranchOfServiceOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Branch of service",
					"Label Branch of Service on Upload Document First Attempt page is mismatched");
			Reporter.log("Label Branch of Service on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail("Label Branch of Service is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Branch of Service is displayed on Upload Document First Attempt
	 * page or not
	 */
	public MilitaryVerificationPage checkBranchOfServiceOnUploadDocumentFirstAttemptPage() {
		try {
			fieldBranchOfServiceOnUploadDocumentFirstAttemptPage.isDisplayed();
			String branchOfService = null;
			branchOfService = fieldBranchOfServiceOnUploadDocumentFirstAttemptPage.getText();
			if (branchOfService == null)
				Assert.fail("Branch of Service is blank on Upload Document First Attempt page");
			else
				Reporter.log("Branch of Service is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("Branch of Service is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Branch of Service is displayed on Upload Document First Attempt
	 * page or not
	 */
	public MilitaryVerificationPage verifyBranchOfServiceOnUploadAndVerificationPage(
			String branchOfServiceOnVerificationPage) {
		try {
			fieldBranchOfServiceOnUploadDocumentFirstAttemptPage.isDisplayed();
			String branchOfServiceOnUploadPage = null;
			branchOfServiceOnUploadPage = fieldBranchOfServiceOnUploadDocumentFirstAttemptPage.getText();
			if (branchOfServiceOnUploadPage.equals(branchOfServiceOnVerificationPage))
				Reporter.log("Branch of Service on Upload and verification page is matched.");
			else
				Assert.fail("Branch of Service on Upload and verification page is mismatched.");
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Verify Relationship status on Upload Document First Attempt page or not
	 */
	public MilitaryVerificationPage verifyRelationshipStatusOnUploadAndVerificationPage(
			String relationshipStatusOnVerificationPage) {
		try {
			fieldStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			String relationshipStatusOnUploadPage = null;
			relationshipStatusOnUploadPage = fieldStatusOnUploadDocumentFirstAttemptPage.getText();
			if (relationshipStatusOnUploadPage.equals(relationshipStatusOnVerificationPage))
				Reporter.log("Relationship status on Upload and verification page is matched.");
			else
				Assert.fail("Relationship status on Upload and verification page is mismatched.");
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Check All fields under Documents must include the following
	 */
	public MilitaryVerificationPage checkAllFieldsUnderDocumentsMustIncludeTheFollowingSectionOnUploadDocumentFirstAttemptPage() {
		checkFirstCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkFirstNameUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkSecondCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkLastNameUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkThirdCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		// checkLabelDOBUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkThirdCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkCurrentMilitaryStatusUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage();
		checkTextStartsWithExampleUnderDocumentMustIncludeSectionOnUploadPage();
		return this;
	}

	/**
	 * Check first checkmark
	 */
	public MilitaryVerificationPage checkFirstCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			firstCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"First check mark is displayed under Document Must Include Section on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail(
					"First check mark is not displayed under Document Must Include Section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check 2nd checkmark
	 */
	public MilitaryVerificationPage checkSecondCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			secondCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Second check mark is displayed under Document Must Include Section on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail(
					"Second check mark is not displayed under Document Must Include Section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check 3rd checkmark
	 */
	public MilitaryVerificationPage checkThirdCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			thirdCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Third check mark is displayed under Document Must Include Section on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail(
					"Third check mark is not displayed under Document Must Include Section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check 4th checkmark
	 */
	public MilitaryVerificationPage checkFourthCheckmarkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			fourthCheckMarkUnderDocumentMustOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Fourth check mark is displayed under Document Must Include Section on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail(
					"Fourth check mark is not displayed under Document Must Include Section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Label First name under Documents must include section on Upload
	 * Document First Attempt page
	 */
	public MilitaryVerificationPage checkFirstNameUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			labelFirstNameUnderDocumentMustOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Label First name is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = labelFirstNameUnderDocumentMustOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "First Name",
					"Label First name under Documents must include the following section on Upload Document First Attempt page is mismatched");
			Reporter.log(
					"Label First name under Documents must include the following section on Upload Document First Attempt page is mismatched");
		} catch (Exception e) {
			Assert.fail(
					"Label First name is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Label Last name under Documents must include section on Upload
	 * Document First Attempt page
	 */
	public MilitaryVerificationPage checkLastNameUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			labelLastNameUnderDocumentMustOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Label Last name is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = labelLastNameUnderDocumentMustOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Last Name",
					"Label Last name under Documents must include the following section on Upload Document First Attempt page is mismatched");
			Reporter.log(
					"Label Last name under Documents must include the following section on Upload Document First Attempt page is mismatched");
		} catch (Exception e) {
			Assert.fail(
					"Label Last name is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check DOB under Documents must include section on Upload Document First
	 * Attempt page
	 */
	public MilitaryVerificationPage checkLabelDOBUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			labelDOBUnderDocumentMustOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Label Last name is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = labelDOBUnderDocumentMustOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "DOB",
					"Label DOB name under Documents must include the following section on Upload Document First Attempt page is mismatched");
			Reporter.log(
					"Label DOB name under Documents must include the following section on Upload Document First Attempt page is mismatched");
		} catch (Exception e) {
			Assert.fail(
					"Label DOB name is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Current Military Status under Documents must include section on
	 * Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkCurrentMilitaryStatusUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			labelCurrentMilitaryStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Label Current Military Status is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = labelCurrentMilitaryStatusOnUploadDocumentFirstAttemptPage.getText();
			if (actualText.contains("Current Military Status"))
				Reporter.log(
						"Label Current Military Status name under Documents must include the following section on Upload Document First Attempt page is matched");
			else
				Assert.fail(
						"Label Current Military Status name under Documents must include the following section on Upload Document First Attempt page is mismatched");
		} catch (Exception e) {
			Assert.fail(
					"Label Current Military Status name is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check text starts with Example Document Upload page
	 */
	public MilitaryVerificationPage checkTextStartsWithExampleUnderDocumentMustIncludeSectionOnUploadPage() {
		try {
			labelCurrentMilitaryStatusOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Label Current Military Status is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = labelExampleOnUploadDocumentPage.getText();
			if (actualText.contains("Example: 'retired' or 'active duty'"))
				Reporter.log(
						"Label Current Military Status name under Documents must include the following section on Upload Document First Attempt page is matched");
			else
				Assert.fail(
						"Label Current Military Status name under Documents must include the following section on Upload Document First Attempt page is mismatched");
		} catch (Exception e) {
			Assert.fail(
					"Label Current Military Status name is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check notification under Documents must include section on Upload
	 * Document First Attempt page
	 */
	public MilitaryVerificationPage checkNotificationUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			notificationTextOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Notification text is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = notificationTextOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText,
					"Please omit or black out any sensitive information such as Social Security number, or Military ID#.",
					"Notification text under Documents must include the following section on Upload Document First Attempt page is mismatched");
			Reporter.log(
					"Notification text under Documents must include the following section on Upload Document First Attempt page is mismatched");
		} catch (Exception e) {
			Assert.fail(
					"Notification text is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Acceptable File formats under Documents must include section on
	 * Upload Document First Attempt page
	 */
	public MilitaryVerificationPage checkAcceptableFileFormatsUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			acceptableFileFormatTextOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Acceptable file formats text is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = acceptableFileFormatTextOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Acceptable file formats: .bmp, .gif, .jpg, .png, and .pdf",
					"Acceptable file formats under Documents must include the following section on Upload Document First Attempt page is mismatched");
			Reporter.log(
					"Acceptable file formats under Documents must include the following section on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail(
					"Acceptable file formats is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check text Max file size under Documents must include section on Upload
	 * Document First Attempt page
	 */
	public MilitaryVerificationPage checkTextMaxFileSizeUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			maxFileSizeTextOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log(
					"Text Max file size is displayed under Documents must include the following section on Upload Document First Attempt page");
			String actualText = maxFileSizeTextOnUploadDocumentFirstAttemptPage.getText();
			Assert.assertEquals(actualText, "Max file size: 10MB",
					"Text Max file size under Documents must include the following section on Upload Document First Attempt page is mismatched");
			Reporter.log(
					"Text Max file size under Documents must include the following section on Upload Document First Attempt page is matched");
		} catch (Exception e) {
			Assert.fail(
					"Text Max file size is not displayed under Documents must include the following section on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Choose file to upload CTA on Upload Documents first attempts page
	 */
	public MilitaryVerificationPage checkChooseFileToUploadCTAOnUploadDocumentFirstAttemptPage() {
		try {
			chooseFileToUploadCTAOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Choose File To Upload CTA is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("Choose File To Upload CTA is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Choose file to upload CTA is enabled or not on Military
	 * Verification page
	 */
	public MilitaryVerificationPage checkChooseFileToUploadCTAIsEnabledOrNot() {
		try {
			boolean submitDocumentationCTAStatus = chooseFileToUploadCTAOnUploadDocumentFirstAttemptPage.isEnabled();
			if (submitDocumentationCTAStatus == true)
				Reporter.log("Choose file to upload CTA is enabled");
			else {
				Assert.fail("Choose file to upload CTA is disabled");
			}
		} catch (Exception e) {
			Reporter.log("Choose file to upload CTA is disabled");
		}
		return this;
	}

	/**
	 * Check Submit documentation CTA on Upload Documents first attempts page
	 */
	public MilitaryVerificationPage checkSubmitDocumentationCTAOnUploadDocumentFirstAttemptPage() {
		try {
			submitDocumentationOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("Submit documentation CTA is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("Submit documentation CTA is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check Submit documentation CTA is disabled or not on Military
	 * Verification page
	 */
	public MilitaryVerificationPage checkSubmitDocumentationCTAIsDisabledOrNot() {
		try {
			boolean submitDocumentationCTAStatus = submitDocumentationOnUploadDocumentFirstAttemptPage.isEnabled();
			if (submitDocumentationCTAStatus == true)
				Assert.fail("Submit Documentation CTA is enabled");
			else {
				Reporter.log("Submit Documentation CTA is disabled");
			}
		} catch (Exception e) {
			Reporter.log("Submit Documentation CTA is disabled");
		}
		return this;
	}

	/**
	 * Check FAQ text under Documents must include section on Upload Document
	 * First Attempt page
	 */
	public MilitaryVerificationPage checkFAQTextUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			faqTextOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("FAQ Text is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("FAQ Text is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Check FAQ link under Documents must include section on Upload Document
	 * First Attempt page
	 */
	public MilitaryVerificationPage checkFAQLinkUnderDocumentMustIncludeSectionOnUploadDocumentFirstAttemptPage() {
		try {
			faqLinkOnUploadDocumentFirstAttemptPage.isDisplayed();
			Reporter.log("FAQ Link is displayed on Upload Document First Attempt page");
		} catch (Exception e) {
			Assert.fail("FAQ Link is not displayed on Upload Document First Attempt page");
		}
		return this;
	}

	/**
	 * Read First name of Military member on Military Verification page
	 */
	public String getFirstNameOfMilitaryMemberBeforeReset() {
		String firstName = textFieldFirstNameOfMilitaryMember.getText();
		return firstName;
	}

	/**
	 * Read Last name of Military member on Military Verification page
	 */
	public String getLastNameOfMilitaryMemberBeforeReset() {
		String lastName = textFieldLastNameOfMilitaryMember.getText();
		return lastName;
	}

	/**
	 * Validate First name
	 */
	public MilitaryVerificationPage validateFirstNameOfMilitaryMemberWithTheAlreadyEnteredName(String actualFirstName,
			String expectedFirstName) {
		if (actualFirstName.equals(expectedFirstName))
			Reporter.log("First name is reseted");
		else
			Assert.fail("First name not reseted after user changes the option");
		return this;
	}

	/**
	 * Validate First name
	 */
	public MilitaryVerificationPage validateLastNameOfMilitaryMemberWithTheAlreadyEnteredName(String actualLastName,
			String expectedLastName) {
		if (actualLastName.equals(expectedLastName))
			Reporter.log("Last name is reseted");
		else
			Assert.fail("Last name not reseted after user changes the option");
		return this;
	}

	/**
	 * Enter First name and Last Name
	 */
	public MilitaryVerificationPage enterFirstNameAndLastName() {
		textFieldFirstNameOfMilitaryMember.sendKeys("ABCDEF");
		textFieldLastNameOfMilitaryMember.sendKeys("XYZ");
		return this;
	}

	/**
	 * Check redirection of Edit Verification link on Upload Document first time
	 */
	public MilitaryVerificationPage checkRedirectionOfEditVerificationLink() {
		editVerificationFormLinkOnUploadDocumentFirstAttemptPage.click();
		verifyMilitaryVerificationPage();
		return this;
	}

	/**
	 * Check whether Back Arrow is displayed on Military Verification page or
	 * not
	 */
	public MilitaryVerificationPage checkBackArrowOnMilitaryVerificationPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(backArrowOnMilitaryVerificationPage.isDisplayed(),
					"Back Arrow is not displayed on Military Verification page");
			Reporter.log("Back Arrow is displayed on Military Verification page");
		} catch (Exception e) {
			Assert.fail("Back Arrow is not displayed on Military Verification page");
		}
		return this;
	}

	/**
	 * Click on Back Arrow on Military Verification page or not
	 */
	public MilitaryVerificationPage clickOnBackArrowOnMilitaryVerificationPage() {
		try {
			backArrowOnMilitaryVerificationPage.click();
			Reporter.log("Back Arrow on Military Verification page is clicked");
		} catch (Exception e) {
			Assert.fail("Back Arrow on Military Verification page is not clicked");
		}
		return this;
	}

	/**
	 * Click on Back Arrow on Military Verification page or not
	 */
	public MilitaryVerificationPage clickOnBackArrowOnMilitaryVerificationStatusPage() {
		try {
			backArrowOnMilitaryVerificationPage.click();
			Reporter.log("Back Arrow on Military Verification Status page is clicked");
		} catch (Exception e) {
			Assert.fail("Back Arrow on Military Verification Status page is not clicked");
		}
		return this;
	}

	/**
	 * Check whether T-Mobile Logo is displayed on Military Verification page or
	 * not
	 */
	public MilitaryVerificationPage checkTMobileLogoOnMilitaryVerificationPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(logoTMobileIcon.isDisplayed(),
					"T-Mobile Logo is not displayed on Military Verification page");
			Reporter.log("T-Mobile Logo is displayed on Military Verification page");
		} catch (Exception e) {
			Assert.fail("T-Mobile Logo is not displayed on Military Verification page");
		}
		return this;
	}

	/**
	 * Click on T-Mobile Logo on Military Verification
	 */
	public MilitaryVerificationPage clickOnTMobileLogoOnMilitaryVerificationStatusPage() {
		try {
			logoTMobileIcon.click();
			Reporter.log("T-Mobile logo on Military Verification Status page is clicked");
		} catch (Exception e) {
			Assert.fail("T-Mobile logo on Military Verification Status page is not clicked");
		}
		return this;
	}

	/**
	 * Check whether T-Contact Us Logo is displayed on Military Verification
	 * page or not
	 */
	public MilitaryVerificationPage checkContactUsLogoOnMilitaryVerificationPage() {
		try {
			waitforSpinner();
			Assert.assertTrue(contactUSIconOnMilitaryVerificationPage.isDisplayed(),"Contact us icon is not displayed on Military Verification page");
			Reporter.log("Contact us icon is displayed on Military Verification page");
		} catch (Exception e) {
			Assert.fail("Contact us icon is not displayed on Military Verification page");
		}
		return this;
	}

	/**
	 * Click on T-Mobile Logo on Military Verification
	 */
	public MilitaryVerificationPage clickOnContactUsLogoOnMilitaryVerificationStatusPage() {
		try {
			contactUSIconOnMilitaryVerificationPage.click();
			Reporter.log("Contact us icon on Military Verification Status page is clicked");
		} catch (Exception e) {
			Assert.fail("Contact us icon on Military Verification Status page is not clicked");
		}
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		return this;
	}

	/**
	 * Click on Done CTA on Verified Status page.
	 */
	public MilitaryVerificationPage clickOnDoneCTAOnVerifiedStatusPage() {
		try {
			doneCTAOnUploadReceivedAndUnderReview.click();
			Reporter.log("Done CTA is clicked on Upload received and under review page");
		} catch (Exception e) {
			Assert.fail("Done CTA is not clicked Upload received and under review page");
		}
		return this;
	}

	/**
	 * Check Icon on Document not accepted page
	 */
	public MilitaryVerificationPage checkIconOnDocumentNotAcceptedPage() {
		try {
			iconOnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Icon is displayed on Document not accepted page.");
		} catch (Exception e) {
			Reporter.log("Icon is not displayed on Document not accepted page");
		}
		return this;
	}

	/**
	 * Check Header on Document not accepted page.
	 */
	public MilitaryVerificationPage checkHeaderOnDocumentNotAcceptedPage() {
		try {
			headerOnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Header is displayed on Document not accepted page.");
			String actualText = headerOnDocumentNotAcceptedPage.getText();
			Assert.assertEquals(actualText, "Military verification",
					" Header text on Document not accepted page is mismatched");
			Reporter.log("Header on Document not accepted page is matched");
		} catch (Exception e) {
			Assert.fail("Header is not displayed on Document not accepted page.");
		}
		return this;
	}

	/**
	 * Check subtitle on Document not accepted page
	 */
	public MilitaryVerificationPage checkSubtitleOnDocumentNotAcceptedPage() {
		try {
			subTitleOnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Subtitle is displayed on Document not accepted page");
			String actualText = subTitleOnTooManyFailedAttemptPage.getText();
			Assert.assertEquals(actualText, "Document not accepted",
					"Subtitle text on Document not accepted page is mismatched");
			Reporter.log("Subtitle on Document not accepted page is matched");
		} catch (Exception e) {
			Assert.fail("Subtitle is not displayed on Document not accepted page");
		}
		return this;
	}

	/**
	 * Check message1 on Document on accepted page
	 */
	public MilitaryVerificationPage checkMessage1OnDocumentOnAcceptedPage() {
		try {
			String messageNote = "Thank you for your submission; unfortunately, it was not accepted as validation of affiliation. To try again, follow the link below to upload a new document.";
			message1OnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Message is displayed on Document on accepted page");
			String actualText = message1OnDocumentNotAcceptedPage.getText();
			Assert.assertEquals(actualText, messageNote, "Message is mismatched on Document on accepted page");
			Reporter.log("Message is matched on Document on accepted page");
		} catch (Exception e) {
			Assert.fail("Message is not displayed on Document on accepted page");
		}
		return this;
	}

	/**
	 * Check message2 on Document on accepted page
	 */
	public MilitaryVerificationPage checkMessage2OnDocumentOnAcceptedPage() {
		try {
			String messageNote = "If you feel the military affiliation confirmation failure is due to an error, please contact SheerID for assistance.";
			message2OnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Message is displayed on Document on accepted page");
			String actualText = message2OnDocumentNotAcceptedPage.getText();
			Assert.assertEquals(actualText, messageNote, "Message is mismatched on Document on accepted page");
			Reporter.log("Message is matched on Document on accepted page");
		} catch (Exception e) {
			Assert.fail("Message is not displayed on Document on accepted page");
		}
		return this;
	}

	/**
	 * Check Error reasons on Document on accepted page
	 */
	public MilitaryVerificationPage checkErrorReasonsOnDocumentNotAcceptedPage() {
		try {
			errorMessageOnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Reasons for document failed is displayed on Document not accepted page");
		} catch (Exception e) {
			Assert.fail("Message is not displayed on Document not accepted page");
		}
		return this;
	}

	/**
	 * Check Error message on Document not accepted page
	 */
	public MilitaryVerificationPage checkErrorMessageOnDocumentNotAcceptedPage() {
		try {
			errorMessageOnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Error message is displayed on Document not accepted page");
		} catch (Exception e) {
			Reporter.log("Error message is not displayed on Document not accepted page");
		}
		return this;
	}

	/**
	 * Check Error message on Document not accepted page
	 */
	public MilitaryVerificationPage checkUploadNewDocumentCTAOnDocumentNotAcceptedPage() {
		try {
			uploadNewDocumentCTAOnDocumentNotAcceptedPage.isDisplayed();
			Reporter.log("Upload New Document is displayed on Document not accepted page");
		} catch (Exception e) {
			Reporter.log("Upload New Document is not displayed on Document not accepted page");
		}
		return this;
	}

	public MilitaryVerificationPage clickOnEditVerificationForm() {
		try {
			editVerificationForm.click();
			Reporter.log("Edit verification form is displayed");
		} catch (Exception e) {
			Reporter.log("Edit verification form is not displayed");
		}
		return this;
	}
}