package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AppointmentsApiV1 extends ApiCommonLib{
	
	/***
	 * T-Mobile AppointmentsApiV1 API’s.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/Accept'
     *   - $ref: '#/parameters/interactionid'
	 * @return
	 * @throws Exception 
	 */
    public Response getAppointment(ApiTestData apiTestData,String timeZone) throws Exception{
    	Map<String, String> headers = builAppointmentAPIHeader(apiTestData);
    	String resourceURL = "v1/appointment/?timeZone="+timeZone;
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
        return response;
    }
    
    /***
	 * T-Mobile AppointmentsApiV1 API’s.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/Accept'
     *   - $ref: '#/parameters/interactionid'
	 * @return
	 * @throws Exception 
	 */
    public Response createAppointment(ApiTestData apiTestData,String requestbody) throws Exception{
    	Map<String, String> headers = builAppointmentAPIHeader(apiTestData);
    	String resourceURL = "v1/appointment/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestbody ,RestServiceCallType.POST);
        return response;
    }
    
    /***
   	 * T-Mobile AppointmentsApiV1 API’s.
   	 * parameters:
        *   - $ref: '#/parameters/oAuth'
        *   - $ref: '#/parameters/Accept'
        *   - $ref: '#/parameters/interactionid'
   	 * @return
   	 * @throws Exception 
   	 */
       public Response updateAppointment(ApiTestData apiTestData,String requestbody) throws Exception{
       	Map<String, String> headers = builAppointmentAPIHeader(apiTestData);
    	String resourceURL = "v1/appointment/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestbody ,RestServiceCallType.PUT);
		return response;
	}
       
    
    /***
	 * T-Mobile AppointmentsApiV1 API’s.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/Accept'
     *   - $ref: '#/parameters/interactionid'
	 * @return
	 * @throws Exception 
	 */
    public Response deleteAppointment(ApiTestData apiTestData,String appointmentId) throws Exception{
    	Map<String, String> headers = builAppointmentAPIHeader(apiTestData);
    	String resourceURL = "v1/appointment/"+appointmentId;
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.DELETE);
        return response;
    }
    
    /***
	 * T-Mobile AppointmentsApiV1 API’s.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/Accept'
     *   - $ref: '#/parameters/interactionid'
	 * @return
	 * @throws Exception 
	 */
    public Response getAvailableSlots(ApiTestData apiTestData,String requestbody) throws Exception{
    	Map<String, String> headers = builAppointmentAPIHeader(apiTestData);
    	String resourceURL = "v1/appointment/availableSlots";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestbody ,RestServiceCallType.POST);
        return response;
    }
    
    /***
   	 * T-Mobile AppointmentsApiV1 API’s.
   	 * parameters:
        *   - $ref: '#/parameters/oAuth'
        *   - $ref: '#/parameters/Accept'
        *   - $ref: '#/parameters/interactionid'
   	 * @return
   	 * @throws Exception 
   	 */
       public Response getEstimateWaitTime(ApiTestData apiTestData,String requestbody) throws Exception{
       	Map<String, String> headers = builAppointmentAPIHeader(apiTestData);
    	String resourceURL = "v1/appointment/estimateWaitTime";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestbody ,RestServiceCallType.POST);
        return response;
       }
       
    /***
	 * T-Mobile AppointmentsApiV1 API’s.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/Accept'
     *   - $ref: '#/parameters/interactionid'
	 * @return
	 * @throws Exception 
	 */
    public Response getAppointmentCallRoutingContext(ApiTestData apiTestData,String requestbody) throws Exception{
    	Map<String, String> headers = builAppointmentAPIHeader(apiTestData);
    	String resourceURL = "v1/appointment/callRoutingContext";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestbody ,RestServiceCallType.POST);
        return response;
    }
    private Map<String, String> builAppointmentAPIHeader(ApiTestData apiTestData) throws Exception {
    	String accessToken = getAccessToken();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("channel_id", "{{channel_id}}");
		headers.put("Authorization", accessToken);
		headers.put("access_token", accessToken);
		headers.put("application_id", "{{application_id}}");
		headers.put("user-token", "{{user-token}}");
		headers.put("tmo-id", "{{tmo-id}}");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("ban", apiTestData.getBan());
		headers.put("usn", "{{usn}}");
		headers.put("application_client", "{{application_client}}");
		headers.put("application_version_code", "{{application_version_code}}");
		headers.put("device_os", "{{device_os}}");
		headers.put("os_version", "{{os_version}}");
		headers.put("os_language", "{{os_language}}");
		headers.put("device_manufacturer", "{{device_manufacturer}}");
		headers.put("device_model", "{{device_model}}");
		headers.put("marketing_cloud_id", "{{marketing_cloud_id}}");
		headers.put("sd_id", "{{sd_id}}");
		headers.put("adobe_uuid", "{{adobe_uuid}}");
		headers.put("exitState", "MYACCT_WEB_CONTACT_US");
		headers.put("timeZone", "IST");
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("transactionid","FlexAPITest");
		return headers;
	}
}
