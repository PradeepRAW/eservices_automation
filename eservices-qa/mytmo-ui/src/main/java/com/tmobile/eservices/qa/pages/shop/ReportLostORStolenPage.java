package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author ksrinivas
 *
 */
public class ReportLostORStolenPage extends CommonPage {

	@FindBy(css = "#lostStolenHeaderSec [class='ui_headline']")
	private WebElement header;

	@FindBy(id = "nextLocateDevice")
	private WebElement nextLocateDeviceButton;

	@FindBy(id = "radioBlockDevice")
	private WebElement blockDeviceRadioButton;

	@FindBy(id = "radioStolen")
	private WebElement stolenRadioButton;

	@FindBy(id = "nextReportDevice")
	private WebElement nextReportDeviceButton;

	@FindBy(id = "suspendY")
	private WebElement suspendYRadioButton;

	@FindBy(id = "suspendN")
	private WebElement suspendNRadioButton;

	@FindBy(id = "nextSuspendDevice")
	private WebElement nextSuspendDeviceButton;

	@FindBy(id = "acceptedTnC")
	private WebElement acceptedTnCRadioButton;

	@FindBy(id = "nextTnC")
	private WebElement nextTnCButton;

	@FindBy(id = "editReportDevice")
	private WebElement editReportDeviceLink;

	@FindBy(id = "editSuspendDevice")
	private WebElement editSuspendDeviceLink;

	@FindBy(id = "editTnC")
	private WebElement editTnCDeviceLink;

	@FindBy(id = "editReportDevice")
	private WebElement editTnCLink;

	@FindBy(id = "radio2")
	private WebElement stolenDeviceRadioButton;

	@FindBy(id = "confirmRLS")
	private WebElement confirmRLSButton;

	@FindBy(css = "[class='fsrCloseBtn']")
	private WebElement popUpCloseButon;

	@FindBy(id = "#suspend_confirm_modal")
	private WebElement confirmSuspendModal;

	@FindBy(css = "#suspend_confirm_modal a[onclick*='blockDevice()']")
	private WebElement yesButtonToSuspend;

	@FindBy(id = "loststolen")
	private WebElement reportedLoststolen;

	@FindBy(id = "missingtypeother")
	private WebElement iconReportLost;

	/**
	 * @param webDriver
	 */
	public ReportLostORStolenPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * @return the nextLocateDeviceButton
	 */
	public ReportLostORStolenPage clickLocateDeviceButton() {
		try {
			nextLocateDeviceButton.click();
		} catch (Exception e) {
			Assert.fail("next locate Device Button is not Displayed to click");
		}
		return this;
	}

	/**
	 * @return the blockDeviceRadioButton
	 */
	public boolean getBlockDeviceRadioButtonDisplayed() {
		return blockDeviceRadioButton.isDisplayed();
	}

	public String getBlockRadioBtnIds() {
		return "radioBlockDevice";
	}

	/**
	 * Verify Report lost or stolen is displayed
	 * 
	 * @return boolean
	 */
	public ReportLostORStolenPage verifyReportedLoststolenDisplayed() {
		boolean reportLoststolen = false;
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(iconReportLost));
		try {
			if (reportedLoststolen.isDisplayed()) {
				reportLoststolen = true;
			}
			Assert.assertTrue(reportLoststolen, "Verify the Stolen device not reported");
			Reporter.log("Verify the Stolen device reported");
		} catch (Exception e) {
			Assert.fail("Verify the Stolen device not reported");
		}
		return this;
	}

	/**
	 * 
	 * Click Submit Button
	 */
	public void clickSubmitbutton() {
		if (getDriver() instanceof AppiumDriver) {
			((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,-250)", "");
		}
		waitFor(ExpectedConditions.visibilityOf(confirmRLSButton));
		confirmRLSButton.click();
	}

	/**
	 * @return the blockDeviceRadioButton
	 */
	public ReportLostORStolenPage getBlockDeviceRadioButtonEnabled() {
		try {
			blockDeviceRadioButton.isEnabled();
			Reporter.log("Blocked Device radio button is enabled");
		} catch (Exception e) {
			Assert.fail("Blocked Device radio button is not enabled");
		}
		return this;

	}

	/**
	 * click Block Device Radio button
	 */
	public ReportLostORStolenPage clickBlockDeviceRadiobutton() {
		try {
			blockDeviceRadioButton.click();
			Reporter.log("Blocked device Radio button is clickable");
		} catch (Exception e) {
			Assert.fail("Blocked device Radio button is not clickable");
		}
		return this;
	}

	/**
	 * Click StolenDeviceRadioButton
	 */
	public ReportLostORStolenPage clickStolenDeviceRadioButton() {
		try {
			stolenDeviceRadioButton.click();
			Reporter.log("Stolen device radio button is clickable");
		} catch (Exception e) {
			Assert.fail("Stolen device radio button is not clickable");
		}
		return this;
	}

	/**
	 * @return the stolenDeviceRadioButton
	 */
	public ReportLostORStolenPage getStolenDeviceRadioButtonEnabled() {
		try {
			stolenDeviceRadioButton.isEnabled();
			Reporter.log("Stolen device radio button is enabled");
		} catch (Exception e) {
			Assert.fail("Stolen device radio button is not enabled");
		}
		return this;
	}

	/**
	 * @return the stolenRadioButton
	 */
	public ReportLostORStolenPage clickStolenRadiobutton() {
		try {
			stolenRadioButton.click();
			Reporter.log("Stolen radio button is clickable");
		} catch (Exception e) {
			Assert.fail("Stolen radio button is not clickable");
		}
		return this;
	}

	/**
	 * @return the nextReportDeviceButton
	 */
	public ReportLostORStolenPage clickReportDevicebutton() {
		try {
			nextReportDeviceButton.click();
			Reporter.log("Next Report device button is clickable");
		} catch (Exception e) {
			Assert.fail("Next Report device button is not clickable");
		}
		return this;
	}

	/**
	 * @return the suspendYRadioButton
	 */
	public ReportLostORStolenPage getSuspendYRadioButtonEnabled() {
		try {
			suspendYRadioButton.isEnabled();
			Reporter.log("Suspend radio button is enabled");
		} catch (Exception e) {
			Assert.fail("Suspend radio button is not enabled");
		}
		return this;
	}

	/**
	 * @return the suspendYRadioButton
	 */
	public ReportLostORStolenPage clickSuspendYRadioButton() {
		try {
			waitFor(ExpectedConditions.presenceOfElementLocated(By.id("suspendY")), 5);
			suspendYRadioButton.click();
			Reporter.log("Suspend radio button is clickable");
		} catch (Exception e) {
			Assert.fail("Suspend radio button is not clickable");
		}
		return this;
	}

	/**
	 * @return the suspendNRadioButton
	 */
	public ReportLostORStolenPage getSuspendNRadioButtonEnabled() {
		try {
			suspendNRadioButton.isEnabled();
			Reporter.log("Suspend NRadio button is enabled");
		} catch (Exception e) {
			Assert.fail("Suspend NRadio button is not enabled");
		}
		return this;
	}

	/**
	 * @return the suspendNRadioButton
	 */
	public ReportLostORStolenPage clickSuspendNRadioButton() {
		try {
			suspendNRadioButton.click();
			Reporter.log("Suspend NRadio button is clickable");
		} catch (Exception e) {
			Assert.fail("Suspend NRadio button is not clickable");
		}
		return this;
	}

	/**
	 * @return the nextSuspendDeviceButton
	 */
	public ReportLostORStolenPage clickNextsuspendDeviceButton() {
		try {
			nextSuspendDeviceButton.click();
			Reporter.log("Next suspend device button is clickable");
		} catch (Exception e) {
			Assert.fail("Next suspend device button is not clickable");
		}
		return this;
	}

	/**
	 * @return the acceptedTnCRadioButton
	 */
	public ReportLostORStolenPage clickAcceptedTncradioButton() {
		try {
			acceptedTnCRadioButton.click();
			Reporter.log("Accepted TnC Radio Button is clickable");
		} catch (Exception e) {
			Assert.fail("Accepted TnC Radio Button is not clickable");
		}
		return this;
	}

	/**
	 * @return the nextTnCButton
	 */
	public ReportLostORStolenPage getNextTnCButtonDisable() {
		try {
			waitFor(ExpectedConditions.visibilityOf(nextTnCButton));
			Assert.assertFalse(nextTnCButton.isEnabled(), "Next TnC button is enabled");
			Reporter.log("Next TnC button is disabled");
		} catch (Exception e) {
			Assert.fail("Next TnC button is not disabled");
		}
		return this;
	}

	/**
	 * @return the nextTnCButton
	 */
	public ReportLostORStolenPage getNextTnCButtonEnabled() {
		try {
			waitFor(ExpectedConditions.visibilityOf(nextTnCButton));
			nextTnCButton.isEnabled();
			Reporter.log("Next TnC button is enabled");
		} catch (Exception e) {
			Assert.fail("Next TnC button is not enabled");
		}
		return this;
	}

	/**
	 * @return the nextTnCButton
	 */
	public ReportLostORStolenPage clickNextTnCButton() {
		try {
			nextTnCButton.click();
			Reporter.log("Next TnC Button is clickable");
		} catch (Exception e) {
			Assert.fail("Next TnC Button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Trade-In Value Page
	 * 
	 * @return
	 */
	public ReportLostORStolenPage verifyReportLostORStolenPage() {
		try {
			verifyPageUrl();
			Reporter.log("Report lost or stolen page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Report lost or stolen page not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ReportLostORStolenPage verifyPageUrl() {
		waitforSpinner();
		checkPageIsReady();
		try {
			getDriver().getCurrentUrl().contains("lost");
			Reporter.log("Page loaded successfully");
		} catch (Exception e) {
			Assert.fail("Page loding error");
		}
		return this;
	}

}
