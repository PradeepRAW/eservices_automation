package com.tmobile.eservices.qa.pages.payments.api;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class FamilyAllowanceApiV1 extends ApiCommonLib {
	
	/***
	 * Summary: This API is used do the payments  through FamilyAllowance API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-channel_id
	 * 	
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	
	private Map<String, String> buildFamilyAllowanceHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json");
		headers.put("Cache-Control", "no-cache");
		headers.put("Connection", "keep-alive");
		headers.put("channel_id", "Desktop");
		headers.put("ban",apiTestData.getBan());
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("X-B3-SpanId", "abc");
		headers.put("X-B3-TraceId", "abc");
		headers.put("accept-encoding", "gzip, deflate");
		headers.put("application_id", "1234");
		
	
		return headers;
	}
	
	public Response SubScriberNonBusinessandGovtUsers(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildFamilyAllowanceHeader(apiTestData);
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		tokenMap.replace("version", "v1");
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		headers.put("usn", "");
		headers.put("access_token", jwtTokens.get("accessToken"));
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/subscriber/lines", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}
	
	
	public Response SubScriberBusinessandGovtUsers(ApiTestData apiTestData,Map<String, String> tokenMap,String AccountType) throws Exception {
		Map<String, String> headers = buildFamilyAllowanceHeader(apiTestData);
		//headers.put("Authorization", checkAndGetAccessToken());
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		tokenMap.replace("version", "v1");
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		headers.put("access_token", "");
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/subscriber/lines?accountType="+AccountType, RestCallType.GET);
		System.out.println(response.asString());
		return response;
		
	}
	
	
}
