package com.tmobile.eservices.qa.pages.payments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class AutoPayConfirmationPage extends CommonPage {

	@FindBy(css = "autopay-confirmation h4")
	private WebElement autoPayConfirmation;

	@FindBy(css = "div#confirmation-page")
	private WebElement autoPayConfirmationMobile;

	@FindBy(css = "a.gbl-logo")
	private WebElement tMobileIcon;

	@FindBy(css = "button[ng-click*='redirectToHome']")
	private WebElement returnToHome;

	@FindBy(id = "cancelConfirmReturnButton")
	private WebElement cancelConfirmReturnButton;

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	@FindAll({ @FindBy(css = "h4.h4-title"), @FindBy(css = "cancel-autopay-confirmation h4") })
	private WebElement apCancelHeader;

	@FindBy(css = "#cancel-confirmation-page .ui_mobile_headline")
	private WebElement apCancelHeaderMobile;

	@FindBy(css = "div.payment-content.payment-content-err-modal")
	private WebElement errorModal;

	@FindBy(css = "h3.h3-title.text-center.ng-binding")
	private WebElement errorHeader;

	@FindBy(css = "")
	private List<WebElement> autopayStatus;

	@FindBy(css = ".text-center-sm.text-left-xs.body-copy-description-regular.text-black.ng-binding")
	private WebElement firstPaymentDateContent;

	@FindBy(css = ".text-center-sm.legal-txt-clr")
	private WebElement billDUetDateOnAutopay;

	@FindBy(css = "a[ng-click='vm.showHideDetails(true)']")
	private WebElement showPaymentDetails;

	@FindBy(css = ".fine-print-body.pull-left")
	private List<WebElement> paymentSection;

	@FindBy(css = ".fine-print-body.pull-right.ng-binding")
	private List<WebElement> paymentSectionValues;
	
	@FindBy(css = ".fine-print-body.pull-right.ng-scope span")
	private WebElement paymentMethod;
	
	@FindBy(css = "a[ng-click='vm.showHideDetails(false)']")
	private WebElement hidePaymentDetails;

	@FindBy(css = "div.p-l-10.body-copy-description.text-black")
	private List<WebElement> cancelApText;

	@FindBy(css = "i.fa.fa-spin.fa-spinner.interceptor-spinner")
	private WebElement pageSpinner;

	@FindBy(css = "h4.h3-title")
	private WebElement pageLoadElement;

	@FindBy(css = "div.text-black span[pid*='cust']")
	private WebElement alertText;
	
	@FindBy(css = "div.display-flex.alert-center")
	private WebElement autopayVeribageText;
	
	
	

	private final String pageUrl = "/autopay/confirmation";

	/**
	 * @param webDriver
	 */
	public AutoPayConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public AutoPayConfirmationPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
	public AutoPayConfirmationPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			autoPayConfirmation.isDisplayed();
			Reporter.log("AutoPay Confirmation page is verified displayed");
		} catch (Exception e) {
			Reporter.log("Unknown Error - Payment submission failed");
		}
		return this;
	}

	public AutoPayConfirmationPage verifySuccessHeader() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(returnToHome));
			autoPayConfirmation.isDisplayed();
			autoPayConfirmation.getText().contains("SUCCESS!");
			Reporter.log("AutoPay Success header is verified displayed");

		} catch (Exception e) {
			Assert.fail("Autopay confirmation Success page not found");
		}
		return this;
	}
	
	
	public AutoPayConfirmationPage verifyAutopayVeribageText() {
		try {
			checkPageIsReady();
			autopayVeribageText.isDisplayed();
			autopayVeribageText.getText().replaceAll("[0-9]", "").contains("Your $ AutoPay discount will be applied to your account each month starting with your next qualifying bill.");
			Reporter.log("AutoPay veribage text is displayed");

		} catch (Exception e) {
			Assert.fail("Autopay veribage text   not found");
		}
		return this;
	}
	


	/**
	 * verify card details are displayed
	 *
	 * @param paymentType
	 * @return
	 */
	public AutoPayConfirmationPage verifyPaymentDetails(String paymentType) {
		try {

			boolean paymentDetailsDisplayed = true;
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			showPaymentDetails.click();
			List<String> paymentdetails;
			if ("Card".equals(paymentType)) {
				paymentdetails = new ArrayList<>(Arrays.asList("Customer:", "T-mobile account #:", "Date:",
						"Payment method:", "Autopay status:"));
			} else {
				paymentdetails = new ArrayList<>(Arrays.asList("Customer:", "T-mobile account #:", "Date:",
						"Payment method:", "Routing #:", "Autopay status:"));
			}
			for (String detail : paymentdetails) {
				for (WebElement webElement : paymentSection) {
					if (webElement.isDisplayed()) {
						if (detail.equals(webElement.getText())) {
							paymentDetailsDisplayed = true;
						}
					}
				}
				if (!paymentDetailsDisplayed) {
					break;
				}
			}
			hidePaymentDetails.click();
			Assert.assertTrue(paymentDetailsDisplayed,
					"Payment details are not correctly displayed on Show Details - Autopay COnfirmation page");
		} catch (Exception e) {
			Assert.fail("Payment details are not correctly displayed on Show Details - Autopay COnfirmation page");
		}
		return this;
	}

	/**
	 * click tmobile home icon to return to home page
	 */
	public AutoPayConfirmationPage clickreturnToHome() {
		try {
			waitFor(ExpectedConditions.visibilityOf(returnToHome));
			returnToHome.click();
			Reporter.log("Clicked on return to Home");
		} catch (Exception e) {
			Assert.fail("Failed to click return to Home");
		}
		return this;
	}

	/**
	 * click confirm return button
	 */
	public AutoPayConfirmationPage cancelConfirmReturnButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cancelConfirmReturnButton));
			cancelConfirmReturnButton.click();
			Reporter.log("Clicked on cancel confirm Return Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on cancel confirm Return Button");
		}
		return this;
	}

	/**
	 * verify First payment date content
	 *
	 * @return boolean
	 */
	public AutoPayConfirmationPage verifyFirstPaymentDateOnAutopay() {
		try {
			firstPaymentDateContent.isDisplayed();
			firstPaymentDateContent.getText().contains("First Autopay payment date:");
			Reporter.log("Verified the First Autopay payment Date");
		} catch (Exception e) {
			Assert.fail("Failed to verify the irst Payment Date on AUto Pay");
		}
		return this;
	}

	/**
	 * veirfy bill dute date content
	 *
	 * @return boolean
	 */
	public AutoPayConfirmationPage verifyBillDuetDateOnAutopay() {
		try {
			billDUetDateOnAutopay.isDisplayed();
			billDUetDateOnAutopay.getText().contains("Your next bill due date is");
			Reporter.log("Verified the Bill Due Date on Autopay");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Bill Due Date on Autopay");
		}
		return this;
	}

	/**
	 * verify return to home button is displayed
	 *
	 * @return boolean
	 */
	public AutoPayConfirmationPage verifyReturnToHomeBtn() {
		try {
			returnToHome.isDisplayed();
			Reporter.log("Return to Home Button is verified");
		} catch (Exception e) {
			Assert.fail("Fail to verify the Return to Home button ");
		}
		return this;
	}

	/**
	 * verify AutoPay confirmation page elements
	 *
	 * @param autoPayConfirmationPage
	 * @param paymentType
	 */
	public AutoPayConfirmationPage verifyAutoPayConfirmationPageElements(
			AutoPayConfirmationPage autoPayConfirmationPage, String paymentType) {
		try {
			autoPayConfirmationPage.verifyPageLoaded();
			autoPayConfirmationPage.verifySuccessHeader();
			autoPayConfirmationPage.verifyFirstPaymentDateOnAutopay();
			autoPayConfirmationPage.verifyBillDuetDateOnAutopay();
		
			autoPayConfirmationPage.verifyAutopayVeribageText(); //US552377-code 
			autoPayConfirmationPage.verifyPaymentDetails(paymentType);
			autoPayConfirmationPage.verifyReturnToHomeBtn();
		} catch (Exception e) {
			Assert.fail("Fail to verify the Auto page confirmation page Elements");
		}
		return this;
	}

	/**
	 * verify AutoPay Update confirmation page elements
	 *
	 * @param autoPayConfirmationPage
	 * @param paymentType
	 */
	public AutoPayConfirmationPage verifyAutoPayUpdateConfirmationPageElements(
			AutoPayConfirmationPage autoPayConfirmationPage, String paymentType) {
		try {
			checkPageIsReady();
			autoPayConfirmationPage.verifyPageLoaded();
			autoPayConfirmation.getText().equals("UPDATE SUCCESSFUL!");
			firstPaymentDateContent.getText().contains("Next AutoPay Payment Date:");
			autoPayConfirmationPage.verifyBillDuetDateOnAutopay();
			autoPayConfirmationPage.verifyPaymentDetails(paymentType);
			autoPayConfirmationPage.verifyReturnToHomeBtn();
			Reporter.log("Verified Autopay update confirmation page");
		} catch (Exception e) {
			Assert.fail("Fail to verify the Auto pay update confirmation page elements");
		}
		return this;

	}

	
	/**
	 * This method is to verify PII masking is done for all payment information
	 */
	public void verifyPIIMasking() {
		try {
			String alertPIDAttr = alertText.getAttribute("pid");
			Boolean isMasked = !alertPIDAttr.isEmpty() && alertPIDAttr.contains("cust_crd")
					&& alertPIDAttr.matches("cust_crd\\d+");
			Assert.assertTrue(isMasked, "PII Masking is not applied for notification text");
			showPaymentDetails.click();
			for (int i = 0; i < paymentSectionValues.size(); i++) {
				switch (paymentSection.get(i).getText()) {
				case "Customer:":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i), "cust_name"));
					break;
				case "T-mobile account #:":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i), "cust_account"));
					break;
				case "Payment method:":
					Assert.assertTrue(checkElementisPIIMasked(paymentMethod, "cust_crd"));
					break;
				case "Routing #:":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i-1), "cust_crd"));
					break;
				case "Zip:":
					Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i), "cust_zip"));
					break;
				case "":
					if (paymentSection.get(i).getText().isEmpty() && !paymentSectionValues.get(i).getText().isEmpty() && paymentSectionValues.get(i).getText().matches(".*\\d+")) {
						Assert.assertTrue(checkElementisPIIMasked(paymentSectionValues.get(i), "cust_msisdn"));
					}
					break;
				default:
					break;
				}
			}
			Reporter.log("Verified PII masking for Payment details");
			hidePaymentDetails.click();
		} catch (Exception e) {
			Assert.fail("Pii masking for Payment details not found");
		}
	}
	
	
	/**
	 * verify Generic Error Screen
	 *
	 * @param autoPayConfirmationPage
	 * @param paymentType
	 */
	public AutoPayConfirmationPage verifyAutoPaySignupErrorforPAScheduledUser() {
		try {
			checkPageIsReady();
			verifyPageLoaded();
			autoPayConfirmation.getText().equals("Unknown Error");
			Reporter.log("Verified Autopay signup error" );
		} catch (Exception e) {
			Assert.fail("Failed to verify the Verified Autopay signup error");
		}
		return this;

	}



}
