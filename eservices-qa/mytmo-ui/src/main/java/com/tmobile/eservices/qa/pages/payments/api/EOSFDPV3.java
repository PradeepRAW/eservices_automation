package com.tmobile.eservices.qa.pages.payments.api;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.response.Response;

public class EOSFDPV3 extends EOSCommonLib {

//aginghistory

	/***********************************************************************************
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 ***********************************************************************************/

	public Response getResponsenewbankfdp(String alist[], Map<Object, Object> optpara) throws Exception {
		Response response = null;

		if (alist != null) {
			JSONObject rootval = new JSONObject();
			JSONArray items = new JSONArray();
			JSONObject paymentmethod = new JSONObject();
			JSONObject bankAccount = new JSONObject();
			JSONArray accountHolderNames = new JSONArray();

			JSONArray creditcards = new JSONArray();
			JSONArray bankaccounts = new JSONArray();
			JSONArray debitcards = new JSONArray();
			JSONObject payment = new JSONObject();
			JSONObject salesinfo = new JSONObject();
			JSONObject custprofile = new JSONObject();
			JSONArray onetimecharge = new JSONArray();
			JSONArray notes = new JSONArray();

			rootval.put("quoteId", "");
			rootval.put("accountNumber", alist[3]);
			rootval.put("msisdn", alist[0]);
			rootval.put("items", items);
			rootval.put("paymentMethod", paymentmethod);
			paymentmethod.put("code", "Check");
			paymentmethod.put("bankAccount", bankAccount);
			bankAccount.put("accountType", "Checking");
			bankAccount.put("accountNumber", optpara.get("accountNumber"));
			bankAccount.put("accountHolderName", "Test Test");
			bankAccount.put("accountHolderNames", accountHolderNames);
			bankAccount.put("routingNumber", optpara.get("routingNumber"));
			bankAccount.put("accountName", "Test Test");
			paymentmethod.put("addType", "");
			paymentmethod.put("creditCards", creditcards);
			paymentmethod.put("bankAccounts", bankaccounts);
			paymentmethod.put("debitCards", debitcards);
			rootval.put("payment", payment);
			rootval.put("salesInfo", salesinfo);
			salesinfo.put("salesChannel", "WEB");
			salesinfo.put("originalDealerCode", "1114144");
			rootval.put("custProfileDetails", custprofile);
			custprofile.put("preferredLanguage", "English");
			custprofile.put("primaryPhoneNumber", alist[0]);
			custprofile.put("primaryEmailAddress", "4254456116@yopmail.com");
			custprofile.put("alternatePhoneNumber", alist[0]);
			custprofile.put("alternateEmailAddress", "4254456116@yopmail.com");
			rootval.put("isSave", optpara.get("isSave"));
			rootval.put("taxAmount", 0.0);
			rootval.put("chargeAmount", optpara.get("chargeAmount"));
			rootval.put("creditClass", "");
			rootval.put("creationTime", "1555398000000");
			rootval.put("onetimecharge", onetimecharge);
			rootval.put("dated", optpara.get("date"));
			rootval.put("paymentType", "New");
			rootval.put("device", "Desktop");
			rootval.put("notes", notes);
			rootval.put("isSedona", false);

			RestService restService = new RestService(rootval.toString(), eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("ban", alist[3]);
			restService.addHeader("cache-control", "no-cache,no-cache");
			restService.addHeader("interactionId", "123456787");
			restService.addHeader("workflowId", "BILLPAY");
			response = restService.callService("payments/v3/fdp", RestCallType.POST);

		}

		return response;
	}

	public Response responseDeleteFDP(String alist[]) throws Exception {
		Response response = null;

		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("ban", alist[3]);
			response = restService.callService("payments/v3/fdp/" + alist[3], RestCallType.DELETE);

		}

		return response;
	}

}
