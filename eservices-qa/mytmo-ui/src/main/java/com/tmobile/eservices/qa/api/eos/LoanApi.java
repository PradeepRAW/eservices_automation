package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class LoanApi extends ApiCommonLib{
	
	/***
	 * The term ‘Loan’ used as micro-service name and its operations reflect the point-of-view of a T-Mobile customer.
	 * The term ‘loan’ used in the context of OFSLL or the EIP system represents the indvidual items being financed.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
	//Get loans for customer’s billing account (or by subscriber)
    public Response getCustomerLoan(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildLoanAPIHeader(apiTestData);
		String resourceURL = "v1/loans/?accountNumber="+getToken("ban");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
         return response;
    }
    
    //Retrieve single loan
    public Response getCustomerLoanByLoanId(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildLoanAPIHeader(apiTestData);
		String resourceURL = "v1/loans/"+getToken("loanId")+"?accountNumber="+getToken("ban");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
        return response;
    }  
    
    //Get device information for payment ‘paymentId’
    public Response getDeviceInfoForPaymentId(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildLoanAPIHeader(apiTestData);
		String resourceURL = "v1/loans/devicepayments?accountNumber="+getToken("ban")+"&paymentId="+getToken("paymentId");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
         return response;
    }
    
  //Get payment history on loan
    public Response getPaymentHistoryOnLoan(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildLoanAPIHeader(apiTestData);
		String resourceURL = "v1/loans/"+getToken("loanId")+"/payments?accountNumber="+getToken("ban");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
        return response;
    }
    
  //Get loan attributes required by Payment Module to process an EIP Balance Payment
    public Response getLoanAttributesForPayment(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildLoanAPIHeader(apiTestData);
		String resourceURL = "v1/loans/"+getToken("loanId")+"/eipbpdetails?accountNumber="+getToken("ban");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
         return response;
    }
    
   
    private Map<String, String> buildLoanAPIHeader(ApiTestData apiTestData) throws Exception {
    	final String accessToken = getAccessToken();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", accessToken);
		headers.put("interactionId","1234132123");
		headers.put("channelId","WEB");
		headers.put("applicationid","Loans");
		headers.put("OriginatingSenderId","MyTMO");
		headers.put("sourceSystemCode","MyTMO");
		headers.put("timestamp","1544012608522");
		headers.put("activityid","fsafsadsad");
		headers.put("Content-Type","application/json");
		return headers;
	}
   
}
