package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author speddis
 *
 */
public class ShopLineSectorDetailsPage extends CommonPage {
	
	@FindBy(css = "h4.Display2")
	private WebElement pageHeader;

	private final String pageUrl = "lineselector#details";

	@FindBy(css = "div>button.PrimaryCTA")
	private WebElement continueButton;
	

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public ShopLineSectorDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public ShopLineSectorDetailsPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public ShopLineSectorDetailsPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
	    	waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("'backToShop' Transition page not found.");
    	}
        return this;
    }
    
    
    /**
	 * Click Continue  Button 
	 * 

	 */
	public void clickContinueButton() {
		try {
			continueButton.click();
			Reporter.log("Continue Button is Clicked on LineSelectorDetails Page");
		} catch (Exception e) {
			Assert.fail("Continue Button is not Clicked on LineSelectorDetails Page");
		}
	}

   
}
