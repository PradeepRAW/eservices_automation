package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class CostDetailsPage extends CommonPage {

	@FindBy(xpath = "//span[contains(text(),'GSM Line - SOC3')]")
	private WebElement palnName;

	@FindBy(xpath = "//div[contains(text(),'Cost Detail')]")
	private WebElement costDetailsPage;

	@FindBy(css = "span.body.black.float-right")
	private List<WebElement> costofmonthlyTotal;

	@FindBy(xpath = "//span[contains(text(),'Plus taxes and fees')]")
	private List<WebElement> taxExclusive;

	@FindBy(xpath = "//div[contains(text(),'Plan')]")
	private List<WebElement> planComponent;

	@FindBy(css = "p.body.float-left")
	private WebElement promotion;

	@FindBy(css = "span.float-left.arrow-size.arrow-back")
	private WebElement hederBackArrow;

	@FindBy(xpath = "//div[contains(text(),'Plan Details')]")
	private WebElement palnDetails;

	@FindBy(xpath = "//span[contains(text(),'Shared Add-ons')]")
	private WebElement sharedAddon;

	@FindBy(css = "span.float-right.body")
	private List<WebElement> discountAmount;

	@FindBy(css = "span[class*='breadcrumb-item active']")
	private WebElement costDetails;

	@FindBy(css = "div.H6-heading")
	private List<WebElement> monthlyTotalDetails;

	@FindBy(css = "span[class*='body'] img")
	private WebElement planName;

	@FindBy(css = ".no-padding div.Display5")
	private List<WebElement> listOfLineNames;

	@FindBy(css = "p.H6-heading")
	private List<WebElement> listOfLineDetails;

	@FindBy(css = "div.d-inline")
	private List<WebElement> lineDetailsLinks;

	@FindBy(css = "span[role='link']")
	private List<WebElement> breadCrumbLink;

	public CostDetailsPage verifyDisountandPromotionAmount() {
		try {
			checkPageIsReady();
			discountAmount.get(5).isDisplayed();
			Reporter.log("DiscountAmount is displayed in CostDetailsPage");
		} catch (Exception e) {
			Assert.fail("DiscountAmount is not displayed in CostDetailsPage");

		}
		return this;
	}

	public CostDetailsPage verifySharedAddOn() {
		try {
			checkPageIsReady();
			sharedAddon.isDisplayed();
			Reporter.log("Clicked on SharedAddon and Navigated to PlanDetails Page");
		} catch (Exception e) {
			Assert.fail("SharedAddon is not available on AccountOverview Page");

		}
		return this;
	}

	public CostDetailsPage verifyPlanComponent() {
		try {
			checkPageIsReady();
			planComponent.get(0).isDisplayed();
			Reporter.log("PlanComponent is available on CostDetails Page");
		} catch (Exception e) {
			Assert.fail("PlanComponent is not available on CostDetails Page");

		}
		return this;
	}

	public CostDetailsPage verifyCostDetailsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(costDetailsPage.isDisplayed(), "CostDetails Page is not loaded");
			Reporter.log("CostDetails Page is loaded Successfully");
		} catch (Exception e) {
			Assert.fail("CostDetails Page is not loaded");

		}
		return this;
	}

	public CostDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	public CostDetailsPage verifyPlanNameBreadCrump() {
		try {
			checkPageIsReady();
			Assert.assertTrue(costDetails.getText().contains("Cost Details"), "Failed verify Cost Details bread crump");
			Reporter.log("Cost Details bread crump verified");
		} catch (Exception e) {
			Assert.fail("Failed verify Cost Details bread crump");
		}
		return this;
	}

	public CostDetailsPage verifyMonthlyTotalDetails() {
		try {
			Assert.assertTrue(verifyElementBytext(monthlyTotalDetails, "Monthly Total"),
					"Monthly total and Amount are not displaying");
			Assert.assertTrue(verifyElementBytext(monthlyTotalDetails, "$"), "$ is not displaying");
			Reporter.log("Monthly total and Amount are displayed");
		} catch (Exception e) {
			Assert.fail("Monthly total and Amount are not displaying");
		}
		return this;
	}

	/**
	 * Verify on Plan name
	 * 
	 */
	public CostDetailsPage verifyPlanName() {
		try {
			Assert.assertTrue(planName.isDisplayed(), "Plan name is not displayed");
			Reporter.log("Plan name is displayed ");
		} catch (Exception e) {
			Assert.fail("Plan name is not displayed");
		}
		return this;
	}

	/**
	 * Verify on Plan name
	 * 
	 */
	public CostDetailsPage verifyListOfLineNames() {
		try {
			Assert.assertTrue(listOfLineNames.get(0).isDisplayed());
			Reporter.log("Line names are displayed ");
		} catch (Exception e) {
			Assert.fail("Line names are not displayed");
		}
		return this;
	}

	/**
	 * Verify on Plan name
	 * 
	 */
	public CostDetailsPage verifyListOfLineDetails() {
		try {
			Assert.assertTrue(listOfLineDetails.get(0).isDisplayed());
			Reporter.log("Line names are displayed ");
		} catch (Exception e) {
			Assert.fail("Line names are not displayed");
		}
		return this;
	}

	/**
	 * Verify on Plan name
	 * 
	 */
	public CostDetailsPage clickOnPlanName() {
		try {
			planName.click();
			Reporter.log("Clicked on plan name ");
		} catch (Exception e) {
			Assert.fail("Not able to click on plan name");
		}
		return this;
	}

	/**
	 * Verify on Plan name
	 * 
	 */
	public CostDetailsPage clickOnLineDetailsLinks(int index) {
		try {
			lineDetailsLinks.get(index).click();
			Reporter.log("Clicked on lineDetailsLinks ");
		} catch (Exception e) {
			Assert.fail("Not able to lineDetailsLinks");
		}
		return this;
	}

	/**
	 * click on bread Crumb Link
	 * 
	 */
	public CostDetailsPage clickOnBreadCrumbLink(int index) {
		try {
			breadCrumbLink.get(index).click();
			Reporter.log("Clicked on bread Crumb Link ");
		} catch (Exception e) {
			Assert.fail("Not able to bread Crumb Link");
		}
		return this;
	}

}
