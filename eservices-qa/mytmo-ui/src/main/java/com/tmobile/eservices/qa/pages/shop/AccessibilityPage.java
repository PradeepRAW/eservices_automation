package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class AccessibilityPage extends CommonPage {

	public AccessibilityPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Accessibility Page
	 * 
	 * @return
	 */
	public AccessibilityPage vefifyAccessibilityPage() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("Navigated to Accessibility page");
		} catch (Exception e) {
			Assert.fail("Accessibility page is not displayed");
		}
		return this;
	}
	
	 /**
     * Verify that current page URL matches the expected URL.
     */
    public AccessibilityPage verifyPageUrl() {
    	try{
			getDriver().getCurrentUrl().contains("accessibility-policy");
		}catch(Exception e){
			Assert.fail("Unable to navigate to accessibility page");
		}
        return this;
    }

}
