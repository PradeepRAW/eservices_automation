package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class WeWillHelpYouJoinUsPage extends TmngCommonPage {
	private int timeout = 30;

	private final String pageLoadedText = "Get a new phone and we’ll pay off your current phone and service contracts – up to $650 per line or $350 in early termination fees, via prepaid card";
	private final String pageUrl = "/resources/how-to-join-us";

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	private WebElement about;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/accessibility-policy']")
	private WebElement accessibility;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	private WebElement accessories;

	@FindBy(css = "a[href='//prepaid-phones.t-mobile.com/prepaid-activate']")
	private WebElement activateYourPrepaidPhoneOrDevice;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) div.panel-collapse.collapse.anchorID-720032 div.panel-body p a:nth-of-type(2)")
	private WebElement android1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) div.panel-collapse.collapse.anchorID-720032 div.panel-body p a:nth-of-type(2)")
	private WebElement android2;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	private WebElement business2;

	@FindBy(css = "a.btn-secondary.btn-brand.btn.tmo_tfn_number")
	private WebElement call1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	private WebElement callUs;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) button.accordion-toggle.collapsed.")
	private WebElement canIJoinTmobileAt1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement canIJoinTmobileAt2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement canIKeepMyPhone1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement canIKeepMyPhone2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button.accordion-toggle.collapsed.")
	private WebElement canISwitchToTmobile1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement canISwitchToTmobile2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) button.accordion-toggle.collapsed.")
	private WebElement canITradeInMy1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement canITradeInMy2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) button.accordion-toggle.collapsed.")
	private WebElement canITransferMyCurrent1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement canITransferMyCurrent2;

	@FindBy(css = "a[href='//www.t-mobile.com/careers']")
	private WebElement careers;

	@FindBy(css = "a[href='/resources/bring-your-own-phone?icid=WMM_TM_Q417SWITCH_VOXUMET4LN12513']")
	private WebElement checkCompatibility;

	@FindBy(css = "a[href='/resources/keep-your-number?icid=WMM_TM_Q417SWITCH_A1U53N2AFXT12512']")
	private WebElement checkEligibility;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-collapse.collapse.anchorID-594920 div.panel-body p a:nth-of-type(1)")
	private WebElement checkIfYourExistingNumberIs1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) div.panel-collapse.collapse.anchorID-594920 div.panel-body p a:nth-of-type(1)")
	private WebElement checkIfYourExistingNumberIs2;

	@FindBy(css = "a[href='/offers/switch-from-verizon-to-t-mobile?icid=WMM_TM_Q417SWITCH_JO0Z7K0SXZX12510']")
	private WebElement checkItOut;

	@FindBy(css = "a[href='http://www.t-mobile.com/orderstatus/default.aspx']")
	private WebElement checkOrderStatus;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	private WebElement checkOutTheCoverage;

	@FindBy(css = "a[href='//www.t-mobile.com/resources/phone-trade-in?icid=WMM_TM_Q417SWITCH_XQM3MLSY8R712839']")
	private WebElement checkTradeinValue;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) div.panel-collapse.collapse.anchorID-285284 div.panel-body p a:nth-of-type(1)")
	private WebElement checkYourPhonesCompatibility1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) div.panel-collapse.collapse.anchorID-285284 div.panel-body p a:nth-of-type(1)")
	private WebElement checkYourPhonesCompatibility2;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info']")
	private WebElement consumerInformation;

	@FindBy(css = "a[href='http://www.t-mobile.com/contact-us.html']")
	private WebElement contactInformation;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.panel-collapse.collapse.anchorID-031248 div.panel-body p a")
	private WebElement coverageMap1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) div.panel-collapse.collapse.anchorID-031248 div.panel-body p a")
	private WebElement coverageMap2;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	private WebElement deals2;

	@FindBy(css = "a[href='//www.telekom.com/']")
	private WebElement deutscheTelekom;

	@FindBy(css = "a[href='//support.t-mobile.com/community/phones-tablets-devices']")
	private WebElement deviceSupport;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-collapse.collapse.anchorID-859988 div.panel-body p a")
	private WebElement deviceTradein1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) div.panel-collapse.collapse.anchorID-859988 div.panel-body p a")
	private WebElement deviceTradein2;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	private WebElement espaol;

	@FindBy(css = "a.lang-toggle-wrapper")
	private WebElement espaolEspaol;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator?icid=WMM_TM_Q417SWITCH_OLSENDPINTK12508']")
	private WebElement findAStore1;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator?icid=WMM_TM_Q417SWITCH_E1ZCM4BQFF12577']")
	private WebElement findAStore2;

	@FindBy(css = "a[href='http://www.t-mobile.com/store-locator.html']")
	private WebElement findAStore3;

	@FindBy(css = "a[href='//www.switch2t-mobile.com/?icid=WMM_TM_Q417SWITCH_RKMQZJCRQO112689']")
	private WebElement findOutHow;

	@FindBy(css = "a[href='http://www.t-mobile.com/promotions']")
	private WebElement getARebate;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement howDoIKnowIf1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement howDoIKnowIf2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement ifISwitchWillTmobile1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement ifISwitchWillTmobile2;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx#choicesaboutadvertising']")
	private WebElement interestbasedAds;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	private WebElement internationalCalling;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global.html']")
	private WebElement internationalRates;

	@FindBy(css = "a[href='http://iot.t-mobile.com/']")
	private WebElement internetOfThings;

	@FindBy(css = "a[href='http://investor.t-mobile.com/CorporateProfile.aspx?iid=4091145']")
	private WebElement investorRelations;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) div.panel-collapse.collapse.anchorID-720032 div.panel-body p a:nth-of-type(3)")
	private WebElement ios1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) div.panel-collapse.collapse.anchorID-720032 div.panel-body p a:nth-of-type(3)")
	private WebElement ios2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) div.panel-collapse.collapse.anchorID-382762 div.panel-body ol li:nth-of-type(2) a:nth-of-type(2)")
	private WebElement learnMore1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) div.panel-collapse.collapse.anchorID-382762 div.panel-body ol li:nth-of-type(2) a:nth-of-type(2)")
	private WebElement learnMore2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div.panel-collapse.collapse.anchorID-935682 div.panel-body p a")
	private WebElement learnMoreAboutThePerksOf1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) div.panel-collapse.collapse.anchorID-935682 div.panel-body p a")
	private WebElement learnMoreAboutThePerksOf2;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	private WebElement logIn;

	@FindBy(css = "button.hamburger.hamburger-border")

	private WebElement menuMenu;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) div.panel-collapse.collapse.anchorID-549957 div.panel-body p a:nth-of-type(1)")
	private WebElement online1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) div.panel-collapse.collapse.anchorID-549957 div.panel-body p a:nth-of-type(1)")
	private WebElement online2;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/policies/internet-service']")
	private WebElement openInternet;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	private WebElement phones;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	private WebElement plans;

	@FindBy(css = "a[href='//support.t-mobile.com/community/plans-services']")
	private WebElement plansServices;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	private WebElement prepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/news']")
	private WebElement press;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/privacy']")
	private WebElement privacyCenter;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx']")
	private WebElement privacyStatement;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/safety/9-1-1']")
	private WebElement publicSafety911;

	@FindBy(css = "a[href='http://www.t-mobilepr.com/']")
	private WebElement puertoRico;

	@FindBy(css = "a[href='//support.t-mobile.com/community/billing']")
	private WebElement questionsAboutYourBill;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-refill-api-bridging']")
	private WebElement refillYourPrepaidAccount;

	@FindBy(css = ".marketing-page.ng-scope div:nth-of-type(8) button")
	private WebElement scrollToTop;

	@FindBy(id = "searchText")
	private WebElement search1;

	@FindBy(id = "searchText")
	private WebElement search2;

	@FindBy(css = "#ac96e4314db210e0fad5a3d49adb12748c746d59 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms1;

	@FindBy(css = "#77f99b026ccbab25cd45c14ee50b50155ae4c7e6 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms2;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	private WebElement shopPhones1;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_Q417SWITCH_4D1HTULO8K12575']")
	private WebElement shopPhones2;

	@FindBy(css = "a[href='#divfootermain']")
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	private WebElement smartDevices;

	@FindBy(css = "a.btn-primary.btn-brand.btn")
	private WebElement startOnline;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	private WebElement stores;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/student-teacher-smartphone-tablet-discounts?icid=WMM_TM_CAMPUSEXCL_UHTZZ7GR1RT11028']")
	private WebElement studentteacherDiscount;

	@FindBy(css = ".marketing-page.ng-scope header.global-header nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch2;

	@FindBy(css = "a[href='//support.t-mobile.com/']")
	private WebElement supportHome;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) div.panel-collapse.collapse.anchorID-382762 div.panel-body ol li:nth-of-type(2) a:nth-of-type(1)")
	private WebElement switch2tmobileCom1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) div.panel-collapse.collapse.anchorID-382762 div.panel-body ol li:nth-of-type(2) a:nth-of-type(1)")
	private WebElement switch2tmobileCom2;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	private WebElement termsConditions;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true']")
	private WebElement termsOfUse;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) div.panel-collapse.collapse.anchorID-285284 div.panel-body p a:nth-of-type(2)")
	private WebElement tmobile11800;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) div.panel-collapse.collapse.anchorID-549957 div.panel-body p a:nth-of-type(2)")
	private WebElement tmobile21800;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) div.panel-collapse.collapse.anchorID-285284 div.panel-body p a:nth-of-type(2)")
	private WebElement tmobile31800;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) div.panel-collapse.collapse.anchorID-549957 div.panel-body p a:nth-of-type(2)")
	private WebElement tmobile41800;

	@FindBy(css = "a[href='https://business.t-mobile.com']")
	private WebElement tmobileForBusiness;

	@FindBy(css = "a[href='http://www.t-mobile.com/cell-phone-trade-in.html']")
	private WebElement tradeInProgram;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) div.panel-collapse.collapse.anchorID-720032 div.panel-body p a:nth-of-type(1)")
	private WebElement transferYourContent1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) div.panel-collapse.collapse.anchorID-720032 div.panel-body p a:nth-of-type(1)")
	private WebElement transferYourContent2;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "a[href='http://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_ReturnPolicy&print=true']")
	private WebElement viewReturnPolicy;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) div.panel-collapse.collapse.anchorID-285284 div.panel-body p a:nth-of-type(3)")
	private WebElement visitYourNearestTmobileStore1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-collapse.collapse.anchorID-594920 div.panel-body p a:nth-of-type(2)")
	private WebElement visitYourNearestTmobileStore2;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) div.panel-collapse.collapse.anchorID-285284 div.panel-body p a:nth-of-type(3)")
	private WebElement visitYourNearestTmobileStore3;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) div.panel-collapse.collapse.anchorID-594920 div.panel-body p a:nth-of-type(2)")
	private WebElement visitYourNearestTmobileStore4;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	private WebElement wellHelpYouJoin;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement whyShouldIJoinTmobile1;

	@FindBy(css = "#6cdefbd5f6bfcd714ea1c8b26f696ff7fa2553c0 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement whyShouldIJoinTmobile2;

	public WeWillHelpYouJoinUsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 */
	public WeWillHelpYouJoinUsPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("We will help you JoinUs page loaded");
		} catch (NoSuchElementException e) {
			Assert.fail("We will help you JoinUs Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public WeWillHelpYouJoinUsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

	/**
	 * Click on About Link.
	 */
	public WeWillHelpYouJoinUsPage clickAboutLink() {
		about.click();
		return this;
	}

	/**
	 * Click on Accessibility Link.
	 */
	public WeWillHelpYouJoinUsPage clickAccessibilityLink() {
		accessibility.click();
		return this;
	}

	/**
	 * Click on Accessories Link.
	 */
	public WeWillHelpYouJoinUsPage clickAccessoriesLink() {
		accessories.click();
		return this;
	}

	/**
	 * Click on Activate Your Prepaid Phone Or Device Link.
	 */
	public WeWillHelpYouJoinUsPage clickActivateYourPrepaidPhoneOrDeviceLink() {
		activateYourPrepaidPhoneOrDevice.click();
		return this;
	}

	/**
	 * Click on Android Link.
	 */
	public WeWillHelpYouJoinUsPage clickAndroid1Link() {
		android1.click();
		return this;
	}

	/**
	 * Click on Android Link.
	 */
	public WeWillHelpYouJoinUsPage clickAndroid2Link() {
		android2.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 */
	public WeWillHelpYouJoinUsPage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 */
	public WeWillHelpYouJoinUsPage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 */
	public WeWillHelpYouJoinUsPage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on Call 1800tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickCall1800tmobileLink() {
		call1800tmobile.click();
		return this;
	}

	/**
	 * Click on Call Us Link.
	 */
	public WeWillHelpYouJoinUsPage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Can I Join Tmobile At Any Store Location Or Online Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanIJoinTmobileAt1Button() {
		canIJoinTmobileAt1.click();
		return this;
	}

	/**
	 * Click on Can I Join Tmobile At Any Store Location Or Online Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanIJoinTmobileAt2Button() {
		canIJoinTmobileAt2.click();
		return this;
	}

	/**
	 * Click on Can I Keep My Phone When I Switch To Tmobile Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanIKeepMyPhone1Button() {
		canIKeepMyPhone1.click();
		return this;
	}

	/**
	 * Click on Can I Keep My Phone When I Switch To Tmobile Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanIKeepMyPhone2Button() {
		canIKeepMyPhone2.click();
		return this;
	}

	/**
	 * Click on Can I Switch To Tmobile But Keep The Phone Number I Already Have
	 * From Another Carrier Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanISwitchToTmobile1Button() {
		canISwitchToTmobile1.click();
		return this;
	}

	/**
	 * Click on Can I Switch To Tmobile But Keep The Phone Number I Already Have
	 * From Another Carrier Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanISwitchToTmobile2Button() {
		canISwitchToTmobile2.click();
		return this;
	}

	/**
	 * Click on Can I Trade In My Old Phone For A New One When I Switch To Tmobile
	 * Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanITradeInMy1Button() {
		canITradeInMy1.click();
		return this;
	}

	/**
	 * Click on Can I Trade In My Old Phone For A New One When I Switch To Tmobile
	 * Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanITradeInMy2Button() {
		canITradeInMy2.click();
		return this;
	}

	/**
	 * Click on Can I Transfer My Current Contacts To My New Phone Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanITransferMyCurrent1Button() {
		canITransferMyCurrent1.click();
		return this;
	}

	/**
	 * Click on Can I Transfer My Current Contacts To My New Phone Button.
	 */
	public WeWillHelpYouJoinUsPage clickCanITransferMyCurrent2Button() {
		canITransferMyCurrent2.click();
		return this;
	}

	/**
	 * Click on Careers Link.
	 */
	public WeWillHelpYouJoinUsPage clickCareersLink() {
		careers.click();
		return this;
	}

	/**
	 * Click on Check Compatibility Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckCompatibilityLink() {
		checkCompatibility.click();
		return this;
	}

	/**
	 * Click on Check Eligibility Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckEligibilityLink() {
		checkEligibility.click();
		return this;
	}

	/**
	 * Click on Check If Your Existing Number Is Eligible Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckIfYourExistingNumberIs1Link() {
		checkIfYourExistingNumberIs1.click();
		return this;
	}

	/**
	 * Click on Check If Your Existing Number Is Eligible Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckIfYourExistingNumberIs2Link() {
		checkIfYourExistingNumberIs2.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckItOutLink() {
		checkItOut.click();
		return this;
	}

	/**
	 * Click on Check Order Status Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckOrderStatusLink() {
		checkOrderStatus.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Check Tradein Value Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckTradeinValueLink() {
		checkTradeinValue.click();
		return this;
	}

	/**
	 * Click on Check Your Phones Compatibility Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckYourPhonesCompatibility1Link() {
		checkYourPhonesCompatibility1.click();
		return this;
	}

	/**
	 * Click on Check Your Phones Compatibility Link.
	 */
	public WeWillHelpYouJoinUsPage clickCheckYourPhonesCompatibility2Link() {
		checkYourPhonesCompatibility2.click();
		return this;
	}

	/**
	 * Click on Consumer Information Link.
	 */
	public WeWillHelpYouJoinUsPage clickConsumerInformationLink() {
		consumerInformation.click();
		return this;
	}

	/**
	 * Click on Contact Information Link.
	 */
	public WeWillHelpYouJoinUsPage clickContactInformationLink() {
		contactInformation.click();
		return this;
	}

	/**
	 * Click on Coverage Map Link.
	 */
	public WeWillHelpYouJoinUsPage clickCoverageMap1Link() {
		coverageMap1.click();
		return this;
	}

	/**
	 * Click on Coverage Map Link.
	 */
	public WeWillHelpYouJoinUsPage clickCoverageMap2Link() {
		coverageMap2.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 */
	public WeWillHelpYouJoinUsPage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 */
	public WeWillHelpYouJoinUsPage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Deutsche Telekom Link.
	 */
	public WeWillHelpYouJoinUsPage clickDeutscheTelekomLink() {
		deutscheTelekom.click();
		return this;
	}

	/**
	 * Click on Device Support Link.
	 */
	public WeWillHelpYouJoinUsPage clickDeviceSupportLink() {
		deviceSupport.click();
		return this;
	}

	/**
	 * Click on Device Tradein Link.
	 */
	public WeWillHelpYouJoinUsPage clickDeviceTradein1Link() {
		deviceTradein1.click();
		return this;
	}

	/**
	 * Click on Device Tradein Link.
	 */
	public WeWillHelpYouJoinUsPage clickDeviceTradein2Link() {
		deviceTradein2.click();
		return this;
	}

	/**
	 * Click on Espanol Link.
	 */
	public WeWillHelpYouJoinUsPage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Espaol Link.
	 */
	public WeWillHelpYouJoinUsPage clickEspaolEspaolLink() {
		espaolEspaol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.
	 */
	public WeWillHelpYouJoinUsPage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 */
	public WeWillHelpYouJoinUsPage clickFindAStore1Link() {
		findAStore1.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 */
	public WeWillHelpYouJoinUsPage clickFindAStore2Link() {
		findAStore2.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 */
	public WeWillHelpYouJoinUsPage clickFindAStore3Link() {
		findAStore3.click();
		return this;
	}

	/**
	 * Click on Find Out How Link.
	 */
	public WeWillHelpYouJoinUsPage clickFindOutHowLink() {
		findOutHow.click();
		return this;
	}

	/**
	 * Click on Get A Rebate Link.
	 */
	public WeWillHelpYouJoinUsPage clickGetARebateLink() {
		getARebate.click();
		return this;
	}

	/**
	 * Click on How Do I Know If Ill Have Coverage In My Area Button.
	 */
	public WeWillHelpYouJoinUsPage clickHowDoIKnowIf1Button() {
		howDoIKnowIf1.click();
		return this;
	}

	/**
	 * Click on How Do I Know If Ill Have Coverage In My Area Button.
	 */
	public WeWillHelpYouJoinUsPage clickHowDoIKnowIf2Button() {
		howDoIKnowIf2.click();
		return this;
	}

	/**
	 * Click on If I Switch Will Tmobile Pay My Early Termination Fees Button.
	 */
	public WeWillHelpYouJoinUsPage clickIfISwitchWillTmobile1Button() {
		ifISwitchWillTmobile1.click();
		return this;
	}

	/**
	 * Click on If I Switch Will Tmobile Pay My Early Termination Fees Button.
	 */
	public WeWillHelpYouJoinUsPage clickIfISwitchWillTmobile2Button() {
		ifISwitchWillTmobile2.click();
		return this;
	}

	/**
	 * Click on Interestbased Ads Link.
	 */
	public WeWillHelpYouJoinUsPage clickInterestbasedAdsLink() {
		interestbasedAds.click();
		return this;
	}

	/**
	 * Click on International Calling Link.
	 */
	public WeWillHelpYouJoinUsPage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on International Rates Link.
	 */
	public WeWillHelpYouJoinUsPage clickInternationalRatesLink() {
		internationalRates.click();
		return this;
	}

	/**
	 * Click on Internet Of Things Link.
	 */
	public WeWillHelpYouJoinUsPage clickInternetOfThingsLink() {
		internetOfThings.click();
		return this;
	}

	/**
	 * Click on Investor Relations Link.
	 */
	public WeWillHelpYouJoinUsPage clickInvestorRelationsLink() {
		investorRelations.click();
		return this;
	}

	/**
	 * Click on Ios Link.
	 */
	public WeWillHelpYouJoinUsPage clickIos1Link() {
		ios1.click();
		return this;
	}

	/**
	 * Click on Ios Link.
	 */
	public WeWillHelpYouJoinUsPage clickIos2Link() {
		ios2.click();
		return this;
	}

	/**
	 * Click on Learn More Link.
	 */
	public WeWillHelpYouJoinUsPage clickLearnMore1Link() {
		learnMore1.click();
		return this;
	}

	/**
	 * Click on Learn More Link.
	 */
	public WeWillHelpYouJoinUsPage clickLearnMore2Link() {
		learnMore2.click();
		return this;
	}

	/**
	 * Click on Learn More About The Perks Of Joining Tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickLearnMoreAboutThePerksOf1Link() {
		learnMoreAboutThePerksOf1.click();
		return this;
	}

	/**
	 * Click on Learn More About The Perks Of Joining Tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickLearnMoreAboutThePerksOf2Link() {
		learnMoreAboutThePerksOf2.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 */
	public WeWillHelpYouJoinUsPage clickLogInLink() {
		logIn.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 */
	public WeWillHelpYouJoinUsPage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickMyTmobileLink() {
		myTmobile.click();
		return this;
	}

	/**
	 * Click on Online Link.
	 */
	public WeWillHelpYouJoinUsPage clickOnline1Link() {
		online1.click();
		return this;
	}

	/**
	 * Click on Online Link.
	 */
	public WeWillHelpYouJoinUsPage clickOnline2Link() {
		online2.click();
		return this;
	}

	/**
	 * Click on Open Internet Link.
	 */
	public WeWillHelpYouJoinUsPage clickOpenInternetLink() {
		openInternet.click();
		return this;
	}

	/**
	 * Click on Phones Link.
	 */
	public WeWillHelpYouJoinUsPage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click on Plans Link.
	 */
	public WeWillHelpYouJoinUsPage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Plans Services Link.
	 */
	public WeWillHelpYouJoinUsPage clickPlansServicesLink() {
		plansServices.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 */
	public WeWillHelpYouJoinUsPage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 */
	public WeWillHelpYouJoinUsPage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Press Link.
	 */
	public WeWillHelpYouJoinUsPage clickPressLink() {
		press.click();
		return this;
	}

	/**
	 * Click on Privacy Center Link.
	 */
	public WeWillHelpYouJoinUsPage clickPrivacyCenterLink() {
		privacyCenter.click();
		return this;
	}

	/**
	 * Click on Privacy Statement Link.
	 */
	public WeWillHelpYouJoinUsPage clickPrivacyStatementLink() {
		privacyStatement.click();
		return this;
	}

	/**
	 * Click on Public Safety911 Link.
	 */
	public WeWillHelpYouJoinUsPage clickPublicSafety911Link() {
		publicSafety911.click();
		return this;
	}

	/**
	 * Click on Puerto Rico Link.
	 */
	public WeWillHelpYouJoinUsPage clickPuertoRicoLink() {
		puertoRico.click();
		return this;
	}

	/**
	 * Click on Questions About Your Bill Link.
	 */
	public WeWillHelpYouJoinUsPage clickQuestionsAboutYourBillLink() {
		questionsAboutYourBill.click();
		return this;
	}

	/**
	 * Click on Refill Your Prepaid Account Link.
	 */
	public WeWillHelpYouJoinUsPage clickRefillYourPrepaidAccountLink() {
		refillYourPrepaidAccount.click();
		return this;
	}

	/**
	 * Click on Scroll To Top Button.
	 */
	public WeWillHelpYouJoinUsPage clickScrollToTopButton() {
		scrollToTop.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 */
	public WeWillHelpYouJoinUsPage clickSeeFullTerms1Link() {
		seeFullTerms1.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 */
	public WeWillHelpYouJoinUsPage clickSeeFullTerms2Link() {
		seeFullTerms2.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 */
	public WeWillHelpYouJoinUsPage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 */
	public WeWillHelpYouJoinUsPage clickShopPhones1Link() {
		shopPhones1.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 */
	public WeWillHelpYouJoinUsPage clickShopPhones2Link() {
		shopPhones2.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 */
	public WeWillHelpYouJoinUsPage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 */
	public WeWillHelpYouJoinUsPage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 */
	public WeWillHelpYouJoinUsPage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Start Online Link.
	 */
	public WeWillHelpYouJoinUsPage clickStartOnlineLink() {
		startOnline.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 */
	public WeWillHelpYouJoinUsPage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 */
	public WeWillHelpYouJoinUsPage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Studentteacher Discount Link.
	 */
	public WeWillHelpYouJoinUsPage clickStudentteacherDiscountLink() {
		studentteacherDiscount.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 */
	public WeWillHelpYouJoinUsPage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 */
	public WeWillHelpYouJoinUsPage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Support Home Link.
	 */
	public WeWillHelpYouJoinUsPage clickSupportHomeLink() {
		supportHome.click();
		return this;
	}

	/**
	 * Click on Switch2tmobile.com Link.
	 */
	public WeWillHelpYouJoinUsPage clickSwitch2tmobileCom1Link() {
		switch2tmobileCom1.click();
		return this;
	}

	/**
	 * Click on Switch2tmobile.com Link.
	 */
	public WeWillHelpYouJoinUsPage clickSwitch2tmobileCom2Link() {
		switch2tmobileCom2.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 */
	public WeWillHelpYouJoinUsPage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Terms Of Use Link.
	 */
	public WeWillHelpYouJoinUsPage clickTermsOfUseLink() {
		termsOfUse.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickTmobile1Link1800() {
		tmobile11800.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickTmobile2Link1800() {
		tmobile21800.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickTmobile3Link1800() {
		tmobile31800.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 */
	public WeWillHelpYouJoinUsPage clickTmobile4Link1800() {
		tmobile41800.click();
		return this;
	}

	/**
	 * Click on Tmobile For Business Link.
	 */
	public WeWillHelpYouJoinUsPage clickTmobileForBusinessLink() {
		tmobileForBusiness.click();
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 */
	public WeWillHelpYouJoinUsPage clickTradeInProgramLink() {
		checkPageIsReady();
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		je.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		tradeInProgram.isDisplayed();
		tradeInProgram.click();
		Reporter.log("Verified and Clicked on Trade in program link");
		return this;
	}

	/**
	 * Click on Transfer Your Content Link.
	 */
	public WeWillHelpYouJoinUsPage clickTransferYourContent1Link() {
		transferYourContent1.click();
		return this;
	}

	/**
	 * Click on Transfer Your Content Link.
	 */
	public WeWillHelpYouJoinUsPage clickTransferYourContent2Link() {
		transferYourContent2.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 */
	public WeWillHelpYouJoinUsPage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 */
	public WeWillHelpYouJoinUsPage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 */
	public WeWillHelpYouJoinUsPage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on View Return Policy Link.
	 */
	public WeWillHelpYouJoinUsPage clickViewReturnPolicyLink() {
		viewReturnPolicy.click();
		return this;
	}

	/**
	 * Click on Visit Your Nearest Tmobile Store Link.
	 *
	 */
	public WeWillHelpYouJoinUsPage clickVisitYourNearestTmobileStore1Link() {
		visitYourNearestTmobileStore1.click();
		return this;
	}

	/**
	 * Click on Visit Your Nearest Tmobile Store Link.
	 */
	public WeWillHelpYouJoinUsPage clickVisitYourNearestTmobileStore2Link() {
		visitYourNearestTmobileStore2.click();
		return this;
	}

	/**
	 * Click on Visit Your Nearest Tmobile Store Link.
	 */
	public WeWillHelpYouJoinUsPage clickVisitYourNearestTmobileStore3Link() {
		visitYourNearestTmobileStore3.click();
		return this;
	}

	/**
	 * Click on Visit Your Nearest Tmobile Store Link.
	 */
	public WeWillHelpYouJoinUsPage clickVisitYourNearestTmobileStore4Link() {
		visitYourNearestTmobileStore4.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 */
	public WeWillHelpYouJoinUsPage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 */
	public WeWillHelpYouJoinUsPage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Click on Why Should I Join Tmobile Button.
	 */
	public WeWillHelpYouJoinUsPage clickWhyShouldIJoinTmobile1Button() {
		whyShouldIJoinTmobile1.click();
		return this;
	}

	/**
	 * Click on Why Should I Join Tmobile Button.
	 */
	public WeWillHelpYouJoinUsPage clickWhyShouldIJoinTmobile2Button() {
		whyShouldIJoinTmobile2.click();
		return this;
	}

	/**
	 * Set value to Search Text field.
	 */
	public WeWillHelpYouJoinUsPage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set value to Search Text field.
	 */
	public WeWillHelpYouJoinUsPage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}

}