package com.tmobile.eservices.qa.pages.global.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSCustomerAccount extends EOSCommonLib {

	public Response getCustomerAccountDetails(String alist[]) throws Exception {
		Response response = null;
		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("Accept", "application/json");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("Content-Type", "application/json");
			restService.addHeader("access_token", alist[1]);
			restService.addHeader("application_id", "MYTMO");
			restService.addHeader("ban", alist[3]);
			restService.addHeader("cache-control", "no-cache");
			restService.addHeader("channel_id", "Desktop");
			restService.addHeader("msisdn", alist[0]);
			restService.addHeader("usn", "dummyUSN");
			restService.addHeader("X-B3-TraceId", "FlexAPITest123");
			restService.addHeader("X-B3-SpanId", "FlexAPITest456");
			restService.addHeader("transactionid", "FlexAPITest");
			restService.addHeader("msisdnList", alist[0]);

			response = restService.callService("v1/subscriber/account/", RestCallType.GET);

		}

		return response;
	}

	/*public String getDeviceUpgradeEligible(Response response) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("myLine.deviceUpgrade.eligible") != null) {
				return jsonPathEvaluator.get("myLine.deviceUpgrade.eligible").toString();
			}
		}

		return null;

	}*/
	
	public Map<String, String> getDeviceUpgradeEligible(Response response) {

		Map<String, String> upgradeMap = new HashMap<String, String>();
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("myLine.deviceUpgrade.eligible") != null) {
				upgradeMap.put("eligible", jsonPathEvaluator.get("myLine.deviceUpgrade.eligible").toString());
			}
			if (jsonPathEvaluator.get("myLine.deviceUpgrade.programType") != null) {
				upgradeMap.put("programType", jsonPathEvaluator.get("myLine.deviceUpgrade.programType").toString());
			}
		}

		return upgradeMap;

	}

}
