package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AutopayManagementApi extends ApiCommonLib{
	
	/***
	 * Summary: This API is used to setup an Autopay through Managepayment API
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 *  -Content-Type
	 *  -activityId 
	 *  -application_userid 
	 *  
	 *  
	 * @return
	 * @throws Exception
	 */
    public Response autopay_Setup(ApiTestData apiTestData, String requestBody) throws Exception{
		Map<String, String> headers = buildAutopayHeader(apiTestData);
    	String resourceURL = "payments/v3/autopay";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
    }
    
    /***
	 * Summary: This API is used to setup an Autopay through Managepayment API
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 *  -Content-Type
	 *  -activityId 
	 *  -application_userid 
	 *  
	 *  
	 * @return
	 * @throws Exception
	 */
    public Response setAutopay(ApiTestData apiTestData, String requestBody) throws Exception{
		Map<String, String> headers = buildAutopayHeader(apiTestData);
		String resourceURL = "autopay/v3";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
    }
    
    /***
	 * Summary: This API is used to setup an Autopay through Managepayment API
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 *  -Content-Type
	 *  -activityId 
	 *  -application_userid 
	 *  
	 *  
	 * @return
	 * @throws Exception
	 */
    public Response updateAutopay(ApiTestData apiTestData, String requestBody) throws Exception{
		Map<String, String> headers = buildAutopayHeader(apiTestData);
		String resourceURL = "payments/v3/autopay";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		return response;
    }
   
    
    /***
	 * Summary: This API is used to setup an Autopay through Managepayment API
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 *  -Content-Type
	 *  -activityId 
	 *  -application_userid 
	 *  
	 *  
	 * @return
	 * @throws Exception
	 */
    public Response deleteAutopay(ApiTestData apiTestData, String requestBody) throws Exception{
		Map<String, String> headers = buildAutopayHeader(apiTestData);
		String resourceURL = "payments/v3/autopay";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.DELETE);
		return response;
    }
    
    /***
   	 * Summary: This API is used to setup an Autopay through Managepayment API
   	 * Headers:Authorization,
   	 * 	-transactionId,
   	 * 	-transactionType,
   	 * 	-applicationId,
   	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
   	 * 	-correlationId, 
   	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
   	 * 	-transactionBusinessKey,
   	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
   	 * 	-usn,required=true
   	 * 	-phoneNumber,required=true
   	 *  -Content-Type
   	 *  -activityId 
   	 *  -application_userid 
   	 *  
   	 *  
   	 * @return
   	 * @throws Exception
   	 */
	public Response autopaySearch(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAutopayHeader(apiTestData);
		String resourceURL = "payments/v3/autopay";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
		return response;
	}
    
    private Map<String, String> buildAutopayHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("Content-Type", "application/json");
		headers.put("channel_id", "B2B_WEB");
		headers.put("application_id", "TFB");
		headers.put("activityid", "123");
		headers.put("application_userid", "123");
		return headers;
	}

}
