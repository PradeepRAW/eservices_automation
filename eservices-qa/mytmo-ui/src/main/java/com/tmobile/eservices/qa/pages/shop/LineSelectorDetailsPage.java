package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class LineSelectorDetailsPage extends CommonPage {

	public static final String LINE_SELECTION_DETAILS_URL = "lineselector#details";
	// public static final String LINE_SELECTION_DETAILS_URL = "phoneselection";

	@FindBy(css = "#lineSelectorDetails-text h4")
	private WebElement tradeInMessage;

	@FindBy(css = "a[ng-click*='ctrl.goToback()']")
	private WebElement back;

	@FindBy(css = "#lineSelectorDetails-text p[class*='description']")
	private WebElement tradeInDescription;

	@FindBy(css = "a[ng-if*='showLearnMoreLink']")
	private WebElement tradeInWorksLink;

	@FindBy(css = "h4[ng-bind-html*='modalDetail.header']")
	private WebElement tradeInPopUpHeader;

	@FindBy(css = "span[ng-bind-html*='bannerBigTxt']")
	private WebElement promoOfferPopUpHeader;

	@FindBy(css = "div[ng-repeat*='expandedLineDetails.modalDetail.description']")
	private List<WebElement> tradeInPopUpDescription;

	@FindBy(css = "i.ico-closeIcon")
	private WebElement closePopUp;

	@FindBy(css = "#object-0 img")
	private WebElement imageDevice;

	@FindBy(css = "p[pid*='cust_msisdn']")
	private List <WebElement> lineNumber;

	@FindBy(css = "p[pid*='cust_name']")
	private List <WebElement> lineName;

	@FindBy(css = "#object-0 span[ng-bind*='line.deviceDetail.deviceName']")
	private WebElement deviceName;

	/*
	 * @FindBy(css = ".pull-left.ico-backArrowIcon2") private WebElement
	 * backButton;
	 */
	@FindBy(css = "button[ng-bind*='tradeinAnotherPhone']")
	private WebElement buttonTradeInDifferentPhone;

	@FindBy(css = "button[ng-click*='ctrl.onClickTradein()']")
	private List<WebElement> yesGetStartedButton;

	@FindBy(css = "button[ng-bind*='ctrl.contentData.skipTradeinCTATxt']")
	// @FindBy(xpath="//a[contains(.,'Skip trade-in')]")
	private WebElement skipTradeInCTA;

	@FindBy(css = "[ng-bind-html*='ctrl.expandedLineDetails.message']") //
	private WebElement inelligibleMessage;

	@FindBy(css = "[ng-bind-html*='ctrl.expandedLineDetails.description']") //
	private WebElement inelligibleMessageDescription;

	@FindBy(css = "#object-1 div.no-padding p.Display5")
	private WebElement secondLine;

	@FindBy(css = "[ng-click*='ctrl.showLineDetails(line)'][style*='display: none']")
	private WebElement remainingLineNotDisplay;	

	@FindBy(css = "[ng-click*='ctrl.onClickContactUs()']")
	private WebElement contactUS;

	@FindBy(css = "[ng-click*='ctrl.onClickStoreLocator()']")
	private WebElement visitAStore;

	@FindBy(css = "[ng-click*='ctrl.onClickStoreLocator()']")
	private WebElement hoeItWorksDescription;

	@FindBy(css = "lines")
	private WebElement lines;

	@FindBy(linkText = "tradeInPromoLink")
	private WebElement tradeInPromoLink;

	@FindBy(css = "a[ng-bind-html*='promoDetailsData']")
	private WebElement promoOfferLink;

	@FindBy(linkText = "offerDetailModel")
	private WebElement offerDetailModel;

	@FindBy(css = "#lineSelectorDetails-text p[class*='description'] ul")
	private WebElement tradeInPromoDescription;

	@FindBy(css = "[ng-click*='ctrl.showLearnTradeinModal()']")
	private WebElement howItWorksLink;

	@FindBy(css = "[ng-bind*='$ctrl.contentData.contactUsCTATxt']")
	private WebElement contactUsButton;

	@FindBy(css = "[ng-bind*='ctrl.contentData.storeLocatorCTATxt']")
	private WebElement visitStoreButton;

	@FindBy(css = "[ng-bind-html*='unknownDevice']")
	private WebElement unknownDeviceHeader;

	@FindBy(css = "[ng-bind*='noDeviceTradeinAnotherCTATxt']")
	private WebElement phoneValueCta;

	@FindBy(css = "EIP NOT PAID Line Description")
	private WebElement jumpEIPNotPaidLineDescription;

	@FindBy(css = "creditAmount")
	private WebElement eipBalance;

	@FindBy(css = "eipBalance")
	private WebElement creditAmount;

	@FindBy(css = "promoAmount")
	private WebElement promoAmount;

	/*
	 * @FindBy(css = "[ng-click*='ClickTradein']") private WebElement
	 * tradeInThisPhone;
	 */	

	@FindBy(css = "h4[ng-bind-html*='$ctrl.expandedLineDetails.message']")
	private WebElement orderMessage;

	@FindBy(css = "button[ng-bind='$ctrl.contentData.contactUsCTATxt']")
	private WebElement callUSButton;

	@FindBy(css = "button[ng-bind='$ctrl.contentData.storeLocatorCTATxt']")
	private WebElement visitAStoreButton;

	// =============
	// =============
	public static final String SkipTradeIn = "SkipTradeIn";
	public static final String SelectDevice = "SelectDevice";
	public static final String DifferentPhone = "DifferentPhone";

	@FindBy(css = "div.phoneSelection h3.h3-title")
	private WebElement phoneSelectionPage;

	@FindBy(css = "p.body-copy-highlight")
	private WebElement selectDevice;

	@FindBy(css = "p[ng-bind*='tradeinAnotherPhone']")
	private WebElement wantTradeInDifferentPhone;

	@FindBy(xpath = "//button[contains(@ng-click,'onClickSkipTradein')]")
	private WebElement skipTradeIn;

	@FindBy(css = "p.h4-title")
	private WebElement paidOffText;

	@FindBy(css = "div.text-center.header-box-shadow.ng-scope")
	private WebElement sedonaHeader;

	@FindBy(css = "fa.fa-spin.fa-spinner.interceptor-spinner")
	private WebElement pageSpinner;

	@FindBy(css = "i.ico-backArrowIcon2")
	private WebElement backButton;

	@FindBy(xpath = "//button[contains(@ng-click,'onClickTradein')]")
	private WebElement tradeInThisPhone;

	@FindBy(css = "p[ng-bind-html*='ctrl.expandedLineDetails.description'] ul li")
	private List<WebElement> messageDescription;
	
	@FindBy(css = "p[ng-if*='customerName']")
	private List<WebElement> lineNameTextPII;

	public LineSelectorDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Line Selector Page
	 *
	 * @return
	 */
	public LineSelectorDetailsPage verifyLineSelectorDetailsPage() {
		checkPageIsReady();
		waitforSpinner();
		try {
			verifyPageUrl();
			Reporter.log("Line selector deatils page displayed");
		} catch (Exception e) {
			Assert.fail("Line selector deatils page not displayed " + e.getMessage());
		}
		return this;
	}

	/**
	 * verify Trade In Message is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInMessage() {
		waitforSpinner();
		try {
			Assert.assertTrue(tradeInMessage.getText().contains("want to trade in this phone"),
					"Trade in Message is not dispalyed");
			Reporter.log("Trade in Message is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail(
					"Failed to verify Want to trade in this phone? You'll get $** Message in line Selector Details Page");

		}
		return this;
	}

	/**
	 * verify Trade In Value with dollar sign is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInValueWithDollarSymbol() {
		waitforSpinner();
		try {
			Assert.assertTrue(tradeInMessage.getText().contains("$"), "Trade in Value $ sign is not displayed");
			Reporter.log("Trade in Value with price and $ sign is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in Value $ sign in line Selector Details Page");

		}
		return this;
	}

	/**
	 * verify Learn How trade in works link
	 */
	public LineSelectorDetailsPage verifyLearnHowTradeInWorksLink() {
		try {
			Assert.assertTrue(tradeInWorksLink.isDisplayed(), "Learn How trade In works link is not displayed");
			Reporter.log("Learn How trade In works link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Learn How trade In works link display");
		}
		return this;
	}

	/**
	 * verify promo offer link is displayed
	 */
	public LineSelectorDetailsPage verifyPromoOfferLinkdisplay() {
		try {
			Assert.assertTrue(promoOfferLink.isDisplayed(), "Promo Offer link is not displayed");
			Reporter.log("Promo Offer link is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo offer link display");
		}
		return this;
	}

	/**
	 * click promo offer link
	 */
	public LineSelectorDetailsPage clickPromoOfferLink() {
		try {
			clickElementWithJavaScript(promoOfferLink);
			Reporter.log("Clicked on Promo Offer link on Line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Promo Offer link is not clickable");
		}
		return this;
	}

	/**
	 * Click on Trade in works Link
	 */
	public LineSelectorDetailsPage clickTradeInWorksLink() {
		try {
			clickElementWithJavaScript(tradeInWorksLink);
			waitforSpinner();
			Reporter.log("Clicked on Trade in link works on Line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Trade In works link is not clickable");
		}
		return this;
	}

	/**
	 * verify Trade In Works pop up header is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInWorksPopUpHeader() {
		checkPageIsReady();
		waitforSpinner();
		try {
			Assert.assertTrue(tradeInPopUpHeader.isDisplayed(), "Trade in works Pop Up header is not dispalyed");
			Reporter.log("Trade in works Pop Up header is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in works Pop Up header in line Selector Details Page");

		}
		return this;
	}

	/**
	 * verify Promo offer pop up
	 */
	public LineSelectorDetailsPage verifyPromoOfferPopUpHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(promoOfferPopUpHeader));
			Assert.assertTrue(promoOfferPopUpHeader.isDisplayed(), "Promo Offer Pop Up header is not dispalyed");
			Reporter.log("Promo Offer Pop Up header is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo offer Pop Up header in line Selector Details Page");

		}
		return this;
	}

	/**
	 * verify Trade In Works pop up description is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInWorksPopUpDescription() {
		try {
			for (WebElement description : tradeInPopUpDescription)
				Assert.assertTrue(description.isDisplayed(), "Trade in works Pop Up description is not dispalyed");
			Reporter.log("Trade in works Pop Up description is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in works Pop Up description in line Selector Details Page");

		}
		return this;
	}

	/**
	 * Click on close Trade in works pop up
	 */
	public LineSelectorDetailsPage clickCloseTradeInWorks() {
		try {
			clickElementWithJavaScript(closePopUp);
			Reporter.log("Clicked on close Trade in link works pop up on Line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Trade In works pop up close button is not clickable");
		}
		return this;
	}

	/**
	 * Verify device image is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyLineDeviceImageIsDisplayed() {
		try {
			Assert.assertTrue(imageDevice.isDisplayed(), "Line Device image is not displayed");
			Reporter.log("Line Device image is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Line Device image");
		}
		return this;
	}

	/**
	 * Verify line number is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyLineNumberIsDisplayed() {
		try {
			Assert.assertTrue(lineNumber.get(0).isDisplayed(), "Line number is not displayed");
			Reporter.log("Line number is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Line number");
		}
		return this;
	}

	/**
	 * Verify line name is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyLineNameIsDisplayed() {
		try {
			for (WebElement name : lineName) {
				if(isElementDisplayed(name)) {
					Reporter.log("Line name is displayed");
					break;
				}
			}
//			Assert.assertTrue(lineName.isDisplayed(), "Line name is not displayed");
//			Reporter.log("Line name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Line name");
		}
		return this;
	}

	/**
	 * Verify device name is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyDeviceNameIsDisplayed() {
		try {
			Assert.assertTrue(deviceName.isDisplayed(), "Device name is not displayed");
			Reporter.log("Device name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Device name");
		}
		return this;
	}

	/**
	 * Click on Back Button
	 */
	public LineSelectorDetailsPage clickBackButton() {
		try {
			clickElementWithJavaScript(backButton);
			Reporter.log("Clicked on Back Button of Line Selector Details Page");
		} catch (Exception e) {
			Assert.fail("Failed to click on Back Button.");
		}
		return this;
	}

	/**
	 * Verify 'Want to trade in a different phone?' link display
	 */
	public LineSelectorDetailsPage verifyWantToTradeDifferentPhoneLinkDisplay() {
		try {
			Assert.assertTrue(wantTradeInDifferentPhone.isDisplayed(),
					"Link text is not present in Phone selection page to verify");
			Assert.assertTrue(
					wantTradeInDifferentPhone.getText().equalsIgnoreCase("Want to trade in a different phone?"),
					"Link text message is not displayed as 'Want to trade in a different phone?'");
			Reporter.log("Trade In Different Phone Link is displayed and text is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Link text");
		}
		return this;
	}

	/**
	 * Click 'Want to trade in a different phone?' link
	 */
	public LineSelectorDetailsPage clickWantToTradeDifferentPhoneLink() {
		try {
			waitforSpinner();
			if (isElementDisplayed(wantTradeInDifferentPhone)) {
				clickElementWithJavaScript(wantTradeInDifferentPhone);
				Reporter.log("Trade In Different Phone link is clicked");
			} else {
				clickElementWithJavaScript(buttonTradeInDifferentPhone);
				Reporter.log("Trade In Different Phone Button is clicked");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Trade In different Phone Link");
		}
		return this;
	}

	/**
	 * Click trade in this phone cta
	 */
	public LineSelectorDetailsPage clickTradeInThisPhoneCTA() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(tradeInThisPhone);
		} catch (Exception e) {
			Assert.fail("Click on trade in this phone cta failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Click on Back link
	 */
	public LineSelectorDetailsPage clickOnBackLink() {
		try {
			Assert.assertTrue(back.isDisplayed(), "Back link is not displayed");
			clickElementWithJavaScript(back);
			Reporter.log("Clicked on Back link");
		} catch (Exception e) {
			Assert.fail("Failed to click on Back Link");
		}
		return this;
	}

	/**
	 * Click on skip Trade In CTA
	 */
	public LineSelectorDetailsPage clickSkipTradeInCTA() {
		try {
			if (isElementDisplayed(skipTradeInCTA)) {
				clickElementWithJavaScript(skipTradeInCTA);
			} else {
				clickElementWithJavaScript(skipTradeIn);
			}
			Reporter.log("Clicked on Skip Trade In Button of Line Selector Details Page");
		} catch (Exception e) {
			Assert.fail("Failed to click on Skip Trade In Button.");
		}
		return this;
	}

	/**
	 * Click on Skip Trade In
	 */
	public LineSelectorDetailsPage verifySkipTradeINText() {
		try {
			Assert.assertTrue(skipTradeInCTA.isDisplayed(), "'No I changed my mind' is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display 'No I changed my mind'");
		}
		return this;
	}

	/**
	 * verify Trade In description is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInDescription() {
		try {
			waitforSpinner();
			Assert.assertTrue(
					tradeInDescription.getText()
							.contains("We'll add the remaining $142.40 owed on this phone to your cart."),
					"Trade in Bullet Description is not dispalyed");
			Reporter.log("Trade in Bullet Description is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in bullet Description in line Selector Details Page");

		}
		return this;
	}

	/*
	 * Assert.assertFalse(linkTradeInDifferentPhone.isEnabled(),
	 * "Link text is present in page"); Reporter.log(
	 * "Trade in a different phone link is not displayed");
	 * if(linkTradeInDifferentPhone.isDisplayed()== true){ Reporter.log(
	 * "Trade in a different phone link is displayed"); } else{ Reporter.log(
	 * "Trade in a different phone link is not displayed");
	 * clickSkipTradeInCTA(); }
	 * //Assert.assertFalse(linkLineTradeInDifferentPhone.isEnabled(),
	 * "Link text is present in page"); //Reporter.log(
	 * "Trade in a different phone link is not displayed"); } catch (Exception
	 * e){ Assert.fail("Failed to verify Trade in a different phone link"); }
	 * return this; }
	 */

	/**
	 * Verify trade-in ineligibility message is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyIneligibilityMessageIsDisplayed() {
		try {
			Assert.assertTrue(inelligibleMessage.isDisplayed(), "Ineligibility message is not displayed");
			Reporter.log("Ineligibility message is displayed");
		} catch (Exception e) {
			Assert.fail("Ineligibility message is Device name");
		}
		return this;
	}

	/**
	 * Verify trade-in ineligibility Description is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyIneligibilityDescriptionIsDisplayed() {
		try {
			Assert.assertTrue(inelligibleMessageDescription.isDisplayed(),
					"Ineligibility Description  is not displayed");
			Reporter.log("Ineligibility Description is displayed");
		} catch (Exception e) {
			Assert.fail("Ineligibility Description is Device name");
		}
		return this;
	}

	/**
	 * Verify Remaining Line Not Displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyRemainingLineNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(remainingLineNotDisplay.isDisplayed(), "Remaining Line Not Displayed is displayed");
			Reporter.log("Remaining Line is not displayed");
		} catch (Exception e) {
			Assert.fail("Remaining Line is displayed");
		}
		return this;
	}

	/**
	 * Verify Trade In
	 */
	public LineSelectorDetailsPage verifyTradeINButtonText() {
		try {
			Assert.assertTrue(yesGetStartedButton.get(0).isDisplayed(),
					"'Continue trade in this phone' is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display 'Continue trade in this phone'");
		}
		return this;
	}

	/**
	 * Click on Trade In CTA
	 */
	public LineSelectorDetailsPage clickTradeInCTA() {
		try {
			yesGetStartedButton.get(0).click();
			Reporter.log("Clicked on Trade In Button of Line Selector Details Page");
		} catch (Exception e) {
			Assert.fail("Failed to click on Trade In Button.");
		}
		return this;
	}

	/**
	 * verify 'Want to trade in a different phone?' link is not displayed
	 */
	public LineSelectorDetailsPage verifyTradeInDiffentPhoneLinkInvisibleAndClickSkipTradeInCTA() {
		try {
			Assert.assertTrue(wantTradeInDifferentPhone.getAttribute("class").contains("ng-hide"),
					"Link text is present in page");
			Reporter.log("Trade in a different phone link is not displayed");
			clickSkipTradeInCTA();
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in a different phone link");
		}
		return this;
	}

	/**
	 * verify 'Want to trade in a different phone?' link is not displayed
	 */
	public boolean verifyYesGetStartedIfItsPresent() {
		boolean presenceOfCta = false;

		if (CollectionUtils.isNotEmpty(yesGetStartedButton) && !CollectionUtils.sizeIsEmpty(yesGetStartedButton)) {
			presenceOfCta = true;
			Reporter.log("'Yes, lets get started' CTA is not present");
		}
		return presenceOfCta;
	}

	/**
	 * Verify Jod Message
	 */
	public LineSelectorDetailsPage verifyJodMessage() {
		try {
			Assert.assertTrue(tradeInMessage.isDisplayed(), "JOD message is not displayed");
			Assert.assertTrue(
					tradeInMessage.getText()
							.equals("You are almost done, let's connect you with someone to complete your order!"),
					"JOD message is not displayed");
			Reporter.log("JOD message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display JOD message");
		}
		return this;

	}

	/**
	 * Verify Eligibility message
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyEligibiltyMessage() {
		try {
			Assert.assertTrue(inelligibleMessage.isDisplayed(), "Eligibility Message is displayed");
			Assert.assertTrue(inelligibleMessage.getText().contains("$"),
					"Eligibility Message with $ amount is displayed");
		} catch (Exception e) {
			Assert.fail("Eligibility is not displayed");
		}
		return this;
	}

	/**
	 * Click on Contact US button
	 */
	public LineSelectorDetailsPage clickContactUSCTA() {
		try {
			clickElementWithJavaScript(contactUS);
			Reporter.log("Clicked on Contact US Button of Line Selector Details Page");
		} catch (Exception e) {
			Assert.fail("Failed to click on Contact US Button");
		}
		return this;
	}

	/**
	 * Click on Visit a Store button
	 */
	public LineSelectorDetailsPage clickVisitAStoreCTA() {
		try {
			clickElementWithJavaScript(visitAStore);
			Reporter.log("Clicked on Visit a Store Button of Line Selector Details Page");
		} catch (Exception e) {
			Assert.fail("Failed to click on  Visit a Store Button");
		}
		return this;
	}

	/**
	 * verify Trade In Promo Header is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInPromoHeader() {
		try {
			waitforSpinner();
			Assert.assertTrue(tradeInMessage.getText().contains("Trade in this phone to get our $"),
					"Trade in Promo Header is not dispalyed");
			Reporter.log("Trade in Promo Header  is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in Promo Header Message in line Selector Details Page");

		}
		return this;
	}

	/**
	 * verify Trade In Promo Header description is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInPromoHeaderDescription() {
		try {
			waitforSpinner();
			// Assert.assertTrue(tradeInDescription.getText().contains("Here's
			// how it works:"),"Trade in Promo Description Header is not
			// dispalyed");
			Assert.assertTrue(tradeInPromoDescription.isDisplayed(), "Trade in Promo Description is not dispalyed");
			Reporter.log("Trade in Promo Header and Description is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in Promo Header Description in line Selector Details Page");

		}
		return this;
	}

	/**
	 * verify Trade In Promo description is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInPromoDescription() {
		try {
			waitforSpinner();
			Assert.assertTrue(tradeInDescription.isDisplayed(), "Trade in Promo Description is not dispalyed");
			Assert.assertTrue(tradeInDescription.getText().contains("you owe to your order."),
					"Trade in Promo Description is not dispalyed");
			Reporter.log("Trade in Description is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in Promo Description in line Selector Details Page");

		}
		return this;
	}

	/**
	 * verify How Trade in Works is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyHowTradeInWorks() {
		try {
			waitforSpinner();
			Assert.assertTrue(howItWorksLink.isDisplayed(), "How it Works is not dispalyed");
			Assert.assertTrue(howItWorksLink.getText().contains("how trade-in works"), "How it Works is not dispalyed");
			Reporter.log("How it Works is displayed in line Selector details Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify How it Works in line Selector Details Page");

		}
		return this;
	}

	/**
	 * Verify trade-in eligibility message is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyEligibilityMessageIsDisplayed() {
		try {
			Assert.assertTrue(inelligibleMessage.isDisplayed(), "Eligibility message is not displayed");
			Reporter.log("Eligibility message is displayed");
		} catch (Exception e) {
			Assert.fail("Eligibility message is not displayed");
		}
		return this;
	}

	/**
	 * Verify trade-in Eligibility Description is displayed
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyEligibilityDescriptionIsDisplayed() {
		try {
			Assert.assertTrue(inelligibleMessageDescription.isDisplayed(), "Eligibility Description  is not displayed");
			Reporter.log("Eligibility messsage description is displayed");
		} catch (Exception e) {
			Assert.fail("Eligibility messsage description is not displayed");
		}
		return this;
	}

	/**
	 * verify Trade In Eligibility message with dollar sign is displayed
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyTradeInEligibilityMessageDollarSymbol() {
		try {
			waitforSpinner();
			Assert.assertTrue(inelligibleMessage.getText().contains("$"), "Eligible message $ sign is not displayed");
			Reporter.log("Eligible message $ sign is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Eligible message $ sign in New Line Selector Details Page");

		}
		return this;
	}

	/**
	 * Verify Jump ineligible message
	 */
	public LineSelectorDetailsPage verifyJumpIneligibleMessage() {
		try {
			Assert.assertTrue(inelligibleMessageDescription.isDisplayed(), "Jump ineligible message is not displayed");
			Assert.assertTrue(
					inelligibleMessageDescription.getText()
							.equals("You are almost done, let's connect you with someone to complete your order!"),
					"Jump ineligible message is not displayed");
			Reporter.log("Jump ineligible message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Jump ineligible message");
		}
		return this;
	}

	/**
	 * verify Contact Us button and click
	 */
	public LineSelectorDetailsPage verifyCallUsAndClick() {
		try {
			Assert.assertTrue(contactUsButton.isDisplayed(), "Contact Us button is not displayed");
			contactUsButton.click();
			Reporter.log("Contact Us button is displayed and clicked");
		} catch (Exception e) {
			Assert.fail("Failed to verify Contact Us button");
		}
		return this;
	}

	/**
	 * verify Visit Store button and click
	 * 
	 */
	public LineSelectorDetailsPage verifyAndClickVisitStoreButton() {
		try {
			Assert.assertTrue(visitStoreButton.isDisplayed(), "Visit Store button is not displayed");
			visitStoreButton.click();
			Reporter.log("Visit Store button is displayed and clicked");
		} catch (Exception e) {
			Assert.fail("Failed to verify Visit Store button");
		}
		return this;
	}

	/**
	 * Verify Unknown Device Header
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyUnknownDeviceHeader() {
		try {
			Assert.assertTrue(unknownDeviceHeader.getText().toLowerCase().contains("want to trade in this phone?"),
					"Unknown Device Header is not dipslayed");
			Reporter.log("Unknown Device Header (" + unknownDeviceHeader.getText() + ")is displayed");

		} catch (Exception e) {
			Assert.fail("Unknown Device Header Is Not Displayed");
		}
		return this;
	}

	/**
	 * Check the phone's value CTA
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyPhoneValueCTA() {
		try {
			Assert.assertTrue(phoneValueCta.getText().contains("Check the phone's value"),
					"Check the phone's value CTA is not dipslayed");
			Reporter.log("Check the phone's value CTA is displayed");

		} catch (Exception e) {
			Assert.fail("Check the phone's value CTA is not dipslayed");
		}
		return this;
	}

	/**
	 * Click on phone's value CTA
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage clickOnPhoneValueCTA() {
		try {
			clickElement(phoneValueCta);
			Reporter.log("Click on Check the phone's value CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Check the phone's value CTA is not dipslayed");
		}
		return this;
	}

	/**
	 * Get price value line details message header
	 */
	public String getLineDetailsMessageHeader() {
		waitforSpinner();
		return getPriceValue(tradeInMessage);
	}

	public LineSelectorDetailsPage verifyCompleteOrderMessage() {
		try {
			Assert.assertTrue(orderMessage.getText().toLowerCase().contains("complete your order"),
					"Complete order message is not displayed");
		} catch (Exception e) {
			Assert.fail("Complete order message is not displayed");
		}
		return this;
	}

	public LineSelectorDetailsPage clickOnCallUSButton() {
		try {
			callUSButton.click();
			Reporter.log("Clicked on call us button");
		} catch (Exception e) {
			Assert.fail("Call US button is not displayed");
		}
		return this;

	}

	public LineSelectorDetailsPage clickOnVisitAStoreButton() {
		try {
			visitAStore.click();
			Reporter.log("Clicked on visit a store button");
		} catch (Exception e) {
			Assert.fail("Visit a store button is not displayed");
		}
		return this;
	}

	/**
	 * Verify Phone Selection Page
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage verifyLineSelectionDetailsPage() {
		checkPageIsReady();
		waitforSpinner();
		try {
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Line selection details page is not Displayed");
		}

		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public LineSelectorDetailsPage verifyPageUrl() {
		checkPageIsReady();
		try {
			getDriver().getCurrentUrl().contains("lineselector");
		} catch (Exception e) {
			Assert.fail("Line selection details page is not Displayed");
		}
		return this;
	}

	/**
	 * Select Device
	 */
	public LineSelectorDetailsPage selectDevice() {
		try {
			waitFor(ExpectedConditions.visibilityOf(selectDevice));
			clickElementWithJavaScript(selectDevice);
		} catch (Exception e) {
			Assert.fail("Device is not Displayed to click");
		}
		return this;
	}

	/**
	 * Click on Want To Trade-In Different Phones
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage clickOnWantToTradeInDifferentPhone() {
		try {
			moveToElement(wantTradeInDifferentPhone);
			clickElementWithJavaScript(wantTradeInDifferentPhone);
		} catch (Exception e) {
			Assert.fail("Want to trade-in a different phone link is not displayed");
		}
		return this;
	}

	/**
	 * Click on Skip Trade-In Link
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage clickOnSkipTradeInLink() {
		checkPageIsReady();
		waitforSpinner();
		try {
			moveToElement(skipTradeIn);
			clickElement(skipTradeIn);
			Reporter.log("Clicked on Skip Trade-In cta");
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("SkipTradeIn link is not available to click");
		}
		return this;
	}

	/**
	 * Verify Skip tradein Link
	 */
	public LineSelectorDetailsPage verifySkipTradeInLink() {
		try {
			skipTradeIn.isDisplayed();
			Reporter.log("SkipTradeIn Link is displayed");
		} catch (Exception e) {
			Assert.fail("SkipTradeIn Link is not displayed");
		}
		return this;
	}

	/**
	 * Verify Want to trade in different phone link
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage veifyWantToTradeInDifferentPhone() {
		try {
			wantTradeInDifferentPhone.isDisplayed();
			Reporter.log("Want to trade in different phone link is displayed");
		} catch (Exception e) {
			Assert.fail("Want to trade in different phone link is not displayed");
		}
		return this;
	}

	/**
	 * Click On Trade-In This Phone
	 * 
	 * @return
	 */
	public LineSelectorDetailsPage clickOnTradeInThisPhone() {
		checkPageIsReady();
		try {
			clickElementWithJavaScript(tradeInThisPhone);
		} catch (Exception e) {
			Assert.fail("Trade in this phone is not available to click");
		}
		return this;
	}

	/**
	 * verify customer name removed from header
	 * 
	 * @return true/false
	 */
	public LineSelectorDetailsPage verifyCustomerNameRemovedFromHeader(String cname) {
		try {
			waitforSpinner();
			Assert.assertFalse(tradeInMessage.getText().contains(cname), "Customer name is not removed from header");
			Reporter.log("Customer name is removed from header");
		} catch (Exception e) {
			Assert.fail("Failed to verify if customer name is removed from header");

		}
		return this;
	}

	/**
	 * Verify Trade in this phone cta is disabled
	 */
	public LineSelectorDetailsPage verifyTradeInThisPhoneCTADisabled() {
		try {
			waitforSpinner();
			Assert.assertFalse(tradeInThisPhone.isEnabled(), "Trade in this phone cta is disabled");
		} catch (Exception e) {
			Assert.fail("Trade in this phone cta is not disabled" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Remaining balance message
	 */

	public LineSelectorDetailsPage verifyRemainingBalanceMessage() {
		try {
			Assert.assertTrue(messageDescription.get(0).isDisplayed(), "Remaining balance message not displayed");
			Reporter.log("Remaining balance message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Remaining balance message");
		}
		return this;
	}

	/**
	 * Verify Give Us call message
	 */

	public LineSelectorDetailsPage verifyGiveUsCallMessage() {
		try {
			Assert.assertTrue(messageDescription.get(1).isDisplayed(), "Give Us call message not displayed");
			Reporter.log("Give Us call message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Give Us call message");
		}
		return this;
	}
	
	/**
	 * Verify PID for Name
	 * 
	 * @param custNamePID
	 */
	public void verifyPIIMaskingForName(String custNamePID) {
		try {
			for (WebElement name : lineName){
				Assert.assertTrue(checkElementisPIIMasked(name, custNamePID),
						"No PII masking for Customer Name");
				Reporter.log("Verified PII masking for Name: " + name);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Name");
		}
	}
	
	/**
	 * Verify PID for Msisdn
	 * 
	 * @param custMsisdnPID
	 */
	public void verifyPIIMaskingForMsisdn(String custMsisdnPID) {
		try {
			for (WebElement number : lineNumber) {
				Assert.assertTrue(checkElementisPIIMasked(number, custMsisdnPID), "No PII masking for Customer Msisdn");
				Reporter.log("Verified PII masking for Msisdn: " + number.getText());
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Msisdn");
		}
	}

}