package com.tmobile.eservices.qa.api.exception;

public class ServiceFailureException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServiceFailureException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }	

}
