package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class SupportPage extends CommonPage {
	
	/**
	 * 
	 * @param webDriver
	 */
	public SupportPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	private static final String pageUrl = "support";

	/**
	 * Verify Legacy Support Page
	 * 
	 * @return
	 */
	public SupportPage verifySupportPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Support page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Support page not displayed");
		}
		return this;
	}
}
