package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class TravelAbroadPage extends TmngCommonPage {

	private final String pageUrl = "/travel-abroad-with-simple-global";

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	private WebElement about;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/accessibility-policy']")
	private WebElement accessibility;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	private WebElement accessories;

	@FindBy(css = "a[href='//prepaid-phones.t-mobile.com/prepaid-activate']")
	private WebElement activateYourPrepaidPhoneOrDevice;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	private WebElement business2;

	@FindBy(css = "a[href='tel:8448903653']")
	private WebElement call1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	private WebElement callUs;

	@FindBy(css = "#accordion-faqs div:nth-of-type(1) div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement canIGetDiscountsOn;

	@FindBy(css = "a[href='//www.t-mobile.com/careers']")
	private WebElement careers;

	@FindBy(css = "a[href='//www.t-mobile.com/optional-services/roaming.html?icid=WMM_TM_Q217INTERN_KU7Y3ZEX0YQ8553']")
	private WebElement checkACountry;

	@FindBy(css = "a[href='http://www.t-mobile.com/orderstatus/default.aspx']")
	private WebElement checkOrderStatus;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans']")
	private WebElement checkOutDetails;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	private WebElement checkOutTheCoverage;

	@FindBy(css = "#09ba1d59f30406cd9a9d2953a098d9cfb2bc5326 div:nth-of-type(2) button.close")
	private WebElement closeVideo;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info']")
	private WebElement consumerInformation;

	@FindBy(css = "a[href='http://www.t-mobile.com/contact-us.html']")
	private WebElement contactInformation;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	private WebElement deals2;

	@FindBy(css = "a[href='//www.telekom.com/']")
	private WebElement deutscheTelekom;

	@FindBy(css = "a[href='//support.t-mobile.com/community/phones-tablets-devices']")
	private WebElement deviceSupport;

	@FindBy(css = "#accordion-faqs div:nth-of-type(2) div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement doAllPlansIncludeGlobal;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	private WebElement espaol;

	@FindBy(css = "a.lang-toggle-wrapper")
	private WebElement espaolEspaol;

	@FindBy(css = "a[href='http://www.t-mobile.com/store-locator.html']")
	private WebElement findAStore;

	@FindBy(css = "a[href='http://www.t-mobile.com/promotions']")
	private WebElement getARebate;

	@FindBy(css = "a[href='http://support.t-mobile.com/docs/DOC-7261']")
	private WebElement here;

	@FindBy(css = "#accordion-faqs div:nth-of-type(1) div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement howCanIGetFaster;

	@FindBy(css = "#accordion-faqs div:nth-of-type(2) div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement howDoIReceiveAnd;

	@FindBy(css = "#accordion-faqs div:nth-of-type(2) div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement ifITravelOutsideThe;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx#choicesaboutadvertising']")
	private WebElement interestbasedAds;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	private WebElement internationalCalling;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global.html']")
	private WebElement internationalRates;

	@FindBy(css = "a[href='http://iot.t-mobile.com/']")
	private WebElement internetOfThings;

	@FindBy(css = "a[href='http://investor.t-mobile.com/CorporateProfile.aspx?iid=4091145']")
	private WebElement investorRelations;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_INTERNATIO_PFDT963OWX12243']")
	private WebElement joinUs;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.sticky div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	private WebElement logIn;

	@FindBy(css = "a[href='https://my.t-mobile.com?icid=WMM_TMNG_Q217HIMOMS_B1N8GUWGQKU8442']")
	private WebElement logInToMyTmobile;

	@FindBy(css = "button.hamburger.hamburger-border")
	private WebElement menuMenu;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile1;

	@FindBy(css = "a[href='https://my.t-mobile.com?icid=WMM_TMNG_Q217HIMOMS_2MQLLD04AG8436']")
	private WebElement myTmobile2;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/policies/internet-service']")
	private WebElement openInternet;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	private WebElement phones;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	private WebElement plans;

	@FindBy(css = "a[href='//support.t-mobile.com/community/plans-services']")
	private WebElement plansServices;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.sticky div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	private WebElement prepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/news']")
	private WebElement press;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/privacy']")
	private WebElement privacyCenter;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx']")
	private WebElement privacyStatement;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/safety/9-1-1']")
	private WebElement publicSafety911;

	@FindBy(css = "a[href='http://www.t-mobilepr.com/']")
	private WebElement puertoRico;

	@FindBy(css = "a[href='//support.t-mobile.com/community/billing']")
	private WebElement questionsAboutYourBill;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-refill-api-bridging']")
	private WebElement refillYourPrepaidAccount;

	@FindBy(css = "a[href='https://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true#Roaming']")
	private WebElement roamingPolicy;

	@FindBy(css = ".marketing-page.ng-scope div:nth-of-type(8) button")
	private WebElement scrollToTop;

	@FindBy(id = "searchText")
	private WebElement search1;

	@FindBy(id = "searchText")
	private WebElement search2;

	@FindBy(css = "a.tmo-modal-link")
	private WebElement seeFullTermsAndRoamingPolicies;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	private WebElement shopPhones;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMD_TM_Q2TRAVELLP_EZBFVNO6SGK12941']")
	private WebElement shopPlans;

	@FindBy(css = "a[href='#divfootermain']")
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	private WebElement smartDevices;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	private WebElement stores;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/student-teacher-smartphone-tablet-discounts?icid=WMM_TM_CAMPUSEXCL_UHTZZ7GR1RT11028']")
	private WebElement studentteacherDiscount;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.sticky nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.sticky div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch2;

	@FindBy(css = "a[href='//support.t-mobile.com/']")
	private WebElement supportHome;

	@FindBy(css = "a.btn-primary.btn.btn-transparent")
	private WebElement switchNow;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	private WebElement termsConditions;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true']")
	private WebElement termsOfUse;

	@FindBy(css = "a[href='https://business.t-mobile.com']")
	private WebElement tmobileForBusiness;

	@FindBy(css = "a[href='http://www.t-mobile.com/cell-phone-trade-in.html']")
	private WebElement tradeInProgram;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "a[href='http://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_ReturnPolicy&print=true']")
	private WebElement viewReturnPolicy;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	private WebElement wellHelpYouJoin;

	@FindBy(css = "#accordion-faqs div:nth-of-type(2) div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whatIfIHaveQualifying;

	@FindBy(css = "#accordion-faqs div:nth-of-type(1) div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement whatPlansQualifyForRoaming;

	@FindBy(css = "#accordion-faqs div:nth-of-type(2) div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement whatTypeOfInternationalFeatures;

	@FindBy(css = "#accordion-faqs div:nth-of-type(1) div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement whoQualifiesForTheTmobile;

	@FindBy(css = "#accordion-faqs div:nth-of-type(1) div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement withTmobileOneWillI;

	@FindBy(css = "a[href='//www.t-mobile.com/optional-services/roaming.html']")
	private WebElement wwwTmobileComoptionalservicesroamingHtml;
	
	@FindBy(css = "a[aria-label='Check a destination']")
	private WebElement checkADestination;

	public TravelAbroadPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Click on About Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickAboutLink() {
		about.click();
		return this;
	}

	/**
	 * Click on Accessibility Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickAccessibilityLink() {
		accessibility.click();
		return this;
	}

	/**
	 * Click on Accessories Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickAccessoriesLink() {
		accessories.click();
		return this;
	}

	/**
	 * Click on Activate Your Prepaid Phone Or Device Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickActivateYourPrepaidPhoneOrDeviceLink() {
		activateYourPrepaidPhoneOrDevice.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on Call 1800tmobile Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCall1800tmobileLink() {
		call1800tmobile.click();
		return this;
	}

	/**
	 * Click on Call Us Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Can I Get Discounts On Data With Ondemand Data Passes In Simple
	 * Global Countries Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCanIGetDiscountsOnButton() {
		canIGetDiscountsOn.click();
		return this;
	}

	/**
	 * Click on Careers Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCareersLink() {
		careers.click();
		return this;
	}

	/**
	 * Click on Check A Country Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCheckACountryLink() {
		checkACountry.click();
		return this;
	}

	/**
	 * Click on Check Order Status Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCheckOrderStatusLink() {
		checkOrderStatus.click();
		return this;
	}

	/**
	 * Click on Check Out Details. Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCheckOutDetailsLink() {
		checkOutDetails.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Close Video Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickCloseVideoButton() {
		closeVideo.click();
		return this;
	}

	/**
	 * Click on Consumer Information Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickConsumerInformationLink() {
		consumerInformation.click();
		return this;
	}

	/**
	 * Click on Contact Information Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickContactInformationLink() {
		contactInformation.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Deutsche Telekom Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickDeutscheTelekomLink() {
		deutscheTelekom.click();
		return this;
	}

	/**
	 * Click on Device Support Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickDeviceSupportLink() {
		deviceSupport.click();
		return this;
	}

	/**
	 * Click on Do All Plans Include Global Coverage Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickDoAllPlansIncludeGlobalButton() {
		doAllPlansIncludeGlobal.click();
		return this;
	}

	/**
	 * Click on Espanol Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Espaol Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickEspaolEspaolLink() {
		espaolEspaol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickFindAStoreLink() {
		findAStore.click();
		return this;
	}

	/**
	 * Click on Get A Rebate Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickGetARebateLink() {
		getARebate.click();
		return this;
	}

	/**
	 * Click on Here. Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickHereLink() {
		here.click();
		return this;
	}

	/**
	 * Click on How Can I Get Faster Data Speeds When Traveling Abroad Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickHowCanIGetFasterButton() {
		howCanIGetFaster.click();
		return this;
	}

	/**
	 * Click on How Do I Receive And Make Calls When Roaming Internationally Like
	 * Contacting Tmobile Customer Care Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickHowDoIReceiveAndButton() {
		howDoIReceiveAnd.click();
		return this;
	}

	/**
	 * Click on If I Travel Outside The 140 Simple Global Countries And Destinations
	 * Will My Device Have Global Coverage Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickIfITravelOutsideTheButton() {
		ifITravelOutsideThe.click();
		return this;
	}

	/**
	 * Click on Interestbased Ads Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickInterestbasedAdsLink() {
		interestbasedAds.click();
		return this;
	}

	/**
	 * Click on International Calling Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on International Rates Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickInternationalRatesLink() {
		internationalRates.click();
		return this;
	}

	/**
	 * Click on Internet Of Things Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickInternetOfThingsLink() {
		internetOfThings.click();
		return this;
	}

	/**
	 * Click on Investor Relations Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickInvestorRelationsLink() {
		investorRelations.click();
		return this;
	}

	/**
	 * Click on Join Us Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickJoinUsLink() {
		joinUs.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickLogInLink() {
		logIn.click();
		return this;
	}

	/**
	 * Click on Log In To My Tmobile Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickLogInToMyTmobileLink() {
		logInToMyTmobile.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickMyTmobile1Link() {
		myTmobile1.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickMyTmobile2Link() {
		myTmobile2.click();
		return this;
	}

	/**
	 * Click on Open Internet Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickOpenInternetLink() {
		openInternet.click();
		return this;
	}

	/**
	 * Click on Phones Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click on Plans Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Plans Services Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPlansServicesLink() {
		plansServices.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Press Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPressLink() {
		press.click();
		return this;
	}

	/**
	 * Click on Privacy Center Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPrivacyCenterLink() {
		privacyCenter.click();
		return this;
	}

	/**
	 * Click on Privacy Statement Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPrivacyStatementLink() {
		privacyStatement.click();
		return this;
	}

	/**
	 * Click on Public Safety911 Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPublicSafety911Link() {
		publicSafety911.click();
		return this;
	}

	/**
	 * Click on Puerto Rico Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickPuertoRicoLink() {
		puertoRico.click();
		return this;
	}

	/**
	 * Click on Questions About Your Bill Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickQuestionsAboutYourBillLink() {
		questionsAboutYourBill.click();
		return this;
	}

	/**
	 * Click on Refill Your Prepaid Account Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickRefillYourPrepaidAccountLink() {
		refillYourPrepaidAccount.click();
		return this;
	}

	/**
	 * Click on Roaming Policy Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickRoamingPolicyLink() {
		roamingPolicy.click();
		return this;
	}

	/**
	 * Click on Scroll To Top Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickScrollToTopButton() {
		scrollToTop.click();
		return this;
	}

	/**
	 * Click on See Full Terms And Roaming Policies Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSeeFullTermsAndRoamingPoliciesLink() {
		seeFullTermsAndRoamingPolicies.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickShopPhonesLink() {
		shopPhones.click();
		return this;
	}

	/**
	 * Click on Shop Plans Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickShopPlansLink() {
		shopPlans.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Studentteacher Discount Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickStudentteacherDiscountLink() {
		studentteacherDiscount.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Support Home Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSupportHomeLink() {
		supportHome.click();
		return this;
	}

	/**
	 * Click on Switch Now Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickSwitchNowLink() {
		switchNow.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Terms Of Use Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickTermsOfUseLink() {
		termsOfUse.click();
		return this;
	}

	/**
	 * Click on Tmobile For Business Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickTmobileForBusinessLink() {
		tmobileForBusiness.click();
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickTradeInProgramLink() {
		tradeInProgram.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on View Return Policy Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickViewReturnPolicyLink() {
		viewReturnPolicy.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Click on What If I Have Qualifying Tmobile One Or Simple Choice Plan But I
	 * Dont Want The International Roaming Capability Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWhatIfIHaveQualifyingButton() {
		whatIfIHaveQualifying.click();
		return this;
	}

	/**
	 * Click on What Plans Qualify For Roaming In 140 Countries And Destinations And
	 * How Do I Sign Up Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWhatPlansQualifyForRoamingButton() {
		whatPlansQualifyForRoaming.click();
		return this;
	}

	/**
	 * Click on What Type Of International Features Can I Add To The Tmobile No
	 * Credit Check Plan Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWhatTypeOfInternationalFeaturesButton() {
		whatTypeOfInternationalFeatures.click();
		return this;
	}

	/**
	 * Click on Who Qualifies For The Tmobile One Plus And One Plus International
	 * Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWhoQualifiesForTheTmobileButton() {
		whoQualifiesForTheTmobile.click();
		return this;
	}

	/**
	 * Click on With Tmobile One Will I Get The Same Highspeed Network Experience
	 * Internationally As I Do Here At Home In The U.s. Button.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWithTmobileOneWillIButton() {
		withTmobileOneWillI.click();
		return this;
	}

	/**
	 * Click on Www.tmobile.comoptionalservicesroaming.html Link.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage clickWwwTmobileComoptionalservicesroamingHtmlLink() {
		wwwTmobileComoptionalservicesroamingHtml.click();
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Travel to abroad page loaded");
		} catch (NoSuchElementException e) {
			Assert.fail("Travel to abroad Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

	/**
	 * Verify and click Check a destination CTA
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TravelAbroadPage verifyAndClickCheckADestination() {
		try {
			checkPageIsReady();
			checkADestination.isDisplayed();
			checkADestination.click();
			Reporter.log("Verified and Clicked on Check a destination cta");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to click on Check a destination cta");
		}
		return this;
	}
	
}
