package com.tmobile.eservices.qa.pages.tmng.functional;

import static org.testng.Assert.assertTrue;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

import io.appium.java_client.AppiumDriver;

public class PdpPage extends TmngCommonPage {

	private final String cellPhonePageUrl = "/cell-phone/";
	private final String accessoriesPdpPageUrl = "/accessory";
	private final String tabletsAndDevicesPdpPageUrl = "/tablet/";
	private final String watchPdpPageUrl = "/smart-watch/";
	private final String simKitPdpPageUrl = "/t-mobile-sim-card";

	@FindBy(css = "img[class*='product-image']")
	private WebElement deviceImg;

	private final String prospectCustomer = "Continue as guest";
	private final String existingCustomer = "Log In";

	@FindBy(css = "button[data-event-action*='CartAdd']")
	private WebElement addToCartBtn;
	
	@FindBy(xpath="//span[contains(text(),' Add to Cart')]")
	private WebElement addToCartBtnMobile;

	@FindBy(css = "button[data-analytics-value='Upgrade']")
	private WebElement upgradeCTA;

	@FindBy(css = "button[data-analytics-value='Add to a new Line']")
	private WebElement addaLineCTA;

	@FindBy(css = "div[class*='finance-prices'] span span")
	private WebElement eiploanTermLength;

	@FindBy(css = "span[class='toggle pay-in-full']")
	private WebElement payInFullPaymentOption;

	@FindBy(css = "a[href*='https://www.t-mobile.com/store-locator/']")
	private WebElement storeName;

	@FindBy(css = "div[class*='full-price']  span")
	private List<WebElement> deviceFRP;

	@FindBy(css = "div[class*='full-price-toggle-container'] strong")
	private WebElement accessoryDeviceFRPDiscounted;

	@FindBy(css = ".mat-dialog-container button.cta-button")
	private List<WebElement> guestOrLogInCta;

	@FindBy(css = "span.down-payment h2")
	private WebElement downPayment;

	@FindBy(css = "strong[data-e2e='inventory-status']")
	private WebElement onlineStockAvailability;

	@FindBy(css = "p.text-small")
	private WebElement storeInventoryStatus;

	@FindBy(css = "p.schedule-appointment-button-container")
	private WebElement ScheduleappointmentCTA;

	@FindBy(css = "span[class='distance']")
	private WebElement storeDistaceInPDP;

	@FindBy(xpath = "//div[contains(@class,'mat-tab-label-active') and contains(@data-analytics-value,'Reviews')]/div")
	private WebElement reviewsSectionactive;

	@FindBy(css = "div[data-analytics-value='Specifications']")
	private WebElement specificationsTab;

	@FindBy(css = "[data-analytics-value='Reviews']")
	private WebElement reviewsTab;

	@FindBy(css = ".ratings-count span")
	private WebElement reviewsLink;

	@FindBy(css = ".mat-tab-body-content .key-features-heading")
	private WebElement specificationsKeyFeaturesHeader;

	@FindBy(css = "#BVRRContentContainerID")
	private WebElement reviewSection;

	@FindBy(css = "button[data-asset-type='memory'] span")
	private List<WebElement> memoryVariant;

	@FindBy(css = "button[data-asset-type='swatch'] img")
	private List<WebElement> colorVariant;

	@FindBy(css = "div[class*='finance-prices'] span[class='fx-column'] h2")
	private WebElement eipPrice;

	@FindBy(css = "span.down-payment div")
	private WebElement downPlusTaxPlusSimStarterKitEip;

	@FindBy(css = "span.down-payment div span")
	private WebElement sskFeeMessageforEIP;

	@FindBy(xpath = "//span[contains(text(),'+ $25.00 SIM starter kit')]")
	private WebElement sskFeeMessageforFRP;

	@FindBy(css = ".full-price-now div.sub-text")
	private WebElement downPlusTaxPlusSimStarterKitFrp;

	@FindBy(css = "div[class='text-legal']")
	private WebElement legaltextAtfooter;

	@FindBy(xpath = "//h1[contains(text(),'Internet SIM Kit')]")
	private WebElement internetSimKitTitletPDPPage;

	@FindBy(css = "button[data-analytics-value='Out of Stock']")
	private WebElement outOfstockCTA;

	@FindBy(css = "button[data-analytics-value='Coming Soon']")
	private WebElement comingSoonCTA;

	@FindBy(css = "button[data-analytics-value='Global Pre-Order Add To Cart']")
	private WebElement preOrderCTA;

	@FindBy(css = "button[data-analytics-value='1-800-T-MOBILE']")
	private WebElement notForSaleOnline;

	@FindBy(css = "div[class='description ng-star-inserted']")
	private WebElement romanceCopy;

	@FindBy(css = "devicesInventoryStatus")
	private WebElement devicesInventoryStatus;

	@FindBy(css = "offers")
	private WebElement offers;

	@FindBy(css = "contactUsInFooter")
	private WebElement contactUsInFooter;

	@FindBy(css = "div[class='finance-prices fx-row ng-star-inserted']")
	private WebElement crporAwesomePrices;

	@FindBy(css = "div[class='verify-pricing-banner ng-star-inserted']")
	private WebElement yourPricingBoxInPDP;

	@FindBy(css = "div[class='verify-pricing-banner ng-star-inserted'] p")
	private WebElement yourPricingBoxTextPDP;

	@FindBy(css = "a[data-analytics-details*='/pre-screen/intro']")
	private WebElement findYourPricingInPDP;

	@FindBy(css = "a[data-event-action='ModalOpen']")
	private WebElement promotionText;

	@FindBy(css = "mat-dialog-container[aria-modal='true']")
	private WebElement promotionModal;

	@FindBy(css = "h6[class*='promo-title']")
	private WebElement deviceNameOnPromotionModal;

	@FindBy(css = "h4[class*='heading-4']")
	private WebElement promotionTextOnPromotionModal;

	@FindBy(css = "h2[class*='list-price']")
	private WebElement frpPriceAfterClickonPayInFull;

	@FindBy(css = "[class*='display-4']")
	private WebElement deviceName;

	@FindBy(css = "h1[aria-label*=' SIM Card']")
	private WebElement simKitName;

	@FindBy(xpath = "//a[contains(.,'How pre-order works')]")
	private WebElement howDoesPreOrderWorkLink;

	@FindBy(css = "div[class*='cdk-overlay-pane']")
	private WebElement preOrderModelWindow;

	@FindBy(css = "tmo-pre-order-faq-modal h4")
	private WebElement preOrderModelWindowHeader;

	@FindBy(css = "div[class='fx-row-wrap ng-star-inserted'] button[class*='selected']")
	private WebElement selectedColor;

	@FindBy(css = "div[class='ng-star-inserted'] button[class*='selected']")
	private WebElement selectedMemory;

	@FindBy(css = "span a[data-analytics-value='Phones']")
	private WebElement phonesLink;

	@FindBy(css = "span a[data-analytics-value='Watches']")
	private WebElement watchesLink;

	@FindBy(css = "tmo-customer-type-selection[class='ng-star-inserted'] div[class*='fx-row']")
	private WebElement selectUserModal;

	@FindBy(css = "button[data-analytics-value='Log In']")
	private WebElement existingUserOption;
	
	
	public PdpPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PdpPage verifyPhonePdpPageLoaded() {
		try {
			checkPageIsReady();
			verifyPdpPageUrl(cellPhonePageUrl);
			Reporter.log("Phone PDP page loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Phones PDP Page");
		}
		return this;
	}

	/**
	 * validate Internet SIM KIT in Mini PDP Page
	 *
	 */
	public PdpPage verifyInternetSimKitTitle() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(internetSimKitTitletPDPPage),
					" Internet SIM kit title on Main PDP Page is not displayed.");
			Reporter.log("Internet SIM KIT title on main PDP Page is loaded Successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Internet SIM kit title on Main PDP Page.");
		}
		return this;
	}

	/**
	 * Verify that Tablets and Devices page loaded completely.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PdpPage verifyTabletsAndDevicesPdpPageLoaded() {
		try {
			checkPageIsReady();
			verifyPdpPageUrl(tabletsAndDevicesPdpPageUrl);
			Reporter.log("Tablets and Devices PLP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Tablets and Devices PLP Page");
		}
		return this;
	}

	/**
	 * Verify that Wearable PDP page loaded completely.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PdpPage verifyWatchesPdpPageLoaded() {
		try {
			checkPageIsReady();
			verifyPdpPageUrl(watchPdpPageUrl);
			Reporter.log("Watch PDP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Watch PDP Page");
		}
		return this;
	}

	/**
	 * Verify that SIM Kit PDP page loaded completely.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PdpPage verifySimKitPdpPageLoaded() {
		try {
			verifyPdpPageUrl(simKitPdpPageUrl);
			Reporter.log("Sim Kit  PDP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Sim Kit PDP Page");
		}
		return this;
	}

	/**
	 * Verify that Accessories page loaded completely.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PdpPage verifyAccessoriesPdpPageLoaded() {
		try {			
			checkPageIsReady();
			moveToElement(reviewsTab);
			verifyPdpPageUrl(accessoriesPdpPageUrl);
			Reporter.log("Accessories PLP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Accessories PLP Page");
		}
		return this;
	}

	/**
	 * Verify device Image in PDP
	 *
	 */
	public PdpPage verifDeviceImageAtPDP() {

		try {
			Assert.assertFalse(deviceImg.getAttribute("src").isEmpty(), "Device Image is not found at PDP");
			Reporter.log("Device Image is displayed at PDP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Image in PDP");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PdpPage verifyPdpPageUrl(String url) {
		try {
			waitFor(ExpectedConditions.urlContains(url), 60);
		} catch (Exception e) {
			Assert.fail("Failed to verify PDP Page Url.");
		}
		return this;
	}

	/**
	 * verify Upgrade CTA in PDP for Cooked user
	 *
	 */
	public PdpPage verifyUpgradeCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(upgradeCTA), 60);
			Assert.assertTrue(upgradeCTA.isDisplayed(), "Upgrade CTA is not displayed on PDP for Cooked User");
		} catch (Exception e) {
			Assert.fail("Failed to verify Upgrade CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * click Upgrade CTA in PDP for Cooked user
	 *
	 */
	public PdpPage clickUpgradeCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(upgradeCTA), 60);
			upgradeCTA.click();
			Reporter.log("Clicked on 'Upgrade' button.");
		} catch (Exception e) {
			Assert.fail("Failed to click Upgrade CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * verify Add A line CTA in PDP for Cooked user
	 *
	 */
	public PdpPage verifyAddaLineCTA() {
		try {
			Assert.assertTrue(addaLineCTA.isDisplayed(), "Add A Line CTA is not displayed on PDP for Cooked User");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add A Line CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * Click Add A line CTA in PDP for Cooked user
	 *
	 */
	public PdpPage clickAddaLineCTA() {
		try {
			clickElementWithJavaScript(addaLineCTA);
			Reporter.log("Clicked on Add A Line CTA");
		} catch (Exception e) {
			Assert.fail("Failed to click Add A Line CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * Click on Add to cart button
	 *
	 */
	public PdpPage clickOnAddToCartBtn() {
		waitForSpinnerInvisibility();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
				clickElementWithJavaScript(addToCartBtnMobile);
			} else {
				moveToElement(addToCartBtn);
				addToCartBtn.click();
			}
			Reporter.log("Clicked on 'Add to cart' button.");
		} catch (Exception e) {
			Assert.fail("Failed to click on Add to cart button.");
		}
		return this;
	}

	/**
	 * Get Eip loan term length
	 *
	 */
	public String getEipLoanTermLength() {
		String eiploantermLength = "";
		try {

			eiploantermLength = eiploanTermLength.getText();
			String eiploantermLengthArr[] = eiploantermLength.split(" ");
			eiploantermLength = eiploantermLengthArr[1];
			Reporter.log("EIP loan term length is obtained");
		} catch (Exception e) {
			Assert.fail("Failed to Get EIP loan term length");
		}
		return eiploantermLength;
	}

	/**
	 * Click pay in full today from payment options
	 * 
	 * @return
	 */
	public PdpPage clickPayInFullPaymentOption() {
		try {
			waitFor(ExpectedConditions.visibilityOf(payInFullPaymentOption), 10);
			moveToElement(payInFullPaymentOption);
			clickElementWithJavaScript(payInFullPaymentOption);
			Reporter.log("Clicked on 'pay in full' payment option");
		} catch (Exception e) {
			Assert.fail("Click on 'pay in full' today payment option is failed ");
		}
		return this;
	}

	/**
	 * Verify store locator name
	 *
	 */
	public PdpPage verifyStoreLocatorName() {
		try {			
			moveToElement(reviewsTab);
			waitFor(ExpectedConditions.visibilityOf(storeName), 60);
			Assert.assertTrue(storeName.isDisplayed(), " Store locator name is not displayed");
			Reporter.log("Store locator name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Store locator name");
		}
		return this;
	}

	/**
	 * Click on Store locator name
	 */
	public PdpPage clickStoreLocatorName() {
		try {
			waitForSpinnerInvisibility();
			clickElementWithJavaScript(storeName);
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			Reporter.log("Store locator name is clicked");
			getDriver().switchTo().window(tabs2.get(1));
			Reporter.log("Moved to SDP tab");
		} catch (Exception e) {
			Assert.fail("Failed to click Store locator name.");
		}
		return this;
	}

	/**
	 * Get total Device FRP on PDP
	 * 
	 * @return
	 */
	public String getAccessoryDeviceFRP() {
		String frp = "";
		try {
			if (isElementDisplayed(accessoryDeviceFRPDiscounted)) {
				frp = accessoryDeviceFRPDiscounted.getText().replace("$", "");
			} else {
				frp = deviceFRP.get(1).getText().replace("$", "");
			}
			Reporter.log(" Got total Device FRP on PDP");
		} catch (Exception e) {
			Assert.fail("Failed to get total Device FRP on PDP");
		}
		return frp;
	}

	/**
	 * Get total Device FRP on PDP
	 * 
	 * @return
	 */
	public String getDeviceFRP() {
		String frp = "";
		try {
			frp = deviceFRP.get(1).getText().replace("$", "");
			Reporter.log(" Got total Device FRP on PDP");
		} catch (Exception e) {
			Assert.fail("Failed to get total Device FRP on PDP");
		}
		return frp;
	}

	/**
	 * Select option from Self Identification Modal
	 */
	public PdpPage selectOptionFromSelfIdentificationModal(String option) {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(guestOrLogInCta.get(0)), 60);
			boolean ctaExist = false;
			for (WebElement cta : guestOrLogInCta) {
				String ctaText = cta.getText();
				if (ctaText.equalsIgnoreCase(option)) {
					clickElementWithJavaScript(cta);
					ctaExist = true;
					Reporter.log("Clicked on '" + option + "' CTA");
					break;
				}
			}
			Assert.assertTrue(ctaExist, "CTA with text '" + option + "' is not present");
		} catch (Exception e) {
			Assert.fail("Failed to click option in Self Identification Modal");
		}
		return this;
	}

	/**
	 * Select Prospect option from Self Identification Modal
	 */
	public PdpPage selectContinueAsGuest() {
		try {
			waitFor(ExpectedConditions.visibilityOf(guestOrLogInCta.get(0)), 60);
			if (guestOrLogInCta.get(0).isDisplayed()) {
				selectOptionFromSelfIdentificationModal(prospectCustomer);
			} else {
				Reporter.log("Self Identification Modal is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Self Identification Modal");
		}

		return this;
	}

	/**
	 * Select Log In option from Self Identification Modal
	 */
	public PdpPage selectLogIn() {
		selectOptionFromSelfIdentificationModal(existingCustomer);
		return this;
	}

	/**
	 * Get DownPayment Price
	 *
	 */
	public String getDownPaymentPrice() {
		String downPaymentPrice = "";
		try {
			waitForSpinnerInvisibility();
			downPaymentPrice = downPayment.getText().replace("$", "");
			Reporter.log("Down Payment Price is Obtained");
		} catch (Exception e) {
			Assert.fail("Failed to Get DownPayment Price.");
		}

		return downPaymentPrice;
	}

	/**
	 * Calculate total monthly payment value
	 */
	public Double calculateTotalMonthlyEIPValue(Double frp, Double downpayment) {
		double finalValue = 0.0;
		try {
			Double eiploantermValue = Double.parseDouble(getEipLoanTermLength());
			Double totalMonthlyPaymentValue = (frp - downpayment) / eiploantermValue;
			DecimalFormat df = new DecimalFormat("0.00");
			String formate = df.format(totalMonthlyPaymentValue);
			finalValue = Double.parseDouble(formate);
		} catch (Exception e) {
			Assert.fail("Failed to Calculate total monthly payment value");
		}

		return finalValue;
	}

	/**
	 * Get EIP Payment Price
	 *
	 */
	public String getEIPPaymentPrice() {
		String eipPaymentPrice = "";
		try {
			eipPaymentPrice = eipPrice.getText();
			eipPaymentPrice = eipPaymentPrice.substring(eipPaymentPrice.indexOf("$") + 1, eipPaymentPrice.indexOf("/"));
			Reporter.log("EIP Price is Obtained");
		} catch (Exception e) {
			Assert.fail("Failed to Get EIP Payment Price.");
		}

		return eipPaymentPrice;
	}

	/**
	 * Compare Two Values
	 *
	 */
	public PdpPage compareEIPValues(Double firstValue, Double secondValue) {
		try {
			double result = firstValue - secondValue;
			Assert.assertTrue(result < 1, "EIP values are not equal");
			Reporter.log("Two Values are Equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare 2 values");
		}
		return this;
	}

	/**
	 * Click on Add A Line button
	 *
	 */
	public PdpPage clickOnAddALineBtn() {
		try {
			checkPageIsReady();
			moveToElement(addaLineCTA);
			clickElementWithJavaScript(addaLineCTA);
			// selectContinueAsGuest();
			checkPageIsReady();
			Reporter.log("Clicked on 'Add a line' button.");
		} catch (Exception e) {
			Assert.fail("Failed to click on Add a line button.");
		}
		return this;
	}

	/**
	 * Verify Add to Cart Button is displayed
	 *
	 */
	public PdpPage verifyAddToCartCTA() {
		try {
			checkPageIsReady();
			Assert.assertTrue(addToCartBtn.isDisplayed(), "'Add to Cart' CTA is not displayed");
			Reporter.log("Add to Cart Button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add to Cart Button");
		}
		return this;
	}

	/**
	 * Verify Add to Cart Button availability
	 *
	 */
	public boolean isAddToCartCTAEnabled() {
		boolean isAddToCartCTAavailable = false;
		try {
			checkPageIsReady();

			if (addToCartBtn.isDisplayed()) {
				isAddToCartCTAavailable = true;
			} else {
				isAddToCartCTAavailable = false;
			}
			Reporter.log("Add to Cart Button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add to Cart Button");
		}
		return isAddToCartCTAavailable;
	}

	/**
	 * Verify Online Stock state
	 *
	 */
	public PdpPage verifyOnlineStock() {
		try {
			assertTrue(onlineStockAvailability.isDisplayed(), "Online Stock inventory message is not displayed");
			Reporter.log("Online Stock inventory message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Online Stock inventory message");
		}
		return this;
	}

	/**
	 * Verify inventory status of product at store
	 *
	 */
	public PdpPage verifyInventoryStatus() {
		try {
			moveToElement(storeInventoryStatus);
			assertTrue(storeInventoryStatus.isDisplayed(), "Inventory message at store level is not displayed");
			Reporter.log("Stock inventory message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Stock inventory message at store level");
		}
		return this;
	}

	/**
	 * Verify Schedule in-store appointment
	 *
	 */
	public PdpPage verifyScheduleInStoreAppointment() {
		try {
			if (storeInventoryStatus.getText().contains("Contact ")
					|| storeInventoryStatus.getText().contains("Inventory status ")) {
				Reporter.log(
						"Schedule Appointmnet CTA is disabled, status in Contact store for availability and Inventory status not available ");
			} else {
				assertTrue(ScheduleappointmentCTA.isDisplayed(), "Appointmnet CTA is not displayed");
				Reporter.log("Schedule Appointmnet CTA is displayed ");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify Appointmnet CTA ");
		}
		return this;
	}

	/**
	 * Verify distance In Inventory Container
	 *
	 */
	public PdpPage verifyDistanceInInventoryContainer() {
		try {
			moveToElement(specificationsTab);
			assertTrue(storeDistaceInPDP.isDisplayed(), "distance In Inventory Container is not displayed");
			Reporter.log("distance In Inventory Container is displayed");
		} catch (Exception e) {
			Assert.fail("distance In Inventory Container is not displayed");
		}
		return this;
	}

	/**
	 * Verify Review Section is displayed
	 *
	 */
	public PdpPage verifyReviewSectionIsDisplayed() {
		checkPageIsReady();
		try {
			reviewsTab.click();
			waitFor(ExpectedConditions.visibilityOf(reviewSection), 60);
			Assert.assertTrue(reviewsSectionactive.getText().equalsIgnoreCase("Reviews"),
					"Review Section is not displayed");
			Reporter.log("Review section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed To verify Review Section");
		}
		return this;
	}

	/**
	 * Verify Specifications Tab is displayed on Main PDP
	 *
	 */
	public PdpPage verifySpecificationsTab() {
		try {
			checkPageIsReady();
			moveToElement(specificationsTab);
			// System.out.println(specificationsTab.getText());
			Assert.assertTrue(specificationsTab.getText().equalsIgnoreCase("specifications"),
					"'Specifications' tab is not displayed on PDP");
			Reporter.log("'Specifications' tab is displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Specifications' tab in PDP");
		}
		return this;
	}

	/**
	 * Click Specifications Tab on Main PDP and verify navigation
	 *
	 */
	public PdpPage clickSpecificationsTabAndVerifyNavigation() {
		try {
			checkPageIsReady();
//			clickElementWithJavaScript(specificationsTab);
//			Reporter.log("Clicked 'Specifications' tab on PDP");
			Assert.assertTrue(specificationsKeyFeaturesHeader.isDisplayed(),
					"'Specifications' section is not displayed on PDP");
			Reporter.log("'Specifications' section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Specifications' tab");
		}
		return this;
	}

	/**
	 * Verify Reviews Tab is displayed on Main PDP
	 *
	 */
	public PdpPage verifyReviewsTab() {
		try {
			checkPageIsReady();
			// System.out.println(reviewsTab.getText());
			Assert.assertTrue(reviewsTab.getText().equalsIgnoreCase("reviews"),
					"'Reviews' tab is not displayed on PDP");
			Reporter.log("'Reviews' Tab is displayed in PDP");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Reviews' tab in PDP");
		}
		return this;
	}

	/**
	 * Click Review Tab on Main PDP and verify navigation
	 *
	 */
	public PdpPage clickReviewsTabAndVerifyNavigation() {
		try {
			clickElementWithJavaScript(reviewsTab);
			Reporter.log("Clicked 'Review' tab on PDP");
			waitFor(ExpectedConditions.visibilityOf(reviewSection), 60);
			Assert.assertTrue(reviewSection.isDisplayed(), "'Review' section is not displayed on PDP");
			Reporter.log(" Review' tab is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Reviews' tab");
		}
		return this;
	}

	/**
	 * validate SIM Starter KIT info in PDP Page
	 *
	 */
	public PdpPage verifyPlusTaxPlusSimStarterKitIsDisplayed() {
		try {
			waitForSpinnerInvisibility();
			String wantToPayTodayDisplayed = payInFullPaymentOption.getText();
			if (wantToPayTodayDisplayed.contains("full")) {
				Assert.assertTrue(downPlusTaxPlusSimStarterKitEip.getText().contains("tax"),
						"'+ tax' is not displayed in Today section");
//				Assert.assertTrue(downPlusTaxPlusSimStarterKitEip.getText().contains("SIM starter kit"),
//						"'SIM starter kit' is not displayed in Today section");
			} else if (wantToPayTodayDisplayed.contains("finance")) {
				Assert.assertTrue(downPlusTaxPlusSimStarterKitFrp.getText().contains("tax"),
						"'+tax' is not displayed in Today section");
//				Assert.assertTrue(downPlusTaxPlusSimStarterKitFrp.getText().contains("SIM starter kit"),
//						"'SIM starter kit' is not displayed in Today section");
			} else {
				Assert.fail("'+ tax + SIM starter kit' is not displayed on PDP Page.");
			}
			Reporter.log("'+ tax + SIM starter kit' is displayed on PDP Page.");
		} catch (Exception e) {
			Assert.fail("Failed to verify '+ tax + SIM starter kit' on PDP Page.");
		}
		return this;
	}

	/**
	 * validate Legal text at footer PDP Page
	 *
	 */
	public PdpPage verifyLegalTextAtFooter() {

		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(legaltextAtfooter),
					"Legal text is not diplayed on main PDP Page footer.");
			Reporter.log("Legal text is diplayed on main PDP Page footer.");
		} catch (Exception e) {
			Assert.fail("Failed verify Legal text on main PDP Page footer.");
		}
		return this;
	}

	/**
	 * Click Review link on Main PDP
	 *
	 */
	public PdpPage clickReviewLinkOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(reviewsLink), 60);
			moveToElement(reviewsLink);
			reviewsLink.click();
			Reporter.log("Clicked 'Reviews' link on PDP");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Reviews' link");
		}
		return this;
	}

	/**
	 * verify Review Section on bottom of Main PDP
	 *
	 */
	public PdpPage verifyReviewsSectionOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(reviewSection), 60);
			Assert.assertTrue(reviewSection.isDisplayed(), "Review Section on bottom of Main PDP is not displayed");
			Reporter.log(" Review Section on bottom of Main PDP is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Review Section on bottom of Main PDP");
		}
		return this;
	}

	/**
	 * verify Memory Variant is displayed
	 */
	public PdpPage verifyMemoryVariant() {
		try {
			Assert.assertTrue(memoryVariant.get(0).isDisplayed(), "Memory Varient is not displayed");
			for (WebElement memory : memoryVariant) {
				Assert.assertTrue(memory.isDisplayed(), "Memory Varient is not displayed");
			}
			Reporter.log("Memory Varient is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Memory Varient");
		}
		return this;
	}

	/**
	 * verify EIP loan term length is displayed
	 */
	public PdpPage verifyLoanTermLength() {
		try {
			Assert.assertTrue(eiploanTermLength.isDisplayed(), "EIP loan term length is not displayed");
			Reporter.log("EIP loan term length is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP loan term length");
		}
		return this;
	}

	/**
	 * verify Monthly payment is displayed
	 */
	public PdpPage verifyEIPPayment() {
		try {
			Assert.assertTrue(eipPrice.isDisplayed(), "Monthly payment is not displayed");
			Reporter.log("Monthly payment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment");
		}
		return this;
	}

	/**
	 * verify DownPayment Price
	 *
	 */
	public PdpPage verifyDownPaymentPrice() {
		try {
			Assert.assertTrue(downPayment.isDisplayed(), "DownPayment Price is not displayed on PDP");

			Reporter.log("Down Payment Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify DownPayment Price.");
		}

		return this;
	}

	/**
	 * verify Memory Variant order
	 */
	public PdpPage verifyMemoryVariantsOrder() {
		try {
			int availableMemoryVariants = memoryVariant.size();
			int availableMemoryVariantsValues[] = new int[availableMemoryVariants];
			if (availableMemoryVariants < 2) {
				Reporter.log("One memory variant is available for the device");
			} else {
				int i = 0;
				for (WebElement memory : memoryVariant) {
					int memoryValue = Integer.parseInt(memory.getText().replace("GB", ""));
					availableMemoryVariantsValues[i] = memoryValue;
					i++;
				}
			}
			boolean isMemoryVaraintsSortedLeftToRight = false;
			for (int j = 0; j < availableMemoryVariantsValues.length - 1; j++) {
				if (availableMemoryVariantsValues[j] < availableMemoryVariantsValues[j + 1]) {
					isMemoryVaraintsSortedLeftToRight = true;
				} else {
					isMemoryVaraintsSortedLeftToRight = false;
				}
			}
			Assert.assertTrue(isMemoryVaraintsSortedLeftToRight,
					"Memory varaints are not sorted in the order Left to Right");

			Reporter.log("Memory varaints are sorted in the order Left to Right");
		} catch (Exception e) {
			Assert.fail("Failed to verify Memory Variant order.");
		}
		return this;
	}

	/**
	 * verify OutOfstock CTA in PDP for Cooked user
	 *
	 */
	public PdpPage verifyOutOfStockCTA() {
		try {
			Assert.assertTrue(outOfstockCTA.isDisplayed(), "Out of Stock CTA is not displayed on PDP for Cooked User");
			Assert.assertFalse(outOfstockCTA.isEnabled(), "Out of Stock CTA is enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Out of Stock CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * verify Coming Soon CTA in PDP for Cooked user
	 *
	 */
	public PdpPage verifyComingSoonCTA() {
		try {
			Assert.assertTrue(comingSoonCTA.isDisplayed(), "Coming soon CTA is not displayed on PDP for Cooked User");
			Assert.assertFalse(comingSoonCTA.isEnabled(), "Coming Soon CTA is enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Coming soon CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * verify Coming Soon CTA in PDP for Cooked user
	 *
	 */
	public PdpPage verifyNotForSaleCTA() {
		try {
			Assert.assertTrue(notForSaleOnline.isDisplayed(),
					"Not For Sale CTA is not displayed on PDP for Cooked User");
			Assert.assertFalse(notForSaleOnline.isEnabled(), "Not For Sale CTA is enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Not For Sale CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * verify Pre-Order CTA in PDP for Cooked user
	 *
	 */
	public PdpPage verifyPreOrderCTA() {
		try {
			Assert.assertTrue(preOrderCTA.isDisplayed(), "Pre-Order CTA is not displayed on PDP for Cooked User");
			Assert.assertTrue(preOrderCTA.isEnabled(), "Pre-Order CTA is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pre-Order CTA in PDP for Cooked user");
		}
		return this;
	}

	/**
	 * verify verifyRomanceCopyOnPDP
	 *
	 */
	public PdpPage verifyRomanceCopyOnPDP() {
		try {
			Assert.assertTrue(romanceCopy.isDisplayed(), "Romance Copy of the device is not displayed on PDP");
			Reporter.log("Romance Copy of the device is displayed on PDP");
		} catch (Exception e) {
			Assert.fail("Romance Copy of device is not available for the device");
		}
		return this;
	}

	/**
	 * Verify Devices Inventory status
	 */
	public PdpPage verifyDeviceInventoryStatus(String option) {
		try {
			Assert.assertTrue(devicesInventoryStatus.isDisplayed(), "Device Inventory status not displayed");
			Reporter.log("Device Inventory staus displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display device inventory status");
		}
		return this;
	}

	/**
	 * Verify EIP price in PDP page
	 */
	public PdpPage verifyEIPPrice() {
		try {
			Assert.assertFalse(eipPrice.isDisplayed(), "EIP price is displayed");
			Reporter.log("EIP price is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP price");
		}
		return this;
	}

	/**
	 * Verify FRP price in PDP page
	 */
	public PdpPage verifyFRPPrice() {
		try {
			Assert.assertTrue(deviceFRP.get(1).isDisplayed(), "FRP price is displayed");
			Reporter.log("FRP price is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display FRP price");
		}
		return this;
	}

	/**
	 * Verify Offers in PDP page
	 */
	public PdpPage verifyOffers() {
		try {
			Assert.assertFalse(offers.isDisplayed(), "Offers is displayed");
			Reporter.log("Offers is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Offers");
		}
		return this;
	}

	/**
	 * Verify Color Swatches in PDP page
	 */
	public PdpPage verifyColorSwatches() {
		try {
			waitFor(ExpectedConditions.visibilityOf(colorVariant.get(0)), 60);
			Assert.assertTrue(colorVariant.get(0).isDisplayed(), "Color swatches is displayed");
			Reporter.log("Color swatches is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Color swatches");
		}
		return this;
	}

	/**
	 * Verify ContactUs in footer
	 */
	public PdpPage verifyContactUsInFooter() {
		try {
			Assert.assertTrue(contactUsInFooter.isDisplayed(), "ContactUs in footer is not displayed");
			Reporter.log("ContactUs in footer is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display ContactUs in footer");
		}
		return this;
	}

	/**
	 * Verify inventory status of product and CTAs
	 *
	 */
	public PdpPage verifyInventoryStatusandAllCTAs() {
		try {
			assertTrue(onlineStockAvailability.isDisplayed(), "Inventory Status is not displayed");
			Reporter.log("Stock inventory Status message is displayed");
			if (onlineStockAvailability.getText().trim().contains("Out of Stock.")) {
				verifyOutOfStockCTA();
				Reporter.log("Out of stock CTA displayed and disabled");
			} else if (getDriver().getCurrentUrl().contains(cellPhonePageUrl)) {
				if (onlineStockAvailability.getText().trim().contains("In Stock.")
						|| onlineStockAvailability.getText().trim().contains("Pre-order")
						|| onlineStockAvailability.getText().trim().contains("Backorder")
						|| onlineStockAvailability.getText().trim().contains("Available")) {
					verifyAddaLineCTA();
					verifyUpgradeCTA();
					Reporter.log("Upgrade and Add A Line CTA displayed and enabled");
				}
			} else if (getDriver().getCurrentUrl().contains(accessoriesPdpPageUrl)) {
				if (onlineStockAvailability.getText().trim().contains("In Stock.")
						|| onlineStockAvailability.getText().trim().contains("Backorder")
						|| onlineStockAvailability.getText().trim().contains("Available")) {
					verifyAddToCartCTA();
					Reporter.log("Add to Cart CTA displayed and enabled");
				}
			} else if (getDriver().getCurrentUrl().contains(accessoriesPdpPageUrl)) {
				if (onlineStockAvailability.getText().trim().contains("Pre-order")) {
					verifyPreOrderCTA();
					Reporter.log("Upgrade and Add A Line CTA displayed and enabled");

				}
			} else if (onlineStockAvailability.getText().trim().contains("Coming Soon")) {
				verifyComingSoonCTA();
				Reporter.log("Coming Soon CTA displayed and disabled");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify inventory message at");
		}
		return this;
	}

	/**
	 * verify CRP or Awesome Prices in PDP
	 *
	 */
	public PdpPage verifyCRPorAwesomePricesInPDP() {
		try {
			if (isElementDisplayed(findYourPricingInPDP)) {
				Assert.assertTrue(crporAwesomePrices.isDisplayed(), "CRP or Awesome Prices are not displaying in PDP");
				Reporter.log("CRP or Awesome Prices are displaying in PDP");
			} else {
				getDriver().navigate().to(getDriver().getCurrentUrl() + "&mboxDisable=true");
				waitFor(ExpectedConditions.visibilityOf(findYourPricingInPDP), 60);
				Assert.assertTrue(crporAwesomePrices.isDisplayed(), "CRP or Awesome Prices are not displaying in PDP");
				Reporter.log("CRP or Awesome Prices are displaying in PDP");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify CRP or Awesome Prices in PDP");
		}
		return this;
	}

	/**
	 * verify Your Pricing Box in PDP
	 *
	 */
	public PdpPage verifyYourPricingBoxInPDP() {
		try {
			if (isElementDisplayed(findYourPricingInPDP)) {
				Assert.assertTrue(yourPricingBoxInPDP.isDisplayed(), "Your Pricing Box is not displaying in PDP");
				Reporter.log("Find Your Pricing Box is displaying in PDP");
			} else {
				getDriver().navigate().to(getDriver().getCurrentUrl() + "&mboxDisable=true");
				waitFor(ExpectedConditions.visibilityOf(findYourPricingInPDP), 60);
				Assert.assertTrue(yourPricingBoxInPDP.isDisplayed(), "Your Pricing Box is not displaying in PDP");
				Reporter.log("Find Your Pricing Box is displaying in PDP");

			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Box in PDP");
		}
		return this;
	}

	/**
	 * verify Your Pricing Box text in PDP
	 *
	 */
	public PdpPage verifyYourPricingBoxTextInPDP() {
		try {
			Assert.assertTrue(yourPricingBoxTextPDP.isDisplayed(), "Your Pricing Box text is not displaying in PDP");
			Reporter.log("Find Your Pricing Box text is displaying in PDP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Box text in PDP");
		}
		return this;
	}

	/**
	 * verify Find Your Pricing Link in PDP
	 *
	 */
	public PdpPage verifyFindYourPricingLinkInPDP() {
		try {
			if (isElementDisplayed(findYourPricingInPDP)) {
				waitFor(ExpectedConditions.visibilityOf(findYourPricingInPDP), 60);
				Assert.assertTrue(findYourPricingInPDP.isDisplayed(),
						"Find Your Pricing Link is not displaying in PDP");
				Reporter.log("Find Your Pricing Link is displaying in PDP");
			} else {
				getDriver().navigate().to(getDriver().getCurrentUrl() + "&mboxDisable=true");
				waitFor(ExpectedConditions.visibilityOf(findYourPricingInPDP), 60);
				Assert.assertTrue(findYourPricingInPDP.isDisplayed(),
						"Find Your Pricing Link is not displaying in PDP");
				Reporter.log("Find Your Pricing Link is displaying in PDP");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Link in PDP");
		}
		return this;
	}

	/**
	 * Click Find Your Pricing Link in PDP
	 *
	 */
	public PdpPage clickFindYourPricingLinkInPDP() {
		try {
			if (isElementDisplayed(findYourPricingInPDP)) {
				waitFor(ExpectedConditions.visibilityOf(findYourPricingInPDP), 60);
				clickElementWithJavaScript(findYourPricingInPDP);
				Reporter.log("Clicked on Find Your Pricing Link");
			} else {
				getDriver().navigate().to(getDriver().getCurrentUrl() + "&mboxDisable=true");
				waitFor(ExpectedConditions.visibilityOf(findYourPricingInPDP), 60);
				clickElementWithJavaScript(findYourPricingInPDP);
				Reporter.log("Clicked on Find Your Pricing Link");
			}

		} catch (Exception e) {
			Assert.fail("Failed to Click Find Your Pricing Link in PDP");
		}
		return this;
	}

	/**
	 * Verifies presence of Find your pricing link disable
	 * 
	 * @return
	 */
	public PdpPage verifyFindYourPricingDisable() {
		try {
			Assert.assertFalse(isElementDisplayed(findYourPricingInPDP), "Find Your Pricing is displaying in PDP.");
		} catch (Exception e) {
			Assert.fail("Find Your Pricing is displaying in PDP");
		}
		return this;
	}

	/**
	 * Verify '+ $25.00 SIM starter kit' to not display on PDP for EIP
	 * 
	 * @return
	 */
	public PdpPage verifySSKFeeMessageDisableOnPDPForEIP() {
		try {
			Assert.assertFalse(isElementDisplayed(sskFeeMessageforEIP),
					"'+ $25.00 SIM starter kit' message is displaying on PDP for EIP");
			Reporter.log("'+ $25.00 SIM starter kit' is not diplaying on PDP for EIP");
		} catch (Exception e) {
			Assert.fail("'+ $25.00 SIM starter kit' is not diplaying on PDP for EIP");
		}
		return this;
	}

	/**
	 * Verify '+ $25.00 SIM starter kit' to not display on PDP for FRP
	 * 
	 * @return
	 */
	public PdpPage verifySSKFeeMessageDisableOnPDPForFRP() {
		try {
			Assert.assertFalse(isElementDisplayed(sskFeeMessageforFRP),
					"'+ $25.00 SIM starter kit' message is displaying on PDP for FRP");
			Reporter.log("'+ $25.00 SIM starter kit' is not diplaying on PDP for FRP");
		} catch (Exception e) {
			Assert.fail("'+ $25.00 SIM starter kit' is not diplaying on PDP for FRP");
		}
		return this;
	}

	/**
	 * Verify Continue as guest CTA for New Customer from Self Identification Modal
	 */
	public PdpPage verifyContinueAsGuestCTA() {
		try {
			guestOrLogInCta.get(0).isDisplayed();
			String existingCustomer = guestOrLogInCta.get(0).getText();
			if (existingCustomer.equalsIgnoreCase(prospectCustomer)) {
				Reporter.log("Continue as guest CTA Displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Continue as guest CTA");
		}
		return this;
	}

	/**
	 * Verify Log In CTA for Existing Customer from Self Identification Modal
	 */
	public PdpPage verifyLogInCTAforExistingCustomer() {
		try {
			guestOrLogInCta.get(1).isDisplayed();
			String logIn = guestOrLogInCta.get(1).getText();
			if (logIn.equalsIgnoreCase(existingCustomer)) {
				Reporter.log("Log In CTA is Displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Log In CTA");
		}
		return this;
	}

	/**
	 * Verify Promotion Text Displayed on PDP
	 * 
	 */
	public PdpPage verifyPromotionTextonPLP() {
		try {
			assertTrue(promotionText.isDisplayed(), "Promotion Text is not diplayed for device");
			Reporter.log("Promotion Text is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Promotion Text for any device");
		}
		return this;
	}

	/**
	 * Click Promotion Text for device on PDP
	 * 
	 */
	public PdpPage clickPromotionTextonPLP() {
		try {
			promotionText.click();
			Reporter.log("Promotion Text is clicked");
		} catch (Exception ex) {
			Assert.fail("Failed to click Promotion Text for any device");

		}
		return this;
	}

	/**
	 * Verify Promotion Modal
	 * 
	 */
	public PdpPage verifyPromotionModal() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(promotionModal), 60);
			assertTrue(promotionModal.isDisplayed(), "Promotion Modal is not diplayed");
			Reporter.log("Promotion Modal is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Promotion Modal");

		}
		return this;
	}

	/**
	 * Verify Device name On Promotion Modal
	 * 
	 */
	public PdpPage verifyDeviceNameOnPromotionModal() {
		try {
			assertTrue(deviceNameOnPromotionModal.isDisplayed(), "Device name on Promotion Modal is not diplayed");
			Reporter.log("Device name on Promotion Modal is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Device name on Promotion Modal");
		}
		return this;
	}

	/**
	 * Verify Promotion Text On Promotion Modal
	 * 
	 */
	public PdpPage verifyPromotionTextOnPromotionModal() {
		try {
			checkPageIsReady();
			assertTrue(promotionTextOnPromotionModal.isDisplayed(),
					"Promotion Text on Promotion Modal is not diplayed");
			Reporter.log("Promotion Text on Promotion Modal is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Promotion Text on Promotion Modal");

		}
		return this;
	}

	/**
	 * verify FRP Price After Clicking On Pay In Full link
	 * 
	 * @return
	 */
	public PdpPage verifyFRPPriceAfterClickingOnPayInFull() {
		try {
			Assert.assertTrue(frpPriceAfterClickonPayInFull.isDisplayed(),
					"FRP price is not displaying on PDP after clicking on Pay in Full.");
			Reporter.log("FRP price is displaying on PDP after clicking on Pay in Full.");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP Price After Clicking On Pay In Full link");
		}
		return this;
	}

	/**
	 * Verify device Image in PDP
	 *
	 */
	public String getDeviceNameAtPDPPage() {
		String name = null;
		try {
			name = deviceName.getText();
			Reporter.log("Device Name is taken is" + name);
		} catch (Exception e) {
			Assert.fail("Failed to verify Device device name");
		}
		return name;
	}

	/**
	 * verify Sim Kit Price
	 * 
	 * @return
	 */
	public PdpPage verifySimKitPrice() {
		try {
			Assert.assertTrue(frpPriceAfterClickonPayInFull.isDisplayed(), "SIM Kit price is not displaying");
			Assert.assertTrue(frpPriceAfterClickonPayInFull.getText().contains("$10"),
					"SIM Kit price is not equal to $10");
			Reporter.log("SIM Kit price is visible");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sim kit price");
		}
		return this;
	}

	/**
	 * Verify Sim kit name for devices
	 * 
	 */
	public PdpPage verifySimKitName() {
		try {
			checkPageIsReady();
			assertTrue(simKitName.getText().contains("SIM Card"), "Sim Kit Name is not diplayed");
			Reporter.log("Sim Kit Name is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Sim Kit Name");

		}
		return this;
	}

	/**
	 * Compare Two Values does not match
	 *
	 */
	public PdpPage compareTwoDoubleValuesNotMatch(String firstValue, String secondValue) {
		try {
			Assert.assertNotEquals(Double.parseDouble(firstValue), Double.parseDouble(secondValue),
					"The prices are same");
			Reporter.log("Prices are not equal/ Prices have now changed");
		} catch (Exception e) {
			Assert.fail("Failed to compare 2 prices");
		}
		return this;
	}

	/**
	 * Compare Two Values does not match
	 *
	 */
	public PdpPage compareTwoStringValues(String firstValue, String secondValue) {
		try {
			Assert.assertNotEquals(firstValue, secondValue, "The prices are same");
			Reporter.log("Prices are not equal/ Prices have now changed");
		} catch (Exception e) {
			Assert.fail("Failed to compare 2 prices");
		}
		return this;
	}

	/**
	 * Click on How does pre-order work? link CTA in PDP
	 */
	public PdpPage clickOnHowPreOrderWorkTextLink() {
		try {
			moveToElement(howDoesPreOrderWorkLink);
			howDoesPreOrderWorkLink.click();
			Reporter.log("Clicked on How does Pre Order work link");
		} catch (Exception e) {
			Assert.fail("Failed to click on How does Pre Order work link");
		}
		return this;
	}

	/**
	 * Verify 'How does pre-order work?' link
	 *
	 */
	public PdpPage verifyHowPreOrderWorkTextLink() {
		try {
			Assert.assertTrue(howDoesPreOrderWorkLink.isDisplayed(),
					"'How Does PreOrder Work?' Link is not displaying");
			Reporter.log("'How Does PreOrder Work?' link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display 'How Does PreOrder Work?' link");
		}
		return this;
	}

	/**
	 * Verify Pre-Order Model
	 *
	 */
	public PdpPage verifyHowPreOrderWorksModel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(preOrderModelWindow), 60);
			Assert.assertTrue(preOrderModelWindow.isDisplayed(), "Pre-Order model window is not displaying");
			Reporter.log("Pre-Order model window is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Pre-Order model window");
		}
		return this;
	}

	/**
	 * Verify Pre-Order model window header
	 *
	 */
	public PdpPage verifyHowPreOrderWorksModelHeader() {
		try {
			Assert.assertTrue(preOrderModelWindowHeader.isDisplayed(),
					"Pre-Order model window header is not displaying");
			Reporter.log("Pre-Order model window header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Pre-Order model window header");
		}
		return this;
	}

	/**
	 * select memory option
	 */

	public PdpPage selectMemoryOption(String memory) {
		try {
			if (memory != null && memoryVariant.size() > 1) {
				moveToElement(memoryVariant.get(0));
				for (WebElement memoryOption : memoryVariant) {
					if (memoryOption.getText().trim().contains(memory)) {
						memoryOption.click();
						Reporter.log("Selected next memory: " + memoryOption.getText().trim());
						break;
					}
				}
			} else {
				Reporter.log("Selected default memory");
			}

		} catch (Exception e) {
			Assert.fail("Failed to select next memory option: " + memory);
		}
		return this;
	}

	/**
	 * select memory option
	 */

	public PdpPage selectColorOption(String color) {
		try {
			if (color != null && colorVariant.size() > 0) {
				moveToElement(colorVariant.get(0));
				color = color.toLowerCase();
				for (WebElement colorOption : colorVariant) {
					if (colorOption.getAttribute("title").toLowerCase().contains(color)) {
						moveToElement(colorOption);
						clickElementWithJavaScript(colorOption);
						Reporter.log("Selected color: " + color);
						break;
					}
				}
			} else {
				Reporter.log("Selected default color option");
			}
		} catch (Exception e) {
			Assert.fail("Failed to select next color option: " + color);
		}
		return this;
	}

	/**
	 * verify Device SKU with Color and Memory
	 */

	public PdpPage verifyDeviceSKUwithColorandMemory(String skus, String colorOptions, String memoryOptions) {
		try {
			PdpPage pdpPage = new PdpPage(getDriver());
			String listofSKU[] = skus.split(",");
			String listofDeviceColorOptions[] = colorOptions.split(",");
			String listofDeviceMemoryOptions[] = memoryOptions.split(",");
			((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,200)");
			for (int i = 0; i < listofSKU.length; i++) {
				pdpPage.selectMemoryOption(listofDeviceMemoryOptions[i]);
				pdpPage.selectColorOption(listofDeviceColorOptions[i]);
				Assert.assertTrue(
						selectedColor.getAttribute("data-analytics-value")
								.equalsIgnoreCase(listofDeviceColorOptions[i]),
						"Color option is not selected properly. Should be: " + listofDeviceColorOptions[i]
								+ ", but found: " + selectedColor.getAttribute("data-analytics-value"));
				Assert.assertTrue(
						selectedMemory.getAttribute("data-analytics-value")
								.equalsIgnoreCase(listofDeviceMemoryOptions[i]),
						"Memory option is not selected properly. Should be: " + listofDeviceMemoryOptions[i]
								+ ", but found: " + selectedMemory.getAttribute("data-analytics-value"));
				Assert.assertTrue(getDriver().getCurrentUrl().contains(listofSKU[i]),
						"SKU: '" + listofSKU[i] + "' is not matching with color: '" + listofDeviceColorOptions[i]
								+ "' and memory option: '" + listofDeviceMemoryOptions[i] + "'");
				Reporter.log("SKU is matching with color: " + listofDeviceColorOptions[i] + " and memory option: "
						+ listofDeviceMemoryOptions[i]);
			}
			Reporter.log("SKU's are matching with color and memory");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device SKU with color and memory");
		}
		return this;
	}

	/**
	 * Click Phones Link in PDP
	 */
	public PdpPage clickPhonesLinkInPDP() {
		try {

			clickElementWithJavaScript(phonesLink);
		} catch (Exception e) {
			Assert.fail("Failed to click phones Link in PDP");
		}
		return this;
	}

	/**
	 * Click Phones Link in PDP
	 */
	public PdpPage clickWatchesLinkInPDP() {
		try {
			clickElementWithJavaScript(watchesLink);
		} catch (Exception e) {
			Assert.fail("Failed to click Watches Link in PDP");
		}
		return this;
	}

	/**
	 * Verify Select user modal
	 */
	public PdpPage verifySelectUserModal() {
		try {
			Assert.assertTrue(selectUserModal.isDisplayed(), "Select user modal is not displaying");
			Reporter.log("Select user modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display select user modal");
		}
		return this;
	}

	/**
	 * Click on Existing user option in Select user modal
	 */
	public PdpPage clickExistingUserOptionInSelectUserModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(existingUserOption), 60);
			clickElementWithJavaScript(existingUserOption);
			// existingUserOption.click();
			Reporter.log("Existing User option is clickable");
		} catch (Exception e) {
			Assert.fail("Failed to click Existing user option");
		}
		return this;
	}

}