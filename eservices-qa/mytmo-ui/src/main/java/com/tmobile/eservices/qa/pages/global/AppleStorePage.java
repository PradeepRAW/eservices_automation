package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class AppleStorePage extends CommonPage {

	private static final String pageUrl = "apple";

	@FindBy(css = "p[class='Display6']")
	private List<WebElement> profilePageLinks;

	public AppleStorePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Apple store page.
	 *
	 * @return the AppleStorePage class instance.
	 */
	public AppleStorePage verifyAppleStorePage() {
		try {
			verifyPageUrl(pageUrl);
			Reporter.log("Apple store page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Apple store page not displayed");
		}
		return this;
	}
}
