/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class ChangeSimPage extends CommonPage {

	@FindBy(css = "div.padding-vertical-large h6.text-gray-dark")
	private WebElement changeSimPage;

	@FindBy(css = "input#idNextButton")
	private WebElement NextBtn;

	@FindBy(css = "div#ui_callrecords")
	private WebElement selectALineDropDown;

	@FindBy(css = "span#di_CDRMsisdn7")
	private WebElement selectDigitsLine;

	@FindBy(css = "div#invalidForSimswap .ptextborder")
	private WebElement simSwapMessage;

	@FindBy(css = "span#di_CDRMsisdn6")
	private WebElement selectSixthLine;

	@FindBy(css = "span#di_CDRMsisdn2")
	private WebElement selectSecondLine;
	
	@FindBy(css = "div#di_lineselector[style*='cursor']")
	private WebElement lineSelectorDropDownDiv;
	
	@FindBy(css = "#di_lineMsisdn")
	private WebElement retainedLine;
	
	

	public ChangeSimPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Change SIM Page
	 * 
	 * @return
	 */
	public ChangeSimPage verifyChangeSimPage() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div#device_loader")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div#device_waitCursor")));
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains("myphone/changesim"),"Change sim page not displayed");
			Reporter.log("Change sim page is displayed");
		} catch (AssertionError e) {
			Reporter.log("Change sim page not displayed");
		}
		return this;
	}

	/**
	 * Click next button
	 */
	public ChangeSimPage clickNextBtn() {
		waitforSpinner();
		clickElementWithJavaScript(NextBtn);
		return this;
	}

	/**
	 * Select digits line
	 */
	public ChangeSimPage selectDigitsLine() {
		waitforSpinner();
		selectALineDropDown.click();
		checkPageIsReady();
		clickElementWithJavaScript(selectDigitsLine);
		return this;
	}

	/**
	 * Verify sim swap message
	 */
	public ChangeSimPage verifySimSwapMessage() {
		waitforSpinner();
		try {
			simSwapMessage.isDisplayed();
			Reporter.log("Sim swap cannot be performed message displayed");
		} catch (AssertionError e) {
			Reporter.log("Sim swap cannot be performed message not displayed");
			Assert.fail("Sim swap cannot be performed message not displayed");
		}
		return this;
	}
	
	/**
	 * Verify disabled line selector div
	 */
	public ChangeSimPage verifydisabledLineSelectorDropDown() {
		waitforSpinner();
		try {
			Assert.assertFalse(isElementDisplayed(lineSelectorDropDownDiv),"Line selector div is not in disabled state");
			Reporter.log("Line selector div is in disabled state");
		} catch (AssertionError e) {
			Reporter.log("Line selector div is not in disabled state");
			Assert.fail("Line selector div is not in disabled state");
		}
		return this;
	}
	
	/**
	 * Verify line selector div
	 */
	public ChangeSimPage verifyLineSelectorDropDown() {
		waitforSpinner();
		try {
			Assert.assertTrue(isElementDisplayed(lineSelectorDropDownDiv),"Line selector div not displayed");
			Reporter.log("Line selector div displayed");
		} catch (AssertionError e) {
			Reporter.log("Line selector div not displayed");
			Assert.fail("Line selector div not displayed");
		}
		return this;
	}
	
	/**
	 * Verify line selector div
	 */
	public ChangeSimPage verifyUndisplayLineSelectorDropDown() {
		waitforSpinner();
		try {
			Assert.assertFalse(isElementDisplayed(lineSelectorDropDownDiv),"Line selector div displayed");
			Reporter.log("Line selector div not displayed");
		} catch (AssertionError e) {
			Reporter.log("Line selector div displayed");
			Assert.fail("Line selector div displayed");
		}
		return this;
	}

	/**
	 * Select sixth line
	 */
	public ChangeSimPage selectSixthLine() {
		waitforSpinner();
		selectALineDropDown.click();
		checkPageIsReady();
		clickElementWithJavaScript(selectSixthLine);
		return this;
	}

	/**
	 * Select second line
	 */
	public ChangeSimPage selectSecondLine() {
		waitforSpinner();
		selectALineDropDown.click();
		checkPageIsReady();
		clickElementWithJavaScript(selectSecondLine);
		return this;
	}
	

	/**
	 * Verify selected line
	 */
	public ChangeSimPage verifySelectedLine(String selectedLine) {
		waitforSpinner();
		try {
			String actualLine=retainedLine.getText().trim();
			Assert.assertEquals(selectedLine,actualLine);
			Reporter.log("Selected line retained in the change sim page");
		} catch (AssertionError e) {
			Reporter.log("Selected line not retained in the change sim page");
			Assert.fail("Selected line not retained in the change sim page");
		}
		return this;
	}

	/**
	 * Verify deeplink for un-authorized user
	 */
	public ChangeSimPage deeplink(String deepLink) {
		getDriver().get(deepLink);
		return this;
	}
}
