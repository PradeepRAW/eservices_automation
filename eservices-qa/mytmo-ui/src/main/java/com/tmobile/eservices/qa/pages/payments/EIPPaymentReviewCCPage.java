package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class EIPPaymentReviewCCPage extends CommonPage {

	@FindAll({ @FindBy(css = ".ui_primary_link.pay-info-link"), @FindBy(id = "editEipPayment") })
	private List<WebElement> editPayment;

	/* @FindBy(css = "div.ui_headline.comfirmation-message") */
	// @FindBy(xpath = "//div[contains(text(),'Review payment information')]")
	/* @FindBy(css = "p.ui_mobile_headline") */

	@FindAll({ @FindBy(css = "div.ui_headline.comfirmation-message"), @FindBy(css = "p.ui_mobile_headline"),
			@FindBy(css = "div.co_checkingpayment") })
	private WebElement verifyReviewPaymentPage;
	/*
	 * @FindBy(css = "div.ui_headline.comfirmation-message") private WebElement
	 * verifyPageLoaded;
	 */

	@FindBy(id = "checking-submit-btn")
	private WebElement checkingAccountSubmitButton;

	@FindAll({ @FindBy(id = "reviewSubmitButton"), @FindBy(id = "cc-submit-btn") })
	private WebElement creditCardSubmitButton;

	// @FindBy(id = "paymentreviewchecking")
	@FindAll({ @FindBy(id = "paymentreviewchecking"), @FindBy(id = "paymentreviewcredit") })
	private WebElement termsAndConditionBankAcc;

	@FindAll({ @FindBy(id = "reviewIAgreeCheckbox"), @FindBy(id = "paymentreviewcredit") })
	private WebElement termsAndConditionCC;

	@FindBy(css = "span#review_payment_customer_phonenumber nobr")
	private WebElement accoutNumber;

	@FindAll({ @FindBy(xpath = "//span[@id='reviewed_pay-cardnum']//span[2]"),
			@FindBy(xpath = "//span[@id='paymentAccountNo']//span[2]") })
	private WebElement creditCardNumber;

	@FindAll({ @FindBy(id = "paymentZip"), @FindBy(id = "reviewed_pay-zip") })
	private WebElement zipCode;

	@FindBy(css = "div#autopay_credit_message1")
	public WebElement AutoPayMsgReviewPaymentPage;

	@FindBy(id = "cc-cancel-btn")
	private WebElement cancelBtn;

	@FindBy(id = "cancelHubPage")
	private WebElement cancelBtnMobile;

	@FindBy(id = "di_cancelmdyestext")
	private WebElement cancelYesBtn;

	@FindBy(id = "modalCancelHubPageYes")
	private WebElement cancelYesBtnMobile;
	
	
	/*
	 * @Find@FindBy(css = "#ajax_waitCursor > div") private WebElement
	 * paymentDeclineddialogBox;
	 */

	@FindAll({ @FindBy(css = "#ajax_waitCursor > div"), @FindBy(id = "alert_modal") })
	private WebElement paymentDeclineddialogBox;

	@FindBy(id = "paymentConfirmationText")
	private WebElement paymentConfirmationtxt;

	@FindBy(css = "div.co_review > div> div:nth-child(2) > div:nth-child(4) > p > a")
	private WebElement linkDisplayedinTermsandConditions;

	@FindBy(id = "di_MakeAPayment")
	private WebElement makePaymentbtn;

	@FindBy(css = "div.row-fluid.radio-box.radio-checked > div.span11")
	private WebElement balanceUpdates;

	@FindBy(css = "#alert_modal > button")
	private WebElement okBtn;

	@FindBy(id = "easypayreviewcredit")
	private WebElement easyPaytermAndconditions;

	// @FindAll({@FindBy(css="div.container-curve.pay-toward-div div
	// div.ui_body:nth-child(3) span.ui_body_bold"),@FindBy(css =
	// "div.pay-text-container")})
	@FindAll({ @FindBy(css = "div#eipReviewDetails > div.ui_mobile_body >span.ui_mobile_body_bold"),
			@FindBy(css = "div.pay-text-container") })
	private WebElement eipPlanId;

	@FindBy(css = "div.ui_third_tier_headline")
	private WebElement termsAndConditions;

	@FindBy(css = ".ajax_loader")
	private WebElement pageSpinner;

	@FindBy(css = "#review-device-title")
	private WebElement pageLoadElement;

	private final String pageUrl = "/eippaymentreviewcc.html";

	@FindBy(id = "di_MakeAPaymentLinkAgain")
	private WebElement paymentFailedLinkOnModal;
	
	
	@FindBy(id = "review-msisdn")
	private WebElement phoneNumber;
	
	@FindBy(id = "review-name")
	private WebElement customerName;
	
	@FindBy(css="span#review-account>span.sprite+span")
	private WebElement paymentInfo;
	
	@FindBy(id="review-zip")
	private WebElement zipcode;
	
	
	@FindBy(id="review-tmoAccount")
	private WebElement banAccountNumber;
	

	/**
	 * 
	 * @param webDriver
	 */
	public EIPPaymentReviewCCPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public EIPPaymentReviewCCPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public EIPPaymentReviewCCPage verifyPageLoaded() {
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
           // waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
            verifyPageUrl();
            verifyReviewPaymentPage.isDisplayed();
            Reporter.log("EIP Payment Review CC page is displayed.");
        }catch(Exception e){
            Assert.fail("Fail to load EIP Payment Review CC page");
        }
        return this;
    }

	/**
	 * Verify Review Payment Page
	 * 
	 * @return
	 */
//    public EIPPaymentReviewCCPage verifyReviewPaymentPageold() {
//        try {
//            waitFor(ExpectedConditions.visibilityOf(verifyPageLoaded));
//            Assert.assertTrue(verifyPageLoaded.isDisplayed(), "Review Payment Page is not displayed.");
//            Reporter.log("Review Payment Page is displayed.");
//        } catch (Exception e) {
//            Assert.fail("verifyReviewPayment element not displayed");
//        }
//        return this;
//
//    }

	/**
	 * Click Terms And Conditions [Checking Account]
	 */
	public EIPPaymentReviewCCPage checkCheckingaAccountTermsAndConditions() {
		try {
			termsAndConditionBankAcc.click();
			Reporter.log("Clicked on checking account terms and conditions");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");

		}
		return this;
	}

	/**
	 * Click Terms And Conditions [Credit Card]
	 */
	public EIPPaymentReviewCCPage clickCCTermsAndConditions() {
		try {
			termsAndConditionCC.click();
			Reporter.log("Credit card terms and conditions displayed");
			Reporter.log("Submit button is enabed.");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Click Submit Button
	 */
	public EIPPaymentReviewCCPage clickCheckingAccountSubmitButton() {
		try {
			checkingAccountSubmitButton.click();
			Reporter.log("Clicked on Submit button");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Click Submit Button
	 */
	public EIPPaymentReviewCCPage clickCreditCardSubmitButton() {
		try {
			creditCardSubmitButton.click();
			Reporter.log("Clicked on Submit button");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * 
	 * @return
	 */
	public EIPPaymentReviewCCPage verifyAccountNumber() {
		try {
			accoutNumber.getText().replaceAll("[^0-9]", "");
			Reporter.log("Account number exists");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Verify Credit Card Number
	 * 
	 * @param creditCardNo
	 * @return
	 */
	public EIPPaymentReviewCCPage verifyCreditCard(String creditCardNo) {
		try {
			waitFor(ExpectedConditions.visibilityOf(creditCardNumber));
			if (creditCardNo.contains(creditCardNumber.getText().replaceAll("[^0-9]", ""))) {
				Reporter.log("Credit card number displayed");
			}
		} catch (Exception e) {
			Assert.fail("Credit card number not displayed");
		}
		return this;
	}

	/**
	 * verify Zip code
	 * 
	 * @return
	 */
	public EIPPaymentReviewCCPage verifyZipcode() {
		try {
			zipCode.getText();
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * click Edit Payment Link
	 */
	public EIPPaymentReviewCCPage clickEditPaymentLink() {
		try {
			editPayment.get(0).click();
			Reporter.log("Clicked on OK button");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Verify Payment Declined Dialog box is displayed
	 * 
	 * @return
	 */
	public EIPPaymentReviewCCPage verifyPayemntdeclineDialogbox() {
		try {
			paymentDeclineddialogBox.isDisplayed();
			Reporter.log("Payment decline dialog box displayed");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	public EIPPaymentReviewCCPage verifyEIPPartialPayemntSuccess() {
		try {
			if (paymentConfirmationtxt.isDisplayed()) {
				Reporter.log("EIP Partial Payment displayed");
			}
		} catch (ElementNotVisibleException ElementNotFound) {
			if (paymentDeclineddialogBox.isDisplayed()) {
				Reporter.log("EIP Partial Payment not displayed");
			}
		} catch (Exception excp) {
			Reporter.log("EIP Partial Payment step FAILED");
		}
		return this;
	}

	public EIPPaymentReviewCCPage getTextpaymentConfirmationMessage() {
		try {
			paymentConfirmationtxt.getText();
			Reporter.log("Confirmation message displayed");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Click Link on Terms on conditions
	 */
	public EIPPaymentReviewCCPage clickLinkTermsandCondition() {
		try {
			linkDisplayedinTermsandConditions.click();
			Reporter.log("Clicked on Link terms and conditions");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Click MakePayment Button
	 */
	public EIPPaymentReviewCCPage clickMakepaymentBtn() {
		try {
			makePaymentbtn.click();
			Reporter.log("Clicked on make a payment button");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Verify Balance Updates is displayed
	 * 
	 * @return
	 */
	public EIPPaymentReviewCCPage verifyBalanceupdatesDisplayed() {
		try {
			balanceUpdates.isDisplayed();
			Reporter.log("Balance updates displayed");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Click OK Button
	 */
	public EIPPaymentReviewCCPage clickOKbtn() {
		try {
			okBtn.click();
			Reporter.log("Clicked on OK button");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * Click Easy pay Terms and Conditions
	 */
	public EIPPaymentReviewCCPage clickEasypayTermsandCondition() {
		try {
			Thread.sleep(1000);
			easyPaytermAndconditions.sendKeys(Keys.TAB);
			easyPaytermAndconditions.click();
			Reporter.log("Clicked on payterms and conditions");
		} catch (Exception sleepException) {
			System.out.println("Error occured with Thread.sleep method." + sleepException.getMessage());
		}
		return this;
	}

	/**
	 * verify EIP plan ID is displayed
	 * 
	 * @return value
	 */
	public EIPPaymentReviewCCPage verifyEIPPlanIdDisplayed() {
		try {
			if (eipPlanId.isDisplayed()) {
				eipPlanId.getText();
			}
		} catch (Exception elemNotFound) {
			Assert.fail("Element is not diaplyed");
		}
		return this;
	}
	// return getText(eipPlanId);

	/**
	 * verify terms and conditions are displayed
	 * 
	 * @return
	 */
	public EIPPaymentReviewCCPage verifyTermsAndConditions() {
		try {
			termsAndConditions.isDisplayed();
			Reporter.log("Review payment page terms and conditions displayed");
		} catch (Exception e) {
			Assert.fail("Review payment page terms and conditions not found");
		}
		return this;
	}

	/**
	 * verify that the payment failed modal is displayed
	 */
	public void verifyPaymentFailedModal() {
		try {
			paymentFailedLinkOnModal.isDisplayed();
			Reporter.log("Payment Failed modal is displayed");
		} catch (Exception e) {
			Assert.fail("Payment Failed modal is displayed");
		}
	}

	/**
	 * Click 'try again' link on Payment Failed modal 
	 */
	public void clickPaymentFailedModal() {
		try {
			paymentFailedLinkOnModal.click();
			Reporter.log("Clicked 'try again' link on Payment Failed modal");
		} catch (Exception e) {
			Assert.fail("try again link on Payment Failed modal is not found");
		}
	}
	
	
	  
    /**
	 * Verify PII details for PhoneNumber
	 */
	public void verifyPIIforPhoneNumber(String misdin) {
			
		try {
			Assert.assertTrue(checkElementisPIIMasked(phoneNumber, misdin),"No PII masking for Selected Line");
			Reporter.log("PII Masking for Phone Number verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the Phone Number");
		}
		
	}
		
	
	 /**
	 * Verify PII details for CustomerName
	 */
	public void  verifyPIIforCustomerName(String customername) {
		
		try {
			Assert.assertTrue(checkElementisPIIMasked(customerName, customername),"No PII masking for Selected Line");
			Reporter.log("PII Masking for Customer Name verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the Customer Name");
		}
		
	}
		
	
	
	
	 /**
		 * Verify PII details for PaymentInfo
		 */
		public void verifyPIIforPaymentInfo(String paymentinfo) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(paymentInfo, paymentinfo),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Payment Info verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Payment Info");
			}
			
		}
			
	
		 /**
		 * Verify PII details for Zip Code
		 */
		public void verifyPIIforZipCode(String zipCode) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(zipcode, zipCode),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Zip Code verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Zip Code");
			}
			
		}
			
	
	
		/**
		 * Verify PII details for BAN Number
		 */
		public void verifyPIIforBanNumber(String banNumber) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(banAccountNumber, banNumber),"No PII masking for Selected Line");
				Reporter.log("PII Masking for BAN Number verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the BAN Number");
			}
			
		}
			
	
}
