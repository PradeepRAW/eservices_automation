/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class InterstitialTradeInPage extends CommonPage {

	@FindBy(css = "[ng-bind*='interstitialTradeinHeaderText']")
	private WebElement tradeInAnotherDeviceHeader;
	
	@FindBy(css = "#tradein [ng-bind*='tradeInDeviceCondition.optionHeader']")
	private WebElement tradeInDeviceCTA;
	
	@FindBy(css = "#skiptradein [ng-bind*='tradeInDeviceCondition.optionHeader']")
	private WebElement skipTradeInCTA;
	
	@FindBy(css = "[ng-bind*='interstitialTradeinSubHeaderText']")
	private WebElement wellTellYouText;

	@FindBy(id = "skiptradein")
	private WebElement skipTradeinTile;
	
	@FindBy(id = "tradein")
	private WebElement yesTradeinTile;
	
	@FindBy(css = "[ng-bind*='tradeInDeviceCondition.optionBody']")
	private WebElement howTradeinWorksLink;
	
	@FindBy(css= "#tradein .img-width-mobile")
	private WebElement tradeInImage;
	
	@FindBy(css= "#tradein [ng-bind*='tradeInDeviceCondition.optionSubHeader']")
	private WebElement tradeInBodyText;
	
	@FindBy(css = "#skiptradein div div img")
	private WebElement authorableImageSkipTradeinTile;
	
	@FindBy(css = "#tradein [ng-bind-html*='howTradeinworksModalData']")
	private WebElement howTradeinworksModalHeader;
	
	@FindBy(css = "#tradein [ng-repeat*='howTradeinworksModalData'] p")
	private List<WebElement> howTradeinworksModalText;
	
	@FindBy(css = "#tradein .close")
	private WebElement closeIcon;
	
	public InterstitialTradeInPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Interstitial Trade In Page
	 * 
	 * @return
	 */
	public InterstitialTradeInPage verifyInterstitialTradeInPage() {
		try {
			checkPageIsReady();
			waitforSpinner();
			verifyPageUrl();
			Reporter.log("Navigated to Interstitial Trade In Page");
		} catch (Exception e) {
			Assert.fail("Interstitial Trade In Page is Not Displayed");
		}
		return this;
	}
	
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public InterstitialTradeInPage verifyPageUrl() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("interstitial-tradein"),
					"URL for Interstitial Page is not correct");
		} catch (Exception e) {
			Assert.fail("Failed to verify Interstitial Trade-In url");
		}
		return this;
	}
	
	
	/**
	 * Click on Skip Trade In CTA
	 */
	public InterstitialTradeInPage clickOnSkipTradeInCTA() {
		waitforSpinner();
		try {
			clickElementWithJavaScript(skipTradeInCTA);
			Reporter.log("Skip Trade in CTA is clicked on interstial page");
		} catch (Exception e) {
			Assert.fail("Skip trade in Button is not Displayed to CLick");
		}
		return this;
	}
	
	
	/**
	 * Click on Yes, Want To Trade In CTA
	 */
	public InterstitialTradeInPage clickOnYesWantToTradeInCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(tradeInDeviceCTA));
			clickElementWithJavaScript(tradeInDeviceCTA);
			Reporter.log("Yes, Want To Trade In CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Yes, Want To Trade In CTA is not Displayed to CLick");
		}
		return this;
	}
	
	/**
	 * Verify Interstitial Tradein page title
	 */
	public InterstitialTradeInPage verifyInterstitialPageTitle() {

		try {
			Assert.assertTrue(tradeInAnotherDeviceHeader.isDisplayed(), "Interstitial Tradein page title not displayed");
			Reporter.log("Interstitial Tradein page title is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Interstitial Tradein page title");
		}
		return this;
	}
	
	/**
	 * Verify Authorable Sub header for Interstitial Tradein page
	 */
	public InterstitialTradeInPage verifyAuthorableSubHeaderForInterstitialPage() {

		try {
			Assert.assertTrue(wellTellYouText.isDisplayed(), "Sub Header for Interstitial Tradein page not displayed");
			Reporter.log("Sub Header for Interstitial Tradein page is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Sub Header for Interstitial Tradein page");
		}
		return this;
	}
	
	/**
	 * Verify Yes Tradein Tile for Interstitial Tradein page
	 */
	public InterstitialTradeInPage verifyYesTradeinTile() {

		try {
			Assert.assertTrue(yesTradeinTile.isDisplayed(), "Yes Tradein Tile for Interstitial Tradein page not displayed");
			Reporter.log("Yes Tradein Tile for Interstitial Tradein page is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Yes Tradein Tile for Interstitial Tradein page");
		}
		return this;
	}
	
	/**
	 * Verify Skip Tradein Tile for Interstitial Tradein page
	 */
	public InterstitialTradeInPage verifySkipTradeinTile() {

		try {
			Assert.assertTrue(skipTradeinTile.isDisplayed(), "Skip Tradein Tile for Interstitial Tradein page not displayed");
			Reporter.log("Skip Tradein Tile for Interstitial Tradein page is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Skip Tradein Tile for Interstitial Tradein page");
		}
		return this;
	}
	
	/**
	 * Verify How Tradein Works Link for Interstitial Tradein page
	 */
	public InterstitialTradeInPage verifyHowTradeinWorksLink() {

		try {
			Assert.assertTrue(howTradeinWorksLink.isDisplayed(), "How Tradein Works Link is not displayed");
			Reporter.log("How Tradein Works Link is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display How Tradein Works Link");
		}
		return this;
	}
	
	/**
	 * Verify Authorable Interstitial Tradein image
	 */
	public InterstitialTradeInPage verifyAuthorableTradeInImage() {

		try {
			waitFor(ExpectedConditions.visibilityOf(tradeInImage));
			Assert.assertTrue(tradeInImage.isDisplayed(), "Authorable Interstitial Tradein image not displayed");
			Reporter.log("Authorable Interstitial Tradein image is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Authorable Interstitial Tradein image");
		}
		return this;
	}
	
	/**
	 * Verify Authorable Interstitial Tradein body text
	 */
	public InterstitialTradeInPage verifyAuthorableTradeInBodyText() {

		try {
			waitFor(ExpectedConditions.visibilityOf(tradeInBodyText));
			Assert.assertTrue(tradeInBodyText.isDisplayed(), "Authorable Interstitial Tradein body text not displayed");
			Reporter.log("Authorable Interstitial Tradein body text is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Authorable Interstitial Tradein body text");
		}
		return this;
	}
	
	/**
	 * Verify Skip Trade In CTA authorable text
	 */
	public InterstitialTradeInPage authorableTextSkipTradeInTile() {
		try {
			assertEquals(skipTradeInCTA.getText(), "No thanks, skip trade-in.");
			Reporter.log("Skip Trade In CTA authorable text is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Skip Trade In CTA authorable text");
		}
		return this;
	}
	
	/**
	 * Verify Authorable image for Skip Tradein tile
	 */
	public InterstitialTradeInPage verifyAuthorableImageSkipTradein() {

		try {
			waitFor(ExpectedConditions.visibilityOf(authorableImageSkipTradeinTile));
			Assert.assertTrue(authorableImageSkipTradeinTile.isDisplayed(), "Authorable image for Skip Tradein is not displayed");
			Reporter.log("Authorable image for Skip Tradein tile is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display Authorable image for Skip Tradein tile");
		}
		return this;
	}
	
	/**
	 * Click On How Trade in work link
	 */
	public InterstitialTradeInPage clickHowTradeinWorksLink() {

		try {
			clickElementWithJavaScript(howTradeinWorksLink);
			Reporter.log("How Tradein Works Link is Clicked");
		}catch (Exception e){
			Assert.fail("Failed to Click on How Tradein Works Link");
		}
		return this;
	}
	
	/**
	 * Verify How Tradein Works Link Modal Header
	 */
	public InterstitialTradeInPage verifyHowTradeinWorksModalHeader() {
			checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(howTradeinworksModalHeader));
			Assert.assertTrue(howTradeinworksModalHeader.isDisplayed(), "How Tradein Works Modal Header is not displayed");
			Reporter.log("How Tradein Works Modal Header is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display How Tradein Works Modal Header");
		}
		return this;
	}
	
	/**
	 * Verify How Tradein Works Link Modal Text
	 */
	public InterstitialTradeInPage verifyHowTradeinWorksModalText() {

		try {
			for (WebElement text : howTradeinworksModalText) {
			
					Assert.assertTrue(text.isDisplayed(), "How Tradein Works Modal Text is not displayed");
			
			}
			Reporter.log("How Tradein Works Modal Text is displayed");
		}catch (Exception e){
			Assert.fail("Failed to display How Tradein Works Modal Text");
		}
		return this;
	}
	
	/**
	 * Click On Close Icon
	 */
	public InterstitialTradeInPage clickCloseIcon() {

		try {
			clickElementWithJavaScript(closeIcon);
			Reporter.log("Close Icon is Clicked");
		}catch (Exception e){
			Assert.fail("Failed to Click on Close Icon");
		}
		return this;
	}
	
	/**
	 * Verify How Tradein Works Link Modal Header
	 */
	public InterstitialTradeInPage verifyHowTradeinWorksModalNotDisplayed() {
			checkPageIsReady();
		try {
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("#tradein .no-margin[ng-if*='sHowTradeinWorksModalVisible']")).size());
			Reporter.log("How Tradein Works Modal is not displayed");
		}catch (Exception e){
			Assert.fail("Failed to check display of How Tradein Works Modal");
		}
		return this;
	}
	
}
