package com.tmobile.eservices.qa.pages.shop;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class PDPPage extends CommonPage {

	public static final String PDP_PAGE_URL = "productdetails";

	@FindBy(css = "h3[ng-bind*='ctrl.deviceDetail.modelName']")
	private WebElement pdpPage;

	// @FindBy(css = ".btn.glueButton.glueButtonPrimary")
	// @FindBy(css = "div[aria-hidden*='true']>button.glueButtonPrimary")
	
	@FindBy(xpath = "//button[text()='Upgrade'] | //button[text()='Add to Cart']")
	private WebElement addToCart;	

	@FindBy(css = "div.modal-background-mobile h2")
	private WebElement downPaymentModelHeader;

	@FindBy(css = "div.modal-background-mobile p[ng-bind*='ctrl.contentData.downPaymentModalTxt']")
	private WebElement downPaymentModelText;

	@FindBy(css = "div.modal-background-mobile p[ng-bind*='ctrl.contentData.downPaymentModalNote']")
	private WebElement downPaymentModelNote;

	@FindBy(linkText = "Accessibility page")
	private WebElement accessibilityPage;

	@FindBy(css = "div[ng-bind-html='$ctrl.contentData.legalTextBottom | displayHTML']")
	private WebElement legelText;

	@FindBy(id = "paymentsDropdown")
	private List<WebElement> paymentOption;

	@FindBy(css = "div.star-rating.p-t-10")
	private WebElement reviewTab;

	@FindBys(@FindBy(css = "a.review-tabs.ng-binding"))
	private List<WebElement> reviewSections;

	@FindBys(@FindBy(css = "ul.dropdown-menu li[role='menuitem'] a[ng-bind='priceOption']"))
	private List<WebElement> paymentOptions;

	@FindBy(css = "p[ng-bind*='ctrl.contentData.memoryLabel']")
	private WebElement memoryHeader;

	@FindBy(css = "div#features-cover")
	private WebElement featuresHeader;

	@FindBy(css = "li.ng-isolate-scope.active")
	private WebElement featuresTab;

	@FindBys(@FindBy(css = "ul.dropdown-menu li a[ng-bind='memoryKey']"))
	private List<WebElement> memoryDropdownOptions;

	@FindBy(xpath = "//a[contains(text(), 'See more')]")
	private WebElement seeMorelink;

	@FindBy(linkText = "See Less")
	private WebElement seeLessLink;

	@FindBy(css = "div.col-xs-12.no-padding.p-b-20 p.legal")
	private WebElement legalText;

	@FindBy(css = "a[ng-bind='$ctrl.contentData.legalModalLink']")
	private WebElement iosSeeLegalDisclaimer;

	@FindBy(css = "span[ng-bind*='ctrl.contentData.colorLabel']")
	private WebElement colorLabel;

	@FindBy(css = "i.fa.fa-close.uib-close")
	private WebElement frpHoldUpModelCancelButton;

	@FindBy(css = "div.text-center.header-box-shadow")
	private WebElement sedonaHeader;

	@FindBy(css = "span[class*='legal-verbiage-text'] span[ng-bind*='offerDollars']")
	private WebElement deviceFRPPrice;

	@FindBy(css = "span[ng-bind='$ctrl.contentData.permissionsError']")
	private WebElement ineligibilityTxt;

	@FindBy(css = "h3[ng-bind='$ctrl.deviceDetail.modelName']")
	private WebElement deviceName;

	@FindBy(css = "div.color-picker span.selected-color")
	private WebElement deviceColor;

	@FindBy(css = "div#idPdpMemoryVariant div.selector.dropdown-toggle span")
	private WebElement deviceMemory;

	@FindBy(css = "div[ng-class*='promo-circle-copy']")
	private WebElement promoBadge;

	@FindBy(css = "span[ng-bind-html*='genericPromoText']")
	private WebElement promoLinkText;

	@FindBys(@FindBy(xpath = "//p[contains(text(),'Offer')]/../../div/span"))
	private List<WebElement> onSaleFilterValue;

	@FindBy(css = "div.pdpPromoDetails")
	private WebElement pdpPromoWindow;

	@FindBy(css = "div.color-picker")
	private WebElement colorPicker;

	@FindBy(css = "span[ng-bind='$ctrl.contentData.monthlyLabel']")
	private WebElement eipMonthlyPriceText;

	@FindBy(css = "div[ng-hide*='$ctrl.contentData.paymentOptions[1]'] sup.dollar-sign")
	private List<WebElement> eIPdollarSign;

	@FindBy(css = "div[ng-hide*='$ctrl.contentData.paymentOptions[0]'] sup.dollar-sign")
	private List<WebElement> fRPDollarSign;

	@FindBy(css = "span[ng-bind='$ctrl.displayNoOfMonthForPDP']")
	private WebElement eipInstallmentMonth;	

	@FindBy(css = "span[ng-bind='$ctrl.monthlyDollars']")
	private WebElement eipMonthlyPrice;

	@FindBy(css = "sup[ng-bind='$ctrl.monthlyCents']")
	private WebElement eipMonthlyPriceInCents;

	@FindBy(css = "span[ng-bind='$ctrl.downPaymentDollars']")
	private WebElement downPaymentAmount;

	@FindBy(css = "div#idPdpMemoryVariant div.dropdown")
	private WebElement memoryDropdown;

	@FindBy(css = "div.col-xs-12.critical-icon-div.wrapper")
	private WebElement warningImgIcon;

	@FindBy(css = ".notificationClose span.break-xs")
	private WebElement headerText;

	@FindBy(css = ".notificationClose span.body-copy-custom")
	private WebElement bodyText;

	@FindBy(css = "#pdpnotificationsAlert>div")
	private WebElement entireStickyBanner;

	@FindBy(css = "#pdpbody > div > div > div> div > div > div > div:nth-child(1)>div:nth-child(2)")
	private WebElement addALineButton;

	@FindBy(css = "#pdpbody div.text-center-xs span:nth-child(2)")
	private WebElement suspendedMessage;	

	@FindBy(css = "a[ng-bind-html*='ctrl.contentData.californiaProposition65LegalLink']")
	private WebElement californiaResidentswarning;

	@FindBy(css = ".h2-title-warning")
	private WebElement californiaPropositionwarningModalHeader;

	@FindBy(css = ".body-copy-description-regular")
	private WebElement californiaPropositionwarningModalMessage;

	@FindBy(css = "span.legal-verbiage-text span[ng-bind*='offerCents']")
	private WebElement deviceFRPPriceCents;

	@FindBy(css = "p[ng-bind='$ctrl.aalWarningModalContent.header']")
	private WebElement modalTextMsgHeader;

	@FindBy(css = "p[ng-bind='$ctrl.aalWarningModalContent.message']")
	private WebElement modalTextMsg;

	@FindBy(css = "h2[class='h2-Uppercase text-bold text-black ng-binding']")
	private WebElement letsTalkTitle;

	@FindBy(css = "a[ng-bind='$ctrl.aalWarningModalContent.ctaLabel']")
	private WebElement contactUs;

	@FindBy(css = "h2[class='h2-Uppercase text-bold text-black ng-binding']")
	private WebElement customerCareModal;

	@FindBy(css = "nav[ng-show='$ctrl.showFooter']")
	private WebElement footer;

	@FindBy(css = "div#pdpnotificationsAlert")
	private WebElement pastDueStickyBanner;

	@FindBy(css = "span[class='break-xs custom-body text-white ng-binding']")
	private WebElement pastDueStickyMsg;	

	@FindBy(css = "button[ng-click*='onClickAddALineBtn()']")
	private WebElement addaLineCTA;

	@FindBy(css = "a[ng-attr-id*='positive']")
	private List<WebElement> positiveReviewCountPDP;

	@FindBy(css = "a[ng-attr-id*='negative']")
	private List<WebElement> negativeReviewCountPDP;

	@FindBy(xpath = "//h3[contains(text(),'Specifications')]")
	private WebElement specificationsCTAOnPDP;

	@FindBy(xpath = "//p[contains(text(),'WEA Capable')]")
	private WebElement weaCapableCTAOnPDP;

	@FindBy(css = "span.glueHyperLink")
	private WebElement weaCapableHyperLinkOnPDP;

	@FindBy(css = "h2.small.no-text-transform ")
	private WebElement weaCapableLinkHeaderTitle;

	@FindBy(xpath = "//p[contains(text(),'Cameras')]")
	private WebElement cameraCTAOnPDP;

	@FindBy(xpath = "//p[contains(text(),'Tier')]")
	private List<WebElement> tierCTAOnPDP;
	
	@FindBy(xpath = "span.glueHyperLink")
	private WebElement weaHyperSPecificationOnPDP;	

	@FindBy(css = "b[ng-bind*='ctrl.availabilityTitle']")
	private WebElement outOfStock;

	@FindBy(css = "p.ng-binding")
	private WebElement removeSimCardText;

	@FindBy(css = "h2[ng-bind='$ctrl.aalWarningModalContent.title']")
	private WebElement letsTalkModelTitle;

	@FindBy(css = "p[ng-bind='$ctrl.aalWarningModalContent.header']")
	private WebElement letsTalkModelContentHeader;

	@FindBy(css = "p[ng-bind='$ctrl.aalWarningModalContent.message']")
	private WebElement letsTalkModelContentMessage;

	@FindBy(css = "a[ng-bind='$ctrl.aalWarningModalContent.ctaLabel']")
	private WebElement contactUsButton;
	
	@FindBy(css = "[class*=' padding-vertical-small p-t-small p-l-15 p-r-15 message-width pdp-sticky-message-width']")
	private WebElement inEligibleMsg;

	@FindBy(css = "i.fa.fa-close.uib-close")
	private WebElement californiaResidentsWarningModel;

	@FindBy(css = ".star-rating img")
	private WebElement reviewStarRating;

	@FindBy(css = ".star-rating a")
	private WebElement reviewsLink;

	@FindBy(css = "div#specificationsBlock")
	private WebElement specificationsSection;

	@FindBy(css = "[ng-if*='$ctrl.selectedSkuStatus.estimatedShipDateFrom']")
	private WebElement estimatedShipDate;

	@FindBy(css = "row.padding-top-xxl-sm")
	private WebElement pendingRatePlanBtn;

	@FindBy(css = "[class*='animate-show']")
	private List<WebElement> selectColor;

	@FindBy(xpath = "//p[text()='Display']")
	private WebElement displaySpecification;

	@FindBy(xpath = "//p[text()='Display']/../following-sibling::div//li")
	private WebElement displaySpecificationValue;

	@FindBy(xpath = "//p[text()='Display resolution']")
	private WebElement displayResolutionSpecification;

	@FindBy(xpath = "//p[text()='Display resolution']/../following-sibling::div//li")
	private WebElement displayResolutionSpecificationValue;

	@FindBy(xpath = "//p[text()='Weight']")
	private WebElement weightSpecification;

	@FindBy(xpath = "//p[text()='Weight']/../following-sibling::div//li")
	private WebElement weightSpecificationValue;

	@FindBy(xpath = "//p[text()='Size']")
	private WebElement sizeSpecification;

	@FindBy(xpath = "//p[text()='Size']/../following-sibling::div//li")
	private WebElement sizeSpecificationValue;

	@FindBy(xpath = "//p[text()='Battery Description']")
	private WebElement batteryDescriptionSpecification;

	@FindBy(xpath = "//p[text()='Battery Description']/../following-sibling::div//li")
	private WebElement batteryDescriptionSpecificationValue;

	@FindBy(xpath = "//p[text()='Battery Talk Time']")
	private WebElement batteryTalkTimeSpecification;

	@FindBy(xpath = "//p[text()='Battery Talk Time']/../following-sibling::div//li")
	private WebElement batteryTalkTimeSpecificationValue;

	@FindBy(xpath = "//p[text()='Ports']")
	private WebElement portsSpecification;

	@FindBy(xpath = "//p[text()='Ports']/../following-sibling::div//li")
	private WebElement portsSpecificationValue;

	@FindBy(xpath = "//p[text()='Connectivity']")
	private WebElement connectivitySpecification;

	@FindBy(xpath = "//p[text()='Connectivity']/../following-sibling::div//li")
	private WebElement connectivitySpecificationValue;

	@FindBy(xpath = "//p[text()='Processor']")
	private WebElement processorSpecification;

	@FindBy(xpath = "//p[text()='Processor']/../following-sibling::div//li")
	private WebElement processorSpecificationValue;

	@FindBy(xpath = "//p[text()='Operating System']")
	private WebElement oSSpecification;

	@FindBy(xpath = "//p[text()='Operating System']/../following-sibling::div//li")
	private WebElement oSSpecificationValue;

	@FindBy(xpath = "//p[text()='Ram']")
	private WebElement ramSpecification;

	@FindBy(xpath = "//p[text()='Ram']/../following-sibling::div//li")
	private WebElement ramSpecificationValue;

	@FindBy(xpath = "//p[text()='Maximum Expandable Memory']")
	private WebElement memorySpecification;

	@FindBy(xpath = "//p[text()='Maximum Expandable Memory']/../following-sibling::div//li")
	private WebElement memorySpecificationValue;

	@FindBy(xpath = "//p[text()='Wireless Network Technology Generations']")
	private WebElement wirelessNetworkTechSpecification;

	@FindBy(xpath = "//p[text()='Wireless Network Technology Generations']/../following-sibling::div//li")
	private WebElement wirelessNetworkTechSpecificationValue;

	@FindBy(xpath = "//p[text()='Supported Email Platforms']")
	private WebElement supportedEmailSpecification;

	@FindBy(xpath = "//p[text()='Supported Email Platforms']/../following-sibling::div//li")
	private WebElement supportedEmailSpecificationValue;

	@FindBy(xpath = "//p[text()='WEA Capable']/../following-sibling::div//li")
	private WebElement wEACapableSpecificationValue;

	@FindBy(xpath = "//p[text()='Hearing Aid Compatibility']")
	private WebElement hearingAidSpecification;

	@FindBy(xpath = "//p[text()='Hearing Aid Compatibility']/../following-sibling::div//li")
	private WebElement hearingAidSpecificationValue;

	@FindBy(xpath = "//p[text()='Mobile Hotspot Capable']")
	private WebElement mobileHotspotSpecification;

	@FindBy(xpath = "//p[text()='Mobile Hotspot Capable']/../following-sibling::div//li")
	private WebElement mobileHotspotSpecificationValue;

	@FindBy(xpath = "//p[text()='Cameras']")
	private WebElement camerasSpecification;

	@FindBy(xpath = "//p[text()='Cameras']/../following-sibling::div//li")
	private WebElement camerasSpecificationValue;

	@FindBy(xpath = "//p[text()='Frequency']")
	private WebElement frequencySpecification;

	@FindBy(xpath = "//p[text()='Frequency']/../following-sibling::div//li")
	private WebElement frequencySpecificationValue;

	@FindBy(css = "[ng-show*='colorPickerLength > 4'] i")
	private WebElement colorDropdown;

	@FindBy(css = ".color-picker>span")
	private List<WebElement> colorVariants;

	public PDPPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify PDP Page
	 * 
	 * @return
	 */
	public PDPPage verifyPDPPage() {
		checkPageIsReady();
		waitforSpinner();
		try {
			verifyPageUrl();
			Reporter.log("PDP page displayed");
		} catch (Exception e) {
			Assert.fail("PDP page not displayed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PDPPage verifyPageUrl() {
		try {
			getDriver().getCurrentUrl().contains(PDP_PAGE_URL);
		} catch (Exception e) {
			Assert.fail("PDP page is not displayed");
		}
		return this;
	}

	/**
	 * Click Add To Cart
	 */
	public PDPPage clickAddToCart() {
		try {
		//	verifyIsDeviceInStock();
			moveToElement(addToCart);
			clickElementWithJavaScript(addToCart);
			try {
				if (addToCart.isDisplayed()) {
					clickElementWithJavaScriptScaling(addToCart);
				}
			} catch (Exception e) {
				//Reporter.log("Navigated To Line Selector Page");
			}
			Reporter.log("Clicked on Add to Cart");
		} catch (Exception e) {
			Assert.fail("Add to Cart Button is not Clickable or Not Navigating after clicking");
		}
		return this;
	}

	/**
	 * Click Today Button
	 */
	public PDPPage clickTodayButton() {
		try {
			clickElement(downPaymentAmount);
		} catch (Exception e) {
			Assert.fail("Today Button is not displayed to Click");
		}
		return this;
	}

	/**
	 * Verify Down Payment Model Header
	 * 
	 * @return
	 */
	public PDPPage verifyDownPaymentModelHeader() {
		try {
			downPaymentModelHeader.isDisplayed();
			Reporter.log("Down payment model header is displayed");
		} catch (Exception e) {
			Assert.fail("Down Payment Model Header is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Down Payment Model Text
	 * 
	 * @return
	 */
	public PDPPage verifyDownPaymentModelText() {
		try {
			downPaymentModelText.isDisplayed();
			Reporter.log("Down payment model Text is displayed");
		} catch (Exception e) {
			Assert.fail("Down Payment Model Text is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Down Payment Model Note
	 * 
	 * @return
	 */
	public PDPPage verifyDownPaymentModelNote() {
		try {
			downPaymentModelNote.isDisplayed();
			Reporter.log("Down payment model Note is displayed");
		} catch (Exception e) {
			Assert.fail("Down Payment Model Note is not Displayed");
		}
		return this;
	}

	/**
	 * Click Accessibility Page Link
	 * 
	 * @return
	 */
	public PDPPage clickAccessibilityPageLink() {
		try {
			if ((getDriver() instanceof AppiumDriver)) {
				moveToElement(iosSeeLegalDisclaimer);
				iosSeeLegalDisclaimer.click();
				checkPageIsReady();
			}
			accessibilityPage.click();
			clickOnAllowPopUp();
		} catch (Exception e) {
			Assert.fail("Accessibility Page is not Displayed to click");
		}
		return this;
	}

	/**
	 * Click On Payment Option
	 * 
	 * @return
	 */
	public boolean clickOnPaymentOption() {
		waitforSpinner();
		boolean paymentOptionDropdown = false;
		if (!paymentOption.isEmpty() && paymentOption.size() >= 1) {
			paymentOptionDropdown = true;
			clickElement(paymentOption);
		}
		return paymentOptionDropdown;
	}

	/**
	 * Select Payment Option[Full Retail Price, Monthly payments]
	 * 
	 * @param option
	 */
	public PDPPage selectPaymentOption(String option) {
		try {
			waitforSpinner();
			if (paymentOptions.size() > 0) {
				for (WebElement webElement : paymentOptions) {
					if (webElement.getText().equals(option)) {
						webElement.click();
						Reporter.log("Selected Payment Option: " +option);
						break;
					}
				}
			} else {
				Reporter.log("Default pricing is selected");
			}
		} catch (Exception e) {
			Assert.fail("Payment options field not displayed");
		}
		return this;
	}

	/**
	 * Verify Memory Header
	 * 
	 * @return
	 */
	public PDPPage verifyMemoryHeader() {
		try {
			memoryHeader.isDisplayed();
			Reporter.log("Memory header is displayed");
		} catch (Exception e) {
			Assert.fail("Memory Header is not Displayed");
		}
		return this;
	}

	/**
	 * verify Review Tab
	 */
	public PDPPage verifyReviewTab() {
		try {
			Assert.assertTrue(reviewTab.isDisplayed(), "review tab is not dispalyed");
		} catch (Exception e) {
			Assert.fail("Review Tab is not Displayed to click");
		}
		return this;
	}

	/**
	 * Click Review Tab
	 */
	public PDPPage clickReviewTab() {
		try {
			moveToElement(reviewTab);
			clickElementWithJavaScript(reviewTab);
		} catch (Exception e) {
			Assert.fail("Review Tab is not Displayed to click");
		}
		return this;
	}

	/**
	 * Verify Features Section
	 * 
	 * @return
	 */
	public PDPPage verifyFeaturesSection() {
		try {
			moveToElement(featuresHeader);
			Assert.assertTrue(featuresHeader.isDisplayed(), "Features Section is not available");
			Reporter.log("Features Section Header is Displayed");
		} catch (Exception e) {
			Assert.fail("Features Section Header is not Displayed");
		}
		return this;
	}

	/**
	 * Pending Rateplan Messag is not Displayed
	 * 
	 * @return
	 */
	public PDPPage verifyPendingRatePlanMessage() {
		try {
			moveToElement(featuresHeader);
			Assert.assertTrue(pendingRatePlanBtn.isDisplayed(), "Pending Rateplan Message is not Displayed");
			Reporter.log("Pending Rateplan Messag is Displayed");
		} catch (Exception e) {
			Assert.fail("Pending Rateplan Message is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Features Tab
	 * 
	 * @return
	 */
	public PDPPage verifyFeaturesTab() {
		try {
			moveToElement(featuresTab);
			Assert.assertTrue(featuresTab.isDisplayed(), "Features tab is not available");
			Reporter.log("Features Tab is ddisplayed");
		} catch (Exception e) {
			Assert.fail("Features Tab is not displayed");
		}
		return this;
	}

	/**
	 * Verify See More Link
	 * 
	 * @return
	 */
	public PDPPage verifySeeMoreLink() {
		try {
			seeMorelink.isDisplayed();
			Reporter.log("See more Link is displayed");

		} catch (Exception e) {
			Assert.fail("See more Link is not displayed");
		}
		return this;
	}

	/**
	 * Click See More Link
	 * 
	 * @return
	 */
	public PDPPage clickSeeMoreLink() {
		try {
			clickElementWithJavaScript(seeMorelink);
		} catch (Exception e) {
			Assert.fail("See more Link is not Displayed to click");
		}
		return this;

	}

	/**
	 * Verify See Less Link
	 * 
	 * @return
	 */
	public PDPPage verifySeeLessLink() {
		try {
			seeLessLink.isDisplayed();
			Reporter.log("See Less Link is displayed");
		} catch (Exception e) {
			Assert.fail("See less Link is not displayed");
		}
		return this;
	}

	/**
	 * Click See Less Link
	 * 
	 * @return
	 */
	public PDPPage clickSeeLessLink() {
		try {
			seeLessLink.click();
		} catch (Exception e) {
			Assert.fail("See less Link is not available to click");
		}
		return this;
	}

	/**
	 * Verify Continue Button
	 * 
	 * @return
	 */
	public PDPPage verifyContinueButton() {
		try {
			Assert.assertTrue(addToCart.isDisplayed(), "Continue button is not displayed");
			Reporter.log("Continue Button is Displayed");
		} catch (Exception e) {
			Reporter.log("Continue Button is not available on PDP Page");
		}
		return this;
	}

	/**
	 * Verify Legal Text
	 * 
	 * @return
	 */
	public PDPPage verifyLegalText() {
		try {
			Thread.sleep(2000);
			Assert.assertTrue(legalText.getText()
					.contains("IF YOU CANCEL WIRELESS SERVICE, REMAINING BALANCE ON PHONE BECOMES DUE"));
			Reporter.log("Legal text is displayed");
		} catch (Exception e) {
			Assert.fail("Legal text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Legal Text is not Displayed
	 * 
	 * @return
	 */

	public PDPPage verifyLegalTextNotDisplayed() {

		try {
			waitFor(ExpectedConditions.invisibilityOf(legalText), 5);
			Assert.assertFalse(legalText.getText().contains("If you cancel wireless service"));
			Reporter.log("Legal text is Not displayed");
		} catch (Exception e) {
			Assert.fail("Legal text is displayed");
		}
		return this;
	}

	/**
	 * Verify HoldUp Model Cancel Button
	 * 
	 * @return
	 */
	public PDPPage verifyHoldUpModelCancelButton() {
		try {
			frpHoldUpModelCancelButton.isDisplayed();
			Reporter.log("frp Hold Up Model Cancel Button is Displayed");
		} catch (Exception e) {
			Assert.fail("frp Hold Up Model Cancel Button is Displayed");
		}
		return this;
	}

	/**
	 * Verify FRP PRice
	 * 
	 * @return
	 */
	public String verifyFRPPRice() {
		String frpPrice = null;
		try {
			frpPrice = deviceFRPPrice.getText();

		} catch (Exception e) {
			Assert.fail("FRP price is Not Displayed");
		}
		return frpPrice;

	}

	/**
	 * Verify HoldUp Model Promotion Text
	 * 
	 * @return
	 */
	public PDPPage verifyIneligibilityTxt() {
		try {
			ineligibilityTxt.isDisplayed();
			Reporter.log("Upgrade InEligible Message is displayed");
		} catch (Exception e) {
			Assert.fail("Ineligibility Messag is Not Displayed");
		}
		return this;
	}

	/**
	 * getDeviceName
	 */
	public String getDeviceName() {
		return deviceName.getText();
	}

	/**
	 * getDeviceColor
	 */
	public String getDeviceColor() {
		return deviceColor.getText();
	}

	/**
	 * getDeviceMemory
	 */
	public String getDeviceMemory() {
		return deviceMemory.getText();
	}

	/**
	 * Verify Promo Badge
	 * 
	 * @return
	 */
	public PDPPage verifyPromoBadge() {
		try {
			promoBadge.isDisplayed();
			Reporter.log("Promo Badge is Displayed");
		} catch (Exception e) {
			Assert.fail("Promotion Badge is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Device FRPPrice
	 * 
	 * @return
	 */
	public PDPPage verifyDeviceFRPPrice() {
		try {
			deviceFRPPrice.isDisplayed();
			Reporter.log("Device FRP is displayed");
		} catch (Exception e) {
			Assert.fail("Device FRP Price is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promo Link Text
	 * 
	 * @return
	 */
	public PDPPage verifypromoLinkText() {
		try {
			promoLinkText.isDisplayed();
			Reporter.log("Promo link text is displayed");
		} catch (Exception e) {
			Assert.fail("Promo link text is not displayed");
		}
		return this;
	}

	/**
	 * Click Promo Link Text
	 */
	public PDPPage clickPromoLinkText() {
		try {
			clickElementWithJavaScript(promoLinkText);
		} catch (Exception e) {
			Assert.fail("Promo link text is not displayed to click");
		}
		return this;
	}

	/**
	 * Verify Promo Link Text
	 * 
	 * @return
	 */
	public PDPPage verifyPDPPromoWindow() {
		try {
			pdpPromoWindow.isDisplayed();
			Reporter.log("PDP promo window is displayed");
		} catch (Exception e) {
			Assert.fail("PDP promo window is not displayed");
		}
		return this;
	}

	/**
	 * Click continue button
	 */
	public PDPPage clickContinueBtn() {
		checkPageIsReady();
		waitforSpinner();
		try {
			Assert.assertTrue(addToCart.isDisplayed());
			moveToElement(addToCart);
			clickElement(addToCart);
			// continueButton.click();
		} catch (Exception e) {
			Assert.fail("Continue button is not displayed to click");
		}
		return this;
	}

	/**
	 * Verify Color picker
	 * 
	 * @return
	 */
	public PDPPage verifyColorPicker() {
		try {
			colorPicker.isDisplayed();
			Reporter.log("Color picker is displayed");
		} catch (Exception e) {
			Assert.fail("Color picker is Not Displayed");
		}
		return this;
	}

	/**
	 * Verify Device EIPMonthlyText
	 * 
	 * @return
	 */
	public PDPPage verifyDeviceEIPMonthlyText() {
		try {
			Assert.assertTrue(eipMonthlyPriceText.isDisplayed(), "Device EIP Monthly Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device EIP Monthly Text");
		}
		return this;
	}

	/**
	 * Verify Device EIPMonthly Installment Terms
	 * 
	 * @return
	 */
	public PDPPage verifyDeviceEIPMonthlyInstallmentTerms() {
		try {
			Assert.assertTrue(eipInstallmentMonth.isDisplayed());
			Assert.assertTrue(
					eipInstallmentMonth.getText().contains("9")
							| eipInstallmentMonth.getText().contains("12")
							| eipInstallmentMonth.getText().contains("24")
							| eipInstallmentMonth.getText().contains("36"),
					"EIP Loan Term is not displayed correctly");
			Reporter.log("EIP Monthly Installment Term is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Installment Term");
		}
		return this;
	}

	/**
	 * Verify Dollars Sign in EIP Price
	 * 
	 * @return
	 */
	public PDPPage verifyEIPDollarsSign() {
		try {
			waitFor(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("sup.dollar-sign")));
			for (WebElement eipDollarSign : eIPdollarSign) {
				Assert.assertTrue(eipDollarSign.getText().contains("$"),
						"Dollar sign is not displayed in device PDP page");
			}
			Reporter.log("Dollar signs for EIP are displayed in device PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Dollars Sign");
		}
		return this;
	}

	/**
	 * Verify Monthly EIP Price
	 * 
	 * @return
	 */
	public PDPPage verifyEIPPrice() {
		try {
			Assert.assertTrue(eipMonthlyPrice.isDisplayed());
			Reporter.log("EIP Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Price");
		}
		return this;
	}

	/**
	 * Verify Dollars Sign in FRP Price
	 * 
	 * @return
	 */
	public PDPPage verifyFRPDollarsSign() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(fRPDollarSign));
			for (WebElement frpDollarSign : fRPDollarSign) {
				Assert.assertTrue(frpDollarSign.getText().contains("$"),
						"FRP Dollars Sign is not displayed");
			}
			Reporter.log("Dollar signs for FRP are displayed in device PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP Dollars Sign");
		}
		return this;
	}

	/**
	 * Verify that loan term from PLP page is equal to loan term from PDP
	 */
	public PDPPage compareLoanTermIsSameForPLPAndPDP(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "PDP loan term is not matched with PLP loan term price");
			Reporter.log("PDP Loan term is matched with PLP loan term");
		} catch (Exception e) {
			Assert.fail("Failed to compare PDP loan term is not matched with PLP loan term");
		}
		return this;
	}

	/**
	 * Verify that monthly payment from PLP page is equal to monthly payment
	 * from PDP
	 */
	public PDPPage compareMonthlyPaymentIsSameForPLPAndPDP(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue,
					"PDP monthly payment is not matched with PLP monthly payment price");
			Reporter.log("PDP Monthly payment is matched with PLP monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to compare PDP monthly payment is not matched with PLP monthly payment");
		}
		return this;
	}

	/**
	 * Verify Down Payment
	 * 
	 * @return
	 */
	public String getDeviceDownPaymentInPDP() {
		String downPayment;
		downPayment = downPaymentAmount.getText();
		return downPayment;
	}

	/**
	 * Verify Loan Term
	 * 
	 * @return
	 */
	public String getDeviceLoanTermInPDP() {
		String loanTerm;
		loanTerm = eipInstallmentMonth.getText();
		loanTerm = loanTerm.substring(loanTerm.indexOf('x') + 1, loanTerm.indexOf(' '));
		return loanTerm;
	}

	/**
	 * Get Monthly Payment
	 * 
	 * @return
	 */
	public String getDeviceMonthlyPaymentInPDP() {
		String monthlyPayment, monthlyPaymentInDollar, monthlyPaymentCents;
		if (getDriver() instanceof AppiumDriver) {
			moveToElement(eipMonthlyPrice);
			monthlyPaymentInDollar = eipMonthlyPrice.getText();
			monthlyPaymentCents = eipMonthlyPriceInCents.getText();
			monthlyPayment = monthlyPaymentInDollar + "." + monthlyPaymentCents;
		} else {
			monthlyPaymentInDollar = eipMonthlyPrice.getText();
			monthlyPaymentCents = eipMonthlyPriceInCents.getText();
			monthlyPayment = monthlyPaymentInDollar + "." + monthlyPaymentCents;
		}
		return monthlyPayment;
	}

	/**
	 * Select the memory variant
	 * 
	 * @param memoryValue
	 */
	public void selectMemory(String memoryValue) {
		try {
			clickElement(memoryDropdown);
			for (WebElement webElement : memoryDropdownOptions) {
				if (memoryValue.equalsIgnoreCase(webElement.getText())) {
					webElement.click();
					Reporter.log("Memory variant: " +memoryValue+ "is selected");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Memory variant is not clickable");
		}
	}

	/**
	 * Verify Sticky Banner
	 * 
	 * @return
	 */
	public PDPPage verifyStickyBanner() {
		try {
			waitforSpinner();
			Assert.assertTrue(entireStickyBanner.isDisplayed(), "The Sticky Banner is not displayed");
			Reporter.log("Sticky Banner is Displayed");
		} catch (Exception e) {
			Reporter.log("Sticky Banner is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Warning Image Icon
	 * 
	 * @return
	 */

	public PDPPage stickyBannerWarningImgIcon() {
		try {
			waitforSpinner();
			Assert.assertTrue(warningImgIcon.isDisplayed(), "The Warning Image Icon is not displayed");
			Reporter.log("The Warning Image Icon is Displayed");
		} catch (Exception e) {
			Reporter.log("The Warning Image Icon is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Authorable Header Text
	 * 
	 * @return
	 */

	public PDPPage stickyBannerHeaderText() {
		try {
			waitforSpinner();
			Assert.assertTrue(headerText.isDisplayed(), "The Authorable header text is not displayed");
			// Assert.assertTrue(headerText.getText().contains("You have a past
			// due balance of"), "The text is not correct");
			Reporter.log("The Authorable header text is Displayed");
		} catch (Exception e) {
			Reporter.log("The Authorable header text is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Authorable Body Text
	 * 
	 * @return
	 */

	public PDPPage stickyBannerBodyText() {
		try {
			waitforSpinner();
			Assert.assertTrue(bodyText.isDisplayed(), "The Authorable body text is not displayed");
			// Assert.assertTrue(bodyText.getText().contains("Please make a
			// payment to proceed with upgrading or adding a line"), "The text
			// is not correct");
			Reporter.log("The Authorable body text is Displayed");
		} catch (Exception e) {
			Reporter.log("The Authorable body text is not Displayed");
		}
		return this;
	}

	/**
	 * Click Sticky Banner
	 * 
	 * @return
	 */
	public PDPPage clickStickyBanner() {
		try {
			waitforSpinner();
			entireStickyBanner.click();
			Reporter.log("Sticky Banner is Clicked");
		} catch (Exception e) {
			Reporter.log("Sticky Banner is not Clickable");
		}
		return this;
	}

	/**
	 * Verify Upgrade Button is disabled
	 * 
	 * @return
	 */

	public PDPPage verifyUpgradeButtonIsDisabled() {
		try {
			waitforSpinner();
			Assert.assertFalse(addToCart.isEnabled(), "The Next Step Icon is not disabled");
			Reporter.log("Next Step Icon is disabled");
		} catch (Exception e) {
			Reporter.log("Next Step Icon is not disabled");
		}
		return this;
	}

	/**
	 * Verify Add a Line Button is disabled
	 * 
	 * @return
	 */

	public PDPPage verifyAddALineButtonIsDisabled() {
		try {
			waitforSpinner();
			Assert.assertFalse(addALineButton.isEnabled(), "The Next Step Icon is not disabled");
			Reporter.log("Next Step Icon is disabled");
		} catch (Exception e) {
			Reporter.log("Next Step Icon is not disabled");
		}
		return this;
	}

	/**
	 * Verify Sticky Banner is not displayed
	 * 
	 * @return
	 */

	public PDPPage verifyStickyBannerNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(entireStickyBanner), "The Sticky Banner is Displayed");
			Reporter.log("The Sticky Banner is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sticky Banner not displayed");
		}
		return this;
	}

	/**
	 * Verify Display of Modal Description for suspended customer
	 */
	public PDPPage verifySuspendedCustomerMessageText() {
		try {
			String suspendedMessageText = suspendedMessage.getText();
			Assert.assertTrue(suspendedMessage.isDisplayed(),
					"Suspended customer message is not present in Past due Modal");
			Assert.assertTrue(suspendedMessageText.equals("Contact Customer Care."));
			Reporter.log("Suspended customer message is displayed and verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Suspended customer message text");
		}
		return this;
	}

	/**
	 * Get FRP price
	 */
	public Double getFRPPDPPage() {
		String FRPamount;
		FRPamount = deviceFRPPrice.getText();
		return Double.parseDouble(FRPamount);
	}

	/**
	 * Verify California Residents warning message
	 * 
	 * @return
	 */
	public PDPPage verifyCaliforniaResidentsWarning() {
		try {
			Assert.assertTrue(californiaResidentswarning.isDisplayed(),
					"california residents warning message not displayed");
			Assert.assertTrue(californiaResidentswarning.getText().contains("California Proposition 65 WARNING"));
			Reporter.log("California Residents warning displayed");
		} catch (Exception e) {
			Assert.fail("California Residents warning not displayed");
		}
		return this;
	}

	public PDPPage clickonCaliforniaPropositionLink() {
		try {
			californiaResidentswarning.click();
			Reporter.log("Clicked on california Proposition link");
		} catch (Exception e) {
			Assert.fail("california Proposition link not displayed");
		}
		return this;

	}

	/**
	 * Verify california Residents warning modal
	 */
	public PDPPage verifyCaliforniaPropositionWarningModal() {
		try {
			Assert.assertTrue(californiaPropositionwarningModalHeader.isDisplayed(),
					"california Proposition warning modal not displayed");
			Assert.assertTrue(
					californiaPropositionwarningModalHeader.getText().equals("California Proposition 65 Warning"),
					"california Proposition warning modal header not displayed");
			Assert.assertTrue(californiaPropositionwarningModalMessage.getText().contains("Harm"),
					"california Proposition warning modal message not displayed");
			Reporter.log("California Residents warning modal displayed");
		} catch (Exception e) {
			Assert.fail("California Residents warning modal  not displayed");
		}
		return this;
	}

	/**
	 * Get New FRP price
	 */
	public Double getNewFRPPDPPage() {
		String FRPamountDollars = deviceFRPPrice.getText();
		String FRPamountCents = deviceFRPPriceCents.getText();
		String TotlaFRP = FRPamountDollars + "." + FRPamountCents;

		return Double.parseDouble(TotlaFRP);
	}

	/**
	 * Verify that Promo FRP value on PDP and PLP Page are equal
	 */
	public PDPPage compareFRPonPLPwithPDP(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo FRP values PDP and PLP are not equal");
			Reporter.log("Promo FRP values onPDP and PLP  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo FRP values on PDP and PLP  page");
		}
		return this;
	}

	/**
	 * Get New Down price
	 */
	public Double getNewDownPaymentPDPPage() {
		String fRPamountDollars = downPaymentAmount.getText();
		String fRPamountCents = downPaymentAmount.getText();
		String totlaDown = fRPamountDollars + "." + fRPamountCents;

		return Double.parseDouble(fRPamountDollars);
	}

	/*
	 * Verify that Down Price value on PDP and PLP Page are equal
	 */
	public PDPPage compareDownPriceonPLPwithPDP(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "PDownPrice values PDP and PLP are not equal");
			Reporter.log("Down Price values on PDP and PLP  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Down Price values on PDP and PLP  page");
		}
		return this;
	}

	/*
	 * Verify that EIP Price value on PDP and PLP Page are equal
	 */
	public PDPPage compareEIPPriceonPLPwithPDP(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "EIP Price values PDP and PLP are not equal");
			Reporter.log("EIP Price values on PDP and PLP  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare EIP Price values on PDP and PLP  page");
		}
		return this;
	}

	/**
	 * Get New EIP Term
	 */
	public Double getEIPTermPDPPage() {
		String fRPamountDollars = eipInstallmentMonth.getText();
		String abc[] = fRPamountDollars.split(" ");
		String xyz[] = abc[0].split("x");

		return Double.parseDouble(xyz[1]);
	}

	/*
	 * Verify that EIP Term value on PDP and PLP Page are equal
	 */
	public PDPPage compareEIPTermonPLPwithPDP(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "EIP Term values PDP and PLP are not equal");
			Reporter.log("EIP Term values on PDP and PLP  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare EIP Term values on PDP and PLP  page");
		}
		return this;
	}

	/**
	 * Verify that Promo FRP value on PDP and PLP Page are equal
	 */
	public PDPPage compareFRPwithTotal(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo FRP values PDP and PLP are not equal");
			Reporter.log("Promo FRP values onPDP and PLP  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo FRP values on PDP and PLP  page");
		}
		return this;
	}

	public PDPPage verifyCustomerCareModal() {
		try {
			Assert.assertTrue(customerCareModal.isDisplayed());
			Reporter.log("customer care modal is displayed");
		} catch (Exception e) {
			Assert.fail("customer care modal not displayed");
		}
		return this;

	}

	public PDPPage clickonContactUsCTA() {
		try {
			contactUs.click();
			Reporter.log("Clicked on contact us CTA");
		} catch (Exception e) {
			Assert.fail("contact us CTA not displayed");
		}
		return this;
	}

	/**
	 * Verify footer is hidden
	 */
	public PDPPage verifyPDPFooter() {
		try {
			Assert.assertFalse(isElementDisplayed(footer), "Footer is displaying in PDP");
			Reporter.log("Footer is hidden in PDP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Footer in PDP");
		}
		return this;
	}

	/**
	 * Verify past due sticky message
	 */
	public PDPPage verifyPastDueStickyMessageDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(pastDueStickyMsg.getText().contains("Your past-due balance is"));
			Reporter.log("Past due sticky message is displayed");
		} catch (Exception e) {
			Assert.fail("Past due sticky message is not displayed");
		}
		return this;
	}

	/**
	 * Click past due sticky bannner
	 */
	public PDPPage clickPastDueBanner() {
		checkPageIsReady();
		try {
			clickElement(pastDueStickyBanner);
			Reporter.log("Clicked onPast due sticky message is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to click on past due banner");
		}
		return this;
	}

	/**
	 * verify Addtocart button disabled
	 */
	public PDPPage verifyAddtoCartButtonisDisabled() {
		try {
			checkPageIsReady();
			Assert.assertFalse(addToCart.isEnabled(), "Add to cart cta is not disabled");
			Reporter.log("add to cart button is disabled");
		} catch (Exception e) {
			Assert.fail("add to cart button is not disabled");
		}
		return this;
	}

	/**
	 * verify upgrade cta disabled
	 */
	public PDPPage verifyUpGradeCTADisabled() {
		try {
			checkPageIsReady();
			Assert.assertFalse(addToCart.isEnabled(), "Upgrade cta is not disabled");
			Reporter.log("Upgrade cta is disabled");
		} catch (Exception e) {
			Assert.fail("Upgrade cta is not disabled");
		}
		return this;
	}

	/**
	 * verify add a line cta disabled
	 */
	public PDPPage verifyAddaLineCTADisabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(addaLineCTA.getAttribute("disabled").contains("true"),
					"add a Line button is not disabled");
			Reporter.log("add a Line button is disabled");
		} catch (Exception e) {
			Assert.fail("add a Line button is not disabled");
		}
		return this;
	}

	/**
	 * verify add a line cta is displayed
	 */
	public PDPPage verifyAddaLineCTADisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(addaLineCTA), "add a Line button is not displayed");
			Reporter.log("add a Line button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify add a Line button");
		}
		return this;
	}

	/**
	 * verify Addtocart button disabled
	 */
	public PDPPage verifyUpGradeCTAEnabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(addToCart.isEnabled(), "upgrade cta is enabled");
			Reporter.log("upgrade cta not enabled");
		} catch (Exception e) {
			Assert.fail("upgrade cta is enabled");
		}
		return this;
	}

	/**
	 * verify ineligible message
	 */
	public PDPPage verifyInEligibleMessage(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(inEligibleMsg.getText().contains(msg), "Ineligible message not displayed");
			Reporter.log("Ineligible message is displayed");
		} catch (Exception e) {
			Assert.fail("Ineligible message not displayed");
		}
		return this;
	}

	/**
	 * verify add a line button hidden
	 */
	public PDPPage verifyAddaLineCTAHidden() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(addaLineCTA), "Add a Line cta is not hidden");
			Reporter.log("Add a Line cta is  hidden");
		} catch (Exception e) {
			Assert.fail("Add a Line cta is not hidden");
		}
		return this;
	}

	/**
	 * verify upgrade CTA hidden
	 */
	public PDPPage verifyupgradeCTAHidden() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(addToCart), "upgrade CTA  is not hidden");
			Reporter.log("upgrade CTA is  hidden");
		} catch (Exception e) {
			Assert.fail("upgrade CTA is not hidden");
		}
		return this;
	}

	/**
	 * Get Monthly Payment
	 * 
	 * @return
	 */
	public String getDeviceMonthlyPaymentInPDPNoCents() {
		String monthlyPayment;
		String monthlyAmount;
		monthlyPayment = eipMonthlyPrice.getText();
		monthlyAmount = monthlyPayment;
		return monthlyAmount;
	}

	/**
	 * verify device availability
	 */
	public PDPPage verifyIsDeviceInStock() {
		try {
			checkPageIsReady();
			moveToElement(outOfStock);
			if (outOfStock.isDisplayed()) {
				Assert.assertFalse(outOfStock.getText().toLowerCase().contains("out of stock"),
						"Device is Out Of Stock");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify avaliability of device");
		}
		return this;
	}

	/**
	 * Click Add A Line Button
	 * 
	 * @return
	 */
	public PDPPage clickOnAddALineButton() {
		try {
			waitforSpinner();
			checkPageIsReady();
			/*
			 * waitFor(ExpectedConditions.visibilityOf(addaLineCTA), 10);
			 * moveToElement(addaLineCTA);
			 */
			clickElementWithJavaScript(addaLineCTA);

		} catch (Exception e) {
			Assert.fail("Add a line button is not displayed");
		}
		return this;
	}

	/**
	 * Click Add A Line Button
	 *
	 * @return
	 */
	public PDPPage clickOnContinueAALButton() {
		try {
			waitforSpinner();
			checkPageIsReady();
			moveToElement(addaLineCTA);
			clickElement(addaLineCTA);
			Reporter.log("Clicked on AAL/Continue cta");
		} catch (Exception e) {
			Assert.fail("Add a line button is not displayed");
		}
		return this;
	}

	/**
	 * verify remove sim card text messsage
	 */
	public PDPPage verifyRemoveSimCardTextMessage() {
		try {
			Assert.assertFalse(removeSimCardText.getText()
					.contains("SIM starter kit will automatically be added when you add a line"));
			Reporter.log("Remove the sim card text message is removed");
		} catch (Exception e) {
			Reporter.log("Remove the sim card text message is not removed");
			Assert.fail("Remove the sim card text message is not removed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Lets Talk Model
	 * 
	 * @return
	 */
	public PDPPage verifyLetsTalkModel() {
		waitFor(ExpectedConditions.visibilityOf(letsTalkModelTitle), 5);
		try {
			Assert.assertTrue(letsTalkModelTitle.isDisplayed(), "Let's Talk model title is not displyed");
			Assert.assertEquals(letsTalkModelTitle.getText().trim(), "Let's talk");
			Reporter.log("Lets Talk model title is displayed");
		} catch (Exception e) {
			Assert.fail("Let's Talk model is not displayed");
		}
		return this;
	}

	/**
	 * Verify Lets Talk Model Description
	 * 
	 * @return
	 */
	public PDPPage verifyLetsTalkModelDescription() {
		try {
			Assert.assertTrue(letsTalkModelContentHeader.isDisplayed(),
					"Let's Talk model content header is not displyed");
			Assert.assertEquals(letsTalkModelContentHeader.getText().trim(),
					"Sorry - we're not able to add a line to your account online");

			Assert.assertTrue(letsTalkModelContentMessage.isDisplayed(),
					"Let's Talk model content message is not displyed");
			Assert.assertEquals(letsTalkModelContentMessage.getText().trim(),
					"To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone.");
			Reporter.log("Lets Talk model description is displayed");
		} catch (Exception e) {
			Assert.fail("Let's Talk model is not displayed");
		}
		return this;

	}

	/**
	 * Click On Contact US Button
	 * 
	 * @return
	 */
	public PDPPage clickOnContactUsButton() {
		try {
			contactUsButton.click();
			Reporter.log("Contact Us button is clickable");

		} catch (Exception e) {
			Assert.fail("Contact Us button is not clickable");
		}
		return this;
	}

	/**
	 * Click On California Proposition Warning Model CLose button
	 * 
	 * @return
	 */
	public PDPPage clickCaliforniaPropositionWarningModelCloseButton() {
		try {
			californiaResidentsWarningModel.click();
		} catch (Exception e) {
			Assert.fail("California residents warning model close button is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device FRPPrice
	 * 
	 * @return
	 */
	public PDPPage verifyDeviceDownPaymentPrice() {
		try {
			Assert.assertTrue(downPaymentAmount.isDisplayed(), "Device DownPayment price is not displayed");
			Reporter.log("Down Payment price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display device down Payment");
		}
		return this;
	}

	/**
	 * Verify that Device Price are equal
	 */
	public PDPPage compareDevicePrice(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Device Price are not equal");
			Reporter.log("Device Pric  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Device Price");
		}
		return this;
	}

	/**
	 * Verify positive Review Count for PDP
	 */
	public PDPPage verifyPositiveReviewCountForPDP() {
		try {
			int reviewLength = positiveReviewCountPDP.size();
			for (int i = 0; i < reviewLength; i++) {
				Assert.assertTrue(positiveReviewCountPDP.get(i).isDisplayed(),
						"Positive Review Count for  PDP is not displayed");
			}
			Reporter.log("Positive review Count for PDP is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to Verify positive Review Count for PDP");
		}
		return this;
	}

	/**
	 * Get positive Review Count for PDP
	 */
	/**
	 * count positive Review for Accessory PDP
	 */
	public String getPositiveReviewCountForPDP() {
		String review1 = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(positiveReviewCountPDP.get(0)));
			checkPageIsReady();
			String review = positiveReviewCountPDP.get(0).getText();
			review1 = review.substring(9, 10);
		} catch (Exception e) {
			Assert.fail("Unable to retrieve positive review count");
		}
		return review1;

	}

	/**
	 * click positive Review for Accessory PDP
	 */
	public PDPPage clickPositiveReviewCountForPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(positiveReviewCountPDP.get(0)));
			clickElementWithJavaScript(positiveReviewCountPDP.get(0));
		} catch (Exception e) {
			Assert.fail("Unable to Click on positive review count");
		}
		return this;

	}

	/**
	 * count negative Review for Accessory PDP
	 */
	public String getNegativeReviewCountForPDP() {
		String review1 = null;
		try {
			checkPageIsReady();
			String review = negativeReviewCountPDP.get(2).getText();
			review1 = review.substring(11, 12);
			Reporter.log("negative review" + review1);
		} catch (Exception e) {
			Assert.fail("Unable to retrieve negative review count");
		}
		return review1;
	}

	/**
	 * click positive Review for Accessory PDP
	 */
	public PDPPage clickNegativeReviewCountForPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(negativeReviewCountPDP.get(2)));
			clickElementWithJavaScript(negativeReviewCountPDP.get(2));
		} catch (Exception e) {
			Assert.fail("Unable to Click on negative review count");
		}
		return this;
	}

	/**
	 * Verify that review count are equal
	 */
	public PDPPage compareReviewCount(String firstValue, String secondValue) {
		try {
			Assert.assertNotEquals(firstValue, secondValue, " review count are  equal");
			Reporter.log("review count are not equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare review count");
		}
		return this;
	}

	/**
	 * Verify negative Review Count for PDP
	 */
	public PDPPage verifyNegativeReviewCountForPDP() {
		try {
			int reviewLength = negativeReviewCountPDP.size();
			for (int i = 0; i < reviewLength; i++) {
				Assert.assertTrue(negativeReviewCountPDP.get(i).isDisplayed(),
						"Negative Review Count for  PDP is not displayed");
			}
			Reporter.log("Negative review Count for PDP is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to Verify Negative Review Count for PDP");
		}
		return this;
	}

	/**
	 * Verifies Reviews Star Rating presence
	 */
	public PDPPage verifyReviewsStarRating() {
		try {
			Assert.assertTrue(reviewStarRating.isDisplayed(), "Review Star rating isn't available");
			Assert.assertTrue(reviewsLink.isDisplayed(), "Reviews link is not available");
			Reporter.log("Verified Review Star rating and link");
		} catch (Exception e) {
			Assert.fail("Unable to verify Review Star rating and its link ");
		}
		return this;
	}

	/**
	 * Verifies Specifications of the device on PDP
	 */
	public void verifySpecificationSection() {

		try {
			Assert.assertTrue(specificationsSection.isDisplayed(), "Specifications section isn't available");
			Reporter.log("Specifications are available");
		} catch (Exception e) {
			Assert.fail("Specification details are not available");
		}
	}

	/**
	 * Verify Device Name and Manufacturer Name
	 */
	public PDPPage verifyDeviceNameAndManufacturerName(String devicename) {
		checkPageIsReady();
		String deviceNamePDP = getDeviceName();
		try {
			waitFor(ExpectedConditions.visibilityOf(deviceName),30);
			/*Assert.assertTrue(deviceNamePDP.contains("Apple") || deviceNamePDP.contains("LG")
					|| deviceNamePDP.contains("Samsung") || deviceNamePDP.contains("T-Mobile®")
					|| deviceNamePDP.contains("Alcatel") || deviceNamePDP.contains("Motorola") || deviceNamePDP.contains("OnePlus"),
					"Manufacturer name is not displayed");*/
			Assert.assertTrue(deviceNamePDP.equals(devicename), "Device Name is not matching");
			Reporter.log("Device Name and Manufacturer name are matching");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Name and Manufacturer name");
		}
		return this;
	}

	/**
	 * Verify Estimated Ship Date
	 */
	public PDPPage verifyEstimatedShipDate() {
		try {
			Assert.assertTrue(estimatedShipDate.isDisplayed(), "Estimated ship date is not displayed");
			Reporter.log("Estimated ship date is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Estimate ship date");
		}
		return this;
	}

	/**
	 * verify Addtocart button disabled
	 */
	public PDPPage verifyAddtoCartButtonIsEnabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(addToCart.isEnabled(), "Add to cart cta is disabled");
			Assert.assertTrue(addToCart.getText().toLowerCase().contains("add to cart"));
			Reporter.log("add to cart button is enabled");
		} catch (Exception e) {
			Assert.fail("add to cart button is disabled");
		}
		return this;
	}

	/**
	 * verify add a line cta disabled
	 */
	public PDPPage verifyAddaLineCTAEnabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(addaLineCTA.isEnabled(), "add a Line button is disabled");
			Reporter.log("add a Line button is enabled");
		} catch (Exception e) {
			Assert.fail("add a Line button is disabled");
		}
		return this;
	}

	/**
	 * verify specifications CTA ON PDP page
	 */
	public PDPPage verifySpecificationsLinkOnPDP() {
		try {
			moveToElement(specificationsCTAOnPDP);
			Assert.assertTrue(specificationsCTAOnPDP.isDisplayed(), "Specifications are not displayed on PDP page ");
			Reporter.log("Specifications are displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display specifications on PDP");
		}
		return this;
	}

	/**
	 * verify camera specifications CTA ON PDP page
	 */
	public PDPPage verifyCameraSpecificationOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cameraCTAOnPDP));
			Assert.assertTrue(cameraCTAOnPDP.isDisplayed(), "camera specification are not displayed on PDP page ");
			Reporter.log("Camera specifications are displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display camera specifications on PDP");
		}
		return this;
	}

	/**
	 * verify camera specifications CTA ON PDP page
	 */
	public PDPPage verifyCameraValueOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(camerasSpecificationValue));
			Assert.assertTrue(camerasSpecificationValue.getText().contains(","),
					"camera concatenation value are not displayed on PDP page ");
			Reporter.log("Camera concatenation value  are displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display camera  concatenation value  on PDP");
		}
		return this;
	}

	/**
	 * verify battery specifications CTA ON PDP page
	 */
	public PDPPage verifyBatterySpecifcationOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(batteryTalkTimeSpecification));
			Assert.assertTrue(batteryTalkTimeSpecification.isDisplayed(),
					"battery specification are not displayed on PDP page ");
			Reporter.log("Battery specifications are displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display battery specifications on PDP");
		}
		return this;
	}

	/**
	 * verify battery specifications value CTA ON PDP page
	 */
	public PDPPage verifyBatterySpecifcationValueOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(batteryTalkTimeSpecificationValue));
			Assert.assertTrue(batteryTalkTimeSpecificationValue.getText().startsWith("Up to"),
					"battery specification value are not displayed on PDP page ");
			Reporter.log("Battery specifications value are displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display battery specificationsvalue  on PDP");
		}
		return this;
	}

	/**
	 * verify tier specifications value CTA ON PDP page
	 */
	public PDPPage verifyTierSpecifcationValueOnPDP() {
		try {
			int numberOfTier = tierCTAOnPDP.size();
			Assert.assertEquals(0, numberOfTier, "Tier specification value is displayed");
			Reporter.log("Tier specifications value is not displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display tier specificationsvalue on PDP");
		}
		return this;
	}

	/**
	 * verify WEA capable specifications CTA ON PDP page
	 */
	public PDPPage verifyWEACapableSpecifcationValueOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(weaCapableCTAOnPDP));
			Assert.assertTrue(weaCapableCTAOnPDP.isDisplayed(), "WEA capable are not displayed on PDP page ");
			Reporter.log("WEA capable are displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display WEA capable specifications on PDP");
		}
		return this;
	}

	/**
	 * verify WEA capable hyper link specifications CTA ON PDP page
	 */
	public PDPPage verifyWEACapableHyperLinkValueOnPDP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(weaCapableHyperLinkOnPDP));
			Assert.assertTrue(weaCapableHyperLinkOnPDP.isDisplayed(),
					"WEA capable hyperlink are not displayed on PDP page ");
			Reporter.log("WEA capable hyperlink are displayed on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to display WEA capable hyper link specifications on PDP");
		}
		return this;
	}

	/**
	 * Click WEA capable hyper link specifications CTA ON PDP page
	 */
	public PDPPage clickWEACapableHyperLinkValueOnPDP() {
		try {
			weaCapableHyperLinkOnPDP.click();
			waitforSpinner();
			String mainWindow = getDriver().getWindowHandle();
			Set<String> s1 = getDriver().getWindowHandles();
			Iterator<String> i1 = s1.iterator();
			while (i1.hasNext()) {
				String ChildWindow = i1.next();

				if (!mainWindow.equalsIgnoreCase(ChildWindow)) {
					getDriver().switchTo().window(ChildWindow);
					waitFor(ExpectedConditions.visibilityOf(weaCapableLinkHeaderTitle));
					Assert.assertTrue(weaCapableLinkHeaderTitle.isDisplayed(),
							"WEA capable Title is verified in new tab ");
				}
			}
			getDriver().switchTo().window(mainWindow);

			Reporter.log("WEA capable hyperlink is clicked and new tab is verified on PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to click WEA capable hyper link specifications on PDP");
		}
		return this;
	}

	/**
	 * Verifies Display Specifications of the device on PDP
	 */
	public void verifyDisplaySpecificationAndValue() {

		try {
			Assert.assertTrue(displaySpecification.isDisplayed(), "Display Specification section isn't available");
			Assert.assertTrue(displaySpecificationValue.isDisplayed(),
					"Display Specification Value section isn't available");
			Reporter.log("Display Specification is available");
		} catch (Exception e) {
			Assert.fail("Feiled to verify Display Specification details");
		}
	}

	/**
	 * Verifies Display Resolution Specifications of the device on PDP
	 */
	public void verifyDisplayResolutionSpecificationAndValue() {

		try {
			Assert.assertTrue(displayResolutionSpecification.isDisplayed(),
					"display resolution Specification section isn't available");
			Assert.assertTrue(displayResolutionSpecificationValue.isDisplayed(),
					"display resolution SpecificationValue section isn't available");
			Reporter.log("display resolution are available");
		} catch (Exception e) {
			Assert.fail("display resolution details are not available");
		}
	}

	/**
	 * Verifies Weight Specifications of the device on PDP
	 */
	public void verifyWeightSpecificationAndValue() {

		try {
			Assert.assertTrue(weightSpecification.isDisplayed(), "weightSpecification isn't available");
			Assert.assertTrue(weightSpecificationValue.isDisplayed(), "weightSpecificationValue isn't available");
			Reporter.log("weightSpecification are available");
		} catch (Exception e) {
			Assert.fail("weightSpecification details are not available");
		}
	}

	/**
	 * Verifies Size Specifications of the device on PDP
	 */
	public void verifyLengthHeightWidthSpecificationAndValue() {

		try {
			Assert.assertTrue(sizeSpecification.isDisplayed(), "sizeSpecification isn't available");
			Assert.assertTrue(sizeSpecificationValue.isDisplayed(), "sizeSpecificationValue isn't available");
			Reporter.log("sizeSpecification are available");
		} catch (Exception e) {
			Assert.fail("sizeSpecification details are not available");
		}
	}

	/**
	 * Verifies battery description Specifications of the device on PDP
	 */
	public void verifyBatteryDescriptionSpecificationAndValue() {

		try {
			Assert.assertTrue(batteryDescriptionSpecification.isDisplayed(),
					"batteryDescriptionSpecification isn't available");
			Assert.assertTrue(batteryDescriptionSpecificationValue.isDisplayed(),
					"batteryDescriptionSpecificationValue isn't available");
			Reporter.log("batteryDescriptionSpecification are available");
		} catch (Exception e) {
			Assert.fail("batteryDescriptionSpecification details are not available");
		}
	}

	/**
	 * Verifies battery talk time Specifications of the device on PDP
	 */
	public void verifyBatteryTalkTimeSpecificationAndValue() {

		try {
			Assert.assertTrue(batteryTalkTimeSpecification.isDisplayed(),
					"batteryTalkTimeSpecification isn't available");
			Assert.assertTrue(batteryTalkTimeSpecificationValue.isDisplayed(),
					"batteryTalkTimeSpecificationValue isn't available");
			Reporter.log("batteryTalkTimeSpecification are available");
		} catch (Exception e) {
			Assert.fail("batteryTalkTimeSpecification details are not available");
		}
	}

	/**
	 * Verifies ports Specifications of the device on PDP
	 */
	public void verifyPortsSpecificationAndValue() {

		try {
			Assert.assertTrue(portsSpecification.isDisplayed(), "portsSpecification isn't available");
			Assert.assertTrue(portsSpecificationValue.isDisplayed(), "portsSpecificationValue isn't available");
			Reporter.log("portsSpecification are available");
		} catch (Exception e) {
			Assert.fail("portsSpecification details are not available");
		}
	}

	/**
	 * Verifies Connectivity Specifications of the device on PDP
	 */
	public void verifyConnectivitySpecificationAndValue() {
		try {
			Assert.assertTrue(connectivitySpecification.isDisplayed(), "connectivitySpecification isn't available");
			Assert.assertTrue(connectivitySpecificationValue.isDisplayed(),
					"connectivitySpecificationValue isn't available");
			Reporter.log("connectivitySpecification are available");
		} catch (Exception e) {
			Assert.fail("connectivitySpecification details are not available");
		}
	}

	/**
	 * Verifies processor Specifications of the device on PDP
	 */
	public void verifyProcessorSpecificationAndValue() {
		try {
			Assert.assertTrue(processorSpecification.isDisplayed(), "processorSpecification isn't available");
			Assert.assertTrue(processorSpecificationValue.isDisplayed(), "processorSpecificationValue isn't available");
			Reporter.log("processorSpecification are available");
		} catch (Exception e) {
			Assert.fail("processorSpecification details are not available");
		}
	}

	/**
	 * Verifies OS Specifications of the device on PDP
	 */
	public void verifyOSSpecificationAndValue() {
		try {
			Assert.assertTrue(oSSpecification.isDisplayed(), "oSSpecification isn't available");
			Assert.assertTrue(oSSpecificationValue.isDisplayed(), "oSSpecificationValue isn't available");
			Reporter.log("oSSpecification are available");
		} catch (Exception e) {
			Assert.fail("oSSpecification details are not available");
		}
	}

	/**
	 * Verifies Ram Specifications of the device on PDP
	 */
	public void verifyRamSpecificationAndValue() {
		try {
			Assert.assertTrue(ramSpecification.isDisplayed(), "ramSpecification isn't available");
			Assert.assertTrue(ramSpecificationValue.isDisplayed(), "ramSpecificationValue isn't available");
			Reporter.log("ramSpecification are available");
		} catch (Exception e) {
			Assert.fail("ramSpecification details are not available");
		}
	}

	/**
	 * Verifies memory Specifications of the device on PDP
	 */
	public void verifyMemorySpecificationAndValue() {
		try {
			Assert.assertTrue(memorySpecification.isDisplayed(), "memorySpecification isn't available");
			Assert.assertTrue(memorySpecificationValue.isDisplayed(), "memorySpecificationValue isn't available");
			Reporter.log("memorySpecification are available");
		} catch (Exception e) {
			Assert.fail("memorySpecification details are not available");
		}
	}

	/**
	 * Verifies wirelessNetworkTechSpecification of the device on PDP
	 */
	public void verifyWirelessNetworkTechSpecificationAndValue() {
		try {
			Assert.assertTrue(wirelessNetworkTechSpecification.isDisplayed(),
					"wirelessNetworkTechSpecification isn't available");
			Assert.assertTrue(wirelessNetworkTechSpecificationValue.isDisplayed(),
					"memorySpecificationValue isn't available");
			Reporter.log("wirelessNetworkTechSpecification are available");
		} catch (Exception e) {
			Assert.fail("wirelessNetworkTechSpecification details are not available");
		}
	}

	/**
	 * Verifies supportedEmailSpecification of the device on PDP
	 */
	public void verifySupportedEmailSpecificationAndValue() {
		try {
			Assert.assertTrue(supportedEmailSpecification.isDisplayed(), "supportedEmailSpecification isn't available");
			Assert.assertTrue(supportedEmailSpecificationValue.isDisplayed(),
					"supportedEmailSpecificationValue isn't available");
			Reporter.log("supportedEmailSpecification are available");
		} catch (Exception e) {
			Assert.fail("supportedEmailSpecification details are not available");
		}
	}

	/**
	 * Verifies wEACapableSpecification of the device on PDP
	 */
	public void verifyWEACapableSpecificationAndValue() {
		try {
			Assert.assertTrue(weaCapableCTAOnPDP.isDisplayed(), "wEACapableSpecification isn't available");
			Assert.assertTrue(wEACapableSpecificationValue.isDisplayed(),
					"wEACapableSpecificationValue isn't available");
			Reporter.log("wEACapableSpecification are available");
		} catch (Exception e) {
			Assert.fail("wEACapableSpecification details are not available");
		}
	}

	/**
	 * Verifies hearingAidSpecification of the device on PDP
	 */
	public void verifyHearingAidSpecificationAndValue() {
		try {
			Assert.assertTrue(hearingAidSpecification.isDisplayed(), "hearingAidSpecification isn't available");
			Assert.assertTrue(hearingAidSpecificationValue.isDisplayed(),
					"hearingAidSpecificationValue isn't available");
			Reporter.log("hearingAidSpecification are available");
		} catch (Exception e) {
			Assert.fail("hearingAidSpecification details are not available");
		}
	}

	/**
	 * Verifies mobileHotspotSpecificationValue of the device on PDP
	 */
	public void verifyMobileHotspotSpecificationAndValue() {
		try {
			Assert.assertTrue(mobileHotspotSpecification.isDisplayed(), "mobileHotspotSpecification isn't available");
			Assert.assertTrue(mobileHotspotSpecificationValue.isDisplayed(),
					"mobileHotspotSpecificationValue isn't available");
			Reporter.log("mobileHotspotSpecification are available");
		} catch (Exception e) {
			Assert.fail("mobileHotspotSpecification details are not available");
		}
	}

	/**
	 * Verifies camerasSpecificationValue of the device on PDP
	 */
	public void verifyCamersSpecificationAndValue() {
		try {
			Assert.assertTrue(camerasSpecification.isDisplayed(), "camerasSpecification isn't available");
			Assert.assertTrue(camerasSpecificationValue.isDisplayed(), "camerasSpecificationValue isn't available");
			Reporter.log("camerasSpecification are available");
		} catch (Exception e) {
			Assert.fail("camerasSpecification details are not available");
		}
	}

	/**
	 * Verifies frequencySpecification of the device on PDP
	 */
	public void verifyFrequencySpecificationAndValue() {
		try {
			Assert.assertTrue(frequencySpecification.isDisplayed(), "frequencySpecification isn't available");
			Assert.assertTrue(frequencySpecificationValue.isDisplayed(), "camerasSpecificationValue isn't available");
			Reporter.log("frequencySpecification are available");
		} catch (Exception e) {
			Assert.fail("frequencySpecification details are not available");
		}
	}

	/**
	 * click positive Review for Accessory PDP
	 */
	public PDPPage clickColorONPDP() {
		try {
			if (isElementDisplayed(selectColor.get(0))) {
				waitFor(ExpectedConditions.visibilityOf(selectColor.get(selectColor.size() - 1)));
				clickElementWithJavaScript(selectColor.get(selectColor.size() - 1));
			}
		} catch (Exception e) {
			Assert.fail("Failed to select color for device in PDP");
		}
		return this;
	}

	/**
	 * Get estimated Ship date in pdp page
	 */
	public String getEstimatedShipDate() {
		String estdate[] = new String[0];
		try {
			estdate = null;
			if (estimatedShipDate.isDisplayed()) {
				estdate = estimatedShipDate.getText().replace(" ", "").split(":");
			}
		} catch (Exception e) {
			Assert.fail("Failed to get estimated ship date");
		}
		return estdate[1];
	}

	/**
	 * Verify PDP default memory variant
	 * 
	 * @return
	 */
	public PDPPage verifyMemoryVariant(String memory) {
		try {
			Assert.assertTrue(deviceMemory.getText().equalsIgnoreCase(memory));
			Reporter.log("Memory variant is available");
		} catch (Exception e) {
			Assert.fail("Memory variant is not displayed");
		}
		return this;
	}

	/**
	 * Verify PDP color variant
	 * 
	 * @return
	 */
	public PDPPage verifyDeviceColor(String color) {
		try {
			Assert.assertTrue(deviceColor.getText().equalsIgnoreCase(color));
			Reporter.log("Device Color is Displayed");
		} catch (Exception e) {
			Assert.fail("Device Color is Not Displayed");
		}
		return this;
	}

	/**
	 * Select the color based on data
	 * 
	 * @param color
	 * @return
	 */
	public PDPPage chooseColor(String color) {
		try {
			if (colorVariants.size() > 4) {
				colorDropdown.click();
				waitforSpinner();
				getDriver().findElement(By.xpath("//label//img[@aria-label='" + color + "']")).click();
				Reporter.log("Device Color " + color + " is selected");
			} else {
				getDriver().findElement(By.xpath("//label//img[@aria-label='" + color + "']")).click();
				Reporter.log("Device Color " + color + " is selected");
			}
		} catch (Exception e) {
			Assert.fail("Unable to select the respective color");
		}
		return this;
	}
}
