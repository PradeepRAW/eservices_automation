package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class PoipConfirmationPage extends CommonPage {
	private final String pageUrl = "/docuSignThankYou";
	
	@FindBy(css = "p.Display4")
	private WebElement pageHeader;

	@FindBy(css = "")
	private WebElement backToLeaseButton;
	
	@FindBy(css = "")
	private WebElement accountHistorylink;
	
	@FindBy(css = "h4.h4-title-mobile")
	private WebElement thanksHeader;
	
	@FindBy(css="span.alert-align-text")
	private WebElement PopipConfirmationPaymentNotification;
	
	@FindBy(css="span.alert-align-text")
	private WebElement PopipConfirmationAlertNotification;
	
	@FindBy(css="span.alert-align-text")
	private WebElement PopipConfirmationTowHoursNotification;
	
	@FindBy(css = "a[ng-click='vm.showDetails(true)']")
	private WebElement showOrderDetailsLink;

	@FindBy(css = "a[ng-click='vm.showDetails(false)']")
	private WebElement hideOrderDetailsLink;
	
	@FindBy(css = ".fine-print-body.pull-left.ng-binding")
	private List<WebElement> OrderSectionDetails;
	
	private By orderSpinner = By.cssSelector("i.fa.fa-spinner");

	public PoipConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PoipConfirmationPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}
	
	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PoipConfirmationPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			thanksHeader.isDisplayed();
			Reporter.log("PoipConfirmation/DocuSignThankYou Page is loaded.");
		} catch (Exception e) {
			Assert.fail("PoipConfirmation/DocuSignThankYou page is not loaded");
		}
		return this;
	}

	
	/**
	 * verify Back To Lease Button is displayed
	 * 
	 */
	public void verifyBackToLeaseButton() {
		try {
			backToLeaseButton.isDisplayed();
			Reporter.log("Backto Lease Button is displayed in POIP Confirmation/DocuSignSuccess Page");
		} catch (Exception e) {
			Assert.fail("Backto Lease Button is not displayed in POIP Confirmation/DocuSignSuccess Page");
		}
	}
	
	
	/**
	 * Click Back To Lease Button 
	 * 

	 */
	public void clickBackToLeaseButton() {
		try {
			backToLeaseButton.click();
			Reporter.log("Backto Lease Button is clicked in POIP Confirmation/DocuSignSuccess Page");
		} catch (Exception e) {
			Assert.fail("Backto Lease Button is not displayed in POIP Confirmation/DocuSignSuccess Page");
		}
	}
	
	/**
	 * verify AccountHistory Link is displayed
	 * 
	 */
	public void verifyAccountHistoryLinkDisplayed() {
		try {
			accountHistorylink.isDisplayed();
			Reporter.log("AccountHistory Link  is displayed in POIP Confirmation/DocuSignSuccess Page");
		} catch (Exception e) {
			Assert.fail("AccountHistory Link is not displayed in POIP Confirmation/DocuSignSuccess Page");
		}
	}
	
	
	/**
	 * Click Account History  Link 
	 * 

	 */
	public void clickAccountHistoryLink() {
		try {
			accountHistorylink.click();
			Reporter.log("AccountHistory Link is clicked in POIP Confirmation/DocuSignSuccess Page");
		} catch (Exception e) {
			Assert.fail("AccountHistory Link is not displayed in POIP Confirmation/DocuSignSuccess Page");
		}
	}
	

	/**
	 * verify Payment Confirmation Alert
	 * 
	 * @return
	 */
	
	public void verifyPopipConfirmationPaymentAlert(String ConfirmationAlert) {
		try {
			
			waitFor(ExpectedConditions.visibilityOf(PopipConfirmationPaymentNotification));
			Assert.assertTrue(PopipConfirmationPaymentNotification.getText().contains(ConfirmationAlert));
			Reporter.log(PopipConfirmationPaymentNotification.getText() + "POIP Payment confirmation  message is displayed on POIP Confirmation/DocuSign Thank you Page");
		} catch (Exception e) {
			Assert.fail("POIP Payment confirmation  message is not displayed on POIP Confirmation/DocuSign Thank you Page");
		}
	}
	

	/**
	 * verify POIP confirmation Alert
	 * 
	 * @return
	 */
	
	public void verifyPopipConfirmationAlert(String ConfirmationAlert) {
		try {
			
			waitFor(ExpectedConditions.visibilityOf(PopipConfirmationAlertNotification));
			Assert.assertTrue(PopipConfirmationAlertNotification.getText().contains(ConfirmationAlert));
			Reporter.log(PopipConfirmationAlertNotification.getText() + "POIP confirmation Alert message is displayed on POIP Confirmation/DocuSign Thank you Page");
		} catch (Exception e) {
			Assert.fail("POIP confirmation Alert message is not displayed on POIP Confirmation/DocuSign Thank you Page");
		}
	}
	
	
	/**
	 * verify POIP Two Hours Alert
	 * 
	 * @return
	 */
	
	public void verifyPopipConfirmationTwoHoursAlert(String ConfirmationAlert) {
		try {
			
			waitFor(ExpectedConditions.visibilityOf(PopipConfirmationTowHoursNotification));
			Assert.assertTrue(PopipConfirmationTowHoursNotification.getText().contains(ConfirmationAlert));
			Reporter.log(PopipConfirmationTowHoursNotification.getText() + "POIP Tow Hours confirmation  message is displayed on POIP Confirmation/DocuSign Thank you Page");
		} catch (Exception e) {
			Assert.fail("POIP Tow Hoursconfirmation  message is not displayed on POIP Confirmation/DocuSign Thank you Page");
		}
	}
	
	/**
	 * verify card details are displayed
	 * 
	 * @return
	 */
	public PoipConfirmationPage verifyOrderDetails() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(orderSpinner));
			showOrderDetailsLink.isDisplayed();
			showOrderDetailsLink.click();
			for (WebElement webElement : OrderSectionDetails) {
				webElement.isDisplayed();
			}
			hideOrderDetailsLink.click();
			Reporter.log("Verified Order details");
		} catch (Exception e) {
			Assert.fail("Order details component not found");
		}
		return this;
	}
	
	/**
	 * verify PII masking on required elements of in POIP confirmation Message
	 * 
	 * 
	 * @param piiAccountHistoryLink
	 * @param piiPaymentConfirmationMessage

	 */
	public void verifyPiiMasking(String piiAccountHistoryLink, String piiPaymentConfirmationMessage) {
		try {
			
				Assert.assertTrue(
						checkElementisPIIMasked(accountHistorylink, piiAccountHistoryLink));
				Assert.assertTrue(checkElementisPIIMasked(PopipConfirmationPaymentNotification, piiPaymentConfirmationMessage));
				
			Reporter.log("Verified PII Masking for Account History Link, PaymentConformation Message  are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking in POIP succsessapage");
		}
	}
	
}
