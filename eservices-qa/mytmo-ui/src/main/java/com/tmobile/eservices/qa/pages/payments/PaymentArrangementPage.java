package com.tmobile.eservices.qa.pages.payments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * 
 * @author pshiva
 *
 */
public class PaymentArrangementPage extends CommonPage {

	@FindBy(css = "div.row.padding-top-medium div.ng-scope span.radio-ht")
	private List<WebElement> installmentOne;

	@FindAll({ @FindBy(css = "div#di_installmentNumber a:nth-of-type(2)"),
			@FindBy(css = "div#instRadioBlock div:nth-of-type(2) >label") })
	private List<WebElement> installmentTwo;

	@FindBy(xpath = "//div[contains(text(), 'Total Balance')]//..//..//span")
	private WebElement currentBalance;

	@FindBy(css = "div#total_amount_onEdit")
	private WebElement currentBalanceMobile;

	@FindAll({ @FindBy(css = "div.body-copy-description-regular"),
			@FindBy(css = ".body-bold.pl-2.pl-sm-2.pl-md-3.pl-xl-3.pl-lg-3") })
	private List<WebElement> installmentdates;

	@FindBy(css = "span.numbered-list-label")
	private List<WebElement> noOfInstallments;

	@FindAll({ @FindBy(css = ".body-copy-description-regular.pull-right"), @FindBy(css = "span[id*='instAmt']"),
			@FindBy(css = ".body.padding-horizontal-xsmall") })
	private List<WebElement> installmentAmounts;

	@FindBy(css = "span.fine-print-body.fine-print-body-color")
	private List<WebElement> installmentsStatus;

	@FindBy(css = "span.fine-print-body.fine-print-body-color2")
	private WebElement paidInstallmentStatus;

	@FindBy(css = "input#installment-amount-1")
	private WebElement installmentAmountOne;

	@FindBy(css = "[id= 'scheduleInstBlock-1'] > div:nth-of-type(3) > input")
	private WebElement installmentAmountOneMobile;

	@FindBy(css = "input#insallment-input-0")
	private WebElement installmentAmountsOne;

	@FindBy(css = ".amount-format-label")
	private WebElement installmentAmountsOneLabel;

	@FindBy(css = "input#installment-amount-2")
	private WebElement installmentAmountTwo;

	@FindBy(css = "[id= 'scheduleInstBlock-2'] > div:nth-of-type(3) > input")
	private WebElement installmentAmountTwoMobile;

	@FindBy(css = "input#modalCancelBtnYes")
	private WebElement paymentArrangementOkButton;

	@FindBy(css = "div#PAAccept")
	private WebElement paymentArrangementOkButtonMobile;

	@FindBy(id = "installment-date-row-1")
	private WebElement clickonInstallement;

	@FindBy(id = "installmentDetails")
	private WebElement clickonInstallementMobile;

	@FindBy(id = "epAlertMsg")
	private WebElement autoPayAlertMsg;

	/*
	 * @FindBy(xpath =
	 * "//div[@id='parent_alert']//div[2]//span[@id='di_alrttitle']") private
	 * WebElement autoPaytxt;
	 */

	@FindBy(xpath = "//div[@id='parent_alert']//div[2]//span[@id='di_alrttitle']")
	private WebElement autoPaytxt;

	@FindBy(id = "di_alrtmssg")
	private WebElement autoPayMsg;

	/*
	 * @FindBy(xpath =
	 * "//div[@id='parent_alert']//div[2]//span[@id='pa_newalertblade_globalalert']")
	 * private WebElement autoPayMsg;
	 */

	@FindBy(css = "input[ng-click='$ctrl.openCalendar($index)']")
	private WebElement datePicker;

	@FindBy(xpath = "//div[contains(text(),'Pick a date')]")
	private WebElement pickdate;

	@FindAll({ @FindBy(css = "div.pagetitle_wrap.co_pagetitle span.ui_headline"),
			@FindBy(css = "div h4.h3-title.h4-title-mobile") })
	private WebElement paymenArrangementheader;

	@FindBy(css = "#paymentDetails > div.span12 > p")
	private WebElement paymentDetailsheader;

	@FindBy(id = "bankdetailsinfo")
	private WebElement bankDetailsHeader;

	@FindBy(css = "div.co_checkingpayment")
	private WebElement reviewPaymentpage;

	@FindBy(css = "label[for='new-checking']")
	private WebElement newCheckingAccountRadioBtn;

	@FindBy(css = "#pay-online-call-care > div.payment_amt1.pull-left.active")
	private WebElement payOnlinecareQuadrant;

	@FindBy(id = "call-care-div")
	private WebElement callCareQuadrant;

	@FindBy(id = "di_pastDueBal_div")
	private WebElement pastDueQuadrant;

	@FindBy(id = "di_currentBal_div")
	private WebElement currentBalanceQuadrant;

	@FindBy(css = "div.week.ng-scope span:not([class*='different-month']):not([class*='button-visibility'])")
	private List<WebElement> allDates;

	@FindBy(xpath = "//span[contains(@class,'day ng-binding ng-scope selected')]")
	private WebElement lastEligibleDate;

	@FindBy(xpath = "//*[@id='idDatePicker']//table/tbody/tr/td/a")
	private List<WebElement> allDatesMobile;

	@FindBy(id = "calender-input-1")
	private WebElement selectedDatetxtBox;

	@FindBy(id = "paymentDetails")
	private WebElement paymentDetailspage;

	@FindBy(css = ".btn.btn-secondary.SecondaryCTA.glueButton.glueButtonSecondary.gluePull-right-sm.ng-binding")
	private WebElement cancelButton;

	@FindBy(css = "div#installmentBlade >div#modify")
	private WebElement scheduledPaymentdetails;

	@FindBy(css = "button.btn.btn-primary.button-title.PrimaryCTA")
	private WebElement paymentArrangementdialogyesBtn;

	@FindBy(css = "i.fa.fa-exclamation-triangle.text-red")
	private WebElement paymentwarningesclamation;

	@FindBy(xpath = "//p[text()='You must add a payment method']")
	private WebElement addpaymentmethodwarning;

	/*
	 * @FindBy(id="paymentMethodBlade") private WebElement addPaymentmethodMobile;
	 */

	@FindAll({ @FindBy(id = "paymentMethodBlade"), @FindBy(id = "paModEditBtn") })
	public List<WebElement> addPaymentmethodMobile;

	@FindBy(id = "paSetupHeader")
	private WebElement paymentArrangementheaderMobile;

	@FindBy(id = "ic_credit_card")
	private WebElement addCreditcardMobile;

	@FindBy(id = "add_card_method_continue")
	private WebElement continueButtonPAmobile;

	@FindBy(id = "ctn-btn-installment")
	private WebElement continueinstallmentBtnPAmobile;

	@FindBy(id = "agreeBtn")
	private WebElement agreeSubmitbtnPAMobile;

	@FindBy(id = "paTitle")
	private WebElement paymentDeclinedPAmobile;

	@FindBy(id = "ic_bank_card")
	private WebElement addBankPAmobile;

	@FindBy(id = "idSuccessTxt")
	private WebElement checkingAcctsucessMessagePA;

	@FindBy(id = "pa_tnC")
	private WebElement termsAndconditionsPAmobile;

	@FindBy(id = "termpo")
	private WebElement payementArrangementtermsConditionsheader;

	@FindAll({ @FindBy(id = "chkacc-number"), @FindBy(id = "account-number") })
	private List<WebElement> accountNumber;

	@FindAll({ @FindBy(id = "chkacc-name"), @FindBy(id = "account-name") })
	private List<WebElement> newCheckingAccountName;

	@FindAll({ @FindBy(id = "chkacc-routingnum"), @FindBy(id = "routing-number") })
	private List<WebElement> routingNumber;

	@FindBy(id = "cc-next")
	private WebElement nxtBtn;

	@FindBy(id = "addBankContinueBtn")
	private WebElement addBankContinueBtn;

	@FindBy(css = "div#installmentSection div.tab-col")
	private WebElement editInstallments;

	@FindBy(id = "paModEditBtn")
	private WebElement editButton;

	@FindBy(css = "tnc-component h4")
	private WebElement termsconditionspage;

	@FindBy(xpath = "//div/span/a[contains(text(),'Terms & Conditions')]")
	private WebElement termsconditionsLink;

	@FindBy(xpath = "//div/span/a[contains(text(),'Terms & Conditions')]//..//..//span")
	private WebElement termsconditionsText;

	@FindBy(css = "div#installmentDetails span")
	private WebElement installmentDetails;

	@FindBy(xpath = "//span[contains(@class,'icon-echeckAccount')]/following-sibling::p")
	private WebElement storedPaymentCheckingNumber;

	private static final String AJAX_WAITCURSOR = "ajax_waitCursor";
	protected By paspinner = By.cssSelector("p.acsBlurb");
	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");
	protected By screenlock = By.cssSelector("div.screen-lock");
	protected By modaldrop = By.cssSelector("div.acsModalBackdrop.acsAbandonButton");

	@FindBy(id = "account_name")
	private WebElement accountName;

	@FindBy(id = "routing_number")
	private WebElement routingnum;

	@FindBy(id = "account_number")
	private WebElement bankAccountNumber;

	@FindBy(css = "button.btn.btn-primary.padding-bottom-large.primaryCTA.no-padding.width-md.ng-binding")
	private WebElement continueAddBank;

	@FindAll({@FindBy(css = "div#app-btn-pin-bottom button.btn.btn-primary.PrimaryCTA"), @FindBy(css = "#acceptButton")})
	private WebElement agreeAndSubmitbtn;

	@FindBy(css = ".btn.btn-secondary.SecondaryCTA.glueButton.glueButtonSecondary.gluePull-right-sm.ng-binding")
	private WebElement cancelPAbtn;

	@FindBy(css = "#missedPaymentBtn")
	private WebElement makePaymentbtn;

	@FindBy(xpath = "//span[contains(@class,'Card')]/following-sibling::p")
	private WebElement storedCardNumber;

	@FindBy(css = "addbank div h4")
	private WebElement bankHeader;

	@FindBy(id = "back")
	private WebElement bankSpokePageBackBtn;

	@FindBy(xpath = "//p[contains(text(),'Add Bank')]")
	private WebElement addBank;

	@FindBy(css = "div#card p.body-copy-description-bold")
	private WebElement addCard;

	@FindBy(xpath = "//h4[contains(text(),'Bank Information')]")
	private WebElement bankSpokePage;

	@FindBy(xpath = "//h3[contains(text(),'Card Information')]")
	private WebElement cardSpokePage;

	@FindBy(css = "div.card-information-title")
	private WebElement cardHeader;

	@FindBy(id = "setup")
	private WebElement paInstallementSetup;

	@FindBy(css = "h4.h3-title-md.text-bold.ng-binding")
	private WebElement spokePageHeader;

	@FindBy(css = "div#addPayment div.body-copy-description-bold")
	private WebElement paymentMethod;

	@FindBy(css = "div#addPayment div p")
	private WebElement savedPaymentMethod;

	protected By jumpSpinnericon = By.className("ajax_loader");

	@FindBy(css = "h4.h3-title")
	private WebElement paymentArrangementHeader;

	@FindBy(xpath = "//p[contains(text(),'Edit details')]")
	private WebElement installmentPageHeader;

	@FindAll({ @FindBy(css = "div#installmentBlade div p"), @FindBy(css = "div.TMO-PAYMENT-SCHEDULE-BLADE div span") })
	private WebElement installmentBlade;

	@FindBy(css = "div#calender-head-section button.PrimaryCTA")
	private WebElement updateDate;

	@FindBy(css = "button[ng-click='$ctrl.updatePaSetup()']")
	private WebElement updateButton;

	@FindBy(css = "div p a")
	private WebElement linkOnTnC;

	@FindBy(id = "name_on_card")
	private WebElement cardName;

	@FindBy(id = "card_number")
	private WebElement cardNumber;

	@FindBy(id = "expiration_month")
	private WebElement expiryMonth;

	@FindBy(id = "expiration_year")
	private WebElement expiryYear;

	@FindBy(id = "CVV")
	private WebElement cvvCode;

	@FindBy(id = "Zip")
	private WebElement zip;

	@FindBy(css = "div#continue button")
	private WebElement continueAddCard;

	@FindBy(css = "div#addPayment div.inline.pull-right")
	private WebElement savedPaymentMethodOnPaymentBlade;

	@FindBy(id = "continue")
	private WebElement continuebtn;

	@FindBy(css = "div.radio.no-margin")
	private List<WebElement> spokePageRadioBtn;

	@FindBy(css = "div.fine-print-body.color-9B9B9B")
	private WebElement convinenceFeeMsg;

	@FindBy(xpath = "//p[contains(text(), 'Remove Existing')]")
	private WebElement removeExisting;

	@FindBy(xpath = "//p[contains(text(), '')]")
	private WebElement paymentLater;

	@FindBy(xpath = "//input[@id='paymentInstrument14']")
	private WebElement radioButtonRemoveExistingPaymentMethod;

	/*
	 * @FindBy(css = "[ng-click='$ctrl.onAgreeSubmitClick()']") private WebElement
	 * agreeNSubmitBtn;
	 */

	@FindBy(css = "[ng-click='$ctrl.goToPaPage()']")
	private WebElement faqbackBtn;

	@FindBy(xpath = "//div/a[contains(text(),'See FAQ')]")
	private WebElement faqsLink;

	@FindBy(css = "tnc-component button")
	private WebElement termsconditionsbackBtn;

	@FindAll({ @FindBy(css = "div.col-12.padding-top-small.col-md-10.offset-md-1 p"),
			@FindBy(css = "div.row.padding-top-medium.ng-scope  div.fine-print-body") })
	private List<WebElement> alertMsgs;

	@FindBy(css = "div.no-pa-eligible.no-body-scroll")
	private WebElement paNotEligibleMsg;

	@FindBy(css = "span.radio-ht.colorGrey")
	private WebElement installemnetsTwo;

	@FindBy(css = "p[ng-if*='ctrl.maxNoOfInstallments']")
	private WebElement oneInstallementMessage;

	@FindBy(css = "h4.h3-title")
	private WebElement faqheader;

	@FindBy(css = "p.body-copy-description.inline")
	private WebElement nopaymentmethodError;

	@FindBy(css = "#calender-head-section div p.button-title")
	private WebElement datePageHeader;

	@FindBy(css = "span.pa-date-legends")
	private WebElement selectedDate;

	@FindBy(css = "div span.pl-2.mb-0.legal")
	private WebElement dueDate;

	@FindBy(css = ".row.p-t-20.p-b-20 div p.body-copy-description-normal")
	private WebElement cancelModal;

	@FindBy(css = "div.modal-dialog button.PrimaryCTA")
	private WebElement cancelModalYesCTA;

	@FindBy(css = "div.modal-dialog button.SecondaryCTA")
	private WebElement cancelModalNoCTA;

	// @FindBy(css = "div[ng-if='$ctrl.toShowViewDetailsLink'] a")
	@FindBy(css = ".row.padding-top-medium .pull-right.fine-print-body.p-r-15-md.ng-scope")
	private WebElement viewDetailsLink;

	@FindBy(css = "div.modal-dialog h2")
	private WebElement breakdownModal;

	@FindBy(css = "div.modal-dialog div.body-copy-description-bold.pull-left")
	private WebElement totalOnBeakdownModal;

	@FindBy(css = "div.modal-dialog div.body-copy-description-bold.pull-right")
	private WebElement totalAmountOnBeakdownModal;

	@FindBy(css = "button.pa-breakdown-close")
	private WebElement paBreakdownCloseBtn;

	@FindBy(css = ".multipleOption .row.padding-top-medium .body-copy-description-regular.pull-right")
	private WebElement amountBal;

	@FindBy(xpath = "p[contains(text(),'Total Balance')]")
	private WebElement totalBalance;

	@FindAll({ @FindBy(css = "span.body-copy-description-bold.pull-right"), @FindBy(css = "span.Display6") })
	private WebElement totalBalAmount;

	@FindBy(xpath = "p[contains(text(),'Minimum to Restore')]")
	private WebElement minToRestore;

	@FindBy(xpath = "p[contains(text(),'Past Due')]")
	private WebElement pastDueBalance;

	@FindBy(xpath = "//div/p[contains(text(),'Choose date')]")
	private WebElement chooseDateHeader;

	@FindBy(css = "button[ng-click='$vm.updateDate()']")
	private WebElement dateUpdateBtn;

	@FindBy(css = "button[ng-click='$vm.cancelInstallmentCalendar()']")
	private WebElement dateCancelBtn;

	@FindBy(css = ".fine-print-body-bold.text-red")
	private WebElement missedPaymentAlertHeader;

	@FindBy(css = ".fa-exclamation-circle.circle_width1")
	private WebElement missedPaymentAlertIcon;

	@FindBy(css = ".fine-print-body.ng-binding.fine-print-body-color1")
	private WebElement missedPaymentAlertInstallment;

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindAll({ @FindBy(css = "#instDate_1"), @FindBy(css = "p.Display3"),
			@FindBy(css = "h4.h3-title.h4-title-mobile") })
	private WebElement pageLoadElement;

	private final String pageUrl = "/paymentarrangement";

	@FindBy(css = "p.body-copy-description-regular.black")
	private WebElement storedPaymentNo;

	@FindBy(css = "div#addPayment div > div.pull-right p")
	private WebElement savedcardonPaymentBlade;

	@FindBy(css = "div#addPayment i.fa.fa-angle-right")
	private WebElement paymentMethodChevron;

	@FindBy(css = "div.fine-print-body p")
	private List<WebElement> msgForPaymentMethodInsideBlackout;

	@FindBy(css = "")
	private WebElement msgForSecuredInsideBlackout;

	@FindBy(css = "span.body.ml-1")
	private List<WebElement> installementStatus;

	@FindBy(css = "[ng-if='$ctrl.showDueDate']")
	private WebElement showDueDate;

	@FindBy(css = "span#instDate_1")
	private WebElement instDateOne;

	@FindBy(css = "span#instDate_2")
	private WebElement instDateTwo;

	@FindBy(css = "")
	private List<WebElement> paDisallowedModal;

	SimpleDateFormat standardFormat = new SimpleDateFormat("yyyy-MM-DD");

	SimpleDateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy");

	@FindBy(css = "")
	private WebElement alertText;

	@FindBy(css = "")
	private WebElement modifySuspendedAlert;

	@FindBy(css = "")
	private WebElement modifySuspendedMesasageLanding;

	@FindBy(css = "")
	private WebElement modifySuspendedMesasageConfirmation;

	@FindBy(css = "div.Display6")
	private WebElement paymentScheduleHeader;

	@FindBy(css = ".box1.text-center")
	private WebElement installmentNumberOne;

	@FindBy(xpath = "//span[contains(text(),'You must add a payment method')]")
	private WebElement addPaymentMethodMesage;

	@FindBy(xpath = "//p[contains(text(),'Your payment arrangement has been scheduled')]")
	private WebElement paymentArrangementScheduleMessage;

	@FindBy(xpath = "//button[contains(text(),'Review Payment Arrangement')]")
	private WebElement reviewPaymentArrangementCTA;

	@FindBy(xpath = "//p[contains(text(),'Are you sure you want to leave this page? Any changes you have made will not be saved.')]")
	private WebElement cancelYesOrNoPopUp;

	@FindAll({ @FindBy(css = "h4.fine-print-body-bold.text-black.suspend-amount"), @FindBy(css = "p.body-bold.black"), @FindBy(css = "p.missedPayment.ng-star-inserted") })
	private WebElement subheaderText;

	@FindBy(css = "span.edit-text")
	private WebElement editLink;

	@FindBy(css = "div.exclamation-circle")
	private WebElement exclamationCircle;

	@FindBy(css = "div.fa-check-circle.green")
	private WebElement paidLogo;

	/**
	 * 
	 * @param webDriver
	 */
	public PaymentArrangementPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PaymentArrangementPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PaymentArrangementPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
		} catch (Exception e) {
			Assert.fail("Payment Arrangement page not loaded");
		}
		return this;
	}

	public PaymentArrangementPage clickDatePicker() {
		try {
			datePicker.click();
			Reporter.log("Clicked on Calendar icon");
		} catch (Exception e) {
			Assert.fail("Calender icon not found");
		}
		return this;
	}

	/**
	 * Select Date
	 */
	public PaymentArrangementPage pickDate() {
		try {

			checkPageIsReady();
			for (WebElement webElement : allDates) {
				webElement.click();
				break;
			}
			waitFor(ExpectedConditions.visibilityOf(updateDate));
			waitFor(ExpectedConditions.elementToBeClickable(updateDate));
			updateDate.click();
		} catch (Exception e) {
			Assert.fail("Date picker not found");
		}
		return this;
	}

	/**
	 * Click installment One;
	 */
	public PaymentArrangementPage clickinstallemtOne() {
		try {
			waitFor(ExpectedConditions.visibilityOf(installmentOne.get(0)));
			clickElementWithJavaScript(installmentOne.get(0));
			Reporter.log("Clicked on Installement One");
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("Installement one component not found");
		}
		return this;
	}

	public PaymentArrangementPage clickUpdateButton() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(updateButton));
			updateButton.click();
			Reporter.log("Clicked on update button");
		} catch (Exception e) {
			Assert.fail("Update button not found");
		}
		return this;
	}

	/**
	 * validate one installment
	 * 
	 * @return boolean
	 */
	public PaymentArrangementPage validateOneInstallment() {
		checkPageIsReady();
		String balance = currentBalance.getText().replace("$", "");
		String installment = null;
		try {
			installment = installmentAmountsOneLabel.getText().replace("$", "");
			balance.trim().equals(installment.trim());
			Reporter.log("Total installments displayed is ::: " + installmentAmountsOneLabel.getText());
		} catch (NoSuchElementException elemNotFound) {
			Assert.fail("Installment  amount not equals to balance");
		}
		return this;
	}

	/**
	 * Verify Payment Arrangement page is displayed or not
	 * 
	 * @return
	 */
	public PaymentArrangementPage verifyPaymentarrangementPage() {
		try {
			checkPageIsReady();
			/*
			 * if (getDriver() instanceof AppiumDriver) { return
			 * isElementDisplayed(paymentArrangementheaderMobile); } else {
			 */
			paymenArrangementheader.isDisplayed();
			Reporter.log("payment arrangement header displayed");
		} catch (Exception e) {
			Assert.fail("Payment arrangement header not found");
		}
		return this;
	}

	/**
	 * Get a Href link of AutoPay OFF
	 * 
	 * @return
	 */
	public PaymentArrangementPage verifyallVisibleDates() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement dateMob : allDatesMobile) {
					Assert.assertTrue(dateMob.isDisplayed(), Constants.VERIFY_VISIBLE_DATES_NOTDISPLAYED);
				}
			} else {
				for (WebElement date : allDates) {
					Assert.assertTrue(date.isDisplayed(), Constants.VERIFY_VISIBLE_DATES_NOTDISPLAYED);
				}
			}
		} catch (Exception e) {
			Assert.fail("Dates component missing ");
		}
		return this;
	}

	/**
	 * Verify Scheduled Payments details is display or not
	 * 
	 * @return
	 */
	public PaymentArrangementPage verifyScheduledpaymentDetails() {
		try {
			scheduledPaymentdetails.isDisplayed();
			Reporter.log("Scheduled payment details displayed");
		} catch (Exception e) {
			Assert.fail("Scheduled payment details not found");
		}
		return this;
	}

	/**
	 * Click Payment Arrangement Model Dialog box Yes
	 */
	public PaymentArrangementPage clickPayementarrangementModelYesBtn() {
		try {
			paymentArrangementdialogyesBtn.click();
			Reporter.log("Payment arrangement dialog Yes button clicked");
		} catch (Exception e) {
			Assert.fail("Yes button not found in payment dialog");
		}
		return this;
	}

	public PaymentArrangementPage clickinstallemtBlade() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.visibilityOf(installmentBlade));
			clickElementWithJavaScript(installmentBlade);
			Reporter.log("Clicked on installment blade");
		} catch (Exception e) {
			Assert.fail("Installment blade not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyInstallmentPageDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(installmentPageHeader));
			installmentPageHeader.isDisplayed();
			Reporter.log("Installment page header displayed");
		} catch (Exception e) {
			Verify.fail("Installement page header not found");
		}
		return this;
	}

	public PaymentArrangementPage clickCancelButton() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			cancelButton.click();
			Reporter.log("Clicked on cancel button");
		} catch (Exception e) {
			Verify.fail("Cancel button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyPApageDisplayed() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(paymentArrangementHeader));
			paymentArrangementHeader.isDisplayed();
			Reporter.log("Payment arrangement header is displayed");
		} catch (Exception e) {
			Verify.fail("PaymentArrangement Header not found");
		}
		return this;
	}

	public PaymentArrangementPage clickpaymentMethod() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(jumpSpinnericon));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("screen-lock")));
			waitFor(ExpectedConditions.elementToBeClickable(paymentMethod));
			clickElementWithJavaScript(paymentMethod);
			Reporter.log("Clicked on payment method");
		} catch (Exception e) {
			Assert.fail("Payment method not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyPaymentSpokePageDispalyed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(spokePageHeader));
			spokePageHeader.isDisplayed();
			Reporter.log("Spoke page header displayed");
		} catch (Exception e) {
			Assert.fail("Spoke page header not found");
		}
		return this;
	}

	public PaymentArrangementPage clickAddCard() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addCard));
			addCard.click();
			Reporter.log("Clicked on addcard");
		} catch (Exception e) {
			Assert.fail("Add card button not found");
		}
		return this;
	}

	/**
	 * get payment account number on blade
	 * 
	 * @return boolean
	 */
	public String getStoredcheckingaccountNumber() {
		try {
			storedPaymentCheckingNumber.isDisplayed();
			Reporter.log("Stored checking account number displayed");
		} catch (Exception e) {
			Assert.fail("Stored checking account number field not found");
		}
		return storedPaymentCheckingNumber.getText();
	}

	public PaymentArrangementPage clickinstallmentDetails() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.visibilityOf(installmentDetails));
			installmentDetails.click();
			Reporter.log("Clicked on installment details");
		} catch (Exception e) {
			Assert.fail("Installment details not found");
		}
		return this;
	}

	public PaymentArrangementPage clickTermsAndConditionsLink() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(termsconditionsLink));
			termsconditionsLink.click();
			Reporter.log("Clicked on terms and conditions");
		} catch (Exception e) {
			Verify.fail("Terms and conditions not found");
		}
		return this;
	}

	public PaymentArrangementPage verifytermsconditionspageDisplayed() {
		try {
			termsconditionspage.isDisplayed();
			Reporter.log("Terms and conditions page displayed");
		} catch (Exception e) {
			Verify.fail("Terms and conditions page not displayed");
		}
		return this;
	}

	public PaymentArrangementPage verifyHyperLinkonTnCpage() {
		try {
			linkOnTnC.isDisplayed();
			Reporter.log("Terms and conditions link displayed");
		} catch (Exception e) {
			Verify.fail("Terms and conditions link missing");
		}
		return this;
	}

	/**
	 * verify card information header
	 * 
	 * @return boolean
	 */
	public PaymentArrangementPage verifyCardInfoHeader() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(cardHeader));
			cardHeader.isDisplayed();
			Reporter.log("Card header displayed");
		} catch (Exception e) {
			Assert.fail("Card header not found");
		}
		return this;
	}

	/**
	 * Fill all the card details and click on continue
	 * 
	 * @param payment
	 */
	public PaymentArrangementPage fillCardDetails(Payment payment) {
		try {
			cardName.sendKeys(payment.getNameOnCard());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id(AJAX_WAITCURSOR)));
			waitFor(ExpectedConditions.elementToBeClickable(cardNumber));
			cardNumber.sendKeys(payment.getCardNumber());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id(AJAX_WAITCURSOR)));
			expiryMonth.sendKeys(payment.getExpiryMonth());
			expiryYear.sendKeys(payment.getExpiryYear());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id(AJAX_WAITCURSOR)));
			zip.sendKeys(payment.getZipCode());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id(AJAX_WAITCURSOR)));
			cvvCode.sendKeys(payment.getCvv());
			Reporter.log("Filled card details");
		} catch (Exception e) {
			Assert.fail("Fill card details component missing");
		}
		return this;
	}

	/**
	 * click continue button for add Card
	 */
	public PaymentArrangementPage clickContinueAddCard() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(continueAddCard));
			continueAddCard.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Continue button");
		} catch (Exception e) {
			Assert.fail("Continue button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyAgreeAndSubmitButton() {
		try {
			agreeAndSubmitbtn.isDisplayed();
			if (System.getProperty("environment").contains("reg9")) {
				Assert.assertTrue(agreeAndSubmitbtn.getText().equals("Agree & Submit"),
						"Agree and Submit CTA button text mismatch");
			}
			Reporter.log("Verified agree and submit button");
		} catch (Exception e) {
			Verify.fail("Agree and submit button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyAddPaymentMethodMessage() {
		try {
			Assert.assertTrue(addPaymentMethodMesage.isDisplayed());
			Reporter.log("Add payment Method message is displayed");
		} catch (Exception e) {
			Verify.fail("Failed to validate payment method message");
		}
		return this;
	}

	public PaymentArrangementPage verifyCancelButton() {
		try {
			cancelPAbtn.isDisplayed();
			Reporter.log("Cancel button found");
		} catch (Exception e) {
			Verify.fail("Cancel button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyMakePaymentButton() {
		try {
			makePaymentbtn.isDisplayed();
			Reporter.log("Make a payment button is displayed");
			makePaymentbtn.getText().contains("Make a payment");
			Reporter.log("Make a payment button text is valid");
			makePaymentbtn.getText().contains(installmentAmounts.get(0).getText());
			Reporter.log("Missed Installment amount is present on button");
		} catch (Exception e) {
			Assert.fail("Make payment component not found");
		}
		return this;
	}

	/**
	 * get payment card number on blade
	 * 
	 * @return
	 */
	public String getStoredCardNumber() {
		String ccn = null;
		try {
			storedCardNumber.getText();
		} catch (Exception e) {
			Assert.fail("Stored card number not found");
		}
		return ccn;
	}

	/**
	 * Click Balance Radio Button
	 */
	public PaymentArrangementPage clickPaymentMethodLater() {
		try {
			waitFor(ExpectedConditions.visibilityOf(spokePageRadioBtn.get(4)));
			if (!spokePageRadioBtn.get(4).isSelected()) {
				spokePageRadioBtn.get(4).click();
				Reporter.log("Clicked on Spoke page radio button");
			}
		} catch (Exception e) {
			Assert.fail("Payment method later button not found");
		}
		return this;
	}

	public PaymentArrangementPage clickContinuebtn() {
		try {
			continuebtn.click();
			Reporter.log("Clicked on continue button");
		} catch (Exception e) {
			Assert.fail("Continue button not found");
		}
		return this;
	}

	/**
	 * Verify Payment Arrangement Text
	 * 
	 * @return String
	 */
	public PaymentArrangementPage verifyPaymentMethodNotProvidedOnPaymentBlade() {
		try {
			checkPageIsReady();
			savedPaymentMethodOnPaymentBlade.getText().contains("None Provided");
		} catch (Exception e) {
			Assert.fail("Payment blade not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyMinimunInstallementCaseErrorDispalyed(String paMinInstallmentCaseErrorMsg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(paNotEligibleMsg));
			if (paNotEligibleMsg.getText().contains(paMinInstallmentCaseErrorMsg)) {
				Reporter.log("PA related message displayed :::: " + paNotEligibleMsg.getText());
			}
		} catch (Exception e) {
			Assert.fail("Minimum installment case error component not found");
		}
		return this;
	}

	/**
	 * Verify Eligible Message For One Installement
	 * 
	 * @return
	 */
	public PaymentArrangementPage verifyEligibleMessageForOneInstallement() {
		try {
			checkPageIsReady();
			oneInstallementMessage.isDisplayed();
			Reporter.log("Installment eligibility message displayed");
		} catch (Exception e) {
			Assert.fail("Eligibility message component not found");
		}
		return this;
	}

	/**
	 * verify autopay unroll message is displayed
	 * 
	 * @return
	 */
	public PaymentArrangementPage verifyAutopayUnrollAlert() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(alertMsgs.get(0)));
			for (WebElement alert : alertMsgs) {
				String autopayAlert = alert.getText();
				if (autopayAlert.replaceAll("[0-9]", "").contains(
						"You will be automatically un-enrolled from AutoPay & will no longer receive your $/mo. AutoPay discount. Re-enroll once your payment arrangement is complete.")
						|| autopayAlert.replaceAll("[0-9]", "").contains(
								"By setting up a payment arrangement, you will be automatically unenrolled from AutoPay")) {
					Reporter.log("Verified Autopay Un-Enrolled alert on PA page" + autopayAlert);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Autopay unroll alert component not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyTnCPageDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(termsconditionspage));
			termsconditionspage.isDisplayed();
			termsconditionspage.getText().equals("TERMS AND CONDITIONS");
		} catch (Exception e) {
			Verify.fail("Terms and conditions page not found");
		}
		return this;
	}

	public PaymentArrangementPage clickTnCBackBtn() {
		try {
			clickElementWithJavaScript(termsconditionsbackBtn);
			Reporter.log("Clicked on back button");
		} catch (Exception e) {
			Verify.fail("Back button not found");
		}
		return this;
	}

	public PaymentArrangementPage clickSeeFAQS() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(faqsLink));
			clickElementWithJavaScript(faqsLink);
			Reporter.log("FAQ Clicked");
		} catch (Exception e) {
			Verify.fail("FAQ's link not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyfaqPageDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(faqheader));
			faqheader.isDisplayed();
			faqheader.getText().equals("Frequently Asked Questions");
			Reporter.log("FAQ page Displayed");
		} catch (Exception e) {
			Verify.fail("FAQs not found");
		}
		return this;
	}

	public PaymentArrangementPage clickFaqBackBtn() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[ng-show='$ctrl.showSpinner']")));
			waitFor(ExpectedConditions.elementToBeClickable(faqbackBtn));
			faqbackBtn.click();
			Reporter.log("Clicked on FAQ back button");
		} catch (Exception e) {
			Verify.fail("FAQ back button missing");
		}
		return this;
	}

	public PaymentArrangementPage verifyFAQLinkIsWorking() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(faqsLink));
			clickElementWithJavaScript(faqsLink);
			waitFor(ExpectedConditions.visibilityOf(faqheader));
			faqheader.isDisplayed();
			Reporter.log("Frequently Asked Questions header is displayed");
			faqheader.getText().equals("Frequently Asked Questions");
			Reporter.log("Frequently Asked Questions header text is valid");
			// waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[ng-show='$ctrl.showSpinner']")));
			waitFor(ExpectedConditions.elementToBeClickable(faqbackBtn));
			faqbackBtn.click();
			verifyPApageDisplayed();
			Reporter.log("Payment Arrangement page is displayed");
		} catch (Exception e) {
			Assert.fail("FAQ link not found");
		}
		return this;
	}

	public PaymentArrangementPage clickAgreeNSubmitBtn() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(agreeAndSubmitbtn));
			clickElementWithJavaScript(agreeAndSubmitbtn);
			Reporter.log("Clicked on AgreeAndSubmitButton");
		} catch (Exception e) {
			Assert.fail("Agree and Submit button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyPaymentArrangementSetup() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paymentArrangementScheduleMessage));
			Assert.assertTrue(paymentArrangementScheduleMessage.isDisplayed());
			Reporter.log("scheduled PA successfully ");
		} catch (Exception e) {
			Assert.fail("failed to schedule PA");
		}
		return this;
	}

	public PaymentArrangementPage verifyAgreeAndSubmitBtnIsActive() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(agreeAndSubmitbtn));
			Assert.assertTrue(agreeAndSubmitbtn.isEnabled());
			Reporter.log(" AgreeAndSubmitButton is Active");
		} catch (Exception e) {
			Assert.fail("Agree and Submit button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyAgreeAndSubmitBtnIsInactive() {
		try {

			Assert.assertFalse(agreeAndSubmitbtn.isEnabled());

			Assert.assertTrue(agreeAndSubmitbtn.getAttribute("class").contains("disabled"),
					"Agree & Submit button is not Inactive");
			Reporter.log("Agree And Submit Button is Inactive");
		} catch (Exception e) {
			Assert.fail("Agree and Submit button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyConvenienceFeeAlertDisplayed() {
		try {
			checkPageIsReady();

			if (convinenceFeeMsg.getText().replaceAll("[0-9]", "").contains(
					"Pay in installments by setting up a payment arrangement online (it's free). Payments made over the phone may incur an $ payment support fee"
							+ "")) {
			}
		} catch (Exception e) {
			Assert.fail("Convenience fee component not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyPaymentMethodBlade() {
		try {
			paymentMethod.isDisplayed();
			Reporter.log("Payment method blade displayed");
		} catch (Exception e) {
			Verify.fail("Payment method blade not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyPaymentInstallmentsBlade() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(installmentBlade));
			installmentBlade.isDisplayed();
			Reporter.log("Installment blade displayed");
		} catch (Exception e) {
			Verify.fail("Installment blade missing");
		}
		return this;
	}

	public PaymentArrangementPage verifyAmountIsDisplayed() {
		try {
			amountBal.isDisplayed();
			Reporter.log("Balance amount displayed");
		} catch (Exception e) {
			Verify.fail("Balance amount not found");
		}
		return this;
	}

	/*
	 * Verify PA - Edit Details - Total Balance is displayed
	 */
	public PaymentArrangementPage verifyTotalBalanceDisplayd() {
		try {
			Assert.assertTrue(totalBalance.isDisplayed(), "Total Balance is not displayed");
			Reporter.log("Total Balance is displayed");
		} catch (Exception e) {
			Assert.fail("Total balance not found");
		}
		return this;
	}

	/*
	 * Verify PA - Edit Details - Past Due is displayed
	 */
	public PaymentArrangementPage verifyPastDueDisplayd() {
		try {
			pastDueBalance.isDisplayed();
			Reporter.log("Past Due is displayed");
		} catch (Exception e) {
			Assert.fail("Past Due is not displayed");
		}
		return this;
	}

	public PaymentArrangementPage verifyMissedPaymentAlertsDisplayd() {
		try {
			checkPageIsReady();
			Assert.assertTrue(missedPaymentAlertHeader.isDisplayed(), "Missed payment message header is not displayed");
			Reporter.log("Missed payment message header is displayed");
			Assert.assertTrue(missedPaymentAlertHeader.getText().equals("Missed payment. Immediate action needed!"),
					"Missed payment message header text is not valid");
			Reporter.log("Missed payment message header text is valid");
			Assert.assertTrue(missedPaymentAlertIcon.isDisplayed(),
					"Missed payment icon in installment line is not displayed");
			Reporter.log("Missed payment icon in installment line is not displayed");
			Assert.assertTrue(missedPaymentAlertInstallment.isDisplayed(),
					"Missed payment message in installment line is not displayed");
			Reporter.log("Missed payment message in installment line is displayed");
			Assert.assertTrue(missedPaymentAlertInstallment.getText().equals("Missed"),
					"Missed message text in installment line is not valid");
			Reporter.log("Missed message text in installment line is not valid");
			return this;
		} catch (Exception e) {
			Assert.fail("Missing Payment Alerts component not found");
		}
		return this;
	}

	/**
	 * verify view details link is displayed
	 * 
	 * @return boolean
	 */
	public PaymentArrangementPage verifyViewDetailsLink() {
		try {
			viewDetailsLink.isDisplayed();
			Reporter.log("View Details link displayed");
		} catch (Exception e) {
			Assert.fail("View details link not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyChooseDate() {
		try {
			chooseDateHeader.isDisplayed();
			chooseDateHeader.getText().contains("Choose date");
			Reporter.log("Choose date verified");
		} catch (Exception e) {
			Verify.fail("Date field not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyUpdateCTA() {
		try {
			updateButton.isDisplayed();
			Reporter.log("Update CTA displayed");
		} catch (Exception e) {
			Verify.fail("Update CTA button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyCancelCTA() {
		try {
			cancelButton.isDisplayed();
			Reporter.log("CTA Cancel Button Displayed");
		} catch (Exception e) {
			Verify.fail("CTA Cancel button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyBreakDownModal() {
		try {
			breakdownModal.isDisplayed();
			Reporter.log("Breakdown model displayed");
		} catch (Exception e) {
			Assert.fail("Break down model not found");
		}
		return this;
	}

	public PaymentArrangementPage verifyTotalOnBreakDownModal() {
		try {
			totalOnBeakdownModal.isDisplayed();
			totalOnBeakdownModal.getText().equals("Total");
			totalAmountOnBeakdownModal.isDisplayed();
			Reporter.log("Break down model verified");
		} catch (Exception e) {
			Assert.fail("Total break down model missing");
		}
		return this;
	}

	/**
	 * click view details link
	 */
	public PaymentArrangementPage clickViewDetails() {
		try {
			viewDetailsLink.isDisplayed();
			viewDetailsLink.click();
			Reporter.log("View details displayed");
		} catch (Exception e) {
			Assert.fail("View details not found");
		}
		return this;
	}

	/**
	 * verify the date page header
	 * 
	 * @return true/false
	 */
	public PaymentArrangementPage verifyDatePageHeader() {
		try {
			datePageHeader.isDisplayed();
			Reporter.log("Date page header verified");
		} catch (Exception e) {
			Assert.fail("Date page header not found");
		}
		return this;
	}

	/**
	 * verify date calendar is displayed
	 * 
	 * @return true/false
	 */
	public PaymentArrangementPage verifycalendar() {
		try {
			selectedDate.isDisplayed();
			selectedDate.getText().equals("Selected date");
			Reporter.log("Calender verified");
		} catch (Exception e) {
			Assert.fail("Calender component not found");
		}
		return this;
	}

	/**
	 * verify cancel modal and text
	 * 
	 * @return boolean
	 */
	public PaymentArrangementPage verifyCancelModal() {
		try {
			cancelModal.isDisplayed();
			cancelModal.getText().contains(Constants.PA_CANCEL_MODAL_TEXT);
			Reporter.log("Cancel Model verified");
		} catch (Exception e) {
			Verify.fail("Cancel model not found");
		}
		return this;
	}

	/**
	 * 
	 * verify cancel modal buttons
	 * 
	 * @return boolean
	 */
	public PaymentArrangementPage verifyCancelModalButtonsCTA() {
		try {
			cancelModalYesCTA.isDisplayed();
			cancelModalNoCTA.isDisplayed();
			Reporter.log("Cancel model button CTA displayed");
		} catch (Exception e) {
			Verify.fail("Cancel model button CTA not found");
		}
		return this;
	}

	/**
	 * click no CTA on cancel modal
	 */
	public PaymentArrangementPage clickNoCTAOnCancelModal() {
		try {
			cancelModalNoCTA.click();
			Reporter.log("Clicked on No CTA in Cancel model");
		} catch (Exception e) {
			Verify.fail("No CTA not found in Cancel model");
		}
		return this;
	}

	/**
	 * click no CTA on cancel modal
	 */
	public PaymentArrangementPage clickYesCTAOnCancelModal() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(jumpSpinnericon));
			cancelModalYesCTA.click();
			Reporter.log("Clicked on YES CTA in Cancel model");
		} catch (Exception e) {
			Assert.fail("YES CTA not found in Cancel Model");
		}
		return this;
	}

	/**
	 * click close icon on breakdown modal
	 */
	public PaymentArrangementPage clickCloseBreakdownModal() {
		try {
			paBreakdownCloseBtn.click();
			Reporter.log("Clicked on close breakdown model");
		} catch (Exception e) {
			Assert.fail("Break down model not found");
		}
		return this;
	}

	public PaymentArrangementPage verifycalendarUpdateBtn() {
		try {
			dateUpdateBtn.isDisplayed();
			Reporter.log("Date update button displayed");
		} catch (Exception e) {
			Assert.fail("Date update button not found");
		}
		return this;
	}

	public PaymentArrangementPage verifycalendarCancelBtn() {
		try {
			dateCancelBtn.isDisplayed();
			Reporter.log("Date cancel button displayed");
		} catch (Exception e) {
			Assert.fail("Date cancel button not found");
		}
		return this;
	}

	public PaymentArrangementPage clickCalendarCancelBtn() {
		try {
			dateCancelBtn.click();
			Reporter.log("Clicked on Calender Cancel button");
		} catch (Exception e) {
			Assert.fail("Cancel button not found in calender");
		}
		return this;
	}

	public PaymentArrangementPage verifyPaymentArrangementHeader() {
		try {
			checkPageIsReady();
			paymentArrangementHeader.isDisplayed();
			getDriver().getTitle().contains("billing/paymentarrangement.html");
		} catch (Exception e) {
			Assert.fail("Payment arrangement page not found");
		}
		return this;
	}

	public void verifyStoredCardNumber(String cardNumber) {
		try {
			String storedPaymentNumber = getStoredCardNumber();
			storedPaymentNumber = storedPaymentNumber.replace("*", "");
			String storedCardNo = storedPaymentNumber.substring(0, 3);
			Assert.assertTrue(cardNumber.contains(storedCardNo));
			Reporter.log("Stored card number is displayed");
		} catch (Exception e) {
			Assert.fail("Stored card number is not displayed correctly.");
		}
	}

	public void verifySavedBankAccount(String accNo) {
		try {
			paymentMethod.isDisplayed();
			Assert.assertTrue(savedPaymentMethod.getText().contains(accNo.substring(accNo.length() - 4)),
					"Saved payment method not found");
		} catch (Exception e) {
			Assert.fail("Saved payment method on OTP is not displayed on PA payment method blade");
		}
	}

	public String getLastFourDigitsOfSavedPaymentMethodFromPaymentBlade() {
		String lastFourDigits = null;
		try {
			savedPaymentMethod.isDisplayed();
			lastFourDigits = savedPaymentMethod.getText().trim().substring(4, 7);
		} catch (Exception e) {
			Assert.fail("Saved payment method on OTP is not displayed on PA payment method blade");
		}
		return lastFourDigits;
	}

	public void verifyInstallementDate(String installmentDate) {
		boolean isDisplayed = false;
		try {
			SimpleDateFormat outFormat = new SimpleDateFormat("MMM d, yyyy");
			SimpleDateFormat inFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date dateString = inFormat.parse(installmentDate);
			String mdy = outFormat.format(dateString);
			for (WebElement date : installmentdates) {
				if (date.getText().equals(mdy)) {
					isDisplayed = true;
					break;
				}
			}
			Reporter.log("Verified Installment date on PA page");
			Assert.assertTrue(isDisplayed, "Installment date mismatched on PA page");
		} catch (Exception e) {
			Assert.fail("Installment date mismatched on PA page");
		}
	}

	public void verifyInstallementAmount(String installmentAmount) {
		boolean isDisplayed = false;
		try {
			for (WebElement amount : installmentAmounts) {
				if (amount.getText().equals(installmentAmount.trim())) {
					isDisplayed = true;
					break;
				}
			}
			Reporter.log("Verified Installment amount on PA page");
			Assert.assertTrue(isDisplayed, "Installment amount mismatched on PA page");
		} catch (Exception e) {
			Assert.fail("Installment amount mismatched on PA page");
		}
	}

	/**
	 * this method is to check if the UserPA is scheduled or not. If PA is
	 * scheduled, then user wont be able to edit/modify/navigate to Edit PA page, He
	 * can only modify payment method type.
	 * 
	 * @return
	 */
	public boolean isPAScheduled() {
		try {
			for (WebElement status : installmentsStatus) {
				if (status.isDisplayed() && "Scheduled".equals(status.getText())) {
					Reporter.log(
							"PA is scheduled and cannot proceed with further steps, as Edit/modify PA is not allowed if it is once scheduled");
					return true;
				}
			}
		} catch (Exception e) {
			Reporter.log("PA is not scheduled");
		}
		return false;
	}

	/**
	 * this method is to check if the User's 1st installment is in Paid status or
	 * not.
	 * 
	 * @return
	 */
	public boolean isFirstInstallmentPaidOrNot() {
		try {
			paidInstallmentStatus.isDisplayed();
			Assert.assertTrue(paidInstallmentStatus.getText().trim().equals("Paid"),
					"First installment is not in Paid status");
			Reporter.log("First installment is in Paid status");
		} catch (Exception e) {
			Reporter.log("Status for first installment is not displayed");
		}
		return false;
	}

	/**
	 * This method is to verify PII masking is done for this page
	 * 
	 * @param custPaymentPID
	 */
	public void verifyPiiMasking(String custPaymentPID) {
		try {
			String pidAttr = storedPaymentNo.getAttribute("pid");
			if (!pidAttr.isEmpty() && pidAttr.contains(custPaymentPID) && pidAttr.matches(custPaymentPID + "\\d+")) {
				Reporter.log("Verified PII masking for Payment method blade");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Payment method blade");
		}
	}

	/**
	 * Verify Payment Arrangement Text
	 * 
	 * @return boolean
	 */
	public Boolean verifyPaymentMethodIsProvided() {
		try {
			if (savedPaymentMethodOnPaymentBlade.getText().contains("None Provided"))
				return true;
			else
				return false;
		} catch (Exception e) {
			Reporter.log("Payment blade not found");
			return false;
		}
	}

	public void verifySavedPaymentonPaymentBlade() {
		try {
			checkPageIsReady();
			Assert.assertTrue(savedcardonPaymentBlade.getText().matches("****.*\\d+"), "saved payment not matched");

		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Payment method blade");
		}
	}

	public void verifypaymentwarningesclamation() {
		try {
			if (paymentwarningesclamation.isDisplayed())
				Reporter.log("payment warning esclamation is displayed");
			else
				Verify.fail("payment warning esclamation is not displayed");

		} catch (Exception e) {

			Verify.fail("payment warning esclamation is not existing");
		}
	}

	public void verifyaddpaymentmethodwarning() {
		try {
			if (addpaymentmethodwarning.isDisplayed())
				Reporter.log("add payment method warning is displayed");
			else
				Verify.fail("add payment method warning is not displayed");

		} catch (Exception e) {

			Verify.fail("add payment method warning is not displayed");
		}
	}

	public PaymentArrangementPage verifyPaymentMethodChevronNotDispalyed() {
		try {

			Assert.assertFalse(paymentMethodChevron.isDisplayed(), "chevron dispalyed-Not expected");
			Reporter.log("chevron not dispalyed");
		} catch (Exception e) {
			Assert.fail("chevron dispalyed-Not expected");
		}
		return this;
	}

	public PaymentArrangementPage verifyMsgForPaymentMethodInsideBlackoutPeriod() {
		try {

			verifyElementBytext(msgForPaymentMethodInsideBlackout,
					"It's too late to add a payment method for your installment scheduled for . Please make a One-Time Payment for $.  to avoid suspension and collections.");

			Reporter.log("msg For PaymentMethod InsideBlackout dispalyed");
		} catch (Exception e) {
			Assert.fail("msg For PaymentMethod InsideBlackout-Not dispalyed");
		}
		return this;
	}

	public PaymentArrangementPage verifyInstallementStatus(String status) {
		try {

			Assert.assertTrue(installementStatus.get(0).getText().contains(status), "Installement status-Not expected");
			Reporter.log("Verified Installement status: "+status);
		} catch (Exception e) {
			Assert.fail("installement status-Not expected");
		}
		return this;
	}

	public PaymentArrangementPage verifyMsgForSecuredInsideBlackoutPeriod() {
		try {

			Assert.assertTrue(msgForSecuredInsideBlackout.getText().contains(
					"Updates will take effect on .Any installements Due before this will be processed as scheduled using previous payment method"),
					"msg For secured InsideBlackout-Not dispalyed");
			Reporter.log("msg For secured InsideBlackout dispalyed");
		} catch (Exception e) {
			Assert.fail("msg For secured InsideBlackout-Not dispalyed");
		}
		return this;
	}

	/**
	 * get Bill due date
	 * 
	 * @return @
	 */
	public String getBilldueDate() {
		checkPageIsReady();
		String[] dueDateText = null;
		Date temp = null;
		try {

			dueDateText = showDueDate.getText().split("on");
			temp = inputFormat.parse(dueDateText[1]);
			dueDateText[1] = standardFormat.format(temp);
			System.out.println("getBilldueDate" + dueDateText[1]);
			Reporter.log("Bill Due date displayed");
		} catch (Exception e) {
			Assert.fail("Bill due date component not found");
		}
		return dueDateText[1];
	}

	/**
	 * get installement One
	 * 
	 * @return
	 */
	public String getInstallementOneDate() {
		String installementOne = null;
		Date temp = null;
		try {

			installementOne = instDateOne.getText();
			temp = inputFormat.parse(installementOne);
			installementOne = standardFormat.format(temp);
			System.out.println("installementOne" + installementOne);
			Reporter.log("Bill Due date displayed");
		} catch (Exception e) {
			Assert.fail("Bill due date component not found");
		}
		return installementOne;
	}

	/**
	 * get installement two
	 * 
	 * @return
	 */
	public String getInstallementTwoDate() {
		String installementTwo = null;
		Date temp = null;
		try {

			installementTwo = instDateTwo.getText();
			temp = inputFormat.parse(installementTwo);
			installementTwo = standardFormat.format(temp);
			System.out.println("installementOne" + installementTwo);
			Reporter.log("Bill Due date displayed");
		} catch (Exception e) {
			Assert.fail("Bill due date component not found");
		}
		return installementTwo;
	}

	/**
	 * verify installement dates are with in due date limit
	 * 
	 * @return
	 * 
	 * 
	 */
	public PaymentArrangementPage verifyInstallementsdatesWithinDueDateLimit(String Duedate, String installementOne,
			String installementTwo) {

		// Date temp = null;
		Date billDueDate = null;
		Date InstallementOnedate = null;
		Date InstallementTwodate = null;

		// SimpleDateFormat standardFormat = new SimpleDateFormat("yyyy-MM-DD");

		// SimpleDateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy");
		try {
			// temp = inputFormat.parse(Duedate);
			// billdueDate = standardFormat.format(temp);
			billDueDate = standardFormat.parse(Duedate);
			System.out.println("billDueDate" + billDueDate);
			InstallementOnedate = standardFormat.parse(installementOne);
			InstallementTwodate = standardFormat.parse(installementTwo);
			verifyInstallementOneWithInDueDateLimit(billDueDate, InstallementOnedate);
			verifyInstallementOneWithInDueDateLimit(billDueDate, InstallementTwodate);
			InstallementOnedate = standardFormat.parse(installementOne);
			InstallementTwodate = standardFormat.parse(installementTwo);
			verifyInstallementOneWithInDueDateLimit(billDueDate, InstallementOnedate);
			verifyInstallementOneWithInDueDateLimit(billDueDate, InstallementTwodate);
		} catch (ParseException e) {
			Assert.fail("Installement dates are not with in due date limit");
		}

		return this;

	}

	/**
	 * verify installement one with in due date limit
	 * 
	 * @return
	 */
	public PaymentArrangementPage verifyInstallementOneWithInDueDateLimit(Date billDueDate, Date InstallementOnedate) {

		try {
			long diffInMillies = Math.abs(billDueDate.getTime() - InstallementOnedate.getTime());
			Assert.assertTrue(TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) > 0,
					"Installement one date is not in due date limit");
			Reporter.log("Installement one date is in due date limit");
		} catch (Exception e) {
			Assert.fail("Installement one date is not in due date limit");
		}
		return this;
	}

	/**
	 * verify installement one with in due date limit
	 * 
	 * @return
	 */
	public PaymentArrangementPage verifyInstallementTwoWithInDueDateLimit(Date billDueDate, Date InstallementTwodate) {

		try {
			long diffInMillies = Math.abs(billDueDate.getTime() - InstallementTwodate.getTime());
			Assert.assertTrue(TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) > 0,
					"Installement two date is not in due date limit");
			Reporter.log("Installement two date is in due date limit");
		} catch (Exception e) {
			Assert.fail("Installement two date is not in due date limit");
		}
		return this;
	}

	public PaymentArrangementPage verifyPaymentArrangementDisAllowedforNonMasterUser() {
		try {

			checkPageIsReady();

			Assert.assertTrue(paDisallowedModal.get(0).getText().contains("Oops!"),
					"autopay Disallowed Modal not dispalyed");
			Assert.assertTrue(
					paDisallowedModal.get(1).getText()
							.contains("Please contact the Primary Account Holder to set up a Payment Arrangement"),
					"PA Disallowed Modal  not dispalyed");
			Reporter.log("PA Disallowed Modal  is dispalyed");

		} catch (Exception e) {
			Reporter.log("PA Disallowed Modal  not dispalyed");
		}

		return this;
	}

	/**
	 * verify TnC link and text are displayed
	 */
	public void verifyTnCLink() {
		try {
			Assert.assertTrue(termsconditionsLink.isDisplayed(), "T&C link is not found");
			Assert.assertTrue(termsconditionsText.getText().contains("By submitting, you agree to the "));
			Reporter.log("Verified T&C link and text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T&C link and text");
		}
	}

	/**
	 * verify FAQ link is displayed
	 */
	public void verifySeeFAQSLink() {
		try {
			Assert.assertTrue(faqsLink.isDisplayed(), "FAQ link is not found");
			Reporter.log("Verified FAQ link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify FAQ link");
		}
	}

	/**
	 * Click on Remove Existing Payment method radio button
	 */
	public PaymentArrangementPage clickOnRemoveExistingPaymentMethod() {
		try {
			radioButtonRemoveExistingPaymentMethod.click();
			Reporter.log("Clicked on Remove Existing Payment Method radio button");
		} catch (Exception e) {
			Assert.fail("Not able to click on Remove Existing Payment Method radio button");
		}
		return this;
	}

	/**
	 * verify Number of Installments displayed based on the amount due.
	 * 
	 * @param totalBal
	 */
	public void verifyNoOfInstallmentsDisplayed(Double totalBal) {
		try {
			if (Double.compare(totalBal, 2.01) == 0) {
				Assert.assertEquals(noOfInstallments.size(), 2);
			} else if (Double.compare(totalBal, 2.01) < 0) {
				if (Double.compare(totalBal, 1.01) < 0) {
					Assert.assertFalse(installmentBlade.isEnabled(), "Installment blade is enabled");
					Assert.assertFalse(paymentMethod.isEnabled(), "Payment method blade is enabled");
					Assert.assertFalse(agreeAndSubmitbtn.isEnabled(), "Agree and Submit button is enabled");
				} else {
					Assert.assertEquals(noOfInstallments.size(), 1);
				}
			} else {
				Assert.assertEquals(noOfInstallments.size(), 2);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify the number of installments displayed");
		}

	}

	/**
	 * get total balance amount
	 * 
	 * @return totalBal
	 */
	public Double getTotalBal() {
		Double totalBal = null;
		try {
			totalBal = Double.parseDouble(totalBalAmount.getText().replace("$", ""));
			Reporter.log("Total balance to be paid for PA: " + totalBalAmount.getText());
		} catch (Exception e) {
			Assert.fail("Unable to retrieve Total Balance amount");
		}
		return totalBal;
	}

	/**
	 * verify suspended lines alert on PA page
	 * 
	 * @param alertMsgToVerify
	 */
	public void verifySuspendedLinesAlert(String alertMsgToVerify) {
		try {
			alertText.getText().equals("");
			Reporter.log("Verified alert message on PA landing page");
		} catch (Exception e) {
			Assert.fail("Failed to verify suspended lines alert");
		}
	}

	/**
	 * Verify suspended alert in spoke/payment collection page
	 * 
	 * @param spokeAlertMSgToVerify
	 */
	public PaymentArrangementPage verifyReSuspendedAlertSpoke(String spokeAlertMSgToVerify) {
		try {
			modifySuspendedAlert.getText().equals("");
			Reporter.log("Resuspended alert in spoke page is displeyed");
		} catch (Exception e) {
			Assert.fail("Failed to display alert in paymentcollection page");
		}
		return this;
	}

	/**
	 * Verify suspended alert in confirmation page when user modify's to unsecure PA
	 * with suspended account
	 * 
	 * @param spokeAlertMSgToVerify
	 */
	public PaymentArrangementPage verifyReSuspendedAlertConfirmation(String AlertMSgToVerifyConfirmation) {
		try {
			modifySuspendedMesasageConfirmation.getText().equals("");
			Reporter.log("Resuspended alert in spoke page is displeyed");
		} catch (Exception e) {
			Assert.fail("Failed to display alert in paymentcollection page");
		}
		return this;
	}

	/**
	 * Verify suspended alert in landing when user modify's to unsecure PA with
	 * suspended account
	 * 
	 * @param spokeAlertMSgToVerify
	 */
	public PaymentArrangementPage verifyReSuspendedAlertLanding(String AlertMSgToVerifyLanding) {
		try {
			modifySuspendedMesasageLanding.getText().equals("");
			Reporter.log("Resuspended alert in spoke page is displeyed");
		} catch (Exception e) {
			Assert.fail("Failed to display alert in paymentcollection page");
		}
		return this;
	}

	/**
	 * Verify Payment Schedule Header
	 * 
	 * @param paymentSchedule
	 */
	public void verifyPaymentScheduleHeader(String paymentSchedule) {
		try {
			paymentScheduleHeader.isDisplayed();
			Assert.assertTrue(paymentScheduleHeader.getText().equals(paymentSchedule),
					"Payment schedule header mismatch");
			Reporter.log("Payment Schedule header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Payment Schedule header");
		}

	}

	/**
	 * 
	 * @param installmentNumber
	 */
	public void verifyInstallmentNumber(String installmentNumber) {
		try {
			installmentNumberOne.isDisplayed();
			Assert.assertTrue(installmentNumberOne.getText().equals(installmentNumber));
			Reporter.log("Installment Number One is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Installment Number One is displayed");
		}
	}

	/**
	 * 
	 * @param verify review payment arrangement CTA
	 */
	public void verifyReviewPaymentArrangementCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(reviewPaymentArrangementCTA));
			Assert.assertTrue(reviewPaymentArrangementCTA.isDisplayed());
			Reporter.log("reviewPaymentArrangementCTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display reviewPaymentArrangementCTA");
		}
	}

	/**
	 * 
	 * @param verify review payment arrangement CTA
	 */
	public void clickReviewPaymentArrangementCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(reviewPaymentArrangementCTA));
			reviewPaymentArrangementCTA.click();
			Reporter.log("clicked review payment CTA successfully");
		} catch (Exception e) {
			Assert.fail("Failed to click reviewPaymentArrangementCTA");
		}
	}

	/**
	 * 
	 * @param verify review payment arrangement CTA
	 */
	public void verifyCancelYesOrNoPopUp() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cancelYesOrNoPopUp));
			Assert.assertTrue(cancelYesOrNoPopUp.isDisplayed());
			Reporter.log("cancel yes/no pop up is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to validate cancel yes/no pop up ");
		}
	}

	/**
	 * verify PA is secured or not
	 * 
	 * @return
	 */
	public boolean isPASecuredOrNot() {
		boolean isSecured = false;
		try {
			isSecured = savedPaymentMethodOnPaymentBlade.isDisplayed();
			Reporter.log("verified the payment method blade.");
		} catch (Exception e) {
			Assert.fail("Failed to verify if the payment method is added or not");
		}
		return isSecured;
	}

	/**
	 * verify total bal is defaulted to pay
	 */
	public void verifyTotalBalisDefaulted() {
		try {
			Double totalBal = getTotalBal();
			String subHeader = subheaderText.getText();
			Double totalFromHeader = Double
					.parseDouble(subHeader.substring(subHeader.indexOf("$"), subHeader.indexOf(" ")).replace("$", ""));
			Assert.assertEquals(Double.compare(totalBal, totalFromHeader), 0);
		} catch (Exception e) {
			Assert.fail("Failed to verify that the Total Balance is defaulted to pay");
		}
	}

	/**
	 * verify that the installments dates are defaulted to max range for total bal
	 */
	public void verifyInstallmentDatesAreDefaultedToMaxTotalBal() {
		try {
			Date date1 = inputFormat.parse(installmentdates.get(0).getText());
			Date date2 = inputFormat.parse(installmentdates.get(1).getText());
			Assert.assertEquals(getDateDiff(date1, date2, TimeUnit.DAYS), 14);
			Reporter.log("Verified that the isntallment dates are defaulted to max range");
		} catch (Exception e) {
			Assert.fail("Failed to verify that the installment dates are defaulted to max range");
		}
	}

	/**
	 * Get a diff between two dates
	 * 
	 * @param date1    the oldest date
	 * @param date2    the newest date
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	/**
	 * verify the Installment status by checking the installment date
	 */
	public void verifyInstallementStatusByInstDate() {
		try {
			for (int i = 0; i < installmentdates.size(); i++) {
				Date insDate = inputFormat.parse(installmentdates.get(i).getText());
				String instStatus = installementStatus.get(i).getText();
				Date currentDate = new Date();
				if (insDate.compareTo(currentDate) == 0) {
					Assert.assertTrue(instStatus.equals("Processing"),
							"Installment status is not as expected(Processing). Actual :" + instStatus);
				} else if (insDate.compareTo(currentDate) > 0) {
					Assert.assertTrue(instStatus.equals("Scheduled"),
							"Installment status is not as expected(Scheduled). Actual :" + instStatus);
				} else {
					boolean status = instStatus.equals("Missed") || instStatus.equals("Paid");
					if (instStatus.equals("Missed")) {
						Assert.assertTrue(exclamationCircle.isDisplayed(),
								"Exclamation circle is not displayed for Missed installment status");
					} else {
						Assert.assertTrue(paidLogo.isDisplayed(),
								"Exclamation circle is not displayed for Missed installment status");
					}
					Assert.assertTrue(status,
							"Installment status is not as expected(Missed|Paid). Actual :" + instStatus);
				}
				Reporter.log("Verified the Installment status is :" + instStatus + " for Installment Date: " + insDate);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify the installment status");
		}
	}

	/**
	 * Verify Edit link is not displayed on payment blade
	 */
	public void verifyEditLinkNotDisplayedOnPaymentBlade() {
		try {
			Assert.assertFalse(editLink.isDisplayed());
			Assert.fail("Failed to verify Edit link on Payment method blade");
		} catch (Exception e) {
			Reporter.log("Verified that the Edit link is not displayed");
		}
	}

	/**
	 * Verify TnC link is not displayed
	 */
	public void verifyTnCLinkNotDisplayed() {
		try {
			Assert.assertFalse(termsconditionsLink.isDisplayed());
			Assert.fail("Failed to verify T&C link");
		} catch (Exception e) {
			Reporter.log("Verified that the T&C link is not displayed");
		}
	}

	/**
	 * Verify make a payment CTA for Unsecure PA 1 installment in blackout
	 */
	public void verifyMakeAPaymentCTA() {
		try {
			agreeAndSubmitbtn.isDisplayed();
			Assert.assertTrue(agreeAndSubmitbtn.getText().contains("Make a payment for "),"Content mismatch on Make a payment CTA. Actual: "+agreeAndSubmitbtn.getText());
			Reporter.log("Verified Make a payment CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Make a payment' CTA");
		}
	}

	/**
	 * verify alerts for Missed PA
	 * @param missedPAalert
	 */
	public void verifyAlertForMissedPayment(String missedPAalert) {
		boolean isDisplayed = false;
		try {
			for (WebElement alert : alertMsgs) {
				if (alert.isDisplayed() && alert.getText().contains(missedPAalert)) {
					isDisplayed = true;	
					Reporter.log("Verified Missed PA alert: "+ alert.getText());
					break;
				}
			}
			if (!isDisplayed) {
				Reporter.log("Failed to Verify Missed PA alert. Expected "+ missedPAalert);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify alerts for Missed payment");
		}
	}

	/**
	 * click on Make a payment CTA
	 */
	public void clickMakeaPaymentCTA() {
		try {
			agreeAndSubmitbtn.click();
			Reporter.log("Clicked on Make a payment CTA");
		} catch (Exception e) {
			Assert.fail("Failed to click on 'Make a payment' CTA");
		}
	}

	public String getAmountForMissedPA() {
		String missedPAAmount = null;
		try {
			missedPAAmount = agreeAndSubmitbtn.getText().trim();
			missedPAAmount = missedPAAmount.substring(missedPAAmount.indexOf('$'));
			Reporter.log("Amount ot be paid for Missed PA");
		} catch (Exception e) {
			Assert.fail("Failed to get the amount of Missed PA installment");
		}		
		return missedPAAmount;
	}
}