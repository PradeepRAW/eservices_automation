package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class AccessoriesHubPage extends TmngCommonPage {
	
	private final String pageUrl = "/hub/accessories-hub";
	
	public AccessoriesHubPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the AccessariesPage class instance.
	 */

	public AccessoriesHubPage verifyAccessoriesHubPageUrl() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(pageUrl),60);
		} catch (Exception e) {
			Assert.fail("Accessories Hub URL is not displayed");
		}
		return this;
	}

}
