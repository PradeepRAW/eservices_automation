package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class DeviceTradeInApi extends ApiCommonLib{
	
	/***
	 * The term ‘Loan’ used as micro-service name and its operations reflect the point-of-view of a T-Mobile customer.
	 * The term ‘loan’ used in the context of OFSLL or the EIP system represents the indvidual items being financed.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
	//Gets a list of devices that can be traded in. Gets an array of carriers, array of makes for each carrier and an array of model
	//for each make.
    public Response getTradeInDevices(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/devices", RestCallType.GET);
 			Response response = service.callService("v2/tradein/devices", RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    //Checks if a device is eligible for tradein
    public Response checkDeviceEligibleForTradeIn(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/eligibility", RestCallType.POST);
 			Response response = service.callService("v2/tradein/eligibility", RestCallType.POST);
         System.out.println(response.asString());
         return response;
    }
    
    //Gets questions regarding the condition of the device based on carrier/make/model. This service gets the questions 
    //and answer options that we need to check regarding the physical condition of the device.
    public Response getQuestionAndAnswersOfDevice(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);
    	Map<String, String> queryParams = buildGetQuestionAndAnswersOfDeviceQueryparams(apiTestData);
    	RestService service = new RestService(requestBody, eos_base_url);
    	System.out.println(service);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			reqSpec.addQueryParams(queryParams);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/questions", RestCallType.GET);
 			Response response = service.callService("v2/tradein/questions", RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    //Creates a quote based on device information
    public Response createQuoteTradeIn(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v2/tradein/", RestCallType.POST);
         System.out.println(response.asString());
         return response;
    }
    
    
    //Submit Quote
    public Response submitQuoteTradeIn(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v2/tradein/", RestCallType.PUT);
         System.out.println(response.asString());
         return response;
    }
    
  //Get a trade in request
    public Response getTradeInBasedOnQuotedId(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/"+getToken("quoteId"), RestCallType.GET);
 			Response response = service.callService("v2/tradein/"+getToken("quoteId"), RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    
    //Get a trade in request
    public Response getShippingLabelBasedOnRMANumber(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);
    	Map<String, String> queryParams = buildGetShippingLabelBasedOnRMANumberQueryparams(apiTestData);
    	
    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
         	reqSpec.addQueryParams(queryParams);
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/"+getToken("rmaNumber")+"/shippinglabel", RestCallType.GET);
 			Response response = service.callService("v2/tradein/"+getToken("rmaNumber")+"/shippinglabel", RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    //Gets lookup data for tradein flow
    public Response getLookUpDataForTradeInFlow(ApiTestData apiTestData, String codeType) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);

    	RestService service = new RestService("", eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/codes?codeType="+codeType, RestCallType.GET);
 			Response response = service.callService("v2/tradein/codes?codeType="+codeType, RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    //list of devices on the account that are eligible for trade-in
    //This service gets the list of devices associated with each line on the account that are eligible for 
    //a trade-in during a qualifying transaction.
    public Response getAccountDeviceListEligibleForTradeIn(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v2/tradein/accountdevices", RestCallType.POST);
 		//Response response = service.callService("v2/tradein/devices", RestCallType.GET);
         System.out.println(response.asString());
         return response;	
    }
    
        
    private Map<String, String> buildDeviceTradeInAPIHeader(ApiTestData apiTestData) throws Exception {
    	//clearCounters(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("Authorization",checkAndGetAccessToken());
		headers.put("correlationId","sameguid");
		headers.put("applicationId","MyTMO");
		headers.put("channelId","Web");
		headers.put("clientId","e-servicesUI");
		headers.put("transactionBusinessKey",apiTestData.getMsisdn());
		headers.put("transactionBusinessKeyType",apiTestData.getMsisdn());
		headers.put("transactionType","Tradein");
		headers.put("Cache-Control","no-cache");
		headers.put("usn","usn");
		headers.put("phoneNumber",apiTestData.getMsisdn());
		//headers.put("X-B3-TraceId", "ShopAPITest123");
		//headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","post-devices-search-MSISDN-200");
		return headers;
	}
    
    private Map<String, String> buildGetQuestionAndAnswersOfDeviceQueryparams(ApiTestData apiTestData) throws Exception {
    	Map<String, String> headers = new HashMap<String, String>();
		headers.put("carrierName","T-Mobile");
		headers.put("makeName","Apple");
		headers.put("modelName","iPhone%205%2032GB%20black%20-%20T-Mo%20-%20ME488LL/A");
		return headers;
	}

    private Map<String, String> buildGetShippingLabelBasedOnRMANumberQueryparams(ApiTestData apiTestData) throws Exception {
    	Map<String, String> headers = new HashMap<String, String>();
		headers.put("banNumber",apiTestData.getBan());
		headers.put("language","en");
		return headers;
	}
}
