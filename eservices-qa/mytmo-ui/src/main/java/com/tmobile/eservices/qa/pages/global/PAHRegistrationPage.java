package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class PAHRegistrationPage extends CommonPage {

	private static final String pageUrl = "claimPAH";

	@FindBy(css = "p[class='Display6']")
	private List<WebElement> profilePageLinks;

	@FindBy(css = "input[value='Skip']")
	private WebElement skipBtn;

	@FindBy(css = "input[value='ASK ME LATER']")
	private WebElement askMeLaterBtn;

	@FindBy(css = ".modal-footer .btn-primary")
	private WebElement modalWindowOkBtn;

	@FindBy(css = "span.ui_subhead")
	private WebElement primaryAccountHolderMissingHeader;

	@FindBy(css = "#ssn_validation_id")
	private WebElement ssnTextBox;

	@FindBy(css = "[value='CONFIRM']")
	private WebElement confirmBtn;

	public PAHRegistrationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify PAH Registration page.
	 *
	 * @return the PAHRegistrationPage class instance.
	 */
	public PAHRegistrationPage verifyPAHRegistrationPage() {
		try {
			verifyPageUrl(pageUrl);
			Reporter.log("PAHRegistration page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("PAHRegistration page not displayed");
		}
		return this;
	}

	/**
	 * Click on skip button.
	 */
	public PAHRegistrationPage clickOnSkipBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(skipBtn));
			skipBtn.click();
			Reporter.log("Modal window popup is displayed");
		} catch (Exception e) {
			Assert.fail("Click failed on skip button ");
		}
		return this;
	}

	/**
	 * Click on modal window ok button.
	 */
	public PAHRegistrationPage clickOnModalWindowOkBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalWindowOkBtn));
			modalWindowOkBtn.click();
		} catch (Exception e) {
			Assert.fail("Click failed on modal window ok button ");
		}
		return this;
	}

	/**
	 * Click on ask me later button.
	 */
	public PAHRegistrationPage clickOnAskMeLaterBtn() {
		try {
			askMeLaterBtn.click();
		} catch (Exception e) {
			Assert.fail("Click failed on ask me later button ");
		}
		return this;
	}

	/**
	 * Verify primary account holder missing header
	 */
	public PAHRegistrationPage verifyPrimaryAccountHolderMissingHeader() {
		try {
			waitforSpinner();
			checkPageIsReady();
			primaryAccountHolderMissingHeader.isDisplayed();
			Reporter.log("Primary account holder missing header displayed");
		} catch (Exception e) {
			Reporter.log("Primary account holder missing header not displayed");
			Assert.fail("Primary account holder missing header not displayed ");
		}
		return this;
	}

	/**
	 * Verify ssn text box
	 */
	public PAHRegistrationPage verifySSNTextBox() {
		try {
			waitforSpinner();
			checkPageIsReady();
			ssnTextBox.isDisplayed();
			Reporter.log("SSN text box displayed");
		} catch (Exception e) {
			Reporter.log("SSN text box not displayed");
			Assert.fail("SSN text box not displayed ");
		}
		return this;
	}

	/**
	 * Verify confirm button
	 */
	public PAHRegistrationPage verifyConfirmBtn() {
		try {
			waitforSpinner();
			checkPageIsReady();
			confirmBtn.isDisplayed();
			Reporter.log("Confirm button displayed");
		} catch (Exception e) {
			Reporter.log("Confirm button not displayed");
			Assert.fail("Confirm button not displayed ");
		}
		return this;
	}

	/**
	 * Verify ask me later button
	 */
	public PAHRegistrationPage verifyAskMeLaterBtn() {
		try {
			waitforSpinner();
			checkPageIsReady();
			askMeLaterBtn.isDisplayed();
			Reporter.log("Ask me later button displayed");
		} catch (Exception e) {
			Reporter.log("Ask me later button not displayed");
			Assert.fail("Ask me later button not displayed ");
		}
		return this;
	}

}
