package com.tmobile.eservices.qa.pages.global.api;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSProfile extends EOSCommonLib {






public Response getProfileresponse(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("access_token", alist[1]);
	    restService.addHeader("X-B3-TraceId", "abc");
	    restService.addHeader("X-B3-SpanId", "abc");
	    restService.addHeader("channel_id", "DESKTOP");
	    restService.addHeader("application_id", "MYTMO");
	    restService.addHeader("msisdn", alist[0]);
	    restService.addHeader("ban", alist[3]);
	    
	    response = restService.callService("v1/profile/",RestCallType.GET);
	    
	}

	    return response;
}



public String getbillingState(Response response) {
	String billstate=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("billingAddress.state")!=null) {
        	return jsonPathEvaluator.get("billingAddress.state").toString();
	}}
		
	
	return billstate;
}




}
