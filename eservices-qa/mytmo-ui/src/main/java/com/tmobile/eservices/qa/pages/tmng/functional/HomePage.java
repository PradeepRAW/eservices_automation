package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

import io.appium.java_client.AppiumDriver;

public class HomePage extends TmngCommonPage {

	private final String pageUrl = "t-mobile.com";

	private final String myTMOpageUrl = "my.t-mobile.com";

	@FindBy(css = ".nav-right .nav-links a")
	private List<WebElement> headerLinks;

	@FindBy(css = "a.block[href*='accessories']")
	private WebElement accessoriesLink;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile;

	@FindBy(css = "a[href*='www.t-mobile.com/resources/bring-your-own-phone']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = ".main-nav a[href*='//www.t-mobile.com/offers']")
	private WebElement deals1;

	@FindBy(css = "a[href*='resources/how-to-join-us']")
	private WebElement wellHelpYouJoin;

	@FindBy(css = "a[href*='www.t-mobile.com/coverage/international-calling']")
	private WebElement internationalCalling;

	@FindBy(css = "a[href*='www.t-mobile.com/offers/t-mobile-digits']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "button.ntm-navbar__hamburger")
	private WebElement menuMenu;

	@FindBy(css = "a[href*='www.t-mobile.com/coverage/4g-lte-network']")
	private WebElement checkOutTheCoverage;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q218NETFLI_FSPA56M21F13432']")
	private WebElement getTmobileOne;

	@FindBy(css = "a[href*='www.t-mobile.com/internet-of-things']")
	private WebElement smartDevices;

	@FindBy(css = "a[href*='switch-to-t-mobile']")
	private WebElement areYouWithUs;

	@FindBy(css = "a[data-sub-category='Traveling abroad']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[data-sub-category='Unlimited plan']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href*='www.t-mobile.com/hub/accessories-hub']")
	private WebElement accessoriesHub;

	@FindBy(css = "i[ng-class*='cartIcon']")
	private WebElement cartIcon;

	@FindAll({ @FindBy(id = "username"), @FindBy(name = "username"), @FindBy(css = "input.input_box") })
	private WebElement emailOrPhoneForMyTMO;

	@FindAll({ @FindBy(css = "input[autocomplete='new-password']"), @FindBy(id = "password"),
			@FindBy(name = "password") })
	private WebElement passwordForMyTMO;

	@FindAll({ @FindBy(css = ".btn.btn-primary.login-btn.pt50"), @FindBy(id = "primary_button_930"),
			@FindBy(css = ".btn.til-btn.ripple") })
	private WebElement loginButtonForMYTMO;

	@FindBy(css = "button.btn.btn-primary.continue-btn.ng-binding")
	private WebElement securityAnswersContinueButton;

	@FindBy(css = "input[value='security_question']")
	private WebElement securityQuestionsRadioBtn;

	@FindBy(css = "button[ng-click*='chooseSecondFactorMethodsController']")
	private WebElement continueButton;

	@FindBy(css = "input[id='que_0_field']")
	private WebElement securityAnswerOne;

	@FindBy(css = "input[id='que_1_field']")
	private WebElement securityAnswerTwo;

	@FindBy(xpath = "//button[contains(.,'Next')]")
	private WebElement nextBtnOnMYTMO;

	@FindBy(css = "li.navbar__item div.nav__link-container a[href*='cell-phones']")
	private List<WebElement> phonesLink;

	@FindBy(linkText = "Tablets & Devices")
	private WebElement tabletsDevicesLink;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/phone-trade-in']")
	private WebElement tradeInProgram;

	public HomePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * This method is to enter user name and password and click submit
	 * 
	 * @param username
	 * @param password
	 */
	public HomePage performMyTMOLoginAction(TMNGData tmngdata) {
		try {
			checkPageIsReady();
			sendTextData(emailOrPhoneForMyTMO, tmngdata.getLoginEmailOrPhone());
			try {
				if (isElementDisplayed(nextBtnOnMYTMO)) {
					waitFor(ExpectedConditions.elementToBeClickable(nextBtnOnMYTMO), 60);
					clickElement(nextBtnOnMYTMO);
				} else {
					Reporter.log("Old login page is displayed");
				}
			} catch (Exception e) {
				Reporter.log("Exception occurred during login action");
			}
			sendTextData(passwordForMyTMO, tmngdata.getPassword());
			clickElement(loginButtonForMYTMO);
			enterChooseSecondFactor();

			checkPageIsReady();
			waitforSpinner();
			if (getDriver().getCurrentUrl().contains("login/permissions.html")) {
				getDriver().get("https://my.t-mobile.com" + "/home.html");
			}

			checkPageIsReady();
			waitforSpinner();

			Assert.assertTrue(getDriver().getCurrentUrl().contains(myTMOpageUrl), "My TMO Login failed");

		} catch (Exception e) {
			Assert.fail("Unable to login with " + tmngdata.getPhoneNumber() + "/" + tmngdata.getPassword() + "");
		}
		return this;
	}

	/**
	 * Enter Choose Second Factor
	 */
	public void enterChooseSecondFactor() {
		checkPageIsReady();
		if (getDriver().getCurrentUrl().contains("chooseSecondFactor")) {
			securityQuestionsRadioBtn.click();
			continueButton.click();
			waitFor(ExpectedConditions.visibilityOf(securityAnswerOne), 5);
			securityAnswerOne.sendKeys("test");
			securityAnswerTwo.sendKeys("test");
			securityAnswersContinueButton.click();
		}
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage verifyPageLoaded() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			if (getDriver().getCurrentUrl().contains(pageUrl)) {
				verifyPageUrl();
				Reporter.log("Home page is displayed");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Home Page not loaded");
		}
		return this;
	}

	/*
	 * Verify that current page URL matches the expected URL.
	 */
	public HomePage verifyPageUrl() {
		try {
			waitFor(ExpectedConditions.urlContains("t-mobile"), 60);
		} catch (Exception e) {
			Assert.fail("Home Page URL is not displayed");
		}
		return this;
	}

	/**
	 * Open Cart page Url
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage openCartPageURl() {
		try {
			String[] url = getDriver().getCurrentUrl().split("com");
			String newUrl = url[0] + "com/cart";
			getDriver().get(newUrl);
		} catch (Exception e) {
			Assert.fail("Failed to navigate Cart page");
		}

		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickHeaderLinks(String linkName) {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			clickElementBytext(headerLinks, linkName);
		} catch (Exception e) {
			Assert.fail("Failed to click on header link: " + linkName);
		}

		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickCheckOutTheCoverageLink() {
		try {
			checkPageIsReady();
			checkOutTheCoverage.click();
		} catch (Exception e) {
			Assert.fail("check out Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickMenuMenuButton() {
		try {
			menuMenu.click();
			// clickElementWithJavaScript(menuMenu);
		} catch (Exception e) {
			Assert.fail("Main Menu Link is not available to click");
		}
		return this;
	}

	/**
	 * Verify Menu Menu Button.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage verifyHamburgerMenuButton() {
		try {
			checkPageIsReady();
			menuMenu.isDisplayed();
			Reporter.log("Menu hamburger icon is displayed");
		} catch (Exception e) {
			Assert.fail("Menu hamburger icon is not displayed");
		}
		return this;
	}

	/**
	 * Click on Menu Menu Button and verify Traveling Abroad link
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickHamburgerMenuAndverifyTravelingAbroadLink() {
		try {
			checkPageIsReady();
			clickMenuMenuButton();
			travelingAbroad.isDisplayed();
			Reporter.log("Traveling abroad link is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display Traveling abroad link");
		}
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickTravelingAbroadLink() {
		try {
			checkPageIsReady();
			travelingAbroad.click();
			Reporter.log("Successfully clicked on Travelling Abroad link.");
		} catch (Exception e) {
			Assert.fail("traveling AbroadLink is not available to click");
		}
		return this;
	}

	/**
	 * Click on Menu Menu Button and verify Traveling Abroad link
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickHamburgerMenuAndverifyWellHelpYouJoinLink() {
		try {
			checkPageIsReady();
			clickMenuMenuButton();
			wellHelpYouJoin.isDisplayed();
			Reporter.log("well Help You Join link is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to well Help You Join link");
		}
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickWellHelpYouJoinLink() {
		try {
			checkPageIsReady();
			wellHelpYouJoin.click();
			checkPageIsReady();
			Reporter.log("We’ll help you join page is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to click on well Help You Join link.");
		}
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 */
	public HomePage clickTradeInProgramLink() {
		checkPageIsReady();
		JavascriptExecutor je = (JavascriptExecutor) getDriver();
		je.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		tradeInProgram.isDisplayed();
		tradeInProgram.click();
		Reporter.log("Verified and Clicked on Trade in program link");
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickUseMultipleNumbersOnMyPhoneLink() {
		try {
			checkPageIsReady();
			useMultipleNumbersOnMyPhone.click();
			Reporter.log("Clicked on Use Multiple Numbers On My Phone page Link");

		} catch (Exception e) {
			Assert.fail("Use Multiple Numbers On My Phone Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Accessories Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickOnAccessoriesLink() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(accessoriesLink), 2);
			scrollToElement(accessoriesLink);
			accessoriesLink.click();
			Reporter.log("Clicked on Accessories page Link");

		} catch (Exception e) {
			Assert.fail("Accessories Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickBringYourOwnPhoneLink() {
		try {
			checkPageIsReady();
			bringYourOwnPhone.click();
			Reporter.log("Clicked on Clicked on Bring your own phone Link");

		} catch (Exception e) {
			Assert.fail("Bring your own phone Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickDeals1Link() {
		try {
			checkPageIsReady();
			deals1.click();
			Reporter.log("Clicked on Deals Link");
		} catch (Exception e) {
			Assert.fail("Clicked on Deals Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on International Calling Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickInternationalCallingLink() {
		try {
			checkPageIsReady();
			internationalCalling.click();
			Reporter.log("Clicked on International Calling Link");
		} catch (Exception e) {
			Assert.fail("international Calling Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Plans Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickPlansLink() {
		try {
			clickElementBytext(headerLinks, "Plans");
		} catch (Exception e) {
			Assert.fail("Plans Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickSmartDevicesLink() {
		try {
			checkPageIsReady();
			smartDevices.click();
		} catch (Exception e) {
			Assert.fail("smart Devices  Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Are You With Us Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickAreYouWithUs5Link() {
		try {
			checkPageIsReady();
			areYouWithUs.click();
		} catch (Exception e) {
			Assert.fail("Are You With Us Link is not available to click");
		}
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickUnlimitedPlanLink() {
		try {
			checkPageIsReady();
			unlimitedPlan.click();
		} catch (Exception e) {
			Assert.fail("unlimited Plan is not available to click");
		}
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickWatchesTabletsLink() {
		try {
			watchesTablets.click();
		} catch (Exception e) {
			Assert.fail("watches Tablets is not available to click");
		}
		return this;
	}

	public HomePage clickTabletsDevicesLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(tabletsDevicesLink), 60);
			tabletsDevicesLink.click();
		} catch (Exception e) {
			Assert.fail("watches Tablets is not available to click");
		}
		return this;
	}

	/**
	 * Click on Accessories Hub Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickAccessoriesHubLink() {
		try {
			checkPageIsReady();
			accessoriesHub.click();
		} catch (Exception e) {
			Assert.fail("Failed to click on Accessories Hub link");
		}
		return this;
	}

	/**
	 * Click Cart Icon
	 * 
	 * @return
	 */
	public HomePage clickCartIcon() {
		try {
			cartIcon.click();
		} catch (Exception e) {
			Assert.fail("Cart Icon is not displayed");
		}
		return this;
	}

	/**
	 * Click Cart Icon
	 * 
	 * @return
	 */
	public HomePage clickPhonesLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickMenuMenuButton();
				clickElementWithJavaScript(phonesLink.get(0));
			} else {
				clickElementWithJavaScript(phonesLink.get(0));
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Phones Navigation Link");
		}
		return this;
	}

	/**
	 * Click on Menu Menu Button and verify International Calling link
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickHamburgerMenuAndverifyInternationalCallingLink() {
		try {
			checkPageIsReady();
			clickMenuMenuButton();
			internationalCalling.isDisplayed();
			Reporter.log("International Calling link is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display International Calling link");
		}
		return this;
	}

	/**
	 * Verify MyTMO Login Page
	 *
	 */
	public HomePage verifyMyTMOLoginPage() {
		try {

			// checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("/signin"), 60);
			Assert.assertTrue(getDriver().getCurrentUrl().contains("/signin"), "My TMO login not loaded");

			Reporter.log("User navigated to MyTMO login page");
		} catch (Exception e) {
			Assert.fail("Failed to verify MyTMO login Page");
		}
		return this;
	}
}
