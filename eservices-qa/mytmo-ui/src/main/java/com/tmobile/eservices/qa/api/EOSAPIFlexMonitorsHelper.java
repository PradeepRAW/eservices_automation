package com.tmobile.eservices.qa.api;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;

public class EOSAPIFlexMonitorsHelper extends ApiCommonLib {

	public Map<String, String> getResponseElementsMap(List<String> elements, Response response) throws Exception {

		Map<String, String> respElementsMap = new HashMap<String, String>();
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Iterator<String> itr = elements.iterator();
				while (itr.hasNext()) {
					String elementName = itr.next().toString();
					respElementsMap.put(elementName, getPathVal(jsonNode, elementName));
				}
			}
		}
		return respElementsMap;
	}

}
