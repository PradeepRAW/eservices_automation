package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class PaymentEstimatorPage extends CommonPage {

	@FindBy(css = "p.Display3")
	private WebElement pageHeader;

	@FindBy(id = "extraPaymentID")
	private WebElement paymentAmountInput;

	@FindBy(css = "button[class$='PrimaryCTA']")
	private WebElement estimateBtn;

	@FindBy(css = "button[class$='disabled']")
	private WebElement disableEstimateBtn;

	@FindBy(css = "button[class$='pr-1']")
	private WebElement payThisAmountBtn;

	@FindBy(css = "p.Display6")
	private WebElement eipInfoCustomerName;

	@FindBy(css = "div[class$='black']")
	private WebElement eipInfoDeviceName;

	@FindBy(css = "div.legal")
	private WebElement eipInfoMisdin;

	@FindBy(css = "div:first-of-type>span.pull-left")
	private WebElement eipInfoEipId;

	@FindBy(css = "div+div>span.pull-left")
	private WebElement eipInfoDeviceBalance;

	@FindBy(css = "div:first-of-type>span.pull-right")
	private WebElement eipInfoEipIdValue;

	@FindBy(css = "div+div>span.pull-right")
	private WebElement eipInfoDeviceBalanceValue;

	@FindBy(xpath = "//p[text()='Results']")
	private WebElement estimationBladeResults;

	@FindBy(xpath = "//p[text()='Current']")
	private WebElement estimationBladeCurrent;

	@FindBy(xpath = "//p[text()='Estimated']")
	private WebElement estimationBladeEstimated;

	@FindBy(xpath = "//p[text()='Plan balance']")
	private WebElement estimationBladePlanBalance;

	@FindBy(xpath = "//p[text()='Plan balance']/parent::div/following::div[contains(@class,'col-lg-2')]/p[contains(text(),'$')]")
	private WebElement estimationBladePlanBalanceAmount;

	@FindBy(xpath = "//p[text()='Remaining payments']")
	private WebElement estimationBladeRemainingPayments;

	@FindBy(xpath = "//p[text()='Remaining payments']/parent::div/following::div[contains(@class,'col-lg-2')]/p")
	private WebElement estimationBladeRemainingpaymentsNumber;

	@FindBy(xpath = "//p[text()='Plan balance']/parent::div/following::div[contains(@class,'col-lg-4')]/p[contains(text(),'$')]")
	private WebElement estimatimatedPlanBalance;

	@FindBy(xpath = "//p[text()='Remaining payments']/parent::div/following::div[contains(@class,'col-lg-4')]/p")
	private WebElement estimatimatedRemainingpaymentsNumber;

	private final String pageUrl = "";

	@FindBy(css = "i#crossIcon")
	private WebElement crossIcon;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public PaymentEstimatorPage(WebDriver webDriver) {

		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PaymentEstimatorPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PaymentEstimatorPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			waitFor(ExpectedConditions.visibilityOf(eipInfoCustomerName));
			Reporter.log("Payment estimator Page is loaded.");
		} catch (Exception e) {
			Assert.fail("payment estimator page is not loaded");
		}
		return this;
	}

	/**
	 * Verify Payment Estimator Header is displayed or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyPaymentEstimatorPageHeader() {
		try {
			pageHeader.isDisplayed();
			Assert.assertTrue(pageHeader.getText().contains("Payment estimator"), "Page header not found");
			Reporter.log("Verified payment estimator page Header is displayed");
		} catch (Exception e) {
			Assert.fail("payment estimator page Header is not displayed");
		}
		return this;
	}

	/**
	 * Verify estimate button is disable or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyEstimateBtnDisable() {
		try {
			Assert.assertTrue(!disableEstimateBtn.isEnabled(), "estimate button is not disabled state");

			Reporter.log("Verified  estimate button is disable state");
		} catch (Exception e) {
			Assert.fail("estimate button is not disable state");
		}
		return this;
	}

	/**
	 * click estimate button
	 * 
	 * @return
	 */

	public PaymentEstimatorPage clickEstimateBtn() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(estimateBtn));
			estimateBtn.click();
			Reporter.log("estimate button is clicked");
		} catch (Exception e) {
			Assert.fail("estimate button is not found");
		}
		return this;
	}

	/**
	 * Verify estimate button is displayed or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyEstimateBtn() {
		try {
			estimateBtn.isDisplayed();

			Reporter.log("Verified  estimate button is displayed");
		} catch (Exception e) {
			Assert.fail("estimate button is not displayed");
		}
		return this;
	}

	/**
	 * Verify Pay this amount button is displayed or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyPayThisAmountBtn() {
		try {
			payThisAmountBtn.isDisplayed();

			Reporter.log("Verified pay this amount button is displayed");
		} catch (Exception e) {
			Assert.fail("pay this amount button is not displayed");
		}
		return this;
	}

	/**
	 * Verify estimation amount field is displayed or not
	 * 
	 * @param Amount
	 * @return
	 */
	public PaymentEstimatorPage fillAmountEstimationAmountField(String Amount) {
		try {
			paymentAmountInput.sendKeys(Amount);
			Reporter.log("filled the amount in estimation amount field");
		} catch (Exception e) {
			Assert.fail("failed to filled the amount in estimation amount field");
		}
		return this;
	}

	/**
	 * Verify EIP info blade is displayed or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyEipInfoBlade() {
		try {

			eipInfoCustomerName.isDisplayed();
			eipInfoDeviceName.isDisplayed();
			eipInfoMisdin.isDisplayed();
			eipInfoEipId.isDisplayed();
			eipInfoDeviceBalance.isDisplayed();
			eipInfoEipIdValue.isDisplayed();
			eipInfoDeviceBalanceValue.isDisplayed();
			Reporter.log("Verified Eip Info blade  is displayed");
		} catch (Exception e) {
			Assert.fail("Verified Eip Info blade  is not displayed");
		}
		return this;
	}

	/**
	 * Verify payment amount estimation blade is displayed or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyPaymentAmountEstimationBlade() {
		try {

			estimationBladeResults.isDisplayed();
			estimationBladeCurrent.isDisplayed();
			estimationBladeEstimated.isDisplayed();
			estimationBladePlanBalance.isDisplayed();
			estimationBladePlanBalanceAmount.isDisplayed();
			estimationBladeRemainingPayments.isDisplayed();
			estimationBladeRemainingpaymentsNumber.isDisplayed();
			eipInfoDeviceBalanceValue.isDisplayed();
			Reporter.log("Verified payment amount estimation blade is displayed");
		} catch (Exception e) {
			Assert.fail("Verified payment amount estimation blade is not displayed");
		}
		return this;
	}

	/**
	 * Verify estimated plan balance is displayed or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyEstimatedPlanbalance() {
		try {

			estimatimatedPlanBalance.isDisplayed();
			Reporter.log("Verified estimated plan balance is displayed");
		} catch (Exception e) {
			Assert.fail("pay this estimated paln balance is not displayed");
		}
		return this;
	}

	/**
	 * Verify estimated remaining payments is displayed or not
	 * 
	 * @return
	 */

	public PaymentEstimatorPage verifyEstimatedRemainingPayments() {
		try {

			estimatimatedRemainingpaymentsNumber.isDisplayed();
			Reporter.log("Verified estimated remaining payments is displayed");
		} catch (Exception e) {
			Assert.fail("pay this estimated remaining payments is not displayed");
		}
		return this;
	}

	/**
	 * Verify all the elements in Payment estimator page
	 * 
	 * @return
	 */
	public PaymentEstimatorPage verifyPaymentEstimatorpage(String estimatorAmount) {
		try {
			verifyPaymentEstimatorPageHeader();
			verifyEipInfoBlade();
			verifyPaymentAmountEstimationBlade();
			verifyEstimateBtnDisable();
			fillAmountEstimationAmountField(estimatorAmount);
			verifyEstimateBtn();
			clickEstimateBtn();
			verifyEstimatedPlanbalance();
			verifyEstimatedRemainingPayments();
			verifyPayThisAmountBtn();
			Reporter.log("Verified Payment Estimator page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Payment estimator page");
		}
		return this;
	}

	/**
	 * Verify all the prepopulated elements on the payment estimator page
	 * 
	 * @param name
	 * @param msisdn
	 * @param eipID
	 * @param deviceName
	 * @param deviceBal
	 */
	public void verifyPrepopulatedFields(String name, String msisdn, String eipID, String deviceName,
			String deviceBal) {
		try {
			eipInfoCustomerName.getText().equals(name);
			eipInfoDeviceName.getText().equals(deviceName);
			eipInfoMisdin.getText().equals(msisdn);
			eipInfoEipId.getText().equals(eipID);
			eipInfoDeviceBalance.getText().equals(deviceBal);
			eipInfoEipIdValue.getText().equals(eipID);
			eipInfoDeviceBalanceValue.getText().equals(deviceBal);
			Reporter.log("Verified prepopulated fields on PaymentEstimator page");
		} catch (Exception e) {
			Assert.fail("Issue in verifying prepopulated fields on PaymentEstimator page");
		}
	}

	/**
	 * 
	 * verify payment amount field
	 */
	public void verifyPaymentAmountField() {
		try {
			paymentAmountInput.isDisplayed();
			paymentAmountInput.getAttribute("placeholder").equals("Enter extra payment here");
			Reporter.log("Verified Payment amount field");
		} catch (Exception e) {
			Assert.fail("Payment amount field not found");
		}
	}

	/**
	 * verify Estimate and cross button
	 */
	public void verifyEstimateAndCrossBtn() {
		try {
			crossIcon.isDisplayed();
			estimateBtn.isDisplayed();
			Reporter.log("Estimate button and Cross Icon are displayed");
		} catch (Exception e) {
			Assert.fail("Estimate button and Cross Icon are not found");
		}
	}

	/**
	 * verify PII masking for Name
	 * 
	 * @param piiCustomerCustfirstnamePid
	 */
	public void verifyPiiMaskingForName(String piiCustomerCustfirstnamePid) {
		try {
			checkElementisPIIMasked(eipInfoCustomerName, piiCustomerCustfirstnamePid);
		} catch (Exception e) {
			Assert.fail("Error verifying PII masking on Name");
		}
	}

	/**
	 * Verify PII masking for MSISDN
	 * 
	 * @param piiCustomerMsisdnPid
	 */
	public void verifyPiiMaskingForMSISDN(String piiCustomerMsisdnPid) {
		try {
			checkElementisPIIMasked(eipInfoMisdin, piiCustomerMsisdnPid);
		} catch (Exception e) {
			Assert.fail("Error verifying PII masking on MSISDN");
		}
	}
}
