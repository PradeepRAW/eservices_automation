/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author
 *
 */
public class ChangePlanPlansBreakdownPage extends CommonPage {

	// Headers
	@FindBy(css = "div[class*='text-right'] p[class='body']")
	private WebElement taxesAndFeesLabel;

	@FindBy(css = "div[class*='body padding-top-small']")
	private WebElement subHeadingOfPlansBreakDownPage;

	@FindBy(css = ".Display3")
	private WebElement headingOfPlansBreakDownPage;

	@FindBy(css = "span[class*='black float-right']")
	private WebElement priceAtAccountLevel;

	// Buttons

	@FindBy(css = "button[class*='SecondaryCTA']")
	private WebElement backToSummaryCTA;

	// Others
	@FindAll({ @FindBy(xpath = "//span[text()='Mobile Internet 2GB']//..//..//input"),
			@FindBy(xpath = "//span[text()='Mobile Internet 6GB']//..//..//input") })
	private WebElement mobileInternet;

	@FindBy(xpath = "(//*[@class='black Display4 float-left'])/following::*[@class='text-right']")
	private List<WebElement> amountsOfEachItemUnderEachLines;

	@FindBy(id = "nextButton_id")
	private WebElement nextButton;

	@FindBy(id = "changeNoticeContinueBtn")
	private WebElement continueButton;

	@FindAll({ @FindBy(css = "#reviewPageContainer_id > div.ui_headline.pt30"),
			@FindBy(css = "#reviewPageContainer_id > div.co_review") })
	private WebElement reviewChangesheader;

	@FindBy(id = "addedServiceName_id")
	private WebElement mobileInternetTwoGBAdded;

	@FindBy(xpath = "//*[@id='dynamicData_id']//..//input[@value='3']")
	private WebElement tMobileonePlan;

	@FindAll({ @FindBy(id = "currentMonthlyTotal_id"),
			@FindBy(css = "#reviewPageContainer_id > div.monthly-total > div.current-monthly-total") })
	private WebElement currentMonthlytotal;

	@FindAll({ @FindBy(id = "newTotal_id"),
			@FindBy(css = "#reviewPageContainer_id > div.monthly-total > div:nth-child(1)") })
	private WebElement newMonthlytotal;

	@FindBy(id = "removedServiceName_id")
	private WebElement removedExistingservice;

	// ######################

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement textOfTotalMonthlyPlanCost;

	@FindBy(css = "p[class*='Display5']")
	private WebElement costAtTotalMonthlyPlan;

	@FindBy(xpath = "p[class='Display6 text-right']")
	private List<WebElement> amountsAtLineLevel;

	@FindBy(xpath = "//*[@class=' black float-right']")
	private WebElement totalMonthlyCost;

	@FindBy(css = "p[class='text-right']")
	private List<WebElement> priceOfEachItemAtAccountLevel;

	@FindBy(xpath = "//*[contains(text(),'Plans')]")
	private WebElement plansTabOnPlansReviewPage;

	@FindBy(xpath = "//*[contains(text(),'Device')]")
	private WebElement deviceTabOnPlansReviewPage;

	@FindBy(css = "span.warning-brown-regular")
	private WebElement warningIconForRemovedSection;

	@FindBy(css = "div[aria-label*='items will be removed']")
	private WebElement warningMessageForRemovedSection;

	@FindBy(xpath = "(//*[contains(@class,'col-12 col-md-12 no-padding padding-top-small-xs  padding-top-small-sm')])")
	private WebElement removedSectionForSingleLine;

	@FindBy(css = "span[aria-label='T-Mobile ONE']")
	private WebElement currentPlanNameUnderRemovedSection;

	@FindBy(css = "p.Display4")
	private List<WebElement> namesOfEachLine;

	@FindBy(css = "p[class*='H6-heading']")
	private List<WebElement> msisdnOfEachLine;

	@FindBy(xpath = "//span[@class='H6-heading']")
	private WebElement accountSectionLabel;

	@FindBy(xpath = "//p[contains(text(),'Lines impacted by plan change')]")
	private List<WebElement> impactedSectionLabel;

	@FindBy(xpath = "//p[contains(text(),'Lines not-impacted by plan change')]")
	private List<WebElement> nonImpactedSectionLabel;

	@FindBy(xpath = "(//span[contains(text(),'AutoPay discount')])")
	private WebElement autopayDiscountNode;

	@FindBy(xpath = "//span[contains(text(),'Your existing Mobile Internet plan is not compatib')]")
	private WebElement mbbIncompatibleMessage;

	@FindBy(xpath = "(//*[contains(@class,'padding-top-base-md padding-top-base-lg padding-top-base-xl')])")
	private List<WebElement> planNameTextUnderAccountAndEachline;

	@FindBy(css = "div[class*='justify-content-between'] p[class*='text-right']")
	private List<WebElement> priceValueUnderAccountAndEachline;

	@FindBy(xpath = "(//*[contains(@class,'col-12 col-md-12 no-padding padding-top-small-xs  padding-top-small-sm padding-bottom-base-xs padding-bottom-base-sm')])")
	private List<WebElement> removedSectionInCaseOfMultipleRemoval;

	@FindBy(xpath = "(//*[@class='body color-brown'])")
	private List<WebElement> multipleInCompatibleMessage;

	// String planName = null;

	/**
	 * 
	 * @param webDriver
	 */
	public ChangePlanPlansBreakdownPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify plans Breakdown page is displayed
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage verifyPlansBreakdownPage() {
		checkPageIsReady();
		waitforSpinner();
		try {
			waitFor(ExpectedConditions.urlContains("plan-breakdown"));
			Reporter.log("Plans Breakdown page is displayed");
		} catch (Exception e) {
			Assert.fail("Plans Breakdown page is not displayed");
		}
		return this;
	}

	// click on Plans tab
	public ChangePlanPlansBreakdownPage clickOnPlansBlade() {
		plansTabOnPlansReviewPage.click();
		return this;
	}

	// click on Device tab
	public ChangePlanPlansBreakdownPage clickOnDevicesBlade() {
		deviceTabOnPlansReviewPage.click();
		return this;
	}

	/**
	 * Check header of Plans Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage checkHeaderOfPlansBreakDownPage() {
		try {
			Assert.assertTrue(headingOfPlansBreakDownPage.getText().equals("Plan change details"),
					"Header of Plans Breakdown page is mismatched");
			Reporter.log("Header of Plans change Details page is matched.");
		} catch (Exception e) {
			Assert.fail("Header of Plans change Details page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Subheader of Plans Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage checkSubHeaderOfPlansBreakDownPage(String passedPlanName) {
		String message = "Heres how " + passedPlanName + " applies to your account.";
		try {
			Assert.assertTrue(
					subHeadingOfPlansBreakDownPage.getText().trim().replaceAll("[^\\x00-\\x7f]", "").contains(message),
					"Sub header on Plans breakdown page is mismatched");
			Reporter.log("Sub header text on Plans breakdown page is matched");
		} catch (Exception e) {
			Assert.fail("Subheading on Plans Breakdown page is not displayed");
		}
		return this;
	}

	/**
	 * Check Subheader of Plans Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage checkSubHeaderOnPlansBreakDownPage(String passedPlanName) {

		String actualPlanName = null;
		String planName = passedPlanName;
		try {
			actualPlanName = subHeadingOfPlansBreakDownPage.getText().trim();
			if ((actualPlanName.contains("Here’s how")) && (actualPlanName.contains("applies to your account.")))
				Reporter.log("Subheader text is present");
			else
				Assert.fail("Subheader text on Plans breadkdown is mismatched");
		} catch (Exception e) {
			Assert.fail("Subheading on Plans Breakdown page is not displayed");
		}

		if (passedPlanName.contains("Magenta")) {
			planName = passedPlanName;
			String name = null;
			String extn = null;
			String regex = "\u2122";
			Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(planName);
			while (matcher.find()) {
				int startIndex = matcher.start();

				name = planName.substring(0, startIndex);
				extn = planName.substring(9);
				String ex3 = name + " " + extn;

				if ((actualPlanName.contains(name)) && (actualPlanName.contains(extn)))
					System.out.println("Plan name is matched in subheader on Plans Breakdown page.");
				else
					Assert.fail("Plan name is mismatched in subheader on Plans Breakdown page.");
			}
		} else {
			String ExpectedText = "Here are the " + planName + " details for your account.";
			Assert.assertEquals(actualPlanName, ExpectedText,
					"Plan name is mismatched in subheader on Plans Breakdown page.");
			Reporter.log("Plan name is matched in Sub Header on Plans breakdown page.");
		}
		return this;
	}

	/**
	 * check whether Warning icon is displayed or not
	 * 
	 * @return
	 */
	public String verifyWarningIcon() {
		String ratePlan = null;
		try {
			warningIconForRemovedSection.isDisplayed();
			Reporter.log("Waning icon is displayed under Removed section");
		} catch (Exception e) {
			Assert.fail("Waning icon is not displayed under Removed section");
		}
		return ratePlan;
	}

	/**
	 * check whether Warning message is displayed or not
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage verifyWarningMessageForRemovedSection() {
		try {
			warningMessageForRemovedSection.isDisplayed();
			Reporter.log("Waning message is displayed under Removed section");
			if (warningMessageForRemovedSection.getText().contains("Plan change—items will be removed"))
				Reporter.log("Warning message matched");
			else
				Assert.fail("Warning message mismatched");
		} catch (Exception e) {
			Assert.fail("Waning message is not displayed under Removed section");
		}
		return this;
	}

	/**
	 * check whether Warning message is displayed or not
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage verifyRemovedSectionForSingleLine() {
		try {
			if (removedSectionForSingleLine.getText().contains("Plan change—items will be removed"))
				Reporter.log("Waning message is displayed under Removed section");
			else
				Assert.fail("Warning message mismatched");
		} catch (Exception e) {
			Assert.fail("Waning message is not displayed under Removed section");
		}
		return this;
	}

	/**
	 * check whether Warning message is displayed or not
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage verifyCurrentPlanNameUnderRemovedSection(String currentplanName) {
		try {
			currentPlanNameUnderRemovedSection.isDisplayed();
			if (currentplanName.contains(currentPlanNameUnderRemovedSection.getText().trim()))
				Reporter.log("Current plan name matched under Removed section");
			else
				Assert.fail("Current plan name mismatched in removed section.");
		} catch (Exception e) {
			Assert.fail("Current Plan Name is not displayed.");
		}
		return this;
	}

	/**
	 * Click on Back to Summary CTA
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage clickOnBackToSummaryCTA() {
		try {
			backToSummaryCTA.isDisplayed();
			backToSummaryCTA.click();
			Reporter.log("Back to Summary CTA is clicked.");
		} catch (Exception e) {
			Assert.fail("Back to Summary CTA is not displayed.");
		}
		return this;
	}

	/**
	 * Click on Back to Summary CTA
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage verifyNavigationOfBackToSummaryCTA() {
		try {
			clickOnBackToSummaryCTA();
			ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
			changePlansReviewPage.verifyChangePlansReviewPage();
			Reporter.log("Back to Summary Navigate to Plans Review page.");
		} catch (Exception e) {
			Assert.fail("Back to Summary not navigate to Plans Review page.");
		}
		return this;
	}

	// Check Name of each line
	public ChangePlanPlansBreakdownPage verifyTaxesAndFeesText(String planName) {
		String taxesAndFeesText = taxesAndFeesLabel.getText().trim();
		if (planName.contains("Magenta")) {
			Assert.assertTrue(taxesAndFeesText.contains("Taxes and fees included."), "Mismatch in Taxes and fees text");
			Reporter.log("Text Taxes and fees matched");
		} else if (planName.contains("Essential")) {
			Assert.assertEquals(taxesAndFeesText, "Taxes and fees additional.", "Mismatch in Taxes and fees text");
			Reporter.log("Text Taxes and fees matched");
		}
		return this;
	}

	// Check Name of each line
	public ChangePlanPlansBreakdownPage verifyNamesOfEachLine() {
		try {
			List<String> slist = new ArrayList<>();
			slist.add("regression");
			slist.add("test");
			slist.add("auto");
			if (namesOfEachLine.stream()
					.anyMatch(str -> slist.contains(str.getText().replaceAll("\\D+", "").matches("\\d+")))) {
				Reporter.log("Line Name is displaying with Digits");
			} else if (namesOfEachLine.stream().anyMatch(str -> slist.contains(str.getText()))) {
				Reporter.log("Line Name is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Line Name is displaying Null");
		}
		return this;
	}

	// Check Name of each line during change plan in same tax treatment
	public ChangePlanPlansBreakdownPage verifyNamesOfEachLineInChangePlanForNonPooled() {
		String lineName = null;
		List<String> slist = new ArrayList<>();
		slist.add("Auto");
		slist.add("test");
		slist.add("Automation");
		int count = namesOfEachLine.size();
		for (int i = 0; i < count; i++) {
			lineName = namesOfEachLine.get(i).getText();
			if (slist.contains(lineName)) {
				Reporter.log(lineName + " displayed for line " + i);
			} else
				Reporter.log("Name for line " + i + " is mismatched on Plans Breakdown page");
		}
		return this;
	}

	// Check MSISDN of each line during change plan for Non pooled account
	public ChangePlanPlansBreakdownPage verifyMSISDNOfEachLine() {
		String mSISDN = null;
		try {
			for (int i = 0; i < msisdnOfEachLine.size(); i++) {
				mSISDN = msisdnOfEachLine.get(i).getText();
				if (mSISDN.replaceAll("\\D+", "").matches("\\d+")) {
					Reporter.log(mSISDN + "  is displayed for line");
				}
			}
		} catch (Exception e) {
			Assert.fail("MSISDN is displaying Null");
		}
		return this;
	}

	public List<String> getPLanNameList() {
		List<String> expectedName = new ArrayList<String>();
		expectedName.add("Magenta");
		expectedName.add("AutoPay discount");
		expectedName.add("T-Mobile ONE");
		expectedName.add("Magenta Plus");
		expectedName.add("T-Mobile ONE Tablet");
		return expectedName;
	}

	// Check Plan name under account and each line
	public ChangePlanPlansBreakdownPage verifyPlanNameSection() {
		String lineName = null;
		int count = planNameTextUnderAccountAndEachline.size();
		try {
			for (int i = 0; i < count; i++) {
				lineName = planNameTextUnderAccountAndEachline.get(i).getText().replaceAll("[^\\x00-\\x7f]", "");
				if (!lineName.isEmpty() && getPLanNameList().contains(lineName))
					Reporter.log(lineName + " Plan Name is displayed");
				else if (!lineName.isEmpty() && !getPLanNameList().contains(lineName)) {
					Reporter.log(lineName + " Plan Name is displayed");
				} else
					Assert.fail("Plan Name is displaying Null");
			}
		} catch (Exception e) {
			Assert.fail("Plan Name is displaying Null");
		}
		return this;
	}

	// Check Plan name under account and each line
	public ChangePlanPlansBreakdownPage verifyPriceAtAccountSectionAndEachLine() {
		String linePrice = null;
		try {
			for (int i = 0; i < priceValueUnderAccountAndEachline.size(); i++) {
				linePrice = priceValueUnderAccountAndEachline.get(i).getText();

				if (linePrice.contains("-")) {
					Reporter.log("Price value displayed for everyline");
				} else if (linePrice.contains("$")) {
					Reporter.log("Price value displayed for everyline");
				} else if (linePrice.contains("Included")) {
					Reporter.log("Price value displayed for everyline");
				} else {
					Assert.fail("Price value is displaying Null");
				}
			}
		} catch (Exception e) {
			Assert.fail("Price value is displaying Null");
		}
		return this;
	}

	// Check Plan name under account and each line
	public ChangePlanPlansBreakdownPage verifyMultipleRemovedSectionDuringHybridMigrationTEToTI() {

		if (verifyElementBytext(removedSectionInCaseOfMultipleRemoval, "Plan change—items will be removed")) {
			Reporter.log("Plan changeitems will be removed Incompatible message displayed");
		} else {
			Reporter.log("Incompatible message is not displayed for line ");
		}
		return this;
	}

	// Check Plan name under account and each line
	public ChangePlanPlansBreakdownPage verifyMultipleIncompatibleMessagesDuringHybridMigrationTEToTI() {
		String lineName = null;
		String msg = "Your existing Mobile Internet plan is not compatible with your new plan. We selected the best match for you.";
		int count = multipleInCompatibleMessage.size();
		for (int i = 0; i < count; i++) {
			lineName = priceValueUnderAccountAndEachline.get(i).getText();
			if (lineName.contains(msg))
				Reporter.log("Incompatible message displayed " + i);
			else
				Reporter.log("Incompatible message is not displayed for line  " + i);
		}
		return this;
	}

	// Count number of lines
	public int getTotalLines() {
		int count = namesOfEachLine.size();
		return count;
	}

	// Check Account section is displayed or not
	public ChangePlanPlansBreakdownPage verifyAccountSection() {
		try {
			accountSectionLabel.isDisplayed();
			Reporter.log("Account section is displayed.");
		} catch (Exception e) {
			Assert.fail("Label Account is not displayed");
		}
		return this;
	}

	// Check Account section is displayed or not
	public ChangePlanPlansBreakdownPage verifyAccountSectionNotDisplayed() {
		try {
			Assert.assertTrue(accountSectionLabel.isDisplayed(), "Account section is not displayed.");
			Assert.fail("Label Account is displayed");
		} catch (Exception e) {
			Reporter.log("Account section is not displayed.");
		}
		return this;
	}

	// Check Impacted section is displayed or not
	public ChangePlanPlansBreakdownPage verifyImpactedSection() {
		try {
			Assert.assertTrue(impactedSectionLabel.get(0).isDisplayed(), "Impacted Label is not displayed");
			Reporter.log("Impacted label is displayed.");
			Assert.assertEquals(impactedSectionLabel.get(0).getText().trim(), "Lines impacted by plan change",
					"Text Lines impacted is mismatched");
		} catch (Exception e) {
			Assert.fail("Impacted Label is not displayed");
		}
		return this;
	}

	// Check Impacted section is displayed during migration or not
	public ChangePlanPlansBreakdownPage verifyImpactedSectionDuringMigration() {
		try {
			Assert.assertTrue(impactedSectionLabel.isEmpty(), "Impacted section is not displayed");
			Reporter.log("Impacted section is displayed");
		} catch (Exception e) {
			Assert.fail("Impacted section displayed during migration");
		}
		return this;
	}

	// Check Non Impacted section is displayed or not
	public ChangePlanPlansBreakdownPage verifyNotImpactedSectionSection() {
		try {
			Assert.assertTrue(nonImpactedSectionLabel.get(0).isDisplayed(), "Non Impacted Label is not displayed");
			Reporter.log("Non Impacted label is displayed.");
		} catch (Exception e) {
			Assert.fail("Non Impacted Label is not displayed");
		}
		return this;
	}

	// Check Non Impacted section is displayed during migration or not
	public ChangePlanPlansBreakdownPage verifyNonImpactedSectionDuringMigration() {
		try {
			Assert.assertTrue(nonImpactedSectionLabel.isEmpty(), "Non Impacted section is not displayed");
			Reporter.log("Non Impacted section is not displayed");
		} catch (Exception e) {
			Assert.fail("Non Impacted section displayed during migration");
		}
		return this;
	}

	// Check MSISDN of each line
	public ChangePlanPlansBreakdownPage verifyPlanNameAndPriceUnderAccountLevelAndEachLineLevel() {
		verifyNamesOfEachLine();
		verifyMSISDNOfEachLine();
		verifyPlanNameSection();
		verifyPriceAtAccountSectionAndEachLine();
		return this;
	}

	// Check MSISDN of each line
	public ChangePlanPlansBreakdownPage verifyAllDetailsDuringHybridMigration(String planName) {
		checkHeaderOfPlansBreakDownPage();
		checkSubHeaderOfPlansBreakDownPage(planName);
		verifyTaxesAndFeesText(planName);
		verifyNamesOfEachLine();
		verifyMSISDNOfEachLine();
		verifyPlanNameSection();
		verifyPriceAtAccountSectionAndEachLine();
		verifyMultipleIncompatibleMessagesDuringHybridMigrationTEToTI();
		verifyMultipleRemovedSectionDuringHybridMigrationTEToTI();
		verifyImpactedSectionDuringMigration();
		verifyNonImpactedSectionDuringMigration();
		checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
		return this;
	}

	// Check MSISDN of each line
	public ChangePlanPlansBreakdownPage verifyAllDetailsDuringChangePlanInSameTaxTreatment(String planName) {
		checkHeaderOfPlansBreakDownPage();
		checkSubHeaderOfPlansBreakDownPage(planName);
		verifyTaxesAndFeesText(planName);
		verifyAccountSection();
		verifyImpactedSection();
		verifyNotImpactedSectionSection();
		verifyNamesOfEachLine();
		verifyMSISDNOfEachLine();
		verifyPlanNameSection();
		verifyPriceAtAccountSectionAndEachLine();
		checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
		return this;
	}

	// Check MSISDN of each line
	public ChangePlanPlansBreakdownPage verifyAllDetailsDuringChangePlanForNonPooledAccount(String planName) {
		checkHeaderOfPlansBreakDownPage();
		checkSubHeaderOfPlansBreakDownPage(planName);
		verifyTaxesAndFeesText(planName);
		verifyAccountSectionNotDisplayed();
		verifyImpactedSection();
		verifyNotImpactedSectionSection();
		verifyNamesOfEachLineInChangePlanForNonPooled();
		verifyMSISDNOfEachLine();
		verifyPlanNameSection();
		verifyPriceAtAccountSectionAndEachLine();
		checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
		return this;
	}

	// Check MSISDN of each line
	public ChangePlanPlansBreakdownPage verifyAllDetailsDuringChangePlanForSingleLine(String planName) {
		checkHeaderOfPlansBreakDownPage();
		checkSubHeaderOfPlansBreakDownPage(planName);
		verifyTaxesAndFeesText(planName);
		verifyAccountSectionNotDisplayed();
		verifyMSISDNOfEachLine();
		verifyPlanNameSection();
		verifyPriceAtAccountSectionAndEachLine();
		verifyImpactedSectionDuringMigration();
		verifyNonImpactedSectionDuringMigration();
		verifyRemovedSectionForSingleLine();
		checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
		return this;
	}

	// Check cost of each item in Account section
	public int verifyPriceOfEachItemInAccountSection() {

		int sum = 0;
		boolean disc = false;
		for (WebElement element : priceOfEachItemAtAccountLevel) {

			int costAtLineLevel = 0;
			String cost = element.getText();
			String realCost[];

			if (cost.contains("-"))
				disc = true;
			if (element.getText().equalsIgnoreCase("Included")) {
				costAtLineLevel = 0;
				sum = sum + costAtLineLevel;
			} else if (disc = true) {
				realCost = cost.split("$");
				sum = sum + Integer.parseInt(realCost[1]);
			} else if (disc = false) {
				realCost = cost.split("$");
				sum = sum + Integer.parseInt(realCost[0]);
			}

		}
		return sum;
	}

	// Check cost at Account level
	public int verifyCostAtAccountSection() {
		int sum = 0;
		boolean disc = false;
		String cost = priceAtAccountLevel.getText();
		String realCost[];

		if (cost.contains("-"))
			disc = true;
		if (disc = true) {
			realCost = cost.split("$");
			sum = sum + Integer.parseInt(realCost[1]);
		} else if (disc = false) {
			realCost = cost.split("$");
			sum = sum + Integer.parseInt(realCost[0]);
		}
		return sum;
	}

	// Verify cost at account level and cost of each item under account section
	public int verifyCostOfEachItemUnderAccountSectionAndCostAtAccountSection() {
		int costOfEachItem = 0;
		int costAtAccountLevel = 0;

		costOfEachItem = verifyPriceOfEachItemInAccountSection();
		costAtAccountLevel = verifyCostAtAccountSection();
		if (costOfEachItem == costAtAccountLevel)
			Reporter.log("Prices are matching in Account section");
		else
			Reporter.log("Prices are not matching in Account section");
		return costAtAccountLevel;
	}

	/**
	 * Read Total Monthly Cost
	 * 
	 * @return
	 */
	public int verifyTotalMonthlyCost() {
		int totalMonthlyCostValue = 0;
		try {
			String cost[] = totalMonthlyCost.getText().split("$");
			totalMonthlyCostValue = Integer.parseInt(cost[0]);
		} catch (Exception e) {
			Assert.fail("Not able to calculate Total Monthly cost.");
		}
		return totalMonthlyCostValue;
	}

	// Verify Total Monthly cost, by adding cost at Account level + Line level
	public int verifyAndCheckTotalMonthlyCostForMultiLine() {
		int totalMonthlyCost = 0;
		double totalCostAtAccountLevel = 0;
		double totalCostAtLineLevel = 0;
		double accountAndLineLevelCost = 0;

		totalMonthlyCost = verifyTotalMonthlyCost();

		totalCostAtAccountLevel = verifyCostOfEachItemUnderAccountSectionAndCostAtAccountSection();
		totalCostAtLineLevel = calculateTotalAmountForEachItemUnderAllLines();

		accountAndLineLevelCost = totalCostAtAccountLevel + totalCostAtLineLevel;

		if (totalMonthlyCost == accountAndLineLevelCost)
			Reporter.log("Prices are matching in Total Monthly section");
		else
			Assert.fail("Prices are not matching in Total Monthly Section");

		return totalMonthlyCost;
	}

	// Verify Total Monthly cost for Single line
	public ChangePlanPlansBreakdownPage verifyAndCheckTotalMonthlyCostForSingleLine() {
		int totalMonthlyCost = 0;
		int totalCostAtAccountLevel = 0;

		totalMonthlyCost = verifyTotalMonthlyCost();

		totalCostAtAccountLevel = verifyCostAtAccountSection();

		if (totalMonthlyCost == totalCostAtAccountLevel)
			Reporter.log("Prices are matching in Total Monthly section");
		else
			Assert.fail("Prices are not matching in Total Monthly Section");

		return this;
	}

	// verify for Single line
	public ChangePlanPlansBreakdownPage verificationOfSingleLineValidation(String planName) {
		checkHeaderOfPlansBreakDownPage();
		checkSubHeaderOfPlansBreakDownPage(planName);
		verifyPlanNameAndPriceUnderAccountLevelAndEachLineLevel();
		verifyCostOfEachItemUnderAccountSectionAndCostAtAccountSection();
		verifyAndCheckTotalMonthlyCostForSingleLine();
		verifyWarningIcon();
		verifyWarningMessageForRemovedSection();
		verifyCurrentPlanNameUnderRemovedSection(planName);
		clickOnBackToSummaryCTA();
		return this;
	}

	// verify for Single line
	public ChangePlanPlansBreakdownPage checkWhetherAutoPayDiscountComingOrNot() {
		try {
			Assert.assertTrue(autopayDiscountNode.isDisplayed(),
					"Autopay Discount node is not coming in Plans breakdown page");
			Reporter.log("Autopay Discount node is coming in Plans breakdown page");
		} catch (Exception e) {
			Assert.fail("Autopay Discount node is not coming in Plans breakdown page");
		}
		return this;
	}

	// verify when Warning message not displayed during migration scenario when
	// MBB
	// is incompatible
	public ChangePlanPlansBreakdownPage checkWhetherMBBIncompatibleMessageIsDisplayed() {
		try {
			String msg = "Your current Mobile Internet plan isn't compatible with this new plan, but we'll select the best available plan for you. Please review this change by tapping 'Plans' in the next step";
			mbbIncompatibleMessage.isDisplayed();
			Reporter.log("InCompatible message for MBB is displayed in Plans breakdown page");
			Assert.assertEquals(mbbIncompatibleMessage.getText().trim(), msg,
					"MI incompatible message is mismatched on Plans Breakdown page");
		} catch (Exception e) {
			Assert.fail("InCompatible message for MBB is not displayed in Plans breakdown page");
		}
		return this;
	}

	// verify when Warning message not displayed during migration scenario when
	// MBB
	// is compatible
	public ChangePlanPlansBreakdownPage checkWhetherMBBIncompatibleMessageIsNotDisplayed() {
		try {
			mbbIncompatibleMessage.isDisplayed();
			Assert.fail("InCompatible message for MBB is displayed in Plans breakdown page");
		} catch (Exception e) {
			Reporter.log("InCompatible message for MBB is not displayed in Plans breakdown page");
		}

		return this;
	}

	// #####

	// ################ Below calculations are for Account section and are
	// correct,
	// rest to be deleted if not
	// used any where

	/**
	 * Verify Account Level cost
	 * 
	 * @return
	 */
	public double checkAndVerifyCostAtAccountLevel() {
		double totalOfEachItem;
		double costatAccountLevel;

		totalOfEachItem = checkAndCalculateAmountForEachItemUnderAccountSection();
		costatAccountLevel = checkCostAtAccountLevel();
		if (totalOfEachItem == costatAccountLevel)
			Reporter.log("Cost at Account level is matching");
		else
			Reporter.log("There is a mismatch in cost at Account level and items under account section");
		return costatAccountLevel;
	}

	/**
	 * Read Account Level cost
	 * 
	 * @return
	 */
	public double checkCostAtAccountLevel() {
		double accountLevelCost = 0.00;
		try {

			String cost = costAtTotalMonthlyPlan.getText().trim().replace("$", "");
			accountLevelCost = accountLevelCost + Double.parseDouble(cost);
		} catch (Exception e) {
			Reporter.log("Not able to calculate Account level Addons cost.");
		}
		return accountLevelCost;

	}

	/**
	 * Calculate amount at Account Level
	 * 
	 * @return
	 */
	public double checkAndCalculateAmountForEachItemUnderAccountSection() {
		double costAtAccountLevel = 0.00;
		try {
			boolean disc = false;
			for (WebElement element : priceOfEachItemAtAccountLevel) {
				double costAtLineLevel = 0.00;
				String cost = element.getText().trim();
				String realCost;

				if (cost.contains("-")) {
					realCost = cost.replace("$", "");
					costAtAccountLevel = costAtAccountLevel + Double.parseDouble(realCost);
				} else if (cost.contains("$")) {
					disc = true;
					realCost = cost.replace("$", "");
					costAtAccountLevel = costAtAccountLevel + Double.parseDouble(realCost);

				} else if (cost.equalsIgnoreCase("Included")) {
					costAtAccountLevel = costAtAccountLevel + costAtLineLevel;
				}
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost under each item.");
		}
		return costAtAccountLevel;
	}

	// ########### Calculations for Line Level Section ##################

	/**
	 * Calculate amount for each item under each line
	 * 
	 * @return
	 */
	public double calculateTotalAmountForEachItemUnderAllLines() {
		double totalCostForEachItemForAllLines = 0.00;
		try {

			for (WebElement element : amountsOfEachItemUnderEachLines) {
				boolean disc = false;
				double costAtLineLevel = 0.00;
				String cost = element.getText().trim();
				String realCost;

				if (cost.contains("-")) {
					disc = true;
					realCost = cost.replace("$", "");
					totalCostForEachItemForAllLines = totalCostForEachItemForAllLines - Double.parseDouble(realCost);
				} else if (cost.equalsIgnoreCase("Included")) {
					totalCostForEachItemForAllLines = totalCostForEachItemForAllLines + costAtLineLevel;
				} else if (disc == false) {
					realCost = cost.replace("$", "");
					totalCostForEachItemForAllLines = totalCostForEachItemForAllLines + Double.parseDouble(realCost);
				}
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost for each item under each line.");
		}
		return totalCostForEachItemForAllLines;
	}

	/**
	 * Calculate amount for each item under each line
	 * 
	 * @return
	 */
	public double calculateTotalAmountAtLineLevelForAllLines() {
		double totalCostAtLineLevelForAllLines = 0.00;
		try {
			for (WebElement element : amountsAtLineLevel) {
				String cost = element.getText().trim();
				totalCostAtLineLevelForAllLines = totalCostAtLineLevelForAllLines + Double.parseDouble(cost);
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost for each item under each line.");
		}
		return totalCostAtLineLevelForAllLines;
	}

	/**
	 * Verify Costs of All Lines and Each item under each line
	 * 
	 * @return
	 */
	public double calculateAndVerifyTotalCostsAtLineLevel() {
		double totalCostAtLineLevelForAllLines;
		double totalCostForEachItemForAllLines;

		totalCostAtLineLevelForAllLines = calculateTotalAmountAtLineLevelForAllLines();
		totalCostForEachItemForAllLines = calculateTotalAmountForEachItemUnderAllLines();

		if (totalCostAtLineLevelForAllLines == totalCostForEachItemForAllLines)
			Reporter.log("Cost at Line level is matching");
		else
			Reporter.log("There is a mismatch in cost at Line level and items under each Line level section");
		return totalCostAtLineLevelForAllLines;
	}

	// Calculations for Total Monthly Cost ## Correct
	/**
	 * Read Total Monthly Cost
	 * 
	 * @return
	 */
	public double checkTotalMonthly() {
		double totalMonthlyCost = 0.00;
		try {
			costAtTotalMonthlyPlan.isDisplayed();
			String cost = costAtTotalMonthlyPlan.getText().trim().replace("$", "");
			totalMonthlyCost = totalMonthlyCost + Double.parseDouble(cost);
		} catch (Exception e) {
			Reporter.log("Not able to calculate Total Monthly cost on Plans Breakdown page.");
		}
		return totalMonthlyCost;
	}

	/**
	 * Verify cost at Account Level and Line level
	 * 
	 * @return
	 */
	public double checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel() {
		double costatAccountLevel;
		double totalMontlyCost;

		totalMontlyCost = checkTotalMonthly();
		Reporter.log("Total Monthly cost on Plans breakdown page is " + totalMontlyCost);

		costatAccountLevel = checkAndVerifyCostAtAccountLevel();
		Reporter.log("Total cost at Account level on Plans Breakdown Page is " + costatAccountLevel);

		// costAtLineLevel = calculateAndVerifyTotalCostsAtLineLevel();
		// Reporter.log("Total cost at Line level on Plans Breakdown Page is " +
		// costatAccountLevel);

		// accountAndLineCost = costatAccountLevel + costAtLineLevel;
		// Reporter.log("Total cost at Account & Line level on Plans Breakdown Page is "
		// + accountAndLineCost);

		if (totalMontlyCost == costatAccountLevel)
			Reporter.log(
					"On Plans Breakdown page Cost at Total Monthly level is matching with Account level plus Line level");
		else
			Assert.fail(
					"On Plans Breakdown page there is a mismatch in cost at Total Monthly level and Account level plus Line level");
		return totalMontlyCost;
	}

	// get Total Monthly Cost after verifying all costs at Account and Line
	// level
	public String getTotalMonthlyCostWithFormatOfDollarSign() {
		String formattedCost = null;
		double totalMontlyCost;

		totalMontlyCost = checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
		formattedCost = "$" + String.format("%.2f", totalMontlyCost);
		Reporter.log("Total monthly cost with dollar sign on Plans Breakdown Page is " + formattedCost);

		return formattedCost;
	}

	/**
	 * Getting TotalCost without Dollar sign
	 * 
	 * @return
	 */
	public double getTotalMonthlyCostWithoutDollarSign() {
		double totalMontlyCost = 0.00;

		totalMontlyCost = checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
		return totalMontlyCost;
	}

	/**
	 * verify AddOn Cost Details
	 * 
	 * @return
	 */
	public ChangePlanPlansBreakdownPage verifyAddOnCostDetails(String costOnPlansBlade,
			String totalAddOnsBreakdownCost) {
		try {
			Assert.assertTrue(costOnPlansBlade.contains(totalAddOnsBreakdownCost),
					"total Add Ons Break down Cost is not correct");
			Reporter.log("Cost on Plans blade and Plans breakdown page is matched");
		} catch (Exception e) {
			Assert.fail("total Add Ons Break down Cost is not correct");
		}
		return this;
	}

}