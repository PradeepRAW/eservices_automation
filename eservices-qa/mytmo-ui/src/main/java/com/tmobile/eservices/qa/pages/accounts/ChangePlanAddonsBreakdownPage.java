
/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri.
 *
 */
public class ChangePlanAddonsBreakdownPage extends CommonPage {

	// Headers
	@FindBy(css = "p[class*='Display5']")
	private WebElement totalMonthlyCost;

	@FindBy(css = "span[class*='H6-heading']")
	private WebElement accountSectionText;

	@FindBy(css = "span[class*='H6-heading'] span")
	private WebElement accountLevelCost;

	@FindBy(css = "*[class*='Display6 text-right']")
	private List<WebElement> lineLevelCost;

	// Buttons
	@FindBy(css = "Button[class*='SecondaryCTA blackCTA']")
	private WebElement backToSummaryCTAOnDeviceBreakdownPage;

	// Others
	@FindBy(css = ".Display3")
	private WebElement headingOfAddonsBreakDownPage;

	@FindBy(css = ".body.padding-top-small")
	private WebElement subHeadingOfAddonsBreakDownPage;

	// #################################

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement textTotalMonthlyAddonCost;

	@FindBy(xpath = "(//*[@class='black Display4 float-left'])/following::*[@class='text-right']")
	private List<WebElement> amountsAtEachLineLevel;

	@FindBy(xpath = "(//*[@class='black Display4 float-left'])")
	private List<WebElement> namesOfEachLine;

	@FindBy(xpath = "(//p[@class='H6-heading'])")
	private List<WebElement> msisdnOfEachLine;

	private By numberOfAddonsForEachLine = By
			.xpath("((//*[@class='col-12 no-padding']//..)[%s]//div//div//*[@class='col-3 body'])");

	private By costOfAddonsForEachLine = By
			.xpath("((//*[@class='col-12 no-padding']//..)[%s]//div//div//*[@class='col-3 body'])[%t]");

	private By addOnsPlanName = By.xpath("//span[text()='%s']//following::p[@aria-label='Included']");

	@FindBy(xpath = "//span[text()='Protection<360>™ for Bring Your Own Device']//following::p[@aria-label='$15.00']")
	private WebElement protection360AddOnWithPrice;

	@FindBy(xpath = "//div[@aria-label ='warning icon Plan change—items will be removed']")
	private WebElement planChangeItemsWillBeRemoved;

	@FindBy(css = "p.H4-heading")
	private WebElement alertMessage;

	/**
	 * 
	 * @param webDriver
	 */
	public ChangePlanAddonsBreakdownPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Addons Breakdown page is displayed
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage verifyAddonsBreakdownPage() {
		checkPageIsReady();
		waitForDataPassSpinnerInvisibility();
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("addons-breakdown"));
			Reporter.log("Addons Breakdown page is displayed");
		} catch (Exception e) {
			Assert.fail("Addons Breakdown page is not displayed");
		}
		return this;
	}

	/**
	 * Check header of Addons Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage checkHeaderOfPlansBreakDownPage() {
		try {
			Assert.assertTrue(headingOfAddonsBreakDownPage.getText().equals("Your add-ons"),
					"Header of Device Breakdown page is mismatched");
			Reporter.log("Header of Device details page is matched.");
		} catch (Exception e) {
			Assert.fail("Header of Device Details page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Subheader of Plans Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage checkSubHeaderOfPlansBreakDownPage() {
		try {
			Assert.assertTrue(
					subHeadingOfAddonsBreakDownPage.getText()
							.contains("Here's the cost breakdown for the add-ons on your account."),
					"SubHeader of Addons Breakdown page is mismatched");
			Reporter.log("Sub Header of Addons page is matched.");
		} catch (Exception e) {
			Assert.fail("Sub Header of Addons details page is not displayed.");
		}
		return this;
	}

	/**
	 * Check text Total Monthly Cost on Addons Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage checkTotalMonthlyDeviceCostText() {
		try {
			Assert.assertTrue(textTotalMonthlyAddonCost.getText().contains("Total monthly add-on cost"),
					"Text Total Monthly Add-on cost on Add-on details Breakdown page is mismatched");
			Reporter.log("Text Total Monthly Add-on cost on Add-on details Breakdown page is matched");
		} catch (Exception e) {
			Assert.fail("Text Total Monthly Add-on cost on Add-on details Breakdown page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Total Monthly Cost for single line
	 * 
	 * @return
	 */
	public int calculateTotalMonthlyCostForAddonsForSingleLine() {
		int sum = 0;
		boolean included = false;
		try {
			String cost = totalMonthlyCost.getText();
			String realCost;
			if (cost.contains("Included")) {
				sum = 0;
				included = true;
			} else if (included = false) {
				realCost = cost.replace("$", "");
				sum = sum + Integer.parseInt(realCost);
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost.");
		}
		return sum;
	}

	/**
	 * Read Total Monthly Cost only
	 * 
	 * @return
	 */
	public int verifyTotalMonthlyCost() {
		int totalMonthlyCostValue = 0;
		// String monthlyCost = totalMonthlyDeviceCostAmount.getText();
		try {
			// String cost[] =
			// totalMonthlyDeviceCostAmount.getText().split("$");
			// totalMonthlyCostValue = Integer.parseInt(cost[0]);
		} catch (Exception e) {
			Assert.fail("Not able to calculate Total Monthly cost.");
		}
		return totalMonthlyCostValue;
	}

	// Verify Total Monthly cost for Single EIP Device
	public ChangePlanAddonsBreakdownPage verifyAndCheckTotalMonthlyCostWhenAccountHasOnlyOneDeviceOnEIP() {
		int totalMonthlyCost = 0;
		int totalCostAtLineLevel = 0;

		totalMonthlyCost = verifyTotalMonthlyCost();

		// totalCostAtLineLevel = calculateTotalMonthlyCostForSingleDevice();

		if (totalMonthlyCost == totalCostAtLineLevel)
			Reporter.log("Prices are matching in Total Monthly section");
		else
			Assert.fail("Prices are not matching in Total Monthly Section");

		return this;
	}

	/**
	 * Click on Back to Summary CTA
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage clickOnBackToSummaryCTA() {
		try {
			backToSummaryCTAOnDeviceBreakdownPage.isDisplayed();
			backToSummaryCTAOnDeviceBreakdownPage.click();
			Reporter.log("Back to Summary CTA is clicked on Device Breakdown page.");
		} catch (Exception e) {
			Assert.fail("Back to Summary CTA is not displayed on Device Breakdown page.");
		}
		return this;
	}

	/**
	 * Check Number Of Addons for each line
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage verifyNumberOFAddonsForEachLine() {
		String lineNumber = null;

		float amt = 0f;
		String cost = null;
		float totalOfAllItemsUnderEachLine = 0f;
		float itemCost = 0;

		// WebElement str = getNewLocator1(numberOfAddonsForEachLine,"1");

		int totalLines = msisdnOfEachLine.size();

		for (int line = 1; line <= totalLines; line++) {
			String item1 = String.valueOf(line);
			lineNumber = msisdnOfEachLine.get(line - 1).getText();
			List<WebElement> ele = getDriver().findElements(getNewLocator(numberOfAddonsForEachLine, item1));

			for (int item = 1; item <= ele.size(); item++) {
				String item2 = String.valueOf(item);
				WebElement ele1 = getDriver().findElement(getNewLocator(costOfAddonsForEachLine, "1", item2));
				cost = ele1.getText().trim();

				if (cost.equals("Included"))
					additionOfPrices(totalOfAllItemsUnderEachLine, amt);
				else if (cost.contains("$")) {
					itemCost = formatPriceValue(cost);
					additionOfPrices(totalOfAllItemsUnderEachLine, itemCost);
				}
			}
		}
		return this;
	}

	/**
	 * Method to format String Price to number
	 * 
	 * @return
	 */
	public float formatPriceValue(String cost) {
		String formattedCost = cost.replace("$", "");
		float actualCost = 0f;
		try {
			actualCost = Float.parseFloat(formattedCost);
		} catch (Exception e) {
			Assert.fail("Not able to format Price");
		}
		return actualCost;
	}

	/**
	 * Method to addition Of Prices
	 * 
	 * @return
	 */
	public float additionOfPrices(float totalCost, float amount) {
		float actualCost = 0f;
		try {
			actualCost = totalCost + amount;
		} catch (Exception e) {
			Assert.fail("Not able to format Price");
		}
		return actualCost;
	}

	/**
	 * verify AddOns PlanName
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage verifyAddOnsPlanName(String planName) {
		try {
			WebElement addOnsplanName = getDriver().findElement(getNewLocator(addOnsPlanName, planName));
			Assert.assertTrue(addOnsplanName.isDisplayed(),
					"Add Ons section " + planName + " add on is not displaying");
			Reporter.log("Add Ons section " + planName + " add on is displaying");
		} catch (Exception e) {
			Assert.fail("Add Ons section " + planName + " add on is not displaying");
		}
		return this;
	}

	/**
	 * verify Protection 360 AddOn With Price
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage verifyProtection360AddOnWithPrice() {
		try {
			Assert.assertTrue(protection360AddOnWithPrice.isDisplayed(),
					"Protection 360 Add On WithPrice is not displaying");
			Reporter.log("Protection 360 Add On With Price is displaying");
		} catch (Exception e) {
			Assert.fail("Protection 360 Add On WithPrice is not displaying");
		}
		return this;
	}

	/**
	 * verify Plan Change Name
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage verifyPlanChangeName(String name) {
		try {
			Assert.assertTrue(alertMessage.getText().contains(name), name + " is not displaying");
			Reporter.log(name + " is displaying");
		} catch (Exception e) {
			Assert.fail(name + " is not displaying");
		}
		return this;
	}

	/**
	 * verify AddOn Cost Details
	 * 
	 * @return
	 */
	public ChangePlanAddonsBreakdownPage verifyAddOnCostDetails(String costOnPlansBlade,
			String totalAddOnsBreakdownCost) {
		try {
			Assert.assertTrue(costOnPlansBlade.contains(totalAddOnsBreakdownCost),
					"total Add Ons Break down Cost is not correct");
			Reporter.log("Cost onAddons blade and Addons breakdown page is matched");
		} catch (Exception e) {
			Assert.fail("total Add Ons Break down Cost is not correct");
		}
		return this;
	}

	/**
	 * Verify Total Monthly Cost
	 * 
	 * @return
	 */
	public String checkAndVerifyCostAtAccountLevelAndLineLevelAgainstTotalMonthlyCost() {
		double totalOfEachItem = 0;
		double costatAccountLevel = 0;
		double accountAndLineCost;
		String formattedCostOfaccountAndLineCost = null;
		String totalMontlyCost = null;

		totalMontlyCost = checkTotalMonthlyLevelCost();
		costatAccountLevel = checkCostAtAccountLevel();
		totalOfEachItem = checkAndCalculateAmountForEachLine();

		accountAndLineCost = costatAccountLevel + totalOfEachItem;
		formattedCostOfaccountAndLineCost = "$" + String.format("%.2f", accountAndLineCost);

		if (totalMontlyCost.equals(formattedCostOfaccountAndLineCost))
			Reporter.log("Cost at Total Monthly level is matching with Account level plus Line level");
		else
			Assert.fail("There is a mismatch in cost at Total Monthly level and Account level plus Line level");

		return totalMontlyCost;
	}

	/**
	 * Getting TotalCost without Dollar sign
	 * 
	 * @return
	 */
	public double getTotalMonthlyCostWithoutDollarSign() {
		double totalMontlyCost = 0.00;
		String totalAddonsCost = null;

		totalAddonsCost = checkAndVerifyCostAtAccountLevelAndLineLevelAgainstTotalMonthlyCost();
		totalAddonsCost = totalAddonsCost.replace("$", "");
		totalMontlyCost = Double.parseDouble(totalAddonsCost);

		Reporter.log("Total monthly cost without dollar sign on Addons Breakdown Page is " + totalMontlyCost);
		return totalMontlyCost;
	}

	/**
	 * Read Total Monthly Level AddOn cost
	 * 
	 * @return
	 */
	public String checkTotalMonthlyLevelCost() {
		double sum = 0;
		String addedDollerSign = null;
		try {
			String cost = totalMonthlyCost.getText().trim().replace("$", "");
			sum = sum + Double.parseDouble(cost);

			String formatCost = (String.format("%.2f", sum));
			String costInString = String.valueOf(formatCost);

			addedDollerSign = "$" + costInString;
		} catch (Exception e) {
			Assert.fail("Not able to calculate Total Monthly level Addons cost.");
		}
		return addedDollerSign;
	}

	/**
	 * Read Account Level AddOn cost
	 * 
	 * @return
	 */
	public double checkCostAtAccountLevel() {
		double sum = 0.00;
		try {
			Assert.assertTrue(accountSectionText.isDisplayed());
			String cost = accountLevelCost.getText().trim().replace("$", "");
			sum = sum + Double.parseDouble(cost);
		} catch (Exception e) {
			Reporter.log("Not able to calculate Account level Addons cost.");
		}
		return sum;
	}

	/**
	 * Calculate amount at Account Level
	 * 
	 * @return
	 */
	public double checkAndCalculateAmountForEachLine() {
		double sum = 0.00;
		try {
			for (WebElement element : lineLevelCost) {
				String actualCost = element.getText().trim();
				String onlyCostValue;
				onlyCostValue = actualCost.replace("$", "");

				sum = sum + Double.parseDouble(onlyCostValue);
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost under each item.");
		}
		return sum;
	}

	/**
	 * verif DiffBetWeen total AddOns Break down Cost And Cost On Plans Blade
	 * 
	 * @return
	 */
	public void verifyDiffBetWeentotalAddOnsBreakdownCostAndCostOnPlansBlade(String totalAddOnsBreakdownCost,
			String costOnPlansBlade) {
		Assert.assertTrue(totalAddOnsBreakdownCost.contains(costOnPlansBlade),
				"total Add Ons Break down Cost and cost On Plan Blade is not same");
	}
}