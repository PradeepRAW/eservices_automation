/**
 * 
 */
package com.tmobile.eservices.qa.api.soap;

import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.service.SoapService;
import com.tmobile.eservices.qa.api.ApiCommonLib;

import io.restassured.response.Response;

/**
 * @author csudheer
 *
 */
public class AccountMezooServices extends ApiCommonLib {

	public Response getLines(String requestBody) throws Exception {

		Response response = null;
		Reporter.log(soapUrl.get("getLines"));

		System.out.println("   Soup url  " + soapUrl.get("getLines"));
		Reporter.log("Request Type : GET");

		String oprationName = "Get lines";
		try {
			SoapService soapService = new SoapService(requestBody, "", soapUrl.get("getLines"));
			soapService.addHeader("Content-Type", "text/xml");
			response = soapService.callService();
		} catch (Exception e) {
			Assert.fail("<b> " + oprationName + " Exception Response :</b> " + e);
			Reporter.log(" <b>" + oprationName + " Exception Response :</b> ");
			Reporter.log(" " + e);
		}
		return response;
	}

	public Response getSoapUrl(String requestBody, String oprationName, String url) throws Exception {

		Response response = null;
		Reporter.log(soapUrl.get(url));
		Reporter.log("Request Type : soup");

		try {
			SoapService soapService = new SoapService(requestBody, "", soapUrl.get(url));
			soapService.addHeader("Content-Type", "text/xml");
			response = soapService.callService();
		} catch (Exception e) {
			Assert.fail("<b> " + oprationName + " Exception Response :</b> " + e);
			Reporter.log(" <b>" + oprationName + " Exception Response :</b> ");
			Reporter.log(" " + e);
		}
		return response;
	}
}
