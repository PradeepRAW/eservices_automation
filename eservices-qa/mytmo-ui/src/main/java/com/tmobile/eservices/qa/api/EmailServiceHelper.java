
package com.tmobile.eservices.qa.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tmobile.eservices.qa.api.unlockdata.ServiceException;
import com.tmobile.eservices.qa.api.unlockdata.EmailServiceImpl;

/**
 * @author blakshminarayana
 *
 */
public class EmailServiceHelper {
	private static Logger logger = LoggerFactory.getLogger(EmailServiceHelper.class);
	private static final String MIME_TYPE = "multipart/ALTERNATIVE";
	private static final String MIME_TYPE_MIXED = "multipart/MIXED";

	private List<Message> getUnReadMessages(String userName, String password) {
		logger.info("verifyLinkInEmailContent method in EmailServiceHelper");
		List<Message> messagesList = new ArrayList<Message>();
		try {
			Message[] messages = new EmailServiceImpl().readEmail(userName, password);
			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];
				String contentType = message.getContentType();
				String[] contentTypeSpilt = contentType.split(";");
				String mimeType = contentTypeSpilt[0];
				if (StringUtils.isNotEmpty(mimeType)
						&& (mimeType.equalsIgnoreCase(MIME_TYPE) || mimeType.equalsIgnoreCase(MIME_TYPE_MIXED))) {
					messagesList.add(message);
				}
			}
		} catch (MessagingException e) {
			throw new ServiceException("unble to connect email server ", e);
		}
		return messagesList;
	}

	@SuppressWarnings("unused")
	public String getEmailContent(String userName, String password) {
		logger.info("getEmailContent method in EmailServiceHelper");
		List<Message> allmessages = getUnReadMessages(userName, password);
		String mailContent = null;
		try {
			for (int i = 0; i < allmessages.size(); i++) {
				Multipart multipart = (Multipart) allmessages.get(i).getContent();
				BodyPart bodyPart = multipart.getBodyPart(i);
				mailContent = IOUtils.toString(bodyPart.getInputStream());
				break;
			}
		} catch (MessagingException | IOException e) {
			throw new ServiceException("Unble to connect email server ", e);
		}
		return mailContent;
	}
}
