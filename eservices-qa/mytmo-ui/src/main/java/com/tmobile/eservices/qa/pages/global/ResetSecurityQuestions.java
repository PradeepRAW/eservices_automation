package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author blakshminarayana
 *
 */
public class ResetSecurityQuestions extends CommonPage {

	public static final String resetSecurityQuestionsPageUrl = "/oauth2/v1/resetSecurityQuestions";
	public static final String resetSecurityQuestionsPageLoadText = "Answer your security questions";

	@FindBy(css = "div[ng-repeat*='securityQuestions']")
	private WebElement resetSeqText;

	@FindBy(id = "que_0")
	private WebElement seQuestion;

	@FindBy(id = "que_1")
	private WebElement seQuestion1;
	@FindBy(css = "div.clearfix.pt30 .btn.btn-primary.mr30.btn-next")
	private WebElement nextBtn;

	/**
	 * @param webDriver
	 */
	public ResetSecurityQuestions(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * reset Seq Text
	 * 
	 * @return
	 */
	public ResetSecurityQuestions resetSeqText() {
		checkPageIsReady();
		Assert.assertNotNull(resetSeqText.isDisplayed());
		Reporter.log("User is able to select questions and enter answers");
		return this;
	}

	/**
	 * submit Reset Security Questions
	 * 
	 * @param data
	 */
	public ResetSecurityQuestions submitResetSecurityQuestions(MyTmoData data) {
		try {
			sendTextData(seQuestion, data.getSignUpData().getSecurityQuestionAns1());
			sendTextData(seQuestion1, data.getSignUpData().getSecurityQuestionAns2());
			nextBtn.click();
			Reporter.log("Succesfully submitted security answers.");
		} catch (Exception e) {
			Assert.fail("Unable to submit security answers.");
		}

		return this;

	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the Reset Security Question class instance.
	 */
	public ResetSecurityQuestions verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(resetSecurityQuestionsPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Reset Security Question class instance.
	 */
	public ResetSecurityQuestions verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(resetSecurityQuestionsPageUrl);
		return this;
	}

}
