package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

public class RegistrationPage extends CommonPage {

	public RegistrationPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(id = "labs")
	protected WebElement labs;

	@FindBy(id = "keys")
	protected WebElement keys;

	@FindBy(id = "input[placeholder*='MSISDN']")
	protected WebElement msisdn;

	@FindBy(id = "actions")
	protected WebElement actions;

	@FindBy(id = "req")
	protected WebElement goButton;

	@FindBy(css = "div.message")
	protected WebElement successMessage;

	public void selectLab(String lab) {
		selectElementFromDropDown(labs, "Text", lab);
	}

	public void selectData() {
		selectElementFromDropDown(keys, "Text", "msisdn");
	}

	public void setMSISDN(String data) {
		msisdn.sendKeys(data);
	}

	public void selectAction(String action) {
		selectElementFromDropDown(actions, "Text", action);
	}

	public void clickOnGo() {
		goButton.click();
	}

	public boolean verifyUnlockSuccessMessage() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div#loader")));
		waitFor(ExpectedConditions.visibilityOf(successMessage));
		return successMessage.isDisplayed();
	}

	/**
	 * Unlock Account
	 * 
	 * @param myTmoData
	 */
	public void unlockAccount(MyTmoData myTmoData) {
		selectLab("lab14");
		selectData();
		setMSISDN(myTmoData.getLoginEmailOrPhone());
		selectAction("unlock");
		clickOnGo();
	}

	public String getPIN(MyTmoData myTmoData) {
		selectLab("lab11");
		selectData();
		setMSISDN(myTmoData.getLoginEmailOrPhone());
		selectAction("pin to msisdn");
		clickOnGo();
		waitFor(ExpectedConditions.visibilityOf(successMessage));
		return successMessage.getText();
	}
}
