package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;
/**
 * @author rnallamilli
 *
 */
public class PortInPage extends CommonPage {
		
	// Locators for Port-In Confirmation Page

	@FindBy(xpath = "//img[@alt='checkIcon']")
	private WebElement confStatus;

	@FindBy(xpath = "//h1[@class='Display2 padding-vertical-small']")
	private WebElement confHeader;
	
	@FindBy(xpath = "//div[@class='body text-container']")
	private WebElement confSubHeader;

	@FindBy(xpath = "//button[@id='submitCTA']")
	private WebElement confButton;
	
	// Locators for Port-In Input Page
	
	@FindBy(xpath = "//img[@alt='checkIcon']")
	private WebElement inputStatus;

	@FindBy(xpath = "//h1[@class='Display2 padding-vertical-small']")
	private WebElement inputHeader;
	
	@FindBy(xpath = "//div[@class='body text-container']")
	private WebElement inputSubHeader;

	@FindBy(xpath = "//input[@id='mat-input-0']")
	private WebElement inputPhoneNo;

	@FindBy(xpath = "//strong[contains(text(),'Type a unique phoneNumber.')]")
	private WebElement inputHelperText;

	@FindBy(xpath = "//div[@class='padding-bottom-xl padding-top-small']")
	private WebElement inputSubmitButton;
	
	@FindBy(xpath = "//span[contains(text(),'Error: Please enter number in correct format')]")
	private WebElement inputPhoneError;

	/**
	 * @param webDriver
	 */
	public PortInPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	//   ------------------------------------------------------- Port-In Confirmation Screen Verification --------------------------------
	
	/**
	 * 1. Verify Port-In Confirmation Page should be displayed:
	 */
	public PortInPage verifyConfirmPage() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("port-in/confirmation"));
			Reporter.log("1. PASS - Portin Confirmation page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("1. FAIL - Portin Confirmation page not displayed"+e.getMessage());
		}
		return this;
	}
	
	/**
	 * 2. Verify Confirmation Status should be displayed:
	 */
	public PortInPage verifyConfStatus() {
		if (getDriver() instanceof AppiumDriver) {
			try {
				checkPageIsReady();
				confStatus.isDisplayed();
				Reporter.log("2. PASS - Confirmation Status displayed in Port-In Confirmation Page");
			} catch (Exception e) {
				Assert.fail("2. FAIL - Confirmation Status not displayed in Port-In Confirmation Page"+e.getMessage());
			}
		}
		if (getDriver() instanceof ChromeDriver) {
		try {
			confStatus.isDisplayed();
			Reporter.log("2. PASS - Confirmation Status displayed in Port-In Confirmation Page");
		} catch (Exception e) {
			Assert.fail("2. FAIL - Confirmation Status not displayed in Port-In Confirmation Page"+e.getMessage());
		}
		}
		return this;
	}
	/**
	 * 3. Verify Confirmation Header - 'Thanks' Message should be displayed:
	 */
	public PortInPage verifyConfHeader() {
		try {
			Assert.assertEquals(confHeader.getText(), Constants.PORTIN_CONF_HEADER);
			Reporter.log("3. PASS -" + confHeader.getText() + "message displayed in Port-In Confirmation Page");
		} catch (Exception e) {
			Assert.fail("3. FAIL -" + Constants.PORTIN_CONF_HEADER + "message not displayed in Port-In Confirmation Page"+e.getMessage());
		}
		return this;
	}
	/**
	 * 4. Verify Confirmation Sub-Header Message should be displayed:
	 */
	public PortInPage verifyConfSubHeader() {
		try {
			Assert.assertEquals(confSubHeader.getText(), Constants.PORTIN_CONF_SUBHEADER);
			Reporter.log("4. PASS -" + confSubHeader.getText() +"message displayed in Port-In Confirmation Page");
		} catch (Exception e) {
			Assert.fail("4. FAIL -" + Constants.PORTIN_CONF_SUBHEADER +"message not displayed in Port-In Confirmation Page"+e.getMessage());
		}
		return this;
	}
	/**
	 * 5. Verify Confirmation 'All Done' Button Is Displayed on page load:
	 * 
	 * @return
	 */
	public PortInPage verifyAllDoneButton() {
		try {
			confButton.isDisplayed();
			Reporter.log("5. PASS - 'All Done' Button is displayed on page load ");
		} catch (Exception e) {
			Assert.fail("5. FAIL - 'All Done' Button is not displayed on page load");
		}
		return this;
	}
	/**
	 * 6. Verify Confirmation 'All Done' Button Is Enabled on page load:
	 * 
	 * @return
	 */
	public PortInPage verifyAllDoneButtonEnable() {
		try {
			confButton.isEnabled();
			Reporter.log("6. PASS - 'All Done' Button is enabled on page load ");
		} catch (Exception e) {
			Assert.fail("6. FAIL - 'All Done' Button is disabled on page load");
		}
		return this;
	}
	/**
	 * 7. Verify font style of 'Thanks' Message in Confirm Page
	 */
	public PortInPage readHeaderFontProperty() {
		try {
			Reporter.log("7. "+ confHeader.getText() + " Header Font Style:");
			  //Read font-size property and print It In console.
			  String fontSize = confHeader.getCssValue("font-size");
			  Reporter.log("Font Size -> "+fontSize);
			  
			  //Read color property and print It In console.
			  String fontColor = confHeader.getCssValue("color");
			  Reporter.log("Font Color -> "+fontColor);
			  
			  //Read font-family property and print It In console.
			  String fontFamily = confHeader.getCssValue("font-family");
			  Reporter.log("Font Family -> "+fontFamily);
			  
			  //Read text-align property and print It In console.
			  String fonttxtAlign = confHeader.getCssValue("text-align");
			  Reporter.log("Font Text Alignment -> "+fonttxtAlign);
			  
				Reporter.log("8. "+ confSubHeader.getText() + " SubHeader Font Style:");
				  //Read font-size property and print It In console.
				  String fontSize1 = confSubHeader.getCssValue("font-size");
				  Reporter.log("Font Size -> "+fontSize1);
				  
				  //Read color property and print It In console.
				  String fontColor1 = confSubHeader.getCssValue("color");
				  Reporter.log("Font Color -> "+fontColor1);
				  
				  //Read font-family property and print It In console.
				  String fontFamily1 = confSubHeader.getCssValue("font-family");
				  Reporter.log("Font Family -> "+fontFamily1);
				  
				  //Read text-align property and print It In console.
				  String fonttxtAlign1 = confSubHeader.getCssValue("text-align");
				  Reporter.log("Font Text Alignment -> "+fonttxtAlign1);
			} catch (Exception e) {
			Reporter.log("Font Style not recognized"+e.getMessage());
		}
		return this;
	}
	
	//   ------------------------------------------------------- Port-In Phone Number Input Screen Verification --------------------------------
	/**
	 * 1. Verify Port-In Phone Number Input Page is Displayed:
	 */
	public PortInPage verifyInputPage() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("port-in/input"));
			Reporter.log("1. PASS - Port-In Phone Number Input page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("1. FAIL - Port-In Phone Number Input page not displayed"+e.getMessage());
		}
		return this;
	}
	
	/**
	 * 2. Verify Phone Number Input Page Status is Displayed:
	 */
	public PortInPage verifyInputPageStatus() {
		try {
			confStatus.isDisplayed();
			Reporter.log("2. PASS - Input Page Status displayed in Port-In Phone Number Input Page");
		} catch (Exception e) {
			Assert.fail("2. FAIL - Input Page Status not displayed in Port-In Phone Number Input Page"+e.getMessage());
		}
		return this;
	}
	/**
	 * 3. Verify Phone Number Input Page Header - 'Thanks' Message is Displayed:
	 */
	public PortInPage verifyInputPageHeader() {
		try {
			Assert.assertEquals(confHeader.getText(), Constants.PORTIN_INPUT_HEADER);
			Reporter.log("3. PASS - '" + confHeader.getText() + "'message displayed in Port-In Phone Number Input Page");
		} catch (Exception e) {
			Assert.fail("3. FAIL -'" + Constants.PORTIN_INPUT_HEADER + "'message not displayed in Port-In Phone Number Input Page"+e.getMessage());
		}
		return this;
	}
	/**
	 * 4. Verify Phone Number Input Page Sub-Header Message is Displayed:
	 */
	public PortInPage verifyInputPageSubHeader() {
		try {
			Assert.assertEquals(confSubHeader.getText(), Constants.PORTIN_INPUT_SUBHEADER);
			Reporter.log("4. PASS -'" + confSubHeader.getText() + "'message displayed");
		} catch (Exception e) {
			Assert.fail("4. FAIL -'" + Constants.PORTIN_INPUT_SUBHEADER + "'message not displayed"+e.getMessage());
		}
		return this;
	}
	/**
	 * 5. This method is to verify Phone Number is displayed in Port-In Input Page
	 */
	public PortInPage verifyPhoneNumberTextField() {
		try {
			inputPhoneNo.isDisplayed();
			Reporter.log("5. PASS - Phone Number field displayed in Port-In Phone Number Input Page");
		} catch (Exception e) {
			Assert.fail("5. FAIL -Phone Number field not displayed in Port-In Phone Number Input Page"+e.getMessage());
		}
		return this;
	}
	/**
	 * 6. Verify Helper Text Message in Port-In Input Page
	 */
	public PortInPage verifyInputHelper() {
		try {
			Assert.assertEquals(inputHelperText.getText(), Constants.PORTIN_INPUT_HELPER);
			Reporter.log("6. PASS '"+ inputHelperText.getText() +"' Helper Text is displayed in Port-In Phone Input Page");
		} catch (Exception e) {
			Assert.fail("6. FAIL '"+ Constants.PORTIN_INPUT_HELPER +"' Helper Text is not displayed in Port-In Phone Input Page"+e.getMessage());
		}
		return this;
	}
	/**
	 * 7. Verify PortIn Input Screen Submit Button is Disabled:
	 * 
	 * @return
	 */
	public PortInPage verifySubmitButtonIsDisplayedonpageload() {
		try {
			inputSubmitButton.isDisplayed();
			Reporter.log("7. PASS - 'Submit' Button is displayed");
		} catch (Exception e) {
			Assert.fail("7. FAIL - 'Submit' Button is not displayed");
		}
		return this;
	}
	/**
	 * 8. Verify PortIn Input Screen Submit Button Is Disabled on page load
	 * 
	 * @return
	 */
	public PortInPage verifySubmitButtonIsDisabledonpageload() {
		try {
			if (inputSubmitButton.isEnabled()) {
				Reporter.log("8. PASS - 'Submit' Button is disabled on pageload");
			}
		} catch (Exception e) {
			Assert.fail("8. FAIL - 'Submit' Button is enabled on pageload" + e.toString());
		}
		return this;
	}
	/**
	 * 12. Validate invalid Phone Number error message in Port-In Input Page
	 */
	public PortInPage verifyInvalidPhoneError() {
		try {
			sendTextData(inputPhoneNo, Constants.PORTIN_INPUT_PHONE_ERROR);
			inputPhoneNo.sendKeys(Keys.TAB);
			if (inputPhoneError.isDisplayed()) {
				Reporter.log("9. PASS -'Error: Please enter number in correct format' message displayed for invalid phone number");
			}
			else
				Assert.fail("9. FAIL - 'Error: Please enter number in correct format' message not displayed for invalid phone number");	
		} catch (Exception e) {
			Assert.fail("9. FAIL - 'Error: Please enter number in correct format' message not displayed for invalid phone number"+e.getMessage());
		}
		return this;
	}
	/**
	 * 9. Enter Phone Number in Port-In Input Page
	 */
	public PortInPage enterPhoneNumber() {
		try {
			sendTextData(inputPhoneNo, Constants.PORTIN_INPUT_PHONE_ENTER);
			inputPhoneNo.sendKeys(Keys.TAB);
			Reporter.log("10. PASS -'"+ Constants.PORTIN_INPUT_PHONE_ENTER +"' Phone Number entered successfully.");
		} catch (Exception e) {
			Assert.fail("10. FAIL '"+ Constants.PORTIN_INPUT_PHONE_ENTER +"'- Unable to to enter Phone Number."+e.getMessage());
		}
		return this;
	}
	/**
	 * 11. Validate the Phone Number in Port-In Input Page
	 */
	public PortInPage validPhoneNumber() {
		try {
			sendTextData(inputPhoneNo, Constants.PORTIN_INPUT_PHONE_ENTER);
			inputPhoneNo.sendKeys(Keys.TAB);
			Assert.assertEquals(inputPhoneNo.getAttribute("value").length(), 16);
			Reporter.log("11. PASS - '" + Constants.PORTIN_INPUT_PHONE_ENTER + "' Valid Phone Number entered.");
		} catch (Exception e) {
			Assert.fail("11. FAIL - '" + Constants.PORTIN_INPUT_PHONE_ENTER + "' Invalid phone number entered.");
		}
		return this;
	}
	/**
	 * 10. Verify PortIn Input Screen Submit Button Is Enabled by enter the phone number
	 * 
	 * @return
	 */
	public PortInPage verifySubmitButtonIsEnabledByEnterPhoneNumber() {
		try {
			if (inputSubmitButton.isEnabled()) {
				Reporter.log("12. PASS - Submit Button is enabled by enter the phone number");
				inputSubmitButton.click();
				Reporter.log("Clicked on Submit Button ");
			}
			else
			{
				Assert.fail("12. FAIL - Submit Button is diabled by enter the phone number");	
			}
		} catch (Exception e) {
			Assert.fail("Click on submit button failed" + e.toString());
		}
		return this;
	}
	
	/**
	 * Verify portin page
	 */
	public PortInPage verifyPortInPage() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("portin"));
			Reporter.log("Portin page is displayed");
		} catch (AssertionError e) {
			Reporter.log("Portin page not displayed");
			Assert.fail("Portin page not displayed");
		}
		return this;
	}

}