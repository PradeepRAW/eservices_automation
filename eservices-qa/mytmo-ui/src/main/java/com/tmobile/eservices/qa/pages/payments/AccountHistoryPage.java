package com.tmobile.eservices.qa.pages.payments;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;

import io.appium.java_client.AppiumDriver;

/**
 * @author ksrinivas
 *
 */
public class AccountHistoryPage extends CommonPage {

	HashMap<String, String> paDetails = new HashMap<>();
	
	@FindBy(css = ".icon-account-history")
	private WebElement accountHistoryIcon;

	@FindBy(css = "div.row-fluid.alertAlign.hide.view-all-alerts div span a")
	private WebElement aaPaccountHistoryIconMobile;

	@FindBy(css = ".span3.acc-history-heading .ui_headline")
	private WebElement accountHistoryHeader;

	@FindBy(css = "div.pagetitle_txt")
	private WebElement accountHistoryHeaderMobile;

	@FindAll({ @FindBy(css = "div.span3.acc-history-heading span.ui_headline"),
			@FindBy(css = "div[class*='maaccounthistoryattribute']") })
	private WebElement accountHistoryHeaderalertActivity;

	@FindAll({ @FindBy(id = "documentsTab"), @FindBy(xpath = "//div[contains(text(),'Documents')]") })
	private WebElement documentsTab;

	@FindBy(id = "Filter")
	private WebElement accountHistoryToggleMobile;

	@FindBy(id = "calendar_cycle")
	private WebElement datesDropdown;

	@FindAll({@FindBy(id = "noRecordsMsgTr"),@FindBy(css="div.valign.padding-top-large.padding-bottom-xl.border-top")})
	private List<WebElement> noRecordsAccHistory;

	private By accHistorySpinner = By.id("di_loaderImage");

	@FindBy(css = "p.ui_darkbody_bold.type")
	private List<WebElement> accHistoryRows;

	@FindBy(css = "#activityRow0 #date0")
	private WebElement accHistory;

	@FindBy(xpath = "//a[text()=' Billing ']")
	private WebElement billingCategory;

	@FindBy(xpath = "//p[contains(text(),'Start date')]")
	private WebElement customOptions;

	@FindAll({ @FindBy(xpath = "//a/span[contains(text(),'payment method')]"),
			@FindBy(xpath = "//div[@class='TMO-ALERTS-COMPONENT'] // span[contains(text(),'Payment Arrangement')]") })
	private WebElement addPaymentMethod;
	
	@FindBy(css = "div.TMO-SCHEDULED-ACTIVITY-PAGE a[href*='arrangement']")
	private WebElement addPaymentMethodPAScheduledActivity;

	@FindBy(css = "#di_alrtmssg .ui_secondary_link")
	private WebElement viewArrangementDetails;

	private By accActivityLoaderMobile = By.id("accountActivityLoader");

	@FindBy(css = "span.ui_mobile_primary_link img.DropArwPadding")
	private WebElement accHistoryDropDownMobile;

	@FindBy(css = "#Filter-dropdown")
	private WebElement accHistoryLinksDivMobile;

	@FindBy(css = "#Filter-dropdown ul li a")
	private List<WebElement> accHistoryLinksMobile;

	@FindBy(css = "#paymentTab a")
	private WebElement paymentsTab;

	@FindBy(linkText = "Download PDF")
	private WebElement downloadPDF;

	@FindBy(linkText = "Download payment receipt")
	private WebElement printAllPayments;

	@FindBy(css = "span[ng-model='selectedLine']")
	private WebElement selectedLine;

	@FindBys({ @FindBy(css = ".col-sm-12 ul.dropdown-menu li") })
	private List<WebElement> allLines;

	@FindBy(css = ".col-lg-2.body")
	private List<WebElement> linesAccHistory;
	
	/* Element to identify p tag //span[contains(@class,'icon-position')]//parent::p  */	
	@FindBy(css = "div.col-9.col-lg-5.pr-xs-0 p.body.bold")
	private List<WebElement> payemntTypes;
	
	@FindBy(css = "div.TMO-SCHEDULED-ACTIVITY-PAGE .pl-1")
	private List<WebElement> paymentScheduledActivity;
	
	@FindBy(xpath = "//p[contains(text(),'Payment Arrangement')]//..//..//span[contains(@class,'body-copy')]")
	private List<WebElement> paymentArrangementDetails;
	
	@FindBy(xpath = "//p[contains(text(),'Payment Arrangement')]//..//..//..//span[contains(@class,'pl-1')]")
	private WebElement paymentArrangementPaymentMethod;
	
	@FindBy(css = ".pr-3 #model")
	private WebElement categoryDropdown;

	@FindBy(css = ".pr-3 li")
	private List<WebElement> dropdowncategories;

	@FindBy(css = ".float-left.body")
	private List<WebElement> categoryAccHistory;
	
	@FindBy(css = "p.H6-heading.bold")
	private List<WebElement> activityAccHistory;
	
	@FindBy(css = "div#model span[ng-model='selectedVal']")
	private List<WebElement> documentsOption;

	@FindBy(xpath = "//p[contains(text(), 'New feature added - iPhone Upgrade Program')]")
	private WebElement jumpRecord;

	@FindBy(xpath = "/p[contains(text(), 'New feature added - iPhone Upgrade Program')]//..//..//div//p[@class='mb20']")
	private WebElement jumpRecordInfo;

	@FindBy(css = "a[href*='iPhoneUpgradeProgram']")
	private WebElement jumpRecordTermsAndConditionsLink;

	@FindBy(xpath = "//span[contains(text(), 'Last 90 days')]/ancestor::div[@id='model']")
	private WebElement dateDropdown;

	@FindBys({
			@FindBy(xpath = "//span[contains(text(), 'Last 90 days')]/ancestor::div[@class='d-inline-block dropdown']//a") })
	private List<WebElement> dateDropdownValues;

	@FindBy(css = "div.TMO-STYLEGUIDE-DATE-PICKER-ACCOUNT-ACTIVITY-PAGE")
	private List<WebElement> datePicker;

	// @FindBy(css = "span[ng-model='selectedVal']")
	// private List<WebElement> dateFilterDefaultOption;
	@FindBy(css = ".col-6.col-sm-6.col-md-4.pl-2.pr-2 span[ng-model='selectedVal']")
	private WebElement dateFilterDefaultOption;

	@FindBy(css = ".fa.fa-calendar")
	private List<WebElement> datePickerCalendar;

	@FindBy(css = ".pr-3 .fa-calendar")
	private WebElement datePickerCalendarStartDate;

	@FindBy(css = ".pr-2 .fa-calendar")
	private WebElement datePickerCalendarEndDate;

	@FindBy(css = ".animation-out.animation-in")
	private WebElement calendarModal;

	@FindBy(css = ".animation-out.animation-in button.PrimaryCTA-accent")
	private WebElement caledarSelectCTA;

	@FindBy(css = "div.row.padding-vertical-large-lg")
	private List<WebElement> totalRecords;

	@FindBy(css = "ul.pagination.justify-content-center")
	private WebElement pagiNation;

	@FindBy(xpath = "//p[contains(text(), 'New bill available')]//..//..//div//a")
	private List<WebElement> downloadPDFLinksForBriteBill;

	@FindBy(xpath = "//a[contains(text(), 'Download PDF')]")
	private List<WebElement> downLoadPDFLink;

	@FindBy(css = ".col-9.col-lg-5")
	private List<WebElement> activityRecord;

	@FindBy(css = "div.loader")
	private WebElement pageSpinner;

	@FindBy(css = "div.TMO-SCHEDULED-ACTIVITY-PAGE")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//a[contains(text(), 'View Bill')]")
	private WebElement viewBillLink;

	private static final String PAGE_URL = "/accounthistory";

	private static final String TITLE = "Account History";

	@FindBy(css = "ul#activityTabs li a[data-toggle='tab']")
	private List<WebElement> activityTabs;

	@FindBy(id = "logout-Ok")
	private WebElement logOutBtn;

	@FindBy(xpath = "//p[contains(@class,'heading bold')]")
	private List<WebElement> activityHeaders;
	
	@FindBy(xpath = "//a[@aria-label='Next' and @class='page-lin']")
	private List<WebElement> enablenext;
	
	
	@FindBy(xpath = "//a[contains(text(),'All (24 months)')]")
	private WebElement all24months;
	
	private By accActivityspinner = By.xpath("//div[@class='accountActivityLoader']");
	
	@FindBy(xpath = "//div[contains(text(),'Payment')][contains(@class,'float-left body')]//..//../div//a")
	private List<WebElement> printPaymentReceiptLinks;
	
	@FindBy(css = "div.TMO-SCHEDULED-ACTIVITY-PAGE a.legal-link")
	private WebElement cancelFDP;
	
	@FindBy(css = "div.dialog.animation-out")
	private WebElement cancelFDPConfirmationModal;
	
	@FindBy(xpath = "//button[contains(text(),'Cancel payment')]")
	private WebElement cancelPayment;
	
	@FindBy(xpath = "//span[contains(text(),'Update Successful')]")
	private WebElement CancelFDPConfirmationModal;
	
	@FindBy(css = "span.body.word-break")
	private WebElement suspendedLineAlert;


	/**
	 * @param webDriver
	 */
	public AccountHistoryPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AccountHistoryPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(PAGE_URL));
		return this;
	}

	/**
	 * Verify that current page title matches the expected title.
	 *
	 * @return instance of the current object
	 */
	public AccountHistoryPage verifyPageTitle() {
		waitFor(ExpectedConditions.titleContains(TITLE));
		return this;
	}

	public AccountHistoryPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			// waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageTitle();
			verifyPageUrl();
			Reporter.log("Account history page is displayed");
		} catch (Exception e) {
			Assert.fail("Account history page not displayed");
		}
		return this;
	}

	/**
	 * to click account history toogle on mobile
	 */
	public AccountHistoryPage clickAccountHistoryToggleMobile() {
		try {
			
			accountHistoryToggleMobile.click();
			Reporter.log("Clicked on Account History Toggle Mobile");
		} catch (Exception e) {
			Assert.fail("Fail to click on Account History Toggle Mobile");
		}
		return this;
	}

	/**
	 * Click Account History Icon
	 */
	public AccountHistoryPage clickAccountHistoryicon() {
		if (getDriver() instanceof AppiumDriver) {
			aaPaccountHistoryIconMobile.click();
			Reporter.log("Clicked on Pa Account History Icon Mobile");
		} else {
			accountHistoryIcon.click();
			Reporter.log("clicked on Account History Icon");
		}
		return this;
	}

	/**
	 * Get a Text of Account History Header
	 *
	 * @return
	 */
	public AccountHistoryPage getAccountHistoryHeaderText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(accountHistoryHeader));
			Reporter.log("Account history page verified");
		} catch (Exception e) {
			Assert.fail("Account history header not found");
		}
		return this;
	}

	/**
	 * Get Text for Account History in Alert and activity Page
	 *
	 * @return
	 */
	public AccountHistoryPage verifyAccountHistoryHeaderTextalertAcitivty(String msg) {
		try {
			accountHistoryHeaderalertActivity.getText().equals(msg);
			Reporter.log("Account history section is loaded");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Account History section text alert ");
		}
		return this;
	}

	/**
	 *
	 * @return
	 */
	public AccountHistoryPage verifyDocumentsTabDisplayed() {
		try {
			documentsTab.isDisplayed();
			Reporter.log("Document tab is displayed");
		} catch (Exception e) {
			
			Assert.fail("Failed to display the Documents tab");
		}
		return this;
	}

	/**
	 * Select dates range by visible text
	 */
	public AccountHistoryPage selectDatesRange() {
		try {
			selectElementFromDropDown(datesDropdown, Constants.SELECT_BY_TEXT, "Custom dates");
			Reporter.log("Selected the Dates Range");

		} catch (Exception e) {
			Assert.fail("Fail to select the Dates Range");
		}
		return this;

	}

	/**
	 * verify Account History
	 * 
	 * @return boolean
	 */
	public AccountHistoryPage verifyAccHistory() {
		boolean isDisplayed;
		String noRecordsDis = StringUtils.EMPTY;
		checkPageIsReady();
		waitFor(ExpectedConditions.invisibilityOfElementLocated(accHistorySpinner));
		if (!noRecordsAccHistory.isEmpty()) {
		noRecordsDis = noRecordsAccHistory.get(0).getAttribute("style");
		}
		if (!noRecordsDis.isEmpty() && noRecordsDis.contains("none")) {
			isDisplayed = accHistory.isDisplayed();
		} else {
			isDisplayed = true;
		}
		Assert.assertTrue(isDisplayed);
		Reporter.log("Verified Account history with custom dates.");
		return this;
	}

	/**
	 * verify Account History
	 * 
	 * @return boolean
	 */
	public AccountHistoryPage verifyCustomOptionsDispalyed() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(accHistorySpinner));
			customOptions.isDisplayed();

		} catch (Exception e) {
			
			Assert.fail("Custom dates section is not displayed");
		}
		return this;
	}

	/**
	 * Click Add payment method
	 */
	public AccountHistoryPage clickAddpaymentmethod() {
		try {
			checkPageIsReady();
			//need to verify scheduled pa alert
			clickElementWithJavaScript(addPaymentMethod);
			Reporter.log("Clicked on Add payment method");
		} catch (Exception e) {
			Assert.fail("Payment arrangement not scheduled for this MSDSIN ,please try with another MSDSIN");
		}
		return this;

	}
	
	/**
	 * Click Add payment method link of PA under Scheduled Activity
	 */
	public AccountHistoryPage clickAddpaymentmethodUnderScheduledActivity() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(addPaymentMethodPAScheduledActivity);
			Reporter.log("Clicked on Add payment method link of PA under Scheduled Activity seciton");
		} catch (Exception e) {
			Assert.fail("Payment arrangement not scheduled for this MSDSIN ,please try with another MSDSIN");
		}
		return this;

	}

	/**
	 * Click View arrangement details
	 */
	public void clickViewArrangementDetails() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(viewArrangementDetails);
			Reporter.log("Clicked on View Arrangement Details");
		} catch (Exception e) {
			Assert.fail("Failed to click on View Arrangement Details or This MSDSIN is not scheduled with Payment Arrangement");
		}
	}

	/**
	 * click on payments tab of account history
	 */
	public AccountHistoryPage clickPaymentTab() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.invisibilityOfElementLocated(accActivityLoaderMobile));
				accHistoryDropDownMobile.click();
				waitFor(ExpectedConditions.visibilityOf(accHistoryLinksDivMobile));
				clickElementBytext(accHistoryLinksMobile, Constants.PAYMENTS_LINK);
				Reporter.log(
						"Its an AppiumDriver instance clicked on Account History Dropdown and Account History Link Mobile");
			} else {
				waitFor(ExpectedConditions.invisibilityOfElementLocated(accHistorySpinner));
				paymentsTab.click();
				Reporter.log("Clicked on payment tab ");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Payment Tab");
		}
		return this;
	}

	/**
	 * verify payment id is displayed for open text + samson records
	 * 
	 * @param activityType
	 * @return
	 */
	public AccountHistoryPage verifyPaymentIDforNewPayments(String activityType) {
		try {
			String actitivyRowID;
			waitFor(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("p.ui_darkbody_bold.type")));
			for (WebElement activityRow : accHistoryRows) {
				if (activityRow.getText().contains(activityType)) {
					actitivyRowID = activityRow.getAttribute("id").replaceAll("[^0-9 ]", "");
					WebElement paymentLabel = getDriver().findElement(
							By.cssSelector("p[id*='quoteIdLabel" + actitivyRowID + "'] span.ui_body_bold"));
					paymentLabel.isDisplayed();
					break;
				}
			}
			Reporter.log("Verified the payment ID for New payments");
		} catch (Exception e) {
			Assert.fail("'Payment #' is not displayed for OpenText Payment Arrangement Receipts in Account History.");
		}
		return this;
	}

	/**
	 * Click Download PDF
	 * 
	 * @return
	 */
	public AccountHistoryPage clickDownLoadPDF() {
		try {
			downloadPDF.click();
			Reporter.log("Clicked on download PDF");
		} catch (Exception e) {
			Assert.fail("Download PDF is not displayed");
		}
		return this;
	}

	/**
	 * Verify Print All Payments Link is present
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyPrintAllPaymentsLink() {
		try {
			if (!(getDriver() instanceof AppiumDriver)) {
				waitFor(ExpectedConditions.visibilityOf(printAllPayments));
				Assert.assertTrue(printAllPayments.isDisplayed(), "Print All link is not present");
				Assert.assertTrue(printAllPayments.getText().equals("Download payment receipt"),
						"Print All link text is not correct. Should be Print All Payments");
				Reporter.log("Print All Payments link is present and link text is correct");
			}
		} catch (Exception e) {
			Assert.fail("Print all payments link is not displayed");
		}
		return this;
	}

	/**
	 * Click Print All Payments Link
	 * 
	 * @return
	 */
	public AccountHistoryPage clickPrintAllPaymentsLink() {
		try {
			printAllPayments.click();
			Reporter.log("Clicked on print all payments");
		} catch (Exception e) {
			Assert.fail("Print all payments link is not displayed");
		}
		return this;
	}

	/**
	 * Verify msisdn is present in Account (all line) dropdown
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyMsisdnInAccountDropDown(MyTmoData myTmoData) {
		try {
			selectedLine.click();
			boolean msisdnIsPresent = false;
			for (WebElement webElement : allLines) {
				if (webElement.getText().replaceAll("[^0-9]+", "").contains(myTmoData.getLoginEmailOrPhone())) {
					Reporter.log("MSISDN is present in the Line Selector");
					msisdnIsPresent = true;
					break;
				}
			}
			Assert.assertTrue(msisdnIsPresent, "MSISDN is not present in the Line Selector");
		} catch (Exception e) {
			Assert.fail("MSISDN is not present in the Line Selector");
		}
		return this;
	}

	/**
	 * Select A Line From All Lines Drop-down
	 * 
	 * @return
	 */
	public AccountHistoryPage selectALineFromDropDown(MyTmoData myTmoData) {
		try {
			selectedLine.click();
			checkPageIsReady();
			for (WebElement webElement : allLines) {
				if (webElement.getText().replaceAll("[^0-9]+", "").contains(myTmoData.getLoginEmailOrPhone())) {
					webElement.click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Fail to select MSISDN from dropdown");
		}
		return this;
	}

	/**
	 * Verify Selected Line In Account History
	 *
	 * @return instance of this page
	 */
	public AccountHistoryPage verifySelectedLineInAccHistory() {
		try {
			checkPageIsReady();
			int count =0;
			if (!(getDriver() instanceof AppiumDriver)) {
				checkPageIsReady();
				String lineString = selectedLine.getText().substring(selectedLine.getText().length() - 14);	
				for (WebElement webElement : linesAccHistory) {
				
					if (webElement.getText().contains(lineString)) {
						count =1;
						break;
					}
				}
			
				if(count==0) {
					Assert.fail("Selected line is not displayed");
				}
			}
			
			
		} catch (Exception e) {
			Assert.fail("Selected line is not displayed");
		}
		return this;
	}

	/**
	 * Click on Date Filter dropdown
	 * 
	 * @return
	 */
	public AccountHistoryPage clickOnCategoryFilterDropdown() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(categoryDropdown));
			categoryDropdown.click();
		} catch (Exception e) {
			Assert.fail("Failed to click on Category Filter Dropdown");
		}
		return this;
	}
	

	/**
	 * Select Category From Drop-down
	 * 
	 * @param categoryType
	 * @return
	 */
	public AccountHistoryPage selectCategoryDropdown(String categoryType) {
		try {
			clickOnCategoryFilterDropdown();
			for (WebElement category : dropdowncategories) {
				if (category.getText().equals(categoryType)) {
					waitFor(ExpectedConditions.visibilityOf(dropdowncategories.get(0)));
					clickElementWithJavaScript(category);
					waitFor(ExpectedConditions.invisibilityOfElementLocated(accActivityspinner));
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Category " + categoryType + " is not displayed");
		}
		return this;
	}

	/**
	 * Verify Category In Drop-down
	 * 
	 * @param categoryType
	 * @return
	 */
	public AccountHistoryPage verifyCategoryPresentInDropdown(String categoryType) {
		try {
			boolean isSelectedCategoryDispalyed = false;
			for (WebElement category : dropdowncategories) {
				if (category.getText().equals(categoryType)) {
					isSelectedCategoryDispalyed = true;
				}
			}
	
			Verify.assertTrue(isSelectedCategoryDispalyed,
					"Selected category " + categoryType + " is not present in dropdown");
			Reporter.log("Selected category " + categoryType + " is present in dropdown");
		} catch (Exception e) {
			
			Verify.fail("Category " + categoryType + " is not displayed");
		}
		return this;
	}

	/**
	 * Verify Selected Category In Account History
	 * 
	 * @param categoryType
	 * @return boolean
	 */
	public AccountHistoryPage verifySelectedCategoryInAccHistory(String categoryType) {
		boolean isSelectedCategoryDispalyed = false;
		try {
			if (!(getDriver() instanceof AppiumDriver)) {
				if (!categoryAccHistory.isEmpty()) {
					for (WebElement line : categoryAccHistory) {
						if (line.getText().equals(categoryType)) {
							isSelectedCategoryDispalyed = true;
						}
					}
					Verify.assertTrue(isSelectedCategoryDispalyed,
							"Selected category " + categoryType + " is not displayed");
					Reporter.log("Selected category " + categoryType + " is displayed");
				} else {
					Reporter.log("Selected category " + categoryType + " is empty");
				}
			}
		} catch (Exception e) {
			Verify.fail("Selected category is not displayed");
		}
		return this;
	}

	/**
	 * verify PA confirmation record in account history Payments filter and category as Documents
	 * @param activityHeader
	 * @param categoryType
	 * @return
	 */
	public AccountHistoryPage verifyPAConfirmationInAccountHistoryActivity(String activityHeader, String categoryType) {
		try {
			if (!(getDriver() instanceof AppiumDriver)) {
				if (!categoryAccHistory.isEmpty()) {
					for (int i = 0; i < activityAccHistory.size(); i++) {
						if (activityAccHistory.get(i).getText().equals(activityHeader)) {
							Assert.assertTrue(categoryAccHistory.get(i).getText().equals(categoryType), "Expected: "+ categoryType +" Actual: "+categoryAccHistory.get(i).getText());
							break;
						}
					}
					}
					Reporter.log("Selected category: " + categoryType + " is displayed");
					Reporter.log("Activity Header:  " + activityHeader + " is displayed");
			}
		} catch (Exception e) {
			Verify.fail("PA Confirmation In Account History Activity is not displayed");
		}
		return this;
	}
	
	/**
	 * Verify Line Selector Account Title
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyLineSelectorAccountTitle() {
		try {
			checkPageIsReady();
			Verify.assertEquals(selectedLine.getText(), "Account (all lines)");
			Reporter.log("Line selector account title 'Account (all lines) is displayed'");
		} catch (Exception e) {
			Verify.fail("Line selector account title 'Account (all lines) is not displayed");
		}
		return this;
	}

	/**
	 * Click On Jump Record
	 * 
	 * @return
	 */
	public AccountHistoryPage clickOnJumpRecord() {
		try {
			jumpRecord.click();
		} catch (Exception e) {
		
			Assert.fail("Jump record is not displayed");
		}
		return this;
	}

	/**
	 * Verify Jump Record Info
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyJumpRecordInfo() {
		try {
			Assert.assertEquals(jumpRecordInfo.getText(), "iPhone Upgrade Program");
			Reporter.log("iPhone Upgrade Program record is displayed");
		} catch (Exception e) {
			Assert.fail("Iphone upgrade program record is not displayed");
		}
		return this;
	}

	/**
	 * Verify Jump Record Terms & Conditions Link
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyJumpRecordTermsAndConditionsLink() {
		try {
			jumpRecordTermsAndConditionsLink.isDisplayed();
			Reporter.log("Jump record terms and conditions link is displayed");
		} catch (Exception e) {
			Assert.fail("Jump record terms and conditions link is not displayed");
		}
		return this;
	}

	/**
	 * Click on Date Filter dropdown
	 * 
	 * @return
	 */
	public AccountHistoryPage clickOnDateFilterDropdown() {
		try {
			dateDropdown.click();
			Reporter.log("Clicked on Date Filter Dropdown");
		} catch (Exception e) {
			Assert.fail("Failed to click on Date Filter Dropdown");
		}
		return this;
	}

	/**
	 * Verify date filter option is present in Drop-down
	 * 
	 * @param option
	 * @return
	 */
	public AccountHistoryPage verifyDateOptionsFromDropDown(String option) {
		try {
			boolean dateOptionIsPresent = false;
			for (WebElement filterOption : dateDropdownValues) {
				if (filterOption.getText().equalsIgnoreCase(option)) {
					Reporter.log("Filter option - " + option + " is present in dropdown");
					dateOptionIsPresent = true;
					break;
				}
			}
			Verify.assertTrue(dateOptionIsPresent, "Date option - " + option + " - is not present in dropdown");
		} catch (Exception e) {
			Verify.fail("Date dropdown is not displayed");
		}
		return this;
	}

	/**
	 * Verify Date Filter Default Option
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyDateFilterDefaultOption(String msg) {
		boolean isDateFilterDefaultOption = false;
		try {
			if (dateFilterDefaultOption.getText().equalsIgnoreCase(msg)) {
				isDateFilterDefaultOption = true;
			}
			Verify.assertTrue(isDateFilterDefaultOption, "Date default filter option " + msg + " is not displayed");
			Reporter.log("Date default filter option " + msg + " is displayed");
		} catch (Exception ex) {
			Verify.fail("Date default filter option is not displayed");
		}
		return this;
	}

	/**
	 * Select Date From Drop-down
	 * 
	 * @param option
	 * @return
	 */
	public AccountHistoryPage selectDateFromDropDown(String option) {
		try {
			clickOnDateFilterDropdown();
			for (WebElement filterOption : dateDropdownValues) {
				if (filterOption.getText().equalsIgnoreCase(option)) {
					filterOption.click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Date dropdown is not displayed");
		}
		return this;
	}

	/**
	 * Verify Date Picker Calendar icon
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyDatePickerCalendar() {
		try {
			Verify.assertTrue(datePickerCalendarStartDate.isDisplayed(), "Calendar Start Date icon is not displayed");
			Verify.assertTrue(datePickerCalendarEndDate.isDisplayed(), "Calendar End Date icon is not displayed");
			datePickerCalendarStartDate.click();
			waitFor(ExpectedConditions.visibilityOf(calendarModal));
			Verify.assertTrue(calendarModal.isDisplayed(), "Calendar modal is not displayed");
			Verify.assertTrue(caledarSelectCTA.isDisplayed(), "Select CTA is not displayed");
			waitFor(ExpectedConditions.elementToBeClickable(caledarSelectCTA));
			caledarSelectCTA.click();
		} catch (Exception e) {
			Verify.fail("Date picker Calendar is not displayed");
		}
		return this;
	}

	/**
	 * Verify Date Picker
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyDatePicker() {
		try {
			for (WebElement dateField : datePicker) {
				Verify.assertTrue(dateField.isDisplayed(), "Date Field is not displayed");
			}
			Reporter.log("Date field is displayed");
		} catch (Exception e) {
			Verify.fail("Date picker is not displayed");
		}
		return this;
	}

	/**
	 * Verify Pagination
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyPagination() {
		try {
			boolean pagination = false;
			int numberOfRecords = totalRecords.size();
			Reporter.log("There are " + numberOfRecords + " records on the page");
			if (numberOfRecords < 12) {
				if (!isElementDisplayed(pagiNation)) {
					pagination = true;
					Reporter.log("Pagination is not displayed for less then 12 records");
				}
			}
			if (totalRecords.size() == 12) {
				Assert.assertTrue(pagiNation.isDisplayed(),
						"Pagination is correctly displayed for " + numberOfRecords + " records");
				pagination = true;
				Reporter.log("Pagination is displayed for 12 or more records");
			}
			Assert.assertTrue(pagination, "Pagination is not correctly displayed for " + numberOfRecords + " records");
		} catch (Exception e) {
			Assert.fail("Pagination is not displayed");
		}
		return null;
	}

	/**
	 * Verify Download PDF Link
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyDownloadPDFLinkForBriteBill() {
		try {
			for (WebElement webElement : downloadPDFLinksForBriteBill) {
				webElement.isDisplayed();
			}
			Reporter.log("Download PDF link is displayed");
		} catch (Exception e) {
			Verify.fail("Download PDF link is not displayed");
		}
		return this;
	}

	/**
	 * Verify Download PDF Link is not present
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyDownloadPDFLinkIsNotPresent() {
		try {
			Assert.assertFalse(activityRecord.get(0).getText().contains("Download PDF"), "Download PDF is displayed.");
			Reporter.log("Download PDF link is not displayed");
		} catch (Exception e) {
			Assert.fail("Download PDF link is displayed for Non PAH User");
		}
		return this;
	}

	/**
	 * Verify Download PDF Link is not present
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyPromotionRecordIsPresent() {
		try {
			for (WebElement activityItem : activityRecord) {
				Assert.assertTrue(activityItem.getText().contains("Promotion"), "Promotion header is not displayed");
				Assert.assertTrue(activityItem.getText().contains("http://www.t-mobile.com/PromotionalOffers"),
						"Promotion link is not displayed");
			}
			Reporter.log("Promotion Record link is displayed");
		} catch (Exception e) {
			Assert.fail("Promotion Records are not displayed");
		}
		return this;
	}

	/**
	 * Verify View Bill Link
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyViewBillLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(viewBillLink));
			Assert.assertTrue(viewBillLink.isDisplayed(), "View Bill link is not visible");
		} catch (Exception e) {
			Assert.fail("View Bill link is not displayed");
		}
		return this;
	}

	/**
	 * Click View Bill Link
	 * 
	 * @return
	 */
	public AccountHistoryPage clickViewBillLink() {
		try {
			clickElementWithJavaScript(viewBillLink);
		} catch (Exception e) {
			Assert.fail("View Bill link is not clicked");
		}
		return this;
	}

	public AccountHistoryPage verifyDocumentsOption() {
		try {
			waitforSpinner();
			Assert.assertTrue(documentsOption.get(0).getText().equalsIgnoreCase("Documents"));
			Reporter.log("Documents option is displayed");
		} catch (Exception e) {
			Assert.fail("Documents option is not displayed");
		}
		return this;
	}

	/**
	 * Verify Download PDF Link
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyDownloadPDFLink() {
		try {
			for (WebElement webElement : downLoadPDFLink) {
				webElement.isDisplayed();
			}
			Reporter.log("Download PDF link is displayed");
		} catch (Exception e) {
			Verify.fail("Download PDF link is not displayed");
		}
		return this;

	}
	
	
	public AccountHistoryPage verifyDownloadPDFLinksinDocumentspage() {
		try {
			checkPageIsReady();
			Thread.sleep(3000);
			if(activityHeaders.size()<=0) {
				Assert.fail("Documents are not shown");
			}
			
			HashMap<String,Integer> activitiesexpected=new HashMap<String,Integer>();
			HashMap<String,Integer> activitiesactual=new HashMap<String,Integer>();
						
			boolean isnextavailable=true;
			while(isnextavailable) {
			for (WebElement webElement : activityHeaders) {
				
				if(activitiesexpected.containsKey(webElement.getText().trim())) {
					activitiesexpected.put(webElement.getText().trim(), activitiesexpected.get(webElement.getText().trim())+1);}
				   else {
					   activitiesexpected.put(webElement.getText().trim(),1);
				       activitiesactual.put(webElement.getText().trim(),0);}
				
				if(webElement.findElements(By.xpath("//../following-sibling::p/a[contains(text(),'Download PDF')]")).size()>0) {
					  activitiesactual.put(webElement.getText().trim(), activitiesactual.get(webElement.getText().trim())+1);
				}
			}
			if(enablenext.size()>0)
			enablenext.get(0).click();
			
			else
				isnextavailable=false;	
			
			}
			boolean ispass=true;
			for (String key : activitiesexpected.keySet()) {
				 Reporter.log("Activity is:"+key+"|Expected number of download links :"+activitiesexpected.get(key)+"|Actual Number is:"+activitiesactual.get(key)); 
				if(activitiesexpected.get(key)!=activitiesactual.get(key))
					ispass=false;  
			}
			
			if(!ispass) {
				Assert.fail("");
			}
		
			
		} catch (Exception e) {
			Assert.fail("Activity headers or next button element is not present");
		}
		return this;

	}
	
	

	
	
	
	public AccountHistoryPage Clickall24monthslink() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(accActivityspinner));
			clickOnDateFilterDropdown();
	        clickElementWithJavaScript(all24months);	
			waitFor(ExpectedConditions.invisibilityOfElementLocated(accActivityspinner));
			Reporter.log("all 24 months option is selected");
		} catch (Exception e) {
			Assert.fail("all 24 months option is not found");
		}
		return this;

	}
	
	

	public CommonPage clickOnlogOutBtn(){
		logOutBtn.click();
		Reporter.log("Clicked on logOut button");
		return this;
	}

	/**
	 * To verify PII masking for selected line
	 * @param custMsisdnPID
	 */
	public void verifyPiiMaskingForSelectedLine(String custMsisdnPID) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(selectedLine, custMsisdnPID),"No PII masking for Selected Line");
			Reporter.log("Verified PII masking for Selected Line");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Selected Line");
		}
		
	}

	/**
	 * To verify PII masking for payment information on Account history section grid
	 * @param custPaymentPID
	 */
	public void verifyPiiMaskingForPaymentInfoOnGrid(String custPaymentPID) {
		try {
			for (WebElement payment : payemntTypes) {
				Assert.assertTrue(checkElementisPIIMasked(payment, custPaymentPID),"No PII masking for Payment type");
			}
			Reporter.log("Verified PII masking for Payment Information" );
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Payment Information");
		}
		
	}

	/**
	 * To verify PII masking for Msisdn on Account history section grid
	 * @param custMsisdnPID
	 */
	public void verifyPiiMaskingForMsisdnOnGrid(String custMsisdnPID) {
		try {
			for (WebElement line : linesAccHistory) {
				Assert.assertTrue(checkElementisPIIMasked(line, custMsisdnPID),"No PII masking for Msisdn");
				}
			Reporter.log("Verified PII masking for Msisdns");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Msisdn");
		}
		
	}

	/**
	 * To verify payment information pii masking on Scheduled activities
	 * @param custPaymentPID
	 */
	public void verifyPiiMaskingForPaymentInfoOnScheduledActivity(String custPaymentPID) {
		try {
			for (WebElement paymentScheduled : paymentScheduledActivity) {
				Assert.assertTrue(checkElementisPIIMasked(paymentScheduled, custPaymentPID),"No PII masking for payment info");
			}
			Reporter.log("Verified PII masking for Payment Information" );
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Payment Information");
		}
		
	}

	/**
	 * Verify Print Payment receipt Link
	 * 
	 * @return
	 */
	public AccountHistoryPage verifyPrintPaymentReceiptLink() {
		try {
			for (WebElement webElement : printPaymentReceiptLinks) {
				if (!webElement.isDisplayed()) {
					Assert.fail("Print Payment receipt link is not displayed");
				}
			}
			Reporter.log("Print Payment receipt link is displayed");
		} catch (Exception e) {
			Assert.fail("Print Payment receipt link is not displayed");
		}
		return this;

	}
	
	public HashMap<String, String> getScheduledPaDetails() {
		try {
			getScheduledPa();
			getScheduledPaPaymentMethod();
		} catch (Exception e) {
			Assert.fail("Error getting PA details from Account History page");
		}
		return paDetails;
	}

	public HashMap<String, String> getScheduledPa() {
		try {
			int i = 0;
			for (WebElement paDetail : paymentArrangementDetails) {
				if (i < 2) {
					if (paDetail.getText().contains("for")) {
						paDetails.put("Date", paDetail.getText().replace(" for", ""));
					} else {
						paDetails.put("Amount", paDetail.getText());
					}
					i++;
				}
			}
		} catch (Exception e) {
			Assert.fail("Error getting PA details from Account History page");
		}
		return paDetails;
	}

	
	public HashMap<String, String> getScheduledPaPaymentMethod() {
		try {
			paDetails.put("Payment Method", paymentArrangementPaymentMethod.getText());
		} catch (Exception e) {
			Reporter.log("No Payment method to PA");
			paDetails.put("Payment Method", "NONE");
		}
		return paDetails;
	}
	

	
	
	
	
	public AccountHistoryPage cancelFDP() {
		try {
			checkPageIsReady();
			cancelFDP.click();
			cancelFDPConfirmationModal.isDisplayed();
			cancelPayment.click();
			
		} catch (Exception e) {
			Assert.fail("cancel FDP not done");
		}
		return this;
	}
	
	public AccountHistoryPage verifyCancelFDPConfirmationModal() {
		try {
			checkPageIsReady();
			
			CancelFDPConfirmationModal.isDisplayed();
		
			
		} catch (Exception e) {
			Assert.fail("cancel FDP confirmation modal not dispalyed");
		}
		return this;
	}
	
	
	public AccountHistoryPage cancelScheduledFDP() {
		try {
			HomePage homePage = new HomePage(getDriver());
			homePage.clickOnAccountHistoryLink();
			AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
			accountHistoryPage.verifyPageLoaded();
			accountHistoryPage.cancelFDP();
			accountHistoryPage.verifyCancelFDPConfirmationModal();
			
		} catch (Exception e) {
			Assert.fail("cancel FDP not done");
		}
		return this;
	}
	
	/**
	 * Get suspended line missdn 
	 */
	public String getSuspendedLineMissdn() {
		String suspendedLineMissdn=null;
		try {
			suspendedLineMissdn=suspendedLineAlert.getText().replaceAll("[^0-9]+", " ");
			Reporter.log("Suspended line missdn details successfully fetched");
		} catch (Exception e) {
			Assert.fail("Fectching suspended line missdn details failed");
		}
		return suspendedLineMissdn.trim();
	}
	
	/**
	 * Click suspended line alert 
	 */
	public AccountHistoryPage clickSuspendedLineAlert() {
		try {
			clickElementWithJavaScript(suspendedLineAlert);
			//suspendedLineAlert.click();
		} catch (NoSuchElementException e) {
			Assert.fail("Click on suspended line alert failed");
		}
		return this;
	}
	
}