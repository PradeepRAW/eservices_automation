package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class NewPaymentErrorPage extends CommonPage {

	private final String pageUrl = "payment-error";

	@FindBy(css = "div.col-md-10.offset-md-1.Display3")
	private WebElement paymentErrorHeading;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement backButton;

	@FindBy(css = "div.col-md-10.offset-md-1.body.black")
	private WebElement paymentErrorMessage;

	public NewPaymentErrorPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewPaymentErrorPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewPaymentErrorPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("payment error page displayed");

		} catch (Exception e) {
			Assert.fail("payment error page not displayed");
		}
		return this;
	}

	public NewPaymentErrorPage verifyDuplicateTransactionError() {
		try {
			checkPageIsReady();

			paymentErrorHeading.getText().contains("Duplicate payment");
			paymentErrorMessage.isDisplayed();
			paymentErrorMessage.getText().contains(
					"To protect you from being overcharged, we prevent payments of the same amount within 24 hours. To proceed with this payment please modify the amount");
			Reporter.log("verified duplicate Transaction error ");
		} catch (Exception e) {
			Assert.fail("Failed to verify duplicate Transaction error");
		}
		return this;
	}

	public NewPaymentErrorPage verifyRejectedTransactionError() {
		try {
			checkPageIsReady();

			paymentErrorHeading.getText().contains("Rejected payment");
			paymentErrorMessage.isDisplayed();
			paymentErrorMessage.getText().contains(
					"We experienced a technical problem while processing your payment. Please wait a few moments and try again");
			Reporter.log("verified Rejected Transaction  error ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Rejected Transaction  error");
		}
		return this;
	}

	public NewPaymentErrorPage verifySoftDeclineError() {
		try {
			checkPageIsReady();

			paymentErrorHeading.getText().contains("Soft Decline");
			paymentErrorMessage.isDisplayed();
			paymentErrorMessage.getText().contains(
					"We experienced a technical problem while processing your payment. Please wait a few moments and try again.");
			Reporter.log("verified Soft Decline error ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Soft Decline error");
		}
		return this;
	}

	public NewPaymentErrorPage verifyHardDeclineError() {
		try {
			checkPageIsReady();

			paymentErrorHeading.getText().contains("Hard Decline");
			paymentErrorMessage.isDisplayed();
			paymentErrorMessage.getText().contains(
					"We experienced a technical problem while processing your payment. Please wait a few moments and try again");
			Reporter.log("verified hard Decline error ");
		} catch (Exception e) {
			Assert.fail("Failed to verify hard Declinet error");
		}
		return this;
	}

	public NewPaymentErrorPage verifySystemError() {
		try {
			checkPageIsReady();

			paymentErrorHeading.getText().contains("System error");
			paymentErrorMessage.isDisplayed();
			paymentErrorMessage.getText().contains(
					"We experienced a technical problem while processing your payment. Please wait a few moments and try again.");
			Reporter.log("verified system error ");
		} catch (Exception e) {
			Assert.fail("Failed to verify system error");
		}
		return this;
	}

	public NewPaymentErrorPage verifybackBtn() {
		try {
			checkPageIsReady();
			backButton.isDisplayed();
			Reporter.log("verified back button");
		} catch (Exception e) {
			Assert.fail("Failed to verify back button");
		}
		return this;
	}

}
