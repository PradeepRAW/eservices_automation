package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class ProductCatalogPromotionCachingApi extends ApiCommonLib{
	
	/***
	 * This is the API specification for the caching product catalog promotions
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
	//Trigger caching of catalog promotions. This API triggers caching of catalog promotions
	//channel Code can take either of the values, MYT/HSU/AAL 
    public Response triggerCachingForCatalogPromotions(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildProductCatPromoAPIHeader(apiTestData);
    	String resourceURL = "/v1/trigger/promotions/cache?channelCode=MYT&marketId="+getToken("marketId");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
        return response;
    }
    
  //Trigger caching of catalog promotions.Evicts the cached promotions
    public Response evictCachedPromotions(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildProductCatPromoAPIHeader(apiTestData);
    	String resourceURL = "/v1/trigger/promotions/cache?channelCode=MYT&marketId="+getToken("marketId");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.DELETE);
         return response;
    }
 
    private Map<String, String> buildProductCatPromoAPIHeader(ApiTestData apiTestData) throws Exception {
    	final String accessToken = getAccessToken();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept","application/json");
		headers.put("Authorization",accessToken);
		headers.put("transactionId","sdfsdfsdsd");
		headers.put("applicationId","sdfsdf");
		headers.put("channelId","sfsdfsd");
		headers.put("correlationId","sdfsdfd");
		headers.put("clientId","sdfsdfd");
		headers.put("transactionBusinessKey","sdfsdf");
		headers.put("transactionBusinessKeyType","sdfsdf");
		headers.put("Cache-Control","no-cache");
		return headers;
	}

}
