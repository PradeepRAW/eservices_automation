package com.tmobile.eservices.qa.pages.payments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class NewAutopayPage extends CommonPage {

	private final String pageUrl = "payments/autopay";

	@FindBy(xpath = "//span[contains(text(),'Terms and Conditions')]")
	private WebElement termsNconditionsLink;

	@FindBy(xpath = "//span[contains(text(),'See the FAQs')]")
	private WebElement faqLink;

	@FindBy(css = "div.dialog.animation-out.animation-in")
	private WebElement cancelPopupModal;

	@FindBy(css = "p.body-link.cursor")
	private WebElement cancelAutopayBtn;

	@FindBy(css = "span.col-12.black.Display5")
	private List<WebElement> cancelPopupHeader;

	@FindBy(css = "div.col-10.offset-1.text-center")
	private List<WebElement> cancelPopupAlert;

	@FindBy(xpath = "//button[contains(text(),'Yes, Cancel AutoPay')]")
	private WebElement cancelPopupYesBtn;

	@FindBy(css = "div.row.padding-top-medium div.col-12.col-md-10")
	private WebElement tNcModal;

	@FindBy(css = "span.Display3")
	private WebElement tNcHeader;

	@FindBy(xpath = "//span[contains(text(),'Toggle Autopay')]")
	private WebElement autopayToggle;

	@FindBy(css = "div.col-12.text-center p.Display3")
	private WebElement autopayHeader;

	@FindBy(css = "div.text-center.Display3")
	private WebElement faqHeader;

	@FindBy(css = "button#acceptButton")
	public WebElement backBtn;

	@FindBy(css = "button#acceptButton")
	private WebElement agreeAndSubmitbtn;

	@FindBy(css = "button#cancelButton")
	private WebElement backButton;

	@FindBy(css = "p.body-bold.black.padding-bottom-xsmall")
	private WebElement firstPaymentDate;

	@FindBy(css = "p.body-bold.black.padding-bottom-small")
	private WebElement nextPaymentDate;

	@FindBy(css = "div.col-8.offset-2.text-center p.legal")
	private List<WebElement> billDUetDatecontent;

	@FindBy(css = "p.body-bold.black.padding-top-small")
	private List<WebElement> autopayConfirmationpageText;

	@FindBy(css = "span.pull-left.black.Display6")
	private WebElement showDetails;

	@FindBy(css = "p.mb-0.Display3")
	private WebElement autopaycancelledHeader;

	@FindBy(css = "p.mb-0.body-bold.black")
	private WebElement autopaycancelledText;

	@FindBy(css = "button#cancelButton")
	private WebElement returnHome;

	@FindBy(css = "span.arrow-right.d-inline-block")
	private WebElement pcmbladeArrow;

	@FindBy(css = "p.mb-0.Display3")
	private WebElement autopaysuccessHeader;

	@FindBy(css = "button#acceptButton")
	private WebElement agreeNsubmitBtn;

	@FindBy(css = "span#paymentLable")
	private WebElement paymentMethodBlade;

	@FindBy(css = ".fine-print-body.pull-left")
	private List<WebElement> paymentSection;

	@FindBy(css = "a[ng-click='vm.showHideDetails(false)']")
	private WebElement hidePaymentDetails;

	@FindBy(css = "#addPayment div.inline.black")
	private WebElement storedPaymentType;

	@FindBy(id = "navToHome")
	private WebElement goToHome;

	@FindBy(id = "govOrLargeBusinessUserModal")
	private WebElement govbusinesspopup;

	@FindBy(css = "#govOrLargeBusinessUserModal span")
	private WebElement govbusinessopsmessage;

	@FindBy(css = "#govOrLargeBusinessUserModal p")
	private WebElement contactPAHtext;

	@FindBy(id = "pendingAutopayModal")
	private WebElement PendingPAModel;

	@FindBy(css = "#pendingAutopayModal span")
	private WebElement pendingpAopsmessage;

	@FindBy(css = "#pendingAutopayModal p")
	private WebElement activePAtext;

	public NewAutopayPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewAutopayPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewAutopayPage verifyPageLoaded() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				getDriver().navigate().refresh();
				Thread.sleep(2000);
				verifyPageUrl();
			} else {
				checkPageIsReady();

				verifyPageUrl();
				Reporter.log("Autopay page displayed");
			}
		} catch (Exception e) {
			Assert.fail("Autopay page not displayed");
		}
		return this;
	}

	public NewAutopayPage verifyActivePAModel() {
		try {
			verifyPageUrl();
			waitFor(ExpectedConditions.elementToBeClickable(PendingPAModel));
			Verify.assertTrue(pendingpAopsmessage.getText().equals("Oops—we can’t set up AutoPay right now."),
					"Oops—we can’t set up AutoPay right now. is not displayed");
			Verify.assertTrue(activePAtext.getText().equals(
					"You have an active Payment Arrangement. Please come back to set up AutoPay when your Payment Arrangement is complete."),
					"You have an active Payment Arrangement. Please come back to set up AutoPay when your Payment Arrangement is complete. is not displayed");
		} catch (Exception e) {
			Assert.fail("Active PA model controls are not located");
		}
		return this;
	}

	public NewAutopayPage verifyGovLargebusinessModel() {
		try {
			verifyPageUrl();
			waitFor(ExpectedConditions.elementToBeClickable(govbusinesspopup));
			Verify.assertTrue(govbusinessopsmessage.getText().equals("Oops!"), "Oops! is not displayed");
			Verify.assertTrue(
					contactPAHtext.getText().equals("Please contact the Primary Account Holder to set up AutoPay"),
					"Please contact the Primary Account Holder to set up AutoPay is not displayed");
		} catch (Exception e) {
			Assert.fail("Contact PAH model controls are not located");
		}
		return this;
	}

	public NewAutopayPage clickGotoHome() {
		try {
			goToHome.click();
		} catch (Exception e) {
			Assert.fail("Goto home link is not located");
		}

		return this;
	}

	public NewAutopayPage clickAutopayTermsnConditionsLink() {
		try {
			checkPageIsReady();
			termsNconditionsLink.click();
			Reporter.log("Clicked on Terms and conditions link");
		} catch (Exception e) {
			Assert.fail("Failed to click on Terms and conditions link");
		}
		return this;
	}

	public NewAutopayPage clickAutopayFAQLink() {
		try {
			checkPageIsReady();
			faqLink.click();
			Reporter.log("Clicked on Terms and conditions link");
		} catch (Exception e) {
			Assert.fail("Failed to click on Terms and conditions link");
		}
		return this;
	}

	public NewAutopayPage verifyAutopayFAQLink() {
		try {
			checkPageIsReady();
			faqLink.isDisplayed();
			Reporter.log("verified FAQ  link");
		} catch (Exception e) {
			Assert.fail("Failed to verify FAQ link");
		}
		return this;
	}

	public NewAutopayPage verifyAutopayTermsnConditionsLinkDispalyed() {
		try {
			checkPageIsReady();
			termsNconditionsLink.isDisplayed();
			Reporter.log("verified Terms and conditions  link");
		} catch (Exception e) {
			Assert.fail("Failed to verify Terms and conditions link");
		}
		return this;
	}

	/**
	 * validate Autopay Terms and Conditions modal
	 */
	public NewAutopayPage validateAutopayTnCPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("autopay/tnc"));
			Verify.assertTrue(tNcModal.isDisplayed(), "Auto Pay Terms and Conditions modal is not displayed");
			Verify.assertTrue(tNcHeader.getText().equals("Terms and Conditions"),
					"Auto Pay Terms and Conditions header is not displayed");
			Verify.assertTrue(backBtn.isDisplayed(), "Back button is not displayed on TnC modal");
			Reporter.log("Autopay Terms & Conditions page successfully validated");
		} catch (Exception e) {
			Verify.fail("Fail to validate Terms and Conditions Model");
		}
		return this;
	}

	public NewAutopayPage clickBackButton() {
		try {
			backBtn.click();
			Reporter.log("Clicked on back button");
		} catch (Exception e) {
			Assert.fail("Failed to click on back button");
		}
		return this;
	}

	/**
	 * validate FAQ page
	 */
	public NewAutopayPage validateAutopayFAQPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("autopay/faq"));
			Verify.assertTrue(faqHeader.getText().equals("Frequently Asked Questions"),
					"Auto Pay FAQ header is not displayed");
			Verify.assertTrue(backBtn.isDisplayed(), "Back button is not displayed on FAQ page");
			Reporter.log("Autopay FAQ page successfully validated");
		} catch (Exception e) {
			Verify.fail("Fail to validate FAQ page");
		}
		return this;
	}

	public NewAutopayPage ToggleOnAutoPay() {
		try {

			checkPageIsReady();
			if (autopayHeader.getText().contains("AutoPay is OFF")) {
				autopayToggle.click();
			}
			checkPageIsReady();
			Assert.assertTrue(autopayHeader.getText().contains("AutoPay is ON"));
			Reporter.log("Autopay turned on");
		} catch (Exception e) {
			Assert.fail("Failed to turned on Autopay");
		}
		return this;
	}

	public NewAutopayPage clickCancelButton() {
		try {
			checkPageIsReady();
			cancelAutopayBtn.click();
			Reporter.log("Clicked on cancel button");
		} catch (Exception e) {
			Assert.fail("Failed to click on cancel button");
		}
		return this;
	}

	/**
	 * validate Autopay cancel popup modal
	 */
	public NewAutopayPage validateAutopayCancelModal() {
		try {
			checkPageIsReady();

			Verify.assertTrue(cancelPopupModal.isDisplayed(), "cancel popup modal is not displayed");
			verifyListOfElementsBytext(cancelPopupHeader, "Are you sure you want to cancel AutoPay?");
			verifyListOfElementsBytext(cancelPopupAlert, "If you cancel AutoPay, you will lose your $40.00 discount");

			Reporter.log("Autopay cancel popup successfully validated");
		} catch (Exception e) {
			Verify.fail("Fail to validate cancel popup");
		}
		return this;
	}

	public NewAutopayPage clickCancelpopupyesButton() {
		try {
			cancelPopupYesBtn.click();
			Reporter.log("Clicked on cancel popup yes button");
		} catch (Exception e) {
			Assert.fail("Failed to click on cancel  popup yes button");
		}
		return this;
	}

	/**
	 * return autopay status
	 * 
	 * @return
	 */
	public boolean verifyAutoPayStatusIsOFF() {
		try {
			waitFor(ExpectedConditions.visibilityOf(autopayHeader));
			return autopayHeader.getText().contains("OFF");
		} catch (Exception e) {
			Assert.fail("Auto pay status field not found");
		}
		return false;
	}

	public NewAutopayPage verifyFirstPaymentDateContent(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(firstPaymentDate));
			Verify.assertTrue(firstPaymentDate.getText().contains(msg), "First Payment Date Content is incorrect");
		} catch (Exception e) {
			Verify.fail("Fail to verify the First Payment Date Content");
		}
		return this;
	}

	public NewAutopayPage verifyNextPaymentDateContent(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(nextPaymentDate));
			Verify.assertTrue(nextPaymentDate.getText().contains(msg), "Next Payment Date Content is incorrect");
		} catch (Exception e) {
			Verify.fail("Fail to verify the Next Payment Date Content");
		}
		return this;
	}

	public NewAutopayPage verifyBillDueDateContentwhenAutoPayON() {
		try {
			checkPageIsReady();
			Verify.assertTrue(billDUetDatecontent.get(0).getText().contains("Next bill due:"),
					"Bill Due Date Content is incorrect");
			Verify.assertTrue(
					billDUetDatecontent.get(1).getText().replace("[0-9]", "")
							.contains("Payments will process on the th of each month"),
					"Payment Processing Date Content is incorrect");
		} catch (Exception e) {
			Verify.fail("Fail to verify Bill Due Date Content");
		}
		return this;
	}

	public NewAutopayPage verifyBillDueDateContentwhenAutoPayOFF() {
		try {
			checkPageIsReady();
			Verify.assertTrue(billDUetDatecontent.get(0).getText().contains("Your bill due:"),
					"Bill Due Date Content is incorrect");

		} catch (Exception e) {
			Verify.fail("Fail to verify Bill Due Date Content");
		}
		return this;
	}

	/***
	 * verify payment method blade
	 * 
	 * @return
	 */

	public NewAutopayPage verifyPaymentMethodBlade() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paymentMethodBlade));
			paymentMethodBlade.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Fail to verify the Payment Method Blade");
		}
		return this;
	}

	/**
	 * validate AutoPay page
	 */

	public NewAutopayPage verifyAutoPayLandingPageFields() {
		try {
			checkPageIsReady();

			String apHeaderText = autopayHeader.getText();
			Verify.assertTrue(apHeaderText.equals("AutoPay is OFF") || apHeaderText.equals("AutoPay is ON"),
					"AutoPay  page header is incorrect");
			checkPageIsReady();
			if (verifyAutoPayStatusIsOFF()) {
				verifyFirstPaymentDateContent("First AutoPay would be:");
				verifyBillDueDateContentwhenAutoPayOFF();
			} else {
				verifyNextPaymentDateContent("Next AutoPay payment:");
				verifyBillDueDateContentwhenAutoPayON();

			}

			verifyPaymentMethodBlade();

			verifyAutopayFAQLink();
			verifyAutopayTermsnConditionsLinkDispalyed();
			verifyAgreeAndSubmitBtn();
			verifyBackBtn();
			Reporter.log("Autopay  page validation succesfully finished");
		} catch (Exception e) {
			Assert.fail("Auto Pay  page did not load or not found all elements");
		}
		return this;
	}

	/**
	 * verify Agree And Submit button is displayed
	 * 
	 * @return true/false
	 */
	public NewAutopayPage verifyAgreeAndSubmitBtn() {
		checkPageIsReady();
		try {

			agreeAndSubmitbtn.isDisplayed();
			agreeAndSubmitbtn.getText().equals("Agree & Submit");
		} catch (NoSuchElementException elementNotFound) {
			Reporter.log("Agree And Submit Button verification step Failed.");
		}
		return this;

	}

	/**
	 * verify cancel button is displayed
	 * 
	 * @return true/false
	 */
	public NewAutopayPage verifyBackBtn() {
		try {
			backButton.isDisplayed();
			backButton.getText().equals("Back");
		} catch (Exception e) {
			Assert.fail("back button not found");
		}
		return this;
	}

	/**
	 * validate cancel confirmation page
	 */
	public NewAutopayPage validateAutopayCancelConfirmationpage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("autopay/cancelConfirmation"));
			Verify.assertTrue(autopaycancelledHeader.getText().equals("AUTOPAY CANCELED"),
					"cancel confirmation page is not displayed");
			Verify.assertTrue(
					autopaycancelledText.getText().replaceAll("[0-9]", "").equals(
							"You will no longer receive the AutoPay discount. Save $/mo. by re-enrolling in AutoPay."),
					"cancel confirmation text is not displayed");
			Reporter.log("Autopay cancel confirmation  page successfully validated");
		} catch (Exception e) {
			Verify.fail("Fail to validate cancel confirmation  page");
		}
		return this;
	}

	public NewAutopayPage clickReturnHome() {
		try {
			returnHome.click();
			Reporter.log("Clicked on return home button");
		} catch (Exception e) {
			Assert.fail("Failed to click on return home  button");
		}
		return this;
	}

	public NewAutopayPage clickPCMBlade() {
		try {
			checkPageIsReady();
			pcmbladeArrow.click();
			Reporter.log("Clicked on PCM Blade");
		} catch (Exception e) {
			Assert.fail("Failed to click on PCM Blade");
		}
		return this;
	}

	public NewAutopayPage clickAgreeandSubmit() {
		try {
			checkPageIsReady();
			agreeNsubmitBtn.click();
			Reporter.log("Clicked on agree and submit button");
		} catch (Exception e) {
			Assert.fail("Failed to click on agree and submit button");
		}
		return this;
	}

	/**
	 * verify AutoPay confirmation page elements
	 *
	 * @param autoPayConfirmationPage
	 * @param paymentType
	 */
	public NewAutopayPage verifyAutoPayConfirmationPageElements(String paymentType) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("autopay/confirmation"));
			Verify.assertTrue(autopaysuccessHeader.getText().equals("AutoPay is now ON"),
					"autopay confirmation page is not displayed");
			verifyFirstPaymentDateOnAutopay();
			verifyBillDuetDateOnAutopay();

			verifyAutopayVeribageText();
			clickShowdetais();
			verifyPaymentDetails(paymentType);

		} catch (Exception e) {
			Assert.fail("Fail to verify the Auto page confirmation page Elements");
		}
		return this;
	}

	/**
	 * verify First payment date content
	 *
	 * @return boolean
	 */
	public NewAutopayPage verifyFirstPaymentDateOnAutopay() {
		try {
			verifyElementBytext(autopayConfirmationpageText, "First Autopay payment date:");

			Reporter.log("Verified the First Autopay payment Date");
		} catch (Exception e) {
			Assert.fail("Failed to verify the irst Payment Date on AUto Pay");
		}
		return this;
	}

	/*
	 * * veirfy bill dute date content
	 *
	 * @return boolean
	 */
	public NewAutopayPage verifyBillDuetDateOnAutopay() {
		try {
			verifyElementBytext(autopayConfirmationpageText, "Your next bill due date is on");
			Reporter.log("Verified the Bill Due Date on Autopay");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Bill Due Date on Autopay");
		}
		return this;
	}

	public NewAutopayPage verifyAutopayVeribageText() {
		try {
			checkPageIsReady();
			verifyElementBytext(autopayConfirmationpageText,
					"Your $ AutoPay discount will be applied each month beginning with your next bill due date");
			Reporter.log("AutoPay veribage text is displayed");

		} catch (Exception e) {
			Assert.fail("Autopay veribage text   not found");
		}
		return this;
	}

	public boolean verifyElementBytext(List<WebElement> elements, String text) {

		boolean isElementDisplayed = false;
		for (WebElement webElement : elements) {
			if (webElement.getText().replaceAll("[0-9]", "").contains(text)) {
				isElementDisplayed = true;
				break;
			}
		}
		return isElementDisplayed;
	}

	public NewAutopayPage clickShowdetais() {
		try {
			checkPageIsReady();
			showDetails.click();
			Reporter.log("Clicked on show details");
		} catch (Exception e) {
			Assert.fail("Failed to click on show details");
		}
		return this;
	}

//	/**
//	 * addbank
//	 * 
//	 * @param myTmoData
//	 * @return
//	 */
//	public Long addBankToPaymentMethod(MyTmoData myTmoData) {
//		Long accNum = 0L;
//		try {
//			clickPCMBlade();
//			NewSpokePage newspokepage = new NewSpokePage(getDriver());
//			newspokepage.verifyPageLoaded();
//			newspokepage.clickAddBank();
//			NewBankInformation newBankInformation = new NewBankInformation(getDriver());
//			newBankInformation.verifyPageLoaded();
//			newBankInformation.fillBankInfo(myTmoData.getPayment());
//			 accNum = newBankInformation.clickContinueAddBank();	
//			NewAutopayPage newAutopayPage = new NewAutopayPage(getDriver());
//			newAutopayPage.clickAgreeandSubmit();
//			verifyAutoPayLandingPageFields();
//			Reporter.log("Added bank to payment method");
//		} catch (Exception e) {
//			Assert.fail("Fail to Add Bank to Payment Method");
//		}
//		return accNum;
//	}

//	/**
//	 * add payment card if there are no stored payment methods
//	 * 
//	 * @param myTmoData
//	 * @return
//	 * @return boolean
//	 */
//	public String verifyStoredPaymentMethodOrAddBank(MyTmoData myTmoData) {
//		checkPageIsReady();
//		if (!paymentMethodBlade.getText().equals("Using my") || (paymentMethodBlade.getText().equals("Using my") && !storedPaymentType.getAttribute("class").contains("checkAccount"))) {
//			addBankToPaymentMethod(myTmoData);
//			return "Bank";
//		} else if (storedPaymentType.getAttribute("class").contains("checkAccount")) {
//			return "Bank";
//		} else {
//			return "Card";
//		}
//	}
//	

	/**
	 * verify card details are displayed
	 *
	 * @param paymentType
	 * @return
	 */
	public NewAutopayPage verifyPaymentDetails(String paymentType) {
		try {

			boolean paymentDetailsDisplayed = true;
			checkPageIsReady();
			showDetails.click();
			List<String> paymentdetails;
			if ("Card".equals(paymentType)) {
				paymentdetails = new ArrayList<>(Arrays.asList("Customer:", "T-mobile account #:", "Date:",
						"Payment method:", "Autopay status:"));
			} else {
				paymentdetails = new ArrayList<>(Arrays.asList("Customer:", "T-mobile account #:", "Date:",
						"Payment method:", "Routing #:", "Autopay status:"));
			}
			for (String detail : paymentdetails) {
				for (WebElement webElement : paymentSection) {
					if (webElement.isDisplayed()) {
						if (detail.equals(webElement.getText())) {
							paymentDetailsDisplayed = true;
						}
					}
				}
				if (!paymentDetailsDisplayed) {
					break;
				}
			}
			hidePaymentDetails.click();
			Assert.assertTrue(paymentDetailsDisplayed,
					"Payment details are not correctly displayed on Show Details - Autopay COnfirmation page");
		} catch (Exception e) {
			Assert.fail("Payment details are not correctly displayed on Show Details - Autopay COnfirmation page");
		}
		return this;
	}

}
