package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class EBillApi extends ApiCommonLib{
	
	/***
	 * T-Mobile BillingV1 API’s.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/Accept'
     *   - $ref: '#/parameters/interactionid'
	 * @return
	 * @throws Exception 
	 */
    public Response billList(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildEBillAPIHeader(apiTestData, tokenMap);
		String resourceURL = "services/rest/listbills/"+tokenMap.get("billNo");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }
    
    public Response printBill(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildEBillAPIHeader(apiTestData, tokenMap);
		String resourceURL = "services/rest/printbill/"+tokenMap.get("statementId")+"/"+tokenMap.get("billNo")+"/BAN/"+tokenMap.get("ban");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }
    
    public Response printSummary(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildEBillAPIHeader(apiTestData, tokenMap);
		String resourceURL = "services/rest/printsummary/"+tokenMap.get("statementId")+"/"+tokenMap.get("billNo")+"/MSISDN/"+tokenMap.get("msisdn");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }
    
    private Map<String, String> buildEBillAPIHeader(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("Cache-Control", "no-cache");
		//headers.put("ban", tokenMap.get("ban"));
		//headers.put("msisdn",tokenMap.get("msisdn"));
		headers.put("Authorization", getAuthHeader());
		//headers.put("X-B3-TraceId", "PaymentsAPITest123");
		//headers.put("X-B3-SpanId", "PaymentsAPITest456");
		//headers.put("transactionid","PaymentsAPITest");
		return headers;
	}
}
