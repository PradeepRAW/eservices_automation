package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class BringYourOwnPhonePage extends TmngCommonPage {

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	private WebElement about;
	
	@FindBy(css = "[ng-model*='vm.imeiFormModel.imeiModal']")
	private WebElement imeiTextBox;
	
	@FindBy(css = "[ng-click*='submit']")
	private WebElement checkThisPhoneCTA;
	
	@FindBy(css = "[aria-label='Buy SIM']")
	private WebElement buySim;

	private final String pageUrl = "resources/bring-your-own-phone";

	public BringYourOwnPhonePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the BringYourOwnPhonePage class instance.
	 */
	public BringYourOwnPhonePage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Navigated to Bring your own phone Page");
		} catch (NoSuchElementException e) {
			Assert.fail("Bring your own phone Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public BringYourOwnPhonePage verifyPageUrl() {
		try {
			waitFor(ExpectedConditions.urlContains(pageUrl),60);
		} catch (Exception e) {
			Assert.fail("Bring your own phone Page not loaded");
		}
		return this;
	}
	
	/**
	 * Set IMEI NUmber
	 * 
	 * @param imeiNumber
	 */
	public BringYourOwnPhonePage setIMEINumber(String option) {

		try {
			moveToElement(imeiTextBox);
			sendTextData(imeiTextBox, option);
		} catch (Exception e) {
			Assert.fail("IMEI NUmber Element not set");
		}
		return this;
	}
	
	/**
	 * Click on Check this Phone CTA
	 *
	 */
	public BringYourOwnPhonePage clickCheckThisPhoneCTA() {
		try {
			clickElementWithJavaScript(checkThisPhoneCTA);
			Reporter.log("Clicked on Check this Phone CTA");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Check this Phone CTA");
		}
		return this;
	}
	
	
	/**
	 * Click on Buy SIM CTA
	 *
	 */
	public BringYourOwnPhonePage clickBuySIMCTA() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(buySim);
			Reporter.log("Clicked on Buy Sim CTA");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Buy Sim CTA");
		}
		return this;
	}

}
