/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author csudheer
 *
 */
public class ODFDataPassReviewPage extends CommonPage {

	@FindBy(css = "span[class*='vertical-medium']")
	private List<WebElement> addedOrRemovedItemsText;

	@FindBy(css = "div[class*='items-baseline']")
	private List<WebElement> removedItemsList;

	@FindBy(css = "span[class='Display3']")
	private List<WebElement> reviewText;

	@FindBy(css = "a[class*='body-link cursor']")
	private WebElement clickChangeDate;

	@FindBy(css = "button[class*='SecondaryCTA blackCTA']")
	private WebElement cancelButton;

	@FindBy(css = "button[class*='SecondaryCTA-accent blackCTA']")
	private WebElement overlayCancelButton;

	@FindBy(xpath = "//div/button[contains(text(),'Select')]")
	private WebElement overlaySelectButton;

	@FindAll({ @FindBy(xpath = "//button[text()='Select']"),
			@FindBy(css = "button[class*='PrimaryCTA-accent pull-right ml-2']") })
	private WebElement clickselect;

	@FindBy(css = "span[class*='no-padding black Display3']")
	private WebElement calendertitle;

	@FindBy(xpath = "//div[contains(text(),'Immediately')]")
	private WebElement immediately;

	@FindBy(xpath = "//div[contains(text(),'Today')]")
	private WebElement today;

	@FindBy(xpath = "//div[contains(text(),'Pick a date')]")
	private WebElement pickADate;

	@FindBy(css = "div[class*='Display5']")
	private WebElement pickADateTitle;
	
	@FindBy(css = "div.Display3")
	private WebElement mobilePickADateTitle;

	@FindBy(css = "button[class='PrimaryCTA-accent pull-right ml-2']")
	private WebElement pickUpDateselect;

	@FindBy(css = "label[for*='PickDate']")
	private WebElement pickUpDateRadioButton;

	@FindBy(xpath = "//div[contains(text(),'These changes will begin immediately')]")
	private WebElement textbelowImmediately;

	@FindBy(xpath = "//button[contains(@class,'disabled')]//..//..//div[@class='text-center']//button[contains(@class,'background body-bold') and not(contains(@class,'disabled'))]")
	private List<WebElement> calendarNextDate;

	@FindBy(css = "span[class='arrow-right pull-right']")
	private List<WebElement> datePickerRightBtn;

	@FindBy(css = "div[class='padding-top-xsmall'] span[class='body']")
	private WebElement effectiveDate;

	@FindBy(css = "span[class='H6-heading']")
	private List<WebElement> increaseDecreaseOrNoChange;

	@FindBy(id = "Today_Billing_Cycle")
	private WebElement todayDate;

	@FindBy(xpath = "//input[@id='Today_Billing_Cycle']//..//..//..//..//div")
	private List<WebElement> todayText;

	@FindBy(xpath = "//input[@id='Next_Billing_Cycle']//..//label")
	private WebElement nextBillCycleRadioBtn;

	@FindBy(xpath = "//input[@id='Next_Billing_Cycle']//..//..//..//..//div")
	private List<WebElement> nextBillCycleText;

	@FindBy(css = "p[class*='H6-heading']")
	private List<WebElement> priceAndTaxText;

	@FindBy(css = "span[class='legal d-inline-block black']")
	private WebElement reviewPageLegaleseTerms;

	@FindBy(css = "p.legal.black")
	private WebElement reviewPageLegaleseText;

	@FindBy(css = "span[class*='legal-bold-link']")
	private List<WebElement> legalLinks;

	@FindBy(xpath = "//div/button[contains(text(),'Back')]")
	private WebElement backButton;

	@FindBy(css = "button[class='PrimaryCTA full-btn-width float-md-left float-lg-left float-xl-left']")
	private WebElement submitbutton;

	@FindBy(css = "span[class*='arrow-back']")
	private WebElement reviewPageBackButton;

	@FindBy(css = ".padding-top-small p.body")
	private List<WebElement> proRateMessage;

	@FindBy(css = "div[class*='Display6']")
	private List<WebElement> effectiveDateRadioBtns;
	
	@FindBy(css = "span[class*='Display6']")
	private WebElement datapassName;

	@FindBy(css = "span.black.Display3")
	private WebElement effectiveDatePopup;

	@FindBy(css = "a.body-link")
	private WebElement changeDateLink;

	@FindBy(css = "span[class*='pull-right H6-heading']")
	private WebElement removedAddOnsPriceText;

	@FindBy(xpath = "//*[@class='body black']")
	private WebElement unblockRoamingMsg;

	@FindBy(linkText = "unblock roaming")
	private WebElement unblockRoaming;

	@FindBy(xpath = "//button[@class='PrimaryCTA full-btn-width']")
	private WebElement continueButton;

	@FindBy(xpath = "//span[contains(text(),'Starts Immediately')]")
	private WebElement startsimmediatlyText;

	@FindBy(css = ".col-12.col-md-10.offset-md-1.padding-top-large")
	private WebElement expandArrowForDescriptionOfDataPass;

	@FindBy(css = ".body.padding-bottom-medium.padding-top-small")
	private WebElement descriptionOfDataPass;

	@FindBy(css = ".col-12.col-md-10.offset-md-1.padding-top-large")
	private WebElement collapseArrowForDescriptionOfDataPass;

	@FindBy(css = ".col-12.col-md-10.offset-md-1.padding-top-large")
	private WebElement caretexpandedforDeepLink;

	@FindBy(css = "span.body.padding-r-6")
	private WebElement dataPassPrice;

	@FindBy(css = "")
	private WebElement taxesAndFeeAmount;

	@FindBy(css = "span.Display4.pr-4")
	private WebElement totalAmount;

	@FindBy(css = "div.TMO-COMMON-BLADE")
	private WebElement upfrontPaymentMethodBlade;

	@FindBy(xpath = "//div[contains(text(),'Edit payment method')]")
	private WebElement editPaymentMethod;

	@FindBy(css = "span[aria-label='Add a card clickable']")
	private WebElement addCardDetails;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement backCTA;

	@FindBy(css = "p.Display3.mb-0")
	private WebElement cardHeaderText;

	@FindBy(id = "cardName")
	private WebElement nameOnCard;

	@FindBy(id = "creditCardNumber")
	private WebElement creditcardNo;

	@FindBy(id = "expirationDate")
	private WebElement expirationDate;

	@FindBy(id = "cvvNumber")
	private WebElement cvvNumber;

	@FindBy(id = "zipCode")
	private WebElement zipCode;

	@FindBy(css = "button.PrimaryCTA.full-btn-width ")
	private WebElement continueCTA;

	@FindBy(css = "button.PrimaryCTA ")
	private WebElement agreeandSubmitbutton;

	@FindBy(xpath = "//span[text()='New add-ons']//following::span[contains(@class,'Display6')]")
	private List<WebElement> newAddOnsList;

	@FindBy(css = "span.pull-left.H6-heading.pull-left")
	private List<WebElement> newRemovedAddOnsHeader;

	@FindBy(css = "div.col-4.col-sm-2.col-lg-2")
	private List<WebElement> price;

	@FindBy(css = "div.col-12.text-center span.H6-heading")
	private WebElement txtPriceIncreaseDecrease;

	@FindBy(css = "div.text-center.padding-top-xsmall")
	private WebElement priceIncreaseDecrease;

	@FindBy(css = "p[class*='color-red']")
	private WebElement serviceProRateMessage;

	private final String pageUrl = "/addcard";

	private By listOfCurrentNextBillCycleDates = By.xpath("(//div[contains(@class,'col-11 offset-1')])[%s]");
	private By serviceTextByPrice = By.xpath("//*[contains(text(),'%s')]//..//..//p");

	public ODFDataPassReviewPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Wait For Spinner Invisible
	 */
	public void reviewPageWaitForSpinner() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("spin-loader")));
	}

	/**
	 * Verify Data Pass List page is loaded
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public ODFDataPassReviewPage verifyReviewPageOfManageDataAndAddOnsPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementBytext(reviewText, "Review your order"), "Review Page of AddOns is loaded");
			Reporter.log("Manage data and AddOns page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Manage data and AddOns page is not displayed");
		}
		return this;
	}

	/**
	 * Verify REview & Pay page is loaded
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public ODFDataPassReviewPage verifyReviewAndPAyPage() {
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementBytext(reviewText, "Review & Pay"), "Review & Pay page is loaded");
			Reporter.log("Review & Pay page is loaded is  displayed.");
		} catch (Exception e) {
			Assert.fail("Review & Pay page is loaded is not displayed");
		}
		return this;
	}

	/**
	 * Verify Removed Items Text
	 * 
	 * @return
	 */
	public ODFDataPassReviewPage verifyRemovedItemsText() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Verify.assertTrue(verifyElementBytext(addedOrRemovedItemsText, "Removed add-ons"),
					"Removed Items text is not displayed");
			Reporter.log("Removed Items text is displayed");
		} catch (Exception e) {
			Assert.fail("Removed Items text is not displayed");
		}
		return this;
	}

	/**
	 * Verify added Items Text
	 * 
	 * @return
	 */
	public ODFDataPassReviewPage verifyAddedItemsText() {
		checkPageIsReady();
		try {
			Verify.assertTrue(verifyElementBytext(addedOrRemovedItemsText, "New add-ons"),
					"New Add-ons Items text is not displayed");
			Reporter.log("New Add-ons Items text is displayed");
		} catch (Exception e) {
			Assert.fail("Added Items text is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage clickChangeDateLink() {
		try {
			checkPageIsReady();
			clickChangeDate.click();
			Reporter.log("Change Date CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Change Date CTA is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage clickChangeDateLinknotdisplayed() {
		try {
			checkPageIsReady();
			if (clickChangeDate.isDisplayed())
				;
			{
				Assert.fail("Change Date link is displayed");
			}
		} catch (Exception e) {
			Reporter.log("Change Date link is not displayed");
		}
		return this;
	}

	/**
	 * Verify Removed item names
	 * 
	 * @return
	 */
	public ODFDataPassReviewPage verifyRemovedItemsNames(String itemName) {
		try {
			boolean isElementDisplayed = false;
			for (WebElement webElement : removedItemsList) {
				if (itemName.contains(webElement.getText())) {
					isElementDisplayed = true;
					break;
				}
			}
			Assert.assertTrue(isElementDisplayed, "Removed Service name is not displayed");
			Reporter.log("Removed Service names" + itemName + "displayed");
		} catch (Exception e) {
			Assert.fail("Removed Service names" + itemName + "not displayed");
		}
		return this;
	}

	/**
	 * Verify review Text
	 * 
	 * @return
	 */
	public boolean verifyReviewText() {
		checkPageIsReady();
		return verifyElementBytext(reviewText, "Review your order");
	}

	public ODFDataPassReviewPage verifyPriceText(String locatorType) {
		try {

			if (verifyElementBytext(addedOrRemovedItemsText, "Removed add-ons")) {
				String removedPrice = removedAddOnsPriceText.getText()
						.substring(0, removedAddOnsPriceText.getText().indexOf('.')).replaceAll("[$,]", "");
				String price = Integer.toString(Integer.parseInt(locatorType) - Integer.parseInt(removedPrice));
				Assert.assertTrue(verifyElementBytext(reviewText, price), "Price is not displayed");
				Reporter.log("Verified Your monthly bill will increase by " + locatorType + "text is displayed");
			} else {
				Assert.assertTrue(verifyElementBytext(reviewText, locatorType), "Price is not displayed");
				Reporter.log("Verified Your monthly bill will increase by " + locatorType + "text is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Monthly bill will increase by is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage clickCancelButton() {
		try {
			cancelButton.click();
			Reporter.log("Cancel CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Cancel CTA is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage clickOverlaySelectButton() {
		try {
			overlaySelectButton.click();
			Reporter.log("Overlay Select CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Overlay Select CTA is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage clickOverlayCancelButton() {
		try {
			overlayCancelButton.click();
			Reporter.log("Overlay Cancel CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Overlay Cancel CTA is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage clickOnSelectInCalenderPage() {
		try {
			clickselect.click();
			Reporter.log("Select CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Select CTA is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage clickPickUpDateRadioButton() {
		try {
			pickUpDateRadioButton.click();
			Reporter.log("Pick up Date Radio button is clicked");
		} catch (Exception e) {
			Assert.fail("Pick up Date Radio button is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage pickupdateselectbutton() {
		try {
			pickUpDateselect.click();
			Reporter.log("Pick up Date select button is clicked");
		} catch (Exception e) {
			Assert.fail("Pick up Date select button is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyCalenderTitle(String msg) {
		checkPageIsReady();
		try {
			Verify.assertTrue(calendertitle.getText().contains(msg), "Effective Date is not displayed");
		} catch (Exception e) {
			Assert.fail("Calender is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyImmediately(String msg) {
		checkPageIsReady();
		try {
			Verify.assertTrue(immediately.getText().contains(msg), "Immediately title is not displayed");
		} catch (Exception e) {
			Assert.fail("Immediately title is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyToday(String msg) {
		checkPageIsReady();
		try {
			Assert.assertTrue(today.getText().contains(msg), "Today title is not displayed");
		} catch (Exception e) {
			Assert.fail("today title is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyPickaDate(String msg) {
		try {
			Assert.assertTrue(verifyElementBytext(effectiveDateRadioBtns, msg), "Pick a date is not displayed");						
			Reporter.log("Pick a date title is displayed");
		} catch (Exception e) {
			Assert.fail("Pick a date is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage pickADateTitle(String msg) {
		try {
			waitForSpinnerInvisibility();	
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(mobilePickADateTitle.getText().contains(msg),"Pick a Date title is not displayed");
			}
			else{
				Assert.assertTrue(pickADateTitle.getText().contains(msg), "Pick a Date title is not displayed");				
			}
		} catch (Exception e) {
			Assert.fail("Pick a date is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyTextBelowImmediatelyTitle(String msg) {
		checkPageIsReady();
		try {
			Verify.assertTrue(textbelowImmediately.getText().contains(msg),
					"These changes will begin immediately text is not dispalyed");
		} catch (Exception e) {
			Assert.fail("Text below Immediately is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage selectNextDate() {
		try {
			for (WebElement nextDate : calendarNextDate) {
				if (!nextDate.getText().isEmpty()) {
					if (nextDate.getText().contains(calendarLastDate())) {
						clickElement(datePickerRightBtn);
					} else {
						nextDate.click();
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Next date is not selected");
		}
		return this;
	}

	public String calendarLastDate() {
		Calendar calendar = Calendar.getInstance();
		int lastDate = calendar.getActualMaximum(Calendar.DATE);
		calendar.set(Calendar.DATE, lastDate);
		int lastDay = calendar.get(Calendar.DAY_OF_WEEK);
		return Integer.toString(lastDay);
	}

	public String getEffectiveDate() {
		return effectiveDate.getText();
	}

	public String verifyEffictiveDateCancel(String dateBeforeModify) {
		clickChangeDateLink();
		verifyElementBytext(todayText, "Current bill cycle");
		verifyElementBytext(todayText, dateBeforeModify);
		nextBillCycleRadioBtn.click();
		verifyElementBytext(nextBillCycleText, "Next bill cycle");
		overlayCancelButton.click();
		return dateBeforeModify;
	}

	public ODFDataPassReviewPage verifyEffectiveDate() {
		try {
			Assert.assertTrue(effectiveDate.isDisplayed());
			Reporter.log("Effective date is displayed");
		} catch (Exception e) {
			Assert.fail("Effective date is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyNextBillCycle() {
		try {
			String nextBillCycleDatenumber;
			clickChangeDateLink();
			nextBillCycleRadioBtn.click();
			verifyElementBytext(nextBillCycleText, "Next bill cycle");
			WebElement nextBillCycleDate = getDriver()
					.findElement(getNewLocator(listOfCurrentNextBillCycleDates, "" + 2));
			nextBillCycleDatenumber = nextBillCycleDate.getText();
			clickOverlaySelectButton();
			Assert.assertTrue(getEffectiveDate().contains(nextBillCycleDatenumber));
			Reporter.log("Verified Next Bill Cycle date is displayed");
		} catch (Exception e) {
			Assert.fail("Effective date does not contain next bill cycle date number");
		}
		return this;
	}

	/**
	 * Verify your Monthly Bill Will Increase
	 */
	public ODFDataPassReviewPage verifyMonthlyBillIncrease(String name) {
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementBytext(increaseDecreaseOrNoChange, name),
					"Your monthly bill will increase by text is not displayed");
			Reporter.log("Verified " + name + " text is displayed");
		} catch (Exception e) {
			Assert.fail("Your monthly bill will increase by text is not displayed");
		}
		return this;
	}

	/**
	 * Verify your Monthly Bill not displaying
	 */
	public ODFDataPassReviewPage verifyMonthlyBillIsNotDispalying() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!CollectionUtils.isNotEmpty(increaseDecreaseOrNoChange)
					&& CollectionUtils.sizeIsEmpty(increaseDecreaseOrNoChange));
			Reporter.log("Verified Monthly billing is not displayed");
		} catch (Exception e) {
			Assert.fail("Your monthly billing text is displayed");
		}
		return this;
	}

	/**
	 * Verify your Monthly Bill Will DecreaseBy
	 */
	public ODFDataPassReviewPage verifyMonthlyBillDecrease() {
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementBytext(increaseDecreaseOrNoChange, "Your monthly cost will decrease by"),
					"Your monthly bill will decreased by is not displayed");
			Reporter.log("Verified Your monthly bill will decreased by text is displayed");
		} catch (Exception e) {
			Assert.fail("Your monthly bill will decreased by is not displayed");
		}
		return this;
	}

	/**
	 * Verify your Monthly Bill Will Increase Or DecreaseBy
	 */
	public ODFDataPassReviewPage verifyMonthlyBillNoChange(String name) {
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementBytext(increaseDecreaseOrNoChange, name),
					"There is no change to your monthly bill is not displayed");
			Reporter.log("Verified " + name + " text is displayed");
		} catch (Exception e) {
			Assert.fail("There is no change to your monthly bill is not displayed");
		}
		return this;
	}

	public String verifyServiceTextByPrice(String name) {
		return getDriver().findElement(getNewLocator(serviceTextByPrice, name)).getText();
	}

	public ODFDataPassReviewPage verifyPriceAndTaxText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementBytext(priceAndTaxText, "+ tax"));
			Reporter.log("Verified + tax text is displayed");
		} catch (Exception e) {
			Assert.fail("+ tax text is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyLegaleseTerms(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(reviewPageLegaleseTerms.getText().contains(msg), "Legalese terms is not displayed");
		} catch (Exception e) {
			Assert.fail("Legalese terms is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyLegaleseText(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(reviewPageLegaleseText.getText().contains(msg), "Legalese text is not displayed");
		} catch (Exception e) {
			Assert.fail("Legalese text is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyLegaleseLinks() {
		try {
			checkPageIsReady();
			clickElementBytext(legalLinks, "Service Agreement");
			Assert.assertTrue(verifyElementBytext(reviewText, "Service Agreement"));
			Reporter.log("Service Agreement is not displayed");
			backButton.click();
			reviewPageWaitForSpinner();
			checkPageIsReady();

			clickElementBytext(legalLinks, "Electronic Signature Terms");
			reviewPageWaitForSpinner();
			checkPageIsReady();
			Assert.assertTrue(verifyElementBytext(reviewText, "Electronic-signature terms"));
			Reporter.log("Electronic-signature terms is not displayed");
			backButton.click();
			reviewPageWaitForSpinner();
			checkPageIsReady();

			clickElementBytext(legalLinks, "Terms & Conditions;");
			checkPageIsReady();
			switchToSecondWindow();
			verifyCurrentPageURL("TermsAndConditions", "terms-and-conditions");
		} catch (Exception e) {
			Assert.fail("Legalese Links  is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage clickOnReviewPageBackButton() {
		try {
			checkPageIsReady();
			reviewPageBackButton.click();
			Reporter.log("Clicked on Review page back button");

		} catch (Exception e) {
			Assert.fail("Review page back button is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage clickOnReviewPageBrowserBackButton() {
		try {
			getDriver().navigate().back();
			Reporter.log("Clicked on Review page Browser back button");

		} catch (Exception e) {
			Assert.fail("Browser back button is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyProRateMessage() {
		try {
			Verify.assertTrue(proRateMessage.get(0).getText().contains(
					"Changes made in the middle of a billing cycle will result in full monthly charges of the new feature. The full data bucket will be available to use during this bill cycle."));
		} catch (Exception e) {

			Assert.fail("Pro Rate Message is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyProRateMessageIsNotDisplaying() {
		try {
			Assert.assertTrue(CollectionUtils.sizeIsEmpty(proRateMessage));
			Reporter.log("Verified Pro rate message is not displayed ");
		} catch (Exception e) {
			Assert.fail("Pro rate message is displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage clickOnNextBillCycleRadioBtn() {
		try {
			nextBillCycleRadioBtn.click();
			Reporter.log("Next Bill Cycle radio button is selected");
		} catch (Exception e) {
			Assert.fail("Next Bill Cycle radio button is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage clickOnAgreeandSubmitButton() {
		try {
			submitbutton.click();
			Reporter.log("Clicked on Agree and submit button");

		} catch (Exception e) {
			Assert.fail("Agree and submit button is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyDataPassNameAndTextReviewpage(String dataPassName, String price) {

		Assert.assertEquals(datapassName.getText(), dataPassName);
		Assert.assertTrue(priceAndTaxText.get(0).getText().contains(price));
		Reporter.log("Verified datapass name and price");
		return this;
	}

	public ODFDataPassReviewPage clickOnChangeDateLink() {
		try {
			checkPageIsReady();
			changeDateLink.click();
			Reporter.log("Clicked on Change date link");
		} catch (Exception e) {
			Assert.fail("Change date link is not displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyEffectiveDatePopUpDisplayed() {
		try {
			checkPageIsReady();
			effectiveDatePopup.isDisplayed();
			Reporter.log("Effective ate PopUp is displayed");

		} catch (Exception e) {
			Assert.fail("Effective ate PopUp isnot displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyNextBillCycleRadioBtnDisplayed() {
		try {
			checkPageIsReady();
			for (WebElement element : effectiveDateRadioBtns) {
				if (element.getText().contains("Next bill cycle")) {
					break;
				}
			}
			Reporter.log("Next bill cycle radio button is displayed");

		} catch (Exception e) {
			Assert.fail("Next bill cycle radio button isnot displayed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyIstodaySelectedEffectiveDate() {
		try {
			Assert.assertTrue(todayDate.isSelected());
			Reporter.log("Today is selected by default");
		} catch (Exception e) {
			Assert.fail("Today is not selected by default");
		}
		return this;
	}

	public boolean verifydateOptions(String option) {
		boolean display = false;
		try {
			checkPageIsReady();
			for (WebElement element : effectiveDateRadioBtns) {
				if (element.getText().contains(option)) {
					break;
				}
			}
			display = true;
			Reporter.log(option + "is displayed");

		} catch (Exception e) {
			Assert.fail(option + "is not displayed");
		}
		return display;
	}

	public ODFDataPassReviewPage verifydateOptionsforMcafeeServiceOnReviewPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifydateOptions("Today"), "Today option not dispalyed");
			Assert.assertFalse(verifydateOptions("Next bill cycle"),
					"Next bill cycle option is displayed is not expected");

		} catch (Exception e) {
			Assert.fail("options are not dispalyed");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyUnblockRoamingMessage() {
		try {
			unblockRoamingMsg.isDisplayed();
			Reporter.log("Unblock Roaming Message is displayed.");
		} catch (Exception e) {
			Assert.fail("Unblock Roaming message is not displayed.");
		}
		return this;
	}

	public ODFDataPassReviewPage clickContinueButton() {
		try {
			continueCTA.click();
			Reporter.log("Continue CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Continue CTA is not clicked");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyUnblockRoamingMsg() {
		try {
			Assert.assertTrue(
					unblockRoamingMsg.getText().contains(
							"International roaming has been disabled. In order to purchase an international roaming data pass, you'll need to unblock roaming first."),
					"International roaming message is not displayed");
			Reporter.log("International roaming message is displayed");
		} catch (Exception e) {
			Assert.fail("International roaming message is not displayed");
		}
		return this;
	}

	/**
	 * Click Unblock Roaming
	 */
	public ODFDataPassReviewPage clickUnblockRoaming() {
		try {
			unblockRoaming.click();
			Reporter.log("Unblock Roaming radion button is clicked");
		} catch (Exception e) {
			Assert.fail("Unblock Roaming radion button is not found");
		}
		return this;
	}

	/**
	 * Verify Start immediately for Std/RES Datapass
	 */
	public ODFDataPassReviewPage verifyStartImmediately(String msg) {
		checkPageIsReady();
		try {
			Verify.assertTrue(startsimmediatlyText.getText().contains(msg));
			Reporter.log("Starts Immediately title is displayed");
		} catch (Exception e) {
			Assert.fail("Starts Immediately title is not displayed");
		}
		return this;
	}

	/**
	 * Expend for description of data Pass
	 */
	public ODFDataPassReviewPage expandArrowForDescriptionOfDataPass() {
		checkPageIsReady();
		try {
			clickElementWithJavaScript(expandArrowForDescriptionOfDataPass);
			Reporter.log("expaned International Data Pass.");
		} catch (Exception e) {
			Assert.fail("International Data Pass not found");
		}
		return this;
	}

	/**
	 * Collapse for description of data Pass
	 */
	public ODFDataPassReviewPage collapseArrowForDescriptionOfDataPass() {
		checkPageIsReady();
		try {
			clickElementWithJavaScript(collapseArrowForDescriptionOfDataPass);
			Reporter.log("collapse International Data Pass.");
		} catch (Exception e) {
			Assert.fail("collapse International Data Pass not found");
		}
		return this;
	}

	/**
	 * Caret expended for description of DeepLink
	 */
	public ODFDataPassReviewPage caretExpandedForDeepLink() {
		checkPageIsReady();
		try {
			clickElementWithJavaScript(caretexpandedforDeepLink);
			Reporter.log("Caret expended for Deep Link");
		} catch (Exception e) {
			Assert.fail("Caret not expended for Deep Link");
		}
		return this;
	}

	/**
	 * Verify description of data Pass
	 */
	public ODFDataPassReviewPage checkWhetherDescriptionOfDataPassIsDisplayedOrNot() {
		try {
			descriptionOfDataPass.isDisplayed();
			Reporter.log("Description of DataPass is dispalyed");
		} catch (Exception e) {
			Assert.fail("Description of DataPass is not expanded");
		}
		return this;
	}

	/**
	 * Verify data Pass total Amount
	 */
	public ODFDataPassReviewPage verifyDataPassTotalAmount() {
		try {
			checkPageIsReady();
			float priceOfDataPass = Float.parseFloat(dataPassPrice.getText().replaceAll("[$]", ""));
			float PriceOftaxesAndFee = Float.parseFloat(taxesAndFeeAmount.getText().replaceAll("[$]", ""));
			float total = Float.parseFloat(totalAmount.getText().replaceAll("[$]", ""));
			float totalAmount = priceOfDataPass + PriceOftaxesAndFee;
			Reporter.log("The total amount is :" + totalAmount);
			if (totalAmount == total) {
				Reporter.log("TotalAmount is verified");
			} else {
				Reporter.log("Total is are not verified");
			}
		} catch (Exception e) {
			Assert.fail("Total is not dispalyed ");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyUpfrontPaymentMethod() {
		try {
			upfrontPaymentMethodBlade.isDisplayed();
			upfrontPaymentMethodBlade.getText().equals("Add payment method");
			Reporter.log("Verified Payment method");
		} catch (Exception e) {
			Assert.fail("Payment method component missing");
		}
		return this;
	}

	/**
	 * click add payment method blade
	 */
	public ODFDataPassReviewPage clickPaymentMethodBlade() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("screen-lock")));
			waitFor(ExpectedConditions.elementToBeClickable(upfrontPaymentMethodBlade));
			upfrontPaymentMethodBlade.click();
			Reporter.log("Clicked on payment method blade");
		} catch (Exception e) {
			Assert.fail("Payment method blade not found");
		}
		return this;
	}

	/**
	 * Verify Edit Payment Method Title
	 * 
	 * @return
	 */
	public ODFDataPassReviewPage verifyEditPaymentMethodText() {
		checkPageIsReady();
		try {
			editPaymentMethod.getText().equals("Editpaymentmethod");
			Reporter.log("Edit Payment Method Page  is displayed");
		} catch (Exception e) {
			Assert.fail("Edit Payment Method Title is not displayed");
		}
		return this;
	}

	/**
	 * 
	 * verify add card blade is displayed or not
	 */
	public ODFDataPassReviewPage verifyAddCardBlade() {
		try {
			Assert.assertTrue(addCardDetails.isDisplayed(),"Add  Card Details is not displaying");
			Reporter.log("add a card blade is diaplayed");
		} catch (Exception e) {
			Assert.fail("Add  Card Details is not displaying");
		}
		return this;
	}

	/**
	 * click on add card
	 */
	public ODFDataPassReviewPage clickAddCard() {
		try {
			addCardDetails.click();
			Reporter.log("Add card button found");
		} catch (Exception e) {
			Assert.fail("Add card not found");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyCardInformationPage(String msg) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(backCTA));
			verifyPageUrl();
			Assert.assertTrue(cardHeaderText.getText().trim().equals(msg));
			Reporter.log("Verified the Card info Header details");
		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ODFDataPassReviewPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Enter Card Name
	 */
	public ODFDataPassReviewPage enterCardName() {
		checkPageIsReady();
		try {
			nameOnCard.sendKeys("TestReleaseData");
			Reporter.log("Entered Card Name");
		} catch (Exception e) {
			Assert.fail("Failed to enter Card name");
		}
		return this;
	}

	/**
	 * Enter Card Number
	 */
	public ODFDataPassReviewPage enterCardNumber() {
		checkPageIsReady();
		try {
			creditcardNo.sendKeys("4012888888881881");
			Reporter.log("Entered Card Number");
		} catch (Exception e) {
			Assert.fail("Failed to enter Card number");
		}
		return this;
	}

	/**
	 * Enter Card Number
	 */
	public ODFDataPassReviewPage enterexpirationDate() {
		checkPageIsReady();
		try {
			expirationDate.sendKeys("22/22");
			Reporter.log("Entered Expiration Data");
		} catch (Exception e) {
			Assert.fail("Failed to enter expiration Date");
		}
		return this;
	}

	/**
	 * Enter CVV
	 */
	public ODFDataPassReviewPage enterCvv() {
		checkPageIsReady();
		try {
			cvvNumber.sendKeys("222");
			Reporter.log("Entered CVV");
		} catch (Exception e) {
			Assert.fail("Failed to enter CVV");
		}
		return this;
	}

	/**
	 * Enter Zip
	 */
	public ODFDataPassReviewPage enterZip() {
		checkPageIsReady();
		try {
			zipCode.sendKeys("98012");
			Reporter.log("Entered Zip");
		} catch (Exception e) {
			Assert.fail("Failed to enter Zip");
		}
		return this;
	}

	/**
	 * Click on continue button
	 */
	public ODFDataPassReviewPage clickContinueCTA() {
		checkPageIsReady();
		try {
			continueCTA.isEnabled();
			continueCTA.click();
			Reporter.log("continue button is enabled and clicked");
		} catch (Exception e) {
			Assert.fail("continue button is not enabled");
		}
		return this;
	}

	/**
	 * Click on Agree and continue button
	 */
	public ODFDataPassReviewPage agreeAndSubmitButton() {
		waitforSpinner();
		checkPageIsReady();
		try {
			agreeandSubmitbutton.isDisplayed();
			agreeandSubmitbutton.click();
			Reporter.log("agree and Submitbutton is displayed and clicked");
		} catch (Exception e) {
			Assert.fail("agreeandSubmitbuttonn is not displayed");
		}
		return this;
	}

	/**
	 * Verify new Add Ons List
	 */
	public ODFDataPassReviewPage verifyNewAddOnsListText(String name) {

		try {
			verifyElementBytext(newAddOnsList, name);
		} catch (Exception e) {
			Assert.fail(name + " is not displayed");
		}

		return this;
	}

	public ODFDataPassReviewPage verifyUpgradeNewAndRemovedAddOns(String removedAddOns, String newAddOns) {
		Double d1, d2;
		Double difference;
		try {
			Assert.assertTrue(newRemovedAddOnsHeader.get(0).getText().equals("New add-ons"),
					"New add-ons text is Not displayed");
			Reporter.log("New add-ons text is displayed");
			Assert.assertTrue(newAddOnsList.get(0).getText().contains(newAddOns),
					newAddOns + " is not displayed under New add-ons");
			Reporter.log("Upgraded Netfix text '" + newAddOns + "'  is displayed on New add-ons");

			Assert.assertTrue(newRemovedAddOnsHeader.get(1).getText().equals("Removed add-ons"),
					"Removed add-ons text is Not displayed");
			Reporter.log("Removed add-ons text is displayed");
			Assert.assertTrue(newAddOnsList.get(1).getText().contains(removedAddOns),
					removedAddOns + " is not displayed under Removed add-ons");
			Reporter.log("removed Netfix text '" + removedAddOns + "' is displayed on Removed add-ons");

			d1 = Double.parseDouble(getNumberValueFromString(newAddOns));
			d2 = Double.parseDouble(getNumberValueFromString(removedAddOns));
			difference = Math.abs(d1 - d2);

			// use if and else condition to check if price is on us or $4
			if (price.get(1).getText().equals("On us")) {
				try {
					Assert.assertTrue(
							String.valueOf(getNumberValueFromString(price.get(0).getText()))
									.contains(String.valueOf(difference)),
							"Price for " + newAddOns + " under New add-ons not mached. Expected price is " + difference
									+ "" + ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(price.get(0).getText())));
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is $" + difference + "/mo");
					Assert.assertTrue(price.get(1).getText().contains("On us"),
							"Price for " + removedAddOns + " under Removed add-ons not mached. Expected price is On us"
									+ ". Actual price displayed is " + price.get(1).getText());
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is On us ");
				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details  on addons Review Order Page");
				}
			} else {
				try {
					Assert.assertTrue(String.valueOf(getNumberValueFromString(price.get(0).getText())).contains("7"),
							"Price for " + newAddOns + " under New add-ons not mached. Expected price is $7.00 "
									+ ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(price.get(0).getText())));
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is $7.00" + "/mo");
					Assert.assertTrue(price.get(1).getText().contains("$4"),
							"Price for " + removedAddOns + " under Removed add-ons not mached. Expected price is $4"
									+ ". Actual price displayed is " + price.get(1).getText());
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is $4 ");
				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
				}
			}
		} catch (Exception e) {
			Assert.fail("Unable to verify add-ons on Review Order Page");
		}
		return this;
	}

	public ODFDataPassReviewPage verifyDowngradeNewAndRemovedAddOns(String removedAddOns, String newAddOns) {
		Double d1, d2;
		// verifying the text in the Review order page
		try {

			Assert.assertTrue(newRemovedAddOnsHeader.get(0).getText().contains("New add-ons"),
					"New add-ons text is Not displayed");
			Reporter.log("New add-ons text is displayed");
			Assert.assertTrue(newAddOnsList.get(0).getText().contains(newAddOns),
					newAddOns + " is not displayed under New add-ons");
			Reporter.log("Upgraded Netfix text '" + newAddOns + "'  is displayed on New add-ons");
			d1 = Double.parseDouble(getNumberValueFromString(newAddOns));
			
			Assert.assertTrue(newRemovedAddOnsHeader.get(1).getText().contains("Removed add-ons"),
					"Removed add-ons text is Not displayed");
			Reporter.log("Removed add-ons text is displayed");
			
			Assert.assertTrue(newAddOnsList.get(1).getText().contains(removedAddOns),
					removedAddOns + " is not displayed under Removed add-ons");
			Reporter.log("removed Netfix text '" + removedAddOns + "' is displayed on Removed add-ons");
			d2 = Double.parseDouble(getNumberValueFromString(removedAddOns));
			Double difference = Math.abs(d1 - d2);

			// use if and else condition to check if price is on us or $4

			if (price.get(0).getText().contains("On us")) {
				try {
					
					Assert.assertTrue(price.get(0).getText().contains("On us"),
							"Price for " + newAddOns + " under Removed add-ons not mached. Expected price is On us"
									+ ". Actual price displayed is " + price.get(0).getText());
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is On us ");
					
					Assert.assertTrue(
							String.valueOf(getNumberValueFromString(price.get(1).getText()))
									.contains(String.valueOf(difference)),
							"Price for " + removedAddOns + " under Remove add-ons not mached. Expected price is "
									+ difference + "" + ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(price.get(1).getText())));
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is $" + difference
							+ "/mo");
				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
				}
			} else {
				try {
					Assert.assertTrue(price.get(0).getText().contains("$4"),
							"Price for " + newAddOns + " under Removed add-ons not mached. Expected price is $4"
									+ ". Actual price displayed is " + price.get(0).getText());
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is $4 ");
					Assert.assertTrue(String.valueOf(getNumberValueFromString(price.get(1).getText())).contains("7"),
							"Price for " + removedAddOns + " under Remove add-ons not mached. Expected price is  $7.00"
									+ "" + ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(price.get(1).getText())));
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is $7.00/mo");

				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
				}
			}

		} catch (Exception e) {
			Assert.fail("Unable to verify add-ons on Review Order Page");
		}
		return this;
	}

	public String getNumberValueFromString(String string) {
		return string.replaceAll("[^0-9\\.]+", "");
	}

	public boolean checckIfStringIsDouble(String str) {
		boolean numeric = true;
		try {
			Double num = Double.parseDouble(str);
		} catch (NumberFormatException e) {
			numeric = false;
		}
		return numeric;
	}

	public ODFDataPassReviewPage verifyServiceProRateMessage() {

		Assert.assertTrue(
				serviceProRateMessage.getText().contains(
						"Changes made in the middle of a billing cycle will result in full monthly charges of the new feature and all international usage going forward will charge at the associated per minute rate. Any international usage prior to today will not be impacted by this change."),
				"Service Pro Rate Message is not displaying correctly");
		return this;
	}
}
