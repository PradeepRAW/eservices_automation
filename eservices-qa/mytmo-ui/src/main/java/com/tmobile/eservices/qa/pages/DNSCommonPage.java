package com.tmobile.eservices.qa.pages;

import java.net.URLDecoder;
import org.json.JSONObject;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import io.appium.java_client.AppiumDriver;

/**
 * This page contains all the DNS elements and methods
 * 
 * @author sputti2
 *
 */
public class DNSCommonPage extends CommonPage {

	private final String pageUrl = "/dns";
	
	// DNS Page Objects
	@FindBy(css=".heading-3")
	private WebElement dnsPageTitle;
	
	@FindBy(css="div:nth-child(2) > .fx-row-wrap")
	private WebElement dnsLegalContent;
	
	@FindBy(css="p:nth-child(1)")
	private WebElement dnsCopyMessage;
	
	@FindBy(css = "#learn-link-0")
	protected WebElement dnsLearnMoreLink;
	
	@FindBy(css="#mat-slide-toggle-1 .mat-slide-toggle-bar")
	private WebElement dnsBrandToggle;
	
	@FindBy(css="#mat-slide-toggle-2 .mat-slide-toggle-bar")
	private WebElement dnsLocalToggle;

	@FindBy(css = "#login-nav-link-0")
	protected WebElement dnsLoginLink;
	
	@FindBy(css=".“margin-vertical-sm” a")
	private WebElement loginAccountLink;
	
	@FindBy(xpath="//*[contains(text(),'Sharing of third part data')]")
	private WebElement unAuthneticatedContent;
	
	/**
	 * 
	 * @param webDriver
	 */
	public DNSCommonPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify DNS Page
	 * 
	 * @return
	 */
	public DNSCommonPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitforSpinner();
			waitFor(ExpectedConditions.urlContains(pageUrl));
		} catch (Exception e) {
			Assert.fail("DNS page is not displayed");
		}
		return this;
	}

	/**
	 * Verify DNS Page
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSPage() {
		
		switchToSecondWindow();
		getDriver().manage().window().maximize();
		waitforSpinner();
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			waitFor(ExpectedConditions.titleContains("dns"));
		} else {
			verifyPageLoaded();
		}
		Reporter.log("DNS page is displayed");
		return this;
	}

	/**
	 * Verify DNS Page Title
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSTitle() {
		try {
			Assert.assertTrue(isElementDisplayed(dnsPageTitle));
			Assert.assertTrue(dnsPageTitle.getText().equalsIgnoreCase("Do not sell my personal information"));
			Reporter.log("DNS Page Title - " +dnsPageTitle.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail("DNS Page Title - " +dnsPageTitle.getText()+"is not dispalyed");
		}
		return this;
	}
	/**
	 * Verify DNS Page Legal Content
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSLegalContent() {
		try {
			Assert.assertTrue(isElementDisplayed(dnsLegalContent));
			//Assert.assertTrue(dnsLegalContent.getText().equalsIgnoreCase("Third Party data sharing"));
			Reporter.log("DNS Page Legal Content - "+dnsLegalContent.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail("DNS Page Legal Content - " +dnsLegalContent.getText()+" is not dispalyed");
		}
		return this;
	}
	/**
	 * Verify DNS Copy Message
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSCopyMessage() {
		try {
			Assert.assertTrue(isElementDisplayed(dnsCopyMessage));
			Assert.assertTrue(dnsCopyMessage.getText().equalsIgnoreCase("We may collect data using marketing technologies made by other companies"));
			Reporter.log("DNS Copy Message - "+dnsCopyMessage.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail("DNS Copy Message - "+dnsCopyMessage.getText()+" is not dispalyed");
		}
		return this;
	}
	/**
	 * Verify DNS Page Learn More Link
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSLearnMoreLink() {
		try {
			Assert.assertTrue(isElementDisplayed(dnsLearnMoreLink));
			Assert.assertTrue(dnsLearnMoreLink.getText().equalsIgnoreCase("Learn More"));
			Reporter.log(dnsLearnMoreLink.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail(dnsLearnMoreLink.getText()+" is not dispalyed");
		}
		return this;
	}
	/**
	 * Click DNS Page Learn More Link
	 */
	public DNSCommonPage clickLearnMoreLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dnsLearnMoreLink));
			dnsLearnMoreLink.click();
			Reporter.log("Clicked on Learn More Link");
		} catch (Exception e) {
			Reporter.log("Click on Learn More Link failed");
			Assert.fail("Click on Learn More Link failed");
		}
		return this;
	}

	/**
	 * Verify Learn More page.
	 *
	 * @return the Learn More class instance.
	 */
	public DNSCommonPage verifyLearnMorePage() {
		try {
			checkPageIsReady();
			verifyPageUrl("");
			Reporter.log("Learn More page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Learn More page not displayed");
		}
		return this;
	}
	/**
	 * Verify DNS Brand Toggle
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSBrandToggle() {
		try {
			Assert.assertTrue(isElementDisplayed(dnsBrandToggle));
			Reporter.log("Brand Toggle is displayed");
		} catch (Exception e) {
			Assert.fail("Brand Toggle is not dispalyed");
		}
		return this;
	}
	/**
	 * Click Brand Toggle
	 */
	public DNSCommonPage clickBrandToggle() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dnsBrandToggle));
			clickElement(dnsBrandToggle);
			Reporter.log("Clicked on Brand Toggle");
		} catch (Exception e) {
			Reporter.log("Click on Brand Toggle failed");
			Assert.fail("Click on Brand Toggle failed");
		}
		return this;
	}
	/**
	 * Verify DNS Local Toggle
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSLocalToggle() {
		try {
			Assert.assertTrue(isElementDisplayed(dnsLocalToggle));
			Reporter.log("Local Toggle is displayed");
		} catch (Exception e) {
			Assert.fail("Local Toggle is not dispalyed");
		}
		return this;
	}
	/**
	 * Click Local Toggle
	 */
	public DNSCommonPage clickLocalToggle() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dnsLocalToggle));
			clickElement(dnsLocalToggle);
			Reporter.log("Clicked on Local Toggle");
		} catch (Exception e) {
			Reporter.log("Click on Local Toggle failed");
			Assert.fail("Click on Local Toggle failed");
		}
		return this;
	}

	/**
	 * Verify DNS Login Link
	 * 
	 * @return
	 */
	public DNSCommonPage verifyDNSLoginLink() {
		try {
			Assert.assertTrue(isElementDisplayed(dnsLoginLink));
			Assert.assertTrue(dnsLoginLink.getText().equalsIgnoreCase("Login"));
			Reporter.log(dnsLoginLink.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail(dnsLoginLink.getText()+" is not dispalyed");
		}
		return this;
	}
	/**
	 * Click DNS Login link
	 */
	public DNSCommonPage clickDNSLoginLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dnsLoginLink));
			clickElement(dnsLoginLink);
			Reporter.log("Clicked on DNS Login Link");
		} catch (Exception e) {
			Reporter.log("Click on DNS Login link failed");
			Assert.fail("Click on DNS Login link failed");
		}
		return this;
	}	
	/**
	 * Get Local Do Not Sell Setting value from DNS cookie
	 */
	public DNSCommonPage getDNSSettingValueFromDNSCookie() {
		try {			
			String encoded = getDriver().manage().getCookieNamed("dns").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			String localdnssetting1 = jsonObject.getString("LocaldoNotSellSetting");
			//Assert.assertTrue(localdnssetting1.equalsIgnoreCase("localSet"));
			Reporter.log("Local DNS Settings Value as" + localdnssetting1 +" is retrieved from DNS cookie on DNS Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve Local DNS Settings Value from DNS cookie on DNS Page");
		}
		return this;
	}	
	/**
	 * Get Site value from DNS cookie
	 */
	public DNSCommonPage getSiteValueFromDNSCookie() {
		try {
			String encoded = getDriver().manage().getCookieNamed("dns").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			String site1 = jsonObject.getString("Site");
			//Assert.assertTrue(site1.equalsIgnoreCase("sellWeb"));
			Reporter.log("Site Value as "+site1 + " retrieved from DNS cookie on DNS Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve Site Value from cookies on DNS Page");
		}
		return this;
	}	
	/**
	 * Get Authorization value from DNS cookie
	 */
	public DNSCommonPage getAuthorizationValueFromDNSCookie() {
		try {
			String encoded = getDriver().manage().getCookieNamed("dns").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			String authorization1 = jsonObject.getString("Authorization");
			//Assert.assertTrue(authorization1.equalsIgnoreCase(""));
			Reporter.log("Authorization Value " + authorization1+ " is retrieved from DNS cookie on DNS Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve Authorization Value from cookies on DNS Page");
		}
		return this;
	}
	/**
	 * Get User ID value from DNS cookie
	 */
	public DNSCommonPage getUserIDValueFromDNSCookie() {
		try {
			String encoded = getDriver().manage().getCookieNamed("dns").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			String userID1 = jsonObject.getString("userID");
			//Assert.assertTrue(userID1.equalsIgnoreCase(""));
			Reporter.log("userID Value " +userID1 + "  retrieved from DNS cookie on DNS Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve userID Value from cookies on DNS Page");
		}
		return this;
	}
	/**
	 * Get Origin_URL value from DNS cookie
	 */
	public DNSCommonPage getOriginURLValueFromDNSCookie() {
		try {
			String encoded = getDriver().manage().getCookieNamed("dns").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			String OriginURL1 = jsonObject.getString("Origin_URL");
			//Assert.assertTrue(userID1.equalsIgnoreCase(""));
			Reporter.log("OriginURL Value as "+OriginURL1 + " is retrieved from DNS cookie on DNS Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve OriginURL Value from cookies on DNS Page");
		}
		return this;
	}
	/**
	 * Get ID Type value from DNS cookie
	 */
	public DNSCommonPage getIDTypeValueFromDNSCookie() {
		try {
			String encoded = getDriver().manage().getCookieNamed("dns").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			String idType1 = jsonObject.getString("idType");
			//Assert.assertTrue(idType1.equalsIgnoreCase(""));
			Reporter.log("idType Value as" + idType1+"  retrieved from DNS cookie on DNS Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve idType Value from cookies on DNS Page");
		}
		return this;
	}
	/**
	 * Get Brand value from DNS cookie
	 */
	public DNSCommonPage getBrandValueFromDNSCookie() {
		try {
			String encoded = getDriver().manage().getCookieNamed("dns").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			String brand1 = jsonObject.getString("Brand");
			//Assert.assertTrue(brand1.equalsIgnoreCase("brandInput"));
			Reporter.log("Brand Value as " + brand1 +" retrieved from DNS cookie on DNS Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve Brand Value from cookies on DNS Page");
		}
		return this;
	}
	
	/**
	 * Verify DNS Page
	 * 
	 * @return
	 */
	public DNSCommonPage verifyBrandingOnDNSPage() {
		checkPageIsReady();
		waitforSpinner();
		return this;
	}
	
	/**
	 * Click login account link
	 */
	public DNSCommonPage clickLoginAccountLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(loginAccountLink));
			clickElement(loginAccountLink);
			Reporter.log("Clicked on login account link");
		} catch (Exception e) {
			Reporter.log("Click on login account link failed");
			Assert.fail("Click on login account link failed");
		}
		return this;
	}	
	
	/**
	 * Verify DNS Page for UnAuthneticated 
	 *  
	 * @return
	 */
	public DNSCommonPage verifyDNSUnAuthneticatedContent() {
		try {
			Assert.assertTrue(isElementDisplayed(unAuthneticatedContent));
			Reporter.log("Verifying DNS page for unauthenticated is success");
		} catch (Exception e) {
			Assert.fail("Verifying DNS page for unauthenticated failed");
		}
		return this;
	}
}