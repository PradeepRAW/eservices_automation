package com.tmobile.eservices.qa.pages.accounts;

import static org.junit.Assert.fail;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author agarpawr
 * 
 */
public class PrepaidBillingAndPaymentPage extends CommonPage {

	private static final String billingAndPaymentsPageUrl = "/account/profile/prepaid/billing_payment";

	private static final String billingAddressPageUrl = "/account/profile/prepaid/billing_payment/billing_address";

	@FindBy(xpath = "//*[contains(text(),'Billing & Payments')]")
	private WebElement BillingandPayments;

	@FindBy(css = "p.Display3.text-center")
	private WebElement billingandPaymentsPageHeader;

	@FindBy(css = "p.body.text-center")
	private WebElement billingAndPaymentsPageSubHeader;

	@FindBy(xpath = "//p[contains(text(),'Billing Address')]")
	private WebElement billingAddressTab;

	@FindBy(xpath = "(//p[contains(text(),'Billing Address')]//../p)[2]")
	private WebElement addressOnBillingAddressTab;

	@FindBy(xpath = "(//p[contains(text(),'Billing Address')]//../p)[3]")
	private WebElement cityAndZipCodeOnBillingAddressTab;

	@FindBy(xpath = "//p[contains(text(),'Paperless Billing')]")
	private WebElement paperlessBillingTab;

	@FindBy(xpath = "//p[contains(text(),'You will be notified')]")
	private WebElement subtextOnpaperlessBillingTab;

	@FindBy(xpath = "//*[text()='Billing Address']//..//..//*[contains(@class,'arrow-right')]")
	private WebElement caratSignOnBillingAddressTab;

	@FindBy(css = "p.Display3.text-center")
	private WebElement headerOfBillingAddressPage;

	@FindBy(xpath = "//p[@class='Display6'][contains(text(),'Billing Address')]")
	private WebElement textBillingAddress;

	@FindBy(xpath = "//input[@id='address1']")
	private WebElement addressLine1Field;

	@FindBy(xpath = "//label[contains(text(),'Address line 1')]")
	private WebElement captionAddressLine1;

	@FindBy(xpath = "//input[@id='address2']")
	private WebElement addressLine2Field;

	@FindBy(xpath = "//label[contains(text(),'Address line 2')]")
	private WebElement captionAddressLine2Field;

	@FindBy(id = "city")
	private WebElement cityField;

	@FindBy(xpath = "//div[@id='model']")
	private WebElement stateDropdown;

	@FindBy(xpath = "//li[@role='menuitem']/a")
	private List<WebElement> valuesInStateDropdown;

	// @FindBy(xpath = "//div[@dropdownid='test']/ul[@class='dropdown-menu']/li/a")
	// private List<WebElement> valuesInStateDropdown;

	@FindBy(xpath = "(//div[@id='model']/span)[1]")
	private WebElement selectedValueOfState;

	@FindBy(id = "zip")
	private WebElement zipcodeField;

	@FindBy(xpath = "//*[text()='Address']")
	private WebElement textAddress;

	@FindBy(xpath = "(//span[@class='legal check1'])[1]")
	private WebElement e911Checkbox;

	@FindBy(xpath = "//*[contains(@class,'PrimaryCTA full-btn-width')]")
	private WebElement saveChangesCTA;

	@FindBy(xpath = "//*[contains(@class,'SecondaryCTA')]")
	private WebElement cancelCTA;

	public PrepaidBillingAndPaymentPage(WebDriver webDriver) {
		super(webDriver);
	}

	// Verify Billing & Payments page
	public PrepaidBillingAndPaymentPage verifyBillingandPaymentsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(billingAndPaymentsPageUrl);
			Reporter.log("Billing & Payments page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Billing & Payments page not displayed");
		}
		return this;
	}

	// Verify Billing Address page
	public PrepaidBillingAndPaymentPage verifyBillingAddressPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(billingAddressPageUrl);
			Reporter.log("Billing address page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Billing Address page not displayed");
		}
		return this;
	}

	// Click on Billing Address tab
	public PrepaidBillingAndPaymentPage clickOnBilingAddressTab() {
		try {
			caratSignOnBillingAddressTab.click();
			Reporter.log("Clicked on Billing & Address tab");
			verifyBillingAddressPage();
		} catch (NoSuchElementException e) {
			Assert.fail("Not able to click on Billing & Address tab.");
		}
		return this;
	}

	/**
	 * Check Element Is Displayed Or Not
	 * 
	 * @param element
	 * @return
	 */
	public PrepaidBillingAndPaymentPage isWebElementDisplayed(WebElement element, String msg, String page) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element));
			element.isDisplayed();
			Reporter.log(msg + " is displayed on " + page);
		} catch (Exception e) {
			Assert.fail(msg + " is not displayed on " + page);
		}
		return this;
	}

	// Check whether Header on Billing Address Page is displayed or not.
	public PrepaidBillingAndPaymentPage checkHeaderOnBilingAddressPage() {
		isWebElementDisplayed(headerOfBillingAddressPage, "Header on Billing Address Page", "Billing Address Page");
		Assert.assertEquals(headerOfBillingAddressPage.getText().trim(), "Billing Address",
				"Billing Address header is mismatched");
		return this;
	}

	// get Address line 1
	public String getAddressLine1() {
		String line1Address = null;
		try {
			isWebElementDisplayed(addressLine1Field, "Address Line1 field ", "Billing Address Page");
			line1Address = addressLine1Field.getAttribute("value").trim();
			Reporter.log("Able to read Address Line1");
		} catch (Exception e) {
			Assert.fail("Not able to read Address Line1");
		}
		// System.out.println(line1Address);
		return line1Address;
	}

	// get Address line 2
	public String getAddressLine2() {
		String line2Address = " ";
		try {
			isWebElementDisplayed(addressLine2Field, "Address Line2 field ", "Billing Address Page");
			line2Address = addressLine2Field.getAttribute("value").trim();
			// System.out.println(line2Address);
			Reporter.log("Able to read Address Line2");
		} catch (Exception e) {
			Reporter.log("Address Line2 field is empty");
		}
		// System.out.println(line2Address);
		return line2Address;
	}

	// get City Name
	public String getCityName() {
		String cityName = null;
		try {
			isWebElementDisplayed(cityField, "City field ", "Billing Address Page");
			cityName = cityField.getAttribute("value").trim();
			// System.out.println(cityName);
			Reporter.log("Able to read City field value");
		} catch (Exception e) {
			Assert.fail("City Field is blank");
		}
		// System.out.println(cityName);
		return cityName;
	}

	// get City Name
	public String getZipCode() {
		String zipcode = null;
		try {
			isWebElementDisplayed(zipcodeField, "ZipCode field ", "Billing Address Page");
			zipcode = zipcodeField.getAttribute("value").trim();
			Reporter.log("Able to read ZipCode field value");
		} catch (Exception e) {
			Assert.fail("ZipCode Field is blank");
		}
		// System.out.println(zipcode);
		return zipcode;
	}

	// get Currently selected State
	public String getSelectedState() {
		String selectedState = null;
		try {
			isWebElementDisplayed(stateDropdown, "State Dropdown field ", "Billing Address Page");
			selectedState = selectedValueOfState.getText().trim();
			Reporter.log("Able to read State Value from State field");
		} catch (Exception e) {
			Assert.fail("Not able to read value from State Field");
		}
		// System.out.println(selectedState);
		return selectedState;
	}

	// Post New Address in Address Line1 field
	public String postNewAddressLine1(String address) {
		try {
			addressLine1Field.clear();
			addressLine1Field.sendKeys(address);
			Reporter.log("Able to post Address in Address field1");
		} catch (Exception e) {
			Assert.fail("Not able to post Address in Address Fields1");
		}
		return address;
	}

	// Post New Address in Address Line2 field
	public String postNewAddressLine2(String address) {
		try {
			addressLine2Field.clear();
			addressLine2Field.sendKeys(address);
			Reporter.log("Able to post Address in Address field2");
		} catch (Exception e) {
			Assert.fail("Not able to post Address in Address Field2");
		}
		return address;
	}

	// Post New City
	public String postNewCity(String cityName) {
		try {
			cityField.clear();
			cityField.sendKeys(cityName);
			Reporter.log("Able to post City name in City Field");
		} catch (Exception e) {
			Assert.fail("Not able to post City name in City Field");
		}
		return cityName;
	}

	// Post New State
	public String postNewState(String stateValue) {
		try {
			stateDropdown.click();
			/*
			 * for (WebElement ele : valuesInStateDropdown) {
			 * System.out.println(ele.getAttribute("innerHTML").trim()); }
			 */
			for (WebElement ele : valuesInStateDropdown) {
				if (ele.getAttribute("innerHTML").trim().contains(stateValue)) {
					ele.click();
					break;
				}
			}
			Reporter.log("Able to post State value in State Field");
		} catch (Exception e) {
			Assert.fail("Not able to select State value in State Field");
		}
		return stateValue;
	}

	// Post New ZipCode
	public String postNewZipCode(String zipcode) {
		try {
			zipcodeField.clear();
			zipcodeField.sendKeys(zipcode);
			Reporter.log("Able to post ZipCode in ZipCode field");
		} catch (Exception e) {
			Assert.fail("Not able to post ZipCode in ZipCode Field");
		}
		return zipcode;
	}

	// Post New Address in Address Line1 field
	public PrepaidBillingAndPaymentPage clickSaveChangeCTA() {
		try {
			isWebElementDisplayed(saveChangesCTA, "Save Change CTA", "On Billing Address Page");
			saveChangesCTA.click();
			Reporter.log("Save Changes CTA is clicked.");
		} catch (Exception e) {
			Assert.fail("Save Changes CTA is not clicked.");
		}
		checkPageIsReady();
		waitforSpinner();
		verifyBillingandPaymentsPage();
		// validateAddressOnBillingTab(enteredValues);
		return this;
	}


/*
	// Update Address1 & Address2
	public PrepaidBillingAndPaymentPage updateValidAddressLine1AndAddressLine2AndCity() {
		ArrayList<String> filedsList = new ArrayList<String>();

		// In this script, i don't want to change State/Zipcode. That's why i passed
		// State value as " ". As City is different so Zipcode would change. That's why
		// i passed zipcodes
		String address1[] = { "22309 30TH DR SE FL 1", "FL 2", "BOTHELL", " ", "98021" };
		String address2[] = { "3305 160TH AVE SE FL 2", "FL 2", "BELLEVUE", " ", "98008" };
		try {
			filedsList.add(getAddressLine1());
			filedsList.add(getAddressLine2());
			filedsList.add(getCityName());
			filedsList.add(getSelectedState());
			filedsList.add(getZipCode());

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (filedsList.get(0).equals(address1[0]))
				filedsList.add(postNewAddressLine1(address2[0]));
			else
				filedsList.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (filedsList.get(1).equals(address1[1]))
				filedsList.add(postNewAddressLine2(address2[1]));
			else
				filedsList.add(postNewAddressLine2(address1[1]));

			// Here we check for City field. If City1 is already displayed then we will
			// enter City2 and vice versa
			if (filedsList.get(0).equals(address1[0]))
				filedsList.add(postNewCity(address2[2]));
			else
				filedsList.add(postNewCity(address1[2]));

			filedsList.add(" ");

			// As city is going to change, so automatically Zip code would change even if we
			// haven't entered new Zipcode. I am not entering new zipcode,but after save
			// zipcode would change and script will fail. So for validation i am passing it.
			if (filedsList.get(0).equals(address1[0]))
				filedsList.add((address2[4]));
			else
				filedsList.add((address1[4]));

			clickSaveChangeCTA();
			checkPageIsReady();
			waitforSpinner();

			verifyBillingandPaymentsPage();
			clickOnBilingAddressTab();

			// Again getting elements after saving changes to cross verify with entered and
			// after saved.
			filedsList.add(getAddressLine1());
			filedsList.add(getAddressLine2());
			filedsList.add(getCityName());
			filedsList.add(getSelectedState());
			filedsList.add(getZipCode());

			verifyFieldsValidations(filedsList);

		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return this;
	}
*/
/*	// Update all fields and verify those
	public PrepaidBillingAndPaymentPage updateCompleteValidAddress() {
		// ArrayList<String> filedsList = new ArrayList<String>();

		List<String> originalValues = new ArrayList<String>();
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();

		// In this script, i don't want to change State/Zipcode. That's why i passed
		// State value as " ". As City is different so Zipcode would change. That's why
		// i passed zipcodes
		String address1[] = { "2408 PRESTON RD STE 704B", "FL 2", "PLANO", "TX", "75093" };
		String address2[] = { "3305 160TH AVE SE FL 2", "FL 1", "BELLEVUE", "WA", "98008" };
		try {
			originalValues.add(getAddressLine1());
			originalValues.add(getAddressLine2());
			originalValues.add(getCityName());
			originalValues.add(getSelectedState());
			originalValues.add(getZipCode());

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (originalValues.get(0).equals(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (originalValues.get(1).equals(address1[1]))
				enteredValues.add(postNewAddressLine2(address2[1]));
			else
				enteredValues.add(postNewAddressLine2(address1[1]));

			// Here we check for City field. If City1 is already displayed then we will
			// enter City2 and vice versa
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewCity(address1[2]));
			else
				enteredValues.add(postNewCity(address2[2]));

			// Here we select New State according to the City which we entered.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState(address1[3]));
			else
				enteredValues.add(postNewState(address2[3]));

			// As city is going to change, so automatically Zip code would change even if we
			// haven't entered new Zipcode. I am not entering new zipcode,but after save
			// zipcode would change and script will fail. So for validation i am passing it.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode((address1[4])));
			else
				enteredValues.add((address2[4]));

			clickSaveChangeCTA();

			verifyBillingandPaymentsPage();
			validateAddressOnBillingTab(enteredValues);
			clickOnBilingAddressTab();

			// Again getting elements after saving changes to cross verify with entered and
			// after saved.
			valuesAfterSaved.add(getAddressLine1());
			valuesAfterSaved.add(getAddressLine2());
			valuesAfterSaved.add(getCityName());
			valuesAfterSaved.add(getSelectedState());
			valuesAfterSaved.add(getZipCode());

			verifyFieldsValidationsForAddressWhenUserEnteredValidAddress(originalValues, enteredValues, valuesAfterSaved);

		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return this;
	}
*/
/*	// Update all fields and verify those
	public PrepaidBillingAndPaymentPage updateValidAddress1CityStateButWrongZipCode() {
		// ArrayList<String> filedsList = new ArrayList<String>();

		List<String> originalValues = new ArrayList<String>();
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();

		// In this script, i don't want to change State/Zipcode. That's why i passed
		// State value as " ". As City is different so Zipcode would change. That's why
		// i passed zipcodes
		String address1[] = { "2408 PRESTON RD STE 704B", "FL 2", "PLANO", "TX", "75093" };
		String address2[] = { "3305 160TH AVE SE FL 2", "FL 1", "BELLEVUE", "WA", "98008" };
		String wrongZipCode = "10007";

		try {
			originalValues.add(getAddressLine1());
			originalValues.add(getAddressLine2());
			originalValues.add(getCityName());
			originalValues.add(getSelectedState());
			originalValues.add(getZipCode());

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (originalValues.get(0).equals(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (originalValues.get(1).equals(address1[1]))
				enteredValues.add(postNewAddressLine2(address2[1]));
			else
				enteredValues.add(postNewAddressLine2(address1[1]));

			// Here we check for City field. If City1 is already displayed then we will
			// enter City2 and vice versa
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewCity(address1[2]));
			else
				enteredValues.add(postNewCity(address2[2]));

			// Here we select New State according to the City which we entered.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState(address1[3]));
			else
				enteredValues.add(postNewState(address2[3]));

			// As city is going to change, so automatically Zip code would change even if we
			// haven't entered new Zipcode. I am not entering new zipcode,but after save
			// zipcode would change and script will fail. So for validation i am passing it.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode((wrongZipCode)));
			else
				enteredValues.add(postNewZipCode(wrongZipCode));

			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(((address1[4])));
			else
				enteredValues.add((address2[4]));

			clickSaveChangeCTA();

			// Again getting elements after saving changes to cross verify with entered and
			// after saved.
			valuesAfterSaved.add(getAddressLine1());
			valuesAfterSaved.add(getAddressLine2());
			valuesAfterSaved.add(getCityName());
			valuesAfterSaved.add(getSelectedState());
			valuesAfterSaved.add(getZipCode());

			verifyZipCodeWhenUserHasEnteredWrongZIPCode(originalValues, enteredValues, valuesAfterSaved);

		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return this;
	}*/

	// Update all fields and verify those
	public List<String> updateValidAddressWithAllValidFields() {
		List<String> originalValues = new ArrayList<String>();
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();
		originalValues = getOriginalAddress();
		enteredValues = enterNewValidAddress();
		clickSaveChangeCTA();

		validateAddressOnBillingTabWhenUserEnteredAllValidFields(enteredValues);
		clickOnBilingAddressTab();
		valuesAfterSaved = getValuesOfEnteredAddress();

		verifyFieldsValidationsForAddressWhenUserEnteredValidAddress(originalValues, enteredValues, valuesAfterSaved);

		return enteredValues;
	}

/*	// Update all fields and verify those
	public List<String> updateValidAddress1Address2CityButNotUpdatingStateWithAllValidFields() {
		List<String> originalValues = new ArrayList<String>()
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();
		originalValues = getOriginalAddress();
		enteredValues = enterNewValidAddress();
		clickSaveChangeCTA();

		validateAddressOnBillingTabWhenUserEnteredAllValidFields(enteredValues);
		clickOnBilingAddressTab();
		valuesAfterSaved = getValuesOfEnteredAddress();

		verifyFieldsValidationsForAddressWhenUserEnteredValidAddress(originalValues, enteredValues, valuesAfterSaved);

		return enteredValues;
	}*/

	
	// Update all fields and verify those
	public List<String> updateValidAddressWithWrongZip() {
		List<String> originalValues = new ArrayList<String>();
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();
		originalValues = getOriginalAddress();
		enteredValues = enterNewAddressWithInvalidZipCode();
		clickSaveChangeCTA();

		validateAddressOnBillingTabWhenUserEnteredWrongZip(enteredValues);
		clickOnBilingAddressTab();
		valuesAfterSaved = getValuesOfEnteredAddress();

		verifyZipCodeWhenUserHasEnteredWrongZIPCode(originalValues, enteredValues, valuesAfterSaved);

		return enteredValues;
	}

	
	// Update all fields and verify those
	public PrepaidBillingAndPaymentPage updateValidAddress1StateZipCodeButWrongCity() {
		// ArrayList<String> filedsList = new ArrayList<String>();

		List<String> originalValues = new ArrayList<String>();
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();

		// In this script, i don't want to change State/Zipcode. That's why i passed
		// State value as " ". As City is different so Zipcode would change. That's why
		// i passed zipcodes
		String address1[] = { "14700 NE SACRAMENTO ST", "FL 2", "PORTLAND", "OR", "97230" };
		String address2[] = { "699 MARKET ST FL", "FL 1", "SAN FRANCISCO", "CA", "94105" };
		String wrongCityName = "Bellevue";

		try {
			originalValues.add(getAddressLine1());
			originalValues.add(getAddressLine2());
			originalValues.add(getCityName());
			originalValues.add(getSelectedState());
			originalValues.add(getZipCode());

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (originalValues.get(0).contains(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (originalValues.get(1).equals(address1[1]))
				enteredValues.add(postNewAddressLine2(address1[1]));
			else
				enteredValues.add(postNewAddressLine2(address2[1]));

			// Here we are entering wrong city name. Rest all fields are valid. Ideally it
			// should save and auto correct the city.
			// After save we need to validate the correct.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewCity(wrongCityName));
			else
				enteredValues.add(postNewCity(wrongCityName));

			// Here we are taking valid city name in variable. So that we will compare it
			// when user saved data comes again to Billing Address page.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(((address1[2])));
			else
				enteredValues.add((address2[2]));

			// Here we select New State according to the City which we entered.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState(address1[3]));
			else
				enteredValues.add(postNewState(address2[3]));

			// Here we select New Zip Code according to the State which we entered.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode((address1[4])));
			else
				enteredValues.add(postNewZipCode(address2[4]));

			clickSaveChangeCTA();

			verifyBillingandPaymentsPage();
			// validateAddressOnBillingTab(enteredValues);
			clickOnBilingAddressTab();

			// Again getting elements after saving changes to cross verify with entered and
			// after saved.
			valuesAfterSaved.add(getAddressLine1());
			valuesAfterSaved.add(getAddressLine2());
			valuesAfterSaved.add(getCityName());
			valuesAfterSaved.add(getSelectedState());
			valuesAfterSaved.add(getZipCode());

			verifyAddressWhenUserHasEnteredWrongCityName(originalValues, enteredValues, valuesAfterSaved);

		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return this;
	}

	// Update all fields and verify those
	public List<String> getOriginalAddress() {
		List<String> originalValues = new ArrayList<String>();
		try {
			originalValues.add(getAddressLine1());
			originalValues.add(getAddressLine2());
			originalValues.add(getCityName());
			originalValues.add(getSelectedState());
			originalValues.add(getZipCode());

		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return originalValues;
	}

	// Update all fields and verify those
	public List<String> enterNewAddressWithInvalidCityName() {

		List<String> enteredValues = new ArrayList<String>();

		String address1[] = { "14700 NE SACRAMENTO ST", "FL 2", "PORTLAND", "OR", "97230" };
		String address2[] = { "699 MARKET ST FL", "FL 1", "SAN FRANCISCO", "CA", "94105" };
		String invalidCityName = "Bellevue";

		try {
			String currentAddress = getAddressLine1();

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.contains(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (enteredValues.get(0).contains(address1[0]))
				enteredValues.add(postNewAddressLine2(address1[1]));
			else
				enteredValues.add(postNewAddressLine2(address2[1]));
			
			// Here we are entering invalid State. But rest address will be valid. After
			// saving it should save data.
			// So we will take the entered value, valid state from address string and will
			// cross check it
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewCity(invalidCityName));
			else
				enteredValues.add(postNewCity(invalidCityName));

			// Get valid city name from address to cross verify on Billing address tab
			if (enteredValues.get(0).contains(address1[0]))
				enteredValues.add((address1[2]));
			else
				enteredValues.add((address2[2]));

			// Here we select New valid Zip
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState((address1[3])));
			else
				enteredValues.add(postNewState((address2[3])));

			// Here we will keep valid State name so that it will be helpful while cross
			// checking.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode(address1[4]));
			else
				enteredValues.add(postNewZipCode(address2[4]));

	
		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return enteredValues;
	}

	// Update all fields and verify those
	public List<String> enterNewValidAddress() {

		List<String> enteredValues = new ArrayList<String>();

		String address1[] = { "14700 NE SACRAMENTO ST", "FL 2", "PORTLAND", "OR", "97230" };
		String address2[] = { "699 MARKET ST FL", "FL 1", "SAN FRANCISCO", "CA", "94105" };

		try {
			String currentAddress = getAddressLine1();

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.equals(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.equals(address1[1]))
				enteredValues.add(postNewAddressLine2(address2[1]));
			else
				enteredValues.add(postNewAddressLine2(address1[1]));

			// Here we check for City field. If City1 is already displayed then we will
			// enter City2 and vice versa
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewCity(address1[2]));
			else
				enteredValues.add(postNewCity(address2[2]));

			// Here we select New State according to the City which we entered.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState(address1[3]));
			else
				enteredValues.add(postNewState(address2[3]));

			//Enter valid ZipCode
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode((address1[4])));
			else
				enteredValues.add(postNewZipCode(address2[4]));
	
		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return enteredValues;
	}

	// Update all fields and verify those
	public List<String> updateNewValidAddressLine1Line2CityZipButNoZip() {

		List<String> enteredValues = new ArrayList<String>();

		String address1[] = { "14700 NE SACRAMENTO ST", "FL 2", "PORTLAND", "OR", "97230" };
		String address2[] = { "699 MARKET ST FL", "FL 1", "SAN FRANCISCO", "CA", "94105" };

		try {
			String currentAddress = getAddressLine1();

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.equals(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.equals(address1[1]))
				enteredValues.add(postNewAddressLine2(address2[1]));
			else
				enteredValues.add(postNewAddressLine2(address1[1]));

			// Here we check for City field. If City1 is already displayed then we will
			// enter City2 and vice versa
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewCity(address1[2]));
			else
				enteredValues.add(postNewCity(address2[2]));

			// Here we select New State according to the City which we entered.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState(address1[3]));
			else
				enteredValues.add(postNewState(address2[3]));

			//Enter valid ZipCode
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode((address1[4])));
			else
				enteredValues.add(postNewZipCode(address2[4]));
	
		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return enteredValues;
	}
	
	// Update all fields and verify those
	public List<String> enterNewAddressWithInvalidZipCode() {

		List<String> enteredValues = new ArrayList<String>();

		String address1[] = { "14700 NE SACRAMENTO ST", "FL 2", "PORTLAND", "OR", "97230" };
		String address2[] = { "699 MARKET ST FL", "FL 1", "SAN FRANCISCO", "CA", "94105" };
		String invalidZipCode = "00001";

		try {
			String currentAddress = getAddressLine1();

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.equals(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.equals(address1[1]))
				enteredValues.add(postNewAddressLine2(address2[1]));
			else
				enteredValues.add(postNewAddressLine2(address1[1]));

			// Here we check for City field. If City1 is already displayed then we will
			// enter City2 and vice versa
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewCity(address1[2]));
			else
				enteredValues.add(postNewCity(address2[2]));

			// Here we select New State according to the City which we entered.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState(address1[3]));
			else
				enteredValues.add(postNewState(address2[3]));

			// As city is going to change, so automatically Zip code would change even if we
			// haven't entered new Zipcode. I am not entering new zipcode,but after save
			// zipcode would change and script will fail. So for validation i am passing it.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode((invalidZipCode)));
			else
				enteredValues.add(postNewZipCode(invalidZipCode));

			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(((address1[4])));
			else
				enteredValues.add((address2[4]));
	
		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return enteredValues;
	}

	// Update all fields and verify those
	public List<String> enterNewAddress(String invalidField) {

		List<String> enteredValues = new ArrayList<String>();

		String address1[] = { "14700 NE SACRAMENTO ST", "FL 2", "PORTLAND", "OR", "97230" };
		String address2[] = { "699 MARKET ST FL", "FL 1", "SAN FRANCISCO", "CA", "94105" };
		String invalidFieldValue = invalidField;

		try {
			String currentAddress = getAddressLine1();

			// Here we check for Line1 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (currentAddress.contains(address1[0]))
				enteredValues.add(postNewAddressLine1(address2[0]));
			else
				enteredValues.add(postNewAddressLine1(address1[0]));

			// Here we check for Line2 field. If address1 is already displayed then we will
			// enter address2 and vice versa
			if (enteredValues.get(0).contains(address1[0]))
				enteredValues.add(postNewAddressLine2(address1[1]));
			else
				enteredValues.add(postNewAddressLine2(address2[1]));

			// Enter valid city name from address.
			if (enteredValues.get(0).contains(address1[0]))
				enteredValues.add(postNewCity(address1[2]));
			else
				enteredValues.add(postNewCity(address2[2]));

			// Here we are entering invalid State. But rest address will be valid. After
			// saving it should save data.
			// So we will take the entered value, valid state from address string and will
			// cross check it
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewState(invalidFieldValue));
			else
				enteredValues.add(postNewState(invalidFieldValue));

			// Here we will keep valid State name so that it will be helpful while cross
			// checking.
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add((address1[3]));
			else
				enteredValues.add((address2[3]));

			// Here we select New valid Zip
			if (enteredValues.get(0).equals(address1[0]))
				enteredValues.add(postNewZipCode((address1[4])));
			else
				enteredValues.add(postNewZipCode(address2[4]));

		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return enteredValues;
	}

	// Update all fields and verify those
	public List<String> getValuesOfEnteredAddress() {
		List<String> valuesAfterSaved = new ArrayList<String>();
		try {
			valuesAfterSaved.add(getAddressLine1());
			valuesAfterSaved.add(getAddressLine2());
			valuesAfterSaved.add(getCityName());
			valuesAfterSaved.add(getSelectedState());
			valuesAfterSaved.add(getZipCode());

		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return valuesAfterSaved;
	}

	// Update all fields and verify those
	public List<String> updateValidAddressWithWrongCity() {
		List<String> originalValues = new ArrayList<String>();
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();
		originalValues = getOriginalAddress();
		enteredValues = enterNewAddressWithInvalidCityName();
		clickSaveChangeCTA();

		validateAddressOnBillingTab(enteredValues);
		clickOnBilingAddressTab();
		valuesAfterSaved = getValuesOfEnteredAddress();

		verifyAddressWhenUserHasEnteredWrongCityName(originalValues, enteredValues, valuesAfterSaved);

		return enteredValues;
	}

	// Update all fields and verify those
	public List<String> updateValidAddressWithWrongState() {
		List<String> originalValues = new ArrayList<String>();
		List<String> enteredValues = new ArrayList<String>();
		List<String> valuesAfterSaved = new ArrayList<String>();
		originalValues = getOriginalAddress();
		String invalidStateCity = "MI";
		enteredValues = enterNewAddress(invalidStateCity);
		clickSaveChangeCTA();

		validateAddressOnBillingTab(enteredValues);
		clickOnBilingAddressTab();
		valuesAfterSaved = getValuesOfEnteredAddress();

		verifyAddressWhenUserHasEnteredWrongState(originalValues, enteredValues, valuesAfterSaved);

		return enteredValues;
	}

	// Validate Address functionality
	public PrepaidBillingAndPaymentPage verifyFieldsValidations(ArrayList<String> fields) {
		String oldLine1Address = fields.get(0);
		String oldLine2Address = fields.get(1);
		String oldCityName = fields.get(2);
		String oldSelectedState = fields.get(3);
		String oldZipCode = fields.get(4);

		String postNewLine1Address = fields.get(5);
		String postNewLine2Address = fields.get(6);
		String postNewCityName = fields.get(7);
		String postNewSelectedState = fields.get(8);
		String postNewZipCode = fields.get(9);

		String newLine1Address = fields.get(10);
		String newLine2Address = fields.get(11);
		String newCityName = fields.get(12);
		String newSelectedState = fields.get(13);
		String newZipCode = fields.get(14);

		validateEnteredAndDisplayedAddress1Line(oldLine1Address, newLine1Address, postNewLine1Address);
		validateEnteredAndDisplayedAddress2Line(newLine1Address, oldLine2Address, postNewLine2Address, newLine2Address);
		validateEnteredAndDisplayedCity(oldCityName, newCityName, postNewCityName);
		validateEnteredAndDisplayedState(oldSelectedState, newSelectedState, postNewSelectedState);
		validateEnteredAndDisplayedZipCode(oldZipCode, newZipCode, postNewZipCode);

		return this;
	}

	// Validate Address functionality
	public PrepaidBillingAndPaymentPage verifyFieldsValidationsForAddressWhenUserEnteredValidAddress(List<String> originalValues,
			List<String> enteredValues, List<String> valuesAfterSaved) {
		String oldLine1Address = originalValues.get(0);
		String oldLine2Address = originalValues.get(1);
		String oldCityName = originalValues.get(2);
		String oldSelectedState = originalValues.get(3);
		String oldZipCode = originalValues.get(4);

		String postNewLine1Address = enteredValues.get(0);
		String postNewLine2Address = enteredValues.get(1);
		String postNewCityName = enteredValues.get(2);
		String postNewSelectedState = enteredValues.get(3);
		String postNewZipCode = enteredValues.get(4);

		String newLine1Address = valuesAfterSaved.get(0);
		String newLine2Address = valuesAfterSaved.get(1);
		String newCityName = valuesAfterSaved.get(2);
		String newSelectedState = valuesAfterSaved.get(3);
		String newZipCode = valuesAfterSaved.get(4);

		validateEnteredAndDisplayedAddress1Line(oldLine1Address, newLine1Address, postNewLine1Address);
		validateEnteredAndDisplayedAddress2Line(newLine1Address, oldLine2Address, postNewLine2Address, newLine2Address);
		validateEnteredAndDisplayedCity(oldCityName, newCityName, postNewCityName);
		validateEnteredAndDisplayedState(oldSelectedState, newSelectedState, postNewSelectedState);
		validateEnteredAndDisplayedZipCode(oldZipCode, newZipCode, postNewZipCode);

		return this;
	}

	// Validate Address when user entered wrong ZIP code
	public PrepaidBillingAndPaymentPage verifyZipCodeWhenUserHasEnteredWrongZIPCode(List<String> originalValues,
			List<String> enteredValues, List<String> valuesAfterSaved) {

		String oldLine1Address = originalValues.get(0);
		String oldLine2Address = originalValues.get(1);
		String oldCityName = originalValues.get(2);
		String oldSelectedState = originalValues.get(3);
		String oldZipCode = originalValues.get(4);

		String postNewLine1Address = enteredValues.get(0);
		String postNewLine2Address = enteredValues.get(1);
		String postNewCityName = enteredValues.get(2);
		String postNewSelectedState = enteredValues.get(3);
		String enteredWrongZipCode = enteredValues.get(4);
		String validZipCode = enteredValues.get(5);

		String newLine1Address = valuesAfterSaved.get(0);
		String newLine2Address = valuesAfterSaved.get(1);
		String newCityName = valuesAfterSaved.get(2);
		String newSelectedState = valuesAfterSaved.get(3);
		String newZipCode = valuesAfterSaved.get(4);

		validateEnteredAndDisplayedAddress1Line(oldLine1Address, postNewLine1Address, newLine1Address);
		validateEnteredAndDisplayedAddress2Line(newLine1Address, oldLine2Address, postNewLine2Address, newLine2Address);
		validateEnteredAndDisplayedCity(oldCityName, newCityName, postNewCityName);
		validateEnteredAndDisplayedState(oldSelectedState, newSelectedState, postNewSelectedState);
		validateWhenUserEnteredWrongZipCodeButSaveSuccessful(enteredWrongZipCode, validZipCode, newZipCode);

		return this;
	}

	// Validate Address when user entered wrong City name
	public PrepaidBillingAndPaymentPage verifyAddressWhenUserHasEnteredWrongCityName(List<String> originalValues,
			List<String> enteredValues, List<String> valuesAfterSaved) {

		String oldLine1Address = originalValues.get(0);
		String oldLine2Address = originalValues.get(1);
		String oldCityName = originalValues.get(2);
		String oldSelectedState = originalValues.get(3);
		String oldZipCode = originalValues.get(4);

		String enteredLine1Address = enteredValues.get(0);
		String enteredLine2Address = enteredValues.get(1);
		String enteredWrongCityName = enteredValues.get(2);
		String validCityName = enteredValues.get(3);
		String enteredState = enteredValues.get(4);
		String enteredZipCode = enteredValues.get(5);

		String newLine1Address = valuesAfterSaved.get(0);
		String newLine2Address = valuesAfterSaved.get(1);
		String newCityName = valuesAfterSaved.get(2);
		String newSelectedState = valuesAfterSaved.get(3);
		String newZipCode = valuesAfterSaved.get(4);

		validateEnteredAndDisplayedAddress1Line(oldLine1Address, enteredLine1Address, newLine1Address);
		validateEnteredAndDisplayedAddress2Line(newLine1Address, oldLine2Address, enteredLine2Address, newLine2Address);
		validateWhenUserEnteredWrongCityNameButSavedItSuccessful(enteredWrongCityName, validCityName, newCityName);
		validateEnteredAndDisplayedState(oldSelectedState, enteredState, newSelectedState);
		validateEnteredAndDisplayedZipCode(oldZipCode, enteredZipCode, newZipCode);

		return this;
	}

	// Validate Address when user entered wrong City name
	public PrepaidBillingAndPaymentPage verifyAddressWhenUserHasEnteredWrongState(List<String> originalValues,
			List<String> enteredValues, List<String> valuesAfterSaved) {

		String oldLine1Address = originalValues.get(0);
		String oldLine2Address = originalValues.get(1);
		String oldCityName = originalValues.get(2);
		String oldSelectedState = originalValues.get(3);
		String oldZipCode = originalValues.get(4);

		String enteredLine1Address = enteredValues.get(0);
		String enteredLine2Address = enteredValues.get(1);
		String enteredCityName = enteredValues.get(2);
		String enteredWrongState = enteredValues.get(3);
		String validStateName = enteredValues.get(4);
		String enteredZipCode = enteredValues.get(5);
		
		String newLine1Address = valuesAfterSaved.get(0);
		String newLine2Address = valuesAfterSaved.get(1);
		String newCityName = valuesAfterSaved.get(2);
		String newSelectedState = valuesAfterSaved.get(3);
		String newZipCode = valuesAfterSaved.get(4);

		validateEnteredAndDisplayedAddress1Line(oldLine1Address, enteredLine1Address, newLine1Address);
		validateEnteredAndDisplayedAddress2Line(newLine1Address, oldLine2Address, enteredLine2Address, newLine2Address);
		validateEnteredAndDisplayedCity(oldCityName, enteredCityName, newCityName);
		validateWhenUserEnteredWrongStateNameButSavedItSuccessful(enteredWrongState, validStateName, newSelectedState);
		validateEnteredAndDisplayedZipCode(oldZipCode, enteredZipCode, newZipCode);

		return this;
	}

	// Validate Entered and displayed Address1. Here we will check, if user has not
	// entered new address then it will check old address. If user has entered new
	// address then we will check old and posted address
	public PrepaidBillingAndPaymentPage validateEnteredAndDisplayedAddress1Line(String oldLine1Address,
			String postNewLine1Address, String newLine1Address) {
		try {
			if (!(postNewLine1Address == " ")) {
				if (!newLine1Address.contains(postNewLine1Address))
					Assert.fail("Mismatch in Address Line1. Not displayed entered Address");
				else
					Reporter.log("Entered address and displayed address matched for Address1 field");

			} else {
				if (!newLine1Address.equalsIgnoreCase(oldLine1Address))
					Assert.fail("We have not updated Address1 seems still it got changed.");
				else
					Reporter.log("Address1 matched");
			}
		} catch (Exception e) {
			Assert.fail("Some issues occured while validating Address1");
		}
		return this;
	}

	// Validate Entered and displayed Address2. Here we will check, if user has not
	// entered new address then it will check old address. If user has entered new
	// address then we will check old and posted address
	public PrepaidBillingAndPaymentPage validateEnteredAndDisplayedAddress2Line(String newLine1Address,
			String oldLine2Address, String postNewLine2Address, String newLine2Address) {
		try {
			/*
			 * boolean isAddress2Empty = false; if (oldLine2Address.equals(" "))
			 * isAddress2Empty = true;
			 * 
			 * if (isAddress2Empty == true) {
			 * if(newLine1Address.contains(postNewLine2Address))
			 * Reporter.log("Address2 is was empty field"); else Assert.
			 * fail("Line2 address supposed to append in Line1.Entered Address Line 2 is not found in Address Line1."
			 * ); } else {
			 */
			boolean isAddress2Empty = false;
			if (newLine2Address.equals(" ") || newLine2Address.equals(null) || newLine2Address.equals(""))
				isAddress2Empty = true;

			if ((isAddress2Empty == true)) {
				if (newLine1Address.contains(postNewLine2Address))
					Reporter.log("Address2 is appended in Address1");
				else
					Assert.fail(
							"Line2 address supposed to append in Line1.Entered Address Line 2 is not found in Address Line1.");
			} else {
				if (!newLine2Address.equals(postNewLine2Address))
					Assert.fail("Mismatch in Address2 field. After saving changes seems Address2 field changed");
				else
					Reporter.log("Address2 is matched with before saving and after saving");
			}

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating Address2");
		}
		return this;
	}

	// Validate Entered and displayed City. Here we will check, if user has not
	// entered new City then it will check old City. If user has entered new
	// address then we will check old and posted City
	public PrepaidBillingAndPaymentPage validateEnteredAndDisplayedCity(String oldCityName, String postNewCityName,
			String newCityName) {
		try {
			if (!(postNewCityName == " ")) {
				if (!newCityName.equalsIgnoreCase(postNewCityName))
					Assert.fail("Mismatch in City. Not displayed entered City name");
				else
					Reporter.log("Entered City name and displayed City names matched");

			} else {
				if (!newCityName.equalsIgnoreCase(oldCityName))
					Assert.fail("We have not updated City name seems still it got changed.");
				else
					Reporter.log("City name matched");
			}
		} catch (Exception e) {
			Assert.fail("Some issues occured while validating City name");
		}
		return this;
	}

	// Validate Selected and displayed State. Here we will check, if user has not
	// selected new State then it will check old State which was displayed. If user
	// has selected new
	// state then we will check old and posted state values
	public PrepaidBillingAndPaymentPage validateEnteredAndDisplayedState(String oldSelectedState,
			String postNewSelectedState, String newSelectedState) {
		try {
			if (!(postNewSelectedState == " ")) {
				if (!newSelectedState.equalsIgnoreCase(postNewSelectedState))
					Assert.fail("Mismatch in State. Not displayed selected State");
				else
					Reporter.log("Entered State name and displayed State names matched");

			} else {
				if (!newSelectedState.equalsIgnoreCase(oldSelectedState))
					Assert.fail("We have not updated State name seems still it got changed.");
				else
					Reporter.log("State name matched");
			}
		} catch (Exception e) {
			Assert.fail("Some issues occured while validating State name");
		}
		return this;
	}

	// Validate Entered and displayed Zipcode. Here we will check, if user has not
	// entered new Zipcode then it will check old Zipcode. If user has entered new
	// Zipcode then we will check old and posted Zipcode
	public PrepaidBillingAndPaymentPage validateEnteredAndDisplayedZipCode(String oldZipCode, String postNewZipCode,
			String newZipCode) {
		try {
			if (!(postNewZipCode == " ")) {
				if (!newZipCode.equalsIgnoreCase(postNewZipCode))
					Assert.fail("Mismatch in ZipCode. Not displayed entered ZipCode");
				else
					Reporter.log("Entered ZipCode and displayed ZipCode matched");

			} else {
				if (!newZipCode.equalsIgnoreCase(oldZipCode))
					Assert.fail("We have not updated ZipCode seems still it got changed.");
				else
					Reporter.log("ZipCode matched");
			}
		} catch (Exception e) {
			Assert.fail("Some issues occured while validating ZipCode");
		}
		return this;
	}

	// Validate Entered and displayed Zipcode. When user entered wrong Zip Code but
	// still user able to change Address with valid City and State at that
	// time Zip code changed to valid state. So we will match whether correct ZIP
	// code displayed or not
	public PrepaidBillingAndPaymentPage validateWhenUserEnteredWrongZipCodeButSaveSuccessful(String enteredZipCode,
			String validZipCode, String newDisplayedZipCode) {
		try {
			if (enteredZipCode.equalsIgnoreCase(newDisplayedZipCode))
				Assert.fail("Entered ZipCode and displayed ZipCode are matched. Means it accepted wrong ZIP code");
			else
				Reporter.log("Entered ZipCode and displayed ZipCode are not matched");

			if (!newDisplayedZipCode.equalsIgnoreCase(validZipCode))
				Assert.fail("Entered ZipCode and displayed ZipCode are matched. Means it accepted wrong ZIP code");
			else
				Reporter.log("ZipCode matched");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating ZipCode");
		}
		return this;
	}

	// Validate Entered and displayed City name. When user entered wrong City name
	// but
	// still user able to change Address with valid State and Zip code at that
	// time City changed to valid city. So we will match whether correct ZIP
	// code displayed or not
	public PrepaidBillingAndPaymentPage validateWhenUserEnteredWrongCityNameButSavedItSuccessful(String enteredCityName,
			String validCityName, String newDisplayedCityName) {
		try {
			if (enteredCityName.equalsIgnoreCase(newDisplayedCityName))
				Assert.fail("Entered CityName and displayed CityName are matched. Means it accepted wrong CityName");
			else
				Reporter.log("Entered CityName and displayed CityName are not matched");

			if (!newDisplayedCityName.equalsIgnoreCase(validCityName))
				Assert.fail("Entered CityName and displayed CityName are matched. Means it accepted wrong CityName");
			else
				Reporter.log("CityName matched");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating CityName");
		}
		return this;
	}

	// Validate Entered and displayed State name. When user entered wrong State name
	// but
	// still user able to change Address with valid City and Zip code at that
	// time State changed to valid address. So we will match whether correct State
	// value displayed or not
	public PrepaidBillingAndPaymentPage validateWhenUserEnteredWrongStateNameButSavedItSuccessful(
			String enteredStateName, String validStateName, String newDisplayedStateName) {
		try {
			if (enteredStateName.equalsIgnoreCase(newDisplayedStateName))
				Assert.fail("Entered StateName and displayed StateName are matched. Means it accepted wrong StateName");
			else
				Reporter.log("Entered StateName and displayed StateName are not matched");

			if (!newDisplayedStateName.equalsIgnoreCase(validStateName))
				Assert.fail("Entered StateName and displayed StateName are matched. Means it accepted wrong StateName");
			else
				Reporter.log("StateName matched");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating StateName");
		}
		return this;
	}

	// validate Address on Billing Tab
	public PrepaidBillingAndPaymentPage validateAddressOnBillingTabWhenUserEnteredAllValidFields(
			List<String> enteredValues) {

		String expectedAddressLine1 = enteredValues.get(0);
		String expectedAddressLine2 = enteredValues.get(1);
		String expectedCity = enteredValues.get(2);
		String expectedState = enteredValues.get(3);
		String expectedZipCode = enteredValues.get(4);

		validateAddressLine1OnBillingAddressTab(expectedAddressLine1);
		validateAddressLine2OnBillingAddressTab(expectedAddressLine2);
		validateCityOnBillingAddressTab(expectedCity);
		validateStateOnBillingAddressTab(expectedState);
		validateZipCodeOnBillingAddressTab(expectedZipCode);

		return this;
	}

	// validate Address on Billing Tab
	public PrepaidBillingAndPaymentPage validateAddressOnBillingTab(List<String> enteredValues) {

		String expectedAddressLine1 = enteredValues.get(0);
		String expectedAddressLine2 = enteredValues.get(1);
		String expectedCity = enteredValues.get(3);
		String expectedState = enteredValues.get(4);
		String expectedZipCode = enteredValues.get(5);

		validateAddressLine1OnBillingAddressTab(expectedAddressLine1);
		validateAddressLine2OnBillingAddressTab(expectedAddressLine2);
		validateCityOnBillingAddressTab(expectedCity);
		validateStateOnBillingAddressTab(expectedState);
		validateZipCodeOnBillingAddressTab(expectedZipCode);

		return this;
	}

	// validate Address on Billing Tab
	public PrepaidBillingAndPaymentPage validateAddressOnBillingTabWhenUserEnteredWrongState(
			List<String> enteredValues) {

		String expectedAddressLine1 = enteredValues.get(0);
		String expectedAddressLine2 = enteredValues.get(1);
		String expectedCity = enteredValues.get(2);
		String wrongState = enteredValues.get(3);
		String expectedState = enteredValues.get(4);
		String expectedZipCode = enteredValues.get(5);

		validateAddressLine1OnBillingAddressTab(expectedAddressLine1);
		validateAddressLine2OnBillingAddressTab(expectedAddressLine2);
		validateCityOnBillingAddressTab(expectedCity);
		validateStateOnBillingAddressTab(expectedState);
		validateZipCodeOnBillingAddressTab(expectedZipCode);

		return this;
	}

	// validate Address on Billing Tab
	public PrepaidBillingAndPaymentPage validateAddressOnBillingTabWhenUserEnteredWrongZip(
			List<String> enteredValues) {

		String expectedAddressLine1 = enteredValues.get(0);
		String expectedAddressLine2 = enteredValues.get(1);
		String expectedCity = enteredValues.get(2);
		String expectedState = enteredValues.get(3);
		String wrongZip = enteredValues.get(4);
		String expectedZipCode = enteredValues.get(5);

		validateAddressLine1OnBillingAddressTab(expectedAddressLine1);
		validateAddressLine2OnBillingAddressTab(expectedAddressLine2);
		validateCityOnBillingAddressTab(expectedCity);
		validateStateOnBillingAddressTab(expectedState);
		validateZipCodeOnBillingAddressTab(expectedZipCode);

		return this;
	}

	// Validate address line1 on Billing tab
	public PrepaidBillingAndPaymentPage validateAddressLine1OnBillingAddressTab(String expectedAddressLine1) {
		try {
			String addressLine1And2OnBillingTab = addressOnBillingAddressTab.getText().trim();
			if (addressLine1And2OnBillingTab.contains(expectedAddressLine1))
				Reporter.log("Address Line 1 is matched on Billing Address Tab");
			else
				Assert.fail("Address Line 1 is not matched on Billing Address Tab");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating Address Line1 on Billing Address tab");
		}
		return this;
	}

	// Validate address line1 on Billing tab
	public PrepaidBillingAndPaymentPage validateAddressLine2OnBillingAddressTab(String expectedAddressLine2) {
		try {
			String addressLine1And2OnBillingTab = addressOnBillingAddressTab.getText().trim();
			if (addressLine1And2OnBillingTab.contains(expectedAddressLine2))
				Reporter.log("Address Line 2 is matched on Billing Address Tab");
			else
				Assert.fail("Address Line 2 is not matched on Billing Address Tab");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating Address Line1 on Billing Address tab");
		}
		return this;
	}

	// Validate City name on Billing tab
	public PrepaidBillingAndPaymentPage validateCityOnBillingAddressTab(String expectedCityName) {
		try {
			String cityOnBillingTab = addressOnBillingAddressTab.getText().trim();
			if (cityOnBillingTab.contains(expectedCityName))
				Reporter.log("City is matched on Billing Address Tab");
			else
				Assert.fail("City 2 is not matched on Billing Address Tab");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating City on Billing Address tab");
		}
		return this;
	}

	// Validate State on Billing tab
	public PrepaidBillingAndPaymentPage validateStateOnBillingAddressTab(String expectedStateName) {
		try {
			String stateOnBillingTab = addressOnBillingAddressTab.getText().trim();
			if (stateOnBillingTab.contains(expectedStateName))
				Reporter.log("State is matched on Billing Address Tab");
			else
				Assert.fail("State 2 is not matched on Billing Address Tab");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating State on Billing Address tab");
		}
		return this;
	}

	// Validate Zip on Billing tab
	public PrepaidBillingAndPaymentPage validateZipCodeOnBillingAddressTab(String expectedZipCode) {
		try {
			String zipCodeOnBillingTab = addressOnBillingAddressTab.getText().trim();
			if (zipCodeOnBillingTab.contains(expectedZipCode))
				Reporter.log("Zip Code is matched on Billing Address Tab");
			else
				Assert.fail("Zip Code is not matched on Billing Address Tab");

		} catch (Exception e) {
			Assert.fail("Some issues occured while validating Zip Code on Billing Address tab");
		}
		return this;
	}

	// Validate Address functionality
	public PrepaidBillingAndPaymentPage tobeDeleted(ArrayList<String> fields) {
		boolean isAddress2Null = false;
		String oldLine1Address = fields.get(0);
		String oldLine2Address = fields.get(1);
		String oldCityName = fields.get(2);
		String oldSelectedState = fields.get(3);
		String oldZipCode = fields.get(4);

		String postNewLine1Address = fields.get(5);
		String postNewLine2Address = fields.get(6);
		String postNewCityName = fields.get(7);
		String postNewSelectedState = fields.get(8);
		String postNewZipCode = fields.get(9);

		String newLine1Address = fields.get(10);
		String newLine2Address = fields.get(11);
		String newCityName = fields.get(12);
		String newSelectedState = fields.get(13);
		String newZipCode = fields.get(14);

		try {

			if (oldLine2Address.equals(" "))
				isAddress2Null = true;

			if (!newLine1Address.equalsIgnoreCase(postNewLine1Address))
				Assert.fail("Mismatch in Address Line1. Not displayed entered Address");
			else
				Reporter.log("Entered address and displayed address matched");

			if (isAddress2Null == true)
				Reporter.log("Address2 is was empty field");
			else {
				if (!oldLine2Address.equals(newLine2Address))
					Assert.fail("Mismatch in Address2 field. After saving changes seems Address2 field changed");
				else
					Reporter.log("Address2 is matched with before saving and after saving");
			}
			if (!oldCityName.equals(newCityName))
				Assert.fail("Mismatch in City field. After saving changes seems City name changed");
			else
				Reporter.log("City name is matched with before saving and after saving");

			if (!oldSelectedState.equals(newSelectedState))
				Assert.fail("Mismatch in Selected State field. After saving changes seems State field changed");
			else
				Reporter.log("State field is matched with before saving and after saving");

			if (!oldZipCode.equals(newZipCode))
				Assert.fail("Mismatch in ZipCode field. After saving changes seems Zipcode field changed");
			else
				Reporter.log("Zipcode field is matched with before saving and after saving");
		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return this;
	}

	// Validate Address functionality
	public PrepaidBillingAndPaymentPage tobeDeleted1() {
		String address = "";
		boolean isAddress2Null = false;
		String oldLine1Address = null;
		String oldLine2Address = null;
		String oldCityName = null;
		String oldSelectedState = null;
		String oldZipCode = null;

		String postNewLine1Address = null;

		String newLine1Address = null;
		String newLine2Address = null;
		String newCityName = null;
		String newSelectedState = null;
		String newZipCode = null;

		try {
			oldLine1Address = getAddressLine1();
			oldLine2Address = getAddressLine2();
			oldCityName = getCityName();
			oldSelectedState = getSelectedState();
			oldZipCode = getZipCode();

			if (oldLine2Address.equals(" "))
				isAddress2Null = true;

			postNewLine1Address = postNewAddressLine1(address);
			clickSaveChangeCTA();

			verifyBillingandPaymentsPage();
			clickOnBilingAddressTab();

			newLine1Address = getAddressLine1();
			newLine2Address = getAddressLine2();
			newCityName = getCityName();
			newSelectedState = getSelectedState();
			newZipCode = getZipCode();

			if (!newLine1Address.equalsIgnoreCase(postNewLine1Address))
				Assert.fail("Mismatch in Address Line1. Not displayed entered Address");
			else
				Reporter.log("Entered address and displayed address matched");

			if (isAddress2Null == true)
				Reporter.log("Address2 is was empty field");
			else {
				if (!oldLine2Address.equals(newLine2Address))
					Assert.fail("Mismatch in Address2 field. After saving changes seems Address2 field changed");
				else
					Reporter.log("Address2 is matched with before saving and after saving");
			}
			if (!oldCityName.equals(newCityName))
				Assert.fail("Mismatch in City field. After saving changes seems City name changed");
			else
				Reporter.log("City name is matched with before saving and after saving");

			if (!oldSelectedState.equals(newSelectedState))
				Assert.fail("Mismatch in Selected State field. After saving changes seems State field changed");
			else
				Reporter.log("State field is matched with before saving and after saving");

			if (!oldZipCode.equals(newZipCode))
				Assert.fail("Mismatch in ZipCode field. After saving changes seems Zipcode field changed");
			else
				Reporter.log("Zipcode field is matched with before saving and after saving");
		} catch (Exception e) {
			Assert.fail("Seems error occured after changing Address1");
		}
		return this;
	}

}