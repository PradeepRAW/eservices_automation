package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * 
 * @author pshiva
 *
 */
public class ChooseAccountPage extends CommonPage {

	public static final String chooseAccountPageUrl = "/oauth2/v1/forgotpassword";
	public static final String chooseAccountPageLoadText = "Reset Your Password";

	@FindBy(css = "div[class='ui_subhead']")
	private WebElement accountHeaderText;

	@FindBy(css = "section#container div[class='ui_mobile_subhead']")
	private WebElement deviceAccountHeaderText;

	@FindBy(css = "div#container ul.co_select-account-list.unstyled.pt20.mb25")
	private List<WebElement> listOfAccounts;

	@FindBy(css = "div[id='#account2']")
	private WebElement switchToSecondAccount;

	@FindBy(id = "#account1")
	private WebElement switchToFirstAccount;

	@FindBy(css = "div[id*='msisdn']")
	private List<WebElement> allAccountsMsisdns;

	@FindBy(css = "div.search_container input#search")
	private WebElement searchForPhoneNumber;

	@FindBy(css = "div.ban i")
	private WebElement selectAccount;

	/**
	 * 
	 * @param webDriver
	 */
	public ChooseAccountPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * get the displayed account header text
	 * 
	 * @return value
	 */
	public ChooseAccountPage getAccountHeaderText() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			deviceAccountHeaderText.getText();
		} else {
			accountHeaderText.getText();
		}
		return this;
	}

	/**
	 * verify the select account header
	 * 
	 * @return boolean
	 */
	public ChooseAccountPage verifySelectAccountHeader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(selectAccount.isDisplayed());
			Reporter.log("Select Account header is visible.");
		} catch (NoSuchElementException e) {
			Assert.fail("Error: Select Account header is missing.");
		}

		return this;
	}

	/**
	 * Choose Account
	 * 
	 * @param msisdn
	 * @return
	 */
	public ChooseAccountPage chooseAccount(String msisdn) {
		try {
			waitFor(ExpectedConditions.visibilityOf(selectAccount), 2);
			searchForPhoneNumber.sendKeys(msisdn);
			waitFor(ExpectedConditions.visibilityOf(selectAccount), 3);
			selectAccount.click();
		} catch (Exception e) {
			Assert.fail("Choose Account is failed");
		}
		return this;
	}

	public ChooseAccountPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(chooseAccountPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Change Password class instance.
	 */
	public ChooseAccountPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(chooseAccountPageUrl);
		return this;
	}

	/**
	 * Choose Login Msisdn
	 * 
	 * @param msisdn
	 */
	public ChooseAccountPage chooseOtherAccount(String msisdn) {

		if (CollectionUtils.isNotEmpty(allAccountsMsisdns) && !CollectionUtils.sizeIsEmpty(allAccountsMsisdns))
			for (WebElement accMsisdn : allAccountsMsisdns) {
				if (!accMsisdn.getText().contains(msisdn.trim())) {
					accMsisdn.click();
					checkPageIsReady();
					break;
				}
			}
		return this;
	}

}