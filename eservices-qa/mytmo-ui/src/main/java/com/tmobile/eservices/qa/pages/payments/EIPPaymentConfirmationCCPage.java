package com.tmobile.eservices.qa.pages.payments;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class EIPPaymentConfirmationCCPage extends CommonPage {
	
	//THIS PAGE CLASS IS NOT COMPLETED DUE TO INELIGIBILITY TO GET DATA ON PPD ORQLAB02
	
	@FindBy(css = "div.comfirmation-message")
	private WebElement pageLoadElement;

	private final String pageUrl = "eippaymentconfirmationcc.html";

	@FindBy(linkText = "View EIP details")
	private WebElement viewEIPDetailsLink;

	@FindBy(linkText = "Return to Billing home")
	private WebElement returnToBillingHomeLink;

	@FindBy(linkText = "Payment history")
	private WebElement paymentHistoryLink;

	@FindBy(id = "contJump_btn")
	private WebElement continueWithMyUpgradeButton;
	
	@FindBy(linkText = "Print confirmation page")
	private WebElement printConfirmationLink ;
	
	@FindBy(id ="payAmount")
	private WebElement paymentAmount ;
	
	@FindBy(xpath ="//span[@id='spanImageClass']/following-sibling::span")
	private WebElement accountNo ;
	
	@FindBy(id="zipcode")
	private WebElement zipCode;
	
	@FindBy(css="span#payDate")
	private WebElement payDate;
	
	
	@FindBy(css="div.payment-inner-pad>span#firstName")
	private WebElement customerFirstName;
	
	@FindBy(css="span#msisdn")
	private WebElement customerPhoneNumber;
	
	
	@FindBy(css ="span#imei")
	private WebElement customerImeiNumber;
	
	
	@FindBy(css ="span#payer")
	private WebElement customerName;
	
	@FindBy(css = "span#card>span:not([id='spanImageClass'])")
	private WebElement customerPaymentInfo;
	
	
	@FindBy(css = "span#zipcode")
	private WebElement customerZipCode;
	
	
	@FindBy(css = "span#accountNo")
	private WebElement customerTmobileAccountNumber;
	
	
	
	
	
	
	
	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public EIPPaymentConfirmationCCPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
     * Verify that current page URL matches the expected URL.
     */
    public EIPPaymentConfirmationCCPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public EIPPaymentConfirmationCCPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
	    	waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("Fail to load EIP payment confirmation page");
    	}
        return this;
    }

    /**
     * verify 'view EIP details' link is displayed or not
     */
	public void verifyViewEIPDetailsLink() {
		try {
			viewEIPDetailsLink.isDisplayed();
			Reporter.log("View EIP details link is displayed");
		} catch (Exception e) {
			Assert.fail("View EIP details link is not displayed");
		}
	}

	/**
	 * verify if 'Return To Billing Home ' Link is displayed or not
	 */
	public void verifyReturnToBillingHomeLink() {
		try {
			returnToBillingHomeLink.isDisplayed();
			Reporter.log("Return To Billing Home Link is displayed");
		} catch (Exception e) {
			Assert.fail("Return To Billing Home Link is not displayed");
		}
	}

	/**
	 * Click 'Return To Billing Home ' Link
	 */
	public void clickReturnToBillingHomeLink() {
		try {
			returnToBillingHomeLink.click();
			Reporter.log("Clicked on Return To Billing Home Link");
		} catch (Exception e) {
			Assert.fail("Return To Billing Home Link is not found");
		}
	}

	/**
	 * verify if 'Payment History' Link is displayed or not
	 */
	public void verifyPaymentHistoryLink() {
		try {
			paymentHistoryLink.isDisplayed();
			Reporter.log("Payment History Link is displayed");
		} catch (Exception e) {
			Assert.fail("Payment History Link is not displayed");
		}
	}

	/**
	 * Click 'Payment History' Link
	 */
	public void clickPaymentHistoryLink() {
		try {
			paymentHistoryLink.click();
			Reporter.log("Clicked on Payment History Link");
		} catch (Exception e) {
			Assert.fail("Payment History Link is not found");
		}
	}

	/**
	 * Click 'view EIP details' Link
	 */
	public void clickViewEIPDetailsLink() {
		try {
			viewEIPDetailsLink.click();
			Reporter.log("Clicked on view EIP details Link");
		} catch (Exception e) {
			Assert.fail("view EIP details Link is not found");
		}
	}
	
	
	 /**
     * verify 'Continue with my upgrade' button is displayed or not
     */
	public void verifyContinuewithmyupgradebutton() {
		try {
			continueWithMyUpgradeButton.isDisplayed();
			Reporter.log("Continue with my upgrade Button is displayed");
		} catch (Exception e) {
			Assert.fail("Continue with my upgrade is not displayed");
		}
	}

	/**
	 * verify if 'Return To Billing Home ' Link is displayed or not
	 */
	public void verifyReturnToBillingHomeLinkIsSupressed() {
		try {
			Assert.assertFalse(returnToBillingHomeLink.isDisplayed(),"Return To Billing Home Link is displayed");
			Reporter.log("Return To Billing Home Link is not displayed");
		} catch (Exception e) {
			Assert.fail("Return To Billing Home Link is displayed");
		}
	}

	/**
	 * verify if 'Payment History' Link is displayed or not
	 */
	public void verifyPaymentHistoryLinkIsSupressed() {
		try {
			Assert.assertFalse(paymentHistoryLink.isDisplayed(),"Payment History Link is displayed");
			Reporter.log("Payment History Link is not displayed");
		} catch (Exception e) {
			Assert.fail("Payment History Link is displayed");
		}
	}

	/**
     * verify 'view EIP details' link is displayed or not
     */
	public void verifyViewEIPDetailsLinkIsSupressed() {
		try {
			Assert.assertFalse(viewEIPDetailsLink.isDisplayed(),"View EIP details link is not displayed");
			Reporter.log("View EIP details link is not displayed");
		} catch (Exception e) {
			Assert.fail("View EIP details link is displayed");
		}
	}
	
	/**
     * verify 'Print Confirmation Page' link is displayed or not
     */
	public void verifyPrintConfirmationLink() {
		try {
			printConfirmationLink.isDisplayed();
		    printConfirmationLink.click();
			Reporter.log("Print Confirmation Page link is  displayed and Clicked Successfully");
		} catch (Exception e) {
			Assert.fail("Print Confirmation Page link is not Found");
		}
	}
	
	
	/**
     * verify Payment ConfirmationDetails
     */
	public void verifyPaymentConfirmationDetails(String Amount,String ZipCode,String AccountInfo) {
		try {
			if(paymentAmount.getText().contains(Amount) &&  accountNo.getText().substring(11, 15).contains(AccountInfo) && 
					zipCode.getText().contains(ZipCode)){
				Reporter.log("Amount,ZipCode,Account Info is diplayed or matched");
			}
			
		} catch (Exception e) {
			Assert.fail("Amount,ZipCode,Account Info is not diplayed or matched");
		}
	}
	
	  public EIPPaymentConfirmationCCPage Validatepaymentdate() {
	    	try {
	    		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	    		LocalDateTime now = LocalDateTime.now();
	    		if(payDate.getText().trim().equalsIgnoreCase(dtf.format(now).toString()))
	    			Reporter.log("Payment date is matched");
	    		else
	    			Assert.fail("Payment date is not matched");
	    	}catch(Exception e) {
	    		Assert.fail("Payment date is missed");
	    	}
	        return this;
	    }
		
		 /**
	     * Click 'Continue with my upgrade' button 
	     */
		public void clickContinueWithMyUpgradeButton() {
			try {
				continueWithMyUpgradeButton.click();
				Reporter.log("Continue with my upgrade Button is clicked");
			} catch (Exception e) {
				Assert.fail("Continue with my upgrade is not clicked");
			}
		}
		
		
		
		 /**
		 * Verify PII details for PhoneNumber
		 */
		public void verifyPIIforPhoneNumber(String misdin) {
				
			try {
				Assert.assertTrue(checkElementisPIIMasked(customerPhoneNumber, misdin),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Phone Number verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Phone Number");
			}
			
		}
		
		
		 /**
		 * Verify PII details for imeiNumber
		 */
		public void verifyPIIforImeiNumber(String imeiNumber) {
				
			try {
				Assert.assertTrue(checkElementisPIIMasked(customerImeiNumber, imeiNumber),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Imei Number verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Imei Number");
			}
			
		}
		
		
		/**
		 * Verify PII details for CustomerFirstName
		 */
		public void  verifyPIIforCustomerFirstName(String customerFirstname) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(customerFirstName, customerFirstname),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Customer First Name verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Customer First Name");
			}
			
		}
		
		
		/**
		 * Verify PII details for CustomerName
		 */
		public void  verifyPIIforCustomerName(String customername) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(customerName, customername),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Customer Name verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Customer Name");
			}
			
		}
		
		
		 /**
		 * Verify PII details for PaymentInfo
		 */
		public void verifyPIIforPaymentInfo(String paymentinfo) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(customerPaymentInfo, paymentinfo),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Payment Info verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Payment Info");
			}
			
		}
		
		
		

		 /**
		 * Verify PII details for Zip Code
		 */
		public void verifyPIIforZipCode(String zipCode) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(customerZipCode, zipCode),"No PII masking for Selected Line");
				Reporter.log("PII Masking for Zip Code verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Zip Code");
			}
			
		}
		
		
		/**
		 * Verify PII details for BAN Number
		 */
		public void verifyPIIforBanNumber(String tmobileAccountNumber) {
			
			try {
				Assert.assertTrue(checkElementisPIIMasked(customerTmobileAccountNumber, tmobileAccountNumber),"No PII masking for Selected Line");
				Reporter.log("PII Masking for T Mobile Account Number verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the T Mobile Account Number");
			}
			
		}
		
		
}