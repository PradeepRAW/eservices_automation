package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 * 
 */
public class PrepaidFamilyControlsPage extends CommonPage {

	private static final String webGuardPage = "family_controls/webguard";

	@FindBy(xpath = "//p[@class='Display3 text-center']")
	private WebElement headerOfFamilyControlsPage;

	@FindBy(xpath = "//*[contains(text(),'Web Guard')]")
	private WebElement webGuardTab;

	@FindBy(xpath = "(//*[contains(text(),'Web Guard')]//../p[2])")
	private WebElement subTextOfWebGuardTab;

	@FindBy(xpath = "(//*[@class='body d-inline-block col-12 no-padding ng-star-inserted'])")
	private WebElement descriptionOfWebGuardTab;

	@FindBy(xpath = "//p[contains(text(),'Web Guard')]//..//..//*[contains(@class,'arrow-right d-inline-block')]")
	private WebElement caratSignOfWebGuardTab;

	// @FindBy(xpath = "(//*[@class='body radio-container']/label)")
	// private List<WebElement> radioButtonsOnWebGuardPage;

	@FindBy(css = "p.Display3.text-center")
	private WebElement headerOfWebGuardPage;

	@FindBy(css = "p.Display6")
	private WebElement permissionTextOnWebGuardPage;

	@FindBy(css = "p.body.padding-top-xsmall")
	private WebElement subTextOnWebGuardPage;

	@FindBy(xpath = "(//input[@name='selector' and @type='radio'])")
	private List<WebElement> radioButtonsOnWebGuardPage;

	@FindBy(xpath = "//label[contains(text(),'Child')]")
	private WebElement childWebGuard;

	@FindBy(xpath = "//label[contains(text(),'Teen')]")
	private WebElement teenWebGuard;

	@FindBy(xpath = "//label[contains(text(),'Young Adult')]")
	private WebElement youngAdultWebGuard;

	@FindBy(xpath = "//label[contains(text(),'No Restrictions')]")
	private WebElement noRestrictionsWebGuard;

	@FindBy(xpath = "//*[contains(@class,'PrimaryCTA full-btn-width')]")
	private WebElement saveChangesCTA;

	@FindBy(xpath = "//*[contains(text(),'Show Filter Details')]")
	private WebElement showFilterDetailsLink;

	@FindBy(xpath = "//*[contains(text(),'Hide Filter Details')]")
	private WebElement hideFilterDetailsLink;

	@FindBy(xpath = "//div[@class='row padding-top-xl ng-star-inserted']")
	private WebElement showFilterDetailsSection;

	@FindBy(xpath = "//label[contains(text(),'Child')]")
	private WebElement textChild;

	@FindBy(xpath = "//p[contains(text(),'Child')]/../p[@class='body padding-top-small']")
	private WebElement subTextOfChildText;

	@FindBy(xpath = "//label[contains(text(),'Teen')]")
	private WebElement textTeen;

	@FindBy(xpath = "//p[contains(text(),'Teen')]/../p[@class='body padding-top-small']")
	private WebElement subTextOfTeenText;

	@FindBy(xpath = "//label[contains(text(),'Young Adult')]")
	private WebElement textYoungAdult;

	@FindBy(xpath = "//p[contains(text(),'Young Adult')]/../p[@class='body padding-top-small']")
	private WebElement subTextOfYoungAdultText;

	@FindBy(xpath = "//label[contains(text(),'No Restriction')]")
	private WebElement textNoRestriction;

	@FindBy(xpath = "//p[contains(text(),'No Restriction')]/../p[@class='body padding-top-small']")
	private WebElement subTextOfNoRestrictionText;

	public PrepaidFamilyControlsPage(WebDriver webDriver) {
		super(webDriver);
	}

	// Verify Family Control page
	public PrepaidFamilyControlsPage verifyFamilyControlsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			// verifyPageUrl(familyControlPage);
			Reporter.log("Family Control page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Family Control page not displayed");
		}
		return this;
	}

	// Verify Header of Family Control page
	public PrepaidFamilyControlsPage verifyHeaderOfFamilyControlsPage() {
		try {
			Assert.assertEquals(headerOfFamilyControlsPage.getText().trim(), "Family Controls",
					"Header of Family Controls page is mismatched");
			Reporter.log("Header of Family Control page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Header of Family Control page is not displayed");
		}
		return this;
	}

	// Verify We Guard Tab of Family Control page
	public PrepaidFamilyControlsPage verifyWebGuardTab() {
		try {
			webGuardTab.isDisplayed();
			Assert.assertEquals(webGuardTab.getText().trim(), "Web Guard", "Text of Web Guard tab is mismatched");
			Reporter.log("WebGuard tab is displayed on Family Controls page");
		} catch (NoSuchElementException e) {
			Assert.fail("WebGuard tab is not displayed on Family Controls page");
		}
		return this;
	}

	// Verify subtext of Web Guard Tab of Family Control page
	public PrepaidFamilyControlsPage verifySubTextOnWebGuardTab() {
		try {
			subTextOfWebGuardTab.isDisplayed();
			Assert.assertEquals(subTextOfWebGuardTab.getText().trim(), "Prevent access to adult web content.",
					"SubText of Web Guard tab is mismatched");
			Reporter.log("SubText of WebGuard tab is displayed on Family Controls page");
		} catch (NoSuchElementException e) {
			Assert.fail("SubText of WebGuard tab is not displayed on Family Controls page");
		}
		return this;
	}

	// Verify description of Web Guard Tab of Family Control page
	public PrepaidFamilyControlsPage verifyDescriptionOnWebGuardTab() {
		try {
			String subtext = "Does not restrict content when browsing over Wi-Fi, when visiting secured (HTTPS) websites, or accessing content via any application.";
			descriptionOfWebGuardTab.isDisplayed();
			Assert.assertEquals(descriptionOfWebGuardTab.getText().trim().contains(subtext), true,
					"Description of Web Guard tab is mismatched");
			Reporter.log("Description of WebGuard tab is displayed on Family Controls page");
		} catch (NoSuchElementException e) {
			Assert.fail("Description of WebGuard tab is not displayed on Family Controls page");
		}
		return this;
	}

	// get current Web Guard value
	public String getCurrentWebGuardSelection() {
		String currentWebGuard = null;
		try {
			currentWebGuard = (descriptionOfWebGuardTab.getText().trim().split("Web content filter:"))[1].trim();
		} catch (NoSuchElementException e) {
			Assert.fail("Description of WebGuard tab is not displayed on Family Controls page");
		}
		return currentWebGuard;
	}

	// Click on Carat sign of WebGuardTab
	public PrepaidFamilyControlsPage clickOnCaratSignOfWebGuardTab() {
		try {
			caratSignOfWebGuardTab.click();
			verifyWebGuardPage();
			Reporter.log("Web Guard tab cliked");
		} catch (NoSuchElementException e) {
			Assert.fail("Web Guard tab not clicked under Family Controls page");
		}
		return this;
	}

	// Verify Family Control page
	public PrepaidFamilyControlsPage verifyWebGuardPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(webGuardPage);
			Reporter.log("Web Guard page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Web Guard page is not displayed");
		}
		return this;
	}

	// Verify Family Control page
	public PrepaidFamilyControlsPage verifySelectedWebGuardOptionAndDisplayedOnPreviousPage(
			String optionOnWebGuardBlade) {
		String getSelectionPermission = null;
		// System.out.println("Total options are " + radioButtonsOnWebGuardPage.size());
		int count = 0;
		try {
			for (int i = 0; i < radioButtonsOnWebGuardPage.size(); i++) {
				if (radioButtonsOnWebGuardPage.get(i).isSelected()) {
					getSelectionPermission = radioButtonsOnWebGuardPage.get(i).getAttribute("id").trim();
					count++;
					// noRestrictionsRadioButton.click();
					break;
				}
			}
			if (count > 0)
				Reporter.log("Able to read selected Permission Option");
			else
				Assert.fail("No Permission option selected on Web Guard page");
		} catch (NoSuchElementException e) {
			Assert.fail("Web Guard page is not displayed");
		}
		if (optionOnWebGuardBlade.equals(getSelectionPermission))
			Reporter.log("Selected Web Guard and Web Guard displayed on Blade are matched");
		else
			Assert.fail("Selected Web Guard and Web Guard displayed on Blade are mismatched");
		return this;
	}

	// Select Child Web Guard option
	// If that option already selected,then we will select another option, save it
	// and again come back to select Child option.
	public PrepaidFamilyControlsPage selectChildWebGuardOption() {
		boolean alreadySelected = false;
		try {
			if (radioButtonsOnWebGuardPage.get(0).isSelected()) {
				alreadySelected = true;
			}
			if (alreadySelected == true) {
				teenWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
				childWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			} else if (alreadySelected == false) {
				childWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Not able to Select Child Web Guard");
		}
		return this;
	}

	// Select Teen Web Guard option
	// If that option already selected,then we will select another option, save it
	// and again come back to select Teen option.
	public PrepaidFamilyControlsPage selectTeenWebGuardOption() {
		boolean alreadySelected = false;
		try {
			if (radioButtonsOnWebGuardPage.get(1).isSelected()) {
				alreadySelected = true;
			}
			if (alreadySelected == true) {
				childWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
				teenWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			} else if (alreadySelected == false) {
				youngAdultWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Not able to Select Teen Web Guard");
		}
		return this;
	}

	// Select Young Adult Web Guard option
	// If that option already selected,then we will select another option, save it
	// and again come back to select Young Adult option.
	public PrepaidFamilyControlsPage selectYoungAdultWebGuardOption() {
		boolean alreadySelected = false;
		try {
			if (radioButtonsOnWebGuardPage.get(2).isSelected()) {
				alreadySelected = true;
			}
			if (alreadySelected == true) {
				noRestrictionsWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
				youngAdultWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			} else if (alreadySelected == false) {
				youngAdultWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Not able to Select Young Adult Web Guard");
		}
		return this;
	}

	// Select No Restriction Web Guard option
	// If that option already selected,then we will select another option, save it
	// and again com back to this to select No Restriction option.
	public PrepaidFamilyControlsPage selectNoRestrictionsWebGuardOption() {
		boolean alreadySelected = false;
		try {
			if (radioButtonsOnWebGuardPage.get(3).isSelected()) {
				alreadySelected = true;
			}
			if (alreadySelected == true) {
				youngAdultWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
				noRestrictionsWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			} else if (alreadySelected == false) {
				noRestrictionsWebGuard.click();
				clickOnSaveChangesCTAAndCheckWebGuardSelectionValues();
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Not able to Select No Restrictions Web Guard");
		}
		return this;
	}

	// Click on Save Changes CTA.
	public PrepaidFamilyControlsPage clickOnSaveChangesCTA() {
		try {
			saveChangesCTA.click();
			checkPageIsReady();
			waitforSpinner();
		} catch (NoSuchElementException e) {
			Assert.fail("Not able to click Save Changes CTA");
		}
		return this;
	}

	// click on Save Changes and coming back to Web Guard page.
	public String clickOnSaveChangesCTAAndCheckWebGuardSelectionValues() {
		String currentWebGuard = null;
		try {
			clickOnSaveChangesCTA();
			currentWebGuard = getCurrentWebGuardSelection();
			clickOnCaratSignOfWebGuardTab();
			verifyWebGuardPage();
			verifySelectedWebGuardOptionAndDisplayedOnPreviousPage(currentWebGuard);
		} catch (NoSuchElementException e) {
			Assert.fail("Not able to Select Child Web Guard");
		}
		return currentWebGuard;
	}

	// Verify all text and options on Web Guard page
	public PrepaidFamilyControlsPage verifyHeaderAndPermissiontextWebGuardPage() {
		String subText = "Does not restrict content when browsing over Wi-Fi, when visiting secured (HTTPS) websites, or accessing content via any application.";
		try {
			isWebElementDisplayed(headerOfWebGuardPage, "Web Guard page");
			Assert.assertEquals(headerOfWebGuardPage.getText().trim(), "Web Guard",
					"Header on Web Guard Page is mismatched");

			isWebElementDisplayed(permissionTextOnWebGuardPage, "Web Guard page");
			Assert.assertEquals(permissionTextOnWebGuardPage.getText().trim(), "Permissions",
					"Permissions text on Web Guard Page is mismatched");

			isWebElementDisplayed(subTextOnWebGuardPage, "Web Guard page");
			Assert.assertEquals(subTextOnWebGuardPage.getText().trim(), subText,
					"Subtext below Permissions text on Web Guard Page is mismatched");
		} catch (NoSuchElementException e) {
			Assert.fail("Header/Permission texts are not displayed on Web Guard page");
		}
		return this;
	}

	/**
	 * Check Element Is Displayed Or Not
	 * 
	 * @param element
	 * @return
	 */
	public PrepaidFamilyControlsPage isWebElementDisplayed(WebElement element, String page) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element));
			element.isDisplayed();
			Reporter.log(element + " is displayed on " + page);
		} catch (Exception e) {
			Assert.fail(element + " is not displayed on " + page);
		}
		return this;
	}

	// Check whether Child Web Guard is displayed or not.
	public PrepaidFamilyControlsPage checkChildWebGuardOption() {
		String childWebGuardOption = null;
		try {
			childWebGuardOption = radioButtonsOnWebGuardPage.get(0).getAttribute("id").trim();
			Assert.assertEquals(childWebGuardOption, "Child", "Child WebGuard option is not displayed");
			Reporter.log("Child WebGuard option is displayed.");
		} catch (Exception e) {
			Assert.fail("Child WebGuard option is not displayed.");
		}
		return this;
	}

	// Check whether Teen Web Guard is displayed or not.
	public PrepaidFamilyControlsPage checkTeenWebGuardOption() {
		String teenWebGuardOption = null;
		try {
			teenWebGuardOption = radioButtonsOnWebGuardPage.get(1).getAttribute("id").trim();
			Assert.assertEquals(teenWebGuardOption, "Teen", "Teen WebGuard option is not displayed");
			Reporter.log("Teen WebGuard option is displayed.");
		} catch (Exception e) {
			Assert.fail("Teen WebGuard option is not displayed.");
		}
		return this;
	}

	// Check whether Young Adult Web Guard is displayed or not.
	public PrepaidFamilyControlsPage checkYoungAdultWebGuardOption() {
		String youngAdultWebGuardOption = null;
		try {
			youngAdultWebGuardOption = radioButtonsOnWebGuardPage.get(2).getAttribute("id").trim();
			Assert.assertEquals(youngAdultWebGuardOption, "Young Adult",
					"Young Adult WebGuard option is not displayed");
			Reporter.log("Young Adult WebGuard option is displayed.");
		} catch (Exception e) {
			Assert.fail("Young Adult WebGuard option is not displayed.");
		}
		return this;
	}

	// Check whether No Restrictions Web Guard is displayed or not.
	public PrepaidFamilyControlsPage checkNoRestrictionsWebGuardOption() {
		String noRestrictionsWebGuardOption = null;
		try {
			noRestrictionsWebGuardOption = radioButtonsOnWebGuardPage.get(3).getAttribute("id").trim();
			Assert.assertEquals(noRestrictionsWebGuardOption, "No Restrictions",
					"No Restrictions WebGuard option is not displayed");
			Reporter.log("No Restrictions WebGuard option is displayed.");
		} catch (Exception e) {
			Assert.fail("No Restrictions WebGuard option is not displayed.");
		}
		return this;
	}

	// Check all Web Guard options
	public PrepaidFamilyControlsPage checkAllWebGuardOptions() {
		checkChildWebGuardOption();
		checkTeenWebGuardOption();
		checkYoungAdultWebGuardOption();
		checkNoRestrictionsWebGuardOption();

		return this;
	}

	// Check whether Show Filter Details link is displayed or not
	public PrepaidFamilyControlsPage checkShowFilterDetailsLink() {
		try {
			showFilterDetailsLink.isDisplayed();
			Reporter.log("Show Filter Details link is displayed on WebGuard Page.");
		} catch (Exception e) {
			Assert.fail("Show Filter Details link is not displayed on WebGuard Page.");
		}
		return this;
	}

	// Click on Show Filter Details link
	public PrepaidFamilyControlsPage clickShowFilterDetailsLink() {
		try {
			showFilterDetailsLink.click();
			Reporter.log("Show Filter Details link is clicked on WebGuard Page.");
		} catch (Exception e) {
			Assert.fail("Show Filter Details link is not clicked on WebGuard Page.");
		}
		return this;
	}

	// Check when Show Filter Details link not displayed
	public PrepaidFamilyControlsPage checkShowFilterDetailsLinkNotDisplayed() {
		try {
			showFilterDetailsLink.isDisplayed();
			Assert.fail("Show Filter Details link is displayed on WebGuard Page.");
		} catch (Exception e) {
			Reporter.log("Show Filter Details link is not displayed on WebGuard Page.");
		}
		return this;
	}

	// Check whether Show Filter Details link is displayed or not
	public PrepaidFamilyControlsPage checkHideFilterDetailsLink() {
		try {
			hideFilterDetailsLink.isDisplayed();
			Reporter.log("Hide Filter Details link is displayed on WebGuard Page.");
		} catch (Exception e) {
			Assert.fail("Hide Filter Details link is not displayed on WebGuard Page.");
		}
		return this;
	}

	// Hide Filter Details link is not displayed
	public PrepaidFamilyControlsPage checkHideFilterDetailsLinkNotDisplayed() {
		try {
			hideFilterDetailsLink.isDisplayed();
			Assert.fail("Hide Filter Details link is displayed on WebGuard Page.");
		} catch (Exception e) {
			Reporter.log("Hide Filter Details link is not displayed on WebGuard Page.");
		}
		return this;
	}

	// Click on Hide Filter Details link
	public PrepaidFamilyControlsPage clickHideFilterDetailsLink() {
		try {
			hideFilterDetailsLink.click();
			Reporter.log("Hide Filter Details link is clicked on WebGuard Page.");
		} catch (Exception e) {
			Assert.fail("Hide Filter Details link is not clicked on WebGuard Page.");
		}
		return this;
	}

	// Check whether Show Filter Details section displayed or not
	public PrepaidFamilyControlsPage checkShowFilterDetailsSection() {
		try {
			showFilterDetailsSection.isDisplayed();
			Reporter.log("Show Filter Details section is displayed on WebGuard Page.");
		} catch (Exception e) {
			Assert.fail("Show Filter Details section is not displayed on WebGuard Page.");
		}
		return this;
	}

	// Check when Show Filter Details section not displayed
	public PrepaidFamilyControlsPage checkShowFilterDetailsSectionNotDisplayed() {
		try {
			showFilterDetailsSection.isDisplayed();
			Assert.fail(
					"Show Filter Details section is displayed on WebGuard Page even if not clicked on Show Filter Details link.");
		} catch (Exception e) {
			Reporter.log("Show Filter Details section is not displayed on WebGuard Page");
		}
		return this;
	}

	// Verify functionality of Show/Hide filter details link
	public PrepaidFamilyControlsPage checkShowandHideFilterDetailsLinkFunctionality() {
		checkShowFilterDetailsLink();
		checkHideFilterDetailsLinkNotDisplayed();
		checkShowFilterDetailsSectionNotDisplayed();
		clickShowFilterDetailsLink();
		checkHideFilterDetailsLink();
		checkShowFilterDetailsLinkNotDisplayed();
		checkShowFilterDetailsSection();
		clickHideFilterDetailsLink();
		checkShowFilterDetailsLink();
		checkHideFilterDetailsLinkNotDisplayed();
		checkShowFilterDetailsSectionNotDisplayed();
		clickShowFilterDetailsLink();
		return this;
	}

	// Check Child Text and Subtext
	public PrepaidFamilyControlsPage checkChildTextAndSubText() {
		String subtext = "Recommended for all ages. Contains little or no sexual content, situation or themes, no strong or coarse language, no alcohol, tobacco or drug use, and no gambling. Some websites may contain minimal cartoon or fantasy violence.";
		try {
			isWebElementDisplayed(textChild, "Web Guard Page");
			Assert.assertEquals(textChild.getText().trim(), "Child", "Child text is mismatched on Web Guard page");
			isWebElementDisplayed(subTextOfChildText, "Web Guard Page");
			Assert.assertEquals(subTextOfChildText.getText().trim(), subtext,
					"SubText of Child text is mismatched on Web Guard page");
		} catch (Exception e) {
			Reporter.log("Child Text is not displayed on WebGuard Page");
		}
		return this;
	}

	// Check Teen Text and Subtext
	public PrepaidFamilyControlsPage checkTeenTextAndSubText() {
		String subtext = "Recommended for ages 13 and up. May contain mild/infrequent violence, sexual content, situations or themes, profanity or crude humor, alcohol, tobacco or drug use, and simulated gambling.";
		try {
			isWebElementDisplayed(textTeen, "Web Guard Page");
			Assert.assertEquals(textTeen.getText().trim(), "Teen", "Teen text is mismatched on Web Guard page");
			isWebElementDisplayed(subTextOfTeenText, "Web Guard Page");
			Assert.assertEquals(subTextOfTeenText.getText().trim(), subtext,
					"SubText of Teen text is mismatched on Web Guard page");
		} catch (Exception e) {
			Reporter.log("Teen Text is not displayed on WebGuard Page");
		}
		return this;
	}

	// Check Young Adult Text and Subtext
	public PrepaidFamilyControlsPage checkYoungAdultTextAndSubText() {
		String subtext = "Recommended for ages 17 and up. Content may contain frequent or intense graphic violence, sexual content, profanity and crude humor.";
		try {
			isWebElementDisplayed(textYoungAdult, "Web Guard Page");
			Assert.assertEquals(textYoungAdult.getText().trim(), "Young Adult",
					"Young Adult text is mismatched on Web Guard page");
			isWebElementDisplayed(subTextOfYoungAdultText, "Web Guard Page");
			Assert.assertEquals(subTextOfYoungAdultText.getText().trim(), subtext,
					"SubText of Young Adult text is mismatched on Web Guard page");
		} catch (Exception e) {
			Reporter.log("Young Adult Text is not displayed on WebGuard Page");
		}
		return this;
	}

	// Check No Restrictions Text and Subtext
	public PrepaidFamilyControlsPage checkNoRestrictionsTextAndSubText() {
		String subtext = "Provides a completely open and unrestricted browsing experience.";
		try {
			isWebElementDisplayed(textNoRestriction, "Web Guard Page");
			Assert.assertEquals(textNoRestriction.getText().trim(), "No Restrictions",
					"No Restrictions text is mismatched on Web Guard page");
			isWebElementDisplayed(subTextOfNoRestrictionText, "Web Guard Page");
			Assert.assertEquals(subTextOfNoRestrictionText.getText().trim(), subtext,
					"SubText of No Restrictions text is mismatched on Web Guard page");
		} catch (Exception e) {
			Reporter.log("Young Adult Text is not displayed on WebGuard Page");
		}
		return this;
	}

	// Verify all texts
	public PrepaidFamilyControlsPage checkAllTextsUnderShowFilterDetailsSection() {
		checkChildTextAndSubText();
		checkTeenTextAndSubText();
		checkYoungAdultTextAndSubText();
		checkNoRestrictionsTextAndSubText();
		return this;
	}

}
