package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import com.tmobile.eservices.qa.pages.CommonPage;


public class JumpConditionPage extends CommonPage{

	@FindBy(css = "i.checkbox_outline")
	private List<WebElement> deviceConditions;

	@FindBy(css="div.body-copy-description")
	private List<WebElement> conditionsText;

	@FindBy(css="button[ng-click='$ctrl.evaluateDeviceCondition()']")
	private WebElement continueCta;

	private final String pageUrl = "jump/tradein";

	@FindBy(css="[ng-click*='continueToCart']")
	private WebElement acceptAndContinueCta;
	
	@FindBy(xpath = "//div[contains(text(),'Does the Device Power on')]") 
	private WebElement devicePowerOn;
	
	@FindBy(xpath = "//div[contains(text(),'Does the Device have acceptable LCD and Display')]") 
	private WebElement deviceLcdDisplay;
	
	public JumpConditionPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Jump Choice Page
	 * 
	 * @return
	 */
	public JumpConditionPage verifyNewJumpConditionPage() {
		try {
			waitforSpinner();
			verifyPageUrl();
			Reporter.log("New JumpChoicePage is displayed");
		} catch (Exception e) {
			Assert.fail("New JumpChoicePage is not displayed" +e);
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public JumpConditionPage verifyPageUrl() {
		try {
			getDriver().getCurrentUrl().contains(pageUrl);
		} catch (Exception e) {
			Assert.fail("New Jump Choice URL is not diplayed");
		}
		return this;
	}

	/**
	 * verify device condition radio buttons
	 * 
	 * @return true/false
	 */
	public JumpConditionPage verifyDeviceCondtionRadioButtons() {
		try {
			boolean isDisplayed = false;
			for (WebElement radioBtn : deviceConditions) {
				if (radioBtn.isDisplayed()) {
					isDisplayed = true;
				}
			}
			Assert.assertTrue(isDisplayed, "Device Condition Radio buttons is not displayed");
			Reporter.log("Device Condition Radio buttons is displayed");
		} catch (Exception e) {
			Assert.fail("Device Condition Radio buttons is not displayed");
		}
		return this;
	}

	/**
	 * verify good device points are displayed
	 * 
	 * @param point
	 * @return true/false
	 */
	public JumpConditionPage verifyGoodDevicePoints(String point) {
		try {
			boolean isGoodDevicePoints = false;
			for (WebElement points : conditionsText) {
				if (points.isDisplayed() && points.getText().equals(point)) {
					isGoodDevicePoints = true;
					Assert.assertTrue(isGoodDevicePoints, "Good deviceCheckbox is not displaying");
				}
				break;
			}
			Reporter.log("Good deviceCheckbox is  displaying");
		} catch (Exception e) {
			Assert.fail("Good deviceCheckbox is not displaying");
		}
		return this;
	}

	/**
	 * select LCD Display
	 *
	 * @return true/false
	 */
	public JumpConditionPage clickLcdDisplayCheckBox() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(deviceConditions.get(2));	
			Reporter.log("Clickable LCD Display");
		} catch (Exception e) {
			Assert.fail("Failed to click on LCD Display");
		}
		return this;
	}
	
	/**
	 * select Disable iphone
	 * 
	 * @param point
	 * @return true/false
	 */
	public JumpConditionPage clickDisableiPhoneCheckBox() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(deviceConditions.get(0));	
			Reporter.log("Clickable Disable iPhone");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Disable iPhone");
		}
		return this;
	}
	
	/**
	 * select Device power on
	 * 
	 * @param point
	 * @return true/false
	 */
	public JumpConditionPage clickDevicePowerOnCheckBox() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(deviceConditions.get(3));	
			Reporter.log("Clickable Device power on");
		} catch (Exception e) {
			Assert.fail("Failed to click on Device power on");
		}
		return this;
	}

	/**
	 * verify good device points are displayed
	 * 
	 * @param point
	 * @return true/false
	 */
	public JumpConditionPage DeviceConditionsPageItsGoodComponent() {
		try {
			waitforSpinner();
			verifyDeviceCondtionRadioButtons();
			verifyGoodDevicePoints("Does the Device Power on?");
			verifyGoodDevicePoints("Does the Device have acceptable LCD and Display?");
			verifyGoodDevicePoints("Have you disabled the Find My iPhone/iPad feature from your iPhone/iPad?");
		} catch (Exception e) {
			Assert.fail("Device good points are Not Displayed Correctly");
		}
		return this;
	}

	/**
	 * select good device points are displayed
	 *
	 * @return true/false
	 */
	public JumpConditionPage selectGoodComponents() {
		waitforSpinner();
		try {
			waitFor(ExpectedConditions.elementToBeClickable(devicePowerOn),20);
			devicePowerOn.click();
			deviceLcdDisplay.click();
			/*if(deviceFindIphoneOption.size()>0){
				deviceFindIphoneOption.get(0).click();
			}*/
		} catch (Exception e) {
			Assert.fail("Failed to click on Good points");
		}
		return this;
	}

	/**
	 * Click Continue
	 * 
	 * @return
	 */
	public JumpConditionPage clickContinueButton() {
		waitforSpinner();
		try {
			waitFor(ExpectedConditions.elementToBeClickable(continueCta),15);
			moveToElement(continueCta);
			continueCta.click();
			Reporter.log("Clicked on Continue Button");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Continue Button");
		}
		return this;
	}

	/**
	 * Verify Accept & Continue Button
	 * 
	 * @return
	 */
	public JumpConditionPage verifyAcceptAndContinueButton() {
		try {
			waitforSpinner();
			Assert.assertTrue(acceptAndContinueCta.isDisplayed(),"Accept & Continue Cta is not displaying");
			Reporter.log("Accept & Continue Cta is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify acceptAndContinue Button");
		}
		return this;
	}

	/**
	 * Click Accept & Continue Button
	 *
	 * @return
	 */
	public JumpConditionPage clickAcceptAndContinueButton() {
		try {
			waitforSpinner();
			acceptAndContinueCta.click();
			Reporter.log("Clicked on acceptAndContinueButton");
		} catch (Exception e) {
			Assert.fail("Failed to click on AcceptAndContinue Button");
		}
		return this;
	}
}
