package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;
/**
 * @author RNallam1
 *
 */
public class PortInProblemFixPage extends CommonPage {
		
	@FindBy(css = "img[aria-label='fixed icon']")
	private WebElement imgIcon;

	@FindBy(xpath = "//*[contains(text(),'Thanks for the help')]")
	private WebElement helpHeader;
	
	@FindBy(xpath = "//*[contains(text(),'resolved')]")
	private WebElement resovedMsg;

	@FindBy(css = ".secondary-cta")
	private WebElement allDoneBtn;
	
	/**
	 * @param webDriver
	 */
	public PortInProblemFixPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify port in problem fix page
	 */
	public PortInProblemFixPage verifyPortInProblemFixPage() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			Assert.assertTrue(getDriver().getCurrentUrl().contains("port-in/problemFix"));
			Reporter.log("Portin problem fix page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Portin problem fix page not displayed");
		}
		return this;
	}
}
