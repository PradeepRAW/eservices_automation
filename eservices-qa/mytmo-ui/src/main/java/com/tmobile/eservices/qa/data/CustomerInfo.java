package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

/**
 * This class bind the data from excel sheet
 * 
 * @author nsuvvada
 *
 */
public class CustomerInfo {

	@Data(name = "FirstName")
	private String firstName;
	@Data(name = "LastName")
	private String lastName;
	@Data(name = "Phone")
	private String phone;
	@Data(name = "SSN")
	private String ssn;
	@Data(name = "UserName")
	private String userName;
	@Data(name = "Password")
	private String password;
	@Data(name = "Confirm")
	private String confirmPassword;
	@Data(name = "Email")
	private String emailAddress;
	@Data(name = "ConfirmEmailAddress")
	private String confirmEmailAddress;
	@Data(name = "DriverLicense")
	private String txtdriverLicense;
	@Data(name = "DateOfBirth")
	private String dateOfBirth;
	@Data(name = "Location")
	private String location;

	@Data
	private Address address;
	@Data
	private Payment payment;
	@Data(name = "ConfirmPassword")
	private String confirmPwd;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @return the ssn
	 */
	public String getSsn() {
		return ssn;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @return the confirmEmailAddress
	 */
	public String getConfirmEmailAddress() {
		return confirmEmailAddress;
	}

	/**
	 * @return the txtdriverLicense
	 */
	public String getTxtdriverLicense() {
		return txtdriverLicense;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @return the payment
	 */
	public Payment getPayment() {
		return payment;
	}

	public String getConfirmPwd() {
		return confirmPwd;
	}

	@Override
	public String toString() {
		return "CustomerInfo [" + (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (lastName != null ? "lastName=" + lastName + ", " : "")
				+ (phone != null ? "phone=" + phone + ", " : "") + (ssn != null ? "ssn=" + ssn + ", " : "")
				+ (userName != null ? "userName=" + userName + ", " : "")
				+ (password != null ? "password=" + password + ", " : "")
				+ (confirmPassword != null ? "confirmPassword=" + confirmPassword + ", " : "")
				+ (emailAddress != null ? "emailAddress=" + emailAddress + ", " : "")
				+ (confirmEmailAddress != null ? "confirmEmailAddress=" + confirmEmailAddress + ", " : "")
				+ (txtdriverLicense != null ? "txtdriverLicense=" + txtdriverLicense + ", " : "")
				+ (dateOfBirth != null ? "dateOfBirth=" + dateOfBirth + ", " : "")
				+ (location != null ? "location=" + location + ", " : "")
				+ (address != null ? "address=" + address + ", " : "")
				+ (payment != null ? "payment=" + payment + ", " : "")
				+ (confirmPwd != null ? "confirmPwd=" + confirmPwd : "") + "]";
	}

}
