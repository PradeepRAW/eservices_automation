package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

/**
 * 
 * @author Thulasiram
 *
 */
public class MyTmoNpiData {

	@Data(name = "TestName")
	private String testName;

	@Data(name = "missdn")
	private String missdn;

	@Data(name = "password")
	private String password;

	@Data(name = "sku")
	private String sku;

	@Data(name = "familyid")
	private String familyid;

	@Data(name = "modelname")
	private String modelname;

	@Data(name = "color")
	private String color;

	@Data(name = "memory")
	private String memory;

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getMissdn() {
		return missdn;
	}

	public void setMissdn(String missdn) {
		this.missdn = missdn;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getFamilyid() {
		return familyid;
	}

	public void setFamilyid(String familyid) {
		this.familyid = familyid;
	}

	public String getModelname() {
		return modelname;
	}

	public void setModelname(String modelname) {
		this.modelname = modelname;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	@Data(name = "availability")
	private String availability;

	@Override
	public String toString() {
		return "[" + (sku != null ? "SKU=" + sku + "" : "") + "]";
	}
}