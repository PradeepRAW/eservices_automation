package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author ramesh
 *
 */
public class NewsPage extends TmngCommonPage {

	private final String newsPageUrl = "news";

	@FindBy(css = ".full-price-now div.sub-text")
	private WebElement storeName;

	@FindBy(css = ".pk--subscription-heading")
	private WebElement subscribeHeader;

	@FindBy(css = "a[href*='rss']")
	private WebElement rssLink;

	@FindBy(css = "a[href*='contact-us'][class='pk--subscription-link']")
	private WebElement contactUsLink;

	public NewsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify news page
	 */
	public NewsPage verifyNewsPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(newsPageUrl), 60);
			Reporter.log("News page displayed");
		} catch (Exception ex) {
			Assert.fail("News page not displayed");
		}
		return this;
	}

	/**
	 * Verify rss feed link
	 */
	public NewsPage verifyRSSFeedLink() {
		try {
			moveToElement(subscribeHeader);
			Assert.assertTrue(isElementDisplayed(rssLink));
			Reporter.log("RSS feed link displayed");
		} catch (Exception e) {
			Assert.fail("RSS feed link not displayed");
		}
		return this;
	}

	/**
	 * Click rss feed link
	 */
	public NewsPage clickRSSFeedLink() {
		try {
			rssLink.click();
		} catch (Exception e) {
			Reporter.log("Click on RSS feed link failed");
			Assert.fail("Click on RSS feed link failed");
		}
		return this;
	}

	/**
	 * Verify contact us link
	 */
	public NewsPage verifyContactUsLink() {
		try {
			moveToElement(contactUsLink);
			Assert.assertTrue(isElementDisplayed(contactUsLink));
			Reporter.log("Contact us link displayed");
		} catch (Exception e) {
			Assert.fail("Contact us link not displayed");
		}
		return this;
	}

	/**
	 * Click and verify contact us link
	 */
	public NewsPage clickAndVerifyContactUsLink() {
		try {
			contactUsLink.click();
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains("contact"));
		} catch (Exception e) {
			Reporter.log("Click on contact us link failed");
			Assert.fail("Click on contact us link failed");
		}
		return this;
	}
}
