package com.tmobile.eservices.qa.pages.shop;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;
import org.testng.Reporter;

import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eservices.qa.pages.CommonPage;

public class OfferDetailsPage extends CommonPage {

	public static final String CONTINUE_WITH_OFFER = "continuewithoffer";
	public static final String CONTINUE_WITH_OUT_OFFER = "continuewithoutoffer";

	@FindBy(css = "span[ng-bind*='$ctrl.contentData.faqHeaderTxt']")
	private WebElement frequentlyAskedQuestionsSection;

	@FindBy(css = "p[ng-bind-html*='$ctrl.promoDetailsData.legalTxt']")
	private WebElement legalText;

	@FindBys(@FindBy(css = "span[ng-bind-html*='steps.offerRedemtionStepHdr']"))
	private List<WebElement> redemptionSteps;

	@FindBy(css = "span[ng-bind*='$ctrl.contentData.ratePlanErrorFirst']")
	private WebElement errorMessage;

	@FindBy(xpath = "//button[contains(.,'Shop now')]")
	private WebElement shopNow;

	@FindBy(css = "span[ng-bind*='promoDetailsData.notInterestedLinkTxt']")
	private WebElement notInterestedLink;

	@FindBy(xpath = "//button[contains(text(), 'Change Rate Plan')]")
	private WebElement changeRatePlanButton;

	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	//@FindBy(css = "button.primary-cta-normal")
	private WebElement acceptAndContinueBtn;
	
	@FindBy(css = "p[ng-bind-html*='promoDetailsData.offerDescriptionHeader']")
	private WebElement offerDetailsText;
	
	@FindBy(css = "#contactModalLink")
	private WebElement contactUs;
	
	@FindBy(css = "button[ng-click*=primary]")
	private WebElement primaryCTA;
	
	@FindBy(css= "div.footer-pict-nav")
	private WebElement orderDetailsPageFooter;

	@FindBy(css= "span[ng-bind-html*='steps.offerRedemtionStepTxt | displayHTML']")
	private List<WebElement> tradeInDeviceDetails;
	
	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement upgradeCTA;
	
	@FindBy(css = "button[ng-click*=addALine]")
	private WebElement addALineCTA;
	
	
	public OfferDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Offer Details Page
	 * 
	 * @return
	 */
	public OfferDetailsPage verifyOfferDetailsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl();
			Assert.assertTrue(offerDetailsText.isDisplayed(), "Offer Details text is not present");
			Reporter.log("Navigated to Offer Details page");
		} catch (Exception e) {
			Assert.fail("Offer Details Page is not dispalyed");
		}return this;
	}
	
	 /**
     * Verify that current page URL matches the expected URL.
     */
    public OfferDetailsPage verifyPageUrl() {
    	try {
			getDriver().getCurrentUrl().contains("offerDetails");
    	} catch (Exception e) {
    		Assert.fail("Offert details URL is not diplayed");
    	}
        return this;
    }

	/**
	 * Choose offer Details and Enters the flow
	 * 
	 * @return
	 */
	public OfferDetailsPage chooseOffer(String option) {
		verifyOfferDetailsPage();
		if (CONTINUE_WITH_OFFER.equalsIgnoreCase(option)) {
			clickAcceptAndContinue();
		} else if (CONTINUE_WITH_OUT_OFFER.equalsIgnoreCase(option)) {
			clickContinueWithoutTheOffer();
		}
		return this;
	}

	/**
	 * Verify Frequently Asked Questions
	 * 
	 * @return
	 */
	public OfferDetailsPage verifyFrequentilyAskedQuestions() {
		try {
			frequentlyAskedQuestionsSection.isDisplayed();
			Reporter.log("Frequently asked questions section is displayed");
		} catch (Exception e) {
			Assert.fail("FrequentilyAskedQuestions is not displayed ");
		}
		return this;
	}

	/**
	 * Click Frequently Asked Questions
	 */
	public OfferDetailsPage clickFrequentilyAskedQuestions() {
		try {
			frequentlyAskedQuestionsSection.click();
			Reporter.log("Clicked Frequently Asked Questions");
		} catch (Exception e) {
			Assert.fail("FrequentilyAskedQuestionsSection is not available to click");
		}
		return this;
	}

	/**
	 * Verify Redemption Steps
	 * 
	 * @return
	 */
	public OfferDetailsPage verifyRedemptionSteps() {
		for (WebElement webElement : redemptionSteps) {
			if (webElement.getText().contains("redemption")) {
				Reporter.log("Redemption steps displayed");
			} else {
				Assert.fail("Redemption steps not displayed");
			}
		}
		return this;
	}

	/**
	 * Verify Legal Text
	 * 
	 * @return
	 */
	public OfferDetailsPage verifyLegalText() {
		try {
			legalText.isDisplayed();
			Reporter.log("Legal text is displayed");
		} catch (Exception e) {
			Assert.fail("LegalText is not displayed");
		}
		return this;
	}

	/**
	 * Verify Not Interested link
	 * 
	 * @return
	 */
	public OfferDetailsPage verifyNotInterestedLink() {
		try {
			Assert.assertTrue(notInterestedLink.isDisplayed());
			Reporter.log("Continue without the offer link is displayed");
		} catch (Exception e) {
			Assert.fail("NotInterestedLink is not displayed");
		}
		return this;
	}

	/**
	 * Click Continue Without The Offer
	 */
	public OfferDetailsPage clickContinueWithoutTheOffer() {
		try {
			shopNow.click();
			Reporter.log("Clicked on Continue Without Offer Button");
		} catch (Exception e) {
			Assert.fail("ContinueWithoutTheOffer is not displayed clcik");
		}
		return this;
	}

	/**
	 * Click Not Interested Link
	 */
	public OfferDetailsPage clickNotInterestedlink() {
		try {
			notInterestedLink.click();
			Reporter.log("Clicked on Not Interested Link");
		} catch (Exception e) {
			Assert.fail("NotInterestedlink is not displayed to click");
		}
		return this;
	}

	/**
	 * click accept and continue button
	 */
	public OfferDetailsPage clickAcceptAndContinue() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(acceptAndContinueBtn);
			Reporter.log("Clicked on Accept and Continue Button");
		} catch (Exception e) {
			Assert.fail("AcceptAndContinue button is not available to clcik");
		}
		return this;

	}
	
	/**
	 * Verify Offer Product name
	 */
	public OfferDetailsPage verifyProductName(String deviceName) {
		try {
			checkPageIsReady();
			Assert.assertTrue(offerDetailsText.getText().contains(deviceName), "Product name is not displayed");
			Reporter.log("Product Name is displayed in Offer details page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Product Name in Offer details page");
		}
		return this;
	}
	
	/**
	 * Verify Offer EIP Down Payment
	 */
	
	public OfferDetailsPage verifyEIPDownPayment() {
		try {
			checkPageIsReady();
			Assert.assertTrue(legalText.getText().contains("/down"), "Down Payment is not displayed");
			Reporter.log("Down Payment is displayed in Offer details page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Down Payment in Offer details page");
		}
		return this;
	}
	
	/**
	 * Verify Offer EIP Installment Terms
	 */
	
	public OfferDetailsPage verifyDeviceEIPInstallmentTerms() {
		
		try {
			checkPageIsReady();
			Assert.assertTrue(legalText.isDisplayed());
			Assert.assertTrue((legalText.getText().contains("mos.")),
					"EIP Loan Term is not displayed correctly");
			Reporter.log("EIP Monthly Installment Term is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Installment Term in Offer details page");
		}
		return this;
	}
	
	/**
	 * Verify Offer EIP Monthly Payment
	 */
	
	public OfferDetailsPage verifyEIPMonthlyPayment() {
		try {
			checkPageIsReady();
			Assert.assertTrue(legalText.getText().contains("monthly"), "Monthly Payment is not displayed");
			Reporter.log("Monthly Payment is displayed in Offer details page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly Payment in Offer details page");
		}
		return this;
	}
	
	/**
	 * Verify Contact US CTA
	 * 
	 * @return
	 */
	public OfferDetailsPage verifyContactUsCTADisplayed() {
		try {
			checkPageIsReady();
			contactUs.isDisplayed();
			Assert.assertTrue(contactUs.isDisplayed(), "Contact us CTA is not displayed");
			Reporter.log("Contact us CTA is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Display Contact Us CTA");
		}
		return this;
	}
	

	/**
	 * Verify Contact US CTA
	 * 
	 * @return
	 */
	public OfferDetailsPage verifyContactUsCTANotDisplayed() {
		try {
			contactUs.isDisplayed();
			Assert.assertFalse(contactUs.isDisplayed(), "Contact us CTA is displayed");
			Reporter.log("Contact us CTA is Not Displayed");
		} catch (Exception e) {
			Assert.fail("Contact Us CTA is Displayed");
		}
		return this;
	}
	
	/**
	 * Click Frequently Asked Questions
	 */
	public OfferDetailsPage verifyPrimaryCTA() {
		try {
			primaryCTA.isDisplayed();
			Assert.assertTrue(primaryCTA.isDisplayed(), "Primary CTA is displayed");
			Reporter.log("Primary CTA is Not Displayed");
		} catch (Exception e) {
			Assert.fail("Primary CTA is Displayed");
		}
		return this;
	}
	
	/**
	 * Verify Offer details page footer
	 */
	public OfferDetailsPage verifyOfferDetailsPageFooterNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(orderDetailsPageFooter), "Offer details Page footer is displayed");
			Reporter.log("Offer details Page footer is  not displayedd");
		} catch (Exception e) {
			Assert.fail("Offer details Page footer is displayed");
		}
		return this;
	}
	
	/**
	 * verify Primary CTA Not Present
	 */
	public OfferDetailsPage verifyPrimaryCTANotPresent() {
		try {
			waitforSpinner();
			Assert.assertEquals(0,getDriver().findElements(By.cssSelector("button[ng-click*=primary]")).size());
			Reporter.log("Primary CTA is not Displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to hide primary CTA");
		}
		return this;
	}
	
	/**
	 * validate Trade in Device details 
	 */
	public OfferDetailsPage verifyTradeInDeviceDetails() {
		try {
			waitforSpinner();
			Assert.assertTrue(tradeInDeviceDetails.get(1).isDisplayed(), "Trade in Device details are not displaying");
			Reporter.log("Trade in Device details are displaying");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in device details");
		}
		return this;
	}
	
	/**
	 * To get Trade in Device details
	 */
	public String getTradeInDeviceDetails() {
		String deviceDetails=null;
		try {
			deviceDetails=tradeInDeviceDetails.get(1).getText();
			Reporter.log("Trade in Device details are stored");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in Device details storing");
		}
		return deviceDetails;
	}

	/**
	 * Validate Trade in Device
	 */
	public OfferDetailsPage verifyTradeInDevice(String deviceInfo) {
		try {
			Assert.assertTrue(deviceInfo.contains("iPhone"), "Trade in Device is not displaying");
			Reporter.log("Trade in Device is displaying");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in Device ");
		}
		return this;
	}
	
	/**
	 * Validate Trade in product name
	 */
	public OfferDetailsPage verifyTradeInProductName(String deviceInfo) {
		try {
			Assert.assertTrue(deviceInfo.contains("iPhone 7 Plus 128GB Black - T-Mobile"), "Trade in product name is not displaying");
			Reporter.log("Trade in product name is  displaying");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in Product name ");
		}
		return this;
	}
	
	/**
	 * Validate Trade in value
	 */
	public OfferDetailsPage verifyTradeInValue(String deviceInfo) {
		try {
			Assert.assertTrue(deviceInfo.contains("$"), "Trade in Value is not displaying");
			Reporter.log("Trade in Value is  displaying");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in Value ");
		}
		return this;
	}
	
	/**
	 * Validate Monthly Trade in Product value
	 */
	public OfferDetailsPage verifyMonthlyTradeInValue(String deviceInfo) {
		try {
			Assert.assertTrue(deviceInfo.contains("month"), "Trade in monthly Value is not displaying");
			Reporter.log("Trade in monthly Value is displaying");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in  monthly Value ");
		}
		return this;
	}
	
	/**
	 * Validate Monthly Trade in Product value
	 */
	public OfferDetailsPage verifyTradeInPromoLength(String deviceInfo) {
		try {
			Assert.assertTrue(deviceInfo.contains("/mos"), "Trade in  Promo length is not displaying");
			Reporter.log("Trade in  Promo length is displaying");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in  Promo length");
		}
		return this;
	}
	
	
	/**
	 * Click upgrade cta.
	 *
	 */
	public OfferDetailsPage clickOnUpgradeCTA() {
		try {
			upgradeCTA.click();
		} catch (Exception e) {
			Assert.fail("Click on upgrade cta failed "+e.getMessage());
		}
		return this;
	}
	
	/**
	 * Click add a line cta.
	 *
	 */
	public OfferDetailsPage clickAddALineCTA() {
		try {
			addALineCTA.click();
		} catch (Exception e) {
			Assert.fail("Click on add a line cta failed "+e.getMessage());
		}
		return this;
	}
	
	/**
	 * Verify Modularity
	 *
	 */
	public Boolean getModularityValue() {
		Boolean modularity = null;
		try{
			Map<String, Object> logType = new HashMap<>();
			logType.put("type", "sauce:network");
			List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) getDriver())
					.executeScript("sauce:log", logType);
			for (Map<String, Object> logEntry : logEntries) {
				String url = (String) logEntry.get("url");
				if (url.contains("environmentvalues")) {
					try {
						URL url1 = new URL(url);
						modularity = JsonPath.from(url1).get("modularityFlag");
						if(modularity) {
							Reporter.log("Modularity is ON");
						}else
							Reporter.log("Modularity is OFF");
					} catch (Exception e) {
						Assert.fail("Failed to verify modularity flag");
					}
					break;
				}
			}
		}catch (Exception e){
			Reporter.log("Unable to get modularity value");
		}
		return modularity;
	}
	
	/**
	 * Verify Not Interested CTA
	 */
	public OfferDetailsPage verifyNotInterestedCTA() {
		try {
			Assert.assertFalse(isElementDisplayed(notInterestedLink), "Not Iterested CTA is displayed in offer details page");
			Reporter.log("Not Iterested CTA is not displayed in offer details page");
		} catch (Exception e) {
			Assert.fail("Failed to Not Interestd link is displayed");
		}
		return this;
	}
	
}
