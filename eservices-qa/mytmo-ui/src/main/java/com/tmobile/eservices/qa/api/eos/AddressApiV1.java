package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AddressApiV1 extends ApiCommonLib {

	/***
	 * Summary: Micro services to add,update and pull shipping/Billing/E911/PPU addresses
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response updateAddress(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAddressHeader(apiTestData);
    	String resourceURL = "v1/address/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		System.out.println(response.asString());
		return response;
	}


	private Map<String, String> buildAddressHeader(ApiTestData apiTestData) throws Exception {
		DCPTokenApi dcpTokenApi = new DCPTokenApi();
		final String dcpToken = dcpTokenApi.createPlattoken(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", dcpToken);
		headers.put("Content-Type", "application/json");
		headers.put("transactionType", "inventoryfeed");
		headers.put("applicationId", "MYTMO");
		headers.put("channelId", "MYT");
		headers.put("correlationId", "api-testing");
		headers.put("clientId","e-servicesUI");
		headers.put("transactionBusinessKey", apiTestData.getMsisdn());
		headers.put("transactionBusinessKeyType", "missdn");
		headers.put("usn", "usn");
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}

}


