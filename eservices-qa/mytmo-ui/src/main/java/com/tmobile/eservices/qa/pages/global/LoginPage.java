package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.accounts.AccountVerificationPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * 
 * @author nmanasa
 *
 */
public class LoginPage extends CommonPage {

	@FindBy(css = "input[id='username']")
	private WebElement loginUsername;

	@FindBy(css = "input[id='password']")
	private WebElement loginPassword;

	@FindBy(css = "input[value='LOG IN']")
	private WebElement loginButon;

	@FindAll({ @FindBy(id = "username"), @FindBy(name = "username"), @FindBy(css = "input.input_box") })
	private WebElement emailOrPhone;

	@FindAll({ @FindBy(css = "input[autocomplete='new-password']"), @FindBy(id = "password"),
			@FindBy(name = "password") })
	private WebElement password;

	@FindBy(css = "[class*='text_box input_box']")
	private WebElement iosEmailOrPhone;

	@FindBy(css = "[class*='text_box ng-untouched']")
	private WebElement iosPassword;

	@FindAll({ @FindBy(css = ".btn.btn-primary.login-btn.pt50"), @FindBy(id = "primary_button_930"),
			@FindBy(css = ".btn.til-btn.ripple") })
	private WebElement loginButton;

	@FindBy(css = ".btn.btn-secondary.signup-btn.mt30.ng-binding")
	private WebElement signUpBtn;

	@FindBy(css = "[ng-show='streamLineErrorFlag']~div .ng-scope.ng-binding")
	private WebElement incorrectLoginText;

	@FindBy(css = "div.ui_headline.ng-binding")
	private List<WebElement> captchaModalHeaderText;

	@FindBy(name = "signinform")
	private WebElement loginForm;

	@FindBy(css = ".maintenance-error > h1")
	private WebElement maintenanceErrorMsg;

	@FindBy(css = ".container-inner .main-inner .error-headline")
	private WebElement connectionFailedError;

	@FindBy(css = ".ui_secondary_link .hidden-mob.ng-binding")
	private WebElement forgotPassword;

	@FindBy(css = ".ui_secondary_link .hiiden-desk.ng-binding")
	private WebElement deviceForgotPassword;

	@FindBy(css = "div[on='error_count'] div#errorShow div.row div[role='alert'] div.error_alignment span")
	private WebElement notRegisteredUser;

	@FindBy(css = "a[class*='signup-btn']")
	private WebElement signUp;

	@FindBy(css = "div#errorShow div.row div[role='alert'] div.error_alignment a.bold.ng-scope b")
	private WebElement resetPasswordNow;

	@FindBy(css = "div.login-softlock-error div#errorShow div.error_alignment a.bold.ng-scope b.ng-binding")
	private WebElement accountwaitFor24OrResetPassword;

	@FindBy(css = "div[ng-show$='&& !main.softLockedEnabled']:not(.ng-hide) #errorShow > div > div > div > span+a > b")
	private WebElement notSoftLockedEnabled;

	@FindBy(css = "div[ng-show$='&& main.softLockedEnabled']:not(.ng-hide) #errorShow > div > div > div > span+a > b")
	private WebElement softLockedEnabled;

	@FindBy(css = ".ui_headline.ng-binding")
	private WebElement captchaHeaderText;

	@FindBy(css = "input[name='captcha']")
	private WebElement captchaTxtBox;

	@FindBy(id = "captchaButton")
	private WebElement captchaValidateButton;

	@FindBy(id = "captchaImgId")
	private WebElement captchaImg;

	@FindBy(xpath = "//button[contains(text(), 'Skip')]")
	private List<WebElement> pswdSkip;

	@FindAll({ @FindBy(xpath = "//div[contains(@id,'loginSpa')]/descendant::button") })
	private WebElement nextButton;

	@FindBy(id = "confirmcode")
	private WebElement confCode;

	@FindBy(css = "button[ng-disabled*='methodSelectionPrompt']")
	private WebElement textMsgNextBtn;

	@FindBy(css = "button[ng-disabled*='msisdnSelectionPrompt']")
	private WebElement phoneNumNextBtn;

	@FindBy(css = "button[ng-click*='smsConfirmationController.verifyPin']")
	private WebElement confirmationCodeNextBtn;

	@FindBy(xpath = "//div[text()='Account verification']")
	private List<WebElement> accountVerificationText;

	@FindBy(id = "secondary_button")
	private WebElement AskMeLater;

	@FindBy(xpath = "//div[contains(text(),'Set up security questions')]")
	private List<WebElement> securityQuestions;

	@FindBy(id = "secondary_button")
	private WebElement askMeLaterBtn;

	@FindBy(css = "div[class*='customModalClose']")
	private WebElement closeButton;

	@FindBy(id = "banner_cross1")
	private WebElement closeButton1;

	@FindBy(css = "div[class='tnt18091CloseX']")
	private WebElement closeButton2;

	@FindBy(xpath = "//div/span[contains(text(),'The login information you provided is incorrect. Please try again')]")
	private List<WebElement> incorrectpassword;

	@FindBy(xpath = "//div/p[contains(text(),'Your password is getting old!')]")
	private WebElement passwordOld;

	@FindBy(xpath = "//div/span[contains(text(),'It looks like you haven’t registered for a ')]")
	private List<WebElement> notRegistered;

	@FindBy(css = ".co_phone-verification-alert#errorShow .error_alignment span")
	private WebElement temporaryLocked;

	@FindBy(css = "div[ng-switch]>div[class*='co_phone-verification-alert ui_body container-curve'] div[class='error_alignment']>span[class*='ng-scope ng-binding']")
	private WebElement invalidPasswordMessage;

	@FindBy(css = "img[class*='tmo-icon-size text-center']")
	private WebElement Tmobilelogo;

	@FindBy(css = "form[name='chooseMethodPrompt'] div.co_reset-password")
	private WebElement secondFactorAuthencationForm;

	@FindBy(css = "input[value='sms']")
	private List<WebElement> textMessageRadioButton;

	@FindBy(css = "input[value='security_question']")
	private WebElement securityQuestionsRadioButton;

	@FindBy(xpath = "//button[contains(text(), 'Next')]")
	private WebElement secondFactornextButton;

	@FindBy(css = "input#confirmcode")
	private WebElement confirmationCode;

	@FindBy(css = "a[class='btn btn-secondary signup-btn mt30 ng-binding']")
	private WebElement signup;

	@FindBy(css = "button[class='btn button-bg is-valid']")
	private WebElement signup1;

	@FindBy(css = "button[type='submit']")
	private WebElement submit;

	@FindBy(id = "msgs")
	private WebElement unlockSuccess;

	@FindBy(css = "span[class='alert-message']")
	private WebElement alertmessage;

	@FindBy(css = "div[class='TMO-LOADER-SPINNER']")
	private WebElement registrationpagespinner;

	@FindBy(css = "input[id='confirmtext']")
	private WebElement enterConfirmationpage;

	@FindBy(css = "select[ng-options='item as item.text for item in MissingSecurityQuestionsCtrl.securityQuestionsList[0] track by item.sqId']")
	private WebElement firstsecurityQuestion;

	@FindBy(css = "select[ng-options='item as item.text for item in MissingSecurityQuestionsCtrl.securityQuestionsList[1] track by item.sqId']")
	private WebElement secondsecurityQuestion;

	@FindBy(css = "select[ng-options='item as item.text for item in MissingSecurityQuestionsCtrl.securityQuestionsList[2] track by item.sqId']")
	private WebElement thirdsecurityQuestion;

	@FindBy(css = "input[id='answer0']")
	private WebElement firstAnswer;

	@FindBy(css = "input[id='answer1']")
	private WebElement secondAnswer;

	@FindBy(css = "input[id='answer2']")
	private WebElement thirdAnswer;

	@FindBy(css = "input[class='btn btn-primary btn-submit button-padding']")
	private WebElement next;

	@FindBy(css = "input[class='btn btn-primary btn-submit button-padding']")
	private WebElement next1;

	@FindBy(css = "div[ng-if='portPinController.isSetPin']")
	private WebElement setpin;

	@FindBy(css = "div[class='error_alignment']")
	private WebElement securityquestions;

	@FindBy(css = "input[id='ssn_validation_id']")
	private WebElement setPah;

	@FindBy(css = "input[id='pintext']")
	private WebElement enterpin;

	@FindBy(id = "primary_button_signup")
	private WebElement nextpin;

	@FindBy(xpath = "/html/body")
	private WebElement getpin;

	@FindBy(css = "div[class='alert alert-success']")
	private WebElement getpin1;

	@FindBy(css = "//div/p[contains(text(),'Sign up for your T-Mobile ID.')]")
	private WebElement signuptitle;

	@FindBy(id = "firstName")
	private WebElement firstname;

	@FindBy(id = "lastName")
	private WebElement lastname;

	@FindBy(id = "PhoneNumber")
	private WebElement phoneNumber;

	@FindBy(css = "input[id='Email']")
	private WebElement Email;

	@FindBy(css = "input[id='password']")
	private WebElement Password;

	@FindBy(css = "span")
	private WebElement somethingNotRightMessage;

	@FindBy(xpath = "//button[contains(.,'Next')]")
	private WebElement nextBtn;

	@FindBy(css = "button[ng-click*='chooseSecondFactorMethodsController']")
	private WebElement continueButton;

	@FindBy(css = "input[id='que_0_field']")
	private WebElement securityAnswerOne;

	@FindBy(css = "input[id='que_1_field']")
	private WebElement securityAnswerTwo;

	@FindBy(css = "Button[class*='btn btn-primary continue']")
	private WebElement securityAnswersContinueButton;

	@FindBy(css = "input[value='security_question']")
	private List<WebElement> securityQuestionsRadioBtn;

	@FindBy(css = "div[ng-repeat*='secondFactorSecurityQController.securityQuestions']")
	private List<WebElement> securityQuestionList;

	@FindBy(css = ".moreOptions>b")
	private List<WebElement> moreOptions;

	@FindBy(xpath = "//*[contains(@class,'moreOptions')]")
	private WebElement moreOption;

	@FindBy(css = "input[id='que_2_field']")
	private List<WebElement> securityAnswerThree;

	/**
	 * 
	 * @param webDriver
	 */
	public LoginPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Login Page
	 * 
	 * @return
	 */
	public boolean verifyLoginPage() {
		checkPageIsReady();
		return getDriver().getCurrentUrl().contains("signin");
	}

	/**
	 * Re enter EmailOrPhone
	 */
	private LoginPage enterEmailOrPhone(String value) {
		try {
			if (getDriver() instanceof IOSDriver) {
				sendTextData(iosEmailOrPhone, value);
			} else {
				sendTextData(emailOrPhone, value);
				waitForSpinner();
			}
		} catch (Exception e) {
			Assert.fail("Entering email or phone value was unsuccessful.");
		}
		return this;
	}

	/**
	 * Re enter Password
	 */
	private LoginPage enterPassword(String value) {
		checkPageIsReady();
		waitForSpinner();
		try {
			if (getDriver() instanceof IOSDriver) {
				sendTextData(iosPassword, value);
			} else {
				waitFor(ExpectedConditions.visibilityOf(password), 5);
				sendTextData(password, value);
				waitForSpinner();
			}
		} catch (Exception e) {
			Assert.fail("Entering password value was unsuccessful.");
		}
		return this;
	}

	/**
	 * Click Submit Button
	 */
	public LoginPage clickSubmitButton() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(loginButton), 5);
			loginButton.click();
		} catch (Exception e) {
			Assert.fail("Unable to click on Login button");
		}
		return this;
	}

	/**
	 * Click SignUp Button
	 */
	public LoginPage clickSignUpButton() {
		clickElementWithJavaScript(signUpBtn);
		return this;
	}

	/**
	 * This method is to enter user name and password and click submit
	 * 
	 * @param username
	 * @param password
	 */
	public LoginPage performLoginAction(String username, String password) {
		checkPageIsReady();
		try {
			enterEmailOrPhone(username);
			try {
				if (isElementDisplayed(nextBtn)) {
					nextBtn.click();
				} else {
					Reporter.log("Old login page is displayed");
				}
			} catch (Exception e) {
				Reporter.log("Exception occurred during login action");
			}
			enterPassword(password);
			clickSubmitButton();
			enterChooseSecondFactor(username);
		} catch (Exception e) {
			Assert.fail("Unable to login with " + username + "/" + password + "");
		}
		return this;
	}

	/**
	 * Enter Choose Second Factor
	 */
	public void enterChooseSecondFactor(String msisdn) {
		waitforSpinner();
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(securityAnswersContinueButton));
		try {
			if (CollectionUtils.isNotEmpty(securityQuestionsRadioBtn)) {
				
				securityQuestionsRadioBtn.get(0).click();
				waitFor(ExpectedConditions.visibilityOf(continueButton), 5);
				continueButton.click();
				waitFor(ExpectedConditions.visibilityOf(securityAnswerOne), 5);
				securityAnswerOne.sendKeys("test");
				securityAnswerTwo.sendKeys("test");
				if (!securityAnswerThree.isEmpty()) {
					securityAnswerThree.get(0).sendKeys("test");
				}
			} else{
				continueButton.click();
				waitForSpinner();
				waitFor(ExpectedConditions.visibilityOf(confirmationCode));
				confirmationCode.click();

				String acctVrfnPin = getTextMessagePin(msisdn);
				confirmationCode.sendKeys(acctVrfnPin);
			}
			waitFor(ExpectedConditions.visibilityOf(securityAnswersContinueButton), 5);
			securityAnswersContinueButton.click();

		} catch (Exception e) {
			Reporter.log("Failed to set security questions");
		}
	}

	
	
	/**
	 * Enter Choose Second Factor
	 */
	public void enterChooseSecondFactorTextMessage(String msisdn) {
		waitforSpinner();
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(securityAnswersContinueButton), 10);
		try {
				continueButton.click();
				waitForSpinner();
				waitFor(ExpectedConditions.visibilityOf(confirmationCode));
				confirmationCode.click();

				String acctVrfnPin = getTextMessagePin(msisdn);
				confirmationCode.sendKeys(acctVrfnPin);
			
			waitFor(ExpectedConditions.visibilityOf(securityAnswersContinueButton), 5);
			securityAnswersContinueButton.click();

		} catch (Exception e) {
			Reporter.log("Failed to set security questions");
		}
	}
	
	
	/**
	 * This method is to verify account verification
	 */
	public LoginPage accountVerification() {
		checkPageIsReady();
		waitforSpinner();

		try {
			AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
			if (accountVerificationPage.verifyPageUrl()) {
				accountVerificationPage.verifyAccountVerificationPage();
				accountVerificationPage.clickOnNextBtn();
				accountVerificationPage.clickOnSecurityQstnsNextBtn();
			}
		} catch (Exception e) {
			Assert.fail("Account verification not successfull ");
		}
		return this;
	}

	/**
	 * Get Incorrect Login Text
	 * 
	 * @return
	 */
	public LoginPage getIncorrectLoginText() {
		this.incorrectLoginText.getText().trim();
		return this;
	}

	/**
	 * Get Modal Captcha
	 * 
	 * @return
	 */
	public boolean getModalCaptchaHeader() {

		boolean verifyCapcha = false;
		if (CollectionUtils.isNotEmpty(captchaModalHeaderText) && !CollectionUtils.sizeIsEmpty(captchaHeaderText)) {
			verifyCapcha = true;
		}
		return verifyCapcha;
	}

	/**
	 * Is User Locked
	 * 
	 * @return
	 */
	public boolean isUserLocked() {
		boolean isUserLocked = false;
		if (incorrectLoginText != null && incorrectLoginText.isDisplayed()) {
			isUserLocked = true;
		}
		return isUserLocked;
	}

	/**
	 * Is Service Unavailable
	 * 
	 * @return
	 */
	public boolean isServiceUnavailable() {
		return this.maintenanceErrorMsg.isDisplayed();
	}

	/**
	 * Is Connection Failed
	 * 
	 * @return
	 */
	public boolean isConnectionFailed() {
		return this.connectionFailedError.isDisplayed();
	}

	/**
	 * This method clicks the forgot password
	 */
	public LoginPage clickForgotPassword() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				deviceForgotPassword.click();
			} else
				this.forgotPassword.click();
			Reporter.log("Forgot Password click link successful.");
		} catch (NoSuchElementException e) {
			Assert.fail("Forgot Password click link was not successful.");
		}
		return this;
	}

	/**
	 * Verify Not Registered user
	 * 
	 * @return
	 */
	public LoginPage verifyNotRegisteredUser() {
		Assert.assertTrue(this.notRegisteredUser.isDisplayed());
		return this;
	}

	/**
	 * Click Sign Up Link
	 */
	public LoginPage clickSignUp() {
		this.signUp.click();
		return this;
	}

	/**
	 * Verify Not Registered user
	 * 
	 * @return
	 */
	public boolean isRsetNewPasswordDisplayed() {
		return this.resetPasswordNow.isDisplayed();
	}

	/**
	 * click reset password Link
	 */
	public void clickResetPasswordLink() {
		this.resetPasswordNow.click();
	}

	/**
	 * click reset password Link
	 */
	public void clickAccountWaitOrForgotPassLink() {
		this.accountwaitFor24OrResetPassword.click();
	}

	/**
	 * verify is soft locked enabled
	 * 
	 * @return boolean
	 */
	public boolean issoftLockedEnabled() {
		boolean enabledSoftLock = false;
		try {
			if (softLockedEnabled != null && softLockedEnabled.isDisplayed()
					&& !softLockedEnabled.getText().isEmpty()) {
				enabledSoftLock = true;
			}
		} catch (Exception ex) {
			logger.error("softLockedEnabled not enabled{}", ex.getMessage());
			throw new FrameworkException("softLockedEnabled not enabled{}", ex);
		}
		return enabledSoftLock;
	}

	/**
	 * verify is not soft lock enabled
	 * 
	 * @return boolean
	 */
	public boolean isNotsoftLockedEnabled() {
		boolean enabledSoftLock;
		try {
			enabledSoftLock = false;
			if (notSoftLockedEnabled != null && notSoftLockedEnabled.isDisplayed()
					&& !notSoftLockedEnabled.getText().isEmpty()) {
				enabledSoftLock = true;
			}
		} catch (Exception ex) {
			logger.error("notSoftLockedEnabled not enabled{}", ex.getMessage());
			throw new FrameworkException("notSoftLockedEnabled not enabled{}", ex);
		}
		return enabledSoftLock;
	}

	/**
	 * verify if captcha is displayed
	 * 
	 * @return boolean
	 */
	public boolean isCaptchaDisplayed() {
		boolean captchaHeader = false;
		try {
			if (captchaHeaderText != null && captchaHeaderText.isDisplayed()
					&& !captchaHeaderText.getText().isEmpty()) {
				captchaHeader = true;
			}
		} catch (Exception ex) {
			logger.error("captchaHeaderText not enabled{}", ex.getMessage());
			throw new FrameworkException("captchaHeaderText not enabled{}", ex);
		}
		return captchaHeader;
	}

	/**
	 * get Captcha Img Src
	 * 
	 * @return value
	 */
	public String getCaptchaImgSrc() {
		waitFor(ExpectedConditions.visibilityOf(this.captchaImg));
		return this.captchaImg.getAttribute("src");
	}

	/**
	 * enter captcha
	 * 
	 * @param captchaText
	 */
	public void enterCaptcha(String captchaText) {
		sendTextData(this.captchaTxtBox, captchaText);
	}

	/**
	 * click validate button of captcha
	 */
	public void clickCaptchaValidate() {
		this.captchaHeaderText.click();
		waitFor(ExpectedConditions.visibilityOf(this.captchaValidateButton));
		this.captchaValidateButton.click();
	}

	public void clickNextButtonInAcctVrfn() {
		try {
			Thread.sleep(2000);
		} catch (Exception ex) {
			logger.error("caught exception while loading Next button", ex.getMessage());
		}
		nextButton.click();
	}

	public LoginPage selectMsisdnToVerify(String loginEmailOrPhone) {
		// IF the select msisdn screen is displayed, select msisdn and click
		// Next. Else click on Next.
		try {
			checkPageIsReady();
			WebElement selectMsisdn = getDriver().findElement(
					By.xpath("//input[contains(@id,'device') and contains(@value,'" + loginEmailOrPhone + "')]"));
			Reporter.log("Multi line msisdn.");
			selectMsisdn.click();
			nextButton.click();
		} catch (WebDriverException noSuchElementException) {
			Reporter.log("Single line msisdn.");
			logger.info("Single line msisdn.", noSuchElementException.getMessage());
		} catch (Exception ex) {
			logger.error("caught exception while loading Next button", ex.getMessage());
		}
		return this;
	}

	public LoginPage enterConfirmationCode(String acctVrfnPin) {
		try {
			checkPageIsReady();
			sendTextData(confCode, acctVrfnPin);
		} catch (Exception ex) {
			logger.error("caught exception while loading Next button", ex.getMessage());
		}
		return this;
	}

	public LoginPage clickOnTextMsgNextBtn() {
		waitFor(ExpectedConditions.elementToBeClickable(textMsgNextBtn));
		textMsgNextBtn.click();
		return this;
	}

	public LoginPage clickOnPhoneNumNextBtn() {
		waitFor(ExpectedConditions.elementToBeClickable(phoneNumNextBtn));
		phoneNumNextBtn.click();
		return this;
	}

	public LoginPage clickOnConfirmationCodeNextBtn() {
		waitFor(ExpectedConditions.elementToBeClickable(confirmationCodeNextBtn));
		confirmationCodeNextBtn.click();
		return this;
	}

	public LoginPage clickOnAskMeLaterButton() {
		waitFor(ExpectedConditions.elementToBeClickable(AskMeLater));
		AskMeLater.click();
		return this;
	}

	public LoginPage verifyAccountVerification() {
		Assert.assertTrue(((WebElement) accountVerificationText).isDisplayed());
		return this;
	}

	public LoginPage skipMobileAppPopUp() {
		try {
			if (closeButton.isDisplayed()) {
				closeButton.click();
			}
		} catch (Exception e) {
		}
		return this;
	}

	public LoginPage skipMobileAppPopUp1() {
		try {
			if (closeButton1.isDisplayed()) {
				closeButton1.click();
			}
		} catch (Exception e) {
		}
		return this;
	}

	public LoginPage skipallShotcuticonsPopUp() {
		try {
			if (closeButton2.isDisplayed()) {
				closeButton2.click();
			}
		} catch (Exception e) {
		}
		return this;
	}

	public boolean passwordOldorInCorrectPassword(MyTmoData myTmoData, LoginPage loginPage) {
		boolean a = false;
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img[ng-show='spinnerStatus']")));
			if (temporaryLocked.isDisplayed() || incorrectpassword.get(1).isDisplayed() || passwordOld.isDisplayed()
					|| notRegistered.get(1).isDisplayed()) {
				a = true;
			}
		} catch (Exception e) {
		}
		return a;
	}

	public LoginPage iphoneXBanner() {
		try {
			if (Tmobilelogo.isDisplayed()) {
				Tmobilelogo.click();
			}
		} catch (Exception e) {
		}
		return this;

	}

	public LoginPage verifyLoginErrorMessage() {
		try {
			Assert.assertTrue(isElementDisplayed(invalidPasswordMessage), "Invalid password message not displayed");
			Reporter.log("Invalid password message displayed");
		} catch (Exception e) {
			Reporter.log("Invalid password message not displayed");
		}
		return this;
	}

	public LoginPage waitForSpinner() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader")));
		return this;
	}

	public void registerMsisdn(MyTmoData myTmoData) throws InterruptedException {
		Thread.sleep(1000);
		signup.click();
		checkPageIsReady();
		// signuptitle.isDisplayed();
		firstname.sendKeys("Automation");
		lastname.sendKeys("Test");
		phoneNumber.sendKeys(myTmoData.getLoginEmailOrPhone());
		Email.sendKeys(myTmoData.getLoginEmailOrPhone() + "@gmail.com");
		Password.sendKeys("Auto12345");
		signup1.click();
		Thread.sleep(2000);
		// waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div[class='TMO-LOADER-SPINNER']")));
		checkPageIsReady();
		// try {
		// if (alertmessage.isDisplayed()) {
		// Email.clear();
		// Email.sendKeys(myTmoData.getLoginEmailOrPhone() + "1243" +
		// "@gmail.com");
		// Password.sendKeys("Auto12345");
		// signup1.click();
		// }
		// } catch (Exception e) {
		// }
		// waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div[class='TMO-LOADER-SPINNER']")));

		Thread.sleep(2000);
		getDriver().get("http://10.92.28.199:10000/msisdn/" + myTmoData.getLoginEmailOrPhone() + "/pincode");
		String pin = getpin.getText();
		getDriver().navigate().back();

		enterConfirmationpage.sendKeys(pin);
		next1.click();
		checkPageIsReady();
		try {
			if (setpin.isDisplayed()) {

				enterpin.sendKeys("007007");
				nextpin.click();
			}
		} catch (Exception e) {
		}

		checkPageIsReady();
		Select select = new Select(firstsecurityQuestion);
		select.selectByIndex(2);
		firstAnswer.sendKeys("Test");

		Select select2 = new Select(secondsecurityQuestion);
		select2.selectByIndex(2);
		secondAnswer.sendKeys("Test");

		Select select3 = new Select(thirdsecurityQuestion);
		select3.selectByIndex(2);
		thirdAnswer.sendKeys("Test");

		next.click();
	}

	public LoginPage verifysetpin() {
		checkPageIsReady();
		if (setpin.isDisplayed()) {

			enterpin.sendKeys("007007");
			nextpin.click();
		}
		return this;
	}

	public LoginPage setQuestions() {
		checkPageIsReady();
		try {
			if (securityquestions.isDisplayed()) {

				Select select = new Select(firstsecurityQuestion);
				select.selectByIndex(2);
				firstAnswer.sendKeys("Test");

				Select select2 = new Select(secondsecurityQuestion);
				select2.selectByIndex(2);
				secondAnswer.sendKeys("Test");

				Select select3 = new Select(thirdsecurityQuestion);
				select3.selectByIndex(2);
				thirdAnswer.sendKeys("Test");

				next.click();
			}
		} catch (Exception e) {
		}
		return this;
	}

	public LoginPage setPAH() {
		checkPageIsReady();
		if (setPah.isDisplayed()) {

			// call service to set PAH
		}
		return this;
	}

	public void registerMsisdnqlab03(MyTmoData myTmoData) throws InterruptedException {
		Thread.sleep(1000);
		signup.click();
		checkPageIsReady();
		// signuptitle.isDisplayed();
		firstname.sendKeys("Automation");
		lastname.sendKeys("Test");
		phoneNumber.sendKeys(myTmoData.getLoginEmailOrPhone());
		Email.sendKeys(myTmoData.getLoginEmailOrPhone() + "@gmail.com");
		Password.sendKeys("Auto12345");
		signup1.click();
		Thread.sleep(1000);
		checkPageIsReady();
		getDriver().get("http://10.92.28.199:9002/action?lab=lab11&sub=" + myTmoData.getLoginEmailOrPhone()
				+ "&action=display_pin");
		Thread.sleep(3000);
		submit.click();
		String pin = getpin1.getText();
		getDriver().navigate().back();
		Thread.sleep(2000);
		enterConfirmationpage.sendKeys(pin);
		next1.click();
		Thread.sleep(2000);

	}

	public void unlinkMsisdn(MyTmoData myTmoData) throws InterruptedException {

		getDriver().get("http://10.92.28.199:9002/action?lab=lab11&sub=" + myTmoData.getLoginEmailOrPhone()
				+ "&action=unlink_msisdn");
		Thread.sleep(2000);
		submit.click();

	}

	public void unlockMsisdnQLAB03(ApiTestData apiTestData) throws InterruptedException {

		getDriver().get("http://10.92.28.199:9002/action?lab=lab11&sub=" + apiTestData.getMsisdn() + "&action=unlock");
		Thread.sleep(2000);
		submit.click();
		waitFor(ExpectedConditions.visibilityOf(unlockSuccess), 10);
	}

	public void unlockMsisdnQLAB02(ApiTestData apiTestData) throws InterruptedException {

		getDriver().get("http://10.92.28.199:9002/action?lab=lab14&sub=" + apiTestData.getMsisdn() + "&action=unlock");
		Thread.sleep(2000);
		submit.click();
		waitFor(ExpectedConditions.visibilityOf(unlockSuccess), 10);

	}

	public void unlockMsisdnQLAB01(ApiTestData apiTestData) throws InterruptedException {

		getDriver().get("http://10.92.28.199:9002/action?lab=lab3&sub=" + apiTestData.getMsisdn() + "&action=unlock");
		Thread.sleep(2000);
		submit.click();
		waitFor(ExpectedConditions.visibilityOf(unlockSuccess), 10);

	}

	/**
	 * Verify 'Something not right' error message is present
	 */
	public boolean verifySomethingNotRightMessageIsPresent() {
		Boolean errorMessage = false;
		try {
			checkPageIsReady();
			if (somethingNotRightMessage.isDisplayed()) {
				errorMessage = true;
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Something not right' error message");
		}
		return errorMessage;
	}

	/**
	 * Verify login Page loaded
	 * 
	 * @return
	 */
	public LoginPage verifyLoginPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("account"));
			Reporter.log("Login page is displayed");
		} catch (AssertionError e) {
			Reporter.log("Login page not displayed");
			Assert.fail("Login page not displayed");
		}
		return this;
	}

	/**
	 * This method is to enter user name and password and click submit for cookied
	 * customers
	 * 
	 * @param username
	 * @param password
	 */
	public LoginPage performLoginActionForCookiedCustomers(String username, String password) {
		checkPageIsReady();
		try {
			enterEmailOrPhone(username);
			nextBtn.click();
			enterPassword(password);
			clickSubmitButton();
		} catch (Exception e) {
			Assert.fail("Unable to login with " + username + "/" + password + " for cookied customers");
		}
		return this;
	}

}