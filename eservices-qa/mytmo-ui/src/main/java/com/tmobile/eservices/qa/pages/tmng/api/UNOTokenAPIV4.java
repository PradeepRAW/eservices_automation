package com.tmobile.eservices.qa.pages.tmng.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class UNOTokenAPIV4 extends ApiCommonLib {

	/***
	 * This is the API will give decoded access JWT Token
	 * 
	 * @return
	 * @throws Exception
	 */

	public Response UNOToken(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildUNOTokenHeader(apiTestData);
		/*
		 * RestService service = new RestService(requestBody,
		 * "https://tmobileqat-qat03-pro.apigee.net"); RequestSpecBuilder reqSpec =
		 * service.getRequestbuilder(); reqSpec.addHeaders(headers);
		 * service.setRequestSpecBuilder(reqSpec); Response response =
		 * service.callService("/oauth2/v4/tokens", RestCallType.POST);
		 */
		String resourceURL = "/oauth2/v4/tokens";
		Response response = invokeRestServiceCall(headers, "https://tmobileqat-qat03-pro.apigee.net", resourceURL, "",
				RestServiceCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildUNOTokenHeader(ApiTestData apiTestData) throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-type", "application/json");
		headers.put("authorization", "Basic RjRGMEVXVEtXeXgwRUhicFpuNTA2ZDBIeTBaRDFUWk46SWU5TjBOT0cxdlNsc3gwcQ==");
		headers.put("Cache-Control", "no-cache");
		headers.put("Grant-type", "client_credentials");

		return headers;
	}

}
