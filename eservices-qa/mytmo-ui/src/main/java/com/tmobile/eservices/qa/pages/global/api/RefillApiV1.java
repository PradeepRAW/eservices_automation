package com.tmobile.eservices.qa.pages.global.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class RefillApiV1 extends ApiCommonLib{
    
    public Response redeemPrepaidCoupon(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildRefillAPIHeader(apiTestData, tokenMap);
    	String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "Refill-redeemPrepaidCoupon");
    	String resourceURL = "v1/refill/redeem";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, updatedRequest ,RestServiceCallType.PUT);
		return response;
    }
    
    private Map<String, String> buildRefillAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();				
		headers.put("Accept","application/json");
		headers.put("Authorization","test");
		headers.put("Content-Type","application/json");
		headers.put("Postman-Token","ec545808-31a7-4d77-8e09-f1f2ab02c245");
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("access_token","01.USR.LtsOhOmEQl267eR1P");
		headers.put("application_id","MYTMO");
		headers.put("cache-control","no-cache");
		headers.put("channel_id","Desktop");
		headers.put("msisdn",apiTestData.getMsisdn());
		
		/*
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("channel_id", "DESKTOP");
		headers.put("Authorization", checkAndGetJWTToken(apiTestData, tokenMap).get("jwtToken"));
		headers.put("application_id", "MYTMO");
		headers.put("user-token", "user-token");
		headers.put("tmo-id", "MYTMO");
		headers.put("msisdn", tokenMap.get("msisdn"));
		headers.put("ban", apiTestData.getBan());
		headers.put("usn", "TestUSN");
		headers.put("application_client", "{{application_client}}");
		headers.put("application_version_code", "{{application_version_code}}");
		headers.put("device_os", "{{device_os}}");
		headers.put("os_version", "{{os_version}}");
		headers.put("os_language", "{{os_language}}");
		headers.put("device_manufacturer", "{{device_manufacturer}}");
		headers.put("device_model", "{{device_model}}");
		headers.put("marketing_cloud_id", "{{marketing_cloud_id}}");
		headers.put("sd_id", "{{sd_id}}");
		headers.put("adobe_uuid", "{{adobe_uuid}}");
		headers.put("access_token", checkAndGetJWTToken(apiTestData, tokenMap).get("accessToken"));
		headers.put("Cache-Control", "no-chache");
		//headers.put("dealerCode", "000002");*/
		return headers;
	}
}
