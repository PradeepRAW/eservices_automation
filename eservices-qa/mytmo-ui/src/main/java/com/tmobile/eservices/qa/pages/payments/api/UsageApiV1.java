package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class UsageApiV1 extends ApiCommonLib {

	/***
	 * Summary: Retreive the usage details
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getUsageSummay_datastash(ApiTestData apiTestData,String requestBody) throws Exception {
		Map<String, String> headers = buildUsageSummaryHeader(apiTestData);

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/usage/datastash", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}
	
	/***
	 * Summary: Retreive the usage details
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getUsageSummay(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildUsageSummaryHeader(apiTestData);

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/usage/summary", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	/***
	 * Summary: Retreive the usage details
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getUsageDetails(ApiTestData apiTestData,String statementId,String documentId) throws Exception {
		Map<String, String> headers = buildUsageSummaryHeader(apiTestData);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		String urlParams=null;
		if(statementId!=null){urlParams="/statementId="+statementId;}
		if(documentId!=null){urlParams="/documentId="+documentId;}
		
		Response response = service.callService("v1/usage/details?"+urlParams, RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	/*private Map<String, String> buildUsageHeader(ApiTestData apiTestData) throws Exception {
		clearCounters(apiTestData);
		final String dcpToken = createPlattoken(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", dcpToken);
		headers.put("Content-Type","application/json");
		headers.put("Postman-Token","4c07735c-4875-4232-83f2-a1437c47234d");
		headers.put("applicationId","MyTMO");
		headers.put("cache-control","no-cache");
		headers.put("channelId","WEB");
		headers.put("clientId","ESERVICE");
		headers.put("correlationId","eos-test-qlab03-4049662979");
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionBusinessKey","BAN");
		headers.put("transactionBusinesskeyType",apiTestData.getBan());
		headers.put("transactionId","eos-test-qlab03-4049662979");
		headers.put("transactionType","UPGRADE");
		headers.put("usn","sampleUsn12");
		return headers;
	}*/
	
	private Map<String, String> buildUsageSummaryHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		//final String dcpToken = createPlattoken(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer "+checkAndGetAccessToken());
		headers.put("Content-Type","application/json");
		headers.put("userRole","PAH");
		headers.put("ban",apiTestData.getBan());
		headers.put("msisdn",apiTestData.getMsisdn());
		return headers;
	}

}


