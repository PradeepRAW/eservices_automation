package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class RoamingApiV1 extends ApiCommonLib {

	/***
	 * Summary: Micro services to getDeviceList
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getDeviceList(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildTeamOfExpertsHeader(apiTestData);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/roaming/list/devices", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}


	/***
	 * Summary: Micro services to getCountryList
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getCountryList(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildTeamOfExpertsHeader(apiTestData);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/roaming/list/countries", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	/***
	 * Summary: Micro services to getCruiseList
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getCruiseList(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildTeamOfExpertsHeader(apiTestData);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/roaming/list/cruises", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	/***
	 * Summary: Micro services to getRoamingDetailsWithCountryID
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getRoamingDetailsWithCountryID(ApiTestData apiTestData, String countryID) throws Exception {
		Map<String, String> headers = buildTeamOfExpertsHeader(apiTestData);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/roaming/details/"+countryID, RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}
	
	/***
	 * Summary: Micro services to getRoamingDetailsWithCruiseID
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getRoamingDetailsWithCruiseID(ApiTestData apiTestData, String cruiseID) throws Exception {
		Map<String, String> headers = buildTeamOfExpertsHeader(apiTestData);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/roaming/list/devices/"+cruiseID, RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}
	
	private Map<String, String> buildTeamOfExpertsHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept","application/json");
		headers.put("X-B3-TraceId","abc");
		headers.put("X-B3-SpanId","abc");
		headers.put("channel_id","Desktop");
		headers.put("application_id","MYTMO");
		return headers;
	}

}


