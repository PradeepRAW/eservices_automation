package com.tmobile.eservices.qa.api.eos.monitors;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

public class ShopAPIMonitorsHelperNew extends ApiCommonLib {

	public void standardUpgradeFRPFlow(ApiTestData apiTestData) {

		try {
			OrderProcess standardUpgradeFRPFlow = new StandardUpgradeFRPFlow();
			standardUpgradeFRPFlow.processStandardUpgradeOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}

	}

	public void standardUpgradeEIPFlow(ApiTestData apiTestData) {

		try {
			OrderProcess standardUpgradeEIPFlow = new StandardUpgradeEIPFlow();
			standardUpgradeEIPFlow.processStandardUpgradeOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

	public void standardUpgradeAddSocsEIPFlow(ApiTestData apiTestData) {

		try {
			OrderProcess standardUpgradeAddSocsEIPFlow = new StandardUpgradeAddSocsEIPFlow();
			standardUpgradeAddSocsEIPFlow.processStandardUpgradeOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

	public void standardUpgradeAddAccessoriesEIPFlow(ApiTestData apiTestData) {

		try {
			OrderProcess standardUpgradeAddAccessoriesEIPFlow = new StandardUpgradeAddAccessoriesEIPFlow();
			standardUpgradeAddAccessoriesEIPFlow.processStandardUpgradeOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

	public void standardUpgradeTradeInEIPFlow(ApiTestData apiTestData) {

		try {
			OrderProcess standardUpgradeTradeInEIPFlow = new StandardUpgradeTradeInEIPFlow();
			standardUpgradeTradeInEIPFlow.processStandardUpgradeOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

	public void aalPhoneOnEIPNoPDPSOC(ApiTestData apiTestData) {

		try {
			OrderProcess addALinePhoneOnEIPNoPDPSOC = new AddALinePhoneOnEIPNoPDPSOC();
			addALinePhoneOnEIPNoPDPSOC.processAddALineOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

	public void aal_FRP_PDP_NY_WithDeposit(ApiTestData apiTestData) {

		try {
			OrderProcess addALineFrpPdpNyWithDeposit = new AddALineFrpPdpNyWithDeposit();
			addALineFrpPdpNyWithDeposit.processAddALineOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

	public void aal_EIP_PDP_WithSecurityDeposit(ApiTestData apiTestData) {

		try {
			OrderProcess addALineEipPdpWithSecurityDeposit = new AddALineEipPdpWithSecurityDeposit();
			addALineEipPdpWithSecurityDeposit.processAddALineOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

	public void BYODFlow(ApiTestData apiTestData) {

		try {
			OrderProcess standardBYODFlow = new StandardBYODFlow();
			standardBYODFlow.processAddALineOrder(apiTestData);
		} catch (Exception e) {
			if (null != exceptionMsgs) {
				if (null != exceptionMsgs.get("errorMessage")) {
					failWithExcpetion(exceptionMsgs.get("errorMessage"));
				}
			}
		}
	}

}
