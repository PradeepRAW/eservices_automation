package com.tmobile.eservices.qa.api.unlockdata;

import javax.xml.soap.SOAPMessage;

import org.jdom2.Document;

/**
 * @author blakshminarayana
 *
 */
public interface SOAPService {

	SOAPMessage createRequest(Document document, String soapAction);

	SOAPMessage invokeService(String endpoint, SOAPMessage request);
}
