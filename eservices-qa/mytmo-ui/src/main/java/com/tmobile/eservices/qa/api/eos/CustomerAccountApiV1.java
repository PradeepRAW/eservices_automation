package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class CustomerAccountApiV1 extends ApiCommonLib {

	/***
	 * Summary: Micro services to getCustomerAccountDetails
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getCustomerAccountDetails(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildCustomerAccountHeader(apiTestData,tokenMap);
		String resourceURL = "v1/subscriber/account/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
	}

	/***
	 * Summary: Micro services to getSubscriberLines
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getSubscriberLines(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildCustomerAccountHeader(apiTestData,tokenMap);
		String resourceURL = "v1/subscriber/lines/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
    	return response;
	}

	/***
	 * Summary: Micro services to getSubscriberLines
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getSubscriberABC(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildCustomerAccountHeader(apiTestData,tokenMap);
		String resourceURL = "v1/subscriber/abc/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
	}
	
	/***
	 * Summary: Micro services to getSubscriberLines
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getPrepaidAccount(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildCustomerAccountHeader(apiTestData,tokenMap);
		String resourceURL = "v1/subscriber/prepaidaccount/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
	}
	
	/***
	 * Summary: Micro services to getCustomerAccountDetails
	 * 	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getCustomerDNSDetails(ApiTestData apiTestData,String requestBody,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildCustomerAccountHeader(apiTestData,tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "getEOSDNSSettings");
		String resourceURL = "v1/subscriber/dns";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, updatedRequest ,RestServiceCallType.POST);
		return response;
	}
	
	private Map<String, String> buildCustomerAccountHeader(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String,String> jwtTokens=checkAndGetJWTToken(apiTestData, tokenMap);
		Map<String, String> headers = new HashMap<String, String>();				
		headers.put("Accept","application/json");
		headers.put("Authorization",jwtTokens.get("jwtToken"));
		headers.put("Content-Type","application/json");
		headers.put("access_token",jwtTokens.get("accessToken"));
		headers.put("application_id","MYTMO");
		headers.put("ban",apiTestData.getBan());
		headers.put("cache-control","no-cache");
		headers.put("channel_id","Desktop");
		headers.put("msisdn",apiTestData.getMsisdn());
		headers.put("usn","dummyUSN");
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("transactionid","FlexAPITest");
		headers.put("msisdnList",apiTestData.getMsisdn());
		return headers;
	}
	
	public Map<String,String> getCustomerDNSDetailsResponseMap(ApiTestData apiTestData,Map<String,String> tokenMap)throws Exception {
		String requestBody = new ServiceTest().getRequestFromFile("getCustomerDNSSettings.txt");
		String operationName="CustomerAccountApi-getCustomerDNSDetails";
		Response rs = getCustomerDNSDetails(apiTestData,requestBody,tokenMap);
		Map<String,String> responseMap=new HashMap<String, String>();
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String localDnsValue= getPathVal(jsonNode, "localDnsValue");
				String globalDnsValue= getPathVal(jsonNode, "globalDnsValue");
				responseMap.put("localDnsValue", localDnsValue);
				responseMap.put("globalDnsValue", globalDnsValue);
				Reporter.log("response localDnsValue = "+localDnsValue+" - globalDnsValue ="+globalDnsValue);
				//Assert.assertNotEquals(globalDnsValue,"","Invalid globalDnsValue.");
				//Assert.assertNotEquals(localDnsValue,"","Invalid localDnsValue.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
		return responseMap;
	}

}


