package com.tmobile.eservices.qa.pages.payments.api;

import java.util.List;

import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;
import com.tmobile.eservices.qa.listeners.Verify;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSActivitiesV3 extends EOSCommonLib {

	public Response getResponseAccountsummary(String alist[], String fromdate, String todate) throws Exception {
		Response response = null;

		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("userRole", alist[4]);
			restService.addHeader("ban", alist[3]);
			restService.addHeader("Authorization", "Bearer " + alist[2]);
			restService.addHeader("access_token", "Bearer " + alist[1]);
			// LocalDate a = LocalDate.now();
			response = restService.callService(
					"v1/accountactivity/all?category=all&fromDate=" + fromdate + "&toDate=" + todate, RestCallType.GET);

		}

		return response;
	}

	public EOSActivitiesV3 checkjsontagitems(Response response, String tag) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get(tag) != null)
				Reporter.log(tag + " is existing");
			else
				Verify.fail(tag + " is not existing");
		}

		return this;
	}

	public EOSActivitiesV3 checkexpectedvalues(Response response, String tag, String expectedvalue) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get(tag) != null) {
				if (jsonPathEvaluator.get(tag).toString().equalsIgnoreCase(expectedvalue))
					Reporter.log(tag + " value is " + expectedvalue);
				else
					Verify.fail(tag + " value is not " + expectedvalue);
			}
		}

		return this;
	}

	public String checkstatuscodeAccountsummary(Response response) {
		String statuscode = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusCode") != null) {
				return jsonPathEvaluator.get("statusCode").toString();
			}

		}
		return statuscode;
	}

	public String checkstatusmessageAccountsummary(Response response) {
		String statuscode = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusMessage") != null) {
				return jsonPathEvaluator.get("statusMessage").toString();
			}

		}
		return statuscode;
	}

	public List<String> getalldocids(Response response) {
		List<String> docids = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("accountActivity.eventCategory") != null) {
				return jsonPathEvaluator.get("accountActivity.findAll{it.eventCategory=='Documents'}.downloadURL");
			}

		}
		return docids;
	}

	public List<String> getalleippaymentids(Response response) {
		List<String> eipids = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("accountActivity.eventType") != null) {
				return jsonPathEvaluator
						.get("accountActivity.findAll{it.eventType=='DevicePaymentPosted'}.confirmationNumber");
			}

		}
		return eipids;
	}

	public List<String> getalleippaymentrefundids(Response response) {
		List<String> eipids = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("accountActivity.eventType") != null) {
				return jsonPathEvaluator
						.get("accountActivity.findAll{it.eventType=='DevicePaymentRefund'}.confirmationNumber");
			}

		}
		return eipids;
	}

	/***************************************************************************************************************************************************
	 * 
	 */
	public Response getResponsegetpdfsummary(String alist[], String docid) throws Exception {
		Response response = null;

		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("ban", alist[3]);
			restService.addHeader("Authorization", "Bearer " + alist[2]);

			response = restService.callService("v1/accountactivity/document?documentId=" + docid, RestCallType.GET);

		}

		return response;
	}

	public String checkstatuscodepdfsummary(Response response) {
		String statuscode = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusCode") != null) {
				return jsonPathEvaluator.get("statusCode").toString();
			}

		}
		return statuscode;
	}

	/***************************************************************************************************************************************************
	 * 
	 */
	public Response getResponsegeteipHistory(String alist[], String docid) throws Exception {
		Response response = null;

		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("ban", alist[3]);
			restService.addHeader("Authorization", "Bearer " + alist[2]);

			response = restService.callService("v1/accountactivity/eip?paymentId=" + docid, RestCallType.GET);

		}

		return response;
	}

	public String checkstatuscodeEiphistory(Response response) {
		String statuscode = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusCode") != null) {
				return jsonPathEvaluator.get("statusCode").toString();
			}

		}
		return statuscode;
	}

}
