package com.tmobile.eservices.qa.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WebAnalyticsPage extends CommonPage{
	
	@FindBy(css = "head>script:nth-child(n)")
	public List<WebElement> pageAnalytics;
	
	@FindBy(xpath = ".//a[@href!='']")
	public List<WebElement> webAnalytics;
	
	

	public WebAnalyticsPage(WebDriver webDriver) {
		super(webDriver);
	}

}
