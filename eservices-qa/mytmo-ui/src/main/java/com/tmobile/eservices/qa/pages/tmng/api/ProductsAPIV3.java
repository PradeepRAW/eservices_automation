package com.tmobile.eservices.qa.pages.tmng.api;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
//import com.google.common.io.Files;
import com.tmobile.eservices.qa.api.RestServiceCallType;

import io.restassured.response.Response;

public class ProductsAPIV3 extends GetTMNGAccessTokens{

	public Response retrieveProductsUsingPOST(String url) {
		//setUpEnv();
		Response response = null;
		try {
			String requestBody = null;
			if (url.contains("cell-phone")) {
				requestBody = "{ \"productTypes\": [\"DEVICE\"], \"transactionType\": \"ACTIVATION\", \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"HANDSET\", \"SIMCARD\"] } ], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			} else if (url.contains("tablets")) {
				requestBody = "{ \"productTypes\": [\"DEVICE\"], \"transactionType\": \"ACTIVATION\", \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"TABLET\"] } ], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			} else if (url.contains("smart-watches")) {
				requestBody = "{ \"productTypes\": [\"DEVICE\"], \"transactionType\": \"ACTIVATION\", \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"WEARABLE\"] } ], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			} else if (url.contains("accessories")) {
				requestBody = "{ \"productTypes\": [\"ACCESSORY\"], \"transactionType\": \"ACTIVATION\", \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			}else {
				requestBody = "{ \"transactionType\": \"ACTIVATION\", \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			}
			Map<String, String> headers = buildProductsApiV3Header();
			String resourceURL = "commerce/v3/products";
			response = invokeRestServiceCall(headers, tmng_api_base_url, resourceURL, requestBody, RestServiceCallType.POST);
		} catch (Exception e) {
			Assert.fail("Failed to retirieve products using POST");
		}
//		System.out.println("Response: " +response.body().toString());
		return response;
	}

	public Response retrieveProductsUsingPOST() {
		//setUpEnv();
		Response response = null;
		try {
			String requestBody = "{ \"transactionType\": \"ACTIVATION\", \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			Map<String, String> headers = buildProductsApiV3Header();
			String resourceURL = "commerce/v3/products";
			response = invokeRestServiceCall(headers, tmng_api_base_url, resourceURL, requestBody, RestServiceCallType.POST);
		} catch (Exception e) {
			Assert.fail("Failed to retirieve products using POST");
		}
		return response;
	}

	public Response retrieveProductsUsingPOSTForPhones() {
		//setUpEnv();
		Response response = null;
		try {
			String requestBody = "{ \"productTypes\": [\"DEVICE\"], \"transactionType\": \"ACTIVATION\", \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"HANDSET\", \"SIMCARD\"] } ], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";;
			Map<String, String> headers = buildProductsApiV3Header();
			String resourceURL = "commerce/v3/products";
			response = invokeRestServiceCall(headers, tmng_api_base_url, resourceURL, requestBody, RestServiceCallType.POST);
		} catch (Exception e) {
			Assert.fail("Failed to retirieve products using POST");
		}
		return response;
	}

	public Response retrieveProductsUsingPOSTForTablets() {
		//setUpEnv();
		Response response = null;
		try {
			String requestBody = "{ \"productTypes\": [\"DEVICE\"], \"transactionType\": \"ACTIVATION\", \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"TABLET\"] } ], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			Map<String, String> headers = buildProductsApiV3Header();
			String resourceURL = "commerce/v3/products";
			response = invokeRestServiceCall(headers, tmng_api_base_url, resourceURL, requestBody, RestServiceCallType.POST);
		} catch (Exception e) {
			Assert.fail("Failed to retirieve products using POST");
		}
		return response;
	}

	public Response retrieveProductsUsingPOSTForSmartWatches() {
		//setUpEnv();
		Response response = null;
		try {
			String requestBody = "{ \"productTypes\": [\"DEVICE\"], \"transactionType\": \"ACTIVATION\", \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"WEARABLE\"] } ], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			Map<String, String> headers = buildProductsApiV3Header();
			String resourceURL = "commerce/v3/products";
			response = invokeRestServiceCall(headers, tmng_api_base_url, resourceURL, requestBody, RestServiceCallType.POST);
		} catch (Exception e) {
			Assert.fail("Failed to retirieve products using POST");
		}
		return response;
	}

	public Response retrieveProductsUsingPOSTForAccessories() {
		//setUpEnv();
		Response response = null;
		try {
			String requestBody = "{ \"productTypes\": [\"ACCESSORY\"], \"transactionType\": \"ACTIVATION\", \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			Map<String, String> headers = buildProductsApiV3Header();
			String resourceURL = "commerce/v3/products";
			response = invokeRestServiceCall(headers, tmng_api_base_url, resourceURL, requestBody, RestServiceCallType.POST);
		} catch (Exception e) {
			Assert.fail("Failed to retirieve products using POST");
		}
		return response;
	}
	
	public String saveAvailableDevices(String url) {
		String finalListOfAvailableProducts = "";
		try {
			File file = new File("src/test/resources/testdata/tmngAvailiableDevices.txt");
//			FileReader fr = new FileReader("src/test/resources/testdata/tmngAvailiableDevices.txt");
			if (file.exists()) {
				System.out.println("File is exist");
				Path filePath = file.toPath();
				BasicFileAttributes attr = Files.readAttributes(filePath, BasicFileAttributes.class);
				long lastModifiedDate = attr.lastModifiedTime().toMillis();
				Date date = new Date();
				long todayDate = date.getTime();
				long difference = todayDate - lastModifiedDate;
				System.out.println(lastModifiedDate + " and " + todayDate);
				System.out.println(difference);
				if (difference < 3600000) {
					System.out.println("Difference is less than one hour");
				} else {
					System.out.println("Difference is over one hour");
					List<String> products = getProducts(url);
					FileWriter file1 = new FileWriter("src/test/resources/testdata/tmngAvailiableDevices.txt");
					file1.write(products.toString());
					file1.close();
				}
			} else {
				System.out.println("File is not present");
				List<String> products = getProducts(url);
				FileWriter file1 = new FileWriter("src/test/resources/testdata/tmngAvailiableDevices.txt");
				file1.write(products.toString());
				file1.close();
			}
			
//			 int i;    
//	         while((i=fr.read())!=-1)  {
//	        	 
//	         }
			
		} catch (Exception e) {
			Assert.fail("Failed to create file");
		}
		return finalListOfAvailableProducts;
	}

	/**
	 * Provides list of available devices
	 * 
	 * @return
	 */
	public List<String> getProducts(String url) {
		List<String> products = new ArrayList<>();
		try {
			Response productsresponse = retrieveProductsUsingPOST(url);
//			System.out.println("ProductResponse" +productsresponse.toString());
			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					String productNode = productsNode.get(i).path("familyName").asText();

					JsonNode skuNode = productsNode.get(i).path("skus");
					if (!skuNode.isMissingNode() && skuNode.isArray()) {
						for (int j = 0; j < skuNode.size(); j++) {
							String defaultSKU = skuNode.get(j).path("isDefaultSku").asText();
							String skuCode = skuNode.get(j).path("skuCode").asText();
							String memory = skuNode.get(j).path("memory").asText();
							String color = skuNode.get(j).path("color").asText();

							JsonNode availabilityNode = skuNode.get(j).path("availability");
							if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
								String availabilityStatus = availabilityNode.path("availabilityStatus").asText();
								// Reporter.log("Product Details: " +productNode+ "|"+defaultSKU+ "|" +skuCode+
								// "|" +memory+ "|" +color+ "|"+availabilityStatus);
								// System.out.println("Product Details: " +productNode+ "|"+defaultSKU+ "|"
								// +skuCode+ "|" +memory+ "|" +color+ "|"+availabilityStatus);

								if (defaultSKU.equalsIgnoreCase("True") && availabilityStatus.equals("AVAILABLE")) {
									products.add(productNode);
								}
							}
						}
					}
				}
			}
			return products;
		} catch (Exception e) {
			Reporter.log("Failed to retrieve products");
		}
		return null;
	}

	/**
	 * Provides list of available devices
	 *
	 * @return
	 */
	public List<String> getProducts() {
		List<String> products = new ArrayList<>();
		try {
			Response productsresponse = retrieveProductsUsingPOST();
			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					String productNode = productsNode.get(i).path("familyName").asText();

					JsonNode skuNode = productsNode.get(i).path("skus");
					if (!skuNode.isMissingNode() && skuNode.isArray()) {
						for (int j = 0; j < skuNode.size(); j++) {
							String defaultSKU = skuNode.get(j).path("isDefaultSku").asText();
							/*String skuCode = skuNode.get(j).path("skuCode").asText();
							String memory = skuNode.get(j).path("memory").asText();
							String color = skuNode.get(j).path("color").asText();*/

							JsonNode availabilityNode = skuNode.get(j).path("availability");
							if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
								String availabilityStatus = availabilityNode.path("availabilityStatus").asText();

								if (defaultSKU.equalsIgnoreCase("True") && (availabilityStatus.equals("AVAILABLE") || availabilityStatus.equals("AVAILABLE_FOR_BACK_ORDER"))) {
									products.add(productNode);
								}
							}
						}
					}
				}
			}
			return products;
		} catch (Exception e) {
			Reporter.log("Failed to retrieve products");
		}
		return null;
	}

	public List<String> getTmngProducts() {
		List<String> tmngProducts = new ArrayList<>();
		List<String> phones = getPhones();
		List<String> tablets =getTablets();
		List<String> smartWatches = getSmartWatches();
		List<String> accessories = getAccessories();

		tmngProducts.addAll(phones);
		tmngProducts.addAll(tablets);
		tmngProducts.addAll(smartWatches);
		tmngProducts.addAll(accessories);
		return tmngProducts;
	}


	/**
	 * Provides list of available devices
	 *
	 * @return
	 */
	public List<String> getPhones() {
		List<String> tmngPhones = new ArrayList<>();
		try {
			Response productsresponse = retrieveProductsUsingPOSTForPhones();

			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					String productNode = productsNode.get(i).path("familyName").asText();

					JsonNode skuNode = productsNode.get(i).path("skus");
					if (!skuNode.isMissingNode() && skuNode.isArray()) {
						for (int j = 0; j < skuNode.size(); j++) {
							String defaultSKU = skuNode.get(j).path("isDefaultSku").asText();

							JsonNode availabilityNode = skuNode.get(j).path("availability");
							if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
								String availabilityStatus = availabilityNode.path("availabilityStatus").asText();

								if (defaultSKU.equalsIgnoreCase("True") && (availabilityStatus.equals("AVAILABLE") || availabilityStatus.equals("AVAILABLE_FOR_BACK_ORDER"))) {
									tmngPhones.add(productNode);
								}
							}
						}
					}
				}
			}
			return tmngPhones;
		} catch (Exception e) {
			Reporter.log("Failed to retrieve products");
		}
		return null;
	}

	/**
	 * Provides list of available devices
	 *
	 * @return
	 */
	public List<String> getTablets() {
		List<String> tmngTablets = new ArrayList<>();
		try {

			Response productsresponse = retrieveProductsUsingPOSTForTablets();

			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					String productNode = productsNode.get(i).path("familyName").asText();

					JsonNode skuNode = productsNode.get(i).path("skus");
					if (!skuNode.isMissingNode() && skuNode.isArray()) {
						for (int j = 0; j < skuNode.size(); j++) {
							String defaultSKU = skuNode.get(j).path("isDefaultSku").asText();

							JsonNode availabilityNode = skuNode.get(j).path("availability");
							if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
								String availabilityStatus = availabilityNode.path("availabilityStatus").asText();

								if (defaultSKU.equalsIgnoreCase("True") && (availabilityStatus.equals("AVAILABLE") || availabilityStatus.equals("AVAILABLE_FOR_BACK_ORDER"))) {
									tmngTablets.add(productNode);
								}
							}
						}
					}
				}
			}
			return tmngTablets;
		} catch (Exception e) {
			Reporter.log("Failed to retrieve products");
		}
		return null;
	}

	/**
	 * Provides list of available devices
	 *
	 * @return
	 */
	public List<String> getSmartWatches() {
		List<String> tmngSmartWatches = new ArrayList<>();
		try {

			Response productsresponse = retrieveProductsUsingPOSTForSmartWatches();

			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					String productNode = productsNode.get(i).path("familyName").asText();

					JsonNode skuNode = productsNode.get(i).path("skus");
					if (!skuNode.isMissingNode() && skuNode.isArray()) {
						for (int j = 0; j < skuNode.size(); j++) {
							String defaultSKU = skuNode.get(j).path("isDefaultSku").asText();

							JsonNode availabilityNode = skuNode.get(j).path("availability");
							if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
								String availabilityStatus = availabilityNode.path("availabilityStatus").asText();

								if (defaultSKU.equalsIgnoreCase("True") && (availabilityStatus.equals("AVAILABLE") || availabilityStatus.equals("AVAILABLE_FOR_BACK_ORDER"))) {
									tmngSmartWatches.add(productNode);
								}
							}
						}
					}
				}
			}
			return tmngSmartWatches;
		} catch (Exception e) {
			Reporter.log("Failed to retrieve products");
		}
		return null;
	}

	/**
	 * Provides list of available devices
	 *
	 * @return
	 */
	public List<String> getAccessories() {
		List<String> tmngAccessories = new ArrayList<>();
		try {

			Response productsresponse = retrieveProductsUsingPOSTForAccessories();

			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					String productNode = productsNode.get(i).path("familyName").asText();

					JsonNode skuNode = productsNode.get(i).path("skus");
					if (!skuNode.isMissingNode() && skuNode.isArray()) {
						for (int j = 0; j < skuNode.size(); j++) {
							String defaultSKU = skuNode.get(j).path("isDefaultSku").asText();

							JsonNode availabilityNode = skuNode.get(j).path("availability");
							if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
								String availabilityStatus = availabilityNode.path("availabilityStatus").asText();

								if (defaultSKU.equalsIgnoreCase("True") && (availabilityStatus.equals("AVAILABLE") || availabilityStatus.equals("AVAILABLE_FOR_BACK_ORDER"))) {
									tmngAccessories.add(productNode);
								}
							}
						}
					}
				}
			}
			return tmngAccessories;
		} catch (Exception e) {
			Reporter.log("Failed to retrieve products");
		}
		return null;
	}

	private Map<String, String> buildProductsApiV3Header() throws Exception {

		String accessToken = getOAuthTmngToken();

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + accessToken);
		headers.put("Content-Type", "application/json");
		headers.put("applicationID", "TMO");
		headers.put("cache-control", "no-cache,no-cache");
		headers.put("channelid", "WEB");
		headers.put("interactionid", "xasdf1234as");
		//headers.put("transactiontype", "ACTIVATION");
		headers.put("x-auth-originator",accessToken);

		return headers;
	}

}
