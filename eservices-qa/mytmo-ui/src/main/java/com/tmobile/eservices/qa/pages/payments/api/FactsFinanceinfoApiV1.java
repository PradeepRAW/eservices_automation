package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class FactsFinanceinfoApiV1 extends ApiCommonLib {
	
	

	 private Map<String, String> buildFactsFinanceInfoAPIHeader(ApiTestData apiTestData) throws Exception {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "dfsdfsdfsdffgf");
			headers.put("application_id", "MyTMO");
			headers.put("channel_id", "mobile");
			headers.put("transactionType", "AAL");
			headers.put("application_version_code", "900");
			headers.put("ban", apiTestData.getBan());
			headers.put("Content-Type", "application/json");
			headers.put("Accept", "application/json");
			headers.put("msisdns", "\""+apiTestData.getMsisdn()+"\"");
			headers.put("X-B3-TraceId", "PaymentsAPITest123");
			headers.put("X-B3-SpanId", "PaymentsAPITest456");
			headers.put("transactionid","473c9718-47de-4f09-a1d9-e5a6ff444645");
			return headers;
		}
	 
	 
	public Response FactsFinanceInfo(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildFactsFinanceInfoAPIHeader(apiTestData);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/facts/finance/data/summary", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

}
