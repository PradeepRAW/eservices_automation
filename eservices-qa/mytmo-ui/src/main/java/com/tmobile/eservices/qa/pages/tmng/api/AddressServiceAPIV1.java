package com.tmobile.eservices.qa.pages.tmng.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class AddressServiceAPIV1 extends ApiCommonLib{
	
 
    public Response createAddressValidation(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildCreditCheckAPIHeader(apiTestData,tokenMap);
         RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 		 reqSpec.addHeaders(headers);
 		 service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("credit-screen/v1/identity/screen", RestCallType.POST);
        // System.out.println(response.asString());
         return response;
    } 
    
    private Map<String, String> buildCreditCheckAPIHeader(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
    
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Authorization",  checkAndGetTMNGUNOToken());
		headers.put("correlationId","prescreen_9999");
		headers.put("ApplicationId","TMO");
		headers.put("channelId","WEB");
		headers.put("clientId", "TMNG");
		headers.put("transactionId", "xasdf1234as");
		headers.put("transactionType","ACTIVATION");
		headers.put("Cache-Control", "no-cache");
		headers.put("Content-type", "application/json");
		headers.put("usn", "check_3256");
		
		
		return headers;
	}
    

}
