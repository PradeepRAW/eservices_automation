/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class FamilyAllowanceManagePage extends CommonPage {

	private static final Logger logger = LoggerFactory.getLogger(FamilyAllowanceManagePage.class);
	private static final String pageUrl = "manage";

	@FindBy(css = "div.ui_headline")
	private WebElement header;

	public FamilyAllowanceManagePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Family allowance manage Page.
	 *
	 * @return the Family allowance manage page class instance.
	 */
	public FamilyAllowanceManagePage verifyFamilyAllowanceManagePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Family allowance manage page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Family allowance manage page not displayed");
		}
		return this;
	}
}
