package com.tmobile.eservices.qa.pages.tmng.functional;

import static org.testng.Assert.assertTrue;

import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

import io.appium.java_client.AppiumDriver;

public class CartPage extends TmngCommonPage {

	private final String pageUrl = "/cart";

	@FindBy(css = "img[src*='save-cart']")
	private WebElement cartIcon;

	@FindBy(css = "[ng-bind-html*='myCartCtrl.keyValues.cartHeader.cartTitle']")
	private WebElement myCartHeader;

	@FindBy(css = "button[type='submit'][aria-label='Retrieve cart']")
	private WebElement retrieveCartButton;

	@FindBy(css = "p#deviceFamilyName")
	private WebElement OrderDetailsSummary;

	@FindBy(css = "h5.p-t-15")
	private WebElement CartDetailsSummary;

	@FindBy(xpath = "//h3[contains(text(),'Save your cart online')]")
	private WebElement titleInSaveCartOnlineModalWindow;

	@FindBy(css = "img[alt='Click to add']")
	private WebElement addAnAccessoryImgOld;

	@FindBy(id = "finalizeInStoreEmail")
	private WebElement email;

	@FindBy(id = "firstName")
	private WebElement firstName;

	@FindBy(id = "phoneNumber")
	private WebElement phoneNumber;

	@FindBy(id = "callMeNowModalphoneNumber")
	private WebElement phoneNumberInRequestCallModel;

	@FindBy(id = "lastName")
	private WebElement lastName;

	@FindBy(id = "callMeNowModallastName")
	private WebElement lastNameInRequestCallModel;

	@FindBy(id = "zipcode")
	private WebElement zipCode;

	@FindBy(id = "callMeNowModalzipCode")
	private WebElement zipCodeInRequestCallModel;

	@FindBy(xpath = "//a[contains(@id,'cart_remove')]")
	private WebElement removeCTA;

	@FindBy(xpath = "//div[contains(@id,'cartDeviceSection')]")
	private WebElement addedDeviceNotInCartPage;

	@FindBy(css = "a[ng-enter='myCartCtrl.editProduct(selectedLine)']")
	private List<WebElement> editCTA;

	@FindBy(xpath = "/div/img[contains(@src,'3-in-1-sim-starter-kit-all')]/ancestor::div[contains(@class,'cart-product')]//span[contains(@class,'duplicate-icon')]/span[contains(.,'Duplicate')]")
	private List<WebElement> duplicateCTAForInternetSimKit;

	@FindBy(css = "#acctile a[title='Duplicate']")
	private WebElement duplicateCTAForaccessory;

	@FindBy(css = "div[class*='box-shadow-top'] span[ng-bind-html*='stickyBanner.preCreditPricingTxt']")
	private WebElement cartpagestickybannerbasedon;

	@FindBy(css = "[class*='fixed'][ng-if*='vm.isTPPOn'] p[class*='heading-5 '] span")
	private WebElement cartpagestickybannerMonthlyTotalPrice;

	@FindBy(css = "div[class*='box-shadow-top'] div[ng-if='vm.lineDetails.downPayment'] div p")
	private WebElement cartStickyBannerTodayTotalPrice;

	@FindBy(css = "div[class*='box-shadow-top'] span[ng-bind-html*='shippingEstMsgTxt']")
	private WebElement cartpagestickybannerEstimatedShippingText;

	@FindBy(css = "div[class*='box-shadow-top'] [aria-label*='+taxes and shipping']")
	private WebElement cartpagestickybannerTaxAndShippingText;

	@FindBy(css = "div[class*='box-shadow-top'] span[class='font-bold ng-binding']")
	private WebElement cartpagestickybannerEstimatedDate;

	@FindBy(css = "a[aria-label*='Complete in store']")
	private WebElement comepleteInStoreLinkStickyBanner;

	@FindBy(xpath = "//div[@class='service-details-wrapper p-xs-b-10 p-xs-l-5']/p[@aria-label='Protection<360>™']")
	private WebElement serviceForAddOns;

	@FindBy(css = "[aria-label*='Protection'] [ng-bind-html*='service']")
	private WebElement serviceAddOnOnCart;

	@FindBy(css = "div[class='modal-content']")
	private WebElement cartpagestickybannerOrderDetailsModel;

	@FindBy(css = "button[type='submit'][aria-label='Save my cart']")
	private WebElement saveMyCartOnlineInModalWindow;

	@FindBy(css = "button[ng-click*='!vm.overrideSaveCart']")
	private WebElement saveCartInReplaceYourCart;

	@FindBy(css = "button[aria-label='Close']")
	private WebElement closeOnRetailSaveCartModal;

	@FindBy(css = "a[id*='cart_duplicate_line'] span")
	private List<WebElement> duplicateCTA;

	@FindBy(css = "span[class*='disableLink']")
	private List<WebElement> duplicateLineDisabled;

	@FindBy(css = "span a[ng-bind*='vm.getSelectedStore().getName()']")
	private List<WebElement> storeLocator;

	@FindBy(css = "span.distance-rect-4")
	private List<WebElement> distanceToStore;

	@FindBy(css = "button[aria-label='Continue']")
	private WebElement continueBtn;

	@FindBy(css = "h2[ng-bind-html*='emptyCart']")
	private WebElement emptyCart;

	@FindBy(css = "a[ng-click*='Service.openSaveCartModal()']")
	private WebElement saveCart;

	@FindBy(css = "#dialogTitle[ng-bind-html*='vm.overrideSaveCart ?']")
	private WebElement saveCartPopupheader;

	@FindBy(css = "div[ng-class*='(vm.overrideSaveCart) ?']")
	private WebElement saveCartPopupSubheader;

	@FindBy(css = "input[name*='email']")
	private WebElement emailField;

	@FindBy(css = "p[ng-bind-html*='invalidEmail']")
	private WebElement emailFieldErrorMessage;

	@FindBy(css = "div[ng-if*='vm.overrideCart'] p.error-message")
	private WebElement errorMessageToOverrideInSaveCart;

	@FindBy(css = "button[type='submit'][aria-label='Save']")
	private WebElement saveButtonOnSaveCart;

	@FindBy(css = "button[ng-click*='!vm.overrideSaveCart']")
	private WebElement confirmationSaveCta;

	@FindBy(css = "div[ng-if*= 'd2rRetailSaveCart']")
	private WebElement completeStoremodalWindow;

	@FindBy(css = "button[ng-enter*='vm.cancel()']")
	private WebElement closeOnSaveCartModel;

	@FindBy(css = "[modal-instance*='vm.modalInstance']")
	private WebElement tradeInModal;

	@FindBy(css = "#deviceConditionGood input")
	private WebElement goodConditionRadioBtn;

	@FindBy(css = "#submitTradeInCTA")
	private WebElement getEstimatedCTA;

	@FindBy(css = "button[aria-label*='Agree']")
	private WebElement agreeCTA;

	@FindBy(css = "[pdl-modal-data*='Device Trade-in Confirmation']")
	private WebElement selectYourOptionsModal;

	@FindBy(css = "p[ng-bind-html*='standardTradeInHeader']")
	private WebElement standardTradeInRadioBtn;

	@FindBy(css = "[aria-labelledby*='promotionFlow']")
	private WebElement promoTradeInRadioBtn;

	@FindBy(css = "div[class*='box-shadow-top'] span[ng-click*='openPriceBreakdownModal']")
	private WebElement viewOrderDetailsLink;

	@FindBy(css = "#dialogTitle")
	private WebElement orderDetailsModal;

	@FindBy(css = "[aria-label*='SIM Starter Kit']")
	private WebElement simStarterKitText;

	@FindBy(css = "[ng-if*='tradeInDetails '] div[class*='border top right bottom']")
	private WebElement standardTradeInDetail;

	@FindBy(xpath = "//li[contains(@ng-if,' item.isTradeInPromoFlow')]")
	private WebElement promoTradeinDetailsonOrderDetail;

	@FindBy(css = "div[ng-if='myCartCtrl.toggles.tradeIn']")
	private WebElement promoTradeInDetail;

	@FindBy(css = "#deviceFamilyName")
	private WebElement deviceNameInOrderDetail;

	@FindBy(css = "span[ng-bind*='vm.getModel().product.getInventoryStatusMessage()']")
	private WebElement inventoryStatusOnCartPage;

	@FindBy(css = ".device-section [ng-click*='vm.openStoreLocatorModal()']")
	private List<WebElement> findNearbyStoresForDeviceLink;

	@FindBy(css = "#acctile [ng-click*='vm.openStoreLocatorModal()']")
	private List<WebElement> findNearbyStoresForAccessoryLink;

	@FindBy(css = "[ng-bind-html*='Values.tradeIn.tradeInCTA']")
	private List<WebElement> tradeDeviceBtnForEachLine;

	@FindBy(css = "button.btn.btn-secondary.ng-binding")
	private List<WebElement> tradeDeviceBtnForEachLineForMobile;

	@FindBy(css = "[ng-bind-html*='myCartCtrl.keyValues.cartRowItems.deviceTitle | parseHtml']")
	private WebElement deviceLabel;

	@FindBy(css = "div.p-t-5[ng-bind-html*='saveCartConfirmationModal']")
	private WebElement confirmationMessageOnSaveCartModel;

	@FindBy(css = "img[src*='Save_Cart']")
	private WebElement cartImageonSaveCartModel;

	@FindBy(css = "span.ng-binding.ng-isolate-scope.dollars")
	private List<WebElement> EIPPrice;

	@FindBy(css = "div[ng-bind-html*='vm.selectedStore.location.address.streetAddress']")
	private WebElement storeInSavecartModalPopup;

	@FindBy(css = "[ng-bind-html*='vm.authorValue.tradeInValueScreen.standardTradeInPriceLabel']")
	private WebElement estimatedTradeinValueTextSelectYourOptionModal;

	@FindBy(css = "[class='p-t-10'] span:nth-child(2)")
	private WebElement estimatedTradeinValueAmountSelectYourOptionModal;

	@FindBy(css = "[class='p-t-5 p-b-5 ng-binding']")
	private WebElement tradeinValueOnCart;

	@FindBy(xpath = "//div[@class='ng-scope']/div[@class='price-lockup-wrapper ng-scope ng-isolate-scope']")
	private List<WebElement> todayDownPaymentPriceInOrderDetailPage;

	@FindBy(css = "div[class*='border top'] div[class*='price-lockup-wrapper ng-isolate-scope']")
	private WebElement todayTotalPriceAtOrderDetailPageForFirstDevice;

	@FindBy(css = "div[ng-if*='priceBreakdownCtrl'] div.price-lockup-wrapper")
	private WebElement totalPriceAtOrderDetailPage;

	@FindBy(xpath = "//div[contains(@class,'price-lockup-wrapper ng-isolate-scope') and contains(@right-auto-pay-text,'monthly')]")
	private List<WebElement> monthlyLinePriceCartPage;

	@FindBy(css = "button[ng-click*='priceBreakdownCtrl.close()']")
	private WebElement orderDetailCloseIcon;

	@FindBy(xpath = "//div[contains(@class,'modal-wrapper')]//span//p[contains(@class,'priceBreakdown-whiteSpace')]/div")
	private WebElement totalEIPFinanceAmount;

	@FindBy(css = "div[aria-label='Total']")
	private WebElement totalFinanceTextOnOrderdetailModal;

	@FindBy(xpath = "//div[contains(@pdl-modal-data,'SaveCartID')]")
	private WebElement saveCartModelWindow;

	@FindBy(css = "[ng-bind-html*='finalizeInStoreConfirmationModal']")
	private WebElement confirmationMessage;

	@FindBy(css = "a[class*='tmo_tfn_number ng-binding']")
	private WebElement letsTalkMessage;

	@FindBy(css = "span[ng-bind-html*='tfnDisplay']")
	private WebElement tfnFunctionalityDisplay;

	@FindBy(id = "zipcode")
	private WebElement zipcodeTextBox;

	@FindBy(css = "div[class*='zip-wrapper'] button.btn")
	private WebElement enterYourzipcodeNextBtn;

	@FindBy(css = "#VM2TEXT")
	private WebElement otherservicesVoiceMailToTextOption;

	@FindBy(css = "#INCNAM2")
	private WebElement otherservicesNameIDOption;

	@FindBy(css = "button[aria-label*='Update']")
	private WebElement selectServiceUpdate;

	@FindBy(css = "span[ng-class*='penny-hidden']")
	private List<WebElement> selectedServicePricesinaddOnsContainer;

	@FindBy(xpath = "//span[contains(text(),'Financed Amount')]")
	private WebElement totalFinanceAmountText;

	@FindBy(css = "span[ng-bind='priceBreakdownCtrl.totalFinancedAmount']")
	private WebElement totalFinanceAmount;

	@FindBy(css = "span[ng-if*='item.fsp'] div[ng-if*='priceBreakdown']")
	private WebElement totalPriceOnOrderDetailedModal;

	@FindBy(css = "[ng-click*='vm.showItemsCTAButton()']")
	private WebElement showItemsLinksOnSaveCartModal;

	@FindBy(css = "[pdl-event='click'] span[class*= 'text-green ng-binding']")
	private WebElement inventoryStatusOnSaveCartModal;

	@FindBy(css = "span[ng-bind='accessory.familyName']")
	private WebElement accessoryNameOnSaveCartModal;

	@FindBy(css = "[id=acctile] [size-class='price-lockup-small']")
	private WebElement priceOfAccessoryOnCart;

	@FindBy(css = ".accessory-price-component money[price='leftAmount']")
	private WebElement priceOfAccessoryTodayOnCart;

	@FindBy(css = ".accessory-price-component money[price='rightAmount']")
	private WebElement priceOfAccessoryMonthlyOnCart;

	@FindBy(css = "[size-class='price-lockup-xsmall'][ng-if*='accessories']")
	private WebElement totalPriceOfAccessoryOnCart;

	@FindBy(css = "a[ng-click*='Service.openLoadCartModal()']")
	private WebElement retrieveCTAOnCart;

	@FindBy(css = "div[pdl-modal-data*=Retrieve]")
	private WebElement retrieveModalOnCart;

	@FindBy(css = "div.p-t-20.col-lg-offset-1")
	private WebElement itemsOnOrderDetailsForRetrieve;

	@FindBy(css = "span div[show-as-discount='true']")
	private WebElement orderDetailsModalStrikeThroughPrice;

	@FindBy(css = "span[class='ng-scope']")
	private List<WebElement> orderDetailsModalFRP;

	@FindBy(css = "p[class*='small priceBreakdown']")
	private WebElement orderDetailsModalEIP;

	@FindBy(css = "a[ng-if*='promo.longDescription']")
	private WebElement orderDetailsModalInstanceDiscount;

	@FindBy(css = "a[ng-click*='vm.showEditAccessory(acc)']")
	private List<WebElement> editCTAForAccessory;

	@FindBy(css = "#addBtnAcc")
	private WebElement editAccessoryButton;

	@FindBy(css = "div.modal-content div.cost-divider.text-magenta + div span[ng-class='dollarClass']")
	private List<WebElement> secondDeviceMonthlyPrice;

	@FindBy(css = ".modal-dialog div[right-text='Monthly']")
	private List<WebElement> monthlyLinePriceOrderDetails;

	@FindBy(css = ".modal-dialog span[ng-if*='autoPayDiscount']")
	private List<WebElement> autoPayTextForEachLineOrderDetailsCartPage;

	@FindBy(css = "[ng-if*='vm.selectedLine.tradeInDetails']  p")
	private List<WebElement> deviceInfoInTradeinContainerInCart;

	@FindBy(css = "div[ng-if*='isAutoPayOn']")
	private WebElement totalAutoPayDiscountBannerOrderDetailsCartPage;

	@FindBy(css = "[ng-click*='vm.myCartCtrl.showAccessories']")
	private List<WebElement> addAnAcessorySessionemptyCartOld;

	@FindBy(css = "div[class*='fixed-banner-bottom']")
	private WebElement cartpageStickyBanner;

	@FindBy(css = "div[aria-label*='Add a phone']")
	private WebElement addAPhoneLinkOnCart;

	@FindBy(css = "div[aria-label*='Add a tablet']")
	private WebElement addATabletLinkOnCart;

	@FindBy(css = "div[aria-label*='Add a wearable']")
	private WebElement addAWearableLinkOnCart;

	@FindBy(css = "div[aria-label*='Add an accessory']")
	private WebElement addAAcessoryLinkOnCart;

	@FindBy(css = "div[aria-label*='Bring your device']")
	private WebElement byodLinkOnemptyCartEssentials;

	@FindBy(css = "button[aria-label*='Add a tablet or wearable']")
	private List<WebElement> tabletOrWearableOld;

	@FindBy(xpath = "//span[text()='Add a phone']")
	private WebElement addAPhoneLinkOnCartEssentials;

	@FindBy(xpath = "//span[text()='Add a tablet']")
	private WebElement addATabletLinkOnCartEssentials;

	@FindBy(xpath = "//span[text()='Add a wearable']")
	private WebElement addAWearableLinkOnCartEssentials;

	@FindBy(css = "div[aria-label*='Add an accessory'] img")
	private WebElement addAAccessoryImgOnemptyCartEssentials;

	@FindBy(xpath = "//span[text()='Add an accessory']")
	private WebElement addAAccessoryLinkOnCartEssentials;

	@FindBy(css = "div[aria-label*='Bring your device'] img")
	private WebElement byodImgOnemptyCartEssentials;

	@FindBy(xpath = "//span[text()='Bring your device']")
	private WebElement byodLinkOnCartEssentials;

	@FindBy(css = "span[ng-bind-html*='addMoreLinesHeaderText']")
	private WebElement addMoreLinesOrDevicesHeader;

	@FindBy(css = "div.modal-custom-container.clearfix h4")
	private WebElement selectServicesHeader;

	@FindBy(css = "div[class*='service-checkbox-section'] label div[class='indicator checkbox-indicator'] span")
	private List<WebElement> selectService;

	@FindBy(css = "[ng-bind-html*='myCartCtrl.keyValues.essentials']")
	private WebElement yourPlanHeader;

	@FindBy(css = "div[ng-if*='plan.isHeroPlan']")
	private WebElement megentaPlan;

	@FindBy(css = ".plan-container p[aria-label*='Plus']")
	private WebElement megentaPlusPlanHeader;

	@FindBy(css = "[ng-bind-html*='vm.keyValues.bestValue']")
	private WebElement bestValuePlanBadge;

	@FindBy(css = "[aria-label*='Taxes and fees included']")
	private WebElement TMobileOnePlanBottomText;

	@FindBy(xpath = "//p[@aria-label='Basic plan: Unlimited talk, text, & 4G LTE smartphone data.']")
	private WebElement TMobileEssentialsPlanDescription;

	@FindBy(css = "[ng-bind-html*='plan.planCost']")
	private List<WebElement> TMobileEssentialsPlanCost;

	@FindBy(css = "[aria-label='Plus taxes and fees']")
	private WebElement TMobileEssentialsPlanBottomtext;

	@FindBy(css = "[ng-bind-html*='vm.keyValues.legalTextForPlan']")
	private WebElement legalTextForPlan;

	@FindBy(css = "[ng-bind-html*='vm.keyValues.plansCtaText']")
	private WebElement seeDetailsAndComparePlansCTA;

	@FindBy(css = "[ng-bind-html*='vm.keyValues.planDisable']")
	private WebElement essentialPlanIneligibleMessafeDisabled;

	@FindBy(css = "div.nudge.bg-magenta.note-bar")
	private WebElement topNotificationBanner;

	@FindBy(css = "p[ng-bind-html*='message.messageValue']")
	private WebElement switchMegentaPlusBanner;

	@FindBy(xpath = "//div[contains(@ng-bind-html,'lineMaxHandsetLimitReached')]")
	private WebElement lineMaxLimitReachedMessage;

	@FindBy(css = "span[ng-bind='vm.keyValues.essentials.wearableDisable']")
	private WebElement authorableMsgOnAddWearableImg;

	@FindBy(css = "div.plan")
	private WebElement planBlockForLine;

	@FindBy(xpath = "//p[contains(@ng-bind-html,'plansTitle')]")
	private WebElement planHeaderForLine;

	@FindBy(xpath = "//span[contains(@ng-bind-html,'planName')]")
	private List<WebElement> planItemForLineList;

	@FindBy(xpath = "//span[contains(@ng-bind-html,'planName')]/../a")
	private WebElement planToolTipForLine;

	@FindBy(css = "button[aria-label*='Add a phone']")
	private List<WebElement> addAPhoneCTAS;

	@FindBy(css = "div.popover")
	private WebElement planDescriptionOnModal;

	@FindBy(xpath = "//p[contains(text(),'ADD AN ACCESSORY')]")
	private List<WebElement> addAccessoryHeader;

	@FindBy(css = "[ng-click*='vm.myCartCtrl.showAccessories'] img")
	private List<WebElement> addAccessoryImg;

	@FindBy(css = ".plan-container:nth-child(1).active p[aria-label*='Selected']")
	private WebElement activePlan;

	@FindBy(css = "[class*='cart-col pricing-section']")
	private List<WebElement> lineLevelPricingBlock;

	@FindBy(css = "[ng-bind*='monthlyPaymentTitle']")
	private List<WebElement> monthlyPaymentHeader;

	@FindBy(css = "div[ng-repeat*='monthlyPaymentDetails'] span[aria-label*='Line']")
	private List<WebElement> monthlyPaymentLineDeposite;

	@FindBy(css = "div[ng-repeat*='monthlyPaymentDetails'] span[aria-label*='Device']")
	private List<WebElement> monthlyPaymentDevice;

	@FindBy(css = "span[aria-label*='Discount']")
	private List<WebElement> monthlyPaymentDiscount;

	@FindBy(css = "span[aria-label*='Add-ons']")
	private List<WebElement> AddOnsLineItem;

	@FindBy(css = "span[aria-label*='Add-ons']~span")
	private List<WebElement> AddOnsLineItemPrice;

	@FindBy(css = "div[ng-repeat*='monthlyPaymentDetails'] span[aria-label*='Line']~span")
	private List<WebElement> monthlyPaymentLinePrice;

	@FindBy(css = "div[ng-repeat*='monthlyPaymentDetails'] span[aria-label*='Device']~span")
	private List<WebElement> monthlyPaymentDevicePrice;

	@FindBy(css = "span[aria-label*='Discount']~span")
	private List<WebElement> monthlyPaymentDiscountPrice;

	@FindBy(css = "[ng-bind*='oneTimePaymentTitle']")
	private List<WebElement> oneTimePaymentHeader;

	@FindBy(css = "span[aria-label*='SIM Starter Kit']~span")
	private List<WebElement> simstarterKitPrice;

	@FindBy(css = "span[aria-label*='SIM Starter Kit']")
	private List<WebElement> simstarterKitLabel;

	@FindBy(css = "p[aria-label*='taxes and shipping']")
	private List<WebElement> taxesAndShippingTextMultiline;

	@FindBy(css = "span[ng-bind*='myCartCtrl'][aria-label='Device']~span")
	private List<WebElement> priceBreakdownUnderOneTimePayment;

	@FindBy(css = "div[ng-if='vm.isTPPON'] div div div p[aria-label*='$']")
	private List<WebElement> priceBreakdownTodayPrice;

	@FindBy(css = "div[class*='no-padding c'] p[class*='heading-5'] span[aria-label*='$']")
	private List<WebElement> priceBreakdownMonthlyPrice;

	@FindBy(css = "[ng-bind*='autoPayDiscountTextWithValue']")
	private List<WebElement> priceBreakdownAutoPaymentMessage;

	@FindBy(css = "[class*='protection-modal-content']")
	private WebElement protectionModal;

	@FindBy(css = "[for='P3605']")
	private WebElement protection360OnprotectionModal;

	@FindBy(css = "[id='cart_service_add_to_cart']")
	private WebElement updateCTAOnprotectionModal;

	@FindBy(css = "[ng-bind-html*='zipCodeModal.headerZip']")
	private WebElement zipCodeModal;

	@FindBy(css = "[ng-click*='validateZipCode(zipCodeForm)']")
	private WebElement nextCTAOnZipCodeModal;

	@FindBy(css = "span[ng-bind-html='service.familyName']")
	private List<WebElement> listOfAdOnsAvailable;

	@FindBy(css = "span[ng-bind-html='service.servicePrice']")
	private List<WebElement> listOfPriceForAddOns;

	@FindBy(css = "[type='checkbox']")
	private List<WebElement> listOfCheckBoxForAddOns;

	@FindBy(css = "a[ng-class*='myCartCtrl.keyValues.icons.tooltipIcon'][aria-hidden='false']")
	private List<WebElement> listOfToolTipsForAddOns;

	@FindBy(css = "[class*='bottom'] [ng-bind-html*='includeLabel']")
	private WebElement autoPaymentMessageStickyBanner;

	@FindBy(css = "div.modal-wrapper.zip-wrapper h4")
	private WebElement enterYourZipCodeHeader;

	@FindBy(css = "div[class*='nav-tabs-wrapper'] span[aria-label*='Recommended services']")
	private WebElement recommendedServiceLabel;

	@FindBy(css = "#device-service-panel p[aria-label*='Protection<360>']")
	private WebElement recommendedService;

	@FindBy(css = "div[class*='nav-tabs-wrapper'] span[aria-label*='Other services']")
	private WebElement otherServicesLabel;

	@FindBy(css = "#device-service-panel h5[aria-label*='Communication and Data']")
	private WebElement communicationAndData;

	@FindBy(css = "#device-service-panel h5[aria-label*='Device Protection']")
	private WebElement deviceProtection;

	@FindBy(css = "#device-service-panel h5[aria-label*='Entertainment and More']")
	private WebElement entertaintmentAndMore;

	@FindBy(css = "[ng-bind-html*='ineligibleModalCtrl.authorValue.cart.essentials.modalMessage']")
	private WebElement ineligibleModal;

	@FindBy(css = "[ng-click*='ineligibleModalCtrl.changePlan()']")
	private WebElement ineligibleModalYes;

	@FindBy(css = "div[class*='box-shadow visible-xs']")
	private WebElement cartPageStickyBannerMobile;

	@FindBy(css = "div[class*='box-shadow visible-xs'] div[ng-if='vm.lineDetails.estimatedMonthly'] div p[class*='heading'] span")
	private WebElement cartpagestickybannerMonthlyTotalPriceMobile;

	@FindBy(css = "div[class*='box-shadow visible-xs'] div[ng-if='vm.lineDetails.downPayment'] div p")
	private WebElement cartpagestickybannerTotalPriceMobile;

	@FindBy(css = "div[class*='box-shadow visible-xs'] span[aria-label*='Order estimated to ship']")
	private WebElement cartpagestickybannerEstimatedShippingTextMobile;

	@FindBy(css = "div[class*='box-shadow visible-xs'] div[aria-label='+taxes and shipping']")
	private WebElement cartpagestickybannerTaxAndShippingTextMobile;

	@FindBy(css = "div[class*='box-shadow visible-xs'] div[ng-if*='shippingEstimate'] span[aria-label*='availFromDate'] ~ span[class*='bold']")
	private WebElement cartpagestickybannerEstimatedDateMobile;

	@FindBy(css = "div[class*='box-shadow visible-xs'] div[ng-if='vm.lineDetails.estimatedMonthly'] p[class*='limited-time-offer']")
	private WebElement autoPaymentMessageStickyBannerMobile;

	@FindBy(css = "span[ng-if*='myCartCtrl.toggles.finalizeInStore'] a")
	private WebElement completeInStoreMobileOutside;

	@FindBy(css = "div[class*='box-shadow visible-xs'] button[aria-label='Continue']")
	private WebElement continueBtnMobile;

	@FindBy(css = "div[class*='box-shadow visible-xs'] [aria-label*='View Order Details']")
	private WebElement viewOrderDetailsLinkMobile;

	@FindBy(css = ".modal-custom-container.clearfix")
	private WebElement wearablesIneliigibleModal;

	@FindBy(css = "span[ng-bind-html*='ineligibleModalCtrl.authorValue.cart.essentials.modalMessage']")
	private WebElement wearablesIneliigibleModalMessage;

	@FindBy(css = "button[ng-click*='ineligibleModalCtrl.deleteFromCart()']")
	private WebElement removeWearableCTA;

	@FindBy(css = "button[ng-click*='ineligibleModalCtrl.changePlan()']")
	private WebElement changePlanCTA;

	@FindBy(css = "#cartDeviceSection_2")
	private WebElement wearableAddedInCart;

	@FindBy(css = "button[ng-click*='ineligibleModalCtrl.close()']")
	private WebElement wearablesIneliigibleModalCloseCTA;

	@FindBy(id = "protection-plans-1")
	private WebElement protectionAddon;

	@FindBy(css = "label#protection-plans-0")
	private List<WebElement> eachLineFirstAddon;

	@FindBy(id = "serviceFamilyName")
	private WebElement serviceFamilyName;

	@FindBy(xpath = "//p[contains(@ng-bind,'oneTimePaymentTitle')]/following-sibling::div/span[contains(@ng-bind,'deviceLabel')]")
	private List<WebElement> deviceLabelOneTimePayment;

	@FindBy(css = "span[ng-bind-html*='dataOptions.displayName']")
	private WebElement priceBreakdownAddOnPriceText;

	@FindBy(css = "span[ng-click*='vm.openStoreLocatorModal()']")
	private WebElement findNearByStoresInSavecartModalPopup;

	@FindBy(id = "get-inline-cta")
	private WebElement getInLineCTA;

	@FindBy(css = "button[aria-label='Find a store']")
	private WebElement findAStore;

	@FindBy(id = "storeName")
	private WebElement storeNameInFilteredSearch;

	@FindBy(css = ".selected-protection-plan")
	private List<WebElement> addOnsListFromTiles;

	@FindBy(css = "button.close")
	private WebElement closeSelectServicesIcon;

	@FindBy(css = "div[ng-click*='myCartCtrl.editProduct(selectedLine)']")
	private WebElement deviceInCart;

	@FindBy(css = "div.addon-extras.plan")
	private WebElement addOnsAndExtraSection;

	@FindBy(css = "p[ng-bind-html*='plan.planName']")
	private List<WebElement> planHeaderFromPlansSection;

	@FindBy(css = "b[id*='store-name']")
	private List<WebElement> storesInStoreListPage;

	@FindBy(css = "button.store-result-item")
	private List<WebElement> filteredStoreslist;

	@FindBy(css = "p[ng-bind*='estimatedMonthLabel']")
	private WebElement estimatedMonthlyText;

	@FindBy(css = "p[class*='limited-time-offer']")
	private List<WebElement> authorableText;

	@FindBy(css = "div[ng-click*='plan.planName, plan.planType']")
	private List<WebElement> allPlanSection;

	@FindBy(css = "label[for='cartFilterCheckbox']")
	private WebElement checkBoxStatus;

	@FindBy(css = "p[ng-bind-html*='pbReinforceMessageTablet']")
	private WebElement tabletLineDiscountOrderDetailsModal;

	@FindBy(css = "span[aria-label*='Taxes and fee included']")
	private WebElement magentaPlusPlanTaxesText;

	@FindBy(css = "span[aria-label*='1 line $85/mo.']")
	private WebElement magentaPlusPlanPricingDetails;

	@FindBy(css = "span[aria-label*='Please remove']")
	private WebElement magentaPlusAuthorableErrorMessage;

	@FindBy(xpath = "//p[contains(text(),'Magenta™ Plus')]/..")
	private WebElement magentaPlusPlanTile;

	@FindBy(css = "div[ng-if*='vm.creditCheck'] div[class*='color-black text-center']")
	private WebElement newPricesBasedOnCreditClass;

	@FindBy(css = ".added-product-detail-sec h5")
	private WebElement miSimKitName;

	@FindBy(css = "[class*='bottom'] [ng-bind-html*='finalDiscountValue']")
	private WebElement stickeyBannertotalAutoPayDiscount;

	@FindBy(css = "[class*='bottom'] [ng-bind-html*='paymentLbl']")
	private WebElement tppStickyBannerAutoPayEndingText;

	@FindBy(css = "#simKitDisplayName")
	private WebElement simKitNameOnOrderDetailsModal;

	@FindBy(css = "span[ng-if*='item.frp == item.fsp']")
	private WebElement simKitPriceOnOrderDetailsModal;

	@FindBy(xpath = "//span[contains(@aria-label, 'Estimated monthly')]/parent::p/preceding-sibling::p")
	private List<WebElement> monthlyPriceUnderMonthlyPaymentSection;

	@FindBy(css = "p[class*='priceBreakdown'] div[ng-if*='deviceDetails.pricingOption']")
	private List<WebElement> orderDetailsLineMonthlyPrice;

	@FindBy(css = "#collapseIcon")
	private List<WebElement> orderDetailsModalCollapseIcon;

	@FindBy(css = "p[aria-labelledby*='0price6'] div[class*='ng-isolate-scope']")
	private List<WebElement> orderDetailsModalLinePlanPrice;

	@FindBy(css = "#itemPrice div div[class*='price-lockup-wrapper ng-isolate-scope']")
	private List<WebElement> orderDetailsModalLineMonthlyTotalPrice;

	@FindBy(css = "p[aria-labelledby*='1price6'] div[class*='ng-isolate-scope']")
	private WebElement orderDetailsModalLinePlanPriceWearable;

	@FindBy(css = "p[aria-labelledby*='2price6'] div[class*='ng-isolate-scope']")
	private WebElement orderDetailsModalLinePlanPriceTablet;

	@FindBy(css = "[class*='hidden-xs'] [class*='verify-pricing-btn']")
	private WebElement findYourPriceButton;
	
	@FindBy(css = "[class*='box-shadow visible-xs'] [ng-click='vm.findYourPriceUrl()']")
	private WebElement findYourPriceButtonMobile;

	@FindBy(css = "span[ng-bind-html*='stickyBanner.postCreditPricingTxt']")
	private WebElement basedOnCreditText;

	@FindBy(css = "div[ng-if*='priceBreakdownCtrl'] div[size-class='price-lockup-medium']")
	private WebElement todayTotalPriceonOrderDetailPage;

	@FindBy(css = "div[ng-if*='item.simKitDetails.listPrice']")
	private WebElement simStarterKitPrice;

	@FindBy(css = "p[id='simKitPayNowPrice'] div[class*='price-lockup-wrapper']")
	private WebElement simStarterKitPriceAfterPromoDiscount;

	@FindBy(css = "[class*='font-arial-bold text-gray-dark']")
	private List<WebElement> productListOnOrderDetailsPage;

	@FindBy(css = "div[class*='nav-tabs-wrapper'] span[aria-label*='Recommended services'] ~p")
	private WebElement recommendedServiceCheckboxSelected;

	@FindBy(css = "[ng-bind-html*='seeMoreCTAText']")
	private List<WebElement> seeMoreOptionsCTAAddOnsAndExtrasList;

	@FindBy(css = "span[ng-bind-html*='addOnsAndExtraHeaderTitle']")
	private List<WebElement> addOnExtrasHeaderLineLevel;

	@FindBy(css = "h5[ng-bind-html*='findCartCta']")
	private WebElement retrieveCartHeader;

	@FindBy(css = "label[ng-bind-html*='emailLabel']")
	private WebElement retrieveCartText;

	@FindBy(css = "#promoInput")
	private WebElement addPromoCodeButton;

	@FindBy(css = "button[ng-click*='vm.applyPromocode']")
	private WebElement applyButton;

	@FindBy(css = "div[ng-if*='vm.toggles.callMeNow']")
	private WebElement callMeNow;

	@FindBy(css = "#callMeNowModalpersonalDisclaimer")
	private WebElement checkBoxInRequestCallModel;

	@FindBy(css = "div[class*='modal-content']")
	private WebElement requestCallModelPage;

	@FindBy(css = "#infoNextButton")
	private WebElement callMeNowButtonInModel;

	@FindBy(css = "div[ng-bind-html*='vm.authorValue.immediateMsgLabel']")
	private WebElement confirmationMessageInReuqestCallModel;

	@FindBy(css = "h4[ng-bind-html*='vm.authorValue.headerLabel']")
	private WebElement confirmationHeaderInReuqestCallModel;

	@FindBy(css = "input[class*='ng-invalid-pattern']")
	private WebElement invalidEmailErrorTextField;

	@FindBy(css = "h5[ng-bind-html*='contactUsTitle']")
	private WebElement customerCartContainerHeaderOncart;

	@FindBy(css = "[ng-bind-html*='contactUsDesc']")
	private WebElement customerCartContainerDescOnEmptycart;

	@FindBy(css = "img[alt='Call us Image']")
	private WebElement headsetIconOncustomerCartContainerOnEmptycart;

	@FindBy(css = "[ng-bind-html*='contactUsCallText']")
	private WebElement callUstextOncustomerCartContainerOnEmptycart;

	@FindBy(css = "[ng-bind-html*='contactUsCallValue'] span")
	private WebElement callUsNumberOncustomerCartContainerOnEmptycart;

	@FindBy(css = "span[ng-bind*='lineDepositLabel']")
	private WebElement refundableDepositText;

	@FindBy(css = "div[ng-if*='refundableDesposit.amount'] span[aria-label*='$']")
	private WebElement refundableDepositAmt;

	public CartPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 */
	public CartPage verifyCartPageLoaded() {
		checkPageIsReady();
		waitforSpinner();
		try {
			Thread.sleep(5000);
			waitFor(ExpectedConditions.visibilityOf(myCartHeader), 60);
			verifyCartPageUrl();
			Reporter.log("Navigated to Cart Page");
			// verifyDuplicatedElements(this.getClass());
		} catch (Exception e) {
			Assert.fail("Cart Page not loaded");
		}
		return this;
	}

	/**
	 * Get Price Of Accessory On CartPage
	 *
	 */
	public String getPriceOfAccessoryOnCartPage() {
		String priceOfAccessory = null;
		try {
			checkPageIsReady();
			priceOfAccessory = priceOfAccessoryOnCart.getAttribute("left-amount");
			priceOfAccessory = priceOfAccessory.replace("$", "");
			Reporter.log("Get Price of Accessory on Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to get Price of Accessory on Cart Page");
		}
		return priceOfAccessory;
	}

	/**
	 * Get total Price Of Accessories On CartPage
	 *
	 */
	public String getTotalPriceOfAccessoryOnCartPage() {
		String priceOfAccessory = null;
		try {
			checkPageIsReady();
			priceOfAccessory = totalPriceOfAccessoryOnCart.getAttribute("left-amount");
			Reporter.log("Get Total Price of Accessory on Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to get total Price of Accessory on Cart Page");
		}
		return priceOfAccessory;
	}

	/**
	 * verify Modal window
	 */
	public CartPage verifyCompleteStoreModalWindow() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(completeStoremodalWindow), 10);
			Assert.assertTrue(completeStoremodalWindow.isDisplayed(), "Modal Window is not displaying");
			Reporter.log("Modal Window popup is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Modal window by clicking on Complete in store in cart");
		}
		return this;
	}

	/**
	 * verify cart icon in Modal window
	 */
	public CartPage verifyCarticonInModalWindow() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cartIcon), 60);
			Assert.assertTrue(cartIcon.isDisplayed(), "Cart icon is not displaying");
			Reporter.log("Cart icon is displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Cart Icon");
		}
		return this;
	}

	/**
	 * verify Title message in modal window
	 */
	public CartPage verifyTitleInSaveCartOnlineModalWindow() {
		try {
			waitFor(ExpectedConditions.visibilityOf(titleInSaveCartOnlineModalWindow), 60);
			Assert.assertTrue(titleInSaveCartOnlineModalWindow.isDisplayed(),
					"Modal Window Title message is not dispalying");
			Reporter.log("Modal Window Title message is dispalying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Title at Modal Window");
		}
		return this;
	}

	/**
	 * verify Email Input field in Modal window
	 */
	public CartPage verifyEmailFieldInmodalWindow() {
		try {
			Assert.assertTrue(email.isDisplayed(), "Email field is not displaying");
			Reporter.log("Email field is displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Email filed in modal window");
		}
		return this;
	}

	/**
	 * Enter email
	 */
	public CartPage enterEmailInmodalWindow(TMNGData tMNGData) {
		try {
			email.clear();
			email.sendKeys(tMNGData.getEmail());
			Reporter.log("Able to enter email address successfully");
		} catch (Exception e) {
			Assert.fail("Failed to enter email address");
		}
		return this;
	}

	/**
	 * Enter first name
	 */
	public CartPage enterFirstNameInmodalWindow(TMNGData tMNGData) {
		try {
			firstName.clear();
			firstName.sendKeys(tMNGData.getFirstName());
			Reporter.log("Able to enter first name successfully");
		} catch (Exception e) {
			Assert.fail("Failed to enter first name ");
		}
		return this;
	}

	/**
	 * Enter last name
	 */
	public CartPage enterLastNameInmodalWindow(TMNGData tMNGData) {
		try {
			lastName.clear();
			lastName.sendKeys(tMNGData.getLastName());
			Reporter.log("Able to enter last name  successfully");
		} catch (Exception e) {
			Assert.fail("Failed to enter last name ");
		}
		return this;
	}

	/**
	 * Enter phone number
	 */
	public CartPage enterPhoneNumberInmodalWindow(TMNGData tMNGData) {
		try {
			phoneNumber.clear();
			phoneNumber.sendKeys(tMNGData.getPhoneNumber());
			Reporter.log("Able to enter PhoneNumber  successfully");
		} catch (Exception e) {
			Assert.fail("Failed to enter PhoneNumber ");
		}
		return this;
	}

	/**
	 * Enter zip code
	 */
	public CartPage enterZipCodeInmodalWindow(TMNGData tMNGData) {
		try {
			zipCode.clear();
			zipCode.sendKeys(tMNGData.getZipcode());
			Reporter.log("Able to enter ZipCode  successfully");
		} catch (Exception e) {
			Assert.fail("Failed to enter ZipCode");
		}
		return this;
	}

	/**
	 * Verify remove CTA in Cart page
	 */
	public CartPage verifyRemoveCTA() {
		try {
			Assert.assertTrue(removeCTA.isDisplayed(), "Remove CTA is not displayed on cart");
			Reporter.log("Remove CTA is displayed on cart");
		} catch (Exception e) {
			Assert.fail("Remove CTA is not displayed on cart");
		}
		return this;
	}

	/**
	 * Verify Edit CTA in Cart page
	 */
	public CartPage verifyEditCTA() {
		try {
			Assert.assertTrue(editCTA.get(0).isDisplayed(), "Edit CTA is not displayed on cart");
			Reporter.log("Edit CTA is displayed on cart");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit CTA on cart");
		}
		return this;
	}

	/**
	 * Verify Edit CTA for BYOD in Cart page
	 */
	public CartPage verifyBYODEditCTALink() {
		try {
			scrollDownToelement(tradeDeviceBtnForEachLine.get(0));
			Assert.assertFalse(isElementDisplayed(editCTA.get(0)), "edit CTA for BYOD is displayed");
		} catch (Exception e) {
			Assert.fail("BYOD edit cta link not displayed");
		}
		return this;
	}

	/**
	 * Click remove CTA in Cart page
	 */
	public CartPage clickRemoveCTA() {
		try {
			moveToElement(duplicateCTA.get(0));
			waitFor(ExpectedConditions.elementToBeClickable(removeCTA), 60);
			clickElementWithJavaScript(removeCTA);
			Reporter.log("Remove CTA is clicked on cart");
		} catch (Exception e) {
			Assert.fail("Remove CTA is not clickable on cart" + e.getMessage());
		}
		return this;
	}

	/**
	 * Click edit CTA in Cart page
	 */
	public CartPage clickEditCTA() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(editCTA.get(0)), 60);
			moveToElement(tradeDeviceBtnForEachLine.get(0));
			editCTA.get(0).click();
			Reporter.log("Edit CTA is clicked on cart");
		} catch (Exception e) {
			Assert.fail("Edit CTA is not clickable on cart");
		}
		return this;
	}

	/**
	 * Verify Duplicate CTA in Cart page
	 */
	public CartPage verifyDuplicateCTA() {
		try {
			Assert.assertTrue(duplicateCTA.get(0).isDisplayed(), "Duplicate CTA is not displayed on cart");
			Reporter.log("Duplicate CTA is displayed on cart");
		} catch (Exception e) {
			Assert.fail("Failed to verify Duplicate CTA on cart");
		}
		return this;
	}

	/**
	 * Click Duplicate CTA in Cart page
	 */
	public CartPage clickDuplicateCTA() {
		try {
			moveToElement(removeCTA);
			waitFor(ExpectedConditions.elementToBeClickable(duplicateCTA.get(0)), 60);
			clickElementWithJavaScript(duplicateCTA.get(0));
			Reporter.log("Duplicate CTA is clicked on cart");

		} catch (Exception e) {
			Assert.fail("Duplicate CTA is not clickable on cart");
		}
		return this;
	}

	/**
	 * Click Duplicate CTA for Internet Sim Kit Cart page
	 */
	public CartPage clickDuplicateCTAForInterNetSimKit() {
		try {
			if (duplicateCTAForInternetSimKit.size() > 1) {
				clickElementWithJavaScript(duplicateCTAForInternetSimKit.get(duplicateCTAForInternetSimKit.size() - 1));
			} else {
				clickElementWithJavaScript(duplicateCTAForInternetSimKit.get(0));
			}
			Reporter.log("Duplicate CTA for Internet Sim Kit is clicked on cart");
		} catch (Exception e) {
			Assert.fail("Duplicate CTA is not clickable on cart");
		}
		return this;
	}

	/**
	 * Verify Duplicate line in Cart page
	 */
	public CartPage verifyDuplicateLinesOnCart() {
		try {
			checkPageIsReady();
			Assert.assertEquals(duplicateCTA.size(), 2,
					"Duplicate Line is not added after clicking on Duplicate CTA on Cart");
			Reporter.log("Duplicate Line is added after clicking on Duplicate CTA on Cart");

		} catch (Exception e) {
			Assert.fail("Failed to verify duplicate Lines on Cart after clicking on Duplicate CTA");
		}
		return this;
	}

	/**
	 * Validate Save cart Cta in ModalWindow
	 */
	public CartPage verifySaveCartCtaInModalWindow() {
		try {
			Assert.assertTrue(saveButtonOnSaveCart.isDisplayed(), "Save cart Cta is not enabled and not dispalying");
			Reporter.log("Save cart Cta is  enabled and  dispalying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Cart cta enable/Not displaying");
		}
		return this;
	}

	/**
	 * click Save cart CTA in ModalWindow
	 */
	public CartPage clickSaveMyCartOnlineCtaInModelWindow() {
		try {
			waitFor(ExpectedConditions.visibilityOf(saveMyCartOnlineInModalWindow), 60);
			saveMyCartOnlineInModalWindow.click();
			Reporter.log("Save cart CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Save cart CTA");
		}
		return this;
	}

	/**
	 * verify save cart model window
	 * 
	 */
	public CartPage verifySaveCartModelWindow() {

		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(saveCartModelWindow), "Save Cart model window not displayed");
			Reporter.log("Save Cart model window displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify save cart model");
		}
		return this;
	}

	/**
	 * verify confirmation message
	 */
	public CartPage verifyConfirmationMessage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(confirmationMessage), 60);
			Assert.assertTrue(confirmationMessage.isDisplayed(), "Confirmation message is not displayed");
			Reporter.log("Confirmation message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify confirmation message");
		}
		return this;
	}

	/**
	 * verify 'Let's talk. 1-800-TMobile' message
	 */
	public CartPage verifyLetsTalkMessage() {
		try {
			Assert.assertTrue(letsTalkMessage.isDisplayed(), "Let's talk. 1-800-TMobile message is not displayed");
			Reporter.log("Let's talk. 1-800-TMobile message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Let's talk. 1-800-TMobile message");
		}
		return this;
	}

	/**
	 * verify TFN number
	 */
	public CartPage verifyTFNnumber() {
		try {
			checkPageIsReady();
			Assert.assertTrue(letsTalkMessage.getAttribute("href").contains("tel:"), "TFN number not displayed");
			Reporter.log("TFN Number is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify TFN number");
		}
		return this;
	}

	/**
	 * verify TFN functionality turn on
	 */
	public CartPage VerifyTFNFunctionalityTurnOn() {
		try {
			Assert.assertTrue(tfnFunctionalityDisplay.isDisplayed(), "TFN functionality not turned on");
			Reporter.log("TFN functionality turned on");
		} catch (Exception e) {
			Assert.fail("Failed to verify TFN functionality");
		}
		return this;
	}

	/**
	 * Validate Cross Icon
	 */
	public CartPage verifyCloseIconInModalWindow() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(closeOnRetailSaveCartModal.isDisplayed(), "Close Icon Cta is  not dispalying");
			Reporter.log("Close Icon Cta is  dispalying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Close Icon ");
		}
		return this;
	}

	/**
	 * Click Cross icon on Complete store modal
	 */
	public CartPage clickCloseIconInCompleteInStoreModalWindow() {
		try {
			waitForSpinnerInvisibility();
			closeOnRetailSaveCartModal.click();
			Reporter.log("Clicked on Close icon");
		} catch (Exception e) {
			Assert.fail("Failed to click Close Icon ");
		}
		return this;
	}

	/**
	 * verify save cart override model window and click save -Save cart
	 */
	public CartPage verifySaveCartReplaceModalAndClickSaveForWebSaveCart() {
		try {
			waitForSpinnerInvisibility();
			if (isElementDisplayed(saveCartInReplaceYourCart)) {
				waitFor(ExpectedConditions.elementToBeClickable(saveCartInReplaceYourCart), 60);
				saveCartInReplaceYourCart.click();
			} else {
				Reporter.log("Save cart override modal Not displayed web save cart");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Cart Replace modal");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public CartPage verifyCartPageUrl() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains(pageUrl), "Cart Page not loaded");
		} catch (Exception e) {
			Assert.fail("Failed to verify cart URL");
		}
		return this;
	}

	/**
	 * Verify Empty Cart
	 *
	 */
	public CartPage verifyEmptyCart() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(emptyCart), "Empty Cart is not displayed");
			Reporter.log("Empty Cart  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Empty cart Page");
		}
		return this;
	}

	/**
	 * Verify edit Device By changing color
	 *
	 */
	public CartPage verifyeditDeviceBychangingcolor(String oldDeviceColor, String newDeviceColor) {
		try {
			Assert.assertNotEquals(oldDeviceColor, newDeviceColor, "Edit device is not working by changing the color");
			Reporter.log("Edit device is working by changing the color of device");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit CTA for Line on cart");
		}
		return this;
	}

	/**
	 * Verify Selected store locator name
	 */
	public CartPage verifySelectedStoreLocatorName(String storeLocatorName) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(storeLocator.get(0)), 60);
			String displayedStoreLocator = storeLocator.get(0).getText();
			assertTrue(displayedStoreLocator.equals(storeLocatorName),
					"Store locator name is not updated on Cart page");
			Reporter.log("Selected Store locator name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Selected Store locator name.");
		}
		return this;
	}

	/**
	 * Click on Cart continue button
	 *
	 */
	public CartPage clickOnCartContinueBtn() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.elementToBeClickable(continueBtnMobile), 60);
				continueBtnMobile.click();
			} else {
				waitFor(ExpectedConditions.elementToBeClickable(continueBtn), 60);
				clickElementWithJavaScript(continueBtn);
			}
			Reporter.log("Click on Cart continue button");
		} catch (Exception e) {
			Assert.fail("Failed to verify continue CTA on Cart page");
		}
		return this;
	}

	/**
	 * Verify Cart continue button is disabled
	 *
	 */
	public CartPage verifyCartContinueBtnisDisabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(continueBtn.getAttribute("disabled").equals("true"),
					"Cart continue button is enabled if only wearable on cart");
			Reporter.log("Cart continue button is disabled when only wearable in the cart");
		} catch (Exception e) {
			Assert.fail("Failed to verify Cart Continue Btn is Disabled");
		}
		return this;
	}

	/**
	 * Verify Cart continue button is enabled
	 *
	 */
	public CartPage verifyCartContinueBtnisEnabled() {
		try {
			checkPageIsReady();
			Assert.assertTrue(continueBtn.getAttribute("aria-disabled").contains("false"),
					"Cart continue button is disabled ");
			Reporter.log("Cart continue button is enabled ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Cart Continue Btn is enabled");
		}
		return this;
	}

	/**
	 * Click on Cart continue button
	 *
	 */
	public CartPage clickOnSaveCart() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(myCartHeader);
				clickElementWithJavaScript(saveCart);
				Reporter.log("Click on Save Cart Button");
			} else {
				saveCart.click();
				Reporter.log("Click on Save Cart Button");
			}

		} catch (Exception e) {
			Assert.fail("Save Cart POPUP is not displayed");
		}
		return this;
	}

	/**
	 * validate Save Cart sub Header
	 *
	 */
	public CartPage verifySaveCartHeader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(saveCartPopupheader.isDisplayed(), "Save Cart Header not displayed");
			Reporter.log("Save Cart Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Cart Header");
		}
		return this;
	}

	/**
	 * validate Save Cart sub Header
	 *
	 */
	public CartPage verifySaveCartSubHeader() {
		try {
			Assert.assertTrue(saveCartPopupSubheader.isDisplayed(), "Save Cart sub Header not displayed");
			Reporter.log("Save Cart sub Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to veridy Save Cart sub Header");
		}
		return this;
	}

	/**
	 * Enter valid email in Save cart modal popup
	 *
	 */
	public CartPage enterEmail(TMNGData tMNGData) {
		try {
			sendTextData(emailField, tMNGData.getEmail());
			emailField.sendKeys(Keys.TAB);
			Reporter.log("Email has been entered");
		} catch (Exception e) {
			Assert.fail("Falied to enter email in email field");
		}
		return this;
	}

	/**
	 * Enter Invalid Email in SaveCart
	 *
	 */
	public CartPage enterInvalidEmailinSaveCart(TMNGData tMNGData) {
		try {
			sendTextData(emailField, tMNGData.getInvalidEmail());
			emailField.sendKeys(Keys.TAB);
			Reporter.log("Invaled Email has been entered");
		} catch (Exception e) {
			Assert.fail("Falied to enter email in email field");
		}
		return this;
	}

	/**
	 * Verify Error Message For email field
	 *
	 */
	public CartPage verifyErrorMessageForEmailField() {
		try {
			Assert.assertTrue(isElementDisplayed(emailFieldErrorMessage),
					"Error message is not displyed for First Name filed");
			Reporter.log("Verified Error Message For email field");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For email field");
		}
		return this;
	}

	/**
	 * validate Save Cart
	 *
	 */
	public CartPage clickSaveButtonOnSaveCartModel() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(saveButtonOnSaveCart), 60);
			saveButtonOnSaveCart.click();
			Reporter.log("Save button on Save Cart is enabled to Click");
		} catch (Exception e) {
			Assert.fail("Save button on Save Cart is disabled to Click");
		}
		return this;
	}

	/**
	 * validate changeStoreLocation
	 *
	 */
	public CartPage verifyFindNearbyStoresIsDisplayed() {
		try {
			Assert.assertTrue(findNearbyStoresForDeviceLink.get(0).isDisplayed(),
					"Change Store Location is not Displayed.");
			Reporter.log("Change Store Location is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Change Store Location.");
		}
		return this;
	}

	/**
	 * To get Store name from cart page
	 *
	 */
	public String getStoreName() {
		String storeName = null;
		try {
			moveToElement(tradeDeviceBtnForEachLine.get(0));
			waitFor(ExpectedConditions.visibilityOf(storeLocator.get(0)), 60);
			for (WebElement store : storeLocator) {
				moveToElement(store);
				storeName = store.getText();
				Reporter.log("Store name is saved");
				break;
			}
		} catch (Exception e) {
			Assert.fail("Failed to get Store name");
		}
		return storeName;
	}

	/**
	 * Click on change store locator
	 *
	 */
	public CartPage clickChangeStoreLocation() {
		try {
			waitForSpinnerInvisibility();
			moveToElement(customerCartContainerHeaderOncart);
			for (WebElement store : findNearbyStoresForDeviceLink) {
				store.click();
				Reporter.log("Clicked on change Store locator cta");
				break;
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on change Store locator cta");
		}
		return this;
	}

	/**
	 * Click on change store locator for Accessories
	 *
	 */
	public CartPage clickChangeStoreLocationForAccessory() {
		try {
			moveToElement(customerCartContainerHeaderOncart);
			for (WebElement store : findNearbyStoresForAccessoryLink) {
				moveToElement(store);
				store.click();
				Reporter.log("Clicked on change Store locator cta");
				break;
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on change Store locator cta");
		}
		return this;
	}

	/**
	 * Close on Save Cart Model
	 *
	 */
	public CartPage clickCloseOnSaveCartModel() {
		try {
			waitForSpinnerInvisibility();
			closeOnSaveCartModel.click();
			Reporter.log("closed Save Card model.");
		} catch (Exception e) {
			Assert.fail("close on save cart is not displayed to click.");
		}
		return this;
	}

	/**
	 * click store locator name
	 */
	public CartPage clickStoreLocatorName() {

		try {
			moveToElement(customerCartContainerHeaderOncart);
			Assert.assertTrue(storeLocator.get(0).isDisplayed(), "Store locator name is not displayed");
			storeLocator.get(0).click();
			Reporter.log("Store locator name is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Store locator name.");
		}
		return this;
	}

	/**
	 * Verify Distane range is displayed
	 *
	 */
	public CartPage verifyDistanceToStore() {
		try {
			Assert.assertTrue(distanceToStore.get(0).isDisplayed(), "Distance to the Store is not displayed");
			Reporter.log("Failed to verify Distance to the store");
		} catch (Exception e) {
			Assert.fail("Failed to verify Distance to the store");
		}
		return this;
	}

	/**
	 * Verify store locator name
	 *
	 */
	public CartPage verifyAllProductsStoreName() {
		try {
			String store = null;
			String otherStore = null;
			waitForSpinnerInvisibility();
			for (int i = 0; i < storeLocator.size(); i++) {
				Assert.assertTrue(storeLocator.get(i).isDisplayed(), "Store locator is not displayed for device: " + i);
				store = storeLocator.get(i).getText();
			}
			if (storeLocator.size() > 2) {
				store = storeLocator.get(0).getText();
				otherStore = storeLocator.get(1).getText();
				Assert.assertTrue(store.contains(otherStore), "Store names are different");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Store name on SLP");
		}
		return this;
	}

	/**
	 * Click on Trade Device button
	 *
	 */
	public CartPage clickOnTradeDeviceCTA() {
		try {
			checkPageIsReady();
			moveToElement(addAPhoneLinkOnCartEssentials);
			waitFor(ExpectedConditions.visibilityOf(tradeDeviceBtnForEachLine.get(0)), 60);
			clickElementWithJavaScript(tradeDeviceBtnForEachLine.get(0));
			Reporter.log("Clicked on Trade Device button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Trade Device button.");
		}
		return this;
	}

	/**
	 * Verify Trade In Modal is displayed
	 *
	 */
	public CartPage verifyTradeInModalDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(tradeInModal), "Trade In Modal is not displayed");
			Reporter.log("Trade In Modal is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade In Modal.");
		}
		return this;
	}

	/**
	 * Click on Good Condition button
	 *
	 */
	public CartPage clickOnGoodRadioBtn() {
		try {
			waitForSpinnerInvisibility();
			goodConditionRadioBtn.click();
			Assert.assertTrue(goodConditionRadioBtn.isSelected(), "Good radio button not selected");
			Reporter.log("Clicked on Good Condition button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Good Condition button.");
		}
		return this;
	}

	/**
	 * Click on Get Estimated button
	 *
	 */
	public CartPage clickOnGetEstimatedBtn() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.elementToBeClickable(getEstimatedCTA), 60);
			clickElementWithJavaScript(getEstimatedCTA);
			Reporter.log("Clicked on Get Estimated  button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Get Estimated  button.");
		}
		return this;
	}

	/**
	 * Click on Get Estimated button
	 *
	 */
	public CartPage clickOnAgreeCTABtn() {
		try {
			moveToElement(agreeCTA);
			waitFor(ExpectedConditions.visibilityOf(agreeCTA), 60);
			agreeCTA.click();
			Reporter.log("Clicked on Agree button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Get Agree button.");
		}
		return this;
	}

	/**
	 * Verify Select your options Modal is displayed
	 *
	 */
	public CartPage verifyselectYourOptionsModalDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(selectYourOptionsModal), "'Select Your Option' modal is not visible");
			Reporter.log("Select your options Modal is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Select your options Modals.");
		}
		return this;
	}

	/**
	 * Click on select standard trade-in option button
	 *
	 */
	public CartPage clickOnStandardTradeInBtn() {
		try {
			standardTradeInRadioBtn.click();
			Reporter.log("Clicked on select standard trade-in option button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on select standard trade-in option button.");
		}
		return this;
	}

	/**
	 * Click on select Promotional trade-in option button
	 *
	 */
	public CartPage clickOnPromoTradeInBtn() {
		try {
			promoTradeInRadioBtn.click();
			Reporter.log("Clicked on Promotional trade-in option button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Promotional trade-in option button.");
		}
		return this;
	}

	/**
	 * Click on view order details link
	 *
	 */
	public CartPage clickOnViewOrderDetailsLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(viewOrderDetailsLinkMobile), 60);
				clickElementWithJavaScript(viewOrderDetailsLinkMobile);
			} else {
				waitFor(ExpectedConditions.visibilityOf(viewOrderDetailsLink), 60);
				// viewOrderDetailsLink.click();
				clickElementWithJavaScript(viewOrderDetailsLink);
				Reporter.log("Clicked on View order details link.");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Click on View order details link.");
		}
		return this;
	}

	/**
	 * Verify sim Starter Kit Text is Not displayed
	 *
	 */
	public CartPage verifySimStarterKitTextNotDisplayed() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertFalse(isElementDisplayed(simStarterKitText), "Sim Starter Kit Text is Displayed");
			Reporter.log("Sim Starter Kit Text is Not Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Sim Starter Kit Text.");
		}
		return this;
	}

	/**
	 * verify standard trade-in value displayed
	 *
	 */
	public CartPage verifyStandardTradeInDetailDisplayed() {
		try {
			checkPageIsReady();
			moveToElement(deviceNameInOrderDetail);
			waitFor(ExpectedConditions.visibilityOf(standardTradeInDetail), 60);
			Assert.assertTrue(isElementDisplayed(standardTradeInDetail), "Standard trade in details is not displayed");
			Reporter.log("Standard trade-in value is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Standard trade-in value.");
		}
		return this;
	}

	/**
	 * verify standard trade-in value displayed
	 *
	 */
	public CartPage verifyPromoTradeInDetailDisplayedonOrderDetails() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(promoTradeinDetailsonOrderDetail), 60);
			Assert.assertTrue(isElementDisplayed(promoTradeinDetailsonOrderDetail),
					"Promo trade in details is not displayed");
			Reporter.log("Promo trade-in value is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Promo trade-in details.");
		}
		return this;
	}

	/**
	 * verify Promo trade-in value displayed on Order Detail modal
	 *
	 */
	public CartPage verifyPromoTradeinDetailsDisplayedOnCart() {
		try {
			waitForSpinnerInvisibility();
			moveToElement(deviceNameInOrderDetail);
			waitFor(ExpectedConditions.visibilityOf(promoTradeInDetail), 60);
			Assert.assertTrue(isElementDisplayed(promoTradeInDetail), "Promo trade in details is not displayed");
			Reporter.log("Promo trade-in value is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Standard trade-in value.");
		}
		return this;
	}

	/**
	 * Verify Inventory Status on Cart Page
	 *
	 */
	public CartPage verifyInventoryStatusOnCartPage() {
		try {
			waitForSpinnerInvisibility();
			scrollDownToelement(deviceLabel);
			waitFor(ExpectedConditions.visibilityOf(inventoryStatusOnCartPage), 60);
			Assert.assertTrue(inventoryStatusOnCartPage.isDisplayed(), "Inventory status is not Dispalyed");
			Reporter.log("Inventory status is Dispalyed");
		} catch (Exception e) {
			Assert.fail("Failed to display Inventory status on Cart page");
		}
		return this;
	}

	/**
	 * Verify Change Store Locator CTA on Cart Page
	 *
	 */
	public CartPage verifyChangeStoreLocatorCTAOnCartPage() {
		try {
			waitForSpinnerInvisibility();
			for (WebElement webElement : findNearbyStoresForAccessoryLink) {
				if (webElement.isDisplayed()) {
					Assert.assertTrue(webElement.isDisplayed(), "Change Store Locator CTA is not Dispalyed");
					Reporter.log("Change Store Locator CTA is Dispalyed");
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Change Store Locator CTA");
		}
		return this;
	}

	/**
	 * Verify that click on duplicate button by N times
	 */
	public CartPage clickOnDuplicateNTimes(int n) {
		try {
			checkPageIsReady();
			int i;
			for (i = 0; i < n; i++) {
				clickDuplicateCTA();
				verifyCartPageLoaded();
			}
			Reporter.log("Duplicate button able to click multiple times");
		} catch (Exception e) {
			Assert.fail("Duplicate button not able to click by multiple times");
		}
		return this;
	}

	/**
	 * To verify Confirmation - "You're all Set" on Save Your Cart Model
	 *
	 */
	public CartPage verifyConfirmationMessageonSaveCartModel() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(confirmationMessageOnSaveCartModel), 60);
			Assert.assertTrue(isElementDisplayed(confirmationMessageOnSaveCartModel),
					"Confirmation message on Save Cart model is not displayed");
			Reporter.log("Confirmation message on Save Cart model is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Display Confirmation message on Save Cart model");
		}
		return this;
	}

	/**
	 * To verify Cart Icon on Save Cart Model
	 *
	 */
	public CartPage verifyCartIconOnSaveCartModel() {
		try {
			Assert.assertTrue(isElementDisplayed(cartImageonSaveCartModel),
					"Cart Icon on Save Cart model is not displayed");
			Reporter.log("Cart Icon on Save Cart model is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Display Cart Icon on Save Cart model");
		}
		return this;
	}

	/**
	 * validate Stores in Save catr modal popup Window
	 *
	 */
	public CartPage verifyStoresInSaveCartModalPopup() {
		try {
			Assert.assertTrue(storeInSavecartModalPopup.isDisplayed(), "Stores are not displaying in save cart");
			Reporter.log("Stores are displaying in Save cart");
		} catch (Exception e) {
			Assert.fail("Failed to veirfy Store in Save cart modal.");
		}
		return this;
	}

	/**
	 * verify Trade-in Estimated text on Select Your options modal
	 *
	 */
	public CartPage verifyEstimatedTradeinValueOnSelectYourOptionsModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(estimatedTradeinValueTextSelectYourOptionModal), 60);
			String estAmountText = estimatedTradeinValueTextSelectYourOptionModal.getText();
			assertTrue(estAmountText.equals("Your estimated value:"), "Trade-in value text is not displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify estimated Trade-in value text");
		}
		return this;
	}

	/**
	 * verify Trade-in Estimated amount on Select Your options modal
	 *
	 */
	public String verifyEstimatedTradeinValueAmountOnSelectYourOptionsModal() {
		String estAmount = null;
		try {
			estAmount = estimatedTradeinValueAmountSelectYourOptionModal.getText();
			assertTrue(estAmount.contains("$"), "Trade-in value amount is not displayed");
			Reporter.log("Trade-in value amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify estimated Trade-in value");
		}
		return estAmount;
	}

	/**
	 * verify Trade-in Value on Cart Page as compared with Select Your Options Modal
	 *
	 */
	public CartPage verifyTradeinValueOnCartPage(String tradeinValue) {
		try {
			String displayTradeinValue = tradeinValueOnCart.getText();
			assertTrue(displayTradeinValue.contains(tradeinValue), "Trade-in value is not correctly displayed on Cart");
			Reporter.log("Trade-in value on Cart page is displayed correctly");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade-in value on Cart page");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner is displayed
	 *
	 */
	public CartPage verifyStickyBannerDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.elementToBeClickable(viewOrderDetailsLinkMobile), 60);
				Assert.assertTrue(cartPageStickyBannerMobile.isDisplayed(), "Sticky banner is not displayed");
			} else {
				waitFor(ExpectedConditions.elementToBeClickable(viewOrderDetailsLink), 60);
				Assert.assertTrue(cartpageStickyBanner.isDisplayed(), "Sticky banner is not displayed");
			}
			Reporter.log("Sticky banner is  displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify sticky banner.");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner monthly total price option is displayed
	 *
	 */
	public CartPage verifyStickyBannerMonthlyTotalPriceDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(cartpagestickybannerMonthlyTotalPriceMobile.isDisplayed(),
						"sticky banner monthly total price option is not Displayed");
			} else {
				Assert.assertTrue(cartpagestickybannerMonthlyTotalPrice.isDisplayed(),
						"sticky banner monthly total price option is not Displayed");
			}
			Reporter.log("Sticky banner monthly total price option  is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify sticky banner monthly total price option .");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner total price option is displayed
	 *
	 */
	public CartPage verifyStickyBannerTodayTotalPriceDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(cartpagestickybannerTotalPriceMobile.isDisplayed(),
						"sticky banner total price option is not Displayed");
			} else {
				Assert.assertTrue(cartStickyBannerTodayTotalPrice.isDisplayed(),
						"sticky banner total price option is not Displayed");
			}
			Reporter.log("Sticky banner total price option  is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify sticky banner total price option .");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner EstimatedShippingText is displayed
	 *
	 */
	public CartPage verifyStickyBannerEstimatedShippingTextDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(cartpagestickybannerEstimatedShippingTextMobile.isDisplayed(),
						"sticky banner EstimatedShippingTextis Displayed");
				Reporter.log("Sticky banner EstimatedShippingText  is Not Displayed For Mobile");
			} else {
				Assert.assertTrue(cartpagestickybannerEstimatedShippingText.isDisplayed(),
						"sticky banner EstimatedShippingTextis not Displayed");
				Reporter.log("Sticky banner EstimatedShippingText  is Displayed.");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify sticky banner EstimatedShippingText .");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner EstimatedShipping date is displayed
	 *
	 */
	public CartPage verifyStickyBannerEstimatedShippingDateDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(cartpagestickybannerEstimatedDateMobile.isDisplayed(),
						"sticky banner  EstimatedShipping  date is Displayed");
				Reporter.log("Sticky banner  EstimatedShipping date  Not Displayed For Mobile");
			} else {
				Assert.assertTrue(cartpagestickybannerEstimatedDate.isDisplayed(),
						"sticky banner  EstimatedShipping date is not Displayed");
				Reporter.log("Sticky banner  EstimatedShipping date  is Displayed.");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify sticky  EstimatedShipping from date.");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner Order details displayed
	 *
	 */
	public CartPage verifyStickyBannerOrderDetailsLinkDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(viewOrderDetailsLinkMobile.isDisplayed(),
						"Sticky banner  Orderdetails is not Displayed");
			} else {
				Assert.assertTrue(viewOrderDetailsLink.isDisplayed(), "Sticky banner  Orderdetails is not Displayed");
			}
			Reporter.log("Sticky banner  Orderdetails is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify sticky banner Orderdetails");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner EstimatedShipping To date is displayed
	 *
	 */
	public CartPage clickStickyBannerOrderDetailsLink() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(viewOrderDetailsLinkMobile), 60);
				viewOrderDetailsLinkMobile.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(viewOrderDetailsLink), 60);
				viewOrderDetailsLink.click();
			}
			Reporter.log("Sticky banner  OrderDetails able to click");
		} catch (Exception e) {
			Assert.fail("Failed to click sticky banner OrderDetails");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner order details model is displayed
	 *
	 */
	public CartPage verifyViewOrderDetailsModel() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(cartpagestickybannerOrderDetailsModel), 60);
			Assert.assertTrue(cartpagestickybannerOrderDetailsModel.isDisplayed(),
					"sticky banner  order details model is not Displayed");
			Reporter.log("Sticky banner order details model is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify sticky banner order details model");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner Order details displayed
	 *
	 */
	public CartPage verifyCompleteInStoreLinkDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(completeInStoreMobileOutside.isDisplayed(),
						"sticky banner Outside  CompleteInStore is not Displayed");
				Reporter.log("'Complete In Store' link is Displayed for Mobile view.");
			} else {
				Assert.assertTrue(comepleteInStoreLinkStickyBanner.isDisplayed(),
						"sticky banner  CompleteInStore is not Displayed");
				Reporter.log("'Complete In Store' link is displayed in Sticky Banner.");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Complete In Store' link");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner EstimatedShipping To date is displayed
	 *
	 */
	public CartPage clickCompleteInStoreLink() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(completeInStoreMobileOutside), 60);
				completeInStoreMobileOutside.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(comepleteInStoreLinkStickyBanner), 60);
				clickElementWithJavaScript(comepleteInStoreLinkStickyBanner);
			}
			Reporter.log("Clicked on 'Complete In Store' link");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Complete In Store' link");
		}
		return this;
	}

	/**
	 * Verify sticky Starter banner tax and shipping text option is displayed
	 *
	 */
	public CartPage verifyStickyBannerTaxAndShippingTextDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(cartpagestickybannerTaxAndShippingTextMobile.isDisplayed(),
						"sticky bannermonthly tax and shipping text option is not Displayed");
			} else {
				Assert.assertTrue(cartpagestickybannerTaxAndShippingText.isDisplayed(),
						"sticky bannermonthly tax and shipping text option is not Displayed");
			}
			Reporter.log("Sticky banner monthly tax and shipping text option is  displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify tax and shipping text option .");
		}
		return this;
	}

	/**
	 * Get Total Today price at Order Detail page
	 */
	public Double getTotalTodaypriceAtOderDetailPage() {
		String todayprice = null;
		try {
			todayprice = todayTotalPriceAtOrderDetailPageForFirstDevice.getAttribute("left-amount");
			Reporter.log("Today Price in Order detail page is " + todayprice);
		} catch (Exception e) {
			Assert.fail("Failed to get Today total amount into Double.");
		}

		return Double.parseDouble(todayprice);
	}

	/**
	 * Get Today Captured price at Order Detail page
	 */
	public Double getTodayTotalCapturedPriceOnOrderDetailedPage() {
		String todayprice = "";
		try {
			todayprice = todayTotalPriceonOrderDetailPage.getAttribute("left-amount");
			Reporter.log("Today Price in Order detail page is " + todayprice);
		} catch (Exception e) {
			Assert.fail("Failed to get today total amount on View Order Detail page");
		}
		return Double.parseDouble(todayprice);
	}

	/**
	 * Get Monthly Captured price at Order Detail page
	 */
	public Double getMonthlyCapturedPriceInOrderDetailPage() {
		String monthlyprice = "";
		try {
			monthlyprice = totalPriceAtOrderDetailPage.getAttribute("right-amount");
			Reporter.log("Monthly Price in Order detail page is " + monthlyprice);
		} catch (Exception e) {
			Assert.fail("Failed to get Monthly amount at order detail page.");
		}

		return Double.parseDouble(monthlyprice);
	}

	/**
	 * Get Monthly price at Cart page
	 */
	public CartPage verifyMonthlyPriceIsCorrectForBYOD() {
		try {
			String monthlyprice;
			for (int i = 0; i < monthlyLinePriceCartPage.size(); i++) {
				monthlyprice = monthlyLinePriceCartPage.get(i).getAttribute("right-amount");
				if (i == 0) {
					Assert.assertTrue(monthlyprice.equals("70.00"), "Price of first line is not = $70 in Cart Page");
				}
				if (i == 1) {
					Assert.assertTrue(monthlyprice.equals("50.00"), "Price of second line is not = $50 in Cart Page");
				}
				if (i > 1) {
					Assert.assertTrue(monthlyprice.equals("20.00"),
							"Price of " + i + " line is not = $20 in Cart Page");
				}
			}
			Reporter.log("Plan price for each line is correct in Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly prices for services in Cart Page");
		}
		return this;
	}

	/**
	 * Get Today Down Payment Integer format
	 */
	public Double getTodayDownPaymentPriceAtOderDetailPage() {
		String todayDownPayment = "";
		try {
			todayDownPayment = todayDownPaymentPriceInOrderDetailPage.get(0).getAttribute("left-amount");
			Reporter.log("Today Down Payment at Order Detail page is " + todayDownPayment);
		} catch (Exception e) {
			Assert.fail("Failed to get Today down payment price in order detailed page");
		}
		return Double.parseDouble(todayDownPayment);
	}

	/**
	 * Verify that Today price = (EIP Downpayment + SSK price)'
	 */
	public CartPage compareEIPDownpaymentAndSSKprice(Double firstValue, Double secondValue) {
		try {
			Double roundedFirstValue = (double) (Math.round((firstValue) * 100) / 100);
			Double roundedsecondValue = (double) (Math.round((secondValue) * 100) / 100);
			Assert.assertEquals(roundedFirstValue, roundedsecondValue,
					"Today price is not matched with Downpayment price.");
			Reporter.log("Total Today price is matched with Downpayment price.");
		} catch (Exception e) {
			Assert.fail("Failed to compare Total Today price with Downpayment price.");
		}
		return this;
	}

	/**
	 * Click on Close Icon At Order Detail Page
	 *
	 */
	public CartPage clickOnCloseIconAtOrderDetailPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(orderDetailCloseIcon), 60);
			orderDetailCloseIcon.click();
			Reporter.log("Clicked on Close Icon At Order Detail Page.");
		} catch (Exception e) {
			Assert.fail("Failed to click on Close Icon At Order Detail Page.");
		}
		return this;
	}

	/**
	 * Compare Price of Accessory in PDP and Cart
	 *
	 */
	public CartPage comparePricesOfAccessoryInPDPandCartPage(String priceOnPDP, String priceOnCart) {
		try {
			Assert.assertEquals(priceOnPDP, priceOnCart, "Price of accessory is not matching in  PDP and Cart page.");
			Reporter.log("Price of accessory is matching in  PDP and Cart page.");
		} catch (Exception e) {
			Assert.fail("Failed to compare price Accessory in PDP and Cart pages");
		}
		return this;
	}

	/**
	 * Get Monthly Captured price at Order Detail page
	 */
	public Double getMonthlyCapturedPriceForAccessory() {
		String monthlyprice = null;
		try {
			monthlyprice = todayTotalPriceAtOrderDetailPageForFirstDevice.getAttribute("right-amount");
			Reporter.log("Monthly Price in Order detail page is " + monthlyprice);
		} catch (Exception e) {
			Assert.fail("Failed to Get Monthly Captured price at Order Detail page");
		}
		return Double.parseDouble(monthlyprice);
	}

	/**
	 * Click on see All Protection Options Link
	 *
	 */
	public CartPage enterYourZIPCode(String zipCode) {
		try {
			checkPageIsReady();
			zipcodeTextBox.clear();
			zipcodeTextBox.sendKeys(zipCode);
			Reporter.log("Zipcode: '" + zipCode + "' is entered");
		} catch (Exception e) {
			Assert.fail("Failed to enter zipcode");
		}
		return this;
	}

	/**
	 * Click on next button
	 *
	 */
	public CartPage clickOnNextBtn() {
		try {
			waitforSpinner();
			enterYourzipcodeNextBtn.click();
			Reporter.log("Clicked on Next button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Next button");
		}
		return this;
	}

	/**
	 * Verify Select services overlay
	 *
	 */
	public CartPage verifySelectServicesOverlay() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(orderDetailsModal), "Select service modal is not displayed");
			Reporter.log("Select service overlay is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Select service modal");
		}
		return this;
	}

	/**
	 * verify total finance amount
	 *
	 */

	public CartPage verifyTotalFinanceAmount() {
		try {
			Assert.assertTrue(totalFinanceAmount.isDisplayed(), "Total finance amount  is not displayed");
			Assert.assertTrue(totalFinanceAmountText.isDisplayed(), "Total finance amount text is not displayed");
			Reporter.log("Total finance amount text and amount are displayed");
		} catch (Exception e) {
			Assert.fail("Total finance amount text and amount are not displayed ");
		}
		return this;
	}

	/**
	 * verify total price amount for EIP on OrderDetailed pop up
	 *
	 */

	public CartPage verifyTotalPriceOnOrderDetailedModal(String frp) {
		try {
			Assert.assertTrue(totalPriceOnOrderDetailedModal.isDisplayed(), "Total Price amount  is not displayed");
			Assert.assertTrue(
					totalPriceOnOrderDetailedModal.getAttribute("left-amount").contains(frp.replaceAll(",", "")),
					"Total Price is not matched with FRP");
			Reporter.log("Total price amount is displayed and amount is matched with Device FRP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Total price amount on Order detailed pop up.");
		}
		return this;
	}

	/**
	 * Total finance amount valuation for EIP
	 *
	 */
	public CartPage financeAmountValuationForEIP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(totalEIPFinanceAmount), 60);
			String totalEIPmonthsString = totalEIPFinanceAmount.getAttribute("left-text");
			String amountPerMonthString = totalEIPFinanceAmount.getAttribute("left-amount");
			String totalFinancedAmountDisplayedStr = totalFinanceAmount.getText();
			totalEIPmonthsString = totalEIPmonthsString.substring(totalEIPmonthsString.lastIndexOf("X") + 2,
					totalEIPmonthsString.lastIndexOf("m") - 1);
			String eipMonthsArray[] = totalEIPmonthsString.split("x");
			String eipMonthsStr = eipMonthsArray[1];
			double eipMonths = Double.parseDouble(eipMonthsStr.trim());
			double eipAmountPerMonth = Double.parseDouble(amountPerMonthString.trim());
			double totalFinancedAmountDisplayed = Double.parseDouble(totalFinancedAmountDisplayedStr);
			double totalFinacedAmountCalilulated = (double) (Math.round((eipMonths * eipAmountPerMonth) * 100) / 100);
			Assert.assertEquals(totalFinacedAmountCalilulated, totalFinancedAmountDisplayed,
					"Total finance amount not displayed");
		} catch (AssertionError e) {
			Reporter.log("Total finance amount not displayed");
			Assert.fail("Failed to verify Total Finacial amount for EIP");
		}
		return this;
	}

	/**
	 * verify total finance amount
	 *
	 */
	public CartPage verifyTotalFinanceAmountForFRP() {
		try {
			scrollDownToelement(totalFinanceTextOnOrderdetailModal);
			Assert.assertFalse(isElementDisplayed(totalEIPFinanceAmount), "Total finance amount displayed");
		} catch (AssertionError e) {
			Reporter.log("Total finance amount displayed");
			Assert.fail("Total finance amount displayed ");
		}
		return this;
	}

	/**
	 * validate Disabled Save Cart CTA on Save Cart Modal
	 *
	 */
	public CartPage verifyDisabledSaveCTAOnSaveCartModel() {
		try {
			Assert.assertFalse(saveMyCartOnlineInModalWindow.isEnabled(), "Save CTA is Enabled");
			Reporter.log("Save CTA not Enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verifySave CTA");
		}
		return this;

	}

	/**
	 * Verify 'Find Nearby Store' for accessory
	 */
	public CartPage verifyFindNearbyStoresForAccessory() {
		try {
			if (findNearbyStoresForDeviceLink.size() == 0) {
				moveToElement(findNearbyStoresForAccessoryLink.get(0));
				Reporter.log("There is no device, so 'Find Nearby Store' should be present for accessory");
				Assert.assertTrue(findNearbyStoresForAccessoryLink.get(0).isDisplayed(),
						"'Find Nearby Store' is not displayed for accessory");
				Reporter.log("'Find Nearby Store' is displayed for accessory");
			} else {
				Reporter.log("'Find Nearby Store' is displayed for device, so it should NOT be present for accessory");
				Assert.assertFalse(findNearbyStoresForAccessoryLink.size() > 0,
						"'Find Nearby Store' is displayed for accessory");
				Reporter.log("'Find Nearby Store' is not displayed for accessory");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Find Nearby Store' for accessory on Cart page");
		}
		return this;
	}

	/**
	 * Click on show Items Link on Save Cart Confirmation Modal
	 */
	public CartPage clickShowItemsLinkOnSaveCartConfirmationModal() {
		try {
			checkPageIsReady();
			showItemsLinksOnSaveCartModal.click();
			Reporter.log("Clicked on show items link");
		} catch (Exception e) {
			Assert.fail("Failed to click on show items link");
		}
		return this;
	}

	/**
	 * Verify Accessory Name on Save Cart Confirmation MOdal
	 */
	public CartPage verifyAccessoryNameOnSavecartConfirmationModal() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(accessoryNameOnSaveCartModal), 60);
			Assert.assertTrue(accessoryNameOnSaveCartModal.isDisplayed(), "Accessory Name is not displaying");
			Reporter.log("Accessory Name  is displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory Name");
		}
		return this;
	}

	/**
	 * Verify Inventory Status on Save Cart COnfirmation Modal
	 */
	public CartPage verifyInventoryStatusOnSavecartConfirmationModal() {
		try {
			waitforSpinner();
			Assert.assertTrue(inventoryStatusOnSaveCartModal.isDisplayed(),
					"Inventory Status is not displaying on Save Cart Confirmation Modal");
			Reporter.log("Inventory Status is displaying on Save Cart Confirmation Modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify Inventory Status on Save Cart Confirmation Modal");
		}
		return this;
	}

	/**
	 * Verify store name
	 *
	 */
	public CartPage verifyStoreName() {
		try {
			scrollDownToelement(customerCartContainerHeaderOncart);
			assertTrue(storeLocator.get(0).isDisplayed(), "Store locator name is not displayed");
			Reporter.log("Store locator name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Store locator name.");
		}
		return this;
	}

	/**
	 * verify summary at order detailed page
	 */
	public String cartSummaryTextOnOrderDetailPage() {
		String text = null;
		try {
			checkPageIsReady();
			Assert.assertTrue(OrderDetailsSummary.isDisplayed(), "Summary is not displayed on OrderDetailed page");
			text = OrderDetailsSummary.getText();

		} catch (Exception e) {
			Assert.fail("Failed to verify Summary at order detailed page");
		}
		return text;
	}

	/**
	 * verify Cart summary
	 */
	public String cartSummaryTextOnCartPage() {
		String text = null;
		try {
			checkPageIsReady();
			scrollDownToelement(deviceLabel);
			Assert.assertTrue(CartDetailsSummary.isDisplayed(), "Summary is not displayed on cart page");
			text = CartDetailsSummary.getText();
		} catch (Exception e) {
			Assert.fail("Failed to verify Summary at cart page");
		}
		return text;
	}

	/**
	 * verify order cart device details
	 */
	public CartPage compareSummaryForAddedcart(String myCartSummary, String orderDetailsSummary) {
		try {
			assertTrue((myCartSummary.trim()).equals(orderDetailsSummary.trim()),
					"Added cart details summary is not matched");
		} catch (Exception e) {
			Assert.fail("Added cart details summary is not matched");
		}
		return this;
	}

	/**
	 * Verify Save Cart Cta on Cart
	 *
	 */

	public CartPage verifySaveCartCTAOnCart() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(saveCart), 60);
			assertTrue(saveCart.isDisplayed(), "Save Cart Link is not displaying on Cart");
			Reporter.log("Save Cart Link is  displaying on Cart");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Cart Link on Cart");
		}
		return this;
	}

	/**
	 * Verify Inventory Status for Accessory
	 *
	 */
	public CartPage verifyInventoryStatusForAccessory() {
		try {
			waitForSpinnerInvisibility();
			moveToElement(duplicateCTAForaccessory);
			String displayedMessage = inventoryStatusOnCartPage.getText();
			if (displayedMessage.equalsIgnoreCase("In stock")) {
				Assert.assertTrue(inventoryStatusOnCartPage.getText().equalsIgnoreCase("In stock"),
						"Store inventory message is not displayed");
				Reporter.log("Store inventory message:" + inventoryStatusOnCartPage.getText());
			} else if (displayedMessage.equalsIgnoreCase("Hurry, only a few left")) {
				Assert.assertTrue(inventoryStatusOnCartPage.getText().equalsIgnoreCase("Hurry, only a few left"),
						"Store inventory message is not displayed");
				Reporter.log("Store inventory message:" + inventoryStatusOnCartPage.getText());
			} else if (displayedMessage.equalsIgnoreCase("Contact store for availability")) {
				Assert.assertTrue(
						inventoryStatusOnCartPage.getText().equalsIgnoreCase("Contact store for availability"),
						"Store inventory message is not displayed");
				Reporter.log("Store inventory message:" + inventoryStatusOnCartPage.getText());
			} else {
				Reporter.log("Store inventory message:" + inventoryStatusOnCartPage.getText());
				Assert.fail("inventory status in-stock and  limited stock is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Store inventory message for accessory");
		}
		return this;
	}

	/**
	 * Get Device Loan Term length In Cart Page
	 */
	public String getDeviceLoanTermLengthInCartPage() {
		String loanTermLengthInCartPage = "";
		try {
			loanTermLengthInCartPage = totalEIPFinanceAmount.getAttribute("left-text").toLowerCase();
			loanTermLengthInCartPage = loanTermLengthInCartPage.replaceAll("[^\\d.]", "");
		} catch (Exception e) {
			Assert.fail("Failed to Get Device Loan Term length In Cart Page.");
		}

		return loanTermLengthInCartPage;
	}

	/**
	 * Verify Device loan term length
	 *
	 */
	public CartPage verifyDeviceLoanTermLength() {
		try {
			checkPageIsReady();
			Assert.assertTrue(totalEIPFinanceAmount.isDisplayed(), "Device loan term length not displayed.");
			Reporter.log("Device loan term length not is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to Device loan term length, Its not displayed");
		}
		return this;
	}

	/**
	 * Verify that Device loan term length are equal in PDP and CartPages.
	 */
	public CartPage compareDeviceLoanTermLengthSameForPDPAndCartPages(String loanTermLengthInPDP,
			String loanTermLengthInCartPage) {
		try {
			Assert.assertTrue(loanTermLengthInPDP.contains(loanTermLengthInCartPage),
					"PDP FRP price is not matched with PLP FRP price");
			Reporter.log("Device loan term lengths is matched with PDP and CartPages");
		} catch (Exception e) {
			Assert.fail("Failed to compare device loan term lengths is not matched with PDP and CartPages");
		}
		return this;
	}

	/**
	 * Verify Device Name In Order Details Model
	 */
	public CartPage verifyDeviceNameInOrderDetailsModel() {
		try {
			assertTrue(deviceNameInOrderDetail.isDisplayed(), " Device name is not displayed in Order details model");
			Reporter.log("Device name is displayed in Order details model");
		} catch (Exception e) {
			Assert.fail("Failed to display Device name in Orderdetails model");
		}
		return this;
	}

	/**
	 * Verify retrieve cta in cart page
	 */
	public CartPage verifyRetrieveCartOnCartPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(retrieveCTAOnCart), 60);
			assertTrue(retrieveCTAOnCart.isDisplayed(), " retrieve cta is not displayed in cart page");
			Reporter.log("Retrieve CTA is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Retrieve CTA  in cart page");
		}
		return this;
	}

	/**
	 * Click on retrieve CTA on cart page
	 *
	 */
	public CartPage clickRetrieveCartOnCartPage() {
		try {
			moveToElement(retrieveCTAOnCart);
			clickElementWithJavaScript(retrieveCTAOnCart);
			Reporter.log("Clicked on retrieve  CTA in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to click on retrieve CTA in cart page.");
		}
		return this;
	}

	/**
	 * Verify retrieve modal in cart page
	 */
	public CartPage verifyRetrieveModalForCartPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(retrieveModalOnCart), 60);
			assertTrue(retrieveModalOnCart.isDisplayed(), "Retrieve modal is not displayed in cart page");
			Reporter.log("Retrieve modal is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify retrieve cart modal in cart page");
		}
		return this;
	}

	/**
	 * Verify items on order details after retrieve cart
	 */
	public CartPage verifyRetrieveCartItemsOnOrderDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(itemsOnOrderDetailsForRetrieve), 60);
			assertTrue(itemsOnOrderDetailsForRetrieve.isDisplayed(), "Items not displayed in order details");
			Reporter.log(" items displayed in order details");
		} catch (Exception e) {
			Assert.fail("Failed to verify items in order details from retrive cart");
		}
		return this;
	}

	/**
	 * Verify strike through price displayed In Order Details Model
	 */
	public CartPage verifystrikeThroughPriceInOrderDetailsModel() {
		try {
			assertTrue(orderDetailsModalStrikeThroughPrice.isDisplayed(),
					"Strikethrough price is not displayed in Order details model");
			Reporter.log("Strikethrough price is displayed in Order details model");
		} catch (Exception e) {
			Assert.fail("Failed to verify Strikethrough price in Orderdetails model");
		}
		return this;
	}

	/**
	 * Verify strike through price displayed In Order Details Model
	 */
	public CartPage verifyOrderDetailsModalFRP() {
		try {
			assertTrue(orderDetailsModalFRP.get(5).isDisplayed(), "FRP price is not displayed in Order details model");
			Reporter.log("FRP price is displayed in Order details model");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP price in Orderdetails model");
		}
		return this;
	}

	/**
	 * Verify EIP price displayed In Order Details Modal
	 */
	public CartPage verifyOrderDetailsModalEIP() {
		try {
			assertTrue(orderDetailsModalEIP.isDisplayed(), "EIP price is not displayed in Order details model");
			Reporter.log("EIP price is displayed in Order details model");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP price in Orderdetails model");
		}
		return this;
	}

	/**
	 * Verify Instant discount displayed In Order Details Model
	 */
	public CartPage verifyOrderDetailsModalInstantDiscount() {
		try {
			assertTrue(orderDetailsModalInstanceDiscount.isDisplayed(),
					"Instance discount is not displayed in Order details model");
			Reporter.log("Instance discount is displayed in Order details model");
		} catch (Exception e) {
			Assert.fail("Failed to verify Instant discount in Orderdetails model");
		}
		return this;
	}

	/**
	 * Verify edit CTA on cart page for accessory
	 */
	public CartPage verifyEditCTAOnCartPageForAccessory() {
		try {
			waitFor(ExpectedConditions.visibilityOf(editCTAForAccessory.get(0)), 60);
			assertTrue(editCTAForAccessory.get(0).isDisplayed(), "accessory edit button is not displayed");
			Reporter.log("Edit accessory button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify edit button on cart page for accessory");
		}
		return this;
	}

	/**
	 * Click on edit CTA on cart page for accessory
	 *
	 */
	public CartPage clickEditCTAOnCartPageForAccessory() {
		try {
			moveToElement(customerCartContainerHeaderOncart);
			editCTAForAccessory.get(0).click();
			Reporter.log("Clicked on edit accessory button on cart page");
		} catch (Exception e) {
			Assert.fail("Failed to click on edit accessory button on cart page");
		}
		return this;
	}

	/**
	 * Click on edit CTA for accessory
	 *
	 */
	public CartPage clickEditButtonForAccessory() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(editAccessoryButton), 60);
			editAccessoryButton.click();
			Reporter.log("Clicked on edit  button for accessory");
		} catch (Exception e) {
			Assert.fail("Failed to click on edit  button for accessory");
		}
		return this;
	}

	/**
	 * Compare accessory color in mini pdp and cart
	 */
	public CartPage compareAccessoryColourForAccessory(String accessoryColourMiniPDP, String accessoryColourCart) {
		try {
			Assert.assertFalse(accessoryColourMiniPDP.equalsIgnoreCase(accessoryColourCart),
					"both colour values are same");
			Reporter.log("Colour values in cart and mini pdp are different");
		} catch (Exception e) {
			Assert.fail("Failed to compare color of accessory in Mini PDP and cart");
		}

		return this;
	}

	/**
	 * Verify EIP message
	 */
	public CartPage verifyEipMessage() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(topNotificationBanner), 60);
				Assert.assertTrue(topNotificationBanner.isDisplayed(),
						" EIP price message is not displayed in cart page");
			} else {
				waitFor(ExpectedConditions.visibilityOf(topNotificationBanner));
				Assert.assertTrue(topNotificationBanner.isDisplayed(),
						" EIP price message is not displayed in cart page");
				Assert.assertTrue(
						topNotificationBanner.getText().contains("accessories will be payable in monthly payments"),
						"Accessory EIP message is not correct");
			}
			Reporter.log("EIP price message is  displayed for accessory in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP message in cart page");
		}
		return this;
	}

	/**
	 * Verify SecondLine Today price
	 */
	public CartPage verifySecondLineTodayPrice(Double todayPriceInMiniPDP, Double simStarterKitPrice,
			Double totalTodayPriceForSecondLine) {
		Double totalTodayPrice;
		try {
			totalTodayPrice = todayPriceInMiniPDP + simStarterKitPrice;
			Assert.assertEquals(totalTodayPrice, totalTodayPriceForSecondLine, "Today prices are not matching");
		} catch (Exception e) {
			Assert.fail("Unable to verify total today prices on Crat Page");
		}
		return this;
	}

	/**
	 * Verify Second line Monthly price
	 * 
	 */
	public CartPage verifySecondLineMonthlyPrice(Double monthlyPriceInMiniPDP, Double tMobileOnePrice,
			Double totalMonthlyPriceForSecondLine) {
		Double totalMonthlyPrice;
		try {
			totalMonthlyPrice = monthlyPriceInMiniPDP + tMobileOnePrice - 20; // Magenta
																				// Plan
																				// will
																				// be
																				// 50
																				// for
																				// second
																				// line
			Assert.assertEquals(totalMonthlyPrice, totalMonthlyPriceForSecondLine, "Monthly prices are not matching");
		} catch (Exception e) {
			Assert.fail("Unable to verify total monthly prices on Crat Page");
		}
		return this;
	}

	/**
	 * Compare Accessory Price In Cart Page
	 */
	public CartPage compareAccessoryPriceInCartPage(String totalPriceInMiniPDP, String todayPrice) {
		try {
			Assert.assertEquals(totalPriceInMiniPDP, todayPrice, "Prices are not matching");
			Reporter.log("Accessory prices are matched");
		} catch (Exception e) {
			Assert.fail("Fail to mismatched accessory prices");
		}
		return this;
	}

	/**
	 * Verify Accessory Price is > 69
	 */
	public CartPage verifyAccessoryFrpPrice(String todayPriceText) {
		try {
			boolean isGraterThan69 = false;
			Double todayPrice = Double.parseDouble(todayPriceText);
			if (todayPrice > 69.0) {
				isGraterThan69 = true;
			}
			Assert.assertTrue(isGraterThan69, "Selected Accessory price is not grater than 69.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessoty price.");
		}
		return this;
	}

	/**
	 * Verify Accessory Price is < 69
	 */
	public CartPage verifyFrpAccessoryPrice(String todayPriceText) {
		try {
			boolean isLessThan69 = false;
			Double todayPrice = Double.parseDouble(todayPriceText);
			if (todayPrice < 69.0) {
				isLessThan69 = true;
			}
			Assert.assertTrue(isLessThan69, "Selected Accessory price is not less than 69.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessoty price.");
		}
		return this;
	}

	/**
	 * Get AutoPay discount price at Cart page
	 */
	public CartPage verifyAutoPayDiscountIsAppliedForEachLine() {
		try {
			for (WebElement autoPayDiscount : priceBreakdownAutoPaymentMessage) {
				Assert.assertTrue(autoPayDiscount.getText().contains("$5"),
						"AutoPay discount $5 is not applied for each line");
			}
			Reporter.log("$5 AutoPay discount is applied for each line");
		} catch (Exception e) {
			Assert.fail("Failed to verify AutoPay discount price at Cart page");
		}
		return this;
	}

	/**
	 * Verify Total AutoPay discount price is correct at Cart page
	 */
	public CartPage verifyTotalAutoPayDiscountIsCorrect() {
		try {
			waitForSpinnerInvisibility();
			String totalAutoPayText = stickeyBannertotalAutoPayDiscount.getText().substring(1);
			Integer totalAutoPay = Integer.parseInt(totalAutoPayText);
			Integer autoPaySumByLines = priceBreakdownAutoPaymentMessage.toArray().length * 5;
			Assert.assertTrue(totalAutoPay.equals(autoPaySumByLines),
					"Total AutoPay discount (from bottom of Cart Page) doesnt match summary of discounts");
			Reporter.log("AutoPay discount summary is matching total");
		} catch (Exception e) {
			Assert.fail("Failed to verify AutoPay discount price at Cart page");
		}
		return this;
	}

	/**
	 * Get Monthly price at Order Details page
	 */
	public CartPage verifyMonthlyPriceIsCorrectinOrderDetailsCartPage() {
		try {
			String monthlyprice;
			for (int i = 0; i < monthlyLinePriceOrderDetails.size() - 1; i++) {
				monthlyprice = monthlyLinePriceCartPage.get(i).getAttribute("right-amount");
				if (i == 0) {
					Assert.assertTrue(monthlyprice.equals("70.00"),
							"Price of first line is not = $70 in Order Details Cart Page");
				}
				if (i == 1) {
					Assert.assertTrue(monthlyprice.equals("50.00"),
							"Price of second line is not = $50 in Order Details Cart Page");
				}
				if (i > 1) {
					Assert.assertTrue(monthlyprice.equals("20.00"),
							"Price of " + i + " line is not = $20 in Order Details Cart Page");
				}
			}
			Reporter.log("Plan price for each line in Order Details pageis correct");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly prices for services in Order Details Cart Page");
		}
		return this;
	}

	/**
	 * Verify Total AutoPay discount price is correct at Cart page
	 */
	public CartPage verifyTotalAutoPayDiscountIsCorrectInOrderDetailCartPage() {
		try {
			String totalAutoPayText = autoPayTextForEachLineOrderDetailsCartPage
					.get(autoPayTextForEachLineOrderDetailsCartPage.size() - 1).getText().substring(1);
			Integer totalAutoPay = Integer.parseInt(totalAutoPayText);
			Integer autoPaySumByLines = (autoPayTextForEachLineOrderDetailsCartPage.toArray().length - 1) * 5;

			String totalAutoPayBannerText = totalAutoPayDiscountBannerOrderDetailsCartPage.getText();
			totalAutoPayBannerText = totalAutoPayBannerText.substring(totalAutoPayBannerText.lastIndexOf("$") + 1);
			Integer totalAutoPayBanner = Integer.parseInt(totalAutoPayBannerText);

			Assert.assertTrue(totalAutoPay.equals(autoPaySumByLines) && totalAutoPayBanner.equals(autoPaySumByLines),
					"Total AutoPay discount (from bottom of Order Details Cart Page) doesnt match summary of discounts");
			Reporter.log("AutoPay discount summary is matching total in Order Details Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify AutoPay discount price at Order Details Cart page");
		}
		return this;
	}

	/**
	 * Verify Device EIP price In Order Details Modal
	 */
	public CartPage verifyDeviceEipPriceInOrderDetails() {
		try {
			assertTrue(orderDetailsModalEIP.isDisplayed(), "Device EIP price is not displayed in Order details model");
			Reporter.log("Device EIP price is displayed in Order details model");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device EIP price in Orderdetails model");
		}
		return this;
	}

	/**
	 * Verify Accessory device EIP price In Order Details Modal
	 */
	public CartPage verifyAccessoryEipPriceInOrderDetails() {
		try {
			assertTrue(secondDeviceMonthlyPrice.get(0).isDisplayed(),
					"Accessory EIP price is not displayed in Order details model");
			Reporter.log("Accessory EIP price is displayed in Order details model");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory EIP price in Orderdetails model");
		}
		return this;
	}

	/**
	 * Compare Device Total today price in Order details
	 */
	public CartPage compareDevicePriceInOrderDetails(Double deviceTotalPrice, Double simStarterKitPrice,
			Double refundableDepositPrice, Double totalTodayPrice) {
		try {
			Assert.assertEquals(deviceTotalPrice + simStarterKitPrice + refundableDepositPrice, totalTodayPrice);
			Reporter.log("Device Total Today Prices are equal");
		} catch (Exception e) {
			Assert.fail("Failed to Total Today price in Order details ");
		}
		return this;
	}

	/**
	 * Click Retrieve cart button
	 */
	public CartPage clickRetrieveCartButton() {
		try {
			retrieveCartButton.click();
			Reporter.log("Retrieve cart button is clickable");
		} catch (Exception e) {
			Assert.fail("Failed to Retrieve cart button");
		}
		return this;
	}

	/**
	 * Verify Added device in Order details
	 * 
	 * @param deviceName
	 * @return
	 */
	public CartPage verifyAddedDeviceInOrderDetails(String deviceName) {
		try {
			for (WebElement webElement : productListOnOrderDetailsPage) {
				if (webElement.getText().contains(deviceName))
					break;
			}
			Reporter.log("Added device is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify newly added device name in order details");
		}
		return this;
	}

	/**
	 * Verify BYOD image at Empty Cart Essentials
	 *
	 */
	public CartPage verifyBYODImgOnCartEssentials() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(byodImgOnemptyCartEssentials), "BYOD image is not Displayed.");
			Reporter.log("BYOD image is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify BYOD image.");
		}
		return this;
	}

	/**
	 * Verify Add a phone Link at Empty Cart Essentials
	 *
	 */
	public CartPage verifyAddAPhoneLinkOnCartEssentials() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(addAPhoneLinkOnCartEssentials), "Add a phone link is not Displayed.");
			Reporter.log("Add a phone link is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add phone link.");
		}
		return this;
	}

	/**
	 * Verify Add a accessory link at Empty Cart Essentials
	 *
	 */
	public CartPage verifyAddAnAccessoryLinkOnCartEssentials() {
		try {
			Assert.assertTrue(isElementDisplayed(addAAccessoryLinkOnCartEssentials),
					"Add a Accessory link is not Displayed.");
			Reporter.log("Add a Accessory link is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add Accessory link.");
		}
		return this;
	}

	/**
	 * Verify Add a tablet link at Empty Cart Essentials
	 *
	 */
	public CartPage verifyAddATabletLinkOnCartEssentials() {
		try {
			Assert.assertTrue(isElementDisplayed(addATabletLinkOnCartEssentials),
					"Add a tablet link is not Displayed.");
			Reporter.log("Add a tablet link is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add a tablet link.");
		}
		return this;
	}

	/**
	 * Verify Add a wearable link at Empty Cart Essentials
	 *
	 */
	public CartPage verifyAddAWearableLinkOnCartEssentials() {
		try {
			Assert.assertTrue(isElementDisplayed(addAWearableLinkOnCartEssentials),
					"Add a Wearable link is not Displayed.");
			Reporter.log("Add a Wearable link is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add a Wearable link.");
		}
		return this;
	}

	/**
	 * Verify BYOD link at Empty Cart Essentials
	 *
	 */
	public CartPage verifyBYODLinkOnCartEssentials() {
		try {
			Assert.assertTrue(isElementDisplayed(byodLinkOnCartEssentials), "BYOD link is not Displayed.");
			Reporter.log("BYOD link is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify BYOD link.");
		}
		return this;
	}

	/**
	 * Click on Add a phone link at Empty Cart page
	 *
	 */
	public CartPage clickOnAddAPhoneLinkOnCart() {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(addAPhoneLinkOnCart);
			} else {
				moveToElement(customerCartContainerHeaderOncart);
			}

			checkPageIsReady();
			waitForSpinnerInvisibility();

			waitFor(ExpectedConditions.visibilityOf(addAPhoneLinkOnCart), 60);
			addAPhoneLinkOnCart.click();
			Reporter.log("Clicked on Add a Phone link Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to click Add a Phone link on Cart Page");
		}
		return this;
	}

	/**
	 * Click on Add a tablet link at Empty Cart page
	 *
	 */
	public CartPage clickOnAddATabletLinkOnCart() {
		try {
			waitForSpinnerInvisibility();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(addATabletLinkOnCart);
			} else {
				moveToElement(customerCartContainerHeaderOncart);
			}

			checkPageIsReady();
			waitForSpinnerInvisibility();

			waitFor(ExpectedConditions.visibilityOf(addATabletLinkOnCart), 60);
			addATabletLinkOnCart.click();
			Reporter.log("Clicked on Add a Tablet link on Cart");
		} catch (Exception e) {
			Assert.fail("Failed to click Add a Tablet link on Cart Page.");
		}
		return this;
	}

	/**
	 * Click on Add a wearable link at Empty Cart page
	 *
	 */
	public CartPage clickOnAddAWearableLinkOnCart() {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(addAWearableLinkOnCart);
			} else {
				moveToElement(customerCartContainerHeaderOncart);
			}

			checkPageIsReady();
			waitForSpinnerInvisibility();

			waitFor(ExpectedConditions.visibilityOf(addAWearableLinkOnCart), 60);
			addAWearableLinkOnCart.click();
			Reporter.log("Clicked on Add a wearable link on Cart");
		} catch (Exception e) {
			Assert.fail("Failed to click Add a wearable link on Cart Page.");
		}
		return this;
	}

	/**
	 * Click on Add a accessory link at Empty Cart page
	 *
	 */
	public CartPage clickOnAddAnAccessoryLinkOnCart() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(addAAcessoryLinkOnCart);
			} else {
				moveToElement(customerCartContainerHeaderOncart);
			}
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(addAAcessoryLinkOnCart), 60);
			addAAcessoryLinkOnCart.click();
			Reporter.log("Clicked on Add a acessory link on Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to click Add a acessory link on Cart Page.");
		}
		return this;
	}

	/**
	 * Click on Add a BYOD link at Empty Cart page
	 *
	 */
	public CartPage clickOnBYODLinkOnCartEssentials() {
		waitForSpinnerInvisibility();
		try {
			waitFor(ExpectedConditions.visibilityOf(byodLinkOnemptyCartEssentials), 20);
			moveToElement(customerCartContainerHeaderOncart);
			byodLinkOnemptyCartEssentials.click();
			Reporter.log("Clicked on  BYOD link on Empty Cart");
		} catch (Exception e) {
			Assert.fail("Failed to click BYOD link on Empty Cart Page.");
		}
		return this;
	}

	/**
	 * Verify Add a Phone CTA should not be displayed
	 */
	public CartPage verifyAddAPhoneCTAOnCartNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(addAPhoneCTAS.size(), 0, "Add a Phone CTA is displayed");
			Reporter.log("Add a Phone CTA is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add a Phone CTA");
		}
		return this;
	}

	/**
	 * Verify Add a accessory CTA should not be displayed
	 */
	public CartPage verifyOldAddAAccessoryCTAOnCartNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(addAnAcessorySessionemptyCartOld.size(), 0, "Add a accessory CTA is displayed");
			Reporter.log("Add a accessory CTA is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add a accessory CTA");
		}
		return this;
	}

	/**
	 * Verify Add a tablet or wearable should not be displayed
	 */
	public CartPage verifyOldAddATabletOrWearableOnCartNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(tabletOrWearableOld.size(), 0, "Add a tablet or wearable is displayed");
			Reporter.log("Add a tablet or wearable is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify tablet or wearable CTA");
		}
		return this;
	}

	/**
	 * Verify Add a line header displayed
	 *
	 */
	public CartPage verifyAddMoreLinesHeaderOnCartEssentials() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(addMoreLinesOrDevicesHeader),
					"Add more lines or devices header is not Displayed.");
			Reporter.log("Add more lines or devices header is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Add more lines or devices header.");
		}
		return this;
	}

	/**
	 * Verify Select Service Model Header Displayed
	 */
	public CartPage verifySelectServiceModelHeaderDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(selectServicesHeader), 60);
			Assert.assertTrue(selectServicesHeader.isDisplayed(), "Service header is not displayed");
			Reporter.log("Select Services header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Select services header");
		}
		return this;
	}

	/**
	 * Select Service Option
	 */
	public CartPage selectServiceOption() {
		try {
			checkPageIsReady();
			waitforSpinner();
			selectService.get(0).click();
			Reporter.log("User consent box is in checked state");
		} catch (Exception e) {
			Assert.fail("Failed to Select Service Option check box.");
		}
		return this;
	}

	/**
	 * Validate Your Plan Header is displayed
	 */
	public CartPage verifyYourPlanHeaderIsDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(yourPlanHeader), 60);
			Assert.assertTrue(yourPlanHeader.isDisplayed(), "Your Plan Header is not displayed");
			Reporter.log("Your Plan Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Your Plan header");
		}
		return this;
	}

	/**
	 * Validate T-Mobile One Plan best Value Badge is displayed
	 */
	public CartPage verifyBestValuePlanBadge() {
		try {
			Assert.assertTrue(bestValuePlanBadge.isDisplayed(), "T-Mobile One Plan best Value Badge is not displayed");
			Reporter.log("T-Mobile One Plan best Value Badge is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile One Plan best Value Badge");
		}
		return this;
	}

	/**
	 * Validate T-Mobile One Plan Bottom Text is displayed
	 */
	public CartPage verifyTMobileOnePlanBottomText() {
		try {
			Assert.assertTrue(TMobileOnePlanBottomText.isDisplayed(), "T-Mobile One Plan BottomText is not displayed");
			Reporter.log("T-Mobile One Plan Bottom text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile One Plan Bottom text");
		}
		return this;
	}

	/**
	 * Validate T-Mobile Essential Plan Bottom Text is displayed
	 */
	public CartPage verifyTMobileEssentialPlanBottomText() {
		try {
			Assert.assertTrue(TMobileEssentialsPlanBottomtext.isDisplayed(),
					"T-Mobile Essential Plan BottomText is not displayed");
			Reporter.log("T-Mobile One Plan Bottom Essential is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essential Plan Bottom text");
		}
		return this;
	}

	/**
	 * Validate T-Mobile Essential Plan Cost is displayed
	 */
	public CartPage verifyTMobileEssentialPlanCost() {
		try {
			Assert.assertTrue(TMobileEssentialsPlanCost.get(0).isDisplayed(),
					"T-Mobile Essential Plan cost is not displayed");
			Reporter.log("T-Mobile Essential Plan cost is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essential Plan cost");
		}
		return this;
	}

	/**
	 * Validate T-Mobile Essential Plan header is displayed
	 */
	public CartPage verifyTMobileEssentialPlanHeader() {
		try {
			Assert.assertTrue(planHeaderFromPlansSection.get(0).isDisplayed(),
					"T-Mobile Essential Plan Description is not displayed");
			Reporter.log("T-Mobile Essential Plan Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essential Plan Description");
		}
		return this;
	}

	/**
	 * Validate T-Mobile Essential Plan Description is displayed
	 */
	public CartPage verifyTMobileEssentialPlanDescription() {
		try {
			Assert.assertTrue(TMobileEssentialsPlanDescription.isDisplayed(),
					"T-Mobile Essential Plan Description is not displayed");
			Reporter.log("T-Mobile Essential Plan Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essential Plan Description");
		}
		return this;
	}

	/**
	 * Validate T-Mobile Essential Plan section is displayed
	 */
	public CartPage verifyTMobileEssentialPlanSection() {
		try {
			waitFor(ExpectedConditions.visibilityOf(planHeaderFromPlansSection.get(0)), 60);
			Assert.assertTrue(planHeaderFromPlansSection.get(0).isDisplayed(),
					"T-Mobile Essential Plan section is not displayed");
			Reporter.log("T-Mobile Essential Plan section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essential Plan section");
		}
		return this;
	}

	/**
	 * Validate T-Mobile Essential Plan section is disabled
	 */
	public CartPage verifyTMobileEssentialPlanSectionDisabled() {
		try {
			Assert.assertFalse(planHeaderFromPlansSection.get(0).isSelected(),
					"T-Mobile Essential Plan section is not disabled");
			Reporter.log("T-Mobile Essential Plan section is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essential Plan section disabled");
		}
		return this;
	}

	/**
	 * Validate T-Mobile Essential Plan section is disabled
	 */
	public CartPage verifyTMobileEssentialPlanIneligiblemsgDisabled() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(essentialPlanIneligibleMessafeDisabled.getAttribute("ng-bind-html").contains("Disable"),
					"T-Mobile Essential Plan Ineligible message is not disabled");
			Reporter.log("T-Mobile Essential Plan Ineligible message is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essential Plan Ineligible message disabled");
		}
		return this;
	}

	/**
	 * Validate Plan Legal text
	 */
	public CartPage verifyPlanLegaltext() {
		try {
			Assert.assertTrue(legalTextForPlan.isDisplayed(), "Legal text for Plan is not displayed");
			Reporter.log("Legal text for Plan is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Legal text for Plan");
		}
		return this;
	}

	/**
	 * Validate See Details and Compare plan CTA
	 */
	public CartPage verifySeeDetailsAndComparePlanCTA() {
		try {
			Assert.assertTrue(seeDetailsAndComparePlansCTA.isDisplayed(),
					"See Details and Compare plan CTA is not displayed");
			Reporter.log("See Details and Compare plan CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify See Details and Compare plan CTA");
		}
		return this;
	}

	/**
	 * Click See Details and Compare plan CTA
	 */
	public CartPage clickSeeDetailsAndComparePlanCTA() {
		try {
			moveToElement(deviceLabel);
			waitFor(ExpectedConditions.elementToBeClickable(seeDetailsAndComparePlansCTA), 60);
			seeDetailsAndComparePlansCTA.click();
			Reporter.log("See Details and Compare plan CTA is Clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click See Details and Compare plan CTA");
		}
		return this;
	}

	/**
	 * Validate See Details and Compare plan CTA disabled
	 */
	public CartPage verifySeeDetailsAndComparePlansCTADisabled() {
		try {
			Assert.assertFalse(seeDetailsAndComparePlansCTA.isEnabled(),
					"Validate See Details and Compare plan CTA is not disabled");
			Reporter.log("Validate See Details and Compare plan CTA is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Validate See Details and Compare plan CTA disabled");
		}
		return this;
	}

	/**
	 * Verify dynamic message in cart banner
	 */
	public CartPage verifyDynamicMessageForSwitchPlanInCartBanner() {
		try {
			waitFor(ExpectedConditions.visibilityOf(switchMegentaPlusBanner), 60);
			Assert.assertTrue(switchMegentaPlusBanner.getText().contains("Consider switching"),
					"switching to Megenta+ message is not displayed");
			Reporter.log("switching to Megenta+ message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify switching to Megenta+ message");
		}
		return this;
	}

	/**
	 * Verify dynamic message in cart banner
	 */
	public CartPage verifyDynamicMessageForSwitchPlanInCartBannerNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(switchMegentaPlusBanner),
					"switching to Megenta+ banner is displayed");
			Reporter.log("switching to Megenta+ banner is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify switching to Megenta+ banner");
		}
		return this;
	}

	/**
	 * Verify Duplicate line count in Cart page
	 */
	public CartPage verifyDuplicateLinesCountOnCart(int count) {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			Assert.assertEquals(duplicateCTA.size(), count,
					"Duplicate Line" + count + " is not added after clicking on Duplicate CTA on Cart ");
			Reporter.log("Duplicated Line #" + count + " is added after clicking on Duplicate CTA on Cart ");
		} catch (Exception e) {
			Assert.fail("Failed to verify duplicate Lines " + count + " on Cart after clicking on Duplicate CTA ");
		}
		return this;
	}

	/**
	 * Verify max line reached message in Cart page
	 * 
	 */
	public CartPage verifyLineMaxLimitReachedMessageOnCart() {
		try {
			Assert.assertTrue(lineMaxLimitReachedMessage.isDisplayed(),
					"'Line Max Limit Reached' Message is not displaying");
			Reporter.log("'Line Max Limit Reached' Message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Line Max Limit Reached' Message");
		}
		return this;
	}

	/**
	 * validate Disabled addAPhonebtn
	 *
	 */
	public CartPage verifyAddAPhonebtnDisabled() {
		waitForSpinnerInvisibility();
		try {
			moveToElement(addAPhoneLinkOnCart);
			Assert.assertTrue(addAPhoneLinkOnCart.getAttribute("class").contains("disabled"),
					"Add A Phone CTA is Enabled");
			Reporter.log("Add A Phone cta not Enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add A Phone cta disabled");
		}
		return this;

	}

	/**
	 * validate Disabled addAWearable
	 *
	 */
	public CartPage verifyAddAWearablebtnDisabled() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addAAccessoryImgOnemptyCartEssentials), 60);
			waitforSpinner();
			Assert.assertTrue(addAWearableLinkOnCart.getAttribute("class").contains("div-disabled"),
					"Add A Wearable CTA is Enabled");
			Reporter.log("Add A Wearable cta not Enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add A Wearable cta disabled");
		}
		return this;
	}

	/**
	 * verify Authorable Msg On AddWearable Img - in eligible msg
	 *
	 */
	public CartPage verifyAuthorableMsgOnAddWearableImg() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(authorableMsgOnAddWearableImg), 2);
			Assert.assertTrue(authorableMsgOnAddWearableImg.isDisplayed(),
					"'To add a wearable change your plan to T-Mobile One' Message is not displaying");
			Reporter.log("'To add a wearable change your plan to T-Mobile One' Message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'To add a wearable change your plan to T-Mobile One' Message");
		}
		return this;

	}

	/**
	 * validate Disabled addATablet
	 *
	 */
	public CartPage verifyAddATabletbtnDisabled() {
		try {
			Assert.assertTrue(addATabletLinkOnCart.getAttribute("class").contains("disabled"),
					"Add A Tablet CTA is Enabled");
			Reporter.log("Add A Tablet cta not Enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add A Tablet cta disabled");
		}
		return this;
	}

	/**
	 * validate Disabled byodcta
	 *
	 */
	public CartPage verifyBYODbtnDisabled() {
		try {
			moveToElement(byodLinkOnemptyCartEssentials);
			Assert.assertTrue(byodLinkOnemptyCartEssentials.getAttribute("class").contains("disabled"),
					"BYOD cta is Enabled");
			Reporter.log("BYOD cta not Enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify BYOD cta disabled");
		}
		return this;
	}

	/**
	 * validate enabled AddAAcessorybtn
	 *
	 */
	public CartPage verifyAddAAcessorybtnEnabled() {
		try {
			Assert.assertFalse(addAAcessoryLinkOnCart.getAttribute("class").contains("disabled"),
					"Add A Acessory cta is disabled");
			Reporter.log("Add A Acessory cta Enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add A Acessory cta enabled");
		}
		return this;
	}

	/**
	 * Verify Plan Block for line
	 *
	 */
	public CartPage verifyPlanBlockForLine() {
		try {
			Assert.assertTrue(planBlockForLine.isDisplayed(), "Plan block for line is not displayed");
			Reporter.log("Plan block for line is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Plan block for line");
		}
		return this;
	}

	/**
	 * Verify Plan Header for line
	 *
	 */
	public CartPage verifyPlanHeaderForLine() {
		try {
			Assert.assertTrue(planHeaderForLine.isDisplayed(), "Plan Header for line is not displayed");
			Reporter.log("Plan Header for line is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Plan Header for line");
		}
		return this;
	}

	/**
	 * Verify Plan Item for line
	 *
	 */
	public CartPage verifyPlanItemForLine() {
		try {
			moveToElement(planItemForLineList.get(0));
			Assert.assertTrue(planItemForLineList.get(0).isDisplayed(), "Plan Item for line is not displayed");
			Reporter.log("Plan Item for line is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Plan Item for line");
		}
		return this;
	}

	/**
	 * Verify Plan Item for line contains plan T-Mobile Essentials
	 *
	 */
	public CartPage verifyPlanItemForLineIsTMobileEssentials() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			for (WebElement plan : planItemForLineList) {
				Assert.assertTrue(plan.isDisplayed(), "Plan Item for line is not displayed");
				Assert.assertTrue(plan.getText().contains("T-Mobile Essentials"),
						"Plan Item for line does not contains T-Mobile Essentials Plan");
			}
			Reporter.log("T-Mobile Essentials plan is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify T-Mobile Essentials plan");
		}
		return this;
	}

	/**
	 * Verify Tool Tip for Plan
	 *
	 */
	public CartPage verifyToolTipForPlan() {
		try {
			moveToElement(tradeDeviceBtnForEachLine.get(0));
			Assert.assertTrue(planToolTipForLine.isDisplayed(), "Tool tip for plan is not displayed");
			Reporter.log("Tool tip for plan is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool tip for plan");
		}
		return this;
	}

	/**
	 * Click Tool Tip for Plan
	 *
	 */
	public CartPage clickToolTipForPlan() {
		try {
			checkPageIsReady();
			moveToElement(planToolTipForLine);
			planToolTipForLine.click();
			Reporter.log("Clicked on Tool Tip for Plan");
		} catch (Exception e) {
			Assert.fail("Failed to click Tool tip for plan");
		}
		return this;
	}

	/**
	 * Verify Plan Description Text on Modal
	 *
	 */
	public CartPage verifyPlanDescriptionTextOnModal() {
		try {
			checkPageIsReady();
			Assert.assertTrue(planDescriptionOnModal.isDisplayed(), "Plan Description Text on Modal is not displayed");
			Reporter.log("Plan Description Text on Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Plan Description Text on Modal");
		}
		return this;
	}

	/**
	 * Verify Add an Accessory Tile is not displayed
	 *
	 */
	public CartPage verifyOldAddAnAccessoryTileIsNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(addAnAccessoryImgOld), "Image Tile is visible");
			Reporter.log("Add an Accessory Tile is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add an Accessory Tile not displayed");
		}
		return this;
	}

	/**
	 * Verify Rate Plan Section is not displayed
	 *
	 */
	public CartPage verifyRatePlanSectionIsNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(yourPlanHeader), "Plan Section is displayed, but it should not");
			Reporter.log("Rate Plan Section is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Rate Plan Section not displayed");
		}
		return this;
	}

	/**
	 * click on TMobileEssentialsPlan
	 */
	public CartPage clickOnTMobileEssentialsPlan() {
		try {
			waitForSpinnerInvisibility();
			moveToElement(planHeaderFromPlansSection.get(0));
			waitFor(ExpectedConditions.visibilityOf(planHeaderFromPlansSection.get(0)), 60);
			planHeaderFromPlansSection.get(0).click();
			Reporter.log("Clicked on TMobile Essentials Plan");
		} catch (Exception e) {
			Assert.fail("Failed to click on TMobile Essentials Plan");
		}
		return this;
	}

	/**
	 * click on Magenta Plan
	 */
	public CartPage clickOnMagentaPlan() {
		try {
			waitFor(ExpectedConditions.visibilityOf(megentaPlan), 60);
			moveToElement(megentaPlan);
			clickElementWithJavaScript(megentaPlan);
			Reporter.log("Clicked on Megenta Plan");
		} catch (Exception e) {
			Assert.fail("Failed to click on megenta Plan");
		}
		return this;
	}

	/**
	 * verify megenta Plan selected
	 */
	public CartPage verifyMagentaPlanSelected() {
		try {
			waitFor(ExpectedConditions.visibilityOf(activePlan), 60);
			Assert.assertTrue(activePlan.getText().equalsIgnoreCase("Magenta™"), "Megenta Plan is not selected");
			Reporter.log("Megenta Plan is  selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Megenta Plan selected");
		}
		return this;
	}

	/**
	 * verify megenta Plus Plan selected
	 */
	public CartPage verifyMagentaPlusPlanSelected() {
		try {
			checkPageIsReady();
			Assert.assertTrue(activePlan.getText().equalsIgnoreCase("Magenta™ Plus"),
					"Megenta Plus Plan is not selected");
			Reporter.log("Megenta Plus Plan is  selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Megenta Plus Plan selected");
		}
		return this;
	}

	/**
	 * verify TMobileEssentialsPlan selected
	 */
	public CartPage verifyTMobileEssentialsPlanSelected() {
		try {
			checkPageIsReady();
			Assert.assertTrue(activePlan.getText().equalsIgnoreCase("Essentials"),
					"TMobileEssentialsPlan is not selected");
			Reporter.log("TMobileEssentialsPlan is  selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify TMobileEssentialsPlan  selected");
		}
		return this;
	}

	/**
	 * Verify Add a accessory Header should not be displayed
	 */
	public CartPage verifyOldAddAAccessoryHeaderOnCartNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(addAccessoryHeader.size(), 0, "Add a accessory Header is displayed");
			Reporter.log("Add a accessory Header is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add a accessory Header");
		}
		return this;
	}

	/**
	 * Verify Add a accessory Image should not be displayed
	 */
	public CartPage verifyOldAddAAccessoryImageOnCartNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(addAccessoryImg.size(), 0, "Add a accessory Image is displayed");
			Reporter.log("Add a accessory Image is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add a accessory Image");
		}
		return this;
	}

	/**
	 * Verify By Line level pricing block Displayed
	 */
	public CartPage verifylineLevelPriceBlockDisplayed() {
		try {
			Assert.assertTrue(lineLevelPricingBlock.get(0).isDisplayed(), "Line level pricing block is not displayed");
			Reporter.log("Line level pricing block is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line level pricing block ");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment header Displayed
	 */
	public CartPage verifyMonthlyPaymentHeaderDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentHeader.get(0)), 60);
			Assert.assertTrue(monthlyPaymentHeader.get(0).isDisplayed(), "Monthly payment header is not displayed");
			Reporter.log("Monthly payment header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment header");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Line Displayed
	 */
	public CartPage verifyMonthlyPaymentLineDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentLineDeposite.get(0)), 60);
			Assert.assertTrue(monthlyPaymentLineDeposite.get(0).isDisplayed(), "Monthly payment Line is not displayed");
			Reporter.log("Monthly payment Line  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Line");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Line Price Displayed
	 */
	public CartPage verifyMonthlyPaymentLinePriceDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentLinePrice.get(0)), 60);
			Assert.assertTrue(monthlyPaymentLinePrice.get(0).isDisplayed(),
					"Monthly payment Line price is not displayed");
			Reporter.log("Monthly payment Line price  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Line price");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Addon Price Displayed
	 */
	public CartPage verifyMonthlyPaymentAddonPriceDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(AddOnsLineItemPrice.get(0)), 60);
			Assert.assertTrue(AddOnsLineItemPrice.get(0).isDisplayed(),
					"Monthly payment Addons price is not displayed");
			Reporter.log("Monthly payment Addons price  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Addons price");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Addon Price Displayed
	 */
	public CartPage verifyMonthlyPaymentAddonPriceNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(AddOnsLineItemPrice.size(), 0, "Addons Line Item Price is displayed");
			Reporter.log("Monthly payment Addons price  is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Addons price");
		}
		return this;
	}

	/**
	 * Verify By Addons line item Displayed
	 */
	public CartPage verifyMonthlyPaymentAddonsLineDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(AddOnsLineItem.get(0)), 60);
			Assert.assertTrue(AddOnsLineItem.get(0).isDisplayed(), "Monthly payment Addon line item is not displayed");
			Reporter.log("Add On Line Item is displayed in monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Addon line item");
		}
		return this;
	}

	/**
	 * Verify By Addons line item not Displayed
	 */
	public CartPage verifyMonthlyPaymentAddonsLineNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(AddOnsLineItem.size(), 0, "Addons Line Item is displayed");
			Reporter.log("Add On Line Item is not displayed in monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Addon line item");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Device Displayed
	 */
	public CartPage verifyMonthlyPaymentDeviceDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentDevice.get(0)), 60);
			Assert.assertTrue(monthlyPaymentDevice.get(0).isDisplayed(),
					"Monthly payment Device text is not displayed");
			Reporter.log("Device text is displayed under monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Device text ");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Device Price Displayed
	 */
	public CartPage verifyMonthlyPaymentDevicePriceDisplayed() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentDevicePrice.get(0)));
			Assert.assertTrue(monthlyPaymentDevicePrice.get(0).isDisplayed(),
					"Monthly payment Device Price is not displayed");
			Reporter.log("Device price is displayed under monthly payment /CRP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment/CRP Device Price ");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Discount Displayed
	 */
	public CartPage verifyMonthlyPaymentDiscountDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentDiscount.get(0)), 60);
			Assert.assertTrue(monthlyPaymentDiscount.get(0).isDisplayed(), "Monthly payment Discount is not displayed");
			Reporter.log("Monthly payment Discount is displayed under monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Discount");
		}
		return this;
	}

	/**
	 * Verify By Monthly payment Discount price Displayed
	 */
	public CartPage verifyMonthlyPaymentDiscountPriceDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentDiscountPrice.get(0)), 60);
			Assert.assertTrue(monthlyPaymentDiscountPrice.get(0).isDisplayed(),
					"Monthly payment Discount price is not displayed");
			Reporter.log("Monthly payment Discount price is displayed under monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Discount price");
		}
		return this;
	}

	/**
	 * Verify By Hybrid account Discount Displayed
	 */
	public CartPage verifyHybridDiscountDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentDiscount.get(1)), 60);
			Assert.assertTrue(monthlyPaymentDiscount.get(1).isDisplayed(), "Hybrid account  Discount is not displayed");
			Reporter.log("Hybrid account  Discount is displayed under monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Hybrid account  Discount");
		}
		return this;
	}

	/**
	 * Verify By Hybrid account Discount price Displayed
	 */
	public CartPage verifyHybridDiscountPriceDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentDiscountPrice.get(1)), 60);
			Assert.assertTrue(monthlyPaymentDiscountPrice.get(1).isDisplayed(),
					"Hybrid account Discount price is not displayed");
			Reporter.log("Hybrid account Discount price is displayed under monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Hybrid account Discount price");
		}
		return this;
	}

	/**
	 * Verify By One time payment Text Displayed
	 */
	public CartPage verifyOneTimePaymentDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(oneTimePaymentHeader.get(0)), 60);
			Assert.assertTrue(oneTimePaymentHeader.get(0).isDisplayed(), "One Time payment Header is not displayed");
			Reporter.log("One Time payment header is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify One time  payment header");
		}
		return this;
	}

	/**
	 * Verify By SimStaterKit price Displayed
	 */
	public CartPage verifySimStaterKitPriceDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(simstarterKitPrice.get(0)), 60);
			Assert.assertTrue(simstarterKitPrice.get(0).isDisplayed(), "SimStaterKit price is not displayed");
			Reporter.log("SimStaterKit price is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify SimStaterKit price");
		}
		return this;
	}

	/**
	 * Verify By LineLevel Tax And Shipping Displayed
	 */
	public CartPage verifyLineLevelTaxAndShippingDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(taxesAndShippingTextMultiline.get(0)), 60);
			Assert.assertTrue(taxesAndShippingTextMultiline.get(0).isDisplayed(),
					"LineLevel Tax And Shipping is not displayed");
			Reporter.log("LineLevel Tax And Shipping is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Level Tax And Shipping");
		}
		return this;
	}

	/**
	 * Verify By LineLevel Price BreakDown Displayed
	 */
	public CartPage verifyPriceBreakDownUnderOneTimePaymentDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(priceBreakdownUnderOneTimePayment.get(0)), 60);
			Assert.assertTrue(priceBreakdownUnderOneTimePayment.get(1).isDisplayed(),
					"LineLevel Price BreakDown is not displayed");
			Reporter.log("LineLevel Price BreakDown  is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify LineLevel Price BreakDown");
		}
		return this;
	}

	/**
	 * Verify By LineLevel PriceBreakDown TotalPrice Displayed
	 */
	public CartPage verifyPriceBreakDownTotalPriceDisplayedLineLevel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(priceBreakdownTodayPrice.get(0)), 60);
			Assert.assertTrue(priceBreakdownTodayPrice.get(0).isDisplayed(),
					"LineLevel PriceBreakDown TotalPrice is not displayed");
			Reporter.log("LineLevel PriceBreakDown TotalPrice is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify LineLevel PriceBreakDown TotalPrice");
		}
		return this;
	}

	/**
	 * Verify By LineLevel PriceBreakDown MOnthly Displayed
	 */
	public CartPage verifyPriceBreakDownMonthlyPriceDisplayedLineLevel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(priceBreakdownMonthlyPrice.get(0)), 60);
			Assert.assertTrue(priceBreakdownMonthlyPrice.get(0).isDisplayed(),
					"LineLevel PriceBreakDown Monthly price is not displayed");
			Reporter.log("LineLevel PriceBreakDown Monthly price is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify LineLevel PriceBreakDown Monthly price");
		}
		return this;
	}

	/**
	 * Verify By LineLevel PriceBreakDown AutoPay Discount Message Displayed
	 */
	public CartPage verifyPriceBreakDownAutoPayDiscountMessageDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(priceBreakdownAutoPaymentMessage.get(0)), 60);
			Assert.assertTrue(priceBreakdownAutoPaymentMessage.get(0).isDisplayed(),
					"LineLevel PriceBreakDown AutoPay Discount Message is not displayed");
			Reporter.log("LineLevel PriceBreakDown AutoPay Discount Message is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify LineLevel PriceBreakDown AutoPay Discount Message");
		}
		return this;
	}

	/**
	 * Click Outside the modal for Plan Tool Tip
	 *
	 */
	public CartPage clickOutsideToolTipForPlan() {
		try {
			planBlockForLine.click();
			Reporter.log("Clicked Outside Modal of Tool Tip for Plan");
		} catch (Exception e) {
			Assert.fail("Failed to click Outside Modal Tool tip for plan");
		}
		return this;
	}

	/**
	 * Verify Tool Tip Block is Closed
	 *
	 */
	public CartPage verifyToolTipBlockClosed() {
		try {
			checkPageIsReady();
			Assert.assertEquals(getDriver().findElements(By.cssSelector(".popover")).size(), 0,
					"Tool Tip Block is displayed");
			Reporter.log("Tool Tip Block is Closed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool Tip Block is Closed");
		}
		return this;
	}

	/**
	 * Verify Add On and Extras Header Displayed
	 */
	public CartPage verifyAddOnExtrasHeaderDisplayed() {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(addOnExtrasHeaderLineLevel.get(0)), 60);
			Assert.assertTrue(addOnExtrasHeaderLineLevel.get(0).isDisplayed(),
					"Add On and Extras Header is not Displayed");
			Reporter.log("Add On and Extras Header is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add On and Extras Header is Displayed");
		}
		return this;
	}

	/**
	 * Verify See More Options CTA displayed with Add On and Extras Header
	 */
	public CartPage verifyAddOnsAndExtrasSeeMoreOptionsCTADisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(0)), 60);
			Assert.assertTrue(seeMoreOptionsCTAAddOnsAndExtrasList.get(0).isDisplayed(),
					"See More Options CTA is not Displayed");
			Reporter.log("See More Options CTA is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify See more options CTA");
		}
		return this;
	}

	/**
	 * Click See More Options CTA displayed with Add On and Extras Header
	 */
	public CartPage clickAddOnsAndExtrasSeeMoreOptionsCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(0)), 60);
			moveToElement(tradeDeviceBtnForEachLine.get(0));
			seeMoreOptionsCTAAddOnsAndExtrasList.get(0).click();
			Reporter.log("See More Options CTA is Clicked");
		} catch (Exception e) {
			Assert.fail("Failed to Click See more options CTA");
		}
		return this;
	}

	/**
	 * Click See More Options CTA displayed with Add On and Extras Header
	 */
	public CartPage verifyProtectionModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(protectionModal), 60);
			Assert.assertTrue(protectionModal.isDisplayed(), "Protection Modal is not Displayed");
			Reporter.log("Protection Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Protection Modal");
		}
		return this;
	}

	/**
	 * Verify Enter Zip Modal
	 */
	public CartPage verifyEnterZipModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(zipCodeModal), 60);
			Assert.assertTrue(zipCodeModal.isDisplayed(), "Zip Code Modal is not Displayed");
			Reporter.log("Zip Code Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Zip Code Modal");
		}
		return this;
	}

	/**
	 * Verify and select Protection 360 Plan on Protection Modal
	 */
	public CartPage selectProtection360PlanOnModal() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(protection360OnprotectionModal), 60);
			Assert.assertTrue(protection360OnprotectionModal.isDisplayed(), "Protection 360 Plan is not displayed");
			protection360OnprotectionModal.click();
			Reporter.log("Protection 360 Plan is selected");
		} catch (Exception e) {
			Assert.fail("Failed to select Protection 360 Plan");
		}
		return this;
	}

	/**
	 * Verify and click Update CTA on Protection Modal
	 */
	public CartPage clickUpdateCTAonProtectionModal() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(updateCTAOnprotectionModal), 60);
			clickElementWithJavaScript(updateCTAOnprotectionModal);
			Reporter.log("Update CTA is selected");
		} catch (Exception e) {
			Assert.fail("Failed to click on Update CTA");
		}
		return this;
	}

	/**
	 * Verify and click next CTA on zipcode Modal
	 */
	public CartPage clickNextCTAOnZipCodeModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(nextCTAOnZipCodeModal), 60);
			nextCTAOnZipCodeModal.click();
			Reporter.log("Next CTA is selected");
		} catch (Exception e) {
			Assert.fail("Failed to click on Next CTA");
		}
		return this;
	}

	/**
	 * Verify Data Soc Displayed
	 */
	public CartPage verifyDataSocDisplayed() {
		try {
			waitforSpinner();
			int i;
			int j = 0;
			for (i = 0; i <= listOfAdOnsAvailable.size() - 1; i++) {
				if (listOfAdOnsAvailable.get(i).isDisplayed()) {
					Assert.assertTrue((listOfAdOnsAvailable.get(i).isDisplayed()),
							"Data soc and services is not displayed");
					j = j + 1;
				}
			}
			if (j > 0) {
				Reporter.log("Data soc and Services are Displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify data soc and Services");
		}
		return this;
	}

	/**
	 * Verify List of Add On and Extras Header Displayed
	 */
	public CartPage verifyListOfAddOnsDisplayed() {
		try {
			waitForSpinnerInvisibility();
			moveToElement(tradeDeviceBtnForEachLine.get(0));

			int index = 0;
			for (WebElement listOfAddOnsElement : listOfAdOnsAvailable) {
				waitFor(ExpectedConditions.visibilityOf(listOfAddOnsElement), 60);
				if (listOfAddOnsElement.getText() != null) {
					index = index + 1;
				}
			}
			if (index > 0) {
				Reporter.log("List of Add On and Extras Header is Displayed");
			} else {
				Assert.fail("List of Add On and Extras Header is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify Add On and Extras Header is Displayed");
		}
		return this;
	}

	/**
	 * Verify List of Add On and Extras Header Displayed
	 */
	public CartPage verifyListOfPriceAddOnExtrasHeaderDisplayed() {
		try {
			int index = 0;
			for (WebElement listOfPriceAddOnsElement : listOfPriceForAddOns) {

				if (listOfPriceAddOnsElement.getText() != null && listOfPriceAddOnsElement.getText().contains("$")) {
					index = index + 1;
				}
			}
			if (index > 0) {
				Reporter.log("List of Price Add On and Extras Header is Displayed");
			} else {
				Assert.fail("List of Price Add On and Extras Header is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify List of Price Add On and Extras Header is Displayed");
		}
		return this;
	}

	/**
	 * Verify List of Add On and Extras Header Displayed
	 */
	public CartPage verifyListOfToolTipsAddOnExtrasHeaderDisplayed() {
		try {
			checkPageIsReady();
			// scrollToElement(deviceLabel);
			moveToElement(deviceLabel);
			clickOnTooltip(listOfToolTipsForAddOns.get(0));
			verifyTooltipDescription(); // failing here
			clickOutsideTheTooltipbox();
			if (listOfToolTipsForAddOns.size() > 0) {
				Reporter.log("List of Tool Tips Add On and Extras Header is Displayed");
			} else {
				Assert.fail("Number of tooltips is zero");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify List of Tool Tips Add On and Extras Header is Displayed");
		}
		return this;
	}

	/**
	 * Verify List of Add On and Extras Header Displayed
	 */
	public CartPage verifyListOfToolTipsAddOnExtrasHeaderDisplayedForMobile() {
		try {
			int index = 0;
			for (WebElement listOfToolTipAddOnsElement : listOfToolTipsForAddOns) {
				if (listOfToolTipAddOnsElement.isDisplayed()) {
					index = index + 1;
				}
			}
			if (index > 0) {
				Reporter.log("List of Tool Tip for Add On and Extras Header is Displayed");
			} else {
				Assert.fail("List of Tool Tip for Add On and Extras Header is not Displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify List of Tool Tips Add On and Extras Header is Displayed");
		}
		return this;
	}

	/**
	 * Verify Enter Your Zip Code Modal Header Displayed
	 */
	public CartPage verifyEnterYourZipCodeModalHeaderDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(enterYourZipCodeHeader), 60);
			Assert.assertTrue(enterYourZipCodeHeader.isDisplayed(),
					"Enter Your Zip Code Modal header is not displayed");
			Reporter.log("Enter Your Zip Code Modal header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Enter Your Zip Code Modal header");
		}
		return this;
	}

	/**
	 * Verify recommendedService Header Displayed
	 */
	public CartPage verifyRecommendedServiceHeaderDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(recommendedServiceLabel.isDisplayed(), "Recommended Service header is not displayed");
			Reporter.log("Recommended Service header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Recommended Service header");
		}
		return this;
	}

	/**
	 * Verify Recommended Service Displayed
	 */
	public CartPage verifyRecommendedServiceDisplayed() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(recommendedService), 60);
			Assert.assertTrue(recommendedService.isDisplayed(), "Recommended Service is not displayed");
			Reporter.log("Recommended Service is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Recommended Service");
		}
		return this;
	}

	/**
	 * Verify otherServicesLabel Displayed
	 */
	public CartPage verifyOtherServicesDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(otherServicesLabel.isDisplayed(), "Otrher Service is not displayed");
			Reporter.log("Other Service is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Other Service");
		}
		return this;
	}

	/**
	 * Click on otherServicesLabel
	 */
	public CartPage clickOnOtherServices() {
		try {
			checkPageIsReady();
			otherServicesLabel.click();
			Reporter.log("Other Service is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Other Service");
		}
		return this;
	}

	/**
	 * Verify communicationAndData Displayed
	 */
	public CartPage verifyCommunicationAndDataDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(communicationAndData.isDisplayed(), "Communication and data is not displayed");
			Reporter.log("Communication and data is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Communication and data");
		}
		return this;
	}

	/**
	 * Verify deviceProtection Displayed
	 */
	public CartPage verifyDeviceProtectionDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(deviceProtection.isDisplayed(), "Device Protection is not displayed");
			Reporter.log("Device Protection is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Device Protection");
		}
		return this;
	}

	/**
	 * Verify entertaintmentAndMore Displayed
	 */
	public CartPage verifyEntertaintmentAndMoreDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(entertaintmentAndMore.isDisplayed(), "Entertaintment and more is not displayed");
			Reporter.log("Entertaintment and more is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Entertaintment and more");
		}
		return this;
	}

	/**
	 * Verify In eligible plan message
	 */
	public CartPage verifyIneleigibleModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(ineligibleModal), 60);
			Assert.assertTrue(ineligibleModal.isDisplayed(), "In eligible plan message is not displayed");
			Reporter.log("In eligible plan message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify In eligible plan message");
		}
		return this;
	}

	/**
	 * Click on ineligible Modal Yes
	 */
	public CartPage clickOnineligibleModalYes() {
		try {
			ineligibleModalYes.click();
			Reporter.log("Clicked on ineligible Modal Yes");
		} catch (Exception e) {
			Assert.fail("Failed to click on ineligible Modal Yes");
		}
		return this;
	}

	/**
	 * verify selected services in addon and extras container
	 *
	 */
	public CartPage verifySelectedServicesOnAddOnContainer() {
		try {
			checkPageIsReady();
			waitforSpinner();
			int i;
			for (i = 0; i <= listOfAdOnsAvailable.size() - 1; i++) {
				Assert.assertTrue(listOfAdOnsAvailable.get(i).isDisplayed(),
						"Selected service is not displayed on container.");
			}
			Reporter.log("selected service is displayed in add on and extras container ");
		} catch (Exception e) {
			Assert.fail("Failed to verify selected service in add on and extras container");
		}
		return this;
	}

	/**
	 * verify selected service prices in addon and extras container
	 *
	 */
	public CartPage verifySelectedServicePricesOnAddOnContainer() {
		try {
			checkPageIsReady();
			waitforSpinner();
			int i;
			for (i = 1; i <= selectedServicePricesinaddOnsContainer.size() - 1; i++) {
				Assert.assertTrue(selectedServicePricesinaddOnsContainer.get(i).isDisplayed(),
						"Prices of selected service is not displayed");
			}
			Reporter.log("selected service price is displayed in add on and extras container ");
		} catch (Exception e) {
			Assert.fail("Failed to verify selected service price in add on and extras container");
		}
		return this;
	}

	/**
	 * Select other services VoiceMailTo Text Option
	 *
	 */
	public CartPage selectOtherServicesVoiceMailToTextOption() {
		try {
			checkPageIsReady();
			waitforSpinner();
			// otherservicesVoiceMailToTextOption.click();
			clickElementWithJavaScript(otherservicesVoiceMailToTextOption);
			Reporter.log("Select on otherservices VoiceMailToText Option");
		} catch (Exception e) {
			Assert.fail("Failed to select otherservices VoiceMailToText Option");
		}
		return this;
	}

	/**
	 * Select other services NameID Option
	 *
	 */
	public CartPage selectOtherServicesNameIDOption() {
		try {
			checkPageIsReady();
			waitforSpinner();
			// otherservicesNameIDOption.click();
			clickElementWithJavaScript(otherservicesNameIDOption);
			Reporter.log("select on otherservices NameID Option");
		} catch (Exception e) {
			Assert.fail("Failed to select otherservices NameID Option");
		}
		return this;
	}

	/**
	 * Click on update button
	 *
	 */
	public CartPage clickOnSelectServiceUpdateBtn() {
		try {
			checkPageIsReady();
			// scrollToElement(selectServiceUpdate);
			clickElementWithJavaScript(selectServiceUpdate);
			Reporter.log("Clicked on update button");
		} catch (Exception e) {
			Assert.fail("Failed to click on update  button");
		}
		return this;
	}

	/**
	 * verify Wearables Ineligible Modal window
	 */
	public CartPage verifyWearablesIneligibleModal() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(wearablesIneliigibleModal), 60);
			Assert.assertTrue(wearablesIneliigibleModal.isDisplayed(), "Wearables Ineligible Modal is not displayed");
			Reporter.log("Wearables Ineligible Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Wearables Ineligible Modal in cart");
		}
		return this;
	}

	/**
	 * verify Wearables Ineligible Modal Message
	 */
	public CartPage verifyWearablesIneligibleModalMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(
					wearablesIneliigibleModalMessage.getText().contains("Oops-wearable devices require the Magenta"),
					"Wearables Ineligible Modal Message is not displayed");
			Reporter.log("Wearables Ineligible Modal Message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Wearables Ineligible Modal Message in cart");
		}
		return this;
	}

	/**
	 * verify change plan CTA displayed
	 */
	public CartPage verifyChangePlanCtaDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(changePlanCTA.isDisplayed(), "change plan CTA is not displayed");
			Reporter.log("change plan CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display change plan CTA");
		}
		return this;
	}

	/**
	 * verify change plan CTA displayed
	 */
	public CartPage clickChangePlanCta() {
		try {
			changePlanCTA.click();
			Reporter.log("change plan CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click change plan CTA");
		}
		return this;
	}

	/**
	 * To get service name from add ons extra service
	 *
	 */

	public String getServiceNameForMoreAddOnsService() {
		String service = null;
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(serviceForAddOns), 60);
			service = serviceForAddOns.getText();
			Reporter.log("Service add ons name is saved");
		} catch (Exception e) {
			Assert.fail("Failed to get service add ons name");
		}
		return service;
	}

	/**
	 * Verify Selected service for add ons updated in cart
	 */
	public CartPage verifySelectedServiceAndPriceForAddOns(String service) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(serviceAddOnOnCart), 60);
			String displayedService = serviceAddOnOnCart.getText();
			assertTrue(displayedService.equals(service), "Service is not updated on Cart page");
			Reporter.log("Selected service is displayed");
			assertTrue(selectedServicePricesinaddOnsContainer.get(1).isDisplayed(),
					"Service Price is not displayed at Add ons container");
			Reporter.log("Selected Service Price is displayed at Add ons container");
		} catch (Exception e) {
			Assert.fail("Failed to verify Selected Service at Add on container");
		}
		return this;
	}

	/**
	 * Get Total Today price at Sticky banner from cart page
	 */
	public String getTotalTodayPriceInStickyBanner() {
		String todayPrice = null;
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				todayPrice = cartpagestickybannerTotalPriceMobile.getAttribute("aria-label").replace("$", "");
				Reporter.log("Saved Today price from sticky banner");

			} else {
				todayPrice = cartStickyBannerTodayTotalPrice.getAttribute("aria-label").replace("$", "");
				Reporter.log("Saved Today price from sticky banner");
			}
		} catch (Exception e) {
			Assert.fail("Failed to get Today price");
		}
		return todayPrice;
	}

	/**
	 * Get Total Monthly price at Sticky banner from cart page
	 */
	public String getTotalMonthlyPriceStickyBanner() {
		String monthlyPrice = null;
		try {
			checkPageIsReady();
			monthlyPrice = cartpagestickybannerMonthlyTotalPrice.getAttribute("aria-label").replace("$", "");
			Reporter.log("Saved Monthly price from sticky banner");
		} catch (Exception e) {
			Assert.fail("Failed to get Monthly price");
		}
		return monthlyPrice;
	}

	/**
	 * Compare Sticky Banner Today Price
	 */
	public CartPage compareStickyBannerTodayPrice(String totalTodayPriceForGoodCredit) {
		try {
			Assert.assertEquals(getTotalTodayPriceInStickyBanner(), totalTodayPriceForGoodCredit);
			Reporter.log("Today price should be equal");
		} catch (Exception e) {
			Assert.fail("Failed to Today prices are not equal");
		}
		return this;
	}

	/**
	 * Compare Sticky Banner Monthly Price
	 */
	public CartPage compareStickyBannerMonthlyPrice(String totalMonthlyPriceForGoodCredit) {
		try {
			Assert.assertEquals(getTotalMonthlyPriceStickyBanner(), totalMonthlyPriceForGoodCredit);
			Reporter.log("Monthly price should be equal");
		} catch (Exception e) {
			Assert.fail("Failed to Monthly prices are not equal");
		}
		return this;
	}

	/**
	 * Compare Sticky Banner Monthly Price
	 */
	public CartPage compareStickyBannerMonthlyPriceNotEqual(String totalMonthlyPriceForGoodCredit) {
		try {
			Assert.assertNotEquals(getTotalMonthlyPriceStickyBanner(), totalMonthlyPriceForGoodCredit,
					"Monthly prices are equal for both the plans");
			Reporter.log("Monthly price should not be equal");
		} catch (Exception e) {
			Assert.fail("Failed to Monthly prices are equal");
		}
		return this;
	}

	/**
	 * 
	 * Verify No Remove Wearable CTA displayed
	 */
	public CartPage verifyNoRemoveCTADisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(removeWearableCTA.isDisplayed(), "No Remove Wearable CTA is not displayed");
			Reporter.log("No Remove Wearable CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display No Remove Wearable CTA");
		}
		return this;
	}

	/**
	 * Click No Remove Wearable CTA
	 */
	public CartPage clickNoRemoveWearableCTA() {
		try {
			removeWearableCTA.click();
			Reporter.log("No Remove Wearable CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click No Remove Wearable CTA");
		}
		return this;
	}

	/**
	 * Verify wearable is removed from cart
	 *
	 */
	public CartPage verifyWearableIsNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(wearableAddedInCart), "Web Offer Message is displayed");
			Reporter.log("Web Offer message is not displayed");
		} catch (AssertionError e) {
			Reporter.log("Web Offer message is displayed");
		}
		return this;
	}

	/**
	 * Click Wearable ineligible modal close CTA
	 */
	public CartPage clickWearableIneligibleModalCTA() {
		try {
			wearablesIneliigibleModalCloseCTA.click();
			Reporter.log("Wearable Ineligible Modal Close CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Wearable Ineligible Modal Close CTA");
		}
		return this;
	}

	/**
	 * Click First option in Add-ons and extras
	 *
	 */
	public CartPage clickFirstOptionInAddOnsAndExtras() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(seeMoreOptionsCTAAddOnsAndExtrasList.get(0));
				waitFor(ExpectedConditions.visibilityOf(eachLineFirstAddon.get(0)), 60);
				eachLineFirstAddon.get(0).click();
			} else {
				moveToElement(customerCartContainerHeaderOncart);
				waitFor(ExpectedConditions.visibilityOf(eachLineFirstAddon.get(0)), 60);
				eachLineFirstAddon.get(0).click();
			}
			Reporter.log("First option in AddOns and Extras clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click First option in AddOnsa and Extras ");
		}
		return this;
	}

	/*
	 * Verify Line Deposit Displayed
	 */
	public CartPage verifyLineDepositDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentLineDeposite.get(0)), 60);
			Assert.assertTrue(monthlyPaymentLineDeposite.get(0).isDisplayed(), "Line Deposit is not displayed");
			Reporter.log("Line Deposit  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Deposit");
		}
		return this;
	}

	/**
	 * Verify Line Deposit Price Displayed
	 */
	public CartPage verifyLineDepositPriceDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentLinePrice.get(0)), 60);
			Assert.assertTrue(monthlyPaymentLinePrice.get(0).isDisplayed(), "Line Deposit price is not displayed");
			Reporter.log("Line Deposit price  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Deposit price");
		}
		return this;
	}

	/**
	 * 
	 * Verify Add-ons are not present in monthly payment
	 */
	public CartPage verifyMonthlyPaymentAddonsLineIsNotDisplayed() {
		try {
			moveToElement(tradeDeviceBtnForEachLine.get(0));
			Assert.assertFalse(AddOnsLineItem.size() > 0, "Monthly payment Addon line item is displayed");
			Reporter.log("Add On Line Item is not displayed in monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment Addon line item is not displayed");
		}
		return this;
	}

	/**
	 * Click outside the Tooltip box
	 *
	 */
	public CartPage clickOutsideTheTooltipbox() {
		try {
			waitforSpinner();
			checkPageIsReady();
			monthlyPaymentHeader.get(0).click();
		} catch (Exception e) {
			Assert.fail("Failed to Click outside the Tooltip box ");
		}
		return this;
	}

	/**
	 * Click and verify Tool tip box for Add ons
	 *
	 */
	public CartPage clickOnTooltip(WebElement tooltip) {
		try {
			waitFor(ExpectedConditions.visibilityOf(tooltip), 60);
			Assert.assertTrue(tooltip.isDisplayed(), "Tool Tip icon is not displaying");
			tooltip.click();
		} catch (Exception e) {
			Assert.fail("Failed to Click on the Tooltip box ");
		}
		return this;
	}

	/**
	 * Verify Tool tip Description
	 *
	 */
	public CartPage verifyTooltipDescription() {
		try {
			// waitforSpinner();
			// scrollToElement(planDescriptionOnModal);
			waitFor(ExpectedConditions.visibilityOf(planDescriptionOnModal), 60);
			Assert.assertTrue(isElementDisplayed(planDescriptionOnModal),
					"Tool tip plan description is not displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Tool Tip Description.");
		}
		return this;
	}

	/**
	 * Click on Tooltip box
	 *
	 */
	public CartPage verifySelectedDeviceIsNotDisplayedOnCartPage() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(addedDeviceNotInCartPage),
					"Selected device is displayed in cart page");
			Reporter.log("Selected device is not displayed in cart page");
		} catch (AssertionError e) {
			Assert.fail("Failed to veriy device in cart page");
		}
		return this;
	}

	/**
	 * Verify Monthly Payment and One Time Payment Tile is Displayed
	 */
	public CartPage verifylineLevelPriceBlockForMultilineDisplayed() {
		moveToElement(lineLevelPricingBlock.get(0));
		try {
			for (WebElement lineLevelPricingBlockElement : lineLevelPricingBlock) {
				Assert.assertTrue(lineLevelPricingBlockElement.isDisplayed(),
						"Monthly Payment and One Time Payment Tile is not displayed");
			}
			for (WebElement monthlyPaymentHeader : monthlyPaymentHeader) {
				Assert.assertTrue(monthlyPaymentHeader.isDisplayed(), "Monthly Payment Header is not displayed");
			}
			for (WebElement oneTimePaymentHeader : oneTimePaymentHeader) {
				Assert.assertTrue(oneTimePaymentHeader.isDisplayed(), "One Time Payment Header is not displayed");
			}
			Reporter.log("Monthly Payment and One Time Payment Tile is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly Payment and One Time Payment Tile for All Lines ");
		}
		return this;
	}

	/**
	 * 
	 * Verify Device Cost Under One Time Payment (FTP)
	 */
	public CartPage verifyDeviceCostUnderOneTimePaymentFullTimePaymentDevice(String nwFrpPaymentPrice) {
		String frpCartPrice = priceBreakdownUnderOneTimePayment.get(0).getText().replace("$", "");
		try {
			waitforSpinner();
			Assert.assertTrue(priceBreakdownUnderOneTimePayment.get(0).isDisplayed(),
					"Device Price is not present Under One Time Payment");
			Assert.assertTrue(priceBreakdownUnderOneTimePayment.get(0).getText().contains("$"),
					"Device Price is not present Under One Time Payment");
			Assert.assertEquals(nwFrpPaymentPrice, frpCartPrice, "FRP Price on Cart Page and TMO PDP are not equal");
			Reporter.log("Device Cost DownPayment is displayed under One Time Payment(Full Time Payment)");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Cost DownPayment is displayed (Full Time Payment)");
		}
		return this;
	}

	/**
	 * 
	 * Verify Device Cost Under One Time Payment (MPD)
	 */
	public CartPage verifyDeviceCostUnderOneTimePaymentMonthlyPaymentDevice() {

		try {
			waitforSpinner();
			Assert.assertTrue(deviceLabelOneTimePayment.get(1).isDisplayed(),
					"Device label is not present Under One Time Payment");
			Assert.assertTrue(priceBreakdownUnderOneTimePayment.get(1).isDisplayed(),
					"Device Price is not present Under One Time Payment");
			Assert.assertTrue(priceBreakdownUnderOneTimePayment.get(1).getText().contains("$"),
					"Device Price is not present Under One Time Payment");
			Reporter.log(
					"Device Cost DownPayment and cost is displayed under One Time Payment(Monthly Payment device)");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Cost DownPayment and cost is displayed (Monthly Payment device)");
		}
		return this;
	}

	/**
	 * Verify Line Under Monthly Payment Displayed
	 */
	public CartPage verifyLineUnderMonthlyPaymentDisplayedMultiline() {
		try {
			for (WebElement linelabel : monthlyPaymentLineDeposite) {
				Assert.assertTrue(linelabel.isDisplayed(), "Line label under Monthly Payment is not displayed");
			}
			for (WebElement linePrice : monthlyPaymentLinePrice) {
				Assert.assertTrue(linePrice.isDisplayed(), "Line Price under Monthly Payment is not displayed");
				Assert.assertTrue(linePrice.getText().contains("$"),
						"Line Price under Monthly Payment is not displayed");
			}
			Reporter.log("Line Under Monthly Payment is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Under Monthly Payment for All Lines ");
		}
		return this;
	}

	/**
	 * Verify Device Under Monthly Payment Displayed
	 */
	public CartPage verifyDeviceUnderMonthlyPaymentDisplayedMultiline() {
		try {
			for (WebElement deviceLabel : monthlyPaymentDevice) {
				Assert.assertTrue(deviceLabel.isDisplayed(), "Device label under Monthly Payment is not displayed");
			}
			for (WebElement devicePrice : monthlyPaymentDevicePrice) {
				Assert.assertTrue(devicePrice.isDisplayed(), "Device Price under Monthly Payment is not displayed");
				Assert.assertTrue(devicePrice.getText().contains("$"),
						"Device Price under Monthly Payment is not displayed");
			}
			Reporter.log("Device Under Monthly Payment/CRP Displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Under Monthly Payment/CRP Displayed for All Lines ");
		}
		return this;
	}

	/**
	 * Verify Discount Under Monthly Payment Displayed
	 */
	public CartPage verifyDiscountUnderMonthlyPaymentDisplayedMultiline() {
		try {
			for (WebElement discountLabel : monthlyPaymentDiscount) {
				Assert.assertTrue(discountLabel.isDisplayed(), "Discount label under Monthly payment is not displayed");
			}
			for (WebElement discountPrice : monthlyPaymentDiscountPrice) {
				Assert.assertTrue(discountPrice.isDisplayed(), "Discount Price under Monthly payment is not displayed");
				Assert.assertTrue(discountPrice.getText().contains("$"),
						"Discount Price under Monthly payment is not displayed");
			}
			Reporter.log("Discount Under Monthly Payment is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Discount Under Monthly Payment for All Lines ");
		}
		return this;
	}

	/**
	 * Verify Taxes and Shipping under One Time Payment
	 */
	public CartPage verifyTaxesAndShippingDisplayedMultiline() {
		try {
			for (WebElement taxesAndShipping : taxesAndShippingTextMultiline) {
				Assert.assertTrue(taxesAndShipping.isDisplayed(),
						"Taxes and Shipping under One Time payment is not displayed");
			}
			Reporter.log("Taxes and Shipping Under One Time Payment is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Taxes and Shipping Under One Time Payment for All Lines ");
		}
		return this;
	}

	/**
	 * Verify Total/Monthly under One Time Payment
	 */
	public CartPage verifyTotalMontlybrakdownDisplayedMultiline() {
		try {
			for (WebElement priceBreakdown : priceBreakdownUnderOneTimePayment) {
				Assert.assertTrue(priceBreakdown.isDisplayed(),
						"Total/Monthly Breakdown under One Time payment is not displayed");
			}
			for (WebElement priceBreakdown : priceBreakdownTodayPrice) {
				Assert.assertTrue(priceBreakdown.isDisplayed(), "Total price under One Time payment is not displayed");
			}
			for (WebElement priceBreakdown : priceBreakdownMonthlyPrice) {
				Assert.assertTrue(priceBreakdown.isDisplayed(),
						"Monthly price under One Time payment is not displayed");
			}
			Reporter.log("Total/Monthly Section Under One Time Payment is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Total/Monthly Section Under One Time Payment for All Lines ");
		}
		return this;
	}

	/**
	 * Verify AutoPay Message under One Time Payment
	 */
	public CartPage verifyAutoPayMessageDisplayedMultiline() {
		try {
			for (WebElement autoPayMessage : priceBreakdownAutoPaymentMessage) {
				Assert.assertTrue(autoPayMessage.isDisplayed(),
						"Includes $5 monthly Autopay Discount under One Time payment is not displayed");
			}
			Reporter.log("Includes $5 monthly Autopay Discount Under One Time Payment is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Includes $5 monthly Autopay Discount Under One Time Payment for All Lines ");
		}
		return this;
	}

	/**
	 * Verify Taxes and Shipping under One Time Payment
	 */
	public CartPage verifyTaxesAndShippingDisplayedMultilineOTP() {
		try {
			for (WebElement taxesAndShipping : taxesAndShippingTextMultiline) {
				Assert.assertTrue(taxesAndShipping.isDisplayed(),
						"Taxes and Shipping under One Time payment is not displayed");
			}
			Reporter.log("Taxes and Shipping Under One Time Payment is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Taxes and Shipping Under One Time Payment for All Lines ");
		}
		return this;
	}

	/**
	 * Verify Device Under One Time Payment Displayed
	 */
	public CartPage verifyDeviceUnderOneTimePaymentDisplayedMultiline() {
		try {
			for (WebElement discountLabel : deviceLabelOneTimePayment) {
				Assert.assertTrue(discountLabel.isDisplayed(), "Device label under One Time payment is not displayed");
			}
			for (WebElement discountPrice : priceBreakdownUnderOneTimePayment) {
				Assert.assertTrue(discountPrice.isDisplayed(), "Device Price under One Time payment is not displayed");
				Assert.assertTrue(discountPrice.getText().contains("$"),
						"Device Price under One Time payment is not displayed");
			}
			Reporter.log("Device Under One Time Payment is displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Under One Time Payment for All Lines ");
		}
		return this;
	}

	/**
	 * Click on Get In Line Button
	 * 
	 * @return
	 */
	public CartPage verifyAndclickOnGetInLineCTA() {
		try {
			getInLineCTA.isEnabled();
			getInLineCTA.click();
			Reporter.log("'Get in line' CTA was highlighted and clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Get in line' CTA");
		}
		return this;
	}

	/**
	 * Click on Find NearBy stores link
	 * 
	 * @return
	 */
	public CartPage clickFindNearByStoresOnSaveCartModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(findNearByStoresInSavecartModalPopup), 60);
			findNearByStoresInSavecartModalPopup.click();
			Reporter.log("Find Nearby Stores link clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to click Find Nearby Stores link.");
		}
		return this;
	}

	/**
	 * Select second Filtered stores
	 */
	public CartPage selectSecondFilteredStore() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(filteredStoreslist), 60);
			filteredStoreslist.get(1).click();
			Reporter.log("Store selected successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to select Store from the list");
		}
		return this;
	}

	/**
	 * Verify Store name in filtered Search results
	 *
	 */
	public CartPage verifySelectedStoreInFilteredSearch(String storeName) {
		try {
			checkPageIsReady();
			assertTrue(storeNameInFilteredSearch.getText().equals(storeName), "Selected store is not displaying");
			Reporter.log("Store name value successfully verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify Store name value");
		}
		return this;
	}

	/**
	 * Click Find A Store
	 *
	 */
	public CartPage clickFindAStoreOnRetailSaveCartModal() {
		try {
			findAStore.click();
			Reporter.log("Find a Store CTA was highlighted and clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to click Find a Store CTA ");
		}
		return this;
	}

	/**
	 * Click Find Nearby Stores for Device
	 *
	 */
	public CartPage clickFindNearbyStoresForDevice() {
		try {
			Assert.assertTrue(findNearbyStoresForDeviceLink.get(0).isDisplayed(),
					"Find nearby stores link is not Displayed.");
			findNearbyStoresForDeviceLink.get(0).click();
			;
			Reporter.log("Find Nearby Stores link clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Change Store Location.");
		}
		return this;
	}

	/**
	 * Verify List of Add On and Extras Header Displayed
	 */
	public CartPage verifyListOfCheckBoxDisplayed() {
		try {
			int index = 0;
			for (WebElement listOfCheckBoxAddOnsElement : listOfCheckBoxForAddOns) {
				if (listOfCheckBoxAddOnsElement.isDisplayed()) {
					index = index + 1;
				}
			}
			if (index > 0) {
				Reporter.log("List of Check Box for Add On and Extras Header is Displayed");
			} else {
				Assert.fail("List of CheckBox for  Add On and Extras Header is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Failed to verify List of Price Add On and Extras Header is Displayed");
		}
		return this;
	}

	/**
	 * Verify error message for email
	 *
	 */
	public List<String> verifyPricesForAddOns() {
		List<String> protection360prices = new ArrayList<String>();
		try {
			for (WebElement addOn : addOnsListFromTiles) {
				if (addOn.getText().contains("Protection")) {
					String service = addOn.getText();
					String price = service.substring(service.lastIndexOf("$") + 1, service.indexOf("M"));
					protection360prices.add(price.trim());
				}
			}
			Reporter.log("Service prices are collected: " + protection360prices);
		} catch (Exception e) {
			Assert.fail("Failed to collect service prices");
		}
		return protection360prices;
	}

	/**
	 * Click On close CTA
	 */
	public CartPage clickSelectServicesCloseIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(closeSelectServicesIcon), 60);
			closeSelectServicesIcon.click();
			Reporter.log("Clicked on close icon on Select Services modal");
		} catch (Exception e) {
			Assert.fail("Failed to click on close icon in Select Services modal");
		}
		return this;
	}

	/**
	 * Compare arrays
	 */
	public boolean comparePrices(List<String> firstArray, List<String> secondArray) {
		Boolean areTheyEqual = false;
		try {
			if (firstArray.equals(secondArray)) {
				areTheyEqual = true;
			}
		} catch (Exception e) {
			Assert.fail("Failed to compare prices");
		}
		return areTheyEqual;
	}

	/**
	 * Compare arrays
	 */
	public CartPage verifyPricesHaveBeenChanged(List<String> firstArray, List<String> secondArray) {
		try {
			Assert.assertFalse(comparePrices(firstArray, secondArray), "Prices are same, but should be updates");
			Reporter.log("Service prices are updated");
		} catch (Exception e) {
			Assert.fail("Failed to verify are prices equal or not");
		}
		return this;
	}

	/**
	 * verify duplicate CTA for all lines
	 *
	 */
	public CartPage verifyDuplicateCTADisabledForAllLines() {
		try {
			checkPageIsReady();
			for (WebElement duplicatecta : duplicateLineDisabled) {
				Assert.assertTrue(duplicatecta.isDisplayed(), "Duplicate CTA for all lines is not disabled");
			}
			Reporter.log("Duplicate CTA for all lines is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Duplicate CTA for all lines");
		}
		return this;
	}

	/**
	 * Verify device in cart
	 */
	public CartPage verifyDeviceInCart() {
		try {
			Assert.assertTrue(deviceInCart.isDisplayed(), "Device is not added in cart");
			Reporter.log("Device is added to cart");
		} catch (Exception e) {
			Assert.fail("Failed to add device to cart");
		}
		return this;
	}

	/**
	 * Verify Magenta Plan header
	 */
	public CartPage verifyMagentaPlanHeader() {
		try {
			Assert.assertTrue(activePlan.isDisplayed(), "Magenta plan header not displayed");
			Assert.assertTrue(activePlan.getText().contains("Magenta"), "Magenta header not displayed");
			Reporter.log("Magenta plan header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Magenta plan header.");
		}
		return this;
	}

	/**
	 * Verify Plan Item for line contains plan Magenta
	 *
	 */
	public CartPage verifyPlanItemForLineIsMagenta() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			for (WebElement plan : planItemForLineList) {
				Assert.assertTrue(plan.isDisplayed(), "Plan Item for line is not displayed");
				Assert.assertTrue(plan.getText().contains("Magenta"),
						"Plan Item for line does not contains Magenta Plan");
			}
			Reporter.log("Magenta plan is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Magenta plan");
		}
		return this;
	}

	/**
	 * Verify Add Ons And Extra Section
	 */
	public CartPage verifyaddOnsAndExtraSection() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addOnsAndExtraSection), 60);
			Assert.assertTrue(addOnsAndExtraSection.isDisplayed(), "Add Ons And Extra Section not displayed");
			Reporter.log("Add Ons And Extra Section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Add Ons And Extra Section.");
		}
		return this;
	}

	/**
	 * Verify Magenta Plan section
	 */
	public CartPage verifyMagentaPlanSection() {
		try {
			Assert.assertTrue(megentaPlan.isDisplayed(), "Magenta plan section not displayed");
			Reporter.log("Magenta plan section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Magenta plan section.");
		}
		return this;
	}

	/**
	 * Verify Default Add On's for Magenta plan
	 */
	public CartPage verifyDefaultAddOnsForMegentaPlan() {
		try {
			waitforSpinner();
			for (WebElement listOfAddOnsElement : listOfAdOnsAvailable) {
				Assert.assertTrue(listOfAddOnsElement.isDisplayed(),
						"Default addon's not displayed in add on's container");
				Assert.assertTrue(
						listOfAddOnsElement.getText().contains("Plus")
								|| listOfAddOnsElement.getText().contains("Protection<360>™"),
						"default addon's Plus Up and Protection<360>™ not displayed");
			}
			Reporter.log("default addon's Plus Up and Protection<360>™ is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify default addon's Plus Up and Protection<360>™");
		}
		return this;
	}

	/**
	 * Verify Tmobile essential should display in first place
	 */
	public CartPage verifyMegentaPlanTileDisplayRightOfTmobileEssentialPlan() {
		try {
			waitforSpinner();
			Assert.assertTrue(planHeaderFromPlansSection.get(1).getText().contains("T-Mobile Essentials"),
					"Megenta plan section is not displayed right of t-mobile essential plan");
			Assert.assertTrue(
					TMobileEssentialsPlanDescription.getText()
							.contains("The basics: Unlimited talk, text, and 4G LTE data."),
					" t-mobile essential plan description not displayed");
			Reporter.log("Megenta plan is displaying right of t-mobile essential plan");
		} catch (Exception e) {
			Assert.fail("Failed to verify megenta and t-mobile essential plan verification ");
		}
		return this;
	}

	/**
	 * verify Store locator map
	 */
	public CartPage verifySelectedStore(String storeName) {
		try {
			String displayedStore = null;
			for (WebElement name : storesInStoreListPage) {
				displayedStore = name.getText();
				assertTrue(displayedStore.equals(storeName), "Selected store is not displaying");
				break;
			}
			Reporter.log("Selected store is  displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify selected store");
		}
		return this;
	}

	/**
	 * Verify List of Add On and Extras Header Displayed
	 */
	public CartPage verifyEssentialMagentaAndMagentaPlusPlanSectionDisplayed() {
		try {
			int index = 0;
			for (WebElement plan : allPlanSection) {
				if (plan.isDisplayed()) {
					index = index + 1;
				}
			}
			if (index == 3) {
				Reporter.log("All 3 plan are displayed");
			} else {
				Assert.fail("All 3 plan are not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify all the plan");
		}
		return this;
	}

	/**
	 * Verify List of Add On and Extras Header Displayed
	 */
	public CartPage verifyEssentialMagentaAndMagentaPlusPlanSectionNotDisplayed() {
		try {
			int index = 0;
			for (WebElement plan : allPlanSection) {
				if (plan.isDisplayed()) {
					index = index + 1;
				}
			}
			if (index < 3) {
				Reporter.log("All 3 plan are not displayed");
			} else {
				Assert.fail("All 3 plan are displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify all the plan");
		}
		return this;
	}

	/**
	 * click on Magenta Plus Plan
	 */
	public CartPage clickOnMagentaPlusPlan() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(megentaPlusPlanHeader));
			moveToElement(megentaPlusPlanHeader);
			megentaPlusPlanHeader.click();
			waitforSpinner();
			Reporter.log("Clicked on Megente Plus Plan");
		} catch (Exception e) {
			Assert.fail("Failed to click on megenta Plus Plan");
		}
		return this;
	}

	/**
	 * Verify Plan Item for line contains plan T-Mobile One
	 *
	 */
	public CartPage verifyPlanItemForLineIsMagentaPlus() {

		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			for (WebElement plan : planItemForLineList) {
				Assert.assertTrue(plan.isDisplayed(), "Plan Item for line is not displayed");
				Assert.assertTrue(plan.getText().contains("Plus"),
						"Plan Item for line does not contains Magenta Plus Plan");
			}
			Reporter.log("Magenta Plus Plan is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Magenta Plus Plan");
		}
		return this;
	}

	/**
	 * To select checkbox
	 *
	 */
	public CartPage selectCheckbox() {
		try {
			checkBoxStatus.click();
			Reporter.log("Selected check box");
		} catch (Exception e) {
			Assert.fail("Failed to select checkbox");
		}
		return this;
	}

	/**
	 * Validate Magenta Plus Plan section is disabled
	 */
	public CartPage verifyMagentaPlusPlanSectionDisabled() {
		try {
			Assert.assertTrue(magentaPlusPlanTile.getAttribute("class").contains("disableplan"),
					"Magenta Plus Plan section is not disabled");
			Reporter.log("Magenta Plus Plan section is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Magenta Plus Plan section disabled");
		}
		return this;
	}

	/**
	 * Verify Tablet Line Discount on Order Details Modal
	 */
	public CartPage verifyTabletLineDiscountOnOrderDetailsModal() {
		try {
			Assert.assertTrue(tabletLineDiscountOrderDetailsModal.getText().contains("$45.00 tablet line discount"),
					"Tablet line discount is displayed incorrectly");
			Reporter.log("Tablet line discount is displayed correct");
		} catch (Exception e) {
			Assert.fail("Failed to display Tablet line discount");
		}
		return this;
	}

	/**
	 * Verify Discount In Monthly Payment Displayed $50 =$45 + $5 AutoPay
	 */
	public CartPage verifyDiscountInMonthlyPaymentDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyPaymentDiscountPrice.get(0)), 60);
			Assert.assertTrue(monthlyPaymentDiscountPrice.get(0).getText().contains("$50.00"),
					"Tablet line discount is displayed incorrectly in Monthly Line");
			Reporter.log("Tablet line discount is displayed in Monthly Line");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tablet line discount in Monthly Line");
		}
		return this;
	}

	/**
	 * Verify Taxes line is hidden for Magenta Plus Plan tile
	 *
	 */
	public CartPage verifymagentaPlusPlanTaxesTextNotDisplayed() {

		try {
			waitForSpinnerInvisibility();
			Assert.assertFalse(isElementDisplayed(magentaPlusPlanTaxesText),
					"Taxes line is not hidden for Magenta Plus Plan tile");
			Reporter.log("Taxes line is hidden for Magenta Plus Plan tile");
		} catch (Exception e) {
			Assert.fail("Failed to verify Taxes line for Magenta Plus Plan tile");
		}
		return this;
	}

	/**
	 * Verify pricing details for Magenta Plus Plan tile
	 *
	 */
	public CartPage verifymagentaPlusPlanPricingDetailsNotDisplayed() {

		try {
			waitForSpinnerInvisibility();
			Assert.assertFalse(isElementDisplayed(magentaPlusPlanPricingDetails),
					"Pricing details are not hidden for Magenta Plus Plan tile");
			Reporter.log("Pricing details are hidden for Magenta Plus Plan tile");
		} catch (Exception e) {
			Assert.fail("Failed to verify pricing details for Magenta Plus Plan tile");
		}
		return this;
	}

	/**
	 * Validate Magenta Plus Plan error message is disabled
	 */
	public CartPage verifyMagentaPlusPlanErrorMessageDisabled() {
		try {
			Assert.assertTrue(magentaPlusAuthorableErrorMessage.getAttribute("ng-bind-html").contains("Disable"),
					"Magenta Plus Plan error message is not disabled");
			Reporter.log("Magenta Plus Plan error message is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Magenta Plus Plan error message disabled");
		}
		return this;
	}

	/**
	 * Validate Magenta Plus Plan is Enabled
	 */
	public CartPage verifyMagentaPlusPlanEnabled() {
		try {
			Assert.assertFalse(magentaPlusPlanTile.getAttribute("class").contains("disableplan"),
					"Magenta Plus Plan section is disabled");
			Reporter.log("Magenta Plus Plan is enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Magenta Plus Plan");
		}
		return this;
	}

	/**
	 * Get Monthly Payment LinePrice
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentLinePrice() {
		String linePrice = null;
		try {
			String monthlyPaymentsLinePrice;
			checkPageIsReady();
			monthlyPaymentsLinePrice = monthlyPaymentLinePrice.get(0).getText();
			linePrice = monthlyPaymentsLinePrice.substring(monthlyPaymentsLinePrice.lastIndexOf("$") + 1);
		} catch (Exception e) {
			Assert.fail("Failed to get Monthly Payment Line Price");
		}
		return Double.parseDouble(linePrice);
	}

	/**
	 * Verify that Monthly payment line prices are not equal
	 */
	public CartPage compareMonthlyPaymentsLinePrices(double V1, double V2) {
		try {
			Assert.assertNotEquals(V1, V2, " MonthlyPayments Lines are equal");
			Reporter.log("MonthlyPayments Lines are not equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Monthly Payments Lines price");
		}
		return this;
	}

	/**
	 * Verify that Monthly Payment line price is equal
	 */
	public CartPage verifyMonthlyPaymentsLinePrices(double V1, double V2) {
		try {
			Assert.assertEquals(V1, V2, "MonthlyPayments Lines are not equal");
			Reporter.log("Total price matching calculations");
		} catch (Exception e) {
			Assert.fail("Failed to compare Monthly Payments Lines price");
		}
		return this;
	}

	/**
	 * Click Protection 360 Add ons service in Add-ons and extras
	 *
	 */
	public CartPage clickProtectionInAddOnsAndExtras() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(protectionAddon);
			Reporter.log("Protection in AddOns and Extras clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Protection in AddOns and Extras ");
		}
		return this;
	}

	/**
	 * Click Plus UP Add ons service in Add-ons and extras
	 *
	 */
	public CartPage clickFirstAddOn() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(eachLineFirstAddon.get(0)), 60);
			moveToElement(tradeDeviceBtnForEachLine.get(0));
			eachLineFirstAddon.get(0).click();
			waitForSpinnerInvisibility();
			Reporter.log("Protection plus up in AddOns and Extras clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Protection in AddOns and Extras ");
		}
		return this;
	}

	/**
	 * Click Plus UP Add ons service in Add-ons and extras for second line
	 *
	 */
	public CartPage clickFirstAddOnForSecondLine() {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			moveToElement(tradeDeviceBtnForEachLine.get(1));
			eachLineFirstAddon.get(1).click();
			Reporter.log("plus up in AddOns and Extras clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click plus up in AddOns and Extras ");
		}
		return this;
	}

	public CartPage verifyProtectionAddonsService() {
		try {
			checkPageIsReady();
			Assert.assertTrue(serviceFamilyName.getText().contains("Protection"), "Addons service is not displayed");
			Reporter.log("Addons service is displayed");
		} catch (Exception e) {
			Assert.fail("Addons service is not displayed");
		}
		return this;
	}

	/**
	 * Verify FRP price for accessory is displayed
	 * 
	 */
	public CartPage verifyAccessoryFrpPriceInAccessoryTileIsDisplayed() {
		try {
			Assert.assertTrue(priceOfAccessoryTodayOnCart.isDisplayed(),
					"Today price for accessory is not displayed in accessory tile");
		} catch (Exception e) {
			Assert.fail("Failed to verify accessory FRP in accessory tile");
		}
		return this;
	}

	/**
	 * Verify EIP price for accessory is not present
	 * 
	 */
	public CartPage verifyAccessoryIepPriceNotDisplayedInAccessoryTile() {
		try {
			Assert.assertFalse(isElementDisplayed(priceOfAccessoryMonthlyOnCart),
					"Monthly price for accessory is not displayed in accessory tile");
		} catch (Exception e) {
			Assert.fail("Failed to verify accessory FRP in accessory tile");
		}
		return this;
	}

	/**
	 * Get BYOD SimStarter Kit Price for phones
	 */
	public Double getBYODPriceForPhones() {
		String simStarterKitPricestr = "";
		try {
			checkPageIsReady();
			String simStarterKit;
			simStarterKit = simstarterKitPrice.get(2).getText();
			simStarterKitPricestr = simStarterKit.substring(simStarterKit.lastIndexOf("$") + 1);
		} catch (Exception e) {
			Assert.fail("Failed to Get SimStarter Kit Price");
		}

		return Double.parseDouble(simStarterKitPricestr);
	}

	/**
	 * Get BYOD SimStarter Kit Price for tablets
	 */
	public Double getBYODPriceForTablet() {
		String simStarterKitPricestr = "";
		try {
			checkPageIsReady();
			String simStarterKit;
			simStarterKit = simstarterKitPrice.get(3).getText();
			simStarterKitPricestr = simStarterKit.substring(simStarterKit.lastIndexOf("$") + 1);
		} catch (Exception e) {
			Assert.fail("Failed to Get SimStarter Kit Price");
		}

		return Double.parseDouble(simStarterKitPricestr);
	}

	/**
	 * Get BYOD SimStarter Kit Price for SIM
	 */
	public Double getBYODPriceForSIM() {
		String simStarterKitPricestr = "";
		try {
			checkPageIsReady();
			String simStarterKit;
			simStarterKit = simstarterKitPrice.get(0).getText();
			simStarterKitPricestr = simStarterKit.substring(simStarterKit.lastIndexOf("$") + 1);
		} catch (Exception e) {
			Assert.fail("Failed to Get SimStarter Kit Price");
		}

		return Double.parseDouble(simStarterKitPricestr);
	}

	/**
	 * Compare Price of BYOD devices for Phones, When user swith one plan to another
	 * plan
	 *
	 */
	public CartPage compareFRPPricesForBYODPhones(Double magentaPlanBYODPriceForPhones,
			Double essentialsPlanBYODPriceForPhones) {
		try {
			Assert.assertEquals(magentaPlanBYODPriceForPhones, essentialsPlanBYODPriceForPhones,
					"Price of BYOD deves not equal");
			Reporter.log("Price of BYOD deves is equal, When user switch one plan to another plan");
		} catch (Exception e) {
			Assert.fail("Failed to compare price BYOD devices for Phones");
		}
		return this;
	}

	/**
	 * Compare Price of BYOD devices for Tablets, When user swith one plan to
	 * another plan
	 *
	 */
	public CartPage compareFRPPricesForBYODTablets(Double magentaPlanBYODPriceForTablet,
			Double essentialsPlanBYODPriceForTablet) {
		try {
			Assert.assertEquals(magentaPlanBYODPriceForTablet, essentialsPlanBYODPriceForTablet,
					"Price of BYOD deves not equal");
			Reporter.log("Price of BYOD deves is equal, When user switch one plan to another plan");
		} catch (Exception e) {
			Assert.fail("Failed to compare price BYOD devices for Phones");
		}
		return this;
	}

	/**
	 * Enter valid email with date in Save cart modal popup
	 *
	 */
	public CartPage enterEmailWithDateInSaveCart(String email) {
		try {
			sendTextData(emailField, email);
			emailField.sendKeys(Keys.TAB);
			Reporter.log("Email with date has been entered: " + email);
		} catch (Exception e) {
			Assert.fail("Falied to enter email with date in email field");
		}
		return this;
	}

	/**
	 * Get cartId from cookies
	 *
	 */
	public String getCartIdFromCookiesInCartPage() {
		String cartId = null;
		try {
			String encoded = getDriver().manage().getCookieNamed("tmobglobalshareddata").getValue();
			String s = URLDecoder.decode(encoded, "utf8");
			JSONObject jsonObject = new JSONObject(s);
			cartId = jsonObject.getString("cartId");
			Reporter.log("CartId retrieved from cookies on Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve cartId from cookies on Cart Page");
		}
		return cartId;
	}

	/**
	 * Get email with date
	 *
	 */
	public String getEmailWithDate(TMNGData tMNGData) {
		String email = null;
		try {
			Date date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
			email = tMNGData.getEmail() + dateFormat.format(date);
			Reporter.log("Email with date on the end is created: " + email);
		} catch (Exception e) {
			Assert.fail("Failed to create email with date at the end on Cart Page");
		}
		return email;
	}

	/**
	 * Add wearable to Cart from cart Page.
	 *
	 */
	public CartPage addAWearabletoCartFromCartPage(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddAWearableLinkOnCart();

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyWatchesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyWatchesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Add Phone to Cart from cart Page.
	 *
	 */
	public CartPage addAPhonetoCartFromCartPage(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddAPhoneLinkOnCart();

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getSecondDeviceName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Add Tablet to Cart from cart Page.
	 *
	 */
	public CartPage addATablettoCartFromCartPage(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddATabletLinkOnCart();

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Add BYOD Tablet to Cart from cart Page through PLP
	 *
	 */
	public CartPage addAByodTabletToCartFromCartPageFromPLP(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddATabletLinkOnCart();

		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		phonesPlpPage.selectSimCardFromPlp();

		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Complete TPP in cart page SCC
	 *
	 */
	public CartPage completeTPPInCartPage(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tPPOfferPage = new TPPOfferPage(getDriver());
		tPPOfferPage.verifyTPPOfferPage();
		tPPOfferPage.clickGotItCTA();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Add BYOD Phone to Cart from cart Page through PLP
	 *
	 */
	public CartPage addAByodPhoneToCartFromCartPageFromPLP(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddAPhoneLinkOnCart();
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.selectSimCardFromPlp();

		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Add Accessory to Cart from cart Page.
	 *
	 */
	public CartPage addAnAccessorytoCartFromCartPage(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getSecondAccessoryName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * verify Find Your Pricing Link in Cart
	 *
	 */
	public CartPage verifyFindYourPricingCTA() {
		try {
			if(getDriver() instanceof AppiumDriver) {
				
				if (isElementDisplayed(findYourPriceButtonMobile)) {
					Assert.assertTrue(findYourPriceButtonMobile.isDisplayed(),
							"Find Your Pricing Link/CTA is not displaying in Cart");
					Reporter.log("Find Your Pricing Link/CTA is displaying in Cart");
				} else {
					getDriver().navigate().to(getDriver().getCurrentUrl() + "?mboxDisable=true");
					verifyCartPageLoaded();
					Assert.assertTrue(findYourPriceButtonMobile.isDisplayed(),
							"Find Your Pricing Link/CTA is not displaying in Cart");
					Reporter.log("Find Your Pricing Link/CTA is displaying in Cart");
				}
			}else {
			if (isElementDisplayed(findYourPriceButton)) {
				Assert.assertTrue(findYourPriceButton.isDisplayed(),
						"Find Your Pricing Link/CTA is not displaying in Cart");
				Reporter.log("Find Your Pricing Link/CTA is displaying in Cart");
			} else {
				getDriver().navigate().to(getDriver().getCurrentUrl() + "?mboxDisable=true");
				verifyCartPageLoaded();
				Assert.assertTrue(findYourPriceButton.isDisplayed(),
						"Find Your Pricing Link/CTA is not displaying in Cart");
				Reporter.log("Find Your Pricing Link/CTA is displaying in Cart");
			}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Link/CTA in Cart");
		}
		return this;
	}

	/**
	 * Click Find Your Pricing Link in Cart
	 *
	 */
	public CartPage clickFindYourPricingCTA() {
		try {
			if(getDriver() instanceof AppiumDriver) {
				if (isElementDisplayed(findYourPriceButtonMobile)) {
				//	clickElementWithJavaScript(findYourPriceButtonMobile);
					findYourPriceButtonMobile.click();
					Reporter.log("Clicked on Find Your Pricing Link/CTA");
				} else {
					getDriver().navigate().to(getDriver().getCurrentUrl() + "?mboxDisable=true");
					verifyCartPageLoaded();
					clickElementWithJavaScript(findYourPriceButtonMobile);
					Reporter.log("Clicked on Find Your Pricing Link/CTA");
				}
			}else {
			if (isElementDisplayed(findYourPriceButton)) {
				clickElementWithJavaScript(findYourPriceButton);
				Reporter.log("Clicked on Find Your Pricing Link/CTA");
			} else {
				getDriver().navigate().to(getDriver().getCurrentUrl() + "?mboxDisable=true");
				verifyCartPageLoaded();
				clickElementWithJavaScript(findYourPriceButton);
				Reporter.log("Clicked on Find Your Pricing Link/CTA");
			}
			}
		} catch (Exception e) {
			Assert.fail("Failed to Click Find Your Pricing Link/CTA in Cart");
		}
		return this;
	}

	/**
	 * verify New Prices based on credit class in cart
	 *
	 */
	public CartPage verifyNewPriceBasedOnCreditClass() {
		try {
			Assert.assertTrue(newPricesBasedOnCreditClass.isDisplayed(),
					"New Prices are not displaying in cart after credit check");
			Reporter.log("New Prices are updated in cart after credit check");
		} catch (Exception e) {
			Assert.fail("Failed to verify New Price based on credit class in cart.");
		}
		return this;
	}

	/**
	 * Verify Edit CTA for Tablet Not displayed in Cart page
	 */
	public CartPage verifyEditCTAForAllLinesNotDisplayed() {
		try {
			if (editCTA.size() > 0)
				Assert.assertFalse(false, "Edit CTA for Phone,tablet,wearable are displayed on cart");
			if (editCTAForAccessory.size() > 0)
				Assert.assertFalse(false, "Edit CTA for Accessory is displayed on cart");
			Reporter.log("Edit CTA for All lines not displayed on cart");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit CTA for All lines on cart");
		}
		return this;
	}

	/**
	 * Verify FRP Price For Device Is displayed on Cart page
	 */
	public CartPage verifyFRPPriceForDeviceIsDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(priceBreakdownUnderOneTimePayment.get(0)),
					"FRP Price For Device is not displayed on Cart page");
			Reporter.log("FRP Price For Device Is displayed on Cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP Price For Device Is displayed on Cart page");
		}
		return this;
	}

	/**
	 * Verify Sim Starter Kit Under One Time Payment Not Displayed
	 */
	public CartPage verifySimStarterKitNotDisplayedMultiline() {
		try {
			for (WebElement simkit : simstarterKitLabel) {
				Assert.assertFalse(isElementDisplayed(simkit),
						"Sim Starter Kit label under One Time payment is displayed");
			}
			for (WebElement simkitPrice : simstarterKitPrice) {
				Assert.assertFalse(isElementDisplayed(simkitPrice),
						"Sim Starter Kit label under One Time payment is displayed");
			}
			Reporter.log("Sim Starter Kit Under One Time Payment is Not displayed for All Lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sim Starter Kit Under One Time Payment for All Lines ");
		}
		return this;
	}

	/**
	 * 
	 * Verify MI Sim Kit name displayed
	 */
	public CartPage verifyMiSimKitNameDisplayed() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(miSimKitName), 60);
			Assert.assertTrue(miSimKitName.isDisplayed(), "MI Sim Kit name is not displayed");
			Reporter.log("MI Sim kit name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display MI Sim Kit name");
		}
		return this;
	}

	/**
	 * 
	 * Verify Sim Card Text
	 */
	public CartPage verifySimCardTextDisplayed() {
		try {
			waitforSpinner();
			for (WebElement simkit : simstarterKitLabel) {
				Assert.assertTrue(simkit.isDisplayed(),
						"Sim Starter Kit label under One Time payment is not displayed");
				Assert.assertTrue(simkit.getText().contains("SIM Card"), "Sim Starter Kit label contains Sim Card");
			}
			Reporter.log("MI Sim Kit name contains Sim Card");
		} catch (Exception e) {
			Assert.fail("Failed to verify if MI sim kit name contains sim card");
		}
		return this;
	}

	/**
	 * 
	 * Verify TPP Sticky AutoPay Message Displayed
	 */
	public CartPage verifyStickyBannerAutoPayTextDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(autoPaymentMessageStickyBannerMobile.isDisplayed(),
						"Auto Pay price Text option is not Displayed");
			} else {
				waitforSpinner();
				Assert.assertTrue(autoPaymentMessageStickyBanner.isDisplayed(),
						"TPP Sticky Banner AutoPay Message Not Displayed");
				Assert.assertTrue(stickeyBannertotalAutoPayDiscount.isDisplayed(),
						"TPP Sticky Banner AutoPay Message Not Displayed");
				Assert.assertTrue(tppStickyBannerAutoPayEndingText.isDisplayed(),
						"TPP Sticky Banner TAutoPay Message Not Displayed");
				Reporter.log("TPP Sticky Banner AutoPay Message Displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify TPP Sticky Banner AutoPay Message Displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify Sim kit name on order details modal
	 */
	public CartPage verifySimKitNameOnOrderDetailsModalDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(simKitNameOnOrderDetailsModal.isDisplayed(),
					"Sim kit name is Not Displayed on order details modal");
			Reporter.log("Sim kit name on order details modal is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify sim name on order details modal");
		}
		return this;
	}

	/**
	 * 
	 * Verify Sim kit name on order details modal
	 */
	public CartPage verifySimKitPriceOnOrderDetailsModalDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(simKitPriceOnOrderDetailsModal.isDisplayed(),
					"Sim kit price is Not Displayed on order details modal");
			Reporter.log("Sim kit price on order details modal is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify sim price on order details modal");
		}
		return this;
	}

	/**
	 * Get Monthly Payment LinePrice
	 * 
	 * @return
	 */
	public Double getMonthlyLineTotalPriceForFirstItem() {
		String monthlyLineTotal;
		monthlyLineTotal = priceBreakdownMonthlyPrice.get(0).getAttribute("aria-label");
		String linePrice = monthlyLineTotal.substring(monthlyLineTotal.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Monthly Payment LinePrice
	 * 
	 * @return
	 */
	public Double getMonthlyLineTotalPriceForSecondItem() {
		String monthlyLineTotal;
		monthlyLineTotal = priceBreakdownMonthlyPrice.get(1).getAttribute("aria-label");
		String linePrice = monthlyLineTotal.substring(monthlyLineTotal.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Monthly Payment LinePrice
	 * 
	 * @return
	 */
	public Double getMonthlyLineTotalPriceForThirdItem() {
		String monthlyLineTotal;
		monthlyLineTotal = priceBreakdownMonthlyPrice.get(2).getAttribute("aria-label");
		String linePrice = monthlyLineTotal.substring(monthlyLineTotal.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Line + Device - Discount Price first item
	 * 
	 * @return
	 */
	public Double getTotalLinePriceForFirstItem() {
		Double autopay = 5.0;
		CartPage cartPage = new CartPage(getDriver());
		Double V1 = cartPage.getMonthlyPaymentLinePrice();
		Double V2 = cartPage.getMonthlyPaymentDevicePrice();
		Double V3 = cartPage.getMonthlyPaymentDiscountPrice();
		Double V4 = V1 + V2 - V3 + autopay;

		return (V4);
	}

	/**
	 * Get Line + Device - Discount Price second item
	 * 
	 * @return
	 */
	public Double getTotalLinePriceForSecondItem() {
		Double autopay = 5.0;
		CartPage cartPage = new CartPage(getDriver());
		Double V1 = cartPage.getMonthlyPaymentLinePriceForSecondItem();
		Double V2 = cartPage.getMonthlyPaymentDevicePriceForSecondItem();
		Double V3 = cartPage.getMonthlyPaymentDiscountPriceForSecondItem();
		Double V4 = V1 + V2 - V3 + autopay;

		return (V4);
	}

	/**
	 * Get Line + Device - Discount Price second item
	 * 
	 * @return
	 */
	public Double getTotalLinePriceForThirdItem() {
		Double autopay = 5.0;
		CartPage cartPage = new CartPage(getDriver());
		Double V1 = cartPage.getMonthlyPaymentLinePriceForThirdItem();
		Double V2 = cartPage.getMonthlyPaymentDevicePriceForThirdItem();
		Double V3 = cartPage.getMonthlyPaymentDiscountPriceForThirdItem();
		Double V4 = V1 + V2 - V3 + autopay;

		return (V4);
	}

	/**
	 * Get Monthly Payment DevicePrice for 1st item
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentDevicePrice() {
		String monthlyDevicePrice;
		monthlyDevicePrice = monthlyPaymentDevicePrice.get(0).getText();
		String devicePrice = monthlyDevicePrice.substring(monthlyDevicePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(devicePrice);
	}

	/**
	 * Get Monthly Payment DiscountPrice for 1st item
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentDiscountPrice() {
		String monthlyDiscountPrice;
		monthlyDiscountPrice = monthlyPaymentDevicePrice.get(0).getText();
		String discountPrice = monthlyDiscountPrice.substring(monthlyDiscountPrice.lastIndexOf("$") + 1);
		return Double.parseDouble(discountPrice);
	}

	/**
	 * Get Monthly Payment LinePrice
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentLinePriceForSecondItem() {
		String monthlyPaymentsLinePrice;
		monthlyPaymentsLinePrice = monthlyPaymentLinePrice.get(1).getText();
		String linePrice = monthlyPaymentsLinePrice.substring(monthlyPaymentsLinePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Monthly Payment LinePrice
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentLinePriceForThirdItem() {
		String monthlyPaymentsLinePrice;
		monthlyPaymentsLinePrice = monthlyPaymentLinePrice.get(2).getText();
		String linePrice = monthlyPaymentsLinePrice.substring(monthlyPaymentsLinePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Monthly Payment DevicePrice for 1st item
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentDevicePriceForSecondItem() {
		String monthlyDevicePrice;
		monthlyDevicePrice = monthlyPaymentDevicePrice.get(1).getText();
		String devicePrice = monthlyDevicePrice.substring(monthlyDevicePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(devicePrice);
	}

	/**
	 * Get OneTime Payment DevicePrice for 2nd item
	 * 
	 * @return
	 */
	public Double getOneTimePaymentDevicePriceSecondDevice() {
		String monthlyDevicePrice;
		monthlyDevicePrice = priceBreakdownUnderOneTimePayment.get(1).getText();
		String devicePrice = monthlyDevicePrice.substring(monthlyDevicePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(devicePrice);
	}

	/**
	 * Get Monthly Payment DevicePrice for 1st item
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentDevicePriceForThirdItem() {
		String monthlyDevicePrice;
		monthlyDevicePrice = monthlyPaymentDevicePrice.get(2).getText();
		String devicePrice = monthlyDevicePrice.substring(monthlyDevicePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(devicePrice);
	}

	/**
	 * Get Monthly Payment DiscountPrice for 2nd item
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentDiscountPriceForSecondItem() {
		String monthlyDiscountPrice;
		monthlyDiscountPrice = monthlyPaymentDevicePrice.get(1).getText();
		String discountPrice = monthlyDiscountPrice.substring(monthlyDiscountPrice.lastIndexOf("$") + 1);
		return Double.parseDouble(discountPrice);
	}

	/**
	 * Get Monthly Payment DiscountPrice for 3rd item
	 * 
	 * @return
	 */
	public Double getMonthlyPaymentDiscountPriceForThirdItem() {
		String monthlyDiscountPrice;
		monthlyDiscountPrice = monthlyPaymentDevicePrice.get(2).getText();
		String discountPrice = monthlyDiscountPrice.substring(monthlyDiscountPrice.lastIndexOf("$") + 1);
		return Double.parseDouble(discountPrice);
	}

	/**
	 * Verify Today Price Under MonthlyPayment Section
	 */
	public CartPage verifyTodayPriceUnderMonthlyPaymentSection() {
		try {
			for (WebElement todayPrice : priceBreakdownTodayPrice) {
				Assert.assertTrue(todayPrice.isDisplayed(),
						"Today Price Under MonthlyPayment Section is not displayed");
			}
			Reporter.log("Today Price Under MonthlyPayment Section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Today Price Under MonthlyPayment Section");
		}
		return this;
	}

	/**
	 * Verify Monthly Price Under MonthlyPayment Section
	 */
	public CartPage verifyMonthlyPriceUnderMonthlyPaymentSection() {
		try {
			for (WebElement monthlyPrice : monthlyPriceUnderMonthlyPaymentSection) {
				Assert.assertTrue(monthlyPrice.isDisplayed(),
						"Monthly Price Under MonthlyPayment Section is not displayed");
			}
			Reporter.log("Monthly Price Under MonthlyPayment Section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Monthly Price Under MonthlyPayment Section");
		}
		return this;
	}

	/**
	 * Get Order Details Line Monthly price for first device
	 * 
	 * @return
	 */
	public Double getOrderDetailsLineMonthlyPriceForFirstDevice() {
		String monthlyLineTotal;
		monthlyLineTotal = orderDetailsLineMonthlyPrice.get(0).getAttribute("left-amount");
		// System.out.println("Monthly price for device" + monthlyLineTotal);
		return Double.parseDouble(monthlyLineTotal);
	}

	/**
	 * Get Order Details Line Monthly price for second device
	 * 
	 * @return
	 */
	public Double getOrderDetailsLineMonthlyPriceForSecondDevice() {
		String monthlyLineTotal;
		monthlyLineTotal = orderDetailsLineMonthlyPrice.get(1).getAttribute("left-amount");
		// System.out.println("Monthly price for wearable" + monthlyLineTotal);
		return Double.parseDouble(monthlyLineTotal);
	}

	/**
	 * Get Order Details Line Monthly price for second device
	 * 
	 * @return
	 */
	public Double getOrderDetailsLineMonthlyPriceForThirdDevice() {
		String monthlyLineTotal;
		monthlyLineTotal = orderDetailsLineMonthlyPrice.get(2).getAttribute("left-amount");
		// System.out.println("Monthly price for tablet" + monthlyLineTotal);
		return Double.parseDouble(monthlyLineTotal);
	}

	/**
	 * Click on collapse icon on order details modal
	 */
	public CartPage clickOrderDetailsCollapseIcon() {
		try {
			clickElementWithJavaScript(orderDetailsModalCollapseIcon.get(1));
			Reporter.log("Clicked on Order Detaisl Collapse Icon");
		} catch (Exception e) {
			Assert.fail("Failed to Click Your Order Detaisl Collapse Icon");
		}
		return this;
	}

	/**
	 * Get Plan price for 1st device
	 * 
	 * @return
	 */
	public Double getOrderDetailsModalPlanPriceForFirstItem() {
		String monthlyPlanLinePrice;
		monthlyPlanLinePrice = orderDetailsModalLinePlanPrice.get(0).getAttribute("left-amount");
		String linePrice = monthlyPlanLinePrice.substring(monthlyPlanLinePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Plan price for 2nd device
	 * 
	 * @return
	 */
	public Double getOrderDetailsModalPlanPriceForSecondItem() {
		String monthlyPlanLinePrice;
		// scrollToElement(orderDetailsModalLinePlanPriceWearable);
		monthlyPlanLinePrice = orderDetailsModalLinePlanPriceWearable.getAttribute("left-amount");
		String linePrice = monthlyPlanLinePrice.substring(monthlyPlanLinePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Plan price for 2nd device
	 * 
	 * @return
	 */
	public Double getOrderDetailsModalPlanPriceForThirdItem() {
		String monthlyPlanLinePrice;
		monthlyPlanLinePrice = orderDetailsModalLinePlanPriceTablet.getAttribute("left-amount");
		String linePrice = monthlyPlanLinePrice.substring(monthlyPlanLinePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(linePrice);
	}

	/**
	 * Get Order Details Line Monthly price for first device
	 * 
	 * @return
	 */
	public Double getOrderDetailsLineMonthlyTotalPriceForFirstItem() {
		String monthlyLineTotal;
		monthlyLineTotal = orderDetailsModalLineMonthlyTotalPrice.get(0).getAttribute("right-amount");
		Double monthlyLineTotalPrice = Double.parseDouble(monthlyLineTotal);
		return (double) Math.round(monthlyLineTotalPrice);
	}

	/**
	 * Get Order Details Line Monthly price for second device
	 * 
	 * @return
	 */
	public Double getOrderDetailsLineMonthlyTotalPriceForSecondItem() {
		String monthlyLineTotal;
		monthlyLineTotal = orderDetailsModalLineMonthlyTotalPrice.get(1).getAttribute("right-amount");
		Double monthlyLineTotalPrice = Double.parseDouble(monthlyLineTotal);
		return (double) Math.round(monthlyLineTotalPrice);
	}

	/**
	 * Get Order Details Line Monthly price for third device
	 * 
	 * @return
	 */
	public Double getOrderDetailsLineMonthlyTotalPriceForThirdItem() {
		String monthlyLineTotal;
		monthlyLineTotal = orderDetailsModalLineMonthlyTotalPrice.get(2).getAttribute("right-amount");
		Double monthlyLineTotalPrice = Double.parseDouble(monthlyLineTotal);
		return (double) Math.round(monthlyLineTotalPrice);
	}

	/**
	 * Get Line total Price first item
	 * 
	 * @return
	 */
	public Double getOrderDetailsModalTotalLinePriceForFirstItem() {

		CartPage cartPage = new CartPage(getDriver());
		Double V1 = cartPage.getOrderDetailsModalPlanPriceForFirstItem();
		Double V2 = cartPage.getOrderDetailsLineMonthlyPriceForFirstDevice();
		Double V3 = Double.sum(V1, V2);
		Double lineTotalPrice = (double) Math.round(V3);
		return (lineTotalPrice);
	}

	/**
	 * Get Line total Price 2nd item
	 * 
	 * @return
	 */
	public Double getOrderDetailsModalTotalLinePriceForSecondItem() {

		CartPage cartPage = new CartPage(getDriver());
		Double V1 = cartPage.getOrderDetailsModalPlanPriceForSecondItem();
		Double V2 = cartPage.getOrderDetailsLineMonthlyPriceForSecondDevice();
		Double V3 = Double.sum(V1, V2);
		Double lineTotalPrice = (double) Math.round(V3);
		return (lineTotalPrice);
	}

	/**
	 * Get Line total Price 3nd item
	 * 
	 * @return
	 */
	public Double getOrderDetailsModalTotalLinePriceForThirdItem() {

		CartPage cartPage = new CartPage(getDriver());
		// Double V2 =
		// cartPage.getOrderDetailsLineMonthlyPriceForThirdDevice();//25.00
		// Double lineTotalPrice = (double) Math.round(V2);
		Double V1 = cartPage.getOrderDetailsModalPlanPriceForThirdItem();
		Double V2 = cartPage.getOrderDetailsLineMonthlyPriceForThirdDevice();
		Double V3 = Double.sum(V1, V2);
		Double lineTotalPrice = (double) Math.round(V3);
		return (lineTotalPrice);
	}

	/**
	 * Click Find Your Pricing Link in Cart
	 *
	 */
	public CartPage verifyFindYourPricingLinkNotDisplayedInCart() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(findYourPriceButton),
					"find your pricing link displayed in one time payment section");
			Reporter.log("Find your pricing link not displayed in one time section and sticky banner");
		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Link/CTA in Cart");
		}
		return this;
	}

	/**
	 * Verify post credit check sticky banner text
	 *
	 */
	public CartPage verifyPricingBasedOnCreditTextInCart() {
		try {
			checkPageIsReady();
			Assert.assertTrue(basedOnCreditText.getText().contains("your credit history"),
					"Device payments are based on your credit history in sticky banner not displayed");
			Reporter.log("Device payments are based on your credit history is displayed in sticky banner");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device payments are based on your credit history text");
		}
		return this;
	}

	/**
	 * Verify Estimated Monthly Text not displayed
	 */
	public CartPage verifyEstimatedMonthlyTextNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(estimatedMonthlyText), "Estimated Monthly text is displayed");
			Reporter.log("Estimated Monthly text is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Estimated Monthly text.");
		}
		return this;
	}

	/**
	 * verify total price amount for EIP on OrderDetailed pop up
	 *
	 */

	public CartPage verifyTotalPriceOnOrderDetailedModal() {
		try {

			Assert.assertFalse(totalPriceOnOrderDetailedModal.getAttribute("left-amount").isEmpty(),
					"Total Price is not matched with FRP");
			Reporter.log("Total price amount is displayed and amount is matched with Device FRP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Total price amount on Order detailed pop up.");
		}
		return this;
	}

	/**
	 * Get Monthly Payment DevicePrice for 1st item
	 * 
	 * @return
	 */
	public Double getOneTimePaymentDevicePrice() {
		String monthlyDevicePrice;
		monthlyDevicePrice = priceBreakdownUnderOneTimePayment.get(0).getText();
		String devicePrice = monthlyDevicePrice.substring(monthlyDevicePrice.lastIndexOf("$") + 1);
		return Double.parseDouble(devicePrice);
	}

	/**
	 * Compare Two Values does not match
	 *
	 */
	public CartPage compareTwoDoubleValuesNotEqual(Double firstValue, Double secondValue) {
		try {
			Assert.assertNotEquals(firstValue, secondValue, "The prices are same");
			Reporter.log("Prices are not equal/ Prices have now changed");
		} catch (Exception e) {
			Assert.fail("Failed to compare 2 prices");
		}
		return this;
	}

	/**
	 * Get Order Details Monthly add on price for first device
	 * 
	 * @return
	 */
	public Double getTotalCalculationWithAddon() {

		CartPage cartPage = new CartPage(getDriver());
		Double V1 = cartPage.getOrderDetailsLineMonthlyPriceForFirstDevice();
		Double V2 = cartPage.getOrderDetailsModalPlanPriceForFirstItem();
		Double V3 = cartPage.getOrderDetailsAddonPrice();
		Double V4 = V1 + V2 + V3;
		Double lineTotalPrice = (double) Math.round(V4);
		return (double) Math.round(lineTotalPrice);
	}

	/**
	 * Verify order details PriceBreakDown Displayed
	 */
	public CartPage verifyPriceBreakDownAddOnDisplayedOnOrderdetailedModel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(priceBreakdownAddOnPriceText), 60);
			Assert.assertTrue(priceBreakdownAddOnPriceText.isDisplayed(), "Add on PriceBreakDown is not displayed");
			Assert.assertTrue(orderDetailsModalLinePlanPrice.get(1).isDisplayed(),
					"Add on PriceBreakDown price is not displayed");
			Reporter.log("Add on PriceBreakDown is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to verify AddOnPrice");
		}
		return this;
	}

	/**
	 * Get Order Details Monthly add on price for first device
	 * 
	 * @return
	 */
	public Double getOrderDetailsAddonPrice() {
		String monthlyLineTotal;
		monthlyLineTotal = orderDetailsModalLinePlanPrice.get(1).getAttribute("left-amount");
		Double monthlyaddon = Double.parseDouble(monthlyLineTotal);
		return (double) Math.round(monthlyaddon);
	}

	/**
	 * Verify sim Starter Kit Text is displayed
	 *
	 */
	public CartPage verifySimStarterKitTextDisplayed() {
		try {
			Assert.assertFalse(simStarterKitText.isDisplayed(), "Sim Starter Kit Text is not Displayed");
			Reporter.log("Sim Starter Kit Text is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sim Starter Kit Text.");
		}
		return this;
	}

	/**
	 * Get Sim Starter Kit Price Integer format
	 */
	public Double getSimStarterKitPriceIntegerAtOderDetailPage() {
		String simStarterKit = "";
		try {
			simStarterKit = simStarterKitPriceAfterPromoDiscount.getAttribute("left-amount");
			Reporter.log("Sim Starter Kit Price at Order Detail page is " + simStarterKit);
		} catch (Exception e) {
			Assert.fail("Failed to get Sim starter kit price in order detailed page");
		}

		return Double.parseDouble(simStarterKit);
	}

	/**
	 * Verify view order details link is displayed
	 *
	 */
	public CartPage verifyViewOrderDetailsLinkDisplayed() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(viewOrderDetailsLink), "Order details link is not diplayed");
			Reporter.log("View order details link is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display View order details link.");
		}
		return this;
	}

	/**
	 * Verify Today price at Order Detail page
	 */
	public CartPage verifyTodayPriceOrderDetailModel() {
		String todayprice = "";
		try {
			todayprice = todayTotalPriceAtOrderDetailPageForFirstDevice.getAttribute("left-amount");
			Assert.assertNotEquals(todayprice, "", "Today price is not displayed");
			Reporter.log("Today Price in Order detail page is " + todayprice);
		} catch (Exception e) {
			Assert.fail("Failed to get today total amount.");
		}
		return this;
	}

	/**
	 * Verify Monthly price at Order Detail page
	 */
	public CartPage verifyMonthlyPriceOrderDetailModel() {
		String monthlyprice = "";
		try {
			monthlyprice = todayTotalPriceAtOrderDetailPageForFirstDevice.getAttribute("right-amount");
			;
			Assert.assertNotEquals(monthlyprice, "", "Monthly price is not displayed");
			Reporter.log("Monthly Price in Order detail page is " + monthlyprice);
		} catch (Exception e) {
			Assert.fail("Failed to get Monthly amount.");
		}
		return this;
	}

	/**
	 * Get Line count on Cart Page
	 *
	 */
	public int getLineCountOnCartPage() {
		int count = 0;
		try {
			checkPageIsReady();
			count = duplicateCTA.size();
			Reporter.log("Total lines on cart Page is " + count);
		} catch (Exception e) {
			Assert.fail("Failed to verify total lines on cart Page");
		}
		return count;
	}

	/**
	 * Verify devices are listed in same sequence as added on Cart page
	 *
	 */
	public CartPage verifyDeviceListedInSameSequenceAsAddedOnCartPage(String firstDevice, String secondDevice,
			String thirdDevice, String fourthDevice) {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertTrue(
					(getDriver()
							.findElement(By.xpath("//span/span[contains(text(),'1')]/../../../..//h5[contains(text(),'"
									+ firstDevice + "')]"))).isDisplayed(),
					"Device on First Line is not same as added");
			Assert.assertTrue(
					(getDriver()
							.findElement(By.xpath("//span/span[contains(text(),'2')]/../../../..//h5[contains(text(),'"
									+ secondDevice + "')]"))).isDisplayed(),
					"Device on second Line is not same as added");
			Assert.assertTrue(
					(getDriver()
							.findElement(By.xpath("//span/span[contains(text(),'3')]/../../../..//h5[contains(text(),'"
									+ thirdDevice + "')]"))).isDisplayed(),
					"Device on third Line is not same as added");
			Assert.assertTrue(
					(getDriver()
							.findElement(By.xpath("//span/span[contains(text(),'4')]/../../../..//h5[contains(text(),'"
									+ fourthDevice + "')]"))).isDisplayed(),
					"Device on Fourth Line is not same as added");
			Reporter.log("Devices are listed in same sequence as added on Cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify devices are listed in same sequence as added on Cart page");
		}
		return this;
	}

	/**
	 * Verify Devices are listed in same sequence as added on Order Modal
	 *
	 */
	public CartPage verifyDeviceListedInSameSequenceAsAddedOnViewOrderModal(String firstDevice, String secondDevice,
			String thirdDevice, String fourthDevice) {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue((getDriver().findElement(
					By.xpath("//span[contains(text(),'1')]/../..//span[contains(text(),'" + firstDevice + "')]")))
							.isDisplayed(),
					"Device on First Line is not same as added");
			Assert.assertTrue((getDriver().findElement(
					By.xpath("//span[contains(text(),'2')]/../..//span[contains(text(),'" + secondDevice + "')]")))
							.isDisplayed(),
					"Device on second Line is not same as added");
			Assert.assertTrue((getDriver().findElement(
					By.xpath("//span[contains(text(),'3')]/../..//span[contains(text(),'" + thirdDevice + "')]")))
							.isDisplayed(),
					"Device on third Line is not same as added");
			Assert.assertTrue((getDriver().findElement(
					By.xpath("//span[contains(text(),'4')]/../..//span[contains(text(),'" + fourthDevice + "')]")))
							.isDisplayed(),
					"Device on fourth Line is not same as added");
			Reporter.log("Devices are listed in same sequence as added on Order Modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify Devices are listed in same sequence as added on Order Modal");
		}
		return this;
	}

	/**
	 * Verify Devices are listed in same sequence as added on Order Modal
	 *
	 */
	public CartPage verifyAccessoryIsLastOnOrderDetailsModal(String accessoryName) {
		try {
			waitforSpinner();
			Assert.assertTrue(productListOnOrderDetailsPage.get((productListOnOrderDetailsPage.size()) - 2).getText()
					.contains(accessoryName), "Accessory is not the last product.");
			Reporter.log("Accessory is listed at the end of the Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify if accessory is at the last of the page");
		}
		return this;
	}

	/**
	 * Validate Update CTA in Add Ons Modal is Disabled
	 */
	public CartPage verifyUpdateCTAOnAddOnsModalIsDisabled() {
		try {
			Assert.assertFalse(updateCTAOnprotectionModal.isEnabled(), "Update CTA is not disabled");
			Reporter.log("Update CTA is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Update CTA disabled");
		}
		return this;
	}

	/**
	 * Validate Update CTA in Add Ons Modal is Enabled
	 */
	public CartPage verifyUpdateCTAOnAddOnsModalIsEnabled() {
		try {
			Assert.assertTrue(updateCTAOnprotectionModal.isEnabled(), "Update CTA is not enabled");
			Reporter.log("Update CTA is enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Update CTA enabled");
		}
		return this;
	}

	/**
	 * Verify Recommended Services checkbox selected in modal
	 */
	public CartPage verifyRecommendedServicesCheckboxSelected() {
		try {
			waitFor(ExpectedConditions.visibilityOf(recommendedServiceCheckboxSelected), 60);
			Assert.assertTrue(recommendedServiceCheckboxSelected.getText().contains("1 selected"),
					"Recommended Service is not selected");
			Reporter.log("Recommended Service is selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Recommended Service is selected");
		}
		return this;
	}

	/**
	 * Add Second Accessory to Cart from cart Page.
	 *
	 */
	public CartPage addSecondAccessorytoCartFromCartPage(TMNGData tMNGData) {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getSecondAccessoryName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Verify Devices are listed in same sequence as added on Order Modal
	 *
	 */
	public CartPage verifyAccessoryDevicesLineOrder(String accessoryName, String secondAccessoryName) {
		try {
			waitforSpinner();
			Assert.assertTrue(productListOnOrderDetailsPage.get((productListOnOrderDetailsPage.size()) - 2).getText()
					.contains(accessoryName), "First Accessory is Displayed in sequence as added");
			Assert.assertTrue(productListOnOrderDetailsPage.get((productListOnOrderDetailsPage.size()) - 1).getText()
					.contains(secondAccessoryName), "Second Accessory is Displayed in sequence as added");
			Reporter.log("Accessory is listed at the end of the Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify if accessory is at the last of the page");
		}
		return this;
	}

	/**
	 * Add wearable to Cart from cart Page.
	 *
	 */
	public CartPage addASimKittoCartFromCartPage() {
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnBYODLinkOnCartEssentials();
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifySimKitPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		return this;
	}

	/**
	 * Verify Add On and Extras Header Displayed for Phone
	 */
	public CartPage verifyAddOnExtrasHeaderDisplayedForPhone() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addOnExtrasHeaderLineLevel.get(0)), 60);
			Assert.assertTrue(addOnExtrasHeaderLineLevel.get(0).isDisplayed(),
					"Add On and Extras Header is not Displayed for Phone");
			Reporter.log("Add On and Extras Header is Displayed for Phone");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add On and Extras Header is Displayed for Phone");
		}
		return this;
	}

	/**
	 * Verify See More Options CTA displayed with Add On and Extras Header For Phone
	 */
	public CartPage verifySeeMoreOptionsCTADisplayedForPhone() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(0)), 60);
			Assert.assertTrue(seeMoreOptionsCTAAddOnsAndExtrasList.get(0).isDisplayed(),
					"See More Options CTA is not Displayed for Phone");
			Reporter.log("See More Options CTA is Displayed for Phone");
		} catch (Exception e) {
			Assert.fail("Failed to verify See more options CTA for Phone");
		}
		return this;
	}

	/**
	 * Click See More Options CTA displayed with Add On and Extras Header for Phone
	 */
	public CartPage clickSeeMoreOptionsCTAAddOnsAndExtrasForPhone() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(0)), 60);
			moveToElement(seeMoreOptionsCTAAddOnsAndExtrasList.get(0));
			clickElementWithJavaScript(seeMoreOptionsCTAAddOnsAndExtrasList.get(0));
			Reporter.log("See More Options CTA is Clicked for Phone");
		} catch (Exception e) {
			Assert.fail("Failed to Click See more options CTA for Phone");
		}
		return this;
	}

	/**
	 * Verify Add On and Extras Header Displayed for Tablet
	 */
	public CartPage verifyAddOnExtrasHeaderDisplayedForTablet() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addOnExtrasHeaderLineLevel.get(1)), 60);
			Assert.assertTrue(addOnExtrasHeaderLineLevel.get(1).isDisplayed(),
					"Add On and Extras Header is not Displayed for Tablet");
			Reporter.log("Add On and Extras Header is Displayed for Tablet");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add On and Extras Header is Displayed for Tablet");
		}
		return this;
	}

	/**
	 * Verify See More Options CTA displayed with Add On and Extras Header For
	 * Tablet
	 */
	public CartPage verifySeeMoreOptionsCTADisplayedForTablet() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(1)), 60);
			Assert.assertTrue(seeMoreOptionsCTAAddOnsAndExtrasList.get(1).isDisplayed(),
					"See More Options CTA is not Displayed for Tablet");
			Reporter.log("See More Options CTA is Displayed for Tablet");
		} catch (Exception e) {
			Assert.fail("Failed to verify See more options CTA for Tablet");
		}
		return this;
	}

	/**
	 * Click See More Options CTA displayed with Add On and Extras Header for Tablet
	 */
	public CartPage clickSeeMoreOptionsCTAAddOnsAndExtrasForTablet() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(1)), 60);
			moveToElement(seeMoreOptionsCTAAddOnsAndExtrasList.get(1));
			clickElementWithJavaScript(seeMoreOptionsCTAAddOnsAndExtrasList.get(1));
			Reporter.log("See More Options CTA is Clicked for Tablet");
		} catch (Exception e) {
			Assert.fail("Failed to Click See more options CTA for Tablet");
		}
		return this;
	}

	/**
	 * Verify Add On and Extras Header Displayed for Wearable
	 */
	public CartPage verifyAddOnExtrasHeaderDisplayedForWearable() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addOnExtrasHeaderLineLevel.get(2)), 60);
			Assert.assertTrue(addOnExtrasHeaderLineLevel.get(2).isDisplayed(),
					"Add On and Extras Header is not Displayed for Wearable");
			Reporter.log("Add On and Extras Header is Displayed for Wearable");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add On and Extras Header is Displayed for Wearable");
		}
		return this;
	}

	/**
	 * Verify See More Options CTA displayed with Add On and Extras Header For
	 * Wearable
	 */
	public CartPage verifySeeMoreOptionsCTADisplayedForWearable() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(2)), 60);
			Assert.assertTrue(seeMoreOptionsCTAAddOnsAndExtrasList.get(2).isDisplayed(),
					"See More Options CTA is not Displayed for Wearable");
			Reporter.log("See More Options CTA is Displayed for Wearable");
		} catch (Exception e) {
			Assert.fail("Failed to verify See more options CTA for Wearable");
		}
		return this;
	}

	/**
	 * Click See More Options CTA displayed with Add On and Extras Header for
	 * Wearable
	 */
	public CartPage clickSeeMoreOptionsCTAAddOnsAndExtrasForWearable() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(2)), 60);
			moveToElement(seeMoreOptionsCTAAddOnsAndExtrasList.get(2));
			clickElementWithJavaScript(seeMoreOptionsCTAAddOnsAndExtrasList.get(2));
			Reporter.log("See More Options CTA is Clicked for Wearable");
		} catch (Exception e) {
			Assert.fail("Failed to Click See more options CTA for Wearable");
		}
		return this;
	}

	/**
	 * Verify Add On and Extras Header Displayed for Phone SIM
	 */
	public CartPage verifyAddOnExtrasHeaderDisplayedForPhoneSIM() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addOnExtrasHeaderLineLevel.get(3)), 60);
			Assert.assertTrue(addOnExtrasHeaderLineLevel.get(3).isDisplayed(),
					"Add On and Extras Header is not Displayed for Phone SIM");
			Reporter.log("Add On and Extras Header is Displayed for Phone SIM");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add On and Extras Header is Displayed for Phone SIM");
		}
		return this;
	}

	/**
	 * Verify See More Options CTA displayed with Add On and Extras Header For Phone
	 * SIM
	 */
	public CartPage verifySeeMoreOptionsCTADisplayedForPhoneSIM() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(3)), 60);
			Assert.assertTrue(seeMoreOptionsCTAAddOnsAndExtrasList.get(3).isDisplayed(),
					"See More Options CTA is not Displayed for Phone SIM");
			Reporter.log("See More Options CTA is Displayed for Phone SIM");
		} catch (Exception e) {
			Assert.fail("Failed to verify See more options CTA for Phone SIM");
		}
		return this;
	}

	/**
	 * Click See More Options CTA displayed with Add On and Extras Header for Phone
	 * SIM
	 */
	public CartPage clickSeeMoreOptionsCTAAddOnsAndExtrasForPhoneSIM() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(3)), 60);
			moveToElement(seeMoreOptionsCTAAddOnsAndExtrasList.get(3));
			clickElementWithJavaScript(seeMoreOptionsCTAAddOnsAndExtrasList.get(3));
			Reporter.log("See More Options CTA is Clicked for Phone SIM");
		} catch (Exception e) {
			Assert.fail("Failed to Click See more options CTA for Phone SIM");
		}
		return this;
	}

	/**
	 * Verify Add On and Extras Header Displayed for Tablet SIM
	 */
	public CartPage verifyAddOnExtrasHeaderDisplayedForTabletSIM() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addOnExtrasHeaderLineLevel.get(4)), 60);
			Assert.assertTrue(addOnExtrasHeaderLineLevel.get(4).isDisplayed(),
					"Add On and Extras Header is not Displayed for Tablet SIM");
			Reporter.log("Add On and Extras Header is Displayed for Tablet SIM");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add On and Extras Header is Displayed for Tablet SIM");
		}
		return this;
	}

	/**
	 * Verify See More Options CTA displayed with Add On and Extras Header For
	 * Tablet SIM
	 */
	public CartPage verifySeeMoreOptionsCTADisplayedForTabletSIM() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(4)), 60);
			Assert.assertTrue(seeMoreOptionsCTAAddOnsAndExtrasList.get(4).isDisplayed(),
					"See More Options CTA is not Displayed for Tablet SIM");
			Reporter.log("See More Options CTA is Displayed for Tablet SIM");
		} catch (Exception e) {
			Assert.fail("Failed to verify See more options CTA for Tablet SIM");
		}
		return this;
	}

	/**
	 * Click See More Options CTA displayed with Add On and Extras Header for Tablet
	 * SIM
	 */
	public CartPage clickSeeMoreOptionsCTAAddOnsAndExtrasForTabletSIM() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsCTAAddOnsAndExtrasList.get(4)), 60);
			moveToElement(seeMoreOptionsCTAAddOnsAndExtrasList.get(4));
			clickElementWithJavaScript(seeMoreOptionsCTAAddOnsAndExtrasList.get(4));
			Reporter.log("See More Options CTA is Clicked for Tablet SIM");
		} catch (Exception e) {
			Assert.fail("Failed to Click See more options CTA for Tablet SIM");
		}
		return this;
	}

	/**
	 * Enter PromoCode Value
	 */
	public CartPage enterPromoCodeValue(String data) {
		try {
			moveToElement(addAPhoneLinkOnCart);
			clickElementWithJavaScript(addPromoCodeButton);
			sendTextData(addPromoCodeButton, data);
			Reporter.log("entered promo code");
		} catch (Exception e) {
			Assert.fail("User unable to enter promo code");
		}
		return this;
	}

	/**
	 * Click Apply Button
	 */
	public CartPage clickApplyButton() {
		try {
			waitforSpinner();
			applyButton.click();
			Reporter.log("Clicked on Apply button");
		} catch (Exception e) {
			Assert.fail("Apply button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Call me now icon in cart page
	 */
	public CartPage verifyCallMeNowIcon() {
		try {
			checkPageIsReady();
			scrollToElement(callMeNow);
			Assert.assertTrue(callMeNow.isDisplayed(), "Call me now icon is not displayed in cart page");
			Reporter.log("Call me now icon is displayed in cart page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Call me now icon in cart page");
		}
		return this;
	}

	/**
	 * Click call me now icon
	 */
	public CartPage clickCallMeNowIcon() {
		try {
			checkPageIsReady();
			callMeNow.click();
			Reporter.log("Clicked on call me now button");
		} catch (Exception e) {
			Assert.fail("Failed to click on call me now icon");
		}
		return this;
	}

	/**
	 * verify request a cal model
	 * 
	 */
	public CartPage verifyRequestCallModel() {
		try {
			checkPageIsReady();
			Assert.assertTrue(requestCallModelPage.isDisplayed(), "request a cal model not displayed");
			Reporter.log("request a cal model displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify request a cal model");
		}
		return this;
	}

	/**
	 * click Checkbox on request call modal
	 */
	public CartPage clickCheckBoxInRequestCallModal() {
		try {
			// checkPageIsReady();
			// checkBoxInRequestCallModel.click();
			waitFor(ExpectedConditions.elementToBeClickable(checkBoxInRequestCallModel));
			if (!checkBoxInRequestCallModel.isSelected()) {
				clickElementWithJavaScript(checkBoxInRequestCallModel);
			}
			checkBoxInRequestCallModel.sendKeys(Keys.TAB);
			Reporter.log("Clicked on check box on request cal model");
		} catch (Exception e) {
			Assert.fail("Failed to verify Check box on request cal model");
		}
		return this;
	}

	/**
	 * click call me now button on request call modal
	 */
	public CartPage clickCallMeNowButtonInRequestCallModal() {
		try {
			checkPageIsReady();
			callMeNowButtonInModel.click();
			Reporter.log("Clicked on call me now button in model");
		} catch (Exception e) {
			Assert.fail("Failed to verify call me now button in request cal model");
		}
		return this;
	}

	/**
	 * verify confirmation message in request call model
	 */
	public CartPage verifyConfirmationMessageInRequestCallModel() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(confirmationMessageInReuqestCallModel), 60);
			Assert.assertTrue(isElementDisplayed(confirmationHeaderInReuqestCallModel),
					"Confirmation message is not displayed");
			Assert.assertTrue(confirmationHeaderInReuqestCallModel.getText().contains("Thank you"),
					"Confirmation message is not displayed");
			Assert.assertTrue(isElementDisplayed(confirmationMessageInReuqestCallModel),
					"Confirmation message is not displayed");
			Assert.assertTrue(
					confirmationMessageInReuqestCallModel.getText().contains("We will try our best to have a T-Mobile"),
					"Confirmation message is not displayed");
			Reporter.log("Confirmation message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify confirmation message");
		}
		return this;
	}

	/**
	 * Fill personal info in request call model
	 *
	 * @param tmngData
	 */
	public CartPage fillPersonalInfoInRequestCallModel(TMNGData tmngData) {
		try {
			waitForSpinnerInvisibility();
			firstName.sendKeys(tmngData.getFirstName());
			lastNameInRequestCallModel.sendKeys(tmngData.getLastName());
			phoneNumberInRequestCallModel.sendKeys(tmngData.getPhoneNumber());
			zipCodeInRequestCallModel.sendKeys(tmngData.getZipcode());
			Reporter.log("Filled personal info");
		} catch (Exception e) {
			Assert.fail("Failed to fill personal info");
		}
		return this;
	}

	/**
	 * verify Retrieve Cart Header retrieveCartText
	 */
	public CartPage verifyRetrieveCartHeader() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(retrieveCartHeader), 60);
			Assert.assertTrue(retrieveCartHeader.isDisplayed(), "Retrieve Cart Header is not displayed");
			Reporter.log("Retrieve cart header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Retrieve Cart Header");
		}
		return this;
	}

	/**
	 * verify retrieve Cart Text
	 */
	public CartPage verifyRetrieveCartText() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(retrieveCartText.isDisplayed(), "retrieve Cart Text is not displayed");
			Assert.assertTrue(retrieveCartText.getText().contains("If you previously saved a cart"),
					"retrieve Cart Text is not displayed");
			Reporter.log("Retrieve cart text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify retrieve Cart Text");
		}
		return this;
	}

	/**
	 * Verify invalid email error text field with red exclamation icon
	 *
	 */
	public CartPage verifyInvalidEmailErrorRedExclamationIcon() {
		try {
			Assert.assertTrue(isElementDisplayed(invalidEmailErrorTextField),
					"Invalid email error text field not displayed with red exclamation icon");
			Reporter.log("Invalid email error text field displayed with red exclamation icon ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Invalid email error text field with red exclamation icon");
		}
		return this;
	}

	public CartPage verifyDuplicates() {
		try {
			verifyDuplicatedElements(this.getClass());
		} catch (Exception e) {
			Assert.fail();
		}
		return this;
	}

	/**
	 * Verify Customer Care Container Header on Emptycart
	 *
	 */
	public CartPage verifyCustomerCareContainerHeaderOnEmptycart() {
		try {
			Assert.assertTrue(isElementDisplayed(customerCartContainerHeaderOncart),
					"Customer Care container Herder is not displayed on Empty cart");
			Reporter.log("Customer Care container Herder is displayed on Empty cart");
			Assert.assertTrue(
					customerCartContainerHeaderOncart.getText().equalsIgnoreCase("Need help with your order?"),
					"Customer care header is not matched with 'Need help with your order?'");
			Reporter.log("Customer Care container Herder is matched with 'Need help with your order?' on Empty cart");

		} catch (Exception e) {
			Assert.fail("Failed to Verify Customer Care Container Header on Emptycart");
		}
		return this;
	}

	/**
	 * Verify Customer Care Container Description text on Emptycart
	 *
	 */
	public CartPage verifyCustomerCareContainerDescTextOnEmptycart() {
		try {
			Assert.assertTrue(isElementDisplayed(customerCartContainerDescOnEmptycart),
					"Customer Care container Desc text is not displayed on Empty cart");
			Reporter.log("Customer Care container Desc text is displayed on Empty cart");
			Assert.assertTrue(
					customerCartContainerDescOnEmptycart.getText()
							.equalsIgnoreCase("Your T-Mobile rep will help you find the right fit"),
					"Customer care Desc text is not matching with 'Your T-Mobile rep will help you find the right fit'");
			Reporter.log(
					"Customer Care container Desc text is matched with 'Your T-Mobile rep will help you find the right fit' on Empty cart");

		} catch (Exception e) {
			Assert.fail("Failed to Verify Customer Care Container Description text on Emptycart");
		}
		return this;
	}

	/**
	 * Verify Headset icon in Customer Care Container on Emptycart
	 *
	 */
	public CartPage verifyHeadsetIconOnCustomerCareContainerOnEmptycart() {
		try {
			Assert.assertTrue(isElementDisplayed(headsetIconOncustomerCartContainerOnEmptycart),
					"Headset icon is not displayed on Customer Care container on Empty cart");
			Reporter.log("Headset icon is displayed on Customer Care container on Empty cart");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Headset icon in Customer Care Container on Emptycart");
		}
		return this;
	}

	/**
	 * Verify CallUs Text and number in Customer Care Container on Emptycart
	 *
	 */
	public CartPage verifyCallUsTextandNumberOnCustomerCareContainerOnEmptycart() {
		try {
			Assert.assertTrue(isElementDisplayed(callUstextOncustomerCartContainerOnEmptycart),
					"CallUs Text is not displayed on Customer Care container on Empty cart");
			Reporter.log("CallUs Text is displayed on Customer Care container on Empty cart");
			if(!(getDriver() instanceof AppiumDriver)) {
			Assert.assertTrue(isElementDisplayed(callUsNumberOncustomerCartContainerOnEmptycart),
					"CallUs Number is not displayed on Customer Care container on Empty cart");
			Reporter.log("CallUs Number is displayed on Customer Care container on Empty cart");
			
			Assert.assertTrue(
					callUsNumberOncustomerCartContainerOnEmptycart.getText().equalsIgnoreCase("1-800-T-Mobile"),
					"CallUs Number is not matching with '1-800-T-Mobile' on Customer Care container on Empty cart");
			Reporter.log("CallUs Number is matching with '1-800-T-Mobile' on Customer Care container on Empty cart");
			}

		} catch (Exception e) {
			Assert.fail("Failed to Verify CallUs Text and number in Customer Care Container on Emptycart");
		}
		return this;
	}

	/**
	 * verify refundable deposit text
	 */
	public CartPage verifyRefundableDepositText() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(refundableDepositText.isDisplayed(), "Refundable Deposit Text is not displayed");
			Assert.assertTrue(refundableDepositText.getText().contains("Refundable Deposit"),
					"Refundable Deposit Text is not displayed correctly");
			Reporter.log("Refundable Deposit text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Refundable Deposit Text");
		}
		return this;
	}

	/**
	 * verify refundable deposit amount
	 */
	public CartPage verifyRefundableDepositAmount() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(refundableDepositAmt.isDisplayed(), "Refundable Deposit amount is not displayed");
			Reporter.log("Refundable Deposit amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Refundable Deposit amount");
		}
		return this;
	}
}
