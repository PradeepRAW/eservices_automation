/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import static java.lang.Integer.parseInt;

import java.text.DecimalFormat;
import java.util.List;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class AccessoriesStandAlonePLPPage extends CommonPage {

	public static final String CURRENTCATEGORY = "Cases";
	public static final String DEVICENAME = "T-Mobile® REVVL® PLUS";
	public static final String DEVICENAME2 = "Apple iPhone XS - Gold - 256GB";

	@FindBy(css = ".hideinphone #clearAllFilters")
	private WebElement clearFilterCTA;
	
	@FindBy(xpath="//div[@class='hidefilter']//a[@id='clearAllFilters']")
	private WebElement clearFilterCTAMobile;
	
	@FindBy(css = "[class*='form-control custom-ddl-hr ng-valid ng-dirty ng-touched ng-empty']")
	private WebElement emptycatagoryname;

	@FindBy(css = "div a.product-name")
	private List<WebElement> accessoryTitle;

	@FindBy(css = "div.EIPPayment.ng-binding")
	private List<WebElement> eipMonthlyPaymentInPLP;

	@FindBy(css = "span.generic-upfront-price.ng-binding")
	private List<WebElement> eipDownPaymentInPLP;

	@FindBy(css = "div.EIPInstallment.ng-binding")
	private List<WebElement> eipLoanTermInPLP;

	@FindBy(css = "div.webPrice.ng-binding.ng-scope")
	private List<WebElement> eipFRPInPLP;

	// *[@id="eipPriceDetail"]
	@FindBy(css = ".out-of-stock.ng-scope")
	private List<WebElement> addToCartList;

	@FindBy(css = "div.col-md-12.accessorydiv")
	private List<WebElement> accessoryPage;

	@FindBy(xpath = "//button[contains(text(),'cart')]")
	private WebElement addToCartBtn;

	@FindBy(css = ".fullRetailPricing del.striked-price")
	private List<WebElement> accessoryFRPwithPromotion;

	@FindBy(css = ".accessoriespagination.ng-scope a")
	private List<WebElement> navigationtabs;

	@FindBy(css = "div.webPrice.ng-binding.ng-scope .super-decimal-price.ng-binding")
	private List<WebElement> eipFRPCentsInPLP;

	@FindBy(css = "div.EIPPayment.ng-binding .super-decimal-price.ng-binding")
	private List<WebElement> eipMonthlyPaymentCentsInPLP;

	@FindBy(css = ".striked-price ~ div.webPrice")
	private List<WebElement> eipFRPInPLPforPromoItem;

	@FindBy(xpath = "//del[contains(@class,'striked-price')]/../../../../preceding-sibling::div/child::a")
	private List<WebElement> firstItemWithCatalogueP;

	@FindBy(xpath = "//del[contains(@class,'striked-price')]/following-sibling::div[contains(@class,'webPrice')]")
	private List<WebElement> firstPromoFRP;

	@FindBy(xpath = "//del[contains(@class,'striked-price')]/following-sibling::div[contains(@class,'webPrice')]/child::sup")
	private List<WebElement> firstPromoFRPCents;

	@FindBy(xpath = "//del[contains(@class,'striked-price')]/../preceding-sibling::div/div[contains(@class,'browse')]/div/span[contains(@class,'price')]")
	private List<WebElement> firstPromoDownPayment;

	@FindBy(xpath = "//del[contains(@class,'striked-price')]/../preceding-sibling::div/div[contains(@class,'EIPPayment')]/sup")
	private List<WebElement> firstPromoEIPMonthlyPaymentCents;

	@FindBy(xpath = "//del[contains(@class,'striked-price')]/../preceding-sibling::div/div[contains(@class,'Installment')]")
	private List<WebElement> firstPromoEIPLoanTerms;
	
	@FindBy(xpath = "//del[contains(@class,'striked-price')]/../preceding-sibling::div/div[contains(@class,'EIPPayment')]")
	private List<WebElement> firstPromoEIPMonthlyPayment;

	@FindBy(css = "div.product-img-plp")
	private List<WebElement> accessoryProductList;

	@FindBy(xpath = "//a[contains(text(), 'Next')]")
	private WebElement pageNationNextButton;

	@FindBy(xpath = "//a[contains(text(), 'First')]")
	private WebElement pageNationFirstButton;

	@FindBy(xpath = "//a[contains(text(), 'Previous')]")
	private WebElement pageNationPreviousButton;

	@FindBy(xpath = "//a[contains(text(), 'Last')]")
	private WebElement pageNationLastButton;

	@FindBy(css = "select[ng-model*='selectedCategory']")
	private WebElement selectCategory;

	@FindBy(css = "input[list*='allDevices']")
	private WebElement deviceList;

	@FindBy(css = "span.totalresult b")
	private WebElement accessoriesCount;

	@FindBy(css = "#manufacturerFilter .btn-filter")
	private WebElement manufacturerDropdown;

	@FindBy(css = ".manufacturerFilterData.activated a")
	private List<WebElement> manufacturerList;

	@FindBy(css = ".sort-dropdown a.pricesort")
	private WebElement priceSortDropdown;

	@FindBy(css = "#sort-dropdown-list li")
	private List<WebElement> priceSortOption;

	@FindBy(css = "[ng-model*=resultsPerPage]")
	private WebElement resultsPerPage;

	@FindBy(css = "#acsMainInvite .acsCloseButton--container>span>a")
	private List<WebElement> mobileFeedBackAlert;
	
	@FindBy(css="button#filter_btn")
	private WebElement filterCTAMobile;
	
	@FindBy(css="input#ManufacturerApple")
	private WebElement manufactureApple;

	public AccessoriesStandAlonePLPPage(WebDriver webDriver) {
		super(webDriver);
		
	}

	public AccessoriesStandAlonePLPPage verifyAccessoriesStandAlonePLPPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
			Reporter.log("Accessories stand alone PLP page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Accessories stand alone PLP page is not displayed");
		}
		return this;

	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AccessoriesStandAlonePLPPage verifyPageUrl() {
		try {
			if(getDriver() instanceof AppiumDriver){
				if (mobileFeedBackAlert.size() == 1) {
					mobileFeedBackAlert.get(0).click();
					Assert.assertTrue(getDriver().getCurrentUrl().contains("accessories"), "Accessories page is not displayed");
				}
			}else{
				Assert.assertTrue(getDriver().getCurrentUrl().contains("accessories"), "Accessories page is not displayed");
			}
		} catch (Exception e) {
			Reporter.log("Accessories page is not displayed");
		}
		return this;
	}

	/**
	 * Click Accessory device by name
	 */
	public AccessoriesStandAlonePLPPage clickAccesoriesDeviceByName(String accesoriesName) {
		waitforSpinner();
		try {
			if (accesoriesName == null) {
				moveToElement(accessoryTitle.get(0));
				clickElement(accessoryTitle.get(0));
			}
			for (WebElement item : accessoryTitle) {
				if (item.getText().contains(accesoriesName)) {
					moveToElement(item);
					clickElement(item);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Accesssories Name is not available to click");
		}
		return this;
	}

	/**
	 * Click Add to Cart Button By Accessory Name
	 */
	public AccessoriesStandAlonePLPPage clickAddtoCartBtnByAccessoryName(String accesoriesName) {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOfAllElements(accessoryPage));
			for (int i = 0; i < accessoryProductList.size(); i++) {
				if (accessoryProductList.get(i).getText().equalsIgnoreCase(accesoriesName)) {
					clickElementWithJavaScript(accessoryProductList.get(i));
					Reporter.log("Clicked On Item");
					AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(
							getDriver());
					accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
					accessoriesStandAlonePDPPage.clickAddToCartButton();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Accessories Name is not availble to click");
		}
		return this;
	}

	/**
	 * Verify EIP and FRP for Standard Users
	 */
	public AccessoriesStandAlonePLPPage verifyFRPForStandardUsers(String accesoriesName) {
		try {
			waitforSpinner();
			WebElement frpDivison = getDriver().findElement(
					By.xpath("//a[contains(text(), ' " + accesoriesName + "')]//..//div[@class='fullRetailPricing']"));
			Assert.assertTrue(frpDivison.isDisplayed(), "FRP section is displayed");
		} catch (Exception e) {
			Assert.fail("FRP section is not displayed");
		}
		return this;
	}

	/**
	 * Select stand alone accessory device
	 */
	public AccessoriesStandAlonePLPPage selectFirstStandAloneAccessoryDevice() {
		try {
			moveToElement(accessoryProductList.get(0));
			accessoryProductList.get(0).click();
			Reporter.log("Clicked on Device on Stand Alone PLP Page");
		} catch (Exception e) {
			Assert.fail("StandAlone accessories device is not available");
		}
		return this;

	}

	/**
	 * Verify that EIP Down Payment is displayed
	 */

	public AccessoriesStandAlonePLPPage verifyEIPDownPayment() {
		try {
			waitforSpinner();
			for (WebElement eipDownPayment : eipDownPaymentInPLP) {
				Assert.assertTrue(eipDownPayment.isDisplayed(), "EIP Down Payment is not displayed");
			}
			Reporter.log("EIP Down Payments are present for all accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Down Payment on Accessories PLP page");
		}
		return this;
	}

	/**
	 * Verify that Monthly Payment is displayed
	 */
	public AccessoriesStandAlonePLPPage verifyMonthlyPayment() {
		try {
			waitforSpinner();
			for (WebElement eipMonthlyPayment : eipMonthlyPaymentInPLP) {
				Assert.assertTrue(eipMonthlyPayment.isDisplayed(), "EIP Monthly Payment is not displayed");
			}
			Reporter.log("EIP Monthly Payments are present for all accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Payment on Accessories PLP page");
		}
		return this;
	}

	/**
	 * Verify that Loan Terms is displayed
	 */

	public AccessoriesStandAlonePLPPage verifyLoanTerm() {
		try {
			waitforSpinner();
			for (WebElement eipLoanTerm : eipLoanTermInPLP) {
				Assert.assertTrue(eipLoanTerm.isDisplayed(), "EIP Loan Term is not displayed");
			}
			Reporter.log("EIP Loan Terms are present for all accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Payment on Accessories PLP page");
		}
		return this;
	}

	/**
	 * Verify that FRP is displayed
	 */
	public AccessoriesStandAlonePLPPage verifyFRPinPLP() {
		try {
			waitforSpinner();
			for (WebElement deviceFRP : eipFRPInPLP) {
				Assert.assertTrue(deviceFRP.isDisplayed(), "FRP is not displayed");
			}
			Reporter.log("FRPs are present for all accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP in Accessories PLP page");
		}
		return this;
	}

	/**
	 * Verify that Loan Term Length is only 9, 12, 24 or 36
	 */

	public AccessoriesStandAlonePLPPage verifyLoanTermLengthInPLP() {
		try {
			waitforSpinner();
			for (WebElement eipLoanTerm : eipLoanTermInPLP) {
				Assert.assertTrue(
						eipLoanTerm.getText().contains("9") | eipLoanTerm.getText().contains("12")
								| eipLoanTerm.getText().contains("24")
								| eipLoanTerm.getText().contains("36"),
						"Loan term length are correct for all accessories in Accessories PLP page");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Loan Term in Accessories PLP page ");
		}
		return this;
	}

	/**
	 * Get Accessory FRP with $ symbol
	 * 
	 * @return
	 */
	public Double getAccessoryFRPPriceInPLP() {
		String frpPrice;
		frpPrice = eipFRPInPLP.get(0).getText();
		frpPrice = frpPrice.substring(1, frpPrice.length() - 2) + "." + frpPrice.substring(frpPrice.length() - 2);
		return Double.parseDouble(frpPrice);
	}

	/**
	 * Get Accessory Down Payment with $ symbol
	 * 
	 * @return
	 */
	public String getAccessoryDownPaymentInPLP() {
		String downPayment;
		return downPayment = eipDownPaymentInPLP.get(0).getText();
	}

	/**
	 * Get Accessory Loan Term
	 * 
	 * @return
	 */
	public String getAccessoryLoanTermInPLP() {
		String loanTerm;
		loanTerm = eipLoanTermInPLP.get(0).getText().toLowerCase();
		loanTerm = loanTerm.substring(0, loanTerm.length() - 4) + " " + loanTerm.substring(loanTerm.length() - 4);
		return loanTerm;
	}

	/**
	 * Get Accessory Monthly Payment
	 * 
	 * @return
	 */
	public String getAccessoryMonthlyPaymentInPLP() {
		String monthlyPayment;
		monthlyPayment = eipMonthlyPaymentInPLP.get(0).getText();
		return monthlyPayment;
	}

	/**
	 * Click Accessory to add to cart
	 *
	 */
	public AccessoriesStandAlonePLPPage clickAddToCart(int number) {
		try {
			waitforSpinner();
			moveToElement(addToCartList.get(number));
			addToCartList.get(number).click();
			Reporter.log("Accessory is added to cart");

		} catch (Exception e) {
			Assert.fail("Failed to add accessory to the cart");
		}
		return this;
	}

	/**
	 * Get Accessory Total Price for particular accessory
	 *
	 */
	public Double getAccessoryPriceForParticularAccessory(int index) {

		String frpTotal;
		frpTotal = eipFRPInPLP.get(index).getText();
		String frpWithCents = frpTotal.substring(0, frpTotal.length() - 2) + "."
				+ eipFRPCentsInPLP.get(index).getText();
		String[] arr = frpWithCents.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Verify if the EIP total price is greater than $69
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePLPPage verifyAccessoryPriceForParticularAccessory(Double frpPrice) {
		try {
			if (frpPrice > 69.99) {
				Reporter.log("Total Accessory price is greater than $69.99");
			} else {
				Assert.fail("Total Accessory price is less than $69.99");
			}
		} catch (Exception e) {
			Assert.fail("This Accessory dont have EIP eligible accessories");
		}
		return this;
	}

	/**
	 * Verify if the EIP total price is greater than $69
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePLPPage verifyEIPEligibilityAndAddAccessory() {
		try {
			waitforSpinner();
			for (int i = 0; i < eipFRPInPLP.size(); i++) {
				String totalPrice = eipFRPInPLP.get(i).getText();
				int dollarIndex = totalPrice.indexOf("$");
				totalPrice = totalPrice.substring(dollarIndex + 1, totalPrice.length() - 2);
				Double parsedPrice = Double.parseDouble(totalPrice);
				Reporter.log(parsedPrice.toString());
				if (addToCartBtn.isDisplayed()) {
					if (parsedPrice > 69.99) {
						accessoryProductList.get(i).click();
						waitFor(ExpectedConditions.visibilityOf(addToCartBtn));
						clickElementWithJavaScript(addToCartBtn);
						break;
					}
				}
			}
			Reporter.log("Accessory verified and added");
		} catch (Exception e) {
			Assert.fail("Failed to verify and add accessory");
		}
		return this;
	}

	/**
	 * Verify Accessory with Promotion
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePLPPage verifyAccessoryWithCatalogPromotion() {
		try {
			waitforSpinner();
			checkPageIsReady();
			for (int i = 4; i < navigationtabs.size(); i++) {
				waitforSpinner();
				checkPageIsReady();
				if (accessoryFRPwithPromotion.size() > 0) {
					Assert.assertTrue(accessoryFRPwithPromotion.get(0).isDisplayed(),
							"Accessory with Catalog Promomtion is not displayed.");
					break;
				}
				clickElementWithJavaScript(navigationtabs.get(i + 1));
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Accessory with Catalog Promomtion");
		}
		return this;
	}

	/**
	 * Get Accessory Down Payment Price for particular accessory
	 *
	 */
	public Double getAccessoryDownPaymentPriceForParticularAccessory(int index) {

		String frpTotal;
		frpTotal = eipDownPaymentInPLP.get(index).getText();
		String[] arr = frpTotal.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Accessory EIP Price for particular accessory
	 *
	 */
	public Double getAccessoryEIPPriceForParticularAccessory(int index) {

		String frpTotal;
		frpTotal = eipMonthlyPaymentInPLP.get(index).getText();
		String frpWithCents = frpTotal.substring(0, frpTotal.length() - 2) + "."
				+ eipMonthlyPaymentCentsInPLP.get(index).getText();
		String[] arr = frpWithCents.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Accessory EIP Loan Term for particular accessory
	 *
	 */
	public Double getAccessoryEIPLoanTermForParticularAccessory(int index) {

		String loanTerm;
		loanTerm = eipLoanTermInPLP.get(index).getText().toLowerCase();
		loanTerm = loanTerm.substring(5, loanTerm.length() - 4);
		return Double.parseDouble(loanTerm);
	}

	/**
	 * Compare EIP Price for an Accessory
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePLPPage compareEIPPrice(Double eipPriceActual, Double calculatedEIPPrice) {
		try {
			DecimalFormat df = new DecimalFormat("0.00");
			String formate = df.format(calculatedEIPPrice);
			double finalValue = Double.parseDouble(formate);
			Assert.assertEquals(eipPriceActual, finalValue,
					"EIP Actual Price PLP does not Match with Calculated EIP Price PLP");
			Reporter.log("EIP Actual price PLP matches with Calculated EIP Price PLP");
		} catch (Exception e) {
			Assert.fail("Failed to compare EIP Actual price PLP is not matched with EIP Calculated Price PLP");
		}
		return this;
	}

	/**
	 * Select Accessory with Promotion
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePLPPage selectEIPAccessoryWithPromo() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(firstItemWithCatalogueP.get(0));

		} catch (Exception e) {
			Assert.fail("Failed to latest select accessory with promo");
		}
		return this;
	}

	/**
	 * Verify FRP is displayed for item with promo
	 */
	public AccessoriesStandAlonePLPPage verifyFRPinPLPforPromoItem() {
		try {
			waitforSpinner();
			for (WebElement deviceFRP : eipFRPInPLPforPromoItem) {
				Assert.assertTrue(deviceFRP.isDisplayed(), "FRP is not displayed");
			}
			Reporter.log("FRPs are present for all accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP in Accessories PLP page");
		}
		return this;
	}

	/**
	 * Get Accessory Total Price for particular accessory
	 *
	 */
	public Double getAccessoryPromoPriceForFirstAccessory() {

		String frpTotal;
		frpTotal = firstPromoFRP.get(0).getText();
		String frpWithCents = frpTotal.substring(0, frpTotal.length() - 2) + "." + firstPromoFRPCents.get(0).getText();
		String[] arr = frpWithCents.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Accessory Down Payment Price for particular accessory
	 *
	 */
	public Double getAccessoryDownPaymentPromoPriceForFirstAccessory() {

		String frpTotal;
		frpTotal = firstPromoDownPayment.get(0).getText();
		String[] arr = frpTotal.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Accessory EIP Price for particular accessory
	 *
	 */
	public Double getAccessoryPromoEIPPriceForFirstAccessory() {

		String frpTotal;
		frpTotal = firstPromoEIPMonthlyPayment.get(0).getText();
		String frpWithCents = frpTotal.substring(0, frpTotal.length() - 2) + "."
				+ firstPromoEIPMonthlyPaymentCents.get(0).getText();
		String[] arr = frpWithCents.split("\\$", 0);
		if (arr[1].contains("-")) {
			arr[1] = "-" + arr[1].replaceAll("-", "");
		}
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Accessory EIP Loan Term for particular accessory
	 *
	 */
	public Double getAccessoryPromoEIPLoanTermForFirstAccessory() {

		String loanTerm;
		loanTerm = firstPromoEIPLoanTerms.get(0).getText().toLowerCase();
		loanTerm = loanTerm.substring(5, loanTerm.length() - 4);
		return Double.parseDouble(loanTerm);
	}

	/**
	 * Verify that EIP Down Payment is displayed
	 */

	public AccessoriesStandAlonePLPPage verifyEIPDownPaymentPromo() {
		try {
			waitforSpinner();
			for (WebElement eipDownPayment : firstPromoDownPayment) {
				Assert.assertTrue(eipDownPayment.isDisplayed(), "EIP Down Payment for Promo devices is not displayed");
			}
			Reporter.log("EIP Down Payments are present for all Promo accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Down Payment for Promo devices on Accessories PLP page");
		}
		return this;
	}

	/**
	 * Verify that Monthly Payment is displayed
	 */
	public AccessoriesStandAlonePLPPage verifyMonthlyPaymentPromo() {
		try {
			waitforSpinner();
			for (WebElement eipMonthlyPayment : firstPromoEIPMonthlyPayment) {
				Assert.assertTrue(eipMonthlyPayment.isDisplayed(),
						"EIP Monthly Payment for Promo devices is not displayed");
			}
			Reporter.log("EIP Monthly Payments are present for all Promo accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Payment for Promo devices on Accessories PLP page");
		}
		return this;
	}

	/**
	 * Verify that Loan Terms is displayed
	 */

	public AccessoriesStandAlonePLPPage verifyLoanTermPromo() {
		try {
			waitforSpinner();
			for (WebElement eipLoanTerm : firstPromoEIPLoanTerms) {
				Assert.assertTrue(eipLoanTerm.isDisplayed(), "EIP Loan Term for Promo devices is not displayed");
			}
			Reporter.log("EIP Loan Terms are present for all Promo accessories in Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Payment for Promo devices on Accessories PLP page");
		}
		return this;
	}

	/**
	 * Verify that Loan Term Length is only 9, 12, 24 or 36
	 */

	public AccessoriesStandAlonePLPPage verifyLoanTermLengthInPLPPromo() {
		try {
			waitforSpinner();
			for (WebElement eipLoanTerm : firstPromoEIPLoanTerms) {
				Assert.assertTrue(eipLoanTerm.isDisplayed(), "EIP monthly installments text is displayed");
				Assert.assertTrue(eipLoanTerm.getText().contains("mos."),
						"EIP Loan Term is not displayed correctly");

				Reporter.log("EIP monthly installments text is displayed for all devices and accessories");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Loan Term for Promo devices in Accessories PLP page ");
		}
		return this;
	}

	/**
	 * Verify Clear Filter CTA
	 */
	public AccessoriesStandAlonePLPPage verifyClearFilterCTA() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(clearFilterCTAMobile), "Clear Filter CTA is not displayed");
			} else {
				Assert.assertTrue(isElementDisplayed(clearFilterCTA), "Clear Filter CTA is not displayed");
			}
			Reporter.log("Clear Filter CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Clear Filter CTA");
		}
		return this;
	}

	/**
	 * Click Clear Filter CTA
	 */
	public AccessoriesStandAlonePLPPage clickClearFilterCTA() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(clearFilterCTA);
				waitforSpinner();
			} else {
				// clearFilterCTA.click();
				clickElementWithJavaScript(clearFilterCTA);
				waitforSpinner();
			}
			Reporter.log("Clear Filter CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Clear Filter CTA");
		}
		return this;
	}

	/**
	 * verify accessory Categories
	 */
	public AccessoriesStandAlonePLPPage verifyEmptycatagoryName() {
		waitforSpinner();
		try {
			String catagorynotselected = emptycatagoryname.getAttribute("class");
			if (catagorynotselected.contains("ng-empty"))
				Reporter.log("selected category got reset");
		} catch (Exception e) {
			Assert.fail("Failed to verify empty category name");
		}
		return this;
	}

	/**
	 * verify accessory Categories
	 */
	public AccessoriesStandAlonePLPPage verifyEmptyForTextField() {
		waitforSpinner();
		try {
			String catagorynotselected = deviceList.getAttribute("class");
			if (catagorynotselected.contains("ng-empty"))
				Reporter.log("Text got reset");
		} catch (Exception e) {
			Assert.fail("Failed to verify Text in for field");
		}
		return this;
	}

	/**
	 * Verify Accessory Products in Accessory PLP page
	 */

	public AccessoriesStandAlonePLPPage verifyAccessorysProducts() {
		try {
			waitforSpinner();
			for (WebElement webElement : accessoryProductList) {
				Assert.assertTrue(webElement.isDisplayed());
				Reporter.log("Accessory Products are displayed");
			}
		} catch (Exception e) {
			Assert.fail("Accessory Products are not displayed");
		}
		return this;
	}

	/**
	 * Verify Accessory Products Using Page Nation
	 */
	public AccessoriesStandAlonePLPPage verifyAccessoryProductsUsingPageNation() {
		try {
			if (pageNationNextButton.isEnabled()) {
				pageNationNextButton.click();
				verifyAccessorysProducts();
			}
		} catch (Exception e) {
			Assert.fail("Accessory Page nation is not displayed");
		}
		return this;
	}

	/**
	 * Click PageNation First button
	 */
	public AccessoriesStandAlonePLPPage clickPageNationFirstButton() {
		try {
			pageNationFirstButton.click();
			Reporter.log("PageNation first button is clickable");
		} catch (Exception e) {
			Assert.fail("PageNation first button is not clickable");
		}
		return this;
	}

	/**
	 * Click PageNation button on accessories page
	 */
	public AccessoriesStandAlonePLPPage verifyPageNationButton() {
		checkPageIsReady();
		try {
			Assert.assertTrue(pageNationFirstButton.isDisplayed(), "First page navigation tag is not displayed ");
			Assert.assertTrue(pageNationPreviousButton.isDisplayed(), "Previous page navigation tag is not displayed ");
			Assert.assertTrue(pageNationNextButton.isDisplayed(), "Next page navigation tag is not displayed ");
			Assert.assertTrue(pageNationLastButton.isDisplayed(), "Last page navigation tag is not displayed ");
			Reporter.log("PageNation  buttons are verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Page navigation button's on accessories page");
		}
		return this;
	}

	/**
	 * verify button are greyed(non- enabled) in accessories page
	 */
	public AccessoriesStandAlonePLPPage verifyPageNavigationButtonAreGreyed(String value1, String value2) {
		checkPageIsReady();
		waitforSpinner();
		try {
			moveToElement(pageNationFirstButton);
			Assert.assertTrue((findElement(By.xpath("//*[@class='disableButton']/*[contains(text(), '" + value1 + "')]"))).isDisplayed(),
					value1 + "Button is enabled");
			Assert.assertTrue(
					(findElement(By.xpath("//*[@class='disableButton']/*[contains(text(), '" + value2 + "')]")))
							.isDisplayed(),
					value2 + "button is enabled");
			Reporter.log(value1 + value2 + "buttons are disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify page navigation button on accessory page is enabled or not");
		}
		return this;
	}

	/**
	 * Click PageNation Next button
	 * 
	 */
	public AccessoriesStandAlonePLPPage clickPageNationLastButton() {
		try {
			moveToElement(pageNationLastButton);
			clickElement(pageNationLastButton);
			Reporter.log("PageNation Last button is clickable");
		} catch (Exception e) {
			Assert.fail("PageNation last button is not clickable");
		}
		return this;
	}

	/**
	 * Selecting Category for Dropdown
	 */
	public AccessoriesStandAlonePLPPage selectCurrentCategoryFromDropdown(String cases) {
		waitforSpinner();

		try {
			selectElementFromDropDown(selectCategory, Constants.SELECT_BY_TEXT, cases);
			Reporter.log("Selected the category from dropdown");
		} catch (Exception e) {
			Assert.fail("Unable to select the category");
		}
		return this;
	}

	/**
	 * Selecting device for Current category
	 */
	public AccessoriesStandAlonePLPPage chooseDeviceForCategory() {
		waitforSpinner();
		try {
			sendTextData(deviceList, DEVICENAME);
			Reporter.log("Clicked on Device on Stand Alone PLP Page");
			deviceList.sendKeys(Keys.TAB);
		} catch (Exception e) {
			Assert.fail("Unable to select the device");
		}
		return this;
	}

	/**
	 * Verifies Accessory name
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePLPPage verifyAccessoryWithCaseNames() {
		waitforSpinner();
		checkPageIsReady();
		try {
			for (WebElement webElement : accessoryTitle) {
				Assert.assertTrue(webElement.getText().toLowerCase().contains("case"),"Incorrect accessories are displayed");
				Reporter.log("Accessory Products are displayed");
			}
		} catch (Exception e) {
			Assert.fail("Incorrect accessories are displayed");
		}
		return this;
	}

	/**
	 * Gets the total accessories count on PLP
	 * 
	 * @return
	 */
	public int getAccessoriesCount() {
		waitforSpinner();
		int i = 0;
		try {
			i = parseInt(accessoriesCount.getText());
			Reporter.log("Captured total accessories count :" + i);
		} catch (Exception e) {
			Assert.fail("Unable to capture Accessories count");
		}
		return i;
	}

	/**
	 * Applies Manufacturer filter
	 */
	public void applyManufacturerFilter() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				filterCTAMobile.click();
				waitFor(ExpectedConditions.visibilityOf(manufacturerDropdown));
				manufacturerDropdown.click();
				checkPageIsReady();
				manufactureApple.click();
				}
			 else {
				clickElement(manufacturerDropdown);
				for (WebElement manufacturer : manufacturerList) {
					manufacturer.click();
					waitforSpinner();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Unable to select manufacturer from dropdown");
		}
	}

	/**
	 * Verifies the Accessories result count after applying filter
	 */
	public void verifyAccessoriesCountAfterApplyingFilter(int accessoriesCount, int resultAccessoriesCount) {

		try {
			Assert.assertTrue(accessoriesCount != resultAccessoriesCount,
					"Results count is same even after applying filter");
			Reporter.log("Verified the Results count after applying filter");
		} catch (Exception e) {
			Assert.fail("Results count is same even after applying filter");
		}
	}

	/**
	 * Apply Sorting on Accessories PLP
	 */
	public void applySorting() {
		waitforSpinner();
		try {
			clickElement(priceSortDropdown);
			clickElement(priceSortOption.get(0));
			Reporter.log("Applied Sorting");
		} catch (Exception e) {
			Assert.fail("Unable to Sort the Accessories");
		}
	}

	/**
	 * Verifies Accessories Count after applying Sorting
	 * 
	 * @param accessoriesCount
	 * @param resultAccessoriesCount
	 */
	public void verifyAccessoriesCountAfterApplyingSorting(int accessoriesCount, int resultAccessoriesCount) {

		try {
			Assert.assertEquals(accessoriesCount,resultAccessoriesCount,
					"Results count isn't same after applying Sorting");
			Reporter.log("Verified the Results count after applying sorting");
		} catch (Exception e) {
			Assert.fail("Results count isn't same after applying sorting");
		}
	}

	/**
	 * Get Accessories Count per page
	 * 
	 * @return
	 */
	public int getAccessoriesResultsCountPerPage() {
		waitforSpinner();
		int i = 0;
		try {
			i = accessoryTitle.size();
			Reporter.log("Accessories per page :" + i);
		} catch (Exception e) {
			Reporter.log("Unable to get the Accessories count per page");
		}
		return i;
	}

	/**
	 * To change the Accessories per page value
	 */
	public void changeAccessoriesCountPerPage() {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Reporter.log("we are unable to count no of accessories displayed in mobile view");
			} else {
				selectElementFromDropDown(resultsPerPage, Constants.SELECT_BY_INDEX, "1");
				Reporter.log("Changed Results per page");
			}
		} catch (Exception e) {
			Assert.fail("Unable to change results per page");
		}
	}

	/**
	 * Verifies the Accessories Count per page
	 */
	public void verifyResultsCountPerPage(int resultsPerPage, int updatedResultsPerPage) {
		waitforSpinner();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Reporter.log("we can not compare total number accessories displayed  in mobile view");
			} else {
				Assert.assertTrue(resultsPerPage != updatedResultsPerPage,
						"Results count per page are same even after changing the count value from dropdown");
				Reporter.log("Results per page are displaying accordingly");
			}

		} catch (Exception e) {
			Assert.fail("Results count per page are same");
		}
	}

	/**
	 * Entering Generic Device Name for Current category
	 */
	public AccessoriesStandAlonePLPPage enterGenericDeviceName() {
		waitforSpinner();
		try {
			sendTextData(deviceList, DEVICENAME2);
			Reporter.log("Generic Device Name Entered on Stand Alone PLP Page");
		} catch (Exception e) {
			Assert.fail("Unable to enter the generic device name");
		}
		return this;
	}
	
	/**
	 * Verify stand alone accessory display
	 */
	public AccessoriesStandAlonePLPPage verifyFirstStandAloneAccessoryDevice() {
		waitforSpinner();
		try {
			waitForSpinnerInvisibility();
			moveToElement(accessoryProductList.get(0));
			Assert.assertTrue(accessoryProductList.get(0).isDisplayed(),
					"Accessories are not displayed");
			Assert.assertTrue(isElementDisplayed(accessoriesCount),"Accessories Results are not displayed");
			Reporter.log("Verify Stand Alone Accessories are displayed");
		} catch (Exception e) {
			Assert.fail("StandAlone accessories device is not available");
		}
		return this;	
	}
}
