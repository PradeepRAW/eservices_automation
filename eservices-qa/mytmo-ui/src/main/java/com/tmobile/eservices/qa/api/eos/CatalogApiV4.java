package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class CatalogApiV4 extends ApiCommonLib {
	public static String accessToken;
	Map<String, String> headers;

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, (description: 'ProductType value (PHONES,
	 * TABLETS, WEARABLES,HOTSPOTS) to return specific filters') -channelCode,
	 * -marketId. QueryParams:
	 * 
	 * @param tokenMap
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getFilterOptionsUsingGET(ApiTestData apiTestData, String filter, String marketID,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);
		

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		service.addQueryParam("productType", filter);
		// service.addQueryParam("channelCode", "MYT");
		// service.addQueryParam("marketId", marketID);
		Response response = service.callService("v4/catalog/deviceFilters", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, -channelCode, -marketId. QueryParams:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response retrieveProductsUsingPOST(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);
		String resourceURL = "v4/catalog/products";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, -channelCode, -marketId. QueryParams:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response retrieveGroupedProductsUsingPOST(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);
		String resourceURL = "v4/catalog/products/family";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}
	
	public Response retrieveGroupedProductApi(ApiTestData apiTestData, String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);
		String resourceURL = "product-catalog/v1/browse";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, -channelCode, -marketId. QueryParams:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response retrievePromotionsAtFamilyLevel(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);
		String resourceURL = "v4/catalog/products/family/promotions";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}

	private Map<String, String> buildGetCatalogHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		tokenMap.put("version", "v1");
		Map<String, String> headers = new HashMap<String, String>();
		Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		headers.put("accept", "application/json");
		headers.put("applicationid", "MYTMO");
		headers.put("cache-control", "no-cache");
		headers.put("channelid", "WEB");
		headers.put("clientid", "e-servicesUI");
		headers.put("Content-Type", "application/json");
		headers.put("correlationid", checkAndGetPlattokenJWT(apiTestData, jwtTokens.get("jwtToken")));
		headers.put("transactionbusinesskeytype", apiTestData.getBan());
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("transactionType", "ADDALINE");
		headers.put("usn", "12345");
		headers.put("interactionid", "xasdf1234asa4444");
		// headers.put("Authorization",checkAndGetJWTToken(apiTestData,
		// tokenMap).get("jwtToken"));
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		headers.put("transactionBusinessKey", "BAN");
		// headers.put("X-Auth-Originator",checkAndGetJWTToken(apiTestData,
		// tokenMap).get("jwtToken"));
		headers.put("X-Auth-Originator", jwtTokens.get("jwtToken"));
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}

}
