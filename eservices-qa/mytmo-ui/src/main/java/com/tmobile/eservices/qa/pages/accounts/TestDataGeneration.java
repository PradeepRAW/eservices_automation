package com.tmobile.eservices.qa.pages.accounts;

import java.awt.AWTException;
import java.awt.event.KeyEvent;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.tmobile.eservices.qa.pages.CommonPage;

public class TestDataGeneration extends CommonPage {

	String smisdin;
	String url = "http://devenas124.unix.gsm1900.org:9202/#/";
	String emailpin = "http://10.135.44.41:8008/cvspin/" + smisdin + "@t-mobile.com";
	String cvspin = "http://10.135.44.41:8008/cvspin/" + smisdin;
	String acturl = "";

	@FindBy(name = "userName")
	private WebElement userName;

	@FindBy(name = "password")
	private WebElement password;

	@FindBy(xpath = "//button[@type='submit']")
	private WebElement login;

	@FindBy(xpath = "//input[@name='qryInput']")
	private WebElement misdin;

	@FindBy(xpath = "//a[text()='Reconcile']")
	private WebElement Reconcile;

	@FindBy(xpath = "//pre")
	private WebElement pin;

	@FindBy(id = "usernameTextBox")
	private WebElement misdintxtbox;

	@FindBy(xpath = "//button[contains(text(),'Sign up')]")
	private WebElement Signup;

	@FindBy(id = "firstName")
	private WebElement firstName;

	@FindBy(id = "lastName")
	private WebElement lastName;

	@FindBy(id = "Email")
	private WebElement Email;

	@FindBy(xpath = "//button[text()='Sign me up']")
	private WebElement Signmeupbtn;

	@FindBy(id = "emailOTP")
	private WebElement emailOTP;

	@FindBy(id = "OTP")
	private WebElement OTP;

	@FindBy(xpath = "//input[@type='text']")
	private WebElement sixDigitpin;

	// testuser
	// testuser1

	public TestDataGeneration(WebDriver webDriver) {
		super(webDriver);
	}

	public TestDataGeneration NavigatetoURL(String url) {
		getDriver().get(url);
		return this;

	}

	public TestDataGeneration PerfomLoginSdat(String Uname, String pswd) {

		userName.sendKeys(Uname);
		password.sendKeys(pswd);
		login.click();
		return this;

	}

	public TestDataGeneration EnterMisdinNSearch() throws AWTException {
		misdin.sendKeys(smisdin);
		java.awt.Robot robot = new java.awt.Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		Reconcile.click();
		return this;

	}

	public String getpin() {

		String spin = pin.getText();
		int i = spin.length() - 2;
		int j = spin.length() - 8;
		String substrpin = spin.substring(j, i);
		return substrpin;

	}

	public TestDataGeneration EnterDtlsForReg() {

		misdintxtbox.sendKeys(smisdin);
		Signup.click();

		firstName.sendKeys("Test");
		lastName.sendKeys("Test");
		Email.sendKeys(smisdin + "@t-mobile.com");
		password.sendKeys("TM0Test1");
		Signmeupbtn.click();

		return this;

	}

	public TestDataGeneration EnterEmailOTPNVerify(String spin) {
		emailOTP.sendKeys(spin);
		login.click();
		return this;

	}

	public TestDataGeneration EntermobileOTPNVerify(String spin) {
		OTP.sendKeys(spin);
		login.click();
		return this;

	}

	public TestDataGeneration Enter6DigitPint() {

		sixDigitpin.sendKeys("121212");
		return this;

	}

	public TestDataGeneration EnterSecurityQuestionNAnswers() {
		return this;

	}

	public TestDataGeneration misdinRegistration() throws AWTException {
		NavigatetoURL(url);

		PerfomLoginSdat("testuser", "testuser1");

		EnterMisdinNSearch();

		NavigatetoURL(acturl);

		EnterDtlsForReg();

		NavigatetoURL(emailpin);
		String emailpin = getpin();

		EnterEmailOTPNVerify(emailpin);

		NavigatetoURL(cvspin);
		String cvspin = getpin();

		EntermobileOTPNVerify(cvspin);

		Enter6DigitPint();

		EnterSecurityQuestionNAnswers();

		return this;
	}

	@Test()
	public void TDG() throws AWTException {
		misdinRegistration();
	}
}
