package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class PaymentArrangementApiV3 extends ApiCommonLib {

	/***
	 * Summary: This API is used to setup and update paymentArrangement through
	 * PaymentArrangement API Headers:Authorization, -Content-Type , -activityId,
	 * -interactionId , -channelId,(This indicates whether the original request came
	 * from Mobile / web / retail / mw etc) -application_id ,
	 * 
	 * 
	 * 
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */

	private Map<String, String> buildPaymentArrangementCollectionHeader(ApiTestData apiTestData) throws Exception {
		clearCounters(apiTestData);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("applicationId", "ESERVICE");
		headers.put("activityId", "123");
		headers.put("channelId", "WEB");
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("workflowId", "MANAGEPAYMENTARRANGEMENT");
		headers.put("Content-Type", "application/json");
		headers.put("isV3ServicesEnabled", "true");
		headers.put("interactionId", "123456787");

		return headers;
	}

	public Response createPaymentArrangement(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildPaymentArrangementCollectionHeader(apiTestData);

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/paymentarrangement", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	public Response updatePaymentArrangement(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildPaymentArrangementCollectionHeader(apiTestData);

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/pa", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	public Response getPALandingDetails(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildHeadersForPALandingDetails(apiTestData, tokenMap);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/paymentmanager/palandingdetails", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildHeadersForPALandingDetails(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		/*
		 * JWTTokenApi jwtToken = new JWTTokenApi(); Map<String, String> jwtTokens =
		 * jwtToken.checkAndGetJWTToken(apiTestData, tokenMap);
		 * jwtToken.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"),
		 * tokenMap); headers.put("Authorization", "Bearer "+jwtTokens.get("jwtToken"));
		 */
		headers.put("Authorization", "Bearer " + checkAndGetAccessToken());
		headers.put("applicationId", "MYTMO");
		headers.put("ban", apiTestData.getBan());
		headers.put("channelId", "WEB");
		headers.put("clientId", "ESERVICE");
		headers.put("isSedona", tokenMap.get("isSedona"));
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("paRestApiEnabled", "false");
		headers.put("userRole", "PAH");

		return headers;
	}

	public Response migrationEosPA(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildHeadersForMigrationOfEosPA(apiTestData, tokenMap);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/paymentArrangement/paDetails", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildHeadersForMigrationOfEosPA(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + checkAndGetAccessToken());
		headers.put("Content-Type", "application/json");
		headers.put("ban", apiTestData.getBan());
		headers.put("Host", "stage2.eos.corporate.t-mobile.com");
		headers.put("application_id", "ESERVICE");
		headers.put("channel_id", "WEB");
		headers.put("msisdn", apiTestData.getMsisdn());
		return headers;
	}

	public Response generateQuote(ApiTestData apiTestData, Map<String, String> tokenMap, String updatedRequest) throws Exception {
		Map<String, String> headers = buildHeadersForGenerateQuote(apiTestData, tokenMap);

		RestService service = new RestService(updatedRequest, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/paymentmanager/generatequote", RestCallType.POST);
		System.out.println(response.getBody().asString());
		return response;
	}

	private Map<String, String> buildHeadersForGenerateQuote(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", getSmallAccessToken());
		headers.put("Content-Type", "application/json");
		headers.put("Host", "stage2.eos.corporate.t-mobile.com");
		headers.put("interactionId", "123123123");
		headers.put("workflowId", "MANAGEPAYMENTARRANGEMENT");
		headers.put("usn", "usn_sq_105");
		headers.put("Accept-Encoding", "gzip, deflate");
		
		return headers;
	}
}
