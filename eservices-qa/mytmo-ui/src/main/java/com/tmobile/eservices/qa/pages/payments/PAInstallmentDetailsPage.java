package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class PAInstallmentDetailsPage extends CommonPage {

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement pageLoadElement;

	@FindBy(css = ".multipleOption .row.padding-top-medium .body-copy-description-regular.pull-right")
	private WebElement amountBal;

	@FindBy(xpath = "//p[contains(text(),'Total Balance')]")
	private WebElement totalBalance;

	@FindBy(xpath = "//p[contains(text(),'Minimum to Restore')]")
	private WebElement minToRestore;

	@FindBy(xpath = "//p[contains(text(),'Past Due')]/../../../../../span")
	private WebElement pastDueBalance;

	@FindBy(css = "div#installmentBlade div p")
	private WebElement installmentBlade;

	@FindBy(xpath = "//p[contains(text(),'Edit details')]")
	private WebElement installmentPageHeader;

	private final String pageUrl = "/paymentarrangement#installmentDetails";
	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	@FindBy(css = "h4[ng-if='$ctrl.showDueDate']")
	private WebElement billDueDate;

	@FindBy(css = "span.Display3")
	private WebElement editDetailsHeader;

	@FindBy(css = "div.body.black")
	private List<WebElement> subHeaders;

	@FindBy(css = "span.Display6.d-inline-block.black")
	private List<WebElement> paymentOptions;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement cancelCTA;

	@FindBy(css = "div.mat-select-arrow-wrapper")
	private WebElement dropdownSelector;

	@FindBy(css = "span.mat-option-text")
	private List<WebElement> dropdownOptions;

	@FindBy(css = "span span.ng-tns-c15-3.ng-star-inserted")
	private WebElement dropdownValue;

	@FindBy(css = "i.fa.fa-calendar")
	private List<WebElement> installmentDateCalendarIcon;

	@FindBy(css = "#installment1")
	private WebElement firstInstallmentAmountInputField;

	@FindBy(css = "#installment2")
	private WebElement secondInstallmentAmountInputField;

	@FindBy(xpath = "//p[contains(text(),'Min $')]")
	private WebElement minMaxAmount;

	@FindBy(xpath = "//p[contains(text(),'Total Installments')]")
	private WebElement totalInstallment;

	@FindBy(xpath = "//div[contains(text(),'Payment method')]")
	private WebElement paymentMethodBlade;

	@FindBy(xpath = "//div[contains(text(),'Payment method')]/following-sibling::*/p[contains(text(),'*')]")
	private WebElement cardSprite;

	@FindBy(linkText = "view details")
	private WebElement viewDetailsLink;

	private WebElement breakdownModal;

	private WebElement breakdownModalHeader;

	private WebElement breakdownModalValues;

	private WebElement closeIconBreakDownModal;

	@FindBy(css = "")
	private WebElement dateCalendarModal;

	@FindBy(css = "")
	private WebElement cancelCTAonCalendarModal;

	@FindBy(css = "div.multipleOption p.body-copy-description-regular.text-black")
	private List<WebElement> radioButtons;

	@FindBy(css = "div.row.ng-star-inserted div.font-italic")
	private List<WebElement> spokeMessage;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public PAInstallmentDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PAInstallmentDetailsPage verifyPageUrl() {
		waitFor(ExpectedConditions.or(ExpectedConditions.urlContains(pageUrl),
				ExpectedConditions.urlContains("/editDetails")));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PAInstallmentDetailsPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Plan installment page not loaded");
		}
		return this;
	}

	/**
	 * Verify payment method blade is displayed
	 * 
	 * @throws InterruptedException
	 */
	public PAInstallmentDetailsPage verifypaymentMethodBlade() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(paymentMethodBlade));
			Assert.assertTrue(paymentMethodBlade.isDisplayed());
			Reporter.log("Payment method is displayed successfully");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("unable to validate payment method successfully");
		}
		return this;
	}

	/**
	 * Verify payment method blade is displayed
	 * 
	 * @throws InterruptedException
	 */
	public PAInstallmentDetailsPage verifyCardSprite() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(cardSprite));
			Assert.assertTrue(cardSprite.isDisplayed());
			Reporter.log("card sprite last 4 digit is displayed successfully");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("unable to validate card sprite last 4 digit");
		}
		return this;
	}

	public PAInstallmentDetailsPage verifyAmountIsDisplayed() {
		try {
			amountBal.isDisplayed();
			Reporter.log("Balance amount displayed");
		} catch (Exception e) {
			Assert.fail("Balance amount not found");
		}
		return this;
	}

	/*
	 * Verify PA - Edit Details - Total Balance is displayed
	 */
	public PAInstallmentDetailsPage verifyTotalBalanceDisplayd() {
		try {
			Assert.assertTrue(totalBalance.isDisplayed(), "Total Balance is not displayed");
			Reporter.log("Total Balance is displayed");
		} catch (Exception e) {
			Assert.fail("Total balance not found");
		}
		return this;
	}

	/*
	 * Verify PA - Edit Details - Minimum to restore is displayed
	 */
	public PAInstallmentDetailsPage verifyMinToRestoreDisplayd() {
		try {
			Assert.assertTrue(minToRestore.isDisplayed(), "Minimum to Restore is not displayed");
			Reporter.log("Minimum to Restore is displayed");
		} catch (Exception e) {
			Assert.fail("Minimum to Restore is not displayed");
		}
		return this;
	}

	/*
	 * Verify PA - Edit Details - Past Due balance is displayed
	 */
	public PAInstallmentDetailsPage verifyPastDueBalDisplayd() {
		try {
			checkPageIsReady();
			pastDueBalance.isDisplayed();
			Reporter.log("Past Due is displayed");
		} catch (Exception e) {
			Assert.fail("Past Due is not displayed");
		}
		return this;
	}

	public PAInstallmentDetailsPage clickinstallemtBlade() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.visibilityOf(installmentBlade));
			clickElementWithJavaScript(installmentBlade);
			Reporter.log("Clicked on installment blade");
		} catch (Exception e) {
			Assert.fail("Installment blade not found");
		}
		return this;
	}

	public PAInstallmentDetailsPage verifyInstallmentPageDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(installmentPageHeader));
			installmentPageHeader.isDisplayed();
			Reporter.log("Installment page header displayed");
		} catch (Exception e) {
			Assert.fail("Installement page header not found");
		}
		return this;
	}

	/*
	 * get billDueDate
	 */
	public PAInstallmentDetailsPage getBillDueDate() {
		try {

			checkPageIsReady();
			String date = billDueDate.getText();
			date = date.substring(date.indexOf("on "), date.length());
			System.out.println(date);

			Reporter.log("Past Due is displayed");
		} catch (Exception e) {
			Assert.fail("Past Due is not displayed");
		}
		return this;
	}

	/**
	 * Verify header and sub header on Installment page
	 */
	public void verifyHeaderandSubHeader(String pageHeaderText, String subHeaderText) {
		try {
			boolean isDisplayed = false;
			editDetailsHeader.isDisplayed();
			Assert.assertTrue(editDetailsHeader.getText().equals(pageHeaderText),
					"Page Header text mismatch. Actual: " + pageHeaderText + " | Expected: " + subHeaderText);
			Reporter.log("Verified Header on PA Installment page");
			for (WebElement subHeader : subHeaders) {
				if (subHeader.getText().equals(subHeaderText)) {
					Reporter.log("Verified Sub Header on PA Installment page");
					isDisplayed = true;
					break;
				}
			}
			if (!isDisplayed) {
				Assert.fail("Failed to verify Sub Header on PA Installment page");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Headers on PA Installment page");
		}
	}

	/**
	 * Verify amount options displayed for payment
	 * 
	 * @param amountOption
	 */
	public void verifyAmountOptions(String amountOption) {
		try {
			boolean isDisplayed = false;
			for (WebElement options : paymentOptions) {
				if (options.isDisplayed() && options.getText().equals(amountOption)) {
					isDisplayed = true;
					break;
				}
				if (!isDisplayed) {
					Assert.fail("Amount option: " + amountOption + " is not displayed.");
				}
				Reporter.log("Verified that Amount option: " + amountOption + " is displayed.");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify amount options on PA Installment page");
		}
	}

	/**
	 * select installment dropdown value and check respective installments displayed
	 */
	public void selectAndVerifyInstallmentsDropdown() {
		try {
			int size;
			for (int i = 0; i < dropdownOptions.size(); i++) {
				dropdownSelector.click();
				dropdownOptions.get(i).isDisplayed();
				dropdownOptions.get(i).click();
				size = Integer.parseInt(dropdownValue.getText().replaceAll("[^.0-9]", "").trim());
				Assert.assertEquals(installmentDateCalendarIcon.size(), size,
						"Selected No. of Installments and Displayed isntallments mismatch");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify installments dropdown");
		}
	}

	/**
	 * click on Cancel CTA
	 */
	public void clickCancelCTA() {
		try {
			cancelCTA.click();
			Reporter.log("Clicked on Cancel CTA");
		} catch (Exception e) {
			Assert.fail("Failed to click on Cancel CTA");
		}
	}

	/**
	 * verify view details link
	 * 
	 * @param displayedOrNot
	 */
	public void verifyViewDetailsLink(boolean displayedOrNot) {
		try {
			Assert.assertEquals(viewDetailsLink.isDisplayed(), displayedOrNot,
					"View details link - Expected: " + displayedOrNot + " Actual: " + viewDetailsLink.isDisplayed());
			Reporter.log("View details link is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify view details link");
		}
	}

	/**
	 * click view details link
	 */
	public void clickViewDetailsLinkTotalBal() {
		try {
			viewDetailsLink.click();
			Reporter.log("Clicked on view details link");
		} catch (Exception e) {
			Assert.fail("Failed to click on View details link");
		}
	}

	/**
	 * Enters amount in first installment input box
	 */
	public void enterAmountInFirstInstallmentfield(String firstInstmntValue) {
		try {
			sendTextData(firstInstallmentAmountInputField, firstInstmntValue);
			firstInstallmentAmountInputField.sendKeys(Keys.TAB);
			Reporter.log("Successfully Entered amount for installment one ");
		} catch (Exception e) {
			Assert.fail("Failed to enter value in installment one");
		}
	}

	/**
	 * retrieves Minimum Amount
	 */
	public String retrieveMinimumAmount() {

		String minimumAmount = "";
		try {

			String minMaxValues[] = minMaxAmount.getText().toString().split(" ");
			minimumAmount = minMaxValues[1].substring(1);

		} catch (Exception e) {

		}
		return minimumAmount;
	}

	/**
	 * retrieves total Amount
	 */
	public String retrieveTotalAmount() {

		String totalAmount = "";
		try {

			String totalInstallments[] = totalInstallment.getText().split(" ");
			totalAmount = totalInstallments[2].substring(1);

		} catch (Exception e) {

		}
		return totalAmount;
	}

	/**
	 * retrieves Maximum Amount
	 */
	public String retrieveMaximumAmount() {

		String maximumAmount = "";
		try {

			String minMaxValues[] = minMaxAmount.getText().toString().split(" ");
			maximumAmount = minMaxValues[3].substring(1);

		} catch (Exception e) {

		}
		return maximumAmount;
	}

	/**
	 * It will verify if amount entered is less than minimum amount then value is
	 * adjusted to minimum amount
	 */
	public PAInstallmentDetailsPage verifyEnteringAmountLessThanMinimumAmount() {
		try {
			String minAmount = retrieveMinimumAmount();
			int minAmnt = Integer.parseInt(minAmount);
			int firstInstallmentAmount = Integer.parseInt(firstInstallmentAmountInputField.getText());
			if (firstInstallmentAmount < minAmnt) {
				firstInstallmentAmountInputField.sendKeys(Keys.TAB);
				Assert.assertEquals(minAmount, firstInstallmentAmountInputField.getText());
				Reporter.log(
						"Amount adjusted to minimum amount when first installment amount is less than minimum amount");
			}
		} catch (Exception e) {
			Assert.fail("Failed to validate amount which is less than minimium amount");
		}

		return this;
	}

	/**
	 * It will verify if amount entered is more than maximum amount then value is
	 * adjusted to minimum amount
	 */
	public PAInstallmentDetailsPage verifyEnteringAmountMoreThanMaximumAmount() {
		try {
			String maxAmount = retrieveMaximumAmount();
			int maxAmnt = Integer.parseInt(maxAmount);
			int firstInstallmentAmount = Integer.parseInt(firstInstallmentAmountInputField.getText());
			if (firstInstallmentAmount < maxAmnt) {
				firstInstallmentAmountInputField.sendKeys(Keys.TAB);
				Assert.assertEquals(maxAmount, firstInstallmentAmountInputField.getText());
				Reporter.log(
						"Amount adjusted to maximum amount when first installment amount is more than maximum amount");
			}

		} catch (Exception e) {
			Assert.fail("Failed to validate amount which is more than maximum amount");
		}

		return this;
	}

	/**
	 * It will verify if amount entered is more than maximum amount then value is
	 * adjusted to minimum amount
	 */
	public PAInstallmentDetailsPage verifyEnteringAmountBetweenMinAndMaxAmount(String AmountBetweenMinAndMax) {
		try {

			int totalAmount = Integer.parseInt(retrieveTotalAmount());
			int minAmount = Integer.parseInt(retrieveMinimumAmount());
			int maxAmount = Integer.parseInt(retrieveMaximumAmount());
			int firstInstallmentAmount = Integer.parseInt(AmountBetweenMinAndMax);

			if ((firstInstallmentAmount > minAmount) && (firstInstallmentAmount < maxAmount)) {
				firstInstallmentAmountInputField.sendKeys(Keys.TAB);
				int secondInstallmentAmount = Integer.parseInt(secondInstallmentAmountInputField.getAttribute("value"));
				Assert.assertTrue(totalAmount == firstInstallmentAmount + secondInstallmentAmount);
				Reporter.log("Amount adjusted");
			}

		} catch (Exception e) {
			Assert.fail("Failed to validate  entering  amount between min and max");
		}

		return this;
	}

	/**
	 * It will verify if amount entered is more than maximum amount then value is
	 * adjusted to minimum amount
	 */
	public PAInstallmentDetailsPage verifyWhenAmountEnteredWithoutDecimals(String firstInstallmentAmount) {
		try {
			sendTextData(firstInstallmentAmountInputField, firstInstallmentAmount);
			firstInstallmentAmountInputField.sendKeys(Keys.TAB);
			Assert.assertEquals(firstInstallmentAmountInputField.getText(), firstInstallmentAmount + ".00");
			Reporter.log("Decimal validation is done successfully");

		}

		catch (Exception e) {
			Assert.fail("Failed to validate decimal validation");
		}

		return this;
	}

	/**
	 * click view details link
	 */
	public void clickViewDetailsLinkPastDueBal() {
		try {
			viewDetailsLink.click();
			Reporter.log("Clicked on view details link");
		} catch (Exception e) {
			Assert.fail("Failed to click on View details link");
		}
	}

	/**
	 * verify breakdown modal and fee break down details
	 */
	public void verifyBreakDownModal() {
		try {
			breakdownModal.isDisplayed();
			breakdownModalHeader.isDisplayed();
			breakdownModalValues.isDisplayed();
			Reporter.log("Verified Breakdown modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify breakdown modal");
		}
	}

	/**
	 * click close on break down modal
	 */
	public void closeBreakDownModal() {
		try {
			closeIconBreakDownModal.click();
			Reporter.log("Clicked close icon on breakdown modal");
		} catch (Exception e) {
			Assert.fail("Failed to click on close icon breakdown modal");
		}
	}

	/**
	 * click date icon on installment blades
	 */
	public void clickDateIcon() {
		try {
			installmentDateCalendarIcon.get(0).click();
			Reporter.log("Clicked on Date icon");
		} catch (Exception e) {
			Assert.fail("Failed to click on Date icon");
		}
	}

	/**
	 * verify calendar modal
	 */
	public void verifyCalendarModal() {
		try {
			dateCalendarModal.isDisplayed();
			Reporter.log("Verified Date calendar modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify date calendar modal");
		}
	}

	/**
	 * verify cancel CTA on Calendar modal
	 */
	public void verifyCancelCTAOnCalendarModal() {
		try {
			dateCalendarModal.isDisplayed();
			Reporter.log("Verified Date calendar modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify date calendar modal");
		}
	}

	/**
	 * click cancel CTA on Modal
	 */
	public void clickCancelCTAOnModal() {
		try {
			cancelCTAonCalendarModal.click();
			Reporter.log("Clicked on Cancel CTA of Calendar modal");
		} catch (Exception e) {
			Assert.fail("Faield to click on cancel CTA on Date modal");
		}
	}

	/**
	 * verify amount in the first installment field
	 * 
	 * @param expected
	 * @param string
	 */
	public void verifyAmountinFirstInstallmentField(String amtVal, boolean expected) {
		try {
			Assert.assertEquals(firstInstallmentAmountInputField.getAttribute("value").replace("$", "").equals(amtVal),
					expected);
			Reporter.log("Verified the amount in first installment field");
		} catch (Exception e) {
			Assert.fail("Failed to verify amount value in first installment field");
		}
	}

	public void verifyAmountinSecondInstallmentField(String amtVal, boolean expected) {
		try {
			Assert.assertEquals(secondInstallmentAmountInputField.getAttribute("value").replace("$", "").equals(amtVal),
					expected);
			Reporter.log("Verified the amount in second installment field");
		} catch (Exception e) {
			Assert.fail("Failed to verify amount value in second installment field");
		}
	}

	/**
	 * click on past due radio button
	 * 
	 * @param amountType
	 */
	public void clickRadioButton(String amountType) {
		try {
			WebElement radioButton = getDriver().findElement(By.xpath("//span[contains(text(),'" + amountType
					+ "')]//..//..//..//div[contains(@class,'radio-container')]"));
			radioButton.click();
			/*
			 * for (WebElement option : radioButtons) { if
			 * (option.getText().contains(amountType)) { option.click(); break; } }
			 */
		} catch (Exception e) {
			Assert.fail("Failed to click on past due radio button");
		}
	}

	public void verifySpokeMessaging(String paSpokeMessaging) {
		try {
			for (WebElement msg : spokeMessage) {
				if (msg.isDisplayed() && msg.getText().equals(paSpokeMessaging)) {
					Reporter.log("Verified that the spoke message is displayed");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify spoke message");
		}
	}

	public void verifySpokeMessagingIsNotDisplayed() {
		try {
			Assert.assertTrue(spokeMessage.size() == 0);
			Reporter.log("Verified that the spoke message is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify spoke message");
		}
	}

}
