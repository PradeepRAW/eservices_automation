package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class PAConfirmationPage extends CommonPage {
	
	@FindBy(css = "span.check-icon")
	private WebElement pageLoadElement;
	
	@FindBy(css = "button.PrimaryCTA")
	private WebElement returnHomeCTA;

	private final String pageUrl = "/paymentarrangement/confirmation";

	@FindBy(css="p.mb-0")
	private List<WebElement> updateHeaderAndAlerts;
	
	@FindBy(css="div.Display3 p")
	private WebElement paSetupConfirmationHeader;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public PAConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}
	/**
     * Verify that current page URL matches the expected URL.
     */
    public PAConfirmationPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public PAConfirmationPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
	    	waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("Payment Arrangemet Confirmaton page not loaded");
    	}
        return this;
    }
    
    public PAConfirmationPage clickReturnToHome() {
		try {
			returnHomeCTA.click();
			Reporter.log("Clicked on Return to home");
		} catch (Exception e) {
			Assert.fail("Return to home button not found");
		}
		return this;
	}

    /**
     * verify PA confirmation page header
     */
	public void verifyPAConfirmationPageHeader(String headerMsg) {
		try {
		Assert.assertTrue(paSetupConfirmationHeader.getText().equals(headerMsg), "Mismatch of expected and actual PA Confirmation Header. Actual: "+paSetupConfirmationHeader.getText());
		Reporter.log("Verified PA Confirmation Header: "+headerMsg);
		} catch (Exception e) {
			Assert.fail("Failed to verify PA confirmation page Header");
		}
	}
    
    /**
     * verify confirmation page alerts for modify PA
     */
	public void verifyModifyPaConfirmationAlerts(String alertMsg) {
		try {
			
			for (WebElement alert : updateHeaderAndAlerts) {
				String alertText = alert.getText();
				if (alert.isDisplayed() && alertText.contains(alertMsg)) {
					Reporter.log("Verified Modify PA Header: "+alertText);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Modify PA alerts on confirmation page");
		}
	}

}
