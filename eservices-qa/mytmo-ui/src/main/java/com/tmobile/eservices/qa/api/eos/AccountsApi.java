package com.tmobile.eservices.qa.api.eos;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class AccountsApi extends ApiCommonLib {

	/***
	 * This is the API for EOS Account Management Service parameters: - $ref:
	 * '#/parameters/oAuth' - $ref: '#/parameters/transactionId' - $ref:
	 * '#/parameters/correlationId' - $ref: '#/parameters/applicationId' - $ref:
	 * '#/parameters/channelId' - $ref: '#/parameters/clientId' - $ref:
	 * '#/parameters/transactionBusinessKey' - $ref:
	 * '#/parameters/transactionBusinessKeyType' - $ref:
	 * '#/parameters/transactionType'
	 * 
	 * @return
	 * @throws Exception
	 */

	// Gets stored payment methods on an account. Returns collection of
	// PaymentMethod objects.

	public Response getStoredPayment(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v2/accounts/" + tokenMap.get("ban") + "/paymentmethods";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.GET);
		return response;
	}

	// Gets reponse for linedetails api

	public Response getlineDetailsApi(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = getHeadersForaccountdetailslookup_linedetails(apiTestData);
		String resourceURL = "/v1/accountdetailslookup/linedetails/" + apiTestData.ban;
		Response response = invokeRestServiceCall(headers, "https://stage.eos.corporate.t-mobile.com", resourceURL, "",
				RestServiceCallType.GET);
		return response;
	}

	private Map<String, String> getHeadersForaccountdetailslookup_linedetails(ApiTestData apiTestData) {
		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("Accept", "application/json");
		tokenMap.put("Authorization", "");
		tokenMap.put("Content-Type", "application/json");
		tokenMap.put("X-B3-SpanId", "1241234");
		tokenMap.put("X-B3-TraceId", "123456787");
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("channel_id", "DESKTOP");
		return tokenMap;
	}

	// Gets reponse for BenefitsV1 api

	public Response getBenefitsV1Api(String requestBody) throws Exception {
		Map<String, String> headers = getHeadersForBenefitV1();
		String resourceURL = "/cx/v1/benefits";
		Response response = invokeRestServiceCall(headers, "https://qlab02.eos.corporate.t-mobile.com", resourceURL,
				requestBody, RestServiceCallType.POST);
		return response;
	}

	private Map<String, String> getHeadersForBenefitV1() {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization",
				"Bearer eyJraWQiOiI0Nzc1M2UzZi0zOGFjLTQ1ODQtZDcxYy0zZDgxYTE1Nzk5NWMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJmWU9zRjlGZTVGR3czTFFlNmYyQUcwWE5sSlpObnZ0byIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlBPTEFSSVNcIn0iLCJpc3MiOiJodHRwczpcL1wvcWF0MDEuYXBpLnQtbW9iaWxlLmNvbVwvb2F1dGgyXC92NCIsIm1hc3RlckRlYWxlckNvZGUiOiIiLCJhdXRoVGltZSI6IjE1NTg0NzQ0NTY5OTUiLCJ1c24iOiI5ODA0MThhYy01YWExLTUxOWQtODFiNy0yZGNkNGM2ODY4ZTgiLCJhdWQiOiJmWU9zRjlGZTVGR3czTFFlNmYyQUcwWE5sSlpObnZ0byIsInNlbmRlcklkIjoiIiwibmJmIjoxNTU4NDc0NDU3LCJhcHBsaWNhdGlvbklkIjoiIiwiZXhwIjoxNTU4NDc4MDU3LCJpYXQiOjE1NTg0NzQ0NTcsImNoYW5uZWxJZCI6IiIsImp0aSI6IjNkMWE3ZGNlLTNmYjAtODQ4MC00NGUwLTAxMzMwOGUzNTc1YiJ9.w9fbACqsL2LotYKVPQ7PH7XvEYnHonnFwI_molY9uWzel-Xa0ktj9hN-4AAUchKyiO1u3rX3bA2gtk4DhbS58peuetXcZs7Nutjssl4MTwGnXIhLx_7eyXB7YpDbxNtyRSSVvKGrbJ7DkssjM3S9GAGKAnpxEj2R7kts-PqX2gP3UXO4fLSyeBQj_5g4F8hpJHBR7gFu3xVHGmMyshvP8gQ_zxyQRfndIUCClgLcsTfeoU4s-J1ncZPaWAMautJOHUac5slfV4dhjzodzV49nDjub7i0vxJYJ11vvwvYl7v6avRoIF-y6yE5JE10J7s6cZq7HSXAOLKriSgzAgi6lw");
		headers.put("interactionId", "12345");
		headers.put("contentType", "application/json");
		headers.put("accept", "application/json");
		return headers;
	}

	// Gets reponse for BenefitsV1 api

	public Response getCallerIdV1Api(String requestBody) throws Exception {
		Map<String, String> headers = getHeadersForBenefitV1();
		String resourceURL = "/cx/v1/profile/line-settings/callerid";
		Response response = invokeRestServiceCall(headers, "https://qlab02.eos.corporate.t-mobile.com", resourceURL,
				requestBody, RestServiceCallType.POST);
		return response;
	}

	public Response updateCallerIdV1Api(String requestBody) throws Exception {
		Map<String, String> headers = getHeadersForBenefitV1();
		String resourceURL = "/cx/v1/profile/line-settings/updatecallerid";
		Response response = invokeRestServiceCall(headers, "https://qlab02.eos.corporate.t-mobile.com", resourceURL,
				requestBody, RestServiceCallType.POST);
		return response;
	}

	// Create new payment method

	public Response createNewPaymentMethodBank(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v2/accounts/" + tokenMap.get("ban") + "/paymentmethods/CHECK";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.PUT);
		return response;
	}

	// Create new payment method

	public Response createNewPaymentMethodCard(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v2/accounts/" + tokenMap.get("ban") + "/paymentmethods/CREDIT";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.PUT);
		return response;
	}

	// Create new payment method

	public Response updateStoredPaymentMethodCard(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v2/accounts/" + tokenMap.get("ban") + "/paymentmethods/CREDIT";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.PUT);
		return response;
	}

	public Response updatePaymentMethodBank(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v2/accounts/" + tokenMap.get("ban") + "/paymentmethods/CHECK";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.PUT);
		return response;
	}

	// Delete payment method

	public Response deletePaymentMethod(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v2/accounts/" + apiTestData.getBan() + "/paymentmethods/" + apiTestData.getPaymentCode();
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.DELETE);
		return response;
	}

	public Response validatePaymentMethod(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v2/accounts/" + apiTestData.getBan() + "/paymentmethods";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	private Map<String, String> buildAccountPaymentsAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		String accessToken = checkAndGetAccessToken();
		tokenMap.put("accessToken", accessToken);
		Map<String, String> headers = new HashMap<String, String>();

		headers.put("Content-Type", "application/json");
		headers.put("Authorization", accessToken);
		headers.put("interactionId", "1222");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid", "PaymentsAPITest");
		return headers;
	}

	private Map<String, String> buildAccountPlansAPIHeader(ApiTestData apiTestData, String jwtToken) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "Bearer " + jwtToken);
		headers.put("interactionId", "1222");
		headers.put("X-Auth-Originator", jwtToken);
		headers.put("Accept", "application/json");
		headers.put("activity-id", "121212");
		headers.put("application-id", "333");
		headers.put("channel-id	", "444");
		headers.put("dealercode", "123");

		return headers;
	}

	private Map<String, String> buildAccountGetEligiblePlansAPIHeader(ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		headers.put("Accept", "application/json");
		headers.put("Authorization", "Bearer " + jwtTokens.get("jwtToken"));
		headers.put("Content-Type", "application/json");
		headers.put("X-B3-SpanId", "222");
		headers.put("X-B3-TraceId", "222");
		headers.put("ban", apiTestData.getBan());
		headers.put("cache-control", "no-cache");
		headers.put("channel_id", "WEB");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("usn", "123456");
		return headers;
	}

	private Map<String, String> buildAccountAPIHeader(ApiTestData apiTestData) throws Exception {
		// clearCounters(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("channel_id", "DESKTOP");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("ban", apiTestData.getBan());
		headers.put("Content-Type", "application/json");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid", "PaymentsAPITest");
		return headers;
	}

	private Map<String, String> buildAccountBenefitsHeader(ApiTestData apiTestData) throws Exception {
		// clearCounters(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("interactionid", "321321312");
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid", "PaymentsAPITest");
		return headers;
	}

	// post_OnDeviceFulFillment
	public Response getOnDeviceFulFillment(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		String resourceURL = "v1/cpslookup/ondevicefulfillment";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	// post_benefits
	public Response getBenefits(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountBenefitsHeader(apiTestData);
		String resourceURL = "cx/v1/benefits";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	// post_getCurrentPasses
	public Response getCurrentPasses(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		String resourceURL = "v1/cpslookup/getCurrentPasses";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	// post_getSocParameter
	public Response getSocParameter(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		String resourceURL = "v1/cpslookup/getSocParameter";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	// post_getCurrentPasses
	public Response getServiceChangeEffectiveDates(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		String resourceURL = "v1/cpslookup/getServiceChangeEffectiveDates";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	// post_getEligiblePasses
	public Response getEligiblePasses(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		String resourceURL = "v1/cpslookup/getEligiblePasses";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	// getCurrentServices
	public Response getEligibleServices(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		String resourceURL = "v1/cpslookup/eligibleServices";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	// MPM Plans
	public Response getMPMRatePlans(ApiTestData apiTestData, String requestBody, String rateplanSOC) throws Exception {

		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		RestService service = new RestService(requestBody, "https://core.op.api.internal.t-mobile.com:443");
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		String url = "/products/v3/rateplans?soc=" + rateplanSOC + "&channel=MYT";
		Response response = service.callService(url, RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	public Response getCurrentServices(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildAccountAPIHeader(apiTestData);
		String resourceURL = "v1/cpslookup/getCurrentServices";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	public Response getAccountPlansMethod(ApiTestData apiTestData, String jwtToken) throws Exception {
		Map<String, String> headers = buildAccountPlansAPIHeader(apiTestData, jwtToken);
		String encodedBan = Base64.getEncoder().encodeToString(apiTestData.ban.getBytes("utf-8"));
		String resourceURL = "billing-experience/v3/recurring-items/" + encodedBan;
		Response response = invokeRestServiceCall(headers, "https://api.t-mobile.com", resourceURL, "",
				RestServiceCallType.GET);
		return response;
	}

	// getEligiblePlans
	public Response getEligiblePlansMethod(ApiTestData apiTestData, Map<String, String> tokenMap, String requestBody)
			throws Exception {
		Map<String, String> headers = buildAccountGetEligiblePlansAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/cpslookup/getEligiblePlans";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	public List<String> getValuesForGivenKey(String jsonArrayStr, String key) {
		JSONArray jsonArray = new JSONArray(jsonArrayStr);
		return IntStream.range(0, jsonArray.length())
				.mapToObj(index -> ((JSONObject) jsonArray.get(index)).optString(key)).collect(Collectors.toList());
	}

	public List<String> getRatePlanFeatureTM1TI2() {

		List<String> ls = new ArrayList<>();

		ls.add("Voicemail to Text");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Music streaming included with Music Freedom");
		ls.add("Talk & text");
		ls.add("Unused data rollover");
		ls.add("Name ID");
		ls.add("Optimized video streaming");
		ls.add("Mobile hotspot");
		ls.add("Taxes and fees included");
		ls.add("AutoPay discount");
		ls.add("HD video streaming");
		ls.add("No annual service contract");
		ls.add("Gogo in-flight benefits");
		ls.add("Other discounts");
		ls.add("Netflix™ On Us");
		ls.add("Wi-Fi calling");
		ls.add("Family Allowances");
		ls.add("Unlimited international texting from home");
		ls.add("Video streaming included with Binge On");
		ls.add("High-speed data");
		ls.add("T-Mobile Family Mode");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Overseas voice calling");

		return ls;
	}

	public List<String> getRatePlanFeatureTMESNTL2() {

		List<String> ls = new ArrayList<>();

		ls.add("Taxes and fees included");
		ls.add("Other discounts");
		ls.add("Family Allowances");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Unlimited international texting from home");
		ls.add("Music streaming included with Music Freedom");
		ls.add("Video streaming included with Binge On");
		ls.add("HD video streaming");
		ls.add("Optimized video streaming");
		ls.add("Overseas voice calling");
		ls.add("Gogo in-flight benefits");
		ls.add("T-Mobile Family Mode");
		ls.add("Voicemail to Text");
		ls.add("Name ID");
		ls.add("AutoPay discount");
		ls.add("Netflix™ On Us");
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("No annual service contract");
		ls.add("Wi-Fi calling");
		ls.add("Unused data rollover");

		return ls;
	}

	public List<String> getRatePlanFeatureTMSERVE2() {

		List<String> ls = new ArrayList<>();
		ls.add("Voicemail to Text");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Music streaming included with Music Freedom");
		ls.add("Talk & text");
		ls.add("Unused data rollover");
		ls.add("Name ID");
		ls.add("Optimized video streaming");
		ls.add("Mobile hotspot");
		ls.add("Taxes and fees included");
		ls.add("AutoPay discount");
		ls.add("HD video streaming");
		ls.add("No annual service contract");
		ls.add("Gogo in-flight benefits");
		ls.add("Other discounts");
		ls.add("Netflix™ On Us");
		ls.add("Wi-Fi calling");
		ls.add("Family Allowances");
		ls.add("Unlimited international texting from home");
		ls.add("Video streaming included with Binge On");
		ls.add("High-speed data");
		ls.add("T-Mobile Family Mode");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Overseas voice calling");
		return ls;
	}

	/*
	 * public JsonNode getParentNodeFromResponse(Response response) { ObjectMapper
	 * mapper = new ObjectMapper(); JsonNode jsonNode = null; try { jsonNode =
	 * mapper.readTree(response.asString()); } catch (JsonProcessingException e) {
	 * Assert.fail("<b>JsonProcessingException Response :</b> " + e);
	 * Reporter.log(" <b>JsonProcessingException Response :</b> "); Reporter.log(" "
	 * + e); e.printStackTrace(); } catch (IOException e) {
	 * Assert.fail("<b>IOException Response :</b> " + e);
	 * Reporter.log(" <b>IOException Response :</b> "); Reporter.log(" " + e);
	 * e.printStackTrace(); } return jsonNode; }
	 */

	public AccountsApi checkjsontagitems(Response response, String tag) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();
			try {
				Assert.assertTrue(jsonPathEvaluator.get(tag) != null, tag + " value is " + tag + " is existing");
				Reporter.log(tag + " is existing");
			} catch (Exception e) {
				Assert.fail(tag + " is not existing");
			}
		}

		return this;
	}

	public AccountsApi checkjsontagitemsNotExist(Response response, String tag) {
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();
			try {
				Assert.assertTrue(jsonPathEvaluator.getBoolean(tag) == false,
						tag + " value is " + tag + " is NOT existing");
				Reporter.log(tag + " is not existing");
			} catch (Exception e) {
				Assert.fail(tag + " is  existing");
			}
		}

		return this;
	}

}
