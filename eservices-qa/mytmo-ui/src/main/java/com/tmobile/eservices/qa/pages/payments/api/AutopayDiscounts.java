package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class AutopayDiscounts extends ApiCommonLib {

	/***
	 * Summary: This API is used do the payments  through OrchestrateOrders API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-interactionId
	 * 	-workflowId
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */

	private Map<String, String> buildAutoPayDiscountsApiHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("interactionId","123");
		headers.put("Accept", "application/json");
		headers.put("ban", apiTestData.getBan());




		return headers;
	}



	public Response AutoPayDiscountEligibility(ApiTestData apiTestData,Map<String, String> tokenMap,String accountNumber) throws Exception {
		Map<String, String> headers = buildAutoPayDiscountsApiHeader(apiTestData);
		headers.put("Authorization", getAccessToken());
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/autopaydiscount/eligibility/"+accountNumber, RestCallType.GET);
		System.out.println(response.asString());
		return response;

	}

	public Response AutoPayDiscountStatus(ApiTestData apiTestData,Map<String, String> tokenMap,String accountNumber) throws Exception {
		Map<String, String> headers = buildAutoPayDiscountsApiHeader(apiTestData);
		headers.put("Authorization", getAccessToken());
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/autopaydiscount/status/"+accountNumber, RestCallType.GET);
		System.out.println(response.asString());
		return response;

	}
	
	public Response listOfAutopayAndListOfEmployeeDiscountOnCurrentRateplan(ApiTestData apiTestData,Map<String, String> tokenMap,String accountNumber) throws Exception {
		Map<String, String> headers = buildAutoPayDiscountsApiHeader(apiTestData);
		headers.put("Authorization", getAccessToken());
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/autopaydiscount/current/"+accountNumber, RestCallType.GET);
		System.out.println(response.asString());
		return response;

	}

	public Response AutopayDisocuntDetailsBuilder(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildAutoPayDiscountsApiHeader(apiTestData);
		headers.put("Authorization", getAccessToken());
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/autopaydiscount/details", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}


}
