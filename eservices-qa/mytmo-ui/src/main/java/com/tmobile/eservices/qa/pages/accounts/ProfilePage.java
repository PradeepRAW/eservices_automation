package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class ProfilePage extends CommonPage {

	private static final String pageUrl = "profile";
	private static final String blockingPageUrl = "blocking";

	@FindBy(css = "p[class='Display6']")
	private List<WebElement> profilePageLinks;

	@FindBy(css = "span[ng-model='selectedVal']")
	private WebElement selectedDropDownValue;

	@FindBy(xpath = "//*[contains(text(),'Blocking')]")
	private WebElement blockingBlade;

	@FindBy(xpath = "//*[@class='switch mt-1 stateEnabled' and @id='Block International Roaming']//*[@class='slider round']")
	private WebElement blockInternationalRoamingEnabled;

	@FindBy(xpath = "//*[@class='switch mt-1' and @id='Block International Roaming']//*[@class='slider round']")
	private WebElement blockInternationalRoamingDisabled;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement lineHeader;

	@FindBys(@FindBy(css = "div#test ul li"))
	private List<WebElement> lineList;

	@FindBy(xpath = "//*[contains(text(),'Billing & Payments')]")
	private WebElement BillingandPaymnets;

	@FindBy(xpath = "//*[contains(text(),'Employment verification')]")
	private WebElement EmployeVerification;

	@FindBy(xpath = "//p[text()='Payment methods']")
	private WebElement PaymnetMethod;

	@FindBy(xpath = "//p[text()='Save up to 10 payment methods per line to secure for secure,easy paymnet methods ]")
	private WebElement paymentbody;

	@FindBy(xpath = "//p[text()='Permission']")
	private WebElement permissionsBlade;

	@FindBy(xpath = "//p[contains(text(),'Profile') and contains(@class,'text-center')]")
	private WebElement profileHeader;

	public ProfilePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Profile page.
	 *
	 * @return the ProfilePage class instance.
	 */
	public ProfilePage verifyProfilePage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			waitFor(ExpectedConditions.visibilityOf(BillingandPaymnets));
			Reporter.log("Profile page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Profile page not displayed");
		}
		return this;
	}

	/**
	 * Click Profile Page Links
	 * 
	 * @param linkName
	 * @return
	 */
	public ProfilePage clickProfilePageLink(String linkName) {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(profilePageLinks.get(0)));
			clickElementBytext(profilePageLinks, linkName);
		} catch (Exception e) {
			Assert.fail(linkName + " is not displayed");
		}
		return this;
	}

	/***
	 * Fetches the selected line in the Profile page
	 * 
	 * @return
	 */
	public String returnSelectedLine() {
		String selectedVal = "";
		try {
			checkPageIsReady();
			selectedVal = selectedDropDownValue.getText().trim().toString();
			Reporter.log("Fetched the selected line value in the Profile");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to fetch the selected msisdn");
		}
		return selectedVal;
	}

	/*
	 * Click on Blocking blade
	 */
	public ProfilePage clickOnBlockingBlade() {
		blockingBlade.click();
		return this;
	}

	/**
	 * Verify Blocking page.
	 *
	 * @return the blocking Page under Profile page.
	 */
	public ProfilePage verifyBlockingPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(blockingPageUrl);
			Reporter.log("Blocking page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Blocking page not displayed");
		}
		return this;
	}

	/*
	 * Verify whether International Blocking is Enabled or not
	 * 
	 */
	public boolean verifyInternationalBlockingStatus() {
		boolean statusOfInternationalBlocking = false;
		try {
			if (blockInternationalRoamingEnabled.isDisplayed())
				statusOfInternationalBlocking = true;
		} catch (Exception e) {
			Reporter.log("Block International Roaming is disabled");
		}

		return statusOfInternationalBlocking;

	}

	/*
	 * Click button of Block International Roaming
	 * 
	 */

	public ProfilePage clickOnBlockInternationalRoamingButton() {
		if (!verifyInternationalBlockingStatus()) {
			try {
				blockInternationalRoamingDisabled.click();
				checkPageIsReady();
			} catch (Exception e) {
				Reporter.log("Block International button not clicked.");
			}
		} else
			Reporter.log("Block International Roaming button is already clicked");
		return this;

	}

	/*
	 * method to click on Block Internation Roaming buton
	 * 
	 */
	public ProfilePage verifyInternationalBlockingAndClickingOnIt() {
		clickOnBlockingBlade();
		verifyBlockingPage();
		verifyInternationalBlockingStatus();
		clickOnBlockInternationalRoamingButton();

		return this;
	}

	/**
	 * Change current line to suspended line.
	 */
	public ProfilePage changeToSuspendedLine() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(lineHeader));
			lineHeader.click();
			moveToElement(lineList.get(3));
			lineList.get(3).click();
			Reporter.log("Change to suspend line is success");
		} catch (Exception e) {
			Assert.fail("Change to suspend line is failed");
		}
		return this;
	}

	public ProfilePage clickBillingPayments() {
		try {
			checkPageIsReady();
			BillingandPaymnets.click();
			Reporter.log("Clicked on Billing and paymnets");
		} catch (Exception e) {
			Assert.fail("Billing and paymnets  failed");
		}
		return this;
	}

	public ProfilePage clickEmployeeVerification() {
		try {
			checkPageIsReady();
			EmployeVerification.click();
			Reporter.log("Clicked on Employee Verification");
		} catch (Exception e) {
			Assert.fail("Employee Verification click is failed");
		}
		return this;
	}

	/**
	 * Verify Profile page.
	 *
	 * @return the ProfilePage class instance.
	 */
	public ProfilePage verifyAutopayredirectedtoProfilePageforGovernamentNonMasterUsers(MyTmoData myTmoData) {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Autopay link redirected to Profile page for" + myTmoData.getPayment().getUserType());
		} catch (NoSuchElementException e) {
			Assert.fail("Autopay link redirected to Profile page for" + myTmoData.getPayment().getUserType());
		}
		return this;
	}

}
