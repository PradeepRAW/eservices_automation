package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 *
 */
public class PrivacyAndNotificationsPage extends CommonPage {

	private static final String pageUrl = "privacy_notifications";

	// Headers

	@FindBy(css = "p.Display3")
	private WebElement pageHeader;

	@FindBy(css = "div.TMO-PROFILE-BLADE-PAGE .Display6")
	private List<WebElement> listOfPageHeaders;

	@FindBy(xpath = "//p[contains(text(),'Interest-Based Ads')]")
	private WebElement internetBasedAdsHeader;

	@FindBy(xpath = "//p[contains(text(),'Web Browsing & App Information')]")
	private WebElement webBrowsingHeader;

	@FindBy(xpath = "//p[contains(text(),'Device Location')]")
	private WebElement deviceLocationHeader;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement lineHeader;

	// Buttons and Links

	@FindBy(xpath = "//p[text()='Marketing Communications']")
	private WebElement marketingCommunicationLink;

	@FindBy(xpath = "//p[text()='Notifications']")
	private WebElement notificationsLink;

	@FindBy(xpath = "//p[text()='Advertising & Insights']")
	private WebElement advertisingLink;

	@FindBy(xpath = "//p[text()='Text or Call My Phone']/../../div/span")
	private WebElement textMyPhoneToggle;

	@FindBy(xpath = "//p[text()='Send Me Email']/../../div/span")
	private WebElement sendMeEmailToggle;

	@FindBy(xpath = "//p[text()='Mail to My Billing Address']/../../div/span")
	private WebElement mailToMyAddresstoggle;

	@FindBy(xpath = "//p[text()='Receive Monthly T-Mobile Newsletter']/../../div/span")
	private WebElement recieveMonthlyNewsletterToggle;

	@FindBy(xpath = "//p[contains(text(),'T-Mobile will always send notifications')]/../../div/span")
	private WebElement tMobileNotificationstoggle;

	@FindBy(xpath = "//p[contains(text(),'Receive alerts when other')]/../../div/span")
	private WebElement receiveAlertsToggle;

	@FindBy(xpath = "//p[contains(text(),'Web Browsing & App Information')]/../../div/span/label")
	private WebElement webBrowsingToggle;

	@FindBy(xpath = "//p[contains(text(),'Device Location')]/../../div/span/label")
	private WebElement deviceLocationToggle;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement insightsToggleStatus;

	@FindBy(xpath = "//p[contains(text(),'Interest-Based Ads')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement internetBasedAdsToggleStatus;

	@FindBy(xpath = "//p[contains(text(),'Web Browsing & App Information')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement webBrowsingToggleStatus;

	@FindBy(xpath = "//p[contains(text(),'Device Location')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement deviceLocationToggleStatus;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/p")
	private WebElement insightsMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Interest-Based Ads')]/../../div/span/p")
	private WebElement interestBasedAdsMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Web Browsing & App Information')]/../../div/span/p")
	private WebElement webBrowsingMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Device Location')]/../../div/span/p")
	private WebElement deviceLocationMorebtn;

	@FindBy(css = ".body.d-inline-block")
	private WebElement showMoreTxt;

	@FindBys(@FindBy(css = "div#test ul li"))
	private List<WebElement> lineList;

	@FindBy(css = "p.red")
	private WebElement warningMessage;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/label")
	private WebElement insightsToggle;

	@FindBy(id = "Insights")
	private WebElement insightsId;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement insightsToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/label[@class='switch mt-1']")
	private WebElement insightsToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement insightsToggleDisableStatus;

	@FindBy(xpath = "//p[contains(text(),'Interest-Based Ads')]/../../div/span/label")
	private WebElement internetBasedAdsToggle;

	@FindBy(xpath = "//p[contains(text(),'Interest-Based Ads')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement internetBasedAdsToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Interest-Based Ads')]/../../div/span/label[@class='switch mt-1']")
	private WebElement internetBasedAdsToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Interest-Based Ads')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement internetBasedAdsToggleDisableStatus;

	@FindBy(linkText = "Privacy & Notifications")
	private WebElement privacyAndNotificationsLink;

	public PrivacyAndNotificationsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Billing And Payments Page.
	 *
	 * @return
	 */
	public PrivacyAndNotificationsPage verifyPrivacyAndNotificationsPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Privacy and notifications page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Privacy and notifications page not displayed");
		}
		return this;

	}

	/**
	 * Click Marketing Communication link.
	 */
	public PrivacyAndNotificationsPage clickMarketingCommunicationsLink() {
		waitforSpinnerinProfilePage();
		try {
			marketingCommunicationLink.click();
			Reporter.log("Marketing communication page displayed");
		} catch (Exception e) {
			Assert.fail("Click on marketingCommunication link failed");
		}
		return this;
	}

	/**
	 * Click Notifications link.
	 */
	public PrivacyAndNotificationsPage clickNotificationsLink() {
		waitforSpinnerinProfilePage();
		try {
			notificationsLink.click();
			Reporter.log("Notifications page displayed");
		} catch (Exception e) {
			Assert.fail("Click on notifications link failed");
		}
		return this;
	}

	/**
	 * Click Advertising link.
	 */
	public PrivacyAndNotificationsPage clickAdvertisingLink() {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOf(advertisingLink));
		try {
			advertisingLink.click();
			Reporter.log("Click on advertising link is success");
		} catch (Exception e) {
			Reporter.log("Click on advertising link failed");
			Assert.fail("Click on advertising link failed");
		}
		return this;
	}

	/**
	 * Verify page Header.
	 */
	public PrivacyAndNotificationsPage verifyPageHeader() {
		waitforSpinnerinProfilePage();
		try {
			pageHeader.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Page header not displayed");
		}
		return this;
	}

	/**
	 * Verify Header By Name.
	 */
	public PrivacyAndNotificationsPage verifyHeaderByName(String headerName) {
		waitforSpinner();
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(listOfPageHeaders.get(0)));
		try {
			for (WebElement webElement : listOfPageHeaders) {
				if (webElement.getText().contains(headerName)) {
					Reporter.log(headerName + " header displayed");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail(headerName + " header is not displayed");
		}
		return this;
	}

	/**
	 * Click Advertising link.
	 */
	public PrivacyAndNotificationsPage clickTextMyPhoneToggle() {
		waitforSpinnerinProfilePage();
		try {
			textMyPhoneToggle.click();
			Reporter.log("Clicked on text my phone toggle");
		} catch (Exception e) {
			Assert.fail("Click on text my phone toggle failed");
		}
		return this;
	}

	/**
	 * Click SendMeEmail toggle.
	 */
	public PrivacyAndNotificationsPage clickSendMeEmailToggle() {
		waitforSpinnerinProfilePage();
		try {
			sendMeEmailToggle.click();
			Reporter.log("Clicked on send me mail toogle");
		} catch (Exception e) {
			Assert.fail("Click on send me mail toggle failed");
		}
		return this;
	}

	/**
	 * Click MailTo My Address toggle.
	 */
	public PrivacyAndNotificationsPage clickMailToMyAddresstoggle() {
		waitforSpinnerinProfilePage();
		try {
			mailToMyAddresstoggle.click();
			Reporter.log("Clicked on mail to billing address toogle");
		} catch (Exception e) {
			Assert.fail("Click on mail to billing addresss toggle failed");
		}
		return this;
	}

	/**
	 * Click Recieve Monthly News letter toggle.
	 */
	public PrivacyAndNotificationsPage clickRecieveMonthlyNewsletterToggle() {
		waitforSpinnerinProfilePage();
		try {
			recieveMonthlyNewsletterToggle.click();
			Reporter.log("Clicked on receive news letter toogle");
		} catch (Exception e) {
			Assert.fail("Click on receive news letter toogle failed");
		}
		return this;
	}

	/**
	 * Click mailToMyAddress toggle.
	 */
	public PrivacyAndNotificationsPage clickTMobileNotificationsToggle() {
		waitforSpinnerinProfilePage();
		try {
			tMobileNotificationstoggle.click();
			Reporter.log("Clicked on tmobile notifications toggle");
		} catch (Exception e) {
			Assert.fail("Click on tmobile notifications toggle failed");
		}
		return this;
	}

	/**
	 * Click recieveMonthlyNewsletter toggle.
	 */
	public PrivacyAndNotificationsPage clickReceiveAlertsToggle() {
		waitforSpinnerinProfilePage();
		try {
			receiveAlertsToggle.click();
			Reporter.log("Clicked on recieve alerts toggle");
		} catch (Exception e) {
			Assert.fail("Click on recieve alerts toggle failed");
		}
		return this;
	}

	/**
	 * Verify receive alerts header.
	 */
	public PrivacyAndNotificationsPage verifyDeviceLocationHeader() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(deviceLocationHeader));
			deviceLocationHeader.isDisplayed();
			Reporter.log("DeviceLocation header displayed");
		} catch (Exception e) {
			Assert.fail("DeviceLocation header not displayed");
		}
		return this;
	}

	/**
	 * Verify internet based ads header.
	 */
	public PrivacyAndNotificationsPage verifyInterestBasedAdsHeader() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(internetBasedAdsHeader));
			internetBasedAdsHeader.isDisplayed();
			Reporter.log("Internet based ads header displayed");
		} catch (Exception e) {
			Assert.fail("Internet based ads header not displayed");
		}
		return this;
	}

	/**
	 * Verify web browsing header.
	 */
	public PrivacyAndNotificationsPage verifyWebBrowsingHeader() {
		try {
			if ((getDriver() instanceof AppiumDriver)) {
				WebElement spinner = getDriver().findElement(By.cssSelector(".overlay:not(.overlay-out)"));
				waitFor(ExpectedConditions.invisibilityOf(spinner));
			}
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(webBrowsingHeader));
			webBrowsingHeader.isDisplayed();
			Reporter.log("Web browsing header displayed");
		} catch (Exception e) {
			Assert.fail("Web browsing header not displayed");
		}
		return this;
	}

	/**
	 * Click insights toggle.
	 */
	public PrivacyAndNotificationsPage clickInsightsToggle() {
		try {
			waitforSpinnerinProfilePage();
			insightsToggle.click();
			Reporter.log("Click on insights toggle is success");
		} catch (Exception e) {
			Assert.fail("Click on insights toggle failed");
		}
		return this;
	}

	/**
	 * Hold insights toggle value
	 */
	public boolean holdInsightsTogglevalue() {
		boolean insightsToggleValue = false;
		try {
			if (getAttribute(insightsId, "class").contains("stateEnabled")) {
				insightsToggleValue = true;
			}
			Reporter.log("insights Toggle value holded");
		} catch (Exception e) {
			Assert.fail("Failed to hold insights Toggle value");
		}
		return insightsToggleValue;
	}

	/**
	 * Click interestBasedAds toggle.
	 */
	public PrivacyAndNotificationsPage clickInterestBasedAdsToggle() {
		try {
			waitforSpinnerinProfilePage();
			internetBasedAdsToggle.click();
			Reporter.log("Click on interestBasedAds toggle is success");
			waitforSpinnerinProfilePage();
		} catch (Exception e) {
			Assert.fail("Click on interestBasedAds toggle failed");
		}
		return this;
	}

	/**
	 * Change current line to unregistered line.
	 */
	public PrivacyAndNotificationsPage changeToUnregisteredLine() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(lineHeader));
			lineHeader.click();
			moveToElement(lineList.get(1));
			lineList.get(1).click();
			Reporter.log("Change to unregisterd line is success");
		} catch (Exception e) {
			Assert.fail("Change to unregisterd line is failed");
		}
		return this;
	}

	/**
	 * Verify warning message.
	 */
	public PrivacyAndNotificationsPage verifyWarningMessage() {
		try {
			waitforSpinnerinProfilePage();
			warningMessage.isDisplayed();
			Reporter.log("Warning message displayed");
		} catch (Exception e) {
			Assert.fail("Warning message not displayed");
		}
		return this;
	}

	/**
	 * Check insights toggle status.
	 */
	public PrivacyAndNotificationsPage checkToggleStatusForInsights() {
		try {
			waitforSpinnerinProfilePage();
			if (isElementDisplayed(insightsToggleStatus)) {
				Reporter.log("Insights toggle is in enabled state");
			} else {
				Reporter.log("Insights toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Insights toggle is not displayed");
		}
		return this;
	}

	/**
	 * Check internet based ads toggle.
	 */
	public PrivacyAndNotificationsPage checkToggleStatusForInternetBasedAds() {
		try {
			waitforSpinnerinProfilePage();
			if (isElementDisplayed(internetBasedAdsToggleStatus)) {
				Reporter.log("Internet based ads toggle is in enabled state");
			} else {
				Reporter.log("Internet based ads toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Internet based ads toggle is not displayed");
		}
		return this;
	}

	/**
	 * Check web browsing toggle status.
	 */
	public PrivacyAndNotificationsPage checkToggleStatusForWebBrowsing() {
		try {
			waitforSpinnerinProfilePage();
			if (isElementDisplayed(webBrowsingToggleStatus)) {
				Reporter.log("Web browsing toggle is in enabled state");
			} else {
				Reporter.log("Web browsing toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Web browsing toggle is not displayed");
		}
		return this;
	}

	/**
	 * Check device location toggle status.
	 */
	public PrivacyAndNotificationsPage checkToggleStatusForDeviceLocation() {
		try {
			waitforSpinnerinProfilePage();
			if (isElementDisplayed(deviceLocationToggleStatus)) {
				Reporter.log("Device location toggle is in enabled state");
			} else {
				Reporter.log("Device location toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Device locationToggle toggle is not displayed");
		}
		return this;
	}

	/**
	 * Verify insights toggle operations.
	 */
	public PrivacyAndNotificationsPage verifyinsightsToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(insightsToggleOnStatus, insightsToggleOffStatus,
					insightsToggleDisableStatus);
			verifyToggleOpeartions(insightsToggleOnStatus, insightsToggleOffStatus, insightsToggleDisableStatus,
					toggleStatus, insightsToggle);
		} catch (Exception e) {
			Reporter.log("Insights toggle status update failed");
			Assert.fail("Insights toggle status update failed ");
		}
		return this;
	}

	/**
	 * Verify insights toggle default state.
	 */
	public PrivacyAndNotificationsPage verifyinsightsToggleDefaultState() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(insightsToggleOnStatus, insightsToggleOffStatus,
					insightsToggleDisableStatus);
			Assert.assertEquals(toggleStatus, "ON", "Insights toggle default status is off");
			Reporter.log("Insights toggle default status is on");
		} catch (Exception e) {
			Reporter.log("Insights toggle default status is off");
			Assert.fail("Insights toggle default status is off ");
		}
		return this;
	}

	/**
	 * Verify internetBasedAds toggle operations.
	 */
	public PrivacyAndNotificationsPage verifyInternetBasedAdsToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(internetBasedAdsToggleOnStatus, internetBasedAdsToggleOffStatus,
					internetBasedAdsToggleDisableStatus);
			verifyToggleOpeartions(internetBasedAdsToggleOnStatus, internetBasedAdsToggleOffStatus,
					internetBasedAdsToggleDisableStatus, toggleStatus, internetBasedAdsToggle);
		} catch (Exception e) {
			Reporter.log("InternetBasedAds toggle status update failed");
			Assert.fail("InternetBasedAds toggle status update failed ");
		}
		return this;
	}

	/**
	 * Verify internetBasedAds default state.
	 */
	public PrivacyAndNotificationsPage verifyInternetBasedAdsToggleDefaultState() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(internetBasedAdsToggleOnStatus, internetBasedAdsToggleOffStatus,
					internetBasedAdsToggleDisableStatus);
			Assert.assertEquals(toggleStatus, "ON", "InternetBasedAds toggle default status is off");
			Reporter.log("InternetBasedAds toggle default status is on");
		} catch (Exception e) {
			Reporter.log("InternetBasedAds toggle default status is off");
			Assert.fail("InternetBasedAds toggle default status is off ");
		}
		return this;
	}
}
