package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class QuoteApi extends ApiCommonLib{
	
	/***
	 * This is the API specification for the EOS Quote . The EOS API will inturn
	 * invoke DCP apis to execute the quote functions.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
    public Response createQuote(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildQuoteAPIHeader(apiTestData, tokenMap);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v4/quote/",RestCallType.POST);
         System.out.println(response.asString());
         return response;
    }       
    
    public Response updateQuote(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildQuoteAPIHeader(apiTestData, tokenMap);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v4/quote/"+tokenMap.get("quoteId"), RestCallType.PUT);
         System.out.println(response.asString());
         return response;
    } 
    
    public Response updatePayment(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildQuoteAPIHeader(apiTestData, tokenMap);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v4/quote/"+tokenMap.get("quoteId")+"/payment",RestCallType.POST);
         System.out.println(response.asString());
         return response;
    }
    
    private Map<String, String> buildQuoteAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	//final String accessToken = getAccessToken();
    	//clearCounters(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("transactionId", "3ec0a12d-dee1-46b2-87b7-a4e2c8d02029");
		headers.put("correlationId", "tester");
		headers.put("applicationId", "MYTMO");
		headers.put("channelId", "web");
		headers.put("clientId", "e-serviceUI");
		headers.put("transactionBusinessKey", "BAN");
		headers.put("transactionBusinessKeyType", apiTestData.getBan());
		headers.put("transactionType", "UPGRADE");
		headers.put("usn", "testUSN");
		headers.put("phoneNumber",  apiTestData.getMsisdn() );
		headers.put("Cache-Control", "no-cache");
		headers.put("content-type", "application/json");
		headers.put("interactionid", "xasdf1234asa4444");
		return headers;
	}

}
