/**
 *
 */
package com.tmobile.eservices.qa.pages.shop;

import static java.lang.Integer.parseInt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class PLPPage extends CommonPage {

	public static final String PLPPAGEURL = "productlist";

	@FindBy(css = "div.tab-pane.ng-scope.active div#sortByDropdown span[ng-show*='sortSelected']")
	private WebElement sortingDropDownSelectedOption;

	@FindBy(css = "#sortBypos p")
	private WebElement sortingDropDownSelectedOptionForMobilView;

	// @FindBy(css = "div#sortByDropdown>div>span[ng-show*='sortSelected']")
	@FindBy(xpath = "(//div[contains(@class,'dropSelect')]//div[contains(@id,'sortByDropdown')])[2]")
	private WebElement sortingDropdown;

	@FindBy(css = "#sortBypos")
	private WebElement sortingDropdownForMobile;

	@FindBy(css = "div#sortByDropdown div[ng-click*='dropDown'] span")
	private WebElement filterDropdown;

	@FindBy(xpath = "//p[contains(text(), 'Filter by')]")
	private WebElement iOSFilterDropdown;

	@FindBy(xpath = "//p[contains(text(),'Manufacturer')][contains(@class,'text-bold')]")
	private WebElement manufacturerLabel;

	@FindBy(xpath = "//p[contains(text(),'Condition')][contains(@class,'text-bold')]")
	private WebElement conditionLabel;

	@FindBy(xpath = "//p[contains(text(),'Operating System')][contains(@class,'text-bold')]")
	private WebElement osLabel;

	@FindBy(xpath = "//p[contains(text(),'Deals')][contains(@class,'text-bold')]")
	private WebElement dealsLabel;

	@FindBy(id = "SamsungdesktopphoneManufacturer")
	private WebElement filterDropdownValue;

	@FindBy(css = "label[for='AppledesktopphoneManufacturer'] span")
	private WebElement filterDropdownValueApple;

	@FindBy(xpath = "//div[@aria-expanded='true']//p[contains(text(), 'Manufacturer')]")
	private WebElement iOSfilterDropdownValueManufacturer;

	@FindBy(css = "label[for='ApplemobilephoneManufacturer'] span")
	private WebElement mobileFilterDropdownValueApple;

	@FindBy(css = "a.icon.close-dropdown i")
	private WebElement filterDropdownCloseButton;

	@FindBys(@FindBy(css = "div.tab-pane.ng-scope.active div#sortByDropdown ul li a"))
	private List<WebElement> sortingList;
	// $ctrl.contentData.addALineBtnLabel

	@FindBys(@FindBy(css = "div[ng-if*='item.strikeSalePrice']"))
	private List<WebElement> pricesLowToHigh;

	@FindBys(@FindBy(css = "div.phones-list p.model-name-hover-plp a"))
	private List<WebElement> devicesList;

	@FindBys(@FindBy(css = "div.phones-list div.row.no-margin"))
	private List<WebElement> rows;

	@FindBys(@FindBy(css = "div.active div.phones-list div[ng-repeat-start*='item']"))
	private List<WebElement> devicesDisplayedPerRow;

	@FindBy(css = ".desktopPagination p[ng-bind-html*='additionalOfferDescription']")
	private WebElement promotionDescription;

	@FindBy(css = "#userPosition img")
	private List<WebElement> deviceImagesInPLPPage;

	@FindBy(css = "p[ng-bind-html*='legalTextDesktop']")
	private WebElement authorableLegalText;

	@FindBy(css = "p[ng-bind-html*='ctrl.legalTextParagraph | displayHTML']")
	private WebElement authorableLegalTextForMobileView;

	@FindBy(css = "p.legal.hidden-xs")
	private WebElement legalTextUnderDeviceImage;

	@FindBy(css = "p.legal.padding-bottom-xl")
	private WebElement legalTextUnderDeviceImageForMobileView;

	@FindBy(css = "a[ng-click*='ctrl.openlegalTextModal']")
	private WebElement clickSeeLegalDisclaimerForMobileView;

	@FindBy(css = "i.ico-closeIcon.uib-close.modal-close-legalText")
	private WebElement closeSeeLegalDisclaimerModelForMobileView;

	@FindBy(css = "div#idNoItemsFound h3")
	private WebElement noItemsFound;

	@FindBy(css = "h6.text-center.extra-large")
	private WebElement noItemsFoundSuggestionText;

	@FindBy(css = "div#pricingModal h2.h2-title")
	private WebElement downPaymentModelHeader;

	@FindBy(xpath = "(//div[contains(@id,'pricingModal')]//p)[1]")
	private WebElement downPaymentModelText;

	@FindBy(xpath = "(//div[contains(@id,'pricingModal')]//p)[2]")
	private WebElement downPaymentModelNote;

	@FindBy(css = "label[for='SamsungdesktopphoneManufacturer'] span")
	private WebElement filterDropdownValueSamsung;

	@FindBy(css = "label[for='SamsungmobilephoneManufacturer'] span")
	private WebElement iOSfilterDropdownValueSamsung;

	@FindBy(css = "label[for='SPECIAL OFFERdesktopphoneDEALS'] span")
	private WebElement filterDropdownDealsValueSpecialOffer;

	@FindBy(css = "label[for='ON SALEdesktopphoneDEALS'] span")
	private WebElement onSaleFilterValue;

	@FindBy(css = "a[ng-click='$ctrl.getDataOnPageChange($index+1,12)']")
	private List<WebElement> plpPaginationPagesList;

	//@FindBy(css = ".mobilePagination [ng-click*='getDataOnPageChange']")
	@FindBy(css = ".mobilePagination [ng-if*='totalPages '] a")
	private List<WebElement> plpPaginationPagesListMobileView;

	@FindBy(css = "i[class*='backArrowLarge']")
	private WebElement backBtn;

	@FindBy(linkText = "Samsung Galaxy S7")
	private WebElement selectDeviceFromPLP;

	@FindBy(xpath = "//li[@ng-click='$ctrl.nextPage(12)']/a")
	private WebElement paginationNextArrow;

	@FindBy(css=".mobilePagination li[ng-click*='nextPage'] a")
	private WebElement paginationNextArrowMobileView;

	@FindBy(css = "div[ng-show='dropDown'] p.filter-text.ng-binding")
	private List<WebElement> plpPageDevicesAndOperatingSystemsList;

	@FindBy(css = "label[for='T-Mobile®desktopphoneManufacturer'] span")
	private WebElement selectTMobileManufacturer;

	@FindBy(css = "#plp-star-rating.icon")
	private List<WebElement> deviceReviewStars;

	@FindBy(css = "#plp-star-rating span")
	private List<WebElement> deviceReviewNumbers;

	@FindBy(xpath = "//span[contains(text(), 'Today')]")
	private List<WebElement> deviceTodayComponent;

	@FindBy(xpath = "//span[contains(text(), 'Monthly')]")
	private List<WebElement> deviceMonthlyComponents;

	@FindBy(xpath = "//div[@class='divFRPCls']")
	private List<WebElement> deviceFullRetailPriceComponent;

	@FindBy(css = "sup.dollar-sign")
	private List<WebElement> dollarSign;

	@FindBy(css = ".cost-header.reg.arial.grey-dark.bold")
	private List<WebElement> eipInstallmentMonths;

	@FindBy(css = "span.monthly-total.dollars")
	private List<WebElement> eipMonthlyPrice;

	@FindBy(css = "div.divFRPCls p")
	private List<WebElement> fRPPrice;

	@FindBy(css = ".daily-total.dollars")
	private List<WebElement> todayDownPayment;

	@FindBy(css = ".save-promotion-textsize.ng-binding")
	private List<WebElement> discountOffers;

	@FindBy(css = "ul.dropdown-menu")
	private WebElement sortDropdownOptionsList;

	@FindBy(css = "div.footer-pict-nav")
	private WebElement pLPPageFooter;

	@FindBy(css = "#memoryDropdown")
	private WebElement featuredFilter;

	@FindBy(css = "div[ng-if*='isEIPEligible']>ul")
	private List<WebElement> eipPriceModal;

	@FindBy(css = "#position #userPosition div.promo-stamp")
	private List<WebElement> specialOfferDevices;

	@FindBy(css = "div#resultDesktopBlock p.m-r-results>span:nth-child(1)")
	private WebElement devicesOnPLP;

	@FindBy(css = ".categories a>button.hide-on-mobile>span")
	private List<WebElement> category;

	@FindBy(css = ".categories a.active>button.hide-on-mobile>span")
	private WebElement selectedCategory;

	@FindBy(css = "#acsMainInvite .acsCloseButton--container>span>a")
	private List<WebElement> mobileFeedBackAlert;

	public PLPPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify PLP Page
	 *
	 * @return
	 */
	public PLPPage verifyPageLoaded() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
			Reporter.log(Constants.PLP_PAGE_HEADER);
		} catch (Exception e) {
			Assert.fail("Product List Page is Not Loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PLPPage verifyPageUrl() {
		checkPageIsReady();
		waitforSpinner();
		try {
			if(getDriver() instanceof AppiumDriver){
				if (mobileFeedBackAlert.size() == 1) {
					mobileFeedBackAlert.get(0).click();
					getDriver().getCurrentUrl().contains(PLPPAGEURL);
				}
			}else{
				getDriver().getCurrentUrl().contains(PLPPAGEURL);
			}
		} catch (Exception e) {
			Assert.fail("PLP page is not displayed");
		}
		return this;
	}

	/**
	 * Get Selected Option From Sort By
	 *
	 * @return
	 */
	public PLPPage getSelectedOptionFromSoryBy() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertEquals(sortingDropDownSelectedOptionForMobilView.getText(), "Featured");
				Reporter.log("Verify defeault option +Featured Is Displayed In PLP");
			} else {
				Assert.assertEquals(sortingDropDownSelectedOption.getText(), "Featured");
				Reporter.log("Verify defeault option +Featured Is Displayed In PLP");
			}
		} catch (Exception e) {
			Assert.fail("Verify defeault option +Featured Is not Displayed In PLP");
		}
		return this;
	}

	/**
	 * Select Sory By Value
	 *
	 * @param sortByValue
	 */
	public void selectSortBy(String sortByValue) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				sortingDropdownForMobile.click();
				waitforSpinner();
				for (WebElement webElement : sortingList) {
					if (sortByValue.equalsIgnoreCase(webElement.getText())) {
						webElement.click();
						break;
					}
				}
			} else {
				sortingDropdown.click();
				waitforSpinner();
				for (WebElement webElement : sortingList) {
					if (sortByValue.equalsIgnoreCase(webElement.getText())) {
						webElement.click();
						break;
					}
				}
			}

		} catch (Exception e) {
			Assert.fail("Prices Low to high is not sorting");
		}
	}

	/**
	 * Verify Price Low To High
	 *
	 * @return
	 */
	public PLPPage verifyPriceLowToHigh() {
		try {
			waitforSpinner();
			boolean lowToHigh = false;
			String replaceValue = "[^0-9]";
			int i = 1;
			for (WebElement webElement : pricesLowToHigh) {
				if (pricesLowToHigh.size() - 1 > i) {
					if (Integer.parseInt(webElement.getText().replaceAll(replaceValue, "").trim()) <= Integer
							.parseInt(pricesLowToHigh.get(i).getText().replaceAll(replaceValue, "").trim())
							|| Integer.parseInt(webElement.getText().replaceAll(replaceValue, "").trim()) == Integer
									.parseInt(pricesLowToHigh.get(i).getText().replaceAll(replaceValue, "").trim())) {
						lowToHigh = true;
						break;
					} else {
						lowToHigh = false;
						break;
					}
				}
			}
			Assert.assertTrue(lowToHigh, "Prices Low to high is not sorting");
			Reporter.log("Prices Low to high is sorting");
		} catch (Exception e) {
			Assert.fail("Prices Low to high is not sorting");
		}
		return this;
	}

	/**
	 * Verify Rows
	 *
	 * @return
	 */
	public PLPPage verifyRows() {
		try {
			waitforSpinner();
			boolean rowsDisplayed = false;
			if (getDriver() instanceof AppiumDriver) {
				if (rows.size() == 2) {
					rowsDisplayed = true;
				}
			} else {
				if (rows.size() == 3) {
					rowsDisplayed = true;
				}
			}
			Assert.assertTrue(rowsDisplayed, "3 rows not displayed on PLP");
			Reporter.log("Verify 3 rows displayed on PLP");
		} catch (Exception e) {
			Assert.fail("3 rows not displayed on PLP");
		}
		return this;
	}

	/**
	 * Verify Devices Per Row
	 *
	 * @return
	 */
	public PLPPage verifyDevicesPerRow() {
		try {
			waitforSpinner();
			boolean devicesPerRowDisplayed = false;
			if (getDriver() instanceof AppiumDriver) {
				if (devicesDisplayedPerRow.size() == 5) {
					devicesPerRowDisplayed = true;
				}
			} else {
				if (devicesDisplayedPerRow.size() == 12) {
					devicesPerRowDisplayed = true;
				}
			}
			Assert.assertTrue(devicesPerRowDisplayed, "4 devices not displayed per row on PLP");
			Reporter.log("4 devices displayed per row on PLP");
		} catch (Exception e) {
			Assert.fail("4 devices not displayed per row on PLP");
		}
		return this;
	}

	/**
	 * Verify Legal Text Under Device Image
	 *
	 * @return
	 */
	public PLPPage verifyLegalTextComponentInPLP() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(legalTextUnderDeviceImageForMobileView.isDisplayed());
				Reporter.log("Legal Text Under Device Image is Displayed");
				clickElementWithJavaScript(clickSeeLegalDisclaimerForMobileView);
				Assert.assertTrue(authorableLegalTextForMobileView.isDisplayed());
				Reporter.log("Authorable Legal Text at the Bottom of PLP is Displayed");
				clickElementWithJavaScript(closeSeeLegalDisclaimerModelForMobileView);
			} else {
				Assert.assertTrue(legalTextUnderDeviceImage.isDisplayed());
				Reporter.log("Legal Text Under Device Image is Displayed");
				Assert.assertTrue(authorableLegalText.isDisplayed());
				Reporter.log("Authorable Legal Text at the Bottom of PLP is Displayed");
			}
		} catch (Exception e) {
			Assert.fail("Legal Text Component on PLP is not Displayed");
		}
		return this;
	}

	/**
	 * Select Manufactures
	 */
	public PLPPage selectManufactures() {
		try {
			filterDropdown.click();
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(mobileFilterDropdownValueApple);
			} else {
				clickElementWithJavaScript(filterDropdownValueApple);
			}
			Reporter.log("Manufactures filter drop down is clickable");
		} catch (Exception e) {
			Assert.fail("Manufactures filter drop down is not clickable");
		}
		return this;
	}

	/**
	 * Click Filter Drop-down
	 */
	public PLPPage clickFilteDropdown() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				iOSFilterDropdown.click();
			} else {
				clickElementWithJavaScript(filterDropdown);
				Reporter.log("Clicked on Filter DropDown");
			}
		} catch (Exception e) {
			Assert.fail("Filter drop down is not clickable");
		}
		return this;
	}

	/**
	 * Verify Manufacturer Label
	 *
	 * @return
	 */
	public PLPPage verifyFilterDropDownHeaders() {
		try {
			Assert.assertTrue(manufacturerLabel.isDisplayed());
			Reporter.log("Manufacturer label is displayed");
			Assert.assertTrue(conditionLabel.isDisplayed());
			Reporter.log("Condition label is displayed");
			Assert.assertTrue(osLabel.isDisplayed());
			Reporter.log("Operating system label is displayed");
			Assert.assertTrue(dealsLabel.isDisplayed());
			Reporter.log("Deals label is displayed");
		} catch (Exception e) {
			Assert.fail("Filter DropDown Header is Missing Options or Not Displayed");
		}
		return this;
	}

	/**
	 * Click On Close Filter
	 */
	public PLPPage clickOnCloseFilter() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitforSpinner();
				iOSFilterDropdown.click();
			} else {
				waitforSpinner();
				clickElementWithJavaScript(filterDropdownCloseButton);
			}
			Reporter.log("Filter drop down close button is clickable");
		} catch (Exception e) {
			Assert.fail("Filter drop down close button is notclickable");
		}
		return this;
	}

	/**
	 * Get No Items Found Text
	 *
	 * @return
	 */
	public String getNoItemsFoundText() {
		return noItemsFound.getText();
	}

	/**
	 * Get No Items Found Text
	 *
	 * @return
	 */
	public String getSuggetionNoItemsFoundText() {
		return noItemsFoundSuggestionText.getText();
	}

	/**
	 * Click Today Button
	 */
	public void clickTodayButton() {
		for (WebElement webElement : deviceTodayComponent) {
			webElement.click();
			break;
		}
	}

	/**
	 * Verify Down Payment Model Header
	 *
	 * @return
	 */
	public PLPPage verifyDownPaymentModelHeader() {
		try {
			downPaymentModelHeader.isDisplayed();
			Reporter.log("Down Payment Model Header is Displayed");
		} catch (Exception e) {
			Assert.fail("Down Payment Model Header is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Down Payment Model Text
	 *
	 * @return
	 */
	public PLPPage verifyDownPaymentModelText() {
		try {
			downPaymentModelText.isDisplayed();
			Reporter.log("Down Payment Model Text is Displayed");
		} catch (Exception e) {
			Assert.fail("Down Payment Model Text is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Down Payment Model Note
	 *
	 * @return
	 */
	public PLPPage verifyDownPaymentModelNote() {
		try {
			downPaymentModelNote.isDisplayed();
			Reporter.log("Down Payment Model Note is Displayed");
		} catch (Exception e) {
			Assert.fail("Down Payment Model Note is not Displayed");
		}
		return this;

	}

	/**
	 * Select Device - Samsung
	 */
	public PLPPage selectSamsung() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				iOSfilterDropdownValueManufacturer.click();
				iOSfilterDropdownValueSamsung.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(filterDropdownValueSamsung), 3);
				clickElementWithJavaScript(filterDropdownValueSamsung);
				Reporter.log("Clicked on Samsung in DropDown");
			}
		} catch (Exception e) {
			Assert.fail("Filter drop down samsung is not clickable");
		}
		return this;
	}

	/**
	 * Select Device- Apple
	 */
	public PLPPage selectApple() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				iOSfilterDropdownValueManufacturer.click();
				mobileFilterDropdownValueApple.click();
			} else {
				Assert.assertTrue(filterDropdownValueApple.isDisplayed());
				clickElementWithJavaScript(filterDropdownValueApple);
			}
			Reporter.log("Clicked on Apple device in DropDown");
		} catch (Exception e) {
			Assert.fail("Apple device is not clickable");
		}
		return this;
	}

	/**
	 * Filter in plp page and select Special offer devices under Deals
	 */
	public PLPPage selectSpecialOfferOnDeals() {
		try {
			clickElementWithJavaScript(filterDropdownDealsValueSpecialOffer);
			Reporter.log("Special offer on deals is clickable");
		} catch (Exception e) {
			Assert.fail("Special offer on deals is not clickable");
		}
		return this;
	}

	/**
	 * Click Device By Name
	 *
	 * @param deviceName
	 * @return
	 */
	public PLPPage clickDeviceByName(String deviceName) {
		checkPageIsReady();
		waitforSpinner();
		int i = 0;
		try {
			if(getDriver() instanceof AppiumDriver){
				if (mobileFeedBackAlert.size() == 1) {
					mobileFeedBackAlert.get(0).click();
				}
				for (int k = 1; k <= plpPaginationPagesListMobileView.size()+1; k++) {
					for (WebElement device : devicesList) {
						if (device.getText().trim().equalsIgnoreCase(deviceName)) {
							moveToElement(device);
							clickElementWithJavaScript(device);
							Reporter.log("Clicked on " + deviceName + "");
							i = i + 1;
							break;
						}
					}
					if (i == 0) {
						moveToElement(paginationNextArrowMobileView);
						clickElementWithJavaScript(paginationNextArrowMobileView);
						waitforSpinner();
					} else {
						break;
					}
				}
				checkPageIsReady();
				if (i == 0) {
					// getDriver().getCurrentUrl().contains("productlist") {
					devicesList.get(0).click();
					Reporter.log("Provided device is not available. Clicked on first device from PLP page");
				}
			}else{
				for (int k = 1; k <= plpPaginationPagesList.size(); k++) {
					for (WebElement device : devicesList) {
						if (device.getText().trim().equalsIgnoreCase(deviceName)) {
							moveToElement(device);
							clickElementWithJavaScript(device);
							Reporter.log("Clicked on " + deviceName + "");
							i = i + 1;
							break;
						}
					}
					if (i == 0) {
						moveToElement(paginationNextArrow);
						clickElementWithJavaScript(paginationNextArrow);
						waitforSpinner();
					} else {
						break;
					}
				}

				checkPageIsReady();
				if (i == 0) {
					// getDriver().getCurrentUrl().contains("productlist") {
					devicesList.get(0).click();
					Reporter.log("Provided device is not available. Clicked on first device from PLP page");
				}
			}

		} catch (Exception e) {
			Assert.fail("Unable to select device in product list page");
		}
		return this;
	}

	/**
	 * Get Device FRP Price
	 *
	 * @return
	 */
	public String getDeviceFRPPrice(String deviceName) {
		String frpPrice = null;

		if (getDriver().getCurrentUrl().contains("productlist")) {
			for (WebElement device : devicesList) {

				if (device.getText().contains(deviceName)) {
					// frpPrice =
					// device.findElement(By.xpath("//a[text()='"+deviceName+"']//..//..//div//p[@class='text-bold
					// padding-top-small xsmall ng-binding
					// ng-scope']")).getText();
					frpPrice = device
							.findElement(By.xpath("//p/a[text()='" + deviceName
									+ "']//..//..//div//div[@class='text-bold padding-top-small xsmall ng-binding ng-scope']/span"))
							.getText();
					frpPrice = frpPrice.substring(frpPrice.indexOf('$') + 1);
					break;
				}
			}
		}
		return frpPrice;
	}

	/**
	 * Click Filter Dropdown
	 *
	 * @return
	 */
	public PLPPage clickFilterDropdown() {
		try {
			clickElementWithJavaScript(filterDropdown);
			Reporter.log("Filter drop down is clickable");
		} catch (Exception e) {
			Assert.fail("Filter drop down is not clickable");
		}
		return this;
	}

	/**
	 * Select OnSale Filter Value
	 */
	public PLPPage selectOnSaleFilterValue() {
		try {
			waitFor(ExpectedConditions.visibilityOf(onSaleFilterValue), 3);
			clickElementWithJavaScript(onSaleFilterValue);
			Reporter.log("On sale filter value is clickable");
		} catch (Exception e) {
			Assert.fail("On sale filter value is not clickable");
		}
		return this;
	}

	/**
	 * click samsung galaxy device
	 *
	 * @return
	 */

	public PLPPage clickSelectedPromoDevice() {
		try {
			for (int k = 1; k <= plpPaginationPagesList.size(); k++) {
				if (selectDeviceFromPLP.isDisplayed()) {
					clickElementWithJavaScript(selectDeviceFromPLP);
					break;
				}
				clickElementWithJavaScript(paginationNextArrow);
				waitforSpinner();
			}
			Reporter.log("Clicked promo device");
		} catch (Exception e) {
			Assert.fail("Not found promo device");
		}
		return this;
	}

	/**
	 * click IPhone Plus 8 device
	 *
	 * @param deviceName
	 */
	public PLPPage clickIPhonePlus8DeviceByName(String deviceName) {
		try {
			if (getDriver().getCurrentUrl().contains("productlist")) {
				for (WebElement device : devicesList) {
					if (device.getText().contains("Apple iPhone 8") || device.getText().contains(deviceName)) {
						device.click();
						break;
					}
				}
			}
			Reporter.log("Apple Iphone is clickable");
		} catch (Exception e) {
			Assert.fail("Apple Iphone is not clickable");
		}
		return this;
	}

	/**
	 * Verify Manufacture Devices
	 */
	public PLPPage verifyManufactureDevicesAndOperatingSystems() {
		try {
			boolean isManufactureAndOSDisplayed = false;
			List<String> manufacturerAndOSdetails = new ArrayList<>(Arrays.asList("T-Mobile", "Apple", "Samsung", "LG",
					"Alcatel", "HTC", "Coolpad", "Kyocera", "Motorola", "iOS", "Android", "Other"));
			for (String detail : manufacturerAndOSdetails) {
				for (WebElement webElement : plpPageDevicesAndOperatingSystemsList) {
					if (webElement.isDisplayed()) {
						if (detail.equals(webElement.getText())) {
							isManufactureAndOSDisplayed = true;
							break;
						} else {
							isManufactureAndOSDisplayed = false;
						}
					}
				}
				if (!isManufactureAndOSDisplayed) {
					break;
				}
			}
			Assert.assertTrue(isManufactureAndOSDisplayed,
					"Manufature device types and operating system types is not dispalyd");
			Reporter.log("Manufature device types and operating system types is displayed");
		} catch (Exception e) {
			Assert.fail("Manufature device types and operating system types is not dispalyd");
		}
		return this;
	}

	/**
	 * Select T-Mobile Manufacturer
	 */
	public PLPPage selectTMobileManufacturer() {
		try {
			Assert.assertTrue(selectTMobileManufacturer.isDisplayed());
			clickElementWithJavaScript(selectTMobileManufacturer);
			Reporter.log("T-Mobile manufacturer is clickable");
		} catch (Exception e) {
			Assert.fail("T-Mobile manufacturer is not clickable");
		}
		return this;
	}

	/**
	 * Verify Filter Devices In PLPPage
	 */
	public PLPPage verifyFilterDevicesInPLPPage(String option) {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.loader")));
			boolean isFilterDevices = false;
			for (WebElement webElement : devicesList) {
				if (webElement.getText().contains(option)) {
					isFilterDevices = true;
				}
			}
			Assert.assertTrue(isFilterDevices, "Devices are not filtered by " + option);
			Reporter.log("All Devices are filtered in in PLP Page correctly");
		} catch (Exception e) {
			Assert.fail("Failed to verify filtered devices in in PLP Page");
		}
		return this;
	}

	/**
	 * Verify Device Component in PLP page
	 *
	 * @return
	 */
	public PLPPage verifyDeviceComponentInPlpPage() {
		try {
			Assert.assertTrue(deviceImagesInPLPPage.get(0).isDisplayed());
			Reporter.log("Device images is Displayed");
			Assert.assertTrue(devicesList.get(0).isDisplayed());
			Reporter.log("Device Names is Displayed");
			Assert.assertTrue(deviceReviewStars.get(0).isDisplayed());
			Reporter.log("Device Review stars is Displayed");
			Assert.assertTrue(deviceReviewNumbers.get(0).isDisplayed());
			Reporter.log("Device Review numbers is Displayed");
			Assert.assertTrue(deviceTodayComponent.get(0).isDisplayed());
			Reporter.log("Device Today component is Displayed");
			Assert.assertTrue(deviceMonthlyComponents.get(0).isDisplayed());
			Reporter.log("Device Monthly component is Displayed");
			Assert.assertTrue(deviceFullRetailPriceComponent.get(0).isDisplayed());
			Reporter.log("Device Full retail price component is Displayed");
		} catch (Exception e) {
			Assert.fail("Device Component is Missing Feature or Is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Device EIPMonthlyText
	 *
	 * @return
	 */
	public PLPPage verifyDeviceEIPMonthlyText() {
		try {
			waitforSpinner();
			for (WebElement deviceMonthlyComponent : deviceMonthlyComponents) {
				Assert.assertTrue(deviceMonthlyComponent.isDisplayed(), "Device EIP Monthly Text is not displayed");
			}
			Reporter.log("Device EIP Mothly Text is present for all Devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device ERP Monthly Text");
		}
		return this;
	}

	/**
	 * Verify Device Dollar Sign
	 *
	 * @return
	 */
	public PLPPage verifyDollarSign() {
		try {
			waitforSpinner();
			for (WebElement eipDollarSign : dollarSign) {
				Assert.assertTrue(eipDollarSign.isDisplayed(), "Dollar sign is not displayed");
			}
			Reporter.log("Dollar sign is present for all devices prices");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Dollar sign");
		}
		return this;
	}

	/**
	 * Verify Installment Month Terms
	 *
	 * @return
	 */
	public PLPPage verifyInstallmentTerm() {
		try {
			waitforSpinner();
			for (WebElement eipInstallmentMonth : eipInstallmentMonths) {
				Assert.assertTrue(eipInstallmentMonth.isDisplayed(), "Installment Monthly term is not displayed");
				Assert.assertTrue(
						eipInstallmentMonth.getText().contains("9")
								| eipInstallmentMonth.getText().contains("12")
								| eipInstallmentMonth.getText().contains("24")
								| eipInstallmentMonth.getText().contains("36"),
						"Loan term length are not correct for some devices in phone PLP page");
			}
			Reporter.log("Installment Monthly term is present for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Installment Monthly term");
		}
		return this;
	}

	/**
	 * Verify Device Stars
	 *
	 * @return
	 */
	public PLPPage verifyDeviceStars() {
		try {
			waitforSpinner();
			for (WebElement deviceReviewStar : deviceReviewStars) {
				Assert.assertTrue(deviceReviewStar.isDisplayed(), "Device Stars are not displayed");
			}
			Reporter.log("Device Stars are present For all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Starts");
		}
		return this;
	}

	/**
	 * Verify Monthly EIP Price
	 *
	 * @return
	 */
	public PLPPage verifyEIPPrice() {
		try {

			waitforSpinner();
			for (WebElement eipMonthlyPrices : eipMonthlyPrice) {
				Assert.assertTrue(eipMonthlyPrices.isDisplayed(), "EIP Monthly price is not displayed");
			}
			Reporter.log("EIP Price is displayed for all the devices");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP Price for the devices");
		}
		return this;
	}

	/**
	 * Verify FRP Price
	 *
	 * @return
	 */
	public PLPPage verifyFRPPriceText() {
		try {

			waitforSpinner();
			for (WebElement FRPPricesText : fRPPrice) {
				Assert.assertTrue(FRPPricesText.getText().contains("Full retail price"), "FRP price is not displayed");
				Assert.assertTrue(FRPPricesText.getText().contains("$"), "FRP price Dollar Sign is not displayed");
			}
			Reporter.log("FRP Prices are displayed for all the devices in PLP page");
		} catch (Exception e) {
			Assert.fail("Failed to display FRP Price Text on PLP page");
		}
		return this;
	}

	/*
	 * Get Device Down Payment with $ symbol
	 *
	 * @return
	 */
	public String getDeviceDownPaymentInPLP() {
		String downPayment;
		downPayment = todayDownPayment.get(0).getText();
		return downPayment;
	}

	/**
	 * Get Device Loan Term
	 *
	 * @return
	 */
	public String getDeviceLoanTermInPLP() {
		String loanTerm;
		loanTerm = eipInstallmentMonths.get(0).getText();
		loanTerm = loanTerm.substring(loanTerm.indexOf('x') + 1, loanTerm.indexOf(' '));
		return loanTerm;
	}

	/**
	 * Get Device Monthly Payment
	 *
	 * @return
	 */
	public String getDeviceMonthlyPaymentInPLP() {
		String monthlyPayment;
		monthlyPayment = eipMonthlyPrice.get(0).getText();
		return monthlyPayment;
	}

	/**
	 * click device by name
	 *
	 */
	public PLPPage clickFirstDevice() {
		try {
			waitforSpinner();
			if (getDriver().getCurrentUrl().contains("productlist")) {
				devicesList.get(0).click();
			}
			Reporter.log(devicesList.get(0).getText() + " - is selected");
		} catch (Exception e) {
			Assert.fail("Unable to select first device");
		}
		return this;
	}

	/**
	 * Verify EIP Downpayment
	 */
	public PLPPage verifyEIPDownPayment() {
		try {
			waitforSpinner();
			for (WebElement eipDownpayment : todayDownPayment) {
				Assert.assertTrue(eipDownpayment.isDisplayed(), "EIP Downpayment is not displayed");
			}
			Reporter.log("EIP Downpayment is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP downpayment for the devices");
		}
		return this;
	}

	/**
	 * Click Back Button
	 *
	 * @return
	 */
	public PLPPage clickBackButton() {
		try {
			backBtn.click();
		} catch (Exception e) {
			Assert.fail("Back button is not displayed");
		}
		return this;
	}

	/**
	 * Verify Filter Box
	 */
	public PLPPage verifyFilterBox() {
		try {
			Assert.assertTrue(filterDropdown.isDisplayed(), "Filter box not displayed in PLP page");
			Reporter.log("Filter box is displyed in PLP page");
		} catch (Exception e) {
			Assert.fail("Filter box not displayed in PLP page");
		}
		return this;
	}

	/**
	 * Click Filter Box
	 */
	public PLPPage clickFilterBox() {
		try {
			filterDropdown.click();
			Reporter.log("Filter Box is clickable in plp page");
		} catch (Exception e) {
			Assert.fail("Filter box is not clickable");
		}
		return this;
	}

	/**
	 * Verify Sort Dropdown Options When we already select any option, Sort menu
	 * should be close
	 */
	public PLPPage verifySortDropdownOptionsList() {
		try {
			Assert.assertFalse(isElementDisplayed(sortDropdownOptionsList), "Sort dropdown options is displayed");
			Reporter.log("Sort dropdown options is not displayed");
		} catch (Exception e) {
			Assert.fail("Sort dropdown options is displayed");
		}
		return this;
	}

	/**
	 * Verify PLP page footer
	 */
	public PLPPage verifyPLPPageFooterNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(pLPPageFooter), "PLP Page footer is displayed");
			Reporter.log("PLP Page footer is  not displayed");
		} catch (Exception e) {
			Assert.fail("PLP Page footer is displayed");
		}
		return this;
	}

	/**
	 * verify Featured Filter
	 */
	public PLPPage verifyFeaturedFilter() {
		try {
			Assert.assertTrue(featuredFilter.isDisplayed());
			Reporter.log("Featured filter is displayed");

		} catch (Exception e) {
			Assert.fail("Featured filter is not displayed");
		}
		return this;
	}

	/**
	 * Click Promo Offer text
	 */
	public PLPPage clickPromoOfferText() {
		try {
			discountOffers.get(0).click();
			Reporter.log("Promo Text for first device clicked on PLP");
		} catch (Exception e) {
			Assert.fail("Failed to click Promo Text for first device");
		}
		return this;
	}

	/**
	 * verify Promo Description in Modal
	 */
	public PLPPage verifyPromoDescriptionInModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(promotionDescription));
			Assert.assertTrue(promotionDescription.isDisplayed(), "Promo description is not present");
			Reporter.log("Promo description is present");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo Description");
		}
		return this;
	}

	/**
	 * Verify that Device Price are equal
	 */
	public PLPPage compareDevicePrice(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Device Price are not equal");
			Reporter.log("Device Pric  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Device Price");
		}
		return this;
	}

	/**
	 * Verifies EIP Price modal for device on PLP
	 */
	public void verifyEIPPricingModal() {
		waitforSpinner();
		try {
			for (WebElement ele : eipPriceModal) {
				Assert.assertTrue(ele.isDisplayed(), "EIP Price modal isn't available for the device");
				Reporter.log("Verified EIP Price modal for the devices on PLP");
			}
		} catch (Exception e) {
			Assert.fail("EIP Price modal isn't available for the device");
		}
	}

	/**
	 * Verifies Special offers badges for the devices
	 */
	public void verifySpecialOfferBadgesForDevices() {
		waitforSpinner();
		try {
			for (WebElement ele : specialOfferDevices) {
				Assert.assertTrue(ele.isDisplayed(), "EIP Price modal isn't available for the device");
				Reporter.log("Verified EIP Price modal for the devices on PLP");
			}
		} catch (Exception e) {
			Assert.fail("Special offer devices aren't available on PLP");
		}
	}

	/**
	 * Gets devices count from PLP
	 *
	 * @return
	 */
	public int getDevicesCountFromPLP() {
		waitforSpinner();
		String results = null;
		int resultsCount = 0;
		try {
			results = devicesOnPLP.getText();
			String[] Count = results.split("&");
			resultsCount = parseInt(Count[0].trim());
			Reporter.log("Captured total devices on PLP:" + resultsCount);
		} catch (Exception e) {
			Assert.fail("Unable to get devices count");
		}
		return resultsCount;
	}

	/**
	 * Verified Devices count after applying filter
	 * 
	 * @param devicesCount
	 * @param deviceCountAfterFilter
	 */
	public void verifyDevicesCountAfterApplyingFilter(int devicesCount, int deviceCountAfterFilter) {
		try {
			Assert.assertTrue(devicesCount != deviceCountAfterFilter, "Devices count is still same");
			Reporter.log("Verified devices count after applying filters");
		} catch (Exception e) {
			Assert.fail("Devices count are same");
		}
	}

	/**
	 * Verified Devices count after applying sorting
	 * 
	 * @param devicesCount
	 * @param deviceCountAfterSorting
	 */
	public void verifyDevicesCountAfterApplyingSorting(int devicesCount, int deviceCountAfterSorting) {
		try {
			Assert.assertTrue(devicesCount == deviceCountAfterSorting, "Devices count is still same");
			Reporter.log("Verified devices count after applying filters");
		} catch (Exception e) {
			Assert.fail("Devices count are same");
		}
	}

	/**
	 * Verify Manufacturer Name
	 */
	public PLPPage verifyAllDeviceManufacturerName() {
		try {
			for (WebElement deviceName : devicesList) {
				String deviceNameText = deviceName.getText();
				Assert.assertTrue(
						deviceNameText.contains("Apple") || deviceNameText.contains("LG")
								|| deviceNameText.contains("Samsung") || deviceNameText.contains("T-Mobile®")
								|| deviceNameText.contains("Alcatel") || deviceNameText.contains("Motorola")
								|| deviceNameText.contains("OnePlus"),
						"Manufacturer name is not displayed for a Device");
				moveToElement(deviceName);
			}
			Reporter.log("Manufacturer name is displayed for devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Manufacturer name for devices");
		}
		return this;
	}

	/**
	 * Verify Device Names for all Devices on PLP
	 */
	public PLPPage verifyAllDeviceName() {
		try {
			for (WebElement deviceName : devicesList) {
				Assert.assertTrue(deviceName.isDisplayed(), "Device name is not displayed for a Device");
				moveToElement(deviceName);
			}
			Reporter.log("Device name is displayed for devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device name for devices");
		}
		return this;
	}

	/**
	 * Verify Device Names for all Devices on PLP
	 */
	public PLPPage clickOnPage(int page) {
		try {
			clickElementWithJavaScript(plpPaginationPagesList.get(page - 1));
			Reporter.log("Device name is displayed for devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device name for devices");
		}
		return this;
	}

	/**
	 * Verifies the selected category
	 *
	 * @param category
	 * @return
	 */
	public PLPPage verifySelectedCategory(String category) {
		try {
			waitFor(ExpectedConditions.visibilityOf(selectedCategory));
			Assert.assertTrue(selectedCategory.getText().equalsIgnoreCase(category), "Respective category is selected");
			Reporter.log("Selected category is: " + category);
		} catch (Exception e) {
			Assert.fail("Failed to select '" + category + "' category");
		}
		return this;
	}
}
