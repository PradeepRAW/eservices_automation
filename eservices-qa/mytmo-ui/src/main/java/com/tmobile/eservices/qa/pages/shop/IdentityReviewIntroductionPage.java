package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class IdentityReviewIntroductionPage extends CommonPage {
	
	@FindBy(css = "div[ng-bind*='ctrl.contentData.identityVerifyHeader']")
	private WebElement introductionPageHeaderText;
	
	@FindBy(css = "div[ng-bind*='ctrl.contentData.identityVerifyBody']")
	private WebElement introductionPageTextBelowHeader;
	
	@FindBy(css = "button[ng-click*='onClickContinue']")
	private WebElement introductionPageContinueCTA;
	
	@FindBy(css = "[ng-click*='openCancelModal']")
	private WebElement introductionPageCancelCTA;
	
	@FindBy(css = "[ng-class*='isHideHeader']")
	private WebElement introductionPageHeader;
	
	@FindBy(css = "[ng-class*='isHideFooter']")
	private WebElement introductionPageFooter;
	
	@FindBy(css = "span[ng-bind*='contentData.cancelModalHeader']")
	private WebElement warningModalHeaderText;
	
	@FindBy(css = "span[ng-bind*='cancelModalBody']")
	private WebElement warningModalBodyText;
	
	@FindBy(css = "span[ng-bind*='ctrl.contentData.modalPrimary']")
	private WebElement warningModalPrimaryContinueCTA;
	
	@FindBy(css = "button[ng-click*='$ctrl.redirectToReject()']")
	private WebElement warningModalSecondaryCancelCTA;
	
	@FindBy(css = "i[ng-click*='close($event)']")
	private WebElement closeIconOnWarningModal;
	
	@FindBy(css = "[ng-click*='ctrl.goToback']")
	private WebElement backButton;
	
	@FindBy(css = "[ng-if*='ctrl.isCustomHeader']")
	private WebElement sedonaHeader;
	
	/**
	 * @param webDriver
	 */
	public IdentityReviewIntroductionPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	
	/**
	 * Verify Identity Review Introduction Page
	 * 
	 * @return
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
			Reporter.log("Navigated to Identity Review Introduction Page");
		} catch (Exception e) {
			Assert.fail("Identity Review Introduction Page not loaded");
		}
		return this;
	}
	
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public boolean verifyPageUrl() {

		boolean isIdentityReviewPageDisplayed = false;
		try {
			if(getDriver().getCurrentUrl().contains("identityreviewintroduction")) {
			
			isIdentityReviewPageDisplayed = true;
			Reporter.log("IdentityReviewIntroduction page is displayed");
			}
		}
		catch(Exception e) {
			Assert.fail("IdentityReviewIntroduction is not displayed");
		}
		return isIdentityReviewPageDisplayed;
	}
	
	
	/**
	 * Verify Identity Review Introduction Page Header
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionPageHeaderText() {
		try {
			Assert.assertTrue(introductionPageHeaderText.isDisplayed(), "Header is not displayed");
			Reporter.log("Header is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Header on Introduction Page");
		}
		return this;
	}
	
	/**
	 * Verify Identity Review Introduction Page Text Below Header
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionPageTextBelowHeader() {
		try {
			Assert.assertTrue(introductionPageTextBelowHeader.isDisplayed(), "Text Below Header is not displayed");
			Reporter.log("Text Below Header is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Text Below Header on Introduction Page");
		}
		return this;
	}
	
	
	/**
	 * Verify Identity Review Introduction Page Continue CTA
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionContinueCTA() {
		checkPageIsReady();
		try {
			Assert.assertTrue(introductionPageContinueCTA.isDisplayed(), "Continue CTA is not displayed");
			Reporter.log("Continue CTA is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Continue CTA on Introduction Page");
		}
		return this;
	}

	
	/**
	 * Verify Identity Review Introduction Page Cancel CTA
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionCancelCTA() {
		try {
			Assert.assertTrue(introductionPageCancelCTA.isDisplayed(), "Cancel CTA is not displayed");
			Reporter.log("Cancel CTA is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Cancel CTA on Introduction Page");
		}
		return this;
	}

	
	/**
	 * Verify Identity Review Introduction Page Header & Footer
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionheaderAndFooter() {
		try {
			Assert.assertFalse(introductionPageHeader.isDisplayed(), "Header is displayed");
			Assert.assertFalse(introductionPageFooter.isDisplayed(), "Footer is displayed");
			Reporter.log("Header & footer are not Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Header & footer on Introduction Page");
		}
		return this;
	}

	
	/**
	 * Click Identity Review Introduction Page Continue CTA
	 */
	public IdentityReviewIntroductionPage clickIdentityReviewIntroductionContinueCTA() {
		try {
			Assert.assertTrue(introductionPageContinueCTA.isDisplayed(), "Continue CTA is not displayed");
			clickElement(introductionPageContinueCTA);
			Reporter.log("Continue CTA is Clicked");
		} catch (Exception e) {
			Reporter.log("Failed to Click Continue CTA on Introduction Page");
		}
		return this;
	}

	
	/**
	 * Click Identity Review Introduction Page Cancel CTA
	 */
	public IdentityReviewIntroductionPage clickIdentityReviewIntroductionCancelCTA() {
		try {
			Assert.assertTrue(introductionPageCancelCTA.isDisplayed(), "Cancel CTA is not displayed");
			introductionPageCancelCTA.click();
			Reporter.log("Cancel CTA is Clicked");
		} catch (Exception e) {
			Reporter.log("Failed to Click Cancel CTA on Introduction Page");
		}
		return this;
	}
	
	
	/**
	 * Verify Identity Review Introduction Page Warning Modal
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionWarningModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(warningModalHeaderText));
			Assert.assertTrue(warningModalHeaderText.isDisplayed(), "Warning Modal Header Text is not displayed");
			Reporter.log("Warning Modal is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Warning Modal on Introduction Page");
		}
		return this;
	}
	
	/**
	 * Verify Identity Review Introduction Page Warning Modal
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionWarningModalTextBody() {
		try {
			waitFor(ExpectedConditions.visibilityOf(warningModalBodyText));
			Assert.assertTrue(warningModalBodyText.isDisplayed(), "Warning Modal Body Text is not displayed");
			Reporter.log("Warning Modal is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Warning Modal on Introduction Page");
		}
		return this;
	}
	/**
	 * Verify Identity Review Introduction Page Warning Modal
	 */
	public IdentityReviewIntroductionPage clickCloseIconOnIdentityReviewIntroductionWarningModal() {
		try {
			Assert.assertTrue(closeIconOnWarningModal.isDisplayed(), "Warning Modal Close Icon is not displayed");
			closeIconOnWarningModal.click();
			Reporter.log("Warning Modal close Icon is clicked");
		} catch (Exception e) {
			Reporter.log("Failed to click on Warning Modal close Icon on Introduction Page");
		}
		return this;
	}
	

	/**
	 * Verify Identity Review Introduction Page Warning Modal
	 */
	public IdentityReviewIntroductionPage verifyCloseIconOnIdentityReviewIntroductionWarningModal() {
		try {
			Assert.assertTrue(closeIconOnWarningModal.isDisplayed(), "Warning Modal Close Icon is not displayed");
			closeIconOnWarningModal.click();
			Reporter.log("Warning Modal close Icon is clicked");
		} catch (Exception e) {
			Reporter.log("Failed to click on Warning Modal close Icon on Introduction Page");
		}
		return this;
	}
	
	/**
	 * Verify Identity Review Introduction Page Warning Modal primary cta is dispalyed
	 */
	public IdentityReviewIntroductionPage verifyPrimaryContinueCTAIconOnIdentityReviewIntroductionWarningModal() {
		try {
			Assert.assertTrue(warningModalPrimaryContinueCTA.isDisplayed(), "Warning Modal Primary CTA is not displayed");
		
			Reporter.log("Warning Modal Primary CTA Icon is verified");
		} catch (Exception e) {
			Reporter.log("Failed to verify on Warning Modal Primary CTA  Icon ");
		}
		return this;
	}
	
	/**
	 * Verify Identity Review Introduction Page Warning Modal secondary CTA is displayed
	 */
	public IdentityReviewIntroductionPage verifySecondaryCancelCTAIconOnIdentityReviewIntroductionWarningModal() {
		try {
			Assert.assertTrue(warningModalSecondaryCancelCTA.isDisplayed(), "Warning Modal Secondary CTA  Icon is not displayed");
			
			Reporter.log("Warning Modal Secondary CTA  Icon is verified");
		} catch (Exception e) {
			Reporter.log("Failed to verify on Warning Modal Secondary CTA  Icon ");
		}
		return this;
	}
	
	/**
	 * click primary cta on Identity Review Introduction Page Warning Modal
	 */
	public IdentityReviewIntroductionPage clickPrimaryContinueCTAIconOnIdentityReviewIntroductionWarningModal() {
		try {
			
			warningModalPrimaryContinueCTA.click();
			Reporter.log("Warning Modal Primary CTA Icon is clicked");
		} catch (Exception e) {
			Reporter.log("Failed to click on Warning Modal close Primary CTA  Icon on Introduction Page");
		}
		return this;
	}
	
	/**
	 * click secondary cta on Identity Review Introduction Page Warning Modal
	 */
	public IdentityReviewIntroductionPage clickSecondaryCancelCTAIconOnIdentityReviewIntroductionWarningModal() {
		try {
			clickElementWithJavaScript(warningModalSecondaryCancelCTA);
			Reporter.log("Warning Modal secondary CTA Icon is clicked");
		} catch (Exception e) {
			Reporter.log("Failed to click on Warning Modal close secondary CTA  Icon on Introduction Page");
		}
		return this;
	}
	
	
	/**
	 * Verify Back Button not displayed
	 */
	public IdentityReviewIntroductionPage verifyBackButtonNotDisplayed() {
		try {
			Assert.assertFalse(backButton.isDisplayed(), "Back Button is displayed");
			Reporter.log("Back Button is not displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Back Button");
		}
		return this;
	}
	
	
	/**
	 * Verify Identity Review Introduction Page Header
	 */
	public IdentityReviewIntroductionPage verifySedonaHeaderDisplayed() {
		try {
			Assert.assertTrue(sedonaHeader.isDisplayed(), "Header is not displayed");
			Reporter.log("Sedona Header is Displayed");
		} catch (Exception e) {
			Reporter.log("Failed to Verify Sedona Header on Introduction Page");
		}
		return this;
	}
	
	/**
	 * Verify Identity Review Introduction Page Not Displayed
	 * 
	 * @return
	 */
	public IdentityReviewIntroductionPage verifyIdentityReviewIntroductionPageNotDisplayed() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertFalse(getDriver().getCurrentUrl().contains("identityreviewintroduction"),"Fraud Check Page is displayed ");
			Reporter.log("Fraud Check Page is not Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Fraud Check Page");
		}
		return this;
	}
	
}
