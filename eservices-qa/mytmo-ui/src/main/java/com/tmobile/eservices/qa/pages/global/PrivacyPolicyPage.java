/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class PrivacyPolicyPage extends CommonPage {

	public static final String privacyPolicyPageUrl = "https://www.t-mobile.com/responsibility/privacy/privacy-policy";
	public static final String interestBasedAdsPageUrl = "/company/website/privacypolicy.aspx#choicesaboutadvertising";
	public static final String interestBasedAdsPageLoadText = "Interest-Based Ads.";

	@FindBy(xpath = "//h2[contains(text(),'T-Mobile Privacy Statement')]")
	private WebElement privacyPolicyPageHeader;

	@FindBy(xpath = "//h1[contains(text(),'Interest-Based Ads')]")
	private WebElement interestBasedAdsText;

	/**
	 * 
	 * @param webDriver
	 */
	public PrivacyPolicyPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Privacy Policy Page
	 * 
	 * @return
	 */
	public PrivacyPolicyPage verifyPrivacyPolicyPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(privacyPolicyPageHeader.isDisplayed(), Constants.PRIVACYPOLICY_PAGE_ERROR);
			Reporter.log("Privacy policy page is displayed");
		} catch (Exception e) {
			Reporter.log("Privacy policy page not displayed");
		}

		return this;
	}

	/**
	 * Verify Privacy Policy Page
	 * 
	 * @return
	 */
	public PrivacyPolicyPage verifyInterestBasedAds() {
		Assert.assertTrue(interestBasedAdsText.isDisplayed(), Constants.INTERESTBASEDADS_TEXT_ERROR);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Privacy Policy class instance.
	 */
	public PrivacyPolicyPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(privacyPolicyPageUrl);
		return this;
	}

}
