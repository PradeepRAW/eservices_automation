
package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.CommonPage;

public class BenefitsStatementPage extends CommonPage {

	@FindBy(id = "bb-benefits-ui")
	private WebElement pageLoadElement;

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindBy(css = "h1.bb-title")
	private WebElement pageHeader;

	private final String pageUrl = "benefits";

	public BenefitsStatementPage(WebDriver webDriver) {
		super(webDriver);
	}

	public BenefitsStatementPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public BenefitsStatementPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			Thread.sleep(1000);
			// waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			// verifyPageHeader();
		} catch (Exception e) {
			Assert.fail("benefit statement page not loaded");
		}
		return this;
	}

}
