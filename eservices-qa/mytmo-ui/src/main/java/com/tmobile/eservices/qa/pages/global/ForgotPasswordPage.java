package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.web.components.TextBox;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author blakshminarayana
 *
 */
public class ForgotPasswordPage extends CommonPage {

	public static final String pageUrl = "/oauth2/v1/forgotpassword";
	public static final String pageText = "Reset Your Password";

	@FindBy(css = ".ui_headline.mt30.mobile-margin-top.ng-binding")
	private WebElement resetPasswordText;

	@FindBy(linkText = "Resend password")
	private WebElement resendPassword;

	@FindBy(id = "userId")
	private TextBox emailOrPhone;

	@FindBy(id = "ZipcodeBox")
	private WebElement biilingZipCode;

	@FindBy(css = ".btn.btn-primary.mr30.btn-next")
	private WebElement nextButton;

	@FindBy(linkText = "Back")
	private WebElement backButton;

	/**
	 * 
	 * @param webDriver
	 */
	public ForgotPasswordPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * 
	 * @param emailOrOptionsText
	 */
	public ForgotPasswordPage enterEmailOrPhone(String emailOrOptionsText) {
		try {
			emailOrPhone.clear();
			emailOrPhone.enterText(emailOrOptionsText);
			Reporter.log("Successfully sent emailOrPhone");
		} catch (Exception e) {
			Reporter.log("Sending emailOrPhone was unsuccessful." + e.getMessage());
		}
		return this;
	}

	/**
	 * enter zip code value
	 * 
	 * @param zipCode
	 * @return
	 */
	public ForgotPasswordPage enterZipCode(String zipCode) {
		try {
			waitforSpinner();
			sendTextData(biilingZipCode, zipCode);
			biilingZipCode.sendKeys(Keys.TAB);
			Reporter.log("Successfully sent zip code.");
		} catch (Exception e) {
			Reporter.log("Sending zip code was unsuccessful." + e.getMessage());
		}
		return this;
	}

	/**
	 * get reset password text
	 * 
	 * @return
	 */
	public ForgotPasswordPage getResetPasswordText() {
		Assert.assertEquals(resetPasswordText.getText().trim(), Constants.FORGOT_PSWD_HEADER_TEXT);
		Reporter.log("Reset your password text is not displayed");
		return this;
	}

	/**
	 * click next button
	 */
	public ForgotPasswordPage clickNextButton() {
		try {
			nextButton.click();
			Reporter.log("Clicked on nextButton link");
		} catch (Exception e) {
			Assert.fail("Click on nextButton link failed");
		}
		return this;
	}

	/**
	 * Click Resend Password
	 */
	public ForgotPasswordPage clickResendPassword() {
		try {
			resendPassword.click();
			Reporter.log("Clicked on resendPassword link");
		} catch (Exception e) {
			Assert.fail("Click on resendPassword link failed");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the Forgot Password class instance.
	 */
	public ForgotPasswordPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			getDriver().getPageSource().contains(pageText);
		} catch (Exception e) {
			Assert.fail("May not be correct page");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Forgot Password class instance.
	 */
	public ForgotPasswordPage verifyPageUrl() {
		try {
			checkPageIsReady();
			getDriver().getCurrentUrl().contains(pageUrl);
		} catch (Exception e) {
			Assert.fail("May not be correct page");
		}
		return this;
	}

}
