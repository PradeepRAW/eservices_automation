package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class LazerSharkApi extends ApiCommonLib {

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response isAddressExist(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildGetLazerSharkHeader(apiTestData);
		String resourceURL = "customer-identity/v1/customer-addresses/address";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}
	private Map<String, String> buildGetLazerSharkHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer ada3ce6e-8b47-43ef-9d5e-6eb93b91f189");
		headers.put("Content-Type", "application/json");
		headers.put("activityid", "ADJH12KLJ");
		return headers;
	}

}
