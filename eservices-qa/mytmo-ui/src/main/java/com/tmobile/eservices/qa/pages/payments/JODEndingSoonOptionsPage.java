package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class JODEndingSoonOptionsPage extends CommonPage {

	private final String pageUrl = "leaseEndingOptions";

	@FindBy(css = "p.Display3")
	private WebElement pageHeader;
	
	@FindBy(css = "img.phone-image")
	private WebElement phoneImage;
	
	@FindBy(css = "p.Display6")
	private WebElement friendlyName;
	
	@FindBy(css = "p.legal.padding-top-xsmall")
	private WebElement msisdn;
	
	@FindBy(css = "p.body.padding-top-xsmall-xs")
	private WebElement deviceName;
	
	@FindBy(css = "div.d-inline-block.pr-3.body-bold.black>span.Display5")
	private WebElement dueAmount;
	
	@FindBy(css = "span.H5-heading")
	private WebElement monthlyPaymentAmount;
	
	@FindBy(css = "button.PrimaryCTA.full-btn-width.float-md-left.float-lg-left.float-xl-left.w-78")
	private WebElement setupInstallationPlanCTA;
	
	@FindBy(css = "p.legal>b")
	private WebElement legalText;
	
	@FindBy(css = "button.SecondaryCTA.blackCTA.full-btn-width.w-343")
	private WebElement findALocationCTA;
	
	@FindBy(css = "p.body-bold.text-center.black")
	private WebElement fullBalanceLabel;
	
	@FindBy(css = "div.body.col-6.offset-4.col-md-5.offset-md-5.padding-bottom-medium.d-flex.text-center>p:nth-of-type(1)")
	private WebElement remainingBalance;

	@FindBy(css = "p.Display5.text-center")
	private List<WebElement> optionHeaders;

	@FindBy(css = "p.body.text-center")
	private List<WebElement> optionSubHeaders;

	public JODEndingSoonOptionsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public JODEndingSoonOptionsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public JODEndingSoonOptionsPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("JOD  Ending Soon Options Page is loaded.");
		} catch (Exception e) {
			Assert.fail("JOD  Ending Soon Options page is not loaded");
		}
		return this;
	}
	
	public void verifyOption1BladeDetails() {
		verifyPhoneImage(phoneImage);
		verifyFriendlyName(friendlyName);
		verifyMSISDN(msisdn);
		verifyDeviceName(deviceName);
		verifyDueAmount(dueAmount);
		verifyMonthlyPaymentAmount(monthlyPaymentAmount);
	}
	
	public void verifyOption3BladeDetails() {
		verifyFullBalanceText();
		verifyRemainingBalanceAmount(remainingBalance);
	}
	
	/**
	 *
	 * Verify that the legal text is loaded completely.
	 */
	public void verifyLegalText() {
		try {
			legalText.isDisplayed();
			Reporter.log("Legal Text displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Legal Text is not found in JOD Ending Soon Options Page");
		}
		
	}
	
	/**
	 * verify phone image is displayed
	 * 
	 * @param phoneImage
	 */
	public void verifyPhoneImage(WebElement phoneImage) {
		try {
			phoneImage.isDisplayed();
			Reporter.log("Phone Image is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Phone Image is not found in JOD Ending Soon Options Page");
		}
	}
	
	/**
	 * verify friendlyName is displayed
	 * 
	 * @param friendlyName
	 */
	public void verifyFriendlyName(WebElement friendlyName) {
		try {
			friendlyName.isDisplayed();
			Reporter.log("Friendly Name is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Friendly Name is not found in JOD Ending Soon Options Page");
		}
	}
	
	/**
	 * verify MSISDN is displayed
	 * 
	 * @param MSISDN
	 */
	public void verifyMSISDN(WebElement msisdn) {
		try {
			msisdn.isDisplayed();
			Reporter.log("MSISDN is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("MSISDN is not found in JOD Ending Soon Options Page");
		}
	}
	
	/**
	 * verify Device Name is displayed
	 * 
	 * @param Device Name
	 */
	public void verifyDeviceName(WebElement deviceName) {
		try {
			deviceName.isDisplayed();
			Reporter.log("Device Name is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Device Name is not found in JOD Ending Soon Options Page");
		}
	}
	
	/**
	 * verify Due Amount is displayed
	 * 
	 * @param Due Amount
	 */
	public void verifyDueAmount(WebElement dueAmount) {
		try {
			dueAmount.isDisplayed();
			Reporter.log("Due Amount is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Due Amount is not found in JOD Ending Soon Options Page");
		}
	}
	
	/**
	 * verify Monthly Payment is displayed
	 * 
	 * @param monthly Payment
	 */
	public void verifyMonthlyPaymentAmount(WebElement monthlyPaymentAmount) {
		try {
			monthlyPaymentAmount.isDisplayed();
			Reporter.log("Monthly Payment Amount is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Monthly Payment Amount is not found in JOD Ending Soon Options Page");
		}
	}

	/**
	 * @return the phoneImage
	 */
	public WebElement getPhoneImage() {
		return phoneImage;
	}

	/**
	 * @return the friendlyName
	 */
	public String getFriendlyName() {
		String name = null;
		try {
			name = friendlyName.getText();
		} catch (Exception e) {
			Assert.fail("Friendly Name is not found");
		}
		return name;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		String msisdnName = null;
		try {
			msisdnName = msisdn.getText();
		} catch (Exception e) {
			Assert.fail("msisdn is not found");
		}
		return msisdnName;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		String name = null;
		try {
			name = deviceName.getText();
		} catch (Exception e) {
			Assert.fail("Device Name is not found");
		}
		return name;
	}

	/**
	 * @return the dueAmount
	 */
	public String getDueAmount() {
		String amount = null;
		try {
			amount = dueAmount.getText();
		} catch (Exception e) {
			Assert.fail("Amount due is not found");
		}
		return amount;
	}

	/**
	 * @return the monthlyPaymentAmount
	 */
	public String getMonthlyPaymentAmount() {
		String amount = null;
		try {
			amount = monthlyPaymentAmount.getText();
		} catch (Exception e) {
			Assert.fail("Amount due is not found");
		}
		return amount;
	}

	/**
	 * Click on Setup Installation Plan CTA
	 */
	public void clickSetupInstallementPlan() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(setupInstallationPlanCTA));
			setupInstallationPlanCTA.click();
			Reporter.log("Setup Installation Plan CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Error: Setup Installation Plan CTA not found");
		}
	}
	
	/**
	 *
	 * Verify that the Full Balance text is loaded completely.
	 */
	public void verifyFullBalanceText() {
		try {
			fullBalanceLabel.isDisplayed();
			Assert.assertTrue("Full balance will be on your monthly bill".equalsIgnoreCase(fullBalanceLabel.getText()), "Error in validating Full Balance Label");
			Reporter.log("Full balance  text displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Full balance text is not as expected in JOD Ending Soon Options Page");
		}
		
	}
	
	/**
	 * verify Remaining Balance is displayed
	 * 
	 * @param monthly Payment
	 */
	public void verifyRemainingBalanceAmount(WebElement remainingBalance) {
		try {
			remainingBalance.isDisplayed();
			Reporter.log("Remaining Balance Amount is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Remaining Balance Amount is not found in JOD Ending Soon Options Page");
		}
	}
	
	/**
	 * Click on Find A Location Plan CTA
	 */
	public void clickFindALocationButton() {
		try {
			findALocationCTA.click();
			Reporter.log("Find A Location CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Error: Find A Location CTA not found");
		}
	}

	/**
	 * verify Option 1 Header
	 * @param header
	 */
	public void verifyOptionOneHeader(String headerText) {
		try {
			Assert.assertTrue(verifyHeader(headerText));
			Reporter.log("Verified Option 1 header");
		} catch (Exception e) {
			Assert.fail("Option 1 header is not found");
		}
	}

	/**
	 * verify Option 1 sub header
	 * @param subHeader
	 */
	public void verifyOptionOneSubHeader(String subHeaderText) {
		try {
			verifySubHeader(subHeaderText);
			Reporter.log("Verified Option 1 Sub Header");
		} catch (Exception e) {
			Assert.fail("Option 1 Sub header is not found");
		}
	}

	/**
	 * verify Option 2 Header
	 * @param header
	 */
	public void verifyOptionTwoHeader(String headerText) {
		try {
			Assert.assertTrue(verifyHeader(headerText));
			Reporter.log("Verified Option 2 Header");
		} catch (Exception e) {
			Assert.fail("Option 2 header is not found");
		}
	}

	/**
	 * verify Option 2 sub header
	 * @param subHeader
	 */
	public void verifyOptionTwoSubHeader(String subHeaderText) {
		try {
			verifySubHeader(subHeaderText);
			Reporter.log("Verified Option 2 Sub Header");
		} catch (Exception e) {
			Assert.fail("Option 2 Sub header is not found");
		}
	}

	/**
	 * verify Option 3 Header
	 * @param header
	 */
	public void verifyOptionThreeHeader(String headerText) {
		try {
			Assert.assertTrue(verifyHeader(headerText));
			Reporter.log("Verified Option 3 Header");
		} catch (Exception e) {
			Assert.fail("Option 3 header is not found");
		}
	}

	/**
	 * verify Option 3 sub header
	 * @param subHeader
	 */
	public void verifyOptionThreeSubHeader(String subHeaderText) {
		try {
			verifySubHeader(subHeaderText);
			Reporter.log("Verified Option 3 Sub Header");
		} catch (Exception e) {
			Assert.fail("Option 3 Sub header is not found");
		}
	}
	
	/**
	 * common method to verify headers
	 * @param headerText
	 */
	private boolean verifyHeader(String headerText) {
		for (WebElement header : optionHeaders) {
			if (header.isDisplayed() && header.getText().equals(headerText)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * common method to verify sub headers
	 * @param headerText
	 */
	private boolean verifySubHeader(String subHeaderText) {
		for (WebElement header : optionSubHeaders) {
			if (header.isDisplayed() && header.getText().equals(subHeaderText)) {
				return true;
			}
		}
		return false;
	}
}
