package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class CSLDeviceInformationApiV1 extends ApiCommonLib {

	/***
	 * Summary: get the CMS information (devices, (links, images etc. ).)
	 * Headers:Authorization,
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getCMSInfo(ApiTestData apiTestData, String taccode) throws Exception {
		Map<String, String> headers = buildDeviceInfoHeader(apiTestData);
		String resourceURL = "v1/devices/cmsinfo?taccode=" + taccode;
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.POST);
		return response;
	}

	/***
	 * Summary: get the CMS information (devices, (links, images etc. ).)
	 * Headers:Authorization,
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getManfacturerInfo(ApiTestData apiTestData, String taccode) throws Exception {
		Map<String, String> headers = buildDeviceInfoHeader(apiTestData);
		String resourceURL = "v1/devices/manfacturerinfo?taccode=" + taccode;
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.POST);
		return response;
	}

	private Map<String, String> buildDeviceInfoHeader(ApiTestData apiTestData) throws Exception {
		// final String dcpToken = createPlattoken(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		// headers.put("Authorization", dcpToken);
		headers.put("Content-Type", "application/json");
		return headers;
	}

}
