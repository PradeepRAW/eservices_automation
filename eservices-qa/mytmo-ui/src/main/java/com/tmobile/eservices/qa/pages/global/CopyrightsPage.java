package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class CopyrightsPage extends CommonPage {

	public static final String copyrightsPageUrl = "right";
	public static final String copyrightsPageLoadText = "Internet Services";

	@FindBy(xpath = "//h3[contains(text(),'Copyright')]")
	private WebElement copyrightsPageHeader;

	/**
	 * 
	 * @param webDriver
	 */
	public CopyrightsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Privacy Policy Page
	 * 
	 * @return
	 */
	public CopyrightsPage verifyCopyrightsPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(copyrightsPageUrl);
			Reporter.log("Copyrights page is displayed");
		} catch (Exception e) {
			Assert.fail("Copyrights page not displayed");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the Privacy Policy class instance.
	 */
	public CopyrightsPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(copyrightsPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Privacy Policy class instance.
	 */
	public CopyrightsPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(copyrightsPageUrl);
		return this;
	}

}