/**
 * 
 */
package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.List;

import org.apache.commons.lang3.text.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author ksrivani
 *
 */
public class RoamingPage extends TmngCommonPage {

	public RoamingPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	@FindBy(css="input[placeholder='Enter device name']")
	private WebElement enterDeviceName;	
	 
	@FindBy(css = "input[placeholder*='Enter cruise/ferry nam']")
	private WebElement enterCruiseHintBox;
	
	@FindBy(xpath = "//h3[contains(text(),'Awesome! Your device is compatible.')]")
	private WebElement headerTextDeviceCompatabilityDestination;
	
	@FindBy(xpath = "//h3[contains(text(),'Awesome! Your device is compatible.')]")
	private WebElement headerTextDeviceCompatabilityForCruise;
	
	@FindBy(xpath = "//h3[contains(text(),'Sorry, your device is not compatible.')]")
	private WebElement headerTextDeviceUnCompatability;
	
	private final String pageUrl = "/roaming";
	
	private final String loginPageUrl = "/account.t-mobile.com";
	
	@FindBy(xpath = "//h2[contains(text(),'Traveling internationally?')]")
	private WebElement travelingInternationallyHeader;
	
	@FindBy(css = "button[href='#deviceCountryResults']")
	private WebElement checkDestinationCompatabilityButton;
	
	@FindBy(css = "button[href='#deviceCruiseResults']")
	private WebElement checkCruiseCompatabilityButton;
	
	@FindBy(css = "input[placeholder='Enter destination']")
	private WebElement enterDestinationHintBox;
	
	@FindBy(xpath = "//input[@placeholder='Enter destination']/following::ul[1]/li")
	private List<WebElement> typeAheadListForDestination;
	
	@FindBy(css = "button[href='#roamingResults']")
	private WebElement checkRatesAndCoveragesButton;
	
	@FindBy(css =".result-data.result-data-desktop")
	private WebElement resultantDataTable;
	
	@FindBy(xpath="//th[@class='title']/p[contains(text(),'Data')]")
	private WebElement dataColumn;
	
	@FindBy(xpath="//th[@class='title']/p[contains(text(),'Text')]")
	private WebElement textColumn;
	
	@FindBy(xpath="//th[@class='title']/p[contains(text(),'Talk')]")
	private WebElement talkColumn;
	
	@FindBy(xpath="//tr[contains(@class,'table-row')]/th/p[text()='T-Mobile ONE and Simple Choice']")
	private WebElement TMobileOneSimpleChoiceHeader;
	
	@FindBy(xpath="//tr[contains(@class,'table-row')]/th/p[text()='Other T-Mobile plans']")
	private WebElement otherTMobilePlansHeader;
	
	@FindBy(xpath="//tr[contains(@class,'table-row')]/th/p[text()='All T-Mobile Plans']")
	private WebElement allTMobilePlansHeader;
	
	@FindBy(xpath = "//tr[@class='table-row']//td[@class='result-detail']")
	private List<WebElement> tmoOneAndSimpleChoiceRowData;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement dataRateTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td/div/p/span/span[contains(@ng-bind,'DataRate')]")
	private WebElement dataRateValueTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td[1]/p/span[contains(text(),'No Data')]")
	private WebElement emptyDataRateTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement textRateTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td/div/p/span/span[contains(@ng-bind,'TextRate')]")
	private WebElement textRateValueTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td[2]/p/span[contains(text(),'No Data')]")
	private WebElement emptyTextRateTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td[3]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement talkRateTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td/div/p/span/span[contains(@ng-bind,'VoiceRate')]")
	private WebElement talkRateValueTmoOneSimple;
	
	@FindBy(xpath="//p[contains(text(),'ONE and Simple Choice')]/../../td[3]/p/span[contains(text(),'No Data')]")
	private WebElement emptyTalkRateTmoOneSimple;
	
	@FindBy(xpath = "//tr[@class='table-row ng-scope']//td[@class='result-detail']")
	private List<WebElement> otherTMOPlansRowData;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement dataRateOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td/div/p/span/span[contains(@ng-bind,'DataRate')]")
	private WebElement dataRateValueOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td[1]/p/span[contains(text(),'No Data')]")
	private WebElement emptyDataRateOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement textRateOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td/div/p/span/span[contains(@ng-bind,'TextRate')]")
	private WebElement textRateValueOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td[2]/p/span[contains(text(),'No Data')]")
	private WebElement emptyTextRateOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td[3]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement talkRateOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td/div/p/span/span[contains(@ng-bind,'VoiceRate')]")
	private WebElement talkRateValueOtherTmo;
	
	@FindBy(xpath="//p[contains(text(),'Other T-Mobile plans')]/../../td[3]/p/span[contains(text(),'No Data')]")
	private WebElement emptyTalkRateOtherTmo;
	
	@FindBy(xpath = "//tr[@class='table-row']//td[@class='result-detail']")
	private List<WebElement> allTMobilePlansRowData;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement dataRateAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td/div/p/span/span[contains(@ng-bind,'DataRate')]")
	private WebElement dataRateValueAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td[1]/p/span[contains(text(),'No Data')]")
	private WebElement emptyDataRateAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement textRateAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td/div/p/span/span[contains(@ng-bind,'TextRate')]")
	private WebElement textRateValueAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td[2]/p/span/span[contains(text(),'No Data')]")
	private WebElement emptyTextRateAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td[3]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement talkRateAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td/div/p/span/span[contains(@ng-bind,'VoiceRate')]")
	private WebElement talkRateValueAllTmo;
	
	@FindBy(xpath="//p[contains(text(),'All T-Mobile Plans')]/../../td[3]/p/span/span[contains(text(),'No Data')]")
	private WebElement emptyTalkRateAllTmo;
		
	@FindBy(css = "section[id='roamingResults'] h2[class='ng-binding ng-scope']")
	private WebElement checkRatesAndCoverageMsgDestination;
	
	@FindBy(css = "section[id='cruiseResults'] h2[class='ng-binding ng-scope']")
	private WebElement checkRatesAndCoverageMsgCruise;
	
	@FindBy(css = "input[placeholder='Enter cruise/ferry name']")
	private WebElement enterCruiseOrFerryHintBox;
	
	@FindBy(xpath = "//input[@placeholder='Enter cruise/ferry name']/following::ul[1]/li")
	private List<WebElement> typeAheadListForCruise;
	
	@FindBy(xpath = "//input[@placeholder='Enter cruise/ferry name']/following::div[1]/button[text()='Check rates & coverage']")
	private WebElement checkRatesAndCoveragesButtonForCruise;
	
	@FindBy(css = "section[id='roamingResults'] h2[class='ng-scope']")
	private WebElement invalidDestinationMsg;
	
	@FindBy(css = "section[id='cruiseResults'] h2[class='ng-scope']")
	private WebElement invalidCruiseMsg;
	
	@FindBy(css=".result-data.result-data-desktop tr")
	private List<WebElement> resultantDataTableRows;	
	
	@FindBy(css=".result-data.result-data-desktop > tbody > tr:first-child > th")
	private List<WebElement> resultantDataTableCols;
	
	@FindBy(css="td > div > a.btn.btn-primary.tatCustShopBtn")
	private WebElement SeeMoreButton;
	
	@FindBy(css="div.description.ng-scope a[aria-label='Log in to My T-Mobile']")
	private WebElement loginToTMobileLink;
	
	@FindBy(xpath="//tr[contains(@class,'table-row')]/th/p[text()='Plans with Simple Global']")
	private WebElement plansWithSimpleGlobalHeader;
	
	@FindBy(xpath = "//p[contains(text(),'Plans with Simple Global')]/../../td")
	private List<WebElement> plansWithSimpleGlobalRowData;
	
	@FindBy(xpath = "//p[contains(text(),'Rates you can expect')]/../../td")
	private List<WebElement> plansWithRatesYouCanExpectRowData;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement dataRatePlansWithSimpleGlobal;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[1]/div/p/span/span")
	private WebElement dataRateValuePlansWithSimpleGlobal;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[1]/p/span[contains(text(),'No Data')]")
	private WebElement emptyDataRatePlansWithSimpleGlobal;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]")
	private WebElement textRatePlansWithSimpleGlobal;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[2]/div/p/span/span")
	private WebElement textRateValuePlansWithSimpleGlobal;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[2]/p/span[contains(text(),'No Data')]")
	private WebElement emptyTextRatePlansWithSimpleGlobal;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[3]/div/p/span/span")
	private WebElement talkRatePlansWithSimpleGlobal;
	
	@FindBy(xpath="//p[contains(text(),'Rates you can expect')]/../../td[1]/div/p/span/span")
	private WebElement dataRateValueRatesForCountry;
	
	@FindBy(xpath="//p[contains(text(),'Rates you can expect')]/../../td[1]/p/span")
	private WebElement emptyDataRateRatesForCountry;
	
	@FindBy(xpath="//p[contains(text(),'Rates you can expect')]/../../td[2]/div/p/span/span")
	private WebElement textRateValueRatesForCountry;
	
	@FindBy(xpath="//p[contains(text(),'Rates you can expect')]/../../td[2]/p/span")
	private WebElement emptyTextRateRatesForCountry;
	
	@FindBy(xpath="//p[contains(text(),'Rates you can expect')]/../../td[3]/div/p/span/span")
	private WebElement talkRateValueRatesForCountry;
	
	@FindBy(xpath="//p[contains(text(),'Rates you can expect')]/../../td[3]/p/span")
	private WebElement emptyTalkRateRatesForCountry;
	
	@FindBy(xpath="//p[contains(text(),'Plans with Simple Global')]/../../td[3]/p/span[contains(text(),'No Data')]")
	private WebElement emptyTalkRatePlansWithSimpleGlobal;
	
	private String unlimitedDataHeader = "Unlimited data in ";
	private String unlimitedTextHeader = "Unlimited texting in ";
	private String unlimitedDataAndTextHeader = "Unlimited data and texting in ";
	private String unlimitedEverythingHeader = "Unlimited everything in ";
	private String ratesForCountryHeader = "Rates for ";
	
	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public RoamingPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}
	
	/***
	 * Verify roaming page by 'traveling Internationally' header
	 * 
	 * @return
	 */
	public RoamingPage verifyRoamingPage() {
		checkPageIsReady();
		try {
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(travelingInternationallyHeader),60);
			Assert.assertTrue(travelingInternationallyHeader.isDisplayed());
			Reporter.log("Roaming Page is displayed");
		} catch (Exception e) {
			Assert.fail("Roaming Page is not displayed");
		}
		return this;
	}
	
	/***
	 * Verify and Enter Destination details in the text box
	 * 
	 * @param searchValue
	 */
	public RoamingPage verifyAndEnterDestinationDetails(String destinationName) {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(enterDestinationHintBox),60);
			Assert.assertTrue(enterDestinationHintBox.isDisplayed());
			sendTextData(enterDestinationHintBox, destinationName);
			Reporter.log("Entered : " + destinationName + " in the Destination text box");
		} catch (Exception e) {
			Assert.fail("Failed to enter value " + destinationName + " into the text box");
		}
		return this;
	}
	
	/***
	 * Verify user can see name of the country to which Alias belongs
	 * 
	 */
	public RoamingPage verifyDestinationNameForTheAliasNameEntered() {
		checkPageIsReady();
		try {
			typeAheadListForDestination.get(0).isDisplayed();
			Reporter.log("User can see name of the country '"+typeAheadListForDestination.get(0).getText()+"' to which Alias belongs");
		} catch (Exception e) {
			Assert.fail("failed to see the name of the country to which Alias belong");
		}
		return this;
	}
	
	/***
	 * Verify Type ahead list is displayed of Destination details
	 * 
	 */
	public RoamingPage verifyTypeAheadListOfDestinationDetails() {
		checkPageIsReady();
		try {
			typeAheadListForDestination.get(0).click();
			Reporter.log("Selected the value from the type ahead list");
		} catch (Exception e) {
			Assert.fail("failed to select value from the type ahead list");
		}
		return this;
	}
	
	/***
	 * verify and click 'Check Rates & Coverage' Button
	 * 
	 * @return
	 */
	public RoamingPage verifyAndClickCheckRatesAndCoveragesButton() {
		checkPageIsReady();
		try {
			Assert.assertTrue(checkRatesAndCoveragesButton.isDisplayed());
			clickElement(checkRatesAndCoveragesButton);
			Reporter.log("Clicked on 'Check Rates & Coverage' CTA successfully");
		} catch (Exception e) {
			Assert.fail("Unable to click on 'Check Rates & Coverage' CTA");
		}
		return this;
	}
	
	/**
	 * verify entered destination name in the header
	 * 
	 * @return
	 */
	public RoamingPage verifyDestinationNameInHeader(String destinationName) {
		checkPageIsReady();
		try {
			String actualDestination = checkRatesAndCoverageMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(destinationName);
			Assert.assertTrue(actualDestination.contains(expectedDestination),"Destination name is not displayed in the header");{
			Reporter.log("Destination name is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the entered destination name in the header");
		}
		return this;
	}
	
	/**
	 * verify selected destination name in the header
	 * 
	 * @return
	 */
	public RoamingPage verifySelectedDestinationNameInHeader() {
		checkPageIsReady();
		try {
			String actualDestination = checkRatesAndCoverageMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(typeAheadListForDestination.get(0).getText());
			Assert.assertTrue(actualDestination.contains(expectedDestination),"Destination name is not displayed in the header");{
			Reporter.log("Destination name is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the selected destination name in the header");
		}
		return this;
	}
	
	/***
	 * verify Resultant table rows and columns after clicking on 'Check Rates & Coverage' Button
	 * 
	 * @return
	 */
	public RoamingPage verifyResultantTableRowsandColumns() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(resultantDataTable),60);
			Assert.assertTrue(dataColumn.isDisplayed());
			Assert.assertTrue(textColumn.isDisplayed());
			Assert.assertTrue(talkColumn.isDisplayed());
			Reporter.log("Data, text  and talk columns are displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display the Data, text and talk columns");
		}
		return this;
	}
	
	/***
	 * verify 'T-Mobile ONE and Simple Choice' Header including its data
	 * 
	 * @return
	 */
	public RoamingPage verifyTMobileONEandSimpleChoicePlans() {
		checkPageIsReady();
		try {
			if (TMobileOneSimpleChoiceHeader.isDisplayed()) {
				Reporter.log("'T-Mobile ONE and Simple Choice' Header is displayed");
				verifyDataDisplayedForTMOOneAndSimpleChoice();
			} else {
				Reporter.log("'T-Mobile ONE and Simple Choice' plans are not displayed for this destination");
			}
		} catch (Exception e) {
			Assert.fail("'T-Mobile ONE and Simple Choice' Header is not displayed");
		}
		return this;
	}
	
	/***
	 * verify 'Other T-Mobile plans' header including its data
	 * 
	 * @return
	 */
	public RoamingPage verifyOtherTMobilePlans() {
		checkPageIsReady();
		try {
			if (otherTMobilePlansHeader.isDisplayed()) {
				Reporter.log("'Other T-Mobile plans' Header is displayed");
				verifyDataDisplayedForOtherTMOPlans();
				} else {
				Reporter.log("'Other T-Mobile plans' are not displayed for this destination");
			}
		} catch (Exception e) {
			Assert.fail("'Other T-Mobile plans' are not displayed for this destination");
		}
		return this;
	}
	
	/***
	 * verify 'All T-Mobile plans' Header including its data
	 * 
	 * @return
	 */
	public RoamingPage verifyAllTMobilePlans() {
		checkPageIsReady();
		try {
			if (allTMobilePlansHeader.isDisplayed()) {
				Reporter.log("'All T-Mobile plans' Header is displayed");
				verifyDataDisplayedForAllTMobilePlans();
			} else {
				Reporter.log("'All T-Mobile plans' are not displayed for this destination");
			}
		} catch (Exception e) {
			Assert.fail("'All T-Mobile plans' Header is not displayed");
		}
		return this;
	}
	
	/***
	 * verify the data displayed for TMO One and Simple choice
	 * 
	 * @return
	 */
	public RoamingPage verifyDataDisplayedForTMOOneAndSimpleChoice() {
		checkPageIsReady();
		try {
			for (WebElement ele : tmoOneAndSimpleChoiceRowData) {
				Assert.assertTrue(ele.isDisplayed());
			}
			Reporter.log("Data is displayed successfully for TMO One and Simple choice");
			String dataValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td/div/p/span/span[contains(@ng-bind,'DataRate')]";
			String dataNoValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayDataValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td[1]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(dataValue)).size() != 0){
				Assert.assertTrue(dataRateValueTmoOneSimple.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Data column for TMO One and Simple choice");
			}else if(getDriver().findElements(By.xpath(dataNoValue)).size() != 0){
				Assert.assertTrue(dataRateTmoOneSimple.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Data column for TMO One and Simple choice plan, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayDataValue)).size() != 0){
				Assert.assertTrue(emptyDataRateTmoOneSimple.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Data column for TMO One and Simple choice plan, as we received Data column with Empty array values");
			}
			String textValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td/div/p/span/span[contains(@ng-bind,'TextRate')]";
			String textNoValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayTextValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td[2]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(textValue)).size() != 0){
				Assert.assertTrue(textRateValueTmoOneSimple.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Text column for TMO One and Simple choice");
			}else if(getDriver().findElements(By.xpath(textNoValue)).size() != 0){
				Assert.assertTrue(textRateTmoOneSimple.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Text column for TMO One and Simple choice plan, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTextValue)).size() != 0){
				Assert.assertTrue(emptyTextRateTmoOneSimple.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Text column for TMO One and Simple choice plan, as we received Text column with Empty array values");
			}
			String talkValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td/div/p/span/span[contains(@ng-bind,'VoiceRate')]";
			String talkNoValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td[3]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayTalkValue = "//p[contains(text(),'ONE and Simple Choice')]/../../td[3]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(talkValue)).size() != 0){
				Assert.assertTrue(talkRateValueTmoOneSimple.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Talk column for TMO One and Simple choice");
			}else if(getDriver().findElements(By.xpath(talkNoValue)).size() != 0){
				Assert.assertTrue(talkRateTmoOneSimple.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Talk column for TMO One and Simple choice plan, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTalkValue)).size() != 0){
				Assert.assertTrue(emptyTalkRateTmoOneSimple.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Talk column for TMO One and Simple choice plan, as we received Talk column with Empty array values");
			}
			}catch (Exception e) {
			Assert.fail("Unable to display the data in the columns for TMO One and Simple choice");
		}
		return this;
	}
	
	/***
	 * verify the data displayed for Other TMO plans
	 * 
	 * @return
	 */
	public RoamingPage verifyDataDisplayedForOtherTMOPlans() {
		checkPageIsReady();
		try {
			for (WebElement ele : otherTMOPlansRowData) {
				Assert.assertTrue(ele.isDisplayed());
			}
			Reporter.log("Data is displayed successfully for Other TMO plans");
			String dataValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td/div/p/span/span[contains(@ng-bind,'DataRate')]";
			String dataNoValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayDataValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td[1]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(dataValue)).size() != 0){
				Assert.assertTrue(dataRateValueOtherTmo.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Data column for Other TMO plans");
			}else if(getDriver().findElements(By.xpath(dataNoValue)).size() != 0){
				Assert.assertTrue(dataRateOtherTmo.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Data column for Other TMO plans, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayDataValue)).size() != 0){
				Assert.assertTrue(emptyDataRateOtherTmo.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Data column for Other TMO plans, as we received Data column with Empty array values");
			}
			String textValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td/div/p/span/span[contains(@ng-bind,'TextRate')]";
			String textNoValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayTextValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td[2]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(textValue)).size() != 0){
				Assert.assertTrue(textRateValueOtherTmo.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Text column for Other TMO plans");
			}else if(getDriver().findElements(By.xpath(textNoValue)).size() != 0){
				Assert.assertTrue(textRateOtherTmo.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Text column for Other TMO plans, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTextValue)).size() != 0){
				Assert.assertTrue(emptyTextRateOtherTmo.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Text column for Other TMO plans, as we received Text column with Empty array values");
			}
			String talkValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td/div/p/span/span[contains(@ng-bind,'VoiceRate')]";
			String talkNoValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td[3]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayTalkValue = "//p[contains(text(),'Other T-Mobile plans')]/../../td[3]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(talkValue)).size() != 0){
				Assert.assertTrue(talkRateValueOtherTmo.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Talk column for Other TMO plans");
			}else if(getDriver().findElements(By.xpath(talkNoValue)).size() != 0){
				Assert.assertTrue(talkRateOtherTmo.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Talk column for Other TMO plans, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTalkValue)).size() != 0){
				Assert.assertTrue(emptyTalkRateOtherTmo.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Talk column for Other TMO plans, as we received Talk column with Empty array values");
			}
			}catch (Exception e) {
				Assert.fail("Unable to display the data in the columns for Other TMO plans");
		}
		return this;
	}
			
	/***
	 * verify the data displayed for All T-Mobile plans
	 * 
	 * @return
	 */
	public RoamingPage verifyDataDisplayedForAllTMobilePlans() {
		checkPageIsReady();
		try {
			for (WebElement ele : allTMobilePlansRowData) {
				Assert.assertTrue(ele.isDisplayed());
			}
			Reporter.log("Data is displayed successfully for All T-Mobile plans");
			String dataValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td/div/p/span/span[contains(@ng-bind,'DataRate')]";
			String dataNoValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayDataValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td[1]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(dataValue)).size() != 0){
				Assert.assertTrue(dataRateValueAllTmo.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Data column for All T-Mobile plans");
				}else if(getDriver().findElements(By.xpath(dataNoValue)).size() != 0){
				Assert.assertTrue(dataRateAllTmo.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Data column for All T-Mobile plans, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayDataValue)).size() != 0){
				Assert.assertTrue(emptyDataRateAllTmo.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Data column for All T-Mobile plans, as we received Data column with Empty array values");
			}
			String textValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td/div/p/span/span[contains(@ng-bind,'TextRate')]";
			String textNoValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayTextValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td[2]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(textValue)).size() != 0){
				Assert.assertTrue(textRateValueAllTmo.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Text column for All T-Mobile plans");
			}else if(getDriver().findElements(By.xpath(textNoValue)).size() != 0){
				Assert.assertTrue(textRateAllTmo.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Text column for All T-Mobile plans, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTextValue)).size() != 0){
				Assert.assertTrue(emptyTextRateAllTmo.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Text column for All T-Mobile plans, as we received Text column with Empty array values");
			}
			String talkValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td/div/p/span/span[contains(@ng-bind,'VoiceRate')]";
			String talkNoValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td[3]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayTalkValue = "//p[contains(text(),'All T-Mobile Plans')]/../../td[3]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(talkValue)).size() != 0){
				Assert.assertTrue(talkRateValueAllTmo.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Talk column for All T-Mobile plans");
			}else if(getDriver().findElements(By.xpath(talkNoValue)).size() != 0){
				Assert.assertTrue(talkRateAllTmo.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Talk column for All T-Mobile plans, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTalkValue)).size() != 0){
				Assert.assertTrue(emptyTalkRateAllTmo.getText().contains("No Data"));
				Reporter.log("'Unlimited' is shown in Talk column for All T-Mobile plans, as we received Talk column with Empty array values");
			}
			}catch (Exception e) {
				Assert.fail("Unable to display the data in the columns for All T-Mobile plans");
		}
		return this;
	}
			
	/***
	 * verify 'Sorry, we don’t have coverage there yet.' message is displayed for invalid destination
	 * 
	 * @return
	 */
	public RoamingPage verifyMsgForInvalidDestination() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(invalidDestinationMsg),60);
			Assert.assertTrue(invalidDestinationMsg.isDisplayed());
			Reporter.log(
					"'Sorry, we don't have coverage there yet.' message is displayed, as the entered destination is invalid");
		} catch (Exception e) {
			Assert.fail("'Sorry, we don't have coverage there yet.' message is not displayed");
		}
		return this;
	}
	
	/***
	 * verify 'Sorry, we don’t have coverage there yet.' message is displayed for invalid cruise
	 * 
	 * @return
	 */
	public RoamingPage verifyMsgForInvalidCruise() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(invalidCruiseMsg),60);
			Assert.assertTrue(invalidCruiseMsg.isDisplayed());
			Reporter.log(
					"'Sorry, we don't have coverage there yet.' message is displayed, as the entered cruise is invalid");
		} catch (Exception e) {
			Assert.fail("'Sorry, we don't have coverage there yet.' message is not displayed");
		}
		return this;
	}
	
	/***
	 * verify and click on 'See more' Button
	 * 
	 * @return
	 */
	public RoamingPage verifyAndClickSeeMoreCTA() {
		checkPageIsReady();
		try {
			Assert.assertTrue(SeeMoreButton.isDisplayed());
			clickElement(SeeMoreButton);
			Reporter.log("Clicked on 'See More' CTA");
		} catch (Exception e) {
			Assert.fail("Unable to click on 'See more' CTA");
		}
		return this;
	}
		
	/***
	 * verify 'login To T-Mobile' link is displayed
	 * 
	 * @return
	 */
	public RoamingPage verifyAndClickOnLoginToTMobileLink() {
		checkPageIsReady();
		try {
			Assert.assertTrue(loginToTMobileLink.isDisplayed());
			loginToTMobileLink.click();
			Reporter.log("Clicked on Login to My T-Mobile link successfully");
			} catch (Exception e) {
			Assert.fail("Unable to click on 'Login To My T-Mobile' Link");
		}
		return this;
	}
	
	/**
	 * verify myTMO login page display
	 * 
	 * @return
	 */
	public RoamingPage verifyMyTMOLoginPage() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains(loginPageUrl),60);
			Reporter.log("MYTMO login page is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to load MyTMO Login page");
		}
		return this;
	}
	
	/***
	 * Verify Enter Cruise details
	 * 
	 */
	public RoamingPage verifyAndEnterCruiseOrFerryDetails(String cruiseOrFerryName) {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(enterCruiseOrFerryHintBox),60);
			Assert.assertTrue(enterCruiseOrFerryHintBox.isDisplayed());
			sendTextData(enterCruiseOrFerryHintBox, cruiseOrFerryName);
			Reporter.log("Entered : " + cruiseOrFerryName + " in the Cruise/Ferry hint box");
			} catch (Exception e) {
			Assert.fail("failed to enter value " + cruiseOrFerryName + " into the search text box");
		}
		return this;
	}
	
	/***
	 * Verify Type ahead list is displayed of Cruise details
	 * 
	 * @param searchValue
	 */
	public RoamingPage verifyTypeAheadListOfCruiseOrFerryDetails() {
		checkPageIsReady();
		try {
			typeAheadListForCruise.get(0).click();
			Reporter.log("Selected the value from the type ahead list");
		} catch (Exception e) {
			Assert.fail("failed to select value from the type ahead list");
		}
		return this;
	}
	
	/***
	 * verify and click 'Check Rates & Coverage' Button for Cruise
	 * 
	 * @return
	 */
	public RoamingPage verifyAndClickCheckRatesAndCoveragesButtonForCruise() {
		checkPageIsReady();
		try {
			Assert.assertTrue(checkRatesAndCoveragesButtonForCruise.isDisplayed());
			clickElement(checkRatesAndCoveragesButtonForCruise);
			Reporter.log("Clicked on 'Check Rates & Coverage' CTA for Cruise successfully");
		} catch (Exception e) {
			Assert.fail("Unable to click on 'Check Rates & Coverage' CTA for Cruise");
		}
		return this;
	}
	
	/**
	 * verify entered cruise name in the header
	 * 
	 * @return
	 */
	public RoamingPage verifyCruiseNameInHeader(String CruiseName) {
		checkPageIsReady();
		try {
			String actualCruise = checkRatesAndCoverageMsgCruise.getText();
			String expectedCruise = WordUtils.capitalize(CruiseName);
			Assert.assertTrue(actualCruise.contains(expectedCruise),"Cruise name is not displayed in the header");
			Reporter.log("Cruise name is displayed in the header");
			} catch (Exception e) {
			Assert.fail("Unable to display the entered cruise name in the header");
		}
		return this;
	}
	
	/***
	 * verify and click 'Check Rates & Coverage' Button
	 * 
	 * @return
	 */
	public RoamingPage verifyAndClickDestinationCheckCompatabilityButton() {
		checkPageIsReady();
		try {
			Assert.assertTrue(checkDestinationCompatabilityButton.isDisplayed());
			clickElement(checkDestinationCompatabilityButton);
			Reporter.log("Clicked on Check Compatibility CTA successfully");
		} catch (Exception e) {
			Assert.fail("Unable to click on 'check compatability'");
		}
		return this;
	}
	
	/***
	 * verify and click 'Check Rates & Coverage' Button for cruise 
	 * 
	 * @return
	 */
	public RoamingPage verifyAndClickCruiseCheckCompatabilityButton() {
		checkPageIsReady();
		try {
			Assert.assertTrue(checkCruiseCompatabilityButton.isDisplayed());
			clickElement(checkCruiseCompatabilityButton);
			Reporter.log("Clicked on Check Compatibility CTA successfully");
		} catch (Exception e) {
			Assert.fail("Unable to click on 'check compatability'");
		}
		return this;
	}
	/***
	 * Enter device name
	 * 
	 * @param searchValue
	 */
	public RoamingPage verifyAndEnterDeviceName(String deviceName) {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(enterDeviceName),60);
			Assert.assertTrue(enterDeviceName.isDisplayed());
			sendTextData(enterDeviceName, deviceName);
			enterDeviceName.sendKeys(Keys.ENTER);
			Reporter.log("Entered : " + deviceName + " in the Destination hint box");
			} catch (Exception e) {
			Assert.fail("failed to enter value " + deviceName + " into the search text box");
		}
		return this;
	}
	
	/**
	 * verify header text for device compatability 
	 * @return
	 */
	public RoamingPage verifyHeaderTextDeviceCompatabilityForDestination() {
		checkPageIsReady();
		try {
			Assert.assertTrue(headerTextDeviceCompatabilityDestination.isDisplayed());
			Reporter.log("Header text for device compatibility successfully verified");
		} catch (Exception e) {
				Assert.fail("Unable to validate header text device compatability for destination");
		}
		return this;
	}
	
	/**
	 * verify header text for device compatability 
	 * @return
	 */
	public RoamingPage verifyHeaderTextDeviceCompatabilityForCruise() {
		checkPageIsReady();
		try {
			Assert.assertTrue(headerTextDeviceCompatabilityForCruise.isDisplayed());
			Reporter.log("Header text for device compatibility successfully verified");
		} catch (Exception e) {
				Assert.fail("Unable to validate header text device compatability for destination");
		}
		return this;
	}
	
	/**
	 * verify header text for device compatability 
	 * @return
	 */
	public RoamingPage verifyHeaderTextDeviceUnCompatabilityMessage() {
		checkPageIsReady();
		try {
			Assert.assertTrue(headerTextDeviceUnCompatability.isDisplayed());
			Reporter.log("Header text for wrong device compatibility successfully verified");
		} catch (Exception e) {
				Assert.fail("Unable to validate header text device Uncompatability for destination");
		}
		return this;
	}
	/***
	 * Verify post Destination details in the text box
	 * 
	 * @param searchValue
	 */
	public RoamingPage verifyPostDestinationDetails(String destinationName) {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(enterDestinationHintBox),60);
			verifyExactContentOfElementBytext(typeAheadListForDestination, destinationName);
			Reporter.log("Searched Destination : " + destinationName + " is displayed in the Destination text box");
		} catch (Exception e) {
			Assert.fail("Searched Destination : " + destinationName + " is not displayed in the Destination text box");
		}
		return this;
	}
	
	/***
	 * verify 'Plans with Simple Global' Header including its data
	 * 
	 * @return
	 */
	public RoamingPage verifyPlansWithSimpleGlobal() {
		checkPageIsReady();
		try {
			if (plansWithSimpleGlobalHeader.isDisplayed()) {
				Reporter.log("'Plans with Simple Global' Header is displayed");
				verifyDataDisplayedForTMOOneAndSimpleChoice();
			} else {
				Reporter.log("'Plans with Simple Global' plans are not displayed for this destination");
			}
		} catch (Exception e) {
			Assert.fail("'Plans with Simple Global' Header is not displayed");
		}
		return this;
	}
	
	/***
	 * verify the data displayed for Plans With Simple Global
	 * 
	 * @return
	 */
	public RoamingPage verifyDataDisplayedForPlansWithSimpleGlobal() {
		checkPageIsReady();
		try {
			for (WebElement ele : plansWithSimpleGlobalRowData) {
				Assert.assertTrue(ele.isDisplayed());
			}
			Reporter.log("Data is displayed successfully for Plans with Simple Global");
			String dataValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[1]/div/p/span/span";
			String dataNoValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[1]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayDataValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[1]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(dataValue)).size() != 0){
				Assert.assertTrue(dataRateValuePlansWithSimpleGlobal.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Data column for Plans with Simple Global");
			}else if(getDriver().findElements(By.xpath(dataNoValue)).size() != 0){
				Assert.assertTrue(dataRatePlansWithSimpleGlobal.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Data column for Plans with Simple Global, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayDataValue)).size() != 0){
				Assert.assertTrue(emptyDataRatePlansWithSimpleGlobal.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Data column for Plans with Simple Global, as we received Data column with Empty array values");
			}
			String textValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[2]/div/p/span/span";
			String textNoValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[2]/div/p/span[contains(text(),'Unlimited')]";
			String emptyArrayTextValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[2]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(textValue)).size() != 0){
				Assert.assertTrue(textRateValuePlansWithSimpleGlobal.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Text column for Plans with Simple Global");
			}else if(getDriver().findElements(By.xpath(textNoValue)).size() != 0){
				Assert.assertTrue(textRatePlansWithSimpleGlobal.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Text column for Plans with Simple Global, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTextValue)).size() != 0){
				Assert.assertTrue(emptyTextRatePlansWithSimpleGlobal.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Text column for Plans with Simple Global, as we received Text column with Empty array values");
			}
			String talkValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[3]/div/p/span/span";
			//String talkNoValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[3]/div/p/span/span[contains(text(),'Unlimited')]";
			String emptyArrayTalkValue = "//p[contains(text(),'Plans with Simple Global')]/../../td[3]/p/span[contains(text(),'No Data')]";
	
			//System.out.println("xyz "+talkRatePlansWithSimpleGlobal.getText());
			if(getDriver().findElements(By.xpath(talkValue)).size() != 0 && talkRatePlansWithSimpleGlobal.getText().contains("Unlimited"))
			{
				//Assert.assertTrue(talkRateValuePlansWithSimpleGlobal.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Talk column for Plans with Simple Global");
			}else if(getDriver().findElements(By.xpath(talkValue)).size() != 0 && talkRatePlansWithSimpleGlobal.getText().matches(".*\\d.*"))
				{
				//Assert.assertTrue(talkRatePlansWithSimpleGlobal.getText().contains("Unlimited"));
				Reporter.log("'Unlimited' is shown in Talk column for Plans with Simple Global, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTalkValue)).size() != 0){
				Assert.assertTrue(emptyTalkRatePlansWithSimpleGlobal.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Talk column for Plans with Simple Global, as we received Talk column with Empty array values");
			}
			}catch (Exception e) {
			Assert.fail("Unable to display the data in the columns for Plans with Simple Global");
		}
		return this;
	}
	
	/***
	 * verify the data displayed for Plans With Simple Global with All Column values
	 * 
	 * @return
	 */
	public RoamingPage verifyDataDisplayedForPlansWithAllDataTextTalkValues() {
		checkPageIsReady();
		try {
			for (WebElement ele : plansWithRatesYouCanExpectRowData) {
				Assert.assertTrue(ele.isDisplayed());
			}
			Reporter.log("Data is displayed successfully for Rates you can expect Plans");
			String dataValue = "//p[contains(text(),'Rates you can expect')]/../../td[1]/div/p/span/span";
			String emptyArrayDataValue = "//p[contains(text(),'Rates you can expect')]/../../td[1]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(dataValue)).size() != 0){
				Assert.assertTrue(dataRateValueRatesForCountry.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Data column while displaying data for all columns");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayDataValue)).size() != 0){
				Assert.assertTrue(emptyDataRateRatesForCountry.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Data column while displaying Rates for country");
			} 
			String textValue = "//p[contains(text(),'Rates you can expect')]/../../td[2]/div/p/span/span";
			String emptyArrayTextValue = "//p[contains(text(),'Rates you can expect')]/../../td[2]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(textValue)).size() != 0){
				Assert.assertTrue(textRateValueRatesForCountry.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Text column while displaying data for all columns");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTextValue)).size() != 0){
				Assert.assertTrue(emptyTextRateRatesForCountry.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Text column while displaying Rates for country");
			}
			String talkValue = "//p[contains(text(),'Rates you can expect')]/../../td[3]/div/p/span/span";
			String emptyArrayTalkValue = "//p[contains(text(),'Rates you can expect')]/../../td[3]/p/span[contains(text(),'No Data')]";
			if(getDriver().findElements(By.xpath(talkValue)).size() != 0){
				Assert.assertTrue(talkRateValueRatesForCountry.getText().matches(".*\\d.*"));
				Reporter.log("Data is displayed successfully in Talk column while displaying Rates for country");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTalkValue)).size() != 0){
				Assert.assertTrue(emptyTalkRateRatesForCountry.getText().contains("No Data"));
				Reporter.log("'No Data' is shown in Talk column while displaying data for all columns");
			}
			}catch (Exception e) {
			Assert.fail("Unable to display the data in the columns while displaying Rates for country with all the column values");
		}
		return this;
	}
	
	/**
	 * verify header for Unlimited Data
	 * 
	 * @return
	 */
	public RoamingPage verifyHeaderForUnlimitedData(String destinationName) {
		checkPageIsReady();
		try {
			
			String actualDestination = checkRatesAndCoverageMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(destinationName);
			Assert.assertTrue(actualDestination.contains(unlimitedDataHeader+expectedDestination),"Header is not displayed for Unlimited data");{
			Reporter.log("'Unlimited data in "+expectedDestination+"' is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the header for Unlimited Data");
		}
		return this;
	}
	
	/**
	 * verify header for Unlimited Text
	 * 
	 * @return
	 */
	public RoamingPage verifyHeaderForUnlimitedText(String destinationName) {
		checkPageIsReady();
		try {
			
			String actualDestination = checkRatesAndCoverageMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(destinationName);
			Assert.assertTrue(actualDestination.contains(unlimitedTextHeader+expectedDestination),"Header is not displayed for Unlimited text");{
			Reporter.log("'Unlimited texting in "+expectedDestination+"' is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the header for Unlimited text");
		}
		return this;
	}
	
	/**
	 * verify header for Unlimited Data and Text
	 * 
	 * @return
	 */
	public RoamingPage verifyHeaderForUnlimitedDataAndText(String destinationName) {
		checkPageIsReady();
		try {
			
			String actualDestination = checkRatesAndCoverageMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(destinationName);
			Assert.assertTrue(actualDestination.contains(unlimitedDataAndTextHeader+expectedDestination),"Header is not displayed for Unlimited data and text");{
			Reporter.log("'Unlimited data and texting in "+expectedDestination+"' is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the header for Unlimited data and text");
		}
		return this;
	}
	
	/**
	 * verify header for Unlimited everything
	 * 
	 * @return
	 */
	public RoamingPage verifyHeaderForUnlimitedEverything(String destinationName) {
		checkPageIsReady();
		try {
			
			String actualDestination = checkRatesAndCoverageMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(destinationName);
			Assert.assertTrue(actualDestination.contains(unlimitedEverythingHeader+expectedDestination),"Header is not displayed for Unlimited everything");{
			Reporter.log("'Unlimited everything in "+expectedDestination+"' is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the header for Unlimited everything");
		}
		return this;
	}
	
	/**
	 * verify header for Rates for Country
	 * 
	 * @return
	 */
	public RoamingPage verifyHeaderForRatesForCountry(String destinationName) {
		checkPageIsReady();
		try {
			
			String actualDestination = checkRatesAndCoverageMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(destinationName);
			Assert.assertTrue(actualDestination.contains(ratesForCountryHeader+expectedDestination),"Header is not displayed for Unlimited everything");{
			Reporter.log("'Rates for "+expectedDestination+"' is displayed successfully in header while displaying the Rates for all the columns");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the header for Rates for country");
		}
		return this;
	}

}