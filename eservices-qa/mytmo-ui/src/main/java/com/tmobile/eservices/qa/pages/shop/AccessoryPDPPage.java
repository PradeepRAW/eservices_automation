package com.tmobile.eservices.qa.pages.shop;

import java.text.DecimalFormat;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class AccessoryPDPPage extends CommonPage {

	private static final Logger logger = LoggerFactory.getLogger(AccessoryPDPPage.class);

	@FindBy(css = "accessoryDeviceImage")
	private WebElement accessoryDeviceImage;

	@FindBy(css = "p[ng-bind*='specifications']")
	private WebElement specificationsHeader;

	@FindBy(xpath = "//p[text()='Warranty']")
	private WebElement warrantyHeader;

	@FindBy(css = "warrantyText")
	private WebElement warrantyText;

	@FindBy(css = "weightHeader")
	private WebElement weightHeader;

	@FindBy(css = "weightText")
	private WebElement weightText;

	@FindBy(css = "dimensionsHeader")
	private WebElement dimensionsHeader;

	@FindBy(css = "dimensionsHeader")
	private WebElement dimensionsText;

	@FindBy(css = "inTheBoxHeader")
	private WebElement inTheBoxHeader;

	@FindBy(css = "inTheBoxText")
	private WebElement inTheBoxText;

	@FindBy(css = "p[ng-bind*='leaglTextaboveCTA']")
	private WebElement legalText;

	@FindBy(xpath = "//p[text()='Overview']")
	private WebElement overViewHeader;

	@FindBy(css = "ul.lh-19-xs")
	private WebElement overViewDetailsText;

	@FindBy(css = "i.backArrowLarge")
	private WebElement clickBackArrow;

	@FindBy(css = "button.PrimaryCTA-Normal-accessories-pdp")
	private WebElement addToOrderButton;

	@FindBy(css = "img#header-size")
	private WebElement bigTIcon;

	@FindBy(css = "i#header-size.fa")
	private WebElement phoneIcon;

	@FindBy(css = "instantDiscountSection")
	private WebElement instantDiscountSection;

	@FindBy(css = "div.eipprice")
	private WebElement eipDivison;

	@FindBy(css = "div.totalprice")
	private WebElement frpDivison;

	@FindBy(css = "p[ng-if*='isEMIAvailable']>span.fine-print-body-mos")
	private WebElement eipInstallmentMonths;

	@FindBy(css = "span#todayoffer")
	private WebElement eipMonthlyPrice;

	@FindBy(css = "div[class*='marginTopImagePDP'] p[class*='text-color-gray ng-binding ng-scope']")
	private WebElement eipDownPayment;

	@FindBy(css = "#style .price-month")
	private WebElement frpAfterPromo;

	@FindBy(css = "a[ng-click*='ctrl.showPromoModal()']")
	private WebElement catalogPromoValue;

	@FindBy(css = "p[ng-bind*='modelName']")
	private WebElement accessoryName;

	public AccessoryPDPPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Verify Accessory ProductDetails Page.
	 *
	 * @return the ShopPage class instance.
	 */
	public AccessoryPDPPage verifyAccessoryProductDetailsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("Accessory product details page is displayed");
		} catch (NoSuchElementException e) {
			e.addInfo("Page:" + getCurrentWindowTitle(), "accessory Details Page not loaded");
			logger.info("accessory details page url not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AccessoryPDPPage verifyPageUrl() {
		try {
			getDriver().getCurrentUrl().contains("accessorydetail");
		} catch (Exception e) {
			Assert.fail("Accessory details page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Accessory Specifications
	 */
	public AccessoryPDPPage verifyAccessorySpecificationComponent() {
		try {
			waitforSpinner();
			Assert.assertNotNull(specificationsHeader.isDisplayed(), "Accessory Specification header is not displayed");
			Reporter.log("Accessory Specification header is displayed");
			Assert.assertNotNull(warrantyHeader.isDisplayed(), "Accessory Warranty header is not displayed");
			Reporter.log("Accessory Warranty header is displayed");
			Assert.assertNotNull(warrantyText.isDisplayed(), "Accessory Warranty text is not displayed");
			Reporter.log("Accessory Warranty text is displayed");
			Assert.assertNotNull(weightHeader.isDisplayed(), "Weight header is not displayed");
			Reporter.log("Weight header is displayed");
			Assert.assertNotNull(weightText.isDisplayed(), "Weight text is not displayed");
			Reporter.log("Weight text is displayed");
			Assert.assertNotNull(dimensionsHeader.isDisplayed(), "Dimensions header is not displayed");
			Reporter.log("Dimensions header is displayed");
			Assert.assertNotNull(dimensionsText.isDisplayed(), "Dimensions text is not displayed");
			Reporter.log("Dimensions text is displayed");
			Assert.assertNotNull(inTheBoxHeader.isDisplayed(), "In the box header is not displayed");
			Reporter.log("In the box header is displayed");
			Assert.assertNotNull(inTheBoxText.isDisplayed(), "In the box text is not displayed");
			Reporter.log("In the box text is displayed");
			specificationsHeader.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Accessory Specifications not displayed");
		}
		return this;
	}

	/**
	 * Verify Warranty Header
	 */
	public AccessoryPDPPage verifyWarrantyHeader() {
		try {
			waitforSpinner();
			warrantyHeader.isDisplayed();
			Reporter.log("Warranty header is diplayed");
		} catch (Exception e) {
			Assert.fail("Warranty header is not displayed");
		}

		return this;
	}

	/**
	 * Verify Legal Text
	 */
	public AccessoryPDPPage verifyLegalText() {
		try {
			waitforSpinner();
			legalText.isDisplayed();
			Reporter.log("Accessory Legal Text is displayed");
		} catch (Exception e) {
			Assert.fail("Accessory Legal Text is not displayed");
		}
		return this;
	}

	/**
	 * Verify OverView Section
	 */
	public AccessoryPDPPage verifyOverViewSection() {
		try {
			waitforSpinner();
			overViewHeader.isDisplayed();
			Reporter.log("Accessory Over View Header is displayed");
			overViewDetailsText.isDisplayed();
			Reporter.log("Accessory Over View Details is displayed");
		} catch (Exception e) {
			Assert.fail("Accessory Over View Header is not displayed");
		}
		return this;
	}

	/**
	 * Click Back Arrow
	 */
	public AccessoryPDPPage clickBackArrow() {
		try {
			waitforSpinner();
			clickBackArrow.click();
			Reporter.log("BackArrow is available to click");
		} catch (Exception e) {
			Assert.fail("BackArrow is not available to click");
		}
		return this;
	}

	/**
	 * Click Add to Order Button
	 */
	public AccessoryPDPPage clickAddToOrderButton() {
		try {
			waitforSpinner();
			clickElement(addToOrderButton);
			Reporter.log("Clicked on AddToOrder button");
		} catch (Exception e) {
			Assert.fail("AddToOrder button is not available to click");
		}
		return this;
	}

	/**
	 * Click Big T icon
	 */
	public AccessoryPDPPage clickBigTIcon() {
		try {
			waitforSpinner();
			bigTIcon.click();
			Reporter.log("Big T Icon is available to click");
		} catch (Exception e) {
			Assert.fail("Big T Icon is not available to click");
		}
		return this;
	}

	/**
	 * Click phone icon
	 */
	public AccessoryPDPPage clickPhoneIcon() {
		try {
			waitforSpinner();
			phoneIcon.click();
			Reporter.log("PhoneIcon is availale to click");
		} catch (Exception e) {
			Assert.fail("PhoneIcon is not availale to click");
		}
		return this;
	}

	/**
	 * Verify EIP Installment Months for upgrade users
	 */
	public AccessoryPDPPage verifyEIPInstallmentsMonths() {
		try {
			waitforSpinner();
			Assert.assertTrue(eipInstallmentMonths.isDisplayed(), "EIP Monthly terms are not displayed");
			Assert.assertTrue(
					eipInstallmentMonths.getText().contains("9") | eipInstallmentMonths.getText().contains("12")
							| eipInstallmentMonths.getText().contains("24")
							| eipInstallmentMonths.getText().contains("36"),
					"EIP Monthly terms are not correct for some devices in accessories PDP page");
			Reporter.log("EIP length is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP Installment Months ");
		}
		return this;
	}

	/**
	 * Verify EIP Monthly price for upgrade users
	 */
	public AccessoryPDPPage verifyEIPMonthlyPrice() {
		try {
			waitforSpinner();
			Assert.assertTrue(eipMonthlyPrice.isDisplayed(), "EIP Monthly Price not displayed");
			Reporter.log("EIP Monthly Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP Monthly Price ");
		}
		return this;
	}

	/**
	 * Verify EIP DownPayment Price for upgrade users
	 */
	public AccessoryPDPPage verifyEIPDownPaymentPrice() {
		try {
			waitforSpinner();
			Assert.assertTrue(eipDownPayment.getText().contains("$"), "EIP DownPayment Price not displayed");
			Assert.assertTrue(eipDownPayment.getText().toLowerCase().contains("down + tax"),
					"EIP DownPayment Text not displayed");
			Reporter.log("EIP DownPayment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP Downpayment Price and Text ");
		}
		return this;
	}

	/**
	 * Verify Add To Order Button
	 */
	public AccessoryPDPPage verifyAddToOrderButton() {
		try {
			waitforSpinner();
			Assert.assertTrue(addToOrderButton.isDisplayed(), "Add To Order Button is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display add to order button");
		}
		return this;
	}

	/**
	 * Verify FRP After Promo
	 */
	public AccessoryPDPPage verifyFRPAfterPromo() {
		try {
			waitforSpinner();
			Assert.assertTrue(frpAfterPromo.isDisplayed(), "FRP after Promo is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display FRP after promo");
		}
		return this;
	}

	/**
	 * // * Get Accessory FRP with Promotion // * // * @return //
	 */
	public Double getFRPOfAccessoryWithCatalogPromotion() {

		String promoPricetext = frpAfterPromo.getText();
		String arr[] = promoPricetext.split(" ");
		String promoPrice = arr[3].substring(1, arr[3].length());
		return Double.parseDouble(promoPrice);

	}

	/**
	 * // * Get Accessory DownPayment with Promotion // * // * @return //
	 */
	public Double getDownPaymentOfAccessoryWithCatalogPromotion() {

		String promoDPPricetext = eipDownPayment.getText();
		String arr[] = promoDPPricetext.split(" ");
		String promoDPPrice = arr[0].substring(1, arr[0].length());
		return Double.parseDouble(promoDPPrice);

	}

	/**
	 * // * Get Accessory EIP with Promotion // * // * @return //
	 */
	public Double getEIPPaymentOfAccessoryWithCatalogPromotion() {

		String promoEIPPricetext = eipMonthlyPrice.getText();
		String arr[] = promoEIPPricetext.split(" ");
		String promoEIPPrice = arr[1].substring(1, arr[1].length());
		return Double.parseDouble(promoEIPPrice);
	}

	/**
	 * // * Get Accessory EIP Term with Promotion // * // * @return //
	 */
	public Double getEIPTermOfAccessoryWithCatalogPromotion() {

		String promoEIPTermPricetext = eipInstallmentMonths.getText();
		String arr[] = promoEIPTermPricetext.split(" ");
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Click AddToCart Button 2
	 */
	public AccessoryPDPPage clickAddToCartButton2() {
		try {
			waitForSpinnerInvisibility();
			addToOrderButton.click();
			Reporter.log("AddToCart button is available to click");
		} catch (Exception e) {
			Assert.fail("AddToCart button is available to click");
		}
		return this;
	}

	/**
	 * Verify that Promo DownPayment value on Accessory PLP and Accessory PDP Page
	 * are equal
	 */
	public AccessoryPDPPage compareAccessoryPromoDownPayment(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "Promo DownPayment values on PLP and PDP are not equal");
			Reporter.log("Promo DownPayment values on PLP and PDP are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo DownPayment values on PLP and PDP page");
		}
		return this;
	}

	/**
	 * Verify that Promo FRP value on Accessory PLP and Accessory PDP Page are equal
	 */
	public AccessoryPDPPage compareAccessoryPromoEIP(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo  EIP values on PLP and PDP are not equal");
			Reporter.log("Promo  EIP values on PLP and PDP are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo  EIP values on PLP and PDP page");
		}
		return this;
	}

	/**
	 * Verify that Promo EIP value on Accessory PLP and Accessory PDP Page are equal
	 */
	public AccessoryPDPPage compareAccessoryPromoFRPAmounts(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo FRP values on PLP and PDP are not equal");
			Reporter.log("Promo FRP values on PLP and PDP are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo FRP values on PLP and PDP page");
		}
		return this;
	}

	/**
	 * Verify that Promo EIP Term value on Accessory PLP and Accessory PDP Page are
	 * equal
	 */
	public AccessoryPDPPage compareAccessoryPromoEIPTerm(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo EIPTerms values on PLP and PDP are not equal");
			Reporter.log("Promo EIPTerms values on PLP and PDP are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo EIPTerms values on PLP and PDP page");
		}
		return this;
	}

	/**
	 * Verify that monthly amount from accessory PDP page is equal to calculated
	 * Accessory
	 */
	public AccessoryPDPPage compareAccessoryDueMonthlyAmounts(double firstValue, double secondValue) {
		try {

			DecimalFormat df = new DecimalFormat("0.00");
			String formate = df.format(secondValue);
			double finalValue = Double.parseDouble(formate);

			Assert.assertEquals(firstValue, finalValue, "EIP value on Accessory PDP page and Calculated EIP are equal");
			Reporter.log("EIP Monthly amount from Accessory PDP is matched with Calculated EIP");
		} catch (Exception e) {
			Assert.fail("Failed to compare EIP price and calculated EIP price from accesory PDP page.");
		}
		return this;
	}

	/**
	 * // * Get Accessory FRP with Promotion // * // * @return //
	 */
	public Double getFRPOfAccessoryWithoutCatalogPromotion() {

		String promoPricetext = frpAfterPromo.getText();
		String arr[] = promoPricetext.split(" ");
		String promoPrice = arr[2].substring(1, arr[2].length());
		return Double.parseDouble(promoPrice);

	}

	/**
	 * Verify Accessory Striked through Price is equal to Original Price minus
	 * Catalogue Promo
	 * 
	 * @return
	 */
	public AccessoryPDPPage verifyStrikedThroughValue() {
		try {
			Assert.assertTrue(frpAfterPromo.isDisplayed(), "FRP Striked through Price is not displayed.");
			Double accStrikeThroughPricecalc = getFRPOfAccessoryWithoutCatalogPromotion() - getCatalogPromotionValue();
			Double accStrikeThroughPrice = getFRPOfAccessoryWithCatalogPromotion();
			Assert.assertEquals(accStrikeThroughPricecalc, accStrikeThroughPrice,
					"Calculated StrikeThrough price does not match with Actual Striked Through Price");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP Striked through Price.");
		}
		return this;
	}

	/**
	 * // * Get Accessory Catalog Promo Value with Promotion // * // * @return //
	 */
	public Double getCatalogPromotionValue() {

		String catlogPromo = catalogPromoValue.getText();
		String arr[] = catlogPromo.split(" ");
		String catlogPromoPrice = arr[1].substring(1, arr[1].length());
		return Double.parseDouble(catlogPromoPrice);

	}

	/**
	 * getDeviceName
	 */
	public String getAccessoryName() {
		return accessoryName.getText();
	}

	/**
	 * Verify Device Name and Manufacturer Name
	 */
	public AccessoryPDPPage verifyDeviceNameAndManufacturerName(String accessoryName) {
		String accessoryNamePDP = getAccessoryName();
		try {
			Assert.assertTrue(accessoryNamePDP.equals(accessoryName),
					"Manufacturer name and Device Name is not displayed");
			Reporter.log("Device Name and Manufacturer name are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Name and Manufacturer name");
		}
		return this;
	}

}
