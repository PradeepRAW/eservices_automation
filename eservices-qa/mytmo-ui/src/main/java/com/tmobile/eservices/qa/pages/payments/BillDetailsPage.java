/**
 * 
 */
package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class BillDetailsPage extends CommonPage {

	@FindBy(css = "div.current-act-heading.pull-left span.ui_headline")
	private WebElement verifyBillDetailsPage;

	@FindBy(css = "span.ui_mobile_headline")
	private WebElement verifyBillDetailsPageMobile;

	@FindBy(css = "span.ui_lineselector.verbiage_billdet")
	private WebElement verbaigeText;

	@FindBy(id = "verb")
	private WebElement verbaigeTextMobile;

	@FindBy(id = "showPrevMonth")
	private WebElement compareToLastBillLink;

	@FindBy(css = "div.previous-month-col span.ui_body")
	private List<WebElement> pastMonthTitles;

	@FindBy(linkText = "View details")
	private WebElement plansViewDetails;

	@FindBy(css = "div.bb-charge-details .bb-charge-detail")
	private WebElement billDetailspageFromBB;

	@FindBy(css = "#di_CDRbillCycle")
	private WebElement billSelector;

	@FindBy(css = "span[id*='di_billCycle']")
	private List<WebElement> billCycleElements;

	@FindBy(css = "#di_downloadcurrentpdficon")
	private WebElement downloadCurrentPdfIcon;

	@FindBy(linkText = "Back to summary")
	private WebElement backToSummary;

	@FindBy(css = "div.current-act-heading.pull-left span.ui_headline")
	private WebElement pageLoadElement;

	private final String pageUrl = "/details.html";

	@FindBy(id = "paynow")
	private WebElement paynowBtn;

	@FindBy(id = "accountLineSelector")
	private WebElement lineSelectedDropDown;

	@FindBy(id = "di_downloadcurrentpdficon")
	private WebElement downloadPdf;

	@FindBy(id = "choose-Bill")
	private WebElement chooseBillDialogue;

	@FindBy(id = "summaryBill")
	private WebElement summaryBill;

	@FindBy(id = "detailedBill")
	private WebElement detailedBill;
	

	@FindBy(css = "span#acc_dropImgSelector")
	private WebElement accountsDropDownArrow;
	
	@FindAll({@FindBy(css="span#di_accCycle>span.ui_body"),@FindBy(xpath="//span[contains(@id,'acc_LineElement')]")})
	private List<WebElement> linesinAccountsDropDown;
	

	@FindBy(id = "autopayTxt")
	private WebElement autopayTxt;
	
	@FindBy(id = "due_date")
	private WebElement due_date;
	
	@FindBy(id = "payWith-checking")
	private WebElement payWith_checking;
	
	@FindBy(xpath = "//*[@id='payWith-checking']/span[contains(@class,'sprite')]")
	private WebElement sprite_check;
	
	@FindBy(id = "payWith-cardType")
	private WebElement payWith_cardType;
	
	@FindBy(xpath = "//*[@id='payWith-cardType']/span[contains(@class,'sprite')]")
	private WebElement sprite_card;


	
	/**
	 * 
	 * @param webDriver
	 */
	public BillDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public BillDetailsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	/**
	 * Verify Bill Details Page
	 *
	 * @return boolean
	 */
	public BillDetailsPage verifyPageLoaded() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(verifyBillDetailsPageMobile));
				verifyBillDetailsPageMobile.isDisplayed();
			} else {
				checkPageIsReady();
				verifyPageUrl();
			}
			Thread.sleep(1000);
			Reporter.log("Verified bill details page");
		} catch (Exception e) {
			Assert.fail("Bill details page not found");
		}
		return this;
	}

	/**
	 * verify verbiage text displayed for Standard users
	 * 
	 * @return String
	 */
	public BillDetailsPage verifyVerbiageForNonPAHUser() {
		String displayedText;
		if (getDriver() instanceof AppiumDriver) {
			waitFor(ExpectedConditions.visibilityOf(verbaigeTextMobile));
			Assert.assertTrue(
					verbaigeTextMobile.getText().contains(
							"To see details for additional lines on the account, please check with the Primary Account Holder."),
					"verbiage text notdisplayed for Standard users");
		} else {
			checkPageIsReady();
			scrollToElement(verbaigeText);
			Assert.assertTrue(
					verbaigeText.getText().contains(
							"To see details for additional lines on the account, please check with the Primary Account Holder."),
					"verbiage text notdisplayed for Standard users");

		}
		return this;
	}

	/**
	 * Verify Bill Details Page
	 *
	 * @return boolean
	 */
	// public BillDetailsPage verifyBillDetailsPage() {
	// try {
	// if (getDriver() instanceof AppiumDriver) {
	// waitFor(ExpectedConditions.visibilityOf(verifyBillDetailsPageMobile));
	// verifyBillDetailsPageMobile.isDisplayed();
	// } else {
	// waitFor(ExpectedConditions.visibilityOf(verifyBillDetailsPage));
	// verifyBillDetailsPage.isDisplayed();
	// }
	// Reporter.log("Verified bill details page");
	// } catch (Exception e) {
	// Assert.fail("Bill details page not found");
	// }
	// return this;
	// }

	public BillDetailsPage verifyBillDetailsPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("details"));
			Reporter.log("Verified the Bill Details Page");
		} catch (Exception e) {
			Assert.fail("Fail to verify Bill details Page");
		}
		return this;

	}

	/**
	 * verify Bill details page is displayed from Brite Bill
	 * 
	 * @return boolean
	 */
	public BillDetailsPage verifyBillDetailsPageBB() {
		try {
			billDetailspageFromBB.isDisplayed();
			Reporter.log("Verified the Bill Details Page");
		} catch (Exception e) {
			Assert.fail("Fail to verify Bill details Page");
		}
		return this;
	}

	/**
	 * click on Compare ToLast Bill Link
	 */
	public BillDetailsPage clickCompareToLastBillLink() {
		try {
			checkPageIsReady();
			compareToLastBillLink.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ajax_loader_compare")));
			Reporter.log("Clicked on compare to Last Bill link");
		} catch (Exception e) {
			Assert.fail("Fail to click on Compare to Last Bill link");
		}
		return this;

	}

	/**
	 * verify if past & current months comparision is displayed
	 * 
	 * @return true/false
	 */
	public BillDetailsPage verifyPreviousMonthTitle() {
		try {
			checkPageIsReady();
			for (WebElement pastMonthTitle : pastMonthTitles) {
				pastMonthTitle.isDisplayed();
			}
			Reporter.log("\"Last month bill and current month bull are loaded in comaprison table");
		} catch (Exception e) {
			Assert.fail("Fail to verifyLast month bill and current month bull are loaded in comaprison table");
		}

		return this;
	}

	/**
	 * Select Bill Period on Details page
	 */
	public BillDetailsPage selectBillCycle() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(billSelector));
			billSelector.click();
			waitFor(ExpectedConditions.visibilityOfAllElements(billCycleElements));
			for (WebElement webElement : billCycleElements) {
				if (!webElement.isDisplayed()) {
					billSelector.click();
				}
				checkPageIsReady();
				if (downloadCurrentPdfIcon.isDisplayed()) {
					billSelector.click();
					break;
				} else {
					webElement.click();
				}
			}
			Reporter.log("Selected bill cycle");
		} catch (Exception e) {
			Assert.fail("Bill cycle component not found");
		}
		return this;
	}

	/**
	 * Click On Back To Summary Link
	 * 
	 * @return
	 */
	public BillDetailsPage clickOnBackToSummaryLink() {
		try {
			backToSummary.click();
		} catch (Exception e) {
			Assert.fail("Back to summary link is not displayed");
		}
		return this;
	}

	public void clickPayNowBtn() {
		try {
			checkPageIsReady();
			scrollToElement(paynowBtn);
			paynowBtn.click();
		} catch (Exception e) {
			Assert.fail("Pay Now button is not displayed");
		}
	}

	/**
	 * Verify Line Selected dropDown
	 * 
	 * @return
	 */
	public BillDetailsPage verifyLineSelectedDropDown(String LineNumber) {
		try {
			Assert.assertEquals(lineSelectedDropDown.getText().trim(), LineNumber.trim());
		} catch (Exception e) {
			Assert.fail("Back to summary link is not displayed");
		}
		return this;
	}

	/**
	 * click on download bill
	 * 
	 * @return
	 */
	public BillDetailsPage clickDownloadBill() {
		try {
			downloadPdf.click();
			Reporter.log("clicked on download pdf");
		} catch (Exception e) {
			Assert.fail("download pdf is not displayed");
		}
		return this;
	}

	/**
	 * verify choose bill dialogue
	 * 
	 * @return
	 */
	public BillDetailsPage verifyChooseBillDialogueDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(chooseBillDialogue.isDisplayed(), "choose bill dialogue not dispalyed");
			Reporter.log("choose bill dialogue is dispalyed");
		} catch (Exception e) {
			Assert.fail("choose bill dialogue not dispalyed");
		}
		return this;
	}

	/**
	 * verify choose bill dialogue
	 * 
	 * @return
	 */
	public BillDetailsPage verifyViewSummaryandDetailedBillDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(summaryBill.isDisplayed(), "summary  bill button not dispalyed");
			Assert.assertTrue(detailedBill.isDisplayed(), "detailed  bill button not dispalyed");
			Reporter.log("summary and detaled bill  are dispalyed");
		} catch (Exception e) {
			Assert.fail("summary and detaled bill  not dispalyed");
		}
		return this;
	}
	
	/**
	 * Select Bill Period
	 */
	public BillDetailsPage selectBillCycleDesktop() {
		try {
			checkPageIsReady();
			Thread.sleep(2000);
			if (!compareToLastBillLink.isDisplayed()) {
				waitFor(ExpectedConditions.visibilityOf(billSelector));
				billSelector.click();
				waitFor(ExpectedConditions.visibilityOfAllElements(billCycleElements));
				for (WebElement webElement : billCycleElements) {
					if (!webElement.isDisplayed()) {
						billSelector.click();
					}
					checkPageIsReady();
					if (compareToLastBillLink.isDisplayed()) {
						billSelector.click();
						break;
					} else {
						webElement.click();
					}
				}
				Reporter.log("Selected the bill cycle elements");
			}
		} catch (Exception e) {
			Assert.fail("Fail to get select bill cycle");
		}
		return this;
	}
	
	/**
	 * Click On AccountsDropDown Arrow
	 * 
	 * @return
	 */
	public BillDetailsPage clickOnAccountsDropDownArrow() {
		try {
			accountsDropDownArrow.click();
		} catch (Exception e) {
			Verify.fail("AccountsDropDown arrow is not displayed");
		}
		return this;
	}

	

	/**
	 * verify text in Accounts DropDown for Signed in Msisdin
	 */
	public BillDetailsPage verifySingleLineMsisdininAccountDropDown(String msisdinNumber) {
		try {
			//checkPageIsReady();
				for (WebElement webElement : linesinAccountsDropDown){
					//waitFor(ExpectedConditions.visibilityOf(linesinAccountsDropDown));
					if(webElement.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
							.equalsIgnoreCase(msisdinNumber)){
						Reporter.log("Verified that the displayed msisdin is matching w.r.to Signed in Msisdin : "+msisdinNumber);
					}
				}
						
			} catch (Exception e) {
				Verify.fail("Signed in Msisdin by Accounts Dropdown is not displayed");
			}
		return this;
	}

	public BillDetailsPage checkAutopayText() {
		try {
			if(autopayTxt.isDisplayed()) Reporter.log("Autopay text is displayed");
			else Verify.fail("Autopay text is not displayed");
		}
		catch(Exception e) {Verify.fail("autopayTxt element is not located");}
	    return this;
		}
	
		public BillDetailsPage checkAutopayduedateText() {
			try {
				if(due_date.isDisplayed()) Reporter.log("Autopay due_date text is displayed");
				else Verify.fail("Autopay due_date text is not displayed");
			}catch(Exception e) {Verify.fail("due_date element is not located");}
			return this;
		}
		
		public BillDetailsPage Checksprite() {
			try {
				if(payWith_checking.isDisplayed()) {
					if(sprite_check.isDisplayed()) Reporter.log("sprite image for check is displayed");
					else Verify.fail("sprite image for check is not displayed");
				}
				else if(payWith_cardType.isDisplayed()) {
					if(sprite_card.isDisplayed()) Reporter.log("sprite image for card is displayed");
					else Verify.fail("sprite image for card is not displayed");
					
				}
				else { 
			    Verify.fail("sprite image for eighter check or card are not displayed");}
				
	
				
			}catch(Exception e) {Verify.fail("paywith check or card elemnts are not located");}
			return this;
		}
	
	
	


}
