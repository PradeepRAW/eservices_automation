package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class EmployeeVerificationPage extends CommonPage {

	private static final String pageUrl = " profile/employee";

	private static final String noPermissionsPageUrl = " profile/employee/nopermission";

	private static final String documentUpload = "employee/upload";

	private static final String TITLE = "Employee verification";

	@FindBy(css = "div.TMO-SCHEDULED-ACTIVITY-PAGE")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//*[contains(text(),'Employment verification')]")
	private WebElement EmploymentVerfication;

	public EmployeeVerificationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Wait For Spinner
	 *
	 * @return
	 */
	public boolean waitForNewSpinner() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector("div.circle-clipper.right div.circle"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @throws InterruptedException
	 */

	public EmployeeVerificationPage verifyPageTitle() {
		waitFor(ExpectedConditions.titleContains(TITLE));
		return this;
	}

	public EmployeeVerificationPage verifyPageLoaded() {

		try {
			waitForNewSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			verifyPageTitle();
			Reporter.log("Employement Verification  page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Employement Verification page is  not displayed");
		}
		return this;
	}

	public EmployeeVerificationPage verifyNoPermissionsPageLoaded() {

		try {
			waitForNewSpinner();
			checkPageIsReady();
			verifyPageUrl(noPermissionsPageUrl);
			Reporter.log("Employement Verification no permissions page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Employement Verification no permissions page is  not displayed");
		}
		return this;
	}

	public EmployeeVerificationPage verifyEmployeeDocumentUploagPage() {
		try {
			waitForNewSpinner();
			checkPageIsReady();
			verifyPageUrl(documentUpload);
			Reporter.log("Employement Verification document upload is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Employement Verification document upload page is not displayed");
		}
		return this;
	}

}
