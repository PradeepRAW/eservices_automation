package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class OrderApi extends ApiCommonLib{

	/***
	 * This API retrieves devices/accessories and their prices. Also, it sorts
        and filters the hard goods based on price and availability, sets the
        default sku
	 * parameters:
	 *   - $ref: '#/parameters/oAuth'
	 *   - $ref: '#/parameters/transactionId'
	 *   - $ref: '#/parameters/correlationId'
	 *   - $ref: '#/parameters/applicationId'
	 *   - $ref: '#/parameters/channelId'
	 *   - $ref: '#/parameters/clientId'
	 *   - $ref: '#/parameters/transactionBusinessKey'
	 *   - $ref: '#/parameters/transactionBusinessKeyType'
	 *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
	public Response createOrder(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildOrderAPIHeader(apiTestData,tokenMap);
		String resourceURL = "v4/orders/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}       


	private Map<String, String> buildOrderAPIHeader(ApiTestData apiTestData,Map<String, String> tokenMap
			) throws Exception {
		//clearCounters(apiTestData);
		String transactionType = "UPGRADE";
		Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		//	final String accessToken = getAccessToken();
		Map<String, String> headers = new HashMap<String, String>();
		if(StringUtils.isNoneEmpty(tokenMap.get("addaline"))){
			transactionType = "ADDALINE";
		}
		headers = new HashMap<String, String>();
		headers.put("accept","application/json");
		headers.put("applicationid","MYTMO");
		headers.put("cache-control","no-cache");
		headers.put("channelid","WEB");
		headers.put("content-type","application/json");
		headers.put("correlationid",checkAndGetPlattokenJWT(apiTestData,jwtTokens.get("jwtToken")));
		headers.put("transactionbusinesskeytype",apiTestData.getBan());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionType",transactionType);
		headers.put("usn","testUSN");
		headers.put("interactionid","xasdf1234asa4444");
		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
		headers.put("Authorization",jwtTokens.get("jwtToken"));
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}

}
