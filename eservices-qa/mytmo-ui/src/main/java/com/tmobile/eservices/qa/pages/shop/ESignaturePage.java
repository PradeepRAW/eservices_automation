package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class ESignaturePage extends CommonPage  {

	@FindBy(css = "[class*='cb_label']")
	private WebElement eSignCheckBoxtoContinue;

	@FindBy(css = "[id*='action-bar-btn-continue']")
	private WebElement continueCTA;
	
	@FindBy(css = "[id*='action-bar-btn-finish']")
	private WebElement sendOrderCTAOnTopOfpage;
	
	@FindBy(css = "[class*='tab-image-for-signature']")
	private WebElement signatureImage;
	
	@FindBy(css = "[id*='tab-select-style']")
	private WebElement selectStyleTab;
	
	@FindBy(css = "[data-qa*='adopt-submit']")
	private WebElement adoptSubmitCTA;
	
	@FindBy(css = ".hidden-xs button[id*='end-of-document-btn-finish']")
	private WebElement sendOrderCTAOnBottomOfpage;
	
	
	/**
	 * @param webDriver
	 */
	public ESignaturePage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify that page is ESignaturePage
	 */
	public ESignaturePage verifyPageUrl() {
		checkPageIsReady();
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("docusign.net"), "E-sign page URL is incorrect");
			Reporter.log("Verified E-Signature page");
		}catch (Exception e){
			Assert.fail("Failed to verify E-sign page URL");
		}
		return this;
	}
	
	/**
	 * Verify ESignaturePage
	 * 
	 * @return
	 */
	public ESignaturePage verifyESignaturePage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(eSignCheckBoxtoContinue));
			Reporter.log("ESignaturePage is Displayed");
		} catch (Exception e) {
			Assert.fail("ESignaturePage is not Displayed");
		}
		return this;
	}
	
	
	
	/**
	 * Select ESignature Continue CheckBox
	 * 
	 * @return
	 */
	public ESignaturePage selectESignatureCheckBox() {
		try {
			waitFor(ExpectedConditions.visibilityOf(eSignCheckBoxtoContinue));
			Assert.assertTrue(eSignCheckBoxtoContinue.isDisplayed(), "CheckBox is not displayed");
			eSignCheckBoxtoContinue.click();
			Reporter.log("ESignaturePage Check Box is selected");
		} catch (Exception e) {
			Assert.fail("ESignaturePage Check Box is not selected");
		}
		return this;
	}
	
	
	/**
	 * Click ESignature Continue CTA
	 * 
	 * @return
	 */
	public ESignaturePage clickESignatureContinueCTA() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(continueCTA));
			continueCTA.click();
			Reporter.log("Continue CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Continue CTA");
		}
		return this;
	}
	
	/**
	 * Click Send The Order CTA
	 * 
	 * @return
	 */
	public ESignaturePage clickSendTheOrderCTA() {
		try {
			moveToElement(sendOrderCTAOnBottomOfpage);
			checkPageIsReady();
			clickElementWithJavaScript(sendOrderCTAOnBottomOfpage);
			Reporter.log("Send The Order CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Send The Order CTA");
		}
		return this;
	}
	
	
	/**
	 * Click Signature Image
	 * 
	 * @return
	 */
	public ESignaturePage clickSignatureImage() {
		try {
			moveToElement(signatureImage);
			signatureImage.click();
			Reporter.log("Signature Image is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Signature Image");
		}
		return this;
	}
	
	
	/**
	 * Click Select Style Tab
	 * 
	 * @return
	 */
	public ESignaturePage clickSelectStyleTab() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(selectStyleTab));
			selectStyleTab.click();
			Reporter.log("Select Style Tab is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Select Style Tab");
		}
		return this;
	}
	
	
	/**
	 * Click Adopt Signature CTA
	 * 
	 * @return
	 */
	public ESignaturePage clickAdoptSignatureCTA() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(adoptSubmitCTA));
			adoptSubmitCTA.click();
			Reporter.log("Adop[t Submit CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Adopt Submit CTA");
		}
		return this;
	}
	
	
}

