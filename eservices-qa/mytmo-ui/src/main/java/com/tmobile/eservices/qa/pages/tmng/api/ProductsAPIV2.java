package com.tmobile.eservices.qa.pages.tmng.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eservices.qa.api.RestServiceCallType;

import io.restassured.response.Response;

public class ProductsAPIV2 extends GetTMNGAccessTokens {

	public Response retrieveProductsUsingPOST(String url) {
		// setUpEnv();
		// GetTMNGAccessTokens getTMNGAccessTokens = new GetTMNGAccessTokens();
		Response response = null;
		try {
			String requestBody = null;
			if (url.contains("cell-phone")) {
				requestBody = "{ \"productTypes\": [\"DEVICE\"], \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"HANDSET\", \"SIMCARD\"] } ],"
						+ " \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			} else if (url.contains("tablets")) {
				requestBody = "{ \"productTypes\": [\"DEVICE\"], \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", "
						+ "\"values\": [\"TABLET\"] } ], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			} else if (url.contains("smart-watches")) {
				requestBody = "{ \"productTypes\": [\"DEVICE\"], \"filterOptionsSelected\": [ { \"fieldName\": \"productSubType\", \"values\": [\"WEARABLE\"] } ],"
						+ " \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			} else if (url.contains("accessories")) {
				requestBody = "{ \"productTypes\": [\"ACCESSORY\"], \"pageNumber\":\"1\", \"pageSize\":\"1000\" }";
			}
			Map<String, String> headers = buildProductsApiV2Header(iam_server, tmng_api_base_url);
			String resourceURL = "v2/products";
			response = invokeRestServiceCall(headers, tmng_api_base_url, resourceURL, requestBody,
					RestServiceCallType.POST);
		} catch (Exception e) {
			Reporter.log(e.getMessage());
			Assert.fail("Failed to retirieve products using POST");
		}
		return response;
	}

	/**
	 * Provides list of available devices
	 * 
	 * @return
	 */
	public List<String> getProducts(String url) {
		List<String> products = new ArrayList<>();
		try {
			Response productsresponse = retrieveProductsUsingPOST(url);
			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					String productNode = productsNode.get(i).path("familyName").asText();

					JsonNode skuNode = productsNode.get(i).path("skus");
					if (!skuNode.isMissingNode() && skuNode.isArray()) {
						for (int j = 0; j < skuNode.size(); j++) {
							String defaultSKU = skuNode.get(j).path("isDefaultSku").asText();
							String skuCode = skuNode.get(j).path("skuCode").asText();
							String memory = skuNode.get(j).path("memory").asText();
							String color = skuNode.get(j).path("color").asText();

							JsonNode availabilityNode = skuNode.get(j).path("availability");
							if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
								String availabilityStatus = availabilityNode.path("availabilityStatus").asText();
								// Reporter.log("Product Details: " +productNode+ "|"+defaultSKU+ "|" +skuCode+
								// "|" +memory+ "|" +color+ "|"+availabilityStatus);
								// System.out.println("Product Details: " +productNode+ "|"+defaultSKU+ "|"
								// +skuCode+ "|" +memory+ "|" +color+ "|"+availabilityStatus);

								if (defaultSKU.equalsIgnoreCase("True") && availabilityStatus.equals("AVAILABLE")) {
									products.add(productNode);
								}
							}
						}
					}
				}
			}
			return products;
		} catch (Exception e) {
			Reporter.log("Failed to retrieve products");
		}
		return null;
	}

	private Map<String, String> buildProductsApiV2Header(String iam_server, String baseURL) throws Exception {

		// ApiCommonLib apiCommonLib = new ApiCommonLib();
		String accessToken = getTmngAccessToken(iam_server, baseURL);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + accessToken);
		headers.put("Content-Type", "application/json");
		headers.put("Postman-Token", "75883537-c5e6-49cc-a47c-2b96791247eb");
		headers.put("applicationID", "TMO");
		headers.put("cache-control", "no-cache,no-cache");
		headers.put("channelid", "WEB");
		headers.put("interactionid", "interactionid");
		headers.put("transactiontype", "ACTIVATION");
		return headers;
	}

	public JsonNode getParentNodeFromResponse(Response response) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		try {
			jsonNode = mapper.readTree(response.asString());
		} catch (JsonProcessingException e) {
			Assert.fail("<b>JsonProcessingException Response :</b> " + e);
			Reporter.log(" <b>JsonProcessingException Response :</b> ");
			Reporter.log(" " + e);
			e.printStackTrace();
		} catch (IOException e) {
			Assert.fail("<b>IOException Response :</b> " + e);
			Reporter.log(" <b>IOException Response :</b> ");
			Reporter.log(" " + e);
			e.printStackTrace();
		}
		return jsonNode;
	}

}
