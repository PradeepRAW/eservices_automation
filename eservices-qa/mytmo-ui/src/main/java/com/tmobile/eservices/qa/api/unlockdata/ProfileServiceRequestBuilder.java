package com.tmobile.eservices.qa.api.unlockdata;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import com.tmobile.eservices.qa.api.unlockdata.RequestData;

/**
 * @author blakshminarayana
 *
 */
public final class ProfileServiceRequestBuilder {
	private static Logger logger = LoggerFactory.getLogger(ProfileServiceRequestBuilder.class);

	private ProfileServiceRequestBuilder() {
	}

	/**
	 * This method construct the URL and call the service using rest assured
	 * 
	 * @param requestData
	 * @return
	 */
	public static RequestSpecification createAccessTokenRequest(RequestData requestData) {
		String environment = System.getProperty("iam_servers").toLowerCase();
		String baseUri = null;
		if (StringUtils.isNotEmpty(environment)) {
			baseUri = CommonUtils.getAccessTokenEndPointUrl(environment);
		}
		RequestSpecBuilder requestSpecBuilder = createRequestSpecification(environment, baseUri);
		requestSpecBuilder.addQueryParameters(addQueryParametersToBaseUri(requestData, false));
		return requestSpecBuilder.build();
	}

	private static RequestSpecBuilder createRequestSpecification(String environment, String endPointURL) {
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setRelaxedHTTPSValidation();
		
		requestSpecBuilder.addHeader("Authorization", CommonUtils.getAuthorization(environment));
		requestSpecBuilder.addHeader("Accept", "application/json");
		requestSpecBuilder.setBaseUri(endPointURL);
		
		return requestSpecBuilder;
	}

	/**
	 * This method construct the query parameters
	 * 
	 * @return requestData
	 */
	private static Map<String, Object> addQueryParametersToBaseUri(RequestData requestData, boolean sendPinToMsisdn) {
		Map<String, Object> queryString = new LinkedHashMap<String, Object>();
		String msisdn = requestData.getMsisdn();
		String profileType = requestData.getProfileType();
		String token = requestData.getToken();
		if (StringUtils.isNotEmpty(msisdn) && sendPinToMsisdn) {
			queryString.put("msisdn", msisdn);
		} else if (StringUtils.isNotEmpty(msisdn) && StringUtils.isNotEmpty(profileType)
				&& StringUtils.isNotEmpty(token)) {
			queryString.put("token", token);
			queryString.put("profileType", profileType);
			queryString.put("userKey", "msisdn");
			queryString.put("keyValue", msisdn);
		} else {
			queryString.put("scope", "TMO_ID_profile token_validation");
			queryString.put("grant_type", "client_credentials");
		}
		return queryString;
	}

	/**
	 * This method construct the URL and made call the service using rest
	 * assured
	 * 
	 * @param requestData
	 * @return
	 */
	public static RequestSpecification createPinToMsisdnRequest(RequestData requestData) {
		//logger.info("createPinToMsisdnRequest method called in RequestBuilder");
		String baseUri = null;
		/*String environment = requestData.getEnvironment();*/
		String environment = System.getProperty("iam_servers").toLowerCase();
		if (StringUtils.isNotEmpty(environment)) {
			baseUri = CommonUtils.getTempPinEndPointUrl(environment);
		}
		RequestSpecBuilder requestSpecBuilder = createBearerRequestSpecification(requestData, baseUri);
		requestSpecBuilder.addQueryParameters(textMsgVerification(requestData));
		return requestSpecBuilder.build();
	}

	/**
	 * This method construct the URL and made call the service using rest
	 * assured
	 * 
	 * @param requestData
	 * @return
	 */
	public static RequestSpecification createProfileRequest(RequestData requestData) {
		//logger.info("createProfileRequest method called in RequestBuilder");
		String baseUri = null;
		/*String environment = requestData.getEnvironment();*/
		String environment = System.getProperty("iam_servers").toLowerCase();
		if (StringUtils.isNotEmpty(environment)) {
			baseUri = CommonUtils.getProfileEndPointURL(environment);
		}
		RequestSpecBuilder requestSpecBuilder = createRequestSpecification(environment, baseUri);
		requestSpecBuilder.addQueryParameters(addQueryParametersToBaseUri(requestData, false));
		return requestSpecBuilder.build();
	}

	/**
	 * This method construct the URL and made call the service using rest
	 * assured
	 * 
	 * @param requestData
	 * @return
	 */
	public static RequestSpecification clearCounterRequest(RequestData requestData) {
		logger.info("clearCounterRequest method called in clearCounterRequest");
		String baseUri = null;
		String environment = requestData.getEnvironment();
		if (StringUtils.isNotEmpty(environment)) {
			baseUri = CommonUtils.getClearCounterEndPointURL(environment);
		}
		RequestSpecBuilder requestSpecBuilder = createBearerRequestSpecification(requestData, baseUri);
		requestSpecBuilder.setBody(createClearCountBody(requestData)).setContentType(ContentType.JSON);
		return requestSpecBuilder.build();
	}

	/**
	 * This method construct the URL and made call the service using rest
	 * assured
	 * 
	 * @param requestData
	 * @return
	 */
	public static RequestSpecification createChangePasswordRequest(RequestData requestData) {
		logger.info("createChangePasswordRequest method called in RequestBuilder");
		String baseUri = null;
		/*String environment = requestData.getEnvironment();*/
		String environment = System.getProperty("iam_servers").toLowerCase();
		if (StringUtils.isNotEmpty(environment)) {
			baseUri = CommonUtils.getChangePasswordEndPointURL(environment);
		}
		RequestSpecBuilder requestSpecBuilder = createBearerRequestSpecification(requestData, baseUri);
		requestSpecBuilder.setBody(createChangePasswordBody(requestData)).setContentType(ContentType.JSON);
		return requestSpecBuilder.build();
	}
	
	
	private static Map<String, ?> createChangePasswordBody(RequestData requestData) {
		Map<String, Object> setBody = new LinkedHashMap<String, Object>();
		String newPassword = null;
		String msisdn = requestData.getMsisdn();
		if (StringUtils.isNotEmpty(msisdn)) {
			StringBuilder appendmsisdn = new StringBuilder();
			appendmsisdn.append("tel:").append(msisdn);
			setBody.put("user_id", appendmsisdn.toString());
			setBody.put("old_or_temp_password",requestData.getTempPassword());
			String environment = System.getProperty("iam_servers").toLowerCase();
			if (StringUtils.isNotEmpty(environment)) {
				newPassword = CommonUtils.getNewProfilePassword(environment);
			}
			setBody.put("new_password",newPassword);
			return setBody;
		}
		return setBody;
	}
	
		private static Map<String, ?> textMsgVerification(RequestData requestData) {
	        Map<String, Object> setBody = new LinkedHashMap<String, Object>();
	        String msisdn = requestData.getMsisdn().trim();
	        if (StringUtils.isNotEmpty(msisdn)) {
	            StringBuilder appendmsisdn = new StringBuilder();
	            appendmsisdn.append("tel:").append(msisdn);
	            setBody.put("user_id", appendmsisdn.toString().trim());
	            setBody.put("notification", appendmsisdn.toString());
	        }
	        return setBody;
	    }
	/**
	 * This method is used to construct the request for unlock accounts
	 * 
	 * @param requestData
	 * @param baseUri
	 * @return
	 */
	private static RequestSpecBuilder createBearerRequestSpecification(RequestData requestData, String baseUri) {
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setRelaxedHTTPSValidation();
		requestSpecBuilder.addHeader("Authorization", "Bearer " +requestData.getToken().trim());
		
		requestSpecBuilder.addHeader("Accept", "application/json");
		requestSpecBuilder.addHeader("Content-Type", "application/json");
		requestSpecBuilder.setBaseUri(baseUri);
		return requestSpecBuilder;	
	}
	    
    private static Map<String, ?> secondFactorTempPinBody(RequestData requestData) {
        Map<String, Object> setBody = new LinkedHashMap<String, Object>();
        String msisdn = requestData.getMsisdn().trim();
        if (StringUtils.isNotEmpty(msisdn)) {
            StringBuilder appendmsisdn = new StringBuilder();
            appendmsisdn.append("tel:").append(msisdn);
            setBody.put("user_id", appendmsisdn.toString().trim());
            setBody.put("notification", appendmsisdn.toString());
            return setBody;
        }
        return setBody;
    }


	/**
	 * This method is used to construct the request body to unlock the account
	 * 
	 * @param requestData
	 * @return
	 */
	private static Map<String, ?> createClearCountBody(RequestData requestData) {
		Map<String, Object> setBody = new LinkedHashMap<String, Object>();
		String msisdn = requestData.getMsisdn();
		if (StringUtils.isNotEmpty(msisdn)) {
			StringBuilder appendmsisdn = new StringBuilder();
			appendmsisdn.append("tel:").append(msisdn);
			setBody.put("user_id", appendmsisdn.toString());
			return setBody;
		}
		return setBody;
	}

	public static RequestSpecification createDeleteProfileRequest(RequestData requestData) {
		logger.info("createDeleteProfileRequest method called in ServiceRequestBuilder");
		String environment = requestData.getEnvironment();
		String baseUri = null;
		if (StringUtils.isNotEmpty(environment)) {
			baseUri = CommonUtils.getProfileEndPointURL(environment);
		}
		RequestSpecBuilder requestSpecBuilder = createDeleteRequestSpecification(baseUri);
		requestSpecBuilder.addQueryParameters(addQueryParametersToBaseUri(requestData, false));
		return requestSpecBuilder.build();
	}

	private static RequestSpecBuilder createDeleteRequestSpecification(String endPointURL) {
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setRelaxedHTTPSValidation();
		requestSpecBuilder.addHeader("Accept", "application/json");
		requestSpecBuilder.setBaseUri(endPointURL);
		return requestSpecBuilder;
	}

	public static RequestSpecification createDecryptRequest(RequestData requestData) {
        logger.info("createEncryptAndDecryptRequest method called in ServiceRequestBuilder");
        String environment = requestData.getEnvironment();
        String baseUri = null;
        if (StringUtils.isEmpty(environment)) {
            environment = System.getProperty("iam_servers");
        }
        if (StringUtils.isNotEmpty(environment)) {
            baseUri = CommonUtils.getDecryptEndPointUrl(environment);
        }
        RequestSpecBuilder requestSpecBuilder = createDecryptRequestSpecification(baseUri);
        if (StringUtils.isNotEmpty(requestData.getTempPassword())) {
            requestSpecBuilder.setBody(requestData.getTempPassword()).setContentType(ContentType.JSON);
        }
        return requestSpecBuilder.build();
    }

	private static RequestSpecBuilder createDecryptRequestSpecification(String baseUri) {
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setRelaxedHTTPSValidation();
		requestSpecBuilder.addHeader("Accept", "application/json");
		requestSpecBuilder.setBaseUri(baseUri);
		return requestSpecBuilder;
	}

	public static RequestSpecification createTempPinRequest(RequestData requestData) {
		String baseUri = null;
		String environment = System.getProperty("iam_servers").toLowerCase();
		if (StringUtils.isNotEmpty(environment)) {
			baseUri = CommonUtils.getTempPinEndPointUrl(environment);
		}
		RequestSpecBuilder requestSpecBuilder = createBearerRequestSpecification(requestData, baseUri);
		requestSpecBuilder.setBody(textMsgVerification(requestData)).setContentType(ContentType.JSON);
		return requestSpecBuilder.build();
	}

}
