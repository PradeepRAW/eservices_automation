package com.tmobile.eservices.qa.pages.shop;

import static org.testng.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

public class UNOPLPPage extends CommonPage {
	
	@FindBy(css = "span[class='text-legal']")
	private List<WebElement> deviceFRPPrice;
	
	@FindBy(css = "div[data-analytics-details*='Offer Modal']")
	private List<WebElement> promotionText;
	
	@FindBy(css = "p.product-name")
	private List<WebElement> productName;

	@FindBy(css = "div.manufacturer span[data-e2e='product-card-manufacturer']")
	private List<WebElement> productManufacturer;
	
	@FindBy(css = "span[class*='spacer-h-m legal-container']")
	private WebElement legalDisclaimer;
	
	@FindBy(css = "div[class*='text-legal has-mobile-margin ng']")
	private WebElement legalDisclaimerFooter;
	
	@FindBy(xpath = "//mat-checkbox[@id='mat-checkbox-3']//div[@class='mat-checkbox-inner-container']")
	private WebElement specialOffer;
	
	@FindBy(xpath = "//p[contains(text(), 'Filter by')]")
	private WebElement iOSFilterDropdown;
	
	@FindBy(css = "div[data-asset-type='link']")
	private WebElement promotionDescription;
	
	@FindBy(xpath="//span[text()='Deals']")
	private WebElement clickOnDeals;
	
	@FindBy(xpath="//span[text()='Sort by: Featured']")
	private WebElement sortingDropdown;
	
	@FindBys(@FindBy(css = "span.option-name"))
	private List<WebElement> sortingList;
	
	@FindBy(xpath="//h1[contains(text(),'Phones')]")
	private WebElement phonesHeader;
	
	@FindBy(css = "span.mobile-gridTitle")
	private WebElement mobilePhonesHeader;
	
	@FindBy(xpath = "//h1[contains(text(),'Accessories')]")
	private WebElement accessoriesHeader;
	
	@FindBy(css = ".mobile .category-button")
	private WebElement mobileDeviceTypeDropdown;
	
	@FindBy(css = "tmo-product-categories.ng-star-inserted button.only-mobile span")
	private List<WebElement> mobileMenuLinks;
	
	@FindBy(css = "a[routerlinkactive='active'] button.hide-on-mobile span")
	private List<WebElement> menuLinks;
	
	@FindBy(css = "span.mobile-gridTitle")
	private WebElement mobileAccessoriesHeader;
	
	@FindBy(css = "button.sort-menu-button.mat-stroked-button div.fx-row.fx-layout-align-start-center mat-icon")
	private WebElement priceSortDropdown;
	
	@FindBy(css = "button[role='menuitem'] span")
	private List<WebElement> priceSortOption;
	
	
	public UNOPLPPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Verify UNO PLP Page
	 *
	 * @return
	 */
	public UNOPLPPage verifyAccessoriesPageLoaded() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains("/accessories"));
			Reporter.log("Stand Alone Accessories Stand Alone PLP is Loaded");
		} catch (Exception e) {
			Assert.fail("Stand Alone Accessories  PLP is Not Loaded");
		}
		return this;
	}

	/**
	 * Verify UNO PLP Page
	 *
	 * @return
	 */
	public UNOPLPPage verifyPageLoaded() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains("/cell-phones"));
			Reporter.log(Constants.PLP_PAGE_HEADER);
		} catch (Exception e) {
			Assert.fail("Product List Page is Not Loaded");
		}
		return this;
	}
	
	
	
	
	/**
	 * Verify legal disclaimer on Phones page.
	 * 
	 */
	public UNOPLPPage verifyLegalDisclaimer() {
		checkPageIsReady();
		try {
			assertTrue(legalDisclaimer.isDisplayed(), "LegalDisclaimer text is not diplayed on phones page.");
			Reporter.log("LegalDisclaimer text is diplayed on phones page.");
		} catch (Exception ex) {
			Assert.fail("Failed to verify LegalDisclaimer text on phones page.");
		}
		return this;
	}


	/**
	 * Click device containing availability
	 *
	 * @param pName
	 *
	 */
	public void clickDeviceByName(String pName) {
		try {
			pName = pName.trim();
			Boolean elementClick = true;
			do {
				for (int i = 0; i < productName.size(); i++) {
					String productNameWithManufacturer = productManufacturer.get(i).getText() + " "
							+ productName.get(i).getText();
					if (productName.get(i).getText().trim().endsWith(pName)
							|| productNameWithManufacturer.endsWith(pName)) {
						String deviceName = productName.get(i).getText();
						// moveToElement(productName.get(i));
						clickElementWithJavaScript(productName.get(i));
						Reporter.log("Clicked on product: " + deviceName);
						elementClick = false;
						break;
					}
				}
				waitForSpinnerInvisibility();
				if (getDriver().getCurrentUrl().contains("cell-phones")
						| getDriver().getCurrentUrl().contains("smart-watches")
						| getDriver().getCurrentUrl().contains("accessories")
						| getDriver().getCurrentUrl().contains("tablets")) {
					scrollToNextLine();
				} else {
					break;
				}
			} while (elementClick);
		} catch (Exception e) {
			Assert.fail("Fail to click on next device: " + pName);
		}
	}
	
	/**
	 * Scroll to next line in PLP
	 *
	 */
	public void scrollToNextLine() {
		try {
			if (getDriver() instanceof IOSDriver) {
				scrollDownMobileIOS();
			} else {
				((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
			}
		} catch (Exception e) {
			Assert.fail("Failed to scroll to next line");
		}
	}
	
	/**
	 * Get total Device FRP on PDP
	 * 
	 * @return
	 */
	public String getDeviceFRP() {
		String frp = "";
		try {
			frp = deviceFRPPrice.get(0).getText().replace("$", "");			
			Reporter.log(" Got total Device FRP on PDP");
		} catch (Exception e) {
			Assert.fail("Failed to get total Device FRP on PDP");
		}
		return frp;
	}

	
	/**
	 * Verify Promotion Text Displayed
	 * 
	 */
	public UNOPLPPage verifyPromotionTextonPLP() {
		try {
			checkPageIsReady();
			assertTrue(promotionText.get(0).isDisplayed(), "Promotion Text is not diplayed for any device");
			Reporter.log("Promotion Text is diplayed in PLP Page");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Promotion Text for any device");
		}
		return this;
	}
	
	public void scrollDownMobileIOS() {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		js.executeScript("mobile: scroll",scrollObject);


	}

	/**
	 * Filter in unoplp page and select Special offer devices under Deals
	 */
	public UNOPLPPage selectSpecialOfferOnDeals() {
		try {
			clickElementWithJavaScript(specialOffer);
			Reporter.log("Special offer on deals is clickable");
		} catch (Exception e) {
			Assert.fail("Special offer on deals is not clickable");
		}
		return this;
	}
	/**
	 * verify Promo Description
	 */
	public UNOPLPPage verifyPromoDescription() {
		try {
			waitFor(ExpectedConditions.visibilityOf(promotionDescription));
			Assert.assertTrue(promotionDescription.isDisplayed(), "Promo description is not present");
			Reporter.log("Promo description is present");
		} catch (Exception e) {
			Assert.fail("Failed to verify Promo Description");
		}
		return this;
	
	}
	/*
	 * click on deals tab
	 */
	public UNOPLPPage clickOnDealsTab() {
		try {
			clickOnDeals.click();
			Reporter.log("clicked on deals tab");
		} catch(Exception e) {
			Assert.fail("fail to click on deals tab");
		}
	return this;
	}
	
	/**
	 * Select Sort By Value
	 */	
	public void selectSortBy(String sortByValue) {
		try {
			sortingDropdown.click();
			waitforSpinner();
			for (WebElement webElement : sortingList) {
				if (sortByValue.equalsIgnoreCase(webElement.getText())) {
					webElement.click();
					break;
				}
			}
			Reporter.log("price high to low is selected");

		} catch (Exception e) {
			Assert.fail("Prices high to low is not sorting");
		}
	}
	
	/**
	 * Verify Phones header
	 */
	public UNOPLPPage verifyPhonesHeader() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(mobilePhonesHeader.getText().contains("Phones"), "Phones header not displayed");
				Reporter.log("Phones header is displayed"); 
			} else {
				Assert.assertTrue(phonesHeader.getText().contains("Phones"),"Phones header not displayed");
				Reporter.log("Phones header is displayed");
			}
			
		} catch (Exception e) {
			Assert.fail("Failes to display Phones header");
		}
		return this;
	}	
	
	/**
	 * Verify Accessories header
	 */
	public UNOPLPPage verifyAccessoriesHeader() {
		try {
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(mobileAccessoriesHeader.getText().contains("Accessories"), "Accessries header not displayed");
				Reporter.log("Accessories header is displayed");
			} else {
				Assert.assertTrue(accessoriesHeader.getText().contains("Accessories"), "Accessries header not displayed");
				Reporter.log("Accessories header is displayed");
			}
			
		} catch (Exception e) {
			Assert.fail("Failed to display accessories header");
		}
		return this;
	}
	
	/**
	 * Verify Product Categories links
	 */

	public UNOPLPPage verifyProductCategoriesLinks() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(mobileDeviceTypeDropdown);
				waitFor(ExpectedConditions.visibilityOf(mobileMenuLinks.get(0)), 20);
				for (WebElement link : mobileMenuLinks) {
					Assert.assertTrue(
							link.getText().contains("Phones") || link.getText().contains("Tablets & devices")
									|| link.getText().contains("Watches") || link.getText().contains("Accessories"),
							"Product Categories links not displayed");

				}
				Reporter.log("Product Categories links displayed");

			} else {

				waitFor(ExpectedConditions.visibilityOf(menuLinks.get(0)), 20);
				for (WebElement link : menuLinks) {
					Assert.assertTrue(
							link.getText().contains("Phones") || link.getText().contains("Tablets & devices")
									|| link.getText().contains("Watches") || link.getText().contains("Accessories"),
							"Product Categories links not displayed");

				}
				Reporter.log("Product Categories links displayed");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display Product Categories links");
		}
		return this;
	}
	
	/**
	 * Apply Sorting on Accessories PLP
	 */
	public UNOPLPPage applySorting() {
		waitforSpinner();
		try {
			clickElement(priceSortDropdown);
			clickElement(priceSortOption.get(1));
			Reporter.log("Applied Sorting");
		} catch (Exception e) {
			Assert.fail("Unable to Sort the Accessories");
		}
		return this;
	}
	
	
}
