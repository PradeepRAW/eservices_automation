package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class AUCMessagePage extends CommonPage {

	private static final String pageUrl = "aucMessages";
	
	public AUCMessagePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify AUC message page
	 */
	public AUCMessagePage verifyAUCMessagePage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("AUC message page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("AUC message page not displayed");
		}
		return this;
	}
}
