package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class LoanAgreementApiV3 extends ApiCommonLib{
	
	/***
	 * This is the API specification for the EOS Loan Agreement.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
    public Response loanAgreementOps(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildLoanAgreementAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v3/loan/agreement";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
    }       

    private Map<String, String> buildLoanAgreementAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	//clearCounters(apiTestData);
    	JWTTokenApi jwtTokenApi = new JWTTokenApi();
    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
    	jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"),tokenMap);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept","application/json");
		headers.put("applicationid","MYTMO");
		headers.put("correlationid",checkAndGetPlattokenJWT(apiTestData,jwtTokens.get("jwtToken")));
		headers.put("transactionbusinesskeytype",apiTestData.getBan());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionType","UPGRADE");
		headers.put("usn","testUSN");
		headers.put("interactionid","xasdf1234asa44");
		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
		headers.put("channelId","WEB");
		headers.put("Authorization",jwtTokens.get("jwtToken"));
		headers.put("cache-control","no-cache");
		headers.put("content-type","application/json");
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}

}
