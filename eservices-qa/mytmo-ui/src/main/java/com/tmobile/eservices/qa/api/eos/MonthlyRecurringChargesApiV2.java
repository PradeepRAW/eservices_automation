package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class MonthlyRecurringChargesApiV2 extends ApiCommonLib{
	
	/***
	 * Summary: This is the API specification for the Monthly Recurring charges.
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-phoneNumber,required=true
	 *  -Content-Type
	 *  -dealerCode
	 *  
	 * @return
	 * @throws Exception
	 */
    public Response monthlyRecurringCharge(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildMonthlyRecurringChargeHeader(apiTestData);
    	String resourceURL = "v1/finance/recurringChargeSummary";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
        return response;
    }
    
    private Map<String, String> buildMonthlyRecurringChargeHeader(ApiTestData apiTestData) throws Exception {
    	final String oauth = getAccessToken();
    	
		Map<String, String> headers = new HashMap<String, String>();
		
		headers.put("Authorization",oauth);
		headers.put("transactionType","jump");
		headers.put("applicationId","CSM");
		headers.put("channelId","web");
		headers.put("correlationId",oauth);
		headers.put("clientId","TMO");
		headers.put("transactionBusinessKey","msisdn");
		headers.put("transactionBusinessKeyType",apiTestData.getMsisdn());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("Content-Type","application/json");
		headers.put("dealerCode","000002");
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}

}
