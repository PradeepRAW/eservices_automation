package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author
 *
 */
public class ChangePlansConfirmation extends CommonPage {

	@FindBy(css = "div[role='title'] p")
	private List<WebElement> headerTile;

	@FindBy(xpath = "(//img[@class='img-height'])")
	private WebElement greenCheckMark;

	@FindBy(xpath = "(//p[contains(text(),'Thanks,')])")
	private WebElement thanksUserName;

	@FindBy(xpath = "(//p[contains(text(),'Your plan change')])")
	private WebElement textOrderInProgress;

	@FindBy(xpath = "(//p[contains(text(),'Your order is complete')])")
	private WebElement textOrderComplete;

	@FindBy(xpath = "(//*[@class='pull-right arrow-up arrowup_rotation'])")
	private WebElement expandableOrderDetails;

	@FindBy(xpath = "//*[contains(text(),'Plan change details')]")
	private WebElement textChangePlanDetails;

	@FindBy(xpath = "//*[contains(text(),'Order details')]")
	private WebElement textOrderDetails;

	@FindBy(xpath = "(//span[@class='pull-left Display6'])")
	private WebElement textYourNewPlan;

	@FindBy(xpath = "(//span[contains(text(),'Autopay:')])")
	private WebElement textAutopay;

	@FindBy(xpath = "(//span[contains(text(),'Enrolled')])")
	private WebElement autopayStatus;

	@FindBy(xpath = "(//span[contains(text(),'Autopay method:')])")
	private WebElement textAutopayMethod;

	@FindBy(xpath = "((//*[@class='body'])[2])")
	private WebElement valueOfAutopayMethod;

	@FindBy(xpath = "(//span[contains(text(),'First Autopay date:')])")
	private WebElement textFirstAutopayDate;

	@FindBy(xpath = "(//span[contains(text(),'First Autopay date:')]//..//..//span)[2]")
	private WebElement valueOfFirstAutopayDate;

	@FindBy(xpath = "(//span[contains(text(),'Name on account')]//..//..//span)[1]")
	private WebElement textNameOnAccount;

	@FindBy(xpath = "(//span[contains(text(),'Name on account')]//..//..//span)[2]")
	private WebElement nameOnAccount;

	@FindBy(xpath = "(//span[contains(text(),'T-Mobile account no.')]//..//..//span)[1]")
	private WebElement textTMobileAccountNumber;

	@FindBy(xpath = "(//span[contains(text(),'T-Mobile account no.')]//..//..//span)[2]")
	private WebElement tMobileAccountNumber;

	@FindBy(xpath = "(//*[@class='black body'])[1]")
	private WebElement ratePlanName;

	@FindBy(xpath = "(//span[contains(text(),'Effective')])")
	private WebElement effectiveDate;

	@FindBy(xpath = "//span[contains(text(),'New monthly total')]")
	private WebElement textNewMonthlyTotal;

	@FindBy(xpath = "(//*[@class='Display5'])[2]")
	private WebElement newMonthlyTotalValue;

	@FindBy(xpath = "//span[contains(text(),'Next steps:')]")
	private WebElement textNextSteps;

	@FindBy(xpath = "(//span[@class='body-bold black'])")
	private WebElement netflixHeaderText;

	@FindBy(xpath = "//*[contains(text(),'Your Netflix benefit')]")
	private WebElement nextflixText;

	@FindBy(css = "button[aria-label='Click to Manage Netflix']")
	private WebElement manageNetflixCTA;

	@FindBy(xpath = "(//div[contains(@class,'float-left arrow-size arrow-back')])")
	private WebElement backNavArrow;

	@FindBy(xpath = "(//button[@class='SecondaryCTA blackCTA full-btn-width-xs'])")
	private WebElement gotoAccountOverviewCTA;

	/**
	 * 
	 * @param webDriver
	 */
	public ChangePlansConfirmation(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(xpath = "//div[contains(text(),'autopay']")
	private WebElement autoPay;

	/**
	 * Verify Plans confirmation page
	 * 
	 * @return
	 */
	public ChangePlansConfirmation verifyPlansConfirmationPage() {
		checkPageIsReady();
		waitforSpinner();
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("plan-confirmation"));
			Reporter.log("Plans Confirmation page is displayed");
		} catch (Exception e) {
			Assert.fail("Plans Confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether GreenCheckmark displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation verifYourPlanChangeHeader() {
		try {
			Assert.assertTrue(verifyElementBytext(headerTile, "Your plan change is in progress."),
					"Your plan change is in progress not displaying");

			Reporter.log("Your plan change is in progress text message is displayed on confirmation page");
		} catch (Exception e) {
			Assert.fail("Your plan change is in progress not displaying on confirmation page");
		}
		return this;
	}

	/**
	 * Verify whether GreenCheckmark displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkWhetherGreenCheckMarkIconDisplayedOrNot() {
		try {
			greenCheckMark.isDisplayed();
			Reporter.log("Green Checkmark Icon is displayed on confirmation page");
		} catch (Exception e) {
			Assert.fail("Green Checkmark icon is not displayed on confirmation page");
		}
		return this;
	}

	/**
	 * Verify whether Thanks username is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkWhetherThanksUserNameTextDisplayedOrNot() {
		try {
			thanksUserName.isDisplayed();
			Reporter.log("Text Thanks Username displayed on confirmation page");
		} catch (Exception e) {
			Assert.fail("Text Thanks Username is not displayed on confirmation page");
		}
		return this;
	}

	/**
	 * Verify whether Order Complete text is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkWhetherOrderIsCompleteTextDisplayedOrNot() {
		try {
			textOrderInProgress.isDisplayed();
			Reporter.log("Text Plan change in progress is displayed on confirmation page");
		} catch (Exception e) {
			Assert.fail("Text Plan change in progress is not displayed on confirmation page");
		}
		return this;
	}

	/**
	 * Verify whether Order Details on confirmation page is expanded or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkWhetherOrderDetailsIsExpandedOrNot() {
		try {
			expandableOrderDetails.isDisplayed();
			Reporter.log("Order Details on confirmation page is expanded");
		} catch (Exception e) {
			Assert.fail("Order Details on confirmation page is not expanded");
		}
		return this;
	}

	/**
	 * Verify whether text Change Plan Details on confirmation page is displayed or
	 * not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkChangePlanDetailsText() {
		try {
			textChangePlanDetails.isDisplayed();
			if (textChangePlanDetails.getText().trim().equals("Plan change details"))
				Reporter.log("Text Plan Change Details on confirmation page is matched");
			else
				Assert.fail("Text Plan Change Details on confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Text Plan Change Details on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Order Details on confirmation page is expanded or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkOrderDetailsText() {
		try {
			textOrderDetails.isDisplayed();
			if (textOrderDetails.getText().trim().equals("Order details"))
				Reporter.log("Order Details on confirmation page is matched");
			else
				Assert.fail("Order Details on confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Order Details on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether text Your new plan on confirmation page is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkTextYourNewPlanText() {
		try {
			textYourNewPlan.isDisplayed();
			if (textYourNewPlan.getText().trim().equals("Your new plan:"))
				Reporter.log("Text Your New Plan on confirmation page is matched");
			else
				Assert.fail("Text Your New Plan on confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Text Your New Plan on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether text Autopay is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkAutopayTextNotDisplayed() {
		try {
			textAutopay.isDisplayed();
			Assert.fail("Autopay details displayed even when user not submitted with Autopay.");
		} catch (Exception e) {
			Reporter.log("Autopay details are not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Name on account is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation textNameOnAccount() {
		try {
			textNameOnAccount.isDisplayed();
			if ((textNameOnAccount.getText().trim().equals("Name on account:")))
				Reporter.log("Text Name on Account on confirmation page is displayed");
			else
				Assert.fail("Text Name on Account on confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Text aName on Account on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Name on account is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation nameOnAccount() {
		try {
			nameOnAccount.isDisplayed();
			if (!(nameOnAccount.getText().trim().equals("")))
				Reporter.log("Name on Account is displayed on confirmation page");
			else
				Assert.fail("Name on Account value on confirmation page is displayed as blank");
		} catch (Exception e) {
			Assert.fail("Value of Name on Account on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether T-Mobile account no. is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation textTMobileAccountNumber() {
		try {
			textTMobileAccountNumber.isDisplayed();
			if ((textTMobileAccountNumber.getText().trim().equals("T-Mobile account no.:")))
				Reporter.log("Text T-Mobile Account Number on confirmation page is matched");
			else
				Assert.fail("Text T-Mobile Account Number on confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Text T-Mobile Account Number on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether T-Mobile account no. is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation tMobileAccountNumber() {
		try {
			tMobileAccountNumber.isDisplayed();
			if (!(tMobileAccountNumber.getText().trim().equals("")))
				Reporter.log("T-Mobile BAN on confirmation page is matched");
			else
				Assert.fail("T-Mobile BAN on confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("T-Mobile BAN on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Plan name is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation verifyPlanName(String planName) {
		try {
			ratePlanName.isDisplayed();
			if ((ratePlanName.getText().trim().equals(planName)))
				Reporter.log("Rate Plan on confirmation page is matched");
			else
				Assert.fail("Rate Plan on confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Rate Plan on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Effective Date is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkEffectiveDate() {
		try {
			effectiveDate.isDisplayed();
			if (!(effectiveDate.getText().trim().equals("")))
				Reporter.log("Effective Date on confirmation page is displayed");
			else
				Assert.fail("Effective Date on confirmation page is blank");
		} catch (Exception e) {
			Assert.fail("Effective Date on confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether text New Monthly Total text is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkTextNewMonthlyTotal() {
		try {
			textNewMonthlyTotal.isDisplayed();
			if ((textNewMonthlyTotal.getText().trim().equals("New Monthly Total")))
				Reporter.log("Text New Monthly Total on Confirmation page is matched");
			else
				Assert.fail("Text New Monthly Total on Confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Text New Monthly Total on Confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether New Monthly Total value is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkNewMonthlyTotalCost(String cost) {
		try {
			newMonthlyTotalValue.isDisplayed();
			if ((newMonthlyTotalValue.getText().trim().equals(cost)))
				Reporter.log("New Monthly Total cost on Confirmation page is matched");
			else
				Assert.fail("New Monthly Total cost on Confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("New Monthly Total cost on Confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether text Next Steps is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkTextNextSteps() {
		try {
			textNextSteps.isDisplayed();
			if ((textNextSteps.getText().trim().equals("Next Steps:")))
				Reporter.log("Text Next Steps on Confirmation page is matched");
			else
				Assert.fail("Text Next Steps on Confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Text Next Steps on Confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether header text for Netflix is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkHeaderTextForNetflixSection() {
		try {
			netflixHeaderText.isDisplayed();
			if ((netflixHeaderText.getText().trim().equals("Start enjoying Netflix at no extra charge.")))
				Reporter.log("Netflix header Text on Confirmation page is matched");
			else
				Assert.fail("Netflix header Text on Confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Netflix header Text on Confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Netflix copy text is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkTextForNetflixSection() {
		try {
			String msg = "We’ll cover a standard 2-screen subscription for almost any device, even TV. Simply add Netflix with Family Allowance to your account and connect to netflix.";
			nextflixText.isDisplayed();
			if (nextflixText.getText().trim().equals(msg))
				Reporter.log("Netflix header Text on Confirmation page is matched");
			else
				Assert.fail("Netflix header Text on Confirmation page is mismatched");
		} catch (Exception e) {
			Assert.fail("Netflix header Text on Confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Manage Netflix CTA is displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkManageNetflixCTA() {
		try {
			manageNetflixCTA.isDisplayed();
			Reporter.log("Manage Netflix CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Manage Netflix CTA is not displayed");
		}
		return this;
	}

	/**
	 * Click on Netflix CTA
	 * 
	 * @return
	 */
	public ChangePlansConfirmation clickOnNetflixCTA() {
		try {
			manageNetflixCTA.click();
			Reporter.log("Netflix CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Netflix CTA is not displayed");
		}
		return this;
	}

	/**
	 * Verify whether Back Nav arrow displayed or not
	 * 
	 * @return
	 */
	public ChangePlansConfirmation checkBackNavArrow() {
		try {
			backNavArrow.isDisplayed();
			Assert.fail("Back Nav Arrow is displayed");
		} catch (Exception e) {
			Reporter.log("Back Nav Arrow is not displayed");
		}
		return this;
	}

}
