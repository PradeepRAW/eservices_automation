package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class MaintenancePage extends CommonPage{
	
	@FindBy(css = "button.PrimaryCTA")
	private WebElement payBillButton;
	
	private final String pageUrl = "/billandpay/maintenance";
	
	/**
	 * 
	 * @param webDriver
	 */
	public MaintenancePage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public MaintenancePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}
	
	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public MaintenancePage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(payBillButton));
			Reporter.log("Bill And Pay Maintenance Page is loaded ");
		} catch (Exception e) {
			Assert.fail("Failed to load Bill And Pay Maintenance Page");
		}
		return this;
	}
	
	/**
	 * Verify that the Pay bill button color
	 * 
	 * @throws InterruptedException
	 */
	public MaintenancePage verifyPayNowButtonBackgroundColor() {
		try {
			waitFor(ExpectedConditions.visibilityOf(payBillButton));
			String bgColor = payBillButton.getCssValue("background-color");
			if(bgColor.contains("rgb(226, 0, 116)"))
			{
				Reporter.log("Pay bill bg color is Pink");
			}
			//if bgColor is rgb(226, 0, 116) then it's pink color
			Reporter.log("Bill And Pay Summary Page is loaded ");
		} catch (Exception e) {
			Assert.fail("Failed to load Bill And Pay Summary Page");
		}
		return this;
	}

}
