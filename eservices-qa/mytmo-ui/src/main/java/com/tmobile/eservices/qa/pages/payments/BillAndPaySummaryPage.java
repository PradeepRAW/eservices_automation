package com.tmobile.eservices.qa.pages.payments;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;

import io.appium.java_client.AppiumDriver;

/**
 * @author pshiva
 *
 */
public class BillAndPaySummaryPage extends CommonPage {

	@FindBy(css = "[class='bb-historical-summary-amount']")
	private WebElement historicalSummaryAmt;

	@FindBy(css = "[class='bb-historical-summary-amount'] span[class='integer']")
	private WebElement balanceDueAmountInteger;

	@FindBy(css = "[class='bb-historical-summary-amount'] span[class='decimal']")
	private WebElement balanceDueAmountDecimal;

	@FindBy(css = "a.bb-pay-btn")
	private WebElement btnMakeAPayment;

	@FindBy(css = "div.bb-shortcut-title")
	private List<WebElement> bbShortcutTitles;

	@FindBy(css = "a.bb-payment-options-link")
	private WebElement moreOptions;

	@FindBy(css = "div.bb-shortcuts")
	private WebElement bbShortCutsSection;

	@FindBy(css = "div.bb-shortcut-description")
	private List<WebElement> bbShortcutDescriptions;

	@FindBy(xpath = "//span[contains(text(),'You used')]")
	private WebElement usageSection;

	@FindBy(css = "bb-slider-pagination:not([class='d-none']) div.bb-pagination div")
	private List<WebElement> mobilePageNation;

	@FindBy(linkText = "See all usage")
	private WebElement seeAllUsageLink;

	@FindBy(xpath = "//div[contains(@class,'bb-charge-summary')]//h3[@class='bb-charge-title']")
	private List<WebElement> chargeCategoriesOrLines;

	@FindBy(xpath = "//div[contains(@class,'d-block d-md-none')]//h3[@class='bb-charge-title']")
	private List<WebElement> mobileCategoriesOrLines;

	@FindBy(css = "[class='col-lg-12 bb-charge-summary d-none d-md-block']")
	private List<WebElement> viewByLineNames;

	@FindBy(css = "a.bb-download-btn")
	private WebElement downloadPDFBtn;

	@FindBy(css = "h3.bb-title")
	private WebElement downloadPdfModal;

	@FindBy(css = "button.bb-button--summary")
	private WebElement downloadSummaryBillBtn;

	@FindBy(css = "button.bb-button--detailed")
	private WebElement downloadDetailedBillBtn;

	@FindBy(css = "img.bb-close-image")
	private WebElement closeModal;

	@FindBy(linkText = "Make a payment")
	private WebElement makePaymentButton;

	@FindBy(css = "div.popup-content-inner a.body-copy-highlight")
	private List<WebElement> billCycleMonths;

	@FindBy(css = "div.bb-month-total")
	private WebElement monthTotalHeader;

	// @FindBy(css = "div.bb-type-selector.bb-line-selector")
	@FindBy(xpath = "//div[@class='bb-charges-selector row']/button[contains(text(),'View by line')]")
	private WebElement viewByLineTab;

	@FindBy(xpath = "//div[@class='bb-charges-selector row']/button[contains(text(),'View by category')]")
	private WebElement viewBycategorytab;

	@FindBy(css = "a.bb-pay-btn")
	private WebElement pageLoadElement;

	@FindBy(linkText = "View Historical Bills")
	private WebElement viewHistoricalBillsLink;

	@FindBy(css = "div.bb-historical-summary-intro")
	private WebElement previousBillSumamry;

	private final String pageUrl = "/summary";

	@FindAll({ @FindBy(css = "div.bb-charges-total.text-right"), @FindBy(css = "div.col-4.text-right.no-padding") })
	private WebElement totalChargeAmount;

	@FindBy(css = "span.bb-charge-amount")
	private List<WebElement> amountCells;

	@FindBy(css = "span.mat-slide-toggle-link")
	private WebElement showMeMoreDetailsLink;

	@FindBy(css = "div.row.col-lg-8 > div.bb-charge-detail")
	private List<WebElement> chargeDetails;

	@FindBy(css = "a#paperlessStatusLink")
	private WebElement paperLessBillingMobile;

	@FindBy(css = "div.Paperless billing")
	private WebElement paperLessBilling;

	@FindBy(xpath = "//div[contains(@class,'bb-charge-summary')]//h3[contains(text(),'Plans')]")
	private WebElement plans;

	@FindBy(xpath = "//div[contains(@class,'bb-charge-summary')]//h3[contains(text(),'Plans')]")
	private WebElement mobileplans;

	@FindBy(xpath = "//div[contains(@class,'bb-charge-summary')]//h3[contains(text(),'One-time charges')]")
	private WebElement onetimecharges;

	@FindBy(xpath = "//div[contains(@class,'bb-charge-summary')]//h3[contains(text(),'One-time charges')]")
	private WebElement mobileonetimecharges;

	@FindBy(css = "span#switch_account")
	private WebElement switchAccount;

	@FindBy(css = "li#account0")
	private List<WebElement> multiAccounts;

	@FindBy(css = "a.bb-payment-options-link")
	private WebElement morePaymentsOptionsLink;

	@FindBy(css = "")
	private WebElement billForNewUser;
	@FindBy(css = "")
	private WebElement billendDate;

	@FindBy(css = "div.bb-good-to-know-card-content div.bb-card-sub-title.row span")
	private List<WebElement> goodToKnowContentTitle;

	@FindBy(css = "div.bb-good-to-know-card-content div.bb-card-link a")
	private List<WebElement> goodToKnowContentLink;

	@FindBy(css = "div.bb-historical-summary-message a")
	private WebElement autopaySignupLinkLiveArea;

	@FindBy(css = "div.bb-historical-summary-intro")
	private WebElement historicalSummaryIntro;

	@FindBy(css = "div.bb-historical-summary-intro span")
	private WebElement customernameinhistoricalSummaryIntro;

	@FindBy(css = "div.bb-historical-summary-message")
	private WebElement historicalSummaryMessage;

	@FindBy(css = "div.bb-charges-title")
	private WebElement bbChargesTitle;

	@FindBy(css = "div.bb-charges-what-changed-message")
	private WebElement bbWhatChangedMessage;

	@FindBy(xpath = "//span[contains(@id,'cust_msisdn')]  [contains(@class,'bb-charge-number')]")
	private List<WebElement> associatedLine;

	@FindBy(css = "div.bb-month-total")
	private WebElement bbMonthTotal;

	@FindBy(css = "div.bb-price span.integer")
	private WebElement totalDue;

	@FindBy(css = "div.bb-due-date")
	private WebElement bbDuedate;

	@FindBy(css = "img.bb-loader-icon")
	private WebElement ajaxSprinner;

	@FindBy(xpath = "//span[contains(@id,'cust_msisdn')]")
	private List<WebElement> viewMsisdinByLine;

	@FindBy(css = "#acsMainInvite .acsCloseButton--container>span>a")
	private List<WebElement> mobileFeedBackAlert;

	/**
	 * 
	 * @param webDriver
	 */
	public BillAndPaySummaryPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public BillAndPaySummaryPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	public boolean verifyContainsPageUrl(String urlPath) {
		boolean bFlag = false;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(), 1);
			wait.until(ExpectedConditions.urlContains(urlPath));
			bFlag = true;
		} catch (Exception e) {
			Reporter.log("URL containing " + urlPath + " page not loaded " + e.getMessage());
		}
		return bFlag;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public BillAndPaySummaryPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(makePaymentButton));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img.loader-icon")));
			verifyPageUrl();
			if (mobileFeedBackAlert.size() == 1) {
				mobileFeedBackAlert.get(0).click();
			}
			Reporter.log("Bill And Pay Summary Page is loaded ");
		} catch (Exception e) {
			Assert.fail("Failed to load Bill And Pay Summary Page");
		}
		return this;
	}

	/**
	 * click 'Make a payment' button
	 */
	public BillAndPaySummaryPage clickMakeAPaymentLinkOnBBPage() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(btnMakeAPayment));
			btnMakeAPayment.click();
			Reporter.log("Clicked on 'Make a payment' button.");
		} catch (Exception btnNotClicked) {
			Assert.fail("Could NOT click on button 'Make a payment' - Step FAILED");
		}
		return this;
	}

	public BillAndPaySummaryPage verifyAutoPayShortcut() {
		try {
			isBBShortcutDisplayed("Autopay");
			Reporter.log("Verfied the AUtopay Shortcut");
		} catch (Exception e) {
			Verify.fail("Autopay Shortcut is not displayed");
		}
		return this;

	}

	public BillAndPaySummaryPage clickAutopayShortcut() {
		try {
			clickBBShortcut("Autopay");
			Reporter.log("Clicked on Autopay Shortcut");
		} catch (Exception e) {
			Assert.fail("Failed to click on Auto pay Shortcut");
		}
		return this;
	}

	public BillAndPaySummaryPage verifyPaymentArrangementShortcut() {
		try {
			isBBShortcutDisplayed("Payment arrangements");
			Reporter.log("Payment Arrangement Shortcut is displayed");
		} catch (Exception e) {
			Assert.fail(
					"Payment Arrangement Shortcut is not displayed Or This data may not  eligible for Payment Arrangement");
		}
		return this;
	}

	/**
	 * Click On Payment Arrangement Shortcut
	 * 
	 * @return
	 */
	public BillAndPaySummaryPage clickPaymentArrangementShortcut() {
		try {
			clickBBShortcut("Payment arrangements");
		} catch (Exception e) {
			Assert.fail("Failed to Click");
		}
		return this;
	}

	public BillAndPaySummaryPage verifyJumpOnDemandLeaseShortcut() {
		try {
			isBBShortcutDisplayed("JUMP! on Demand leases");
			Reporter.log("Verified the Jump on Demand leases");
		} catch (Exception e) {
			Assert.fail("Fail to verify Jump on Demand Lease Shortcut");
		}
		return this;
	}

	public BillAndPaySummaryPage clickJumpOnDemandShortcut() {
		try {
			clickBBShortcut("JUMP! on Demand leases");
			Reporter.log("Clicked on Jump on Demand leases");
		} catch (Exception e) {
			Assert.fail("Fail to click on Jump on Demand leases");
		}
		return this;
	}

	public BillAndPaySummaryPage verifyAccountHistoryShortcut() {
		try {
			isBBShortcutDisplayed("Account history");
			Reporter.log("Verified the Account history");
		} catch (Exception e) {
			Assert.fail("Fail to verify Account history");
		}
		return this;
	}

	public BillAndPaySummaryPage clickAccountHistoryShortcut() {
		try {
			clickBBShortcut("Account history");
			Reporter.log("Clicked on account history");
		} catch (Exception e) {
			Assert.fail("Fail to click on Account history");
		}
		return this;
	}

	private boolean isBBShortcutDisplayed(String title) {
		if (!(getDriver() instanceof AppiumDriver)) {
			for (WebElement shortcut : bbShortcutTitles) {
				if (shortcut.getText().contains(title)) {
					return true;
				}
			}
		}
		return false;
	}

	private BillAndPaySummaryPage clickBBShortcut(String title) {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			moreOptions.click();
		}
		checkPageIsReady();
		for (WebElement shortcut : bbShortcutTitles) {
			if (shortcut.getText().contains(title)) {
				waitFor(ExpectedConditions.visibilityOf(shortcut));
				clickElementWithJavaScript(shortcut);
				break;
			}
		}
		return this;
	}

	public BillAndPaySummaryPage clickPageNation(String option) {
		if (getDriver() instanceof AppiumDriver) {
			for (WebElement webElement : mobilePageNation) {
				seeAllUsageLink.click();
				break;
			}
		}
		return this;
	}

	/**
	 * verify usage section is displayed
	 * 
	 * @return boolean
	 */
	public BillAndPaySummaryPage verifyUsageSection() {
		try {
			if (!(getDriver() instanceof AppiumDriver)) {
				usageSection.isDisplayed();
			}
			Reporter.log("Verified the Usage Section");
		} catch (Exception e) {
			Verify.fail("Fail to verify Usage section");
		}
		return this;
	}

	/**
	 * click on 'see all usage' link
	 */
	public BillAndPaySummaryPage clickSeeAllUsageLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : mobilePageNation) {
					moveToElement(webElement);
					webElement.click();
					if (seeAllUsageLink.isDisplayed()) {
						seeAllUsageLink.click();
					}
					break;
				}
			} else {
				seeAllUsageLink.click();
			}
			Reporter.log("Clicked on See all usage link");
		} catch (Exception e) {
			Assert.fail("Fail to click on see all usage link");
		}
		return this;
	}

	/**
	 * click on any category under charges
	 */
	public BillAndPaySummaryPage clickChargeCategory() {
		try {
			clickElement(chargeCategoriesOrLines);
			Reporter.log("Clicked on charge Category");
		} catch (Exception e) {
			Assert.fail("Failed to click on charge category");
		}
		return this;
	}

	/**
	 * click on any category under charges
	 */
	public BillAndPaySummaryPage clickPalns() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(mobileplans);
				mobileplans.click();
			} else {
				plans.click();
				Reporter.log("Clicked plans");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click plans");
		}
		return this;
	}

	public BillAndPaySummaryPage clickonetimecharges() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(mobileonetimecharges);
				clickElement(mobileonetimecharges);
			} else {
				moveToElement(onetimecharges);
				clickElement(onetimecharges);
			}
			Reporter.log("Clicked one-time charges");
		} catch (Exception e) {
			Assert.fail("Failed to click one-timecharges");
		}
		return this;
	}

	public BillAndPaySummaryPage verifyEquipmentInstallmentPlansShortcut() {
		try {
			isBBShortcutDisplayed("Equipment installment plans");
			Reporter.log("Verified the Equipment Installment Plan");
		} catch (Exception e) {
			Assert.fail("Fail to verify the Equipment Installment plan");
		}
		return this;
	}

	public BillAndPaySummaryPage clickEquipmentInstallmentPlansShortcut() {
		try {
			clickBBShortcut("Equipment installment plans");
			Reporter.log("Clicked on Equipment Installment plan");
		} catch (Exception e) {
			Assert.fail("Fail to click Equipment Installment plan");
		}
		return this;

	}

	/**
	 * click category by category name
	 * 
	 * @param categoryName
	 */
	public void clickChargeCategory(String categoryName) {
		for (WebElement category : chargeCategoriesOrLines) {
			if (category.getText().equals(categoryName)) {
				category.click();
				break;
			}
		}
	}

	/**
	 * click balance category
	 * 
	 * @param categoryName
	 */
	public void clickBalanceCategory() {
		if (getDriver() instanceof AppiumDriver) {
			for (WebElement category : mobileCategoriesOrLines) {
				if (category.getText().equals("Balance") || category.getText().equals("Balance forward")) {
					category.click();
					break;
				}
			}
		} else {
			for (WebElement category : chargeCategoriesOrLines) {
				if (category.getText().equals("Balance") || category.getText().equals("Balance forward")) {
					category.click();
					break;
				}

			}
		}
	}

	public BillAndPaySummaryPage clickDownloadPDF() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(downloadPDFBtn));
			moveToElement(downloadPDFBtn);
			// downloadPDFBtn.click();
			clickElementWithJavaScript(downloadPDFBtn);
			Reporter.log("Clicked on Download PDF");
		} catch (Exception e) {
			Assert.fail("Fail to click Download PDF");
		}
		return this;
	}

	public BillAndPaySummaryPage verifyDownloadPdfModal() {
		try {
			checkPageIsReady();
			downloadPdfModal.isDisplayed();
			downloadSummaryBillBtn.isDisplayed();
			downloadDetailedBillBtn.isDisplayed();
			Reporter.log("Verified Download PDF MOdal");
		} catch (Exception e) {
			Assert.fail("Fail to verify Download PDF Modal");
		}

		return this;
	}

	public void clickCloseDownloadPdfModal() {
		try {
			closeModal.click();
			Reporter.log("Clicked on close download PDF Modal");
		} catch (Exception e) {
			Assert.fail("Fail to click on close download PDF Modal");
		}
	}

	/**
	 * click view by line selector
	 */
	public BillAndPaySummaryPage clickViewByLineSelector() {
		try {
			checkPageIsReady();
			scrollToElement(viewBycategorytab);
			checkPageIsReady();
			retryingFindClick(viewByLineTab);
			// clickElementWithJavaScript(viewByLineTab);
			// viewByLineTab.click();
			Reporter.log("Clicked on view By Line Tab");
		} catch (Exception e) {
			Verify.fail("Fail to click on view By Line Tab");
		}
		return this;
	}

	/**
	 * click view by line selector
	 */
	public BillAndPaySummaryPage clickViewBycategory() {
		try {
			checkPageIsReady();
			scrollToElement(viewByLineTab);
			checkPageIsReady();
			clickElementWithJavaScript(viewByLineTab);
			// viewByLineTab.click();
			Reporter.log("Clicked on view By Line Tab");
		} catch (Exception e) {
			Verify.fail("Fail to click on view By Line Tab");
		}
		return this;
	}

	/**
	 * verify nick names are displayed for each line
	 */
	public BillAndPaySummaryPage verifyNamesOnAccountLines() {
		try {
			Thread.sleep(3000);
			checkPageIsReady();
			for (WebElement line : viewByLineNames) {

				scrollToElement(line);
				checkPageIsReady();
				line.isDisplayed();

			}
			Reporter.log("Account/Line names are displayed for all lines");
		} catch (Exception e) {
			Verify.fail("Account/Line name is not found");
		}
		return this;
	}

	/**
	 * click on previous billing cycle
	 * 
	 * @param month
	 * @param year
	 */
	public void clickPreviousBillCycle(String month, String year) {
		try {
			moveToElement(viewHistoricalBillsLink);
			viewHistoricalBillsLink.click();
			waitFor(ExpectedConditions.visibilityOf(billCycleMonths.get(0)));
			for (WebElement webElement : billCycleMonths) {
				if (webElement.getText().contains(month) && webElement.getText().contains(year)) {
					moveToElement(webElement);
					webElement.click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Previous month cycle is not selected");
		}
	}

	/**
	 * Verify Previous Bill Summary
	 * 
	 * @return
	 */
	public BillAndPaySummaryPage verifyPreviousBillSummary() {
		try {
			waitFor(ExpectedConditions
					.invisibilityOfElementLocated(By.cssSelector("div[src='assets/img/loader.svg']")));
			waitFor(ExpectedConditions.visibilityOf(previousBillSumamry));
			previousBillSumamry.isDisplayed();
			Reporter.log("Previous bill summary is displayed");
		} catch (Exception e) {
			Assert.fail("Previous bill summary is not displayed");
		}
		return this;
	}

	public BillAndPaySummaryPage verifyAmountTotalInCurrentCharges() {
		try {
			checkPageIsReady();
			Double amountSum = 0.0;
			Double negativeAmounts = 0.0;
			String totalAmount = null;
			Double total;
			for (WebElement amount : amountCells) {
				if (amount.isDisplayed()) {
					String amountTxt = amount.getText();
					checkPageIsReady();
					if (amountTxt.contains("-")) {
						if (amountTxt.length() != 0)
							negativeAmounts = negativeAmounts + Double.parseDouble(amountTxt.replaceAll("[^.0-9]", ""));
					} else {
						if (amountTxt.length() != 0)
							amountSum = amountSum + Double.parseDouble(amountTxt.replaceAll("[^.0-9]", ""));
					}
				}

			}

			amountSum = amountSum - negativeAmounts;
			totalAmount = totalChargeAmount.getText();
			if (totalAmount.contains("-")) {

				Verify.assertTrue(amountSum.toString().contains("-"));
			} /*
				 * else {
				 * 
				 * Verify.assertFalse(amountSum.toString().contains("-")); }
				 */
			totalAmount = totalAmount.replaceAll("[^.0-9]", "");
			total = Double.parseDouble(totalAmount);
			Verify.assertEquals(Double.compare(
					new BigDecimal(amountSum).setScale(2, RoundingMode.HALF_UP).abs().doubleValue(), total), 0);
			Reporter.log("Amount math is correctly displayed to Total amount");
		} catch (Exception e) {
			Verify.fail("Fail: Amount math doesn't equals Total amount");
		}
		return this;
	}

	/**
	 * click on show more details
	 */
	public void clickShowMoreDetails() {
		try {
			checkPageIsReady();
			showMeMoreDetailsLink.click();
			Reporter.log("clicked on Show me more details link");
		} catch (Exception e) {
			Assert.fail("Fail to click on Show me more details link");
		}
	}

	/**
	 * verify that charge category details are displayed
	 */
	public void verifyChargeCategoryDetails() {
		try {
			checkPageIsReady();
			for (WebElement details : chargeDetails) {
				if (!details.isDisplayed()) {
					Assert.fail("Charge details are not displayed");
				}
			}
			Reporter.log("Charge details are displayed");
		} catch (Exception e) {
			Assert.fail("Charge details are not displayed");
		}
	}

	/**
	 * verify that charge category details are collapsed
	 */
	public void verifyChargeCategoryDetailsCollapsed() {
		try {
			checkPageIsReady();
			for (WebElement details : chargeDetails) {
				if (details.isDisplayed()) {
					Assert.fail("Charge details are not collapsed");
				}
			}
			Reporter.log("Charge details are collapsed");
		} catch (Exception e) {
			Assert.fail("Charge details are not collapsed");
		}
	}

	/**
	 * verify view By category tab selected by default
	 */
	public void verifyViewbycategorySelectedBydefault() {
		try {
			checkPageIsReady();
			viewBycategorytab.getAttribute("class").contains("");

			Reporter.log(" view By category tab selected by default");
		} catch (Exception e) {
			Assert.fail("view By category tab not selected by default");
		}
	}

	/**
	 * click paperless billing shortcut
	 */
	public BillAndPaySummaryPage clickPaperLessBillingShortCut() {
		try {
			clickBBShortcut("Paperless billing");
			Reporter.log("Clicked on Paperless billing");
		} catch (Exception e) {
			Assert.fail("Fail to click on Paperless billing");
		}
		return this;

	}

	/**
	 * click paperless billing shortcut
	 */
	public BillAndPaySummaryPage clickBalanceShortcut() {
		try {
			clickBBShortcut("Balance");
			Reporter.log("Clicked on Balance");
		} catch (Exception e) {
			Assert.fail("Fail to click on Balance");
		}
		return this;

	}

	/**
	 * click on previous billing cycle
	 * 
	 * @param month
	 * @param year
	 */
	public void clickViewHistoricalBillsLink() {
		try {
			clickElement(viewHistoricalBillsLink);
			// viewHistoricalBillsLink.click();
			Reporter.log("Historical bills link is clicked");
		} catch (Exception e) {
			Assert.fail("View historical bills link is not found.");
		}
	}

	public void verifyBalanceonBllingPageMatchwithHomePage(String Homebal) {
		String bal = "";
		try {
			if (historicalSummaryAmt.isDisplayed()) {
				bal = balanceDueAmountInteger.getText() + "." + balanceDueAmountDecimal.getText();

				Verify.assertTrue(Homebal.replace("$", "").equals(bal));
				Reporter.log("Balance Due displayed on Bill and pay Page :: " + bal);
			}

		} catch (Exception balDueNotCaptured) {
			Reporter.log("Balance Due on Home Page could NOT be captured. " + balDueNotCaptured.getMessage());

		}

	}

	public void clickSwitchAccount() {
		try {
			checkPageIsReady();
			clickElement(switchAccount);
			Reporter.log("switchAccount is clicked");
		} catch (Exception e) {
			Assert.fail("switchAccount link is not found.");
		}
	}

	/**
	 * click Benefits statement shortcut
	 */
	public BillAndPaySummaryPage clickBenefitsstatementShortCut() {
		try {
			clickBBShortcut("Benefits statement");
			Reporter.log("Clicked on Benefits statement");
		} catch (Exception e) {
			Assert.fail("Fail to click on Benefits statement");
		}
		return this;

	}

	/**
	 * click more payment options link
	 */
	public BillAndPaySummaryPage clickMorePaymentOptionsLink() {
		try {
			clickElement(morePaymentsOptionsLink);
			Reporter.log("Clicked on more payment options link");
		} catch (Exception e) {
			Assert.fail("Fail to click on more payment options link");
		}
		return this;

	}

	/**
	 * verify payment options displayed
	 */
	public BillAndPaySummaryPage verifyPaymentOptionsDispalyed() {
		try {
			Assert.assertFalse(bbShortcutTitles.isEmpty(), "payment options not displayed");
			Reporter.log("payment options displayed");
		} catch (Exception e) {
			Assert.fail("Fail to dispaly payment options");
		}
		return this;

	}

	/**
	 * verify BillandPay page for new user
	 */
	public BillAndPaySummaryPage verifyNoBillGeneratedMessageforNewUser(long diffinDays) {
		try {
			Assert.assertTrue(
					billForNewUser.getText().contains(
							"Your bill has not been generated yet. Please check again in" + diffinDays + "days"),
					"Bill Not generaed for Ne user message not displayed");

			Reporter.log("Bill Not generaed for Ne user message is displayed");
		} catch (Exception btnNotClicked) {
			Assert.fail("Bill Not generaed for Ne user message not displayed");
		}
		return this;
	}

	/**
	 * get Bill due date
	 * 
	 * @return
	 * 
	 */
	public long getDiffBetweendueDateandCurrentDate() {

		Date temp = null;
		Date billCycleEndDate = null;
		Date currentDate = null;
		long diffinDays = 0;
		SimpleDateFormat standardFormat = new SimpleDateFormat("yyyy-MM-DD");

		// SimpleDateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy");

		try {
			// temp = inputFormat.parse(billendDate.getText());
			String billDueDate = standardFormat.format(billendDate.getText());
			String todayDate = standardFormat.format(Calendar.getInstance().getTime());
			billCycleEndDate = standardFormat.parse(billDueDate);
			currentDate = standardFormat.parse(todayDate);

			long diffInMillies = Math.abs(billCycleEndDate.getTime() - currentDate.getTime());
			diffinDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		} catch (ParseException e) {
			Assert.fail("Test failed in date formatting");
		}

		return diffinDays;

	}

	public void verifyBillandpayPageLoadedforMultiAccounts() {
		try {
			clickSwitchAccount();
			Thread.sleep(3000);
			for (int i = 0; i <= multiAccounts.size(); i++) {

				checkPageIsReady();
				multiAccounts.get(i).click();
				HomePage homePage = new HomePage(getDriver());
				homePage.verifyPageLoaded();
				homePage.clickBillingLink();
				verifyPageLoaded();
				clickSwitchAccount();

			}
			Reporter.log("BillandPay page loaded for All Accounts");
		} catch (Exception e) {
			Assert.fail("BillandPay page not loaded for All Accounts");
		}
	}

	public void verifyGoodToKnowSectionsForUsage() {
		try {
			checkPageIsReady();
			verifyElementBytext(goodToKnowContentTitle, "You used");
			verifyElementBytext(goodToKnowContentLink, "See all usage");
			clickGoodToKnowLink("See all usage");
			UsagePage usage = new UsagePage(getDriver());
			usage.verifyPageLoaded();
			getDriver().navigate().back();

			Reporter.log("Good To Know Sections For Usage dispalyed");
		} catch (Exception e) {
			Assert.fail("Good To Know Sections For Usage not dispalyed");
		}
	}

	public void verifyGoodToKnowSectionsForAutoPay() {
		try {
			checkPageIsReady();
			verifyElementBytext(goodToKnowContentTitle, "You saved");
			verifyElementBytext(goodToKnowContentLink, "Sign up for AutoPay and save");
			clickGoodToKnowLink("Sign up for AutoPay and save");
			AutoPayPage autopaypage = new AutoPayPage(getDriver());
			autopaypage.verifyPageLoaded();
			getDriver().navigate().back();

			Reporter.log("Good To Know Sections For Autopay dispalyed");
		} catch (Exception e) {
			Assert.fail("Good To Know Sections For Autopay not dispalyed");
		}
	}

	public void verifyGoodToKnowSectionsForBenefits() {
		try {
			checkPageIsReady();
			verifyElementBytext(goodToKnowContentTitle, "Un-carrier benefits");
			verifyElementBytext(goodToKnowContentLink, "See more");
			clickGoodToKnowLink("See more");
			checkPageIsReady();
			getDriver().getCurrentUrl().contains("benefits");

			Reporter.log("Good To Know Sections For Usage dispalyed");
		} catch (Exception e) {
			Assert.fail("Good To Know Sections For Usage not dispalyed");
		}
	}

	public void clickGoodToKnowLink(String link) {
		try {
			checkPageIsReady();
			clickElementBytext(goodToKnowContentLink, link);

			Reporter.log("Good To Know Sections For Usage dispalyed");
		} catch (Exception e) {
			Assert.fail("Good To Know Sections For Usage not dispalyed");
		}
	}

	/**
	 * verify historical summary intorduction
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyHistoricalSummaryIntroduction() {
		try {
			historicalSummaryIntro.isDisplayed();
			historicalSummaryIntro.getText().replaceAll("[']", "").contains("Here is your bill for");// Here's
																										// your
																										// bill
																										// for

			customernameinhistoricalSummaryIntro.getAttribute("id").equals("cust_first");
			Reporter.log("Verfied historical summary intorduction");
		} catch (Exception e) {
			Verify.fail("historical summary intorduction is not displayed");
		}
		return this;

	}

	/**
	 * verify what changed in this month section
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyWhatChangedinThisMonthSection() {
		try {
			bbChargesTitle.isDisplayed();
			bbWhatChangedMessage.isDisplayed();
			bbChargesTitle.getText().contains("What changed this month?");
			Reporter.log("Verfied what changed in this month section");
		} catch (Exception e) {
			Verify.fail("what changed in this month section is not displayed");
		}
		return this;

	}

	/**
	 * verify live Area Messaging when Autopay is OFF
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyLiveAreaMessagingwhenAutopayOFF() {
		try {
			historicalSummaryMessage.isDisplayed();

			historicalSummaryMessage.getText().contains(
					"Sign up for Autopay today!. You'll never have to worry about late fees or missed payments again. Click here to sign up");
			Reporter.log("Verfied live Area Messaging when Autopay is OFF");
		} catch (Exception e) {
			Verify.fail("live Area Messaging when Autopay is OFF is not displayed");
		}
		return this;

	}

	/**
	 * verify live Area Messaging when Autopay is ON
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyLiveAreaMessagingwhenAutopayON() {
		try {
			historicalSummaryMessage.isDisplayed();

			historicalSummaryMessage.getText().contains(
					"Go paperless and get your bill online or through our app. Save trees, save stamps. Click here to enroll.");
			Reporter.log("Verfied live Area Messaging when Autopay is on");
		} catch (Exception e) {
			Verify.fail("live Area Messaging when Autopay is on is not displayed");
		}
		return this;

	}

	/**
	 * click autopay Signup Link Live Area
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage clickAutopaySignupLinkonLiveAreaMessaging() {
		try {
			autopaySignupLinkLiveArea.isDisplayed();

			autopaySignupLinkLiveArea.click();
			Reporter.log("clicked  autopay Signup Link Live Area");
		} catch (Exception e) {
			Verify.fail(" autopay Signup Link Live Area is not displayed");
		}
		return this;

	}

	/**
	 * verify live Area Messaging when Autopay is OFF
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyAssociatedLineDisplayedonInteractiveSummaryOptions(MyTmoData myTmoData) {
		try {
			for (WebElement element : associatedLine) {
				if (element.getText().replaceAll("[^0-9]", "").contains(myTmoData.getLoginEmailOrPhone())) {
					break;
				}
			}

			Reporter.log("Verfied associated Line is dispalyed on Interactive Summary Options");
		} catch (Exception e) {
			Verify.fail("associated Line not dispalyed on Interactive Summary Options");
		}
		return this;

	}

	/**
	 * verify live Area Messaging when Autopay is OFF
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyMakePaymentButtonActiveforPastDueCustomer() {
		try {
			makePaymentButton.getAttribute("class").contains("bb-pay-btn mx-auto d-block active");

			Reporter.log("Verfied make payment button active for pastdue customer");
		} catch (Exception e) {
			Verify.fail("make payment button is not active for pastdue customer");
		}
		return this;

	}

	/**
	 * verify live Area Messaging when Autopay is OFF
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyMakePaymentButtonInActiveforZeroBalanceCustomer() {
		try {
			makePaymentButton.getAttribute("class").contains("bb-pay-btn mx-auto d-block active");

			Reporter.log("Verfied make payment button inactive for zero balance customer");
		} catch (Exception e) {
			Verify.fail("make payment button is not active for inactive for zero balance customer");
		}
		return this;

	}

	/**
	 * verify total balance section when total balance less than zero
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyTotalBalanceSectionwhenBalanceLessthanZero() {
		try {

			bbMonthTotal.getText().contains("You have a credit amount of");
			totalDue.getAttribute("class").contains("integer integer--isNegative");
			bbDuedate.getText().contains("No payment is due");
			Reporter.log("Verfied total balance section when total balance less than zero");
		} catch (Exception e) {
			Verify.fail("total balance section when total balance less than zero is not verified");
		}
		return this;

	}

	/**
	 * verify due date section when total balance more than zero due immediately
	 * 
	 * @return
	 */

	public BillAndPaySummaryPage verifyDueDateSectionwhenBalanceMorethanZeroDueimmediately() {
		try {

			bbDuedate.getText().replaceAll("[0-9,.]", "").contains("Due immediately: $ was due");
			Reporter.log("Verfied due date section when total balance more than zero due immediatel");
		} catch (Exception e) {
			Verify.fail("due date section when total balance more than zero due immediatel is not verified");
		}
		return this;

	}

	public BillAndPaySummaryPage verifyAutopaySectionwhenAutopayscheduled() {
		try {

			isBBShortcutDisplayed("Autopay on");
			isBBShortdescriptionDisplayed("You're saving $ each month");
		} catch (Exception e) {
			Verify.fail("Auto pay alert component not found");
		}
		return this;
	}

	private boolean isBBShortdescriptionDisplayed(String title) {
		for (WebElement shortcut : bbShortcutDescriptions) {
			if (shortcut.getText().replaceAll("[0-9]", "").contains(title)) {
				return true;
			}
		}
		return false;
	}

	public void verifyAjaxSpinnerInvisible() {
		try {
			waitFor(ExpectedConditions.visibilityOf(ajaxSprinner));
			waitFor(ExpectedConditions.invisibilityOf(ajaxSprinner));
			Reporter.log("Ajax Spinner is invisible");
		} catch (Exception e) {
			Assert.fail("Page has not loaded");
		}
	}

	/**
	 * verify text in ViewByLine for Signed in Msisdin
	 */
	public BillAndPaySummaryPage verifySingleLineMsisdin(String msisdinNumber) {
		try {
			checkPageIsReady();
			for (WebElement webElement : viewMsisdinByLine) {
				// waitFor(ExpectedConditions.visibilityOf(webElement));
				if (webElement.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
						.equalsIgnoreCase(msisdinNumber)) {
					Reporter.log("Verified that the displayed msisdin is matching w.r.to Signed in Msisdin : "
							+ msisdinNumber);
				}
			}
		} catch (Exception e) {
			Verify.fail("Signed in Msisdin by Categorylines are not displayed");
		}
		return this;
	}

	public void navigateToMaintenancePage() {

	}

}
