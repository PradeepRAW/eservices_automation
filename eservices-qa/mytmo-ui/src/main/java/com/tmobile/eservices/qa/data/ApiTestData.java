package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

public class ApiTestData {

	@Data(name = "env")
	private String env;

	@Data(name = "iMEINumber")
	private String iMEINumber;

	@Data(name = "ban")
	public String ban;

	@Data(name = "msisdn")
	public String msisdn;

	@Data(name = "password")
	public String password;

	@Data(name = "sku")
	public String sku;

	@Data(name = "cardNumber")
	public String cardNumber;

	@Data(name = "cardAlias")
	public String cardAlias;

	@Data(name = "bankAccountAlias")
	public String bankAccountAlias;

	@Data(name = "ExpiryMonth")
	public String expiryMonth;

	@Data(name = "CVV")
	public String cvv;

	@Data(name = "ZipCode")
	public String zipcode;

	@Data(name = "creditCheck")
	public String creditCheck;

	@Data(name = "paymentCode")
	public String paymentCode;

	@Data(name = "emailId")
	public String emailId;

	@Data(name = "chargeAmount")
	public String chargeAmount;

	@Data(name = "encodedBan")
	public String encodedBan;

	@Data(name = "primaryMsisdn")
	public String primaryMsisdn;

	@Data(name = "currentPlanCodes")
	public String currentPlanCode;

	@Data(name = "futurePlanCodes")
	public String futurePlanCode;

	@Data(name = "productTypes")
	public String productType;

	@Data(name = "paactionCode")
	public String paactionCode;

	@Data(name = "paissecure")
	public String paissecure;

	@Data(name = "makeName")
	public String makeName;

	@Data(name = "carrierName")
	public String carrierName;

	@Data(name = "modelName")
	public String modelName;

	@Data(name = "channelId")
	public String channelId;

	@Data(name = "applicationId")
	public String applicationId;

	@Data(name = "ssn")
	public String ssn;

	@Data(name = "phoneNumber")
	public String phoneNumber;

	@Data(name = "Filter")
	private String filter;

	@Data(name = "keyType")
	private String keyType;

	@Data(name = "legacyaccountnumber")
	private String legacyaccountnumber;

	@Data(name = "legacysystemindicator")
	private String legacysystemindicator;

	@Data(name = "cardHolderFirstName")
	private String cardHolderFirstName;

	@Data(name = "tmoid")
	private String tmoId;

	@Data(name = "cardHolderLastName")
	private String cardHolderLastName;

	@Data(name = "nickName")
	private String nickName;

	@Data(name = "expirationMonthYear")
	private String expirationMonthYear;

	@Data(name = "cvv")
	private String cvvCode;

	@Data(name = "bankroutingNo")
	private String routingNo;

	@Data(name = "publicKey")
	private String publicKey;

	@Data(name = "accounttype")
	private String accounttype;

	@Data(name = "Product-Type")
	private String productTypeForAAL;

	@Data(name = "Product-SubType")
	private String productSubType;
	
	@Data(name = "poptoken")
	private String popToken;

	public String getiMEINumber() {
		return iMEINumber;
	}

	public void setiMEINumber(String iMEINumber) {
		this.iMEINumber = iMEINumber;
	}

	public String getfilter() {
		return filter;
	}

	public String getEncodedBan() {
		return encodedBan;
	}

	public void setEncodedBan(String encodedBan) {
		this.encodedBan = encodedBan;
	}

	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	/***
	 * 
	 * @return
	 */
	public String getBan() {
		return ban;
	}

	/***
	 * 
	 * @param ban
	 */
	public void setBan(String ban) {
		this.ban = ban;
	}

	/***
	 * 
	 * @return
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @return
	 */
	public String getcallerId() {
		return msisdn;
	}

	/***
	 * 
	 * @param msisdn
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/***
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/***
	 * 
	 * @param msisdn
	 */
	public void setMsisdn(String missdn) {
		this.msisdn = missdn;
	}

	/**
	 * @return the env
	 */
	public String getEnv() {
		return env;
	}

	/**
	 * @return the env
	 */
	public String getChargeAmount() {
		return chargeAmount;
	}

	/***
	 * 
	 * @param env
	 */
	public void setEnv(String env) {
		this.env = env;
	}

	/***
	 * 
	 * @return
	 */
	public String getSku() {
		return sku;
	}

	/***
	 * 
	 * @param sku
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardAlias() {
		return cardAlias;
	}

	public void setCardAlias(String cardAlias) {
		this.cardAlias = cardAlias;
	}

	public String getExpiryMonth() {
		return expiryMonth;
	}

	public String getCVV() {
		return cvv;
	}

	public String getZipCode() {
		return zipcode;
	}

	public String getBankAccountAlias() {
		return bankAccountAlias;
	}

	public void setBankAccountAlias(String bankAccountAlias) {
		this.bankAccountAlias = bankAccountAlias;
	}

	public String getPrimaryMsisdn() {
		return primaryMsisdn;
	}

	public void setPrimaryMsisdn(String primaryMsisdn) {
		this.primaryMsisdn = primaryMsisdn;
	}

	public String getCurrentPlanCode() {
		return currentPlanCode;
	}

	public String getFuturePlanCode() {
		return futurePlanCode;
	}

	public String getpaActionCode() {
		return paactionCode;
	}

	public String getpaisSecure() {
		return paissecure;
	}

	public String getProductType() {
		return productType;
	}

	public String getAccountType() {
		return accounttype;
	}

	@Override
	public String toString() {
		return "ApiTestData [env=" + env + ", iMEINumber=" + iMEINumber + ", ban=" + ban + ", msisdn=" + msisdn
				+ ", password=" + password + ", sku=" + sku + ", cardNumber=" + cardNumber + ", cardAlias=" + cardAlias
				+ ", bankAccountAlias=" + bankAccountAlias + ", expiryMonth=" + expiryMonth + ", cvv=" + cvv
				+ ", zipcode=" + zipcode + ", creditCheck=" + creditCheck + ", paymentCode=" + paymentCode
				+ ", emailId=" + emailId + ", chargeAmount=" + chargeAmount + ", encodedBan=" + encodedBan
				+ ", primaryMsisdn=" + primaryMsisdn + ", currentPlanCode=" + currentPlanCode + ", futurePlanCode="
				+ futurePlanCode + ", productType=" + productType + ", paactionCode=" + paactionCode + ", paissecure="
				+ paissecure + ", makeName=" + makeName + ", carrierName=" + carrierName + ", modelName=" + modelName
				+ ", channelId=" + channelId + ", applicationId=" + applicationId + ", ssn=" + ssn + ", phoneNumber="
				+ phoneNumber + ", filter=" + filter + ", legacyaccountnumber=" + legacyaccountnumber
				+ ", legacysystemindicator=" + legacysystemindicator + ", cardHolderFirstName=" + cardHolderFirstName
				+ ", cardHolderLastName=" + cardHolderLastName + ", nickName=" + nickName + ", expirationMonthYear="
				+ expirationMonthYear + ", cvvCode=" + cvvCode + ", accounttype=" + accounttype + "]";
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/***
	 * 
	 * @return
	 */
	public String getMakeName() {
		return makeName;
	}

	/***
	 * 
	 * @param sku
	 */
	public void setMakeName(String makeName) {
		this.makeName = makeName;
	}

	/***
	 * 
	 * @return
	 */
	public String getModelName() {
		return modelName;
	}

	/***
	 * 
	 * @return
	 */
	public String getHardCreditCheckName() {
		return creditCheck;
	}

	/***
	 * 
	 * @return
	 */
	public String getKeyType() {
		return keyType;
	}

	/***
	 * 
	 * @param sku
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/***
	 * 
	 * @param sku
	 */
	public void setHardCreditCheckName(String modelName) {
		this.creditCheck = creditCheck;
	}

	/***
	 * 
	 * @return
	 */
	public String getCarrierName() {
		return carrierName;
	}

	/***
	 * 
	 * @param sku
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	/***
	 * 
	 * @return
	 */
	public String getChannelForCreditCheck() {
		return channelId;
	}

	/***
	 * 
	 * @param ban
	 */
	public void setChannelForCreditCheck(String ban) {
		this.channelId = channelId;
	}

	/***
	 * 
	 * @return
	 */
	public String getApplicationForCreditCheck() {
		return applicationId;
	}

	/***
	 * 
	 * @param ban
	 */
	public void setApplicationIdForCreditCheck(String ban) {
		this.applicationId = applicationId;
	}

	/***
	 * 
	 * @return
	 */
	public String getSsn() {
		return ssn;
	}

	/***
	 * 
	 * @return
	 */
	public String getPublicKey() {
		return publicKey;
	}

	/***
	 * 
	 * @param ban
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	/***
	 * 
	 * @param ban
	 */
	public void setPublickey(String publicKey) {
		this.publicKey = publicKey;
	}

	/***
	 * 
	 * @return
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/***
	 * 
	 * @param ban
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/***
	 * 
	 * @return
	 */
	public String getlegacyaccountnumber() {
		return legacyaccountnumber;
	}

	/***
	 * 
	 * @return
	 */
	public String getlegacysystemindicator() {
		return legacysystemindicator;
	}

	/***
	 * 
	 * @return
	 */
	public String gettmoId() {
		return tmoId;
	}

	public String getCardHolderFirstName() {
		return cardHolderFirstName;
	}

	public void setCardHolderFirstName(String cardHolderFirstName) {
		this.cardHolderFirstName = cardHolderFirstName;
	}

	public String getCardHolderLastName() {
		return cardHolderLastName;
	}

	public void setCardHolderLastName(String cardHolderLastName) {
		this.cardHolderLastName = cardHolderLastName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getExpirationMonthYear() {
		return expirationMonthYear;
	}

	public void setExpirationMonthYear(String expirationMonthYear) {
		this.expirationMonthYear = expirationMonthYear;
	}

	public String getCvvCode() {
		return cvvCode;
	}

	public void setCvvCode(String cvvCode) {
		this.cvvCode = cvvCode;
	}

	public String getRoutingNo() {
		return routingNo;
	}

	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}

	public void setaccountType(String accounttype) {
		this.accounttype = accounttype;
	}

	/***
	 * 
	 * @return
	 */
	public String getProductTypeForAAL() {
		return productTypeForAAL;
	}

	/***
	 * 
	 * @param sku
	 */
	public void setProductType(String productType) {
		this.productTypeForAAL = productTypeForAAL;
	}

	/***
	 * 
	 * @return
	 */
	public String getProductSubTypeForAAL() {
		return productSubType;
	}

	/***
	 * 
	 * @param sku
	 */
	public void setProductSubType(String productSubType) {
		this.productSubType = productSubType;
	}

	public String getPopToken() {
		return popToken;
	}

	public void setPopToken(String popToken) {
		this.popToken = popToken;
	}
}
