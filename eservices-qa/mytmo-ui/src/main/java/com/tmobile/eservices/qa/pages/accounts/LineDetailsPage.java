package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class LineDetailsPage extends CommonPage {

	// Links &headers
	@FindBy(css = "li span[role='link']")
	private List<WebElement> breadcrumbLInks;

	@FindBy(css = "*.Display6")
	private List<WebElement> settingsElementsList;

	@FindBy(css = "*[class*='Display5']")
	private List<WebElement> listOfheaders;

	@FindBy(css = "div.float-left.H6-heading")
	private List<WebElement> activeAddOnsByNameList;

	// Buttons

	@FindBy(css = "button.SecondaryCTA")
	private WebElement manageAddOns;

	@FindBy(xpath = "//button[contains(text(),'Cancel')]")
	private WebElement clickCancel;

	@FindBy(css = "div.plan-n-i")
	private WebElement planName;

	@FindBy(css = "img.phone-icon")
	private WebElement deviceIcon;

	@FindBy(xpath = "//span[contains(text(),'Device support')]")
	private WebElement deviceName;

	// others

	@FindBy(css = "p.body-bold.text-right.black")
	private List<WebElement> lineCost;

	@FindBy(xpath = "//span[contains(text(),'Active Add-ons')]")
	private WebElement activeaddOns;

	@FindBy(css = "p.body.body-10.pull-right.text-right.padding-top-xsmall")
	private List<WebElement> clikOnLine;

	@FindBy(xpath = "//p[contains(text(),'Add TV Service')]")
	private WebElement addtvService;

	@FindBy(xpath = "//span[contains(text(),'equipment Financing')]")
	private WebElement equipment;

	@FindBy(xpath = "//p[contains(text(),'Monthly Charges')]")
	private WebElement eipPage;

	@FindBy(xpath = "//span[contains(text(),'Contact us')]")
	private WebElement contactUs;

	@FindBy(css = "a.gbl-logo")
	private WebElement homeLogo;

	@FindBy(css = "img.icon-size")
	private WebElement tmobileHeadder;

	@FindBy(xpath = "//span[contains(text(),'Shared Add-ons')]")
	private WebElement sharedaddOn;

	@FindBy(xpath = "//span[contains(text(),'Shared')]")
	private WebElement linesharedaddOn;

	@FindBy(xpath = "//span[contains(text(),'Line Usage')]")
	private WebElement lineUsage;

	@FindBy(xpath = "//p[contains(text(),'My promotions')]")
	private WebElement promotions;

	@FindBy(css = "div.Display4")
	private WebElement lineDetailsHeader;

	@FindBy(xpath = "//span[text()='Active Add-ons']//..//..//..//span[contains(@class,'H6-heading')]")
	private List<WebElement> activeaddOnsList;

	@FindBy(css = "div[class*='msdin'")
	private WebElement msdin;

	@FindBy(css = "img.imgopacity")
	private WebElement pencilIcon;

	@FindBy(xpath = "//p[contains(text(),' Edit name')]")
	private WebElement editName;

	@FindBy(css = "input#firstName")
	private WebElement firstName;

	@FindBy(css = "input#lastName")
	private WebElement lastName;

	@FindBy(xpath = "//button[contains(text(),'Save')]")
	private WebElement clickSave;

	@FindBy(xpath = "//span[contains(text(),'Please try again.')]")
	private WebElement veifySuccessfull;

	@FindBy(css = ".no-padding div.Display5")
	private List<WebElement> listOfLineNames;

	@FindBy(xpath = "//span[contains(text(),'Contact your Primary Account Holder.')]")
	private WebElement suspendedErrorRestricted;

	@FindBy(css = "span.f-l.statusHeight")
	private WebElement lineSuspnedError;

	@FindAll({ @FindBy(css = "div.plan-n-i.H4-heading"), @FindBy(css = "div.plan-n-i.body") })
	private WebElement ispLine;

	@FindBy(css = "div.col-12.px-md-0.padding-vertical-small-xs.padding-vertical-small-sm.padding-vertical-medium.cursor-default")
	private List<WebElement> addonCount;

	@FindBy(xpath = "//div[contains(text(),'Settings')]//following::p[contains(text(),'Manage DIGITS Settings')]")
	private WebElement manageDIgits;

	@FindBy(css = "img.flag-icon")
	private WebElement flagIcon;

	@FindBy(css = "span.pl-3.pl-md-3.H6-heading")
	private WebElement alert;

	@FindBy(css = "span.pl-3.pl-md-3.H6-heading")
	private WebElement eosAlert;

	/**
	 * Verify Line details page loaded
	 */
	public LineDetailsPage verifyLineDetailsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(lineDetailsHeader));
			lineDetailsHeader.isDisplayed();
			Reporter.log("LineDetailsPage is loaded");
		} catch (Exception e) {
			Assert.fail("LineDetailsPage is not loaded");
		}
		return this;
	}

	/**
	 * Click On My Promotions
	 */
	public LineDetailsPage clickOnMypromotions() {
		try {
			checkPageIsReady();
			clickElementBytext(settingsElementsList, "My Promotions");
			Reporter.log("Clicked on My promotions blade");
		} catch (Exception e) {
			Assert.fail("My promotions blade is not displaying");
		}
		return this;
	}

	/**
	 * Verify Line details page loaded
	 */

	public LineDetailsPage verifyManageDigitsSetting() {
		try {
			checkPageIsReady();
			manageDIgits.isDisplayed();
			Reporter.log("Manage DIGITS Settings is available under the Setting component");
		} catch (Exception e) {
			Assert.fail("Manage DIGITS Settings is not available under the Setting component");
		}
		return this;
	}

	/**
	 * Click On Profile And Line
	 */
	public LineDetailsPage clickOnProfileAndLine() {
		try {
			clickElementBytext(settingsElementsList, "Profile and Line");
			Reporter.log("Manage addOn CTA is clicked on line details page  ");
		} catch (Exception e) {
			Assert.fail("Manage addOn CTA is not clicked on line details page ");
		}
		return this;
	}

	/**
	 * Click on Cancel Button
	 */
	public LineDetailsPage clickCancelButton() {
		try {
			checkPageIsReady();
			clickCancel.click();
			Reporter.log("Clicked Cancel button and navigated back to Previous page");
		} catch (Exception e) {
			Assert.fail("Cancel button is not available to click");
		}
		return this;
	}

	/**
	 * Verify Plan Details Page Headers
	 */
	public LineDetailsPage verifyPlanDetailsPageHeaders() {
		try {
			checkPageIsReady();
			verifyListOfElementsBytext(listOfheaders, "Plan");
			verifyListOfElementsBytext(listOfheaders, "Device");
			verifyListOfElementsBytext(listOfheaders, "settings");
			verifyListOfElementsBytext(listOfheaders, "Do you need help?");
			verifyListOfElementsBytext(listOfheaders, "Active Add-ons");
			Reporter.log("Headers are available on the line Details Page");
		} catch (Exception e) {
			Assert.fail("Headers are not available on the line Details Page");
		}
		return this;
	}

	/**
	 * Click on Plan name
	 * 
	 */
	public LineDetailsPage clickOnPlanName() {
		try {
			planName.click();
			Reporter.log("Clicked on PlanName and Navigated to PlanDetailsPage ");
		} catch (Exception e) {
			Assert.fail("PlanName is not available and unable to navigate to PlanDetailsPage ");
		}
		return this;
	}

	/**
	 * Verify Plan Name
	 * 
	 */
	public LineDetailsPage verifyPlanName() {
		try {
			Assert.assertTrue(planName.isDisplayed());
			Reporter.log("PlanName is displayed");
		} catch (Exception e) {
			Assert.fail("PlanName is not displayed ");
		}
		return this;
	}

	/**
	 * Verify Plan Name Text
	 * 
	 */
	public LineDetailsPage verifyPlanNameText(String name) {
		try {
			Assert.assertTrue(planName.getText().contains(name));
			Reporter.log("PlanName is displayed");
		} catch (Exception e) {
			Assert.fail("PlanName is not displayed ");
		}
		return this;
	}

	/**
	 * Verify Plan Name Text
	 * 
	 */
	public LineDetailsPage clickManageaddOnButton() {
		try {
			checkPageIsReady();
			manageAddOns.click();
			Reporter.log("Manage addOn CTA is clicked on line details page");
		} catch (Exception e) {
			Assert.fail("Manage addOn CTA is not clicked on line details page");
		}
		return this;
	}

	/**
	 * Verify Settings Title
	 * 
	 */
	public LineDetailsPage verifySettingsTitle() {
		try {
			checkPageIsReady();
			verifyElementBytext(listOfheaders, "Settings");
			Reporter.log("Settings header displayed");
		} catch (Exception e) {
			Assert.fail("Settings header si not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Icon
	 * 
	 */
	public LineDetailsPage verifyDeviceIcon() {
		try {
			Assert.assertTrue(deviceIcon.isDisplayed(), "Device Icon is not displying");
			Reporter.log("Device Icon is displayed");
		} catch (Exception e) {
			Assert.fail("Device Icon is not displayed");
		}
		return this;
	}

	/**
	 * Click On Active Add Ons By Name
	 * 
	 */

	public LineDetailsPage clickOnActiveAddOnsByName(String name) {
		try {
			if (!name.isEmpty()) {
				clickElementBytext(activeAddOnsByNameList, name);
			} else {
				activeAddOnsByNameList.get(0).click();
			}
			Reporter.log("Clicked on Active add on " + name);
		} catch (Exception e) {
			Assert.fail("Active add ons not displaying");
		}
		return this;
	}

	/**
	 * Verify Active Add Ons By Soc Name Is Not Displaying
	 * 
	 */

	public LineDetailsPage verifyActiveAddOnsBySocNameIsNotDisplaying(String name) {
		try {
			Assert.assertFalse(verifyElementBytext(activeAddOnsByNameList, name), name + " is displayed");
			Reporter.log(name + " is not displayed");
		} catch (Exception e) {
			Assert.fail(name + " is displayed");
		}
		return this;
	}

	/**
	 * Click On bread crumb LInks
	 * 
	 */

	public LineDetailsPage clickOnbreadcrumbLInks(int index) {
		try {
			breadcrumbLInks.get(index).click();
		} catch (Exception e) {
			Assert.fail("Breadcrumb Links are not displayed");
		}
		return this;
	}

	public LineDetailsPage verifyActiveAddonsCount() {
		try {
			checkPageIsReady();
			int count = 0;
			int size = addonCount.size();
			if (size > 0) {
				for (int i = 0; i < size; i++) {
					if (addonCount.get(i).getText().contains("Shared")) {
						count++;
					}
				}
			}
			int count1 = size - count;
			Reporter.log(count + "  sharedAddon is available in activeAddon Section");
			Reporter.log(count1 + "  lineLevel addOn is available in activeAddon Section");
			Reporter.log("totalAddon Count for this line is : " + size);
			getDriver().navigate().back();
			if (clikOnLine.get(1).getText().contains(size + " Add-Ons")) {
				Reporter.log("addonCount is includded both lineLevelandShared addon's in AcctOverviewPage");
			} else {
				Reporter.log("addonCount is not includded both lineLevelandShared addon's in AcctOverviewPage");
			}

		} catch (Exception e) {
			Assert.fail("No addons available in active addonSection");
		}
		return this;
	}

	public LineDetailsPage clickonISPline(String planName) {
		try {
			checkPageIsReady();
			Assert.assertTrue(ispLine.getText().contains(planName));
			ispLine.click();
			Reporter.log("Clicked on ISP line and navigate to plandetails page");
		} catch (Exception e) {
			Assert.fail("ISP line is not available to click");
		}
		return this;
	}

	public LineDetailsPage verifySuspnedErrorMessageforRestoreLine() {
		try {
			checkPageIsReady();
			suspendedErrorRestricted.isDisplayed();
			Reporter.log("ErrorMessage is Diplayed ");
		} catch (Exception e) {
			Assert.fail("ErrorMessage is not Diplayed");
		}
		return this;
	}

	public LineDetailsPage updatedSuccesful() {
		try {
			checkPageIsReady();
			veifySuccessfull.isDisplayed();
			Reporter.log("Unable to Submit Now and Please try again.");
		} catch (Exception e) {
			Assert.fail("Name updated successfully");
		}
		return this;
	}

	public LineDetailsPage clickSaveButton() {
		try {
			checkPageIsReady();
			clickSave.click();
			Reporter.log("Clicked Save button");
		} catch (Exception e) {
			Assert.fail("Save button is not available to click");
		}
		return this;
	}

	public LineDetailsPage lastFirstName() {
		try {
			checkPageIsReady();
			lastName.sendKeys("CccccDdddd");
			Reporter.log("Enterd the Last Name");
		} catch (Exception e) {
			Assert.fail("Last Name field is not avalable to Enter the Nanme");
		}
		return this;
	}

	public LineDetailsPage enterFirstName() {
		try {
			checkPageIsReady();
			String firstname = "AaaaaaBbbbbb";
			firstName.sendKeys(firstname);
			Reporter.log("Enterd the First Name");
		} catch (Exception e) {
			Assert.fail("First Name field is not avalable to Enter the Nanme");
		}
		return this;
	}

	public LineDetailsPage verifyEditNameWindow() {
		try {
			checkPageIsReady();
			editName.isDisplayed();
			Reporter.log("Window is Displayed");
		} catch (Exception e) {
			Assert.fail("Window is not Displayed");
		}
		return this;

	}

	public LineDetailsPage clcikOnpencilIcon() {
		try {
			checkPageIsReady();
			pencilIcon.click();
			Reporter.log("Clicked on pencilIcon and opened Madal window");
		} catch (Exception e) {
			Assert.fail("pencilIcon is not available to Click");
		}
		return this;

	}

	public LineDetailsPage clickOnDeviceSupport() {
		try {
			deviceName.click();
			Reporter.log(
					"deviceName and deviceSupport details should be available and Navigated to existingPhoneDetailsPage when click on deviceSupport ");
		} catch (Exception e) {
			Assert.fail("deviceName and deviceSupport details are not availale ");
		}
		return this;
	}

	public LineDetailsPage clickOnAddTVService() {
		try {
			checkPageIsReady();
			addtvService.click();
			Reporter.log("Clicked on My promotions blade");
		} catch (Exception e) {
			Assert.fail("not Clicked on My promotions blade");
		}
		return this;
	}

	public LineDetailsPage verifyactiveaddOnsComponent() {
		try {
			checkPageIsReady();
			Assert.assertTrue(activeaddOns.getText().contains("Active Add-ons"));
			Reporter.log("Active Addon Componenet is Available ");
		} catch (Exception e) {
			Assert.fail("Active Addon Componenet is not Availablek ");
		}
		return this;
	}

	public LineDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	public LineDetailsPage clickOnFirstactiveaddOnsList() {
		try {
			checkPageIsReady();
			activeaddOnsList.get(0).click();
			Reporter.log("Clicked on active Add Ons List");
		} catch (Exception e) {
			Assert.fail("Not Clicked on active Add Ons");
		}
		return this;
	}

	public LineDetailsPage verifymsdin() {
		try {
			checkPageIsReady();
			Assert.assertTrue(msdin.isDisplayed());
			Reporter.log("Clicked on active Add Ons List");
		} catch (Exception e) {
			Assert.fail("Not Clicked on active Add Ons");
		}
		return this;
	}

	/**
	 * Verify manage addOns cta
	 */
	public LineDetailsPage verifyManageAddOnsCTA() {
		try {
			Assert.assertFalse(isElementDisplayed(manageAddOns));
			Reporter.log("Manage add ons cta not displayed");
		} catch (AssertionError e) {
			Assert.fail("Manage add ons cta displayed");
		}
		return this;
	}

	public LineDetailsPage verifyAlert() {
		try {
			Assert.assertTrue(isElementDisplayed(flagIcon), "Flag icon is displayed");
			Reporter.log("Flag icon is displayed");
			if (alert.getText().contains("You've used 80% of your data")) {
				Assert.assertTrue(alert.getText().contains("You've used 80% of your data"),
						"The Alert 'You've used 80% of your data' is displayed");
				Reporter.log("The Alert 'You've used 80% of your data' is displayed");
			} else if (alert.getText().contains("You've used 100% of your data")) {
				Assert.assertTrue(alert.getText().contains("You've used 100% of your data"),
						"The Alert 'You've used 100% of your data' is displayed");
				Reporter.log("The Alert 'You've used 100% of your data' is displayed");
			} else {
				Assert.fail("Unable to verify alert on line details Page");
			}

		} catch (AssertionError e) {
			Assert.fail("Unable to verify alert on line details Page");
		}
		return this;
	}

}
