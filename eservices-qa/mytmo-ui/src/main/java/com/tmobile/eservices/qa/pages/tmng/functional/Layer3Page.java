package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author sputti
 *
 */
public class Layer3Page extends TmngCommonPage {

	private final String pageUrl = "http://dmo-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/tv/demo.html";
	// Auth Link :
	// dmo-tmo-author.corporate.t-mobile.com:4502/editor.html/content/t-mobile/consumer/tv/demo.html

	// Locators for Layer3 Channel Component Elements
	@FindBy(xpath = "//form[@name='channelLookupForm']//input[@name='channelZip']")
	private WebElement channelZipCode;

	@FindBy(xpath = "//label[@id='channelZipError']")
	private WebElement channelZipCodeError;

	@FindBy(xpath = "//button[@id='channelBtn']")
	private WebElement channelSubmitCTA;

	@FindBy(xpath = "//h2[@id='channelListHeadline']")
	private WebElement channelListHeading;

	@FindBy(xpath = "//p[@id='channelListParagraph']")
	private WebElement channelListParagraph;

	// Locators for Layer3 Service Component Elements
	@FindBy(xpath = "//form[@name='serviceLookupForm']//input[@name='serviceZip']")
	private WebElement serviceZipCode;

	@FindBy(xpath = "//label[@id='serviceZipError']")
	private WebElement serviceZipCodeError;

	@FindBy(xpath = "//button[@id='serviceBtn']")
	private WebElement serviceSubmitCTA;

	// Locators for Layer3 Generic Elements
	@FindBy(xpath = "//select[@name='category']")
	private WebElement listCategory;

	@FindBy(xpath = "//div[@class='showVerticalScroll']")
	private WebElement listScroll;

	@FindBy(xpath = "//img[@ng-src='https://d2k8duubn1xfe6.cloudfront.net/image?group=l3-023300-dir-01&profile=lcm&language=en']")
	private WebElement imglistScroll;

	@FindBy(xpath = "//button[@id='checkNewZipCta']")
	private WebElement checkNewZipCta;

	@FindBy(xpath = "//button[@id='shopNowCta']")
	private WebElement shopNowCta;

	@FindAll({ @FindBy(xpath = "//button[@id='serviceBtn']"), @FindBy(xpath = "//button[@id='channelBtn']") })
	private WebElement SubmitCTA;

	/**
	 * @param webDriver
	 */
	public Layer3Page(WebDriver webDriver) {
		super(webDriver);
	}

	// ------------------------------------------------------- Layer3Page Load
	// Verification --------------------------------
	/**
	 * Verify that the page loaded completely.
	 */
	public Layer3Page verifyLayer3PageLoaded() {
		checkPageIsReady();
		waitForSpinnerInvisibility();
		try {
			if (getDriver().getCurrentUrl().contains(pageUrl)) {
				verifyLayer3PageUrl();
				Reporter.log("Layer3 page loaded");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Layer3 page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public Layer3Page verifyLayer3PageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		Reporter.log("Layer3 page URL verified");
		return this;
	}

	// ------------------------------------------------------- Layer3 Screen
	// Verification --------------------------------
	/**
	 * Accepts 5 digits
	 */
	public Layer3Page validZipCode() {
		try {
			waitFor(ExpectedConditions.visibilityOf(channelZipCode), 6);
			sendTextData(channelZipCode, "123");
			if (channelZipCode.getAttribute("value").length() < 5) {
				if (channelZipCodeError.isDisplayed()) {
					Reporter.log("'Enter valid zip code' message displayed if zip code entered < 5 digits");
					sendTextData(channelZipCode, Constants.LAYER3_ZIP);
				} else
					Assert.fail("'Enter valid zip code' message not displayed if zip code entered < 5 digits");
			}
		} catch (Exception e) {
			Assert.fail("'Enter valid zip code' message not displayed if zip code entered < 5 digits");
		}
		return this;
	}

	/**
	 * Click on CTA
	 */
	public Layer3Page clickOnCTABtn() {
		checkPageIsReady();
		try {
			moveToElement(channelSubmitCTA);
			channelSubmitCTA.click();
			Reporter.log("Clicked on CTA button.");
			waitFor(ExpectedConditions.visibilityOf(channelListHeading), 6);
		} catch (Exception e) {
			Assert.fail("Failed to click on CTA button.");
		}
		return this;
	}

	/**
	 * Verify Channels
	 */
	public Layer3Page verifyChannels() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(channelListHeading));
			Assert.assertTrue(isElementDisplayed(channelListParagraph));
			Assert.assertTrue(isElementDisplayed(listCategory));
			Reporter.log("List filter dropdown is displayed a top of the search results");
			Assert.assertTrue(isElementDisplayed(listScroll));
			Reporter.log(
					"Channel List Headers, Paragraph, Category and Scroll List Images are displayed in Layer3 Channels Page");
			Assert.assertTrue(isElementDisplayed(imglistScroll));
			Reporter.log(
					"Group ID parameter is added to each image URL - https://d2k8duubn1xfe6.cloudfront.net/image?group=l3-023300-dir-01&profile=lcm&language=en']");
		} catch (Exception e) {
			Assert.fail(
					"Channel List Headers, Paragraph, Category and Scroll List Images are not displayed in Layer3 Channels Page");
		}
		return this;
	}

	/**
	 * Select List Category
	 */
	public Layer3Page selectListCategory() {
		try {
			listCategory.click();
			selectElementFromDropDown(listCategory, "Text", "HBO");
			listCategory.click();
			getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
			Assert.assertTrue(isElementDisplayed(listScroll));
			Reporter.log("Selected the List Category from filter DropDown");
		} catch (Exception e) {
			Assert.fail("'Not Able to select the List Category from filter DropDown" + e.getMessage());
		}
		return this;
	}

	/**
	 * Click on New Zip CTA
	 */
	public Layer3Page clickOnNewZipCTABtn() {
		checkPageIsReady();
		try {
			moveToElement(checkNewZipCta);
			checkNewZipCta.click();
			Reporter.log("Clicked on New Zip CTA button.");
			waitFor(ExpectedConditions.visibilityOf(channelZipCode), 5);
			if (channelZipCode.isDisplayed()) {
				Reporter.log(
						"By clicking on New Zip CTA, Component anchors to the top of the parent hero-wrapper component");
				Assert.assertTrue(channelZipCode.getAttribute("value").length() == 0,
						"ZipCode Field value is not cleared");
				Reporter.log("Zip code input field value is cleared");
				Assert.assertTrue(!isElementDisplayed(listScroll));
				Reporter.log("List Category and Search results section is collapsed");
			} else
				Assert.fail("Zip code input field value is not cleared");
		} catch (Exception e) {
			Assert.fail("Failed to click on New Zip CTA button.");
		}
		return this;
	}

	/**
	 * Verify Shop CTA
	 */
	public Layer3Page verifyShopCTABtn() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(shopNowCta), 5);
			moveToElement(shopNowCta);
			Assert.assertTrue(isElementDisplayed(shopNowCta));
			Reporter.log("Shop New CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Shop New CTA is not displayed" + e.getMessage());
		}
		return this;
	}
}