package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class EipJodPaymentConfirmationPage extends CommonPage {

	public EipJodPaymentConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(css = "p.H3-heading")
	private WebElement pageHeader;

	private final String pageUrl = "/eip-payment-confirmation";
	
	@FindBy(css="a[_ngcontent-c25]")
	private WebElement showHideDetailsLink;
	
	@FindBy(css="button.PrimaryCTA")
	private WebElement backToShopPlansBtn;
	
	

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public EipJodPaymentConfirmationPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("EIP Payment Confirmation Page is loaded.");

		} catch (Exception e) {
			Assert.fail("EIP Payment Confirmation page is not loaded");
		}
		return this;
	}
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EipJodPaymentConfirmationPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}
	
	/**
	 * click amount blade
	 */
	public void clickBacktoShopButton() {
		try {
			backToShopPlansBtn.isDisplayed();
			backToShopPlansBtn.click();
			Reporter.log("BacktoShop Button is Displayed and clicked");
		} catch (Exception e) {
			Assert.fail("BacktoShop Button is not found");
		}
	}
	

}