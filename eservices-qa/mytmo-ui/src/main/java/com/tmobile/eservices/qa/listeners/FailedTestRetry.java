package com.tmobile.eservices.qa.listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class FailedTestRetry implements IRetryAnalyzer {
	public static final int MAX_RETRY_COUNT =2;
	private  int RETRY_COUNT = 0;

	/**
	 * 
	 * @param result
	 * @return
	 */
	public boolean retry(ITestResult result) {
		boolean retry = result.getStatus() != ITestResult.SKIP;
		if (retry) {
			if(RETRY_COUNT<MAX_RETRY_COUNT){
				RETRY_COUNT++;
				return true;				
			}
			else
				return false;
		}
		return retry;
	}
}


