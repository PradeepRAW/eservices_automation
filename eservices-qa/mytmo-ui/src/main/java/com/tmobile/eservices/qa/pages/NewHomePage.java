package com.tmobile.eservices.qa.pages;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;

import io.appium.java_client.AppiumDriver;

/**
 * This page contains all the Home page elements and methods
 * 
 * @author rnallamilli
 *
 */
public class NewHomePage extends CommonPage {

	private final String pageUrl = "/home";

	@FindBy(xpath = "//div[contains(@class,'Display')]")
	private WebElement modalPopUp;

	@FindBy(xpath = "//div[contains(@aria-label,'Starting')]")
	private WebElement modalPopUpAuthoringMessage;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	private WebElement continueButtonOnPopUp;

	@FindBy(css = ".gh-lang-font.gh-white")
	private WebElement langFont;

	@FindBy(css = ".gh-tmo-icon")
	private WebElement tmobileMagentaIcon;

	@FindBy(css = ".gh-header-title.gh-inline-block")
	private WebElement tmobileHeaderTitle;

	@FindBy(css = ".gh-dropdown-toggle .person-whitee")
	private WebElement profileIcon;

	@FindBy(css = ".gh-dropdown-toggle .gh-dropdown-name")
	private WebElement profileName;

	@FindBy(css = ".gh-dropdown-toggle .gh-arrow-down-gray")
	private WebElement profileDropDownIcon;

	@FindBy(css = "div.gh-menu-white.gh-cursor")
	private WebElement mobileDropDown;

	@FindBy(css = "#user-link-dropdown-item-category-name-0-0-0")
	private WebElement profileMenu;

	@FindBy(linkText = "Profile")
	private WebElement mobileProfileMenu;

	@FindBy(xpath = "//*[contains(text(),'Account history') or contains(text(),'Account History')]")
	private WebElement accountHistoryMenu;

	@FindBy(css = "#user-link-dropdown-item-category-name-0-0-1")
	private WebElement accountHistoryMenuForMobile;

	@FindBy(xpath = "//*[contains(text(),'Switch account')]")
	private WebElement switchAccountMenu;

	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	private WebElement logOutMenu;

	@FindBy(css = "#user-link-dropdown-item-category-name-0-0-3")
	private WebElement logOutMenuForMobile;

	@FindBy(css = "[u1st-aria-label*='Bill']")
	private WebElement billMenu;

	@FindBy(xpath = "//*[contains(text(),'Bill')]")
	private WebElement billMenuForMobile;

	@FindBy(css = "[data-analytics-value*='Usage']")
	private WebElement usageMenu;

	@FindBy(css = "#digital-header-nav-link-1")
	private WebElement usageMenuForMobile;

	@FindBy(css = "[u1st-aria-label*='PLAN']")
	private WebElement planMenu;

	@FindBy(xpath = "//button[contains(text(),'Make a payment')]")
	private List<WebElement> paymentLink;

	@FindBy(css = "#digital-header-nav-link-2")
	private WebElement accountMenu;

	@FindBy(css = "a[u1st-aria-label*='phone']")
	private WebElement phoneMenu;

	@FindBy(css = "#digital-header-nav-link-3")
	private WebElement phoneMenuForMobile;

	@FindBy(css = "#digital-header-nav-link-3")
	private WebElement shopMenu;

	@FindBy(css = "#digital-header-nav-link-3")
	private WebElement shopMenuForMobile;

	@FindBy(css = "#digital-header-nav-link-2")
	private WebElement accountMenuForMobile;

	@FindBy(css = "[aria-label='click to call us']")
	private WebElement phoneCallIcon;

	@FindBy(css = "[aria-label='click to message us']")
	private WebElement messageUsIcon;

	@FindBy(css = "[data-analytics-value*='Search']")
	private WebElement search;

	@FindBy(css = ".TMO-NOTIFICATION-COMPONENT")
	private WebElement notificationBar;

	@FindBy(css = ".warning-outline-white")
	private WebElement notificationBarWarningIcon;

	@FindBy(css = ".legal[tmo-layout*='show@md']")
	private WebElement notificationBarPayOutLink;

	@FindBy(css = ".TMO-NOTIFICATION-COMPONENT .right-icon")
	private WebElement notificationBarRightArrowIcon;

	@FindBy(xpath = "//*[contains(text(),'login')]")
	private WebElement lastLoginDiv;

	@FindBy(xpath = "//*[contains(text(),'Account #')]")
	private WebElement accountDiv;

	@FindBy(css = "[u1st-aria-label*='Welcome']")
	private WebElement welcomeText;

	@FindBy(css = "[aria-label*='Welcome']")
	private WebElement welcomeTextForMobile;

	@FindBy(css = ".warning-outline-white")
	private WebElement notificationCountIcon;

	@FindBy(css = "[data-analytics-value*='Support']")
	private WebElement footerSupportLink;

	@FindBy(xpath = "//*[contains(text(),'CONTACT US')]")
	private WebElement footerContactUsLink;

	@FindBy(css = "[data-analytics-value*='Store locator'],[data-analytics-value*='Store Locator']")
	private WebElement footerStoreLocatorLink;

	@FindBy(xpath = "//*[contains(text(),'COVERAGE')]")
	private WebElement footerCoverageLink;

	@FindBy(xpath = "//*[contains(text(),'T-MOBILE.COM')]")
	private WebElement footerTMobileLink;

	@FindBy(xpath = "//*[contains(text(),'About T-Mobile USA')]")
	private WebElement footerTMobileUSALink;

	@FindBy(xpath = "//*[contains(text(),'Terms & Conditions')]")
	private WebElement footerTermsAndConditionsLink;

	@FindBy(xpath = "//*[contains(text(),'Privacy Policy')]")
	private WebElement footerPrivacyPolicyLink;

	@FindBy(xpath = "//*[contains(text(),'Interest-Based Ads')]")
	private WebElement footerInterestBasedAdsLink;

	@FindBy(xpath = "//*[contains(text(),'Return Policy')]")
	private WebElement footerReturnPolicyALink;

	@FindBy(xpath = "//*[contains(text(),'Terms Of Use')]")
	private WebElement footerTermsOfUseLink;

	@FindBy(xpath = "//*[contains(text(),'Privacy Resources')]")
	private WebElement footerPrivacyResourcesLink;

	@FindBy(xpath = "//*[contains(text(),'Open Internet Policy')]")
	private WebElement footerOpenInternetPolicyLink;

	@FindBy(css = "[aria-label*='copyright']")
	private WebElement copyRightDiv;

	@FindBy(css = "[u1st-aria-label*='My line']")
	private WebElement myLineSection;

	@FindBy(css = "[aria-label*='My line']")
	private WebElement myLineSectionForMobile;

	@FindBy(xpath = "//a[contains(text(),'View details')]")
	private WebElement viewDetailsCTA;

	@FindBy(css = "[src*='images']")
	private WebElement deviceImageInMyLineSection;

	@FindBy(css = "[u1st-aria-label*='Upgrade']")
	private WebElement upgradeCTAInMyLineSection;

	@FindBy(css = "[aria-label*='Upgrade']")
	private WebElement upgradeCTAInMyLineSectionForMobile;

	@FindBy(css = "[u1st-aria-label*='My line']")
	private WebElement missdnInMyLineSection;

	@FindBy(css = "span[pid*='cust_msisdnN']")
	private WebElement missdnInMyLineSectionForMobile;

	@FindBy(css = "[aria-label*='copyright']")
	private WebElement toolTipText;

	@FindBy(xpath = "//*[contains(text(),'My T-Mobile benefits')]")
	private WebElement planBenefitTiles;

	@FindBy(xpath = "//*[contains(text(),'See more')]")
	private WebElement seeMoreCTA;

	@FindBy(xpath = "//*[contains(text(),'View bill')]")
	private WebElement viewBillCTA;

	@FindBy(css = "span[u1st-aria-label*='$']")
	private WebElement dueDate;

	@FindBy(css = "div[aria-label*='$']")
	private WebElement amountDue;

	@FindBy(xpath = "//button[contains(text(),'Make a payment')]")
	private WebElement paymentCTA;

	@FindBy(xpath = "//*[contains(text(),'Thanks for your last payment')]")
	private WebElement lastPayment;

	@FindBy(css = "div.TMO-NOTIFICATION-COMPONENT")
	private WebElement pastDueAlert;

	@FindBy(css = "[aria-label*='copyright']")
	private WebElement pastDuePaymentCTA;

	@FindBy(css = "[aria-label*='copyright']")
	private WebElement autoPayCTA;

	@FindBy(css = "div#model")
	private WebElement profileDropDown;

	@FindBy(css = ".mdi-menu")
	private WebElement hamburgerMenu;

	@FindBy(css = ".mdi-close")
	private WebElement hamburgerMenuCloseBtn;

	@FindBy(css = "span.mdi.mdi-account")
	private WebElement myAccountForMobile;

	@FindBy(css = "#digital-header-logo-url")
	private WebElement tMobileIcon;

	@FindBy(css = "#showIosId [id*='banner_cross']")
	private WebElement tMobileAppIOSPopUp;

	@FindBy(css = "#showIosId [id*='banner_cross']")
	private WebElement mobileSearchButton;

	@FindBy(css = "#nav_search_text")
	private WebElement searchTextBox;

	@FindBy(css = "a[id='addHighSpeedDataLink']")
	private List<WebElement> searchResults;

	@FindBy(xpath = "//*[contains(text(),'Contact us') or contains(text(),'Contact Us')]")
	private WebElement contactUsLink;

	@FindBy(css = "[data-analytics-value*='Coverage']")
	private WebElement coverageLink;

	@FindBy(xpath = "//*[contains(text(),'Return Policy')]")
	private WebElement returnPolicyLink;

	@FindBy(linkText = "About T-Mobile USA")
	private WebElement aboutTmobileLink;

	@FindBy(linkText = "Privacy Policy")
	private WebElement privacyPolicyLink;

	@FindBy(css = "[data-analytics-value*='Terms & conditions']")
	private WebElement termsAndConditionsDesktop;

	@FindBy(css = "[data-analytics-value*='Terms & conditions']")
	private WebElement termsAndConditionsLinkForMobile;

	@FindAll({ @FindBy(css = ".active .searchpagination"), @FindBy(css = "div#support div.line-usage-pagination") })
	private WebElement supportPagination;

	@FindAll({ @FindBy(css = "#devices .searchpagination.clearfix"),
			@FindBy(css = "div#devices div.line-usage-pagination") })
	private WebElement devicePagination;

	@FindAll({ @FindBy(css = "#accessories .searchpagination.clear"),
			@FindBy(css = "div#accessories div.line-usage-pagination") })
	private WebElement accessoriesPagination;

	@FindBys(@FindBy(css = ".tab-pane.support.active .span9.span9m.pl25 .searchsupportdetail.mb30"))
	private List<WebElement> noOfSearchContentsInSupport;

	@FindBys(@FindBy(css = "#devices .span9.span9m .span4.spacbot4.span4d"))
	private List<WebElement> noOfSearchContentsInDevices;

	@FindBys(@FindBy(css = "#accessories .span9.acce-col.span9m .span4.spacbot4.span4accessories"))
	private List<WebElement> noOfSearchContentsInAccessories;

	@FindBy(css = "#Support")
	private WebElement supportTab;

	@FindBy(css = "#Devices")
	private WebElement devicesTab;

	@FindBy(css = "li.devices.active")
	private WebElement devicesTabActive;

	@FindBy(css = "li.accessories a")
	private WebElement accessoriesTab;

	@FindBys(@FindBy(css = ".leftlinkcont ul li"))
	private List<WebElement> mobileYourSelectionTextSupport;

	@FindBy(css = "#devices .span3.span3m ul li")
	private WebElement yourSelectionTextDevices;

	@FindBys(@FindBy(css = ".ui_third_tier_headline strong"))
	private List<WebElement> mobileSelectionTextDevices;

	@FindBys(@FindBy(css = "#accessories .ui_third_tier_headline strong"))
	private List<WebElement> mobileSelectionTextAccessories;

	@FindBy(css = "#accessories .span3.span3m ul li")
	private WebElement yourSelectionTextAccessories;

	private String accessoriesText = " ";

	private String deviceText = " ";

	@FindBy(css = "#support .span3.span3m ul li")
	private WebElement yourSelectionTextSupport;

	@FindBy(id = "Internet-Based Ads")
	private WebElement interestBasedAdsLink;

	@FindBy(css = "[data-analytics-value*='Open Internet']")
	private WebElement openInternetPolicyLink;

	@FindBy(xpath = "//*[contains(text(),'Privacy Resources')]")
	private WebElement privacyResourcesLink;

	@FindBy(linkText = "Privacy Resources")
	private WebElement privacyResourcesLinkDesktop;

	@FindBy(xpath = "//h2[contains(text(),'Lifeline')]")
	private WebElement PuertoRicoCustomerHeader;

	@FindBy(xpath = "//h2[contains(text(),'VIP')]")
	private WebElement vipCustomerHeader;

	@FindBy(xpath = "//h2[contains(text(),'Government')]")
	private WebElement governmentCustomerHeader;

	@FindBy(xpath = "//*[contains(text(),'We need help transferring your number')]")
	private WebElement portInLink;

	@FindBy(css = ".gf-footer")
	private WebElement checkListForFooter;

	@FindBy(css = "div.co-importantmsgs p")
	private List<WebElement> importantMessage;

	@FindBy(xpath = "//*[text()='My line']/following-sibling::span")
	private WebElement linkedMobileNumber;

	@FindBy(css = ".arrow-down.notification-down")
	private WebElement notificationDiv;

	@FindBy(xpath = "//button[text()=' See all']")
	private WebElement seeAllBtn;

	@FindBy(css = "[u1st-aria-label*='T-Mobile Tuesdays']")
	private WebElement tMobileTuesdayImg;

	@FindBy(css = "[u1st-aria-label*='Wi-Fi calling']")
	private WebElement freeWiFiCallingImg;

	@FindBy(css = "[u1st-aria-label*='Flight']")
	private WebElement goGoInFlightimg;

	@FindBy(css = "#digital-footer-copyright")
	private WebElement copyrightsLink;

	@FindBy(xpath = "//*[contains(text(),'I want to')] /../..")
	private WebElement iWantToDiv;

	@FindBy(xpath = "//a[contains(text(),'Set up AutoPay')]")
	private WebElement autoPaySetupQuickLink;

	@FindBy(xpath = "//*[text()='Change my plan']")
	private WebElement changeMyPlanQuickLink;

	@FindBy(css = "a[data-href='/purchase/device-intent']")
	private WebElement addAPersonOrDeviceQuickLink;

	@FindBy(xpath = "//a[contains(text(),'Report a lost or stolen device')]")
	private WebElement reportALostOrStolenQuickLink;

	@FindBy(css = "a[data-href='/odf/DataService:ALL/Service:ALL/DataPass:ALL']")
	private WebElement manageAddOnsQuickLink;

	@FindBy(xpath = "//*[contains(text(),'Includes $23.96 Restore from Suspend Charge and taxes')]")
	private WebElement serviceRestoreMessage;

	@FindBy(xpath = "//*[contains(text(),'Includes Restore from Suspend Charge and taxes')]")
	private WebElement serviceRestoreMessageForNoRestorationFee;

	@FindBy(xpath = "//span[@gf-footer='md']")
	private WebElement autopayQuickLink;

	@FindBy(xpath = "//span[@gf-footer='cd']")
	private WebElement autopayLinkAtBillingCompnent;

	@FindBy(xpath = "")
	private WebElement storeLocatorQuickLink;

	@FindBy(xpath = "//*[contains(text(),'Family Allowances')]")
	private WebElement familyAllowancesQuickLink;

	@FindBys(@FindBy(css = "[src*='cdn.tmobile.com/images/png/products/phones']"))
	private List<WebElement> imageContentForKnownIMEI;

	@FindBys(@FindBy(css = "[u1st-aria-label='no-image']"))
	private List<WebElement> imageContentForUnKnownIMEI;

	@FindBy(xpath = "//span[contains(text(),'Payment Arrangement')]")
	private WebElement paymentArrangementCTA;

	@FindBy(xpath = "//*[contains(text(),'ACCESS')]")
	private WebElement accessbilityLink;

	@FindBy(xpath = "//*[contains(text(),'PUBLIC')]")
	private WebElement publicSafetyLink;

	@FindBy(css = "[u1st-aria-label*='Stay connected with calling and texting over Wi-Fi.']")
	private WebElement freeWiFiCallingText1;

	@FindBy(css = "[u1st-aria-label*='With capable phone & Wi-Fi connection.']")
	private WebElement freeWiFiCallingText2;

	@FindBy(linkText = "Manage add-ons")
	private WebElement manageAddOnsLink;

	@FindBy(css = "div[class*='align-items-center'] a[role='link']")
	private List<WebElement> headerMenuLinks;

	private final String placeHolderPageUrl = "/plan-benefits";

	@FindBy(css = "img[aria-label*='WiFi calling']")
	private WebElement freeWiFiCallingImgReg2;

	@FindBy(css = "img[aria-label*='WiFi calling']")
	private WebElement tMobileTuesdayImgReg2;

	@FindBy(css = "img[aria-label*='flight']")
	private WebElement goGoInFlightImgReg2;

	@FindBy(css = ".gbl-logo")
	private WebElement tMobileHomeLink;

	@FindBy(xpath = "//span[contains(text(),'Other')]")
	private WebElement otherLineSection;

	@FindBy(xpath = "//span[contains(text(),'Benefits')]")
	private WebElement benefitsSection;

	@FindBy(css = "button[data-href*='onetimepayment']")
	private WebElement payBillButton;

	@FindBy(css = ".notification-count.notification-down")
	private WebElement alertCount;

	@FindBy(css = "[u1st-aria-label*='No payment']")
	private WebElement noPaymentHeader;

	@FindBy(xpath = "//button[contains(text(),'Call to upgrade early')]")
	private WebElement callToUpgradeText;

	@FindBy(xpath = "//a[contains(text(),'Support')]")
	private WebElement supportLink;

	@FindBy(xpath = "//a[contains(text(),'Contact us')]")
	private WebElement contactUSLink;

	@FindBy(xpath = "//a[contains(text(),'Store locator') or contains(text(),'Store Locator')]")
	private WebElement storeLocatorLink;

	@FindBy(xpath = "//a[contains(text(),'Coverage')]")
	private WebElement footercoverageLink;

	@FindBy(xpath = "//a[contains(text(),'T-Mobile.com')]")
	private WebElement tmobileLink;

	@FindBy(xpath = "//a[contains(text(),'order') or contains(text(),'Order')]")
	private WebElement orderStatusQuickLink;

	@FindBy(xpath = "//a[contains(text(),'rebate') or contains(text(),'Rebate')]")
	private WebElement rebateStatusQuickLink;

	@FindBy(xpath = "//*[contains(text(),'trade')]")
	private WebElement tradeInStatusQuickLink;

	@FindBy(xpath = "//*[contains(text(),'Suspend Charge')]")
	private WebElement accountRestoreMessage;

	@FindBy(css = "#doneButton")
	private WebElement announcementModalWindow;

	@FindBy(xpath = "//*[contains(text(),'Purchase')]")
	private WebElement purchaseAccesoriesQuickLink;

	@FindBy(xpath = "//*[contains(text(),'Financially')]")
	private WebElement messageInNewBillLandingPage;

	@FindBy(css = "p.body-copy-descripton")
	private WebElement restrictedMessageContent;

	@FindBy(css = "#callMe")
	private WebElement callTeamCTA;

	@FindBy(css = ".scheduleCallButton")
	private WebElement messageCTA;

	@FindBys(@FindBy(xpath = "//*[contains(text(),' PM') or contains(text(),' pm')]"))
	private List<WebElement> hoursAvailabiltyDiv;

	@FindBy(css = "#setUpCallModal")
	private WebElement setUpCallPopUp;

	@FindBy(css = "div#headline[aria-label*='schedule']")
	private WebElement setUpCallPopUpHeaderText;

	@FindBy(css = "div.Display3.text-center")
	private WebElement setUpCallPopUpHeaderTextForIOS;

	@FindBy(css = "img.d-lg-block")
	private WebElement teamPictureOnNewAngularPage;

	@FindBy(css = "img.d-block.d-lg-none")
	private WebElement teamPictureOnNewAngularPageForIOS;

	@FindBy(css = ".Display3")
	private WebElement teamOfExpertsDivOnNewAngularPage;

	@FindBy(xpath = "//*[contains(text(),'Make a change')]")
	private WebElement makeAChangeBtn;

	@FindBy(id = "makeAChangeSchedule")
	private WebElement makeAChangeScheduleBtn;

	@FindBy(xpath = "//*[text()='Cancel']")
	private WebElement cancelBtn;

	@FindBy(xpath = "//*[text()='Schedule a call']")
	private WebElement scheduleACallBtn;

	@FindBy(xpath = "//*[text()='Confirm']")
	private WebElement confirmBtn;

	@FindBys(@FindBy(css = "button[id*='dateButton']"))
	private List<WebElement> timeSlots;

	@FindBy(xpath = "//*[text()='Done']")
	private WebElement doneBtn;

	@FindBy(css = ".gb-secondary-cta-accent")
	private WebElement cancelCallBack;

	@FindBy(css = "a[ng-bind='$ctrl.aalWarningModalContent.ctaLabel']")
	private WebElement contactUsButton;

	@FindBy(xpath = "//*[text()='Device & Coverage']")
	private WebElement deviceCoverageHeader;

	@FindBy(xpath = "//*[text()='Plans & Usage ']")
	private WebElement plansUsageHeader;

	@FindBy(css = ".PrimaryCTA-accent")
	private WebElement errorModalPopUp;

	@FindBy(css = ".SecondaryCTA")
	private WebElement callTeamBtn;

	@FindBy(css = ".img_smiley_resize")
	private WebElement starImage;

	@FindBy(xpath = "//*[contains(text(),'We’ve made your Mobile Internet')]")
	private WebElement accountManagementHeader;

	@FindBy(xpath = "//a[contains(text(),'Mobile')]")
	private WebElement mobileInternetManagementSite;

	@FindBy(css = "[data-analytics-value*='Refill'],[data-analytics-value*='REFILL']")
	private WebElement refillUnavMenu;

	@FindBy(xpath = "//span[contains(text(),'REFILL') or contains(text(),'Refill')]")
	private WebElement refillUnavMenuForMobile;

	@FindBy(css = "#digital-header-nav-link-1")
	private WebElement accountUnavMenu;

	@FindBy(css = "#digital-header-nav-link-1")
	private WebElement accountUnavMenuForMobile;

	@FindBy(css = "#digital-header-nav-link-2")
	private WebElement phoneUnavMenu;

	@FindBy(css = "#digital-header-nav-link-2")
	private WebElement phoneUnavMenuForMobile;

	@FindBy(xpath = "//a[contains(text(),'Voicemail')]")
	private WebElement voicemailQuickLink;

	@FindBy(xpath = "//*[contains(@u1st-aria-label,'Current Plan')]/../../div/div/span")
	private WebElement planBladeDivInMyLineSection;

	@FindBy(xpath = "//*[contains(@aria-label,'Current Plan')]/../../div/div/span")
	private WebElement planBladeDivInMyLineSectionForMobile;

	@FindBy(xpath = "//*[contains(@u1st-aria-label,'iPhone 5')]/../../div/div/span")
	private WebElement deviceBladeDivInMyLineSection;

	@FindBy(xpath = "//*[contains(@aria-label,'iPhone 5')]/../../div/div/span")
	private WebElement deviceBladeDivInMyLineSectionForMobile;

	@FindBy(xpath = "//*[contains(@u1st-aria-label,'Minutes')]/../../div/div")
	private WebElement usageBladeDivInMyLineSection;

	@FindBy(xpath = "//*[contains(@aria-label,'Minutes')]/../../div/div")
	private WebElement usageBladeDivInMyLineSectionForMobile;

	@FindBy(css = "[u1st-aria-label*='$']")
	private WebElement accountBalanceDiv;

	@FindBy(css = "span[aria-label*='$']:not(.legal)")
	private WebElement accountBalanceDivForMobile;

	@FindBy(css = "[u1st-aria-label*='Refill']")
	private WebElement refillAccountCTA;

	@FindBy(css = "#digital-header-nav-link-0")
	private WebElement refillAccountCTAForMobile;

	@FindBy(css = "[u1st-aria-label*='Account status']")
	private WebElement accountStatusDiv;

	@FindBy(css = "[aria-label*='Account status']")
	private WebElement accountStatusDivForMobile;

	@FindBy(css = "[u1st-aria-label*='Next payment amount']")
	private WebElement nextPaymentAmountDiv;

	@FindBy(css = "[aria-label*='Next payment amount']")
	private WebElement nextPaymentAmountDivForMobile;

	@FindBy(css = "[u1st-aria-label*='Next charge date']")
	private WebElement nextChargeDiv;

	@FindBy(css = "[aria-label*='Next charge date']")
	private WebElement nextChargeDivForMobile;

	@FindBy(css = "[data-href*='account-overview'][role='link']")
	private WebElement viewAccountTextInMyLineSection;

	@FindBy(css = "[data-href*='account-overview'][routerlinkactive]")
	private WebElement viewAccountTextOtherLineSection;

	@FindBy(css = "#digital-header-utility-2")
	private WebElement searchIcon;

	@FindBy(css = ".d-inline.show-on-sm-modal")
	private WebElement searchMenuForMobile;

	@FindBy(css = "[aria-label*='search']")
	private WebElement tmoSearchIcon;

	@FindBy(css = "i[aria-label*='search']")
	private WebElement tmoSearchIconForMobile;

	@FindBy(css = "input#searchId")
	private WebElement searchIconTextBox;

	@FindBy(css = "#nav_search_text")
	private WebElement searchIconTextBoxForMobile;

	@FindBy(css = "[data-analytics-value*='Search']:not(#nav_search_text)")
	private WebElement searchIconForMobile;

	@FindBy(css = ".deviceImg")
	private List<WebElement> tmoSearchResults;

	@FindBy(xpath = "//*[contains(text(),'Data Maximizer')]/../../..")
	private WebElement dataMaximiserDiv;

	@FindBy(css = ".slider.round[aria-label='OFF']")
	private WebElement dataMaximiserToggleOffState;

	@FindBy(css = ".slider.round[aria-label='ON']")
	private WebElement dataMaximiserToggleOnState;

	@FindBy(css = ".slider.round")
	private WebElement dataMaximiserToggle;

	@FindBy(css = "img[src*='decoration']")
	private WebElement legacyPageMyTmoLogo;

	@FindBy(xpath = "//*[contains(text(),'Complete account setup')]")
	private WebElement completeAccountSetUpLink;

	@FindBy(css = ".body-copy-descripton h2")
	private WebElement prepaidWelcomeText;

	@FindBy(css = ".body-copy-descripton p")
	private WebElement customerCareHeader;

	@FindBy(css = ".body-copy-descripton p")
	private WebElement linkedAccountMissdn;

	@FindBy(xpath = "//p[contains(text(), 'Featured phones')]")
	private WebElement featuredPhonesSection;

	@FindBy(css = "#seeLegalModal")
	private WebElement legalTermsModalWindow;

	@FindBy(css = "div#bannerDiv a.tmo-modal-link")
	private WebElement legalTermsLink;

	@FindBy(css = "#seeLegalModal")
	private WebElement modalWindowTitle;

	@FindBy(css = "#seeLegalModal")
	private WebElement modalWindowDisclaimerText;

	@FindBy(css = "#seeLegalModal")
	private WebElement modalWindowCloseCTA;

	@FindBy(css = ".bg-color-red")
	private WebElement webAlert;

	@FindBy(css = "#user-links-dropdown")
	private WebElement myAccountDropDown;

	@FindBy(xpath = "//button[contains(text(),'REFILL') or contains(text(),'Refill')]")
	private WebElement refillInMyAccountSection;

	@FindBy(xpath = "//*[contains(text(),'Switch')]")
	private WebElement switchAccount;
	
	@FindBy(css = "img[src*='quick']~a")
	private List<WebElement> availableQuickLinks;

	private static final String prepaidText = "Welcome Prepaid Customers";

	private static final String customerCareText = "To make changes to your Prepaid rate plan and or to change a feature on your account, please contact Customer Care at 1-877-778-2106";

	/**
	 * 
	 * @param webDriver
	 */
	public NewHomePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Home Page
	 * 
	 * @return
	 */
	public NewHomePage verifyHomePage() {
		try {
			checkPageIsReady();
			waitforSpinner();
			if (isElementDisplayed(announcementModalWindow)) {
				announcementModalWindow.click();
			}
			// waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			// waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			waitFor(ExpectedConditions.urlContains(pageUrl));
			Reporter.log("Home page is displayed");
		} catch (Exception e) {
			Assert.fail("Home page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Home Page
	 * 
	 * @return
	 */
	public NewHomePage verifyPRHomePage() {
		try {
			waitFor(ExpectedConditions.titleContains("Inicio"));
			Reporter.log("Navigated to PR home page");
		} catch (Exception e) {
			Assert.fail("PortRicho homepage is not displayed");
		}

		return this;
	}

	/**
	 * Verify last login div
	 */
	public NewHomePage verifyLastLoginDiv() {
		try {
			Assert.assertTrue(isElementDisplayed(lastLoginDiv), "Last login div not displayed");
			Reporter.log("Last login div displayed");
		} catch (AssertionError e) {
			Assert.fail("Last login div not displayed");
		}
		return this;
	}

	/**
	 * Verify welcome text
	 */
	public NewHomePage verifyWelcomeText() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(welcomeTextForMobile), "Welcome text not displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(welcomeText));
				Assert.assertTrue(isElementDisplayed(welcomeText), "Welcome text not displayed");
			}
			Reporter.log("Welcome text displayed");
		} catch (AssertionError e) {
			Assert.fail("Welcome text not displayed");
		}
		return this;
	}

	/**
	 * Verify customer account number
	 */
	public NewHomePage verifyCustomerAccountNumber() {
		try {
			waitFor(ExpectedConditions.visibilityOf(accountDiv));
			Assert.assertTrue(isElementDisplayed(accountDiv), "customer account number not displayed");
			Reporter.log("Customer account number displayed");
		} catch (AssertionError e) {
			Assert.fail("Customer account number not displayed");
		}
		return this;
	}

	/**
	 * Verify banner ads
	 */
	public NewHomePage verifyBannerAds() {
		try {
			Assert.assertTrue(isElementDisplayed(accountDiv), "Banner ads not displayed");
			Reporter.log("Banner ads displayed");
		} catch (AssertionError e) {
			Assert.fail("Banner ads not displayed");
		}
		return this;
	}

	/**
	 * Verify my line section
	 */
	public NewHomePage verifyMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(myLineSectionForMobile));
				Assert.assertTrue(isElementDisplayed(myLineSectionForMobile), "My line section not displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(myLineSection));
				Assert.assertTrue(isElementDisplayed(myLineSection), "My line section not displayed");
			}
			Reporter.log("My line section displayed");
		} catch (AssertionError e) {
			Assert.fail("My line section not displayed");
		}
		return this;
	}

	/**
	 * Verify view details CTA
	 */
	public NewHomePage verifyViewDetailsCTA() {
		try {
			Assert.assertTrue(isElementDisplayed(viewDetailsCTA), "View details CTA not displayed");
			Reporter.log("View details CTA displayed");
		} catch (AssertionError e) {
			Assert.fail("View details CTA not displayed");
		}
		return this;
	}

	/**
	 * Click view details CTA
	 */
	public NewHomePage clickViewDetailsCTA() {
		try {
			viewDetailsCTA.click();
		} catch (AssertionError e) {
			Assert.fail("Click on view details CTA failed ");
		}
		return this;
	}

	/**
	 * Verify device image in my line section
	 */
	public NewHomePage verifyDeviceImageInMyLineSection() {
		try {
			Assert.assertTrue(isElementDisplayed(deviceImageInMyLineSection),
					"Device image in my line section not displayed");
			Reporter.log("Device image in my line section displayed");
		} catch (AssertionError e) {
			Assert.fail("Device image in my line section not displayed");
		}
		return this;
	}

	/**
	 * Verify upgrade cta in my line section
	 */
	public NewHomePage verifyUpgradeCTAInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				if (isElementDisplayed(upgradeCTAInMyLineSectionForMobile))
					Assert.assertTrue(isElementDisplayed(upgradeCTAInMyLineSectionForMobile),
							"Upgrade cta in my line section not displayed");
			} else {
				Assert.assertTrue(isElementDisplayed(upgradeCTAInMyLineSection),
						"Upgrade cta in my line section not displayed");
			}
			Reporter.log("Upgrade cta in my line section displayed");
		} catch (AssertionError e) {
			Assert.fail("Upgrade cta in my line section not displayed");
		}
		return this;
	}

	/**
	 * Click upgrade cta in my line section
	 */
	public NewHomePage clickUpgradeCTAInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				upgradeCTAInMyLineSectionForMobile.click();
			} else {
				upgradeCTAInMyLineSection.click();
			}
		} catch (AssertionError e) {
			Reporter.log("Click on upgrade cta failed");
			Assert.fail("Click on upgrade cta failed");
		}
		return this;
	}

	/**
	 * Verify missdn in my line section
	 */
	public NewHomePage verifyMissdnInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(missdnInMyLineSectionForMobile));
				Assert.assertTrue(isElementDisplayed(missdnInMyLineSectionForMobile),
						"Missdn in my line section not displayed");
			} else {
				Assert.assertTrue(isElementDisplayed(missdnInMyLineSection), "Missdn in my line section not displayed");
			}
			Reporter.log("Missdn in my line section displayed");
		} catch (AssertionError e) {
			Assert.fail("Missdn in my line section not displayed");
		}
		return this;
	}

	/**
	 * Verify profile name
	 * 
	 * @return
	 */
	public NewHomePage verifyProfileName() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(myAccountForMobile));
				myAccountForMobile.click();
				waitFor(ExpectedConditions.visibilityOf(profileMenu));
				profileMenu.isDisplayed();
			} else {
				waitFor(ExpectedConditions.visibilityOf(myAccountDropDown));
				myAccountDropDown.click();
				waitFor(ExpectedConditions.visibilityOf(profileMenu));
				profileMenu.isDisplayed();
			}
			Reporter.log("Profile name displayed");
		} catch (Exception e) {
			Assert.fail("Profile name not dispalyed");
		}
		return this;
	}

	/**
	 * Verify tool tip text on mouse over to upgrade CTA
	 */
	public NewHomePage verifyToolTipTextInMyLineSection() {
		try {
			moveToElement(upgradeCTAInMyLineSection);
			Assert.assertTrue(isElementDisplayed(toolTipText), "Tool tip text not displayed");
			Reporter.log("Tool tip text displayed");
		} catch (AssertionError e) {
			Assert.fail("Tool tip text not displayed");
		}
		return this;
	}

	/**
	 * Verify plan benefit tiles
	 */
	public NewHomePage verifyPlanBenefitTiles() {
		try {
			Assert.assertTrue(isElementDisplayed(planBenefitTiles), "Plan benefit tiles not displayed");
			Reporter.log("Plan benefit tiles displayed");
		} catch (AssertionError e) {
			Assert.fail("Plan benefit tiles not displayed");
		}
		return this;
	}
	
	/**
	 * Click view details CTA
	 */
	public NewHomePage clickSeeMoreCTA() {
		try {
			seeMoreCTA.click();
		} catch (AssertionError e) {
			Assert.fail("Click on see more cta failed ");
		}
		return this;
	}

	/**
	 * Verify view bill CTA
	 */
	public NewHomePage verifyViewBillCTA() {
		try {
			Assert.assertTrue(isElementDisplayed(viewBillCTA), "View bill CTA not displayed");
			Reporter.log("View bill CTA displayed");
		} catch (AssertionError e) {
			Assert.fail("View bill CTA not displayed");
		}
		return this;
	}

	/**
	 * Verify Due date
	 */
	public NewHomePage verifyDueDate() {
		try {
			Assert.assertTrue(isElementDisplayed(dueDate), "Due date not displayed");
			Reporter.log("Due date displayed");
		} catch (AssertionError e) {
			Reporter.log("Due date not displayed");
			Assert.fail("Due date not displayed");
		}
		return this;
	}

	/**
	 * Verify Amount due
	 */
	public NewHomePage verifyAmountDue() {
		try {
			Assert.assertTrue(isElementDisplayed(amountDue), "Amount due not displayed");
			Reporter.log("Amount due displayed");
		} catch (AssertionError e) {
			Reporter.log("Amount due not displayed");
			Assert.fail("Amount due not displayed");
		}
		return this;
	}

	/**
	 * Verify payment CTA
	 */
	public NewHomePage verifyPaymentCTA() {
		try {
			Assert.assertTrue(isElementDisplayed(paymentCTA), "Payment CTA not displayed");
			Reporter.log("Payment CTA displayed");
		} catch (AssertionError e) {
			Assert.fail("Payment CTA not displayed");
		}
		return this;
	}

	/**
	 * Verify Last payment
	 */
	public NewHomePage verifyLastPayment() {
		try {
			Assert.assertTrue(isElementDisplayed(lastPayment), "Last payment not displayed");
			Reporter.log("Last payment displayed");
		} catch (AssertionError e) {
			Assert.fail("Last payment not displayed");
		}
		return this;
	}

	/**
	 * Verify Past Due Alert
	 */
	public NewHomePage verifyPastDueAlert() {
		try {
			Assert.assertTrue(isElementDisplayed(pastDueAlert), "Past due alert not displayed");
			Reporter.log("Past due alert displayed");
		} catch (AssertionError e) {
			Assert.fail("Past due alert not displayed");
		}
		return this;
	}

	/**
	 * Verify Past due payment CTA
	 */
	public NewHomePage verifyPastDuePaymentCTA() {
		try {
			Assert.assertTrue(isElementDisplayed(pastDuePaymentCTA), "Past due payment CTA not displayed");
			Reporter.log("Past due paymentCTA displayed");
		} catch (AssertionError e) {
			Assert.fail("Past due payment CTA not displayed");
		}
		return this;
	}

	/**
	 * Verify autopay CTA
	 */
	public NewHomePage verifyAutoPayCTA() {
		try {
			Assert.assertTrue(isElementDisplayed(autoPayCTA), "Autopay CTA not displayed");
			Reporter.log("Autopay CTA displayed");
		} catch (AssertionError e) {
			Assert.fail("Autopay CTA not displayed");
		}
		return this;
	}

	/**
	 * Click profile menu
	 */
	public NewHomePage clickProfileMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitforSpinner();
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				moveToElement(myAccountForMobile);
				waitFor(ExpectedConditions.visibilityOf(myAccountForMobile));
				myAccountForMobile.click();
				waitFor(ExpectedConditions.visibilityOf(mobileProfileMenu));
				mobileProfileMenu.click();

			} else {
				waitforSpinner();
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf(myAccountDropDown));
				myAccountDropDown.click();
				waitFor(ExpectedConditions.visibilityOf(profileMenu));
				profileMenu.click();
			}
		} catch (Exception e) {
			Reporter.log("Click on profile menu failed");
		}
		return this;
	}

	/**
	 * Click tmobile icon.
	 */
	public NewHomePage clickTMobileIcon() {
		try {
			tMobileIcon.click();
		} catch (Exception e) {
			Reporter.log("Click on tmobile icon failed");
			Assert.fail("Click on tmobile icon failed");
		}
		return this;
	}

	/**
	 * Click account history menu
	 */
	public NewHomePage clickAccountHistoryMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(myAccountForMobile));
				myAccountForMobile.click();
				waitFor(ExpectedConditions.visibilityOf(accountHistoryMenuForMobile));
				accountHistoryMenuForMobile.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(myAccountDropDown));
				myAccountDropDown.click();
				waitFor(ExpectedConditions.visibilityOf(accountHistoryMenu));
				accountHistoryMenu.click();
			}
		} catch (Exception e) {
			Reporter.log("Click on account history menu failed");
			Assert.fail("Click on account history menu failed");
		}
		return this;
	}

	/**
	 * Click Billing Link
	 */
	public NewHomePage clickBillingLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(billMenuForMobile));
				billMenuForMobile.click();
			} else {
				clickElement(billMenu);
			}
			Reporter.log("Clicked on billing link");
		} catch (Exception e) {
			Reporter.log("Click on billing link failed");
			Assert.fail("Billing link not found");
		}
		return this;
	}

	/**
	 * Click shop menu
	 * 
	 * @return
	 */
	public NewHomePage clickShoplink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(shopMenuForMobile));
				shopMenuForMobile.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(shopMenu));
				shopMenu.click();
			}
			Reporter.log("Clicked on shop menu");
		} catch (Exception e) {
			Reporter.log("Click on shop menu failed");
			Assert.fail("Click on shop menu failed");
		}
		return this;
	}

	/**
	 * Verify announcement modal window
	 * 
	 * @return
	 */
	public NewHomePage verifyannouncementModalWindow() {
		try {
			checkPageIsReady();
			waitforSpinner();
			waitFor(ExpectedConditions.urlContains(pageUrl));
			if (isElementDisplayed(announcementModalWindow)) {
				announcementModalWindow.click();
			}

		} catch (Exception e) {
			Assert.fail("Home page is not displayed");
		}
		return this;
	}

	/**
	 * Verify modal pop up
	 */
	public NewHomePage verifyModalPopUp() {
		try {
			Assert.assertTrue(isElementDisplayed(modalPopUp));
			Reporter.log("modal pop up displayed");
		} catch (AssertionError e) {
			Assert.fail("unable to verify the modal pop up ");
		}
		return this;
	}

	/**
	 * Verify modal pop up is disappeared
	 */
	public NewHomePage verifyModalPopUpIsNotDisplayed() {
		try {
			Assert.assertTrue(!isElementDisplayed(modalPopUp));
			Reporter.log("modal pop up is disappeared");
		} catch (AssertionError e) {
			Assert.fail("unable to verify the modal pop up ");
		}
		return this;
	}

	/**
	 * Verify the content on modal pop up
	 */
	public NewHomePage verifyContentInModalPopUp() {
		try {
			Assert.assertTrue(isElementDisplayed(modalPopUpAuthoringMessage));
			Assert.assertTrue(isElementDisplayed(continueButtonOnPopUp));
			Reporter.log("Authoring content and continue CTA are displayed on modal pop up successfully");
		} catch (AssertionError e) {
			Assert.fail("unable to verify the modal pop up content ");
		}
		return this;
	}

	/**
	 * To verify search field-Insert data and search
	 * 
	 * @param dataToEnter
	 */
	public NewHomePage enterSearchData(String dataToEnter) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(searchIcon));
				searchIcon.click();
				waitFor(ExpectedConditions.visibilityOf(searchIconTextBoxForMobile));
				sendTextData(searchIconTextBoxForMobile, dataToEnter);
				Reporter.log("Entered data in search box");
				waitFor(ExpectedConditions.visibilityOf(tmoSearchIconForMobile));
				tmoSearchIconForMobile.click();
			} else {
				searchIcon.click();
				sendTextData(searchTextBox, dataToEnter);
				Reporter.log("Entered data in search box");
				search.click();
				Reporter.log("Clicked on search button");
			}
		} catch (Exception e) {
			Assert.fail("Search component not found");
		}
		return this;
	}

	/***
	 * click on continue button
	 * 
	 * @param
	 * @return
	 */
	public NewHomePage clickContinueCTAOnModalPopUp() {

		try {
			checkPageIsReady();
			clickElement(continueButtonOnPopUp);
			Reporter.log("Clicked on continue button");
		} catch (Exception e) {
			Assert.fail("Unable to validate continue button");
		}
		return this;
	}

	/***
	 * verifying the auto complete functionality for line selector field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public NewHomePage verifyAutoCompleteSearchResultsForHomePage(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			if (isElementDisplayed(searchResults.get(0))) {
				for (WebElement ele : searchResults) {
					if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
						actListOfValuesThatMatchedTheSearchValue++;
					}
				}
				Assert.assertEquals(searchResults.size(), actListOfValuesThatMatchedTheSearchValue);
				Reporter.log("Autocomplete search functionality works for search field");
			} else {
				Reporter.log("No results found");
			}
		} catch (Exception e) {
			Assert.fail("failed to verify the Autocomplete search functionality for search field");
		}
		return this;
	}

	/**
	 * Click phone menu
	 */
	public NewHomePage clickPhoneMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(phoneMenuForMobile));
				phoneMenuForMobile.click();
			} else {
				clickElement(phoneMenu);
			}
			Reporter.log("Clicked on phone menu");
		} catch (Exception e) {
			Reporter.log("Click on phone menu failed");
			Assert.fail("Click on phone menu failed");
		}
		return this;
	}

	/**
	 * Click Usage menu
	 */
	public NewHomePage clickUsageMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(usageMenuForMobile));
				usageMenuForMobile.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(usageMenu));
				usageMenu.click();
			}
			Reporter.log("Clicked on usage menu");
		} catch (Exception e) {
			Reporter.log("Click on usage menu failed");
			Assert.fail("Click on usage menu failed");
		}
		return this;
	}

	/**
	 * Click plan menu
	 */
	public NewHomePage clickPlanMenu() {
		try {
			waitFor(ExpectedConditions.visibilityOf(planMenu));
			clickElement(planMenu);
			Reporter.log("Clicked on plan menu");
		} catch (Exception e) {
			Reporter.log("Click on plan menu failed");
			Assert.fail("Click on plan menu failed");
		}
		return this;
	}

	/**
	 * verify payment link is hidden
	 */
	public NewHomePage verifyPaymentLinkIsHidden() {
		try {
			Assert.assertTrue(paymentLink.size() == 0);
			Reporter.log("Payment link is hidden");
		} catch (Exception e) {
			Reporter.log("unable to validate payment link");
			Assert.fail("unable to validate payment link");
		}
		return this;
	}

	/**
	 * Click contactus link
	 */
	public NewHomePage clickContactUSlink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
			}
			waitFor(ExpectedConditions.visibilityOf(contactUsLink));
			clickElementWithJavaScript(contactUsLink);
		} catch (Exception e) {
			Verify.fail("Click on contact us link failed ");
		}
		return this;
	}

	/**
	 * click coverage link
	 */

	public NewHomePage clickCoverageLink() {
		try {
			moveToElement(coverageLink);
			waitFor(ExpectedConditions.visibilityOf(coverageLink), 2);
			clickElementWithJavaScript(coverageLink);
			Reporter.log("Clicked on Coverage link");
			if (getDriver() instanceof AppiumDriver) {
				acceptIOSAlert();
			}
		} catch (Exception e) {
			Assert.fail("Coverage link not found");
		}
		return this;
	}

	/**
	 * Click return policy link
	 */
	public NewHomePage clickReturnPolicyLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(returnPolicyLink));
			returnPolicyLink.click();
			Reporter.log("Clicked on return policy link");
			if (getDriver() instanceof AppiumDriver) {
				Alert alert = getDriver().switchTo().alert();
				alert.accept();
			}
		} catch (Exception e) {
			Assert.fail("Return policy link not found");
		}
		return this;
	}

	/**
	 * Click Footer support link
	 */
	public NewHomePage clickFooterSupportlink() {
		try {
			moveToElement(footerSupportLink);
			waitFor(ExpectedConditions.visibilityOf(footerSupportLink), 2);
			clickElementWithJavaScript(footerSupportLink);
			Reporter.log("Clicked on Footer Support link");
		} catch (Exception e) {
			Assert.fail("Footer support link not found");
		}
		return this;
	}

	/**
	 * Click Footer support link
	 */
	public NewHomePage clickFooterStoreLocatorlink() {
		try {
			moveToElement(footerStoreLocatorLink);
			waitFor(ExpectedConditions.visibilityOf(footerStoreLocatorLink), 2);
			clickElementWithJavaScript(footerStoreLocatorLink);
			Reporter.log("Clicked on footer store locator link");
		} catch (Exception e) {
			Assert.fail("Click on footer store locator link failed");
		}
		return this;
	}

	/**
	 * Click About T Mobile Link
	 */
	public NewHomePage clickAboutTmobileLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(aboutTmobileLink));
			aboutTmobileLink.click();
			Reporter.log("Clicked on 'About T-mobile link'");
		} catch (Exception e) {
			Assert.fail("About T-mobile link not found");
		}
		return this;
	}

	/**
	 * Click Privacy Policy Link
	 */
	public NewHomePage clickPrivacyPolicyLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(privacyPolicyLink));
			privacyPolicyLink.click();
			Reporter.log("Clicked on privacy policy link");
		} catch (Exception e) {
			Reporter.log("Privacy policy link not found");
		}
		return this;
	}

	/**
	 * Click Terms And Conditions Link
	 */
	public NewHomePage clickTermsAndConditionsLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(termsAndConditionsLinkForMobile);
				termsAndConditionsLinkForMobile.click();
				acceptIOSAlert();
			} else {
				waitFor(ExpectedConditions.visibilityOf(termsAndConditionsDesktop));
				clickElementWithJavaScript(termsAndConditionsDesktop);
			}
		} catch (Exception e) {
			Assert.fail("Terms and conditions link not found");
		}
		return this;
	}

	/**
	 * Get Support Tab
	 * 
	 * @return getSupportTab
	 */
	public NewHomePage getSupportTab() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#ajax-spinner")));
			waitFor(ExpectedConditions.visibilityOf(supportTab));
			supportTab.isDisplayed();
			Reporter.log("Support link displayed");
		} catch (Exception e) {
			Assert.fail("Support link not found");
		}
		return this;
	}

	/**
	 * Get Devices Tab
	 * 
	 * @return getDevicesTab
	 */
	public NewHomePage getDevicesTab() {
		try {
			devicesTab.isDisplayed();
			Reporter.log("Devices link displayed");
		} catch (Exception e) {
			Assert.fail("Devices link not found");
		}
		return this;
	}

	/**
	 * Click Devices Tab
	 */
	public NewHomePage clickDevicesTab() {
		try {
			devicesTab.click();
			Reporter.log("Clikced on devices link");
		} catch (Exception e) {
			Assert.fail("Devices link not found");
		}
		return this;
	}

	/**
	 * Get Accessories Tab
	 * 
	 * @return getAccessoriesTab
	 */
	public NewHomePage getAccessoriesTab() {
		try {
			accessoriesTab.isDisplayed();
			Reporter.log("Get accessaries link displayed");
		} catch (Exception e) {
			Assert.fail("Accessaries link not found");
		}
		return this;
	}

	/**
	 * Click Accessories Tab
	 * 
	 * @return getAccessoriesTab
	 */
	public NewHomePage clickAccessoriesTab() {
		try {
			accessoriesTab.click();
			Reporter.log("Clicked on accessaries link");
		} catch (Exception e) {
			Assert.fail("Accessaries link not found");
		}
		return this;
	}

	/**
	 * Get Support Pagination
	 * 
	 * @return getSupportPagination
	 */
	public NewHomePage getSupportPagination() {
		try {
			moveToElement(supportPagination);
			supportPagination.isDisplayed();
			Reporter.log("Support pagination displayed");
		} catch (Exception e) {
			Assert.fail("Support pagination not found");
		}
		return this;
	}

	/**
	 * Get Device Pagination
	 * 
	 * @return getDevicePagination
	 */
	public boolean getDevicePagination() {
		return devicePagination.isDisplayed();
	}

	/**
	 * Verify Search Data
	 * 
	 * @param searchData
	 * @return
	 */
	public NewHomePage getYourSelectionTextSupport(String searchData) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : mobileYourSelectionTextSupport) {
					if (webElement.getText().contains(searchData)) {
						break;
					}
				}
			} else {
				yourSelectionTextSupport.getText().contains(searchData);
			}
		} catch (Exception e) {
			Assert.fail("Search box not found");
		}
		return this;
	}

	/**
	 * Get Your Selection Text Devices
	 * 
	 * @return getYourSelectionTextDevices
	 */
	public NewHomePage getYourSelectionTextDevices() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				deviceText = mobileSelectionTextDevices.get(0).getText();
			} else {
				deviceText = yourSelectionTextDevices.getText().substring(0,
						yourSelectionTextDevices.getText().length() - 6);
				Assert.assertEquals(deviceText, "Apple");
			}
		} catch (Exception e) {
			Assert.fail("Selection Text device not found");
		}
		return this;
	}

	/**
	 * Get Your Selection Text Accessories
	 * 
	 * @return accessoriesText
	 */
	public NewHomePage getYourSelectionTextAccessories() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				accessoriesText = mobileSelectionTextAccessories.get(0).getText();
			} else {
				accessoriesText = yourSelectionTextAccessories.getText().substring(0,
						yourSelectionTextAccessories.getText().length() - 6);
				Assert.assertEquals(accessoriesText, "Apple");
			}
		} catch (Exception e) {
			Assert.fail("Your Selection Text Accessories not found");
		}
		return this;
	}

	/**
	 * Get Accessories Pagination
	 * 
	 * @return getAccessoriesPagination
	 */
	public NewHomePage getAccessoriesPagination() {
		try {
			accessoriesPagination.isDisplayed();
			Reporter.log("Accesaries pagination displayed");
		} catch (Exception e) {
			Assert.fail("Accesseries pagination not found");
		}
		return this;
	}

	/**
	 * Click Internet Based Ads Link
	 */
	public NewHomePage clickInterestBasedAdsLink() {
		try {
			interestBasedAdsLink.click();
			Reporter.log("Clicked on Interest Based Adds link");
		} catch (Exception e) {
			Assert.fail("Interest Base Ads Link not found");
		}
		return this;
	}

	/**
	 * Click Open Internet Policy Link
	 */
	public NewHomePage clickOpenInternetPolicyLink() {
		try {
			moveToElement(openInternetPolicyLink);
			clickElementWithJavaScript(openInternetPolicyLink);
		} catch (Exception e) {
			Assert.fail("Open Internet Plocy link not found");
		}
		return this;
	}

	/**
	 * Click Privacy Resources Link
	 */
	public NewHomePage clickPrivacyResourcesLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				privacyResourcesLink.click();
				acceptIOSAlert();
			} else {
				waitFor(ExpectedConditions.visibilityOf(privacyResourcesLinkDesktop));
				clickElementWithJavaScript(privacyResourcesLinkDesktop);
			}
		} catch (Exception e) {
			Assert.fail("Privacy resource link not found");
		}
		return this;
	}

	/**
	 * Click logout menu
	 */
	public NewHomePage clickLogOutMenu() {
		if (getDriver() instanceof AppiumDriver) {
			waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
			hamburgerMenu.click();
			waitFor(ExpectedConditions.visibilityOf(myAccountForMobile));
			myAccountForMobile.click();
			waitFor(ExpectedConditions.visibilityOf(logOutMenuForMobile));
			logOutMenuForMobile.click();
		} else {
			waitFor(ExpectedConditions.visibilityOf(myAccountDropDown));
			myAccountDropDown.click();
			waitFor(ExpectedConditions.visibilityOf(logOutMenu));
			clickElement(logOutMenu);
		}
		return this;
	}

	/**
	 * Verify PuertoRico customer
	 * 
	 * @return
	 */
	public NewHomePage verifyPuertoRicoCustomerHeader() {
		try {
			Assert.assertTrue(isElementDisplayed(PuertoRicoCustomerHeader));
			Reporter.log("PuertoRico customer header displayed");
		} catch (AssertionError e) {
			Assert.fail("PuertoRico customer header not displayed");
		}
		return this;
	}

	/**
	 * Verify VIP customer header
	 * 
	 * @return
	 */
	public NewHomePage verifyVIPCustomerHeader() {
		try {
			Assert.assertTrue(isElementDisplayed(vipCustomerHeader));
			Reporter.log("VIP customer header displayed");
		} catch (AssertionError e) {
			Assert.fail("VIP customer header not displayed");
		}
		return this;
	}

	/**
	 * Verify On-boarding checkList For Footer
	 * 
	 * @return
	 */
	public NewHomePage verifyOnBoardingFooterCheckList() {
		try {
			waitFor(ExpectedConditions.visibilityOf(checkListForFooter));
			checkListForFooter.isDisplayed();
			Reporter.log("Checklist for footer displayed");
		} catch (Exception e) {
			Assert.fail("Checklist for footer not found");
		}
		return this;
	}

	/**
	 * verify Important Message On home page
	 * 
	 * @return
	 */
	public NewHomePage verifyImportantMessage(String message) {
		try {
			checkPageIsReady();
			verifyElementBytext(importantMessage, message);
			Reporter.log("Verified important message section contains register for netflix");
		} catch (Exception e) {
			Assert.fail("Important message component not found");
		}
		return this;
	}

	/**
	 * Click account history menu
	 */
	public NewHomePage clickSwitchAccountMenu() {
		try {
			waitFor(ExpectedConditions.visibilityOf(myAccountDropDown));
			myAccountDropDown.click();
			waitFor(ExpectedConditions.visibilityOf(switchAccountMenu));
			switchAccountMenu.click();
		} catch (Exception e) {
			Reporter.log("Click on switch account menu failed");
			Assert.fail("Click on switch account menu failed");
		}
		return this;
	}

	/**
	 * Verify Missdn number.
	 */
	public NewHomePage verifyMissdnNumber(String phoneNumber) {
		try {
			String loggedInPhoneNumber = phoneNumber.replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			String s = linkedMobileNumber.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			Assert.assertEquals(s, loggedInPhoneNumber, "Linked phone number not matched with selected account missdn");
			Reporter.log("Linked phone number matched with selected account missdn");
		} catch (Exception e) {
			Assert.fail("Linked phone number not matched with selected account missdn" + e.toString());
		}
		return this;
	}

	/**
	 * Verify notification divison
	 */
	public NewHomePage verifyNotificationDiv() {
		try {
			Assert.assertTrue(isElementDisplayed(notificationDiv));
			Reporter.log("Notification divison displayed");
		} catch (AssertionError e) {
			Assert.fail("Notification divison not displayed");
		}
		return this;
	}

	/**
	 * Click notification dropdown
	 */
	public NewHomePage clickNotificationDropDown() {
		try {
			waitFor(ExpectedConditions.visibilityOf(notificationDiv));
			clickElement(notificationDiv);
			Reporter.log("Clicked on notification dropdown");
		} catch (AssertionError e) {
			Assert.fail("Click on notification dropdown failed");
		}
		return this;
	}

	/**
	 * Verify see all button
	 */
	public NewHomePage verifySeeAllBtn() {
		try {
			Assert.assertTrue(isElementDisplayed(seeAllBtn));
			Reporter.log("Notification divison displayed");
		} catch (AssertionError e) {
			Assert.fail("Notification divison not displayed");
		}
		return this;
	}

	/**
	 * Click see all button
	 */
	public NewHomePage clickSeeAllBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeAllBtn));
			seeAllBtn.click();
		} catch (AssertionError e) {
			Assert.fail("Click on see all button failed");
		}
		return this;
	}

	/**
	 * Verify tMobile tuesday image
	 */
	public NewHomePage verifyTMobileTuesdayImg() {
		try {
			moveToElement(tMobileTuesdayImg);
			Assert.assertTrue(isElementDisplayed(tMobileTuesdayImg));
			Reporter.log("TMobile tuesday image displayed");
		} catch (AssertionError e) {
			Assert.fail("TMobile tuesday image not displayed");
		}
		return this;
	}

	/**
	 * Verify free wifi calling image
	 */
	public NewHomePage verifyFreeWiFiCallingImg() {
		try {
			Assert.assertTrue(isElementDisplayed(tMobileTuesdayImg));
			Reporter.log("Free wifi calling image displayed");
		} catch (AssertionError e) {
			Assert.fail("Free wifi calling image not displayed");
		}
		return this;
	}

	/**
	 * Verify gogo in flight image
	 */
	public NewHomePage verifyGoGoInFlightImg() {
		try {
			Assert.assertTrue(isElementDisplayed(goGoInFlightimg));
			Reporter.log("GoGo in flight image displayed");
		} catch (AssertionError e) {
			Assert.fail("GoGo in flight image not displayed");
		}
		return this;
	}

	/**
	 * Verify account suspend alert div
	 */
	public NewHomePage verifyAccountSuspendAlertDiv() {
		try {
			Assert.assertTrue(isElementDisplayed(notificationBar));
			Reporter.log("Account suspend alert divison displayed");
		} catch (AssertionError e) {
			Assert.fail("Account suspend alert divison not displayed");
		}
		return this;
	}

	/**
	 * Verify pay now link
	 */
	public NewHomePage verifyPayNowLink() {
		try {
			Assert.assertTrue(isElementDisplayed(notificationBarPayOutLink));
			Reporter.log("Pay now link displayed");
		} catch (AssertionError e) {
			Assert.fail("Pay now link not displayed");
		}
		return this;
	}

	/**
	 * Click pay now link
	 */
	public NewHomePage clickPayNowLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(notificationBarPayOutLink));
			clickElement(notificationBarPayOutLink);
		} catch (AssertionError e) {
			Assert.fail("Click on pay now link failed");
		}
		return this;
	}

	/**
	 * Click pay now link
	 */
	public NewHomePage clickViewBillCTA() {
		try {
			clickElement(viewBillCTA);
			Reporter.log("Click on View Bill Button");
		} catch (AssertionError e) {
			Assert.fail("Click on view bill link failed");
		}
		return this;
	}

	/**
	 * Click copyrights Link
	 */
	public NewHomePage clickCopyrightsLink() {
		try {
			moveToElement(copyrightsLink);
			clickElementWithJavaScript(copyrightsLink);
		} catch (Exception e) {
			Assert.fail("Copyrights link not found");
		}
		return this;
	}

	/**
	 * Verify i want to divison
	 */
	public NewHomePage verifyIWantToDiv() {
		try {
			Assert.assertTrue(isElementDisplayed(iWantToDiv), "I want to divison not displayed");
			Reporter.log("I want to divison displayed");
		} catch (AssertionError e) {
			Assert.fail("I want to divison not displayed");
		}
		return this;
	}

	/**
	 * Verify autopay setup quicklink
	 */
	public NewHomePage verifyAutoPaySetUpQuickLink() {
		try {
			Assert.assertTrue(isElementDisplayed(autoPaySetupQuickLink), "Autopay setup quicklink not displayed");
			Reporter.log("Autopay setup quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Autopay setup quicklink not displayed");
		}
		return this;
	}

	/**
	 * Click on autopay setup quicklink
	 */
	public NewHomePage clickAutoPaySetUpQuickLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(autoPaySetupQuickLink));
			autoPaySetupQuickLink.click();
		} catch (AssertionError e) {
			Assert.fail("Click on autopay setup quicklink failed");
		}
		return this;
	}

	/**
	 * Verify change my plan quicklink
	 */
	public NewHomePage verifyChangeMyPlanQuickLink() {
		try {
			Assert.assertTrue(isElementDisplayed(changeMyPlanQuickLink), "Change my plan quicklink not displayed");
			Reporter.log("Change my plan quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Change my plan not displayed");
		}
		return this;
	}

	/**
	 * Click on change my plan quicklink
	 */
	public NewHomePage clickChangeMyPlanQuickLink() {
		try {
			clickElement(changeMyPlanQuickLink);
		} catch (AssertionError e) {
			Assert.fail("Click on change my plan quicklink failed");
		}
		return this;
	}

	/**
	 * Verify add a person or device quicklink
	 */
	public NewHomePage verifyAddAPersonOrDeviceQuickLink() {
		try {
			Assert.assertTrue(isElementDisplayed(addAPersonOrDeviceQuickLink),
					"Add a person or device quicklink not displayed");
			Reporter.log("Add a person or device quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Add a person or device quicklink not displayed");
		}
		return this;
	}

	/**
	 * Click on add a person or device quicklink
	 */
	public NewHomePage clickAddAPersonOrDeviceQuickLink() {
		try {
			clickElement(addAPersonOrDeviceQuickLink);
		} catch (AssertionError e) {
			Assert.fail("Click on add a person or device quicklink failed");
		}
		return this;
	}

	/**
	 * Verify report a lost or stolen device quicklink
	 */
	public NewHomePage verifyReportALostOrStolenDeviceQuickLink() {
		try {
			Assert.assertTrue(isElementDisplayed(reportALostOrStolenQuickLink),
					"Report a lost or stolen device quicklink not displayed");
			Reporter.log("Report a lost or stolen device quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Report a lost or stolen device quicklink not displayed");
		}
		return this;
	}

	/**
	 * Click on report a lost or stolen device quicklink
	 */
	public NewHomePage clickReportALostOrStolenDeviceQuickLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(reportALostOrStolenQuickLink));
			reportALostOrStolenQuickLink.click();
		} catch (AssertionError e) {
			Assert.fail("Click on Report a lost or stolen device quicklink failed");
		}
		return this;
	}

	/**
	 * Verify manage addOns quickLink
	 */
	public NewHomePage verifyManageAddOnsQuickLink() {
		try {
			Assert.assertTrue(isElementDisplayed(manageAddOnsQuickLink), "Manage add ons quicklink not displayed");
			Reporter.log("Manage add ons quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Manage add ons quicklink not displayed");
		}
		return this;
	}

	/**
	 * Click on change my plan quicklink
	 */
	public NewHomePage clickManageAddOnsQuickLink() {
		try {
			clickElement(manageAddOnsQuickLink);
		} catch (AssertionError e) {
			Assert.fail("Click on manage add ons quicklink failed");
		}
		return this;
	}

	/**
	 * Verify service restore message
	 */
	public NewHomePage verifyServiceRestoreMessage() {
		try {
			Assert.assertTrue(isElementDisplayed(serviceRestoreMessage));
			Reporter.log("Service restore message displayed");
		} catch (AssertionError e) {
			Assert.fail("Service restore message not displayed");
		}
		return this;
	}

	/**
	 * Verify service restore message
	 */
	public NewHomePage verifyServiceRestoreMessageForNoRestorationFee() {
		try {
			Assert.assertTrue(isElementDisplayed(serviceRestoreMessageForNoRestorationFee));
			Reporter.log("Service restore message displayed");
		} catch (AssertionError e) {
			Assert.fail("Service restore message not displayed");
		}
		return this;
	}

	/**
	 * Verify autopay link in quicklinks
	 */
	public NewHomePage verifyAutopayQuickLink() {
		try {
			Assert.assertFalse(isElementDisplayed(autopayQuickLink));
			Reporter.log("Autopay in quicklinks not displayed");
		} catch (AssertionError e) {
			Assert.fail("Autopay in quicklinks displayed");
		}
		return this;
	}

	/**
	 * Verify autopay link in billing component
	 */
	public NewHomePage verifyAutopayLinkAtBillingComponent() {
		try {
			Assert.assertFalse(isElementDisplayed(autopayLinkAtBillingCompnent));
			Reporter.log("Autopay link in billing component not displayed");
		} catch (AssertionError e) {
			Assert.fail("Autopay link in billing component displayed");
		}
		return this;

	}

	/**
	 * Verify store locator link
	 */
	public NewHomePage verifyStoreLocatorInQuickLinks() {
		try {
			Assert.assertFalse(storeLocatorQuickLink.isDisplayed());
			Reporter.log("Store locator link in quicklinks not displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Store locator link in quicklinks displayed");
		}
		return this;
	}

	/**
	 * Verify change my plan quicklink for IS customers
	 */
	public NewHomePage verifyChangeMyPlanQuickLinkForIScustomers() {
		try {
			Assert.assertFalse(isElementDisplayed(changeMyPlanQuickLink), "Change my plan quicklink displayed");
			Reporter.log("Change my plan quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Change my plan quicklink displayed");
		}
		return this;
	}

	/**
	 * Verify manage addOns quickLink for IS customers
	 */
	public NewHomePage verifyManageAddOnsQuickLinkForISCustomers() {
		try {
			Assert.assertFalse(isElementDisplayed(manageAddOnsQuickLink), "Manage add ons quicklink displayed");
			Reporter.log("Manage add ons quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Manage add ons quicklink displayed");
		}
		return this;
	}

	/**
	 * Verify family allowance quick link
	 */
	public NewHomePage verifyFamilyAllowanceInQuickLinks() {
		try {
			Assert.assertTrue(familyAllowancesQuickLink.isDisplayed());
			Reporter.log("FamilyAllowances link in quicklinks displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("FamilyAllowances link in quicklinks not displayed");
		}
		return this;
	}

	/**
	 * Click family allowance quick link
	 */
	public NewHomePage clickFamilyAllowanceQuickLink() {
		try {
			familyAllowancesQuickLink.click();
		} catch (NoSuchElementException e) {
			Assert.fail("Click on family allowances link failed");
		}
		return this;
	}

	/**
	 * Verify image for known IMEI devices
	 */
	public NewHomePage verifyImageForKnownIMEI() {
		String imageDetails = null;
		try {
			for (WebElement image : imageContentForKnownIMEI) {
				imageDetails = image.getAttribute("src");
				Assert.assertFalse(imageDetails.contains("unknown"));
			}
			Reporter.log("Image displayed for known IMEI devices");
		} catch (Exception e) {
			Assert.fail("Image not displayed for known IMEI devices");
		}
		return this;
	}

	/**
	 * Verify grey image for unknown IMEI devices
	 */
	public NewHomePage verifyGrayImageForUnknownIMEIDevices() {
		String imageDetails = null;
		try {
			for (WebElement image : imageContentForUnKnownIMEI) {
				imageDetails = image.getAttribute("src");
				Assert.assertTrue(imageDetails.contains("unknown"));
			}
			Reporter.log("Gray image displayed for unknown IMEI devices");
		} catch (Exception e) {
			Assert.fail("Gray image not displayed for unknown IMEI devices");
		}
		return this;
	}

	/**
	 * Verify payment arrangement cta
	 */
	public NewHomePage verifyPaymentArrangementCTA() {
		try {
			Assert.assertTrue(isElementDisplayed(paymentArrangementCTA));
			Reporter.log("Payment arrangement cta displayed");
		} catch (Exception e) {
			Assert.fail("Payment arrangement cta not displayed");
		}
		return this;
	}

	/**
	 * Verify payment arrangement cta
	 */
	public NewHomePage verifyPaymentArrangementCTANotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(paymentArrangementCTA));
			Reporter.log("Payment arrangement cta not displayed");
		} catch (Exception e) {
			Assert.fail("Payment arrangement cta displayed");
		}
		return this;
	}

	/**
	 * Click accessibility link
	 */

	public NewHomePage clickAccessbilityLink() {
		try {
			accessbilityLink.click();
			if (getDriver() instanceof AppiumDriver) {
				acceptIOSAlert();
			}
		} catch (Exception e) {
			Assert.fail("Click on accessbility link failed");
		}
		return this;
	}

	/**
	 * Click public safety link
	 */

	public NewHomePage clickPublicSafetyLink() {
		try {
			publicSafetyLink.click();
			if (getDriver() instanceof AppiumDriver) {
				acceptIOSAlert();
			}
		} catch (Exception e) {
			Assert.fail("Click on public safety link failed");
		}
		return this;
	}

	/**
	 * Verify free wifi text
	 */
	public NewHomePage verifyFreeWiFiText() {
		try {
			Assert.assertTrue(isElementDisplayed(freeWiFiCallingText1));
			Assert.assertTrue(isElementDisplayed(freeWiFiCallingText2));
			Reporter.log("Free wifi calling text displayed");
		} catch (AssertionError e) {
			Assert.fail("Free wifi calling text not displayed");
		}
		return this;
	}

	/**
	 * Verify change my plan quicklink for non master
	 */
	public NewHomePage verifyChangeMyPlanQuickLinkForNonMaster() {
		try {
			Assert.assertFalse(isElementDisplayed(changeMyPlanQuickLink), "Change my plan quicklink displayed");
			Reporter.log("Change my plan quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Change my plan displayed");
		}
		return this;
	}

	/**
	 * Verify add a person or device quicklink
	 */
	public NewHomePage verifyAddAPersonOrDeviceQuickLinkForNonMaster() {
		try {
			Assert.assertFalse(isElementDisplayed(addAPersonOrDeviceQuickLink),
					"Add a person or device quicklink displayed");
			Reporter.log("Add a person or device quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Add a person or device quicklink displayed");
		}
		return this;
	}

	/**
	 * Verify manage addOns quickLink
	 */
	public NewHomePage verifyManageAddOnsQuickLinkForNonMaster() {
		try {
			Assert.assertFalse(isElementDisplayed(manageAddOnsQuickLink), "Manage add ons quicklink displayed");
			Reporter.log("Manage add ons quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Manage add ons quicklink displayed");
		}
		return this;
	}

	/**
	 * Verify other line section
	 */
	public NewHomePage verifyOtherLineSectionNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(otherLineSection));
			Reporter.log("Other line section not displayed");
		} catch (Exception e) {
			Assert.fail("Other line section displayed");
		}
		return this;
	}

	/**
	 * Verify call us icon
	 */
	public NewHomePage verifyCallUsIconNotDispalyed() {
		try {
			Assert.assertFalse(isElementDisplayed(phoneCallIcon));
			Reporter.log("Other line section displayed");
		} catch (Exception e) {
			Assert.fail("Other line section not displayed");
		}
		return this;
	}

	/**
	 * Verify message us icon
	 */
	public NewHomePage verifyMessageUSIconNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(messageUsIcon));
			Reporter.log("Other line section displayed");
		} catch (Exception e) {
			Assert.fail("Other line section not displayed");
		}
		return this;
	}

	/**
	 * Click tmobile logo
	 */
	public NewHomePage clickTMobileLogo() {
		try {
			tMobileHomeLink.click();
			Reporter.log("Clicked on tmobile logo");
		} catch (Exception e) {
			Assert.fail("Click on tmobile logo failed" + e.toString());
		}
		return this;
	}

	public NewHomePage clickOnManageAddOnsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(manageAddOnsLink));
			manageAddOnsLink.click();
			Reporter.log("Clicked on Manage add ons");
		} catch (AssertionError e) {
			Assert.fail("Manage add ons is not displayed");
		}
		return this;
	}

	public NewHomePage clickOnHeaderMenuLinks(String linkName) {
		try {
			clickElementBytext(headerMenuLinks, linkName);
			Reporter.log("Clicked on " + linkName + "");
		} catch (AssertionError e) {
			Assert.fail(linkName + " link is not displayed");
		}
		return this;
	}

	/**
	 * Verify free wifi calling plan benefit tile in Reg2
	 */
	public NewHomePage verifyTMobileTuesdayTileReg2() {
		try {
			Assert.assertTrue(isElementDisplayed(tMobileTuesdayImgReg2));
			Reporter.log("Tmobile Tuesday plan benefit tile is displayed");
		} catch (AssertionError e) {
			Assert.fail("Tmobile Tuesday plan benefit tile is not displayed");
		}
		return this;
	}

	/**
	 * Verify free wifi calling plan benefit tile in Reg2
	 */
	public NewHomePage verifyFreeWiFiCallingTileReg2() {
		try {
			Assert.assertTrue(isElementDisplayed(freeWiFiCallingImgReg2));
			Reporter.log("Free wifi calling plan benefit tile is displayed");
		} catch (AssertionError e) {
			Assert.fail("Free wifi calling plan benefit tile is not displayed");
		}
		return this;
	}

	/**
	 * Verify Gogo in-flight plan benefit tile in Reg2
	 */
	public NewHomePage verifyGoGoInFlightImgReg2() {
		try {
			Assert.assertTrue(isElementDisplayed(goGoInFlightImgReg2));
			Reporter.log("GoGo in flight plan benefit tile is displayed");
		} catch (AssertionError e) {
			Assert.fail("GoGo in flight plan benefit tile is not displayed");
		}
		return this;
	}

	/**
	 * Verify tMobile tuesday tile enabled
	 */
	public NewHomePage verifyTMobileTuesdayEnabled() {
		try {
			Assert.assertTrue(isElementEnabled(tMobileTuesdayImgReg2));
			Reporter.log("Tmobile Tuesday tile is enabled");
		} catch (AssertionError e) {
			Assert.fail("Tmobile Tuesday tile is not enabled");
		}
		return this;
	}

	/**
	 * Verify Free WiFi Calling tile enabled
	 */
	public NewHomePage verifyFreeWiFiCallingEnabled() {
		try {
			Assert.assertTrue(isElementEnabled(freeWiFiCallingImgReg2));
			Reporter.log("Free wifi calling tile is enabled");
		} catch (AssertionError e) {
			Assert.fail("Free wifi calling tile is not enabled");
		}
		return this;
	}

	/**
	 * Verify GoGo in flight tile enabled
	 */
	public NewHomePage verifyGoGoInFlightEnabled() {
		try {
			Assert.assertTrue(isElementEnabled(goGoInFlightImgReg2));
			Reporter.log("GoGo in flight tile is enabled");
		} catch (AssertionError e) {
			Assert.fail("GoGo in flight tile is not enabled");
		}
		return this;
	}

	/**
	 * Click on tMobile tuesday tile
	 */
	public NewHomePage clickOnTMobileTuesdayTile() {
		try {
			clickElement(tMobileTuesdayImgReg2);
			Reporter.log("Clicked on Tmobile Tuesday tile");
		} catch (AssertionError e) {
			Assert.fail("Tmobile Tuesday tile is not clickable");
		}
		return this;
	}

	/**
	 * Click on Free wifi calling tile
	 */
	public NewHomePage clickOnFreeWiFiCallingTile() {
		try {
			clickElement(freeWiFiCallingImgReg2);
			Reporter.log("Clicked on Free wifi calling tile");
		} catch (AssertionError e) {
			Assert.fail("Free wifi calling tile is not clickable");
		}
		return this;
	}

	/**
	 * Click on GoGo in flight tile
	 */
	public NewHomePage clickOnGoGoInFlightTile() {
		try {
			clickElement(goGoInFlightImgReg2);
			Reporter.log("Clicked on GoGo in flight tile");
		} catch (AssertionError e) {
			Assert.fail("GoGo in flight tile is not clickable");
		}
		return this;
	}

	/***
	 * Verify the Redirected Url Upon clicking on TMobile Tuesday
	 * 
	 * @return
	 */
	public NewHomePage verifyRedirectedUrlUponTMobileTuesdayClick() {
		waitFor(ExpectedConditions.urlContains(placeHolderPageUrl));
		Reporter.log("Redirected to the TMobile Tuesday placeholder URL successfully upon click");
		return this;
	}

	/***
	 * Verify the Redirected Url Upon clicking on Free wifi calling
	 * 
	 * @return
	 */
	public NewHomePage verifyRedirectedUrlUponFreeWiFiCallingClick() {
		waitFor(ExpectedConditions.urlContains(placeHolderPageUrl));
		Reporter.log("Redirected to the Free WiFi Calling placeholder URL successfully upon click");
		return this;
	}

	/***
	 * Verify the Redirected Url Upon clicking on GoGo in flight
	 * 
	 * @return
	 */
	public NewHomePage verifyRedirectedUrlUponGoGoInFlightClick() {
		waitFor(ExpectedConditions.urlContains(placeHolderPageUrl));
		Reporter.log("Redirected to the GoGo in flight placeholder URL successfully upon click");
		return this;
	}

	/**
	 * Verify benefits section
	 */
	public NewHomePage verifyBenefitsSectionNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(benefitsSection));
			Reporter.log("Benefits section not displayed");
		} catch (Exception e) {
			Assert.fail("Benefits section displayed");
		}
		return this;
	}

	/**
	 * Verify footer support link
	 */
	public NewHomePage verifyFooterSupportLink() {
		try {
			Assert.assertTrue(isElementDisplayed(supportLink));
			Reporter.log("Support link displayed");
		} catch (Exception e) {
			Assert.fail("Support link not displayed");
		}
		return this;
	}

	/**
	 * Verify footer contact us link
	 */
	public NewHomePage verifyFooterContactUsLink() {
		try {
			Assert.assertTrue(isElementDisplayed(contactUSLink));
			Reporter.log("ContactUs link displayed");
		} catch (Exception e) {
			Assert.fail("ContactUs link not displayed");
		}
		return this;
	}

	/**
	 * Verify footer store locator link
	 */
	public NewHomePage verifyFooterStoreLocatorLink() {
		try {
			Assert.assertTrue(isElementDisplayed(storeLocatorLink));
			Reporter.log("Store locator link displayed");
		} catch (Exception e) {
			Assert.fail("Store locator link not displayed");
		}
		return this;
	}

	/**
	 * Verify footer coverage link
	 */
	public NewHomePage verifyFooterCoverageLink() {
		try {
			Assert.assertTrue(isElementDisplayed(footercoverageLink));
			Reporter.log("Coverage link displayed");
		} catch (Exception e) {
			Assert.fail("Support link not displayed");
		}
		return this;
	}

	/**
	 * Verify footer tmobile link
	 */
	public NewHomePage verifyFooterTMobileLink() {
		try {
			Assert.assertTrue(isElementDisplayed(tmobileLink));
			Reporter.log("TMobile link displayed");
		} catch (Exception e) {
			Assert.fail("TMobile link not displayed");
		}
		return this;
	}

	/**
	 * Click Pay bill button
	 */
	public NewHomePage clickPayBillbutton() {
		try {
			payBillButton.click();
			Reporter.log("Clicked on PAY BILL button on Home page");
		} catch (Exception e) {
			Assert.fail("Pay bill button not found");
		}
		return this;
	}

	/**
	 * Click account menu
	 */
	public NewHomePage clickAccountMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(accountMenuForMobile));
				accountMenuForMobile.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(accountMenu));
				accountMenu.click();
			}
		} catch (Exception e) {
			Reporter.log("Click on account menu failed");
			Assert.fail("Click on account menu failed");
		}
		return this;
	}

	/**
	 * Verify account history menu
	 */
	public NewHomePage verifyAccountHistoryMenuNotDisplayed() {
		try {
			waitforSpinner();
			checkPageIsReady();
			profileDropDown.click();
			Assert.assertFalse(isElementDisplayed(accountHistoryMenu));
			Reporter.log("Account history menu not displayed");
		} catch (AssertionError e) {
			Reporter.log("Account history menu displayed");
			Assert.fail("Account history menu displayed");
		}
		return this;
	}

	/**
	 * Verify alerts count in notification tray
	 */
	public NewHomePage verifyAlertsCountInNotificationTray() {
		try {
			Assert.assertEquals(alertCount.getText().trim(), "0");
			Reporter.log("Alerts count in notifications tray is zero");
		} catch (AssertionError e) {
			Reporter.log("Alerts count in notifications tray is greater than zero");
			Assert.fail("Alerts count in notifications tray is greater than zero");
		}
		return this;
	}

	/**
	 * Verify no payment header
	 */
	public NewHomePage verifyNoPaymentHeader() {
		try {
			Assert.assertTrue(isElementDisplayed(noPaymentHeader));
			Reporter.log("No payment header displayed");
		} catch (Exception e) {
			Assert.fail("No payment header not displayed");
		}
		return this;
	}

	/**
	 * Verify call to upgrade text
	 */
	public NewHomePage verifyCallToUpgradeText() {
		try {
			Assert.assertTrue(isElementDisplayed(callToUpgradeText), "Call to upgrade early text not displayed");
			Reporter.log("Call to upgrade early text displayed");
		} catch (AssertionError e) {
			Assert.fail("Call to upgrade early text not displayed");
		}
		return this;
	}

	/**
	 * Click call to upgrade text
	 */
	public NewHomePage clickCallToUpgradeText() {
		try {
			callToUpgradeText.click();
		} catch (Exception e) {
			Assert.fail("Click on call to upgrade early tet failed");
		}
		return this;
	}

	/**
	 * Verify add a person or device quicklink
	 */
	public NewHomePage verifyAddAPersonOrDeviceQuickLinkForGovtCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(addAPersonOrDeviceQuickLink),
					"Add a person or device quicklink displayed");
			Reporter.log("Add a person or device quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Add a person or device quicklink displayed");
		}
		return this;
	}

	/**
	 * Verify manage addOns quickLink
	 */
	public NewHomePage verifyManageAddOnsQuickLinkForGovtCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(manageAddOnsQuickLink), "Manage add ons quicklink displayed");
			Reporter.log("Manage add ons quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Manage add ons quicklink displayed");
		}
		return this;
	}

	/**
	 * Verify change my plan quicklink for non master
	 */
	public NewHomePage verifyChangeMyPlanQuickLinkForGovtCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(changeMyPlanQuickLink), "Change my plan quicklink displayed");
			Reporter.log("Change my plan quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Change my plan displayed");
		}
		return this;
	}

	/**
	 * Verify check order status quickLink
	 */
	public NewHomePage verifyCheckOrderStatusQuickLinkForGovtCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(orderStatusQuickLink), "Check order status quicklink not displayed");
			Reporter.log("Check order status quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Check order status quicklink not displayed");
		}
		return this;
	}

	/**
	 * Verify rebate status quickLink
	 */
	public NewHomePage verifyCheckRebateStatusForGovtCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(rebateStatusQuickLink), "Check rebate status quicklink not displayed");
			Reporter.log("Check rebate status quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Check rebate status quicklink not displayed");
		}
		return this;
	}

	/**
	 * Verify check tradeIn status quickLink
	 */
	public NewHomePage verifyCheckTradeInStausQuickLinkForGovtCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(tradeInStatusQuickLink),
					"Check trade in status quicklink not displayed");
			Reporter.log("Check trade in status quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Check trade in status quicklink not displayed");
		}
		return this;
	}

	/**
	 * Verify Amount due
	 */
	public NewHomePage verifyAmountDueNotDisplayed() {
		try {
			Assert.fail("Amount due displayed");
			Assert.assertFalse(isElementDisplayed(amountDue), "Amount due displayed");
			Reporter.log("Amount due not displayed");
		} catch (AssertionError e) {
			Reporter.log("Amount due displayed");
			Assert.fail("Amount due displayed");
		}
		return this;
	}

	/**
	 * Verify Due date
	 */
	public NewHomePage verifyDueDateNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(dueDate), "Due date displayed");
			Reporter.log("Due date not displayed");
		} catch (AssertionError e) {
			Reporter.log("Due date displayed");
			Assert.fail("Due date displayed");
		}
		return this;
	}

	/**
	 * Verify suspended account restore message
	 */
	public NewHomePage verifyAccountRestoreMessage() {
		try {
			Assert.assertTrue(isElementDisplayed(accountRestoreMessage), "Account restore message not displayed");
			Reporter.log("Account restore message displayed");
		} catch (AssertionError e) {
			Reporter.log("Account restore message not displayed");
			Assert.fail("Account restore message not displayed");
		}
		return this;
	}

	/**
	 * Verify purchase accessories quickLink
	 */
	public NewHomePage verifyPurchaseAccessoriesQuickLink() {
		try {
			Assert.assertTrue(isElementDisplayed(purchaseAccesoriesQuickLink),
					"Purchase accessories quickLink not displayed");
			Reporter.log("Purchase accessories quickLink displayed");
		} catch (AssertionError e) {
			Assert.fail("Purchase accessories quickLink not displayed");
		}
		return this;
	}

	/**
	 * Click purchase accessories quickLink
	 */
	public NewHomePage clickPurchaseAccessoriesQuickLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(purchaseAccesoriesQuickLink));
			clickElement(purchaseAccesoriesQuickLink);
		} catch (AssertionError e) {
			Assert.fail("Click purchase accessories  quickLink failed");
		}
		return this;
	}

	/**
	 * Verify message in new bill landing page
	 */
	public NewHomePage verifyMessageInNewBillLandingPage() {
		try {
			Assert.assertTrue(isElementDisplayed(messageInNewBillLandingPage),
					"Message in new bill landing page not displayed");
			Reporter.log("Message in new bill landing page displayed");
		} catch (AssertionError e) {
			Assert.fail("Message in new bill landing page not displayed");
		}
		return this;
	}

	/**
	 * Click rebate status quickLink
	 */
	public NewHomePage clickCheckRebateStatusForGovtCustomer() {
		try {
			clickElement(rebateStatusQuickLink);
		} catch (AssertionError e) {
			Assert.fail("Click on rebate status quickLink failed");
		}
		return this;
	}

	/**
	 * Click check tradeIn status quickLink
	 */
	public NewHomePage clickCheckTradeInStausQuickLinkForGovtCustomer() {
		try {
			clickElement(tradeInStatusQuickLink);
		} catch (AssertionError e) {
			Assert.fail("click on trade in status quicklink faied");
		}
		return this;
	}

	/**
	 * Verify restricted message content
	 */
	public NewHomePage verifyRestrictedMessageContent() {
		try {
			Assert.assertTrue(restrictedMessageContent.getText().contains("Financially Responsible Party"));
			Reporter.log("Restricted message content displayed");
		} catch (AssertionError e) {
			Assert.fail("Restricted message content not displayed");
		}
		return this;

	}

	/**
	 * Verify set up call popup window.
	 */
	public NewHomePage verifySetUpCallPopUpForNewAngular() {
		try {
			if (!isElementDisplayed(makeAChangeBtn)) {
				if (getDriver() instanceof AppiumDriver) {
					checkPageIsReady();
					waitforSpinnerinProfilePage();
					setUpCallPopUpHeaderTextForIOS.isDisplayed();
					Reporter.log("Set up call popup window displayed");
				} else {
					checkPageIsReady();
					waitforSpinnerinProfilePage();
					setUpCallPopUpHeaderText.isDisplayed();
					checkPageIsReady();
					scheduleACallBtn.click();
					Reporter.log("Click on schedule a call button is success");
				}
			} else {
				makeAChangeBtn.click();
			}
		} catch (Exception e) {
			Assert.fail("Set up call popup window not displayed");
		}
		return this;
	}

	/**
	 * click on schedule a call button
	 */
	public NewHomePage clickScheduleACallBtn() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			scheduleACallBtn.click();
			Reporter.log("Clicked on schedule a call button");
		} catch (Exception e) {
			Reporter.log("Click on schedule a call button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * click on confirm button
	 */
	public NewHomePage clickConfirmBtn() {
		try {
			moveToElement(confirmBtn);
			confirmBtn.click();
			Reporter.log("Clicked on confirm button");
		} catch (Exception e) {
			Reporter.log("Click on confirm button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * click on schedule a call button
	 */
	public NewHomePage selectScheduledTime() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			for (WebElement webElement : timeSlots) {
				clickElementWithJavaScript(webElement);
				Reporter.log("Clicked on time slot");
				break;
			}

		} catch (Exception e) {
			Reporter.log("Click on time slot button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify confirmation window.
	 */
	public NewHomePage verfiyConfirmationWindow() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			doneBtn.isDisplayed();
			Reporter.log("Confirmation window displayed");
		} catch (Exception e) {
			Assert.fail("Confirmation window not displayed");
		}
		return this;
	}

	/**
	 * Click confirmation window.
	 */
	public NewHomePage clickConfirmationWindow() {

		try {
			doneBtn.click();
			Reporter.log("Clicked on confirmation window");
		} catch (Exception e) {
			Assert.fail("Click on confirmation window failed");
		}
		return this;
	}

	/**
	 * Click cancel call back button.
	 */
	public NewHomePage clickCancelCallBack() {
		try {
			cancelCallBack.click();
			Reporter.log("Clicked on cancel call back button");
		} catch (Exception e) {
			Assert.fail("Click on  cancel call back button failed ");
		}
		return this;
	}

	/**
	 * Verify cancel call back button.
	 */
	public NewHomePage verfiyCancelCallBack() {

		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.circle")));
			cancelCallBack.isDisplayed();
			Reporter.log("Cancel call back cta displayed");
		} catch (Exception e) {
			Assert.fail("Cancel call back cta not displayed");
		}
		return this;
	}

	/**
	 * Click on browser back button.
	 */

	public NewHomePage clickOnBrowserBackButton() {
		try {
			getDriver().navigate().back();
			Reporter.log("Clicked on Contact Us Browser back button");

		} catch (Exception e) {
			Assert.fail("Failed to click on Browser back button");
		}
		return this;
	}

	/**
	 * Verify call us icon
	 */
	public NewHomePage verifyCallUsButtonNotDispalyed() {
		try {
			Assert.assertFalse(isElementDisplayed(phoneCallIcon));
			Reporter.log("Call us button not displayed");
		} catch (Exception e) {
			Assert.fail("Call us button displayed");
		}
		return this;
	}

	/**
	 * Verify message us icon
	 */
	public NewHomePage verifyMessageUSButtonNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(messageUsIcon));
			Reporter.log("Message us button not displayed");
		} catch (Exception e) {
			Assert.fail("Message us button displayed");
		}
		return this;
	}

	/**
	 * Click error modal popup
	 */
	public NewHomePage clickErrorModalPopUp() {
		try {
			errorModalPopUp.click();
		} catch (Exception e) {
			Assert.fail("Click on error modal popup failed");
		}
		return this;
	}

	/**
	 * click on call team button
	 */
	public NewHomePage clickCallTeamBtn() {
		try {
			callTeamBtn.click();
			Reporter.log("Clicked on call team button");
		} catch (Exception e) {
			Reporter.log("Click on call team button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify call team cta.
	 */
	public NewHomePage verifyCallTeamBtn() {
		try {
			callTeamBtn.isDisplayed();
			Reporter.log("Call team button displayed");
		} catch (Exception e) {
			Assert.fail("Call team button not displayed");
		}
		return this;
	}

	/**
	 * Verify make a change button.
	 */
	public NewHomePage verifyMakeAChangeBtn() {
		try {
			Assert.assertTrue(isElementDisplayed(makeAChangeBtn));
			Reporter.log("Make a change button displayed");
		} catch (AssertionError e) {
			Assert.fail("Make a change button not displayed");
		}
		return this;
	}

	/**
	 * click Make a Change button
	 */
	public NewHomePage clickMakeAChangeButton() {
		if (getDriver() instanceof AppiumDriver) {
			clickElement(makeAChangeScheduleBtn);
		} else {
			clickElement(makeAChangeBtn);
		}
		return this;
	}

	/**
	 * Verify star image for MBB customer
	 */
	public NewHomePage verifyStarImage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(starImage));
			Assert.assertTrue(isElementDisplayed(starImage));
			Reporter.log("Star image displayed");
		} catch (AssertionError e) {
			Assert.fail("Star image not displayed");
		}
		return this;
	}

	/**
	 * Verify mobile internet account management header
	 */
	public NewHomePage verifyAccountManagementHeader() {
		try {
			Assert.assertTrue(isElementDisplayed(accountManagementHeader));
			Reporter.log("Mobile internet account management header displayed");
		} catch (AssertionError e) {
			Assert.fail("Mobile internet account management header not displayed");
		}
		return this;
	}

	/**
	 * Verify mobile internet management link
	 */
	public NewHomePage verifyMobileInternetManagementLink() {
		try {
			Assert.assertTrue(isElementDisplayed(mobileInternetManagementSite));
			Reporter.log("Mobile internet management link displayed");
		} catch (AssertionError e) {
			Assert.fail("Mobile internet management link not displayed");
		}
		return this;
	}

	/**
	 * click mobile internet management link
	 */
	public NewHomePage clickMobileInternetManagementLink() {

		try {
			mobileInternetManagementSite.click();
			Reporter.log("Click on mobile internet management site is success");
		} catch (Exception e) {
			Reporter.log("Click on mobile internet management site failed");
			Assert.fail("Click on mobile internet management site failed");
		}
		return this;
	}

	/**
	 * Verify refill unav menu
	 */
	public NewHomePage verifyRefillUnavMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(refillUnavMenuForMobile));
				Assert.assertTrue(isElementDisplayed(refillUnavMenuForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(refillUnavMenu));
			}
			Reporter.log("Refill unav menu displayed");
		} catch (AssertionError e) {
			Assert.fail("Refill unav menu not displayed");
		}
		return this;
	}

	/**
	 * Verify phone unav menu
	 */
	public NewHomePage verifyPhoneUnavMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(phoneUnavMenuForMobile));
				Assert.assertTrue(isElementDisplayed(phoneUnavMenuForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(phoneUnavMenu));
			}
			Reporter.log("Phone unav menu displayed");
		} catch (AssertionError e) {
			Assert.fail("Phone unav menu not displayed");
		}
		return this;
	}

	/**
	 * Verify account unav menu
	 */
	public NewHomePage verifyAccountUnavMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(accountUnavMenuForMobile));
				Assert.assertTrue(isElementDisplayed(accountUnavMenuForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(accountUnavMenu));
			}
			Reporter.log("Account unav menu displayed");
		} catch (AssertionError e) {
			Assert.fail("Account unav menu not displayed");
		}
		return this;
	}

	/**
	 * Click refill unav menu
	 */
	public NewHomePage clickRefillUnavMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(refillUnavMenuForMobile));
				refillUnavMenuForMobile.click();
			} else {
				refillUnavMenu.click();
			}
		} catch (AssertionError e) {
			Assert.fail("Click on refill unav menu failed");
		}
		return this;
	}

	/**
	 * Click account unav menu
	 */
	public NewHomePage clickAccountUnavMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(accountUnavMenuForMobile));
				accountUnavMenuForMobile.click();
			} else {
				accountUnavMenu.click();
			}
		} catch (AssertionError e) {
			Assert.fail("Click on account unav menu failed");
		}
		return this;
	}

	/**
	 * Click phone unav menu
	 */
	public NewHomePage clickPhoneUnavMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(phoneUnavMenuForMobile));
				phoneUnavMenuForMobile.click();
			} else {
				phoneUnavMenu.click();
			}
		} catch (AssertionError e) {
			Assert.fail("Click on phone unav menu failed");
		}
		return this;
	}

	/**
	 * Verify check order status quickLink
	 */
	public NewHomePage verifyCheckOrderStatusQuickLinkForPrePaidCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(orderStatusQuickLink), "Check order status quicklink not displayed");
			Reporter.log("Check order status quicklink displayed");
		} catch (AssertionError e) {
			Assert.fail("Check order status quicklink not displayed");
		}
		return this;
	}

	/**
	 * Click check order status quickLink
	 */
	public NewHomePage clickCheckOrderStatusQuickLinkForPrePaidCustomer() {
		try {
			orderStatusQuickLink.click();
		} catch (AssertionError e) {
			Assert.fail("Click on  order status quicklink failed");
			Reporter.log("Click on  order status quicklink failed");
		}
		return this;
	}

	/**
	 * Verify rebate status quickLink
	 */
	public NewHomePage verifyCheckRebateStatusForPrepaidCustomer() {
		try {
			Assert.assertTrue(isElementDisplayed(rebateStatusQuickLink), "Check rebate status quicklink not displayed");
			Reporter.log("Check rebate status quicklink displayed");
		} catch (AssertionError e) {
			Reporter.log("Check rebate status quicklink not displayed");
			Assert.fail("Check rebate status quicklink not displayed");
		}
		return this;
	}

	/**
	 * Click rebate status quickLink
	 */
	public NewHomePage clickCheckRebateStatusForPreapidCustomer() {
		try {
			clickElement(rebateStatusQuickLink);
		} catch (AssertionError e) {
			Reporter.log("Click on rebate status quickLink failed");
			Assert.fail("Click on rebate status quickLink failed");
		}
		return this;
	}

	/**
	 * Verify voice mail quicklink
	 */
	public NewHomePage verifyVoiceMailQuickLink() {
		try {
			Assert.assertTrue(isElementDisplayed(voicemailQuickLink), "Voicemail quicklink not displayed");
			Reporter.log("Voicemail quicklink displayed");
		} catch (AssertionError e) {
			Reporter.log("Voicemail quicklink not displayed");
			Assert.fail("Voicemail quicklink not displayed");
		}
		return this;
	}

	/**
	 * Click voice mail quicklink
	 */
	public NewHomePage clickVoiceMailQuickLink() {
		try {
			clickElement(voicemailQuickLink);
		} catch (AssertionError e) {
			Reporter.log("Click on voicemail quicklink failed");
			Assert.fail("Click on voicemail quicklink failed");
		}
		return this;
	}

	/**
	 * Verify device blade div in my line section
	 */
	public NewHomePage verifyDeviceBladeDivInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(deviceBladeDivInMyLineSectionForMobile),
						"Device blade div in my line section not displayed");
			} else {
				Assert.assertTrue(isElementDisplayed(deviceBladeDivInMyLineSection),
						"Device blade div in my line section not displayed");
			}
			Reporter.log("Device blade div in my line section displayed");
		} catch (AssertionError e) {
			Reporter.log("Device blade div in my line section not displayed");
			Assert.fail("Device blade div in my line section not displayed");
		}
		return this;
	}

	/**
	 * Verify plan blade div in my line section
	 */
	public NewHomePage verifyPlanBladeDivInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(planBladeDivInMyLineSectionForMobile),
						"Plan blade div in my line section not displayed");
			} else {
				Assert.assertTrue(isElementDisplayed(planBladeDivInMyLineSection),
						"Plan blade div in my line section not displayed");
			}
			Reporter.log("Plan blade div in my line section displayed");
		} catch (AssertionError e) {
			Reporter.log("Plan blade div in my line section not displayed");
			Assert.fail("Plan blade div in my line section not displayed");
		}
		return this;
	}

	/**
	 * Verify usage blade div in my line section
	 */
	public NewHomePage verifyUsageBladeDivInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(usageBladeDivInMyLineSectionForMobile),
						"Usage div blade in my line section not displayed");
			} else {
				Assert.assertTrue(isElementDisplayed(usageBladeDivInMyLineSection),
						"Usage div blade in my line section not displayed");
			}
			Reporter.log("Usage blade div in my line section displayed");
		} catch (AssertionError e) {
			Reporter.log("Usage blade div in my line section not displayed");
			Assert.fail("Usage div blade in my line section not displayed");
		}
		return this;
	}

	/**
	 * Click device blade div
	 */
	public NewHomePage clickDeviceBladeDivInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickElement(deviceBladeDivInMyLineSectionForMobile);
			} else {
				clickElement(deviceBladeDivInMyLineSection);
			}
		} catch (AssertionError e) {
			Reporter.log("Click on device blade div failed");
			Assert.fail("Click on device blade div failed");
		}
		return this;
	}

	/**
	 * Click plan blade div
	 */
	public NewHomePage clickPlanBladeDivInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickElement(planBladeDivInMyLineSectionForMobile);
			} else {
				clickElement(planBladeDivInMyLineSection);
			}
		} catch (AssertionError e) {
			Reporter.log("Click on plan blade div failed");
			Assert.fail("Click on plan blade div failed");
		}
		return this;
	}

	/**
	 * Verify Missdn number.
	 */
	public NewHomePage verifyLoggedMissdnNumber(String phoneNumber) {
		try {
			String loggedInPhoneNumber = phoneNumber.replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			String s = linkedMobileNumber.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			Assert.assertEquals(s, loggedInPhoneNumber,
					"Linked phone number in my line section not matched with logged missdn");
			Reporter.log("Linked phone number in my line section matched with logged missdn");
		} catch (AssertionError e) {
			Reporter.log("Linked phone number in my line section not matched with logged missdn");
			Assert.fail("Linked phone number in my line section not matched with logged missdn");
		}
		return this;
	}

	/**
	 * Verify shop unav menu
	 */
	public NewHomePage verifyShopUnavMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(shopMenuForMobile));
				Assert.assertTrue(isElementDisplayed(shopMenuForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(shopMenu));
			}
			Reporter.log("Shop unav menu displayed");
		} catch (AssertionError e) {
			Assert.fail("Shop unav menu not displayed");
		}
		return this;
	}

	/**
	 * Verify account balance div
	 */
	public NewHomePage verifyAccountBalanceDiv() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(accountBalanceDivForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(accountBalanceDiv));
			}
			Reporter.log("Account balance div displayed");
		} catch (AssertionError e) {
			Reporter.log("Account balance div not displayed");
			Assert.fail("Account balance div not displayed");
		}
		return this;
	}

	/**
	 * Verify refill account cta
	 */
	public NewHomePage verifyRefillAccountCTA() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				Assert.assertTrue(isElementDisplayed(refillAccountCTAForMobile));
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenuCloseBtn));
				hamburgerMenuCloseBtn.click();
			} else {
				Assert.assertTrue(isElementDisplayed(refillAccountCTA));
			}
			Reporter.log("Refill account CTA displayed");
		} catch (AssertionError e) {
			Reporter.log("Refill account CTA not displayed");
			Assert.fail("Refill account CTA not displayed");
		}
		return this;
	}

	/**
	 * Verify account status div
	 */
	public NewHomePage verifyAccountStatusDiv() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(accountStatusDivForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(accountStatusDiv));
			}
			Reporter.log("Account status div displayed");
		} catch (AssertionError e) {
			Reporter.log("Account status div not displayed");
			Assert.fail("Account status div not displayed");
		}
		return this;
	}

	/**
	 * Verify next payment div
	 */
	public NewHomePage verifyNextPaymentAmountDiv() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(nextPaymentAmountDivForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(nextPaymentAmountDiv));
			}
			Reporter.log("Next payment div displayed");
		} catch (AssertionError e) {
			Reporter.log("Next payment div not displayed");
			Assert.fail("Next payment div not displayed");
		}
		return this;
	}

	/**
	 * Verify next charge date div
	 */
	public NewHomePage verifyNextChargeDateDiv() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(isElementDisplayed(nextChargeDivForMobile));
			} else {
				Assert.assertTrue(isElementDisplayed(nextChargeDiv));
			}
			Reporter.log("Charge date div displayed");
		} catch (AssertionError e) {
			Reporter.log("Charge date div not displayed");
			Assert.fail("Charge date div not displayed");
		}
		return this;
	}

	/**
	 * Verify view account text in my line section
	 */
	public NewHomePage verifyViewAccountTextInMyLineSection() {
		try {
			Assert.assertTrue(isElementDisplayed(viewAccountTextInMyLineSection));
			Reporter.log("View account text in my line section displayed");
		} catch (AssertionError e) {
			Reporter.log("View account text in my line section not displayed");
			Assert.fail("View account text in my line section not displayed");
		}
		return this;
	}

	/***
	 * verifying search results for TMO home page
	 * 
	 * @param expectedValue
	 * @return
	 */
	public NewHomePage verifySearchResultsForTMOHomePage(String expectedValue) {
		checkPageIsReady();
		waitforSpinnerinPrepaidPage();
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			clickOnAllowPopUp();
			waitFor(ExpectedConditions.visibilityOf(tmoSearchResults.get(0)));
			if (isElementDisplayed(tmoSearchResults.get(0))) {
				for (WebElement ele : tmoSearchResults) {
					if (ele.getAttribute("src").trim().toLowerCase().contains(expectedValue.toLowerCase())) {
						actListOfValuesThatMatchedTheSearchValue++;
					}
				}
				Assert.assertEquals(tmoSearchResults.size(), actListOfValuesThatMatchedTheSearchValue);
				Reporter.log("Search functionality fetching results as per search field");
			} else {
				Reporter.log("No results found");
			}
		} catch (Exception e) {
			Assert.fail("Search functionality is not fetching results as per search field");
		}
		return this;
	}

	/**
	 * To verify search field-Insert data and search
	 * 
	 * @param dataToEnter
	 */
	public NewHomePage enterDataInSearchField(String dataToEnter) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(searchMenuForMobile));
				searchMenuForMobile.click();
				waitFor(ExpectedConditions.visibilityOf(searchIconTextBoxForMobile));
				sendTextData(searchIconTextBoxForMobile, dataToEnter);
				waitFor(ExpectedConditions.visibilityOf(searchIconForMobile));
				searchIconForMobile.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(searchIcon));
				searchIcon.click();
				waitFor(ExpectedConditions.visibilityOf(searchIconTextBox));
				sendTextData(searchIconTextBox, dataToEnter);
				Reporter.log("Entered data in search box");
				waitFor(ExpectedConditions.visibilityOf(tmoSearchIcon));
				tmoSearchIcon.click();
			}
			Reporter.log("Clicked on search button");

		} catch (Exception e) {
			Assert.fail("Search component not found");
		}
		return this;
	}

	/**
	 * Verify data maximiser div
	 */
	public NewHomePage verifyDataMaximiserDiv() {
		try {
			Assert.assertTrue(isElementDisplayed(dataMaximiserDiv));
			Reporter.log("Data maximiser div displayed");
		} catch (AssertionError e) {
			Reporter.log("Data maximiser div not displayed");
			Assert.fail("Data maximiser div not displayed");
		}
		return this;
	}

	/**
	 * Verify data maximiser toggle off state
	 */
	public NewHomePage verifyDataMaximiserToggleOffState() {
		try {
			if (isElementDisplayed(dataMaximiserToggleOffState)) {
				Reporter.log("Data maximiser toggle is in off state");
			} else {
				dataMaximiserToggle.click();
				Assert.assertTrue(isElementDisplayed(dataMaximiserToggleOffState));
				Reporter.log("Data maximiser toggle is in off state");
			}
		} catch (AssertionError e) {
			Reporter.log("Data maximiser toggle is not in off state");
			Assert.fail("Data maximiser toggle is not in off state");
		}
		return this;
	}

	/**
	 * Verify data maximiser toggle on state
	 */
	public NewHomePage verifyDataMaximiserToggleOnState() {
		try {
			if (isElementDisplayed(dataMaximiserToggleOnState)) {
				Reporter.log("Data maximiser toggle is in on state");
			} else {
				dataMaximiserToggle.click();
				Assert.assertTrue(isElementDisplayed(dataMaximiserToggleOnState));
				Reporter.log("Data maximiser toggle is in on state");
			}
		} catch (AssertionError e) {
			Reporter.log("Data maximiser toggle is not in on state");
			Assert.fail("Data maximiser toggle is not in on state");
		}
		return this;
	}

	/**
	 * Click legacy page mytmobile logo
	 */
	public NewHomePage clickLeagacyPageMyTMobileLogo() {
		try {
			legacyPageMyTmoLogo.click();
		} catch (Exception e) {
			Reporter.log("Click on legacy page mytmobile logo failed");
			Assert.fail("Click on legacy page mytmobile logo failed");
		}
		return this;
	}

	/**
	 * Click view account text in my line section
	 */
	public NewHomePage clickViewAccountTextInMyLineSection() {
		try {
			viewAccountTextInMyLineSection.click();
			Reporter.log("View account text in my line section displayed");
		} catch (AssertionError e) {
			Reporter.log("Click on view account text in my line section failed");
			Assert.fail("Click on view account text in my line section failed");
		}
		return this;
	}

	/**
	 * Verify language settings page navigation for master and non master gov
	 * customers
	 */
	public NewHomePage verifyLanguageSettingsPageNavigation() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			if (!getDriver().getCurrentUrl().contains("language_settings")
					&& (getDriver().getCurrentUrl().contains("profile")
							|| getDriver().getCurrentUrl().contains("home"))) {
				Reporter.log("System successfully navigated to profile/home page");
			} else {
				Reporter.log("Language settings page navigation failed");
				Assert.fail("Language settings page navigation failed");
			}

		} catch (Exception e) {
			Reporter.log("Language settings page navigation failed");
			Assert.fail("Language settings page navigation failed");
		}
		return this;
	}

	/**
	 * Click complete account setup link dropdown
	 */
	public NewHomePage clickCompleteAccountSetUpLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(completeAccountSetUpLink));
			completeAccountSetUpLink.click();
		} catch (Exception e) {
			Assert.fail("Click on complete account setup link failed");
		}
		return this;
	}

	/**
	 * Verify welcome prepaid text
	 */
	public NewHomePage verifyWelcomePrePaidCustomersText() {
		try {
			Assert.assertTrue(prepaidWelcomeText.getText().contains(prepaidText), "Prepaid welcome text not displayed");
			Reporter.log("Prepaid welcome text displayed");
		} catch (AssertionError e) {
			Assert.fail("Prepaid welcome text not displayed");
		}
		return this;
	}

	/**
	 * Verify contact customer care text
	 */
	public NewHomePage verifyContactCustomerCareText() {
		try {
			Assert.assertTrue(customerCareHeader.getText().contains(customerCareText),
					"Contact customer care text not displayed");
			Reporter.log("Contact customer care text displayed");
		} catch (AssertionError e) {
			Assert.fail("Contact customer care text not displayed");
		}
		return this;
	}

	/**
	 * get linked account missdn
	 */
	public String getLinkedAccountMissdn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(linkedAccountMissdn));
			Assert.assertTrue(linkedAccountMissdn.isDisplayed());
			String splittedMobileNumber[] = linkedAccountMissdn.getText().split(",");
			String linkedAccountMissdn = splittedMobileNumber[1].replace("(", "").replace(")", "").replace("-", "")
					.replace(" ", "").trim();
			Reporter.log("Fetching other account missdn is success");
			return linkedAccountMissdn;
		} catch (AssertionError e) {
			Assert.fail("Fetching other account missdn is failed");
			return null;
		}
	}

	/**
	 * Click legal terms link
	 * 
	 * @return
	 */
	public NewHomePage clickLegalTermsLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(featuredPhonesSection);
				legalTermsLink.click();
				Reporter.log("Clicked onLegal Terms Link");
			} else {
				legalTermsLink.click();
				Reporter.log("Clicked onLegal Terms Link");
			}

		} catch (Exception e) {
			Assert.fail("Legal Terms Link is not Dispalyed to click");
		}
		return this;

	}

	/**
	 * Verify legal terms modal window
	 * 
	 * @return
	 */
	public NewHomePage verifyLegalTermsModalWindow() {
		try {
			legalTermsModalWindow.isDisplayed();
			Reporter.log("Legal terms modal window displayed");
		} catch (Exception e) {
			Assert.fail("Legal terms modal window not displayed");
		}
		return this;
	}

	/**
	 * Verify legal terms modal window not displayed
	 * 
	 * @return
	 */
	public NewHomePage verifyLegalTermsModalWindowNotDisplayed() {
		try {
			Assert.assertFalse(legalTermsModalWindow.isDisplayed());
			Reporter.log("Modal window not displayed");
		} catch (AssertionError e) {
			Assert.fail("Modal window displayed");
		}
		return this;
	}

	/**
	 * Verify modal window title
	 * 
	 * @return
	 */
	public NewHomePage verifyModalWindowTitle() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalWindowTitle));
			Assert.assertFalse(modalWindowTitle.isDisplayed());
			Reporter.log("Modal window title displayed");
		} catch (AssertionError e) {
			Assert.fail("Modal window title not displayed");
		}
		return this;
	}

	/**
	 * Verify modal window disclaimer text
	 * 
	 * @return
	 */
	public NewHomePage verifyModalWindowDisclaimersText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalWindowDisclaimerText));
			Assert.assertFalse(modalWindowDisclaimerText.isDisplayed());
			Reporter.log("Modal window disclaimer text displayed");
		} catch (AssertionError e) {
			Assert.fail("Modal window disclaimer text not displayed");
		}
		return this;
	}

	/**
	 * Verify modal window close cta
	 * 
	 * @return
	 */
	public NewHomePage verifyModalWindowdCloseCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalWindowCloseCTA));
			Assert.assertFalse(modalWindowCloseCTA.isDisplayed());
			Reporter.log("Modal window close cta displayed");
		} catch (AssertionError e) {
			Assert.fail("Modal window close cta notdisplayed");
		}
		return this;
	}

	/**
	 * Click modal window close cta
	 * 
	 * @return
	 */
	public NewHomePage clickModalWindowdCloseCTA() {
		try {
			modalWindowCloseCTA.click();
			Reporter.log("Click on modal window close cta is success");
		} catch (AssertionError e) {
			Assert.fail("Click on modal window close cta failed");
		}
		return this;
	}

	/**
	 * Verify modal window close cta
	 * 
	 * @return
	 */
	public NewHomePage verifyLegalTermsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalWindowCloseCTA));
			Assert.assertFalse(modalWindowCloseCTA.isDisplayed());
			Reporter.log("Modal window close cta displayed");
		} catch (AssertionError e) {
			Assert.fail("Modal window close cta notdisplayed");
		}
		return this;
	}

	/**
	 * Verify configured web alerts
	 */
	public NewHomePage verifyWebAlerts() {
		try {
			Assert.assertTrue(isElementDisplayed(webAlert));
			Reporter.log("Alert is displayed");
		} catch (AssertionError e) {
			Assert.fail("Alert not displayed");
		}
		return this;
	}

	/**
	 * Click new unavlogout menu
	 */
	public NewHomePage clickNewUnavLogOutMenu() {
		try {
			waitFor(ExpectedConditions.visibilityOf(myAccountDropDown));
			myAccountDropDown.click();
			waitFor(ExpectedConditions.visibilityOf(logOutMenu));
			clickElement(logOutMenu);
		} catch (Exception e) {
			Reporter.log("Click on logout menu failed");
			Assert.fail("Click on logout menu failed");
		}
		return this;
	}

	/**
	 * Verify refill button in my account section
	 */
	public NewHomePage verifyRefillButtonInMyAccountSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(refillUnavMenuForMobile));
				Assert.assertTrue(isElementDisplayed(refillUnavMenuForMobile));
			} else {

				Assert.assertTrue(isElementDisplayed(refillInMyAccountSection));
			}
			Reporter.log("Refill button in my account section displayed");
		} catch (AssertionError e) {
			Reporter.log("Refill button in my account section not displayed");
			Assert.fail("Refill button in my account section not displayed");
		}
		return this;
	}

	/**
	 * Click refill button in my account section
	 */
	public NewHomePage clickRefillButtonInMyLineSection() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(refillUnavMenuForMobile));
				refillUnavMenuForMobile.click();
			} else {
				refillInMyAccountSection.click();
			}
		} catch (AssertionError e) {
			Reporter.log("Click on refill button in my account section failed");
			Assert.fail("Click on refill button in my account section failed");
		}
		return this;
	}

	/***
	 * Verify switch account header menu
	 * 
	 * @return
	 */

	public NewHomePage verifySwitchAccountHeaderMenu() {
		try {
			waitFor(ExpectedConditions.visibilityOf(myAccountDropDown));
			myAccountDropDown.click();
			Assert.assertFalse(isElementDisplayed(switchAccount), "Switch account header menu displayed");
			Reporter.log("Switch account header menu not displayed");
		} catch (AssertionError e) {
			Assert.fail("Switch account header menu displayed ");
		}
		return this;
	}
	
	/***
	 * Verify qucikLink navigations from Home Page
	 * 
	 * @return
	 */

	public NewHomePage verifyQuickLinksNavigationsFromHomePage() {
		String quickLinkNavigateURL = "";
		String quickLinkTitle = "";
		List<WebElement> quickLink = null;
		int availableQuickLinksCount = 0;
		String env = System.getProperty("environment").trim();
		try {
			availableQuickLinksCount = availableQuickLinks.size();
			if (availableQuickLinksCount > 0) {
				for (int i = 0; i < availableQuickLinksCount; i++) {
					quickLink = getDriver().findElements(By.cssSelector("img[src*='quick']~a"));
					quickLinkNavigateURL = quickLink.get(i).getAttribute("data-href");
					quickLinkTitle = quickLink.get(i).getText();
					clickElement(quickLink.get(i));
					checkPageIsReady();
					if (getDriver().getWindowHandles().size() == 2) {
						switchToWindow();
						Assert.assertTrue(getDriver().getCurrentUrl().contains(quickLinkNavigateURL),
								"Page loading is failed after clicking on quick link : " + quickLinkTitle);
						Reporter.log("Quick link navigation is working fine " + quickLinkTitle);
						switchToHomeWindow();
						verifyHomePage();
					} else {
						Assert.assertTrue(getDriver().getCurrentUrl().contains(quickLinkNavigateURL),
								"Page load is failed after clicking on quick link : " + quickLinkTitle);
						Reporter.log("Quick link navigation is working fine " + quickLinkTitle);

						getDriver().get(env + "/home");
						verifyHomePage();
					}
					availableQuickLinksCount = quickLink.size();
				}
			} else {
				Reporter.log("Quick links are not available for this User");
			}
		} catch (AssertionError e) {
			Assert.fail("Failed to verify qucikLink navigations from Home Page");
		}
		return null;
	}
	
	/**
	 * Verify see more cta
	 */
	public NewHomePage verifySeeMoreCTA() {
		try {
			Assert.assertTrue(isElementDisplayed(seeMoreCTA));
			Reporter.log("See more cta displayed");
		} catch (AssertionError e) {
			Assert.fail("See more cta not displayed");
		}
		return this;
	}
	
	/**
	 * Verify change my plan quicklink for large business customers
	 */
	public NewHomePage verifyChangeMyPlanQuickLinkForLargeBusinessCustomers() {
		try {
			Assert.assertFalse(isElementDisplayed(changeMyPlanQuickLink), "Change my plan quicklink displayed");
			Reporter.log("Change my plan quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Change my plan quicklink displayed");
		}
		return this;
	}

	/**
	 * Verify manage addOns quickLink for large business customers
	 */
	public NewHomePage verifyManageAddOnsQuickLinkForForLargeBusinessCustomers() {
		try {
			Assert.assertFalse(isElementDisplayed(manageAddOnsQuickLink), "Manage add ons quicklink displayed");
			Reporter.log("Manage add ons quicklink not displayed");
		} catch (AssertionError e) {
			Assert.fail("Manage add ons quicklink displayed");
		}
		return this;
	}

}
