package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class BillingApiV3 extends ApiCommonLib{
	
	/***
	 * Summary: This API is used to get the Sprint Billing Details and Tmobile Billing details  through Billing APIV3
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-interactionId
	 * 	-Authorization
	 * 	-userRole
	 *  -unBilledCycleStartDate
	 *  -easyPayStatus
	 *  -transactionid
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	// FIXME: svasamse - Move the util method to a common class for reusability
	   public static Map<String, String> buildBillingAPIHeader(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("transactionId","123");
		headers.put("ban",apiTestData.getBan());
		headers.put("correlationId","123");
		//Time being it is Hardcode. Since it is mock response
    	headers.put("Authorization","eyJ0eXAiOiJKV0UiLCJhbGciOiJSU0ExXzUiLCJlbmMiOiJBMjU2Q0JDK0hTNTEyIn0.g8QdfCqum8dhG_3LRyNCkhitiWgTzTUkEzIHglUA09xlGPSOJfoVQ2o2snL4sOBcO3NbKbFQZbMdImkHDedD7s_OBAuR7nLUmizNVTXe1G2EQl5oHqr6I6IIzrXkQwwdqffiO-HfQ--tPhyDyvOpYWSEtiWpGuAxn2d5b-TVkF-CdW6vvFcQMOonetlnfFQEv7SnOXAI-les6CUoVIQr9FaeHhuZMdlFkLNMF7AtN4yjqT4LK-3KOUuAXVLj9_KkibcAwlKmX3WLqrM9uEJ2pzWSZyibAjSmT1PWiVx80TTg4crTPwb_5ISIE1fpjMn7Ff6GK1b4FTbBMIxIHpAO5g.X8R243DG2A8b5LuF6uMe0Q.UFmURqVL5f5UpK6OWdSvoRQJq6oq6t3gGE7lDsF2gr8kNHMz3ZlN_uwRj3IMcSNXhHFch_11vrsJpVcWEGUA3w.kMCygS6_haiwQjQz_tL3S9yYb3BeFiJGdomwzutPJKwBGRG4-BK9eQIUxH8QHF2EZdVAzA-ZMfwVURa53cNJoA");
    	//headers.put("Authorization",getAccessToken());
    	headers.put("applicationId","123");
		headers.put("channelId","123");
		headers.put("channelVersion","123");
		headers.put("clientId","123");
		headers.put("transactionBusinessKey","123");
		headers.put("transactionBusinessKeyType","123");
		headers.put("transactionType","123");
		return headers;
	}
	
	
	/***
	 *BillingV3 API’s.
	 * parameters:
     *   - $ref: '#/parameters/TmobileBan'
     *   - $ref: '#/parameters/Legacy AccountNumber'
     *   - $ref: '#/parameters/Legacy Indicator'
	 * @return
	 * @throws Exception 
	 */
    
	public Response getBillListwithLegacyAccountandIndicator(ApiTestData apiTestData, Map<String, String> tokenMap)	throws Exception {
		Map<String, String> headers = buildBillingAPIHeader(apiTestData, tokenMap);
		String resourceURL = "/v1/billing/cycles?ban=" + apiTestData.getBan() + "&legacyAccountNumber="+ apiTestData.getlegacyaccountnumber() + "&legacySystemIndicator="+ apiTestData.getlegacysystemindicator();
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.GET);
		return response;
	}
    
    /***
	 *BillingV3 API’s.
	 * parameters:
     *   - $ref: '#/parameters/TmobileBan'
     *   - $ref: '#/parameters/Legacy AccountNumber'
	 * @return
	 * @throws Exception 
	 */
    
    public Response getBillListwithLegacyAccount(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildBillingAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "v1/billing/cycles?ban="+apiTestData.getBan()+"&legacyAccountNumber="+apiTestData.getlegacyaccountnumber();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
    	return response;
    }
    
    /***
	 *BillingV3 API’s.
	 * parameters:
     *   - $ref: '#/parameters/TmobileBan'
 	 * @return
	 * @throws Exception 
	 */
    
    public Response getBillListwithTmobileAccount(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildBillingAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "v1/billing/cycles?ban="+apiTestData.getBan();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
    	//To do the parameterization based on actual implementation
    	return response;
    }
}  
