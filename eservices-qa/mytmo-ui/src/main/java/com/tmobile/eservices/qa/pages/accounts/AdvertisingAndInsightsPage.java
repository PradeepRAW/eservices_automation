package com.tmobile.eservices.qa.pages.accounts;

import java.util.Arrays;
import java.util.List;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.global.NewContactUSPage;
import io.appium.java_client.AppiumDriver;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class AdvertisingAndInsightsPage extends CommonPage {

	private static final String pageUrl = "privacy_notifications/advertising";

	@FindBy(xpath = "//p[text()='Insights']")
	private WebElement insightsHeader;

	@FindBy(xpath = "//p[text()='Advertising - Network Usage Data']")
	private WebElement networkDataConsentHeader;

	@FindBy(xpath = "//p[text()='Advertising - Network Location Data']")
	private WebElement locationDataConsentHeader;

	@FindBy(xpath = "//p[text()='Advertising & Insights']")
	private WebElement advertisingLink;

	@FindBy(css = "span.slider.round")
	private WebElement toggleStatus;

	@FindBy(id = "Advertising - Network Usage Data")
	private WebElement advertisingNetworkUsageDataLabel;

	@FindBy(id = "Advertising - Network Location Data")
	private WebElement advertisingNetworkLocationDataLabel;

	@FindBy(css = "span.breadcrumb-item.active.ng-star-inserted")
	private WebElement breadCrumbAdvertisingandInsights;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement selectMisisdin;

	@FindBy(css = "div.TMO-PROFILE-BLADE-PAGE .Display6")
	private List<WebElement> listOfPageHeaders;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/label")
	private WebElement insightsToggle;

	@FindBy(xpath = "//p[contains(text(),'Insights')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement insightsToggleStatus;

	@FindBy(css = "div.dialog.animation-out.modal_landscapeht.animation-in")
	private WebElement errorModel;


	/**
	 * @param webDriver
	 */
	public AdvertisingAndInsightsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Advertising and insights page.
	 *
	 * @return the profileAdvertisingInsights class instance.
	 */
	public AdvertisingAndInsightsPage verifyAdvertisingandInsightsPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Privacy and notifications page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Privacy and notifications page not displayed");
		}
		return this;

	}

	/**
	 * Click advertising link.
	 */
	public AdvertisingAndInsightsPage clickAdvertisingLink() {
		waitforSpinner();
		checkPageIsReady();
		try {
			advertisingLink.click();
			Reporter.log("Click on advertising link is success");
		} catch (Exception e) {
			Reporter.log("Click on advertising link failed");
			Assert.fail("Click on advertising link failed");
		}
		return this;
	}

	/**
	 * Verify NetwowrkDataConsent header.
	 */
	public AdvertisingAndInsightsPage verifyNetworkDataConsentHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(networkDataConsentHeader));
			networkDataConsentHeader.isDisplayed();
			Reporter.log("NewtworkDataConsent header displayed");
		} catch (Exception e) {
			Assert.fail("NewtworkDataConsent header not displayed");
		}
		return this;
	}

	/**
	 * Check insights toggle status.
	 */
	public AdvertisingAndInsightsPage checkToggleStatusForNetworkDataConsent() {
		try {
			waitforSpinnerinProfilePage();
			if (isElementDisplayed(toggleStatus)) {
				Reporter.log("Network Data Consent toggle is in enabled state");
			} else {
				Reporter.log("Network Data consent toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Network Data consent toggle is not displayed");
		}
		return this;
	}

	/**
	 * Verify NetwowrkDataConsent header.
	 */
	public AdvertisingAndInsightsPage verifyLocationDataConsentHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(locationDataConsentHeader));
			locationDataConsentHeader.isDisplayed();
			Reporter.log("NewtworkDataConsent header displayed");
		} catch (Exception e) {
			Assert.fail("NewtworkDataConsent header not displayed");
		}
		return this;
	}
	/**
	 * Check Location Data Consent.
	 */
	public AdvertisingAndInsightsPage checkToggleStatusForLocationDataConsent() {
		try {
			waitforSpinnerinProfilePage();
			if (isElementDisplayed(toggleStatus)) {
				Reporter.log("Location Data Consent toggle is in enabled state");
			} else {
				Reporter.log("Location Data Consent toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Location Data Consent toggle is not displayed");
		}
		return this;
	}

	public boolean advertisingNetworkUsageDataTurnedOn() {
		return getToggleState(this.advertisingNetworkUsageDataLabel);
	}

	public boolean toggleAdvertisingNetworkUsageData(boolean value) throws InterruptedException {
		if (advertisingNetworkUsageDataTurnedOn() != value) {
			this.advertisingNetworkUsageDataLabel.click();
			Thread.sleep(2000);
			Assert.assertTrue(getToggleState(advertisingNetworkUsageDataLabel));
		} else {
			this.advertisingNetworkUsageDataLabel.click();
			Thread.sleep(2000);
			Assert.assertFalse(getToggleState(advertisingNetworkUsageDataLabel));

		}

		return value;
	}

	public boolean advertisingNetworkLocationDataTurnedOn() {
		return getToggleState(this.advertisingNetworkLocationDataLabel);
	}

	public boolean toggleAdvertisingLocationConsent(boolean value) throws InterruptedException {
		if (advertisingNetworkLocationDataTurnedOn() != value) {
			this.advertisingNetworkLocationDataLabel.click();
			Thread.sleep(2000);
			Assert.assertTrue(getToggleState(advertisingNetworkLocationDataLabel));
		} else {
			this.advertisingNetworkLocationDataLabel.click();
			Thread.sleep(2000);
			Assert.assertFalse(getToggleState(advertisingNetworkLocationDataLabel));
		}
		return value;
	}

	private boolean getToggleState(WebElement element) {
		String cssClass = StringUtils.defaultString(element.getAttribute("class"));
		String[] cssClasses = cssClass.split("\\s+");
		return Arrays.asList(cssClasses).contains("stateEnabled");
	}
	/**
	 * Check Advertising and Insights color
	 */
	public AdvertisingAndInsightsPage checkcolorForBreadCrumbAdvertisingandInsights() {
		try {
			if (isElementDisplayed(breadCrumbAdvertisingandInsights)) {
				String AdvertisingColor = breadCrumbAdvertisingandInsights.getCssValue("color");
				String hex = Color.fromString(AdvertisingColor).asHex();
				Assert.assertEquals(hex,"#e20074");
				System.out.println(hex);
				Reporter.log("Can see the advertising and insights in pink color");
			} else {
				Reporter.log("Not able to find the element");
			}
		} catch (Exception e) {
			Assert.fail("Advertising and Insights breadcurmb is not in pink color");
		}
		return this;
	}

	/**
	 * Check Advertising and Insights color
	 */
	public AdvertisingAndInsightsPage checkAdvertisingInsightsNotClickableForDeeplink() {
		try {
			if (isElementDisplayed(breadCrumbAdvertisingandInsights)) {
				String name= breadCrumbAdvertisingandInsights.getTagName();
				Assert.assertEquals(name,"span" );
				Reporter.log("Not a clickable link");
			} else {
				Reporter.log("Not able to find the element");
			}
		} catch (Exception e) {
			Assert.fail("It is a clickable link");
		}
		return this;
	}

	/**
	 * select Schedule Date
	 */
	public AdvertisingAndInsightsPage selectMsisdindropdown() {
		try {
			waitforSpinner();
			checkPageIsReady();
			selectElementFromDropDown(selectMisisdin, "Index", "1");
			Reporter.log("selected the msisdin");
		} catch (Exception e) {
			Assert.fail("msisdin dropdown not found");
		}
		return this;

	}
	/**
	 * select Schedule Date
	 */
	public AdvertisingAndInsightsPage VerifycheckForOnlyInsightsHeader() {
		try {
			verifyHeaderByName("Insights");
			verifyLocationDataConsentHeader();
			Reporter.log("Insights header is displayed");
		} catch (Exception e) {
			Assert.fail("Insights header is not displayed");
		}
		return this;

	}


	/**
	 * Verify Header By Name.
	 */
	public AdvertisingAndInsightsPage verifyHeaderByName(String headerName) {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOf(listOfPageHeaders.get(0)));
		try {
			for (WebElement webElement : listOfPageHeaders) {
				if (webElement.getText().equalsIgnoreCase(headerName)) {
					Reporter.log(headerName + " header displayed");
					break;
				} else {
					Assert.fail(headerName + " header is not displayed");
				}
			}
		} catch (Exception e) {
			Assert.fail(headerName + " header is not displayed");
		}
		return this;
	}

	/**
	 * Click insights toggle.
	 */
	public AdvertisingAndInsightsPage clickInsightsToggle() {
		try {
			waitforSpinnerinProfilePage();
			insightsToggle.click();
			Reporter.log("Click on insights toggle is success");
		} catch (Exception e) {
			Assert.fail("Click on insights toggle failed");
		}
		return this;
	}

	/**
	 * Check insights toggle status.
	 */
	public AdvertisingAndInsightsPage checkToggleStatusForInsights() {
		try {
			waitforSpinnerinProfilePage();
			if (isElementDisplayed(insightsToggleStatus)) {
				Reporter.log("Insights toggle is in enabled state");
			} else {
				Reporter.log("Insights toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Insights toggle is not displayed");
		}
		return this;
	}

	/**
	 * Check for error model
	 */
	public AdvertisingAndInsightsPage checkForErrorModel() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(errorModel));
			if (isElementDisplayed(errorModel)) {
				Reporter.log("Insights toggle is in enabled state");
			} else {
				Reporter.log("Insights toggle is in disable state");
			}
		} catch (Exception e) {
			Assert.fail("Insights toggle is not displayed");
		}
		return this;
	}


}
