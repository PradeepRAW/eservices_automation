/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */

public class ManageAddOnsSelectionPage extends CommonPage {

	// Manage Data And AddOns header text
	@FindBy(css = "div[id='upgradeServicesPage'] p[class*='Display3']")
	private WebElement manageDataAndAddOns;

	// Manage Data And AddOns header text
	@FindBy(css = "div.body:nth-child(2)")
	private WebElement pendingChanges;

	// continue & Conflict buttons
	@FindBy(css = "button.PrimaryCTA.full-btn-width")
	private WebElement continueButton;

	// Overlays
	@FindBy(css = "div[class*='dialog modal'] span[class*='Display5']")
	private List<WebElement> removeDataPassOverlay;

	@FindBy(css = "div[class='dialog modal_landscapeht'] button[class*='PrimaryCTA-accent']")
	private List<WebElement> conflictDialogContinue;

	@FindBy(css = "button.PrimaryCTA-accent")
	private List<WebElement> continueRemoval;

	@FindBy(css = "button.SecondaryCTA-accent")
	private WebElement cancelBtn;

	@FindBy(xpath = "(//*[@class='SecondaryCTA-accent blackCTA pull-right'])")
	private WebElement cancelCTAForRemovalOfPHPSOC;

	@FindBy(css = "div[class='dialog modal_landscapeht'] div")
	private List<WebElement> conflictDialog;

	@FindBy(css = "button[class*='blackCTA']")
	private WebElement conflictModalCancelBtn;

	@FindBy(css = "div[class='dialog-container'] div[class*='text-center body']")
	private List<WebElement> conflictModalMessageText;

	@FindAll({ @FindBy(id = "sharedServiceHeaderId"), @FindBy(id = "sharedServiceHeaderId") })
	private List<WebElement> conflictModalHeaderText;

	@FindAll({ @FindBy(css = "div[class*='float-right float-md-none'] button[class*='none d-md-block']"),
			@FindBy(xpath = "//button[text()='CONTINUE REMOVAL']") })
	private WebElement removeServiceBtn;

	// All type of Radio Buttons (Data plans and Add ons)
	@FindAll({ @FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//following-sibling::div[1]//label"),
			@FindBy(xpath = "//*[contains(text(),'Services')]//..//following-sibling::div[1]//label"),
			@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//following-sibling::div[1]//label") })
	private WebElement firstRadioButton;

	@FindBy(css = "input[type='radio']")
	private List<WebElement> radoiButtons;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//label")
	private List<WebElement> dataPlanradoiButtons;

	@FindBy(xpath = "//p[contains(text(),'You currently')]")
	private WebElement youCurrentlyActiveDataPass;

	@FindBy(xpath = "//*[contains(text(),'T-Mobile ONE Plus')]//..//..//label")
	private WebElement oNEPlusInternationalRadioBtn;

	@FindBy(xpath = "//*[text()='Free']//..//..//div[contains(@class, 'radio-container')]/label")
	private List<WebElement> listOfFreeRadioButtons;

	@FindBy(xpath = "//div[contains(@class,'body radio-container')]//..//..//..//span")
	private List<WebElement> listOfRadioButtonsText;

	@FindBy(css = "span[class*='description-regular']")
	private WebElement datapassRestrictedError;

	@FindAll({ @FindBy(xpath = "//*[contains(text(), 'On-Network Data')]//../..//span"),
			@FindBy(xpath = "//*[contains(text(), 'HD 24 Hour Pass')]//../..//span"),
			@FindBy(xpath = "//*[contains(text(), 'International Data')]//../..//span"),
			@FindBy(xpath = "//*[contains(text(), 'Mobile HotSpot 1 Week')]//../..//span") })
	private WebElement dataPass;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//label")
	private List<WebElement> listOfDataPassRadiosLable;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//label")
	private List<WebElement> listOfDataPlanRadiosLable;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//input")
	private List<WebElement> listOfDataPassRadios;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//p[contains(@class,'Display6')]")
	private List<WebElement> listOfDataPlanNames;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//p[contains(@class,'Display6')]")
	private List<WebElement> listOfDataPassNames;

	@FindBy(xpath = "//*[contains(text(),'Data Extras')]//..//..//p[contains(@class,'Display6')]")
	private List<WebElement> listOfDataExtras;

	@FindBy(xpath = "//*[contains(text(),'Other')]//..//..//p[contains(@class,'Display6')]")
	private List<WebElement> listOfServiceNames;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//p[contains(@class,'mb-0 body')]")
	private List<WebElement> dataPlanDescription;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//p[contains(@class,'mb-0 body')]")
	private List<WebElement> dataPassDescription;

	@FindBy(xpath = "//*[contains(text(),'Other')]//..//..//p[contains(@class,'mb-0 body')]")
	private List<WebElement> serviceDescription;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//i[contains(@class,'arrow-down arrowdown_rotation')]")
	private List<WebElement> listOfDataPlanArrowDownbtns;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//i[contains(@class,'arrow-up arrowup_rotation')]")
	private List<WebElement> listOfDataPlanArrowupbtns;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//i[contains(@class,'arrow-down arrowdown_rotation')]")
	private List<WebElement> listOfDataPassArrowDownbtns;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//i[contains(@class,'arrow-up arrowup_rotation')]")
	private List<WebElement> listOfDataPassArrowupbtns;

	@FindBy(xpath = "//*[contains(text(),'Other')]//..//..//i[contains(@class,'arrow-down arrowdown_rotation')]")
	private List<WebElement> listOfServiceArrowDownbtns;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//i[contains(@class,'arrow-up arrowup_rotation')]")
	private List<WebElement> listOfServiceArrowupbtns;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//input[@type='radio']//..//..//..//..//p[contains(@class,'Display')]")
	private List<WebElement> dataPassNamesList;

	@FindBy(xpath = "//*[contains(text(), 'Data Plan')]//../..//p[contains(@class,'Display')]")
	private List<WebElement> dataPlanNamesList;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//input[@type='radio']//..//..//..//..//span")
	private List<WebElement> dataPassPriceList;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//input[@type='radio']//..//..//..//..//span")
	private List<WebElement> dataPlanPriceList;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//..//input")
	private List<WebElement> listOfDataPlanRadios;

	@FindBy(xpath = "//*[contains(text(), 'Data Plan')]//../..//p[contains(@class, 'Display6')]")
	private List<WebElement> dataPlansList;

	@FindBy(xpath = "//*[contains(text(),'T-Mobile ONE Plus')]//..//..//input")
	private WebElement onePlusInternationalDataPlan;

	// All type of Check Boxes (Services Related)

	@FindBy(xpath = "//span[contains(text(),'Active')]")
	private WebElement currentlyActiveServices;

	@FindBy(xpath = "//span[contains(text(),'Active')]//..//..//..//..//span[@class='legal check1']")
	private List<WebElement> currentlyActiveCheckBoxes;

	@FindBy(xpath = "//span[contains(text(),'Active')]/..")
	private WebElement currentlyActiveServicesText;

	@FindBy(xpath = "//*[contains(text(),'Other')]//..//following-sibling::div[1]//label")
	private WebElement firstOtherCheckbox;

	@FindBy(xpath = "//*[contains(text(),'Other')]//..//following-sibling::div[1]//label")
	private WebElement secondOtherCheckbox;

	@FindBy(xpath = "//*[contains(text(),'Data Plan')]//..//following-sibling::div[1]//label")
	private WebElement firstDataPlanCheckbox;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//following-sibling::div[1]//label")
	private WebElement firstDataPassCheckbox;

	@FindBy(xpath = "//*[contains(text(),'Messaging')]//..//following-sibling::div[1]//label")
	private WebElement firstMessagingCheckbox;

	@FindBy(xpath = "//*[contains(text(),'International')]//..//following-sibling::div[1]//label")
	private WebElement firstInternationalCheckbox;

	@FindBy(xpath = "//p[contains(text(),'Name ID')]//..//..//span[@class='legal check1']")
	public List<WebElement> nameIDCheckBox;

	@FindBy(xpath = "//p[contains(text(),'Family Allowances')]//..//..//span[@class='legal check1']")
	private List<WebElement> familyAllowance;

	@FindBy(xpath = "//p[contains(text(),'Netflix Standard')]//..//..//span[@class='legal check1']")
	private List<WebElement> netFlixStandardSoc;

	@FindBy(xpath = "//p[contains(text(),'Netflix Standard')]//..//..//div//..//i")
	private WebElement expandnetFlixStandardSoc;

	@FindBy(xpath = "//p[contains(text(),'Family Allowances')]//..//..//..//..//..//div//..//i")
	private WebElement expandFamilyAloowanceSoc;

	@FindBy(xpath = "//p[contains(text(),'Netflix Premium')]//..//..//span[@class='legal check1']/../../input")
	private List<WebElement> netFlixPremiumSoc;

	@FindBy(css = "button.PrimaryCTA-accent.pull-right")
	private List<WebElement> addOrRemoveServiceButton;

	@FindBy(xpath = "//*[text()='International']//..//..//p[contains(@class,'Display6')]")
	private WebElement internationalFirstCheckBoxText;

	@FindBy(xpath = "//*[text()='Free']//..//..//div[@class='body checkbox']/label")
	private List<WebElement> listOfFreecheckBoxes;

	@FindBy(xpath = "//div[@class='body checkbox']//..//..//..//span[contains(@class,'H6-heading')]")
	private List<WebElement> listOfCheckBoxText;

	@FindBy(xpath = "//span[contains(@class,'H6-heading')][not(contains(text(), 'Free'))]//..//..//..//div[contains(@class, 'd-flex')]//input[@type='checkbox']")
	private List<WebElement> numberOfCheckBoxs;

	@FindBy(xpath = "//*[contains(text(),'Netflix Premium $13.99 with Fam Allowances ($19 value)')]//..//..//..//*[contains(@class, 'H6-heading black pull-right')]")
	private WebElement priceOfPremiumNetworkSoc;

	@FindBy(xpath = "//span[contains(text(),'Hotspot and Data Extras')]")
	private WebElement hotspotAndDataExtras;

	@FindBy(css = "p.Display6.black.mb-0")
	private WebElement tetheringSoc;

	// By elements

	private By activeDataPlanName = By.xpath("//p[normalize-space(text())='%s']//..//..//input");
	private By firstCheckBox = By
			.xpath("//*[contains(text(),'%s')]//..//following-sibling::div//span[contains(@class,'legal')]");
	private By internationalFirstCheckBoxServiceName = By
			.xpath("//*[contains(text(),'%s')]//../..//span[contains(@class,'legal')]");
	private By radioButtonByName = By.xpath("//*[text()='%s']//..//..//div[1]/label");
	private By radioButtonByNameLable = By.xpath("//*[text()='%s']//..//..//div[1]/input");

	private By checkBoxByName = By.xpath("//*[contains(text(),'%s')]//..//..//span[@class='legal check1']");

	private By checkBoxNameByPrice = By
			.xpath("//*[contains(text(),'%s')]//..//..//div[contains(@class,'splitVertical')]/span");

	private By radioButton = By.xpath("//*[text()='%s']//..//..//div[1]/input");
	private By internationalFirstCheckBox = By.xpath("//*[contains(text(),'%s')]//../..//input");

	private By checkBoxName = By.xpath("//p[contains(text(),'%s')]//..//..//span[@class='legal check1']");

	private By price = By
			.xpath("//*[contains(text(),'%s')]//..//..//..//*[contains(@class, 'H6-heading black pull-right')]");
	private By activeService = By.xpath("//p[contains(text(),'%s')]//span[text()='Active']");

	// Other elements
	@FindBy(css = "div[class*='body black']")
	private WebElement headerSuspensionMessage;

	@FindBy(css = "span[ng-bind-html*='errorHeaderMessage']")
	private WebElement errorHeaderMessage;

	@FindBy(css = "span[ng-bind-html*='b52ErrorMessage']")
	private WebElement unblockRoamingMsg;

	@FindBy(linkText = "unblock roaming")
	private List<WebElement> unblockRoaming;

	@FindBy(css = "div[class*='body black p-l-10']")
	private WebElement contactYourPAH;

	@FindBy(css = "#handset-placeholder p")
	private WebElement handsetProtectionWarranty;

	@FindBy(css = "button[class*='PrimaryCTA-accent pull-']")
	private List<WebElement> addServiceButton;

	@FindBy(css = "div.dialog.modal_landscapeht div.text-center.body")
	private List<WebElement> removeSocDescription;

	@FindBy(css = "div.dialog.modal_landscapeht div.text-center.body")
	private List<WebElement> downGradingAlert;

	@FindBy(css = "div[class='col-8 col-sm-8 col-md-9 pull-left no-padding']")
	private List<WebElement> downGradingAlertRemoved;

	@FindBy(css = "div.dialog.modal_landscapeht div.text-center.body")
	private List<WebElement> upGradingAlert;

	@FindBy(css = "div.dialog.modal_landscapeht div.text-center.body")
	private List<WebElement> addSocDescription;

	@FindBy(linkText = "Customer Care")
	private WebElement customerCareLink;

	@FindBy(xpath = "//*[contains(text(),'You have reached the maximum number of allowed data passes')]")
	private List<WebElement> maxlimit;

	@FindBy(css = ".body.padding-bottom-medium")
	private List<WebElement> promoPlanMessage;

	@FindBy(css = "p.legal")
	private List<WebElement> deprioritizationMessage;

	@FindBy(css = "p.H6-heading")
	private List<WebElement> planNameheader;

	@FindBy(css = "span[aria-label*='phone number']")
	private List<WebElement> numberOfLines;

	@FindBy(css = "div.dialog-container button[class*='PrimaryCTA']")
	private List<WebElement> dataPlanConflict;

	@FindBy(xpath = "//*[contains(text(),'Services')]//..//..//p[contains(@class, 'Display6')]")
	private List<WebElement> familyandEntertainmentSectionSocs;

	@FindBy(css = "p[class='body padding-bottom-small padding-top-xsmall']")
	private WebElement activePassHeader;

	@FindBy(css = "li[class='padding-bottom-small text-justify']")
	private List<WebElement> activeDataPasses;

	@FindBy(css = "a[class='body-link cursor text-decoration-underline p-r-5']")
	private List<WebElement> changeDateandRemove;

	@FindBy(css = "button[class='PrimaryCTA-accent pull-right margin-left-xxsmall ml-2']")
	private WebElement removeonremovedatapass;

	@FindBy(css = "button[class='PrimaryCTA-accent pull-right ml-2']")
	private WebElement selectdate;

	@FindBy(css = "button.PrimaryCTA-accent")
	private List<WebElement> removeService;

	@FindBy(css = "label[for*='Family Mode']")
	private static WebElement familyMOde;

	@FindBy(css = "span[class*='arrow-back']")
	private WebElement backButton;

	@FindBy(css = "p.legal.black")
	private WebElement reviewPageLegaleseText;

	@FindBy(xpath = "//p[contains(text(),'Premium Device Protection with Lookout Mobile Security Premium')]")
	private WebElement pHPHeaderSOC;

	@FindBy(xpath = "//*[contains(text(),'Services')]")
	private WebElement familyandEntertainmentHeader;

	@FindBy(xpath = "//span[contains(text(), 'Manage HD')]")
	private WebElement enableHDLink;

	@FindBy(linkText = "Manage Family Allowances")
	private WebElement manageFamilyAloowancesLink;

	@FindBy(linkText = "Manage Netflix")
	private WebElement manageNetflix;

	@FindBy(css = "a[href='/profile/blocking']")
	private WebElement unBlockRoamingLink;

	@FindBy(css = "")
	private WebElement noComapatableDataPassMessage;

	@FindBy(xpath = "//*[contains(text(),'Messaging')]//..//following-sibling::div//p[contains(@class,'Display6')]")
	private List<WebElement> servicesHeader;

	@FindBy(xpath = "(//*[text()='Services']//following::*[@class='Display6 black mb-0'])")
	private static List<WebElement> familySOCCodes;

	@FindBy(xpath = "((.//p[contains(text(),'Services')])[1]//..//..//i)")
	private static List<WebElement> familySOCDescriptionDownwardArrows;

	@FindBy(css = "legend[class='hide-outline']")
	private List<WebElement> headersOnAddonsPage;

	@FindBy(css = "div.dialog.modal_landscapeht")
	private WebElement warmingModal;

	@FindBy(css = "div[class='col-12 no-padding text-center body']")
	private List<WebElement> warmingModalRemovingNetFlix;

	@FindBy(css = "div.dialog.modal_landscapeht div.text-center.body")
	private List<WebElement> warmingMsgRemovingNetFlix;

	private By checkBoxState = By.xpath("//*[contains(text(),'%s')]//..//..//input");

	@FindBy(xpath = "(.//*[contains(@for,'Stateside International with')]//../span)")
	private WebElement pDPSOC;

	@FindBy(xpath = "(//*[@class='dialog modal_landscapeht'])")
	private WebElement warmingModalRemovingPDPSoc;

	@FindBy(xpath = "(//*[@class='col-12 no-padding black Display5'])")
	private WebElement warmingModalHeader;

	@FindBy(xpath = "(//*[@class='col-12 no-padding text-center body'])")
	private WebElement warmingMsgRemovingPDPPHP;

	@FindBy(css = ".pull-right.cursor.arrow-up.arrowup_rotation")
	private WebElement expandedArrowForDescriptionOfSOC;

	@FindBy(xpath = "(.//*[@class='accordion-content content-open']//p)")
	private WebElement descriptionOfSOC;

	@FindBy(css = ".body.black.p-l-10.col-11.offset-md-1.p-t-5")
	private WebElement messageForIneligibleSOC;

	@FindBy(linkText = "Edit")
	private WebElement changeDateLink;

	@FindBy(linkText = "Remove")
	private List<WebElement> removeDataPassLink;

	@FindBy(xpath = "//span[contains(text(),'|')]")
	private WebElement pipeCharacter;

	@FindBy(css = "input[id*='T-Mobile iPhone Upgrade Program']")
	private WebElement nonremovable;

	@FindBy(xpath = "//span[contains(text(),'One-time Data Pass')]")
	private WebElement datapasslabel;

	@FindBy(css = ".icon-size")
	private WebElement tMobileIcon;

	@FindBy(css = "i[class*='arrow-down']")
	private WebElement downArrow;

	@FindBy(css = "div[class='body black p-l-10 col-11 offset-md-1 p-t-5']")
	private WebElement msgFeatureNotAvailable;

	@FindBy(xpath = "//p[contains(text(),'Netflix Standard $10.99 with Fam Allowances ($16 value)')]//span[text()='Active']")
	private List<WebElement> activeServiceFA16List;

	@FindBy(xpath = "//p[contains(text(),'Netflix Premium $13.99 with Fam Allowances ($19 value)')]//span[text()='Active']")
	private List<WebElement> activeServiceFA19List;

	@FindBy(xpath = "//span[text()='Active']//..//..//..//span[@class='legal check1']")
	private List<WebElement> activeServiceCheckBox;

	@FindBy(xpath = "/div[@class='d-flex padding-top-small align-items-center']//p[contains(text(),'%s')]/parent::div/parent::div//i")
	private By downcarrot;

	@FindBy(xpath = "//fieldset//p[@class='Display6 black mb-0']")
	private List<WebElement> listOfService;

	@FindBy(xpath = "//span[text()='Active']/..")
	private static WebElement activeWebElement;

	@FindBy(xpath = "//span[contains(text(),'Active')]/..")
	private WebElement currentlyActiveServicesWithFutureDateText;

	@FindBy(css = "div[class='dialog modal_landscapeht'] div")
	private WebElement conflictDialogWindow;

	@FindBy(xpath = "//span[contains(text(),'Active')]/..")
	private List<WebElement> listOfFutureDatedService;

	@FindBy(xpath = "//a[contains(text(),'Select a different line.')]")
	private WebElement selectDifferentLineLink;

	@FindBy(xpath = "//span[contains(text(),'Select a line to manage')]")
	private WebElement headerOfLineSelectorPage;

	@FindBy(xpath = "//span[@class='float-left arrow-size arrow-back']")
	private WebElement backNavArrowOfLineSelectorPage;

	@FindBy(xpath = "//*[contains(text(),'Data Pass')]//..//..//input")
	private WebElement dataPassRadioButtons;

	@FindBy(linkText = "Select a different line.")
	private WebElement selectADifferentLineLink;

	@FindBy(css = ".mb-0.body.text-italic.m-c-r")
	private WebElement mostPopularLabel;

	@FindAll({ @FindBy(css = "div.col-6.col-md-8.no-padding.pl-1 p") })
	private List<WebElement> ManageAddonSocs;

	@FindBy(xpath = "//*[contains(text(),'No Data')]//..//..//label")
	private WebElement noDataRadioBtn;

	@FindBy(css="p.Display6.black.mb-0")
	private WebElement Dataplan;

	@FindBy(css = "p.body.padding-bottom-medium.padding-top-xsmall")
	private WebElement monthlyDataplan;

	@FindBy(css = "legend.hide-outline.ng-star-inserted")
	private List<WebElement> DataSections;


	public ManageAddOnsSelectionPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Data Pass List page is loaded
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public ManageAddOnsSelectionPage verifymanageDataAndAddOnsPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementText(manageDataAndAddOns, "Manage Data and Add-ons"),
					"Manage your data and AddOns page is loaded");
			Reporter.log("Manage data and AddOns page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Manage data and addons page not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyPendingChangesDataOnAddOnsPage() {
		try {
			Assert.assertTrue(pendingChanges.getText().contains("pending changes on your account"),
					"This account has pending changes");
			Reporter.log("This account has pending changes.");
		} catch (Exception e) {
			Assert.fail("This account doesn't have the pending changes");
		}
		return this;
	}
	// Continue & conflict Dialog buttons

	/**
	 * 
	 * Click on continue button
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage clickOnContinueBtn() {

		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(continueButton));
			clickElementWithJavaScript(continueButton);
			Reporter.log("Continue button is clicked.");
		} catch (Exception e) {
			Assert.fail("Continue button is not clicked");
		}
		return this;
	}

	/**
	 * 
	 * Verify Continue Button Is Disabled
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyContinueBtnIsDisabled() {
		try {
			Verify.assertTrue(continueButton.getAttribute("disabled").contains("true"));
			Reporter.log("Continue Button is disabled");
		} catch (Exception e) {
			Assert.fail("Continue button is not displayed.");
		}
		return this;
	}

	/**
	 * 
	 * Verify Continue button Is Enabled
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyContinueBtnIsEnabled() {
		try {
			Verify.assertTrue(continueButton.isDisplayed());
			Reporter.log("Continue Button is displayed.");
		} catch (Exception e) {
			Assert.fail("Continue button is not displayed.");
		}
		return this;
	}

	/**
	 * Click Conflict Dialog continue
	 */
	public ManageAddOnsSelectionPage clickOnConflictContinue() {
		checkPageIsReady();
		try {
			if (!conflictDialogContinue.isEmpty()) {
				conflictDialogContinue.get(0).click();
			}
			Reporter.log("Continue button of conflict modal is clicked");
		} catch (Exception e) {
			Reporter.log("Skipped::Conflict Dialog overlay is not displayed");
		}
		return this;
	}

	/**
	 * Verify Conflict Dialog continue is displayed
	 */
	public ManageAddOnsSelectionPage verifyContinuePDPRemovalBtnisDisplayed() {
		try {
			Verify.assertTrue(conflictDialogContinue.get(0).isDisplayed());
			Reporter.log("continue Removal button is dispalyed");
		} catch (Exception e) {
			Assert.fail("continue Removal button  is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify Conflict Dialog continue is displayed
	 */
	public ManageAddOnsSelectionPage verifyContinueRemovalBtnisDisplayed() {
		checkPageIsReady();
		try {
			for (int i = 0; i < continueRemoval.size(); i++) {
				if (continueRemoval.get(i).isDisplayed() && continueRemoval.get(i).isEnabled()) {
					Reporter.log("Continue Removal button is dispalyed");
					continueRemoval.get(i).click();
					Reporter.log("Clicked on Continue Removal button");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Continue Removal button  is not dispalyed");
		}
		return this;
	}

	/**
	 * Click on Conflict Dialog cancel
	 */
	public ManageAddOnsSelectionPage clickOnConflictModalCancelBtn() {
		try {
			conflictModalCancelBtn.click();
			Reporter.log("Cancel CTA on Conflict modal is clicked");
		} catch (Exception e) {
			Assert.fail("Cancel CTA on Conflict modal is not clicked");
		}
		return this;
	}

	/**
	 * Verify Conflict Modal
	 */
	public ManageAddOnsSelectionPage verifyConflictModal() {

		try {
			Assert.assertTrue(verifyElementBytext(conflictModalHeaderText, "We're Conflicted"));
			Reporter.log("Continue to proceed with these changes text is  displayed");
		} catch (Exception e) {
			Assert.fail("Continue to proceed with these changes text is not displayed");
		}
		return this;
	}

	// Data Pass & Data Plans (Radio buttons)

	/**
	 * Verify Data Pass Label
	 */

	public ManageAddOnsSelectionPage verifyDataPassLabel() {
		try {
			checkPageIsReady();
			Assert.assertEquals("One-time Data Pass", datapasslabel.getText());
			Reporter.log("One time Data Pass label is present.");
		} catch (Exception e) {
			Assert.fail("One time Data Pass label is not present");
		}
		return this;
	}

	/**
	 * Click On Network data
	 * 
	 */
	public ManageAddOnsSelectionPage clickOnDataPass() {
		checkPageIsReady();
		try {
			dataPass.click();
			Reporter.log("Data Pass link is clicked.");
		} catch (Exception e) {
			Assert.fail("Data Pass link is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnAllDataPlans() {
		try {
			for (WebElement radioButtons : dataPlanradoiButtons) {
				radioButtons.click();
			}
			Reporter.log("Conflict Model is not displayed while changing different Data plans ");
		} catch (Exception e) {
			Assert.fail("All Data Plans button is not clicked");
		}
		return this;
	}

	// Check boxes

	public ManageAddOnsSelectionPage verifyLegaleseText(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(reviewPageLegaleseText.getText().contains(msg), "Legalese text is not displayed");
		} catch (Exception e) {
			Assert.fail("Legalese text is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyCurrentlyActiveText() {
		try {
			String[] currentActiveDataPlan = youCurrentlyActiveDataPass.getText().split("You currently have");
			String activeDataPlan = currentActiveDataPlan[1].trim();
			Verify.assertTrue(getDriver().findElement(getNewLocator(getNewLocator(activeDataPlanName, activeDataPlan)))
					.isSelected(), "Currently Active text is not displayed");
			Reporter.log("Currently active text is displayed");
		} catch (Exception e) {
			Assert.fail("Active DataPass is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage checkNameIDCheckBoxs() {
		try {
			randomElementClick(nameIDCheckBox);
		} catch (Exception e) {
			Assert.fail("Name Id CheckBox is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnAddRemoveService() {

		try {
			clickElement(addOrRemoveServiceButton);
			Reporter.log("Clicked on Add Service CTA");
		} catch (Exception e) {
			Assert.fail("Add SErvice CTA is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage acceptAddServicePopup() {
		try {
			for (WebElement element : addServiceButton) {
				if (element.isDisplayed()) {
					element.click();
					break;
				}
			}
			// addServiceButton.click();
			Reporter.log("Clicked on Add Service CTA");
		} catch (Exception e) {
			Assert.fail("Add SErvice CTA is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnCheckBox(String ckName) {
		try {
			if (!ckName.isEmpty()) {
				getDriver().findElement(getNewLocator(checkBoxName, ckName)).click();
				Reporter.log("Clicked on " + ckName + " check box ");
			} else {
				checkFirstCheckBox();
				verifyContinueRemovalBtnisDisplayed();
				Reporter.log("Clicked on first check box ");
			}
		} catch (Exception e) {
			Assert.fail("Not clicked on " + ckName + " for T-Mobile check box");
		}
		return this;
	}

	public ManageAddOnsSelectionPage uncheckActiveChekBox() {
		try {
			activeServiceCheckBox.get(0).click();
			Reporter.log("Clicked on Active checkbox");
		} catch (Exception e) {
			Assert.fail("Not able to click on Active checkbox ");
		}
		return this;
	}

	public ManageAddOnsSelectionPage unCheckCheckBox(String ckName) {
		try {
			getDriver().findElement(getNewLocator(checkBoxName, ckName)).click();
			Reporter.log("unchecked  " + ckName + " check box ");
			Reporter.log("Clicked on " + ckName + " check box ");
		} catch (Exception e) {
			Assert.fail("Not clicked on " + ckName + " for T-Mobile check box");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnCheckBoxForPDPSOC() {
		try {
			pDPSOC.click();
			Reporter.log("Clicked on PDP soc");
		} catch (Exception e) {
			Assert.fail("Not clicked on PDP Soc check box");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyCurrentlyActiveServices() {
		try {

			Verify.assertTrue(currentlyActiveServices.isDisplayed());
			Reporter.log("Currently Active service is displayed.");
		} catch (Exception e) {
			Assert.fail("Currently Active service is not displayed.");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnCurrentlyActiveCheckBoxe() {
		try {
			currentlyActiveCheckBoxes.get(0).click();
			Reporter.log("Active checkbox is clicked");
		} catch (Exception e) {
			Assert.fail("Active checkbox is not clicked");
		}
		return this;
	}

	public String getCurrentlyActiveServicesText() {
		return currentlyActiveServicesText.getText();
	}

	public String getCurrentlyActiveFutureDatedText() {
		return currentlyActiveServicesWithFutureDateText.getText();
	}

	public ManageAddOnsSelectionPage waitForConflictDialogAndClickContinueBtn() {

		try {
			removeServiceBtn.click();
			Reporter.log("Remove Service CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Remove Service CTA is not clicked");
		}

		return this;
	}

	public ManageAddOnsSelectionPage checkFirstCheckBox() {
		try {
			if (!getDriver().findElement(getNewLocator(getNewLocator(firstCheckBox, "Services"))).isSelected()) {
				getDriver().findElement(getNewLocator(getNewLocator(firstCheckBox, "Services"))).click();

			} else if (!getDriver().findElement(getNewLocator(getNewLocator(firstCheckBox, "Services"))).isSelected()) {
				getDriver().findElement(getNewLocator(getNewLocator(firstCheckBox, "Services"))).click();
			}
		} catch (Exception e) {
			Assert.fail("Check box is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage unselectActiveCheckBox() {
		try {
			for (WebElement element : currentlyActiveCheckBoxes) {
				if (element.isSelected()) {
					element.click();
					break;
				}
			}

		} catch (Exception e) {
			Assert.fail("Check box is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnONEPlusInternationalRadioBtn() {
		try {
			oNEPlusInternationalRadioBtn.click();
			Reporter.log("T-Mobile ONE Plus radio button is clicked");
		} catch (Exception e) {
			Assert.fail("T-Mobile ONE Plus radio button is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnFirstInternationalCheckbox() {
		try {
			firstInternationalCheckbox.click();
			Reporter.log("International radio button is clicked");
		} catch (Exception e) {
			Assert.fail("International radio button is not clicked");
		}
		return this;
	}

	public String getInternationalFirstCheckBoxText() {

		Reporter.log("Get International Service Name text");
		moveToElement(internationalFirstCheckBoxText);
		return internationalFirstCheckBoxText.getText().trim();
	}

	public ManageAddOnsSelectionPage verifyInternationalServiceIsNotChecked(String serviceName) {
		try {
			Assert.assertFalse(getDriver()
					.findElement(getNewLocator(getNewLocator(internationalFirstCheckBoxServiceName, serviceName)))
					.isSelected());
			Reporter.log("Verified International service is checked");
		} catch (Exception e) {
			Assert.fail("Verified International service is not checked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyInternationalServiceIsChecked(String serviceName) {
		try {

			Assert.assertTrue(
					getDriver().findElement(getNewLocator(internationalFirstCheckBox, serviceName)).isSelected());

			Reporter.log("Verified International service is checked");
		} catch (Exception e) {
			Assert.fail("Verified International service is not checked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnFreeRadioOrCheckBoxButton(String type) {
		try {
			if (type.contains("Radio") && !type.isEmpty()) {
				for (WebElement webElement : listOfFreeRadioButtons) {
					if (!webElement.isSelected()) {
						webElement.click();
						Reporter.log("Free radio button is selected");
						break;
					}
				}
			} else {
				for (WebElement webElement : listOfFreecheckBoxes) {
					if (!webElement.isSelected()) {
						webElement.click();
						Reporter.log("Free check box button is selected");
						break;
					}
				}
			}
		} catch (Exception e) {

			Assert.fail("Free Radio button or check box not selected");
		}

		return this;

	}

	public ManageAddOnsSelectionPage clickOnNumberRadioButton() {
		try {
			for (WebElement webElement : listOfRadioButtonsText) {
				String radioBtnText = webElement.getText();
				if (!radioBtnText.contains("Free")) {
					Reporter.log("Free radio button");
					WebElement radiobtnName = getDriver().findElement(getNewLocator(radioButtonByName, radioBtnText));
					WebElement radiobtn = getDriver().findElement(getNewLocator(radioButton, radioBtnText));
					if (!radiobtn.isSelected()) {
						radiobtnName.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Radio button is not clicked");
		}
		return this;
	}

	public String clickOnNumberRadioButtonAndGetText() {
		String radioBtnText = null;
		for (WebElement webElement : listOfRadioButtonsText) {
			radioBtnText = webElement.getText();
			WebElement raiobtnName = getDriver().findElement(getNewLocator(radioButtonByName, radioBtnText));
			WebElement raiobtnLable = getDriver().findElement(getNewLocator(radioButtonByNameLable, radioBtnText));
			if (!radioBtnText.contains("Free") && !raiobtnLable.isSelected()) {
				raiobtnName.click();
				break;
			}
		}
		return radioBtnText;
	}

	public void clickOnDecreaseService() {
		for (WebElement webElement : numberOfCheckBoxs) {
			if (webElement.isSelected()) {
				String web = webElement.getAttribute("id");
				WebElement ele = getDriver().findElement(By.cssSelector("[id='" + web + "'] + div span"));
				ele.click();
				break;
			}
		}
	}

	/*
	 * Standard Restricted customer messsage to contact primary AH
	 */

	public ManageAddOnsSelectionPage verifySuspendedorContactPAHMessage(String message) {
		try {
			Assert.assertTrue(headerSuspensionMessage.getText().contains(message), message + " is not displayed");
			Reporter.log(message + " is  displayed");
		} catch (Exception e) {
			Assert.fail(message + " is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage getErrorHeaderMessage(String msg) {
		try {
			waitforSpinner();
			Assert.assertTrue(headerSuspensionMessage.getText().contains(msg));
			Reporter.log("Customer Care message is displayed");
		} catch (Exception e) {
			Assert.fail("Customer Care message is not displayed");
		}
		return this;
	}

	/**
	 * Click Unblock Roaming
	 */
	public ManageAddOnsSelectionPage verifyUnblockRoamingAndUnblockIt() {
		try {

			if (!CollectionUtils.isEmpty(unblockRoaming) && CollectionUtils.sizeIsEmpty(unblockRoaming)) {
				unblockRoaming.get(0).click();
				checkPageIsReady();
				BlockingPage blockingPage = new BlockingPage(getDriver());
				blockingPage.clickBlockInternationalToggle();
				waitforSpinner();

				String[] currentUrl = getDriver().getCurrentUrl().split("account/profile/blocking");
				String upSellURL = currentUrl[0] + "account/odf/DataService:ALL/Service:ALL/DataPass:ALL";
				getDriver().get(upSellURL);
				checkPageIsReady();
				verifymanageDataAndAddOnsPage();
				Reporter.log("Unblock Roaming radion button is clicked");
			}
		} catch (Exception e) {
			Assert.fail("Unblock Roaming radion button is not found");
		}
		return this;
	}

	/**
	 * Net flix premium price
	 */
	public ManageAddOnsSelectionPage verifypriceforpremiumNetflixSoc() {

		try {

			Assert.assertTrue(
					priceOfPremiumNetworkSoc.isDisplayed() && priceOfPremiumNetworkSoc.getText().contains("$3.00/mo"),
					"price for premium Netflix soc is not $3");
			Reporter.log("price for premium Netflix soc is $3");
		} catch (Exception e) {
			Assert.fail("price for premium Netflix soc is not matched with $3");
		}
		return this;
	}

	/**
	 * Data plan conflict continue
	 */
	public ManageAddOnsSelectionPage clickOnDataplanConflictContinue() {

		try {
			if (!dataPlanConflict.isEmpty()) {
				dataPlanConflict.get(1).click();
				Reporter.log("Continue button of conflict modal is clicked");
			}
		} catch (Exception e) {
			Reporter.log("Skipped::Conflict Dialog overlay is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnDataPassRadioButton() {

		try {
			for (WebElement webElement : listOfDataPassRadiosLable) {
				if (!webElement.isSelected()) {
					webElement.click();
					Reporter.log("Radio button is selected");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Radio button is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnDataPlanRadioButton() {

		try {
			for (WebElement webElement : listOfDataPlanRadiosLable) {
				if (!webElement.isSelected()) {
					webElement.click();
					Reporter.log("Radio button is selected");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Radio button not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnCheckBoxByName(String name) {
		try {
			getDriver().findElement(getNewLocator(checkBoxByName, name)).click();
			Reporter.log("Clicked check box" + name + " ");
		} catch (Exception e) {
			Assert.fail("" + name + " Check box is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyCheckBoxByNameIsSelected(String name) {
		try {
			checkPageIsReady();
			WebElement blockContent = getDriver().findElement(getNewLocator(checkBoxByName, name));
			scrollToElement(blockContent);
			blockContent.isSelected();
			Reporter.log(" " + name + " is selected");
		} catch (Exception e) {
			Assert.fail("" + name + " Not selected");
		}
		return this;
	}

	public String getCheckBoxPrice(String name) {
		return getDriver().findElement(getNewLocator(checkBoxNameByPrice, name)).getText();
	}

	public ManageAddOnsSelectionPage verifyTextInConflictedOverlay(String name) {
		try {
			Reporter.log(" " + name + " is displayed");
		} catch (Exception e) {
			Assert.fail("" + name + "is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyPAHToMakeChanges(String msg) {
		try {
			Assert.assertTrue(contactYourPAH.getText().contains(msg));
			Reporter.log("Please contact your primary account holder to make changes on this line text is displayed");
		} catch (Exception e) {
			Assert.fail(
					"Please contact your primary account holder to make changes on this line text is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyRadioBtnsAndCheckBoxes() {

		try {
			Assert.assertTrue(!CollectionUtils.isNotEmpty(radoiButtons) && CollectionUtils.sizeIsEmpty(radoiButtons));
			Reporter.log("Radio buttons and Check boxes are not displayed");
		} catch (Exception e) {
			Assert.fail("Radio buttons and Check boxes are displayed for restricted user");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyHandsetProtectionWarranty(String msg) {

		try {
			Assert.assertTrue(handsetProtectionWarranty.getText().contains(msg));
			Reporter.log(
					"Handset Protection Extended Warranty cannot be added back and you will no longer be covered for mechanical or electrical breakdown is displayed");
		} catch (Exception e) {
			Assert.fail(
					"Handset Protection Extended Warranty cannot be added back and you will no longer be covered for mechanical or electrical breakdown text is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyPendingChangesEffectiveDate(String date) {
		try {
			String formatedDate = changeDateFormate(date);

			getErrorHeaderMessage(formatedDate);
		} catch (Exception e) {
			Assert.fail("Pending Changes Effective Date is not displaying correctly");
		}
		return this;
	}

	public ManageAddOnsSelectionPage modifyActiveNetFlixSOC(String ckName1, String ckName2) {
		try {
			if (getDriver().findElement(getNewLocator(checkBoxName, ckName1)).isSelected()) {
				getDriver().findElement(getNewLocator(checkBoxName, ckName1)).click();
				Reporter.log("Clicked on " + ckName1 + " check box ");
			} else {
				getDriver().findElement(getNewLocator(checkBoxName, ckName2)).click();
				Reporter.log("Clicked on " + ckName2 + " check box ");
			}

		} catch (Exception e) {
			Assert.fail("Not clicked on " + ckName1 + " for T-Mobile check box");
		}
		return this;
	}

	public boolean verifyFamilyAllowanceCheckBoxisSelected() {
		boolean display = false;
		try {
			if (familyAllowance.get(0).isSelected()) {
				display = true;
			}
		} catch (Exception e) {
			Assert.fail("Family Allowance CheckBox is not clicked");
		}
		return display;
	}

	public boolean verifyStandardCheckBoxisSelected() {
		boolean display = false;
		try {

			if (netFlixStandardSoc.get(0).isSelected()) {
				display = true;
			}
		} catch (Exception e) {
			Assert.fail("Standard CheckBox is not clicked");
		}
		return display;
	}

	public boolean verifyNetFlixPremiumSocCheckBoxisSelected() {
		boolean display = false;
		try {
			if (netFlixPremiumSoc.get(0).isSelected()) {
				display = true;
			}
		} catch (Exception e) {
			Assert.fail("Family Allowance CheckBox is not clicked");
		}
		return display;
	}

	public ManageAddOnsSelectionPage checkNetFlixPremiumSocCheckBox() {
		try {
			clickElement(netFlixPremiumSoc);
		} catch (Exception e) {
			Assert.fail("premium netflix  is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyRemovingPremiumSocDescription() {
		try {
			verifyElementBytext(removeSocDescription,
					"By removing Netflix Premium $13.99 with Fam Allowances ($19 value) from this line you are also removing it from other lines on your account");
			/*
			 * Assert.assertTrue( removeSocDescription.getText().contains(
			 * "By removing Netflix Premium $13.99 with Fam Allowances ($19 value) from this line you are also removing it from other lines on your account"
			 * ), "Removing  Netflix Premium alert message is not displayed");
			 */
			Reporter.log("Removing Netflix Premium message is displayed");
		} catch (Exception e) {
			Assert.fail("Removing Netflix Premium alert message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyRemovingFASocDescription() {
		try {
			verifyElementBytext(removeSocDescription, "");
			/*
			 * Assert.assertTrue(removeSocDescription.getText().contains(""),
			 * "Removing  Netflix Premium alert message is not displayed");
			 */
			Reporter.log("Removing Netflix Premium message is displayed");
		} catch (Exception e) {
			Assert.fail("Removing Netflix Premium alert message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyRemovingStandardSocDescription() {
		try {
			verifyElementBytext(removeSocDescription, "");
			/*
			 * Assert.assertTrue(removeSocDescription.getText().contains(""),
			 * "Removing  Netflix standard alert message is not displayed");
			 */
			Reporter.log("Removing Netflix standard message is displayed");
		} catch (Exception e) {
			Assert.fail("Removing Netflix standard alert message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAddingPremiumSocDescription() {
		try {

			Assert.assertTrue(addSocDescription.get(0).getText().contains(""),
					"adding  Netflix Premium alert message is not displayed");
			Reporter.log("adding Netflix Premium message is displayed");
		} catch (Exception e) {
			Assert.fail("adding Netflix Premium alert message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAddingFASocDescription() {
		try {
			verifyElementBytext(downGradingAlert,
					"By adding Family Allowances to this line you are also adding it to other lines on your account. This service will be added to the following lines");
			/*
			 * Assert.assertTrue(addSocDescription.get(0).getText().contains("") ,
			 * "adding  FA soc alert message is not displayed");
			 */
			Reporter.log("adding FA soc message is displayed");
		} catch (Exception e) {
			Assert.fail("adding FA soc alert message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAddingNetFlixStandatdSocDescription() {
		try {
			verifyElementBytext(addSocDescription,
					"By adding Netflix Standard $10.99 with Fam Allowances ($16 value) to this line you are also adding it to other lines on your account. This service will be added to the following lines:");
			/*
			 * Assert.assertTrue( addSocDescription.getText().contains(
			 * "By adding Netflix Standard $10.99 with Fam Allowances ($16 value) to this line you are also adding it to other lines on your account. This service will be added to the following lines:"
			 * ), "adding  standard soc alert message is not displayed");
			 */
			Reporter.log("adding standard soc message is displayed");
		} catch (Exception e) {
			Assert.fail("adding standard soc alert message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAddingNetFlixPremiumSocDescription() {
		try {
			verifyElementBytext(addSocDescription,
					"By adding Netflix Premium $13.99 with Fam Allowances ($19 value) to this line you are also adding it to other lines on your account. This service will be added to the following lines:");
			Reporter.log("adding standard soc message is displayed");
		} catch (Exception e) {
			Assert.fail("adding standard soc alert message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage checkfamilyAllowanceCheckBoxs() {
		try {
			clickElement(familyAllowance);
		} catch (Exception e) {
			Assert.fail("Family Allowance CheckBox is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage checkStandardCheckBoxs() {
		try {
			clickElement(netFlixStandardSoc);
		} catch (Exception e) {
			Assert.fail("netflix standard CheckBox is not clicked");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyNetFlixDownGradingAlerts(String msg) {
		try {

			verifyElementBytext(downGradingAlert, msg);
			/*
			 * Assert.assertTrue(downGradingAlert.getText().contains(msg),
			 * "netflix downgrading  alert message is not displayed");
			 */
			Reporter.log("netflix downgrading message is displayed");
		} catch (Exception e) {
			Assert.fail("netflix downgrading is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyNetFlixDownGradingAlertsRemovedService(String msg) {
		try {
			verifyElementBytext(downGradingAlertRemoved, msg);
			/*
			 * Assert.assertTrue(downGradingAlertRemoved.getText().contains(msg) ,
			 * "netflix downgrading  alert message is not displayed");
			 */
			Reporter.log(msg + "service name is displayed");
		} catch (Exception e) {
			Assert.fail(msg + " service name is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyNetFlixUpGradingAlertsRemovedService(String msg) {
		try {
			verifyElementBytext(downGradingAlertRemoved, msg);
			/*
			 * Assert.assertTrue(downGradingAlertRemoved.getText().contains(msg) ,
			 * "netflix downgrading  alert message is not displayed");
			 */
			Reporter.log("netflix Upgrading removed service name is displayed");
		} catch (Exception e) {
			Assert.fail("netflix Upgrading removed service name is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyNetFlixUPGradingAlerts(String msg) {
		try {
			verifyElementBytext(upGradingAlert, msg);
			/*
			 * Assert.assertTrue(upGradingAlert.getText().contains(msg),
			 * "netflix upgrading  alert message is not displayed");
			 */
			Reporter.log("netflix upgrading message is displayed");
		} catch (Exception e) {
			Assert.fail("netflix upgrading is not displayed");
		}
		return this;
	}

	public String clickOnDataPassRadioButton(String dataPassName) {
		checkPageIsReady();
		int count = 0;
		String selectedPassName = "";
		try {
			for (WebElement webElement : dataPassNamesList) {
				count++;
				if (!dataPassName.isEmpty()) {
					if (webElement.getText().contains(dataPassName)) {
						listOfDataPassRadiosLable.get(count - 1).click();
						selectedPassName = dataPassPriceList.get(count - 1).getText();
						break;
					}
				} else {
					listOfDataPassRadiosLable.get(0).click();
					selectedPassName = dataPassPriceList.get(0).getText();
					break;
				}
			}
			Reporter.log("" + dataPassName + "radio button is selected ");
		} catch (Exception e) {
			Assert.fail("Radio button not displayed");
		}
		return selectedPassName;
	}

	public ManageAddOnsSelectionPage clickOnDataPlanRadioButton(String dataPlanName) {
		int count = 0;
		try {

			for (WebElement webElement : dataPlanNamesList) {
				count++;
				if (webElement.getText().equalsIgnoreCase(dataPlanName)) {
					listOfDataPlanRadiosLable.get(count - 1).click();
					break;
				}
			}

			Reporter.log("" + dataPlanName + "radio button is selected");
		} catch (Exception e) {
			Assert.fail("Radio button not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAllRadioButtonsNotDisplayed() {
		try {
			Verify.assertTrue(listOfDataPassRadios.isEmpty());
		} catch (Exception e) {
			Assert.fail("Radio buttons are dispalyed ");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyCancelButtonisInDisabled() {
		try {

			Assert.assertFalse(conflictModalCancelBtn.isEnabled(), "cancel button is not disabled");
		} catch (Exception e) {
			Assert.fail("cancel button is not disabled");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickOnCustomerCareLink() {
		try {

			customerCareLink.click();
		} catch (Exception e) {
			Assert.fail("customer care link not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyContactUsPageDispalyed() {
		try {

			getDriver().getCurrentUrl().contains("contact-us");
		} catch (Exception e) {
			Assert.fail("contact-us page not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyPromoPlanMessage() {
		try {

			verifyElementBytext(promoPlanMessage,
					"If on a promo plan, you'll lose promotional discount(s) when migrating to a new plan.");
			Reporter.log(
					"If on a promo plan, you'll lose promotional discount(s) when migrating to a new plan text is displayed");
		} catch (Exception e) {
			Assert.fail(
					"If on a promo plan, you'll lose promotional discount(s) when migrating to a new plan text not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyDeprioritizationMessage() {
		try {
			verifyElementBytext(deprioritizationMessage,
					"On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization.");
			Reporter.log(
					"On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization. text is displayed");
		} catch (Exception e) {
			Assert.fail(
					"On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization. not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan(
			String planName) {
		try {
			if (planName.equalsIgnoreCase("Data Plan")) {
				randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan(listOfDataPlanNames,
						listOfDataPlanArrowDownbtns, dataPlanDescription, listOfDataPlanArrowupbtns, planName);
			} else if (planName.equalsIgnoreCase("Data Pass")) {
				randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan(listOfDataPassNames,
						listOfDataPassArrowDownbtns, dataPassDescription, listOfDataPassArrowupbtns, planName);
			} else if (planName.equalsIgnoreCase("Service")) {
				randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan(listOfServiceNames,
						listOfServiceArrowDownbtns, serviceDescription, listOfServiceArrowupbtns, planName);
			}
		} catch (Exception e) {
			Assert.fail("" + planName + " description is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan(
			List<WebElement> listofnames, List<WebElement> listOfArrowDownbtns, List<WebElement> listOfBodyDescription,
			List<WebElement> listOfArrowupbtns, String planName) {
		try {
			for (WebElement element : listofnames) {
				for (WebElement listofArrowbtns : listOfArrowDownbtns) {
					listofArrowbtns.click();
					String dataPlanName = element.getText();
					checkPageIsReady();
					if (dataPlanName.contains("No Data")) {
						verifyElementBytext(listOfBodyDescription, "Customer declined data service");
						Reporter.log(planName + " description is displayed");
					} else {
						Assert.assertTrue(!dataPlanDescription.isEmpty());
						Reporter.log(planName + " description is displayed");
					}
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail(planName + " description is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyPlanNameHeader(String planName) {
		try {
			verifyElementBytext(planNameheader, planName);
		} catch (Exception e) {
			Assert.fail(" " + planName + " is not displayed");
		}
		return this;
	}

	public String getActiveDataPlan() {
		int count = 0;
		try {
			for (WebElement webElement : listOfDataPlanRadios) {
				count++;
				if (webElement.isSelected()) {
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Radio button not displayed");
		}
		return dataPlansList.get(count - 1).getText();
	}

	public ManageAddOnsSelectionPage selectDataPlanODFPage() {
		checkPageIsReady();
		try {
			for (int i = 0; i < listOfDataPlanRadios.size(); i++) {

				if (!listOfDataPlanRadios.get(i).isSelected()) {
					dataPlanradoiButtons.get(i).click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("data plan Radio button not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage selectDataPlan(String activeDataPlan) {
		checkPageIsReady();
		try {
			if (activeDataPlan.equals("ONE Plus")) {
				listOfDataPlanRadiosLable.get(0).click();
			} else if (activeDataPlan.equals("T-Mobile ONE")) {
				listOfDataPlanRadiosLable.get(2).click();
			}
		} catch (Exception e) {
			Assert.fail("Not a good test data to test this scenario , so  failing the test case");
		}
		return this;
	}

	public ManageAddOnsSelectionPage selectDataPlanODFPAge(String DataPlan) {
		checkPageIsReady();
		try {
			if (DataPlan.equals("ONE Plus")) {
				listOfDataPlanRadiosLable.get(2).click();
			}
		} catch (Exception e) {
			Assert.fail("Not a good test data to test this scenario , so  failing the test case");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyNumberOfLines(int lines) {
		try {
			checkPageIsReady();
			Assert.assertTrue(numberOfLines.size() == lines, "Number of lines not displaying correct");
			Reporter.log("Number of lines are displaying");
		} catch (Exception e) {
			Assert.fail("Number of lines not displaying correct");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyNetFlixPremiumPresentUnderFamilyEntertainmentSection() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(verifyElementBytext(familyandEntertainmentSectionSocs, "Netflix Premium"),
					"Netflix Premium is not displayed under Family and Entertainment Section-failed");
			Reporter.log("Netflix Premium is displayed under Family and Entertainment Section");
		} catch (Exception e) {
			Assert.fail("Netflix Premium is not displayed under Family and Entertainment Section-failed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyFamilyServicesNotAvailableforStandardCustomer() {
		try {

			Assert.assertFalse(verifyElementBytext(familyandEntertainmentSectionSocs, "Family"));
			familyandEntertainmentSectionSocs.isEmpty();
			Reporter.log("family services are not displayed for standard customer");
		} catch (Exception e) {
			Assert.fail("family services are displayed for standard customer --failed");
		}
		return this;
	}

	public boolean verifyElementBytext(WebElement elements, String textOne, String textTwo) {
		boolean isElementDisplayed = false;
		if (elements.getText().contains(textOne) || elements.getText().contains(textTwo)) {
			isElementDisplayed = true;
		}
		return isElementDisplayed;
	}

	public boolean verifyElementBytext(WebElement elements, String textOne) {
		boolean isElementDisplayed = false;
		if (elements.getText().contains(textOne)) {
			isElementDisplayed = true;
		}
		return isElementDisplayed;
	}
	/*
	 * public ManageAddOnsSelectionPage verifyFamilySectionSortingOrder() { try {
	 * Assert.assertTrue(
	 * verifyElementBytext(familyandEntertainmentSectionSocs.get(0),
	 * "Family Allowances" ), "family Allowances List is not matched");
	 * Assert.assertTrue(
	 * verifyElementBytext(familyandEntertainmentSectionSocs.get(1),
	 * "Netflix Premium $13.99 with Fam Allowances ($19 value)" ,
	 * "Netflix Standard $10.99 with Fam Allowances ($16 value)"),
	 * "Netflix premium or standard List is not matched"); Assert.assertTrue(
	 * verifyElementBytext(familyandEntertainmentSectionSocs.get(2),
	 * "Netflix Premium $13.99 with Fam Allowances ($19 value)" ,
	 * "Netflix Standard $10.99 with Fam Allowances ($16 value)"),
	 * "Netflix premium or standard List is not matched"); Reporter.log(
	 * "family and entertainment  services are  displayed");
	 * 
	 * } catch (Exception e) { Assert.fail(
	 * "family and entertainment  services are not displayed"); } return this; }
	 */

	public ManageAddOnsSelectionPage verifyFamilySectionSortingOrder() {
		try {
			Assert.assertTrue(verifyElementBytext(familyandEntertainmentSectionSocs.get(0), "Family Allowances"),
					"family Allowances List is not matched");
			Assert.assertTrue(
					verifyElementBytext(familyandEntertainmentSectionSocs.get(1),
							"Netflix Standard $10.99 with Fam Allowances ($16 value)"),
					"Netflix  standard List is not matched");
			Assert.assertTrue(
					verifyElementBytext(familyandEntertainmentSectionSocs.get(2),
							"Netflix Premium $13.99 with Fam Allowances ($19 value)"),
					"Netflix premium  List is not matched");
			Assert.assertTrue(verifyElementBytext(familyandEntertainmentSectionSocs.get(3), "Family Mode"),
					"Netflix premium  List is not matched");
			Reporter.log("family and entertainment  services are sorted in order");

		} catch (Exception e) {
			Assert.fail("family and entertainment  services are not sorted");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyOnlyPlusNetflixServicesareDispalyed() {
		try {

			Assert.assertTrue(familyandEntertainmentSectionSocs.size() == 2);

			if (familyandEntertainmentSectionSocs.get(0).getText().contains("MagentaPlus 2S")) {

				Assert.assertTrue((familyandEntertainmentSectionSocs.get(0).getText().contains("MagentaPlus 2S")));
				Assert.assertTrue((familyandEntertainmentSectionSocs.get(1).getText().contains("MagentaPlus 4S")));
				Reporter.log("Netflix Plus Services are Displayed");
			} else {
				Assert.assertTrue((familyandEntertainmentSectionSocs.get(1).getText().contains("MagentaPlus 2S")));
				Assert.assertTrue((familyandEntertainmentSectionSocs.get(0).getText().contains("MagentaPlus 4S")));

				Reporter.log("Netflix Plus Services are Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Netflix plus services are not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyOnlyNONPlusNetflixServicesareDispalyed() {
		try {

			Assert.assertTrue(familyandEntertainmentSectionSocs.size() == 3);

			List<String> nonPlusNetflixServices = new LinkedList<String>();
			nonPlusNetflixServices.add("Magenta 1S");
			nonPlusNetflixServices.add("Magenta 2S");
			nonPlusNetflixServices.add("Magenta 3S");
			Assert.assertTrue(familyandEntertainmentSectionSocs.containsAll(nonPlusNetflixServices));

			Reporter.log("Netflix non Plus Services are Displayed");

		} catch (Exception e) {
			Assert.fail("Netflix non plus services are not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyMessageForFeatureNotAvailableDispalyed() {
		try {

			Assert.assertTrue(verifyElementBytext(msgFeatureNotAvailable,
					"This feature is no longer available. Check out other available add-ons to amp up your account!"),
					"Feature not available message is not displayed");
			Reporter.log("The feature is no longer available message is displayed");
		} catch (Exception e) {
			Assert.fail("The feature is no longer available message is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyFamilySectionSortingOrderdirectedfromplanbenefits() {
		try {
			Assert.assertTrue(verifyElementBytext(familyandEntertainmentSectionSocs.get(0), "Family Allowances"),
					"family Allowances List is not matched");
			Assert.assertTrue(
					verifyElementBytext(familyandEntertainmentSectionSocs.get(1),
							"Netflix Standard $10.99 with Fam Allowances ($16 value)"),
					"Netflix  standard List is not matched");
			Assert.assertTrue(
					verifyElementBytext(familyandEntertainmentSectionSocs.get(2),
							"Netflix Premium $13.99 with Fam Allowances ($19 value)"),
					"Netflix premium  List is not matched");

			Reporter.log("family and entertainment  services are sorted in order");

		} catch (Exception e) {
			Assert.fail("family and entertainment  services are not sorted");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyRemoveService() {
		try {
			if (!removeService.isEmpty()) {
				removeService.get(0).click();
				Reporter.log("Clicked on Remove Service button");
			} else {
				Reporter.log("Remove Service button is not displayed");
			}

		} catch (Exception e) {
			Assert.fail("family services are displayed for standard customer --failed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifySyncUpFleetNotPresent() {
		try {
			for (WebElement element : familyandEntertainmentSectionSocs) {
				if ((element.getText().contains("ZFLEETPRO")) || (element.getText().contains("GFLEETPRO"))) {
					Assert.fail("Sync Up Fleet pro plans are displayed");
				}
			}
			Reporter.log("Sync Up Fleet pro plans are not Displayed");
		} catch (Exception e) {
			Assert.fail("Sync Up Fleet pro plans are displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyActivePassesHeader() {
		try {
			Assert.assertTrue(verifyElementBytext(activePassHeader, "Current passes:"));
			Reporter.log("Active Pass Header is Displayed");

		} catch (Exception e) {
			Assert.fail("Active Pass Header is not Displayed");
		}
		return this;
	}

	public int verifyActivePassesIsActive() {
		int a = 0;
		try {
			a = removeDataPassLink.size();
		} catch (Exception e) {
		}
		return a;
	}

	public ManageAddOnsSelectionPage verifyActivePassesWithDate() {

		try {
			Assert.assertTrue(activeDataPasses.get(0).getText().contains("HD 24 Hour Pass - Available for"));
			Assert.assertFalse(activeDataPasses.get(0).getText().contains("Remove"));
			Assert.assertTrue(activeDataPasses.get(1).getText()
					.contains("HD 24 Hour Pass - Begins immediate after previous pass Change Date | Remove"));
			Reporter.log("Active Passes are Displayed");

		} catch (Exception e) {
			Assert.fail("Active Passes are not Displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyChangeDateFunctionality() {
		try {
			changeDateandRemove.get(0).click();
			conflictModalCancelBtn.click();
			Reporter.log("Change Date Functionality Working as Expeceted");
		} catch (Exception e) {
			Assert.fail("Change Date Functionality Failed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyRemovelink() {

		try {
			changeDateandRemove.get(1).click();
			// header.getText().contains("Remove data pass");
			// header1.getText().contains("Are you sure you would like to remove
			// your data pass?");
			Reporter.log("Remove link Working as Expeceted");

		} catch (Exception e) {
			Assert.fail("Remove link failed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyRemovelinkandChangeDataNotPresent() {

		try {

			Assert.assertFalse(activeDataPasses.get(1).getText().contains(" Remove"));

			Reporter.log("Remove link is not Present as Expeceted");

			Assert.assertFalse(activeDataPasses.get(1).getText().contains("Change Date "));

			Reporter.log(" change date is not Present as Expeceted");

		} catch (Exception e) {
			Assert.fail("Remove link and change date is present for Standard customer");
		}
		return this;
	}

	public ManageAddOnsSelectionPage cancelbuttonOnRemoveDataPass() {

		try {
			conflictModalCancelBtn.click();
			Reporter.log("Remove Functionality Working as Expeceted");
		} catch (Exception e) {
			Assert.fail("Remove Functionlity failed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage removebuttonOnRemoveDataPass() {

		try {
			removeonremovedatapass.click();
			Reporter.log("Remove Functionality Working as Expeceted");

		} catch (Exception e) {
			Assert.fail("Remove Functionlity failed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage selectdatepick() {

		try {
			selectdate.click();
			Reporter.log("Remove Functionality Working as Expeceted");
		} catch (Exception e) {
			Assert.fail("Remove Functionlity failed");
		}
		return this;
	}

	public String getPriceOfDataPlan(String dataPlanName) {
		int count = 0;
		try {
			for (WebElement webElement : dataPlanNamesList) {
				count++;
				if (webElement.getText().equalsIgnoreCase(dataPlanName)) {
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Radio button not displayed");

		}
		return dataPlanPriceList.get(count - 1).getText();
	}

	public void changeURLToNewAddOnURLWhenUserHitDirectURLOfOldServicePage(String url) {
		String currentURL[];
		currentURL = getDriver().getCurrentUrl().split("home");
		currentURL[0] = currentURL[0] + url;
		getDriver().get(currentURL[0]);
		verifymanageDataAndAddOnsPage();
	}

	public void changeURLPlansLandingPageURLWhenUserHitDirectURLOfPlansConfigurePage(String url) {
		String currentURL[];
		currentURL = getDriver().getCurrentUrl().split("/home");
		currentURL[0] = currentURL[0] + url;
		getDriver().get(currentURL[0]);
		// PlanPage planPageObj = new PlanPage(getDriver());
		// planPageObj.verifyPlanPageLoad();
	}

	/**
	 * verify PHP Header in upgrade your services page
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyPHPSOCInplanPage() {
		try {
			checkPageIsReady();
			scrollToElement(pHPHeaderSOC);
			pHPHeaderSOC.isDisplayed();
			Reporter.log("PHP SOC is Present in upgrade your services page");
		} catch (Exception e) {
			Assert.fail("PHP SOC is not Present in upgrade your services page");
		}
		return this;
	}

	/**
	 * verify familyandEntertainmentHeader
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyfamilyandEntertainmentHeader() {
		try {
			checkPageIsReady();
			familyandEntertainmentHeader.isDisplayed();
			Reporter.log("family and Entertainment Header is displayed");
		} catch (Exception e) {
			Assert.fail("family and Entertainment Header is not displayed");
		}
		return this;
	}

	/**
	 * Click on Enable HD link
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage clickOnEnableHDLink() {
		checkPageIsReady();

		try {
			enableHDLink.click();
			Reporter.log("Enable HD link is clicked.");
		} catch (Exception e) {
			Assert.fail("Enable HD link is not clicked.");
		}
		return this;
	}

	/**
	 * Click on expand netFlix Standard Soc
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage expandnetFlixStandardSoc() {
		checkPageIsReady();
		try {
			clickElementWithJavaScript(expandnetFlixStandardSoc);
			Reporter.log("Expaned standard netflix soc.");
		} catch (Exception e) {
			Assert.fail("Standard netflix soc not found");
		}
		return this;
	}

	/**
	 * verify manage family allowances link
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyManageFamilyAllowancesLink() {
		checkPageIsReady();
		try {
			manageFamilyAloowancesLink.isDisplayed();
			Reporter.log("manage family allowances link displayed");
		} catch (Exception e) {
			Assert.fail("manage family allowances link not displayed");
		}
		return this;
	}

	/**
	 * verify manage family allowances link
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage unBlockRoamingLink() {
		checkPageIsReady();
		try {
			checkPageIsReady();
			unBlockRoamingLink.click();
			Reporter.log("clicked on un block roaming link");
		} catch (Exception e) {
			Assert.fail("un block roaming link not displayed");
		}
		return this;
	}

	/**
	 * verify manage family allowances link
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyNoComapatableDataPassMessage() {
		checkPageIsReady();
		try {
			noComapatableDataPassMessage.isDisplayed();
			noComapatableDataPassMessage.getText()
					.contains("There are no datapasses compatable with this plan at this time.");
			Reporter.log("no compatable data pass message is displayed");
		} catch (Exception e) {
			Assert.fail("no compatable data pass message is not displayed");
		}
		return this;
	}

	/**
	 * verify if list of services containing NetFlix only
	 * 
	 * @return boolean
	 */
	public ManageAddOnsSelectionPage verifyIfListOfServiceContaingNetFlixOnly() {
		boolean isDisplayed = true;
		try {
			for (WebElement services : listOfService) {
				if (!services.getText().contains("Netflix") && !services.getText().contains("Nfx")) {
					isDisplayed = false;
					break;
				}
			}
			Assert.assertTrue(isDisplayed, "All services should contain only Netflix on Manage Addons Page");
			Reporter.log("All services contain only Netflix on Manage Addons Page");
		} catch (Exception e) {
			Assert.fail("All services are not Netflix Service SOC.");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyActiveNetflixPlanAndManageFAmilyAllowanceLink() {
		verifyActiveNetflixPlan();
		verifyAndClickOnManageFamilyAllowanceLink();
		return this;
	}

	public ManageAddOnsSelectionPage verifyActiveNetflixPlan() {

		int count = familySOCCodes.size();
		for (int element = 0; element < count; element++) {
			WebElement socName;
			socName = familySOCCodes.get(element);
			if (socName.getText().contains("Active")) {
				Reporter.log("Active Netflix SOC exist");
				moveToElement(familySOCDescriptionDownwardArrows.get(element));
				familySOCDescriptionDownwardArrows.get(element).click();
				break;
			}
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAndClickOnManageFamilyAllowanceLink() {
		checkPageIsReady();
		if (manageFamilyAloowancesLink.isDisplayed()) {
			Reporter.log("Manage Family Allowance link is displayed");
			manageFamilyAloowancesLink.click();
		} else
			Assert.fail("Manage Family Allowance link is not displayed");
		return this;
	}

	public ManageAddOnsSelectionPage verifyAndClickOnManageNetflixLink() {
		checkPageIsReady();
		if (manageNetflix.isDisplayed()) {
			Reporter.log("Manage Family Allowance link is displayed");
			manageNetflix.click();
		} else
			Assert.fail("Manage Family Allowance link is not displayed");
		return this;
	}

	public ManageAddOnsSelectionPage verifyRedirectionOfManageFamilyAllowanceLink() {
		try {
			switchToSecondWindowInMobile();
			String currentUrl = getDriver().getCurrentUrl();
			checkPageIsReady();
			Assert.assertTrue(currentUrl.contains("profile/family_controls/allowances/dashboard"),
					"Manage Family Allowance link not redirected to correct page");
			Reporter.log("Manage Family Allowance link redirected to correct page");
		} catch (Exception e) {
			Assert.fail("Manage Family Allowance link not redirected to correct page");
		}
		return this;
	}

	/**
	 * check whether Conflict Dialog displayed or not
	 */
	public ManageAddOnsSelectionPage checkForConflictModal() {
		checkPageIsReady();
		try {
			conflictDialogWindow.isDisplayed();
			Reporter.log("Conflict modal is displayed");
			conflictDialogContinue.get(0).click();
		} catch (Exception e) {
			Reporter.log("Conflict modal is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickCancelButton() {
		try {
			checkPageIsReady();
			conflictModalCancelBtn.click();
			Reporter.log("Cancel CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Cancel CTA is not clicked");
		}
		return this;
	}

	/**
	 * verify Warning Modal Removing NetFlix
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyPlanPopupModal() {
		checkPageIsReady();
		try {
			warmingModal.isDisplayed();
			Reporter.log("Plan pop up window is dispalyed");
		} catch (Exception e) {
			Assert.fail("Plan pop up window is not dispalyed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage VerifyManageAddOnSOCs(String sSoc) {
		checkPageIsReady();

		try {
			java.util.List<WebElement> elements = ManageAddonSocs;
			for (int i = 0; i < elements.size(); i++) {
				if (elements.get(i).getText().contains(sSoc)) {
					Reporter.log(elements.get(i).getText() + "is enabled");
				}
			}
		} catch (Exception e) {
			Assert.fail("Expected soc is not enabled");
		}
		return this;
	}

	public boolean verifyOnlyFamilyAndEntertainmentHeaderDispalyed() {
		boolean display = false;
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(familyandEntertainmentHeader));
			if (headersOnAddonsPage.size() == 1 && headersOnAddonsPage.get(0).getText().contains("Services")) {
				display = true;
			}
			Reporter.log("Family and Entertainment header only displayed");

		} catch (Exception e) {
			Reporter.log("All headers on Manage Addons Page are dispalyed");
			Assert.fail(" All headers on Manage Addons Page are dispalyed");
		}
		return display;
	}

	public ManageAddOnsSelectionPage verifyFamilyAndEntertainmentSocDispalyed(String familyAllowanceSoc) {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(verifyElementBytext(familyandEntertainmentSectionSocs, familyAllowanceSoc),
					"Family Allowance soc " + familyAllowanceSoc + "  is not displayed");
			Reporter.log("Family Allowance Soc " + familyAllowanceSoc + " is displayed");
		} catch (Exception e) {
			Assert.fail("Family Allowance soc" + familyAllowanceSoc + "  is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyFamilyAndEntertainmentSocNotDisplayed(String FamilyAllowanceSoc) {
		try {
			checkPageIsReady();
			Assert.assertFalse(verifyElementBytext(familyandEntertainmentSectionSocs, FamilyAllowanceSoc),
					"Family Allowance soc" + FamilyAllowanceSoc + "  is  displayed");
			Reporter.log("family Allowance soc" + FamilyAllowanceSoc + "  is not  displayed");

		} catch (Exception e) {
			Assert.fail("family Allowance soc" + FamilyAllowanceSoc + "  is  displayed");
		}
		return this;
	}

	/**
	 * verify Warning Modal Removing NetFlix
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyWarningModalRemovingNetFlixisDisplayed() {
		checkPageIsReady();
		try {
			for (WebElement element : warmingModalRemovingNetFlix) {
				if (element.isDisplayed()) {
					break;
				}
			}
			// warmingModalRemovingNetFlix.isDisplayed();
			Reporter.log("Warning modal removing netflix is dispalyed");
		} catch (Exception e) {
			Assert.fail("Warning modal removing netflix is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Warning message Removing NetFlix
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyWarningMsgRemovingNetFlixisDisplayed(String msg) {
		checkPageIsReady();
		try {
			// warmingMsgRemovingNetFlix.isDisplayed();
			for (WebElement element : warmingMsgRemovingNetFlix) {
				if (element.isDisplayed()) {
					Assert.assertTrue(element.getText().contains(msg), "Warining message is not same +msg+");
					break;
				}
			}
			// Assert.assertTrue(warmingMsgRemovingNetFlix.getText().contains(msg),
			// "warining message is not same");
			Reporter.log("Warning message removing netflix is dispalyed");
		} catch (Exception e) {
			Assert.fail("Warning message removing netflix is not dispalyed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickCancelBtn() {
		checkPageIsReady();
		try {
			cancelBtn.click();
			Reporter.log("Clicked on Cancel button");
		} catch (Exception e) {
			Assert.fail("Cancel button is not dispalyed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyCheckBoxSelectedByName(String checkBoxName) {
		checkPageIsReady();
		try {
			WebElement checkBoxStatus = getDriver().findElement(getNewLocator(checkBoxState, checkBoxName));
			Assert.assertTrue(checkBoxStatus.getAttribute("class").contains("ng-touched"));
			Reporter.log(checkBoxName + "is selected");
		} catch (Exception e) {
			Assert.fail(checkBoxName + "is not selected");
		}
		return this;
	}

	/**
	 * verify Warning Modal for Removing PHP/PDP SOC
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyWarningModalRemovingPDPSOCIsDisplayed() {
		try {
			Verify.assertTrue(warmingModalRemovingPDPSoc.isDisplayed());
			Reporter.log("warning modal removing PDP SOC is dispalyed");
		} catch (Exception e) {
			Assert.fail("warning modal removing PDP SOC is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Warning message Removing PDP/PHP
	 * 
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyWarningMsgRemovingPDPSOCisDisplayed(String msg) {
		try {
			String warningModalHeader;
			String warningModalMessage;
			warmingModalHeader.isDisplayed();
			warningModalHeader = warmingModalHeader.getText();
			Assert.assertEquals(warningModalHeader, "Warning", "Header text of PDP/PHP modal is mismatched");
			warningModalMessage = warmingMsgRemovingPDPPHP.getText();
			Assert.assertEquals(warningModalMessage, msg, "Text of PDP/PHP modal is mismatched");
			Reporter.log("warning message removing netflix is dispalyed");
		} catch (Exception e) {
			Assert.fail("warning message removing netflix is not dispalyed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickCancelBtnOfRemoveWarningOfPHPSoc() {
		try {
			cancelCTAForRemovalOfPHPSOC.click();
			Reporter.log("clicked on cancel button");
		} catch (Exception e) {
			Assert.fail("cancel button is not dispalyed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage getAndHitURLWithOnlyOneEligibleSOC() {
		String getCurrentURL, url[], odfURL;
		getCurrentURL = getDriver().getCurrentUrl();
		url = getCurrentURL.split("home");
		odfURL = url[0] + "odf/DataService:NODATA,LTDATA";
		getDriver().get(odfURL);
		verifymanageDataAndAddOnsPage();
		return this;
	}

	public ManageAddOnsSelectionPage checkWhetherSOCIsExpandedOrNot() {
		try {
			expandedArrowForDescriptionOfSOC.isDisplayed();
			Reporter.log("Description of SOC is expanded");
		} catch (Exception e) {
			Assert.fail("SOC is not expanded");
		}
		return this;
	}

	public ManageAddOnsSelectionPage checkWhetherDescriptionOfSOCIsDisplayedOrNot() {
		try {
			descriptionOfSOC.isDisplayed();
			Reporter.log("Description of SOC is dispalyed");
		} catch (Exception e) {
			Assert.fail("Description of SOC is not expanded");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAddOnPageWhenOnlyOneSOCEligible() {
		getAndHitURLWithOnlyOneEligibleSOC();
		verifymanageDataAndAddOnsPage();
		checkWhetherSOCIsExpandedOrNot();
		checkWhetherDescriptionOfSOCIsDisplayedOrNot();
		return this;
	}

	public ManageAddOnsSelectionPage verifyErrorMessageForIneligibleSOC() {
		try {
			messageForIneligibleSOC.isDisplayed();
			Reporter.log("Error Message is dispalyed");
		} catch (Exception e) {
			Assert.fail("Error Message is not dispalyed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage checkErrorMessageForIneligibleSOCsInURL() {
		try {
			String actualText;
			actualText = messageForIneligibleSOC.getText();
			Assert.assertEquals(actualText,
					"This feature is no longer available. Check out other available add-ons to amp up your account!",
					"InEligible message mismatched");
			Reporter.log("InEligible message matched");
		} catch (Exception e) {
			Assert.fail("InEligible message mismatched");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyAddOnPageWhenUserNotEligibleForAnySOC() {

		verifymanageDataAndAddOnsPage();
		verifyErrorMessageForIneligibleSOC();
		checkErrorMessageForIneligibleSOCsInURL();
		verifyContinueBtnIsEnabled();
		clickOnContinueBtn();
		return this;
	}

	public boolean verifyWhetherChangeDateLinkIsDisplayedOrNot() {
		boolean changeDateLinkStatus = false;
		try {
			Assert.assertTrue(changeDateLink.isDisplayed());
			Reporter.log("Change Date link is displayed.");
			changeDateLinkStatus = true;
		} catch (Exception e) {
			Reporter.log("Change Date link is not displayed.");
		}
		return changeDateLinkStatus;
	}

	public boolean verifyWhetherRemoveLinkIsDisplayedOrNot() {
		boolean removeDataPassLinkStatus = false;
		try {
			Assert.assertTrue(removeDataPassLink.get(0).isDisplayed());
			Reporter.log("Remove link is displayed.");
			removeDataPassLinkStatus = true;
		} catch (Exception e) {
			Reporter.log("Remove link is not displayed.");
		}
		return removeDataPassLinkStatus;
	}

	public ManageAddOnsSelectionPage verifyWhetherPipeCharacterIsDisplayedOrNot() {
		try {
			Assert.assertTrue(pipeCharacter.isDisplayed(), "Pipe Character is not displayed");
			Reporter.log("Pipe character is displayed.");
		} catch (Exception e) {
			Reporter.log("Pipe Character is not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage verifyDataPassLinksStatus() {
		boolean changeDateLinkStatus = verifyWhetherChangeDateLinkIsDisplayedOrNot();
		boolean removeDataPassLinkStatus = verifyWhetherRemoveLinkIsDisplayedOrNot();

		if (changeDateLinkStatus)
			Reporter.log("Change Date link displayed on Add On Selection page for Future Dated Pass");
		else
			Assert.fail("Change Date link not displayed on Add On Selection page for Future Dated Pass");

		if (removeDataPassLinkStatus)
			Reporter.log("Remove data pass link displayed on Add On Selection page for Future Dated Pass");
		else
			Assert.fail("Remove Data pass link not displayed on Add On Selection page for Future Dated Pass");
		return this;
	}

	public ManageAddOnsSelectionPage verifyNonRemovableSoc() {
		try {
			Assert.assertTrue(nonremovable.getAttribute("disabled").contains("true"));
			Reporter.log("Non Removable SOC Chech box is disabled");
		} catch (Exception e) {
			Assert.fail("Non Removable SOC Chech box is not disabled");
		}
		return this;
	}

	public boolean removeNetflixFA16Soc(String netFlixSocName) {
		return removeNetflixSocByName(activeServiceCheckBox, netFlixSocName);
	}

	public ManageAddOnsSelectionPage getManageAddonswithNetflixServices() {

		String currentURL[] = getDriver().getCurrentUrl().split("com");
		currentURL[0] = currentURL[0]
				+ "com/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP";
		getDriver().get(currentURL[0]);
		verifymanageDataAndAddOnsPage();

		return this;
	}

	public ManageAddOnsSelectionPage verifyUpgradeDowgradeFunctionality(String beforeNetFlixSocName,
			String afterNetFlixSocName) {
		if (activeWebElement.getText().contains(beforeNetFlixSocName)) {
			upgradeNetFlixSoc(beforeNetFlixSocName, afterNetFlixSocName);
			ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			manageAddOnsConfirmationPage.verifyODFConfirmationPage();
			manageAddOnsConfirmationPage.verifyOrderDetailsDisplayed();
			manageAddOnsConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
			manageAddOnsConfirmationPage.clickOnOrderDetailsBlade();
			manageAddOnsConfirmationPage.verifyUpgradeNetflixDetailsOnOrderDetails(beforeNetFlixSocName,
					afterNetFlixSocName);
			manageAddOnsConfirmationPage.verifyBackArrowIsDisabledOnConfirmationPage();
			manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();
			CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
			crazylegspage.verifyBenifitsPage();
			getManageAddonswithNetflixServices();
			downGradeNetFlixSoc(afterNetFlixSocName, beforeNetFlixSocName);
			manageAddOnsConfirmationPage.verifyODFConfirmationPage();
			manageAddOnsConfirmationPage.verifyOrderDetailsDisplayed();
			manageAddOnsConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
			manageAddOnsConfirmationPage.clickOnOrderDetailsBlade();
			manageAddOnsConfirmationPage.verifyDowngradeNetflixDetailsOnOrderDetails(afterNetFlixSocName,
					beforeNetFlixSocName);
			manageAddOnsConfirmationPage.verifyBackArrowIsDisabledOnConfirmationPage();
			manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();
			crazylegspage.verifyBenifitsPage();

		} else if (activeWebElement.getText().contains(afterNetFlixSocName)) {
			downGradeNetFlixSoc(afterNetFlixSocName, beforeNetFlixSocName);
			ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			manageAddOnsConfirmationPage.verifyODFConfirmationPage();
			manageAddOnsConfirmationPage.verifyOrderDetailsDisplayed();
			manageAddOnsConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
			manageAddOnsConfirmationPage.clickOnOrderDetailsBlade();
			manageAddOnsConfirmationPage.verifyDowngradeNetflixDetailsOnOrderDetails(afterNetFlixSocName,
					beforeNetFlixSocName);
			manageAddOnsConfirmationPage.verifyBackArrowIsDisabledOnConfirmationPage();
			manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();
			CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
			crazylegspage.verifyBenifitsPage();
			getManageAddonswithNetflixServices();
			upgradeNetFlixSoc(beforeNetFlixSocName, afterNetFlixSocName);
			manageAddOnsConfirmationPage.verifyODFConfirmationPage();
			manageAddOnsConfirmationPage.verifyOrderDetailsDisplayed();
			manageAddOnsConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
			manageAddOnsConfirmationPage.clickOnOrderDetailsBlade();
			manageAddOnsConfirmationPage.verifyUpgradeNetflixDetailsOnOrderDetails(beforeNetFlixSocName,
					afterNetFlixSocName);
			manageAddOnsConfirmationPage.verifyBackArrowIsDisabledOnConfirmationPage();
			manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();
			crazylegspage.verifyBenifitsPage();
		} else {
			Reporter.log("Unable to upgrade and downgrade because " + beforeNetFlixSocName + "and "
					+ afterNetFlixSocName + "is not selected");
		}
		return this;
	}

	public ManageAddOnsSelectionPage upgradeNetFlixSoc(String beforeNetFlixSocName, String afterNetFlixSocName) {
		if (getDriver().findElement(getNewLocator(activeService, beforeNetFlixSocName)).isDisplayed()) {
			getDriver().findElement(getNewLocator(checkBoxName, afterNetFlixSocName)).click();
			verifyWarningMsgRemovingNetFlixisDisplayed(
					"There is a conflict between the service you are adding and another active on your account.");
			verifyContinueRemovalBtnisDisplayed();
			clickOnContinueBtn();
			ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
			oDFDataPassReviewPage.verifyReviewPageOfManageDataAndAddOnsPage();
			oDFDataPassReviewPage.verifyUpgradeNewAndRemovedAddOns(beforeNetFlixSocName, afterNetFlixSocName);
			oDFDataPassReviewPage.agreeAndSubmitButton();
		} else {
			Reporter.log("NefFlix Soc " + beforeNetFlixSocName + " is not selected. Hence Upgrade NetFlix Soc to "
					+ afterNetFlixSocName + " is not possible.");
		}
		return this;
	}

	public ManageAddOnsSelectionPage downGradeNetFlixSoc(String beforeNetFlixSocName, String afterNetFlixSocName) {
		if (getDriver().findElement(getNewLocator(activeService, beforeNetFlixSocName)).isDisplayed()) {
			getDriver().findElement(getNewLocator(checkBoxName, afterNetFlixSocName)).click();
			;
			verifyWarningMsgRemovingNetFlixisDisplayed(
					"There is a conflict between the service you are adding and another active on your account.");
			verifyContinueRemovalBtnisDisplayed();
			clickOnContinueBtn();
			ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
			oDFDataPassReviewPage.verifyReviewPageOfManageDataAndAddOnsPage();
			oDFDataPassReviewPage.verifyDowngradeNewAndRemovedAddOns(beforeNetFlixSocName, afterNetFlixSocName);
			oDFDataPassReviewPage.agreeAndSubmitButton();
		} else {
			Reporter.log("NefFlix Soc " + beforeNetFlixSocName + " is not selected. Hence Upgrade NetFlix Soc to "
					+ afterNetFlixSocName + " is not possible.");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickdowncarotOfAddedNetflix() {
		try {
			downArrow.click();
			Reporter.log("Clicked on carrot icon ");

		} catch (Exception e) {
			Assert.fail("Click on carrot icon failed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage clickdowncarotOfAddedNetflix(String netflix) {
		try {
			getDriver().findElement(getNewLocator(downcarrot, netflix)).click();
			Reporter.log("Clicked on carrot icon ");

		} catch (Exception e) {
			Assert.fail("Click on carrot icon failed");
		}
		return this;
	}

	/**
	 * Netflix price
	 */
	public ManageAddOnsSelectionPage verifypriceforNetflixSoc(String netflix, String netflixprice) {
		try {
			Assert.assertTrue(getDriver().findElement(getNewLocator(price, netflix)).getText().contains(netflixprice),
					netflix + "price is not displayed");
			Reporter.log(netflix + "price is displayed");
		} catch (Exception e) {
			Assert.fail(netflix + "price is not displayed");
		}
		return this;
	}

	/**
	 * Netflix price
	 */
	public ManageAddOnsSelectionPage verifyNetflixServicesSize(int netf) {

		try {
			Assert.assertTrue(familyandEntertainmentSectionSocs.size() == netf,
					"Family and Entertainment Section Socs are not displayed");
			Reporter.log(familyandEntertainmentSectionSocs.size() + "is displayed");
		} catch (Exception e) {
			Assert.fail(familyandEntertainmentSectionSocs.size() + "is not displayed");
		}
		return this;
	}

	/*
	 * Check message for Standard/Restricted users.
	 */

	public ManageAddOnsSelectionPage checkMessageForStandardOrRestrictedUsers() {
		String message = "Looking to make additional changes? Please reach out to your primary account holder.";
		try {
			headerSuspensionMessage.isDisplayed();
			Reporter.log(message + " is  displayed");
			Assert.assertEquals(headerSuspensionMessage.getText().trim(), message,
					"Message for non permissioned user is mismatched on Addons Selection page");
		} catch (Exception e) {
			Assert.fail(message + " is not displayed");
		}
		return this;
	}

	// Get future dated service names
	public String getFutureDatedServiceNames() {
		String fdService = null;
		String futureDatedService = null;
		try {
			// for(WebElement webelement:listOfFutureDatedService)
			for (int i = 0; i <= listOfFutureDatedService.size(); i++) {
				String serviceName = listOfFutureDatedService.get(i).getText();
				if (serviceName.contains("Active")) {
					fdService = serviceName;
					break;
				}
			}

			String activeService[];
			activeService = fdService.split("Active");
			futureDatedService = activeService[0].trim();

		} catch (Exception e) {
			Assert.fail("Future dated service is not displayed");
		}
		return futureDatedService;
	}

	// Click on Select Different line link
	public ManageAddOnsSelectionPage clickOnSelectDifferentLineLink() {

		try {
			selectDifferentLineLink.isDisplayed();
			selectDifferentLineLink.click();
			Reporter.log("Select Different Line link is clicked");
		} catch (Exception e) {
			Reporter.log("Select Different line is not displayed");
		}
		return this;
	}

	/**
	 * Verify Line details page.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public ManageAddOnsSelectionPage verifyLineSelectorPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementText(headerOfLineSelectorPage, "Select a line to Manage"),
					"Line Selector page is loaded");
			Reporter.log("Line Selector page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Line Selector page not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage addOrRemoveNetflixSocs(String beforeNetFlixSocName, String afterNetFlixSocName) {
		if (activeWebElement.getText().contains(beforeNetFlixSocName)) {
			Reporter.log("Upgrading from" + beforeNetFlixSocName + "to " + afterNetFlixSocName);
			upgradeNetFlixSoc(beforeNetFlixSocName, afterNetFlixSocName);
			ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			manageAddOnsConfirmationPage.verifyODFConfirmationPage();
			manageAddOnsConfirmationPage.verifySecondHeaderOnConfirmationPage("You've Added Netflix");
			manageAddOnsConfirmationPage.verifytextMessage();
			manageAddOnsConfirmationPage.verifyGoToPlanBenefits();

		} else if (activeWebElement.getText().contains(afterNetFlixSocName)) {
			Reporter.log("downgrading from" + afterNetFlixSocName + "to " + beforeNetFlixSocName);
			downGradeNetFlixSoc(afterNetFlixSocName, beforeNetFlixSocName);
			ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			manageAddOnsConfirmationPage.verifyODFConfirmationPage();
			manageAddOnsConfirmationPage.verifySecondHeaderOnConfirmationPage("You've Added Netflix");
			manageAddOnsConfirmationPage.verifytextMessage();
			manageAddOnsConfirmationPage.verifyGoToPlanBenefits();
		} else {
			Reporter.log("Unable to add or remove the Netflix because " + beforeNetFlixSocName + "and "
					+ afterNetFlixSocName + "is not selected or not appear");
		}
		return this;
	}

	public boolean verifyServiceCheckBoxByNameIsSelectedOrUnselected(String name) {
		boolean selectionStatus = false;
		try {
			checkPageIsReady();
			WebElement blockContent = getDriver().findElement(getNewLocator(checkBoxByName, name));
			scrollToElement(blockContent);
			selectionStatus = blockContent.isSelected();
			Reporter.log(" " + name + " is selected");
		} catch (Exception e) {
			Assert.fail("" + name + " is Not displayed");
		}
		return selectionStatus;
	}

	public ManageAddOnsSelectionPage selectOrUnselectService(String name) {
		boolean selectionStatus = false;
		try {
			selectionStatus = verifyServiceCheckBoxByNameIsSelectedOrUnselected(name);
			if (selectionStatus == true) {
				unCheckCheckBox(name);
				Reporter.log(" " + name + " is unselected");
			} else {
				clickOnCheckBox(name);
				Reporter.log(" " + name + " is selected");
			}
		} catch (Exception e) {
			Assert.fail("" + name + " is Not displayed");
		}
		return this;
	}

	public ManageAddOnsSelectionPage checkWhetherFutureDatedServiceUnselectedOrNot(String name) {
		boolean selectionStatus = false;
		try {
			selectionStatus = verifyServiceCheckBoxByNameIsSelectedOrUnselected(name);
			if (selectionStatus == true) {
				Assert.fail("Service " + name + " is still selected even after user submitted transaction");
			} else
				Reporter.log(" " + name + " is unselected");
		} catch (Exception e) {
			Assert.fail("" + name + " is Not displayed");
		}
		return this;
	}

	/*
	 * Check radio buttons of Data Passes are displayed or not for
	 * Standard/Restricted users.
	 */

	public ManageAddOnsSelectionPage checkRadioButtonsForDataPassesAreDisplayedOrNotForStandardOrRestrictedUsers() {
		try {
			dataPassRadioButtons.isDisplayed();
			Reporter.log(" Radio buttons for Data Pass for Non Permissioned user is displayed");
		} catch (Exception e) {
			Assert.fail("Radio buttons for Data Pass for Non Permissioned user is not displayed");
		}
		return this;
	}

	/*
	 * Check radio buttons of Data Passes are displayed or not for
	 * Standard/Restricted users.
	 */

	public ManageAddOnsSelectionPage checkRadioButtonsForDataPassesForStandardOrRestrictedUsersWhenAccountHasPendingChanges() {
		try {
			dataPassRadioButtons.isDisplayed();
			Assert.fail("Radio buttons for Data Pass for Non Permissioned user are displayed");
		} catch (Exception e) {
			Reporter.log(" Radio buttons for Data Pass for Non Permissioned user is not displayed");
		}
		return this;
	}

	/**
	 * Verify Conflict Dialog continue is displayed
	 */
	public ManageAddOnsSelectionPage verifyWereConflictedContinueBtn() {
		checkPageIsReady();
		try {
			if (!continueRemoval.isEmpty()) {

				for (WebElement webElement : continueRemoval) {
					if (webElement.isEnabled()) {
						webElement.click();
					}
				}
				continueRemoval.get(1).isEnabled();
			}
			Reporter.log("Clicked on Conflicted Continue button");
		} catch (Exception e) {
			Assert.fail("Conflicted Continue button is not dispalyed");
		}
		return this;
	}

	/**
	 * Click On Select A Different Line Link
	 */
	public ManageAddOnsSelectionPage clickOnSelectADifferentLineLink() {
		checkPageIsReady();
		try {
			selectADifferentLineLink.click();
			Reporter.log("Clicked on select A Different Line Link");
		} catch (Exception e) {
			Assert.fail("Select A Different Line Link is not dispalyed");
		}
		return this;
	}

	/**
	 * Click On Select A Different Line Link
	 * 
	 * @param enclosingValue
	 */
	public ManageAddOnsSelectionPage checkForMostPopularLabelOnlyFor2Screen(String enclosingValue) {
		checkPageIsReady();
		try {
			mostPopularLabel.getText();
			WebElement parentElement = mostPopularLabel.findElement(By.xpath(".."));
			Assert.assertTrue(parentElement.getText().contains(enclosingValue));
			Reporter.log("check for the mostPopular label");
		} catch (Exception e) {
			Assert.fail("Not able to find mostPopular label");
		}
		return this;
	}

	/**
	 * Click On Select A Different Line Link
	 */
	public ManageAddOnsSelectionPage removedAddedDataPass() {
		checkPageIsReady();
		try {
			if (!CollectionUtils.isEmpty(removeDataPassLink) && CollectionUtils.isNotEmpty(removeDataPassLink)) {
				for (WebElement removeLink : removeDataPassLink) {
					waitforSpinner();
					removeLink.click();
					verifyRemoveDataPassOverlayAndRemoveIt();
				}
				Reporter.log("Removed Data pass List");
			}
		} catch (Exception e) {
			Assert.fail("Data pass List is not removed");
		}
		return this;
	}

	/**
	 * Click On Select A Different Line Link
	 */
	public ManageAddOnsSelectionPage verifyRemoveDataPassOverlayAndRemoveIt() {
		checkPageIsReady();
		try {
			verifyUnblockRoamingAndUnblockIt();
			if (!CollectionUtils.isEmpty(removeDataPassOverlay) && CollectionUtils.isNotEmpty(removeDataPassOverlay)) {
				verifyContinueRemovalBtnisDisplayed();
				waitforSpinner();
			}
			Reporter.log("Removed Data pass");
		} catch (Exception e) {
			Assert.fail("Data pass is not Removed");
		}
		return this;
	}

	/**
	 * Verifying hotspot and Data extras is displayed \
	 */
	public ManageAddOnsSelectionPage verifyHotspotAndDataExtrasSection() {
		try {
			waitFor(ExpectedConditions.visibilityOf(hotspotAndDataExtras));
			Verify.assertTrue(hotspotAndDataExtras.isDisplayed());
			Reporter.log("hotspot and Data Extras should displayed.");
		} catch (Exception e) {
			Assert.fail("hotspot and Data Extras should not displayed.");
		}
		return this;
	}

	/**
	 * Check for tethering soc is comming under hotspot and Data extras
	 * 
	 * @param enclosingValue
	 */
	public ManageAddOnsSelectionPage checkForTetheringSocIsUnderHotspotAndDataExtras(String enclosingValue) {
		checkPageIsReady();
		try {
			for (WebElement element : listOfDataExtras) {
				String tetheringSoc = element.getText();
					if (tetheringSoc.contains("10GB of high-speed mobile hotspot data")) {
						Reporter.log( " Tethering soc is displayed under hotspot");
					} else {
						Assert.assertTrue(!listOfDataExtras.isEmpty());
						Reporter.log("Tethering soc is not displayed under hotspot");
					}
					break;
				}
		} catch (Exception e) {
			Assert.fail( " Hotspot and Data extras is empty and tethering soc is not displayed");
		}
		return this;
	}

	/**
	 * verify no data plan is selected
	 *
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyNoDataPlanIsSelected() {
		checkPageIsReady();
		try {
			monthlyDataplan.isDisplayed();
			monthlyDataplan.getText().contains("You currently have No Data");
			Assert.assertTrue(monthlyDataplan.getText().contains("You currently have No Data"));
			Reporter.log("No Data plan is selected");
		} catch (Exception e) {
			Assert.fail("no Data plan is not selected");
		}
		return this;
	}

	/**
	 * verify the conflict model on selecting the No Data
	 *
	 * @return
	 */
	public ManageAddOnsSelectionPage verifyConflictModelOnSelectingNoData() {
		try {
			waitFor(ExpectedConditions.visibilityOf(noDataRadioBtn));
			noDataRadioBtn.click();
			checkForConflictModal();
			Reporter.log("No Data is clicked");
		} catch (Exception e) {
			Assert.fail("No Data is not clicked and conflict modal is not shown up");
		}
		return this;
	}

	/**
	 * Verifying hotspot and Data extras is displayed \
	 */
	public ManageAddOnsSelectionPage verifyHotspotAndDataExtrasSectionisNotPresent() {
		try {
			for (WebElement element : DataSections) {
				String tetheringSoc = element.getText();
				System.out.println("tetheringSoc");
				if (tetheringSoc.contains("10GB of high-speed mobile hotspot data")) {
					Reporter.log( " Tethering soc is displayed under hotspot");
				} else {
					Assert.assertTrue(!listOfDataExtras.isEmpty());
					Reporter.log("Tethering soc is not displayed under hotspot");
				}
				break;
			}
		} catch (Exception e) {
			Assert.fail( " Hotspot and Data extras is empty and tethering soc is not displayed");
		}
		return this;
	}
}
