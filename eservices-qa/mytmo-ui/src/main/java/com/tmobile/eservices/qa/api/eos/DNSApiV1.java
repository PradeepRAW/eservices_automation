package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class DNSApiV1 extends ApiCommonLib {

	/***
	 * Gets/Updates all the DNS Settings info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response setDNSSettings(ApiTestData apiTestData, String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildDNSSettingsHeader(apiTestData);
		String resourceURL = "api/dns/setsettings";
		String accessToken=checkAndGetAccessToken();
		tokenMap.put("accessToken", accessToken);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "setDNSSettings");
    	Response response = invokeRestServiceCall(headers, "https://tos-flex-dev.dd-stg.kube.t-mobile.com", resourceURL, updatedRequest ,RestServiceCallType.POST);
		return response;
	}
	
	public Response getDNSSettings(ApiTestData apiTestData, String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildDNSSettingsHeader(apiTestData);
		String accessToken=checkAndGetAccessToken();
		Map<String, String> jwtTokens=checkAndGetJWTToken(apiTestData, tokenMap);
		tokenMap.put("accessToken", accessToken);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "getDNSSettings");
		String resourceURL = "api/dns/getsettings";
    	Response response = invokeRestServiceCall(headers, "https://tos-flex-dev.dd-stg.kube.t-mobile.com", resourceURL, updatedRequest ,RestServiceCallType.POST);
		return response;
	}
	private Map<String, String> buildDNSSettingsHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", apiTestData.getPopToken());
		headers.put("Content-Type", "application/json");
		headers.put("activityid", "ADJH12KLJ");
		return headers;
	}
}
