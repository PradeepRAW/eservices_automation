package com.tmobile.eservices.qa.api;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;

import com.tmobile.eservices.qa.api.unlockdata.Constants;
import com.tmobile.eservices.qa.api.unlockdata.SoapRequestData;
import com.tmobile.eservices.qa.api.unlockdata.ServiceException;
import com.tmobile.eservices.qa.api.unlockdata.SOAPServiceImpl;
import com.tmobile.eservices.qa.api.unlockdata.CommonUtils;

/**
 * @author blakshminarayana
 * This class handled all the soap requests and responses 
 *
 */
public class SOAPServiceHelper {

	private static final Logger logger =LoggerFactory.getLogger(SOAPServiceHelper.class);

	/**
	 * This method is used to get authorization token.
	 * @param requestData
	 * @return authorizationToken
	 */
	public String getCustomerToken(SoapRequestData requestData) {
		logger.info("called getAthorizationtoken method in SOAPHandlerHelper");
		String token = null;
		try {
			InputStream inputStream = CommonUtils.getAuthorizationFile();
			token = appendDataToMessage(requestData,inputStream,false);
		} catch (SOAPException ex) {
			logger.error("Not able to get authorizationtoken failed{}",ex);
			throw new ServiceException("Not able to get the customer token from service", ex);
		}

		return token;
	}

	/** This is common method to get the token irrespective of service
	 * @param requestData
	 * @param fullfillmentOrnot
	 * @param inputStream
	 * @return token
	 * @throws SOAPException
	 */
	private String appendDataToMessage(SoapRequestData requestData,InputStream inputStream,Boolean fullfillmentOrnot) throws SOAPException {
		SOAPMessage response=null;
		SOAPMessage request=null;
		String token=null;
		try {
			String macID = requestData.getMacID();
			String storeID = requestData.getStoreID();
			String userName = requestData.getUserName();
			String ban = requestData.getBanNumber();
			String msisdn = requestData.getSubscriberNumber();
			String environment = requestData.getEnvironment();
			Document document = fileToDoc(inputStream);
			SOAPServiceImpl handler = new SOAPServiceImpl();
			Element userDataElement = getElementByTagName(document, "userData");
			if(userDataElement != null && userDataElement.getText() != null){
				String userData = userDataElement.getText();
				String[] data = userData.split("\\|");
				if(StringUtils.isNotEmpty(userName)){
					data[1] = userName; // new userName
				}
				if(StringUtils.isNotEmpty(macID)){
					data[2] = macID; // new macid
				}
				if(StringUtils.isNotEmpty(storeID)){
					data[3] = storeID; // new store id
				}
				if(StringUtils.isNotEmpty(ban)){
					data[8] = ban; // new ban
				}
				if(StringUtils.isNotEmpty(msisdn)){
					data[9] = msisdn; // new msisdn
				}
				if(fullfillmentOrnot){
					data[11] = msisdn;
				}
				String appendedData = org.apache.commons.exec.util.StringUtils.toString(data, "|");
				userDataElement.setText(appendedData);
				logger.info("replaced  soap user data with from excel{}",appendedData);
			}
			request = handler.createRequest(document, Constants.GENERATE_SOAP_ACTION);
			logger.info("constructed soap request {}",request.getSOAPBody());
			if(StringUtils.isNotEmpty(environment)){
				String endpoint = CommonUtils.getTokenEndPointUrl(environment);
				response = handler.invokeService(endpoint, request); 
				logger.info("getting soap response from service {}",response.getSOAPBody());

			}
			token = getResponse(response,Constants.AUTHORIZATION_RESPONE_TOKEN);
			logger.info("received token from service{}",token);
		} catch (DOMException ex) {
			logger.error("Error while calling sopa service{}",ex);
			throw new ServiceException("Not able to get the token from service", ex);
		} 
		return token;
	}
	/**This method is used to get agency token.
	 * @param requestData
	 * @return authorizationToken
	 */

	public String getAgencyModelToken(SoapRequestData requestData) {
		String token = null;
		try {
			InputStream inputStream = CommonUtils.getAgencyModelFile();
			token = appendDataToMessage(requestData,inputStream,false);
		} catch (SOAPException ex) {
			logger.error("Not able to get agencytoken failed{} ",ex);
			throw new ServiceException("Not able to get the agencyToken from service", ex);
		}

		return token;
	}
	/**This method is used to get nonAgency token.
	 * @param requestData
	 * @return authorizationToken
	 */

	public String getNonAgencyModelToken(SoapRequestData requestData) {
		String token = null;
		try {
			InputStream inputStream = CommonUtils.getNonAgencyModelFile();
			token = appendDataToMessage(requestData,inputStream,false);
		} catch (SOAPException ex) {
			logger.error("Not able to get non agencytoken failed {}",ex);
			throw new ServiceException("Not able to get the nonagency token from service", ex);
		}

		return token;
	}
	/**This method is used to get coverage selection token.
	 * @param requestData
	 * @return authorizationToken
	 */

	public String getCoverageSelectionToken(SoapRequestData requestData) {
		String token = null;
		try {
			InputStream inputStream = CommonUtils.getCoverageSelectionFile();
			token = appendDataToMessage(requestData,inputStream,false);
		} catch (SOAPException ex) {
			logger.error("Not able to get coverageselectionToken failed{}",ex);
			throw new ServiceException("Not able to get the coverage selection token from service", ex);
		}
		return token;
	}

	/**This method is used to get coverage device token.
	 * @param requestData
	 * @return authorizationToken
	 */

	public String getCoverageDeviceDashboardToken(SoapRequestData requestData) {
		String token = null;
		try {
			InputStream inputStream = CommonUtils.getCoverageDashBoardFile();
			token = appendDataToMessage(requestData,inputStream,false);
		} catch (SOAPException ex) {
			logger.error("Not able to get coverageDeviceToken failed{}",ex);
			throw new ServiceException("Not able to get the coverage dashboard token from service", ex);
		}

		return token;
	}




	/**
	 * @param response
	 * @throws SOAPException 
	 * @Return responseText
	 */
	private static String getResponse(SOAPMessage messageResponse,String tagName) throws SOAPException {
		return messageResponse.getSOAPBody().getElementsByTagName(tagName).item(0).getTextContent();

	}

	/** This method is used to convert the file to document
	 * @param inputStream
	 * @return Document
	 */
	private Document fileToDoc(InputStream inputStream) {
		Document document=null;
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			document = saxBuilder.build(inputStream);
		} catch (JDOMException |IOException ex) {
			logger.error("Error while converting stream to document",ex);
			throw new ServiceException("exception araised while converting file stream to document",ex);
		} 
		return document;
	}

	/**This method is used to get the respective element by tag name 
	 * EX:userData,paymentaccount
	 * @param document
	 * @param tagName
	 * @return element
	 */
	private Element getElementByTagName(Document document, String tagName) {
		ElementFilter filter = new ElementFilter(tagName);
		for (Element element : document.getRootElement().getDescendants(filter)) {
			return element;
		}
		return null;

	}
	/**
	 * This method is used to get the token for exchange warranty
	 * @param requestData
	 * @return authorizationToken
	 */
	public String getWarrantyFulfillmentToken(SoapRequestData requestData) {
		logger.info("called getWarrantyFulfillmentToken method in SOAPHandlerHelper");
		String token = null;
		try {
			InputStream inputStream = CommonUtils.getExchangeWarrantyAndFullfillmentFile();
			token = appendDataToMessage(requestData,inputStream,true);
		} catch (SOAPException ex) {
			logger.error("Error while getting warantyFulfillmentToken from sopa service{}",ex);
			throw new ServiceException("Not able to get the warrantyfulfillment token from service", ex);
		} 
		return token;
	} 
	/**
	 * This method is used to get the token for exchange warranty
	 * @param requestData
	 * @return authorizationToken
	 */
	public String getWarrantyInitiationToken(SoapRequestData requestData) {
		logger.info("called getWarrantyInitiationToken method in SOAPHandlerHelper");
		String token = null;
		try {
			InputStream inputStream = CommonUtils.getExchangeWarrantyInitiationFile();
			token = appendDataToMessage(requestData,inputStream,false);
		} catch (SOAPException ex) {
			logger.error("Error while getting warantyInitiationToken from sopa service{}",ex);
			throw new ServiceException("Not able to get the authorization token from service", ex);
		} 
		return token;
	} 
}
