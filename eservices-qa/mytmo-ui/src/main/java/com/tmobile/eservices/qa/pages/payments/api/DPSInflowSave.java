package com.tmobile.eservices.qa.pages.payments.api;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class DPSInflowSave extends ApiCommonLib {
	
	/***
	 * Summary: This API is used do the payments  through OrchestrateOrders API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-interactionId
	 * 	-workflowId
	 * @param tokenMap 
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	
	private Map<String, String> buildDpsInflowSaveHeaders(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept","application/json");
		headers.put("Content-Type","application/json");
		headers.put("activityId","12345");
		return headers;
	}
	
	public Response dpsInflowSaveNonSedona(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildDpsInflowSaveHeaders(apiTestData);
		headers.put("Authorization", tokenMap.get("token"));
		headers.put("ban", tokenMap.get("ban"));
		headers.put("channel_id","WEB");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/process", RestCallType.POST);
		System.out.println(response.statusCode());
		return response;
	}
	
	public Response dpsInflowSaveAutopay(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildDpsInflowSaveHeaders(apiTestData);
		headers.put("Authorization", tokenMap.get("token"));
		headers.put("ban", tokenMap.get("ban"));
		headers.put("channel_id","B2B_WEB");
		headers.put("application_id","TFB");
		headers.put("application_userid","123");
		
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/autopay", RestCallType.POST);
		System.out.println(response.statusCode());
		
		return response;
	}

	public Response dpsInflowSavePA(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildDpsInflowSaveHeaders(apiTestData);
		headers.put("Authorization", tokenMap.get("token"));
		
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/pa", RestCallType.POST);
		System.out.println(response.statusCode());
		
		return response;
	}

	public Response dpsInflowSaveEIP(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildDpsInflowSaveHeaders(apiTestData);
		headers.put("Authorization", tokenMap.get("token"));
		headers.put("ban", tokenMap.get("ban"));
		headers.put("channel_id","WEB");
		headers.put("application_id","STREAMLINE");
		headers.put("interactionId","123456787");
		
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/eip", RestCallType.POST);
		System.out.println(response.statusCode());
		return response;
	}
}
