/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class OfferSearchPage extends CommonPage {

	@FindBy(css = "div.span12 h2")
	private WebElement offerSearchPage;

	public OfferSearchPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Offer Search Page
	 * 
	 * @return
	 */
	public OfferSearchPage verifyOrderSearchPage() {
		checkPageIsReady();
		try {
			getDriver().getCurrentUrl().contains("OrderSearch");
			Reporter.log("Order search page is displayed");
		} catch (Exception e) {
			Assert.fail("Order search page is not displayed");
		}
		return this;
	}

}
