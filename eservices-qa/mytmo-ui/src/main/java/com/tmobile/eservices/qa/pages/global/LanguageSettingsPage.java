/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author csudheer
 *
 */
public class LanguageSettingsPage extends CommonPage {

	public LanguageSettingsPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(css = "p.Display3")
	private WebElement pageHeader;

	@FindBy(css = "input[Value='English']")
	private WebElement englishBtn;

	@FindBy(xpath = "//label[text()='English']")
	private WebElement englishRadioBtn;

	@FindBy(css = "input[Value='Spanish']")
	private WebElement spanishBtn;

	@FindBy(xpath = "//*[contains(text(),'Save')]")
	private WebElement saveBtn;

	@FindBy(xpath = "//label[text()='Español']")
	private WebElement spanishRadioBtn;

	/**
	 * /** Verify page Header.
	 */
	public LanguageSettingsPage verifyLanguageSettingsPage() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			pageHeader.isDisplayed();
			Reporter.log("Language Settings page dispalyed");
		} catch (Exception e) {
			Assert.fail("Language Settings page not displayed");
		}
		return this;
	}

	public LanguageSettingsPage selectEnglishRadiobutton() {
		try {
			if (!englishBtn.isSelected()) {
				clickElementWithJavaScript(englishRadioBtn);
				saveBtn.click();
			} else {
				getDriver().navigate().back();
			}
		} catch (Exception e) {
			Reporter.log("Click on english radio button failed");
		}
		return this;
	}

	public LanguageSettingsPage isEnglishRadioBtnSelected() {
		try {
			Assert.assertTrue(englishBtn.isSelected());
			Reporter.log("English language radio button is selected");
		} catch (AssertionError e) {
			Reporter.log("English language radio button not selected");
		}
		return this;
	}

	public LanguageSettingsPage selectSpanishRadiobutton() {
		try {
			if (!spanishBtn.isSelected()) {
				clickElementWithJavaScript(spanishRadioBtn);
				saveBtn.click();
			}
		} catch (Exception e) {
			Reporter.log("Click on spanish radio button failed");
		}
		return this;
	}

	public LanguageSettingsPage isSpanishRadioBtnSelected() {
		try {
			Assert.assertTrue(spanishBtn.isSelected());
			Reporter.log("Spanish language radio button is selected");
		} catch (AssertionError e) {
			Reporter.log("Spanish language radio button not selected");
		}
		return this;
	}

}
