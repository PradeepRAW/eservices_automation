package com.tmobile.eservices.qa.api.eos.monitors;

import org.testng.Reporter;

import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class StandardUpgradeEIPFlow extends OrderProcess {

	/*
	 * 
	 * Steps For Standard Upgrade EIP Flow,
	 * 1. getCatalogProducts
	 * 2. Create Cart
	 * 3. Create Quote
	 * 4. Update Quote
	 * 5. Update Address
	 * 6. Make Payment
	 * 7. Place Order
	 * 
	 * */
		
	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			invokeCreateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}	
	
	@Override
	public void updateAddress(ApiTestData apiTestData) throws ServiceFailureException {
		try{
		String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "updateQuoteAddressV5.txt");
		String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
		Response response = quoteApi.updateQuote(apiTestData, updatedRequest, serviceResultsMap);
		boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "UpdateAdress");
		if(isSuccessResponse){
			//no info required from response as of now. Flow continues with next step.
		}
		}catch(Exception e){
			Reporter.log("Service issue while invoking Update Quote's Update Address  : "+e);
			throwServiceFailException();
		}
	}

}
