package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.pages.CommonPage;

public class NewCardInformation extends CommonPage {

	@FindBy(linkText = "What is this?")
	private WebElement whatIsThisCvvlink;

	@FindBy(css = "span[tooltip-position='top']")
	private WebElement toolTipText;

	@FindBy(css = "i.tooltip-icon")
	private WebElement toolTip;

	@FindBy(className = "Display3")
	private WebElement pageHeader;

	@FindBy(id = "cardName")
	private WebElement nameOnCard;

	@FindBy(id = "creditCardNumber")
	private WebElement creditCardNumber;

	@FindBy(id = "expirationDate")
	private WebElement expirationDate;

	@FindBy(id = "cvvNumber")
	private WebElement cvvNumber;

	@FindBy(id = "zipCode")
	private WebElement zip;

	@FindBy(css = "p.Display3")
	private WebElement cardHeader;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement continueAddCard;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement cancelBtn;

	@FindBy(css = "span.slider.round")
	private WebElement savePaymentToggle;

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	private final String pageUrl = "/creditCardUpdated";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public NewCardInformation(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewCardInformation verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * verify card information header
	 *
	 * @return boolean
	 */
	public NewCardInformation verifyCardPageLoaded(String msg) {
		try {

			checkPageIsReady();

			verifyPageUrl();

			Assert.assertTrue(pageHeader.getText().trim().equals(msg));

			Reporter.log("Verified the Card info Header details");
		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	public NewCardInformation clickCancelBtn() {
		try {
			cancelBtn.click();
		} catch (Exception e) {
			Assert.fail("Failed to click cancel button");
		}
		return this;
	}

	/**
	 * click continue button for add Card
	 */
	public NewCardInformation clickContinueAddCard() {
		try {

			waitFor(ExpectedConditions.elementToBeClickable(continueAddCard));
			clickElementWithJavaScript(continueAddCard);
			Reporter.log("Clicked on Continue button");

		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	/**
	 * Fill all the card details
	 *
	 * @param payment
	 */
	public NewCardInformation fillCardDetails(Payment payment) {
		try {
			nameOnCard.sendKeys(payment.getNameOnCard());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			creditCardNumber.sendKeys(payment.getCardNumber());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			expirationDate.sendKeys(payment.getExpiryMonthYear());

			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			zip.sendKeys(payment.getZipCode());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			cvvNumber.sendKeys(payment.getCvv());
			Reporter.log("Filled the Card Info details");
		} catch (Exception e) {
			Assert.fail("Failed to fill the card info details for Payment");
		}
		return this;
	}

}