package com.tmobile.eservices.qa.pages.payments;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class NewAutoPayConfirmationPage extends CommonPage {

	private By Confpagespinner = By.cssSelector("div.circle");

	@FindBy(xpath = "//span[@class='check-icon']")
	private WebElement checkIcon;

	@FindBy(xpath = "//p[contains(text(),'AutoPay is now ON')]")
	private WebElement autoPayConfirmHeader;

	@FindBy(xpath = "//p[contains(text(),'UPDATE SUCCESSFUL!')]")
	private WebElement autoPayUpdateHeader;

	@FindBy(xpath = "//p[contains(text(),'AutoPay Discount will first appear')]")
	private WebElement autoPaydidcounttext;

	@FindBy(xpath = "//p[contains(text(),'First AutoPay payment')]")
	private WebElement firstAutopayDate;

	@FindBy(xpath = "//p[contains(text(),'Your next bill due date is on')]")
	private WebElement nextBillDuedate;

	@FindBy(xpath = "//p[contains(text(),'AutoPay will charge your full balance')]")
	private WebElement autoPaychargePaymentmethod;

	@FindBy(xpath = "//span[contains(text(),'Show details')]")
	private WebElement showDetails;

	@FindBy(xpath = "//span[contains(text(),'Hide details')]")
	private WebElement hideDetails;

	@FindBy(xpath = "//span[contains(text(),'Customer')]")
	private WebElement detailsCustomer;

	@FindBy(xpath = "//span[contains(text(),'Customer')]/following-sibling::span")
	private WebElement detailsCustomerinfo;

	@FindBy(xpath = "//span[contains(text(),'T-mobile account #:')]")
	private WebElement detailsAccountnumber;

	@FindBy(xpath = "//span[contains(text(),'T-mobile account #:')]/following-sibling::span")
	private WebElement detailsAccountnumberinfo;

	@FindBy(xpath = "//span[contains(text(),'Date')]")
	private WebElement detailsDate;

	@FindBy(xpath = "//span[contains(text(),'Date')]/following-sibling::span")
	private WebElement detailsDateinfo;

	@FindBy(xpath = "//span[contains(text(),'Name on bank')]")
	private WebElement detailsnameOnBank;

	@FindBy(xpath = "//span[contains(text(),'Name on bank')]/following-sibling::span")
	private WebElement detailsnameOnBankinfo;

	@FindBy(xpath = "//span[contains(text(),'Payment method')]")
	private WebElement detailsPaymentMethod;

	@FindBy(xpath = "//span[contains(text(),'Payment method')]/following-sibling::span")
	private WebElement detailsPaymentMethodinfo;

	@FindBy(xpath = "//span[contains(text(),'Routing #')]")
	private WebElement detailsRouting;

	@FindBy(xpath = "//span[contains(text(),'Routing #')]/following-sibling::span")
	private WebElement detailsRoutinginfo;

	@FindBy(xpath = "//span[contains(text(),'Autopay status')]")
	private WebElement detailsAutopayStatus;

	@FindBy(xpath = "//span[contains(text(),'Autopay status')]/following-sibling::span")
	private WebElement detailsAutopayStatusinfo;

	@FindBy(xpath = "//span[contains(text(),'Name on card')]")
	private WebElement detailsNameOnCard;

	@FindBy(xpath = "//span[contains(text(),'Name on card')]/following-sibling::span")
	private WebElement detailsNameOnCardinfo;

	@FindBy(xpath = "//span[contains(text(),'Zip')]")
	private WebElement detailsZip;

	@FindBy(xpath = "//span[contains(text(),'Zip')]/following-sibling::span")
	private WebElement detailsZipinfo;

	@FindBy(id = "cancelButton")
	private WebElement backToHome;

	private final String pageUrl = "/autopay/confirmation";

	/**
	 * @param webDriver
	 */
	public NewAutoPayConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewAutoPayConfirmationPage verifyPageUrl() {

		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewAutoPayConfirmationPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(Confpagespinner));
			verifyPageUrl();

			Reporter.log("AutoPay Confirmation page is verified displayed");
		} catch (Exception e) {
			Reporter.log("Unknown Error - Payment submission failed");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifySuccessIcon() {
		try {
			if (checkIcon.isDisplayed())
				Reporter.log("Confirmation page check icon displayed");
			else
				Assert.fail("Confirmation page check icon is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page check icon is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifyAutopayON() {
		try {
			if (autoPayConfirmHeader.isDisplayed())
				Reporter.log("Confirmation page Autopay ON text displayed");
			else
				Assert.fail("Confirmation page page Autopay ON text is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page page Autopay ON text is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifyAutopayUpdated() {
		try {
			if (autoPayUpdateHeader.isDisplayed())
				Reporter.log("Confirmation page Autopay Update text displayed");
			else
				Assert.fail("Confirmation page page Autopay ON text is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page page Autopay Update text is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifyAutopayDiscounttext() {
		try {
			if (autoPaydidcounttext.isDisplayed())
				Reporter.log("Confirmation page Autopay discount text displayed");
			else
				Verify.fail("Confirmation page page Autopay discount text is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page page Autopay discount text is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage checkFirstautopaytextAutopayOFF(String strdate) {
		try {
			LocalDate dt = LocalDate.parse(strdate);
			// DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd yyyy hh:mm
			// a");
			DateTimeFormatter format = DateTimeFormatter.ofPattern("MMMM d");
			String landing = dt.format(format);

			String txtfirstautopay1 = firstAutopayDate.getText().trim();
			String txtfirstautopay = txtfirstautopay1.substring(0, txtfirstautopay1.length() - 2);
			String firstdate = "First AutoPay payment: " + landing;
			if (txtfirstautopay.equalsIgnoreCase(firstdate))
				Reporter.log("First autopay date is correct");
			else
				Verify.fail("First autopay date is not correct");

		} catch (Exception e) {
			Verify.fail("First auto pay date element is not locating ");
		}
		return this;
	}

	public NewAutoPayConfirmationPage checkBilldueAutopayOFF(String strdate) {
		try {
			LocalDate dt = LocalDate.parse(strdate);
			// DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd yyyy hh:mm
			// a");
			DateTimeFormatter format = DateTimeFormatter.ofPattern("MMMM d");
			String landing = dt.format(format);

			String billDueText1 = nextBillDuedate.getText().trim();
			String billDueText = billDueText1.substring(0, billDueText1.length() - 2);
			String billdue = "Your next bill due date is on " + landing;
			if (billDueText.equalsIgnoreCase(billdue))
				Reporter.log("Next Bill due date is correct");
			else
				Verify.fail("Next bill due date is not correct");

		} catch (Exception e) {
			Verify.fail("Billdue element is not locating ");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifyAutopaychargepaymentmethodtext() {
		try {
			if (autoPaychargePaymentmethod.isDisplayed())
				Reporter.log("Confirmation page Autopay Chargepayment method text displayed");
			else
				Verify.fail("Confirmation page page Autopay charge payment method text is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  Autopay chargepayment method text is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifyShowdetails() {
		try {
			if (showDetails.isDisplayed())
				Reporter.log("Confirmation page show details link displayed");
			else
				Verify.fail("Confirmation page show details link is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  show details link is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage ClickshowdetailsLink() {
		try {
			showDetails.click();
			Reporter.log("Confirmation page show details link displayed");
		} catch (Exception e) {
			Assert.fail("Confirmation page  show details link is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage ClickBackToHomebutton() {
		try {
			backToHome.click();
			Reporter.log("Back to home button clicked");
		} catch (Exception e) {
			Assert.fail("Back to home button is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifyHidedetails() {
		try {
			if (hideDetails.isDisplayed())
				Reporter.log("Confirmation page hideDetails link displayed");
			else
				Verify.fail("Confirmation page hideDetails link is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails link is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsCustomer() {
		try {
			if (detailsCustomer.isDisplayed())
				Reporter.log("Confirmation page hideDetails -Customer: displayed");
			else
				Verify.fail("Confirmation page hideDetails -Customer: is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Customer: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsCustomerinfo(String customer) {
		try {
			boolean customerinfo = detailsCustomerinfo.getText().trim().equalsIgnoreCase(customer) ? true : false;
			if (customerinfo)
				Reporter.log("Confirmation page hideDetails -Customer info verified");
			else
				Verify.fail("Confirmation page hideDetails -Customer info not matching");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Customerinfo element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsAccountnumber() {
		try {
			if (detailsAccountnumber.isDisplayed())
				Reporter.log("Confirmation page hideDetails -Accountnumber: displayed");
			else
				Verify.fail("Confirmation page hideDetails -Accountnumber: is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Accountnumber: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsAccountnumberinfo(String accnumber) {
		try {
			boolean accinfo = detailsAccountnumberinfo.getText().trim().equalsIgnoreCase(accnumber) ? true : false;
			if (accinfo)
				Reporter.log("Confirmation page hideDetails -Account info verified");
			else
				Verify.fail("Confirmation page hideDetails -Account info not matching");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Account info element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsDate() {
		try {
			if (detailsDate.isDisplayed())
				Reporter.log("Confirmation page hideDetails -Date: displayed");
			else
				Verify.fail("Confirmation page hideDetails -Date: is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Date: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsDateinfo() {
		try {
			String dt = detailsDateinfo.getText().toString();
			LocalDate date1 = LocalDate.now();
			String formattedDate = date1.format(DateTimeFormatter.ofPattern("MM/dd/YYYY"));

			boolean dateinfo = dt.trim().equalsIgnoreCase(formattedDate) ? true : false;
			if (dateinfo)
				Reporter.log("Confirmation page hideDetails -Date info verified");
			else
				Verify.fail("Confirmation page Date -Account info not matching");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Date info element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsnameOnBank() {
		try {
			if(!(getDriver() instanceof AppiumDriver))
			{
			if (detailsnameOnBank.isDisplayed())
				Reporter.log("Confirmation page hideDetails -Name on Bank: displayed");
			else
				Verify.fail("Confirmation page hideDetails -Name on Bank: is not displayed ");
			}
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Name on Bank: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsnameOnBankinfo(String nameonbank) {
		try {
			if(!(getDriver() instanceof AppiumDriver))
			{
			boolean nameonbankinfo = detailsnameOnBankinfo.getText().trim().equalsIgnoreCase(nameonbank) ? true : false;
			if (nameonbankinfo)
				Reporter.log("Confirmation page hideDetails -Name on Bank info verified");
			else
				Verify.fail("Confirmation page Date -Name on Bank info not matching");
			}
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Name on Bank info element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsPaymentMethod() {
		try {
			if (detailsPaymentMethod.isDisplayed())
				Reporter.log("Confirmation page hideDetails -Payment method: displayed");
			else
				Verify.fail("Confirmation page hideDetails -Payment method: is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Payment method: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsPaymentMethodinfo(String paymentdetails) {
		try {
			String pm = paymentdetails.split("/")[0];
			String input = paymentdetails.split("/")[1].trim();

			String lastFourDigits = input.substring(input.length() - 4);

			boolean paymentdetailsinfo = detailsPaymentMethodinfo.getText().trim().toLowerCase()
					.startsWith(pm.toLowerCase()) && detailsPaymentMethodinfo.getText().trim().endsWith(lastFourDigits)
							? true
							: false;
			if (paymentdetailsinfo)
				Reporter.log("Confirmation page hideDetails -Payment details info verified");
			else
				Verify.fail("Confirmation page Date -Payment details info not matching");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Payment methodinfo element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsRouting() {
		try {
			if (detailsRouting.isDisplayed())
				Reporter.log("Confirmation page hideDetails -Routing: displayed");
			else
				Verify.fail("Confirmation page hideDetails -Routing: is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Routing: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsRoutinginfo(String routingdetails) {
		try {
			String lastFourDigits = routingdetails.substring(routingdetails.length() - 4);
			boolean routingdetailsinfo = detailsRoutinginfo.getText().trim().endsWith(lastFourDigits) ? true : false;
			if (routingdetailsinfo)
				Reporter.log("Confirmation page hideDetails -Routing details info verified");
			else
				Verify.fail("Confirmation page Date -Routing details info not matching");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Routing details info element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsAutopayStatus() {
		try {
			if (detailsAutopayStatus.isDisplayed())
				Reporter.log("Confirmation page hideDetails -AutopayStatus: displayed");
			else
				Verify.fail("Confirmation page hideDetails -AutopayStatus: is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -AutopayStatus: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsAutopayStatusinfo(String autopaystatus) {
		try {
			boolean autopaystatusinfo = detailsAutopayStatusinfo.getText().trim().equalsIgnoreCase(autopaystatus) ? true
					: false;
			if (autopaystatusinfo)
				Reporter.log("Confirmation page hideDetails -Autopay status info verified");
			else
				Verify.fail("Confirmation page Date -Autopay status info not matching");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -Autopay status info element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsNameOnCard() {
		try {
			if (!(getDriver() instanceof AppiumDriver)) {
				if (detailsNameOnCard.isDisplayed())
					Reporter.log("Confirmation page hideDetails -Name on card: displayed");
				else
					Verify.fail("Confirmation page hideDetails -Name on card: is not displayed ");
			}
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -detailsNameOnCard: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsNameOnCardinfo(String nameoncard) {
		try {
			if(!(getDriver() instanceof AppiumDriver))
			{
			boolean NameOnCardinfo = detailsNameOnCardinfo.getText().trim().equalsIgnoreCase(nameoncard) ? true : false;
			if (NameOnCardinfo)
				Reporter.log("Confirmation page hideDetails -Name on card info verified");
			else
				Verify.fail("Confirmation page  -Name on card info not matching");
			}
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -detailsNameOnCardinfo element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsZip() {
		try {
			if (detailsZip.isDisplayed())
				Reporter.log("Confirmation page hideDetails -Zip: displayed");
			else
				Verify.fail("Confirmation page hideDetails -Zip: is not displayed ");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails Zip: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage verifydetailsZipinfo(String zip) {
		try {
			boolean Zipinfo = detailsZipinfo.getText().trim().equalsIgnoreCase(zip) ? true : false;
			if (Zipinfo)
				Reporter.log("Confirmation page hideDetails -zip info verified");
			else
				Verify.fail("Confirmation page  -zip info not matching");
		} catch (Exception e) {
			Assert.fail("Confirmation page  hideDetails -detailsZipinfo element: is not located");
		}
		return this;
	}

	public NewAutoPayConfirmationPage checkdates(String firstday, String duedate) {
		checkFirstautopaytextAutopayOFF(firstday);
		checkBilldueAutopayOFF(duedate);
		return this;
	}

	public NewAutoPayConfirmationPage NewBankconfirmationpageValidations(Map<String, String> paymentInfo) {

		ClickshowdetailsLink();
		verifyHidedetails();
		verifydetailsCustomer();
		verifydetailsCustomerinfo(paymentInfo.get("nickname"));
		verifydetailsAccountnumber();
		verifydetailsAccountnumberinfo(paymentInfo.get("accountnumber"));
		verifydetailsDate();
		verifydetailsDateinfo();
		verifydetailsPaymentMethod();
		verifydetailsPaymentMethodinfo(paymentInfo.get("paymentmethod") + "/" + paymentInfo.get("bankaccountnumber"));
		if (paymentInfo.get("paymenttype").equalsIgnoreCase("new")) {
			verifydetailsnameOnBank();
			verifydetailsnameOnBankinfo(paymentInfo.get("nameonbank"));
		}
		verifydetailsRouting();
		verifydetailsRoutinginfo(paymentInfo.get("routingnumber"));
		verifydetailsAutopayStatus();
		verifydetailsAutopayStatusinfo("Complete");

		return this;
	}

	public NewAutoPayConfirmationPage NewCreditcardconfirmationpageValidations(Map<String, String> paymentInfo) {

		ClickshowdetailsLink();
		verifyHidedetails();
		verifydetailsCustomer();
		verifydetailsCustomerinfo(paymentInfo.get("nickname"));
		verifydetailsAccountnumber();
		verifydetailsAccountnumberinfo(paymentInfo.get("accountnumber"));
		verifydetailsDate();
		verifydetailsDateinfo();
		verifydetailsPaymentMethod();
		verifydetailsPaymentMethodinfo(paymentInfo.get("cardcode") + "/" + paymentInfo.get("cardnumber"));
		verifydetailsNameOnCard();
		verifydetailsNameOnCardinfo(paymentInfo.get("nameoncard"));
		verifydetailsZip();
		verifydetailsZipinfo(paymentInfo.get("zip"));
		verifydetailsAutopayStatus();
		verifydetailsAutopayStatusinfo("Complete");

		return this;
	}

}
