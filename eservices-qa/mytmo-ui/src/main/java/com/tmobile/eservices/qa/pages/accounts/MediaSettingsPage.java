package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class MediaSettingsPage extends CommonPage {

	private static final Logger logger = LoggerFactory.getLogger(MediaSettingsPage.class);

	private static final String pageUrl = "profile/media_settings";

	@FindBy(xpath = "//p[contains(text(),'HD Video')]/../../div/span/label")
	private WebElement hdVideoToggle;

	@FindBy(css = "[id='HD Video'].switch.mt-1")
	private WebElement hdVideoToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Bing')]/../../div/span/label")
	private WebElement bingeOnToggle;

	@FindBy(xpath = "//p[contains(text(),'Bing')]")
	private WebElement bingeOnHeader;

	@FindBy(xpath = "//p[contains(text(),'HD Video')]")
	private WebElement hdVideoHeader;

	@FindBy(xpath = "//p[contains(text(),'Binge On™')]//..//..//input")
	private WebElement bingeOnBtnIP;

	@FindBy(xpath = "//p[contains(text(),'HD Video')]//..//..//input")
	private WebElement hdVideoBtnIP;

	@FindBy(xpath = "//p[contains(text(),'Media Settings')]")
	private WebElement mediaSettings;

	public MediaSettingsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify MediaSettings Page.
	 *
	 * @return the MediaSettingsPage class instance.
	 */
	public MediaSettingsPage verifyMediaSettingsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("MediaSettings page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MediaSettings page not displayed");
		}
		return this;

	}

	/**
	 * Verify bingeOnHeader header.
	 */
	public MediaSettingsPage verifyBingeOnHeader() {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOf(bingeOnHeader));
		try {
			bingeOnHeader.isDisplayed();
			Reporter.log("BingeOn header displayed");
		} catch (Exception e) {
			Assert.fail("BingeOn header not displayed");
		}
		return this;
	}

	/**
	 * Verify hdVideo header.
	 */
	public MediaSettingsPage verifyHDVideoHeader() {
		try {
			waitforSpinnerinProfilePage();
			hdVideoHeader.isDisplayed();
			Reporter.log("HDVideo header displayed");
		} catch (Exception e) {
			Assert.fail("HDVideo header not displayed");
		}
		return this;
	}

	/**
	 * Click hdVideo toggle.
	 */
	public MediaSettingsPage clickHDVideoToggleAndVerifyHDVideoOnOrOff() {
		waitforSpinnerinProfilePage();
		try {
			boolean selected = hdVideoBtnIP.isSelected();
			hdVideoToggle.click();
			Reporter.log("Clicked on Hd Video button");
			waitforSpinnerinProfilePage();
			if (selected) {
				Assert.assertFalse(hdVideoBtnIP.isSelected());
			} else {
				Assert.assertTrue(hdVideoBtnIP.isSelected());
			}

		} catch (Exception e) {
			Assert.fail("Click on HD Video toggle failed");
		}
		return this;
	}

	/**
	 * Click BingeOn toggle.
	 */
	public MediaSettingsPage clickBingeOnToggleAndVerifyBingeOnButtonOnOrOff() {
		waitforSpinnerinProfilePage();
		// waitFor(ExpectedConditions.visibilityOf(bingeOnBtnIP));
		try {
			boolean selected = bingeOnBtnIP.isSelected();

			bingeOnToggle.click();
			Reporter.log("clicked on BingeOn toogle");
			waitforSpinnerinProfilePage();
			if (selected) {

				Assert.assertFalse(bingeOnBtnIP.isSelected());

			} else {

				Assert.assertTrue(bingeOnBtnIP.isSelected());
			}
		} catch (Exception e) {
			Assert.fail("Click on BingeOn toggle failed");
		}
		return this;
	}

	/**
	 * Click hdVideo toggle.
	 */
	public MediaSettingsPage verifyMediaSettingsLinkNotDisplayedonProfilePage() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			mediaSettings.isDisplayed();
			Assert.fail("Media Settings Link is displayed - failed");
		} catch (Exception e) {
			Reporter.log("HD option & Media Settings Link is not  present for Goat fell Plan Customer:");
		}
		return this;
	}

	/**
	 * Verify bingeOnHeader header.
	 */
	public MediaSettingsPage verifyBingeOnHeaderForDisabledCustomer() {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOf(bingeOnHeader));
		try {
			Assert.assertFalse(bingeOnHeader.isDisplayed());
			Reporter.log("BingeOn header not displayed");
		} catch (AssertionError e) {
			Assert.fail("BingeOn header displayed");
		}
		return this;
	}

	/**
	 * Verify hd video toggle disable status.
	 */
	public MediaSettingsPage verifyHDVideoToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			hdVideoToggleDisabledStatus.isDisplayed();
			Reporter.log("HD video toggle is in disabled state");
		} catch (Exception e) {
			Assert.fail("HD video toggle is enabled");
		}
		return this;
	}
}
