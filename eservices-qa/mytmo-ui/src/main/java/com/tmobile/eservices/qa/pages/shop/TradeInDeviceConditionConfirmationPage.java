package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class TradeInDeviceConditionConfirmationPage extends CommonPage {

	public static final String continueToCart = "continue";
	public static final String tradeInAnotherDevice = "TradeInAnotherDevice";
	private static final String pageUrl = "tradeInDeviceConditionValue";

	@FindBy(css = "p[class*='text-black title-text']")
	private WebElement eigibilityMessage;

	@FindBy(css = "p[class*='body-copy-description letter']")
	private WebElement messageDescription;

	@FindBy(css = "div[class*='trade-in-content'] img")
	private WebElement deviceImage;

	@FindBy(css = "button[class*='btn PrimaryCTA-Normal button-title button-width']")
	private WebElement continueTradeINButton;
	
	@FindBy(css = "div.trade-in-content p:nth-child(2)")
	private WebElement badConditionText;

	@FindBy(css = "[ng-click*='ctrl.goToCart']")
	private WebElement tradeInThisPhoenButton;

	@FindBy(css = "h3.h3-title")
	private WebElement tradeInValuePage;
	
	@FindBy(css = "div.goodCondition button.btn.btn1.btn-invisible")
	private WebElement tradeInAnotherDeviceButton;

	@FindBy(css = "div.goodCondition>div.padding-bottom-medium.ng-binding")
	private WebElement legalText;

	@FindBy(css = "div.text-center.header-box-shadow.ng-scope")
	private WebElement sedonaHeader;

	@FindBy(css = ".btn.SecondaryCTA-Normal")
	private WebElement continueTradeIn;
	
	@FindBy(css = "span.h1-title")
	private List<WebElement> tradeInValue;
	
	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement continueButton;
	
	@FindBy(css = "i.fa.fa-spin.fa-spinner.interceptor-spinner")
	private WebElement pageSpinner;
	
	@FindBy(css = "#pageContainer div.text-center p.text-center")
	private WebElement deviceDetails;
	
	@FindBy(css = "#pageContainer div.text-center p.text-uppercase")
	private WebElement imeinew;
	
	@FindBy(css = ".row p i")
    private WebElement tickMarkForQualifiedTradeInpromo;
	
	@FindBy(css = ".text-center.hidden-xs [ng-bind*='ctrl.jsondata.promoPageHeaderTxt']")
	private WebElement tradeInPromoEligibilityText;
	
	@FindBy(css = "[ng-click*='ctrl.skipTradeIn']")
	private WebElement skipTradeInButton;	
	
	@FindBy(css = "[class*='legal']")
	private WebElement legalTextInBadCondition;
	
	public TradeInDeviceConditionConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Trade-In Value Page
	 * 
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage verifyTradeInDeviceConditionConfirmationPage() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("Trade-In device condition confirmation page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Trade-In device condition confirmation page is not displayed");
		}
		return this;
	}
	
	/**
	 * Verify Trade-In Device Condition Confirmation Page
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage verifyPageUrl() {
		checkPageIsReady();
		waitforSpinner();
		try{
			getDriver().getCurrentUrl().contains("tradeInDeviceCondition");
		}catch(Exception e){
			Assert.fail("Trade-In device condition confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Click Trade-In Another Device
	 */
	public TradeInDeviceConditionConfirmationPage clickTradeInAnotherDevice() {
		try {
			tradeInAnotherDeviceButton.click();

		} catch (Exception e) {
			Assert.fail("Trade In ANother Device is not Displayed to click");
		}
		return this;
	}
	
	/**
	 * Verify Agree And Continue Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage verifyAgreeAndContinueButton() {
		try {
			Assert.assertTrue(tradeInThisPhoenButton.isDisplayed(), "Trade In this Phone CTA is not displayed");
		}catch (Exception e) {
			Assert.fail("Trade In ANother Device is not Displayed to click");
		}
		return this;
	}
	
	/**
	 * Verify legal text is displayed
	 */

	public TradeInDeviceConditionConfirmationPage verifyLegaltextInBadConditionDisplayed() {
		try {
			legalTextInBadCondition.isDisplayed();
			Reporter.log("Legal text is displayed with bad condition");
		} catch (Exception e) {
			Assert.fail("Failed to verify legal text with bad condition");
		}
		return this;
	}
	
	/**
	 * verify tick Mark For Trade In promo Device Qualified is displayed
	 * 
	 * @return true/false
	 */
	public TradeInDeviceConditionConfirmationPage verifyTickMarkForTradeInpromoDeviceQualified() {
		try {
			String tradeInPromoText = tradeInPromoEligibilityText.getText(); 
			Assert.assertTrue(tradeInPromoText.equals("Your trade-in device is eligible for promotion"),"Trade in Promo Qualified text is verified");
			Assert.assertTrue(tickMarkForQualifiedTradeInpromo.isDisplayed(),"Trade in Promo Qualified tick mark not displayed");
			Reporter.log("Tick mark for Qualified Trade In Promo device is displayed");
			verifyTradeInPromoDeviceInTradeInValuePage();
		} catch (Exception e) {
			Assert.fail("Failed to display Tick mark for Qualified device.");
		}
		return this;
	}
	
	/**
	 * Verify Trade-In Value Page
	 * 
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage verifyTradeInPromoDeviceInTradeInValuePage() {
		try {
			verifyPageUrl();
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
//			String modelDetails = deviceDetails.getText();
//			String imeinumber = imeinew.getText();
//			Assert.assertTrue(modelDetails.equals(ShopConstants.MODEL_DROPDOWN_OPTION),"The model detail is not verified");//
//			Assert.assertTrue(imeinumber.contains(ShopConstants.EIP_IMEI),"The imei number is not verified");//
			Reporter.log("Trade in device is present on value page");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Trade in device");
		}
		return this;
	}

	/**
	 * Verify Eligibility Message which is authorable and dynamic
	 */

	public TradeInDeviceConditionConfirmationPage verifyEligibilityMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(eigibilityMessage.isDisplayed(), "Eligibility Message is not displayed");
			Assert.assertTrue(eigibilityMessage.getText().contains("$"), "Eligibility Message is not displayed");
			Reporter.log(
					"trade in value message with bill credit amount ('Yes!! You're qualified for the $X bill credit for your old phone') is displayed");
		} catch (Exception e) {
			Assert.fail(
					"trade in value message with bill credit amount ('Yes!! You're qualified for the $X bill credit for your old phone') is not displayed");
		}
		return this;
	}

	/**
	 * Verify Trade in description message which is authorable and dynamic
	 */

	public TradeInDeviceConditionConfirmationPage verifyMessageDescription() {
		try {
			waitforSpinner();
			Assert.assertTrue(messageDescription.isDisplayed(), "Message description not displayed");
			Assert.assertTrue(messageDescription.getText().contains("bill credits for"),
					"Message description not displayed");
			Reporter.log("Trade in value description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in value description");
		}
		return this;
	}

	/**
	 * Verify device condition image on Good device condition page
	 */

	public TradeInDeviceConditionConfirmationPage verifyDeviceConditionImage() {
		try {
			waitforSpinner();
			Assert.assertTrue(deviceImage.isDisplayed(), "Device image is not displayed");
			Reporter.log("'device with issues' image is displayed on Trade-In value page");
		} catch (Exception e) {
			Assert.fail("Failed to verify device image on Trade In Confirmation Page");
		}
		return this;
	}

	/**
	 * Click On Continue TradeIN Button
	 */
	public TradeInDeviceConditionConfirmationPage clickContinueTradeInButton() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(continueTradeINButton));
			continueTradeINButton.click();
			Reporter.log("Continue TradeIN Button is clickable");
		} catch (Exception e) {
			Assert.fail("Continue TradeIN Button is not clickable");
		}
		return this;
	}
	
	
	/**
	 * Click On Trade In This Phone Button
	 */
	public TradeInDeviceConditionConfirmationPage clickOnTradeInThisPhoneButton() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(tradeInThisPhoenButton),10);
			moveToElement(tradeInThisPhoenButton);
			tradeInThisPhoenButton.click();
			Reporter.log("Clicked on Trade in this Phone Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Trade in this phone CTA");
		}
		return this;
	}
	
	/**
	 * Verify skip Trade In CTA
	 * 
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage verifySkipTradeInButton() {
		try {
			Assert.assertTrue(skipTradeInButton.isDisplayed(), "Skip Trade in Button is not displayed");
		}catch (Exception e) {
			Assert.fail("Failed to verify Skip Trade in Button");
		}
		return this;
	}
	
	
	/**
	 * Verify Recycle with Us CTA displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage verifyRecycleWithUsCTADisplayed() {
		try {
			Assert.assertTrue(tradeInThisPhoenButton.isDisplayed(), "Recycle with Us CTA is not displayed");
		}catch (Exception e) {
			Assert.fail("Failed to verify Recycle with Us CTA");
		}
		return this;
	}
	/**
	 * Click On Continue RecycleButton 
	 */
	public TradeInDeviceConditionConfirmationPage clickRecycleButton() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(tradeInThisPhoenButton));
			tradeInThisPhoenButton.click();
			Reporter.log("RecycleButton  is clicked");
		} catch (Exception e) {
			Assert.fail("RecycleButton  is not clicked");
		}
		return this;
	}
	/**
	 * Verifies Bad condition confirmation page
	 */
	public TradeInDeviceConditionConfirmationPage verifyBadConditionText() {
		waitforSpinner();
		try {
			Assert.assertTrue(badConditionText.getText().toLowerCase().contains("sorry"),"Not landed on bad condition page");
			Reporter.log("Bad condition page is verified");
		}catch (Exception e){
			Assert.fail("Bad condition page is not displayed");
		}
		return this;
	}
	
	/**
	 * Click On Skip trade-in Button
	 */
	public TradeInDeviceConditionConfirmationPage clickOnSkipTradeInButton() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(skipTradeInButton));
			skipTradeInButton.click();
			Reporter.log("Clicked on Skip trade-in Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Skip trade-in CTA");
		}
		return this;
	}
	
}
