package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

/**
 * @author pshivakumar
 * 
 */
public class MyTmoData {

	@Data(name = "Env")
	private String env;
	
	@Data(name = "Make")
	private String make;
	
	@Data(name = "iMEINumber")
	private String iMEINumber;
	
	@Data(name = "URL")
	private String url;
	
	@Data(name = "Carrier")
	private String carrier;
	
	@Data(name = "Device")
	private String device;
	
	@Data(name = "QuickLinks")
	private String quickLinks;
	
	@Data(name = "Model")
	private String model;
	
	@Data(name = "LegacyUsername")
	private String legacyUserName;
	
	@Data(name = "LegacyPassword")
	private String legacyPassword;

	@Data(name = "loginEmailOrPhone")
	private String loginEmailOrPhone;

	@Data(name = "loginPassword")
	private String loginPassword;
	
	@Data(name="switchAccount")
	private String switchAcc;
	
	@Data(name="loginAccount")
	private String loginAcc;
	
	@Data(name="AddressLine1")
	private String addressLine1;
	
	@Data(name="City1")
	private String city1;
	
	@Data(name="State1")
	private String state1;
	
	@Data(name="ZipCode1")
	private String zipCode1;
	
	@Data(name="AddressLine2")
	private String addressLine2;
	
	@Data(name="City2")
	private String city2;
	
	@Data(name="State2")
	private String state2;
	
	@Data(name="ZipCode2")
	private String zipCode2;
	
	@Data(name="Allowance")
	private String allowance;  
	
	@Data(name="planname")
	private String planname;
	
	@Data(name="callerIdFName")
	private String callerIdFName;
	
	@Data(name="callerIdLName")
	private String callerIdLName;
	
	public String getcallerIdFName() {
		return callerIdFName;
	}
	
	public String getcallerIdLName() {
		return callerIdLName;
	}
	
	public String getAllowance() {
		return allowance;
	}
	
	public String getplanname() {
		return planname;
	}
	
	public String getAllowance2() {
		return allowance;
	}
	
	public void setAllowance(String allowance) {
		this.allowance = allowance;
	}

	
	public String getLoginAcc() {
		return loginAcc;
	}


	public void setLoginAcc(String loginAcc) {
		this.loginAcc = loginAcc;
	}


	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	
	public void setLoginEmailOrPhone(String loginEmailOrPhone) {
		this.loginEmailOrPhone = loginEmailOrPhone;
	}
	


	public String getSwitchAcc() {
		return switchAcc;
	}

	public String getQuickLinks() {
		return quickLinks;
	}

	@Data
	private SignUpData signUpData;

	@Data(name = "WelcomeHeader")
	private String welcomeHeading;

	@Data(name = "bladeUserInfo")
	private String bladeUserInfo;

	@Data(name = "CardNumber")
	private String cardNumber;

	@Data(name = "FirstName")
	private String userName;

	@Data(name = "firstName")
	private String firstName;

	@Data(name = "lastName")
	private String lastName;

	@Data(name = "AccountNumber")
	private String accNumber;

	@Data(name = "RoutingNumber")
	private String routingNumber;

	@Data(name = "NewPassword")
	private String newPassword;

	@Data(name = "Password")
	private String password;

	@Data(name = "PayOtherAmount")
	private String payOtherAmount;

	@Data(name = "TMobileIDLastName")
	private String tMobileIDLastName;

	@Data(name = "PhoneNumber")
	private String phoneNumber;

	@Data(name = "Answer1")
	private String tMobileIDAnswer1;

	@Data(name = "Answer2")
	private String tMobileIDAnswer2;

	@Data(name = "Answer3")
	private String tMobileIDAnswer3;
	
	@Data(name = "Email")
	private String emailId;
	
	@Data(name="pageTitle")
	private String pageTitle;
	
	@Data(name="manufacturer")
	private String manufacturer;
	
	@Data(name="deviceTitle")
	private String deviceTitle;	
	
	@Data(name="DeviceName")
	private String deviceName;	
	
	@Data(name="ReviewOption")
	private String reviewOption;	
	
	@Data(name="paymentOptions")
	private String paymentOptions;
	
	@Data(name="NumberOfAccessories")
	private String numberOfAccessories;
	
	@Data(name="Memory")
	private String deviceMemory;
	
	@Data(name="color")
	private String deviceColor;

	@Data(name="TestName")
	private String testName;
	
	@Data
	private CustomerInfo customerInfo;
	
	@Data
	private Payment payment;
	
	@Data
	private BillingPaymentInfo billingPaymentInfo;
	
	@Data(name="OptionType")
	private String optionType;
	
	@Data(name="AccessoryName")
	private String accessoryName;
	
	@Data(name="LineNumber")
	private String lineNumber;

	@Data(name="Filter")
	private String filter;
	
	@Data(name = "NickName")
	private String nickName;
	
	public String getfilter() {
		return filter;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}


	public String getDeviceTitle() {
		return deviceTitle;
	}

	public String getFirstName() {

		return firstName;
	}
	public String getLastName() {
		return lastName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public String getWelcomeHeading() {
		return welcomeHeading;
	}
	/**
	 * @return the loginEmailOrPhone
	 */
	public String getLoginEmailOrPhone() {
		return loginEmailOrPhone;
	}

	/**
	 * @return the loginPassword
	 */
	public String getLoginPassword() {
		return loginPassword;
	}
	
	public String getEmailid() {
		return emailId;
	}

	/**
	 * @return the signUpData
	 */
	public SignUpData getSignUpData() {
		return signUpData;
	}

	/**
	 * @return the getBladeUserInfo
	 */
	public String getBladeUserInfo() {		
		return bladeUserInfo;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the accNumber
	 */
	public String getAccNumber() {
		return accNumber;
	}

	/**
	 * @return the routingNumber
	 */
	public String getRoutingNumber() {
		return routingNumber;
	}

	/**
	 * @return String the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}


	/**
	 * @return the payOtherAmount
	 */
	public String getPayOtherAmount() {
		return payOtherAmount;
	}

	/**
	 * @return the customerInfo
	 */
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	/**
	 * @return the env
	 */
	public String getEnv() {
		return env;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the tMobileIDLastName
	 */
	public String getTMobileIDLastName() {
		return tMobileIDLastName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	/**
	 * @return the phoneNumber2
	 */
	public String getPhoneNumber2() {
		return phoneNumber;
	}

	/**
	 * @return tMobileIDAnswer1
	 */
	public String getTMobileIDAnswer1() {
		return tMobileIDAnswer1;
	}

	/**
	 * 
	 * @return tMobileIDAnswer2
	 */
	public String getTMobileIDAnswer2() {
		return tMobileIDAnswer2;
	}

	/**
	 * 
	 * @return tMobileIDAnswer3
	 */
	public String getTMobileIDAnswer3() {
		return tMobileIDAnswer3;
	}

	/**
	 * @return the legacyUserName
	 */
	public String getLegacyUserName() {
		return legacyUserName;
	}

	/**
	 * @return the tMobileIDLastName
	 */
	public String gettMobileIDLastName() {
		return tMobileIDLastName;
	}

	/**
	 * @return the tMobileIDAnswer3
	 */
	public String gettMobileIDAnswer3() {
		return tMobileIDAnswer3;
	}

	/**
	 * @return the legacyPassword
	 */
	public String getLegacyPassword() {
		return legacyPassword;
	}

	public String gettMobileIDAnswer1() {
		return tMobileIDAnswer1;
	}

	public String gettMobileIDAnswer2() {
		return tMobileIDAnswer2;
	}

	public Payment getPayment() {
		return payment;
	}

	public BillingPaymentInfo getBillingPaymentInfo() {
		return billingPaymentInfo;
	}
	
	/**
	 * @return the pageTitle
	 */
	public String getPageTitle() {
		return pageTitle;
	}


	public String getEmailId() {
		return emailId;
	}


	public String getOptionType() {
		return optionType;
	}


	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}


	/**
	 * @return the device
	 */
	public String getDevice() {
		return device;
	}


	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	/**
	 * @return the make
	 */
	public String getMake() {
		return make;
	}
	
	/**
	 * @return the imei number
	 */
	public String getiMEINumber() {
		return iMEINumber;
	}
	
	public void setEnv(String env) {
		this.env = env;
	}


	public String getReviewOption() {
		return reviewOption;
	}


	public String getUrl() {
		return url;
	}
	
	@Override
	public String toString() {
		return "MyTmoData [env=" + env + ", make=" + make + ", iMEINumber=" + iMEINumber + ", url=" + url + ", carrier="
				+ carrier + ", device=" + device + ", quickLinks=" + quickLinks + ", model=" + model
				+ ", legacyUserName=" + legacyUserName + ", legacyPassword=" + legacyPassword + ", loginEmailOrPhone="
				+ loginEmailOrPhone + ", loginPassword=" + loginPassword + ", switchAcc=" + switchAcc + ", loginAcc="
				+ loginAcc + ", addressLine1=" + addressLine1 + ", city1=" + city1 + ", state1=" + state1
				+ ", zipCode1=" + zipCode1 + ", addressLine2=" + addressLine2 + ", city2=" + city2 + ", state2="
				+ state2 + ", zipCode2=" + zipCode2 + ", allowance=" + allowance + ", signUpData=" + signUpData
				+ ", welcomeHeading=" + welcomeHeading + ", bladeUserInfo=" + bladeUserInfo + ", cardNumber="
				+ cardNumber + ", firstName="+ firstName+", lastName="+ lastName + ", userName=" + userName + ", accNumber=" + accNumber + ", routingNumber="
				+ routingNumber + ", newPassword=" + newPassword + ", password=" + password + ", payOtherAmount="
				+ payOtherAmount + ", tMobileIDLastName=" + tMobileIDLastName + ", phoneNumber=" + phoneNumber
				+ ", tMobileIDAnswer1=" + tMobileIDAnswer1 + ", tMobileIDAnswer2=" + tMobileIDAnswer2
				+ ", tMobileIDAnswer3=" + tMobileIDAnswer3 + ", emailId=" + emailId + ", pageTitle=" + pageTitle
				+ ", manufacturer=" + manufacturer + ", deviceTitle=" + deviceTitle + ", deviceName=" + deviceName
				+ ", reviewOption=" + reviewOption + ", paymentOptions=" + paymentOptions + ", numberOfAccessories="
				+ numberOfAccessories + ", deviceMemory=" + deviceMemory + ", testName=" + testName + ", customerInfo="
				+ customerInfo + ", payment=" + payment + ", billingPaymentInfo=" + billingPaymentInfo + ", optionType="
				+ optionType + ", accessoryName=" + accessoryName + ", lineNumber=" + lineNumber + ", filter=" + filter
				+ "]";
	}


//	@Override
//	public String toString() {
//		return "MyTmoData [env=" + env + ", url=" + url + ", carrier=" + carrier + ", device=" + device + ", model="
//				+ model + ", legacyUserName=" + legacyUserName + ", legacyPassword=" + legacyPassword
//				+ ", loginEmailOrPhone=" + loginEmailOrPhone + ", loginPassword=" + loginPassword + ", switchAcc="
//				+ switchAcc + ", signUpData=" + signUpData + ", welcomeHeading=" + welcomeHeading + ", bladeUserInfo="
//				+ bladeUserInfo + ", cardNumber=" + cardNumber + ", userName=" + userName + ", accNumber=" + accNumber
//				+ ", routingNumber=" + routingNumber + ", newPassword=" + newPassword + ", password=" + password
//				+ ", payOtherAmount=" + payOtherAmount + ", tMobileIDLastName=" + tMobileIDLastName + ", phoneNumber="
//				+ phoneNumber + ", tMobileIDAnswer1=" + tMobileIDAnswer1 + ", tMobileIDAnswer2=" + tMobileIDAnswer2
//				+ ", tMobileIDAnswer3=" + tMobileIDAnswer3 + ", emailId=" + emailId + ", pageTitle=" + pageTitle
//				+ ", manufacturer=" + manufacturer + ", deviceTitle=" + deviceTitle + ", deviceName=" + deviceName
//				+ ", reviewOption=" + reviewOption + ", paymentOptions=" + paymentOptions + ", testName=" + testName
//				+ ", accessoriesTab=" + accessoriesTab + ", customerInfo=" + customerInfo + ", payment=" + payment
//				+ ", billingPaymentInfo=" + billingPaymentInfo + ", optionType=" + optionType + "]";
//	}


	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}
	
	/**
	 * @return the paymentOptions
	 */
	public String getpaymentOptions() {
		return paymentOptions;
	}
	
	public String getTestName() {
		return testName;
	}


	public String getPaymentOptions() {
		return paymentOptions;
	}


	public String getNumberOfAccessories() {
		return numberOfAccessories;
	}
	
	/**
	 * @return the accessoryName
	 */
	public String getAccessoryName() {
		return accessoryName;
	}
	
	/**
	 * @return the line Number
	 */
	public String getLineNumber() {
		return lineNumber;
	}


	public String getAddressLine1() {
		return addressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	public String getCity1() {
		return city1;
	}


	public void setCity1(String city1) {
		this.city1 = city1;
	}


	public String getState1() {
		return state1;
	}


	public void setState1(String state1) {
		this.state1 = state1;
	}


	public String getZipCode1() {
		return zipCode1;
	}


	public void setZipCode1(String zipCode1) {
		this.zipCode1 = zipCode1;
	}


	public String getAddressLine2() {
		return addressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	public String getCity2() {
		return city2;
	}


	public void setCity2(String city2) {
		this.city2 = city2;
	}


	public String getState2() {
		return state2;
	}


	public void setState2(String state2) {
		this.state2 = state2;
	}


	public String getZipCode2() {
		return zipCode2;
	}


	public void setZipCode2(String zipCode2) {
		this.zipCode2 = zipCode2;
	}

	public String getDeviceMemory() {
		return deviceMemory;
	}
	
	public String getDeviceColor() {
		return deviceColor;
	}
	
	/**
	 * @return the NickName
	 */
	public String getNickName() {
		return nickName;
	}
}

