package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PlansConfigurePage extends CommonPage {

	@FindBy(xpath = "//h5[contains(text(), '6 GB')]//..//..//div[@class='indicator']//span")
	private WebElement gb6RadioButton;

	@FindBy(xpath = "//h5[contains(text(), '4 GB')]//..//..//div[@class='indicator']//span")
	private WebElement gb4RadioButton;

	@FindBy(xpath = "//h5[contains(text(), 'Unlimited')]//..//..//div[@class='indicator']//span")
	private WebElement unlimitedGBRadioButton;

	@FindBy(id = "pageheader")
	private WebElement pageHeader;

	@FindBy(id = "current-bill-cycle")
	private WebElement billcycleDropdown;

	@FindBy(id = "current-cycle-text")
	private WebElement nextBillCycle;

	@FindBy(css = "label[for='bill-cycle']")
	private WebElement effectiveDateText;

	@FindBy(linkText = "Service Agreement")
	private WebElement serviceAgrement;

	@FindBy(css = "a[href*='#termandconditionmodal']")
	private WebElement termsAndConditions;

	@FindBy(css = "a[href*='#electronicsignaturemodal']")
	private WebElement electronicSignatureTerms;

	@FindBy(id = "commonshowcostbreakdowncls")
	private WebElement seeCostBreakDown;

	@FindBy(id = "changeData")
	private List<WebElement> changeData;

	@FindBy(id = "tmoPlanHeaderNew")
	private WebElement textPlanName;

	@FindBy(id = "planName")
	private WebElement planName;

	@FindBy(css = "span[class='daily-total dollars']")
	private WebElement dollarsText;

	@FindBy(id = "printLinkserviceagreement_id")
	private WebElement printServiceAgreementLink;

	@FindBy(id = "printLinkTnC_id")
	private WebElement printTermsAndConditionsLink;

	@FindBy(id = "printLinkelecsign_id")
	private WebElement printElectronicSignatureTermsLink;

	@FindBy(css = "div[id='serviceagreementmodal'] i[class='fa fa-close']")
	private WebElement serviceAgreementcloseButton;

	@FindBy(css = "div[id='termandconditionmodal'] i[class='fa fa-close']")
	private WebElement termAndConditionscloseButton;

	@FindBy(css = "div[id='electronicsignaturemodal'] i[class='fa fa-close']")
	private WebElement electronicSignaturemodalCloseButton;

	@FindBy(xpath = "//*[text()='Your new monthly plan and feature(s) amount']")
	private WebElement tmoHeaderNew;

	@FindBy(xpath = "//p[text()='Netflix Standard $10.99 with Fam Allowances ($16 value)']")
	private WebElement netflixPlanDetails;

	@FindBy(xpath = "//p[text()='Netflix Premium $13.99 with Fam Allowances ($19 value)']")
	private WebElement premiumnetflixPlanDetails;

	@FindBy(css = "div[class*='legalContent padding-horizontal-small'] ul")
	private List<WebElement> rPFAndTRFApplies;

	@FindBy(id = "TMOPlans")
	private List<WebElement> tMobileOneOptions;

	@FindAll({ @FindBy(id = "data-name-11"), @FindBy(id = "data-name-13") })
	private WebElement onePlusInternationalHeading;

	@FindBy(xpath = "//p[contains(text(),'4G LTE WITH MOBILE HOTSPOT')]")
	private WebElement oNEPlusDescription;

	@FindBy(linkText = "See Cost Breakdown")
	// @FindBy(css = "div#sharedCostBreakDown a.showcostbreakdownAccount")
	private WebElement costBreakDown;

	@FindBy(css = "div.showcostbreakdownAccountCtr a")
	private WebElement seecostBreakDownAccountLevel;

	@FindBy(css = "a#hidecostbreakdown")
	private List<WebElement> hideCostBreakDown;

	@FindBy(linkText = "Service Agreement")
	private WebElement serviceAgreementLink;

	@FindBy(css = "button#btnwarningContinue")
	private WebElement kickBackPopUp;

	@FindBy(css = "span.Autopay-Toggle-switch")
	private WebElement autoPayToggle;

	@FindBy(css = "div.autoPayMsgText h3")
	private WebElement autoPayHeader;

	@FindBy(css = "div#paymentSection")
	private WebElement paymentSection;

	@FindBy(css = "a#commonhidecostbreakdowncls")
	private WebElement commonhidecostbreakdown;

	@FindBy(css = "div#AccountCostbreakdownid")
	private WebElement costBreakDownExpand;

	@FindBy(css = "a.hidecostbreakdownAccount")
	private WebElement hidecostbreakdownAccount;

	@FindBy(css = "div#commoncostbreakdown a")
	private WebElement commoncostbreakdown;

	@FindBy(css = "div#costbreakdowndetails1")
	private WebElement commoncostbreakdownExpand;

	@FindBy(css = "span#mrc-total-predecessor")
	private WebElement monthlyTotal;

	@FindBy(css = "button#agree-submit")
	private WebElement agreensubmitBtn;

	@FindBy(css = "div#planNameLarge")
	private WebElement newPlanName;

	@FindBy(css = "div#customerName")
	private WebElement successHeader;

	@FindBy(css = "p#idAutoPayDiscountMsg")
	private WebElement autoPayDiscountMsg;

	@FindBy(css = "span#totalDollars")
	private WebElement totalDollars;

	@FindBy(css = "button.btn.maElement")
	private WebElement backToHomeBtn;

	@FindBy(css = "a#plansUrl")
	private WebElement viewMoreDetailslink;

	@FindBy(css = "#paymentSection i")
	private List<WebElement> addpaymetnmethod;

	@FindBy(css = "i[class*='fa fa-chevron-right']")
	private List<WebElement> addbank;

	@FindBy(id = "addBankBackBtn")
	private WebElement backbuton;

	@FindBy(id = "back_paymentSpoke")
	private WebElement backbutton;

	@FindBy(xpath = "//p[contains(text(), 'On Us')]")
	private WebElement netflixPlanOnUsDetails;

	private final String pageUrl = "/plans-configure.html";

	/**
	 * 
	 * @param webDriver
	 */
	public PlansConfigurePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PlansConfigurePage verifyPlanConfigurePage() {
		try {
			checkPageIsReady();
			waitforSpinner();
			verifyPageUrl();
			waitFor(ExpectedConditions
					.javaScriptThrowsNoExceptions("return (document.readyState == 'complete' && jQuery.active == 0)"));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("modal_button_loading")));
			isPageHeaderDisplayed();
		} catch (Exception e) {
			Assert.fail("Plan-Configuration Page not Loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PlansConfigurePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify Cost Break down text
	 * 
	 */

	public PlansConfigurePage verifyHideCostBreakDownText() {
		try {
			clickElementBytext(hideCostBreakDown, "Hide Cost Breakdown");
			// hideCostBreakDown.isDisplayed();
			Reporter.log("hide Cost BreakDown is displayed");
		} catch (Exception e) {
			Assert.fail("hide Cost BreakDown is not displayed");
		}
		return this;
	}

	/**
	 * Verify Page Header Displayed
	 * 
	 * @return boolean
	 */

	public PlansConfigurePage isPageHeaderDisplayed() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(tmoHeaderNew));
			tmoHeaderNew.isDisplayed();
			Reporter.log("Plan configuration page is displayed");
		} catch (Exception e) {
			Assert.fail("Plan configuration page not displayed");
		}
		return this;
	}

	/**
	 * get monthly total amount
	 */

	public Integer getMonthlyTotal() {
		Integer monthlyTextwithAutopay = null;
		try {
			checkPageIsReady();
			monthlyTotal.isDisplayed();
			monthlyTextwithAutopay = Integer.parseInt(monthlyTotal.getText());
			Reporter.log("mothly total amount is displayed");
		} catch (Exception e) {
			Assert.fail("mothly total amount is not found");
		}
		return monthlyTextwithAutopay;
	}
}
