package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class TradeInConfirmationPage extends CommonPage {

	private static final String pageUrl = "tradeInDeviceConditionValue";

	@FindBy(css = "button[ng-click*='ctrl.continueToCart()']")
	private WebElement agreeContinueButton;

	@FindBy(css = "a[ng-click*='goToPhoneSelectionPage']")
	private WebElement linkLineTradeInDifferentPhone;

	@FindBy(css = "p[class*='text-black title-text']")
	private WebElement eigibilityMessage;

	@FindBy(css = "div[class*='trade-in-content'] img")
	private WebElement deviceImage;

	@FindBy(css = "button[class*='btn PrimaryCTA-Normal button-title button-width']")
	private WebElement continueTradeINButton;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement noThanksIChangedMyMind;

	@FindBy(css = "[ng-click*='ctrl.showLineDetails(line)'][style*='display: none']")
	private WebElement remainingLineNotDisplay;

	@FindBy(css = "[ng-click*='ctrl.goToCart']")
	private WebElement tradeInThisPhoenButton;

	@FindBy(css = "[ng-if*='selectedBulletPoints']")
	private WebElement tradeInValueMessage;

	@FindBy(css = "[ng-if*='selectedBulletPoints'] p")
	private List<WebElement> tradeInValueMessageBulletPoints;

	@FindBy(css = "[class*='legal']")
	private WebElement legalText;

	@FindBy(css = "[ng-click*='ctrl.skipTradeIn']")
	private WebElement skipTradeInButton;

	@FindBy(css = ".btn-primary.PrimaryCTA")
	private WebElement tradeInThisPhone;
	
	public TradeInConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify TradeIn Confirmation Page URL
	 * 
	 * @return
	 */
	public TradeInConfirmationPage verifyTradeInDeviceConditionPageURL() {
		waitforSpinner();
		try {
			getDriver().getCurrentUrl().contains(pageUrl);
		} catch (Exception e) {
			Assert.fail("Trade In Confirmation page is not displayed");
		}
		return this;
	}

	/**
	 * Verify TradeIn Confirmation Page
	 * 
	 * @return
	 */
	public TradeInConfirmationPage verifyTradeInConfirmationPage() {
		waitforSpinner();
		try {
			verifyTradeInDeviceConditionPageURL();
			checkPageIsReady();
			Reporter.log("Navigated to Trade In Confirmation Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade In Confirmation Page");
		}
		return this;
	}

	/**
	 * Click On Agree and Continue Button
	 */
	public TradeInConfirmationPage clickAgreeContinue() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(agreeContinueButton));
			agreeContinueButton.click();
			Reporter.log("Failed to verify Continue button in Trade in Confirmation good page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Continue button in Trade in Confirmation good page");
		}
		return this;
	}

	/**
	 * Verify 'Want to trade in a different phone?' link not display for Jump User
	 */
	public TradeInConfirmationPage verifyWantToTradeDifferentPhoneLineLinkNotDisplay() {
		try {
			waitforSpinner();
			Assert.assertFalse(linkLineTradeInDifferentPhone.isDisplayed(),
					"'Want to trade in a different phone? Link text is present in Phone selection page.");
			Reporter.log("'Want to trade in a different phone? Link is not Displayed");
		} catch (Exception e) {
			Assert.fail(" 'Want to trade in a different phone? link text is displaying.");
		}
		return this;
	}

	/**
	 * Verify Eligibility Message which is authorable and dynamic
	 */

	public TradeInConfirmationPage verifyEligibilityMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(eigibilityMessage.isDisplayed(), "Eligibility Message is not displayed");
			Reporter.log("trade in value message with bill credit amount ('Yes!! You're qualified for the $X bill credit for your old phone') is displayed");
		} catch (Exception e) {
			Assert.fail(
					"trade in value message with bill credit amount ('Yes!! You're qualified for the $X bill credit for your old phone') is not displayed");
		}
		return this;
	}

	/**
	 * Click On Continue TradeIN Button
	 */
	public TradeInConfirmationPage clickContinueTradeInButton() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(continueTradeINButton));
			continueTradeINButton.click();
			Reporter.log("Continue TradeIN Button is clickable");
		} catch (Exception e) {
			Assert.fail("Continue TradeIN Button is not clickable");
		}
		return this;
	}

	/**
	 * Click On Continue TradeIN Button
	 */
	public TradeInConfirmationPage clickOnNoThanksChangeMyMindButton() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(noThanksIChangedMyMind));
			noThanksIChangedMyMind.click();
			Reporter.log("No thanks i change my mind button is clicked");
		} catch (Exception e) {
			Assert.fail("No thanks i change my mind button is not clicked");
		}
		return this;
	}

	/**
	 * Verify Remaining Line Not Displayed
	 * 
	 * @return
	 */
	public TradeInConfirmationPage verifyRemainingLineNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(remainingLineNotDisplay.isDisplayed(), "Remaining Line Not Displayed is displayed");
			Reporter.log("Remaining Line is not displayed");
		} catch (Exception e) {
			Assert.fail("Remaining Line is displayed");
		}
		return this;
	}

	/**
	 * Click On Trade In This Phone Button
	 */
	public TradeInConfirmationPage clickOnTradeInThisPhoneButton() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(tradeInThisPhoenButton));
			tradeInThisPhoenButton.click();
			Reporter.log("Clicked on Trade in this Phone Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Trade in this phone CTA");
		}
		return this;
	}

	/**
	 * Verify trade in value message bullet points
	 */

	public TradeInConfirmationPage verifyTradeInValueMessageBulletPoints() {
		try {
			Assert.assertTrue(tradeInValueMessage.isDisplayed(), "Trade in value message is not displayed");
			for (WebElement bulletPoints : tradeInValueMessageBulletPoints) {
				Assert.assertTrue(bulletPoints.isDisplayed(), "Trade in value All Bullet points not displayed");
			}
			Reporter.log("Trade in value All Bullet points displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in value All Bullet points");
		}
		return this;
	}

	/**
	 * Verify legal text is displayed
	 */

	public TradeInConfirmationPage verifyLegaltextDisplayed() {
		try {
			Assert.assertTrue(legalText.isDisplayed(), "legal Text is not displayed");
			Reporter.log("Legal text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify legal text");
		}
		return this;
	}

	/**
	 * Verify skip Trade In CTA
	 * 
	 * @return
	 */
	public TradeInConfirmationPage verifySkipTradeInButton() {
		try {
			Assert.assertTrue(skipTradeInButton.isDisplayed(), "Skip Trade in Button is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Skip Trade in Button");
		}
		return this;
	}

	/**
	 * verify Trade In this phone Button Displayed
	 */
	public TradeInConfirmationPage verifyTradeInThisPhoneButtonDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(tradeInThisPhoenButton.isDisplayed(), "Trade in this phone Button is not displayed");
			Reporter.log("Trade in this phone Button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade in this phone Button");
		}
		return this;
	}

	/**
	 * Verify InEligibility Message which is authorable and dynamic
	 */

	public TradeInConfirmationPage verifyInEligibilityMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(eigibilityMessage.isDisplayed(), "Ineligibility Message is not displayed");
			Assert.assertTrue(eigibilityMessage.getText().contains("$"), "Ineligibility Message is not displayed");
			Assert.assertTrue(eigibilityMessage.getText().contains("Due to the condition of this device"),
					"Ineligibility Message is not displayed");
			Reporter.log("Ineligibility message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to displayed Ineligibility message");
		}
		return this;
	}
	
	/**
	 * Verify Partial EligibilityMessage which is authorable and dynamic
	 */

	public TradeInConfirmationPage verifyPartialEligibilityMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(eigibilityMessage.isDisplayed(), "Partial eligibility Message is not displayed");
			Assert.assertTrue(eigibilityMessage.getText().contains("$"), "Partial eligibility Message is not displayed");
			//String storedCardNo = eigibilityMessage.getText().replaceAll("[^0-9]", "");
			Assert.assertTrue (Integer.parseInt(eigibilityMessage.getText().replaceAll("[^0-9]", "")) >= 0,"Partial eligibility price is not >0");
			Assert.assertTrue(eigibilityMessage.getText().contains("Due to the condition of this device"),
					"Partial eligibility Message is not displayed");
			Reporter.log("Partial eligibility message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to displayed Partial eligibility message");
		}
		return this;
	}
	
	/**
	 * Verify $0 tradein message 
	 */

	public TradeInConfirmationPage verifyDollarZeroTradeinMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(eigibilityMessage.isDisplayed(), "Ineligibility Message is not displayed");
			Assert.assertTrue(eigibilityMessage.getText().contains("$0.00"), "Zero dollar tradein message is not displayed");
			Assert.assertTrue(eigibilityMessage.getText().contains("Due to the condition of this device"),
					"Ineligibility Message is not displayed");
			Reporter.log("Ineligibility message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to displayed Ineligibility message");
		}
		return this;
	}
	/**
	 * Click On Continue Button
	 * 
	 * @return
	 */
	public TradeInConfirmationPage clickOnTradeInThisPhone() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(tradeInThisPhone);
			Reporter.log("Clicked on Trade-In This Phone button");
		} catch (Exception e) {
			Assert.fail("Trade-In This Phone button is not Dispalyed to click");
		}
		return this;

	}
}
