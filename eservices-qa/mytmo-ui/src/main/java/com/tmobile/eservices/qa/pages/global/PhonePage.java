package com.tmobile.eservices.qa.pages.global;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 *
 */
public class PhonePage extends CommonPage {

	@FindBy(css = ".clearfix a")
	private List<WebElement> linksList;

	@FindBy(xpath = "//button[text()='Next']")
	private List<WebElement> nextBtn;

	@FindBy(id = "radio1")
	private WebElement yourDeviceIsLostRadioBtn;

	@FindBy(id = "radio2")
	private WebElement yourDeviceIsStolenRadioBtn;

	@FindBy(id = "suspendY")
	private List<WebElement> suspendYesBtn;

	@FindBy(id = "acceptedTnC")
	private WebElement termsAndConditionsCheckBox;

	@FindBy(id = "confirmRLS")
	private WebElement submitBun;

	@FindBy(id = "loststolen")
	private WebElement loststolenSuccessfulMessage;

	@FindBy(className = "ui_primary_link")
	private WebElement goBackToPhonePageBtn;

	@FindBy(linkText = "I found my device")
	private List<WebElement> iFoundMyDevice;

	@FindBy(id = "unblockrestoreDevice")
	private WebElement restoreBtn;

	@FindBy(id = "ok")
	private WebElement okBtn;

	@FindBy(id = "myphonedetailsheadertext")
	private WebElement phonePage;

	@FindBy(xpath = "//a[text()='Change SIM']")
	private WebElement changeSimMenuLink;

	@FindBy(css = "div.discription")
	private WebElement insufficientPermissionsHeader;

	@FindBy(css = ".warning-message")
	private WebElement insufficientPermissionsHeaderForIOS;

	@FindAll({ @FindBy(css = "div#checkOrderStatus a"), @FindBy(css = "a#checkOrderStatus") })
	private WebElement checkOrderstatus;

	@FindBy(id = "phpSection")
	private WebElement phpStatusOnPhonePage;

	@FindBy(id = "php_claim_url")
	private WebElement fileADamageClaimLink;

	@FindAll({ @FindBy(id = "idbackbtn"), @FindBy(id = "promoofferCancelBtn") })
	private WebElement cancelCTAOfFileADamageClaimPopUp;

	@FindAll({ @FindBy(id = "idcontinuebtn"), @FindBy(id = "idContinueButton") })
	private WebElement continueCTAOfFileADamageClaimPopUp;

	@FindBy(xpath = "(.//*[(@id='jump_waitCursor') and ((@style='display: none;'))])")
	private WebElement spinnerOnPhonePage;

	@FindBy(xpath = "(.//*[(@id='quickLinkChanges') and (@style='display: block;')])")
	private WebElement quickLinkBlockOnPhonePage;

	@FindBy(xpath = "(.//img[@alt='Assurant Solutions'])")
	private WebElement assurantImageOnMyPHPInfoPage1;

	@FindBy(id = "MP_device_title")
	public WebElement deviceName;

	@FindAll({ @FindBy(css = "#feature_desc li") })
	private List<WebElement> featuresList;

	@FindBy(css = "li#linePickerList a[id='1']")
	private WebElement selectDigitsLine;

	@FindBy(css = "li#linePickerList a[id='2']")
	private WebElement selectISPLine;

	@FindBy(css = "a[id='1'].subscriberLine")
	private WebElement selectDigitsLineForIOS;

	@FindBy(css = "div#di_lineselector")
	private WebElement selectALineDropDown;

	@FindBy(css = "#di_mobilelineselector .DropArwPadding")
	private WebElement selectALineDropDownForIOS;

	@FindBy(xpath = "//img[@title='iPhone XR - Blue - 64GB']")
	public WebElement iphoneXRImage;

	@FindBy(xpath = "//img[@title='Unknown Device']")
	public WebElement undefinedImage;

	@FindBy(xpath = "//img[@title='Galaxy S7 edge - Blue Coral - 32GB']")
	public WebElement samsungImage;

	@FindAll({ @FindBy(css = "div[id='di_lineselector']"),
	@FindBy(css = "img[alt='Change line']")})
	public WebElement selectLine;

	private String assurantImageOnMyPHPInfoPage = "(.//img[@alt='Assurant Solutions'])";

	@FindBy(xpath = "(//*[@id='idInfoModalForAssurantSiteModal']/div)[1]")
	private WebElement pdpModalText;

	@FindBy(xpath = "(.//*[@id='lostStolenHeaderSec'])")
	private WebElement headerOfReportLostAndStolenPage;

	@FindBy(xpath = "(.//*[@id='phpSocName'])")
	private WebElement phpSOCNameOnReportLostAndStolenPage;

	@FindBy(css = "div.pull-left.title.pt10")
	private List<WebElement> verifyCTAonPhonePage;

	@FindBy(css = "li#linePickerList a[id='1']")
	private WebElement nextLine;

	@FindBy(css = "#MP_msisdn")
	private WebElement selectedLine;

	@FindBy(id = "deviceLockStatusLink")
	private WebElement checkDeviceUnlockStatusLink;

	@FindBy(id = "deviceLockStatusLink")
	private WebElement deviceUnlockStatusLink;

	@FindBy(css = "a.legal-bold-link.black")
	private WebElement unlockPolicyLink;

	@FindBy(css = "//p[contains(text(),'Device unlocked')]")
	private WebElement deviceUnlocktext;

	@FindBy(css = "span#MP_msisdn")
	private WebElement linkedMobileNumber;

	@FindBy(css = "img[src*='internet-devices']")
	private WebElement deviceStatusImg;

	@FindBy(css = "button.TeritiaryCTA")
	private WebElement checkADeviceNotOnYourAccount;

	@FindBy(css = "span[aria-label*='checkDeviceHeading']")
	private WebElement checkYourDevicePage;

	@FindBy(css = "a[class='subscriberLine']")
	private List<WebElement> lineList;

	@FindBy(id = "inputText")
	private WebElement iMEINumberText;

	@FindBy(className = "PrimaryCTA")
	private WebElement checkDeviceButton;

	@FindBy(css = "p[aria-label='Status unknown']")
	private WebElement statusUnknown;

	@FindBy(css = "div[class*='unknown-icon']")
	private WebElement unknownIcon;

	@FindBy(css = "div.statusmessage")
	private WebElement statusMessage;

	@FindBy(css = "div.statusmessage :nth-child(1)")
	private WebElement statusMessageChild1;

	@FindBy(css = "div.statusmessage :nth-child(2)")
	private WebElement statusMessageChild2;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement btnContactUS;

	@FindBy(css = "button.TeritiaryCTA")
	private WebElement btnCheckADeviceNotOnYourAccount;

	@FindBy(css = "img.unknown-device-img")
	private WebElement imgUnknownDevice;

	@FindBy(css = "p.padding-top-large")
	private WebElement deviceStatus;

	@FindBy(css = "div.padding-bottom-xl p:nth-child(3)")
	private WebElement imeiNumber;

	@FindBy(xpath = "(.//img[@alt='manage Solutions'])")
	private WebElement digitsSettingsCTA;

	@FindBy(xpath = "(.//img[@alt='Assurant Solutions'])")
	private WebElement deviceUnlockBlade;

	@FindBy(xpath = "(.//img[@alt='Assurant Solutions'])")
	private WebElement lineSelectorDiv;

	@FindBy(css = "#shop_now")
	private WebElement upgradeCTA;

	@FindBy(css = "#tradeinNow_id > .ui_primary_link")
	private WebElement tradeInLink;

	/**
	 * Click on link by name
	 * 
	 * @return
	 */
	public PhonePage updateDeviceLostOrStolen(String linkName) {
		try {
			checkPageIsReady();
			clickOnLinkByName("Report lost or stolen");
			verifyLostAndStolenPage();
			nextBtn.get(0).click();
			verifyDeviceRadioBtnAndClickOnIt(linkName);
			nextBtn.get(1).click();
			if (!suspendYesBtn.isEmpty() && suspendYesBtn.get(0).isDisplayed()) {
				clickOnSuspendYesBtn();
				nextBtn.get(2).click();
				termsAndConditionsCheckBox.click();
				nextBtn.get(3).click();
			}
			submitBun.click();
			waitforSpinner();
			Assert.assertTrue(loststolenSuccessfulMessage.getText().contains("You have reported this device as"),
					"lost or stolen message not displaying ");
			goBackToPhonePageBtn.click();
			checkPageIsReady();
			verifyPhonePage();
		} catch (Exception e) {
			Assert.fail(linkName + " link is not clicked.");
		}
		return this;
	}

	/**
	 * Click on suspend Yes Btn
	 * 
	 * @return
	 */
	public PhonePage verifyiFoundMyDeviceAndRestoreIt() {
		try {
			if (!iFoundMyDevice.isEmpty()) {
				iFoundMyDevice.get(0).click();
				waitFor(ExpectedConditions.visibilityOf(restoreBtn));
				clickElementWithJavaScript(restoreBtn);
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader zindex")));
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("device_waitCursor")));
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf(okBtn));
				clickElementWithJavaScript(okBtn);
				Reporter.log("I Found My Device");
			}
		} catch (Exception e) {
			Assert.fail("I Found My Device button is not clicked.");
		}
		return this;
	}

	/**
	 * Click on suspend Yes Btn
	 * 
	 * @return
	 */
	public PhonePage clickOnSuspendYesBtn() {
		try {
			suspendYesBtn.get(0).click();
			Reporter.log("Clicked on Suspend yes button");
		} catch (Exception e) {
			Assert.fail("Suspend yes button is not clicked.");
		}
		return this;
	}

	/**
	 * Click on link by name
	 * 
	 * @return
	 */
	public PhonePage verifyDeviceRadioBtnAndClickOnIt(String radioBtnName) {
		try {
			if (radioBtnName.contains("lost")) {
				yourDeviceIsLostRadioBtn.click();
				Reporter.log("Clicked on lost name");
			} else {
				yourDeviceIsStolenRadioBtn.click();
				Reporter.log("Clicked on stolen name");
			}
		} catch (Exception e) {
			Assert.fail("Radio button not clicked");
		}
		return this;
	}

	/**
	 * Click on link by name
	 * 
	 * @return
	 */
	public PhonePage clickOnLinkByName(String linkName) {
		try {
			clickElementBytext(linksList, linkName);
			Reporter.log("Clicked on " + linkName + " name");
		} catch (Exception e) {
			Assert.fail(linkName + " link is not clicked.");
		}
		return this;
	}

	/*
	 * Verify Lost And Stolen Page
	 * 
	 * @return
	 */
	public PhonePage verifyLostAndStolenPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("loststolen.forward.html"),
					"Lost And Stolen page not displayed");
			Reporter.log("Lost And Stolen page is displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Lost And Stolen page not displayed");
		}
		return this;
	}

	/**
	 * @param webDriver
	 */

	@SuppressWarnings("unlikely-arg-type")
	public PhonePage verifyPhoneCTA() {
		int n = 0;

		try {

			checkPageIsReady();

			n = verifyCTAonPhonePage.size();

			if (n <= 2) {

				Boolean ctaOne = verifyCTAonPhonePage.contains("Report lost or stolen");
				Boolean ctaTwo = verifyCTAonPhonePage.contains("Temporary suspension");

				if ((ctaOne == true) && (ctaTwo == true)) {
					Reporter.log("Report lost or stolen and Temporary suspension CTA is Presented in Phone Page");
				} else {
					Reporter.log("Report lost or stolen and Temporary suspension CTA is not Presented in Phone Page");
				}
			} else {
				Reporter.log("MoreThan Two CTA are Presented in Phone Page");

			}
		} catch (Exception e) {

			Assert.fail("There is no CTA is there in Phone CTA");

		}
		return this;
	}

	public PhonePage selectISPline() {
		waitforSpinner();
		try {
			checkPageIsReady();
			selectALineDropDown.click();
			Reporter.log("Cliked on the selectLieDropDown");
			checkPageIsReady();
			selectISPLine.click();
			Reporter.log("Selected ISP line from the DropDown");
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("Unable to select the ISP line from the DropDown");

		}
		return this;
	}

	public PhonePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Phone Page
	 * 
	 * @return
	 */
	public PhonePage verifyPhonePage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains("myphone/myphonedetails"),
					"Phone page not displayed");
			Reporter.log("Phone page is displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Phone page not displayed");
		}
		return this;

	}

	public PhonePage navigateBackToDeviceStatusPage() {
		try {
			getDriver().navigate().back();
			Reporter.log("Naviagted back to device status page");
			// verifyDeviceUnlockPage();
		} catch (NoSuchElementException e) {
			Reporter.log("Unable to Naviagted back to device status page");
		}
		return this;

	}

	/**
	 * Verify Change sim menu link
	 */
	public PhonePage verifyChangeSimMenuLink() {
		waitforSpinner();
		Assert.assertFalse(isElementDisplayed(changeSimMenuLink), "Change sim menu link is displayed");
		Reporter.log("Change sim menu link is not displayed");
		return this;
	}

	/**
	 * Verify Insufficient Permissions Message
	 */
	public PhonePage verifyInsufficientPermissionsMessage() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitforSpinner();
				moveToElement(insufficientPermissionsHeaderForIOS);
				Assert.assertTrue(isElementDisplayed(insufficientPermissionsHeaderForIOS),
						"Insufficient permission message not displayed");
				Reporter.log("Insufficient permission message displayed");

			} else {
				waitforSpinner();
				Assert.assertTrue(isElementDisplayed(insufficientPermissionsHeader),
						"Insufficient permission message not displayed");
				Reporter.log("Insufficient permission message displayed");
			}
		} catch (Exception e) {
			Assert.fail("Insufficient permission message not displayed " + e.getMessage());
		}
		;
		return this;
	}

	/**
	 * Click Change sim menu link
	 */
	public PhonePage clickChangeSimMenuLink() {
		waitforSpinner();
		try {
			clickElementWithJavaScript(changeSimMenuLink);
			Reporter.log("Clicked on change sim menu link");
		} catch (Exception e) {
			Reporter.log("Click on change sim menu link failed" + e.getMessage());
		}
		return this;
	}

	/**
	 * click Check Order Status Link
	 */
	public PhonePage clickCheckOrderStatusLink() {
		waitforSpinner();
		try {
			clickElementWithJavaScript(checkOrderstatus);
			Reporter.log("Clicked on check order status link");
		} catch (Exception e) {
			Reporter.log("Click on check order status link failed" + e.getMessage());
		}
		return this;
	}

	/**
	 * click Check PHP status
	 */
	public PhonePage checkPHPStatusWhenThereIsNoPHPSOCOnLine() {
		String phpStatus;
		checkPageIsReady();
		phpStatus = phpStatusOnPhonePage.getText();
		Assert.assertEquals(phpStatus, "No Device Protection",
				"Incorrect PHP Status shown on Phone page when user has not any PHP SOC on line");
		return this;
	}

	/**
	 * click Check PHP status
	 */
	public PhonePage checkPHPStatusWhenThereIsPHPSOCOnLine() {
		String phpStatus;
		phpStatus = phpStatusOnPhonePage.getText();
		Assert.assertEquals(phpStatus, "SOC Name",
				"Incorrect PHP Status shown on Phone page when user has any PHP SOC on line");
		return this;
	}

	/**
	 * click Check PHP status
	 * 
	 * @throws InterruptedException
	 */
	public PhonePage clickOnFileDamageLinkAndVerifyFunctionalityForAssurantSOC() throws InterruptedException {
		checkPageIsReady();
		fileADamageClaimLink.click();
		verifyPDPModalMessage();
		cancelCTAOfFileADamageClaimPopUp.click();
		fileADamageClaimLink.click();
		continueCTAOfFileADamageClaimPopUp.click();
		waitForSpinner(assurantImageOnMyPHPInfoPage);
		String url = getDriver().getCurrentUrl();
		Assert.assertEquals(url, "https://myphpinfo.com/?lang=en",
				"URL mismatched for Assurant SOC when user clicked Continue on File Damage claim Pop up");
		if ((url.contains("https://myphpinfo.com/?lang=en")))
			Reporter.log("PHP Info page is displayed for Assurant SOC");
		else
			Assert.fail(
					"After clicking on File a Damage link on Phone page for Assurant SOC, it's not redirecting to expected page");
		return this;
	}

	/**
	 * click Check PHP status
	 * 
	 * @throws InterruptedException
	 */
	public PhonePage clickOnFileDamageLinkAndVerifyFunctionalityForAssurionSOC() throws InterruptedException {
		checkPageIsReady();
		fileADamageClaimLink.click();
		verifyPDPModalMessage();
		cancelCTAOfFileADamageClaimPopUp.click();
		fileADamageClaimLink.click();
		continueCTAOfFileADamageClaimPopUp.click();
		checkPageIsReady();
		String url = getDriver().getCurrentUrl();

		if ((url.contains("myphpinfo.com/?lang=en"))) {
			Reporter.log("PHP Info page is displayed for Assurion SOC");
		} else {
			Assert.fail(
					"After clicking on File a Damage link on Phone page for Assurion SOC, it's not redirecting to expected page");
		}
		return this;
	}

	/**
	 * click Check PHP status
	 * 
	 * @throws InterruptedException
	 */
	public PhonePage verifyPDPModalMessage() throws InterruptedException {
		checkPageIsReady();

		String actualTextOfPDPModal = pdpModalText.getText();

		if ((actualTextOfPDPModal.contains("You’ll now be directed to claims website – https://www.mytmoclaim.com")))
			Reporter.log("Correct message displayed on PDP Modal");
		else
			Reporter.log("InCorrect message displayed on PDP Modal");
		waitFor(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("(//*[@id='idInfoModalForAssurantSiteModal']/div)[1]")));
		if ((actualTextOfPDPModal.contains("What’s next?")))
			Reporter.log("Correct message displayed on PDP Modal");
		else
			Reporter.log("InCorrect message displayed on PDP Modal");
		return this;
	}

	public PhonePage waitForSpinner(String image) throws InterruptedException {
		waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(image)));
		return this;
	}

	/*
	 * Get PHP SOC on Phone page
	 * 
	 * 
	 */
	public String getPHPSOCOfLineFromPhonePage() {
		String actualPHPStatusOnPhonePage;
		actualPHPStatusOnPhonePage = phpStatusOnPhonePage.getText();
		// System.out.println("PHP SOC Code on Phone page is " +
		// actualPHPStatusOnPhonePage);
		return actualPHPStatusOnPhonePage;
	}

	// Check PHP SOC on Lost and Stolen page and compare it with PHP SOC on
	// Phone Page
	public PhonePage verifyPHPSOCOnLostAndStolenPageWithPHPSOCOnPhonePage(String phpSOCOnPhonePage) {
		try {
			String actualPHPSOCOnLostAndStolenPage = phpSOCNameOnReportLostAndStolenPage.getText();
			// System.out.println("PHP SOC Code on Phone page is " +
			// actualPHPSOCOnLostAndStolenPage);
			Assert.assertEquals(phpSOCOnPhonePage, actualPHPSOCOnLostAndStolenPage,
					"Incorrect PHP Status shown on Lost And Stolen page");
			Reporter.log("Correct PHP Status shown on Lost And Stolen page");
		} catch (Exception e) {
			Assert.fail("Incorrect PHP Status shown on Lost And Stolen page");
		}
		return this;
	}

	/*
	 * Verify PHP SOC on Lost and stolen page.
	 *
	 */
	public PhonePage checkPHPSOCOnLostAndStolenPage() {
		String phpSOCCodeOnPhonePage;
		// PlanPage planpage = new PlanPage(getDriver());
		// planpage.verifyPlanpage();
		phpSOCCodeOnPhonePage = getPHPSOCOfLineFromPhonePage();
		clickOnLinkByName("Report lost or stolen");
		verifyLostAndStolenPage();
		checkPageIsReady();
		nextBtn.get(0).click();
		verifyLostAndStolenPage();
		checkPageIsReady();
		verifyPHPSOCOnLostAndStolenPageWithPHPSOCOnPhonePage(phpSOCCodeOnPhonePage);
		return this;
	}

	/**
	 * Select digits line
	 */
	public PhonePage selectDigitsLine() {
		if (getDriver() instanceof AppiumDriver) {
			try {
				waitforSpinner();
				clickElementWithJavaScript(selectALineDropDownForIOS);
				checkPageIsReady();
				clickElementWithJavaScript(selectDigitsLineForIOS);
			} catch (Exception e) {
				Assert.fail("Click on digits line failed ");
			}
		} else {
			try {
				waitforSpinner();
				clickElementWithJavaScript(selectALineDropDown);
				checkPageIsReady();
				clickElementWithJavaScript(selectDigitsLine);
			} catch (Exception e) {
				Assert.fail("Click on digits line failed ");
			}
		}
		return this;
	}

	/**
	 * Change to next line digits line
	 */
	public PhonePage changeToNextLine() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(selectALineDropDown);
			checkPageIsReady();
			clickElementWithJavaScript(nextLine);
		} catch (Exception e) {
			Assert.fail("Change to next line failed ");
		}
		return this;
	}

	/**
	 * get selected line
	 */
	public String getSelectedLine() {
		waitforSpinner();
		String s1 = selectedLine.getText().trim();
		String selectedLine = s1.replace("(", "").replace(")", "").replace("-", "").replace(" ", "").trim();
		return selectedLine;
	}

	/**
	 * Click Device Unlock Status Link
	 */
	public PhonePage clickDeviceUnlockStatusLink() {
		try {
			deviceUnlockStatusLink.click();
			Reporter.log("Clicked on Check Device Unlock");
		} catch (Exception e) {
			Assert.fail("Check Device Unlock Status Link is not clicked.");
		}
		return this;
	}

	/**
	 * Check on T-mobile Unlock policy Link
	 */
	public PhonePage unlockPolicyLink() {
		try {
			unlockPolicyLink.click();
			Reporter.log("Check Device Unlock Status Link is clicked.");
		} catch (Exception e) {
			Assert.fail("Check Device Unlock Status Link is not clicked.");
		}
		return this;
	}

	/**
	 * 0pen the popup window by performing some action and then resume back to old
	 * window
	 */
	public PhonePage controlToPopUpNewWindow_UnlockPolicy() {
		try {
			String mwh = getDriver().getWindowHandle();
			unlockPolicyLink.click();
			Set<?> s = getDriver().getWindowHandles();
			Iterator<?> ite = s.iterator();
			while (ite.hasNext()) {
				String popupHandle = ite.next().toString();
				if (!popupHandle.contains(mwh)) {
					getDriver().switchTo().window(popupHandle);
					verifSupportPage();
					getDriver().switchTo().window(mwh);
				}
			}
			Reporter.log("T-Mobile's unlock policy unable to linked to the unlock policy");

		} catch (Exception e) {
			Assert.fail("T-Mobile's unlock policy unable to linked to the unlock policy");
		}

		return this;
	}

	/*
	 * Verify Device Unlock Page
	 * 
	 * @return
	 */
	public PhonePage verifyDeviceUnlockPage() {
		checkPageIsReady();
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("modal_message_loading")));
		waitFor(ExpectedConditions.visibilityOf(deviceStatus));
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("phone/device-status"),
					"Device Unlock page displayed");
			Reporter.log("Device Unlock page is displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Device Unlock page not displayed");
		}
		return this;

	}

	/*
	 * Verify https://support.t-mobile.com/docs/DOC-1588
	 * 
	 * @return
	 */
	public PhonePage verifSupportPage() {
		checkPageIsReady();
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("/docs/DOC-1588"), "Support page not displayed");
			Reporter.log("Support page is displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Support page not displayed");
		}
		return this;

	}

	/**
	 * Verify suspended missdn number in line selector dropdown.
	 */
	public PhonePage verifySuspendedMissdnNumberInLineSelectorDropDown(String phoneNumber) {
		try {
			String s = linkedMobileNumber.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			Assert.assertEquals(s, phoneNumber);
			Reporter.log("Suspened account missdn matched with line selector dropdown missdn");
		} catch (AssertionError e) {
			Assert.fail("Suspened account missdn not matched with line selector dropdown missdn");
		}
		return this;
	}

	/**
	 * Click nick name link.
	 */
	public PhonePage selectLineFromDropDown(int index) {
		try {
			waitforSpinnerinProfilePage();
			selectLine.click();
			for (int i = 0; i < lineList.size(); i++) {
				if (i == index - 1) {
					lineList.get(i).click();
				}
			}

			Reporter.log("selected other line");
		} catch (Exception e) {
			Assert.fail("Selecting other line failed ");
		}
		return this;
	}

	public PhonePage verifyDeviceStatusImg() {
		try {
			Assert.assertTrue(deviceStatusImg.isDisplayed(), "");
			Reporter.log("Device status image page is displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Device status image is not displayed");
		}
		return this;
	}

	public PhonePage clickOnCheckADeviceNotOnYourAccount() {
		try {
			checkADeviceNotOnYourAccount.click();
			Reporter.log("Clicked on check a Device Not On Your Account");
		} catch (NoSuchElementException e) {
			Reporter.log("Check a Device Not On Your Account button is not displayed");
		}
		return this;
	}

	public PhonePage verifyCheckYourDevicePage() {
		checkPageIsReady();
		try {
			checkYourDevicePage.isDisplayed();
			Reporter.log("Verify check Your Device Page");
		} catch (NoSuchElementException e) {
			Reporter.log("Verify check Your Device Page button is not displayed");
		}
		return this;
	}

	public PhonePage enteriMEINumberText() {
		try {
			iMEINumberText.sendKeys("359214079137281");
			Reporter.log("Entered IMEI number");
		} catch (NoSuchElementException e) {
			Reporter.log("IMEI number text is not displayed");
		}
		return this;
	}

	public PhonePage clickOnCheckDeviceButton() {
		waitforSpinner();
		checkPageIsReady();
		try {
			checkDeviceButton.click();
			Reporter.log("Clicked on check Device Button");
		} catch (NoSuchElementException e) {
			Reporter.log("check Device Button is not displayed");
		}
		return this;
	}

	public PhonePage verifyStatusUnknown() {
		checkPageIsReady();
		try {
			Assert.assertTrue(statusUnknown.isDisplayed(), "Status Unknown is not displayed");
			Assert.assertTrue(unknownIcon.isDisplayed(), "Unknown icon is not displayed");
			Reporter.log("Status Unknown is dispplayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Status Unknown is not displayed");
		}
		return this;
	}

	public PhonePage enteriMEINumberText(String imeiNumber) {
		waitforSpinner();
		if (!imeiNumber.isEmpty()) {
			try {
				iMEINumberText.sendKeys(imeiNumber);
				Reporter.log("Entered IMEI number");
			} catch (NoSuchElementException e) {
				Reporter.log("IMEI number text is not displayed");
			}
		} else {
			Reporter.log("There is no IMEI number pass in enteriMEINumberText method in PhonePage");
		}
		return this;
	}

	public PhonePage enterListOfImeiNumberWhenTemporarilyUnlocked(List<String> imeiNumber) {
		if (!imeiNumber.isEmpty()) {
			ContactUSPage contactUSPage = new ContactUSPage(getDriver());
			for (int i = 0; i < imeiNumber.size(); i++) {
				switch (i + 1) {
				case 1:
					Reporter.log("Scenario 1:");
					Reporter.log("Verifying Device is temporarily unlocked and eligible for permanent unlock");

					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Device unlocked");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatus("Temporarily unlocked from");
							verifyStatus("to");
							verifyStatus("Your device is eligible to be permanently unlocked");
							Reporter.log("Verifying Date Format in Authorable Text");
							verifyDateFormatInauthorableText();
							verifyContactUSButton();
							contactUSPage.verifyContactUSPage();
							navigateBackToDeviceStatusPage();
							// verifyCheckADeviceNotOnYourAccount();
							Reporter.log("Verified Device is temporarily unlocked and eligible for permanent unlock");
						} catch (NoSuchElementException e) {
							Reporter.log(
									"Unable to Verify Device is temporarily unlocked and eligible for permanent unlock scenario");
						}
					} else {
						// Assert.fail("IMEI is null for Verifying Device is
						// temporarily unlocked and eligible for permanent
						// unlock ");
					}
					break;

				case 2:

					Reporter.log("Scenario 2:");
					Reporter.log(
							"Test Case : Verifying Device is temporarily unlocked and NOT eligible for permanent unlock");

					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Device unlocked");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatus("Temporarily unlocked from");
							verifyStatus("to");
							verifyStatus("Your device is not eligible to be permanently unlocked");
							Reporter.log("Verifying Date Format in Authorable Text");
							verifyDateFormatInauthorableText();
							verifyBackToPhonePageButton();

						} catch (NoSuchElementException e) {
							Reporter.log(
									"Unable to Verify Device is temporarily unlocked and NOT eligible for permanent unlock");
						}
					} else {
						Assert.fail(
								"IMEI is null for Verifying Device is temporarily unlocked and NOT eligible for permanent unlock ");
					}
					break;
				default:
					Reporter.log("Test Case : Invalid input. There is no scenario for this input");
					break;
				}
			}
		} else {
			Reporter.log("There is no IMEI number pass in enterListOfImeiNumber method in PhonePage");
		}
		return this;
	}

	public PhonePage enterImeiNumberForPermanentlyUnlocked(String imeiNumber) {
		if (!imeiNumber.isEmpty() && imeiNumber != null) {
			try {
				enteriMEINumberText(imeiNumber);
				clickOnCheckDeviceButton();
				verifyDeviceStatus("Device unlocked");
				verifyIemeiNumber(imeiNumber);
				verifyStatus("Your device is unlocked and can be used on all compatible networks.");
				verifyBackToPhonePageButton();
			} catch (NoSuchElementException e) {
				Reporter.log("Unable to verify  when Device is Permanently unlocked");
			}
		} else {
			Assert.fail("IMEI is null for when Device is Permanently unlocked ");
		}
		return this;
	}

	public PhonePage enterImeiNumberForLockStatusUnknown(List<String> imeiNumber) {
		if (!imeiNumber.isEmpty() && imeiNumber != null) {
			ContactUSPage contactUSPage = new ContactUSPage(getDriver());
			for (int i = 0; i < imeiNumber.size(); i++) {
				switch (i + 1) {
				case 1:
					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							Reporter.log("Test Case : Verifying when Lock Status Unknown");
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Status unknown");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatus("We were unable to locate information about your device's unlock status");
							verifyContactUSButton();
							verifyContactUSButton();
							contactUSPage.verifyContactUSPage();
							navigateBackToDeviceStatusPage();
							verifyCheckADeviceNotOnYourAccount();
						} catch (NoSuchElementException e) {
							Reporter.log("Unable to Verify when Lock Status Unknown");
						}
					} else {
						// Assert.fail("IMEI is null for Lock Status Unknown ");
					}
					break;
				case 2:
					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							Reporter.log("Test Case : Verifying Lock Status Unknown for US578313");
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Status unknown");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatusMessage1("This device is not in our system.");
							unlockPolicyLink();
							verifyBackToPhonePageButton();
						} catch (NoSuchElementException e) {
							Reporter.log("Unable to Verify Lock Status Unknown for US578313");
						}
					} else {
						Assert.fail("IMEI is null for Lock Status Unknown for US578313");
					}
					break;
				default:
					Reporter.log("Test Case : Invalid input. There is no scenario for this input");
					break;
				}
			}

		} else {
			Reporter.log("There is no IMEI number pass in enterListOfImeiNumber method in PhonePage");
		}
		return this;
	}

	public PhonePage enterListOfImeiNumberForLockedDevice(List<String> imeiNumber) {
		checkPageIsReady();
		if (!imeiNumber.isEmpty()) {
			ContactUSPage contactUSPage = new ContactUSPage(getDriver());
			for (int i = 0; i < imeiNumber.size(); i++) {
				switch (i + 1) {
				case 1:

					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							Reporter.log("");
							Reporter.log(
									"Test Case : Verifying The device is locked and eligible for permanent unlock and can NOT be unlocked remotely");
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Device locked");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatus(
									"Your device is eligible to be unlocked. Contact us to request a device unlock.");
							verifyContactUSButton();
							contactUSPage.verifyContactUSPage();
							navigateBackToDeviceStatusPage();
							verifyCheckADeviceNotOnYourAccount();
						} catch (NoSuchElementException e) {
							Reporter.log(
									"Unable to Verify The device is locked and eligible for permanent unlock and can NOT be unlocked remotely");
						}
					} else {
						Assert.fail(
								"IMEI is null for The device is locked and eligible for permanent unlock and can NOT be unlocked remotely");
					}
					break;

				case 2:
					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							Reporter.log("");
							Reporter.log(
									"Test Case : Verifying The device is locked and eligible for permanent unlock and can be unlocked remotely");
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Device locked");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatusMessage1("Your device is eligible to be unlocked.");
							verifyStatusMessage2("Learn how to unlock this device.");
							verifyContactUSButton();
							// contactUSPage.verifyContactUSPage();
							// navigateBackToDeviceStatusPage();
							verifyCheckADeviceNotOnYourAccount();
						} catch (NoSuchElementException e) {
							Reporter.log(
									"Unable to Verify device is locked and eligible for permanent unlock and can be unlocked remotely");
						}
						break;
					} else {
						Assert.fail(
								"IMEI is null for device is locked and eligible for permanent unlock and can be unlocked remotely");
					}
					break;
				case 3:
					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							Reporter.log("");
							Reporter.log(
									"Test Case : Verifying The device is locked and eligible for temporary unlock");
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Device locked");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatus(
									"Your device is eligible to be temporarily unlocked for up to 30 days. Your device can only be temporarily unlocked 5 times");
							verifyContactUSButton();
							contactUSPage.verifyContactUSPage();
							navigateBackToDeviceStatusPage();
							verifyCheckADeviceNotOnYourAccount();
						} catch (NoSuchElementException e) {
							Reporter.log("Unable to Verify The device is locked and eligible for temporary unlock");
						}
						break;
					} else {
						// Assert.fail("IMEI is null for The device is locked
						// and eligible for temporary unlock ");
					}
					break;
				case 4:
					if (!imeiNumber.get(i).isEmpty() && imeiNumber.get(i) != null) {
						try {
							Reporter.log("Test Case : Verifying The device is locked and not eligible for unlock");
							enteriMEINumberText(imeiNumber.get(i));
							clickOnCheckDeviceButton();
							verifyDeviceStatus("Device locked");
							verifyIemeiNumber(imeiNumber.get(i));
							verifyStatus("This device is not currently eligible to be unlocked");
							verifyBackToPhonePageButton();
						} catch (NoSuchElementException e) {
							Reporter.log("Unable to Verify The device is locked and not eligible for unlock");
						}
					} else {
						Assert.fail("IMEI is null for Verifying The device is locked and not eligible for unlock");
					}
					break;
				default:
					Reporter.log("Test Case : Invalid input. There is no scenario for this input");
					break;
				}
			}
		} else {
			Reporter.log("There is no IMEI number pass in enterListOfImeiNumber method in PhonePage");
		}
		return this;
	}

	public PhonePage enterImeiNumberForUnkownDevice(String imeiNumber) {
		Reporter.log("Test Case : Verifying image for unkown device");
		if (!imeiNumber.isEmpty() && imeiNumber != null) {
			try {
				enteriMEINumberText(imeiNumber);
				clickOnCheckDeviceButton();
				verifyIemeiNumber(imeiNumber);
				verifyUnknownDeviceImage();
				clickOnCheckADeviceNotOnYourAccount();
				verifyCheckYourDevicePage();
			} catch (NoSuchElementException e) {
				Reporter.log("Unable to Verify image for unkown device");
			}
		} else {
			// Assert.fail("IMEI is null for image for unkown device");
		}
		return this;
	}

	private PhonePage verifyUnknownDeviceImage() {
		try {
			Assert.assertTrue(imgUnknownDevice.getAttribute("src").contains("unknown-device.svg"),
					"Unknown device image is Not displayed ");
			Reporter.log("Verified Unknown device image is displayed successfully");
		} catch (NoSuchElementException e) {
			Reporter.log("Verified unknown device image NOT displayed successfully");
		}
		return this;

	}

	public PhonePage verifyIemeiNumber(String string) {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(imeiNumber));
			Assert.assertTrue(imeiNumber.isDisplayed(), "Imei number is displayed");
			String imei = imeiNumber.getText();
			Assert.assertTrue(imei.contains(string),
					"Verifying whether Expected imeiNumber contains in expected result. Expected result is " + imei
							+ ". Actual response is" + string);
			Reporter.log("IEMEI number appearing on device status page");
		} catch (NoSuchElementException e) {
			Reporter.log("IEMEI number doesnot appear on device status page");
		}
		return this;

	}

	public List<String> getDateFromString(String str) {
		List<String> alist = new ArrayList<String>();
		;
		String regex = "(\\d{1,2})\\/(\\d{1,2})\\/(\\d{1,2})";
		Matcher m = Pattern.compile(regex).matcher(str);
		while (m.find()) {
			alist.add(m.group());
		}
		return alist;
	}

	private static final String DATE_PATTERN = "([1-9]|1[012])/([1-9]|[12][0-9]|3[01])/(\\d\\d)";

	public boolean validateDateFormat(final String date) {
		Pattern pattern = Pattern.compile(DATE_PATTERN);
		Matcher matcher = pattern.matcher(date);
		if (matcher.matches()) {

			matcher.reset();

			if (matcher.find()) {

				String day = matcher.group(1);
				String month = matcher.group(2);
				int year = Integer.parseInt(matcher.group(3));

				if (day.equals("31") && (month.equals("4") || month.equals("6") || month.equals("9")
						|| month.equals("11") || month.equals("04") || month.equals("06") || month.equals("09"))) {
					return false; // only 1,3,5,7,8,10,12 has 31 days
				} else if (month.equals("2") || month.equals("02")) {
					// leap year
					if (year % 4 == 0) {
						if (day.equals("30") || day.equals("31")) {
							return false;
						} else {
							return true;
						}
					} else {
						if (day.equals("29") || day.equals("30") || day.equals("31")) {
							return false;
						} else {
							return true;
						}
					}
				} else {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	public PhonePage verifyStatus(String string) {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(statusMessage));
			Assert.assertTrue(statusMessage.getText().contains(string),
					"Verifying whether " + string + "contains in expected result. " + "Expected result is" + string
							+ ".Actual result is " + statusMessage.getText());
			Reporter.log("Status displayed correctly on device status Page");
		} catch (NoSuchElementException e) {
			Reporter.log("Verifying whether" + string + "contains in expected result. " + "Expected result is"
					+ statusMessage.getText() + ".Actual result is " + string);
			Reporter.log("Status doesnot displayed correctly on device status Page");
		}
		return this;

	}

	public PhonePage verifyStatusMessage1(String string) {
		try {
			Assert.assertTrue(statusMessageChild1.getText().contains(string),
					"Status doesnot displayed correctly on device status Page. It should displayed +" + string);
			Reporter.log("Verified Status displayed correctly on device status Page");
		} catch (NoSuchElementException e) {
			Reporter.log("Verified Status doesnot displayed correctly on device status Page");
		}
		return this;

	}

	public PhonePage verifyStatusMessage2(String string) {
		try {
			Assert.assertTrue(statusMessageChild2.getText().contains(string),
					"Status doesnot displayed correctly on device status Page. It should displayed +" + string);
			Reporter.log("Verified Status displayed correctly on device status Page");
		} catch (NoSuchElementException e) {
			Reporter.log("Verified Status doesnot displayed correctly on device status Page");
		}

		try {
			Assert.assertTrue(statusMessageChild2.getAttribute("class").contains("magenta"),
					string + " doesnot not displayed in magenta color.");
			Reporter.log("Verified " + string + " displayed in magenta color.");
		} catch (NoSuchElementException e) {
			Reporter.log("Verified " + string + " does NOT displayed in magenta color.");
		}

		try {
			Assert.assertTrue(
					statusMessageChild2.getAttribute("href").contains("http://support.t-mobile.com/docs/DOC-5794"),
					string + " NOT linked to http://support.t-mobile.com/docs/DOC-5794");
			Reporter.log("Verified " + string + " linked to http://support.t-mobile.com/docs/DOC-5794");
		} catch (NoSuchElementException e) {
			Reporter.log("Verified " + string + " NOT linked to http://support.t-mobile.com/docs/DOC-5794");
		}
		navigateBackToDeviceStatusPage();
		return this;

	}

	public PhonePage verifyDateFormatInauthorableText() {
		try {
			String string = statusMessage.getText();
			List<String> getDates = getDateFromString(string);
			if (!getDates.isEmpty()) {
				for (int i = 0; i < getDates.size(); i++) {
					Assert.assertTrue(validateDateFormat(getDates.get(i)),
							"Date format is incorrect in authorable text. It should be in MM/DD/YY and remove leading zeros from MM and DD (but not YY). But in actual result, it showing  "
									+ getDates.get(i));
				}
				Reporter.log(
						"Veified Date format in authorable text.It should appear in MM/DD/YY and remove leading zeros from MM and DD (but not YY)");
			} else {
				Assert.fail("Dates are missing in authorable text.");
				Reporter.log("Dates are missing in authorable text.");
			}
		} catch (NoSuchElementException e) {
			Reporter.log("Unable to verify Date format in authorable text ");
		}
		return this;

	}

	public PhonePage verifyContactUSButton() {
		try {
			checkPageIsReady();
			Assert.assertTrue(btnContactUS.isDisplayed() && btnContactUS.isEnabled(),
					"Contact us button is neither displayed nor enabled");
			btnContactUS.click();
			Reporter.log("Contact us button on device status Page is enabled and displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Contact us button on device status Page neither enabled and nor displayed");
		}
		return this;

	}

	public PhonePage verifyBackToPhonePageButton() {
		try {
			Assert.assertTrue(btnContactUS.isDisplayed() && btnContactUS.isEnabled(),
					"Back To Phone Page Button is displayed and enabled");
			Reporter.log("Back To Phone Page Button  on device status Page is enabled and displayed");
			btnContactUS.click();
			Reporter.log("Clicked on Back To Phone Page Button  on device status Page ");
			Assert.assertTrue(getDriver().getCurrentUrl().contains("myphone/myphonedetails"), "Phone page displayed");
			Reporter.log("Phone page is displayed");
		} catch (NoSuchElementException e) {
			Reporter.log("Back To Phone Page Button on device status Page neither enabled and nor displayed");
		}
		return this;
	}

	public PhonePage verifyCheckADeviceNotOnYourAccount() {
		try {
			waitforSpinner();
			Assert.assertTrue(
					btnCheckADeviceNotOnYourAccount.isDisplayed() && btnCheckADeviceNotOnYourAccount.isEnabled(),
					"Check A Device Not On Your Account button is neither displayed nor enabled");
			btnCheckADeviceNotOnYourAccount.click();
			Reporter.log("click on Check A Device Not On Your Account button");
		} catch (NoSuchElementException e) {
			Reporter.log("Check A Device Not On Your Account button is neither displayed nor enabled");
		}
		return this;
	}

	public PhonePage verifyDeviceStatus(String text) {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(deviceStatus));
			Assert.assertTrue(deviceStatus.getText().contains(text),
					"Device status doesnot display expected.Expected device status is " + text
							+ " .Actual device status is " + deviceStatus.getText());
			Reporter.log("Device status display correctly.");
		} catch (NoSuchElementException e) {
			Reporter.log("Expected device status doesnot displayed");
		}
		return this;
	}

	/**
	 * Verify upgradeCTA
	 */
	public PhonePage verifyupgradeCTA() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(upgradeCTA));
			Reporter.log("Upgrade cta not displayed");
		} catch (AssertionError e) {
			Reporter.log("Upgrade cta displayed");
			Assert.fail("Upgrade cta displayed");
		}
		return this;
	}

	/**
	 * Verify manage digits settings cta
	 */
	public PhonePage verifyDigitsSettingsCTA() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(digitsSettingsCTA));
			Reporter.log("Manage digits settings cta not displayed");
		} catch (AssertionError e) {
			Reporter.log("Manage digits settings cta displayed");
			Assert.fail("Manage digits settings cta displayed");
		}
		return this;
	}

	/**
	 * Verify device unlock blade
	 */
	public PhonePage verifyDeviceUnlockBlade() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(deviceUnlockBlade));
			Reporter.log("Device unlock blade not displayed");
		} catch (AssertionError e) {
			Reporter.log("Device unlock blade displayed");
			Assert.fail("Device unlock blade displayed");
		}
		return this;
	}

	/**
	 * Verify line selector divison
	 */
	public PhonePage verifyLineSelectorDiv() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(lineSelectorDiv));
			Reporter.log("Line selector divison not displayed");
		} catch (AssertionError e) {
			Reporter.log("Line selector divison displayed");
			Assert.fail("Line selector divison displayed");
		}
		return this;
	}

	/*
	 * check php soc details
	 */
	public PhonePage checkPHPSocDetaiils(String text) {
		String phpStatus;
		phpStatus = phpStatusOnPhonePage.getText();
		try {
			if (phpStatus.contains(text)) {
				Assert.assertTrue(phpStatus.equalsIgnoreCase(text), "Expected result: PHP soc appeared " + text);
			}
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + text
					+ ".Actual result: Authorable text appearing is " + phpStatus);
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ text + ".Actual result: Authorable text appearing is " + phpStatus);
		}
		return this;

	}

	/**
	 * Verify Trade In Link
	 */
	public PhonePage verifyTradeInLink() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(tradeInLink));
			Reporter.log("Trade In Link not displayed");
		} catch (AssertionError e) {
			Reporter.log("Trade In Link is displayed");
			Assert.fail("Trade In Link is displayed");
		}
		return this;
	}

	public PhonePage verifyFeatureDetaiils(String text) {

		for (int i = 0; i < featuresList.size() - 1; i++) {
			try {
				if (featuresList.get(i).getText().contains(text)) {
					Assert.assertTrue(featuresList.get(i).getText().equalsIgnoreCase(text),
							"Expected result: Feature text should appear " + text);
				}
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + text
						+ ".Actual result: Authorable text appearing is " + featuresList.get(i).getText());
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ text + ".Actual result: Authorable text appearing is " + featuresList.get(i).getText());
			}

		}
		return this;
	}

	public PhonePage verifyPhoneImage(WebElement element) {
		try {
			if (element.isDisplayed()) {
				Reporter.log("Device image is displayed");
			}
		} catch (Exception e) {
			Assert.fail("unable to find device image");
		}

		return this;
	}

}
