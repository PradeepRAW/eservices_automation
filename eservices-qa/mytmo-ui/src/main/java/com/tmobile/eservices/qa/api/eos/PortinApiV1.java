package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PortinApiV1 extends ApiCommonLib {

	/***
	 * Summary: Micro services to get portin details
	 * Headers:
	 * - $ref: '#/parameters/X-B3-TraceId'
    - $ref: '#/parameters/X-B3-SpanId'
    - $ref: '#/parameters/Channel-Id'
    - $ref: '#/parameters/Authorization'
    - $ref: '#/parameters/AccessToken'
    - $ref: '#/parameters/AplicationID'
    - $ref: '#/parameters/User-Token'
    - $ref: '#/parameters/Tmo-Id'
    - $ref: '#/parameters/Msisdn'
    - $ref: '#/parameters/BAN'
    - $ref: '#/parameters/USN'
    - $ref: '#/parameters/Application-Client'
    - $ref: '#/parameters/Application-Version-Code'
    - $ref: '#/parameters/Device-OS'
    - $ref: '#/parameters/OS-Version'
    - $ref: '#/parameters/OS-Language'
    - $ref: '#/parameters/Device-Manufacturer'
    - $ref: '#/parameters/Device-Model'
    - $ref: '#/parameters/Marketing-Cloud-Id'
    - $ref: '#/parameters/Sd-Id'
    - $ref: '#/parameters/Adobe-UUID'
	 * @return
	 * @throws Exception
	 */
	public Response updatePortin(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildPortinHeader(apiTestData,tokenMap);
		String resourceURL = "v1/portin/102190998";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		return response;
	}

	/***
	 * Summary: Micro services toupdate portin details
	 * Headers: 
	 *  - $ref: '#/parameters/X-B3-TraceId'
        - $ref: '#/parameters/X-B3-SpanId'
        - $ref: '#/parameters/Channel-Id'
        - $ref: '#/parameters/Authorization'
        - $ref: '#/parameters/AccessToken'
        - $ref: '#/parameters/AplicationID'
        - $ref: '#/parameters/User-Token'
        - $ref: '#/parameters/Tmo-Id'
        - $ref: '#/parameters/Msisdn'
        - $ref: '#/parameters/BAN'
        - $ref: '#/parameters/USN'
        - $ref: '#/parameters/Application-Client'
        - $ref: '#/parameters/Application-Version-Code'
        - $ref: '#/parameters/Device-OS'
        - $ref: '#/parameters/OS-Version'
        - $ref: '#/parameters/OS-Language'
        - $ref: '#/parameters/Device-Manufacturer'
        - $ref: '#/parameters/Device-Model'
        - $ref: '#/parameters/Marketing-Cloud-Id'
        - $ref: '#/parameters/Sd-Id'
        - $ref: '#/parameters/Adobe-UUID'
	 * @return
	 * @throws Exception
	 */
	public Response getPortin(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildPortinHeader(apiTestData,tokenMap);
		String resourceURL = "v1/portin/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
	}

	private Map<String, String> buildPortinHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		String jwtToken=checkAndGetJWTToken(apiTestData, tokenMap).get("jwtToken");
		Map<String, String> headers = new HashMap<String, String>();				
		headers.put("Accept","application/json");
		headers.put("Content-Type","application/json");
		headers.put("channel_id","TMO-APP");
		headers.put("Authorization",jwtToken);
		headers.put("access_token", jwtToken);
		headers.put("application_id","{{application_id}}");
		headers.put("user-token","{{user-token}}");
		headers.put("tmo-id","{{tmo-id}}");
		headers.put("msisdn",tokenMap.get("msisdn"));
		headers.put("ban",tokenMap.get("ban"));
		headers.put("usn","{{usn}}");
		headers.put("application_client","{{application_client}}");
		headers.put("application_version_code","{{application_version_code}}");
		headers.put("device_os","{{device_os}}");
		headers.put("os_version","{{os_version}}");
		headers.put("os_language","{{os_language}}");
		headers.put("device_manufacturer","{{device_manufacturer}}");
		headers.put("device_model","{{device_model}}");
		headers.put("marketing_cloud_id","{{marketing_cloud_id}}");
		headers.put("sd_id","{{sd_id}}");
		headers.put("adobe_uuid","{{adobe_uuid}}");
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("transactionid","FlexAPITest");
		return headers;
	}

}


