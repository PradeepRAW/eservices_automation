package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class QuoteApiV5 extends ApiCommonLib{
	
	/***
	 * This is the API specification for the EOS Quote . The EOS API will inturn
	 * invoke DCP apis to execute the quote functions.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
    public Response createQuote(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	
    	Map<String, String> headers = buildQuoteAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/quote/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
        return response;
    }       
    
    public Response updateQuote(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildQuoteAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/quote/"+tokenMap.get("cartId");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
         return response;
    } 
    
    public Response updatePayment(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildPaymentAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/quote/"+tokenMap.get("cartId")+"/payment";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    }
    
    public Response updateAddress(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildQuoteAPIHeader(apiTestData, tokenMap);
        String resourceURL = "v5/quote/"+tokenMap.get("cartId")+"/address";
		//String resourceURL = "v5/quote/"+tokenMap.get("cartId")+"/addresses/"+tokenMap.get("addressId");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
         return response;
    } 
    
    public Response fraudCheck(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildQuoteAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/quote/"+tokenMap.get("cartId")+"/fraudCheck";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    } 
    private Map<String, String> buildQuoteAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	//JWTTokenApi jwtTokenApi = new JWTTokenApi();
    	String transactionType = "UPGRADE";
    	tokenMap.put("version", "v1");
    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
    	// = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
    	
    	//jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"));
    	Map<String, String> headers = new HashMap<String, String>();
		
    	if(StringUtils.isNoneEmpty(tokenMap.get("addaline"))){
			transactionType = "ADDALINE";
		}
		
		headers.put("accept","application/json");
		headers.put("applicationid","MYTMO");
		headers.put("cache-control","no-cache");
		headers.put("channelid","WEB");
		headers.put("clientid","233444");
		headers.put("content-type","application/json");
		headers.put("correlationid",checkAndGetPlattokenJWT(apiTestData,jwtTokens.get("jwtToken")));
		headers.put("transactionid",jwtTokens.get("jwtToken"));
		headers.put("transactionbusinesskeytype",apiTestData.getBan());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionType",transactionType);
		headers.put("usn","testUSN");
		headers.put("interactionid",jwtTokens.get("jwtToken"));
		//headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
		//headers.put("authorization",jwtTokens.get("jwtToken"));
		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
		headers.put("authorization",jwtTokens.get("jwtToken"));
		return headers;
	}
    
    private Map<String, String> buildPaymentAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	//JWTTokenApi jwtTokenApi = new JWTTokenApi();
    	String transactionType = "UPGRADE";
    	
    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
    	// = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
    	
    	//jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"));
    	Map<String, String> headers = new HashMap<String, String>();
		
    	if(StringUtils.isNoneEmpty(tokenMap.get("addaline"))){
			transactionType = "ADDALINE";
		}
		
		headers.put("accept","application/json");
		headers.put("applicationid","MYTMO");
		headers.put("cache-control","no-cache");
		headers.put("channelid","WEB");
		headers.put("clientid","233444");
		headers.put("content-type","application/json");
		headers.put("correlationid",checkAndGetPlattokenJWT(apiTestData,jwtTokens.get("jwtToken")));
		headers.put("transactionid",jwtTokens.get("jwtToken"));
		headers.put("transactionbusinesskeytype",apiTestData.getBan());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionType",transactionType);
		headers.put("usn","testUSN");
		headers.put("interactionid","IID");
		
		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
		headers.put("authorization",jwtTokens.get("jwtToken"));
		
		return headers;
	}

}
