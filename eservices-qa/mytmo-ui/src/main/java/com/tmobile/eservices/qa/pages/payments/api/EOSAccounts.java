package com.tmobile.eservices.qa.pages.payments.api;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSAccounts extends EOSCommonLib {






public Response getResponsepaymentMethods(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("interactionId", "1222");
	    restService.addHeader("ban", alist[3]);
	    response = restService.callService("v2/accounts/"+alist[3]+"/paymentmethods?msisdn="+alist[0],RestCallType.GET);
	    
	}

	    return response;
}



public String geteligibilityforbankpayment(Response response) {
	String bankpayment=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("eligibleForBankPayment")!=null) {
        	return jsonPathEvaluator.get("eligibleForBankPayment").toString();
	}}
		
	
	return bankpayment;
}



public String[] getstoreddata(Response response) {
	String [] stored=new String[3];

	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("bankAccounts")!=null) {
        	if(jsonPathEvaluator.get("bankAccounts").toString().equalsIgnoreCase("[]")) {
        		stored[0]="false";
        	}
        	else {stored[0]="true";}
        	}
	
	

		if(jsonPathEvaluator.get("creditCards")!=null) {
        	if(jsonPathEvaluator.get("creditCards").toString().equalsIgnoreCase("[]")) {
        		stored[1]="false";
        	}
        	else {stored[1]="true";}
	        }
		
		if(jsonPathEvaluator.get("debitCards")!=null) {
        	if(jsonPathEvaluator.get("debitCards").toString().equalsIgnoreCase("[]")) {
        		stored[2]="false";
        	}
        	else {
        		stored[2]="true";}
	}
		
		
		
		
	}
		

	
	return stored;
}







}
