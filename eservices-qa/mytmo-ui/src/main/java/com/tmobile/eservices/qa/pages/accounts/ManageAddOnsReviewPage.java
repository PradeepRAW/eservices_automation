/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author csudheer
 *
 */
public class ManageAddOnsReviewPage extends CommonPage {

	@FindBy(css = "button[class*='PrimaryCTA']")
	private WebElement agreeAndSubmitBtn;

	@FindBy(css = ("span[class*='ng-binding'] a"))
	private List<WebElement> listOfLegalLinks;

	@FindBy(css = "span[class='Display4']")
	private WebElement legalHeaderText;

	@FindBy(css = "span[class='H6-heading']")
	private WebElement increaseOrDecreaseText;

	@FindBy(css = "span[class='legal d-inline-block black']")
	private WebElement reviewPageLegaleseTerms;

	@FindBy(css = "p[class='padding-bottom-xxxl legal black']")
	private WebElement reviewPageLegaleseTerms2;

	@FindBy(css = "div[id='reviewPayment'] p[class*='bottom']")
	private WebElement reviewPageMSISDN;

	@FindBy(css = "#sabutton i")
	private WebElement serviceAgreementCloseBtn;

	@FindBy(css = "#tandcbtn i")
	private WebElement termsAndCondCloseBtn;

	@FindBy(css = "#signbtn i")
	private WebElement electronicSignCloseBtn;

	@FindBy(css = "span[class='Display3']")
	private List<WebElement> reviewText;

	@FindBy(css = "span[class='H6-heading']")
	private List<WebElement> increaseDecreaseOrNoChange;

	@FindBy(css = "#DP_R_SignatureTerms h4.h2-uppercase.h3-title.text-bold")
	private WebElement legalHeaderTextElectronicSignature;

	@FindBy(css = "button[class*='PrimaryCTA full-btn-width float-md-left float-lg-left float-xl-left']")
	private WebElement backButton;

	@FindBy(css = "div[class*='items-baseline']")
	private List<WebElement> removedItemsList;

	@FindBy(css = "span[class*='vertical-medium']")
	private List<WebElement> addedOrRemovedItemsText;

	@FindBy(css = "span[class*='pull-right H6-heading']")
	private WebElement removedAddOnsPriceText;

	// @FindBy(css="#DP_R_TermsAndConditionOff
	// div.text-center.padding-top-xl-xs.padding-top-xxl.ng-scope
	// h4.h2-uppercase.h3-title.text-bold")
	@FindBy(css = "div[id*='TermsAndCondition'] div h4")
	private WebElement legalHeaderTextTermsAndCond;

	@FindBy(css = "#DP_R_ServiceAgreement div.text-center.padding-top-xl-xs.padding-top-xxl.ng-scope h4.h2-uppercase.h3-title.text-bold")
	private WebElement legalHeaderTextServiceAgreement;

	@FindBy(css = "span[class*='Display6']")
	private WebElement datapassName;

	@FindBy(css = "span.black.Display3")
	private WebElement effectiveDatePopup;

	@FindBy(css = "a.body-link")
	private WebElement changeDateLink;

	@FindBy(css = "p[class*='H6-heading']")
	private List<WebElement> priceAndTaxText;

	@FindBy(css = "a[class*='body-link cursor']")
	private WebElement clickChangeDate;

	@FindBy(css = "button[class*='SecondaryCTA blackCTA']")
	private WebElement cancelButton;

	@FindBy(css = "button[class*='SecondaryCTA-accent blackCTA']")
	private WebElement overlayCancelButton;

	@FindBy(css = "button[class*='PrimaryCTA-accent']")
	private WebElement overlaySelectButton;

	@FindBy(css = "button[class='PrimaryCTA-accent full-btn-width']")
	private WebElement clickselect;

	@FindBy(css = "span[class*='no-padding black Display3']")
	private WebElement calendertitle;

	@FindBy(xpath = "//div[contains(text(),'Immediately')]")
	private WebElement immediately;

	@FindBy(xpath = "//div[contains(text(),'Pick a date')]")
	private WebElement pickADate;

	@FindBy(css = "div[class*='Display5']")
	private WebElement pickADateTitle;

	@FindBy(css = "input[type='radio']")
	private List<WebElement> pickUpDateRadioButton;

	@FindBy(xpath = "//div[contains(text(),'These changes will begin immediately')]")
	private WebElement textbelowImmediately;

	@FindBy(xpath = "//button[contains(@class,'disabled')]//..//..//div[@class='text-center']//button[contains(@class,'background body-bold') and not(contains(@class,'disabled'))]")
	private List<WebElement> calendarNextDate;

	@FindBy(css = "span[class='arrow-right pull-right']")
	private List<WebElement> datePickerRightBtn;

	@FindBy(css = "div[class='padding-top-xsmall'] span[class='body']")
	private WebElement effectiveDate;

	@FindBy(id = "Today_Billing_Cycle")
	private WebElement todayDate;

	@FindBy(xpath = "//input[@id='Today_Billing_Cycle']//..//..//..//..//div")
	private List<WebElement> todayText;

	@FindBy(xpath = "//input[@id='Next_Billing_Cycle']//..//label")
	private WebElement nextBillCycleRadioBtn;

	@FindBy(xpath = "//input[@id='Next_Billing_Cycle']//..//..//..//..//div")
	private List<WebElement> nextBillCycleText;

	@FindBy(css = "p.legal.black")
	private WebElement reviewPageLegaleseText;

	@FindBy(css = "span[class*='legal-bold-link']")
	private List<WebElement> legalLinks;

	@FindBy(css = "span[class*='arrow-back']")
	private WebElement reviewPageBackButton;

	@FindBy(css = ".padding-top-small p.body")
	private List<WebElement> proRateMessage;

	@FindBy(css = "div.Display6")
	private List<WebElement> effectiveDateRadioBtns;

	@FindBy(css = ".Display3")
	private List<WebElement> reviewAndPayText;

	public ManageAddOnsReviewPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Wait For Spinner Invisible
	 */
	public void reviewPageWaitForSpinner() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("spin-loader")));
	}

	/**
	 * Verify review Text
	 * 
	 * @return
	 */
	public boolean verifyReviewText() {
		checkPageIsReady();
		return verifyElementBytext(reviewText, "Review your order");
	}

	/**
	 * Verify review & pay Text
	 * 
	 * @return
	 */
	public boolean verifyReviewAndPayText() {
		checkPageIsReady();
		return verifyElementBytext(reviewAndPayText, "Review & Pay");
	}

	/**
	 * Clicks on service Agreement Link
	 */
	public ManageAddOnsReviewPage clickOnServiceAgreementLink() {
		try {
			checkPageIsReady();
			clickElementBytext(listOfLegalLinks, "Service Agreement");
			Reporter.log("Clicked on service Agreement link");
		} catch (Exception e) {
			Assert.fail("not Clicked on the Service Agreement link");
		}
		return this;
	}

	/**
	 * Clicks on Terms And Conditions Link
	 */
	public ManageAddOnsReviewPage clickOnTermsAndConditionsLink() {
		waitForSpinnerInvisibility();
		try {
			checkPageIsReady();
			clickElementBytext(listOfLegalLinks, "Terms & Conditions;");
			Reporter.log("Clicked on terms and conditions link");
		} catch (Exception e) {
			Assert.fail("Terms and conditions link is not clicked.");
		}
		return this;
	}

	/**
	 * Clicks on Electronic signature Link
	 */
	public ManageAddOnsReviewPage clickOnElectronicSignatureLink() {
		waitForSpinnerInvisibility();
		try {
			checkPageIsReady();
			clickElementBytext(listOfLegalLinks, "Electronic Signature Terms");// changed
			Reporter.log("Clicked on Electronic Signature Terms");
		} catch (Exception e) {
			Assert.fail("Electronic Signature Terms is not clicked");
		}
		return this;
	}

	public ManageAddOnsReviewPage verifyLegaleseTerms(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(reviewPageLegaleseTerms.getText().contains(msg), "Legalese terms is not displayed");
		} catch (Exception e) {
			Assert.fail("Legalese terms is not displayed");
		}
		return this;
	}

	public ManageAddOnsReviewPage verifyLegaleseTerms2(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(reviewPageLegaleseTerms2.getText().contains(msg));
			Reporter.log("Reduced Speed message and see full terms link is displayed");
		} catch (Exception e) {
			Assert.fail("Reduced Speed message and see full terms link is  not displayed");
		}
		return this;
	}

	public ManageAddOnsReviewPage clickonCloseServiceAgreement() {
		try {
			serviceAgreementCloseBtn.click();
			Reporter.log("Service Agreement Close CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Service Agreement Close CTA is not clicked");
		}
		return this;
	}

	public ManageAddOnsReviewPage clickonCloseTermsAndCond() {
		try {
			termsAndCondCloseBtn.click();
			Reporter.log("Terms & Conditions CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Terms & Conditions CTA is not clicked");
		}
		return this;
	}

	public ManageAddOnsReviewPage closeElectronicSign() {
		try {
			electronicSignCloseBtn.click();
			Reporter.log("Electronic Sign Close CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Electronic Sign Close CTA is not clicked");
		}
		return this;
	}

	public ManageAddOnsReviewPage verifyLegalHeaderTextServiceAgreement(String msg) {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(legalHeaderTextServiceAgreement.getText().equalsIgnoreCase(msg),
					"Service Agreement page is not displayed");
		} catch (Exception e) {
			Assert.fail("Service Agreement is not displayed");
		}
		return this;
	}

	public ManageAddOnsReviewPage verifyLegalHeaderTextTermsAndCond(String msg) {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(legalHeaderTextTermsAndCond.getText().equalsIgnoreCase(msg),
					"Terms & conditions page is not displayed");
		} catch (Exception e) {
			Assert.fail("Terms & conditions is not displayed");
		}
		return this;
	}

	public ManageAddOnsReviewPage verifyLegalHeaderTextElectronicSign(String msg) {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(legalHeaderTextElectronicSignature.getText().equalsIgnoreCase(msg),
					"Electronic signature terms page is not displayed");
		} catch (Exception e) {
			Assert.fail("Electronic signature terms is not displayed");
		}
		return this;
	}

	public ManageAddOnsReviewPage clickOnAgreeandSubmitButton() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(backButton));
			clickElementWithJavaScript(backButton);
			// backButton.click();
			Reporter.log("Clicked on Agree and submit button");

		} catch (Exception e) {
			Assert.fail("Agree and submit button is not displayed");
		}
		return this;
	}

	/**
	 * Verify Removed item names
	 * 
	 * @return
	 */
	public ManageAddOnsReviewPage verifyRemovedItemsNames(String itemName) {
		try {
			boolean isElementDisplayed = false;
			for (WebElement webElement : removedItemsList) {
				if (itemName.contains(webElement.getText())) {
					isElementDisplayed = true;
					break;
				}
			}
			Assert.assertTrue(isElementDisplayed, "Removed Service name is not displayed");
			Reporter.log("Removed Service names" + itemName + "displayed");
		} catch (Exception e) {
			Assert.fail("Removed Service names" + itemName + "not displayed");
		}
		return this;
	}

	public ManageAddOnsReviewPage verifyPriceText(String locatorType) {
		try {

			if (verifyElementBytext(addedOrRemovedItemsText, "Removed add-ons")) {
				String removedPrice = removedAddOnsPriceText.getText()
						.substring(0, removedAddOnsPriceText.getText().indexOf('.')).replaceAll("[$,]", "");
				String price = Integer.toString(Integer.parseInt(locatorType) - Integer.parseInt(removedPrice));
				Assert.assertTrue(verifyElementBytext(reviewText, price), "Price is not displayed");
				Reporter.log("Verified Your monthly bill will increase by " + locatorType + "text is displayed");
			} else {
				Assert.assertTrue(verifyElementBytext(reviewText, locatorType), "Price is not displayed");
				Reporter.log("Verified Your monthly bill will increase by " + locatorType + "text is displayed");
			}

		} catch (Exception e) {
			Assert.fail("Monthly bill will increase by is not displayed");
		}
		return this;
	}

	/**
	 * Verify your Monthly Bill not displaying
	 */
	public ManageAddOnsReviewPage verifyMonthlyBillIsNotDispalying() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!CollectionUtils.isNotEmpty(increaseDecreaseOrNoChange)
					&& CollectionUtils.sizeIsEmpty(increaseDecreaseOrNoChange));
			Reporter.log("Verified Monthly billing is not displayed");
		} catch (Exception e) {
			Assert.fail("Your monthly billing text is displayed");
		}
		return this;
	}

	public ManageAddOnsReviewPage verifyDataPassNameAndTextReviewpage(String dataPassName, String price) {

		Assert.assertTrue(datapassName.getText().contains(dataPassName));
		Assert.assertTrue(priceAndTaxText.get(0).getText().contains(price),
				"Datapass name and price are not displaying");
		Reporter.log("Verified datapass name and price");
		return this;
	}

}