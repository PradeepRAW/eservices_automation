package com.tmobile.eservices.qa.listeners;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;
import java.util.Set;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

/**
 * @author blakshmi
 *
 */
public class ExecuteTestListener extends TestListenerAdapter {
	private static final Logger logger = LoggerFactory.getLogger(ExecuteTestListener.class);
	private static final String WEB_DRIVER_ATTR = "web.driver";

	@Override
	public void onFinish(ITestContext testContext) {
		logger.info("onFinish method called in ExecuteFailTestListener");
		Set<ITestResult> failedTests = testContext.getFailedTests().getAllResults();
		Set<ITestResult> skippedTests = testContext.getSkippedTests().getAllResults();
		Set<ITestResult> passedTests = testContext.getPassedTests().getAllResults();
		if (!failedTests.isEmpty()) {
			for (ITestResult result : failedTests) {
				ITestNGMethod method = result.getMethod();
				if (testContext.getFailedTests().getResults(method).size() > 1) {
					logger.info("failed test case removed as duplicate:" + result.getTestClass().toString());
					failedTests.remove(result);
				} else {
					if (testContext.getPassedTests().getResults(method).size() > 0) {
						logger.info("failed test case remove as pass retry:" + result.getTestClass().getTestName());
						failedTests.remove(result);
					}
				}
			}
		}
		if (!skippedTests.isEmpty()) {
			for (ITestResult result : skippedTests) {
				ITestNGMethod method = result.getMethod();
				if (testContext.getSkippedTests().getResults(method).size() > 1) {
					skippedTests.remove(result);
				} else {
					if (testContext.getPassedTests().getResults(method).size() > 0) {
						skippedTests.remove(result);
					} else {
						if (testContext.getFailedTests().getResults(method).size() > 0) {
							skippedTests.remove(result);
						}
					}
				}
			}
		}
		if (!passedTests.isEmpty()) {
			for (ITestResult result : passedTests) {
				ITestNGMethod method = result.getMethod();
				if (testContext.getPassedTests().getResults(method).size() > 1) {
					passedTests.remove(result);
				}
			}
		}
	}

	@Override
	public void onTestFailure(ITestResult result) {
		WebDriver driver = (WebDriver) result.getAttribute(WEB_DRIVER_ATTR);

		if (!result.isSuccess()) {
			try {
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				String encodedFileString = encodeFileToBase64Binary(scrFile);
				Reporter.setCurrentTestResult(result);
				String imgSrc = "data:image/png;base64," + encodedFileString;
				Reporter.log("<img width='100' height='100' src=" + imgSrc + " onclick=toggleImage(this.src);> ");
			} catch (Exception e) {
				logger.error("Error while capturing screenshot: " + e.getCause());
			}
		}
	}

	private static String encodeFileToBase64Binary(File file) {
		String encodedfile = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(file);
			byte[] bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = new String(Base64.getEncoder().encode(bytes), "UTF-8");
			fileInputStreamReader.close();
		} catch (FileNotFoundException e) {
			logger.debug(e.getMessage());
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}

		return encodedfile;
	}
}
