package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class FeatureDetailsPage extends CommonPage {


	@FindBy(xpath = "//span[contains(text(),'GSM Line - SOC3')]")
	private WebElement palnName;

	@FindBy(css = "button[class='SecondaryCTA blackCTA full-btn-width']")
	private List<WebElement> changePlan;

	@FindBy(xpath = "//div[contains(text(),'Included in the plan')]")
	private WebElement IncludedPlan;

	@FindBy(css = "div.H6-heading.d-inline-block.pl-1")
	private List<WebElement> featurePlan;

	@FindBy(xpath = "//div[contains(text(),'Benefit Details')]")
	private WebElement benefitDetails;

	@FindBy(xpath = "//div[contains(text(),'Feature Details')]")
	private WebElement featureDetails;

	@FindBy(css = "p.Display5")
	private WebElement featureName;

	@FindBy(xpath = "//button[contains(text(),'Back')]")
	private WebElement backCTA;

	@FindBy(xpath = "//div[contains(text(),'Plan Details')]")
	private WebElement planDetails;

	/**
	 * 
	 * @param webDriver
	 */

	public FeatureDetailsPage clickBackCTA() {
		try {
			checkPageIsReady();
			backCTA.click();
			Reporter.log("Clicked on BACK CTA and navigate back to PlanDetailsPage");
		} catch (Exception e) {
			Assert.fail("BACK CTA is not available to click");
		}
		return this;
	}

	public FeatureDetailsPage verifybenefitDetails() {
		try {
			checkPageIsReady();
			benefitDetails.isDisplayed();
			Reporter.log("BenefitDetails Heading is available");
		} catch (Exception e) {
			Assert.fail("BenefitDetails Heading is not available");
		}
		return this;
	}

	public FeatureDetailsPage verifyFeatureDetailsPage() {
		try {
			checkPageIsReady();
			benefitDetails.isDisplayed();
			Reporter.log("Feature Details Page is Loaded");
		} catch (Exception e) {
			Assert.fail("Feature Details Page is not Loaded");
		}
		return this;
	}

	public FeatureDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

}
