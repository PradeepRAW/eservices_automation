package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class NotifyEipAgreement extends ApiCommonLib {

	/***
	 * Summary: This API is used do the Notify EIP Agreements OrchestrateOrders API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-interactionId
	 * 	-workflowId
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */

	private Map<String, String> buildNotifyEipAgreementApiHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/x-www-form-urlencoded");
		headers.put("user-id", "EIP");
		headers.put("application-id", "MYT");
		headers.put("cache-control", "no-cache");
		headers.put("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVVRlVSRUZVTFRJd01Uaz0ifQ.eyJpYXQiOjE1NjM4NDQxMDMsImV4cCI6MTU2Mzg0NzcwMiwiaXNzIjoiaHR0cHM6Ly9xYXQuYnJhc3MuYWNjb3VudC50LW1vYmlsZS5jb20iLCJhdWQiOiJNWVRNTyIsIm5vbmNlIjoiOTA5NTQzMjQwMiIsIkFUIjoiMS5VU1IuY3hadkRham1tVTBXZDZTNHkiLCJzdWIiOiJVLWFhYmIyYTNhLTFmZjQtNDVmOS1hNTdhLWY3NzFhZDJiNWEzNiIsImFjciI6ImxvYTIiLCJhbXIiOlsicGFzc3dvcmQiXSwidXNuIjoiM2E1ZGNlOTkzZmI0ZmM1MCIsImVudF90eXBlIjoiZGlyZWN0IiwiZW50Ijp7ImFjY3QiOlt7InIiOiJBTyIsImlkIjoiODI2ODg4MzMxIiwidHN0IjoiSVIiLCJsaW5lX2NvdW50IjoxLCJsaW5lcyI6W3sicGhudW0iOiI5MDk1NDMyNDAyIiwiciI6IkQifV19XX19.AORatFwUxMWq9U1pFhohFaH0XEVvIMQfni50T32HZT-q2iqXdsnytVZgrKrGDZJIUBtWpgHerdCAR1EZs-qADQkJctbvvLB2BG43hJ9EuYDelQ_nM1wA9W4gVHjf7GfWphJG0TWxdj4dYwll-lquQqr97OMwWv8kks-Q-ShBTB5ILWHAgWZv1mKOBseUgeF2URMucqGmuyyFDyMdEtvfuIkoi-XO2qyVMD3YKCYVnJfN7LXjnSfr5Dg68N4j3IOS3nCgXUCigx3DQl0_vSlS7CMVtM8GLTKkRAlWbbbMBqOGRVk1gcTOQlzxZgbthFLjoiXBxQAJgsG244VF8qxkjA");
		return headers;
	}
	public Response verifyEipSignature(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildNotifyEipAgreementApiHeader(apiTestData);
		RestService service = new RestService(requestBody, "https://qlab03.core.op.api.t-mobile.com");
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("device-finance/v1/agreements/poip/esignature", RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}


}
