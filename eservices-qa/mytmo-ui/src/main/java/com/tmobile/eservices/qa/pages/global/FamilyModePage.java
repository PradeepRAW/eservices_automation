package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class FamilyModePage extends CommonPage {

	private static final Logger logger = LoggerFactory.getLogger(FamilyModePage.class);

	private static final String pageUrl = "family";


	
	public FamilyModePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify military verification page.
	 */
	public FamilyModePage verifyFamilyModePage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Family mode page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Family mode page not displayed");
		}
		return this;

	}
}