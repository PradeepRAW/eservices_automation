/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class AccountVerificationPage extends CommonPage {

	@FindBy(css = "div.pb5")
	private WebElement accountVerificationPage;

	@FindBy(css = ".btn-primary")
	private WebElement NextBtn;

	@FindBy(css = ".btn-primary[ng-click*='secondFactorSecurityQController.validateSecurityAnswers()']")
	private WebElement securityQstnsNextBtn;

	@FindBy(css = "input#que_0_field")
	private WebElement firstSecurityQstn;

	@FindBy(css = "input#que_1_field")
	private WebElement secondSecurityQstn;

	@FindBy(css = ".verify-account")
	private WebElement enterVerificationCodeHeaderText;

	public AccountVerificationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Change SIM Page
	 * 
	 * @return
	 */
	public AccountVerificationPage verifyAccountVerificationPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			accountVerificationPage.isDisplayed();
			Reporter.log("Account verification page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Account verification page not displayed");
		}
		return this;
	}

	/**
	 * Click next button
	 */
	public AccountVerificationPage clickOnNextBtn() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(NextBtn);
			Reporter.log("Clicked next Button.");

		} catch (NoSuchElementException e) {
			Assert.fail("Next Button is not clickable.");
		}
		return this;
	}

	/**
	 * Click next button
	 */
	public AccountVerificationPage clickOnSecurityQstnsNextBtn() {
		try {
			waitforSpinner();
			checkPageIsReady();
			sendTextData(firstSecurityQstn, "test");
			sendTextData(secondSecurityQstn, "test");
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(securityQstnsNextBtn));
			clickElementWithJavaScript(securityQstnsNextBtn);
			Reporter.log("Successfully clicked next button.");
		} catch (NoSuchElementException e) {
			Assert.fail("Next button is not clickable.");
		}
		return this;
	}

	/**
	 * Verify enter verification code header text
	 * 
	 * @return
	 */
	public AccountVerificationPage verifyEnterVerificationCodeHeaderText() {
		try {
			waitforSpinner();
			enterVerificationCodeHeaderText.isDisplayed();
			Reporter.log("Enter verification code header text is displayed");
		} catch (Exception e) {
			Assert.fail("Enter verification code header text not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public boolean verifyPageUrl() {
		boolean isAccountVerificationPageDispalyed = false;
		try {
			if (getDriver().getCurrentUrl().contains("SecondFactor")) {
				isAccountVerificationPageDispalyed = true;
				Reporter.log("AccountVerification page is displayed");
			}
		} catch (Exception e) {
			Assert.fail("AccountVerification is not displayed");
		}
		return isAccountVerificationPageDispalyed;
	}
}
