package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author sputti
 *
 */
public class LazerSharkPage extends TmngCommonPage {

	private final String pageUrl = "lazer-shark";
	// Locators for LazerShark Generic Elements
	@FindBy(xpath = "//p[@id='progressStatus']")
	private WebElement ProgressNumber;

	@FindBy(xpath = "//div[@id='myProgress']")
	private WebElement MyProgressBar;

	@FindBy(xpath = "//div[@id='tncCopyText']")
	private WebElement LegalTCMessage;

	@FindBy(xpath = "//a[@id='tncLink']")
	private WebElement LegalTCLink;

	@FindBy(xpath = "//div[@id='lsModal']")
	private WebElement GenericModal;

	@FindBy(xpath = "//button[@id='closeCta']")
	private WebElement GenericModalButton;

	// Locators for LazerShark Form1 Survey Page
	// @FindBy(xpath = "//span[@id='surveyHeader']")
	@FindBy(xpath = "//span[@id='Header']")
	private WebElement form1Header;

	// @FindBy(xpath = "//span[@id='surveyStatusHeader']")
	@FindBy(xpath = "//span[@id='statusHeader']")
	private WebElement form1SubHeader;

	@FindBy(xpath = "//span[@id='1/4']")
	private WebElement form1ProgressNumber;

	@FindBy(xpath = "//div[@id='carrierHeader']")
	private WebElement ProviderHeader;

	@FindBy(xpath = "//select[@id='currentcarrierInput']")
	private WebElement selectProvider;

	@FindBy(xpath = "//div[@id='ratingHeader']")
	private WebElement RateHeader;

	@FindBy(xpath = "//div[2]/div[2]/div/ul[@class='ratings']")
	private WebElement form1Rate;

	@FindBy(xpath = "//div[2]/div[2]/div/ul/li[2]")
	private WebElement form1RateOption;

	@FindBy(xpath = "//div[@id='tfbHeader']")
	private WebElement BOHeader;

	@FindBy(xpath = "//button[@id='InterestedinTFBBtn']")
	private WebElement form1BusinessOptionYes;

	@FindBy(xpath = "//button[@id='notInterestedinTFBBtn']")
	private WebElement form1BusinessOptionNo;

	@FindBy(xpath = "//button[@id='continueCta']")
	private WebElement form1ContinueButton;

	// Locators for LazerShark Form2 Customer Info Page
	// @FindBy(xpath = "//span[@id='customerHeader']")
	@FindBy(xpath = "//span[@id='Header']")
	private WebElement form2Header;

	// @FindBy(xpath = "//span[@id='customerStatusHeader']")
	@FindBy(xpath = "//span[@id='statusHeader']")
	private WebElement form2SubHeader;

	@FindBy(xpath = "//span[@id='2/4']")
	private WebElement form2ProgressNumber;

	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement form2FirstName;

	@FindBy(xpath = "//div[@id='firstNameError']")
	private WebElement form2FirstNameError;

	@FindBy(xpath = "//input[@name='lastName']")
	private WebElement form2LastName;

	@FindBy(xpath = "//div[@id='lastNameError']")
	private WebElement form2LastNameError;

	@FindBy(xpath = "//input[@name='tel']")
	private WebElement form2PhoneNumber;

	@FindBy(xpath = "//div[@id='phoneError']")
	private WebElement form2PhoneNumberError;

	@FindBy(xpath = "//input[@id='mail']")
	private WebElement form2Email;

	@FindBy(xpath = "//div[@id='emailError']")
	private WebElement form2EmailError;

	@FindBy(xpath = "//input[@name='confirmEmail']")
	private WebElement form2ConfirmEmail;

	@FindBy(xpath = "//div[@id='confirmEmailError']")
	private WebElement form2EmailConfirmError;

	@FindBy(xpath = "//div[@id='existingUser']")
	private WebElement form2ExistingUserHeader;

	@FindBy(xpath = "//button[@id='hideImeiBtn']")
	private WebElement form2PIHOptionNo;

	@FindBy(xpath = "//button[@id='showImeiBtn']")
	private WebElement form2PIHOptionYes;

	@FindBy(xpath = "//input[@name='imei']")
	private WebElement form2IMEINumber;

	@FindBy(xpath = "//div[@id='imeiError']")
	private WebElement form2IMEINumberError;

	@FindBy(xpath = "//a[@id='imeiToolTip']")
	private WebElement form2IMEIIcon;

	@FindBy(xpath = "//p[@id='imeiToolTipText']")
	private WebElement form2IMEIIconMessage;

	@FindBy(xpath = "//button[@id='backCta']")
	private WebElement form2BackButton;

	@FindBy(xpath = "//button[@id='continueCta']")
	private WebElement form2ContinueButton;

	// Locators for LazerShark Form3 Shipping Page
	// @FindBy(xpath = "//span[@id='shippingHeader']")
	@FindBy(xpath = "//span[@id='Header']")
	private WebElement form3Header;

	// @FindBy(xpath = "//span[@id='shippingStatusHeader']")
	@FindBy(xpath = "//span[@id='statusHeader']")
	private WebElement form3SubHeader;

	@FindBy(xpath = "//span[@id='3/4']")
	private WebElement form3ProgressNumber;

	@FindBy(xpath = "//input[@name='address1']")
	private WebElement form3Address1;

	@FindBy(xpath = "//div[@id='address1Error']")
	private WebElement form3Address1Error;

	@FindBy(xpath = "//input[@id='address2']")
	private WebElement form3Address2;

	@FindBy(xpath = "//input[@name='city']")
	private WebElement form3City;

	@FindBy(xpath = "//div[@id='cityErrorMessage']")
	private WebElement form3CityError;

	@FindBy(xpath = "//select[@id='state']")
	private WebElement form3State;

	@FindBy(xpath = "//div[@id='stateErrorMessage']")
	private WebElement form3StateError;

	@FindBy(xpath = "//input[@name='zip']")
	private WebElement form3Zip;

	@FindBy(xpath = "//div[@id='zipErrorMessage']")
	private WebElement form3ZipError;

	@FindBy(xpath = "//button[@id='backButton']")
	private WebElement form3Back;

	@FindBy(xpath = "//button[@id='submitButton']")
	private WebElement form3Submit;

	// Locators for LazerShark Form3 Address Standardization Model
	@FindBy(xpath = "//div[@id='addressModal']")
	private WebElement form3SubmitStandardModal;

	@FindBy(xpath = "//input[@id='address']")
	private WebElement form3ModelIndicator;

	@FindBy(xpath = "//button[@id='closeCta']")
	private WebElement form3ModelBack;

	@FindBy(xpath = "//button[@id='submitCta']")
	private WebElement form3ModelConfirm;

	// Locators for LazerShark Form4 Soft Confirmation Page
	// @FindBy(xpath = "//span[@id='confirmationHeader']")
	@FindBy(xpath = "//span[@id='Header']")
	private WebElement ConfirmFormHeader;

	// @FindBy(xpath = "//span[@id='confirmationStatusHeader']")
	@FindBy(xpath = "//span[@id='statusHeader']")
	private WebElement ConfirmFormSubHeader;

	@FindBy(xpath = "//span[@id='4/4']")
	private WebElement form4ProgressNumber;

	@FindBy(xpath = "//img[@id='smileyImg']")
	private WebElement ConfirmFormImage;

	@FindBy(xpath = "//p[@id='verificationTitle']")
	private WebElement ConfirmFormVerifyTitle;

	@FindBy(xpath = "//p[@id='emailCopy']")
	private WebElement ConfirmFormVerifyEmail;

	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement form1FirstName;

	@FindBy(xpath = "//input[@name='lastName']")
	private WebElement form1LastName;

	@FindBy(xpath = "//input[@name='tel']")
	private WebElement form1TelephoneNumber;

	@FindBy(xpath = "//input[@name='Email']")
	private WebElement form1Email;

	@FindBy(xpath = "//input[@name='confirmEmail']")
	private WebElement form1ConfirmEmail;

	@FindBy(xpath = "//*[contains(@aria-label,'first')]")
	private WebElement form1FirstNameError;

	@FindBy(xpath = "//*[contains(@aria-label,'last')]")
	private WebElement form1LastNameError;

	@FindBy(xpath = "//*[contains(@aria-label,'phone')]")
	private WebElement form1PhoneError;

	@FindBy(xpath = "//*[contains(@aria-label,'email')]")
	private WebElement form1EmailError;

	@FindBy(xpath = "//*[contains(@aria-label,'confirm')]")
	private WebElement form1ConfirmEmailError;
	
	@FindBy(css = "#tfbHeader")
	private WebElement tMobileBusinessHeader;

	/**
	 * @param webDriver
	 */
	public LazerSharkPage(WebDriver webDriver) {
		super(webDriver);
	}

	Integer email1 = getRandomNumberInRange(1000, 10000);
	String sam = email1 + Constants.LAZERSHARK_FORM1_EMAIL;

	// ------------------------------------------------------- LazerShark Page
	// Load Verification --------------------------------
	/**
	 * Verify that the page loaded completely.
	 */
	public LazerSharkPage verifyLazerSharkPageLoaded() {
		checkPageIsReady();
		waitForSpinnerInvisibility();
		try {
			if (getDriver().getCurrentUrl().contains(pageUrl)) {
				verifyLazerSharkPageUrl();
				Reporter.log("LazerShark page loaded");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("LazerShark page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public LazerSharkPage verifyLazerSharkPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		Reporter.log("LazerShark page URL verified");
		return this;
	}

	// ------------------------------------------------------- LazerShark Form1
	// Screen Verification --------------------------------
	/**
	 * Verify Form1 Headers Should be displayed:
	 */
	public LazerSharkPage verifyForm1Headers() {

		try {
			Assert.assertTrue(isElementDisplayed(form1Header));
			Assert.assertEquals(form1Header.getText(), Constants.LAZERSHARK_FORM1_HEADER);
			Assert.assertTrue(isElementDisplayed(form1SubHeader));
			Assert.assertEquals(form1SubHeader.getText(), Constants.LAZERSHARK_FORM1_SUBHEADER);
			Assert.assertTrue(isElementDisplayed(ProviderHeader));
			Assert.assertEquals(ProviderHeader.getText(), Constants.LAZERSHARK_CARRIER_HEADER);
			Assert.assertTrue(isElementDisplayed(RateHeader));
			Assert.assertEquals(RateHeader.getText(), Constants.LAZERSHARK_FORM1_UR_HEADER);
			Assert.assertTrue(isElementDisplayed(BOHeader));
			Assert.assertEquals(BOHeader.getText(), Constants.LAZERSHARK_FORM1_TFD_HEADER);
			Reporter.log("All Headers are displayed in LazerShark Form1 Page");
		} catch (Exception e) {
			Assert.fail("Headers are not displayed in LazerShark Form1 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Form1 Progress Number and Bar Should be displayed:
	 */
	public LazerSharkPage verifyForm1Progress() {

		try {
			Assert.assertTrue(isElementDisplayed(ProgressNumber));
			Assert.assertEquals(form1ProgressNumber.getText(), "1");
			Assert.assertTrue(isElementDisplayed(MyProgressBar));
			Reporter.log("Progress Active Number and Bar are getting displayed in LazerShark Form1 Page");
		} catch (Exception e) {
			Assert.fail("Progress Active Number and Bar are not getting displayed in LazerShark Form1 Page"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * Select Provider
	 */
	public LazerSharkPage selectProviderOption() {
		try {
			if (selectProvider.isEnabled()) {
				selectElementFromDropDown(selectProvider, "Text", Constants.LAZERSHARK_PROVIDER);
				Reporter.log("Provider " + Constants.LAZERSHARK_PROVIDER + " Options is selected.");
			} else {
				Assert.fail("Provider " + Constants.LAZERSHARK_PROVIDER + " Options is not selected");
			}
		} catch (Exception e) {
			Assert.fail("Provider " + Constants.LAZERSHARK_PROVIDER + " Options is not selected" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Rate of Experience Should be displayed and selected:
	 */
	public LazerSharkPage clickOnRate() {
		try {
			waitFor(ExpectedConditions.visibilityOf(form1RateOption), 5);
			if (form1RateOption.isDisplayed()) {
				form1Rate.click();
				Reporter.log("Rate of Experience Option is displayed and selected");
			} else {
				Assert.fail("Rate of Experience Options are not found");
			}
		} catch (Exception e) {
			Assert.fail("Rate of Experience Options are not found" + e.getMessage());
		}
		return this;
	}

	/**
	 * Select Business Interest Option
	 */
	public LazerSharkPage clickOnBusinessOption() {

		try {
			if (form1BusinessOptionYes.isDisplayed() && (form1BusinessOptionNo.isDisplayed())) {
				form1BusinessOptionYes.click();
				Reporter.log("Business Interest Option is displayed and selected");
			} else {
				Assert.fail("Select Business Interest Option is not displayed and selected");
			}
		} catch (Exception e) {
			Assert.fail("Select Business Interest Option is not displayed and selected");
		}
		return this;
	}

	/**
	 * Verify Form2 LegalTandC Should be displayed:
	 *
	 */
	public LazerSharkPage verifyForm1LegalTC() {

		try {
			Assert.assertTrue(isElementDisplayed(LegalTCMessage));
			Assert.assertEquals(LegalTCMessage.getText(), Constants.LAZERSHARK_FORM2_LegalTC);
			Reporter.log(LegalTCMessage.getText() + " displayed in LazerShark Form1 Page");
		} catch (Exception e) {
			Assert.fail(
					Constants.LAZERSHARK_FORM2_LegalTC + " not displayed in LazerShark Form1 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Form2 Legal TC Modal
	 */
	public LazerSharkPage clickOnForm1LegalTCModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(LegalTCLink), 5);
			LegalTCLink.click();
			Reporter.log("Clicked on Form1 LegalTC.");
			waitFor(ExpectedConditions.visibilityOf(GenericModal), 5);
			if (GenericModal.isDisplayed()) {
				Reporter.log("Legal Tearms & Conditions Model is displayed");
				GenericModalButton.click();
			} else
				Assert.fail("Legal Tearms & Conditions Model is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to click on Form1 LegalTC.");
		}
		return this;
	}

	/**
	 * Set Customer Information
	 */
	public LazerSharkPage enterCustExpDetails() {
		waitForSpinnerInvisibility();
		try {
			selectElementFromDropDown(selectProvider, "Text", Constants.LAZERSHARK_PROVIDER);
			form1Rate.click();
			form1BusinessOptionYes.click();
		} catch (Exception e) {
			Assert.fail("'Customer Experience Details were not able to enter/select" + e.getMessage());
		}
		return this;
	}

	/**
	 * Click on Form1 Continue button
	 */
	public LazerSharkPage clickOnForm1ContinueBtn() {
		checkPageIsReady();
		try {
			moveToElement(form1ContinueButton);
			clickElementWithJavaScript(form1ContinueButton);
			Reporter.log("Clicked on Form1 Continue button.");
			waitFor(ExpectedConditions.visibilityOf(form2FirstName), 10);
		} catch (Exception e) {
			Assert.fail("Failed to click on Form1 Continue button.");
		}
		return this;
	}

	// ------------------------------------------------------- LazerShark Form2
	// Screen Verification --------------------------------
	/**
	 * Verify Form2 Header Should be displayed:
	 */
	public LazerSharkPage verifyForm2Headers() {

		try {
			Assert.assertTrue(isElementDisplayed(form2Header));
			Assert.assertEquals(form2Header.getText(), Constants.LAZERSHARK_FORM2_HEADER);
			Assert.assertTrue(isElementDisplayed(form2SubHeader));
			Assert.assertEquals(form2SubHeader.getText(), Constants.LAZERSHARK_FORM2_SUBHEADER);
			Assert.assertTrue(isElementDisplayed(form2ExistingUserHeader));
			Assert.assertEquals(form2ExistingUserHeader.getText(), Constants.LAZERSHARK_FORM2_EUSER_HEADER);
			Reporter.log("Headers are displayed in LazerShark Form2 Page");
		} catch (Exception e) {
			Assert.fail("Headers are not displayed in LazerShark Form2 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Form2 Progress Number and Bar Should be displayed:
	 */
	public LazerSharkPage verifyForm2Progress() {

		try {
			Assert.assertTrue(isElementDisplayed(ProgressNumber));
			Assert.assertEquals(form2ProgressNumber.getText(), "2");
			Assert.assertTrue(isElementDisplayed(MyProgressBar));
			Reporter.log("Progress Active Number and Bar getting displayed in LazerShark Form1 Page");
		} catch (Exception e) {
			Assert.fail(
					"Progress Active Number and Bar not getting displayed in LazerShark Form1 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * First Name set and validations
	 */
	public LazerSharkPage validateFirstName() {
		waitForSpinnerInvisibility();
		try {
			if (form2FirstName.isDisplayed()) {
				form2FirstName.click();
				form2FirstName.sendKeys(Keys.TAB);
				if (form2FirstNameError.isDisplayed()) {
					Reporter.log("'Please enter your First Name' message displayed for First Name Empty Value");
					sendTextData(form2FirstName, Constants.LAZERSHARK_FORM2_FIRSTNAME);
				} else
					Assert.fail("'Please enter your First Name' message is not displayed for First Name Empty Value");
			}
		} catch (Exception e) {
			Assert.fail("'Please enter your First Name' message is not displayed for First Name Empty Value"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * First Name Alpha Numeric validations
	 */
	public LazerSharkPage validateFirstNameAlphaChar() {
		waitForSpinnerInvisibility();
		try {
			if (form2FirstName.isDisplayed()) {
				form2FirstName.click();
				sendTextData(form2FirstName, "simple1234567890,./;[]\"`<>?:{}" + "|~!@#$%^&*()_+=1234567890text");
				form2FirstName.sendKeys(Keys.TAB);
				Reporter.log(form2FirstName.getAttribute("value") + " as entered First Name value");
				Assert.assertEquals(form2FirstName.getAttribute("value"), "simpletext");
				Reporter.log("First Name field is allowing only Alpha Characters");
			} else
				Assert.fail("First Name Field is not Displayed");
		} catch (Exception e) {
			Assert.fail("First Name field is allowing other than Alpha Characters" + e.getMessage());
		}
		return this;
	}

	/**
	 * Last Name Alpha Numeric validations
	 */
	public LazerSharkPage validateLastNameAlphaChar() {
		waitForSpinnerInvisibility();
		try {
			if (form2LastName.isDisplayed()) {
				form2LastName.click();
				sendTextData(form2LastName, "simple1234567890,./;[]\"`<>?:{}" + "|~!@#$%^&*()_+=1234567890text");
				form2LastName.sendKeys(Keys.TAB);
				Reporter.log(form2LastName.getAttribute("value") + " as entered Last Name value");
				Assert.assertEquals(form2LastName.getAttribute("value"), "simpletext");
				Reporter.log("Last Name field is allowing only Alpha Characters");
			} else
				Assert.fail("Last Name Field is not Displayed");
		} catch (Exception e) {
			Assert.fail("Last Name field is allowing other than Alpha Characters" + e.getMessage());
		}
		return this;
	}

	/**
	 * Last Name Empty Field Validations
	 */
	public LazerSharkPage validateLastName1() {
		try {
			form2LastName.click();
			form2LastName.sendKeys(Keys.TAB);
			if (form2LastNameError.isDisplayed()) {
				Reporter.log("'Please enter your Last Name' message displayed for Last Name Empty Value");
			} else
				Assert.fail("'Please enter your Last Name' message is not displayed for Last Name Empty Value");
		} catch (Exception e) {
			Assert.fail("'Please enter your Last Name' message is not displayed for Last Name Empty Value"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * Validation minimum of 2 consecutive characters for Last name
	 */
	public LazerSharkPage validateLastName2() {
		try {
			if (form2LastName.isDisplayed()) {
				sendTextData(form2LastName, Constants.LAZERSHARK_FORM2_INVALID_LASTNAME);
				form2LastName.sendKeys(Keys.TAB);
				if (form2LastNameError.isDisplayed()) {
					Reporter.log("'Please enter your Last Name' message displayed for invalid Last Name");
					sendTextData(form2LastName, Constants.LAZERSHARK_FORM2_LASTNAME);
				} else
					Assert.fail("'Please enter your Last Name' message not displayed for invalid Last Name");
			}
		} catch (Exception e) {
			Assert.fail("'Please enter your Last Name' message not displayed for invalid Last Name" + e.getMessage());
		}
		return this;
	}

	/**
	 * Phone Number Mandatory Field Validations
	 */
	public LazerSharkPage validPhoneNumber1() {
		try {
			form2PhoneNumber.click();
			form2PhoneNumber.sendKeys(Keys.TAB);
			if (form2PhoneNumberError.isDisplayed()) {
				Reporter.log("'Please enter your phone' message displayed for Empty Phone Number");
			} else
				Assert.fail("'Please enter your phone' message displayed for Empty Phone Number");
		} catch (Exception e) {
			Assert.fail("'Please enter your phone' message is not displayed for Empty Phone Number" + e.getMessage());
		}
		return this;
	}

	/**
	 * Accepts 10 digits
	 */
	public LazerSharkPage validPhoneNumber2() {
		try {
			sendTextData(form2PhoneNumber, Constants.LAZERSHARK_FORM2_INVALID_PHNO);
			if (form2PhoneNumber.getAttribute("value").length() < 16) {
				if (form2PhoneNumberError.isDisplayed()) {
					Reporter.log("'Please enter your phone' message displayed if phone number < 10 digits");
					sendTextData(form2PhoneNumber, Constants.LAZERSHARK_FORM2_PHNO);
				} else
					Assert.fail("'Please enter your phone' message not displayed if phone number < 10 digits");
			}
		} catch (Exception e) {
			Assert.fail("'Please enter your phone' message not displayed if phone number < 10 digits");
		}
		return this;
	}

	/**
	 * Set Phone Number
	 */
	public LazerSharkPage setExistPhoneNumber() {
		try {
			waitFor(ExpectedConditions.visibilityOf(form2PhoneNumber), 5);
			sendTextData(form2PhoneNumber, Constants.LAZERSHARK_FORM2_ExistPHNO);
			form2PhoneNumber.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(GenericModal), 5);
			if (GenericModal.isDisplayed()) {
				Reporter.log("Phone Number Error Modal is displayed on existing T-Mobile Number");
				GenericModalButton.click();
				sendTextData(form2PhoneNumber, Constants.LAZERSHARK_FORM2_PHNO);
			} else
				Assert.fail("Phone Number Error Modal is not displayed on existing T-Mobile Number");
		} catch (Exception e) {
			Assert.fail("Phone Number Error Modal is not displayed on existing T-Mobile Number" + e.getMessage());
		}
		return this;
	}

	/**
	 * Email Mandatory Field Validations
	 */
	public LazerSharkPage validateEmail1() {
		try {
			waitFor(ExpectedConditions.visibilityOf(form2Email), 5);
			form2Email.click();
			form2Email.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(form2EmailError), 5);
			if (form2EmailError.isDisplayed()) {
				sendTextData(form2Email, Constants.LAZERSHARK_FORM2_INVALID_EMAIL);
				form2Email.sendKeys(Keys.TAB);
				waitFor(ExpectedConditions.visibilityOf(form2Email), 5);
				Reporter.log("'Please enter your Email' message displayed for Empty Email");
			} else
				Assert.fail("'Please enter your Email' message displayed for Empty Email");
		} catch (Exception e) {
			Assert.fail("'Please enter your Email' message is not displayed for Empty Email" + e.getMessage());
		}
		return this;
	}

	/**
	 * Validation for Email
	 */
	public LazerSharkPage validateEmail2() {
		try {
			sendTextData(form2Email, Constants.LAZERSHARK_FORM2_INVALID_EMAIL);
			form2Email.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(form2Email), 5);
			if (form2EmailError.isDisplayed()) {
				Reporter.log("'Please enter your Email' message displayed for invalid Email");
				sendTextData(form2Email, sam);
			} else
				Assert.fail("'Please enter your Email' message not displayed for invalid Email");
		} catch (Exception e) {
			Assert.fail("'Please enter your Email' message not displayed for invalid Email" + e.getMessage());
		}
		return this;
	}

	/**
	 * Confirm Email Mandatory Field Validations
	 */
	public LazerSharkPage validateConfirmEmail1() {
		try {
			form2ConfirmEmail.click();
			form2ConfirmEmail.sendKeys(Keys.TAB);
			if (form2EmailConfirmError.isDisplayed()) {
				Reporter.log("'Please confirm your Email' message displayed for Empty Email");
			} else
				Assert.fail("'Please confirm your Email' message displayed for Empty Email");
		} catch (Exception e) {
			Assert.fail("'Please confirm your Email' message is not displayed for Empty Email" + e.getMessage());
		}
		return this;
	}

	/**
	 * validation for email formatting only (e.g.:
	 * {characters/digits}@{characters/digits}.{characters/digits})
	 */
	public LazerSharkPage validateConfirmEmail2() {
		try {
			sendTextData(form2ConfirmEmail, Constants.LAZERSHARK_FORM2_INVALID_CEMAIL1);
			form2ConfirmEmail.sendKeys(Keys.TAB);
			if (form2EmailConfirmError.isDisplayed()) {
				Reporter.log("'Please confirm your Email' message displayed for invalid Email");
			} else
				Assert.fail("'Please confirm your Email' message not displayed for invalid Email");
		} catch (Exception e) {
			Assert.fail("'Please confirm your Email' message not displayed for invalid Email" + e.getMessage());
		}
		return this;
	}

	/**
	 * Validate both email and confirm email should be equal
	 */
	public LazerSharkPage validateConfirmEmail3() {
		try {
			sendTextData(form2ConfirmEmail, Constants.LAZERSHARK_FORM2_INVALID_CEMAIL2);
			form2ConfirmEmail.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(form2EmailConfirmError), 5);
			if (form2EmailConfirmError.isDisplayed()) {
				Reporter.log(
						"'Please confirm your Email' message displayed if both email and confirm email is not equal");
				Reporter.log(sam);
				sendTextData(form2ConfirmEmail, sam);
			} else
				Assert.fail(
						"'Please confirm your Email' message not displayed if both email and confirm email is not equal");
		} catch (Exception e) {
			Assert.fail("'Please confirm your Email' message not displayed if both email and confirm email is not equal"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Form2 PIH Yes selection:
	 */
	public LazerSharkPage verifyForm2YesOption() {
		try {
			form2PIHOptionYes.click();
			Assert.assertTrue(form2IMEINumber.isDisplayed());
			Reporter.log("IMEI Number Field dynamically appears by clicking on 'Yes' in Form2 Page");
		} catch (Exception e) {
			Assert.fail(
					"IMEI Number Field not dynamically appears by clicking on 'Yes' in Form2 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Confirm IMEI Mandatory Field Validations
	 */
	public LazerSharkPage verifyForm2IMEIError1() {
		try {
			form2IMEINumber.click();
			form2IMEINumber.sendKeys(Keys.TAB);
			if (form2IMEINumberError.isDisplayed()) {
				Reporter.log("'Please enter correct IMEI Number' message displayed for Empty IMEI");
			} else
				Assert.fail("'Please enter correct IMEI Number' message displayed for Empty IMEI");
		} catch (Exception e) {
			Assert.fail("'Please enter correct IMEI Number' message is not displayed for Empty IMEI" + e.getMessage());
		}
		return this;
	}

	/**
	 * validate IMEI format accepts 15 digits only
	 */
	public LazerSharkPage verifyForm2IMEIError2() {
		try {
			sendTextData(form2IMEINumber, Constants.LAZERSHARK_FORM2_INVALID_IMEINO);
			if (form2IMEINumber.getAttribute("value").length() < 16) {
				if (form2IMEINumberError.isDisplayed()) {
					Reporter.log("'Please enter correct IMEI Number' message displayed if IMEI number < 16 digits");
					sendTextData(form2IMEINumber, Constants.LAZERSHARK_FORM2_IMEINO);
				}
			} else
				Assert.fail("'Please enter correct IMEI Number' message not displayed if IMEI number < 16 digits");
		} catch (Exception e) {
			Assert.fail("'Please enter correct IMEI Number' message not displayed if IMEI number < 16 digits");
		}
		return this;
	}

	/**
	 * Verify Form2 IMEI Icon selection:
	 */
	public LazerSharkPage verifyForm2IMEIIcon() {
		try {
			form2IMEIIcon.click();
			waitFor(ExpectedConditions.visibilityOf(form2IMEIIconMessage), 5);
			Assert.assertTrue(form2IMEIIconMessage.isDisplayed());
			Reporter.log("IMEI Tool Tip Message is dynamically appears by clicking on IMEI Icon in Form2 Page");
		} catch (Exception e) {
			Assert.fail("IMEI Tool Tip Message is not dynamically appears by clicking on IMEI Icon in Form2 Page"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Form2 LegalTandC Should be displayed:
	 */
	public LazerSharkPage verifyForm2LegalTC() {

		try {
			Assert.assertTrue(isElementDisplayed(LegalTCMessage));
			Assert.assertEquals(LegalTCMessage.getText(), Constants.LAZERSHARK_FORM2_LegalTC);
			Reporter.log(LegalTCMessage.getText() + "' displayed in LazerShark Form2 Page");
		} catch (Exception e) {
			Assert.fail(
					Constants.LAZERSHARK_FORM2_LegalTC + "' not displayed in LazerShark Form2 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Click on Form2 Legal TC
	 */
	public LazerSharkPage clickOnForm2LegalTCLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(LegalTCLink), 5);
			LegalTCLink.click();
			Reporter.log("Clicked on Form2 LegalTC.");
			waitFor(ExpectedConditions.visibilityOf(GenericModal), 5);
			if (GenericModal.isDisplayed()) {
				Reporter.log("Legal Tearms & Conditions Model is displayed");
				GenericModalButton.click();
			} else
				Assert.fail("Legal Tearms & Conditions Model is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to click on Form2 LegalTC.");
		}
		return this;
	}

	/**
	 * Validate Email Error Modal
	 */
	public LazerSharkPage validateEmailErrorModal() {
		try {
			sendTextData(form2Email, Constants.LAZERSHARK_FORM2_EMAILMODAL_ERROR);
			sendTextData(form2ConfirmEmail, Constants.LAZERSHARK_FORM2_EMAILMODAL_ERROR);
			form2ConfirmEmail.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(form2ContinueButton), 5);
			form2ContinueButton.click();
			waitFor(ExpectedConditions.visibilityOf(GenericModal), 5);
			if (GenericModal.isDisplayed()) {
				Reporter.log("Email Error Modal is displayed on existing Email");
				GenericModalButton.click();
				waitForSpinnerInvisibility();
				sendTextData(form2Email, Constants.LAZERSHARK_FORM2_EMAIL);
				sendTextData(form2ConfirmEmail, Constants.LAZERSHARK_FORM2_CEMAIL);
			} else
				Assert.fail("Email Error Modal is not displayed on existing Email");
		} catch (Exception e) {
			Assert.fail("Email Error Modal is not displayed on existing Email" + e.getMessage());
		}
		return this;
	}

	/**
	 * Set Customer Information
	 */
	public LazerSharkPage enterCustDetails() {
		waitForSpinnerInvisibility();
		try {
			sendTextData(form2FirstName, Constants.LAZERSHARK_FORM2_FIRSTNAME);
			sendTextData(form2LastName, Constants.LAZERSHARK_FORM2_LASTNAME);
			sendTextData(form2PhoneNumber, Constants.LAZERSHARK_FORM2_PHNO);
			sendTextData(form2Email, Constants.LAZERSHARK_FORM2_EMAIL);
			sendTextData(form2ConfirmEmail, Constants.LAZERSHARK_FORM2_CEMAIL);
			form2PIHOptionNo.click();
		} catch (Exception e) {
			Assert.fail("'Customer Details were not able to enter/select" + e.getMessage());
		}
		return this;
	}

	/**
	 * Click on Form2 Continue Button
	 */
	public LazerSharkPage clickOnForm2ContinueBtn() {
		try {
			checkPageIsReady();
			moveToElement(form2ContinueButton);
			clickElementWithJavaScript(form2ContinueButton);
			Reporter.log("Clicked on Form2 Continue button.");
			waitFor(ExpectedConditions.visibilityOf(form3Address1), 5);
		} catch (Exception e) {
			Assert.fail("Failed to click on Form2 Continue button.");
		}
		return this;
	}

	// ------------------------------------------------------- LazerShark Form3
	// Screen Verification --------------------------------
	/**
	 * Verify Form3 Header Should be displayed:
	 */
	public LazerSharkPage verifyForm3Headers() {

		try {
			Assert.assertTrue(isElementDisplayed(form3Header));
			Assert.assertEquals(form3Header.getText(), Constants.LAZERSHARK_FORM3_HEADER);
			Assert.assertTrue(isElementDisplayed(form3SubHeader));
			Assert.assertEquals(form3SubHeader.getText(), Constants.LAZERSHARK_FORM3_SUBHEADER);
			Reporter.log(" Headers are getting displayed in LazerShark Form2 Page");
		} catch (Exception e) {
			Assert.fail("Headers are not displayed in LazerShark Form2 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Form3 Progress Number Should be displayed:
	 */
	public LazerSharkPage verifyForm3Progress() {

		try {
			Assert.assertTrue(isElementDisplayed(ProgressNumber));
			Assert.assertEquals(form3ProgressNumber.getText(), "3");
			Assert.assertTrue(isElementDisplayed(MyProgressBar));
			Reporter.log("Progress Active Number is getting displayed in LazerShark Form3 Page");
		} catch (Exception e) {
			Assert.fail("Progress Active Number is not getting displayed in LazerShark Form3 Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Confirm Address1 Mandatory Field Validations
	 */
	public LazerSharkPage verifyForm3Address() {
		try {
			waitFor(ExpectedConditions.visibilityOf(form3Address1), 5);
			if (form3Address1.isDisplayed() && form3Address2.isDisplayed()) {
				form3Address1.click();
				form3Address1.sendKeys(Keys.TAB);
				if (form3Address1Error.isDisplayed()) {
					Reporter.log("'Please enter your address' message displayed for Empty Address");
					Integer num = getRandomNumberInRange(1000, 10000);
					sendTextData(form3Address1, Constants.LAZERSHARK_FORM3_ADDRESS1);
					sendTextData(form3Address2, Constants.LAZERSHARK_FORM3_ADDRESS2 + num);
				} else
					Assert.fail("'Please enter your address' message is not displayed for Empty Address");
			}
		} catch (Exception e) {
			Assert.fail("'Please enter your address' message is not displayed for Empty Address" + e.getMessage());
		}
		return this;
	}

	/**
	 * Confirm City Mandatory Field Validations
	 *
	 */
	public LazerSharkPage verifyForm3CityError() {
		try {
			if (form3City.isDisplayed()) {
				form3City.click();
				form3City.sendKeys(Keys.TAB);
				if (form3CityError.isDisplayed()) {
					Reporter.log("'Please enter your city' message displayed for Empty City");
					sendTextData(form3City, Constants.LAZERSHARK_FORM3_CITY);
				} else
					Assert.fail("'Please enter your city' message displayed for Empty City");
			}
		} catch (Exception e) {
			Assert.fail("'Please enter your city' message is not displayed for Empty City" + e.getMessage());
		}
		return this;
	}

	/**
	 * Confirm State Mandatory Field Validations
	 */
	public LazerSharkPage verifyForm3StateError() {
		try {
			form3State.click();
			form3State.sendKeys(Keys.TAB);
			if (form3StateError.isDisplayed()) {
				Reporter.log("'Please enter your state' message displayed for Empty State");
				selectElementFromDropDown(form3State, "Text", "WA");
			} else
				Assert.fail("'Please enter your state' message displayed for Empty State");
		} catch (Exception e) {
			Assert.fail("'Please enter your state' message is not displayed for Empty State" + e.getMessage());
		}
		return this;
	}

	/**
	 * Zip Accepts 5 digits
	 */
	public LazerSharkPage validZip() {
		try {
			sendTextData(form3Zip, Constants.LAZERSHARK_FORM3_INVALID_ZIP);
			form3Zip.sendKeys(Keys.TAB);
			if (form3Zip.getAttribute("value").length() < 5) {
				if (form3ZipError.isDisplayed()) {
					Reporter.log("'Please enter correct zip' message displayed if zip < 5 digits");
				} else
					Assert.fail("'Please enter correct zip' message not displayed if zip < 5 digits");
			}
		} catch (Exception e) {
			Assert.fail("'Please enter correct zip' message not displayed if zip < 5 digits");
		}
		return this;
	}

	/**
	 * Invalid Zip Error Modal
	 * 
	 */
	public LazerSharkPage invalidZip() {
		try {
			sendTextData(form3Zip, Constants.LAZERSHARK_FORM3_INVALID_ZIP1);
			form3Zip.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(form3Submit), 5);
			form3Submit.click();
			waitFor(ExpectedConditions.visibilityOf(GenericModal), 5);
			if (GenericModal.isDisplayed()) {
				Reporter.log("Invalid Zip Error Modal is displayed on invalid Zip");
				GenericModalButton.click();
				Reporter.log("Address Field values are cleared by closing of Zip Modal");
				sendTextData(form3Zip, Constants.LAZERSHARK_FORM3_ZIP);
			} else
				Assert.fail("Invalid Zip Error Modal is not displayed on invalid Zip");
		} catch (Exception e) {
			Assert.fail("Invalid Zip Error Modal is not displayed on invalid Zip" + e.getMessage());
		}
		return this;
	}

	/**
	 * Enter Form3 Customer Address
	 */
	public LazerSharkPage enterAddress() {
		try {
			Integer num1 = getRandomNumberInRange(1000, 10000);
			sendTextData(form3Address1, Constants.LAZERSHARK_FORM3_ADDRESS1);
			sendTextData(form3Address2, Constants.LAZERSHARK_FORM3_ADDRESS2 + num1);
			sendTextData(form3City, Constants.LAZERSHARK_FORM3_CITY);
			selectElementFromDropDown(form3State, "Text", "WA");
			sendTextData(form3Zip, Constants.LAZERSHARK_FORM3_ZIP);
		} catch (Exception e) {
			Assert.fail("Customer Informaiton is not able to entered" + e.toString());
		}
		return this;
	}

	/**
	 * Enter Form3 Existing Customer Address
	 */
	public LazerSharkPage enterExistAddress() {
		try {
			selectElementFromDropDown(selectProvider, "Text", Constants.LAZERSHARK_PROVIDER);
			form1Rate.click();
			form1BusinessOptionYes.click();
			moveToElement(form1ContinueButton);
			clickElementWithJavaScript(form1ContinueButton);
			waitFor(ExpectedConditions.visibilityOf(form2FirstName), 5);
			sendTextData(form2FirstName, Constants.LAZERSHARK_FORM2_FIRSTNAME);
			sendTextData(form2LastName, Constants.LAZERSHARK_FORM2_LASTNAME);
			sendTextData(form2PhoneNumber, Constants.LAZERSHARK_FORM2_PHNO);
			Integer emailnum1 = getRandomNumberInRange(10, 1000);
			String validEmail = emailnum1 + Constants.LAZERSHARK_FORM2_EMAIL;
			sendTextData(form2Email, validEmail);
			sendTextData(form2ConfirmEmail, validEmail);
			form2PIHOptionNo.click();
			moveToElement(form2ContinueButton);
			clickElementWithJavaScript(form2ContinueButton);
			waitFor(ExpectedConditions.visibilityOf(form3Address1), 5);
			sendTextData(form3Address1, "3545 FACTORIA BLVD SE");
			sendTextData(form3Address2, "SUITE 300");
			sendTextData(form3City, Constants.LAZERSHARK_FORM3_CITY);
			selectElementFromDropDown(form3State, "Text", "WA");
			sendTextData(form3Zip, Constants.LAZERSHARK_FORM3_ZIP);
			waitFor(ExpectedConditions.visibilityOf(form3Submit), 3);
			form3Submit.click();
			waitFor(ExpectedConditions.visibilityOf(form3SubmitStandardModal), 5);
			form3ModelConfirm.click();
			waitFor(ExpectedConditions.visibilityOf(GenericModal), 5);

			if (GenericModal.isDisplayed()) {
				Reporter.log("Existing Customer Address Model is displayed");
				GenericModalButton.click();
				waitFor(ExpectedConditions.visibilityOf(form3Address1), 5);
				elementFocus(form3Address1);
			} else
				Assert.fail("Existing Customer Address Model is not displayed");
		} catch (Exception e) {
			Assert.fail("Existing Customer Address Model is not displayed" + e.toString());
		}
		return this;
	}

	/**
	 * Enter Form3 Existing Customer Email
	 */
	public LazerSharkPage enterExistEmail() {
		try {
			selectElementFromDropDown(selectProvider, "Text", Constants.LAZERSHARK_PROVIDER);
			form1Rate.click();
			form1BusinessOptionYes.click();
			moveToElement(form1ContinueButton);
			clickElementWithJavaScript(form1ContinueButton);
			waitFor(ExpectedConditions.visibilityOf(form2FirstName), 5);
			sendTextData(form2FirstName, Constants.LAZERSHARK_FORM2_FIRSTNAME);
			sendTextData(form2LastName, Constants.LAZERSHARK_FORM2_LASTNAME);
			sendTextData(form2PhoneNumber, Constants.LAZERSHARK_FORM2_PHNO);
			sendTextData(form2Email, "test@test.com");
			sendTextData(form2ConfirmEmail, "test@test.com");
			form2PIHOptionNo.click();
			moveToElement(form2ContinueButton);
			clickElementWithJavaScript(form2ContinueButton);
			waitFor(ExpectedConditions.visibilityOf(GenericModal), 5);
			if (GenericModal.isDisplayed()) {
				Reporter.log("Existing Customer Email Model is displayed");
				GenericModalButton.click();
				waitFor(ExpectedConditions.visibilityOf(form2Email), 5);
				elementFocus(form2Email);
			} else
				Assert.fail("Existing Customer Email Model is not displayed");
		} catch (Exception e) {
			Assert.fail("Existing Customer Email Model is not displayed" + e.toString());
		}
		return this;
	}

	/**
	 * Click on Form3 Submit button
	 */
	public LazerSharkPage clickOnForm3SubmitBtn200() {
		try {
			waitFor(ExpectedConditions.visibilityOf(form3Submit), 3);
			form3Submit.click();
			waitFor(ExpectedConditions.visibilityOf(form3ModelConfirm), 3);
			form3ModelConfirm.click();
			Reporter.log("Form3 Submit Modal Confirm Button is clicked");
			waitFor(ExpectedConditions.visibilityOf(ConfirmFormHeader), 3);
		} catch (Exception e) {
			Assert.fail("Click on submit button failed" + e.toString());
		}
		return this;
	}

	/**
	 * Click on Form3 Model - Back button
	 *
	 */
	public LazerSharkPage clickOnForm3ModelBackBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(form3ModelBack), 3);
			if (form3ModelBack.isDisplayed()) {
				Reporter.log("Form3 Submit Model is displayed and Back Button is enabled");
				form3ModelBack.click();
				Reporter.log("Clicked on Model Back Button ");
			} else {
				Assert.fail("Model Back Button is diabled");
			}
		} catch (Exception e) {
			Assert.fail("Model Back Button Click is failed" + e.toString());
		}
		return this;
	}

	/**
	 * Verify Form3 Address Standardization Modal
	 */
	public LazerSharkPage clickOnForm3Model() {
		try {
			waitFor(ExpectedConditions.visibilityOf(form3SubmitStandardModal), 5);
			if (form3SubmitStandardModal.isDisplayed()) {
				Reporter.log("Form3 Submit Modal is displayed");
			} else {
				Assert.fail("Form3 Submit Modal is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Form3 Submit Modal is not displayed" + e.toString());
		}
		return this;
	}

	/**
	 * Click on Form3 Modal - Confirm button
	 */
	public LazerSharkPage clickOnForm3ModelConfirmBtn() {
		try {
			form3ModelConfirm.click();
			Reporter.log("Form3 Submit Modal Confirm Button is clicked");
			waitFor(ExpectedConditions.visibilityOf(ConfirmFormHeader), 5);
		} catch (Exception e) {
			Assert.fail("Form3 Submit Modal Confirm Button is not clicked" + e.toString());
		}
		return this;
	}

	// ------------------------------------------------------- LazerShark Soft
	// Confirmation Screen Verification --------------------------------
	/**
	 * Verify Soft Confirm Header Should be displayed:
	 */
	public LazerSharkPage verifySoftConfirmHeaders() {
		try {
			waitFor(ExpectedConditions.visibilityOf(ConfirmFormHeader), 5);
			Assert.assertTrue(isElementDisplayed(ConfirmFormHeader));
			Assert.assertEquals(ConfirmFormHeader.getText(), Constants.LAZERSHARK_SOFTCONFIRM_HEADER);
			Assert.assertTrue(isElementDisplayed(ConfirmFormSubHeader));
			Assert.assertEquals(ConfirmFormSubHeader.getText(), Constants.LAZERSHARK_SOFTCONFIRM_SUBHEADER);
			Reporter.log("Headers displayed in LazerShark Soft Confirm Page");
		} catch (Exception e) {
			Assert.fail("Headers not displayed in LazerShark Soft Confirm Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Form4 Progress Number & Bar:
	 */
	public LazerSharkPage verifyForm4Progress() {

		try {
			Assert.assertTrue(isElementDisplayed(ProgressNumber));
			Assert.assertEquals(form4ProgressNumber.getText(), "4");
			Assert.assertTrue(isElementDisplayed(MyProgressBar));
			Reporter.log("Progress Active Number and Bar are getting displayed in LazerShark Form3 Page");
		} catch (Exception e) {
			Assert.fail("Progress Active Number and Bare are not getting displayed in LazerShark Form3 Page"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Soft Confirm Image Should be displayed:
	 */
	public LazerSharkPage verifySoftConfirmDetails() {

		try {
			Assert.assertTrue(isElementDisplayed(ConfirmFormImage));
			Assert.assertTrue(isElementDisplayed(ConfirmFormVerifyTitle));
			Assert.assertEquals(ConfirmFormVerifyTitle.getText(), Constants.LAZERSHARK_SOFTCONFIRM_VERIFYTITLE);
			Assert.assertTrue(isElementDisplayed(ConfirmFormVerifyEmail));
			Assert.assertEquals(ConfirmFormVerifyEmail.getText(),
					"Click on the link we sent to " + sam + " to validate your request.");
			Reporter.log("Soft Confirmation Details displayed");
		} catch (Exception e) {
			Assert.fail("Soft Confirmation Details are not displayed" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify LazerShark Page
	 */
	public LazerSharkPage verifyLazerSharkPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			//System.out.println("LazerShark page is displayed");
			Reporter.log("LazerShark page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("LazerShark page not displayed");
		}
		return this;

	}

	/**
	 * First Name set and validations
	 */
	public LazerSharkPage validateForm1FirstNameField() {
		try {
			waitForSpinnerInvisibility();
			if (isElementDisplayed(form1FirstName)) {
				form1FirstName.click();
				form1FirstName.sendKeys(Keys.TAB);
				if (form1FirstNameError.isDisplayed()) {
					sendTextData(form1FirstName, Constants.LAZERSHARK_FORM1_FIRSTNAME);
					Reporter.log("First name text field data validation is success");
				} else
					Assert.fail("First name text field data validation failed");
				    Reporter.log("First name text field data validation failed");
			}
		} catch (Exception e) {
			Assert.fail("First name text field data validation failed");
			Reporter.log("First name text field data validation failed");
		}
		return this;
	}

	/**
	 * Last Name set and validations
	 */
	public LazerSharkPage validateForm1LastNameField() {
		try {
			waitForSpinnerInvisibility();
			if (isElementDisplayed(form1LastName)) {
				form1LastName.click();
				sendTextData(form1LastName, Constants.LAZERSHARK_FORM1_INVALID_LASTNAME);
				form1LastName.sendKeys(Keys.TAB);
				if (form1LastNameError.isDisplayed()) {
					Reporter.log("'Please enter your Last Name' message displayed for invalid Last Name");
					form1LastName.click();
					form1LastName.clear();
				}
				form1LastName.sendKeys(Keys.TAB);
				if (form1LastNameError.isDisplayed()) {
					sendTextData(form1LastName, Constants.LAZERSHARK_FORM1_LASTNAME);
					Reporter.log("Last name text field data validation is success");
				}
			}else
				Assert.fail("Last name text field data validation failed");
		    Reporter.log("Last name text field data validation failed");
		} catch (Exception e) {
			Assert.fail("Last name text field data validation failed");
		    Reporter.log("Last name text field data validation failed");
		}
		return this;
	}
	
	
	

	/**
	 * Validation minimum of 2 consecutive characters for Last name
	 */
	public LazerSharkPage validateForm1LastNameFieldForInValidCharacters() {
		try {
			if (form2LastName.isDisplayed()) {
				sendTextData(form2LastName, Constants.LAZERSHARK_FORM2_INVALID_LASTNAME);
				form2LastName.sendKeys(Keys.TAB);
				if (form2LastNameError.isDisplayed()) {
					Reporter.log("'Please enter your Last Name' message displayed for invalid Last Name");
					sendTextData(form2LastName, Constants.LAZERSHARK_FORM2_LASTNAME);
				} else
					Assert.fail("'Please enter your Last Name' message not displayed for invalid Last Name");
			}
		} catch (Exception e) {
			Assert.fail("'Please enter your Last Name' message not displayed for invalid Last Name" + e.getMessage());
		}
		return this;
	}

	/**
	 * Phone field set and validations
	 */
	public LazerSharkPage validateForm1PhoneField() {
		try {
			waitForSpinnerInvisibility();
			if (isElementDisplayed(form1TelephoneNumber)) {
				form1TelephoneNumber.clear();
				form1TelephoneNumber.click();
				sendTextData(form1TelephoneNumber, Constants.LAZERSHARK_FORM1_INVALID_PHNO);
				//System.out.println(form1TelephoneNumber.getAttribute("value").length());
				if (form1TelephoneNumber.getAttribute("value").length() < 14) {
					if (form1PhoneError.isDisplayed()) {
					form1TelephoneNumber.click();
					form1TelephoneNumber.clear();
					form1TelephoneNumber.sendKeys(Keys.TAB);
					}
					if (form1PhoneError.isDisplayed()) {
						form1TelephoneNumber.click();
						sendTextData(form1TelephoneNumber, Constants.LAZERSHARK_FORM1_PHNO);
						 Reporter.log("Phone number text field data validation is success");
					}
				} else {
					Assert.fail("Phone number text field data validation failed");
				    Reporter.log("Phone number text field data validation failed");
				}
			}
		} catch (Exception e) {
			Assert.fail("Phone number text field data validation failed");
		    Reporter.log("Phone number text field data validation failed");
		}
		return this;
	}
	
	
	

	/**
	 * Email field Name set and validations
	 */
	public LazerSharkPage validateForm1EmailField() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(form1Email), 300);
			form1Email.click();
			form1Email.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(form1EmailError), 5);
			Assert.assertTrue(isElementDisplayed(form1EmailError));
			waitFor(ExpectedConditions.visibilityOf(form1Email), 5);
			form1Email.click();
			sendTextData(form1Email, Constants.LAZERSHARK_FORM1_INVALID_EMAIL);
			Assert.assertTrue(isElementDisplayed(form1EmailError));
			waitFor(ExpectedConditions.visibilityOf(form1Email), 5);
			form1Email.click();
			form1Email.clear();
			sendTextData(form1Email, Constants.LAZERSHARK_FORM1_EMAIL);
			 Reporter.log("Email text field data validation is success");
		} catch (Exception e) {
			Assert.fail("Email text field data validation failed");
		    Reporter.log("Email text field data validation failed");
		}
		return this;
		}

	/**
	 * Last Name set and validations
	 */
	public LazerSharkPage validateForm1ConfirmEmailField() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(form1ConfirmEmail), 30);
			form1ConfirmEmail.click();
			form1ConfirmEmail.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(form1ConfirmEmailError), 5);
			Assert.assertTrue(isElementDisplayed(form1ConfirmEmailError));
			waitFor(ExpectedConditions.visibilityOf(form2ConfirmEmail), 5);
			form2ConfirmEmail.click();
			sendTextData(form2ConfirmEmail, Constants.LAZERSHARK_FORM2_INVALID_CEMAIL1);
			Assert.assertTrue(isElementDisplayed(form1ConfirmEmailError));
			waitFor(ExpectedConditions.visibilityOf(form1ConfirmEmail), 5);
			form1ConfirmEmail.click();
			form1ConfirmEmail.clear();
			sendTextData(form1ConfirmEmail, Constants.LAZERSHARK_FORM1_EMAIL);
			 Reporter.log("Confirm Email text field data validation is success");
			 Thread.sleep(10000);
		} catch (Exception e) {
			Assert.fail("Confirm Email text field data validation failed");
		    Reporter.log("Confirm Email text field data validation failed");
		}
		return this;
		}
	
	
	/**
	 * Verify  Are you interested in T-Mobile for Business? message header
	 */
	public LazerSharkPage verifyTMobileBusineesHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(tMobileBusinessHeader), 10);
			Assert.assertTrue(isElementDisplayed(tMobileBusinessHeader));
			Reporter.log("TMobile businees header displayed");
		} catch (Exception e) {
			Reporter.log("TMobile businees header not displayed");
			Assert.fail("TMobile businees header not displayed");
		}
		return this;
	}
}