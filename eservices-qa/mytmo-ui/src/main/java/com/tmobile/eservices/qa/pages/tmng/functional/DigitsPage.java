package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class DigitsPage extends TmngCommonPage {
	private Map<String, String> data;
	private WebDriver driver;
	private int timeout = 30;

	private final String pageLoadedText = "Keep a group connected with a single number that rings on every phone—perfect for a family that wants to replace their landline with a mobile number";

	private final String pageUrl = "/offers/t-mobile-digits";

	@FindBy(css = "#slick-slide10 button")
	private WebElement _1;

	@FindBy(css = "#slick-slide11 button")
	private WebElement _2;

	@FindBy(css = "#slick-slide12 button")
	private WebElement _3;

	@FindBy(css = "#slick-slide13 button")
	private WebElement _4;

	@FindBy(css = "#slick-slide14 button")
	private WebElement _5;

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	private WebElement about;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/accessibility-policy']")
	private WebElement accessibility;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	private WebElement accessories;

	@FindBy(css = "a[href='//prepaid-phones.t-mobile.com/prepaid-activate']")
	private WebElement activateYourPrepaidPhoneOrDevice;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	private WebElement business2;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	private WebElement callUs;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) button.accordion-toggle.collapsed.")
	private WebElement canICallOrText1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement canICallOrText2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(5) button.accordion-toggle.collapsed.")
	private WebElement canIGetABusiness1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement canIGetABusiness2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button.accordion-toggle.collapsed.")
	private WebElement canIPairMyWearable1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement canIPairMyWearable2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement canIUseMyNew1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement canIUseMyNew2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) button.accordion-toggle.collapsed.")
	private WebElement canIUseWearablesWhile1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement canIUseWearablesWhile2;

	@FindBy(css = "a[href='//www.t-mobile.com/careers']")
	private WebElement careers;

	@FindBy(css = "a[href='http://www.t-mobile.com/orderstatus/default.aspx']")
	private WebElement checkOrderStatus;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	private WebElement checkOutTheCoverage;

	@FindBy(css = "#ebd2521f2b3580018db6f024108f1c64d79dcfc4 div:nth-of-type(2) button.close")
	private WebElement closeVideo1;

	@FindBy(css = "#bcb7a68c8276d743226c7e98f793df298d9b27fc div:nth-of-type(2) button.close")
	private WebElement closeVideo2;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info']")
	private WebElement consumerInformation;

	@FindBy(css = "a[href='http://www.t-mobile.com/contact-us.html']")
	private WebElement contactInformation;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	private WebElement deals2;

	@FindBy(css = "a[href='//www.telekom.com/']")
	private WebElement deutscheTelekom;

	@FindBy(css = "a[href='//support.t-mobile.com/community/phones-tablets-devices']")
	private WebElement deviceSupport;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement doINeedANew1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement doINeedANew2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) button.accordion-toggle.collapsed.")
	private WebElement doesCallerIdWorkWith1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement doesCallerIdWorkWith2;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	private WebElement espaol;

	@FindBy(css = "a.lang-toggle-wrapper")
	private WebElement espaolEspaol;

	@FindBy(css = "a[href='http://www.t-mobile.com/store-locator.html']")
	private WebElement findAStore;

	@FindBy(css = "a[href='http://www.t-mobile.com/promotions']")
	private WebElement getARebate;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement howDoIAccessAnd1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement howDoIAccessAnd2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(6) button.accordion-toggle.collapsed")
	private WebElement howDoIGetDigits1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(6) button.accordion-toggle.collapsed")
	private WebElement howDoIGetDigits2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) button.accordion-toggle.collapsed.")
	private WebElement howDoIKnowIf1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement howDoIKnowIf2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) button.accordion-toggle.collapsed.")
	private WebElement howDoIShareMy1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement howDoIShareMy2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) button.accordion-toggle.collapsed.")
	private WebElement howHaveYouMadeSure1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement howHaveYouMadeSure2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement howMuchDoesDigitsCost1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement howMuchDoesDigitsCost2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(5) div.panel-collapse.collapse.anchorID-312814 div.panel-body p:nth-of-type(3) a")
	private WebElement httpsmydigitsTmobileCom1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(5) div.panel-collapse.collapse.anchorID-312814 div.panel-body p:nth-of-type(3) a")
	private WebElement httpsmydigitsTmobileCom2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div.panel-collapse.collapse.70 div.panel-body ul li:nth-of-type(3) span a")
	private WebElement httpswebdigitsTmobileCom1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) div.panel-collapse.collapse.72 div.panel-body p span:nth-of-type(2) a")
	private WebElement httpswebdigitsTmobileCom2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-collapse.collapse.75 div.panel-body p span:nth-of-type(1) a")
	private WebElement httpswebdigitsTmobileCom3;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) div.panel-collapse.collapse.70 div.panel-body ul li:nth-of-type(3) span a")
	private WebElement httpswebdigitsTmobileCom4;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) div.panel-collapse.collapse.72 div.panel-body p span:nth-of-type(2) a")
	private WebElement httpswebdigitsTmobileCom5;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) div.panel-collapse.collapse.75 div.panel-body p span:nth-of-type(1) a")
	private WebElement httpswebdigitsTmobileCom6;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) div.panel-collapse.collapse.72 div.panel-body p span:nth-of-type(3) a")
	private WebElement httpwwwTmobileComdigits1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(6) div.panel-collapse.collapse.74 div.panel-body p span:nth-of-type(3) a")
	private WebElement httpwwwTmobileComdigits2;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) div.panel-collapse.collapse.72 div.panel-body p span:nth-of-type(3) a")
	private WebElement httpwwwTmobileComdigits3;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(6) div.panel-collapse.collapse.74 div.panel-body p span:nth-of-type(3) a")
	private WebElement httpwwwTmobileComdigits4;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(5) button.accordion-toggle.collapsed.")
	private WebElement iWantToUseMy1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement iWantToUseMy2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement ifIAmAnExisting1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement ifIAmAnExisting2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(6) button.accordion-toggle.collapsed")
	private WebElement ifIHaveTheDigits1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(6) button.accordion-toggle.collapsed")
	private WebElement ifIHaveTheDigits2;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx#choicesaboutadvertising']")
	private WebElement interestbasedAds;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	private WebElement internationalCalling;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global.html']")
	private WebElement internationalRates;

	@FindBy(css = "a[href='http://iot.t-mobile.com/']")
	private WebElement internetOfThings;

	@FindBy(css = "a[href='http://investor.t-mobile.com/CorporateProfile.aspx?iid=4091145']")
	private WebElement investorRelations;

	@FindBy(css = ".marketing-page.ng-scope.no-scroll header.global-header.light div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	private WebElement logIn;

	@FindBy(css = "button.hamburger.hamburger-border")
	private WebElement menuMenu;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button.accordion-toggle.collapsed.")
	private WebElement myMobilePhoneNumberAlready1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement myMobilePhoneNumberAlready2;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile;

	@FindBy(css = "button.slick-next.slick-arrow")
	private WebElement next;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/policies/internet-service']")
	private WebElement openInternet;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	private WebElement phones;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	private WebElement plans;

	@FindBy(css = "a[href='//support.t-mobile.com/community/plans-services']")
	private WebElement plansServices;

	@FindBy(css = ".marketing-page.ng-scope.no-scroll header.global-header.light div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	private WebElement prepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/news']")
	private WebElement press;

	@FindBy(css = "button.slick-prev.slick-arrow.ng-scope.slick-disabled")
	private WebElement previous;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/privacy']")
	private WebElement privacyCenter;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx']")
	private WebElement privacyStatement;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/safety/9-1-1']")
	private WebElement publicSafety911;

	@FindBy(css = "a[href='http://www.t-mobilepr.com/']")
	private WebElement puertoRico;

	@FindBy(css = "a[href='//support.t-mobile.com/community/billing']")
	private WebElement questionsAboutYourBill;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-refill-api-bridging']")
	private WebElement refillYourPrepaidAccount;

	@FindBy(css = ".marketing-page.ng-scope.no-scroll div:nth-of-type(8) button")
	private WebElement scrollToTop;

	@FindBy(id = "searchText")
	private WebElement search1;

	@FindBy(id = "searchText")
	private WebElement search2;

	@FindBy(css = "a.tmo-modal-link")
	private WebElement seeFullTerms;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	private WebElement shopPhones;

	@FindBy(css = "a[href='#divfootermain']")
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	private WebElement smartDevices;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	private WebElement stores;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/student-teacher-smartphone-tablet-discounts?icid=WMM_TM_CAMPUSEXCL_UHTZZ7GR1RT11028']")
	private WebElement studentteacherDiscount;

	@FindBy(css = ".marketing-page.ng-scope.no-scroll header.global-header.light nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.ng-scope.no-scroll header.global-header.light div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch2;

	@FindBy(css = "a[href='//support.t-mobile.com/']")
	private WebElement supportHome;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	private WebElement termsConditions;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true']")
	private WebElement termsOfUse;

	@FindBy(css = "a.decorated-link.buy-tel")
	private WebElement tmobile1800;

	@FindBy(css = "a[href='https://business.t-mobile.com']")
	private WebElement tmobileForBusiness;

	@FindBy(css = "a[href='http://www.t-mobile.com/cell-phone-trade-in.html']")
	private WebElement tradeInProgram;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "a[href='http://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_ReturnPolicy&print=true']")
	private WebElement viewReturnPolicy;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	private WebElement wellHelpYouJoin;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement whatIsAWearable1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement whatIsAWearable2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whatIsDigitsAndWhy1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whatIsDigitsAndWhy2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement whatsTheDifferenceBetweenA1;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement whatsTheDifferenceBetweenA2;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement whatsTheDifferenceBetweenA3;

	@FindBy(css = "#f69afcd45a8008071d65e61bc366a3bb3d2a8b7d div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement whatsTheDifferenceBetweenA4;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement whichSamsungSmartphonesHaveThe1;

	@FindBy(css = "#ff5b25aa91945c0051028bbfb411d58f2f091350 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement whichSamsungSmartphonesHaveThe2;

	public DigitsPage(WebDriver webDriver) {
		super(webDriver);
	}

	public DigitsPage(WebDriver driver, Map<String, String> data) {
		this(driver);
		this.data = data;
	}

	public DigitsPage(WebDriver driver, Map<String, String> data, int timeout) {
		this(driver, data);
		this.timeout = timeout;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Digits page loaded");
		} catch (Exception ex) {
			Assert.fail("Digits Page not loaded");
		}

		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

	/**
	 * Click on About Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickAboutLink() {
		about.click();
		return this;
	}

	/**
	 * Click on Accessibility Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickAccessibilityLink() {
		accessibility.click();
		return this;
	}

	/**
	 * Click on Accessories Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickAccessoriesLink() {
		accessories.click();
		return this;
	}

	/**
	 * Click on Activate Your Prepaid Phone Or Device Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickActivateYourPrepaidPhoneOrDeviceLink() {
		activateYourPrepaidPhoneOrDevice.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on 1 Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickButton1() {
		_1.click();
		return this;
	}

	/**
	 * Click on 2 Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickButton2() {
		_2.click();
		return this;
	}

	/**
	 * Click on 3 Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickButton3() {
		_3.click();
		return this;
	}

	/**
	 * Click on 4 Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickButton4() {
		_4.click();
		return this;
	}

	/**
	 * Click on 5 Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickButton5() {
		_5.click();
		return this;
	}

	/**
	 * Click on Call Us Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Can I Call Or Text 911 With Digits Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanICallOrText1Button() {
		canICallOrText1.click();
		return this;
	}

	/**
	 * Click on Can I Call Or Text 911 With Digits Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanICallOrText2Button() {
		canICallOrText2.click();
		return this;
	}

	/**
	 * Click on Can I Get A Business Family Discount On A Wearable Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIGetABusiness1Button() {
		canIGetABusiness1.click();
		return this;
	}

	/**
	 * Click on Can I Get A Business Family Discount On A Wearable Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIGetABusiness2Button() {
		canIGetABusiness2.click();
		return this;
	}

	/**
	 * Click on Can I Pair My Wearable With A Nontmobile Device Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIPairMyWearable1Button() {
		canIPairMyWearable1.click();
		return this;
	}

	/**
	 * Click on Can I Pair My Wearable With A Nontmobile Device Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIPairMyWearable2Button() {
		canIPairMyWearable2.click();
		return this;
	}

	/**
	 * Click on Can I Use My New Digits Number On A Nontmobile Device Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIUseMyNew1Button() {
		canIUseMyNew1.click();
		return this;
	}

	/**
	 * Click on Can I Use My New Digits Number On A Nontmobile Device Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIUseMyNew2Button() {
		canIUseMyNew2.click();
		return this;
	}

	/**
	 * Click on Can I Use Wearables While Roaming And Internationally Can I Make
	 * Calls Like I Do With My Phone Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIUseWearablesWhile1Button() {
		canIUseWearablesWhile1.click();
		return this;
	}

	/**
	 * Click on Can I Use Wearables While Roaming And Internationally Can I Make
	 * Calls Like I Do With My Phone Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCanIUseWearablesWhile2Button() {
		canIUseWearablesWhile2.click();
		return this;
	}

	/**
	 * Click on Careers Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCareersLink() {
		careers.click();
		return this;
	}

	/**
	 * Click on Check Order Status Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCheckOrderStatusLink() {
		checkOrderStatus.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Close Video Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCloseVideo1Button() {
		closeVideo1.click();
		return this;
	}

	/**
	 * Click on Close Video Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickCloseVideo2Button() {
		closeVideo2.click();
		return this;
	}

	/**
	 * Click on Consumer Information Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickConsumerInformationLink() {
		consumerInformation.click();
		return this;
	}

	/**
	 * Click on Contact Information Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickContactInformationLink() {
		contactInformation.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Deutsche Telekom Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDeutscheTelekomLink() {
		deutscheTelekom.click();
		return this;
	}

	/**
	 * Click on Device Support Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDeviceSupportLink() {
		deviceSupport.click();
		return this;
	}

	/**
	 * Click on Do I Need A New Phone To Use Digits Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDoINeedANew1Button() {
		doINeedANew1.click();
		return this;
	}

	/**
	 * Click on Do I Need A New Phone To Use Digits Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDoINeedANew2Button() {
		doINeedANew2.click();
		return this;
	}

	/**
	 * Click on Does Caller Id Work With Wearables Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDoesCallerIdWorkWith1Button() {
		doesCallerIdWorkWith1.click();
		return this;
	}

	/**
	 * Click on Does Caller Id Work With Wearables Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickDoesCallerIdWorkWith2Button() {
		doesCallerIdWorkWith2.click();
		return this;
	}

	/**
	 * Click on Espanol Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Espaol Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickEspaolEspaolLink() {
		espaolEspaol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickFindAStoreLink() {
		findAStore.click();
		return this;
	}

	/**
	 * Click on Get A Rebate Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickGetARebateLink() {
		getARebate.click();
		return this;
	}

	/**
	 * Click on How Do I Access And Manage My Digits Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIAccessAnd1Button() {
		howDoIAccessAnd1.click();
		return this;
	}

	/**
	 * Click on How Do I Access And Manage My Digits Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIAccessAnd2Button() {
		howDoIAccessAnd2.click();
		return this;
	}

	/**
	 * Click on How Do I Get Digits Set Up Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIGetDigits1Button() {
		howDoIGetDigits1.click();
		return this;
	}

	/**
	 * Click on How Do I Get Digits Set Up Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIGetDigits2Button() {
		howDoIGetDigits2.click();
		return this;
	}

	/**
	 * Click on How Do I Know If I Should Sign Up For Data With Paired Digits Or The
	 * Tmobile One Wearable Plan With A Separate Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIKnowIf1Button() {
		howDoIKnowIf1.click();
		return this;
	}

	/**
	 * Click on How Do I Know If I Should Sign Up For Data With Paired Digits Or The
	 * Tmobile One Wearable Plan With A Separate Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIKnowIf2Button() {
		howDoIKnowIf2.click();
		return this;
	}

	/**
	 * Click on How Do I Share My Phone Number With Other Devices Like My Desktop
	 * Laptop Or Tablet Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIShareMy1Button() {
		howDoIShareMy1.click();
		return this;
	}

	/**
	 * Click on How Do I Share My Phone Number With Other Devices Like My Desktop
	 * Laptop Or Tablet Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowDoIShareMy2Button() {
		howDoIShareMy2.click();
		return this;
	}

	/**
	 * Click on How Have You Made Sure Digits Is Secure Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowHaveYouMadeSure1Button() {
		howHaveYouMadeSure1.click();
		return this;
	}

	/**
	 * Click on How Have You Made Sure Digits Is Secure Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowHaveYouMadeSure2Button() {
		howHaveYouMadeSure2.click();
		return this;
	}

	/**
	 * Click on How Much Does Digits Cost What If I Need An Additional Number
	 * Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowMuchDoesDigitsCost1Button() {
		howMuchDoesDigitsCost1.click();
		return this;
	}

	/**
	 * Click on How Much Does Digits Cost What If I Need An Additional Number
	 * Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHowMuchDoesDigitsCost2Button() {
		howMuchDoesDigitsCost2.click();
		return this;
	}

	/**
	 * Click on Httpsmydigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpsmydigitsTmobileCom1Link() {
		httpsmydigitsTmobileCom1.click();
		return this;
	}

	/**
	 * Click on Httpsmydigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpsmydigitsTmobileCom2Link() {
		httpsmydigitsTmobileCom2.click();
		return this;
	}

	/**
	 * Click on Httpswebdigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpswebdigitsTmobileCom1Link() {
		httpswebdigitsTmobileCom1.click();
		return this;
	}

	/**
	 * Click on Httpswebdigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpswebdigitsTmobileCom2Link() {
		httpswebdigitsTmobileCom2.click();
		return this;
	}

	/**
	 * Click on Httpswebdigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpswebdigitsTmobileCom3Link() {
		httpswebdigitsTmobileCom3.click();
		return this;
	}

	/**
	 * Click on Httpswebdigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpswebdigitsTmobileCom4Link() {
		httpswebdigitsTmobileCom4.click();
		return this;
	}

	/**
	 * Click on Httpswebdigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpswebdigitsTmobileCom5Link() {
		httpswebdigitsTmobileCom5.click();
		return this;
	}

	/**
	 * Click on Httpswebdigits.tmobile.com Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpswebdigitsTmobileCom6Link() {
		httpswebdigitsTmobileCom6.click();
		return this;
	}

	/**
	 * Click on Httpwww.tmobile.comdigits Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpwwwTmobileComdigits1Link() {
		httpwwwTmobileComdigits1.click();
		return this;
	}

	/**
	 * Click on Httpwww.tmobile.comdigits Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpwwwTmobileComdigits2Link() {
		httpwwwTmobileComdigits2.click();
		return this;
	}

	/**
	 * Click on Httpwww.tmobile.comdigits Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpwwwTmobileComdigits3Link() {
		httpwwwTmobileComdigits3.click();
		return this;
	}

	/**
	 * Click on Httpwww.tmobile.comdigits Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickHttpwwwTmobileComdigits4Link() {
		httpwwwTmobileComdigits4.click();
		return this;
	}

	/**
	 * Click on I Want To Use My Main Phone Number But Switch Between Phones Such As
	 * An Iphone And Android Device Without Swapping My Sim. Can I Do This Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickIWantToUseMy1Button() {
		iWantToUseMy1.click();
		return this;
	}

	/**
	 * Click on I Want To Use My Main Phone Number But Switch Between Phones Such As
	 * An Iphone And Android Device Without Swapping My Sim. Can I Do This Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickIWantToUseMy2Button() {
		iWantToUseMy2.click();
		return this;
	}

	/**
	 * Click on If I Am An Existing Simple Choice Customer Can I Get A Data With
	 * Paired Digits Wearable Line Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickIfIAmAnExisting1Button() {
		ifIAmAnExisting1.click();
		return this;
	}

	/**
	 * Click on If I Am An Existing Simple Choice Customer Can I Get A Data With
	 * Paired Digits Wearable Line Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickIfIAmAnExisting2Button() {
		ifIAmAnExisting2.click();
		return this;
	}

	/**
	 * Click on If I Have The Digits App And Can Share My Number Across Many Devices
	 * Why Do I Need A Data With Paired Digits Plan For My Wearable Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickIfIHaveTheDigits1Button() {
		ifIHaveTheDigits1.click();
		return this;
	}

	/**
	 * Click on If I Have The Digits App And Can Share My Number Across Many Devices
	 * Why Do I Need A Data With Paired Digits Plan For My Wearable Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickIfIHaveTheDigits2Button() {
		ifIHaveTheDigits2.click();
		return this;
	}

	/**
	 * Click on Interestbased Ads Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickInterestbasedAdsLink() {
		interestbasedAds.click();
		return this;
	}

	/**
	 * Click on International Calling Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on International Rates Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickInternationalRatesLink() {
		internationalRates.click();
		return this;
	}

	/**
	 * Click on Internet Of Things Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickInternetOfThingsLink() {
		internetOfThings.click();
		return this;
	}

	/**
	 * Click on Investor Relations Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickInvestorRelationsLink() {
		investorRelations.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickLogInLink() {
		logIn.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Mobile Phone Number Already Works On All My Apple Devices Iphone
	 * Ipad Mac. How Is This Different Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickMyMobilePhoneNumberAlready1Button() {
		myMobilePhoneNumberAlready1.click();
		return this;
	}

	/**
	 * Click on My Mobile Phone Number Already Works On All My Apple Devices Iphone
	 * Ipad Mac. How Is This Different Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickMyMobilePhoneNumberAlready2Button() {
		myMobilePhoneNumberAlready2.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickMyTmobileLink() {
		myTmobile.click();
		return this;
	}

	/**
	 * Click on Next Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickNextButton() {
		next.click();
		return this;
	}

	/**
	 * Click on Open Internet Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickOpenInternetLink() {
		openInternet.click();
		return this;
	}

	/**
	 * Click on Phones Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click on Plans Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Plans Services Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPlansServicesLink() {
		plansServices.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Press Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPressLink() {
		press.click();
		return this;
	}

	/**
	 * Click on Previous Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPreviousButton() {
		previous.click();
		return this;
	}

	/**
	 * Click on Privacy Center Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPrivacyCenterLink() {
		privacyCenter.click();
		return this;
	}

	/**
	 * Click on Privacy Statement Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPrivacyStatementLink() {
		privacyStatement.click();
		return this;
	}

	/**
	 * Click on Public Safety911 Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPublicSafety911Link() {
		publicSafety911.click();
		return this;
	}

	/**
	 * Click on Puerto Rico Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickPuertoRicoLink() {
		puertoRico.click();
		return this;
	}

	/**
	 * Click on Questions About Your Bill Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickQuestionsAboutYourBillLink() {
		questionsAboutYourBill.click();
		return this;
	}

	/**
	 * Click on Refill Your Prepaid Account Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickRefillYourPrepaidAccountLink() {
		refillYourPrepaidAccount.click();
		return this;
	}

	/**
	 * Click on Scroll To Top Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickScrollToTopButton() {
		scrollToTop.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSeeFullTermsLink() {
		seeFullTerms.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickShopPhonesLink() {
		shopPhones.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Studentteacher Discount Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickStudentteacherDiscountLink() {
		studentteacherDiscount.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Support Home Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickSupportHomeLink() {
		supportHome.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Terms Of Use Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickTermsOfUseLink() {
		termsOfUse.click();
		return this;
	}

	/**
	 * Click on Tmobile For Business Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickTmobileForBusinessLink() {
		tmobileForBusiness.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickTmobileLink1800() {
		tmobile1800.click();
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickTradeInProgramLink() {
		tradeInProgram.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on View Return Policy Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickViewReturnPolicyLink() {
		viewReturnPolicy.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Click on What Is A Wearable Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatIsAWearable1Button() {
		whatIsAWearable1.click();
		return this;
	}

	/**
	 * Click on What Is A Wearable Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatIsAWearable2Button() {
		whatIsAWearable2.click();
		return this;
	}

	/**
	 * Click on What Is Digits And Why Should I Get Digits For This Device Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatIsDigitsAndWhy1Button() {
		whatIsDigitsAndWhy1.click();
		return this;
	}

	/**
	 * Click on What Is Digits And Why Should I Get Digits For This Device Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatIsDigitsAndWhy2Button() {
		whatIsDigitsAndWhy2.click();
		return this;
	}

	/**
	 * Click on Whats The Difference Between A Smartwatch And An Activity Tracker
	 * Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatsTheDifferenceBetweenA1Button() {
		whatsTheDifferenceBetweenA1.click();
		return this;
	}

	/**
	 * Click on Whats The Difference Between A Smartwatch And An Activity Tracker
	 * Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatsTheDifferenceBetweenA2Button() {
		whatsTheDifferenceBetweenA2.click();
		return this;
	}

	/**
	 * Click on Whats The Difference Between A Truly Connected Device And A
	 * Bluetooth Device How Do They Work Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatsTheDifferenceBetweenA3Button() {
		whatsTheDifferenceBetweenA3.click();
		return this;
	}

	/**
	 * Click on Whats The Difference Between A Smartwatch And An Activity Tracker
	 * Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhatsTheDifferenceBetweenA4Button() {
		whatsTheDifferenceBetweenA4.click();
		return this;
	}

	/**
	 * Click on Which Samsung Smartphones Have The Digits Functionality Built In And
	 * What Do I Do If I Dont Have One Of Those Devices Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhichSamsungSmartphonesHaveThe1Button() {
		whichSamsungSmartphonesHaveThe1.click();
		return this;
	}

	/**
	 * Click on Which Samsung Smartphones Have The Digits Functionality Built In And
	 * What Do I Do If I Dont Have One Of Those Devices Button.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage clickWhichSamsungSmartphonesHaveThe2Button() {
		whichSamsungSmartphonesHaveThe2.click();
		return this;
	}

	/**
	 * Set default value to Search Text field.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage setSearch1TextField() {
		return setSearch1TextField(data.get("SEARCH_1"));
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set default value to Search Text field.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage setSearch2TextField() {
		return setSearch2TextField(data.get("SEARCH_2"));
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the DigitsPage class instance.
	 */
	public DigitsPage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}

}
