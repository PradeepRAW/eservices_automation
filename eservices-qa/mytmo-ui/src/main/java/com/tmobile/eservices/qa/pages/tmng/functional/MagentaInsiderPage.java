package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author sputti
 *
 */
public class MagentaInsiderPage extends TmngCommonPage {
	
	private final String pageUrl = "https://qat-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/brand/insider-register.html";
	
	// Locators for MI Register Elements
	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement FirstName;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-firstName-error']")
	private WebElement FirstNameError;
	
	@FindBy(xpath = "//input[@name='lastName']")
	private WebElement LastName;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-lastName-error']")
	private WebElement LastNameError;
	
	@FindBy(xpath = "//input[@name='address1']")
	private WebElement Address1;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-address1-error']")
	private WebElement Address1Error;
	
	@FindBy(xpath = "//input[@name='address2']")
	private WebElement Address2;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-address2-error']")
	private WebElement Address2Error;
	
	@FindBy(xpath = "//input[@name='city']")
	private WebElement City;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-city-error']")
	private WebElement CityError;
		
	@FindBy(xpath = "//select[@name='state']")
	private WebElement State;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-state-error']")
	private WebElement StateError;
	
	@FindBy(xpath = "//input[@name='zip']")
	private WebElement Zip;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-zip-error']")
	private WebElement ZipError;
	
	@FindBy(xpath = "//input[@name='phone']")
	private WebElement Phone;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-phone-error']")
	private WebElement PhoneError;
	
	@FindBy(xpath = "//input[@name='email']")
	private WebElement Email;
	
	@FindBy(xpath = "//p[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-email-error']")
	private WebElement EmailError;
	
	@FindBy(xpath = "//select[@id='5d62837025bb854dada28d2f2ac494bfba7e3fe5-socialChannel']")
	private WebElement SocialChannel;
	
	@FindBy(xpath = "//input[@name='socialHandle']")
	private WebElement SocialHandle;
	
	@FindBy(xpath = "//div[contains(@class,'indicator m-t-10')]")
	private WebElement IAgreeCheck;
	
	@FindBy(xpath = "//button[@class='btn btn-primary .btn-small']")
	private WebElement OKButton;
	
	@FindBy(xpath = "//button[contains(text(),'Submit')]")
	private WebElement Submit;
	
	@FindBy(xpath = "//div[contains(@class,'success-state pre-reg-heading-section')]")
	private WebElement Form2;
		
	/**
	 * @param webDriver
	 */
	public MagentaInsiderPage(WebDriver webDriver) {
		super(webDriver);
	}
	
//   ------------------------------------------------------- MI Register Page Load Verification --------------------------------
	/**
	 * Verify that the page loaded completely.
	 */
	public MagentaInsiderPage verifyMIRegisterPageLoaded() {
		checkPageIsReady();
		waitForSpinnerInvisibility();
		try {
			if (getDriver().getCurrentUrl().contains(pageUrl)) {
				verifyMIRegisterPageUrl();
				Reporter.log("MI Register page loaded");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("MI Registerpage not loaded");
		}
		return this;
	}
    /**
	 * Verify that current page URL matches the expected URL.
	 */
	public MagentaInsiderPage verifyMIRegisterPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		Reporter.log("MI register page URL verified");
		return this;
	}
	/**
	 * First Name set and validations
	 */
	public MagentaInsiderPage setFirstName() {
		waitForSpinnerInvisibility();
		try {
			if (FirstName.isDisplayed()) {
				FirstName.click();
				FirstName.sendKeys(Keys.TAB);
				FirstName.click();
				if (FirstNameError.isDisplayed()) {
					Reporter.log("'Please enter your first name' message displayed for First Name Empty Value");
					sendTextData(FirstName, Constants.MI_REGISTER_FIRSTNAME);
					Reporter.log("First Name Field Value is set");
				}
				else
					Assert.fail("'Please enter your first name' message is not displayed for First Name Empty Value");	
			}} catch (Exception e) {
			Assert.fail("First Name Field is not displayed" + e.getMessage());
		}
		return this;
	}
	/**
	 * Last Name Empty Field Validations
	 */
	public MagentaInsiderPage validateLastName1() {
		try {
			LastName.click();
			LastName.sendKeys(Keys.TAB);
			LastName.click();
			if (LastNameError.isDisplayed()) {
				Reporter.log("'Please enter your Last Name' message displayed for Last Name Empty Value");
			}
			else
				Assert.fail("'Please enter your Last Name' message is not displayed for Last Name Empty Value");	
		} catch (Exception e) {
			Assert.fail("'Please enter your Last Name' message is not displayed for Last Name Empty Value"+e.getMessage());
		}
		return this;
	}
	/**
	 * 	Validation minimum of 2 consecutive characters for Last name
	 */
	public MagentaInsiderPage validateLastName2() {
		try {
			if (LastName.isDisplayed()) {
				sendTextData(LastName, Constants.LAZERSHARK_FORM2_INVALID_LASTNAME);
				LastName.sendKeys(Keys.TAB);
				LastName.click();
				if (LastNameError.isDisplayed()) {
					Reporter.log("'Please enter your Last Name' message displayed for invalid Last Name");
				}
				else
					Assert.fail("'Please enter your Last Name' message not displayed for invalid Last Name");	
			} }	catch (Exception e) {
			Assert.fail("'Please enter your Last Name' message not displayed for invalid Last Name"+e.getMessage());
		}
		return this;
	}
	/**
	 * Set Last Name
	 */
	public MagentaInsiderPage setLastName() {
		waitForSpinnerInvisibility();
		try {
			LastName.isDisplayed();
			sendTextData(LastName, Constants.MI_REGISTER_LASTNAME);
			LastName.sendKeys(Keys.TAB);
			Reporter.log("Last Name Field is displayed and entered the value.");
		} catch (Exception e) {
			Assert.fail("Last Name Field is not displayed" + e.getMessage());
		}
		return this;
	}
	/**
	 * Confirm Address1 Mandatory Field Validations
	 */
	public MagentaInsiderPage verifyAddress() {
		try {
			if (Address1.isDisplayed()&&Address2.isDisplayed()) {
				Address1.click();
				Address1.sendKeys(Keys.TAB);
				Address1.click();
				if (Address1Error.isDisplayed()) {
					Reporter.log("'Please enter your address' message displayed for Empty Address");
					Integer num = getRandomNumberInRange(1000,10000);
					sendTextData(Address1, Constants.MI_REGISTER_ADDRESS1);
					sendTextData(Address2, Constants.MI_REGISTER_ADDRESS2+num);
					Reporter.log("Address Field Values set");
				}
				else
					Assert.fail("'Please enter your address' message is not displayed for Empty Address");	
			}} catch (Exception e) {
			Assert.fail("'Please enter your address' message is not displayed for Empty Address"+e.getMessage());
		}
		return this;
	}
	/**
	 * Confirm City Mandatory Field Validations
	 *
	 */
	public MagentaInsiderPage setCity() {
		try {
			if (City.isDisplayed()){
				City.click();
				City.sendKeys(Keys.TAB);
				City.click();
				if (CityError.isDisplayed()) {
					Reporter.log("'Please enter your city' message displayed for Empty City");
					sendTextData(City, Constants.MI_REGISTER_CITY);
					Reporter.log("City Field Value is set");
				}
				else
					Assert.fail("'Please enter your city' message displayed for Empty City");	
			}}catch (Exception e) {
			Assert.fail("'Please enter your city' message is not displayed for Empty City"+e.getMessage());
		}
		return this;
	}
	/**
	 * Confirm State Mandatory Field Validations
	 */
	public MagentaInsiderPage stateValidation() {
		try {
			State.click();
			Zip.click();
			State.click();
			if (StateError.isDisplayed()) {
				Reporter.log("'Please enter your state' message displayed clicks in and out of the field without making a State selection");
			}
			else
				Assert.fail("'Please enter your state' message not displayed clicks in and out of the field without making a State selection");	
		} catch (Exception e) {
			Assert.fail("'Please enter your state' message is not displayed clicks in and out of the field without making a State selection"+e.getMessage());
		}
		return this;
	}
	/**
	 * Confirm State Mandatory Field Validations
	 */
	public MagentaInsiderPage setState() {
		try {
			State.click();
			State.sendKeys(Keys.TAB);
			State.click();
			if (StateError.isDisplayed()) {
				Reporter.log("'Please enter your state' message displayed for Empty State");
				selectElementFromDropDown(State, "Text", "WA");
				Reporter.log("State Field Value is set");
			}
			else
				Assert.fail("'Please enter your state' message displayed for Empty State");	
		} catch (Exception e) {
			Assert.fail("'Please enter your state' message is not displayed for Empty State"+e.getMessage());
		}
		return this;
	}
	/**
	 * Zip Accepts 5 digits
	 */
	public MagentaInsiderPage setZip() {
		try {
			sendTextData(Zip, Constants.LAZERSHARK_FORM3_INVALID_ZIP);
			Zip.sendKeys(Keys.TAB);
			Zip.click();
			if (Zip.getAttribute("value").length() < 5) {
				if (ZipError.isDisplayed()) {
				Reporter.log("'Please enter correct zip' message displayed if zip < 5 digits");
				sendTextData(Zip, Constants.MI_REGISTER_ZIP);
				Reporter.log("Zip Field Value is set");
			}
			else
				Assert.fail("'Please enter correct zip' message not displayed if zip < 5 digits");
		} } catch (Exception e) {
			Assert.fail("'Please enter correct zip' message not displayed if zip < 5 digits");
		}
		return this;
	}
	/**
	 * Phone Number Mandatory Field Validations
	 */
	public MagentaInsiderPage setPhoneNumber() {
		try {
			Phone.click();
			Phone.sendKeys(Keys.TAB);
			Phone.click();
			if (PhoneError.isDisplayed()) {
				Reporter.log("'Please enter your phone' message displayed for Empty Phone Number");
				sendTextData(Phone, Constants.MI_REGISTER_PHNO);
				Reporter.log("Phone Field Value is set");
			}
			else
				Assert.fail("'Please enter your phone' message displayed for Empty Phone Number");	
		} catch (Exception e) {
			Assert.fail("'Please enter your phone' message is not displayed for Empty Phone Number"+e.getMessage());
		}
		return this;
	}
	/**
	 * Submit Button is disabled on page load
	 */
	public MagentaInsiderPage submitButtonDisable() {
		try {
			if (!Submit.isEnabled()) {
				Reporter.log("Submit Button is Disabled on Page Load");
		}} catch (Exception e) {
			Assert.fail("Submit Button is not Disabled on Page Load" + e.getMessage());
		}
		return this;
	}
	/**
	 * Email Mandatory Field Validations
	 */
	public MagentaInsiderPage setEmail() {
		try {
			waitForSpinnerInvisibility();
			Email.click();
			Email.sendKeys(Keys.TAB);
			Email.click();
			waitFor(ExpectedConditions.visibilityOf(EmailError),3);
			if (EmailError.isDisplayed()) {
				Reporter.log("'Please enter your Email' message displayed for Empty Email");
				sendTextData(Email, Constants.MI_REGISTER_EMAIL);
				Reporter.log("Email Field Value is set");
			}
			else
				Assert.fail("'Please enter your Email' message displayed for Empty Email");	
		} catch (Exception e) {
			Assert.fail("'Please enter your Email' message is not displayed for Empty Email"+e.getMessage());
		}
		return this;
	}
	/**
	 * Set SocialChannel
	 */
	public MagentaInsiderPage setSocialChannel() {
		try {
			if (SocialChannel.isEnabled()) {
				selectElementFromDropDown(SocialChannel, "Text", "Twitter");
				Reporter.log("SocialChannel Field is displayed and select the value.");
				Assert.assertTrue(SocialHandle.isDisplayed());
				Reporter.log("SocialHandle Field dynamically appears by selecting of SocialChannel Value");
				sendTextData(SocialHandle, Constants.MI_REGISTER_SOCIALHANDLE);
				Reporter.log("SocialHandle Field Value is set");
			}
			else
			{
				Assert.fail("SocialHandle Field not dynamically appears by selecting of SocialChannel Value");	
			}
		} catch (Exception e) {
			Assert.fail("SocialChannel Field is not selected" + e.toString());
		}
		return this;
	}
	/**
	 * Select I Agree Check box
	 */
	public MagentaInsiderPage setAgree() {
		try {
			if (!IAgreeCheck.isSelected()) {
				Reporter.log("I Agree Check box is unchecked as default");
			IAgreeCheck.click();
			Reporter.log("I Agree Check box is checked.");
		}} catch (Exception e) {
			Assert.fail("Unable to Check the check box" + e.getMessage());
		}
		return this;
	}
	/**
	 * Click on Submit Button
	 */
	public MagentaInsiderPage clickOnSubmitBtn() {
		checkPageIsReady();
		try {
			moveToElement(Submit);
			clickElementWithJavaScript(Submit);
			Reporter.log("Clicked on Submit button.");
		} catch (Exception e) {
			Assert.fail("Failed to click on Submit button.");
		}
		return this;
	}
	/**
	 * Click OK
	 */
	public MagentaInsiderPage ClickOK() {
		try {
			waitFor(ExpectedConditions.visibilityOf(OKButton),2);
			OKButton.click();
			Reporter.log("OK Button is Clicked");
			waitFor(ExpectedConditions.visibilityOf(Form2),2);
			Reporter.log("Success Page is loaded by clicking on Submit Button");
			Form2.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Unable to Check the check box" + e.getMessage());
		}
		return this;
	}
	/**
	 * Verify Fields Validations on Blur in MI Register Page
	 */
	public MagentaInsiderPage VerifyFieldBlur() {
		waitForSpinnerInvisibility();
		try {
				FirstName.click();
				LastName.click();
				Assert.assertTrue(FirstNameError.isDisplayed(), "First Name Error message is not disabled");
				Reporter.log("First Name Error message displays when user blur on field.");
				Address1.click();
				Assert.assertTrue(LastNameError.isDisplayed(), "Last Name Error message is not disabled");
				Reporter.log("Last Name Error message displays when user blur on field.");
				City.click();
				Assert.assertTrue(Address1Error.isDisplayed(), "Address Error message is not disabled");
				Reporter.log("Address Error message displays when user blur on field.");
				State.click();
				Assert.assertTrue(CityError.isDisplayed(), "City Error message is not disabled");
				Reporter.log("City Error message displays when user blur on field.");
				Zip.click();
				Assert.assertTrue(StateError.isDisplayed(), "State Error message is not disabled");
				Reporter.log("State Error message displays when user blur on field.");
				Phone.click();
				Assert.assertTrue(ZipError.isDisplayed(), "Zip Error message is not disabled");
				Reporter.log("Zip Error message displays when user blur on field.");
				Email.click();
				Assert.assertTrue(PhoneError.isDisplayed(), "Phone Error message is not disabled");
				Reporter.log("Phone Error message displays when user blur on field.");
				IAgreeCheck.click();
				Assert.assertTrue(EmailError.isDisplayed(), "Email Error message is not disabled");
				Reporter.log("Email Error message displays when user blur on field.");
			} catch (Exception e) {
			Assert.fail("Field Error message is not displayed when user blur on fields" + e.getMessage());
		}
		return this;
	}
	/**
	 * Set MagentaInsider Field Values
	 */
	public MagentaInsiderPage setFields() {
		waitForSpinnerInvisibility();
		try {
			sendTextData(FirstName, Constants.MI_REGISTER_FIRSTNAME);
			sendTextData(LastName, Constants.MI_REGISTER_LASTNAME);
			sendTextData(Address1, Constants.MI_REGISTER_ADDRESS1);
			Integer num1 = getRandomNumberInRange(1000,10000);
			sendTextData(Address2, Constants.MI_REGISTER_ADDRESS2+num1);
			sendTextData(City, Constants.MI_REGISTER_CITY);
			selectElementFromDropDown(State, "Text", "WA");
			sendTextData(Zip, Constants.MI_REGISTER_ZIP);
			sendTextData(Phone, Constants.MI_REGISTER_PHNO);
			sendTextData(Email, Constants.MI_REGISTER_EMAIL);
			moveToElement(Submit);
			clickElementWithJavaScript(Submit);
			waitFor(ExpectedConditions.visibilityOf(OKButton),2);
			OKButton.click();
			Reporter.log("Submit MI Register Form Successfully");
			waitFor(ExpectedConditions.visibilityOf(Form2),2);
			} catch (Exception e) {
			Assert.fail("Unable to set Field Values" + e.getMessage());
		}
		return this;
	}
}