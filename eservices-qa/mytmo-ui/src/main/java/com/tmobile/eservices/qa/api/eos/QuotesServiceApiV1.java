package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class QuotesServiceApiV1 extends ApiCommonLib {

	/***
	 * Summary: Retreive the Service details (SOC details) for sale and conflicts
	 * Headers:interactionId ,
	 * 	-workflowId ,
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getServicesForSale(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/servicequotes", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}


	private Map<String, String> buildGetCatalogHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		//headers.put("Authorization", dcpToken);
		headers.put("Content-Type", "application/json");
		headers.put("interactionId", "interactionId");
		headers.put("workflowId", "workflowId");
		return headers;
	}

}


