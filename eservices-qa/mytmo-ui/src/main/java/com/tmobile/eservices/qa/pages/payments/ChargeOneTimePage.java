package com.tmobile.eservices.qa.pages.payments;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class ChargeOneTimePage extends CommonPage {

	private final String pageUrl = "charges/category/charge/OneTime";

	@FindBy(xpath = "//span[@class='bb-back']/img")
	private WebElement backToSummaryLink;

	@FindBy(css = "img[alt='back']")
	private WebElement mobileBackToSummaryLink;

	@FindBy(css = "i.fa.fa-angle-left")
	private WebElement arrowLeft;

	@FindBy(css = "i.fa.fa-angle-right")
	private WebElement arrowRight;

	@FindBy(css = "bb-slider div.bb-slider-item-amount")
	private List<WebElement> amountBlock;

	@FindBy(css = "img.loader-icon")
	private WebElement pageSpinner;

	@FindBy(css = "span.bb-back")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//bb-slider//div[contains(text(),'One-time charges')]")
	private WebElement oneTimeChargesTabInSlider;

	@FindBy(xpath = "//div[contains(@class,'charge-detail-label')]")
	private WebElement labeltotal;

	@FindBy(xpath = "//div[contains(@class,'charge-detail-amount')]")
	private WebElement amounttotal;

	@FindBy(xpath = "//div[@class='bb-slider']//div[contains(text(),'One-time charges')]/following-sibling::div[@class='bb-slider-item-amount']")
	private WebElement Plansslideramount;

	@FindBy(css = "bb-slider div.bb-slider-item-title")
	private List<WebElement> slideritemstitle;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public ChargeOneTimePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ChargeOneTimePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @throws InterruptedException
	 */
	public ChargeOneTimePage verifyPageLoaded() {
		try {
			checkPageIsReady();
			// waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Fail to load Charge One Time Payment page");
		}
		return this;
	}

	/**
	 * verify that the one time charge is displayed on the slider
	 * 
	 * @return
	 */
	public ChargeOneTimePage verifyOneTimeChargeInSlider() {
		try {
			oneTimeChargesTabInSlider.isDisplayed();
			Reporter.log("One Time Charge slider is displayed");
		} catch (Exception e) {
			Assert.fail("One Time Charge slider is not present");
		}
		return this;
	}

	/**
	 * Verify page elements
	 *
	 * @throws InterruptedException
	 */
	public ChargeOneTimePage verifyChargeOneTimePageElements() {
		try {
			Assert.assertTrue(backToSummaryLink.isDisplayed(), "Back To Summary link is not displayed");
			Assert.assertTrue(oneTimeChargesTabInSlider.isDisplayed(), "Equipment header is not displayed");
			Assert.assertTrue(arrowLeft.isDisplayed(), "Arrow left is not displayed");
			Assert.assertTrue(arrowRight.isDisplayed(), "Arrow right is not displayed");
			for (WebElement amount : amountBlock) {
				Assert.assertTrue(amount.getText().contains("$"), "Dollar sign is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Fail to verify Charge One Time page elements");
		}
		return this;
	}

	public ChargeOneTimePage checklabelTotal(String text) {
		try {
			if (labeltotal.getText().trim().equalsIgnoreCase(text))
				Reporter.log(text + " label is matching with Actual");
			else
				Verify.fail(text + " label is not matching with Actual");
		} catch (Exception e) {
			Verify.fail("Fail to identify total lable");
		}
		return this;
	}

	public ChargeOneTimePage checktotalAmount() {
		try {
			String amount = amounttotal.getText().trim();
			if (amount.contains("$")
					&& amount.substring(amount.length() - 3, amount.length() - 2).equalsIgnoreCase("."))
				Reporter.log("Total amount format is matched");
			else
				Assert.fail("Total amount format is not matched");
		} catch (Exception e) {
			Assert.fail("Fail identify Total amount");
		}
		return this;
	}

	public ChargeOneTimePage checktotalAmountwithslideramount() {
		try {
			String uiamount = amounttotal.getText().trim().replace(".", "");
			String slideramount = Plansslideramount.getText().replace(".", "");
			if (Plansslideramount.findElements(By.xpath(".//span[@class='integer integer--isNegative']")).size() > 0) {
				slideramount = "-" + slideramount;
			}
			if (uiamount.equalsIgnoreCase(slideramount))
				Reporter.log("Total amount in slider is same as ui frame");
			else
				Assert.fail("Total amount in slider is not matching with value in ui frame");
		} catch (Exception e) {
			Assert.fail("Fail identify amount");
		}
		return this;
	}

	public List<String> getslidertitles() {

		List<String> titles = new ArrayList<String>();
		try {
			for (WebElement title : slideritemstitle) {

				titles.add(title.getText().trim());
			}

		} catch (Exception e) {
			Assert.fail("slideritemstitle element is not present");
		}

		return titles;

	}

	public ChargeOneTimePage checkleftslider() {
		try {
			List<String> oldlist = getslidertitles();
			arrowLeft.click();
			Thread.sleep(1000);
			List<String> newList = getslidertitles();
			for (int i = 0; i < oldlist.size() - 1; i++) {
				if (oldlist.get(i).equalsIgnoreCase(newList.get(i + 1)))
					Reporter.log("Left slider is working");
				else
					Verify.fail("Left slider is not working");

			}
		} catch (Exception e) {
			Verify.fail("Fail to identify voice lines lable");
		}
		return this;
	}

	public ChargeOneTimePage checkrightslider() {
		try {
			List<String> oldlist = getslidertitles();
			arrowRight.click();
			Thread.sleep(1000);
			List<String> newList = getslidertitles();
			for (int i = 1; i < oldlist.size(); i++) {
				if (oldlist.get(i).equalsIgnoreCase(newList.get(i - 1)))
					Reporter.log("Left slider is working");
				else
					Verify.fail("Left slider is not working");

			}
		} catch (Exception e) {
			Verify.fail("Fail to identify voice lines lable");
		}
		return this;
	}

	/**
	 * Click Back to Summary page link
	 *
	 */
	public ChargeOneTimePage clickBackToSummaryLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileBackToSummaryLink.click();
			} else {
				backToSummaryLink.click();
			}
			Reporter.log("Clicked on Back To Summary Link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on Back To Summary Link.");
		}
		return this;
	}
}
