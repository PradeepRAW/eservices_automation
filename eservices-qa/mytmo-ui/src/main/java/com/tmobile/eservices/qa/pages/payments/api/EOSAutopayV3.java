package com.tmobile.eservices.qa.pages.payments.api;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSAutopayV3 extends  EOSCommonLib {

	
public Response getResponseAutopaySearch(String alist[]) throws Exception {
	Response response=null;   
	//String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
	    if(alist!=null) {
	    	
	   // RestService restService = new RestService("", "https://stage2.eos.corporate.t-mobile.com");
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("application_Id", "autopay");
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("application_userid", "MYTMO");
	    restService.addHeader("activityId", "123");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("channel_id", "WEB");
	  
	    response = restService.callService("payments/v3/autopay/"+alist[3],RestCallType.GET);
	    
	}

	    return response;
}





public String AutopayPaymentmethod(Response response) {
	String autopayon=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("paymentInstrumentList[0].creditCard")!=null)return "card";
		if(jsonPathEvaluator.get("paymentInstrumentList[0].bankAccount")!=null)return "bank";
		else return "false";
	}
		
	
	return autopayon;
}

public Map<String,String> getautopayinformation(Response response){
	Map<String,String> autopayinfo=new HashMap<String,String>();
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	if(jsonPathEvaluator.get("paymentInstrumentList[0].creditCard")!=null) {
		autopayinfo.put("cardAlias",jsonPathEvaluator.get("paymentInstrumentList[0].creditCard.cardAlias"));
		autopayinfo.put("cardHolderName",jsonPathEvaluator.get("paymentInstrumentList[0].creditCard.cardHolderName"));
		autopayinfo.put("expirationMonthYear",jsonPathEvaluator.get("paymentInstrumentList[0].creditCard.expirationMonthYear"));
		autopayinfo.put("type",jsonPathEvaluator.get("paymentInstrumentList[0].creditCard.type"));
		return autopayinfo;
	}
	if(jsonPathEvaluator.get("paymentInstrumentList[0].bankAccount")!=null) {
		autopayinfo.put("bankAccountAlias",jsonPathEvaluator.get("paymentInstrumentList[0].bankAccount.bankAccountAlias"));
	//	autopayinfo.put("accountHolderNames",jsonPathEvaluator.get("paymentInstrumentList[0].bankAccount.accountHolderNames"));
		autopayinfo.put("routingNumber",jsonPathEvaluator.get("paymentInstrumentList[0].bankAccount.routingNumber"));
		autopayinfo.put("paymentMethodCode",jsonPathEvaluator.get("paymentInstrumentList[0].bankAccount.paymentMethodCode"));
		return autopayinfo;
	}
	
	}
	return autopayinfo;
}





}




	


