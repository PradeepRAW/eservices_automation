package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class BenefitsCLApi extends ApiCommonLib{
	
	/***
	 * T-Mobile Benefits API’s.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/Accept'
     *   - $ref: '#/parameters/interactionid'
	 * @return
	 * @throws Exception 
	 */
    public Response benefitsSearch(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildBenefitsAgreementAPIHeader(apiTestData);
		String resourceURL = "cx/v1/benefits";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    }
    public Response benefitsSubscriptionSearch(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildBenefitsAgreementAPIHeader(apiTestData);
		String resourceURL = "cx/v1/benefits/"+getToken("benefitId")+"/subscriptions";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    }
    public Response benefitsEligibility(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildBenefitsAgreementAPIHeader(apiTestData);
		String resourceURL = "cx/v1/benefits/eligibility";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    }
        
    private Map<String, String> buildBenefitsAgreementAPIHeader(ApiTestData apiTestData) throws Exception {
    	final String accessToken = getAccessToken();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("Accept", "application/json");
		headers.put("interactionid", "3253200388_435");
		headers.put("Authorization", accessToken);
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}

}
