package com.tmobile.eservices.qa.pages.payments;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class EIPPaymentForwardPage extends CommonPage {

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	@FindBy(id = "payInFullCheckBox")
	private WebElement payEIPFullCheckBox;

	@FindAll({ @FindBy(id = "chkacc-routingnum"), @FindBy(id = "routing-number") })
	private WebElement routingNumber;

	@FindAll({ @FindBy(id = "chkacc-number"), @FindBy(id = "account-number") })
	private WebElement accountNumber;

	@FindAll({ @FindBy(css = "input#card-name"), @FindBy(id = "card_name") })
	private WebElement nameOnCard;

	@FindAll({ @FindBy(id = "card-number"), @FindBy(id = "card_number") })
	private WebElement cardNo;

	@FindAll({ @FindBy(id = "expire-year"), @FindBy(id = "expire_year") })
	private WebElement expirationYear;

	@FindAll({ @FindBy(id = "expire-month"), @FindBy(id = "expire_month") })
	private WebElement expirationMonth;

	@FindAll({ @FindBy(id = "card-secu-code"), @FindBy(id = "card_sec_code") })
	private WebElement cvv;

	@FindAll({ @FindBy(id = "card-zipcode"), @FindBy(id = "card_zipcode") })
	private WebElement zipCode;

	@FindAll({ @FindBy(id = "chkacc-name"), @FindBy(id = "account-name") })
	private WebElement newCheckingAccountName;

	@FindAll({ @FindBy(css = "span[ng-show='!$ctrl.paymentMethodBladeHelperService.isCreditCardNumberValid']"),
			@FindBy(id = "chk_card_no_error"), @FindBy(id = "vcard") })
	private WebElement ccError;

	@FindBy(id = "cc-next")
	private WebElement nxtBtn;
	
	@FindBy(id = "cc-cancel")
	private WebElement cancelBtn;

	@FindBys(@FindBy(id = "continueOverPayment_id"))
	private List<WebElement> continueOverPaymentYes;

	@FindAll({ @FindBy(css = "p#new-credit_p >input"), @FindBy(id = "new-credit") })
	private WebElement newCardRadioBtn;

	@FindBy(id = "mybill_dollar")
	private WebElement paymentAmounttxtBox;

	@FindBy(id = "saved-credit")
	private WebElement savedCreditcard;

	@FindBy(css = ".ajax_loader")
	private List<WebElement> pageSpinners;

	@FindAll({@FindBy(css = "div.ui_mobile_headline"),@FindBy(css = "#cc-cancel"),@FindBy(id="di_title")})
	private WebElement pageLoadElement;

	private final String pageUrl = "/eippayment.forward.html";

	@FindBy(linkText="Change")
	private WebElement changeLink;

	@FindBy(id="lineIdentifier")
	private WebElement jumpFlag;
	
	@FindBy(id="shopRedirectUrl")
	private WebElement shopURL;
	
	@FindBy(id="di_firstName")
	private WebElement userFirstName;

	@FindBy(id="di_msisdn")
	private WebElement userMsisdn;

	@FindBy(css="span.sprite+span")
	private WebElement userSavedCard;
	

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public EIPPaymentForwardPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public EIPPaymentForwardPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
	public EIPPaymentForwardPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			//waitFor(ExpectedConditions.invisibilityOfAllElements(pageSpinners));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			Reporter.log("EIP payment page displayed");
		} catch (Exception e) {
			Assert.fail("EIP payment page not found");
		}
		return this;
	}

	/**
	 * Verify EIP Device Payment Page
	 * 
	 * @return
	 */

//	public EIPPaymentForwardPage verifyEIPDevicePaymentPagedata() {
//		try {
//			waitforSpinner();
//			checkPageIsReady();
//			getDriver().getCurrentUrl().contains("eippayment");
//			Reporter.log("EIP payment page displayed");
//		} catch (Exception e) {
//			Assert.fail("EIP payment page not found");
//		}
//		return this;
//	}

	/**
	 * Click EIP Full Check Box
	 */
	public EIPPaymentForwardPage clickPayEIPFullCheckBox() {
		try {
			waitFor(ExpectedConditions.visibilityOf(payEIPFullCheckBox));
			payEIPFullCheckBox.click();
			Reporter.log("Clicked on Pay full checkbox");
		} catch (Exception e) {
			Assert.fail("Pay full checkbox not found");
		}
		return this;
	}

	/**
	 * Fill Payment Info EIP - OLD OTP
	 * 
	 * @param paymentType
	 * @param payment
	 */
	public EIPPaymentForwardPage fillPaymentInfo(Payment payment) {
		try {
				sendTextData(nameOnCard, payment.getNameOnCard());
				setCardNumber(payment.getCardNumber());
				selectExpiryMonth(payment.getExpiryMonth());
				selectExpiryYear(payment.getExpiryYear());
				setCVV(payment.getCvv());
				setZipcode(payment.getZipCode());
				waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Payment info filled");
		} catch (Exception e) {
			Assert.fail("Payment info fields not found");
		}
		return this;
	}

	/**
	 * set credit card number
	 * 
	 * @param data
	 */
	public EIPPaymentForwardPage setCardNumber(String data) {
		try {
			checkPageIsReady();
			sendTextData(cardNo, data);
			cardNo.sendKeys(Keys.TAB);
			checkPageIsReady();
			cvv.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Entered card number");
		} catch (Exception e) {
			Assert.fail("Card number field not found");
		}
		return this;
	}

	/**
	 * select expiration month
	 * 
	 * @param expiryMonth
	 */
	public EIPPaymentForwardPage selectExpiryMonth(String expiryMonth) {
		try {
			if (!ccError.isDisplayed()) {
				getDriver().manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
				waitFor(ExpectedConditions.visibilityOf(expirationMonth));
				Select select = new Select(expirationMonth);
				List<WebElement> months = select.getOptions();
				for (WebElement webElement : months) {
					if (expiryMonth.contains(webElement.getText())) {
						select.selectByVisibleText(webElement.getText());
					}
				}
				Reporter.log("Selected Expiry Month");
			} else
				Assert.fail("Expiry Month not found");
		} catch (Exception e) {
			Assert.fail("Expiry Month not found");
		}
		return this;
	}

	/**
	 * select expiration year
	 * 
	 * @param expiryYear
	 */
	public EIPPaymentForwardPage selectExpiryYear(String expiryYear) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("chk_exp_date_error")));
			waitFor(ExpectedConditions.visibilityOf(expirationYear));
			selectElementFromDropDown(expirationYear, "Text", expiryYear);
			Reporter.log("Selected Expiry year");
		} catch (Exception e) {
			Assert.fail("Expiry not found");
		}
		return this;
	}

	/**
	 * set CVV code
	 * 
	 * @param cvvNumber
	 */
	public EIPPaymentForwardPage setCVV(String cvvNumber) {
		try {
			sendTextData(cvv, cvvNumber);
			cvv.sendKeys(Keys.TAB);
			Reporter.log("CVV entered");
		} catch (Exception e) {
			Assert.fail("CVV field not found");
		}
		return this;
	}

	/**
	 * set Zip code
	 * 
	 * @param zipcode
	 */
	public EIPPaymentForwardPage setZipcode(String zipcode) {
		try {
			waitFor(ExpectedConditions.visibilityOf(zipCode));
			zipCode.sendKeys(zipcode);
			Reporter.log("Entered zip code");
		} catch (Exception e) {
			Assert.fail("Zip code field not found");
		}
		return this;
	}

	/**
	 * Set New Checking Account Name
	 * 
	 * @param name
	 */
	public EIPPaymentForwardPage setNewCheckingAccountName(String name) {
		try {
			newCheckingAccountName.click();
			sendTextData(newCheckingAccountName, name);
			Reporter.log("Entered new checking account name");
		} catch (Exception e) {
			Assert.fail("New checking account name not found");
		}
		return this;
	}

	/**
	 * Set Routing Number
	 * 
	 * @param routingNo
	 */
	public EIPPaymentForwardPage setRoutingNumber(String routingNo) {
		try {
			Thread.sleep(2000);
			for (int sleepCtr = 0; sleepCtr < 10; sleepCtr++) {
				Thread.sleep(1000);
				waitFor(ExpectedConditions.visibilityOf(routingNumber));
				if (routingNumber.isEnabled()) {
					routingNumber.sendKeys(routingNo);
					break;
				}
			}
		} catch (StaleElementReferenceException | InterruptedException excep) {
			Assert.fail("Routing Number was either Not Visible or was Not Clickable");
		}
		return this;
	}

	/**
	 * Set Account Number
	 * 
	 * @param accountNo
	 */
	public EIPPaymentForwardPage setAccoutNumber(String accountNo) {
		try {
			accountNumber.sendKeys(accountNo);
			Reporter.log("Entered account number");
		} catch (Exception e) {
			Assert.fail("Account number field not found");
		}
		return this;
	}

	/**
	 * Click Next Button
	 */
	public EIPPaymentForwardPage clickNextButton() {
		try {
			nxtBtn.click();
			checkPageIsReady();
			if (CollectionUtils.isNotEmpty(continueOverPaymentYes)
					&& !CollectionUtils.sizeIsEmpty(continueOverPaymentYes)) {
				for (WebElement element : continueOverPaymentYes) {
					if (element.isDisplayed()) {
						element.click();
						break;
					}
				}
			}
			Reporter.log("Clicked on Next button");
		} catch (Exception e) {
			Assert.fail("Next button not found");
		}
		return this;
	}

	/**
	 * Click Balance Radio Button
	 */
	public EIPPaymentForwardPage clickNewRadioButton() {
		try {
			if (!newCardRadioBtn.isSelected() && newCardRadioBtn.isDisplayed()) {
				newCardRadioBtn.click();
				Reporter.log("Clicked on new radio button");
			}

		} catch (Exception e) {
			Assert.fail("New radio button not found");
		}
		return this;
	}

	/**
	 * Enter the Payment Amount
	 * 
	 * @param value
	 * @return
	 * 
	 */
	public String enterPaymentamount() {
		Double amount = null;
		try {
			waitforSpinner();
			amount = generateRandomAmount();
			paymentAmounttxtBox.sendKeys(amount.toString());
			Reporter.log("Payment amount entered");
		} catch (Exception e) {
			Assert.fail("Payment entry field not found");
		}
		return amount.toString();
	}

	/**
	 * generate a unique amount value
	 * 
	 * @return double - amount
	 */
	protected Double generateRandomAmount() {
		double min = 1.01;
		double max = 2.00;
		Random r = new Random();
		return r.nextDouble() * (max - min) + min;
	}

	/**
	 * verify saved details are displayed - Old OTP
	 * 
	 * @return boolean
	 */
	public boolean verifySavedCCisChecked() {
		boolean status = false;
		try {
			savedCreditcard.isDisplayed();
			if (savedCreditcard.isSelected())
				status = true;
			Reporter.log("Verified CC is checked");
		} catch (Exception e) {
			Reporter.log("CC checkbox not found");
		}
		return status;
	}

	public void verifyEstimatorAmountField(String amount) {
		try {
			paymentAmounttxtBox.getText().equals(amount);
		} catch (Exception e) {
			Assert.fail("Entered amount in Estimator amount field is not matching");
		}
	}

	/**
	 * verify Pay in Full EIP check box is functioning
	 */
	public void verifyPayEIPFullCheckBox() {
		boolean isSelected=false;
		try {
			isSelected = payEIPFullCheckBox.isSelected();
			clickPayEIPFullCheckBox();
			if (isSelected) {
				Assert.assertFalse(payEIPFullCheckBox.isSelected(),"Pay EIP in Full checkbox is not changed");
			}else{
				Assert.assertTrue(payEIPFullCheckBox.isSelected(),"Pay EIP in Full checkbox is not changed");
			}
		} catch (Exception e) {
			Assert.fail("Pay EIP in Full checkbox is not found");
		}
	}

	public boolean verifyChangeButton() {
		boolean isEnabled = changeLink.isEnabled();
		try {
			if (isEnabled) {
				changeLink.click();
				Reporter.log("clicked on Change link");
			} else {
				Reporter.log("Change link is disabled");
			}
		} catch (Exception e) {
			Assert.fail("Change link is not found");
		}
		return isEnabled;
	}
	
	/**
	 * verify paymentAmounttxtBox read only
	 */
	public EIPPaymentForwardPage verifyPaymentAmountTxtBox() {
		try {
			boolean isEnabled = paymentAmounttxtBox.isEnabled();
			if (isEnabled) {
				Reporter.log("Payment Amount text Box is Enabled");
			} else {
				Reporter.log("Payment Amount text Box is  disabled");
			}
		} catch (Exception e) {
			Assert.fail("Payment Amount text Box is not found");
		}
		return this;
	}
	
	/**
	 * verify EIP Full check box  read only
	 */
	public boolean verifyEIPfullCheckBox() {
		boolean isEnabled = false;
		try {
			isEnabled = payEIPFullCheckBox.isEnabled();
		} catch (Exception e) {
			Assert.fail("Pay EIP in Full CheckBox is not found");
		}
		return isEnabled;
	}

	/**
	 * verify EIP Full check box is enabled
	 */
	public EIPPaymentForwardPage verifyEIPfullCheckBoxIsEnabled() {
		try {
			Assert.assertTrue(verifyEIPfullCheckBox(),"Pay EIP in Full CheckBox is  disabled");
			Reporter.log("Pay EIP in Full CheckBox is Enabled");
		} catch (Exception e) {
			Assert.fail("Pay EIP in Full CheckBox is  disabled");
		}
		return this;
	}
	
	/**
	 * verify EIP Full check box is Disabled
	 */
	public EIPPaymentForwardPage verifyEIPfullCheckBoxIsDisabled() {
		try {
			Assert.assertFalse(verifyEIPfullCheckBox(),"Pay EIP in Full CheckBox is enabled");
			Reporter.log("Pay EIP in Full CheckBox is disabled");
		} catch (Exception e) {
			Assert.fail("Pay EIP in Full CheckBox is enabled");
		}
		return this;
	}
	
	/**
	 * verify JUMP flag is present as a hidden attribute
	 */
	public void verifyJumpFlag() {
		try {
			jumpFlag.getAttribute("value").equals("JUMP");
			Reporter.log("jump Flag attribute is present on the page.");
		} catch (Exception e) {
			Reporter.log("jump Flag attribute is not present on the page.");
		}
	}

	/**
	 * verify Shop URL is present as a hidden attribute
	 */
	public void verifyShopURL() {
		try {
			shopURL.getAttribute("value").equals("/purchase/lineselector");
			Reporter.log("Shop URL attribute is present on the page.");
		} catch (Exception e) {
			Reporter.log("Shop URL attribute is not present on the page.");
		}
	}

	/**
	 * This method is to verify PII masking is done for all personal information
	 * @param custName
	 */
	public void verifyPIImaskingUserName(String custName) {
		try {

			Assert.assertTrue(checkElementisPIIMasked(userFirstName, custName));
			Reporter.log("Verified PII masking for Username");
		} catch (Exception e) {
			Assert.fail("Failed to verify: PII masking for Username");
		}
	}

	/**
	 * This method is to verify PII masking is done for all personal information
	 * @param custMsisdn
	 */
	public void verifyPIImaskingMsisdn(String custMsisdn) {
		try {

			Assert.assertTrue(checkElementisPIIMasked(userMsisdn, custMsisdn));
			Reporter.log("Verified PII masking for Msisdn");
		} catch (Exception e) {
			Assert.fail("Failed to verify: PII masking for Msisdn");
		}
	}

	/**
	 * This method is to verify PII masking is done for all personal information
	 * @param custMsisdn
	 */
	public void verifyPIImaskingSavedCard(String custCrd) {
		try {
			if (verifySavedCCisChecked()) {
				Assert.assertTrue(checkElementisPIIMasked(userSavedCard, custCrd));
				Reporter.log("Verified PII masking for Saved Card");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify: PII masking for Saved Card");
		}
	}
	
}
