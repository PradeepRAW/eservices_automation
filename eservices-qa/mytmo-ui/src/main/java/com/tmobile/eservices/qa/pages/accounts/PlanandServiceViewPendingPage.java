/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author sivas
 *
 */

public class PlanandServiceViewPendingPage extends CommonPage {

	@FindBy(xpath = "//div[contains(text(),'On Us')]")
	private WebElement netflixOnUs;

	@FindBy(xpath = "//div[contains(text(),'$3.00')]")
	private WebElement netflixPremium;

	@FindBy(css = "#summaryrow_id div[class*='date'] span")
	private List<WebElement> takesEffectDate;

	public PlanandServiceViewPendingPage(WebDriver webDriver) {
		super(webDriver);
	}

	public PlanandServiceViewPendingPage verifyONUs(String msg) {
		try {

			checkPageIsReady();
			netflixOnUs.getText().contains(msg);
			Reporter.log("On Us Message is displayed in view pending page");
		} catch (Exception e) {
			Assert.fail("On Us Message not displayed");
		}
		return this;

	}

	public PlanandServiceViewPendingPage verifyPremiumNetflixPrice(String msg) {
		try {
			checkPageIsReady();
			netflixPremium.getText().contains(msg);
			Reporter.log("$3 price  and Netflix premium Detials is displayed in view pending page");
		} catch (Exception e) {
			Assert.fail("$3 price  and Netflix premium Detials is not displayed");
		}
		return this;
	}

	public String getEffectDate() {
		String effectDate = null;
		try {
			checkPageIsReady();
			effectDate = takesEffectDate.get(0).getText();
		} catch (Exception e) {
			Assert.fail("Takes Effect Date is not displayed");
		}
		return effectDate;
	}
	
}