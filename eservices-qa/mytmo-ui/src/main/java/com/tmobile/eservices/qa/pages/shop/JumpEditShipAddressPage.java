/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class JumpEditShipAddressPage extends CommonPage {
	
	public static final String JUMP_EDIT_SHIP_ADDRESS_PAGEURL = "purchase-jump/editshipaddress";
	
	@FindBy(css = "bbutton.btn.btn-primary.PrimaryCTA-Normal")
	private WebElement continueButton;
	
		
	public JumpEditShipAddressPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Jump PLP Page
	 * 
	 * @return
	 */
	public JumpEditShipAddressPage verifyPageUrl() {
		
		waitforSpinner();
		checkPageIsReady();
		try{
			getDriver().getCurrentUrl().contains(JUMP_EDIT_SHIP_ADDRESS_PAGEURL);
		}catch(Exception e){
			Assert.fail(" page is not displayed");
		}
		return this;
	}
	
	
	/**
	 * Click Accept And Continue Button
	 * @return
	 */
	public JumpEditShipAddressPage clickContinueButton() {
		try {
			continueButton.click();
			Reporter.log("ContinueButton is displayed");
		} catch (Exception e) {
			Assert.fail("ContinueButton is not displayed to click");
		}
		return this;
	}
	
	
}
