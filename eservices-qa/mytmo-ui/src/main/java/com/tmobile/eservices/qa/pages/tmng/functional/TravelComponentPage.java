package com.tmobile.eservices.qa.pages.tmng.functional;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.List;

public class TravelComponentPage extends TmngCommonPage {

    private static final Logger logger = LoggerFactory.getLogger(TravelComponentPage.class);

    //***************Page objects for travel component********************//
    @FindBy(css="h1.gh-headline.display-3")
    private WebElement travelHeadline;

    @FindBy(css="h3.gl-subheadline.heading-4")
    private List<WebElement> subHeadline;

    @FindBy(css="p.gl-body-copy")
    private List<WebElement> paragraph;

    @FindBy(css="picture.gb-image-wrapper")
    private List<WebElement> backgroundImage;

    @FindBy(css="input#searchQuery[placeholder='Check location']")
    private WebElement countrySearch;

    @FindBy(css="#countrySearchForm > tmo-travel-search > div > button")
    private List<WebElement> countryFerrySearchButton;

    @FindBy(css="section.roaming  div > div.display-3")
    private WebElement countrySearchResultsHeader;

    @FindBy(css="section.roaming  div.desktop-view > table > tbody")
    private WebElement countrySearchResultsTable;

    @FindBy(css="section.roaming  table > thead > tr > th:nth-child(2) > div")
    private WebElement countrySearchResultsData;

    @FindBy(css="section.roaming  table > thead > tr > th:nth-child(3) > div")
    private WebElement countrySearchResultsText;

    @FindBy(css="section.roaming  table > thead > tr > th:nth-child(4) > div")
    private WebElement countrySearchResultsTalk;

    @FindBy(css="span.legal-copy-text")
    private List<WebElement> legalCopyText;

    @FindBy(css="button.legal-link")
    private List<WebElement> legalTermsLink;

    @FindBy(css=".content-dialog > h3")
    private WebElement extendedLegalTextHeader;

    @FindBy(css="mat-dialog-container.mat-dialog-container p.body>p")
    private WebElement extendedLegalTextBody;

    @FindBy(css="mat-dialog-container.mat-dialog-container p.legal>p")
    private WebElement extendedLegalTextLegal;

    @FindBy(css="button.dialog-close-btn")
    private WebElement extendedLegalCloseButton;

    @FindBy(css="h1.gh-headline.display-2")
    private WebElement cruiseHeadline;

    @FindBy(css="section.cruise  div > div.display-3")
    private WebElement cruiseSearchResultsHeader;

    @FindBy(css="section.cruise  div.desktop-view > table > tbody")
    private WebElement cruiseSearchResultsTable;

    @FindBy(css="section.cruise  table > thead > tr > th:nth-child(2) > div")
    private WebElement cruiseSearchResultsData;

    @FindBy(css="section.cruise  table > thead > tr > th:nth-child(3) > div")
    private WebElement cruiseSearchResultsText;

    @FindBy(css="section.cruise  table > thead > tr > th:nth-child(4) > div")
    private WebElement cruiseSearchResultsTalk;

    @FindBy(css="input#searchQuery[placeholder='Enter cruise/ferry name']")
    private WebElement cruiseSearch;

    //***************methods to validate Travel component********************//
    public TravelComponentPage validateTravelRoamingHeader(String data){
        validatLabel(travelHeadline,data);
        return this;
    }

    public TravelComponentPage validateTravelRoamingSubHeadline(String data){
        validatLabel(subHeadline.get(0),data);
        return this;
    }

    public TravelComponentPage validateTravelRoamingParagraph(String data){
        validatLabel(paragraph.get(0),data);
        return this;
    }

    public TravelComponentPage validateTravelRoamingBackgroundImage(){
        isImageOrLabelPresent(backgroundImage.get(0),"Travel Background");
        return this;
    }

    public TravelComponentPage validateRoamingLegalText(){
        validatLabel(legalCopyText.get(0),"Legal text goes here.");
        validatLabel(legalTermsLink.get(0),"full terms");
        clickElement(legalTermsLink.get(0));
        validatLabel(extendedLegalTextHeader,"Traveling internationally?");
        validatLabel(extendedLegalTextBody,"With T-Mobile ONE™, you get unlimited texting and data at up to 2G speeds in 210+ countries and destinations—all at no extra cost.  ");
        validatLabel(extendedLegalTextLegal,"Extended legal text");
        clickElement(extendedLegalCloseButton);
        return this;
    }

    public TravelComponentPage inputTravelRoamingLocation(String data) throws InterruptedException {
        sendTextData(countrySearch,data);
        clickElement(countryFerrySearchButton.get(0));
        return this;
    }

    public TravelComponentPage searchTravelRoamingLocation() throws InterruptedException {
        clickElement(countryFerrySearchButton.get(0));
        Thread.sleep(3000);
        return this;
    }

    public TravelComponentPage validateCountrySearchResultsColNames() throws InterruptedException {
        validatLabel(countrySearchResultsData,"Data");
        validatLabel(countrySearchResultsText,"Text3");
        validatLabel(countrySearchResultsTalk,"Talk");
        return this;
    }

    public TravelComponentPage validateCountrySearchResultsTable() throws InterruptedException {
        try {
            List<WebElement> countrySearchResultsTableRows = countrySearchResultsTable.findElements(By.tagName("tr"));
            for (WebElement row : countrySearchResultsTableRows) {
                List<WebElement> cells = row.findElements(By.tagName("td"));
                for (WebElement cell : cells) {
                    Assert.assertTrue(isElementDisplayed(cell));
                    Reporter.log(cell.getText() + " - Data Displayed");
                }
            }
        }catch (Exception error){
            Assert.fail("There is a issue with country search results table. Please look into it. "+error.getMessage());
        }

        return this;
    }

    public TravelComponentPage validateTravelCruiseHeader(String data){
        validatLabel(cruiseHeadline,data);
        return this;
    }

    public TravelComponentPage validateTravelCruiseSubHeadline(String data){
        validatLabel(subHeadline.get(1),data);
        return this;
    }

    public TravelComponentPage validateTravelCruiseParagraph(String data){
        validatLabel(paragraph.get(1),data);
        return this;
    }

    public TravelComponentPage validateTravelCruiseBackgroundImage(){
        isImageOrLabelPresent(backgroundImage.get(1),"Travel Background");
        return this;
    }

    public TravelComponentPage validateCruiseLegalText(){
        validatLabel(legalCopyText.get(1),"Legal text goes here.");
        validatLabel(legalTermsLink.get(1),"full terms");
        clickElement(legalTermsLink.get(1));
        validatLabel(extendedLegalTextHeader,"Traveling internationally?");
        validatLabel(extendedLegalTextBody,"With T-Mobile ONE™, you get unlimited texting and data at up to 2G speeds in 210+ countries and destinations—all at no extra cost.  ");
        validatLabel(extendedLegalTextLegal,"Extended legal text");
        clickElement(extendedLegalCloseButton);
        return this;
    }

    public TravelComponentPage inputCruiseName(String data) throws InterruptedException {
        sendTextData(cruiseSearch,data);
        clickElement(countryFerrySearchButton.get(1));
        return this;
    }

    public TravelComponentPage searchCruise() throws InterruptedException {
        clickElement(countryFerrySearchButton.get(1));
        Thread.sleep(3000);
        return this;
    }

    public TravelComponentPage validateCruiseSearchResultsColNames() throws InterruptedException {
        validatLabel(cruiseSearchResultsData,"Data");
        validatLabel(cruiseSearchResultsText,"Text");
        validatLabel(cruiseSearchResultsTalk,"Talk");
        return this;
    }

    public TravelComponentPage validateCruiseSearchResultsTable() throws InterruptedException {
        try {
            List<WebElement> cruiseSearchResultsTableRows = cruiseSearchResultsTable.findElements(By.tagName("tr"));
            for (WebElement row : cruiseSearchResultsTableRows) {
                List<WebElement> cells = row.findElements(By.tagName("td"));
                for (WebElement cell : cells) {
                    Assert.assertTrue(isElementDisplayed(cell));
                    Reporter.log(cell.getText() + " - Data Displayed");
                }
            }
        }catch (Exception error){
            Assert.fail("There is a issue with country search results table. Please look into it. "+error.getMessage());
        }

        return this;
    }

    //********************************* Common Assertion Methods to Validate All labels **************************************//


    private TravelComponentPage validatLabel(WebElement element, String data){
        try {

            Assert.assertTrue(isElementDisplayed(element));
            Assert.assertEquals(element.getText(), data);
//            Assert.assertTrue(element.getText().contains(data),"Expected text: "+data+" did not match with actual text in the application.");
            Reporter.log(element.getText()+" header has been displayed in travel page");

        }
        catch(Exception error){
            Assert.fail(element.getText()+" is not displayed in travel page"+ " "+error.getMessage());
        }

        return this;
    }

    private TravelComponentPage isImageOrLabelPresent(WebElement element, String imagename){
        try {

            Assert.assertTrue(isElementDisplayed(element));
            Reporter.log(imagename+" image has been displayed in travel page");

        }
        catch(Exception error){
            Assert.fail(imagename+" image is not displayed in travel page"+ " Error:"+error.getMessage());
        }

        return this;
    }

    private TravelComponentPage validatLabelTextContent(WebElement element, String data){
        try {

            Assert.assertTrue(isElementDisplayed(element),"Label is not present in the application");
            Assert.assertEquals(element.getText(), data);
//            Assert.assertTrue(element.getAttribute("textContent").contains(data),"Expected text:"+data+" did not match with actual text in the application.");
            Reporter.log(element.getText()+" header has been displayed in travel page");

        }
        catch(Exception error){
            Assert.fail(element.getText()+" is not displayed in travel roaming page"+ " "+error.getMessage());
        }

        return this;
    }


    /**
     * @param webDriver
     */
    public TravelComponentPage(WebDriver webDriver) {
        super(webDriver);
    }
}
