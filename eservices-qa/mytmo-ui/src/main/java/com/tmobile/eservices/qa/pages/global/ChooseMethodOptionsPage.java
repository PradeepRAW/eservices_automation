package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author blakshminarayana
 *
 */
public class ChooseMethodOptionsPage extends CommonPage {

	public static final String chooseMethodOptionsPageUrl = "/oauth2/v1/chooseMethod";
	public static final String chooseMethodOptionsPageLoadText = "Choose verification method";

	public static final String TEXT_MESSGE_TYPE = "TextMessage";
	public static final String EMAIL_MESSAGE_TYPE = "Email";
	public static final String SECURITY_QUESTIONS_MESSGE_TYPE = "SecurityQuestions";

	@FindBy(css = ".ui_subhead.pb5.ng-binding")
	private WebElement chooseTypeText;

	@FindBys(@FindBy(css = ".choose-display.ng-scope.ng-binding"))
	private List<WebElement> chooseOptions;

	@FindBy(css = ".btn.btn-primary.mr30.btn-next")
	private WebElement nextButton;

	@FindBy(css = "div.ui_subhead.ng-binding")
	private WebElement tempPasswordText;

	@FindBy(linkText = "Resend password")
	private WebElement resendPasswordLink;

	@FindBy(linkText = "Back")
	private WebElement backButton;

	@FindBy(css = ".ui_subhead.ng-binding")
	private WebElement checkEmailText;

	@FindBy(linkText = "Resend email")
	private WebElement resentEmailLink;

	@FindBy(css = "div.container-curve.co_reset-password-security > .ui_subhead.ng-binding")
	private WebElement securityQuestionText;

	@FindBy(id = "#que_0")
	private WebElement securityQuestion1;

	@FindBy(id = "#que_1")
	private WebElement securityQuestion2;

	@FindBy(css = ".btn.btn-primary.mr30.btn-next")
	private WebElement securityNextButton;

	@FindAll({ @FindBy(css = "input[value='sdn']"), @FindBy(css = "input[value='sms']") })
	private WebElement selectTextMsg;

	@FindAll({ @FindBy(css = "input[value='question']"), @FindBy(css = "input[value='security_question']") })
	private WebElement selectSecurityQtn;

	@FindBy(css = "input[value='email']")
	private WebElement selectEmail;

	@FindBy(xpath = "//button[contains(text(), 'Next')]")
	private WebElement multiLineNext;

	@FindBy(css = "input[type='password']")
	private List<WebElement> passwords;

	@FindBy(css = "button[type='submit']")
	private WebElement nextBtn;

	/**
	 * 
	 * @param webDriver
	 */
	public ChooseMethodOptionsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * get choose account text
	 * 
	 * @return
	 */
	public ChooseMethodOptionsPage getChooseMethodText() {
		checkPageIsReady();
		try {
			Assert.assertEquals(chooseTypeText.getText().trim(), Constants.VERIFICATION_METHOD_HEADER_TEXT);
			// getText(chooseTypeText);
			Reporter.log("Choose Verification Method text is present");
		} catch (NoSuchElementException e) {
			Assert.fail("Choose Verification Method text is not present.");
		}
		return this;
	}

	/**
	 * choose verification method
	 * 
	 * @param optionValue
	 */
	public ChooseMethodOptionsPage chooseVerificationMethod(String optionValue) {
		checkPageIsReady();
		try {
			switch (optionValue) {
			case SECURITY_QUESTIONS_MESSGE_TYPE:
				selectSecurityQtn.click();
				Reporter.log("Clicked on security question");
				break;
			case EMAIL_MESSAGE_TYPE:
				selectEmail.click();
				Reporter.log("Clicked on email");
				break;
			default:
				selectTextMsg.click();
				Reporter.log("Clicked on text message");
				break;
			}
		} catch (Exception e) {
			Assert.fail("click on choose verification method failed");
		}
		return this;
	}

	/**
	 * This method is used to click the next btn
	 */
	public ChooseMethodOptionsPage clickNextBtn() {
		try {
			nextButton.click();
			Reporter.log("Clicked on next button");
		} catch (Exception e) {
			Assert.fail("click on next button failed");
		}
		return this;
	}

	/**
	 * This method is used to click multi line next btn
	 */
	public ChooseMethodOptionsPage clickMultiLineNext() {
		waitforSpinner();
		try {
			multiLineNext.click();
			Reporter.log("Clicked on multiline next button");
		} catch (Exception e) {
			Assert.fail("click on multiline next failed");
		}
		return this;
	}

	public ChooseMethodOptionsPage clickOnNext() {
		waitforSpinner();
		try {
			multiLineNext.click();
			Reporter.log("Clicked on multiline next button");
		} catch (Exception e) {
			Assert.fail("click on multiline next failed");
		}
		return this;
	}

	public ChooseMethodOptionsPage enterSecurityQuestionsAnswer() {
		checkPageIsReady();
		try {
			for (WebElement password : passwords) {
				password.sendKeys("test");
			}
			Reporter.log("Answer successfully entered in text box");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to enter text in Answer text box");
		}
		return this;
	}

	/**
	 * Verify that current page is loaded.
	 *
	 * @return the Choose Method class instance.
	 */

	public ChooseMethodOptionsPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(chooseMethodOptionsPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Choose Method class instance.
	 */
	public ChooseMethodOptionsPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(chooseMethodOptionsPageUrl);
		return this;
	}

}
