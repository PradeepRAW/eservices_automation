package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author SGaddam5
 *
 */
public class PaymentErrorPage extends CommonPage {

	@FindBy(css = "button.btn.btn-primary")
	private WebElement paymentErrorPage;

	@FindBy(xpath = "//button[contains(text(),'REJECTED') and contains(text(),'addCard')]")
	private WebElement rejectedTransactionButtonAddCard;

	@FindBy(xpath = "//button[contains(text(),'REJECTED') and contains(text(),'addbank')]")
	private WebElement rejectedTransactionButtonAddBank;

	@FindBy(xpath = "//button[contains(text(),'REJECTED') and contains(text(),'bank')]")
	private WebElement rejectedTransactionButtonBank;

	@FindBy(xpath = "//button[contains(text(),'DUPLICATE')]")
	private WebElement dublicateTransactionButton;

	@FindBy(xpath = "//button[contains(text(),'HOLD') and contains(text(),'card')]")
	private WebElement transactionHoldButtonCard;

	@FindBy(xpath = "//button[contains(text(),'HOLD') and contains(text(),'bank')]")
	private WebElement transactionHoldButtonBank;

	@FindBy(xpath = "//button[contains(text(),'UNKNOWN_ERROR')]")
	private WebElement unknownErrorButton;

	@FindBy(xpath = "//button[contains(text(),'HARD_DECLINE')]")
	private WebElement hardDeclineErrorButton;

	@FindBy(xpath = "//button[contains(text(),'SYSTEM_FAILURE')]")
	private WebElement systemErrorButton;

	@FindBy(css = "div.Display3.text-center span")
	private WebElement errorTypeHeader;

	@FindBy(css = "div.padding-top-medium >div.col-12.body span")
	private WebElement errorTypeTransactionMessage;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement backBtn;

	@FindBy(css = "button.PrimaryCTA.full-btn-width")
	private WebElement updatePaymentDetailsBtn;
	
	@FindBy(linkText = "")
	private WebElement pageSpinner;

	@FindBy(linkText = "")
	private WebElement pageLoadElement;

	private final String pageUrl = "";
	
	/***
	 * Constructor to pass driver
	 * @param webDriver
	 */
	public PaymentErrorPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public PaymentErrorPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */

	public PaymentErrorPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			paymentErrorPage.isDisplayed();
			Reporter.log("Navigated to payments error page ");
		} catch (Exception e) {
			Assert.fail("Payment Error page not loaded");
		}
		return this;
	}

	/**
	 * 
	 */

	public PaymentErrorPage clickRejectedTrasactionButtonAddCard() {
		try {
			clickElementWithJavaScript(rejectedTransactionButtonAddCard);
			Reporter.log(" Clicked on Rejected transaction button");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickRejectedTrasactionButtonAddBank() {
		try {
			clickElementWithJavaScript(rejectedTransactionButtonAddBank);
			Reporter.log("Clicked Rejected transaction button ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickRejectedTrasactionButtonBank() {
		try {
			clickElementWithJavaScript(rejectedTransactionButtonBank);
			Reporter.log("Clicked on Rejected transaction bank ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify rejected transaction error
	 */
	public PaymentErrorPage verifyRejectedTransactionHeader() {
		try {
			errorTypeHeader.isDisplayed();
			errorTypeHeader.getText().contains("Incorrect Info");
			Reporter.log("Rejected Transaction Header displayed");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify rejected transaction error message
	 */
	public PaymentErrorPage verifyrejectedTrasactionErrorMessage() {
		try {
			errorTypeTransactionMessage.isDisplayed();
			errorTypeTransactionMessage.getText().contains(
					"Some information entered appears to be incorrect. Please verify the payment info submitted and try again.");
			Reporter.log("Rejected transaction error message displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickDublicateTrasactionButton() {
		try {
			dublicateTransactionButton.click();
			Reporter.log("Clicked on duplicate transaction button ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify dublicate transaction error
	 */
	public PaymentErrorPage verifyDublicateTransactionHeader() {
		try {
			errorTypeHeader.isDisplayed();
			errorTypeHeader.getText().contains("Duplicate Payment");
			Reporter.log("Duplicate transaction header displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify dublicate transaction error message
	 */
	public PaymentErrorPage verifyDublicateTrasactionErrorMessage() {
		try {
			errorTypeTransactionMessage.isDisplayed();
			errorTypeTransactionMessage.getText().contains(
					"To protect you from being overcharged, we prevent payments of the same amount within 24 hours. To proceed with this payment please modify the amount.");
			Reporter.log("Duplicate transaction error message displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickTrasactionHoldButtonCard() {
		try {
			transactionHoldButtonCard.isDisplayed();
			Reporter.log("Clicked on transaction Hold button ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickTrasactionHoldButtonBank() {
		try {
			transactionHoldButtonBank.click();
			Reporter.log("Clicked on transaction hold button bank ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify Transaction on hold error
	 */
	public PaymentErrorPage verifyTransactionHoldHeader() {
		try {
			errorTypeHeader.isDisplayed();
			errorTypeHeader.getText().contains("Unsuccessful");
			Reporter.log("Transaction Hold Header displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify Transaction on hold error message
	 */
	public PaymentErrorPage verifyTrasactionHoldErrorMessageCard() {
		try {
			errorTypeTransactionMessage.isDisplayed();
			errorTypeTransactionMessage.getText().contains(
					"We are unable to accept payment with the card provided. Please try another payment method or contact your bank to resolve the issue");
			Reporter.log("Transaction Hold error message for card displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify Transaction on hold error message
	 */
	public PaymentErrorPage verifyTrasactionHoldErrorMessageBank() {
		try {
			errorTypeTransactionMessage.isDisplayed();
			errorTypeTransactionMessage.getText().contains(
					"We are unable to accept payment with the bank provided. Please try another payment method or contact your bank to resolve the issue");
			Reporter.log("Transaction Hold error message bank displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickUnKnownErrorButton() {
		try {
			unknownErrorButton.click();
			Reporter.log("Clicked on unknown error button  ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify unknown error
	 */
	public PaymentErrorPage verifyUnKnownErrorHeader() {
		try {
			errorTypeHeader.isDisplayed();
			errorTypeHeader.getText().contains("Unknown Error");
			Reporter.log("Unknown error header displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify unknown error message
	 */
	public PaymentErrorPage verifyUnKnownErrorMessage() {
		try {
			errorTypeTransactionMessage.isDisplayed();
			errorTypeTransactionMessage.getText().contains(
					"Something went wrong and we were unable to process your request. Please check the Payment Method details or try again");
			Reporter.log("Unknown Error message displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickHardDeclineErrorButton() {
		try {
			hardDeclineErrorButton.click();
			Reporter.log("Clicked on Decline error button ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify hard decline error
	 */
	public PaymentErrorPage verifyHardDeclineErrorHeader() {
		try {
			errorTypeHeader.isDisplayed();
			errorTypeHeader.getText().contains("Payment Declined");
			Reporter.log("Hard Decline Error Header displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify hard decline error message
	 */
	public PaymentErrorPage verifyHardDeclineErrorMessage() {
		try {
			errorTypeTransactionMessage.isDisplayed();
			errorTypeTransactionMessage.getText()
					.contains("Please try another method or contact your financial institution to resolve the issue");
			Reporter.log("Hard Decline error message displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	public PaymentErrorPage clickSystemErrorButton() {
		try {
			systemErrorButton.click();
			Reporter.log("Clicked on system error button ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify hard decline error
	 */
	public PaymentErrorPage verifySystemErrorHeader() {
		try {
			errorTypeHeader.isDisplayed();
			errorTypeHeader.getText().contains("System Error");
			Reporter.log("System error Header displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify hard decline error message
	 */
	public PaymentErrorPage verifySystemErrorMessage() {
		try {
			errorTypeTransactionMessage.isDisplayed();
			errorTypeTransactionMessage.getText().contains(
					"We experienced a technical problem while processing your payment. Please wait a few moments and try again");
			Reporter.log("System error message displayed ");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify back btn
	 */
	public PaymentErrorPage verifyBackBtnDispalyed() {
		try {
			backBtn.isDisplayed();
			Reporter.log("Back button displayed");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

	/**
	 * verify update payment detais button
	 */
	public PaymentErrorPage verifyUpdatePaymentDetailsBtn() {
		try {
			updatePaymentDetailsBtn.isDisplayed();
			Reporter.log("update payment details button displayed");
		} catch (Exception e) {
			Assert.fail("CVV not found");
		}
		return this;
	}

}
