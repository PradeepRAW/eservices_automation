package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class TPPPreScreenForm extends TmngCommonPage {

	// Invalid WA Address
	public static final String zipValid = "98029";
	public static final String addressValid = "1421 NE Iris St";
	public static final String zipInvalid = "98031";
	public static final String addressInvalid1 = "1421 NE Iri St";
	public static final String addressInvalid2 = "1421";
	public static final String addressInvalid3 = "1421 NE Iris ";
	public static final String addressInvalid4 = "8421 NE Iris St";

	@FindBy(css = "tmo-pre-screen-form>h2")
	private WebElement preScreenFormHeader;

	@FindBy(css = "input[formcontrolname*='firstName']")
	private WebElement firstnameOnPrescreenForm;

	@FindBy(css = "input[formcontrolname*='lastName']")
	private WebElement lastnameOnPrescreenForm;

	@FindBy(css = "input[formcontrolname*='emailAddress']")
	private WebElement emailOnPrescreenForm;

	@FindBy(css = "input[formcontrolname*='addressLine']")
	private WebElement addressOnPrescreenForm;

	@FindBy(css = "input[formcontrolname*='zipCode']")
	private WebElement zipCodeOnPrescreenForm;

	@FindBy(css = "input[formcontrolname*='dateOfBirth']")
	private WebElement dateofBirthOnPrescreenForm;

	@FindBy(css = "input[formcontrolname*='last4Ssn']")
	private WebElement last4digitsOfSSNOnPrescreenForm;

	@FindBy(css = "button[data-analytics-value='See your payments']")
	private WebElement findMyPriceCTAOnPreScreenForm;

	@FindBy(xpath = "//h2[contains(text(),'See your device payment amounts.')]")
	private WebElement preScreenLoadingHeader;

	@FindBy(css = "div.loading-overlay p")
	private WebElement preScreenLoadingText;

	@FindBy(css = "div.ellipsis-animation")
	private WebElement ellipsesAnimation;

	@FindBy(css = ".pre-screen-container h2")
	private WebElement existingCustomerNotifyModalHeader;

	@FindBy(css = ".pre-screen-container")
	private WebElement existingCustomerNotifyModalPageText;

	@FindBy(css = "div[class*='actions-container margin-top-sm'] a")
	private WebElement logInCTAOnExistingCustomerNotifyModal;

	@FindBy(css = "mat-form-field")
	private WebElement processingPageHeader;

	@FindBy(css = "")
	private WebElement unknownerrorduringTPP;

	@FindBy(css = "tmo-pre-screen-form>div")
	private WebElement preScreenFormTextUnderHeader;

	@FindBy(xpath = "//button[contains(.,'Skip')]")
	private WebElement skipForNowCtaOnPreScreenForm;

	@FindBy(css = "p.text-legal")
	private WebElement legalTextOnPreScreenForm;

	@FindBy(css = "mat-error[class='mat-error ng-star-inserted']")
	private List<WebElement> errorMessagesforEmptyFields;

	@FindBy(css = ".actions-container mat-error")
	private WebElement errorMessagesforInvalidAddress;

	public TPPPreScreenForm(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * verify Pre-Screen Form
	 *
	 */
	public TPPPreScreenForm verifyPreScreenForm() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(preScreenFormHeader), 60);
			Assert.assertTrue(preScreenFormHeader.isDisplayed(), "Pre screen form is not loaded successfully");
			Reporter.log("Pre screen form is loaded successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pre-Screen Form");
		}

		return this;
	}

	/**
	 * verify Pre-Screen Form page URL
	 *
	 */
	public TPPPreScreenForm verifyPreScreenFormPageURL() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("/pre-screen/form"),
					"Pre screen form  page url is not correct");
			Reporter.log("Pre screen form page is url verified successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pre-Screen Form page URL");
		}

		return this;
	}

	/**
	 * verify Pre-Screen Form page title
	 *
	 */
	public TPPPreScreenForm verifyPreScreenFormPageTitle() {
		try {

			Assert.assertTrue(preScreenLoadingHeader.isDisplayed(), "Pre screen form  page header is not displayed");
			Assert.assertTrue(preScreenLoadingHeader.getText().contains("See your device payment amounts."),
					"Pre screen form  page header is not correct");
			Reporter.log("Pre screen form page is title verified successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pre-Screen Form page title");
		}

		return this;
	}

	/**
	 * Fill All mandatory fields on Pre Screen form
	 *
	 */
	public TPPPreScreenForm fillAllFieldsOnPreScreenform(TMNGData tmngData) {
		try {
			firstnameOnPrescreenForm.sendKeys(tmngData.getFirstName());
			lastnameOnPrescreenForm.sendKeys(tmngData.getLastName());
			emailOnPrescreenForm.sendKeys(tmngData.getEmail());
			addressOnPrescreenForm.clear();
			addressOnPrescreenForm.sendKeys(tmngData.getShippingAddress());
			zipCodeOnPrescreenForm.sendKeys(tmngData.getZipcode());
			dateofBirthOnPrescreenForm.sendKeys(tmngData.getDOB());
			last4digitsOfSSNOnPrescreenForm.sendKeys(tmngData.getSSNLastFour());
			Reporter.log("Filled all mandatory fields on Pre Screen form");
		} catch (Exception e) {
			Assert.fail("Failed to Fill mandatory fields on Pre Screen form");
		}

		return this;
	}

	/**
	 * Fill Invalid Invalid Address on Pre Screen form
	 *
	 */
	public TPPPreScreenForm fillInvalidAddress(String condition) {
		try {
			if (condition.equalsIgnoreCase("wrong zip code")) {
				zipCodeOnPrescreenForm.click();
				zipCodeOnPrescreenForm.clear();
				zipCodeOnPrescreenForm.sendKeys(zipInvalid);
				Reporter.log("Filled invalid ZIP on prescreen form");
			} else if (condition.equalsIgnoreCase("wrong street")) {
				addressOnPrescreenForm.click();
				addressOnPrescreenForm.clear();
				addressOnPrescreenForm.sendKeys(addressInvalid1);
				zipCodeOnPrescreenForm.click();
				zipCodeOnPrescreenForm.clear();
				zipCodeOnPrescreenForm.sendKeys(zipValid);
				Reporter.log("Filled invalid street address and correct ZIP on prescreen form");
			} else if (condition.equalsIgnoreCase("only house number")) {
				addressOnPrescreenForm.click();
				addressOnPrescreenForm.clear();
				addressOnPrescreenForm.sendKeys(addressInvalid2);
				Reporter.log("Filled only house number and correct ZIP on prescreen form");
			} else if (condition.equalsIgnoreCase("incomplete street address")) {
				addressOnPrescreenForm.click();
				addressOnPrescreenForm.clear();
				addressOnPrescreenForm.sendKeys(addressInvalid3);
				Reporter.log("Filled incomplete street address and correct ZIP on prescreen form");
			} else if (condition.equalsIgnoreCase("wrong house number")) {
				addressOnPrescreenForm.click();
				addressOnPrescreenForm.clear();
				addressOnPrescreenForm.sendKeys(addressInvalid4);
				Reporter.log("Filled invalid house number and correct ZIP on prescreen form");
			} else {
				Reporter.log("Invalid address use case NOT FOUND");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Fill Invalid Invalid Address on Pre Screen form");
		}
		return this;
	}

	/**
	 * Fill Invalid Invalid Address on Pre Screen form
	 *
	 */
	public TPPPreScreenForm provideCorrectAddress(TMNGData tmngData) {
		try {
			addressOnPrescreenForm.clear();
			addressOnPrescreenForm.sendKeys(tmngData.getShippingAddress());
			Reporter.log("Address is provided to prescreen form");
		} catch (Exception e) {
			Assert.fail("Failed to provide address to prescreen form");
		}
		return this;
	}

	/**
	 * click Find My Price On Pre-screen form
	 *
	 */
	public TPPPreScreenForm clickFindMyPriceCTAOnPreScreenForm() {
		try {
			// clickElementWithJavaScript(findMyPriceCTAOnPreScreenForm);
			findMyPriceCTAOnPreScreenForm.click();
			Reporter.log("Clicked on 'Find My Price' CTA on Pre screen form");
		} catch (Exception e) {
			Assert.fail("Failed to Click 'Find My Price' CTA on Pre-screen form");
		}
		return this;
	}

	/**
	 * Verify Pre-Screen loading Page
	 *
	 */
	public TPPPreScreenForm verifyPreScreenloadingPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(preScreenLoadingText), 60);
			Assert.assertTrue(preScreenLoadingText.isDisplayed(), "Pre screen loading screen is not loaded");
			Reporter.log("Pre-Screen loading Page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pre-Screen loading Page");
		}
		return this;
	}

	/**
	 * Verify Pre-Screen loading Page Header
	 *
	 */
	public TPPPreScreenForm verifyPreScreenloadingPageHeader() {
		try {
			Assert.assertTrue(preScreenLoadingHeader.isDisplayed(), "Pre screen loading screen is not loaded");
			Reporter.log("Pre-Screen loading Page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pre-Screen loading Page");
		}
		return this;
	}

	/**
	 * Verify Existing Customer notify Modal header
	 *
	 */
	public TPPPreScreenForm verifyExistingCustomerNotifyModalHeader() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(existingCustomerNotifyModalHeader), 60);
			Assert.assertTrue(existingCustomerNotifyModalHeader.isDisplayed(),
					"Existing Customer notify Modal header is not loaded");
			Assert.assertTrue(existingCustomerNotifyModalHeader.getText().contains("T-Mobile customer"),
					"Existing Customer notify Modal header text is not correct");
			Reporter.log("Existing Customer notify Modal header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Existing Customer notify Modal header");
		}
		return this;
	}

	/**
	 * Verify Existing Customer notify Modal page text
	 *
	 */
	public TPPPreScreenForm verifyExistingCustomerNotifyModalPageText() {
		try {
			Assert.assertTrue(existingCustomerNotifyModalPageText.isDisplayed(),
					"Existing Customer notify Modal page text is not loaded");
			Assert.assertTrue(
					existingCustomerNotifyModalPageText.getText()
							.contains("Please log in to your account to make a purchase"),
					"Existing Customer notify Modal page text is not correct");
			Reporter.log("Existing Customer notify Modal page text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Existing Customer notify Modal page text");
		}
		return this;
	}

	/**
	 * Click LogIn CTA on Existing Customer Notify Modal
	 *
	 */
	public TPPPreScreenForm clickLogInCTAonExistingCustomerNotifyModal() {
		try {
			logInCTAOnExistingCustomerNotifyModal.click();
			// clickElementWithJavaScript(logInCTAOnExistingCustomerNotifyModal);
			Reporter.log("Clicked on login CTAExisting Customer notify Modal");
		} catch (Exception e) {
			Assert.fail("Failed to Click LogIn CTA on Existing Customer Notify Modal");
		}
		return this;
	}

	/**
	 * Verify Processing page Modal header
	 *
	 */
	public TPPPreScreenForm verifyProccessingPageHeader() {
		try {
			Assert.assertTrue(processingPageHeader.isDisplayed(), "Processing page Header is not loaded");
			Reporter.log("Processing page Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Processing page  Modal header");
		}
		return this;
	}

	/**
	 * verify Unknown Error message during TPP prescreen Flow
	 *
	 */
	public TPPPreScreenForm verifyUnknownErrorMessageduringTPPFlow() {
		try {
			Assert.assertTrue(unknownerrorduringTPP.isDisplayed(),
					"Error message is not displaying in during TPP pre screen flow If unexpected or unknown error occurs");
			Reporter.log(
					"Error message is displaying in during TPP pre screen flow If unexpected or unknown error occurs");
		} catch (Exception e) {
			Assert.fail("Failed to verify Unknown Error message during TPP prescreen Flow ");
		}
		return this;
	}

	/**
	 * Verifies Prescreen form page header
	 *
	 * @return
	 */
	public TPPPreScreenForm verifyPrescreenFormPageHeader() {
		try {
			Assert.assertTrue(preScreenFormHeader.isDisplayed(),
					"PreScreen Form Header is not displayed during TPP pre screen");
			Reporter.log("PreScreen Form Header is displayed during TPP pre-screen");
		} catch (Exception e) {
			Assert.fail("Failed to verify PreScreen Form Header during TPP prescreen Flow ");
		}
		return this;
	}

	/**
	 * Used to verify text under prescreen form page header
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyTextUnderPrescreenFormPageHeader() {
		try {
			Assert.assertTrue(preScreenFormTextUnderHeader.isDisplayed(),
					"PreScreen Form text under header is not displayed during TPP pre screen");
			Reporter.log("PreScreen Form text under header is displayed during TPP pre-screen");
		} catch (Exception e) {
			Assert.fail("Failed to verify PreScreen Form text under header during TPP prescreen Flow ");
		}
		return this;
	}

	/**
	 * Used to verify editable fields section
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyEditableFieldsSection() {
		try {
			verifyFirstNameField();
			verifyLastNameField();
			verifyAddressField();
			verifyEmailAddressField();
			verifyZipCodeField();
			verifyDOBField();
			verifySSNField();
			Reporter.log("Verified editable fields section");
		} catch (Exception e) {
			Assert.fail("Failed to verify editable fields section");
		}
		return this;
	}

	/**
	 * Verifies First name field
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyFirstNameField() {
		try {
			Assert.assertTrue(firstnameOnPrescreenForm.isDisplayed(), "First name field is not displayed");
			Assert.assertTrue(firstnameOnPrescreenForm.getText().contains(""),
					"First name placeholder text is not displayed");
			Reporter.log("First name field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify first name field");
		}
		return this;
	}

	/**
	 * Verifies Last name field
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyLastNameField() {
		try {
			Assert.assertTrue(lastnameOnPrescreenForm.isDisplayed(), "Last name field is not displayed");
			Assert.assertTrue(lastnameOnPrescreenForm.getText().contains(""),
					"Last name placeholder text is not displayed");
			Reporter.log("Last name field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify last name field");
		}
		return this;
	}

	/**
	 * Verifies Email address field
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyEmailAddressField() {
		try {
			Assert.assertTrue(emailOnPrescreenForm.isDisplayed(), "Email name field is not displayed");
			Assert.assertTrue(emailOnPrescreenForm.getText().contains(""),
					"Email name field placeholder text is not displayed");
			Reporter.log("Email name field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify email name field");
		}
		return this;
	}

	/**
	 * Verifies Address field
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyAddressField() {
		try {
			Assert.assertTrue(addressOnPrescreenForm.isDisplayed(), "Address name field is not displayed");
			Assert.assertTrue(addressOnPrescreenForm.getText().contains(""),
					"Address name field placeholder text is not displayed");
			Reporter.log("Address name field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify address name field");
		}
		return this;
	}

	/**
	 * Verifies ZIP code field
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyZipCodeField() {
		try {
			Assert.assertTrue(zipCodeOnPrescreenForm.isDisplayed(), "Zipcode field is not displayed");
			Assert.assertTrue(zipCodeOnPrescreenForm.getText().contains(""),
					"Zipcode field placeholder text is not displayed");
			Reporter.log("Zipcode field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Zipcode field");
		}
		return this;
	}

	/**
	 * Verifies DOB field
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyDOBField() {
		try {
			Assert.assertTrue(dateofBirthOnPrescreenForm.isDisplayed(), "DOB field is not displayed");
			Assert.assertTrue(dateofBirthOnPrescreenForm.getText().contains(""),
					"DOB field placeholder text is not displayed");
			Reporter.log("DOB field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify DOB field");
		}
		return this;
	}

	/**
	 * Verifies SSN field
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifySSNField() {
		try {
			Assert.assertTrue(last4digitsOfSSNOnPrescreenForm.isDisplayed(),
					"last 4digits Of SSN field is not displayed");
			Assert.assertTrue(last4digitsOfSSNOnPrescreenForm.getText().contains(""),
					"last 4digits Of SSN field placeholder text is not displayed");
			Reporter.log("last 4digits Of SSN is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify last4digitsOfSSN field");
		}
		return this;
	}

	/**
	 * Verifies presence of Find my price CTA
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyFindMyPriceCta() {
		try {
			Assert.assertTrue(findMyPriceCTAOnPreScreenForm.isDisplayed(), "Find My Price CTA is not displayed");
			Reporter.log("Find My Price CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Find My Price CTA");
		}
		return this;
	}

	/**
	 * Verifies Skip for now CTA
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifySkipForNowCta() {
		try {
			Assert.assertTrue(skipForNowCtaOnPreScreenForm.isDisplayed(), "Skip for now CTA is not displayed");
			Reporter.log("Skip for now CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Skip for now CTA");
		}
		return this;
	}

	/**
	 * Verifies presence of legal text at bottom
	 * 
	 * @return
	 */
	public TPPPreScreenForm verifyLegalTextatBottom() {
		try {
			Assert.assertTrue(legalTextOnPreScreenForm.isDisplayed(), "Legal Text on PreScreen Form is not displayed");
			Reporter.log("Legal Text on PreScreen Form is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Legal Text on PreScreen Form");
		}
		return this;
	}

	/**
	 * Click on Skip for now cta
	 * 
	 * @return
	 */
	public TPPPreScreenForm clickSkipForNowCta() {
		try {
			skipForNowCtaOnPreScreenForm.click();
			Reporter.log("Clicked on 'Skip for now' Cta On PreScreen Form");
		} catch (Exception e) {
			Assert.fail("Failed to click on 'Skip for now' Cta");
		}
		return this;
	}

	/**
	 * verify Error messages for empty fields on Pre-Screen Form page
	 *
	 */
	public TPPPreScreenForm verifyErrorMessagesforAllFieldsForDataMissing() {
		try {
			firstnameOnPrescreenForm.click();
			lastnameOnPrescreenForm.click();
			emailOnPrescreenForm.click();
			addressOnPrescreenForm.click();
			zipCodeOnPrescreenForm.click();
			dateofBirthOnPrescreenForm.click();
			last4digitsOfSSNOnPrescreenForm.click();
			findMyPriceCTAOnPreScreenForm.click();
			for (WebElement errorMessagesforEmptyFields : errorMessagesforEmptyFields) {
				Assert.assertTrue(errorMessagesforEmptyFields.isDisplayed(),
						"Error messages for empty fields on Pre-Screen Form page is not displayed");
			}
			Reporter.log("Error messages for empty fields on Pre-Screen Form page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error messages for empty fields on Pre-Screen Form page");
		}

		return this;
	}

	/**
	 * verify Error Message For DOB Below 18 and incorrect format
	 *
	 */
	public TPPPreScreenForm verifyErrorMessageForDOBBelow18andIncorrectFormat() {
		try {
			dateofBirthOnPrescreenForm.sendKeys("abc");
			Assert.assertTrue(errorMessagesforEmptyFields.get(5).getText().contains("is not a valid date"),
					"Error messages for DOB incorrect format on Pre-Screen Form page is not displayed");
			dateofBirthOnPrescreenForm.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
			dateofBirthOnPrescreenForm.sendKeys("10/12/2019");
			Assert.assertTrue(errorMessagesforEmptyFields.get(5).getText().contains("You must be at least 21"),
					"Error messages for DOB below 18 on Pre-Screen Form page is not displayed");
			dateofBirthOnPrescreenForm.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
			dateofBirthOnPrescreenForm.sendKeys("2019/22/22");
			Assert.assertTrue(errorMessagesforEmptyFields.get(5).getText().contains("is not a valid date"),
					"Error messages for DOB incorrect format on Pre-Screen Form page is not displayed");
			Reporter.log("Error messages for DOB incorrect format on Pre-Screen Form page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For DOB Below 18 and  DOB incorrect format");
		}

		return this;
	}

	/**
	 * verify Error Message For Invalid address on prescreen form
	 *
	 */
	public TPPPreScreenForm verifyErrorMessageForInvalidAddress() {
		try {
			waitFor(ExpectedConditions.visibilityOf(errorMessagesforInvalidAddress));
			Assert.assertTrue(isElementDisplayed(errorMessagesforInvalidAddress),
					"Error Message For Invalid address on prescreen form is not displayed");
			Reporter.log("Error Message For Invalid address on prescreen form is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Invalid address on prescreen form");
		}

		return this;
	}

	/**
	 * verify Error Message For valid address on prescreen form not dipalying on
	 * prescreen
	 *
	 */
	public TPPPreScreenForm verifyErrorMessageForAddressFiledisNotdisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(errorMessagesforInvalidAddress),
					"Error Message For valid address on prescreen form is displayed");
			Reporter.log("Error Message For valid address on prescreen form not dipalying on prescreen ");
		} catch (Exception e) {
			Assert.fail(
					"Failed to verify Error Message For valid address on prescreen form not dipalying on prescreen ");
		}

		return this;
	}
}
