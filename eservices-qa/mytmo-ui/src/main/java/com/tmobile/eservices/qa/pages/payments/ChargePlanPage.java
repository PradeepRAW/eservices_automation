package com.tmobile.eservices.qa.pages.payments;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class ChargePlanPage extends CommonPage {

	private final String pageUrl = "charges/category/charge/Plan";

	@FindBy(xpath = "//span[@class='bb-back']/img")
	private WebElement backToSummaryLink;

	@FindBy(css = "i.fa.fa-angle-left")
	private WebElement arrowLeft;

	@FindBy(css = "i.fa.fa-angle-right")
	private WebElement arrowRight;

	@FindBy(css = "bb-slider div.bb-slider-item-amount")
	private List<WebElement> amountBlock;

	@FindBy(css = "img.loader-icon")
	private WebElement pageSpinner;

	@FindBy(css = "span.bb-back")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//bb-slider//div[contains(text(),'Plans')]")
	private WebElement plansTabInSlider;

	@FindBy(css = "a.bb-taxes-link")
	private WebElement viewIncludedTaxesAndFeesLink;

	@FindBy(css = "div.bb-taxes-title")
	private WebElement includedTaxesAndFeesModal;

	@FindBy(css = "img.bb-close-image")
	private WebElement includedTaxesAndFeesModalCloseBtn;

	@FindBy(css = "a.bb-charge-link")
	private WebElement plansViewDetails;

	@FindBy(xpath = "//div[contains(@class,'charge-detail-label')]")
	private WebElement labeltotal;

	@FindBy(xpath = "//div[contains(@class,'charge-detail-amount')]")
	private WebElement amounttotal;

	@FindAll({ @FindBy(xpath = "//div[contains(@class,'bb-charge-sub-category-title h3')]"),
			@FindBy(xpath = "//div[contains(@class,'bb-charge-detail-category-title h2')]") })
	private List<WebElement> headervoicelines;

	@FindBy(css = "span.bb-subscriber-name")
	private List<WebElement> subsribernames;

	@FindBy(css = "span.bb-subscriber-asset-id")
	private List<WebElement> subscriberids;

	@FindBy(css = "div.bb-charges.separator div.bb-charge-amount")
	private List<WebElement> subscriberamounts;

	@FindBy(css = "bb-slider div.bb-slider-item-title")
	private List<WebElement> slideritemstitle;

	@FindBy(xpath = "//div[contains(text(),'Exemption')]")
	private List<WebElement> Excemptions;

	@FindBy(xpath = "//div[@class='bb-slider']//div[contains(text(),'Plans')]/following-sibling::div[@class='bb-slider-item-amount']//div")
	private WebElement Plansslideramount;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public ChargePlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ChargePlanPage verifyPageUrl() {
		checkPageIsReady();
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @throws InterruptedException
	 */
	public ChargePlanPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Charge Plan page is not present");
		}
		return this;
	}

	/**
	 * Click on View Details link.
	 *
	 * @throws InterruptedException
	 */
	public ChargePlanPage clickPlansViewDetailsLink() {
		try {
			plansViewDetails.click();
			Reporter.log("Clicked on Plan View Details Link");
		} catch (Exception e) {
			Assert.fail("Fail to click on Plans Views Details");
		}
		return this;
	}

	public boolean checkExcemptionsapplied() {

		if (Excemptions.size() > 0)
			return true;
		else
			return false;

	}

	/**
	 * Click on View included taxes and fees link.
	 *
	 * @throws InterruptedException
	 */
	public ChargePlanPage clickViewInlcudedTaxesFeesLink() {
		try {
			viewIncludedTaxesAndFeesLink.click();
			Reporter.log("Clicked on View included taxes and fees link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on View included taxes and fees link.");
		}
		return this;
	}

	public ChargePlanPage checklabelTotal(String text) {
		try {
			if (labeltotal.getText().trim().equalsIgnoreCase(text))
				Reporter.log(text + " label is matching with Actual");
			else
				Verify.fail(text + " label is not matching with Actual");
		} catch (Exception e) {
			Verify.fail("Fail to identify total lable");
		}
		return this;
	}

	public ChargePlanPage checktotalAmount() {
		try {
			String amount = amounttotal.getText().trim();
			if (amount.startsWith("$")
					&& amount.substring(amount.length() - 3, amount.length() - 2).equalsIgnoreCase("."))
				Reporter.log("Total amount format is matched");
			else
				Verify.fail("Total amount format is not matched");
		} catch (Exception e) {
			Verify.fail("Fail identify Total amount");
		}
		return this;
	}

	public ChargePlanPage checktotalAmountwithslideramount() {
		try {
			String uiamount = amounttotal.getText().trim().replace(".", "");
			String slideramount = Plansslideramount.getText().replace(".", "");
			System.out.println(uiamount + "===" + slideramount);
			if (uiamount.equalsIgnoreCase(slideramount))
				Reporter.log("Total amount in slider is same as ui frame");
			else
				Assert.fail("Total amount in slider is not matching with value in ui frame");
		} catch (Exception e) {
			Assert.fail("Fail identify amount");
		}
		return this;
	}

	/**
	 * Click On view Included Taxes Fees Link
	 * 
	 * @return
	 */
	public ChargePlanPage clickviewIncludedTaxesAndFeesLink() {
		try {
			checkPageIsReady();
			moveToElement(viewIncludedTaxesAndFeesLink);
			viewIncludedTaxesAndFeesLink.click();
			Reporter.log("View included taxes & fees link is clicked");
		} catch (Exception e) {
			Assert.fail("View included taxes & fees link is not existing");
		}
		return this;
	}

	public ChargePlanPage checkvoicelinesheader() {
		try {
			checkPageIsReady();
			scrollToElement(headervoicelines.get(0));
			if (verifyElementBytext(headervoicelines, "Voice") || verifyElementBytext(headervoicelines, "Voice Line"))
				Reporter.log("Voice Lines label is matching with Actual");
			else
				Verify.fail("Voice Lines label is not matching with Actual");
		} catch (Exception e) {
			Verify.fail("Fail to identify voice lines lable");
		}
		return this;
	}

	public List<String> getslidertitles() {
		List<String> titles = new ArrayList<String>();
		try {
			for (WebElement title : slideritemstitle) {
				titles.add(title.getText().trim());
			}
		} catch (Exception e) {
			Assert.fail("slideritemstitle element is not present");
		}
		return titles;
	}

	public ChargePlanPage checkleftslider() {
		try {
			List<String> oldlist = getslidertitles();
			arrowLeft.click();
			Thread.sleep(1000);
			List<String> newList = getslidertitles();
			for (int i = 0; i < oldlist.size() - 1; i++) {
				if (oldlist.get(i).equalsIgnoreCase(newList.get(i + 1)))
					Reporter.log("Left slider is working");
				else
					Assert.fail("Left slider is not working");
			}
		} catch (Exception e) {
			Assert.fail("Fail to identify voice lines lable");
		}
		return this;
	}

	public ChargePlanPage checkrightslider() {
		try {
			List<String> oldlist = getslidertitles();
			arrowRight.click();
			Thread.sleep(1000);
			List<String> newList = getslidertitles();
			for (int i = 1; i < oldlist.size(); i++) {
				if (oldlist.get(i).equalsIgnoreCase(newList.get(i - 1)))
					Reporter.log("Left slider is working");
				else
					Assert.fail("Left slider is not working");
			}
		} catch (Exception e) {
			Assert.fail("Fail to identify voice lines lable");
		}
		return this;
	}

	public ChargePlanPage verifyvoicelines() {
		try {
			checkPageIsReady();
			if (subsribernames.size() > 0) {
				Reporter.log("Voice lines are greater than zero and equal to :" + subsribernames.size());
				if (subsribernames.size() == subscriberids.size()
						&& subsribernames.size() == subscriberamounts.size()) {
					Reporter.log("number of subsriber lines matching with subsriber ids and amounts");
				} else {
					Verify.fail("number of subsriber lines are not matching with subsriber ids and amounts");
				}
			} else {
				Verify.fail("No voice lines");
			}
		} catch (Exception e) {
			Verify.fail("Fail to identify voice lines");
		}
		return this;
	}

	/**
	 * Verify that the Included Taxes And Fees Modal loaded completely.
	 *
	 */
	public ChargePlanPage verifyIncludedTaxesAndFeesModal() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(includedTaxesAndFeesModal));
			Assert.assertTrue(includedTaxesAndFeesModal.getText().trim().equalsIgnoreCase("Included taxes & fees"),
					"Expected text is not displayed on modal header");
			Reporter.log("Included Taxes And Fees Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Included Taxes And Fees Modal is not present");
		}
		return this;
	}

	/**
	 * Click on Included taxes and fees modal close button.
	 *
	 */
	public ChargePlanPage clickInlcudedTaxesFeesModalCloseBtn() {
		try {
			includedTaxesAndFeesModalCloseBtn.click();
			Reporter.log("Clicked on included taxes and fees Modal close Button.");
		} catch (Exception e) {
			Assert.fail("Fail to click on included taxes and fees Modal close Button.");
		}
		return this;
	}

	public ChargePlanPage clickviewdetailslink() {
		try {
			checkPageIsReady();
			clickElement(plansViewDetails);
			Reporter.log("View Details link is clicked");
		} catch (Exception e) {
			Assert.fail("View Details link is not present");
		}
		return this;
	}

	/**
	 * Verify page elements
	 *
	 * @throws InterruptedException
	 */
	public ChargePlanPage verifyChargePlanPageElements() {
		try {
			Assert.assertTrue(backToSummaryLink.isDisplayed(), "Back To Summary link is not displayed");
			Assert.assertTrue(plansTabInSlider.isDisplayed(), "Equipment header is not displayed");
			Assert.assertTrue(arrowLeft.isDisplayed(), "Arrow left is not displayed");
			Assert.assertTrue(arrowRight.isDisplayed(), "Arrow right is not displayed");
			for (WebElement amount : amountBlock) {
				Assert.assertTrue(amount.getText().contains("$"), "Dollar sign is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Fail to verify Charge Plan page elements");
		}
		return this;
	}

	/**
	 * Click Back To Summary Link
	 * 
	 * @return
	 */
	public ChargePlanPage clickBackToSummaryLink() {
		try {
			backToSummaryLink.click();
		} catch (Exception e) {
			Assert.fail("Back to summary link is not displayed");
		}
		return this;
	}
}
