package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class PromotionApiV1 extends ApiCommonLib{
	
	/***
	* summary: Search for Promotions based on specified criteria.
	*      	description: >-
	*        Lookup Promotions based on multiple search criteria. This is used to
	*        drive marketing offers. The actual manifestation of promotional
	*        discounts is part of the products or carts operations.
	*      parameters:
	*        - $ref: '#/parameters/oAuth'
	*        - $ref: '#/parameters/transactionId'
	*        - $ref: '#/parameters/correlationId'
	*        - $ref: '#/parameters/applicationId'
	*        - $ref: '#/parameters/channelId'
	*        - $ref: '#/parameters/clientId'
	*        - $ref: '#/parameters/transactionBusinessKey'
	*        - $ref: '#/parameters/transactionBusinessKeyType'
	*        - $ref: '#/parameters/transactionType'
	*        - name: body
	*          in: body
	*          description: request containing the search criteria
	*          schema:
	*            $ref: '#/definitions/promoSearchCriteria'
	*  
	*  
	* @return
	* @throws Exception
	*/
    public Response search_promotions(ApiTestData apiTestData, String requestBody , Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildPromotionsHeader(apiTestData,tokenMap);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v1/promotions/browse",RestCallType.POST);
         System.out.println(response.asString());
         return response;
    }
    
    /***
    *	summary: Lookup for products pricing.
    *      description: >-
    *        Lookup for product pricing which can also deliver promotional price if
    *        promotion details are provided.
    *      parameters:
    *        - $ref: '#/parameters/oAuth'
    *        - $ref: '#/parameters/transactionId'
    *        - $ref: '#/parameters/correlationId'
    *        - $ref: '#/parameters/applicationId'
    *        - $ref: '#/parameters/channelId'
    *        - $ref: '#/parameters/clientId'
    *        - $ref: '#/parameters/transactionBusinessKey'
    *        - $ref: '#/parameters/transactionBusinessKeyType'
    *        - $ref: '#/parameters/transactionType'
    *        - name: body
    *          in: body
    *          description: Body of the request
    *          schema:
    *            $ref: '#/definitions/ProductsPrice'
    *          required: true
    *	 @return
    *	 @throws Exception
    */
    public Response promotions_prices(ApiTestData apiTestData, String requestBody , Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildPromotionsHeader(apiTestData,tokenMap);

    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v1/promotions/prices",RestCallType.POST);
         System.out.println(response.asString());
         return response;
    }
    
    private Map<String, String> buildPromotionsHeader(ApiTestData apiTestData ,Map<String, String> tokenMap ) throws Exception {
    //	String transactionType = "UPGRADE";
		//JWTTokenApi jwtTokenApi = new JWTTokenApi();
    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
    	//jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"));
		Map<String, String> headers = new HashMap<String, String>();
		/*if(StringUtils.isNoneEmpty(tokenMap.get("addaline"))){
			transactionType = "ADDALINE";
		}*/
    	headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("applicationid", "MYTMO");
		headers.put("cache-control", "no-cache");
		headers.put("channelid", "WEB");
		headers.put("clientid", "e-servicesUI");
		headers.put("Content-Type", "application/json");
		headers.put("correlationid", checkAndGetPlattokenJWT(apiTestData, jwtTokens.get("jwtToken")));
		headers.put("transactionbusinesskeytype", apiTestData.getBan());
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("transactionType", "ADDALINE");
		headers.put("usn", "12345");
	    headers.put("Authorization", jwtTokens.get("jwtToken"));
		headers.put("transactionBusinessKey", "BAN");
		headers.put("X-Auth-Originator", jwtTokens.get("jwtToken"));
		headers.put("transactionType","Tradein");
		headers.put("transactionId","Transactionid");
		return headers;
	}

}
