package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class CatalogApiV3 extends ApiCommonLib {

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, (description: 'ProductType value (PHONES, TABLETS,
	 * WEARABLES,HOTSPOTS) to return specific filters') -channelCode, -marketId.
	 * QueryParams:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getFilterOptionsUsingGET(ApiTestData apiTestData, String filter, String marketID,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		service.addQueryParam("productType", filter);
		service.addQueryParam("channelCode", "MYT");
		service.addQueryParam("marketId", marketID);
		Response response = service.callService("v3/catalog/deviceFilters", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, -channelCode, -marketId. QueryParams:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response retrieveProductsUsingPOST(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/catalog/products", RestCallType.POST);
		return response;
	}

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, -channelCode, -marketId. QueryParams:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response retrieveGroupedProductsUsingPOST(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/catalog/products/family", RestCallType.POST);
		return response;
	}

	/***
	 * Summary: Headers:Authorization, -transactionId, -transactionType,
	 * -applicationId, -channelId, -correlationId, -clientId,
	 * -transactionBusinessKey, -transactionBusinessKeyType, -usn, -phoneNumber,
	 * -dealerCode, -productType, -channelCode, -marketId. QueryParams:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response retrievePromotionsAtFamilyLevel(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData, tokenMap);

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/catalog/products/family/promotions", RestCallType.POST);
		return response;
	}

	private Map<String, String> buildGetCatalogHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		final String accessToken = createPlattoken(apiTestData);
		tokenMap.put("accessToken", accessToken);
		Map<String, String> headers = new HashMap<String, String>();

		headers.put("accept", "application/json");
		headers.put("applicationid", "MYTMO");
		headers.put("cache-control", "no-cache");
		headers.put("channelid", "WEB");
		headers.put("clientid", "ESERVICE");
		headers.put("correlationId", checkAndGetAccessToken());
		headers.put("content-type", "application/json");
		headers.put("transactionid", "xasdf1234asa4444");
		headers.put("transactionbusinesskeytype", apiTestData.getBan());
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("transactionType", "UPGRADE");
		headers.put("usn", "testUSN");
		headers.put("transactionBusinessKey", "BAN");
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid", "ShopAPITest");

		return headers;
	}

	private Map<String, String> buildGetCatalogHeader1(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		final String accessToken = createPlattoken(apiTestData);
		tokenMap.put("accessToken", accessToken);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("transactionType", "AAL");
		headers.put("applicationId", "MYTMO");
		headers.put("channelId", "WEB");
		headers.put("correlationId", accessToken);
		headers.put("clientId", "e-servicesUI");
		headers.put("transactionBusinessKey", apiTestData.getMsisdn());
		headers.put("transactionBusinessKeyType", "missdn");
		headers.put("usn", "123");
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("dealerCode", "000000");
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid", "ShopAPITest");
		return headers;
	}

}
