package com.tmobile.eservices.qa.pages.accounts.api;
import java.util.List;

import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSODF extends EOSCommonLib {






public Response getResponseODF(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	//requestbody
	   
	    
	    JSONObject requestParams = new JSONObject();
		requestParams.put("accountNumber",alist[3] );
		requestParams.put("msisdn", alist[0]);
		requestParams.put("dataServiceSoc", "ALL");
		requestParams.put("optionalServiceSocs", "ALL");
		requestParams.put("dataPassSoc", "ALL");
		requestParams.put("userRole", alist[4]);
		  
		RestService restService = new RestService(requestParams.toString(), eep.get(environment));
		
	//requestheaders	
		   restService.setContentType("application/json");
		    restService.addHeader("Authorization",alist[2]);
		    restService.addHeader("ban", alist[3]);
		    restService.addHeader("X-B3-TraceId", "3105181527058d4");
		    restService.addHeader("X-B3-SpanId", "3105181527058d4");
		    restService.addHeader("channel_id", "DESKTOP");
		    restService.addHeader("msisdn", "");
		    restService.addHeader("session-num", "47846366-09d9-44cf-96d3-c9b859d5ef04");
		    restService.addHeader("Phone-Number", alist[0]);
		    restService.addHeader("Cache-Control", "no-cache");
		    restService.addHeader("Postman-Token", "52f63e0d-b2fd-41c0-b461-c6f7e7c5bb44");
		
		
	    
	    response = restService.callService("v1/cpslookup/ondevicefulfillment",RestCallType.POST);
	    response.asString();
	}

	    return response;
}



public String getplansoc(Response response) {
	String plansoc=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("planSoc")!=null) {
        	return jsonPathEvaluator.get("planSoc").toString();
	}}
		
	
	return plansoc;
}

public String getcurrentdataservicedetailssoc(Response response) {
	String currentdataservicedetailssoc=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("currentDataServiceDetails.soc")!=null) {
        	return jsonPathEvaluator.get("currentDataServiceDetails.soc").toString();
	}}
		
	
	return currentdataservicedetailssoc;
}






public List<String> getallCurrentServiceDetails(Response response) {
	List<String> plansoc=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("allCurrentServiceDetails.soc")!=null) {
        	return jsonPathEvaluator.get("allCurrentServiceDetails.soc");
	}}
		
	
	return plansoc;
}








}
