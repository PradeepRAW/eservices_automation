package com.tmobile.eservices.qa.api.unlockdata;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author blakshminarayana
 *
 */
public final class ServiceUtils {
	private static final Logger logger = LoggerFactory.getLogger(ServiceUtils.class);
	private ServiceUtils() {
	}


	/**
	 * This method is load the property file from resources
	 * 
	 * @return property file
	 */
	private static Properties loadPropertyFile() {
		logger.info("called loadPropertyFile method in ServiceUtils");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = ServiceUtils.class.getClassLoader();
			InputStream inputStream = classLoader.getResourceAsStream(Constants.APPLICATION_PROPERTY_FILE);
			properties.load(inputStream);
		} catch (IOException e) {
			logger.error("Fail to load property file{}", e);
			throw new ServiceException("Fail to load property file{}", e);
		}
		return properties;
	}

	/**
	 * This method is able to return the getSubscriberProfileEndPointURL
	 * 
	 * @param environment
	 * @return
	 */
	public static String getPaymentServiceEndPointURL(String environment) {
		logger.info("called getSubscriberProfileEndPointURL method in ServiceUtils");
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.PAYMENT_SERVICE);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method is used to get the streaam by file name
	 * 
	 * @param pathname
	 * @return
	 */
	private static InputStream getStreamByFileName(StringBuilder pathname) {
		ClassLoader classLoader = CommonUtils.class.getClassLoader();
		return classLoader.getResourceAsStream(pathname.toString());
	}

	/**
	 * This method to load the updateAutoPay stream from class path.
	 * 
	 * @param xmlFileName
	 * @return
	 */
	public static InputStream getInputStream(String xmlFileName) {
		logger.info("called getInputStream method in ServiceUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(xmlFileName);
		return getStreamByFileName(pathname);
	}
}
