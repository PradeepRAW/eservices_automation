package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class TnCPage extends CommonPage {
	
	@FindBy(css = "div.container.tnc-modal")
	private WebElement tNcModal;
	
	@FindBy(css = ".h3-title.h4-title-mobile.text-bold")
	private WebElement tNcFaQHeader;
	
	@FindBy(css = "div.tnc-modal button")
	public WebElement backBtn;
	
	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");
	
	@FindBy(css = "i.fa.fa-spin.fa-spinner.interceptor-spinner")
	private WebElement pageSpinner;

	@FindBy(css = ".btn.btn-secondary.SecondaryCTA")
	private WebElement pageLoadElement;

	private final String pageUrl = "/termsAndConditions";
	
//	private final String pageUrlPA = "/tnc";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public TnCPage(WebDriver webDriver) {
		super(webDriver);
	}
	/**
     * Verify that current page URL matches the expected URL.
     */
    public TnCPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public TnCPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
    		waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
	    	waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("Terms and Conditions page not laoded");
    	}
        return this;
    }
    
	/**
	 * validate Autopay Terms and Conditions modal
	 */
	public TnCPage validateTnCPage() {
	    try{
	    	Verify.assertTrue(tNcModal.isDisplayed(), "Auto Pay Terms and Conditions modal is not displayed");
	    	Verify.assertTrue(tNcFaQHeader.getText().equals("Terms and Conditions"),
                    "Auto Pay Terms and Conditions header is not displayed");
	    	Verify.assertTrue(backBtn.isDisplayed(), "Back button is not displayed on TnC modal");
            Reporter.log("Autopay Terms & Conditions page successfully validated");
        }catch (Exception e){
        	Verify.fail("Fail to validate Terms and Conditions Model");
        }
		return this;
	}
	
	public TnCPage clickBackButton() {
	    try{
            waitFor(ExpectedConditions.elementToBeClickable(backBtn));
            clickElementWithJavaScript(backBtn);
            waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
            Reporter.log("Clicked on Back Button");
        }catch (Exception e){
        	Verify.fail("Fail to click on Back Button");
        }
		return this;
	}

}
