package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class SwitchToTmobilePage extends TmngCommonPage {
	private int timeout = 30;

	private final String pageLoadedText = "enter your current monthy phone charges";
	private final String pageUrl = "/switch-to-t-mobile";

	@FindBy(css = "a.call-now-phone.tmo_tfn_number")
	private WebElement _118776709172;

	@FindBy(css = "a.btn-call-now.tmo_tfn_number")
	private WebElement _218776709172;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	private WebElement accessories;

	@FindBy(css = "#5bff992be63a0cb5667dc239fffeb677743b6f1b div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div:nth-of-type(2) a.btn-back")
	private WebElement back1;

	@FindBy(css = "#5bff992be63a0cb5667dc239fffeb677743b6f1b div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div:nth-of-type(3) a.btn-back")
	private WebElement back2;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	private WebElement business2;

	@FindBy(css = "#switcherOBCall button")
	private WebElement callMeBack;

	@FindBy(css = "a.action.tmo_tfn_number")
	private WebElement callNow18776709172;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	private WebElement callUs;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	private WebElement checkOutTheCoverage;

	@FindBy(id = "switcher-email-confirmation")
	private WebElement confirmMail;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	private WebElement deals2;

	@FindBy(id = "carrierCost")
	private WebElement enterYourCurrentMonthyPhoneCharges;

	@FindBy(id = "switcher-email")
	private WebElement enterYourEmail;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	private WebElement espaol;

	@FindBy(id = "request-call-name")
	private WebElement fullName;

	@FindBy(css = "#switcherCostForm div:nth-of-type(2) button")
	private WebElement getYourResults;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	private WebElement internationalCalling;

	@FindBy(css = "#5bff992be63a0cb5667dc239fffeb677743b6f1b div:nth-of-type(4) div:nth-of-type(1) div:nth-of-type(5) div.step-wrap div:nth-of-type(3) a:nth-of-type(1)")
	private WebElement legal;

	@FindBy(css = ".marketing-page.switcher-page.ng-scope div:nth-of-type(4) header.global-header.light.static div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	private WebElement logIn;

	@FindBy(css = "button.hamburger.hamburger-border")
	private WebElement menuMenu;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile;

	@FindBy(id = "request-call-phone")
	private WebElement phoneNumber;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	private WebElement phones;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	private WebElement plans;

	@FindBy(css = ".marketing-page.switcher-page.ng-scope div:nth-of-type(4) header.global-header.light.static div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	private WebElement prepaid2;

	@FindBy(css = "a[href='https://www.t-mobile.com/company/website/privacypolicy.aspx']")
	private WebElement privacyPolicy;

	@FindBy(css = "a.action.request-call")
	private WebElement requestACallAvgWait;

	@FindBy(id = "searchText")
	private WebElement search1;

	@FindBy(id = "searchText")
	private WebElement search2;

	@FindBy(css = "a.tmo-modal-link")
	private WebElement seeFullTerms;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "#switcherEmailCaptureForm button")
	private WebElement sendMyPin;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	private WebElement shopPhones;

	@FindBy(css = "a[href='#divfootermain']")
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	private WebElement smartDevices;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	private WebElement stores;

	@FindBy(css = ".marketing-page.switcher-page.ng-scope div:nth-of-type(4) header.global-header.light.static nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.switcher-page.ng-scope div:nth-of-type(4) header.global-header.light.static div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch2;

	@FindBy(css = "#5bff992be63a0cb5667dc239fffeb677743b6f1b div:nth-of-type(2) span:nth-of-type(1) a:nth-of-type(1)")
	private WebElement switchNow;

	@FindBy(css = "a[href='https://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	private WebElement termsConditions;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "#maincontent div.switcher section:nth-of-type(1) div.question.q-one.active div:nth-of-type(2) div.q-headers.scrn-one div.q-header.qh-one.active p a")
	private WebElement vipExperience;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	private WebElement wellHelpYouJoin;

	@FindBy(id = "request-call-zipcode")
	private WebElement zipCode;

	public SwitchToTmobilePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Click on 18776709172 Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage click1Link18776709172() {
		_118776709172.click();
		return this;
	}

	/**
	 * Click on 18776709172 Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage click2Link18776709172() {
		_218776709172.click();
		return this;
	}

	/**
	 * Click on Accessories Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickAccessoriesLink() {
		accessories.click();
		return this;
	}

	/**
	 * Click on Back Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickBack1Link() {
		back1.click();
		return this;
	}

	/**
	 * Click on Back Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickBack2Link() {
		back2.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on Call Me Back Button.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickCallMeBackButton() {
		callMeBack.click();
		return this;
	}

	/**
	 * Click on Call Now 18776709172 Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickCallNow18776709172Link() {
		callNow18776709172.click();
		return this;
	}

	/**
	 * Click on Call Us Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Espanol Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Get Your Results Button.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickGetYourResultsButton() {
		getYourResults.click();
		return this;
	}

	/**
	 * Click on International Calling Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on Legal Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickLegalLink() {
		legal.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickLogInLink() {
		logIn.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickMyTmobileLink() {
		myTmobile.click();
		return this;
	}

	/**
	 * Click on Phones Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click on Plans Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Privacy Policy Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickPrivacyPolicyLink() {
		privacyPolicy.click();
		return this;
	}

	/**
	 * Click on Request A Call Avg. Wait 15 Min Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickRequestACallAvgWaitLink() {
		requestACallAvgWait.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSeeFullTermsLink() {
		seeFullTerms.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on Send My Pin Button.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSendMyPinButton() {
		sendMyPin.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickShopPhonesLink() {
		shopPhones.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Switch Now Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickSwitchNowLink() {
		switchNow.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on Vip Experience Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickVipExperienceLink() {
		vipExperience.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Set value to Confirm Mail Email field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setConfirmMailEmailField(String confirmMailValue) {
		confirmMail.sendKeys(confirmMailValue);
		return this;
	}

	/**
	 * Set value to Enter Your Current Monthy Phone Charges Number field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setEnterYourCurrentMonthyPhoneChargesNumberField(
			String enterYourCurrentMonthyPhoneChargesValue) {
		enterYourCurrentMonthyPhoneCharges.sendKeys(enterYourCurrentMonthyPhoneChargesValue);
		return this;
	}

	/**
	 * Set value to Enter Your Email Email field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setEnterYourEmailEmailField(String enterYourEmailValue) {
		enterYourEmail.sendKeys(enterYourEmailValue);
		return this;
	}

	/**
	 * Set value to Full Name Text field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setFullNameTextField(String fullNameValue) {
		fullName.sendKeys(fullNameValue);
		return this;
	}

	/**
	 * Set value to Phone Number Tel field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setPhoneNumberTelField(String phoneNumberValue) {
		phoneNumber.sendKeys(phoneNumberValue);
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}

	/**
	 * Set value to Zip Code Number field.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage setZipCodeNumberField(String zipCodeValue) {
		zipCode.sendKeys(zipCodeValue);
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Switch to Tmobile page loaded");
		} catch (NoSuchElementException e) {
			Assert.fail("Switch to Tmobile Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the SwitchToTmobilePage class instance.
	 */
	public SwitchToTmobilePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}
}
