package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Gopimanohar
 *
 */
public class JodDevicePaymentPage extends CommonPage {

	
	@FindBy(css = ".ajax_loader")
	private WebElement pageSpinner;
	
	@FindBy(css = "h4.text-gray-dark")
	private WebElement pageHeader;
	
	
	@FindBy(css = "h4.text-gray-dark")
	private WebElement pageLoadElement;
	
	
	@FindBy(css="button#cancelBackToLeases")
	private WebElement btnCancelandlease;

	@FindBy(css = "div[ng-show='!$ctrl.showbillingform'].padding-top-large div")
	private WebElement billingAddress;
	
	
	@FindBy(css = "div.pull-left>p>span.ng-binding")
	private List<WebElement> piiElements;
	
	
	@FindBy(css ="div.pull-left>p>span.ng-binding:last-child")
	private WebElement phoneNumber;
	
	/*@FindBy(css ="div.pull-left.p-t-15>p.small>span:nth-child(1)")
	private WebElement customerFirstName;
	
	@FindBy(css ="div.pull-left.p-t-15>p.small>span:nth-child(2)")
	private WebElement customerLastName;*/
	
	@FindBy(css = "div[ng-show*='ctrl.showbillingform'] div p:first-child")
	private WebElement billingAddressStreet;
	
	@FindBy(css = "div[ng-show*='ctrl.showbillingform'] div p span")
	private List<WebElement> billingAddressCityStateZip;
	
	String maskedElement = null;
	
	private final String pageUrl = "JOD/device-payment";
	
	
	
	
	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public JodDevicePaymentPage(WebDriver webDriver) {
		super(webDriver);
	}	
	
	
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public JodDevicePaymentPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public JodDevicePaymentPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
    		//waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
    		waitFor(ExpectedConditions.visibilityOf(btnCancelandlease));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			Reporter.log("JUMP! On Demand Device Payment Details Page is loaded.");
			//Assert.assertTrue(pageHeader.getText().contains("JUMP! On Demand Device Payment Details Page"), "Page header not found");
    	}catch(Exception e){
    		Assert.fail("JUMP! On Demand Device Payment Details Page- page is not loaded");
    	}
        return this;
    }
    
    
    /**
	 * Verify PII details for PhoneNumber
	 */
    
    public void verifyPIIforPhoneNumber(String misdin) {
    	boolean ismasked=false;
		try {
			
			for(int i=0;i<=piiElements.size()-1;i++){
				ismasked = comparePidandIndex(piiElements.get(i),misdin);
				if (ismasked) {
					break;
				}
			}
			Assert.assertTrue(ismasked,"PII Masking is not applied for Phone Number");
			Reporter.log("PII Masking for Phone Number verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the Phone Number ");
		}	
	}
    
  
	
	
	 /**
		 * Verify PII details for CustomerFirstName
		 */
		public void verifyPIIforCustomerFirstName(String CustomerFirstName) {
		
			boolean ismasked=false;
				try {
					
					for(int i=0;i<=piiElements.size()-1;i++){
						ismasked = comparePidandIndex(piiElements.get(i),CustomerFirstName);
						if (ismasked) {
							break;
						}
					}
					Assert.assertTrue(ismasked,"PII Masking is not applied for Customer First Name");
					Reporter.log("PII Masking for Customer First Name verified Successfully");
				} catch (Exception e) {
					Assert.fail("Failed to verify PII Masking for the Customer First Name ");
				}	
			}
			
		
		
		 /**
		 * Verify PII details for CustomerLastName
		 */
		public void  verifyPIIforCustomerLastName(String CustomerLastName) {
			
			boolean ismasked=false;
			try {
				
				for(int i=0;i<=piiElements.size()-1;i++){
					ismasked = comparePidandIndex(piiElements.get(i),CustomerLastName);
					if (ismasked) {
						break;
					}
				}
				Assert.assertTrue(ismasked,"PII Masking is not applied for Customer Last Name");
				Reporter.log("PII Masking for Customer Last Name verified Successfully");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Customer Last Name ");
			}	
		}
		
    
		 /**
		 * Verify PII details for Billing Address/911 Address/Shipping Address
		 */
		public JodDevicePaymentPage verifyPIIforBillingAddress(String BillingAddress) {
			try {
					
				Assert.assertTrue(comparePidandIndex(billingAddress,BillingAddress));
				Reporter.log("PII Masking for Customer BillingAddress verified Successfully ");
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Customer Billing Address");
			}
			return this;
		}	
		
	/**
	 * Verify Masked Element for MSISDN
	 */
	public JodDevicePaymentPage verifyMsisdnMaskElement(String mask) {
		try {
			maskedElement = getAttribute(phoneNumber, "class");
			Assert.assertTrue(maskedElement.contains(mask), "Class for MSISDN element doesnt contain Masked value");
		} catch (Exception e) {
			Assert.fail("Failed to verify MSISDN masked element in Payment tab");
		}
		return this;
	}
	
	/**
	 * Verify Masked Element for street address
	 */
	public JodDevicePaymentPage verifyStreetAddressMaskElement(String mask) {
		try {
			maskedElement = getAttribute(billingAddressStreet, "class");
			Assert.assertTrue(maskedElement.contains(mask), "Class for Street element doesnt contain Masked value");
		} catch (Exception e) {
			Assert.fail("Failed to verify Street masked element in Payment tab");
		}
		return this;
	}
	
	/**
	 * Verify Masked Element for Customer Name line
	 * 
	 */
	public JodDevicePaymentPage verifyMaskedElementForCityStateZip(String mask) {
		try {
			waitFor(ExpectedConditions.visibilityOf(billingAddressCityStateZip.get(0)));
			for (int i = 0; i < billingAddressCityStateZip.size(); i++) {
				if (billingAddressCityStateZip.get(i).isDisplayed()) {
					maskedElement = getAttribute(billingAddressCityStateZip.get(i), "class");
					Assert.assertTrue(maskedElement.contains(mask),
							"Class for Address number " + i + " element doesnt contain Masked value");
				}
			}
			Reporter.log("City, State, Zip elements contain masked parameter");
		} catch (Exception e) {
			Assert.fail("Failed to verify City, State, Zip Masked elements");
		}
		return this;
	}
			    
	
}