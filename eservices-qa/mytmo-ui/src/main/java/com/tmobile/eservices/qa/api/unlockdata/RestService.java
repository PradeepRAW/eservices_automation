package com.tmobile.eservices.qa.api.unlockdata;

import javax.ws.rs.client.Client;

/**
 * This class represents the token from soap
 * 
 * @author blakshminarayana
 */
public interface RestService {

	String getAccessToken(RequestData requestData);

	boolean sendPinToMsisdn(RequestData requestData);
	
	boolean changePassword(RequestData requestData);
	
	
	Response getProfile(RequestData requestData);

	Response deleteProfile(RequestData requestData);

	boolean unlockAccount(RequestData requestData);

	String getDecryption(RequestData requestData);
	
	Client createRestClient();
	
}
