/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rsuresh
 *
 */
public class PhoneSelectionPage extends CommonPage {

	@FindBy(css = "p.body-copy-highlight")
	private WebElement selectDevice;

	@FindBy(css = "button[ng-click*='ctrl.GotoTradeInAnotherDevicePage']")
	private WebElement gotADifferentPhone;

	public PhoneSelectionPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify Page URL
	 * 
	 * @return
	 */
	public PhoneSelectionPage verifyPhoneSelectionPage() {
		checkPageIsReady();
		try {
			getDriver().getCurrentUrl().contains("phoneselection");
			Reporter.log("Verified Phone Selection page");
		} catch (Exception e) {
			Assert.fail("Phone Selection page is not displayed");
		}
		return this;
	}

	/**
	 * Select Device
	 */
	public PhoneSelectionPage selectDevice() {
		waitforSpinner();
		try {
			clickElementWithJavaScript(selectDevice);
		} catch (Exception e) {
			Assert.fail("Device is not Displayed to click");
		}
		return this;
	}

	/**
	 * Click On Got A Different Phone
	 * 
	 * @return
	 */
	public PhoneSelectionPage clickGotADifferentPhone() {
		checkPageIsReady();
		waitforSpinner();
		try {
			moveToElement(gotADifferentPhone);
			gotADifferentPhone.click();
			Reporter.log("Got a different link is clickable");
		} catch (Exception e) {
			Assert.fail("Got a different link is not clickable");
		}
		return this;
	}

}
