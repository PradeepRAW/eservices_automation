package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class LeaseDetailsPage extends CommonPage {
	
	@FindBy(css = ".ajax_loader")
	private WebElement pageSpinner;

	@FindBy(css = "#review-device-title")
	private WebElement pageLoadElement;

	@FindBy(css = "h6.text-gray-dark")
	private WebElement pageHeader;

	@FindBy(css = "div.leaseinfo:not([class*='hide']) span.msisdn")
	private List<WebElement> lineMobileNumbers;
	
	@FindBy(css ="div.leaseinfo:not([class*='hide']) p.imei")
	private List<WebElement> lineImeiNumber;
	
	@FindBy(css = "div.leaseinfo:not([class*='hide']) span.linename")
	private List<WebElement> lineCustomerName;
	
	@FindBy(css ="div.in[id*='active'] button.btn-primary")
	private WebElement getInstallmentButton;
	
	
	@FindBy(css="div#dropdownTitle")
	private WebElement leaseDropDown;
	
	@FindBy(css="ul.dropdown-menu>li>a")
	private List<WebElement> leaseDropDownList;
	
	private final String pageUrl = "/lease-details.html";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public LeaseDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public LeaseDetailsPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public LeaseDetailsPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
    		waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
	    	//waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			Assert.assertTrue(pageHeader.getText().contains("JUMP! On Demand Lease Details"), "Page header not found");
    	}catch(Exception e){
    		Assert.fail("JUMP! On Demand Lease Details - page not found.");
    	}
        return this;
    }

    

	/**
	 * verify PII Details for IMEI in Lease Details Page
	 */
	public void verifyPIIImeiNumber(String imeiNumber) {
		
		try {
			for (WebElement line : lineImeiNumber) {
				Assert.assertTrue(checkElementisPIIMasked(line, imeiNumber),"No PII masking for IMEI Number");
				}
			Reporter.log("PII Masking for IMEI Number verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the IMEI Number");
		}
		
	}
		
 
	
	/**
	 * verify PII Details for PhoneNumner in Lease Details Page
	 */
	public void verifyPIIPhoneNumber(String phoneNumber) {
		
		try {
			for (WebElement line : lineMobileNumbers) {
				Assert.assertTrue(checkElementisPIIMasked(line, phoneNumber),"No PII masking for Mobile Number");
				}
			Reporter.log("PII Masking for Mobile Number verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the Mobile Number");
		}
		
	}
		
		

	
	
	/**
	 * verify PII Details in Lease Details Page
	 */
	public void verifyPIICustomerName(String customerName) {
		
		try {
			for (WebElement line : lineCustomerName) {
				Assert.assertTrue(checkElementisPIIMasked(line, customerName),"No PII masking for Customer Name");
				}
			Reporter.log("PII Masking for Customer Name verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the Customer Name");
		}
		
	}
		
	
	/**
	 * Click 'Get InstallMent Plan' Button in Lease Details Page
	 */
	public void clickGetInstallMentPlan() {
		try {
			getInstallmentButton.click();
			Reporter.log("Clicked 'Get InstallMentPlan' Button on Lease Details Page");
		} catch (Exception e) {
			Assert.fail("Get InstallMentPlan Button is not found in Lease details Page");
		}
	}
	
	
	/**
	 * Click 'Lease Drop Down'  in Lease Details Page
	 */
	public void clickLeaseDropDown() {
		try {
			leaseDropDown.click();
			Reporter.log("Clicked 'Lease DropDown'  on Lease Details Page");
		} catch (Exception e) {
			Assert.fail("Lease DropDown is not found in Lease details Page");
		}
	}
	
	
	 /**
	 * Verify PII details for Lines in Lease DropDown
	 */
	public void verifyPIIforLinesInLeaseDropDownList(String customerMobileNumbers) {
	
		boolean ismasked=false;
			try {
				
				for(int i=0;i<leaseDropDownList.size();i++){
					if (!leaseDropDownList.get(i).getText().contains("All leases")) {
						ismasked = comparePidandIndex(leaseDropDownList.get(i),customerMobileNumbers);
						Assert.assertTrue(ismasked,"PII Masking is not applied for Lines in Lease DropDown List");
						Reporter.log("PII Masking for Lines in Lease DropDown List  verified Successfully");
						if(i==leaseDropDownList.size()-1){
							leaseDropDownList.get(leaseDropDownList.size()-i).click();
							Reporter.log("Lease Dropdown Selected successfully");
						}
					}
				}
				
				
			} catch (Exception e) {
				Assert.fail("Failed to verify PII Masking for the Lines in Lease DropDown List ");
			}	
		}
	

	
	  
    /**
	 * Verify PII details for Lease Drop Down
	 */
	public void verifyPIIforLeaseDropDown(String misdin) {
			
		try {
			Assert.assertTrue(checkElementisPIIMasked(leaseDropDown, misdin),"No PII masking for Selected Line");
			Reporter.log("PII Masking for Lease Drop Down verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the Lease DropDown");
		}
		
	}
		

}
