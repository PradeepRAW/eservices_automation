package com.tmobile.eservices.qa.pages.payments;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class EIPDetailsPage extends CommonPage {

	@FindBy(xpath = "//div[contains(text(), 'Installment Plans')]")
	private WebElement verifyEIPDetailsPage;

	@FindAll({ @FindBy(id = "di_makePaymentLink0"), @FindBy(id = "di_image-payment-link00") })
	private WebElement makePayment;

	@FindBy(css = "img#di_deviceImg0")
	private WebElement waitMakePayment;

	@FindBy(linkText = "account activities")
	private WebElement accountActivities;

	@FindBy(css = "#di_eipBody .ui_headline.mt20")
	private WebElement documentsAndReceipts;

	@FindAll({ @FindBy(css = "div.ui_mobile_headline"), @FindBy(css = "div.pagetitle_txt span.ui_headline") })
	private WebElement eipDevicePaymentHeader;

	@FindBy(id = "documentreceipt")
	private WebElement documentsAndReceiptsMobile;

	@FindAll({ @FindBy(css = "p#new-credit_p >input"), @FindBy(id = "new-credit") })
	private WebElement newCardRadioBtn;

	@FindAll({ @FindBy(css = "#di_PlanBody00 > table > tbody > tr:nth-child(1) > td:nth-child(1)"),
			@FindBy(id = "di_planNumberLabel0") })
	private WebElement eipPlanIdOnEipDetailsPage;

	@FindAll({ @FindBy(css = "#eipDetails > div.ui_mobile_body.pt20.co_lineHeight12 > span.ui_mobile_body_bold"),
			@FindBy(id = "planid_para") })
	private WebElement eipPlanIdOnEIPDevicePaymentPage;

	@FindAll({ @FindBy(id = "di_eiptitle0"), @FindBy(id = "di_eipestimatelink00") })
	private WebElement eipEstimator;

	@FindAll({ @FindBy(id = "estimator-EIP0"), @FindBy(id = "estimator-EIP") })
	private WebElement eipEstimatordialogBox;

	@FindAll({ @FindBy(id = "di_makePaymentLink0"), @FindBy(id = "di_image-payment-link00") })
	private WebElement makePaymentlink;

	@FindAll({
			@FindBy(css = "#easypay-options-div > div.row-fluid.confirmation-border-bottom.co_PadTop25Btm25 > div.span12.PaddingLeftRight.stolen-page-div > div.overflow > div > div"),
			@FindBy(css = "div.pagetitle_wrap.co_pagetitle") })
	private WebElement extraDevicepaymentHeader;

	@FindBy(id = "mybill_dollar")
	private WebElement paymentAmounttxtBox;

	@FindAll({ @FindBy(id = "di_amount0"), @FindBy(id = "enteredAmount") })
	private WebElement eipPaymentAmounttxtBox;

	@FindBy(id = "saved-credit")
	private WebElement savedCreditcard;

	@FindBy(id = "cc-next")
	private WebElement nextButton;

	@FindAll({
			@FindBy(css = "#usagepage > div > div.parsys.contentarea > div > div.co_eipPaymentCC > div > div.row-fluid > div.span12 > div.PaddingLeftRight.maElement > p.ui_mobile_headline"),
			@FindBy(css = "div.co_genericCCpayment") })
	private WebElement reviewDevicepayemntPage;

	@FindBy(id = "paymentreviewcredit")
	private WebElement termsAndconditions;

	@FindBy(id = "cc-submit-btn")
	private WebElement submitBtn;

	@FindAll({ @FindBy(css = "div#di_completeeip .pull-left.ui_mobile_headline"),
			@FindBy(css = "#di_eipBody > div.ui_headline.comfirmation-message-padding") })
	private WebElement completedPlans;

	@FindBy(id = "di_viewcompplans")
	private WebElement viewCompletedPlansLink;

	@FindBy(css = "div.showDetail span[id*='di_financeType0']")
	private WebElement installmentPlanType;

	@FindBy(id = "di_financeTypeLabel00")
	private WebElement installmentPlanTypeMobile;

	@FindBy(css = "div.showDetail span[id*='di_planStatus0']")
	private WebElement planStatus;

	@FindBy(id = "di_planStatus00")
	private WebElement planStatusMobile;

	@FindBy(css = "div.showDetail span[id*='di_planNumberLabel0']")
	private WebElement eipPlanIDAttribute;

	@FindBy(css = "div.activeTab[id^='di_activePlanHeading']")
	private List<WebElement> eipPlanIDNumbers;

	@FindBy(css = "div[id^='di_activePlanHeading']")
	private List<WebElement> activePlans;

	@FindBy(css = "div[id^='di_activePlanHeading']")
	private List<WebElement> totalPlans;

	@FindBy(css = "span.ui_body[id^='di_planNumber']")
	private List<WebElement> eipPlanIDs;

	@FindBy(css = "div#di_PlanBody00 tr:nth-child(1) td.ui_mobile_body.btmborder:nth-of-type(1)")
	private WebElement eipPlanIDMobile;

	@FindBy(id = "di_totalplanbalance")
	private WebElement planBalanceAmount;

	@FindAll({ @FindBy(id = "di_estimatebtn0"), @FindBy(id = "estimateBtn") })
	private WebElement estiamteBtn;

	@FindAll({ @FindBy(id = "di_paymentlink0"), @FindBy(id = "payThisAmountBtn") })
	private WebElement payThisAmountLink;

	@FindBy(css = "a.remove-payment")
	private WebElement removeStoredPaymentLink;

	@FindBy(id = "removeCredit-modal")
	private WebElement removeStoredPaymentModal;

	@FindBy(css = ".ajax_loader")
	private WebElement pageSpinner;

	@FindAll({ @FindBy(css = "#di_containercurve > div.ui_headline"),
			@FindBy(css = "#di_activeeip_body div div.ui_mobile_headline") })
	private WebElement pageLoadElement;

	@FindBy(css = "div.pb25.span12 span[id^='di_imei'].ui_body")
	private List<WebElement> imeinumber;

	@FindBy(css = "span[id^='di_msisdn']")
	private List<WebElement> phonenumber;

	private final String pageUrl = "/billing/eipdetails.html";

	/**
	 * 
	 * @param webDriver
	 */
	public EIPDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EIPDetailsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify EIP Details Page
	 *
	 * @return
	 */
	public EIPDetailsPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ajax_loader")));
			verifyPageUrl();
			Reporter.log("EIP details page is loaded");
		} catch (Exception e) {
			Assert.fail("Fail to load EIP details page");
		}
		return this;
	}

	/**
	 * Click Balance Radio Button
	 */
	public EIPDetailsPage clickNewRadioButton() {
		try {
			if (!newCardRadioBtn.isSelected() && newCardRadioBtn.isDisplayed()) {
				newCardRadioBtn.click();
				Reporter.log("Clicked on new radio button");
			}

		} catch (Exception e) {
			Assert.fail("New radio button not found");
		}
		return this;
	}

	/**
	 * Click Make Payment
	 */
	public EIPDetailsPage clickMakePayment() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(makePayment));
			moveToElement(makePayment);
			clickElementWithJavaScript(makePayment);
			Reporter.log("Clicked on make payments");
		} catch (Exception e) {
			Assert.fail("Make payments not found");
		}
		return this;
	}

	/**
	 * This method is able to click the account activities and navigate to the
	 * page
	 */
	public EIPDetailsPage clickAccountActivities() {
		try {
			accountActivities.click();
			Reporter.log("Account activities clicked");
		} catch (Exception e) {
			Verify.fail("Account activities not found");
		}
		return this;
	}

	/**
	 * Verify Documents And Receipts
	 * 
	 * @return
	 */
	public EIPDetailsPage verifyDocumentsAndReceipts() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				documentsAndReceiptsMobile.isDisplayed();
			} else {
				documentsAndReceipts.isDisplayed();
			}
			Reporter.log("Document and Receipts verified");
		} catch (Exception e) {
			Verify.fail("Document and Receipts not found");
		}
		return this;
	}

	/**
	 * Verify Account Activities
	 * 
	 * @return
	 */
	public EIPDetailsPage verifyAccountActivities() {
		try {
			accountActivities.isDisplayed();
			Reporter.log("Verified Account activities");
		} catch (Exception e) {
			Verify.fail("Account activities not found");
		}
		return this;
	}

	/**
	 * Click EIP Estimator Link
	 */
	public EIPDetailsPage clickEIPestimator() {
		try {
			moveToElement(eipEstimator);
			clickElementWithJavaScript(eipEstimator);
			Reporter.log("Clicked on EIP estimator");
		} catch (Exception e) {
			Assert.fail("EIP Estimator not found");
		}
		return this;
	}

	/**
	 * Verify EIP Estimator dialog box is displayed or not
	 * 
	 * @return
	 */
	public EIPDetailsPage verifyEIPEstimatordialogBox() {
		try {
			waitFor(ExpectedConditions.visibilityOf(eipEstimatordialogBox));
			eipEstimatordialogBox.isDisplayed();
			Reporter.log("EIP estimation dialog box verified");
		} catch (Exception e) {
			Verify.fail("EIP Estimation dialog box not found");
		}
		return this;
	}

	/**
	 * CLick Make Payment link
	 */
	public EIPDetailsPage clickMakepayment() {
		try {
			waitFor(ExpectedConditions.visibilityOf(makePaymentlink));
			makePaymentlink.click();
			Reporter.log("Clicked on make payment");
		} catch (Exception e) {
			Assert.fail("Make payment button not found");
		}
		return this;
	}

	/**
	 * Enter the Payment Amount
	 * 
	 * @param value
	 */
	public EIPDetailsPage enterPaymentamount(String value) {
		try {
			waitforSpinner();
			Double amount = generateRandomAmount();
			paymentAmounttxtBox.sendKeys(amount.toString());
			Reporter.log("Payment amount entered");
		} catch (Exception e) {
			Assert.fail("Payment entry field not found");
		}
		return this;
	}

	/**
	 * Enter the Payment Amount in EIP estimator
	 * 
	 * @return
	 */
	public Double enterEIPPaymentamount() {
		Double amount = null;
		checkPageIsReady();
		try {
			amount = generateRandomAmount();
			DecimalFormat df = new DecimalFormat("#.00");
			eipPaymentAmounttxtBox.sendKeys(df.format(amount));
			df.format(amount);
			Reporter.log("EIP amount entered");
		} catch (Exception e) {
			Verify.fail("EIP payment amount not found");
		}
		return amount;
	}

	/**
	 * generate a unique amount value
	 * 
	 * @return double - amount
	 */
	private Double generateRandomAmount() {
		double min = 1.01;
		double max = 2;
		Random r = new Random();
		return (r.nextInt((int) ((max - min) * 10 + 1)) + min * 10) / 10.0;
	}

	/**
	 * Click Next button
	 */
	public EIPDetailsPage clickNextbtn() {
		try {
			nextButton.click();
			Reporter.log("Clicked on next button");
		} catch (Exception e) {
			Assert.fail("Next button not found");
		}
		return this;
	}

	/**
	 * Verify Review Device payement page is displayed
	 * 
	 * @return
	 */
	public EIPDetailsPage verifyReviewdevicepaymentDisplayed() {
		try {
			checkPageIsReady();
			reviewDevicepayemntPage.isDisplayed();
			Reporter.log("Verified Review device Payment details");
		} catch (Exception e) {
			Assert.fail("Review payment details not found");
		}
		return this;
	}

	/**
	 * Verify Completed plan is displayed or not
	 * 
	 * @return
	 */
	public EIPDetailsPage verifyCompletedplandDisplayed() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				viewCompletedPlansLink.click();
				Reporter.log("Clicked on view complete plans Mobile");
			} else {
				completedPlans.isDisplayed();
				Reporter.log("Verified completed plan id");
			}
		} catch (Exception e) {
			Verify.fail("Completed plan id not found");
		}
		return this;
	}

	/**
	 * Verify Plan status
	 */
	public EIPDetailsPage verifyPlanStatusText(String toVerify) {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(planStatusMobile));
				planStatusMobile.getText().contains(toVerify);
			} else {
				waitFor(ExpectedConditions.visibilityOf(planStatus));
				planStatus.getText().contains(toVerify);
			}
			Reporter.log("Plan status text displayed");
		} catch (Exception e) {
			Verify.fail("Plan status text not found");
		}
		return this;
	}

	/**
	 * Verify EIP PlanId
	 */
	public EIPDetailsPage verifyEIPplanId() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				eipPlanIDMobile.getText().isEmpty();
			} else {
				eipPlanIDAttribute.getText().isEmpty();
			}
			Reporter.log("Verified EIP Plan ID");
		} catch (Exception e) {
			Verify.fail("Eip plan id not found");
		}
		return this;
	}

	/**
	 * Verify Plan Balance Amount
	 */
	public EIPDetailsPage verifyPlanBalanceAmount() {
		try {
			planBalanceAmount.getText().isEmpty();
			Reporter.log("Verified plan balance amount");
		} catch (Exception e) {
			Verify.fail("Plan balance field not found");
		}
		return this;
	}

	/**
	 * verify EIP plan ID is displayed
	 * 
	 * @return value
	 */
	public EIPDetailsPage verifyEIPPlanIdDisplayed() {
		try {
			if (eipPlanIdOnEipDetailsPage.isDisplayed()) {
				eipPlanIdOnEipDetailsPage.getText().equals("EIP PLAN ID:");
			}
			Reporter.log("Verified EIP plan ID");
		} catch (Exception elemNotFound) {
			Assert.fail("'EIP PLAN ID:' is not displayed");
		}
		return this;
	}

	/**
	 * Verify EIP Device Payment Page
	 * 
	 * @return
	 */

	public EIPDetailsPage verifyEIPDevicePaymentPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			getDriver().getCurrentUrl().contains("eippayment");
			Reporter.log("EIP payment page displayed");
		} catch (Exception e) {
			Assert.fail("EIP payment page not found");
		}
		return this;
	}

	/**
	 * click estimate button
	 */
	public EIPDetailsPage clickEstimateBtn() {
		try {
			estiamteBtn.click();
			Reporter.log("Clicked on Estimate button");
		} catch (Exception e) {
			Verify.fail("Estimate button not found");
		}
		return this;
	}

	/**
	 * click pay this amount link
	 */
	public EIPDetailsPage clickPayThisAmountLink() {
		try {
			payThisAmountLink.click();
			Reporter.log("Clicked on pay this amount link");
		} catch (Exception e) {
			Verify.fail("pay this amount link not found");
		}
		return this;
	}

	/**
	 * verify EIP payment amount
	 * 
	 * @param amount
	 * @return boolean
	 */
	public EIPDetailsPage verifyEIPPaymentAmount(Double amount) {
		try {
			checkPageIsReady();
			String paymntAmount = paymentAmounttxtBox.getAttribute("value").replace("$", "");
			paymntAmount.equals(amount);
			Reporter.log("EIP Amount on the Review Payment screen displayed");
		} catch (Exception e) {
			Verify.fail("EIP payment amount not matched");
		}
		return this;
	}

	public EIPDetailsPage clickViewCompletedPlansMobile() {
		try {
			viewCompletedPlansLink.click();
			Reporter.log("Clicked on view complete plans Mobile");
		} catch (Exception e) {
			Assert.fail("View completed plans not found");
		}
		return this;
	}

	/**
	 * verify and click Remove link for Stored Payment Link
	 */
	public void verifyRemoveStoredPaymentLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(removeStoredPaymentLink));
			removeStoredPaymentLink.click();
			Reporter.log("Clicked on Remove link for Stored payment link");
		} catch (Exception e) {
			Assert.fail("Remove link for Stored payment link is not found");
		}
	}

	/**
	 * verify Remove Stored Payment Modal is displayed
	 */
	public void verifyRemoveStoredPaymentModal() {
		try {
			removeStoredPaymentModal.isDisplayed();
			Reporter.log("Remove Stored Payment model displayed");
		} catch (Exception e) {
			Assert.fail("Remove Stored payment modal is not displayed");
		}
	}

	/**
	 * verify EIP Plan ID Number is from 14 to 18 chars long
	 */

	public void verifyEipPlanIDNumber() {
		try {
			for (WebElement webElement : totalPlans) {
				if (!webElement.getAttribute("class").contains("activeTab")) {
					webElement.click();
				}
			}
			waitFor(ExpectedConditions.visibilityOfAllElements(eipPlanIDs));
			for (WebElement planID : eipPlanIDs) {
				if (planID.getText().length() >= 14 && planID.getText().length() <= 18) {
					Reporter.log("EIP Plan ID Number " + planID.getText() + " contains " + planID.getText().length()
							+ " chars");
				} else {
					Assert.fail("EIP Plan ID Number " + planID.getText() + " is out of limits 14 to 18 chars");
				}
			}
			Reporter.log("All EIP Plan ID Numbers are in limits 14 to 18 chars");
		} catch (Exception e) {
			Assert.fail("Failed to verify IEP Plan ID Number");
		}
	}

	public EIPDetailsPage checkpromotionlabel(String devid) {
		try {
			if (getDriver().findElement(By.cssSelector("#di_promoNameLabel" + devid)).getText()
					.equalsIgnoreCase("PROMOTION:"))
				Reporter.log("PROMOTION: label is displaying");
		} catch (Exception e) {
			Assert.fail("PROMOTION: label is not displaying");
		}
		return this;
	}

	public EIPDetailsPage checkmonthlyCreditsRemaininglabel(String devid) {
		try {
			if (getDriver().findElement(By.cssSelector("#di_monthlyCreditsRemainingLabel" + devid)).getText()
					.equalsIgnoreCase("MONTHLY CREDITS REMAINING:"))
				Reporter.log("MONTHLY CREDITS REMAINING: lable is displaying");
		} catch (Exception e) {
			Assert.fail("MONTHLY CREDITS REMAINING: lable is not displaying");
		}
		return this;
	}

	public EIPDetailsPage checkamountOfMonthlyCreditsRemainingLabel(String devid) {
		try {
			if (getDriver().findElement(By.cssSelector("#di_amountOfMonthlyCreditsRemainingLabel" + devid)).getText()
					.equalsIgnoreCase("AMOUNT OF MONTHLY CREDIT:"))
				Reporter.log("AMOUNT OF MONTHLY CREDIT: lable is displaying");
		} catch (Exception e) {
			Assert.fail("AMOUNT OF MONTHLY CREDIT:  lable is not displaying");
		}
		return this;
	}

	/**
	 * verify PII Details for IMEI Number
	 */
	public void verifyPIIImeiNumber(String imeiNumber) {
		try {
			for (WebElement line : imeinumber) {
				Assert.assertTrue(checkElementisPIIMasked(line, imeiNumber), "No PII masking for Imei number");
			}
			Reporter.log("PII Masking for IMEI Number verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the IMEI Number");
		}

	}

	/**
	 * verify PII Details for PhoneNumner
	 */
	public void verifyPIIPhoneNumber(String phoneNumber) {
		try {
			for (WebElement line : phonenumber) {
				Assert.assertTrue(checkElementisPIIMasked(line, phoneNumber), "No PII masking for Msisdn");
			}
			Reporter.log("PII Masking for Mobile Number verified Successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII Masking for the Mobile Number");
		}
	}
}
