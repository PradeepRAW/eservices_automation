package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class AccountOverviewPage extends CommonPage {

	// Buttons & Headers
	@FindAll({ @FindBy(css = "span.H4-heading"), @FindBy(css = "div[class*='no-padding H6-heading']") })
	private List<WebElement> planNameText;

	@FindAll({ @FindBy(css = ".ui_headline.planHeadline"),
			@FindBy(css = "div#PlanServicesDiv_id div.ui_mobile_headline") })
	private WebElement planPageHeader;

	@FindAll({ @FindBy(css = "div.Display3"), @FindBy(css = "div.Display4") })
	private WebElement accountHeader;

	@FindBy(css = "div.Display3 div")
	private WebElement banHeader;

	@FindBy(css = "*.Display5")
	private List<WebElement> accountOfheaders;

	@FindBy(xpath = "//button[contains(text(),'Change Plan')]")
	private WebElement changePlanCTAOnAccountOverViewPage;

	@FindBy(xpath = "//button[contains(text(),'Plan page')]")
	private WebElement plansPageCTA;

	@FindBy(xpath = "//*[contains(@class,'padding-horizontal-small Display3')]")
	private WebElement textAccountOnAccountOverviewPage;

	@FindBy(id = "banInput")
	private WebElement banTextField;

	@FindBy(xpath = "//button[contains(text(),'SUBMIT')]")
	private WebElement submitButton;

	@FindBy(xpath = "(//*[@class='font-weight-bold'])[1]")
	private WebElement monthlyTotalCost;

	@FindBy(css = "div.H6-heading")
	private List<WebElement> monthlyTotalDetails;

	@FindBy(css = "span.H4-heading")
	private WebElement planName;

	@FindBy(css = "img.phone-image")
	private List<WebElement> lineImgs;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement addALine;

	@FindBy(css = "p[pid='cust_firstN']")
	private List<WebElement> lineNameByText;

	@FindBy(css = "p[pid='cust_msisdnN']")
	private List<WebElement> lineNameByMisdn;

	// alert

	@FindBy(css = "p.legal.text-dark-pink")
	private WebElement alert;

	// line details

	@FindBy(css = ".Display6")
	private WebElement lineName;

	@FindBy(css = "p[x-ms-format-detection='none']")
	private List<WebElement> lineNumbers;

	@FindBy(css = "div.d-inline-block")
	private List<WebElement> clickNPoolLine;

	@FindBy(css = "img.phone-image")
	private List<WebElement> virtualDigitsLine;

	@FindBy(css = "p.body.padding-bottom-xsmall-md")
	private WebElement suspendedLine;

	@FindBy(css = "span.body .imageSizeMobile")
	private WebElement planLink;

	@FindBy(xpath = "//span[contains(text(),'Click here to restore service')]")
	private WebElement restore;

	@FindBy(css = "img[role='presentation']")
	private List<WebElement> clickEssen;

	// Others
	@FindBy(xpath = "//span[contains(text(),'Your account has been temporarly suspended.Please call 1-800-937-8997.')]")
	private WebElement errorSuspended;

	@FindBy(xpath = "//span[contains(text(),'Your account will be eliglible to add additional lines 60 days after you activated.')]")
	private WebElement errorTenure;

	@FindBy(xpath = "//span[contains(text(),'You've reached the maximum number of lines you can add  to this account.')]")
	private WebElement eligleMBBline;

	@FindBy(xpath = "//p[contains(text(),'(425) 443-1421')]")
	private WebElement digits;

	@FindBy(xpath = "//p[contains(text(),'(425) 365-8114')]")
	private WebElement homeInternet;

	public AccountOverviewPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Account Overview page
	 * 
	 * @return
	 */
	public AccountOverviewPage verifyAccountOverviewPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("account-overview"));
			Assert.assertTrue(verifyElementByText(textAccountOnAccountOverviewPage, "Account"),
					"Account Overview page is not loaded");
			Reporter.log("Account Overview page is loaded.");
		} catch (Exception e) {
			Assert.fail("Account Overview page is not displayed");
		}
		return this;
	}

	/**
	 * Click On NPool Line
	 * 
	 * @return
	 */

	public AccountOverviewPage clickOnNPoolLine() {
		try {
			checkPageIsReady();
			if (!clickNPoolLine.isEmpty()) {
				clickNPoolLine.get(1).click();
				Reporter.log("Click on non pooled line and naviated to LineDetailsPages");
			}
		} catch (Exception e) {
			Assert.fail("Unable to click on the line");
		}
		return this;
	}

	
	/**
	 * Verify Suspended Error Message
	 * 
	 * @return
	 */

	public AccountOverviewPage verifySuspendedErrorMessage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(errorSuspended.isDisplayed(), "Expected Error Message is not displayed");
			Reporter.log(
					"Modal window is displayed with Your account has been temporarly suspended.Please call 1-800-937-8997. error message");
		} catch (Exception e) {
			Assert.fail("Expected Error Message is not displayed");
		}
		return this;
	}

	/**
	 * Verify Add A Line
	 * 
	 * @return
	 */
	public AccountOverviewPage verifyAddALine() {
		try {
			checkPageIsReady();
			Assert.assertTrue(addALine.isDisplayed(),
					"Add a New Line button is not available on Account Overview Page");
			Reporter.log("Add a New Line button is available on Account Overview Page");
		} catch (Exception e) {
			Assert.fail("Add a New Line button is not available on Account Overview Page");
		}
		return this;
	}

	/**
	 * Click on Add A Line
	 * 
	 * @return
	 */
	public AccountOverviewPage clickOnAddaLine() {
		try {
			checkPageIsReady();
			addALine.click();
			Reporter.log("Clicked on Add a Line button and navigated to Customer Intent Page");
		} catch (Exception e) {
			Assert.fail("Add a New Line button is not available to Click");
		}
		return this;
	}

	/**
	 * Click on Change Plan CTA
	 * 
	 * @return
	 */
	public AccountOverviewPage clickOnChangePlanCTAOnAccountOverviewPage() {
		try {
			changePlanCTAOnAccountOverViewPage.click();
			Reporter.log("Clicked on change Plan CTA");
		} catch (Exception e) {
			Assert.fail("Change Plan CTA button is not available to Click");
		}
		return this;
	}

	/**
	 * Click on Plan name
	 * 
	 */
	public AccountOverviewPage clickOnPlanName() {
		try {
			waitFor(ExpectedConditions.visibilityOf(planNameText.get(0)));
			planNameText.get(0).click();
			Reporter.log("Clicked on PlanName and Navigated to PlanDetailsPage ");
		} catch (Exception e) {
			Assert.fail("PlanName is not available and unable to navigate to PlanDetailsPage ");
		}
		return this;
	}

	/**
	 * Verify on Plan name
	 * 
	 */
	public boolean verifyPlanName() {
		boolean isPlanName = false;
		try {
			if (!planNameText.isEmpty()) {
				isPlanName = true;
			}
			Reporter.log("Plan name is displayed ");
		} catch (Exception e) {
			Assert.fail("Plan name is not displayed");
		}
		return isPlanName;
	}

	/**
	 * Click On Line Name
	 * 
	 */
	public AccountOverviewPage clickOnLineName() {
		try {
			waitforSpinner();
			checkPageIsReady();
			lineImgs.get(0).click();
			Reporter.log("Clicked on LineName and Navigated to LineDetailsPage ");
		} catch (Exception e) {
			Assert.fail("LineName is not available to select and unable to navigate to LineDetailsPage ");
		}
		return this;
	}

	/**
	 * Click On Line Name by Text
	 * 
	 */
	public AccountOverviewPage clickOnLineNamebyText(String text) {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementBytext(lineNameByText, text);
			Reporter.log("Clicked on LineName and Navigated to LineDetailsPage ");
		} catch (Exception e) {
			Assert.fail("LineName is not available to select and unable to navigate to LineDetailsPage ");
		}
		return this;
	}

	/**
	 * Click On Line Name by Misdn
	 * 
	 */
	public AccountOverviewPage clickOnLineNameByMisdn(String text) {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementBytext(lineNameByMisdn, text);
			Reporter.log("Clicked on LineName and Navigated to LineDetailsPage ");
		} catch (Exception e) {
			Assert.fail("LineName is not available to select and unable to navigate to LineDetailsPage ");
		}
		return this;
	}

	public boolean verifyCurrentUrlIsLineDetails() {
		return getDriver().getCurrentUrl().contains("line-details");
	}

	/**
	 * Verify Ban
	 * 
	 */
	public AccountOverviewPage verifyBan() {
		try {
			Assert.assertTrue(banHeader.isDisplayed());
			Reporter.log("Ban number is displayed");
		} catch (Exception e) {
			Assert.fail("Ban number is not displaying");
		}
		return this;
	}

	/**
	 * Verify Line Name
	 * 
	 */
	public AccountOverviewPage verifyLineName() {
		try {
			Assert.assertTrue(lineName.isDisplayed());
			Reporter.log("Line number is displayed");
		} catch (Exception e) {
			Assert.fail("Line number is not displaying");
		}
		return this;
	}

	/**
	 * Verify Monthly Total Details
	 * 
	 */
	public AccountOverviewPage verifyMonthlyTotalDetails() {
		try {
			verifyElementBytext(monthlyTotalDetails, "Monthly Total");
			verifyElementBytext(monthlyTotalDetails, "$");
			Reporter.log("Monthly total and Amount are displayed");
		} catch (Exception e) {
			Assert.fail("Monthly total and Amount are not displaying");
		}
		return this;
	}

	/**
	 * Click On Monthly Total Details
	 * 
	 */
	public AccountOverviewPage clickOnMonthlyTotalDetails() {
		try {
			monthlyTotalDetails.get(0).click();
			Reporter.log("Clicked on Monthly total");
		} catch (Exception e) {
			Assert.fail("Failed to click on Monthly total");
		}
		return this;
	}

	/**
	 * Verify Account Details Page Headers
	 * 
	 */
	public AccountOverviewPage verifyAccountDetailsPageHeaders() {
		try {
			checkPageIsReady();
			verifyListOfElementsBytext(accountOfheaders, "Plans");
			verifyListOfElementsBytext(accountOfheaders, "Lines and Devices");
			Reporter.log("Headers are available on the Account Details Page");
		} catch (Exception e) {
			Assert.fail("Headers are not available on the Account Details Page");
		}
		return this;
	}

	/**
	 * Get size of Number Of Lines
	 * 
	 */
	public int getNumberOfLines() {
		return lineNumbers.size();
	}

	/**
	 * click On Digits line
	 * 
	 */
	public AccountOverviewPage clickOnDigitsline() {
		try {
			checkPageIsReady();
			digits.click();
			Reporter.log("Clicked on WearablePlan line and navigated to lineDetails page ");
		} catch (Exception e) {
			Assert.fail("WearablePlan line is not available to select");
		}
		return this;
	}

	/**
	 * click On Digits line
	 * 
	 */
	public AccountOverviewPage verifyRetoreErrorMessage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(restore.isDisplayed(), "restore service Error Message is not available");
			Reporter.log("Click here to restore service Error Message is available");
		} catch (Exception e) {
			Assert.fail("Click here to restore service Error Message is not available");
		}
		return this;
	}

	/**
	 * click On Wearable Plan
	 * 
	 */
	public AccountOverviewPage clickOnWearablePlan() {
		try {
			checkPageIsReady();
			lineImgs.get(1).click();
			Reporter.log("Clicked on WearablePlan line and navigated to lineDetails page ");
		} catch (Exception e) {
			Assert.fail("WearablePlan line is not available to select");
		}
		return this;
	}

	public AccountOverviewPage clickOnAlertsOnAccountOverviewPage() {
		try {
			alert.click();
			Reporter.log("Clicked on Alert");
		} catch (Exception e) {
			Assert.fail("Unable to click on Alert");
		}
		return this;
	}

	/**
	 * verify total cost on Account Overview page
	 * 
	 * @return
	 */
	public double verifyTotalCostOnAccountOverviewPage() {
		double cost = 0;
		String monthlyCost = null;
		try {
			Assert.assertTrue(monthlyTotalCost.isDisplayed(), "Monthly total cost is not displayed");
			monthlyCost = monthlyTotalCost.getText().trim();
			monthlyCost = monthlyCost.replace("$", "");
			cost = Double.parseDouble(monthlyCost);
			Reporter.log("Cost on Account overview page is " + cost);
		} catch (Exception e) {
			Assert.fail("Monthly total cost is not displayed");
		}
		return cost;
	}



	/**
	 * verify Alerts On Account Overview Page
	 * 
	 */
	public AccountOverviewPage verifyAlertsOnAccountOverviewPage() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(alert));
			if (alert.getText().contains("YOU'VE USED 80% OF YOUR DATA")) {
				Assert.assertTrue(alert.getText().contains("YOU'VE USED 80% OF YOUR DATA"),
						"The Alert 'You've used 80% of your data' is displayed");
				Reporter.log("The Alert 'You've used 80% of your data' is displayed");
			} else if (alert.getText().contains("You've used 100% of your data")) {
				Assert.assertTrue(alert.getText().contains("You've used 100% of your data"),
						"The Alert 'You've used 100% of your data' is displayed");
				Reporter.log("The Alert 'You've used 100% of your data' is displayed");
			} else {
				Assert.fail("Unable to verify alert on line details Page");
			}
		} catch (Exception e) {
			Assert.fail("Unable to verify alert on line details Page");
		}
		return this;
	}
}