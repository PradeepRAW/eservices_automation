package com.tmobile.eservices.qa.api.unlockdata;

/**
 * This class has all the properties for soap calls
 * 
 * @author blakshminarayana
 */
public class SoapRequestData {
	private String macID;
	private String storeID;
	private String userName;
	private String environment;
	private String banNumber;
	private String subscriberNumber;
	private Boolean warrantyFulfillment;

	/**
	 * @return the macID
	 */
	public String getMacID() {
		return macID;
	}

	/**
	 * @param macID
	 *            the macID to set
	 */
	public void setMacID(String macID) {
		this.macID = macID;
	}

	/**
	 * @return the storeID
	 */
	public String getStoreID() {
		return storeID;
	}

	/**
	 * @param storeID
	 *            the storeID to set
	 */
	public void setStoreID(String storeID) {
		this.storeID = storeID;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}

	/**
	 * @param environment
	 *            the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	/**
	 * @return the banNumber
	 */
	public String getBanNumber() {
		return banNumber;
	}

	/**
	 * @param banNumber
	 *            the banNumber to set
	 */
	public void setBanNumber(String banNumber) {
		this.banNumber = banNumber;
	}

	/**
	 * @return the subscriberNumber
	 */
	public String getSubscriberNumber() {
		return subscriberNumber;
	}

	/**
	 * @param subscriberNumber
	 *            the subscriberNumber to set
	 */
	public void setSubscriberNumber(String subscriberNumber) {
		this.subscriberNumber = subscriberNumber;
	}

	/**
	 * @return the warrantyFulfillment
	 */
	public Boolean getWarrantyFulfillment() {
		return warrantyFulfillment;
	}

	/**
	 * @param warrantyFulfillment
	 *            the warrantyFulfillment to set
	 */
	public void setWarrantyFulfillment(Boolean warrantyFulfillment) {
		this.warrantyFulfillment = warrantyFulfillment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((banNumber == null) ? 0 : banNumber.hashCode());
		result = prime * result + ((environment == null) ? 0 : environment.hashCode());
		result = prime * result + ((macID == null) ? 0 : macID.hashCode());
		result = prime * result + ((storeID == null) ? 0 : storeID.hashCode());
		result = prime * result + ((subscriberNumber == null) ? 0 : subscriberNumber.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + ((warrantyFulfillment == null) ? 0 : warrantyFulfillment.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SoapRequestData other = (SoapRequestData) obj;
		if (banNumber == null) {
			if (other.banNumber != null) {
				return false;
			}
		} else if (!banNumber.equals(other.banNumber)) {
			return false;
		}
		if (environment == null) {
			if (other.environment != null) {
				return false;
			}
		} else if (!environment.equals(other.environment)) {
			return false;
		}
		if (macID == null) {
			if (other.macID != null) {
				return false;
			}
		} else if (!macID.equals(other.macID)) {
			return false;
		}
		if (storeID == null) {
			if (other.storeID != null) {
				return false;
			}
		} else if (!storeID.equals(other.storeID)) {
			return false;
		}
		if (subscriberNumber == null) {
			if (other.subscriberNumber != null) {
				return false;
			}
		} else if (!subscriberNumber.equals(other.subscriberNumber)) {
			return false;
		}
		if (userName == null) {
			if (other.userName != null) {
				return false;
			}
		} else if (!userName.equals(other.userName)) {
			return false;
		}
		if (warrantyFulfillment == null) {
			if (other.warrantyFulfillment != null) {
				return false;
			}
		} else if (!warrantyFulfillment.equals(other.warrantyFulfillment)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SoapRequestData [macID=" + macID + ", storeID=" + storeID + ", userName=" + userName + ", environment="
				+ environment + ", banNumber=" + banNumber + ", subscriberNumber=" + subscriberNumber
				+ ", warrantyFulfillment=" + warrantyFulfillment + "]";
	}
}
