package com.tmobile.eservices.qa.api.eos.monitors;

import org.apache.commons.lang3.StringUtils;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class StandardUpgradeAddSocsEIPFlow extends OrderProcess {

	/*
	 * 
	 * Steps For Standard Upgrade Add SOCs EIP Flow,
	 * 1. getCatalogProducts
	 * 2. Create Cart
	 * 3. Get SocsForSale
	 * 4. Update Cart with SOC
	 * 5. Create Quote
	 * 6. Update Quote
	 * 7. Make Payment
	 * 8. Place Order
	 * 
	 * */
	
	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			invokeCreateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}
	
	@Override
	public void getSOCForSale(ApiTestData apiTestData) throws ServiceFailureException{
		try{
		String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "getSOCforSale_req_forMonitor.txt");
		String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
		logLargeRequest(updatedRequest, "getSOCForSale");
		Response response = serviceApi.getServicesForSale(apiTestData, updatedRequest, serviceResultsMap);
		boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "getSOCForSale");
		if(isSuccessResponse){
			if (StringUtils.isBlank(response.body().asString()) || "{}".equalsIgnoreCase(response.body().asString())) {
				throw new Exception("getSOCForSale Response is Empty :" + response.body().asString());
			}
			getTokenMapForSocForSale(response);
		}
		}catch(Exception e){
			Reporter.log("Service issue while invoking the getSocForSale : "+e);
			throwServiceFailException();
		}
				
	}
	
	@Override
	public void updateCart(ApiTestData apiTestData) throws ServiceFailureException{
		try{
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_PHPSOC_EIP_req.txt");
			invokeUpdateCart(apiTestData, requestBody);
			}catch(Exception e){
				Reporter.log("Service issue during Update Cart : "+e);
				throwServiceFailException();
			}
	}
	
	public void getTokenMapForSocForSale(Response response) {
		JsonNode jsonNode = getParentNodeFromResponse(response);

		serviceResultsMap.put("socCode", getPathVal(jsonNode, "socForSale[0].socCode"));
		serviceResultsMap.put("socName", getPathVal(jsonNode, "socForSale[0].socName"));
		serviceResultsMap.put("socPrice", getPathVal(jsonNode, "socForSale[0].price"));		
	}	

}
