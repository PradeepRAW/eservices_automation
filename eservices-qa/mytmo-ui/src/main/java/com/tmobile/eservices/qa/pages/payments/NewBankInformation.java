package com.tmobile.eservices.qa.pages.payments;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.pages.CommonPage;

public class NewBankInformation extends CommonPage {

	private Long accNumber = 0L;
	private int count = 0;

	@FindBy(css = "p.Display3")
	private WebElement pageHeader;

	@FindBy(css = "i.tooltip-icon.ml-2")
	private WebElement toolTip;

	@FindBy(css = "span[tooltip-position='top']")
	private WebElement toolTipText;

	@FindBy(css = "label[for='nameOnaAccount']")
	private WebElement nameOnAccountHeader;

	@FindBy(id = "bankNumberLabel")
	private WebElement routingNumberHeader;

	@FindBy(css = "label[for='accountNumber']")
	private WebElement accountNumberHeader;

	@FindBy(id = "accountName")
	private WebElement accountName;

	@FindBy(id = "routingNum")
	private WebElement routingNum;

	@FindBy(id = "accountNum")
	private WebElement bankAccountNumber;

	@FindBy(linkText = "How to find my routing or account number")
	private WebElement howToFindLink;

	@FindBy(className = "imgwidth")
	private WebElement howToFindModel;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement cancelBtn;

	@FindBy(id = "acceptButton")
	private WebElement continueAddBank;

	@FindBy(css = "span.padding-horizontal-xsmall")
	private List<WebElement> replaceStoredPaymentAlert;

	@FindBy(css = "p.legal.color-red.text-left.errormessage")
	private List<WebElement> errorMsg;

	@FindBy(css = "span.slider.round")
	private WebElement savePaymentCheckBox;

	@FindBy(css = "div.overlay-in")
	private WebElement outOfModel;

	@FindBy(css = "span.mt-lg-1")
	private WebElement saveThispaymentErrorMsg;

	private final String pageUrl = "payments/addbank";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public NewBankInformation(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewBankInformation verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewBankInformation verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			pageHeader.isDisplayed();
			Reporter.log("Payment spoke page is loaded and header is verified ");
		} catch (Exception e) {
			Assert.fail("Failed to load payment spoke page");
		}
		return this;
	}

	public NewBankInformation clickCancelBtn() {
		try {
			cancelBtn.click();
		} catch (Exception e) {
			Assert.fail("Failed to click back button");
		}
		return this;
	}

	/**
	 * click continue button for add Card
	 */
	public NewBankInformation clickContinueAddCard() {
		try {

			waitFor(ExpectedConditions.elementToBeClickable(continueAddBank));
			clickElementWithJavaScript(continueAddBank);
			Reporter.log("Clicked on Continue button");

		} catch (Exception e) {
			Assert.fail("Failed to Click on Continue button");
		}
		return this;
	}

	/**
	 * Fill all the bank details and click on continue
	 *
	 * @param payment
	 */
	public void fillBankInfo(Payment payment) {
		try {
			checkPageIsReady();
			accountName.sendKeys(payment.getCheckingName());
			routingNum.sendKeys(payment.getRoutingNumber());
			bankAccountNumber.sendKeys(payment.getAccountNumber());
			accNumber = Long.parseLong(payment.getAccountNumber());
			Reporter.log("Filled the bank info details");
		} catch (Exception e) {
			Assert.fail("Failed to fill bank info details ");
		}

	}

	/**
	 * click continue button for add bank
	 */
	public Long clickContinueAddBank() {
		try {
			checkPageIsReady();
			clickElement(continueAddBank);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("i.fa-spinner")));
			if (verifyErrorMsgIsDisplayed()) {
				accNumber = generateRandomAccNumber() + accNumber;
				bankAccountNumber.clear();
				bankAccountNumber.sendKeys(accNumber.toString());
				clickContinueAddBank();
			}
			Reporter.log("Clicked on continue Add Bank and set the Bank account number");
		} catch (Exception e) {
			Assert.fail("Failed to click on Continue Add Bank");
		}
		return accNumber;
	}

	private boolean verifyErrorMsgIsDisplayed() {
		try {
			for (WebElement error : errorMsg) {
				if (error.isDisplayed()) {
					return true;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * generate a unique amount value
	 *
	 * @return double - amount
	 */
	private int generateRandomAccNumber() {
		Integer min = 1000;
		Integer max = 9999;
		Random r = new Random();
		int randomNumber = r.nextInt() * (max - min) + min;
		// System.out.println("random number : " + randomNumber);
		return randomNumber;
	}

}