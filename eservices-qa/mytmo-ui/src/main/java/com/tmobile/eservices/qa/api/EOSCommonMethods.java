package com.tmobile.eservices.qa.api;

import java.net.URL;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.listeners.Verify;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSCommonMethods extends EOSCommonLib {

	public String[] getjwtfromfiltersforAPI(ApiTestData apiTestData) {

		String msisdn = getMsisdnAndPaswordFromFilter(apiTestData);
		String misdin = msisdn.trim().split("/")[0];
		String pwd = msisdn.trim().split("/")[1];
		if (misdin.trim() == "" || pwd.trim() == "")
			Assert.fail("msisdn or password is empty " + misdin);
		Reporter.log("log in credentials are: " + msisdn);
		apiTestData.setMsisdn(misdin);
		apiTestData.setPassword(pwd);
		return getauthorizationandregisterinapigee(misdin, pwd);

	}

	/**
	 * generate a unique amount value
	 * 
	 * @return double - amount
	 */
	public Double generateRandomAmount() {
		double min = 1.01;
		double max = 2.00;
		Random r = new Random();
		return r.nextDouble() * (max - min) + min;
	}

	public String getMsisdnAndPaswordFromFilter(ApiTestData apiTestData) {
		try {
			String fil = apiTestData.getfilter();
			if (StringUtils.isEmpty(fil)) {

				return apiTestData.getMsisdn() + "/" + apiTestData.getPassword();
			}

			String query = "$.[?(" + fil + ")].misdin";
			List<String> tstdata = com.jayway.jsonpath.JsonPath.parse(new URL(Constants.TEST_DATA_FILTER_LOCATION_PROD))
					.read(query);
			if (tstdata.size() > 0) {
				Random rand = new Random();
				String randomElement = tstdata.get(rand.nextInt(tstdata.size()));
				return randomElement;
			}
			return null;

		} catch (Exception e) {
			return null;
		}
	}

	public void checkexpectedvalues(Response response, String tag, String expectedvalue) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get(tag) != null) {
				if (jsonPathEvaluator.get(tag).toString().equalsIgnoreCase(expectedvalue))
					Reporter.log(tag + " value is " + expectedvalue);
				else
					Verify.fail(tag + " Expected value is: " + expectedvalue + " Actual Value:"
							+ jsonPathEvaluator.get(tag).toString());
			} else {
				Verify.fail(tag + " is not existing");
			}
		}

		else {
			Verify.fail("Response is:" + response.getStatusCode());
		}
	}

	public void checkjsontagitems(Response response, String tag) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get(tag) != null)
				Reporter.log(tag + " is existing");
			else
				Verify.fail(tag + " is not existing");
		}

		else {
			Verify.fail("Response is:" + response.getStatusCode());
		}
	}

	public void checkjsontagitemsList(Response response, String tag) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get(tag) != null) {

				List<Object> vals = jsonPathEvaluator.get(tag);
				boolean nullnot = false;
				for (Object val : vals) {
					if (val != null)
						nullnot = true;
					break;
				}
				if (nullnot == false) {
					Verify.fail(tag + " values are empty :" + vals.toString());
				} else {
					if (!vals.isEmpty())
						Reporter.log("Retrieved values for tag:" + tag + " are " + vals.toString());
					else
						Verify.fail(tag + " values are empty :" + vals.toString());

				}
			}

			else {
				Verify.fail(tag + " is not existing");
			}
		}

		else {
			Verify.fail("Response is:" + response.getStatusCode());
		}
	}

}
