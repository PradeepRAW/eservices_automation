package com.tmobile.eservices.qa.pages.payments.api;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSLegacybilling extends EOSCommonLib {

	public Response getResponselegacybillList(String alist[]) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", lbill.get(environment));
			// restService.setContentType("application/json");
			// restService.given().auth().basic("ebill",
			// "pa88b1llz").accept("application/json").
			restService.addHeader("Accept", "application/json");
			restService.addHeader("Authorization", lbillauth.get(environment));
			response = restService.callService("services/rest/listbills/" + alist[3], RestCallType.GET);

		}

		return response;
	}

	public String getbillingstatementid(Response response) {
		String statementid = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("billStatementId[0]") != null) {
				return jsonPathEvaluator.get("billStatementId[0]").toString();
			}
		}

		return statementid;
	}

	/********************************************************************************************
	 * 
	 * 
	 */

	public Response getResponseprintdetailbillmsisdn(String alist[], String statementid) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", lbill.get(environment));
			restService.addHeader("Accept", "application/json");

			restService.addHeader("Authorization", lbillauth.get(environment));
			response = restService.callService(
					"services/rest/printbill/" + statementid + "/" + alist[3] + "/MSISDN/" + alist[0],
					RestCallType.GET);

		}

		return response;
	}

	/********************************************************************************************
	 * 
	 * 
	 */

	public Response getResponseprintdetailbillban(String alist[], String statementid) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", lbill.get(environment));
			restService.addHeader("Accept", "application/json");
			restService.addHeader("Authorization", lbillauth.get(environment));
			response = restService.callService(
					"services/rest/printbill/" + statementid + "/" + alist[3] + "/BAN/" + alist[0], RestCallType.GET);

		}

		return response;
	}

	/********************************************************************************************
	 * 
	 * 
	 */

	public Response getResponseprintsummarybillmsisdn(String alist[], String statementid) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", lbill.get(environment));
			restService.addHeader("Accept", "application/json");
			restService.addHeader("Authorization", lbillauth.get(environment));
			response = restService.callService(
					"services/rest/printsummary/" + statementid + "/" + alist[3] + "/MSISDN/" + alist[0],
					RestCallType.GET);

		}

		return response;
	}

	/********************************************************************************************
	 * 
	 * 
	 */

	public Response getResponseprintsummarybillban(String alist[], String statementid) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", lbill.get(environment));
			restService.addHeader("Accept", "application/json");

			restService.addHeader("Authorization", lbillauth.get(environment));
			response = restService.callService(
					"services/rest/printsummary/" + statementid + "/" + alist[3] + "/BAN/" + alist[0],
					RestCallType.GET);

		}

		return response;
	}

	/**
	 * A sample request to check service 
	 * @param getjwt
	 * @return
	 * @throws Exception
	 */
	public Response getResponselegacybillListSample(String ban) throws Exception {
		Response response = null;
		if (ban != null) {
			RestService restService = new RestService("", lbill.get(environment));
			//Changed Accept header values from 'Application/json' to 'Text/xml', As the below service is returning XML Output.
			restService.addHeader("Accept", "text/xml");
			//Updated below encoded Authorization directly in EOSCommonLib: 'lbillauth' for 'qlab02'
			/*String authString = "ebill:Dont4Get";
	        String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());*/
			restService.addHeader("Authorization", lbillauth.get(environment));
			response = restService.callService("services/rest/listbills/" + ban, RestCallType.GET);
			System.out.println("Response Output: "+response.body().asString());
		}
		return response;
	}
	
}
