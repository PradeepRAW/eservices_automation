package com.tmobile.eservices.qa.common;

/**
 * @author nsuvvada This class has all the common properties
 */
public final class Constants {
	
	public static final String MYTMO_URL = "app.mytmo.url";
	public static final String ENV_MYTMO_URL = ".app.mytmo.url";
	public static final String ENV_PREPROD_URL = "e5.app.mytmo.url";
	public static final String TMO_MYFAMILY_URL = "app.myfamily.url";
	public static final String ENV_TMO_MYFAMILY_URL = ".app.myfamily.url";
	public static final String NAVIGATE_MYTMO_URL = "Navigate to MYTMO URL";
	public static final String BENEFITS_PAGE_TITLE = "T-Mobile Customer Benefits";
	public static final String VERIFY_BENEFITS_PAGE_TITLE = "Benefits page is displayed";
	public static final String BLADE_HEADER = "Simple Choice North America";
	public static final String BLADE_SUB_HEADER = "Includes coverage in 3 countries for the price of 1. U.S., Mexico, and Canada.";
	public static final String BLADE_BODY = "Unlimited calling to Mexico and Canada from the U.S. for no extra charge. Plus, get 4G LTE data and unlimited calling and texting when you're in Mexico and Canada, just like in the U.S. Get more details: https://my.t-mobile.com/plans.html"
			+ "\n" + "Feature not available on all Simple Choice plans.";
	public static final String CARD_HEADERS_0 = "Extended Range LTE";
	public static final String CARD_HEADERS_1 = "Wi-Fi Calling";
	public static final String CARD_HEADERS_2 = "Upgrade Programs";
	public static final String CARD_HEADERS_3 = "Simple Global";
	public static final String TERMS_CONDITIONS = "T-Mobile Terms and Conditions";
	public static final String PRIVACY_POLICY = "Privacy Policy & Personal Information | T-Mobile";
	public static final String HOME_PAGE_RIGHT_HEADERS_CALL_ME = "CALL ME";
	public static final String HOME_PAGE_RIGHT_HEADERS_ACC_HIST = "ACCOUNT HISTORY";
	public static final String HOME_PAGE_RIGHT_HEADERS_BENIFITS = "BENEFITS";
	public static final String HOME_PAGE_RIGHT_HEADERS_PROFILE = "PROFILE";
	public static final String HOME_PAGE_RIGHT_HEADERS_LOG_OUT = "LOG OUT";
	public static final String SETUP_CALL_TEXT = "Let's set up a call";
	public static final String HOME_PAGE_HEADING = "Welcome";
	public static final String DEFAULT_TEXT = "No topic selected";
	public static final String WHICH_CALL_NUMBER_TEXT = "Which number should we call?";
	public static final String VERIFY_WHICH_CALL_NUMBER_TEXT = "verify which number should we call?";
	public static final String NUMBER_DEFAULT_DISPLAYED = "(917) 213-0635";
	public static final String NUMBER_TO_ENTER = "9172130635";
	public static final String LANGUAGE_SPANISH = "Spanish";
	public static final String CALLME_LINK = "Verify call me is disabled";
	public static final String CALLME_LINK_ERROR = "Call me is enabled";
	public static final String SHECDULE_CALLME_LINK = "Verify shecdule call me is disabled";
	public static final String SHECDULE_CALLME_LINK_ERROR = "shecdule call me is enabled";
	public static final String LIST_TOPIC_BILLING = "Billing & Payments";
	public static final String LIST_TOPIC_PLAN = "Plan, Features & Usage";
	public static final String LIST_TOPIC_FEATURES = "Features & Usage";
	public static final String LIST_TOPIC_DEVICE = "Device Support";
	public static final String LIST_TOPIC_ACC_MANG = "Account Management";
	public static final String LIST_TOPIC_OTHER = "Other";
	public static final String CARD_BODY_1 = "A signal that reaches 2x further and is 4x better in buildings than before.";
	public static final String CARD_BODY_2 = "Now every Wi-Fi connection works like a T-Mobile Tower.";
	public static final String CARD_BODY_3 = "Get the phone you want, when you want up to 3x per year.";
	public static final String CARD_BODY_4 = "Get unlimited data and texting in 95% of the places you travel most.";
	public static final String CARD_LINK = "Learn more";
	public static final String VIEW_FILTER_LEVELS = "View filter levels link is not displaying";
	public static final String NICK_NAME_SAVE = "Nick name Save button is not displaying";
	public static final String CARD_POP_1 = "All-new Extended Range LTE Network is a broader, deeper LTE signal traveling twice as far and four times better in buildings than before.";
	public static final String CARD_POP_2 = "Now calling and texting can be done virtually anywhere. Every time you connect to a Wi-Fi network your phone is ready to go. At no additional cost.";
	public static final String CARD_POP_3 = "With JUMP! On Demand, an upgrade program only from T-Mobile, you can switch to the latest phones with no waiting period or upgrade fees. Simply trade in your phone for a new one up to three times a year. Available in stores on select smartphones.";
	public static final String CARD_POP_4 = "Our Simple Choice Plan makes it simple to stay in touch while you explore the world. Enjoy unlimited data and texting in 140+ countries and destinations around the globe. Oh, and calls?";
	// Usage Page
	public static final String TOTAL_ON_NETWORK_DATA = "Of Total on-network data is not displaying";
	public static final String OF_UNLIMITED_HIGH_SPEED = "Of Unlimited high-speed is displaying";
	// file(AssignedTo: Venkat Reddy)
	public static final String PLAN_NAVI_TEXT = "You are currently on our best Talk and Text plan. To  customize your Plan, you may still Change Data and Add Services";
	public static final String DATA_URL = "https://my.t-mobile.com/plans/viewdataservices.verify.html";
	public static final String SERVICE_URL = "https://my.t-mobile.com/plans/services-list.html";
	public static final String DATAURL = "https://e1.my.t-mobile.com/plans.html";
	public static final String DATAREVIEWURL = "https://e2.my.t-mobile.com/plans/data-review.html";
	public static final String LOST_DEVICE_REPORT = "You have reported this device as lost";
	public static final String ERROR_CHECK = "Test Failed Error is :";
	public static final String ERROR_PAGE_NOT_LOADED = "Page not loaded";
	// login page
	public static final String APPLICATION_NOT_LOADED = "Application not Loaded.";
	public static final String NAVIGATE_TO = "Open browser and Navigate to ";
	public static final String LOGIN_AS = "Entered Userid/Password as ";
	public static final String VERIFY_LANDING_PAGE = " Page Displayed";
	public static final String UNABLE_TO_UNLOCK = "Unable to unlock the user due to: ";
	public static final String CLK_CLOUD_CONTACTUS_LINK = "Clicked on cloud contact us link";
	public static final String CLK_CLOUD_ABOUTTMOBILE_LINK = "Clicked on About T-Mobile link";
	public static final String CLK_CLOUD_COVERAGE_LINK = "Clicked on cloud coverage link";
	public static final String CLK_CLOUD_RETURNPOLICY_LINK = "Clicked on cloud return policy link";
	public static final String CLK_LEGACY_CONTACTUS_LINK = "Clicked on legacy contact us link";
	public static final String CLK_LEGACY_COVERAGE_LINK = "Clicked on legacy coverage link";
	public static final String CLK_LEGACY_HEADER_COVERAGE_LINK = "Clicked on legacy header coverage link";
	public static final String CLK_LEGACY_RETURNPOLICY_LINK = "Clicked on legacy return policy link";
	public static final String CLK_LEGACY_SUPPORT_LINK = "Clicked on legacy support link";
	public static final String CLK_LEGACY_HEADER_STORELOCATOR_LINK = "Clicked on legacy header store locator link";
	public static final String CLK_LEGACY_HEADER_TMOBILE_LINK = "Clicked on legacy header tmobile link";
	public static final String SWITCH_NEW_WINDOW = "New window should be displayed";

	//Forgot Password Page

	public static final String ENTER_ZIPCODE = "Enter Zip Code";
	public static final String SUBMIT_RESET_SEQURITY = "Submit Reset Sequrity Questions";
	
	// Footer Links
	public static final String CLK_FOOTER_CONTACT_US_LINK = "Clicked on Footer ContactUS Link ";
	public static final String CLK_FOOTER_SUPPORT_LINK = "Clicked on Footer Support Link ";
	public static final String VERIFY_DEALSHUB_PAGE = " Verify deals hub page";
	public static final String DEALSHUB_PAGE_ERROR = " deals hub page is not displayed";
	public static final String VERIFY_GET_IN_TOUCH_ICONS = "Verify Get in touch icons ";
	public static final String VERIFY_CONTACTUS_PAGE = " Verify Contact US page";
	public static final String VERIFY_ABOUTTMOBILE_PAGE = " Verify About T-Mobile page";
	public static final String VERIFY_COVERAGE_PAGE = " Verify coverage page";
	public static final String VERIFY_RETURNPOLICY_PAGE = " Verify return policy page";
	public static final String VERIFY_STORELOCATOR_PAGE = "Store locator page is displayed";
	public static final String STORELOCATOR_PAGE_ERROR = "store locator page is not displayed";
	public static final String VERIFY_TMOBILE_PAGE = " Verify tmobile page";
	public static final String VERIFY_BENEFITS_PAGE = " Benefits page is displayed";
	public static final String VERIFY_SUPPORT_PAGE = "Support page is displayed";
	public static final String BENEFITS_LINK = "Click benefits link";
	public static final String STORELOCATOR_LINK = "Click store locator link";
	public static final String HEADER_TMOBILE_LINK = "Click header tmobile link";

	public static final String DISPLAYED_SUPPORT_PAGE = "Success: Support page displayed ";
	public static final String DISPLAYED_GET_IN_TOUCH_ICONS = "Success: Displayed Get In Touch Icons ";

	public static final String ERROR_GET_IN_TOUCH_ICONS = "Get in touch icons not found";
	public static final String ERROR_SUPPORT_PAGE = "Support page not loaded";

	// Header links
	public static final String CLK_PHONE_LINK = "Clicked on Phone link";
	public static final String CLK_TRADE_IN_LINK = "Clicked on trade in link";

	public static final String VERIFY_ORDER_STATUS = "Verify Order Status Page";
	public static final String VERIFY_ORDER_STATUS_PAGE_ERROR = "Order status page is not displayed";

	public static final String ERROR_TRADE_IN_PAGE = "Trade in link not found";
	public static final String ERROR_ORDER_STATUS_PAGE = "Order status page not loaded";

	// Home Page
	public static final String HOMEPAGE_ERROR_MSG = "Home Page is not displayed";
	public static final String HOMEPAGE_ERROR_MSG1 = "Switched account Home Page is not displayed";
	public static final String VERIFY_HOMEPAGE_BILLING_INFO = "Verify Home page billing information";
	public static final String DISPLAYED_HOMEPAGE_BILLING_INFO = "Success: Balance Due, Due Date, Billing Amount, Pay bill and View bill displaying correctly";
	public static final String ALERTSECTION_ERROR_MSG = "Alert section is not diaplyed";
	public static final String T_MOBILE_WORK_PLAN = "T-Mobile ONE TE@Work";
	public static final String T_MOBILE_WORK_PLAN_ERROR = "T-Mobile ONE @Work TE is not displayed";

	// public static final String VERIFY_IMPORTANT_MESSAGETMOBILE_BENEFITS_LINK_ERROR = "Enable free HD and
	// 10GB 4G LTE Mobile HotSpot";
	public static final String VERIFY_IMPORTANT_MESSAGE1 = "Enable free HD and 10GB 4G LTE Mobile HotSpot";
	public static final String VERIFY_IMPORTANT_MESSAGE2 = ", or call Care on 611 using your T-Mobile phone.";
	public static final String ERROR_DUEDATE = "Missing Due date";
	public static final String ERROR_BALANCE_DUE = "Missing Balance Due";
	public static final String ERROR_BILLING_AMOUNT = "Missing Billing Amount";
	public static final String ERROR_PAYBILL = "Missing Paybill";
	public static final String ERROR_VIEWBILL = "Missing View Bill";

	public static final String CLICK_ON_SHOP = "Click On Shop";
	public static final String VERIFY_MANAGEDATAADD_ONS = "Manage Data Add Ons button should display";

	// Shop Page
	public static final String SHOPPAGE_ERROR_MSG = "Shop Page is not displaying";
	public static final String VERIFY_SHOP = "Verify Shop page";
	public static final String SHOP_ADD_A_LINE = "Click on add line button";
	public static final String SHOP_ADDED_LINE = "Verify Added Line";
	public static final String SHOP_ADDED_LINE_ERROR_MSG = "Added Line is displaying";

	// plans Configure Page
	public static final String PRINT_SERVICE_AGREEMENT = "Print Service Agreement is not displaying";
	public static final String VERIFY_SERVICE_AGREEMENT = "Service Agreement should display";
	public static final String PRINT_TERMS_AND_CONDITIONS = "Print Terms And Conditions is not displaying";
	public static final String VERIFY_TERMS_AND_CONDITIONS = "Terms And Conditions should display";
	public static final String PRINT_ELECTRONIC_SIGNATURE = "Print Electronic Signature Terms is not displaying";
	public static final String VERIFY_ELECTRONIC_SIGNATURE = "Electronic Signature Terms should display";

	// Services Review Page
	public static final String PRINT_SERVICE_AGREEMENT_TEXT = "Print Service AgreementText is not displaying";
	public static final String PRINT_TERMS_AND_CONDITIONS_TEXT = "Print Terms And Conditions is not displaying";
	public static final String PRINT_ELECTRONIC_SIGNATURE_TEXT = "Print Electronic Signature Terms is not displaying";

	// Add A Line Page

	public static final String CLICK_ON_ADDALINE = "Click On Add a Line";
	public static final String ADDALINE = "Add a Line";
	public static final String NUMBER_OF_LINES = "Choose New Number of lines";
	public static final String SELECT_GSM_DROPDOWN = "Select GSM dropdown";
	public static final String ADD_VOICE_LINE = "Click on Add Voice Line Button";
	public static final String ADD_VOICE_LINES = "Verify Add Voice Lines Button";
	public static final String ADD_MOBILE_INTERNET="Click on Add Mobile Internet button";
	public static final String SELECT_NEW_MOBILE_INTERNET="Click on Add Mobile Internet button"; 

	// Shop Plans Page
	public static final String SHOP_PLANS_PAGE_ERROR_MSG = "Shope Plans page is not displaying";
	public static final String VERIFY_SHOP_PLANS_PAGE = "Verify Shope Plans page";
	public static final String VERIFY_DATA_CALCULATOR = "Verify data calculator page";
	public static final String VERIFY_DOTNET_PAGES = "Verify dot net pages";
	public static final String TMOBILE_ONE_TABLET_PLAN_NAME="T-Mobile ONE Tablet";
	public static final String $75_PLAN_RATE="75";
	public static final String $75_TMOBILE_ONE_TABLET_PLAN_ERROR_MSG="$75 Tmobile one Tablet plan is not displayed";
	public static final String VERIFY_$75_TMOBILE_ONE_TABLET_PLAN="$75 Tmobile one Tablet plan is displayed";

	// Service Page
	public static final String SERVICE_PAGE_ERROR_MSG = "Service Page is not displaying";
	public static final String VERIFY_SERVICE_PAGE = "Verify Service Page";
	public static final String VERIFY_SERVICELIST_PAGE = "Service list page is displayed";
	public static final String SERVICELIST_PAGE_ERROR = "service list page is not displayed";
	public static final String VERIFY_SERVICEREVIEW_PAGE = "Service review list page is  displayed";
	public static final String SERVICEREVIEW_PAGE_ERROR = "Service review list page is not displayed";
	public static final String ADD_JUMP_SERVICE = "Add JUMP Service";
	public static final String VERIFY_ADD_JUMP_SERVICE = "Verify Add JUMP Service";
	public static final String ADD_PHP_SERVICE = "Add PHP Service";
	public static final String SELECT_6GB = "Click On 6GB plan ";
	public static final String SERVICE_PAGE_PHONE_ERROR_MSG = "Service Page is not displaying";
	public static final String $25_ONEPLUS_SERVICE="25$ one plus service ia added";

	// Accessories page
	public static final String ACCESSORIES_PAGE_ERROR_MSG = "Accessories Page is not displaying";
	public static final String VERIFY_ACCESSORIES = "Verify accessories page";
	public static final String ADD_ACCESSORIES = "Add accessories";
	public static final String VERIFY_COMPARE_ACCESSORIES = "Verify compare accessories";
	public static final String COMPARE_ACCESSORIES_ERROR_MSG = "Compare Accessories are not displaying";
	public static final String DONE_ADDING_ACCESSORIES_ERROR_MSG = "Done Adding Accessories button is not displaying";
	public static final String CLICK_ON_DONE_ADDING_ACCESSORIES = "Click On Done Adding Accessories Button";

	// Review Cart Page
	public static final String REVIEW_CART_PAGE_ERROR_MSG = "Review Cart page is not displaying";
	public static final String VERIFY_REVIEW_CART_PAGE = "Verify Review Cart page";
	public static final String VERIFY_ADDED_DEVICE = "Verify Added Device";
	public static final String ADDED_DEVICE_ERROR_MSG = "Added Device is not displaying";
	public static final String VERIFY_ADDED_ACCESSORIES = "Verify Added Accessories";
	public static final String ADDED_ACCESSORIES_ERROR_MSG = "Added Accessories are not displaying";
	public static final String VERIFY_ADDED_SERVICES = "Verify Added Services";
	public static final String ADDED_SERVICES_ERROR_MSG = "Added Services are not displaying";
	public static final String VERIFY_ADDED_PLAN = "Verify Added Plan";
	public static final String ADDED_PLAN_ERROR_MSG = "Verify Added Plan is not displaying";
	public static final String VERIFY_CLEAR_CART = "Verify clear cart";
	public static final String CLEAR_CART_ERROR_MSG = "Clear cart is not displaying";
	public static final String PRINT_CART_ERROR_MSG = "Print Cart is not displaying";
	public static final String VERIFY_PRINT_CART = "Print Cart is displaying";
	//public static final String CLICK_ON_ADDTOCART_BUTTON = "Click On Add To Cart Button";

	// Pay and Review page
	public static final String PAY_REVIEW_PAGE_ERROR_MSG = "Pay and Review is not displaying";
	public static final String VERIFY_PAY_REVIEW = "Verify Pay and Review";
	public static final String VERIFY_TERMSANDCONDITIONS = "Verify Terms and Conditions";
	public static final String TERMSANDCONDITIONS_ERROR_MSG = "Terms and Conditions are not displaying";

	// Customer Information View Page
	public static final String CUSTOMER_INFORMATION_PAGE_ERROR_MSG = "Customer Information Page is not displaying";
	public static final String VERIFY_CUSTOMER_INFORMATION_PAGE = "Verify Customer Information Page";
	public static final String VERIFY_911_YES_RADIO_BUTTON = "Verify 911 Yes Radio button";
	public static final String VERIFY_911_NO_RADIO_BUTTON = "Verify 911 No Radio button";
	public static final String STREET_ADDRESS_ERROR_MSG = "Street address is not displaying";
	public static final String CITY_ERROR_MSG = "City is not displaying";
	public static final String STATE_ERROR_MSG = "State is not displaying";
	public static final String EMAIL_ERROR_MSG = "Email is not displaying";
	public static final String OVERNIGHTSHIPPING_ERROR_MSG = "Overnight Shipping is not displaying";
	public static final String VERIFY_PAYANDREVIEW = " Verify Continue To PayAndReview";
	public static final String PAYANDREVIEW_ERROR_MSG = "PayAndReview button is not displaying";
	public static final String BACKTOCART_ERROR_MSG = " Back To Add Cart Btn  is not displaying";
	public static final String CANCELOREDR_ERROR_MSG = " Verify Cancel Order Btn  is not displaying";
	public static final String VERIFY_911_YES_ADDRESS_ERROR_MSG = "911 Address Yes Option Radio Button is not displaying";
	public static final String VERIFY_911_NO_ADDRESS_ERROR_MSG = "911 Address No Radio Button is not displaying";
	public static final String VERIFY_RETURN_TO_SHOP_ERROR_MSG = "Return To Shop Button is not displaying";
	public static final String CLICK_ON_BACK_TO_CART = "Click On Back To Cart";

	// Billing page
	public static final String BILLING_PAGE_HEADER_PAGE_ERROR_MSG = "Bill summary Page is not displayed";
	public static final String BILLING_PAGE_HEADER = "Bill summary";
	public static final String CURRENT_CHARGES_HEADER = "Current charges";
	public static final String VIEW_BILL_DETAILS = "View bill details";
	public static final String PRINT_BILL = "Print bill (PDF)";
	public static final String VIEW_USAGE_SUMMARY = "View usage summary";
	public static final String TABLE_HEADER_1 = "AMOUNT";
	public static final String TABLE_HEADER_2 = "CHANGE FROM LAST MONTH";
	public static final int GRAPH_SIZE = 4;
	public static final String CREDITS_AND_ADJUSTMENTS_OVERLAY = "Credits and adjustments overlay is not displaying";
	public static final String CREDITS_AND_ADJUSTMENTS = "Verify Credits and adjustments";
	
	public static final String ACC_HISTORY_HEADER = "ACCOUNT HISTORY";
	public static final String VIEW_ACC_HIST_LINK = "View Account History";
	public static final String UNAV_HEADER_TEXT = "Please select which account to manage:";
	public static final String DEVICE_LIST = "Devices list page is not displayed";

	public static final String BILLING_LINK = "Billing";
	public static final String USAGE_LINK = "Usage";
	public static final String PLAN_LINK = "Plan";
	public static final String PHONE_LINK = "Phone";
	public static final String SHOP_LINK = "Shop";

	// SearchBox
	
	public static final String SEARCH_SUPPORT_TAB = "Support tab is not displayed";
	public static final String SEARCH_DEVICES_TAB = "Devices tab is not displayed";
	public static final String SEARCH_ACCESSORIES_TAB = "Accessories tab is not displayed";

	public static final String SEARCH_TEXT_DATA_1 = "Apple";
	public static final String SEARCH_TEXT_DATA_2 = "Samsung";
	public static final String SEARCH_TEXT_INVALID = "@$%^()";
	public static final String NO_SEARCH_FOUND = "No results found.";

	public static final String REVIEW_CART_HEADER = "Review cart";
	public static final String CHECK_OUT_HEADER = "Checkout";

	// CardDetails
	public static final String CARD_NAME_FIELD = "CardHolder Name";
	public static final String CARD_CVV = "999";
	public static final String ZIP_CODE = "999999";
	public static final String CARD_NUMBER = "4916485957805904";

	// OneTimePayment
	public static final String PAYMENT_AMOUNT_TEXT = "Payment amount";
	public static final String PAYMENT_DETAILS_TEXT = "Enter payment information";
	public static final String PAYMENT_TYPE_TEXT = "Payment type";

	public static final String SHOP_PAGE_TITLE = "My T-Mobile | Shop";
	public static final String SHOP_PAGE_TITLE_MOBILE = "Shop";
	public static final String JUMP_VALIDATION_PAGE_TITLE = "My T-Mobile | JUMP! | JUMP! Device Verification";

	// Easy Pay Sign up details
	public static final String EASYPAY_CARD_NAME = "Test Name";
	public static final String EASYPAY_CARD_NUMBER = "4024007103742713";
	public static final String EASYPAY_CARD_CVV = "999";
	public static final String EASYPAY_CARD_ZIPCODE = "11201";

	public static final String EASYPAY_EDIT_CARD_NAME = "Testing Name";
	public static final String EASYPAY_EDIT_CARD_NUMBER = "4532400840682927";
	public static final String EASYPAY_EDIT_CARD_ZIPCODE = "30346";

	// ===================MyTmo : ADHOC & PAY
	// TestCases=============================
	// Your Profile Page Constants
	public static final String YOUR_PROFILE_HEADER = "Your profile";
	public static final String BILLING_ADDR_2 = "TEST";
	public static final String MANAGE_PAYMENT_OPTION_TXT = "Manage Payment Options";
	public static final String ROUTING_NUMBER = "321370707";
	public static final String ACCOUNT_NUMBER = "09507199";

	// E911 Address
	public static final String ZIP_CODE_E911 = "11201";
	public static final String STREET_ADDR = "250 Joralemon St";
	public static final String ADDRESSLINE1 = "1 RAVINIA DR STE 1000";
	public static final String CITY = "Brooklyn";
	
	// E911 Address
	public static final String INVALID_ZIP_CODE_E911 = "12345";
	public static final String INVALID_STREET_ADDR = "250 zenberg";
	public static final String INVALID_ADDRESSLINE1 = "Fourth lane";
	public static final String INVALID_CITY = "Amsterdam";

	// WA Address
	public static final String WA_ZIP_CODE = "98029";
	public static final String WA_State = "WA";
	public static final String WA_ADDRESSLINE1 = "1421 NE Iris St";
	public static final String WA_CITY = "Issaquah";
	
	
	// Real Credit Address
		public static final String Real_ZIP_CODE = "98029";
		public static final String Real_State = "WA";
		public static final String Real_ADDRESSLINE1 = "1421 NE Iris St";
		public static final String Real_CITY = "Issaquah";

	// BillDeliveryOptions
	public static final String EDIT_BILL_DELIVERY_OPTION = "Your bill delivery method is now paperless billing.";

	// MARKETING COMMUNICATIONS
	public static final String MARKETTING_COMM = "MARKETING COMMUNICATIONS";

	// profile Page
	public static final String EMAIL_ID = "4042001123@yopmail.com";
	public static final String TEST_EMAIL_ID = "ALEXANDER@test.com";
	public static final String PROFILE_PAGE_ERROR_MSG = "Profile page is not displaying";
	public static final String EXISTING_EMAIL_ADDRESS_MSG = "Email Address is not displaying";
	public static final String VERIFY_EMAIL_ERROR_MSG = "Email Error message is not displaying";
	public static final String EXISTING_EMAIL_ADDRESS = "Get Email Address";
	public static final String EXISTING_EMAIL_ADDRESS_ERR_MSG = "Verify exsisting Email Address error message";
	public static final String PROFILE_CANCEL_BTN = "Click on Email Cancel button";
	
	// securityAnswers
	public static final String FIRST_ANSWER = "firstAnswer";
	public static final String SECOND_ANSWER = "secondAnswer";
	public static final String THIRD_ANSWER = "thirdAnswer";

	// AAL_STANDARD SUITE

	public static final String DATA_6_GB = "6 GB";
	public static final String DATA_10_GB = "10GB";
	public static final String DATA_UNLIMITED_GB = "Unlimited";
	public static final String DEVICE_PAGE_TITLE = "My T-Mobile | Add a Line | Select Device";

	// Upgrade
	public static final String DATA_2_GB = "2 GB";

	public static final String PROFILE_ACCOUNTSETTINGS = "Account Settings";
	public static final String PAYMENTHISTORY_PASTCALLRECORDS = "See past call records";

	public static final String USAGEPAGE_TABS_VOICE = "Voice";
	public static final String USAGEPAGE_TABS_MESSAGING = "Messaging";
	public static final String USAGEPAGE_TABS_DATA = "Data";
	public static final String USAGEPAGE_TABS_TMOBILE = "T-Mobile purchases";
	public static final String USAGEPAGE_TABS_THIRD_PARTY = "Third-party purchases";

	public static final String USAGEPAGE_OVERVIEW_TABS_LINES = "Lines";
	public static final String USAGEPAGE_OVERVIEW_TABS_MINUTES = "Minutes";
	public static final String USAGEPAGE_TABS_MESSAGES = "Messages";
	public static final String USAGEPAGE_TABS_MOBILE_VOICE = "Voice: All";
	public static final String USAGEPAGE_TABS_MOBILE_MESSAGING = "Messaging: All";
	public static final String USAGEPAGE_TABS_MOBILE_DATA = "Data: All";
	public static final String USAGEPAGE_TABS_MOBILE_TMOBILE = "T-Mobile purchases: All";
	public static final String USAGEPAGE_TABS_MOBILE_THIRD_PARTY = "3rd party purchases All";
	public static final String TABS_MOBILE_THIRD_PARTY = "Third-party purchases: All";
	public static final String CARD_NUMBER_SUBMIT_ORDER = "4444444444444448";
	public static final String EXPIRY_MONTH_SUBMIT_ORDER = "01";
	public static final String EXPIRY_YEAR_SUBMIT_ORDER = "2017";
	public static final String CARD_CVV_SUBMIT_ORDER = "444";
	public static final String LINE_APPLY_CHANGE_MESSAGE = "Changes apply to all lines on your account";

	// Plans And Services

	public static final String PLAN_PAGE_HEADER = "Plans and services";
	public static final String CHANGE_DATA_HEADING = "Personalize your data";
	public static final String CHANGE_DATA_FOOTER_LINK = "http://www.t-mobile.com/OpenInternet";
	public static final String FREE_DATA_PLAN_PRICES = "free";
	public static final String THANK_YOU_MESSAGE = "Thank you! Your changes have been submitted successfully.";
	public static final String FAMILY_ALLOWANCES = "Family Allowances";
	public static final String DATACALCULATOR_HEADING = "Recommended Data Plan";
	public static final String PLANS_SERVICES = "Plans and Services page should be displayed.";

	public static final String CHANGE_PLANS_CONFIRMATION_MESSAGE = "Your changes have been submitted";
	public static final String PLANS_NICKNAME_MSISDN = "Nickname&MSISDN are not displaying";
	public static final String CHANGE_DATA_4GB_PLAN = "4GB data plan is not displaying";
	public static final String CHANGE_DATA_6GB_PLAN = "6GB data plan is not displaying";
	public static final String CHANGE_DATA_10GB_PLAN = "10GB data plan is not displaying";
	public static final String CHANGE_DATA_NODATA_PLAN = "No data plan is not displaying";
	public static final String CHANGE_DATA_UNLIMITED_PLAN = "Unlimited plan is  displaying";
	public static final String VIEW_PLAN_BTN = "Change plan";

	// Incorrect Login Text
	public static final String INCORRECT_LOGIN_TEXT = "The login information you provided is incorrect. Please try again.";
	public static final String T_MOBILE_ID = "T-Mobile ID";
	public static final String MEDIA_SETTINGS = "Media Settings";
	public static final String LOGIN_PAGE_ERROR = "Login page is not displayed";

	public static final String PROFILE_NAME = "Profile";

	// Device Page
	public static final String SELECT_DEVICE_TEXT = "Upgrade: Select Device";

	public static final String SUCCESS_PAGE = "Thank you!";
	public static final String BILLPAYMENT_SUCCESS_MESSAGE = "Thank you";
	public static final String ERR_SUCCESS_PAGE = "Success Page is not displayed";

	public static final String ERR_BILLING_PAYMENTS_PAGE = "User Is Not Navigated To Billing and Payments Page";
	public static final String PDFFILE_DOWNLOAD_PATH = "\\Downloads" + "\\8817001361.pdf";

	public static final String ORDER_COMPLETED = "order is complete!";
	public static final String ORDER_CONFIRMATION = "Confirmation order not displayed";

	public static final String JUMP_SOC_SERVICES = "JUMP!";
	public static final String JUMP_DEVCE_NAME = "Samsung Galaxy Note7 -64 GB";
	public static final String PLAN_LEGALESE_DATAPASS = "Limited time offer; subject to change. Taxes and fees not already included additional. Compatible device required; not all plans or features available on all devices. Data Passes: Does not include voice or messaging. Post-paid only; pass charges will appear on bill statement for effective date of the pass. Limit of 2 active passes per account in a particular pass category; device pass use dates may not overlap. May purchase new pass in same category prior to expiration of active pass if data allotment has been reached; unless pass includes use at reduced speeds. Partial megabytes rounded up. Full speeds available up to specified data allotment, then slowed to up to 2G speeds. No domestic or international roaming on Domestic Pass, unless Pass is specifically for roaming. International Pass use requires qualifying plan; usage does not impact Mobile Internet plan data allotment. Service available for time/usage amount provided by pass. For time period, a day is 12:00 a.m. to 11:59 p.m. Pacific Time (PT). Credit approval and qualifying agreement required; deposit may apply. Smartphone Mobile Hotspot(Tethering/Wi-Fi Sharing): Plan data allotment applies. No Roaming, unless Pass is specifically for roaming. Use of connected device subject to T-Mobile Terms and Conditions. Must use device manufacturer or T-Mobile feature. Network Management: Service may be slowed, suspended, terminated, or restricted for misuse, abnormal use, interference with our network or ability to provide quality service to other users, or significant roaming. Customers who use 30GB of data in a bill cycle will have their data usage de-prioritized compared to other customers for that bill cycle at locations and times when competing network demands occur, resulting in relatively slower speeds. See T-Mobile.com/OpenInternet for details. LTE is a trademark of ETSI.";
	public static final String VIRTUAL_LINE_MESSAGE = "Virtual Line is not displaying";

	public static final String VIRTUAL_LINE_MINUTES_MESSAGE = "Number of Minutes is not displaying";
	public static final String VIRTUAL_LINE_MESSAGES = "Number of Messages is not displaying";
	public static final String VIRTUAL_LINE_DATA_MESSAGE = "Number of Gigabytes Used is not displaying";

	public static final String PHONE_PAGE_UPGRADE_ERROR = "When you're ready to upgrade, simply visit";

	public static final String ORDER_STATUS = "Order Status";

	public static final String EQUIPMENT_INSTALLMENT_PLAN = "Installment Plan";
	public static final String EQUIPMENT_DOCUMENTS_RECEIPTS = "Documents & Receipts";
	public static final String DOCUMENTS = "Documents";
	public static final String BILLING_SUMMARY_PAGE = "User is not navigated to billing summary page";
	public static final String BILL_DETAILS_TEXT = "Bill details";
	public static final String JUMP_ELIGIBLE = "You're eligible for JUMP! upgrade.";
	public static final String ADDLINE_ELIGIBLE = "You are eligible to add";
	public static final String MP_NAME = "ESOA Test,";
	public static final String SBB_SOC_SERVICES = "Block Charged International Data Roaming - Single Line";
	public static final String EASY_PAY_CONFIRMATION = "successfully signed up for AutoPay";
	public static final String PAYMENT_WITH_TEXT = "Payment with:";

	public static final String ADDLINE_ELIGIBLE_DEFAULT = "You are eligible to add  voice and  mobile internet lines to your account.";
	public static final String ADDLINE_ELIGIBLE_ADDED_VOICE_LINES = "Eligible add voice are not displaying";
	public static final String ADDLINE_ELIGIBLE_ADDED_MOBILE_LINES = "Eligible add Mobile internet are not displaying";

	public static final String VIRTUALLINE_NOTELIGIBLE = "You are logged in with a DIGITS™ virtual line which is not eligible for a device upgrade.";
	public static final String VIRTUALLINE_NONVIRTUALLINE = "Please select a non-DIGITS™ virtual line to upgrade a device.";

	public static final String SIGNUP_HEADER_TEXT = "Sign up for T-Mobile ID";
	public static final String FORGOT_PSWD_HEADER_TEXT = "Reset Your Password";
	public static final String VERIFICATION_METHOD_HEADER_TEXT = "Choose verification method";

	public static final String PHONE_JUMPELIGILE_TXT = "You are a JUMP! customer.";

	public static final String UN_LOCK_ACCOUNT_TXT = "The login information you provided is incorrect. For your security, you have one more attempt at logging in until we lock your account.";

	public static final String UN_LOCK_ACCOUNT_24_TXT = "To protect your security, we have temporarily locked your account. To access your account, wait 24 hours or";

	public static final String FORGOT_PSWD_TXT = "The login information you provided is incorrect. Please try again.";

	public static final String LINE_SUSPENDED = "This line is Suspended";
	public static final String TEMPORARY_SUSPENSION = "Temporary suspension";

	public static final String JOD_WARNINGMESSAGE = "To upgrade your device today please Call us at 1 (800) 937-8997, dial 611 from your T-Mobile phone, or visit your local T-Mobile retail store.";

	public static final String CART_ACCESSORIES_ERROR_MSG = "Added Accessories not displaying";
	public static final String CART_SERVICES_ERROR_MSG = "Added Services are not displaying";
	public static final String CART_PAGE_ERROR_MSG = "Cart page is not displaying";
	public static final String CUSTOMER_INFO_PAGE_CONTINUE = "Customer information page is not displaying";

	public static final String TEMP_LOCKACCT = "Sorry, we’ve temporarily locked your account to protect your information. Please call Customer Care at 1-800-937-8997.";
	public static final String REPORTED_DEVICE_STOLEN = "You have reported this device as stolen";

	public static final String PROFILE_NAME_TEXT = "Make this the nickname for all lines linked to your T-Mobile ID.The system shall defecult the checkbox to checked state.";
	public static final String CLICK_ON_TMOBILE_ID = "Click on Tmobile ID Link";

	public static final String HOME_PAGE_VERIFY = "Not able to navigated to Home page";
	public static final String HOME_PAGE_HEADER = "Navigated To Home Page.";
	public static final String PROFILE_PAGE_VERIFY = "User Is Not Navigated To Profile Page.";
	public static final String PROFILE_PAGE_HEADER = "Navigated To Profile Page.";
	public static final String ACCOUNT_SETTING_HEADER = "Account settings details should be displayed";
	public static final String PROFILE_VALIDATE = "Validate User is able set e911 address and usage address for all the lines using below options";
	public static final String PROFILE_VALIDATE_SAVE = "Save E911 addres is clicked";
	public static final String PROFILE_VERIFY = "Make this the e911 address for all linesMake this the usage address for all lines";
	public static final String PROFILE_EDIT_ADDRESS = "E911 address should be edited";

	public static final String PROFILE_SAVE = "the system shall copy the new user level name to the line level for all MSISDNs linked to the current TMO ID";
	public static final String PROFILE_SAVE_ERROR_MESSAGE_TEXT = "User should be able to see the error message STATING  The number you provided is already registered to a T-Mobile ID. If this is your number, confirm your password to manage everything from a single T-Mobile ID.Confirm password AND the label Confirm password is a link";
	public static final String PROFILE_CANCEL = "User should see a screen prompting the user to enter the password associated with the new MSISDN";

	public static final String CHOOSE_ACCOUNT_TEXT = "Successfully Navigated to switch account";
	public static final String CAPCHA_POPUP = "captcha Screen not displayed";
	public static final String FORGOT_PSWD = "Successfuly Navigated to ForgotPassword Screen";
	public static final String RESETS_SQURITY_QUESTIONS = "Successfully displayed the Reset Sequrity Questions";
	public static final String HOME_HEADER_TEXT = "Home page should be displayed";
	public static final String PROFILE_HEADER_TEXT = "Profile 'Page should be displayed";
	public static final String EMAIL_NOTIFICATION_MSG_TEXT = "User should get email notification";
	public static final String EMAIL_NOTIFICATION_TEXT = "User should get welcome email Notification";
	public static final String CHANGE_PSWD_NAVIGATEDTO_HOME = "Successfully change the password and navigated to Home page";
	public static final String CONFIRMATION_TEXT = "Successfuly Redirect  to Confirm Screen";
	public static final String SEQ_QUESTION_HEADER_TEXT = "Successfuly Navigated to Missing Sequrity Questions Screen";
	public static final String FORGOT_PSWD_TEXT = "Successfully change the password and navigated to Home page";
	public static final String BAN_ERROR_MSG = "Ban is not displaying";
	public static final String VERIFY_UPGRADE = "Verify upgrade page";
	//public static final String UPGRADE_PAGE_ERROR = "Upgrade page is not displayed";
	public static final String VERIFY_DEVICE_PAGE = "verify Device Page header";
	public static final String VERIFY_DEVICE_SELECTION = "Verify Select first Device";
	public static final String VERIFY_CONTINUE_PLAN = "Select Continue in plan page";
	public static final String VERIFY_UPGRADE_TO_2GB_DATA = "Select 2GB Plan Page";
	public static final String SELECTED_2GB_DATA_SERVICE = "2GB data plan is selected";
	public static final String VERIFY_CONTINUE_CHANGE_DATA = "click continue to change data Services";
	public static final String DATA_CHANGED = "data Plan Selected";
	public static final String VERIFY_CONTINUE_SERVICES = "select continue in services page";
	public static final String VERIFY_REVIEW_CART_DEVICE_TEXT = "verify device text in Review cart";
	public static final String REVIEW_CART_SELECTED_DEVICE_TEXT = "Successfully Choosen Device text displayed";
	public static final String VERIFY_CHANGE_DEVICE_FROM_REVIEW_CART = "Verify change device from review cart page";
	public static final String CHANGED_DEVICE_REVIEW_CART = "Successfully changed device from Review Cart";
	public static final String VERIFY_CHANGED_DEVICE_TEXT = "verify changed device text in review cart";
	public static final String CHANGED_DEVICE_TEXT_DISPLAY = "Successfully verified changed device text";
	public static final String INSERT_CARD_DETAILS = "Insert Card Details";
	public static final String ENTERED_CARD_DETAILS = "Card Details entered successfully";
	public static final String VERIFY_ACCEPT_TC_SUBMIT = "Accept Terms and conditions with sign -click submit";
	public static final String DONE_ACCEPT_TC_SUBMIT = "Accepted TC , done sign and submitted successfully";
	public static final String NO_CONFIRMATION_ORDER = "Confirmation order not displayed";

	public static final String ORDER_COMPLETE = "order is complete!";
	public static final String VERIFY_CONFIRMATION_ORDER_DISPLAY = "verify Confirmation Order display";
	public static final String SUCCESSFUL_CONFIRMATION_ORDER = "Confirmation of order verified successfully";
	public static final String VERIFY_SELECT_COMPARE_DEVICES = "verify comparision of devices functionality";
	public static final String DONE_COMPARE_DEVICES = "successfully compared the features of devices";
	public static final String NO_COMPARISION = "devices selected are not appeared to compare";
	public static final String VERIFY_COMPARISION_DEVICE_COUNT = "Verify table for number of devices selected";
	public static final String DEVICES_TO_COMPARE_APPEARED = "selected device to compare are appeared";
	public static final String NO_APPEARANCE_FEATURE_BLOCK = "Compare FeatureBlock not appeared";
	public static final String VERIFY_FEATURES_COMPARE_BLOCK = "Verify Device features to compare";
	public static final String SUCCESSFUL_COMPARISION_DEVICES = "Features of different devices are compared successfully";
	public static final String NO_PAH_UPGRADE = "You are eligible for an upgrade.";
	public static final String VERIFY_NO_PAH_UPGRADE_TEXT = "verify eligible for upgrade option text appearance";
	public static final String TEXT_APPEARED = "-Text appeared";
	public static final String VERIFY_NO_PAH_AAL_TEXT = "For a non PAH, verify 'not eligible to add a line' text appears on shop page.";
	public static final String NO_PAH_AAL_TEXT = "You are not eligible to add a line.";
	public static final String NO_REVIEW_PAGE = "ReviewPage does not appeared";
	public static final String VERIFY_REVIEW_PAGE = "Verify Review Cart Page header is displayed";
	public static final String CLIK_CLEAR_CART = "Click Clear Cart link";
	public static final String CLICK_CHECKOUT = "Verify Checkout from minicart";
	public static final String PRINT_CART_LINK = "Print cart link not appeared";
	public static final String NOT_APPEARED = "-not appeared";
	public static final String VERIFY_PRINT_CART_LINK = "verify Print Cart link display";
	public static final String DISPLAY_PRINT_CART_LINK = "PrintCart link appeared";
	public static final String SELECT_SHIPPING_DETAILS_LINK = "Select Shipping Details link";
	public static final String DISPLAY_SHIPPING_DETAILS_LINK = "Display Shipping Details link";
	public static final String SHIPPING_POP_UP = "Shipping details popup display";
	public static final String SHIPPING_RADIO_BTN = "Shipping details option should be display";
	public static final String VERIFY_SHIPPING_POP_UP = "Verify Shipment detail popup";
	public static final String DISPLAY_SHIPPING_POP_UP = "Shipping details popup appeared successfully";
	public static final String CLOSE_SHIPPING_POP_UP = "Select Close-shipping popup";
	public static final String SELECT_CHANGE_PLAN = "select change button in plan page ";
	public static final String NEW_PLAN_CHOOSEN = "Change button clicked in plan page";
	public static final String VERIFY_UPGRADE_TO_6GB_DATA = "Select 6GB Plan Page";
	public static final String SELECTED_6GB_DATA_SERVICE = "6GB data plan is selected";
	public static final String CHOOSE_JUMP_PHP = "Choose Jump in PHP soc";
	public static final String ADDED_JUMP = "Jump in PHP added";
	public static final String NO_MESSAGE_SERVICES_CHOOSEN = "Message services not selected";
	public static final String VERIFY_JUMP_SOC = "Verify changes in PHP-Jump review cart page";
	public static final String JUMP_DISPLAY_REVIEW_CART = "Jump Services are displayed in review cart";
	public static final String UNSUCCESSFUL_REDIRECTION = "unsuccessful redirection to dotnet";
	public static final String VERIFY_REDIRECTION_DOTNET = "Verify redirection to dotnet";
	public static final String SUCCESSFUL_REDIRECTION_DOTNET = "redirected to dotnet successfully";
	public static final String EDIT_SOC_MESSAGES_SERVICES = "Edit Message and Soc Services";
	public static final String DONE_SOC_SERVICES_EDIT = "Services Page in SOC are edited";
	public static final String VERIFY_SOC_SERVICES_EDIT_MINI_CART = "Verify changes in Message and Soc Services in mini cart";
	public static final String EDIT_SERVICE_COMPLETED = "Edited Services in Soc are displayed in mini cart";
	public static final String VERIFY_SOC_SERVICES_REVIEW_CART = "Verify changes in Message and Soc Services in review cart page";
	public static final String DISPLAY_SOC_SERVICES_REVIEW_CART = "Edited Services are displayed in review cart";
	public static final String CHANGE_LANGUAGE_REVIEW_CART = "Change Language to Spanish";
	public static final String LANGUAGE_CHANGED_REVIEW_CART = "Spanish Language is selected";
	public static final String VERIFY_HOME_PAGE = "Home page should be loaded";
	//public static final String VERIFY_HOME_PAGE_DISPLAYED = "Home page is Displayed";
	public static final String VERIFY_PRIVACYPOLICY_PAGE = "Verify privacy policy page";
	public static final String PRIVACYPOLICY_PAGE_ERROR = "Privacy Policy page is not displayed";
    public static final String PRIVACYRESOURCE_PAGE_ERROR = "Privacy Resource page is not displayed";
    public static final String OPENINTERNETPOLICY_PAGE_ERROR = "Open Internet Policy page is not displayed";
    public static final String INTERESTBASEDADS_TEXT_ERROR = "Interest Based Ads is not displayed";
    public static final String COPYRIGHTS_PAGE_ERROR = "Copyrights Page is not displayed";


	public static final String SHOP_PAGE_HEADER = "Navigated To Shop Page.";

	public static final String NO_DISPLAY_SHOP_PAGE = "Shop page is not displayed after clicking Shop Link";
	public static final String VERIFY_SELECT_MSISDN = "Select MSISDN line selector in shop page";
	public static final String CHOOSEN_MSISDN_LINE_SELECTOR = "Selected Msisdn Selector in shop page";
	public static final String CHOOSE_SECOND_MSISDN = "Select second msisdn in line selector";
	public static final String SELECTED_SECOND_MSISDN = "second msisdn is selected";
	public static final String VERIFY_VIRTUAL_ELIGIBALE_TEXT = "Verify text for virtual eligible message";
	public static final String VERIFY_VIRTUAL_UPGRADE_TEXT = "Verify text for virtual upgrade eligible message";
	public static final String VERIFY_SCORE_UPGRADE = "Verify Score Upgrade button";
	public static final String VERIFY_DEVICE_SPECIFIC_PAGE = "Verify device specification page";
	public static final String DISPLAY_DEVICE_SPECIFIC_PAGE = "Successfully Navigated to device specification Page";
	public static final String VERIFY_PAYMENT_OPTION = "Verify Change Payment Option";
	public static final String DISPLAY_PAYMENT_OPTION = "Payment option appeared in review cart Page";
	public static final String CAPCHA_POPUP_HEADER_TEXT = "Security verification";

	public static final String VERIFY_VIRTUAL_LINE_ACCT = "Click Virtual Line acct";
	public static final String VERIFY_VIEW_PLAN_DETAIL = "Verify View plan details links";
	public static final String VIEW_PLAN_DETAIL = "View plan details link shoul be displayed";
	public static final String VERIFY_CHECK_USAGE_DETAIL = "Verify Check usage link details links";
	public static final String CHECK_USAGE_DETAIL = "Check Usage details link should be displayed";
	public static final String VERIFY_MANAGE_MULTILINE_LINK = "Verify Manage Multiline link details links";
	public static final String MANAGE_MULTILINE_LINK = "Manage Multiline details link should be displayed";
	public static final String USAGE_LINK_CLICK = "Click Usage link";
	public static final String USAGE_PAGE = "Usage page should be displayed";
	public static final String VIEW_ALL_USAGE_DETAILS = "View all usage details should be displayed";
	public static final String VIRTUAL_LINE_SELECTOR = "Select the Virutal line in the Line selector drop down";
	public static final String VERIFY_VIRTUAL_LINE_SELECTOR = "Selected Virtual Line selector should be displayed";
	public static final String VOICE_DATE_OPTION = "Verify the Voice Date option";
	public static final String VERIFY_VOICE_DATE_OPTION = "Voice date option should be displayed";
	public static final String DESTINATION_OPTION = "Verify the Destination option";
	public static final String VERIFY_DESTINATION_OPTION = "Destination option should be displayed";
	public static final String MINUTES_OPTION = "Verify the minutes option";
	public static final String VERIFY_MINUTES_OPTION = "Minutes option should be displayed";
	public static final String TYPE_OPTION = "Verify the Type option";
	public static final String VERIFY_TYPE_OPTION = "Type option should be displayed";
	public static final String NUMBER_OPTION = "Verify the Type option";
	public static final String VERIFY_NUMBER_OPTION = "Type option should be displayed";
	public static final String VERIFY_PLAN_PAGE = "Plan page is displayed";
	public static final String PLAN_PAGE = "Plan should be displayed";
	public static final String VIEW_DATA_PASS = "Click Add Pass link";
	public static final String VERIFY_VIEW_DATA_PASS = "View data pass page should be displayed";
	public static final String VERIFY_LEGEACY_LEASE = "Verify the legeacy lease text";
	public static final String LEGEACY_LEASE = "Legeacy lease text has been verified";
	public static final String PLAN_PAGE_PLUS_BUTTON = "Click plus button on Plan Page";
	public static final String VERIFY_PLAN_EXTRACTED = "Plan and service details should be displayed";
	public static final String VERIFY_VIRTUAL_LINE_MESSAGE = "Verify Virtual Line message";
	public static final String VIRTUAL_LINE_MESSAGE_PLAN = "Virutal Line message should be displayed and verified";
	public static final String VIRTUAL_LINE_MINUTES_MESSAGE_USAGE = "Number of Minutes is displaying";
	public static final String VIRTUAL_LINE_MESSAGES_USAGE = "Number of Messages is displaying";
	public static final String VIRTUAL_LINE_DATA_MESSAGE_USAGE = "Number of Gigabytes Used is displaying";
	public static final String VERIFY_VIRTUAL_LINE_MINUTES_MESSAGE_USAGE = "Verify number of Minutes tab is displayed";
	public static final String VERIFY_VIRTUAL_LINE_MESSAGES_USAGE = "Verify number of Messages tab is displayed";
	public static final String VERIFY_VIRTUAL_LINE_DATA_MESSAGE_USAGE = "Verify number of Gigabytes tab is displayed";
	public static final String PLANS_COMPARISION_PAGE = "Verify plans comparison page";
	public static final String TMOBILE1_PLAN = "Verify tmobile one plan";
	// My Tmo Dot Net tests
	public static final String VERIFY_PROFILE_PAGE = "Verify Profile Page";
	public static final String PROFILE_PAGE_DISPLAYED = "Profile Page is displayed";
	public static final String EDIT_BILLING_DELIVERY_OPTIONS = "Edit Billing Delivery options";
	public static final String EDIT_SECURITY_QUESTIONS = "Edit security questions and answers";
	public static final String EDIT_BILLING_DETAILS = "Edit Billing Address details";
	public static final String EDIT_EASY_PAY = "Edit Easy Pay";
	public static final String EDIT_MARKETING_COMMUNICATIONS = "Edit Marketing Communications";
	public static final String EDIT_PHONE_CONTROLS = "Edit Phone Controls";
	public static final String VERIFY_CUST_INFO_TC_PRIVACY_LINKS = "verify Customer Info TC & PrivacyLinks";
	public static final String EDIT_TMOBILE_PREFERENCES = "Edit Tmobile Preferences";
	public static final String VERIFY_DEVICE_LOST_STOLEN = "Verify Device Lost or Stolen Flow";

	// Phone Page
	public static final String CLICK_PHONE_PAGE = "Click Phone Page";
	public static final String NO_PHONE_PAGE = "not navigated to phone page";
	public static final String VERIFY_PHONE_PAGE = "Phone page is displayed";
	public static final String VERIFY_LITEQUOTETOOL_PAGE = "Lite quote tool Page is displayed";
	public static final String LITEQUOTETOOL_PAGE_ERROR = "lite quote tool Page is not displayed";
	public static final String PHONE_PAGE_ERROR = "Phone page is not displayed";
	public static final String DISPLAY_PHONE_PAGE = "Navigated to Phone page";
	public static final String VERIFY_UNAV_PAGE = "Verify UNAV Page";
	public static final String VERIFY_FILE_CLAIM_JUMP = "Verify file claim Jump option in phone page";
	public static final String DISPLAY_FILE_CLAIM_JUMP_OPTION = "File claim for Jump-option selected";
	public static final String ASSURANCE_POP_UP_CONTINUE = "Choose continue for file claim jump";
	public static final String VERIFY_CLAIM_URL = "verify File claim form displayed";
	public static final String DISPLAY_CLAIM_PAGE = "Assurant Claim form header has been Verified";
	public static final String VERIFY_OPTIONS_DISPLAY_IN_PHONE = "Verify Changes Sim button,Stolenreportbutton,DeviceLockstatus button and Temporary Suspension button";
	public static final String DISPLAYED_OPTIONS = "Changes Sim button,Stolenreportbutton,DeviceLockstatus button and Temporary Suspension button is displayed";
	public static final String VERIFY_DEVICE_IMAGE = "Verify Device Image Displayed";
	public static final String DISPLAY_DEVICE_IMAGE = "Device Image is displayed";
	public static final String VERIFY_REPORT_STOLEN_PAGE = "Verify Report Device as Lost or stolen Page";
	public static final String CHOOSE_LOCATE_DEVICE = "Select Locate device";
	public static final String SELECTED_LOCATE_DEVICES = "Successfully selected Locate Device";
	public static final String VERIFY_STOLEN_DEVICE_FUNCTION = "Verify Stolen device functionality";
	public static final String COMPLETED_STOLEN_DEVICE_PROCESS = "Successfully completed stolen device process";
	public static final String VERIFY_SUSPEND_LINE = "verify Suspend line functionality";
	public static final String COMPLETED_SUSPEND_LINE = "Successfully completed suspend line process";
	public static final String VERIFY_ACCEPT_TC = "Verify terms and conditions functionality";
	public static final String COMPLETED_ACCEPTANCE_TC = "Completed- agree terms and conditions";
	public static final String VERIFY_REPORT_STOLEN_DEVICE = "Verify the Stolen device reported";
	public static final String REPORT_STOLEN_PAGE = "Navigating to Device has been reported as stolen";
	public static final String SUBMIT_REPORT_STOLEN = "Click submit for Report stolen";
	public static final String GO_BACK_PHONE_PAGE = "Choose go back to home page";
	public static final String VERIFY_LINE_SUSPENDED_TEXT = "Verify the Line is suspended";
	public static final String DISPLAY_LINE_SUSPEND_TEXT = "This line is suspended text should be verified";
	public static final String CLICK_FOUND_DEVICE_LINK = "click found device link";
	public static final String CHOOSEN_FOUND_DEVICE_LINK = "Successfully selected found device link";
	public static final String CLICK_UNBLOCK_RESTORE_DEVICE = "click unblock restore device";
	public static final String UNBLOCKED_RESTORED_DEVICE = "Completed unblock of restore device";
	public static final String VERIFY_DEVICE_UNBLOCKED = "Verify the device is unblocked";
	public static final String DEVICE_UNBLOCK_DONE = "successfully verified device got unblocked";
	public static final String SELECT_EIP_DEVICE = "Selects EIP device";
	public static final String SELECT_NON_EIP_DEVICE = "Selects Non-EIP device";

	// desktopAutmationCore
	public static final String VERIFY_VIRTUAL_LINES = "Verify virtual Lines";
	public static final String DISPLAY_VIRTUAL_LINE_TEXT = "Virtual line is displayed and verified successfully";
	public static final String VERIFY_JOD_MESSAGE = "Verify JOD validation Message";
	public static final String DISPLAY_JOD_MESSAGE = "Clicking Phone Page and verified the message of JOD";
	public static final String VERIFY_LINE_SELECTOR = "Verify Msisdn line selector";
	public static final String DISPLAY_LINE_SELECTOR = "Line Selector for msisdn is appeared";
	public static final String NO_IMAGE_LINE_SELECTOR = "Image of device not appeared";
	public static final String VERIFY_IMAGE_LINE_SELECTOR = "Verify Image in line selector";
	public static final String DISPLAY_IMAGE_LINE_SELECTOR = "Image displayed in line selector";
	public static final String VERIFY_DEVICE_NUMBER_LINE_SELECTOR = "verify device number in line selector";
	public static final String DISPLAY_DEVICE_NUMBER_LINE_SELECTOR = "device number displayed in line selector";
	public static final String VERIFY_DEVICE_NAME_LINE_SELECTOR = "verify device name in line selector";
	public static final String DISPLAY_DEVICE_NAME_LINE_SELECTOR = "device name displayed in line selector";
	public static final String GET_TEXT_DOWN_PAYMENT = "get text of down payment in devices page";
	public static final String DOWN_PAYMENT_TEXT_DONE = "saved text for down payment";
	public static final String VERIFY_PRODUCT_DETAILS = "Verify Product Details page";
	public static final String DISPLAY_PRODUCT_DETAILS = "Product details - appeared";
	public static final String VERIFY_PRODUCT_DETAILS_DOWN_PAYMENT = "Verify Product Details page and down payment option";
	public static final String PRODCUT_DETAILS_MATCHED = "successfully verified Product Details page and down payment option";
	public static final String CLICK_BILLING_PAGE = "Click Billing Link";
	public static final String VERIFY_BILLING_PAGE = "Verify bill summary page";
	public static final String BILLING_PAGE_DISPLAYED = "bill summary page is displayed";
	public static final String DISPLAY_BILLING_PAGE = "Navigated to Billing Page";
	public static final String CLICK_VIEW_DETAILS = "Click View Details link";
	public static final String NO_EIP_DETAILS_PAGE = "User Is Not Navigated EIP Details Page";
	public static final String VERIFY_EQUIPMENT_PAGE = "EIP details page should be displayed";
	public static final String EQUIPMENT_PAGE_DISPLAYED = "EIP details page displayed";
	public static final String DISPLAY_EQUIPMENT_INSTALLMENT_PLAN_PAGE = "navigated to equipment installement plan page";
	public static final String CLICK_MAKE_PAYMENT = "Click Make Payment";
	public static final String NO_EIP_DEVICE_PAGE = "User Is Not Navigated To EIP Device Payment Page.";
	public static final String VERIFY_EXTRA_DEVICE_PAY_PAGE = "Verify extra device payment page";
	public static final String DISPLAY_DEVICE_PAY_PAGE = "navigated to extra device payment page";
	public static final String VERIFY_CLICK_CHECK_BOX = "Click Check box";
	public static final String DONE_CHECK_BOX = "selected successfully - check box";
	public static final String CARD = "Card";
	public static final String INSERT_PAYMENT_INFO = "Insert Payment information";
	public static final String ENTERED_PAYMENT_INFO = "Entered Payment information successfully";
	public static final String CLICK_NEXT = "Click Next";
	public static final String CLICK_ON_CANCEL = "Click Cancel button";
	public static final String NO_REVIEW_PAYMENT_PAGE = "User Is Not Navigated To Review Payment Page";
	public static final String VERIFY_REVIEW_PAYMENT = "Verify review payment page";
	public static final String DISPLAY_REVIEW_PAY_PAGE = "navigated to review payment page";
	public static final String ACCEPT_TC_CREDIT_CARD = "Accept terms for credit card";
	public static final String VERIFY_SUBMIT_BUTTON = "verify - click submit button";
	public static final String VERIFY_PAYMENT_MESSAGE = "Verify Payment Process Message";
	public static final String DISPLAY_PAYMENT_MESSAGE = "Successfully- appeared payment message";
	public static final String BILLCYCLE_DROPDOWN = "Verify bill cycle";
	public static final String VERIFY_BILLCYCLE_DROPDOWN_VALUES = "Bill cycle values 'current','next' are displayed";
	public static final String BILLCYCLE_DROPDOWN_VALUES = "Bill cycle values 'current','next' are not displayed";

	public static final String ACCOUNT = "Account";
	public static final String BILL_DELIVERY_OPTIONS_PAGE_VERIFY = "User is not navigated to Bill Delivery Options Page";
	public static final String TMOBILE_ID_PAGE_VERIFY = "User is not navigated to Tmobile ID Page";
	public static final String AUTO_PAY_PAGE_VERIFY = "User is not navigated to Auto Pay Page";
	public static final String AUTO_PAY_REVIEW_PAGE_VERIFY = "User is not navigated to Auto Pay Review Page";
	public static final String AUTO_PAY_SUCCESS_VERIFY = "Edit Easy Pay Success message not displayed";
	public static final String MANAGE_PAYMENT_OPTIONS_PAGE_VERIFY = "User is not navigated to Manage Payment Options Page";
	public static final String PRIVACY_NOTIFICATIONS_PAGE_VERIFY = "User is not navigated to Privacy Notifications Page";
	public static final String PHONE_CONTROLS_PAGE_VERIFY = "User is not navigated to Phone Controls Page";
	public static final String VERIFY_PHONE_CONTROLS_EDIT_SUCCESS = "User is unable to edit phone controls";
	public static final String VERIFY_TMOBILE_PREFERENCE_EDIT_SUCCESS = "User is unable to edit Tmobile Preferences";
	public static final String DEVICE_LOST_STOLEN_VERIFY = "User is not navigated to Device Lost or Stolen Flow Page";
	public static final String DEVICE_LOST_STOLEN = "Lost or Stolen Device";
	public static final String VERIFY_DEVICE_LOST_STOLEN_PAGE = "Lost or Stolen Device Page";
	public static final String DEVICE_LOST_STOLEN_PAGE_VERIFY = "User is unable to edit Device Lost or Stolen Flow";
	public static final String CUSTOMER_INFORMATION_PAGE_VERIFY = "User is not navigated to Customer Information Page";
	public static final String CUSTOMER_NAME_VERIFY = "Customer Name is not edited";
	public static final String CUSTOMER_EMAIL_VERIFY = "Customer Email is not edited";
	public static final String VERIFY_FILTERED_DEVICES = "Verify filtered devices";

	public static final String AUTO_PAY_SUCCESS = "Edit Easy Pay Success";
	public static final String EDIT_PAPPERLESS_BILLING = "Click on the 'Profile' tab and choose 'Edit Paperless Billing' Option.";
	public static final String BILL_DELIVERY_OPTIONS_PAGE = "Bill Delivery Settings Page is displayed.";
	public static final String VERIFY_PHONE_PAGE_LINKS = "Verify all links on phone page";
	public static final String PHONE_PAGE_LINKS_VERIFIED = "all links on phone page are verified";
	public static final String VERIFY_PAY_BILL_PAGE = "Verify Pay Bill Page";
	public static final String VERIFY_EASY_PAY = "Verify Easy/Auto Pay Page";
	public static final String VERIFY_USAGE_PAGE = "Verify Usage page";
	public static final String VERIFY_ACC_HISTORY_PAGE = "Verify Account history page";
	public static final String ACC_HISTORY_PAGE = "Account history page is Displayed";
	public static final String ACC_HISTORY_PAGE_ERROR = "Account history page is not displayed";
	public static final String LOGIN_SUCCESS = "Successfully Logged In to application";
	public static final String VERIFY_LOGIN = "Login to application";
	public static final String VERIFY_BLOCK_CHARGE_INTERNATIONAL = "Verify block Charge International Roaming";
	public static final String BLOCK_CHARGE_INTERNATIONAL = "On/Off settings edited for block Charge International Roaming";
	public static final String VERIFY_BLOCK_CHARGE = "Verify block Charge Roaming";
	public static final String BLOCK_CHARGE = "On/Off settings edited for block Charging ";
	public static final String VERIFY_TEMPORARY_SUSPENSION = "Verify Temporary Suspension flow  for a IR customer";
	public static final String TEMPORARY_SUSPENSION_SUCCESS = "Temporary Suspension flow should be successfully performed and confirmation page should be displayed";
	public static final String CLICK_REPORT_LOST_STOLEN = "Click on REPORT Device as LOST / STOLEN";
	public static final String LOST_STOLEN_PAGE = "Report Device as lost or Stolen Page must be displayed.";
	public static final String SUBMIT_LOST_STOLEN = "Check User able to Submit the request for Lost/Stolen Device  including suspending the service";
	public static final String SUBMIT_LOST_STOLEN_SUCCESS = "User Should  Submit the request for Lost/Stolen including suspending the service";
	public static final String LOST_STOLEN_REVIEW_PAGE = "The Lost /Stolen device review Page is selected.";
	public static final String LOST_STOLEN_SUCCESS = "Device Lost or Stolen request is successful.";
	public static final String CONFIRM_LOST_STOLEN = "In the 'Review your changes' Page, check the 'Terms and Conditions' and click on the Submit Button.";
	public static final String VERIFY_USAGE_OVERVIEW_PAGE = "usage overview page should display";
	public static final String USAGE_OVERVIEW_PAGE = "Usage Overview Page is not displayed";
	public static final String SECURITY_QUESTIONS = "Questions displayed, answers masked.";
	public static final String CLICK_SECURITY_QUESTIONS_LINK = "Click on Change Security questions";
	public static final String MODIFY_SECURITY_QUESTIONS = "Enter new answer and hit save changes";
	public static final String MODIFY_SECURITY_QUESTIONS_SUCCESS = "Upon successful change of questions/answers, user notified via free SMS";
	public static final String CHANGE_PSWD_SUCCESS = "Password successfully changed. SMS sent confirming that password was changed successfully";
	public static final String BILLING_DETAILS_SAVED = "updated billing details";
	public static final String SERVICES_PAGE_VERIFY = "Change Services Page is not displayed";
	public static final String BLOCK = "Block";
	public static final String VERIFY_BLOCKING_SERVICE = "Verify blocking service SOC";
	public static final String BLOCKING_SERVICE_SUCCESS = "blocking service SOC is Displayed";
	public static final String BLOCKING_SERVICE_VERIFY = "blocking service SOC is not displayed";
	public static final String BILL_RADIO_SELECTED = "Bill in the mail radio button is selected.";
	public static final String SELECT_BILL_RADIO_BUTTON = "Select the 'How would you like to receive your Bill' as 'Bill in the mail'.";
	public static final String PLAN_PAGE_VERIFY = "Verify plan page is not displayed";
	public static final String ADDLINE_NON_ELIGIBLE_VERIFY = "This User is eligible for Add A Line. Ineligible User is Expected.";
	public static final String ADDLINE_NON_ELIGIBLE_TEXT = "'To Proceed with Add a Line Set Primary Account Holder for this account' message is displayed";
	public static final String CLICK_EASY_PAY = "Click on Easy Pay";
	public static final String FILL_CREDIT_CARD_DETAILS = "enter card detils and clik on next";
	public static final String ERROR_SUBMIT_BUTTON = "submit button not displayed";
	public static final String E911_ADDRESS_SAVED_SUCCESS = "Updated E911 Address successfully";
	public static final String EDIT_E911_ADDRESS = "Edit E911 Address";
	public static final String VERIFY_PHONES_PAGE = "Navigate to Phones Page";
	public static final String TEST = "test";
	public static final String GMAIL = "@gmail.com";
	public static final String EDIT_CUSTOMER_EMAIL = "Edit Customer Email";
	public static final String PHONE_PAGE_VERIFY = "User Is Not Navigated To Phone Page.";
	public static final String CLICKED_RESTORE_SERVICE = "Restore Service link is clicked";
	public static final String CLICK_RESTORE_SERVICE = "Click on Restore Service link";
	public static final String RESTORE_SERVICE_SUCCESS = "Suspended Service restored successfully";
	public static final String CONFIRM_RESTORE_SERVICE = "Confirm Restore suspended Service";
	public static final String CLICKED_ORDER_STATUS_LINK = "Clicked on Order status link";
	public static final String CHECK_ORDER_STATUS_LINK = "Check and Click Order Status link on phone page";
	public static final String AUTO_PAY_PAGE = "User is navigated to Auto/Easy Pay Page";
	public static final String CLICK_AUTO_PAY_EDIT = "Click Auto Pay Edit link/Edit EasyPay";
	public static final String TRADEIN_NOW_LINK = "Verify trade in now link";
	public static final String TRADEIN_STATUS_LINK = "Verify trade in status link";
	public static final String CHECK_TRADEIN_NOW_LINK = "Trade in now link should be disabled";
	public static final String CHECK_TRADEIN_STATUS_LINK = "Trade in status link should be disabled";
	public static final String ON = "ON";
	public static final String SUMMARY_BILL_RADIO = "Summary Bill radio button is selected.";
	public static final String SELECT_SUMMARY_BILL_RADIO = "In 'Select Reminder method' select the bill format as 'Summary Bill(FREE)'.";
	public static final String CLICK_SAVE = "Click on 'Save' button.";
	public static final String PAPERLESS_BILLING = "Paperless Billing is saved for the account.";

	public static final String LINE_SELECTER_DROPDOWN = "Verify Image in Line selector Drop down";
	public static final String CHECK_LINE_SELECTER_DROPDOWN_IMAGE = "Line selector Image should be verified";
	public static final String LINE_SELECTER_DROPDOWN_NUMBER = "Verify Device Nunmber in Line selector Drop down";
	public static final String CHECK_LINE_SELECTER_DROPDOWN_NUMBER = "Verify Device Nunmber in Line selector Drop down";
	public static final String LINE_SELECTER_DROPDOWN_NAME = "Verify Device name in Line selector Drop down";
	public static final String CHECK_LINE_SELECTER_DROPDOWN_NAME = "Line selector device name should be verified";

	public static final String PLAN_TEXT = "Verify plan text";
	public static final String VERIFY_PLAN_TEXT = "Plan text should be displayed";
	public static final String PLANDETAILS_LINK = "Verify plan details link";
	public static final String VERIFY_PLANDETAILS_LINK = "Verify that Plan details link is displayed and click";
	public static final String COPYRIGHTS_TEXT = "Copyrights text is displayed";
	public static final String VERIFY_COPYRIGHTS_TEXT = "Copyrights text should be displayed";
	public static final String PREMIUM_SERVICES_LINKS = "Premium services are displayed";
	public static final String VERIFY_PREMIUM_SERVICES = "Premium services should be displayed";
	public static final String COMMON_SERVICES_LINKS = "Common services are displayed";
	public static final String VERIFY_COMMON_SERVICES = "Common services should be displayed";
	public static final String BENEFITSPAGE_ERROR_MESSAGE = "Benefits Sub Header Text is not displayed";
	public static final String PLANHEADER_ERROR_MESSAGE = "Plan Header is not displayed in Benefits Page";
	public static final String PLANDETAILS_ERROR_MESSAGE = "Plan Details are not displayed in Benefits Page";
	public static final String COMMON_SERVICES_ERROR_MESSAGE = "Common Services are not displayed in Benefits Page";
	public static final String PREMIUM_SERVICES_ERROR_MESSAGE = "Premium Services are not displayed in Benefits Page";
	public static final String COPYRIGHTS_ERROR_MESSAGE = "Copyrights Text is not displayed in Benefits Page";
	public static final String COVERAGE_PAGE_ERROR_MESSAGE = "Coverage Page is not displayed";
	public static final String CONTACT_PAGE_ERROR_MESSAGE = "Contact US page is not displayed";
	public static final String ABOUTTMOBILE_PAGE_ERROR_MESSAGE = "About T-Mobile page is not displayed";
	public static final String SETUP_CALL_MODEL_DROPDOWN = "Validate dropdown values";
	public static final String VERIFY_SETUPCALLMODEL_DROPDOWN_VALUE = "Verify setup call model dropdown value, ";
	public static final String BILLINGGROUP_BOX = "Verify billing highlights group box";
	public static final String VERIFY_BILLINGGROUP_BOX = "Billing highlighs group box displayed";
	public static final String VERIFY_VIEW_USAGE_SUMMARY = "Verify view usage summary";
	public static final String REVIEWS_TAB = "Verify reviews tab";
	public static final String ELEMENT_VERIFICATION_MESSAGE = "is displayed";
	public static final String ENTER_SEARCH_DATA = "Enter search data as,";
	public static final String ENTERED_SEARCH_DATA = "Entered search data,";

	public static final String DEVICES_TAB = "Click devices tab";
	public static final String VERIFY_DEVICES_TAB = "devices tab is clicked";

	public static final String ACCESSORIES_TAB = "Click accessories tab";
	public static final String VERIFY_ACCESSORIES_TAB = "Accessories tab is clicked";
	public static final String PAGINATION = "Pagination not displayed-accessories";

	public static final String BILL_IN_MAIL = "mail";
	public static final String PAPERLESSBILL = "paperless";

	public static final String VERIFY_MANAGE_TAB = "Move Cursor to Manage Tab in horizantal menu";
	public static final String MANAGE_TAB_DISPLAY = "Cursor moved to manage tab and manage menu displayed";
	public static final String VERIFY_EDIT_JUMP_SOC = "Choose Jump soc in add services";
	public static final String SELECTED_JUMP_SOC = "Jump Soc is selected in add services";
	public static final String VERIFY_JUMP_IN_MINICART = "Verify jump exists in mini Cart";
	public static final String DISPLAY_JUMP = "Jump displayed in mini cart";
	public static final String VERIFY_REVIEW_SOC_CHANGES = "verify soc changes reflected in review cart";
	public static final String DISPLAY_SOC_CHANGES_JUMP = "Jump Soc changes appeared in review cart";
	public static final String DISPLAY_SBB_SOC = "SBB SOC changes appeared in review cart";
	public static final String EDIT_SBB_SOC_SERVICES = "Edit SBB SOC services";
	public static final String ADDED_SBB_SOC = "Added sbb soc services";

	public static final String ADDED_15_SOC = "15";
	public static final String VERIFY_MORE_DETAILS = "Verify more details link in plan page";
	public static final String DONE_MORE_DETAILS = "All more details links are successfully verified";
	public static final String CHOOSE_REMOVE_DATA = "Click Remove data option";
	public static final String DATA_REMOVED = "successfully data removed";
	public static final String VERIFY_PLAN_SERVICE_IN_REVIEW = "Verify plan and services changes reflects in review cart page";
	public static final String DISPLAY_PLAN_SERVICE = "Changes in plan and Services are appeared in review page";
	public static final String CHANGE_TO_ONE_TIME_PAY = "Choose one time payment";
	public static final String SELECTED_ONE_TIME_PAY = "Selected successfully one time payment option";

	// Benefits page
	public static final String ERROR_BENEFITS_PAGE = "Benefits page not loaded";
	public static final String CLICK_STATESIDEINTERNATIONALTALK_MANAGE = "Click on StatesideInternationalTalk";
	public static final String VERIFY_STATESIDEINTERNATIONALTALK_MANAGE = "Verify StatesideInternationalTalk";
	public static final String ERROR_SERVICES = "Error: Voice services not displaying";
	public static final String VERIFY_SERVICES_PAGE = "Navigate to services page";
	public static final String DISPLAY_SERVICES_PAGE = "Services page displayed";

	public static final String CLICK_USGAE_BUTTON = "Click usage button";
	public static final String USAGE_OVERVIEW_VERIFY = "Usage Overview Page is not displayed";
	public static final String CLICK_COMMUNICATION_BUNDLE_MANAGE = "Click on CommunicationBundle";
	public static final String VERIFY_COMMUNICATION_BUNDLE_MANAGE = "Clicked on CommunicationBundle";
	public static final String ERROR_VOICE_SERVICES = "Error: Voice services not displaying";
	public static final String SWITCH_COMM_WINDOW = "Switch to CommunicationBundle Window";
	public static final String SUCCESS_SWITCH_COMM_WINDOW = "SUCCESS :Switched to CommunicationBundle Window";
	public static final String VERIFY_VOICE_PAGE = "Navigate to voice calls page";
	public static final String DISPLAY_VOICE_PAGE = "Voice Mail page displayed";
	public static final String TMOBILE_PURCHAGE_TEXT = "T Mobilt Purchages text is displaying for line $0.00";
	public static final String INDIVIDUAL_THIRD_PARTY_TEXT = "Individual Third Party text is displaying for line $0.00";

	public static final String BILLING_PAGE_VERIFY = "Billing Summary page is not displayed.";
	public static final String BILL_DETAILS_PAGE_VERIFY = "Bill Details page is not displayed.";
	public static final String NAVIGATE_TO_BILL_DETAILS_PAGE = "verify user is navigated to Bill Details page";
	public static final String BILL_DETAILS_VERBIAGE_TEXT = "To see details for additional lines on the account, please check with the Primary Account Holder.";
	public static final String BILL_DETAILS_VERBIAGE_VERIFY = "Verbiage Text is not displayed for Standard User";
	public static final String VERIFY_BILL_DETAILS_VERBIAGE_TEXT = "Verbiage Text is displayed for Standard User";
	public static final String VERIFY_VERBIAGE_TEXT = "Verify text for Verbiage Bill Details";
	public static final String UPDATE_FAILED = "Update Operation Failed because of application issue.";

	public static final String NO_CONFLICT_MESSAGE_JUMP_PHP = "conflict not occured while selecting Jump and PHP both at a time";
	public static final String VERIFY_CONFLICT_FOR_JUMP_PHP_SOCS = "Verify- Add Jump and Php Soc and only one can add at a time-add Php";
	public static final String PHP_ADDED = "Only PHP Soc Added -successfully";
	public static final String VERIFY_CONTINUE_PHP_CHANGE = "Click continue to choose PHP soc";
	public static final String DISPLAY_PHP_SOC = "Successfully added PHP SOC";
	public static final String SHOP_BY_TEXT_0PAY = "0";
	public static final String VERIFY_UPGRADE_TO_10GB_DATA = "Select 10 GB data service";
	public static final String SELECTED_10GB_DATA = "Selected 10GB data Service successfully";
	public static final String VERIFY_DISPLAY_EDIT_DATA_IN_REVIEW_PAGE = "Verify data changed is appeared in review page";
	public static final String DISPLAY_EDIT_DATA_IN_REVIEW_PAGE = "Data services change displayed in review page";
	public static final String CHANGE_SHIPMENT_OPTION_TO_UPS_EXPIDITE = "select shipment option to UPS expidite";
	public static final String SELECTED_UPS_SHIP_OPTION = "Successfully selected UPS Expidite shipment option";
	public static final String VERIFY_UPGRADE_TO_UNLIMITED_DATA = "Verify upgrade to unlimited data plan";
	public static final String SELECTED_UNLIMITED_DATA = "Selected Unlimited data Service successfully";
	public static final String CHANGE_SHIPMENT_OPTION_TO_3DAY = "Select shipment option to UpsExpress-3day shipment";
	public static final String SELECTED_UPS_EXPRESS_SHIP_OPTION = "Successfully selected UPS Express-3day shipment option";

	// CPS

	public static final String ERROR_AUTOPAY_TXT = "Error:Auto Pay Message not displayed";
	public static final String VERIFY_AUTOPAY_TXT = "Verify Auto pay Message";
	public static final String SUCCESS_AUTOPAY_TXT = "Success:Auto pay Message displayed";
	public static final String AUTO_PAY_ON = "successfully signed up for AutoPay";
	public static final String AUTO_PAY_OFF = "AutoPay Off is not displaying";
	public static final String PAYMENT_ARRANGMENTS_AUTOPAY_ALERT_MSG = "You will be automatically un-enrolled from AutoPay & will no longer receive your $5/mo. AutoPay discount. Re-enroll once your payment arrangement is complete";
	public static final String PA_AUTO_PAY_ALERT = "Auto pay alert message not displaying";
	public static final String AUTO_PAY_TEXT = "Auto pay text is not displaying";
	public static final String AUTO_PAY_MESSAGE = "You are currently enrolled in AutoPay and saving $5 per month message is not displaying";

	public static final String ERROR_PLAN_DESCRIPTION_PAGE = "Error:Plan description Page not loaded";
	public static final String CLK_MOREDETAILS_PLAN = "Click on More Details Plan";

	public static final String CLK_PLAN_LINK = "Click on Plan link";
	public static final String ERROR_PLAN_PAGE = "Error:Plan Page not loaded";
	public static final String VERIFY_CPS_VIEW_BTN = "CPS View button should be disabled";
	public static final String SUCCESS_CPS_VIEW_BTN = "Success:CPS View button is disabled";
	public static final String ERROR_CPS_VIEW_BTN = "Error:CPS View button is not disabled for B2B customer";
	public static final String CHANGE_SERVICES_ERROR = "Change Services button is not displaying";

	public static final String ERROR_RESTRICTED_CPS_VIEW = "Error:Change Plan button is displayed for restricted permissions";
	public static final String ERROR_STD_CPS_VIEW = "Error:Change Plan button is displayed for standard permissions";
	public static final String VERIFY_CHANGEPLAN_RESTRCICTED = "Verify change plan button";
	public static final String SUCCESS_CHANGEPLAN_RESTRCICTED = "Success: Change plan is not displayed";
	public static final String VERIFY_CHANGEPLAN_STD = "Verify change plan button";
	public static final String SUCCESS_CHANGEPLAN_STD = "Success: Change plan is not displayed";
	public static final String CLK_CHANGE_PLAN = "Click on Change Plan";
	public static final String ERROR_PLAN_DETAILS_PAGE = "Error:Plan Details Page not loaded";
	public static final String CLK_EXPAND_PLAN_FEATURES = "Clicked on expand Plan details";
	public static final String SUCCESS_PLAN_FEATURES = "Success:Plan details are displayed";
	public static final String CLK_COLLAPSE_PLAN_FEATURES = "Clicked on collapse Plan details";
	public static final String ERROR_EXPAND_PLAN_FEATURES = "Error:Plan features not expanded";
	public static final String ERROR_COLLAPSE_PLAN_FEATURES = "Error:Plan features not collapsed";

	public static final String CLK_CHANGE_SERVICES = "Click on Change Services";
	public static final String ERROR_MANAGE_SERVICES_PAGE = "Error:Manage Services Page not loaded";
	public static final String SELECT_SERVICES_CLK_NXT = "Add Services and clicked on Next";
	public static final String CLK_SUBMIT_CHANGES = "Click on Submit";
	public static final String VERIFY_SUBMIT_CHANGES = "Services verified and clicked on Submit";
	public static final String CLK_SELECT_CONTINUE = "Click on Select & Continue Plan";
	public static final String ERROR_DATA_DETAILS_PAGE = "Error:Data Details Page not loaded";
	public static final String CLK_CHANGE_DATA = "Click on Change data";
	public static final String REMOVE_FA_ADDING_DESCRIPTION="By adding Netflix Premium $13.99 with Fam Allowances ($19 value) to this line you are also adding it to other lines on your account. This service will be added to the following lines:";
	public static final String REMOVESTANDARD_FA_ADDING_CONFLCIT_DESCRIPTION="Netflix Standard $10.99 with Fam Allowances ($16 value)";
	public static final String REMOVE_FA_ADDING_CONFLCIT_DESCRIPTION="Netflix Premium $13.99 with Fam Allowances ($19 value)";
	public static final String STANDARDNETFLIX_ADDING_DESCRIPTION="By adding Netflix Standard $10.99 with Fam Allowances ($16 value) to this line you are also adding it to other lines on your account. This service will be added to the following lines:";
	public static final String REMOVE_FA_DESCRIPTION = "By removing Netflix $10.99 plus Family Allowances, ($16 value), you’ll lose Netflix on Us, unless you add Netflix Prem. $13.99 w/ Fam Allowance, ($19 value) for $3.00/mo";
	public static final String REMOVE_PREMIUMFA_DESCRIPTION = "By removing Netflix Premium, you’ll lose your access to Netflix Premium benefits. If you continue, be sure to add Netflix Basic (a $10.99 value) to avoid losing your Netflix access  and Family Allowance access";
	public static final String REMOVE_PREMIUM_DESCRIPTION = "By removing Netflix Prem. $13.99 w/ Fam Allowances ($19 value) you’ll lose Netflix on us, unless you add Netflix Std $10.99 w/ Fam Allowances ($16 value) on us. This change will result in immediate removal of T-Mobile as your method of payment, with potential charges to any previous method of payment on file with Netflix";
	
	public static final String ERROR_DATA_PASS_PAGE = "Error:Data Pass Page not loaded";
	public static final String CLK_ADD_DATAPASS = "Clicked on Add data pass";
	public static final String ERROR_DATA_PASS_CONFIRMATION_PAGE = "Error:Data Pass Confirmation Page not loaded";
	public static final String SUCCESS_ADD_DATAPASS = "Success:Data Pass Added";
	public static final String SUCCESS_REMOVE_DATAPASS = "Success:Data Pass Removed";
	public static final String ERROR_REMOVE_DATAPASS = "Error:Data Pass Not removed";

	public static final String VERIFY_NAME_USAGESUMMARY = "Shared line name is not displayed";
	public static final String USAGESUMMARY_NAME = "Shared line name is displayed in the usage summary";
	public static final String VERIFY_MSISDN_USAGESUMMARY = "Shared line MSISDN is not displayed";
	public static final String USAGESUMMARY_MSISDN = "Shared line MSISDN is displayed in the usage summary";
	public static final String COST_BREAKDOWN_ERROR = "Cost Break down text is not displaying";
	public static final String COST_PLANTEXT_ERROR = "Plan text is not displaying";
	public static final String COST_PLAN_NAME_ERROR = "Plan name is not displaying";
	public static final String COST_DOLLARS_ERROR = "Dollars number is not displaying";
	public static final String COST_AUTOPAY_ERROR = "Auto Pay is not displaying";
	public static final String BENEFITS_PAGE_VERIFICATION_SIGNUP = "Sign up for Netflix on us";

	// billing

	public static final String VERIFY_PAYMENT_HISTORY_PAGE = "Verify payment history page";
	public static final String NO_PAYMENT_HISTORY_PAGE = "User Is Not Navigated To Payment History Page.";
	public static final String NO_VIEW_BILLED_USAGE_DETAILS_PAGE = "User Is Not Navigated To View Billed Usage Details Page.";
	public static final String VERIFY_VIEW_BILLED_USAGE_DETAILS_PAGE = "Verify view billed usage details page.";
	public static final String VERIFY_TMOBILE_PURCHASES_TAB = "Verify t-mobile purchases tab.";
	public static final String VERIFY_CALL_DEATIL_RECORDS_PAGEHK = "Verify call detail records pagehk.";
	public static final String NO_CALL_DEATIL_RECORDS_PAGEHK = "User Is Not Navigated To Call Detail Records Pagehk.";
	public static final String VERIFY_TABS = "Verify tabs.";
	public static final String NAVIGATED_TO_TAB = "Navigated To the tab.";
	public static final String NO_USAGE_OVERVIEW_PAGE = "Usage Overview Page is not displayed";
	//public static final String CLICK_VIEW_ALL_USAGE_DETAILS = "CLick view all usage details";
	public static final String VERIFY_VIEW_ALL_USAGE_DETAILS = "view all usage details Link should display";
	public static final String VIEW_ALL_USAGE_DETAILS_DISPLAYED = "Verify view all usage details Link displayed";
	public static final String NO_VIEW_ALL_USAGE_DETAILS_LINK = "view All Usage Details Link not displayed";
	public static final String CLICK_VOICE_TAB = "Click voice tab";
	public static final String CLICK_FILTER_VOICE_TAB = "Click filter in voice tab";
	public static final String VERIFY_VOICE_INFORMATION = "Filtered voice information should display";
	public static final String CLICK_MESSAGING_TAB = "Click messaging tab";
	public static final String VERIFY_MESSAGING_TAB = "messaging tab details displayed";
	public static final String VERIFY_MSG_INFORMATION = "Filtered message information should display";
	public static final String VERIFY_DATA_TAB = "data tab displayed";
	public static final String VERIFY_DATA_INFORMATION= "Filtered data information should display";
	public static final String USAGE_OVERVIEW_PAGE_DISPLAYED = "Usage Overview Page displayed";
	public static final String CALL_DETAIL_RECORDS_PAGE_DISPLAYED = "Call details records page displayed";
	public static final String USAGE_RECORDS_DOWNLOADED = "usage records downloaded";
	
	public static final String CLICK_LOGOUT_BUTTON = "Click logout button";
	public static final String CLICK_USAGE_IMAGE_LEGACY = "Click usage image in legacy";
	public static final String VERIFY_USAGE_HISTORY_LEGACY = "Verify your usage history in legacy";
	public static final String VERIFY_VOICE_USAGE = "Verify voice usage details";
	public static final String NO_VOICE_USAGE = "voice usage details not displayed";
	public static final String VERIFY_VOICE_FILTER_DATA = "Verify voice filter data";
	public static final String VERIFY_DOWNLOAD_USAGE_RECORDS_LINK = "Verify download usage records link";
	public static final String DOWNLOAD_USAGE_RECORDS_LINK_DISPLAYED = "download usage records link displayed";
	public static final String NO_DOWNLOAD_USAGE_RECORDS_LINK = "download usage records link not displayed";
	public static final String CLICK_ON = "Click On";
	public static final String VERIFY_BILL_CYCLE_INDICATOR = "Verify bill cycle indicator";
	public static final String COMPLETED = " completed ";
	public static final String SELECT_BILL_CYCLE = "Select bill cycle";
	public static final String CLICK_DOWNLOAD_PDF = "Click download PDF";
	public static final String CLICK_PAPER_LESS_SIGNUP_LINK = "Click paper less sign up link";
	public static final String VERIFY_PAPER_LESS_BILL = "Verify paper less bill";
	public static final String PAPER_LESS_BILL_DISPLAYED = "paper less bill section displayed";
	public static final String VERIFY_BILL_IN_MAIL = "Verify bill in mail";
	public static final String NO_BILL_IN_MAIL = "bill in mail not displayed";
	public static final String BILL_IN_MAIL_DISPLAYED = "bill in mail  radio button is displayed";
	public static final String VERIFY_CHECK_PAPER_LESS_LINK = "paper less billing section should be  displayed";
	public static final String PAPER_LESS_LINK_DISPLAYED = "paper less billing section displayed";
	public static final String VERIFY_MSG_USAGE = "Verify message usage details";
	public static final String MSG_USAGE_DEATILS_DISPLAYED = "Verify message usage details displayed";
	public static final String NO_VERIFY_MSG_USAGE = "message usage details not displayed";
	public static final String VERIFY_DATA_USAGE = "Verify data pass usage deatils";
	public static final String NO_DATA_USAGE = "data pass usage deatils not displayed";
	public static final String DATA_USAGE_DISPLAYED = "data pass usage deatils displayed";
	public static final String VERIFY_DATA_STASH = "Verify Data Stash link is displayed";
	public static final String NO_VERIFY_DATA_STASH = "Verify Data Stash link is not  displayed";
	public static final String DATA_STASH_LINK_DISPLAYED = "Data Stash link is displayed";
	public static final String CLICK_USAGE_BUTTON = "click on usage link";
	public static final String NO_ONE_TIME_PAYMENT_PAGE = "User Is Not Navigated To One Time Payment Page";
	public static final String VERIFY_ONE_TIME_PAYMENT_PAGE = "One time Payment page displayed";
	
	public static final String VERIFY_CREDIT_CARD = "Verified credit card number";
	public static final String VERIFY_ZIP_CODE = "Verify zip code";
	public static final String VERIFY_INVALID_CARD_ERROR_MSG = "Verify invalid credit card error message";
	public static final String VERIFY_INVALID_CHECKING_ACCOUNT_ERROR_MSG = "Verify invalid checking account error message";
	public static final String VERIFY_EIP_HEADER_TEXT = "Verify eip header text";
	public static final String VERIFY_EIP_BALANCE = "eip balance should be displayed";
	public static final String EIP_BALANCE_DISPLAYED = "eip balance displayed correctly";
	public static final String VERIFY_ACCOUNT_HISTORY_TEXT_ALERT = "Verify get account history text alert";
	public static final String VERIRY_ALERTSECTION = "Verify alert section";
	public static final String VERIFY_DOCUMENTS_TAB_DISPLAYED = "Verify documents tab  should be displayed";
	public static final String DOCUMENTS_TAB_DISPLAYED = "Documents tab is displayed";
	public static final String NO_DOCUMENTS_TAB_DISPLAYED = "Documents  tab is not displayed";
	public static final String VERIFY_AUTO_REVIEW_PAGE = "Verify auto review page";
	public static final String CREDIT_CARD_TERMS_CONDITIONS = "Check credit card terms and conditions";
	public static final String CLICK_CREDIT_CARD_SUBMIT_BTN = "Click credit card submit button";
	public static final String CLICK_TMOBILE_GLOBAL_ICON = "Click t-mobile global icon";
	public static final String VERIFY_AUTOSTATE_ON =  "Auto Pay not turned into 'ON'";
	public static final String VERIFY_AUTOSTATE_OFF = "Auto Pay not turned into 'OFF'";
	public static final String VERIFY_VALIDATE_ONE_INSTALLEMENT = "Verify validate one installement";
	public static final String ONE_INSTALLEMENT_AMOUNT_EQUAL_BALANCE = "One installement amount equal to balance";
	public static final String INSTALLEMENT_ONE_AMOUNT_NOT_EQUAL_BALANCE = "Installment one amount not equals to balance";
	public static final String VERIFY_VALIDATE_TWO_INSTALLEMENT = "Verify validate two installements";
	public static final String INSTALLEMENT_TWO_AMOUNT_NOT_EQUAL_BALANCE = "Installment  amount not equals to balance";
	public static final String TWO_INSTALLEMENT_AMOUNT_EQUAL_BALANCE = "two installement amounts equal to balance";
	public static final String VERIFY = "Verify";
	public static final String UPGRADE_ELIGIBLE = "You are eligible for an upgrade.";
	public static final String VERIFY_ADD_LINE_UPGRADE = "Select add a line in - select a line page";
	public static final String SELECT_NEW_PLAN = "Select a plan for added Line";
	public static final String ADDED_NEW_PLAN = "Added new plan for the new line added";
	public static final String VERIFY_REVIEWS = "Verify Reviews for selected device";
	public static final String REVIEWS_TAB_VERIFY = "Reviews Tab is not displayed in Product details page.";
	public static final String VERIFY_PHONEDETAILS_PAGE = "Verify phone details page";
	public static final String VERIFY_HOME_PAGE_ERROR = "Home page is not displayed";
	public static final String VERIFY_CONTACTUS_PAGE_ERROR = "Contact us page is not displayed";
	public static final String VERIFY_TERMSANDCONDITIONS_PAGE_ERROR = "Terms and conditions page is not displayed";
	public static final String VERIFY_MY_DIGITS_PAGE_ERROR = "My Digits page is not displayed";
	public static final String VERIFY_MY_CLAIM = "My Claim";
	public static final String VERIFY_TITLE_FILE_CLAIM = "File or Track My Claim";
	public static final String TEMPORARILY_SUSPENDED = "temporarily suspended";
	public static final String TEMPORARILY_SUSPENDED_VERIFY = "Temporary suspension text is not displayed";
	public static final String DATA_INFO = "Total on-network data";
	public static final String DATA_INFO_INFO = "Total on-network should be displayed";
	public static final String MSISDN = "Phone Number";
	public static final String VERIFY_MSISDN = "Verify Phone Number";
	public static final String MSISDN_USERNAME = "Msisdn user name";
	public static final String VERIFY_MSISDN_USERNAME = "Verify msisdn Phone Number";
	public static final String ORDER_STATUS_ERROR = "Order not avaliable";
	public static final String ORDER = "Verify order";
	public static final String ORDER_DETAILS = "Order should be displayed";
	public static final String SWITCH_ON = "switch on";
	public static final String ATTR_CLASS = "class";
	public static final String VERIFY_TRADE_IN_VALUE = "Trade in value is displayed";
	public static final String TRADE_IN_VALUE = "Trade in value is not displayed";
	public static final String DEVICE_SUSPENDED = "Device suspended";
	public static final String SUSPEND_TEXT = "Suspend text not appeared";
	public static final String FOUND_DEVICE = "I found my device";
	public static final String CLEARCART_LINK = "Clear cart link is not displayed";
	public static final String VERIFY_CLEARCART_LINK = "Verify Clear cart link is displayed";
	public static final String PRINTCART_LINK = "Print cart link is not displayed";
	public static final String VERIFY_PRINTCART_LINK = "Verify Print cart link is displayed";
	public static final String DOWNPAYMENT_LINK = "Down Payment Option link is not displayed";
	public static final String VERIFY_DOWNPAYMENT_LINK = "Verify Down Payment Option link is displayed";
	public static final String SHIPPINGDETAILS_LINK = "Shipping Details Option link is not displayed";
	public static final String DEVICEIMAGE_LINK = "Device Image Option link is not displayed";
	public static final String VERIFY_DEVICEIMAGE_LINK = "Verify Device Image Option link is displayed";
	public static final String SHIPPINGOPION_LINK = "Shipping Option link is not displayed";
	public static final String VERIFY_SHIPPINGOPION_LINK = "Verify Shipping Option link is displayed";
	public static final String NEWSIM_CARD_TEXT = "Insert the new SIM card into your phone";
	public static final String VERIFY_NEWSIM_CARD_TEXT = "Verify text of insert the new SIM card into your phone";
	public static final String VERIFIED_NEWSIM_CARD_TEXT = "Verified SIM card Text in your phone";
	public static final String ACTIVATE_SIM_TEXT = "Activate your new SIM card";
	public static final String VERIFY_ACTIVATE_SIM_TEXT = "Verify text of Activate your new SIM card into your phone";
	public static final String VERIFIED_ACTIVATE_SIM_TEXT = "Verified Activate your new SIM card in your phone";
	public static final String FIXEDBATTERY_NOT_DISPLAYED = "Fixed battery dialog box is not displayed";
	public static final String VERIFY_FIXEDBATTERY_DIALOGBOX = "Verify Fixed battery dialog box is displayed";
	public static final String VERIFY_FIXEDBATTERY_DIALOGBOX_DISPLAYED = "Fixed battery dialog box is displayed";
	public static final String REMOVABLEBATTERY_NOT_DISPLAYED = "Removable battery dialog box is not displayed";
	public static final String VERIFY_REMOVABLEBATTERY_DIALOGBOX = "Verify Removable battery dialog box is displayed";
	public static final String VERIFY_REMOVABLEBATTERY_DIALOGBOX_DISPLAYED = "Removable battery dialog box is displayed";
	public static final String INFORMATION_MODAL_TEXT = "You’ll now be directed to Assurant’s claims website";
	public static final String NO_INFORMATION_MODAL_TEXT = "Information Modal text is not displayed";
	public static final String VERIFY_INFORMATION_MODAL_TEXT = "Verify Information Modal text";
	public static final String DISPLAY_INFORMATION_MODAL = "successfully displayed Information Modal text";
	public static final String VERIFY_ADDITTIONAL_INFO_MODAL_CONTENTS = "Verify additional info modal contents";
	public static final String NO_ADDITIONAL_INFO_MODAL_CONTENTS = "Additional Info modal contents is not displayed";
	public static final String DISPLAY_ADDITIONAL_INFO_MODAL_CONTENTS = "Successfully displayed addtional info modal contents";
	public static final String NO_CONTINUE_CANCEL_INFO_MODAL_PAGE = "Continue and Cancel button are not displayed";
	public static final String VERIFY_CONTINUE_CANCEL_INFO_MODAL_PAGE = "Verify Continue and Cancel buttons";
	public static final String DISPLAY_CONTINUE_CANCEL_INFO_MODAL_PAGE = "Successfully displayed continue and cancel buttons";
	public static final String CLK_PROFILE = "Click on Profile Link";
	public static final String CLK_TMOBILEID = "Click on TMobileID tab";
	public static final String SUCCESS_VERIFY_TMOBILEID = "TMobile ID section displayed";
	// public static final String CALLBACK_TEXT = "You have a callback
	// scheduled";
	public static final String CALLBACK_TEXT_MOBILE = "YOUR CALLBACK HAS BEEN SCHEDULED";
	public static final String CALLBACK_TEXT_DESKTOP = "You have a callback scheduled";
	public static final String VERIFY_CUSTOM_ACC_HISTORY = "Verified Account history with custom dates.";
	public static final String SELECT_BY_INDEX = "Index";
	public static final String SELECT_BY_TEXT = "Text";
	public static final String SELECT_BY_VALUE = "Value";
	public static final String VERIFY_CHECKOUT_PAGE = "Verify Checkout page is displayed.";
	public static final String SHOPPAGE_VOICELINE = "Voice";

	public static final String WITHOUT_AUTO_PAY = "Without Auto Pay Link Not Visible";
	public static final String WITH_AUTO_PAY = "With Auto Pay Link Not Visible";
	public static final String SIGNUP_AUTO_PAY = "Signup Auto Pay Not Visible";
	public static final String MONTHLY_AUTOPAY_TEXT = "with AutoPay";
	public static final String AUTOPAY_DISCOUNT_TEXT = "With Autopay Discount";

	//public static final String LEASEDETAIILS_PAGE_ERROR = "Lease details page is not displayed";
	//public static final String DEVICEPAYMENT_PAGE_ERROR = "Device payment is not displayed";

	public static final String CHANGESIM_PAGE_ERROR = "Change sim page is not displayed";
	public static final String CHANGESIM_ERROR_MESSAGE = "To activate your new SIM card, please contact Customer Care by dialing 611 from your T-Mobile phone or toll free at 1 (800) 937-8997";

	public static final String BILLING_ADDR_EDIT_ERROR = "Error: Profile Edit Billing Address details";

	public static final String PHONEPAGE_TRADEIN_LINK_ERROR = "Trade-in link is displayed";
	public static final String PHONEPAGE_TRADEINSTATUS_LINK_ERROR = "Trade-in status link is displayed";

	public static final String PHONEPAGE_TRADEIN_LINK = "Verify Trade-in link ";
	public static final String PHONEPAGE_TRADEINSTATUS_LINK = "Verify Trade-in status link";
	public static final String FILL_BANK_ACCOUNT_DETAILS = "Fill bank account detils for easy pay setup.";
	public static final String CLICK_TMOBILE_ID_PAGE = "Click on Tmobile ID settings link";

	public static final String PLP_PAGE_ERROR = "Plp page is not displayed";
	public static final String VERIFY_PLP_PAGE = "Verify PLP Page";
	public static final String PLP_PAGE_HEADER = "Navigated To Product List Page.";

	public static final String UPDATED_BILL_DELIVERY_OPTIONS = "Updated Billing delivery options";
	public static final String VERIFY_BILLING_PREF_PAGE = "Verify Billing preferences page is displayed";
	public static final String MODIFY_EASY_PAY = "Either Discontinue or Setup easy pay is successful";
	public static final String VERIFY_PRIVACY_NOTIFICATIONS_PAGE = "Verify Privacy Notifications page is displayed";
	public static final String VERIFY_PHONE_CONTROLS_PAGE = "Verify Phone controls page";
	public static final String DEVICE_LOST_STOLEN_SUCCESS = "Device Lost or Stolen Flow is completed successfully";
	public static final String EDIT_CUSTOMER_NAME = "Edit Customer Name and save";
	public static final String VERIFY_CUST_INFO = "verify updated Customer Information is displayed";

	//public static final String VERIFY_AUTO_PAY_BUTTON = "Verify AutoPay Button";
	public static final String VIEW_PLAN_DETAILS_LINK_ERROR = "View Plan Details link is not displayed on Home Page.";
	public static final String CHANGE_BINGE_ON_OFF = "Modified Binge On/Off status";
	//public static final String EXPECTED_TEST_STEPS = "TestSteps | Expected Results:";
	//public static final String ACTUAL_TEST_STEPS = "Actual Results:";
	//public static final String CLICK_PAY_BILL = "Click Pay bill button on home page";
	public static final String CLICK_PLAN_ICON = "Click Plan button";
	public static final String CLICK_ALERTS_AND_ACTIVITIES = "Click Alerts and Activities link";
	public static final String CLICK_PROFILE_LINK = "Click Profile link";
	public static final String CLICK_JUMP_UPGRADE_BUTTON = "Click JUMP upgrade button";
	public static final String ANSWER_QUESTIONS = "Answer all the questions and Click continue button";
	//public static final String CLICK_CONTINUE_BUTTON = "Click continue button";
	//public static final String VERIFY_JUMP_DEVICE_VERIFICATION_PAGE = "Verify Jump device verification page.";
	public static final String CLICK_MORE_DETAILS_LINK = "Click More Details link on Order status page";
	public static final String VERIFY_TRANSACTION_STATUS = "Verify transaction details section of Order.";
	public static final String VERIFY_PAYMENT_STATUS = "Verify Payment details section of Order.";
	public static final String VERIFY_SHIPPING_STATUS = "Verify Shipping details section of Order.";
	//public static final String CLICK_CURRENT_CHARGES_ROW = "Click current charges row on billing page";
	public static final String PLAN_LEGALESE_DATAPASS_ANDROID = "Limited time offer; subject to change. Taxes and fees not already included additional. Compatible device required; not all plans or features available on all devices. Data Passes: Does not include voice or messaging. Post-paid only; pass charges will appear on bill statement for effective date of the pass. Limit of 2 active passes per account in a particular pass category; device pass use dates may not overlap. May purchase new pass in same category prior to expiration of active pass if data allotment has been reached; unless pass includes use at reduced speeds. Partial megabytes rounded up. Full speeds available up to specified data allotment, then slowed to up to 2G speeds. No domestic or international roaming on Domestic Pass, unless Pass is specifically for roaming. International Pass use requires qualifying plan; usage does not impact Mobile Internet plan data allotment. Service available for time/usage amount provided by pass. For time period, a day is 12:00 a.m. to 11:59 p.m. Pacific Time (PT). Credit approval and qualifying agreement required; deposit may apply. Smartphone Mobile Hotspot (Tethering/Wi-Fi Sharing): Plan data allotment applies. No Roaming, unless Pass is specifically for roaming. Use of connected device subject to T-Mobile Terms and Conditions. Must use device manufacturer or T-Mobile feature. Network Management: Service may be slowed, suspended, terminated, or restricted for misuse, abnormal use, interference with our network or ability to provide quality service to other users, or significant roaming. Customers who use 30GB of data in a bill cycle will have their data usage de-prioritized compared to other customers for that bill cycle at locations and times when competing network demands occur, resulting in relatively slower speeds. See T-Mobile.com/OpenInternet for details. LTE is a trademark of ETSI.";
	public static final String FILTER_DEVICE_ERROR = "Filtered device is not displayed";

	public static final String VERIFY_FILTERED_DEVICE = "Verify filtered device";
	public static final String ACCESSORIES_PAGE_ERROR = "Accessories page is not displayed";
	public static final String VERIFY_ACCESSORIES_PAGE = "Accessories page is displayed";
	public static final String PRICES_SORTING_ORDER = "Verify prices sorting order";
	//public static final String PRICE_LOW_TO_HIGH = "Price low to high";
	//public static final String PRICE_HIGH_TO_LOW = "Price high to low";
	//public static final String PRODUCT_TO_FILTER = "Samsung";
	//public static final String VERIFY_PRICES_LOWTOHIGH = "Prices are not in the low to high order";
	//public static final String VERIFY_PRICES_HIGHTOLOW = "Prices are not in the high low order";

	public static final String FIXED_BATTERY_POPUP = "Fixed battery pop-up is not displayed";
	public static final String REMOVABLE_BATTERY_POPUP = "Removable battery pop-up is not displayed";

	public static final String VERIFY_FIXED_BATTERY_POPUP = "Fixed battery pop-up is displayed";
	public static final String VERIFY_REMOVABLE_BATTERY_POPUP = "Removable battery pop-up is displayed";
	public static final String VERIFY_CHANGESIM_PAGE = "Verify change sim page";
	public static final String FIXED_BATTERY_POPUP_ERROR = "Fixed battery pop-up is not displayed";
	public static final String REMOVABLE_BATTERY_POPUP_ERROR = "Removable battery pop-up is not displayed";
	public static final String OFFER_SEARCH_PAGE_ERROR = "Offer search page is not displayed";
	public static final String VERIFY_OFFER_SEARCH_PAGE = "Offer search page is displayed";

	public static final String NOT_YOUR_DEVICE = "Selected device is not displayed";
	public static final String YOUR_DEVICE = "Selected device is displayed";
	
	public static final String CLICK_PRIVACY_NOTIFICATIONS_LINK = "Click on Privacy & notifications link";
	public static final String ADDALINE_VERIFY = "Add a Line Eligibility/Non-Elegibility Text is not displayed in Shop Page.";
	public static final String ADDRESS_UPDATE_VERIFY = "Address is not updated/displayed correclty.";
	public static final String EDIT_MARKETING_COMMUNICATIONS_VERIFY = "Marketing & Communications page is not loaded successfully.";
	public static final String RESTORE_SERVICE_ERROR = "Suspended Service restoration failed";
	public static final String SUSPEND_SERVICE_ERROR = "Suspended Service Link is not displayed.";
	public static final String JUMP_TXT_VERIFY = "JUMP header is not displayed on Devices list page.";
	public static final String PRODUCT_DETAILS_VERIFY = "Product Details page is not displayed";
	public static final String JUMP_ELIGIBLE_VERIFY = "JUMP eligibility is not displayed on Jump veriifcation page";
	//public static final String CHECKOUT_PAGE_VERIFY = "Checkout page is not displayed.";
	public static final String CHECKOUT_USAGE_PAGE = "Navigated to usage overview page";
	public static final String CHECK_PAPERLESS_LINK = "paper less billing section not displayed";
	public static final String VERIFY_USAGE_HISTORY_IN_LEGACY = "verify Your Usage History In Legacy";
	public static final String VERIFY_DOWNLOAD_BILLING_CHARGES = "Verify Download PDF for Billing charges.";
	public static final String CLICK_COMPARE_TO_LAST_BILL_LINK = "Click on Compare to last bill link.";
	public static final String VERIFY_PREVIOUS_MONTH_COMPARISION = "Verify comparision of current bill with previous bill.";
	//public static final String CURRENT_CHARGES_HEADER_VERIFY = "Currrent charges header on bill summary page is not displayed.";
	//public static final String DOWNLOAD_BILLING_CHARGES_VERIFY = "Error downloading the PDF file.";
	public static final String PREVIOUS_MONTH_COMPARISION_VERIFY = "Comparision of current bill with previous bill is not loaded.";
	
	//public static final String PLAN_PAGE_ERROR = "plan page is not displayed";
	public static final String CURRENT_PLAN_TEXT="T-Mobile ONE";
	public static final String SERVICES_PAGE_ERROR = "Services page is not displayed";
	public static final String NO_MANAGE_AUTOPAY_PAGE = "manage autopay page not loaded";

	public static final String CLICK_EDIT_PHONE_LINK = "Click on Edit button of link Phone numbers under TmobileID";
	public static final String VERIFY_LINK_PHONE_NUMBERS = "Link and Unlink phone numbers page is displayed.";
	public static final String VERIFY_LINKED_MSISDN_CONFIRM = "Linking a new phone number is successful.";
	public static final String CLICK_UNLINK_PHONE = "Click on Unlink button";
	public static final String VERIFY_UNLINKED_MSISDN_CONFIRM = "UnLinking of phone number is successful.";
	public static final String EDIT_INSIGHTS_PAGE_VERIFY = "INSIGHTS Edit form is not loaded";
	public static final String EDIT_INSIGHTS_VERIFY = "INSIGHTS SWITCH ON/OFF change is not working as expected.";
	public static final String EDIT_INTEREST_BASED_ADS_PAGE_VERIFY = "INTETREST Based Ads Edit form is not loaded";
	public static final String EDIT_INTEREST_BASED_ADS_VERIFY = "INTETREST Based Ads SWITCH ON/OFF change is not working as expected.";
	public static final String EDIT_INSIGHTS = "INSIGHTS SWITCH ON/OFF change functionality is successful";
	public static final String EDIT_ADVERTISING = "INTETREST Based Ads SWITCH ON/OFF change functionality is successful";
	
	public static final String CREDIT_CARD_VALIDATION_ERROR = "Credit card is not selected";
	public static final String SUBMITORDER_BUTTON_ERROR = "Submit order button is not enabled";
	
	public static final String TMOBILE_BENEFITS_LINK_ERROR = "T-mobile benefits link is not displayed";
	//public static final String IAM_SERVER_ENVIRONEMT = "iam_servers";
	public static final String SECURITY_QUES_ANS_SUCCESS = "Successfully changed security questions/answers";
	public static final String TMOBILE_PREFERENCE_EDIT = "Edit Tmobile Preferences";
	public static final String VERIFY_MESSAGING_POP_UP = "Verify Messaging op up is displayed for chat.";
	public static final String SEND_MESSAGE_CHAT = "Send any text in Live chat window.";
	public static final String VERIFY_MESSAGE_CLOSE_CHAT = "Verify sent message in live chat window and click close it.";
	public static final String SUBMIT_CHAT_RATING = "Submit chat rating.";
	public static final String CLICK_CALLME_LINK = "Click Call Me button in Contact US page.";
	public static final String SELECT_CALL_REASON = "Select category/reason for call setup.";
	public static final String CLICK_CALLME_NOW_BUTTON = "Click Call me Now Button.";
	public static final String VERIFY_CONFIRMATION = "Verify confirmation for Call Me/Schelued call back setup.";
	public static final String SUBMIT_CALL_ME = "Submit confirmation for Call Me Now.";
	public static final String CLICK_SCHEDULE_CALLBACK_BUTTON = "Click Schedule call back button.";
	public static final String SELECT_TIME_SLOT = "Select available time slot.";
	public static final String CONFIRM_SCHEDULE_CALLBACK = "Click on Confirm Schedule Call Back button";
	public static final String SUBMIT_SCHEDULE_CALLBACK = "Submit the scheduled Call back";
	
	public static final String TEPLAN_TEXT="T-Mobile ONE TE";
	public static final String TIPLAN_TEXT="2 Lines TMO ONE All In Promo No Credit Check";
	public static final String TI_PLAN_NOT_PRESENT_ERROR = "TI plan is not displayed in Plans page";
	public static final String VERIFY_TEPLAN_TEXT="Verify TE Plan Text";
	public static final String VERIFY_TIPLAN="Verify TI Plan";
	public static final String TEPLAN_TEXT_DISPLAYED="TE plan text is available in the Plan page";
	public static final String TIPLAN_DISPLAYED="TI plan is available in the Plan page";
	public static final String VERIFY_CURRENTPLAN="Verify current plan text";
	public static final String CURRENTPLAN_DISPLAYED="Current plan is displayed";
	
	//
	public static final String VOICE_INFORMATION_DISPLAYED = "Voice  details information is displayed";
	public static final String MESSAGING_INFORMATION_DISPLAYED = "Messaging details information is displayed";
	public static final String DATA_INFORMATION_DISPLAYED = "Data details information is displayed";
	public static final String FILTERED_VOICE_INFORMATION_DISPLAYED = "Filtered voice information is displayed";
	public static final String FILTERED_MESSAGING_INFORMATION_DISPLAYED = "Filtered messaging information should be displayed";
	public static final String FILTERED_DATA_INFORMATION_DISPLAYED = "Filtered data information is displayed";
	
	public static final String FILTERED_VOICE_INFORMATION_NOT_DISPLAYED = "Filtered voice information not displayed";
	public static final String FILTERED_MESSAGING_INFORMATION_NOT_DISPLAYED = "Filtered messaging information should not displayed";
	public static final String FILTERED_DATA_INFORMATION_NOT_DISPLAYED = "Filtered data information not displayed";
	
	// Data Pass List page
	public static final String CONTACT_US = "Contact Us is displayed";
	public static final String CLICK_ON_BACK_BTN = "Plan page is displayed";
	public static final String VERIFY_DATA_PASS_PAGE = "Data pass list page is displayed";
	public static final String BACK_BUTTON = "Back button is not displaying";
	public static final String VERIFY_BACK_BUTTON = "Back button is displaying";
	public static final String CONTACT_BUTTON = "Contact US button is not displaying";
	public static final String VERIFY_CONTACT_BUTTON = "Contact US button is displaying";
	public static final String CURRENT_PASS_EXPIRES = "Starts when current pass expires is not displaying";
	public static final String CURRENT_PASS_EXPIRES_MSG = "Starts when current pass expires is displaying";
	public static final String CONTINUE_BUTTON = "Continue button is not disabled";
	public static final String CANCEL_BUTTON = "Cancel button is not displaying";
	public static final String DATA_PASS = "Data Pass is not displaying";
	public static final String BROWSE_AVAILABLE_DATA_PASS = "Browse through our available data passes. Once you've found the one you want, contact your primary account holder to have them add it.";
	public static final String BROWSE_AVAILABLE_DATA_PASS_ERROR = "Browse through our available data passes message is not displaying";
	public static final String BROWSE_AVAILABLE_DATA_PASS_ERROR_MSG = "Browse through our available data passes message is displayed";
	public static final String ENABLED_HD_ERROR = "Enabled Hd link is not displaying";
	public static final String ENABLED_HD = "Success: Enabled HD link is enabled";
	public static final String INTERNATIONAL_DATAPASS = " Selected International Data Pass";
	public static final String CLICK_ON_CONTINUE_BUTTON = " Clicked Continue button";
	public static final String HD_PASS = "HD Pass is not displaying";
	public static final String TIME_DROP_DOWN = "Selected Time Drop Down Value";
	public static final String FREE_DATAPASS = "Free Data Pass is displaying";
	public static final String CALENDAR_VIEW = "Calendar View is not displaying";
	public static final String CALENDAR_VIEW_MSG = "Calendar View is displayed";
	//public static final String DATAPASS_PAGE_LEGAL_INFO_ERROR = "Legalese information is not displayed";
	//public static final String DATAPASS_PAGE_LEGAL_INFO = "Legalese information is displayed";
	//public static final String PLANPAGE_ERROR = "Plan page is not displayed";
	public static final String DATAPASS_WARNING_MESSAGE = "Data pass warning message 'HOLD UP' is not displayed";
	public static final String DATAPASS_WARNING = "Data pass warning message 'HOLD UP' is displayed";
	public static final String DATAPASS_PAGE_ERROR = "Data pass page is not displayed";
	public static final String DATAPASS_REVIEW_PAGE_ERROR = "Data pass review page is not displayed";
	//public static final String DATAPASS_CONFIRMATION_PAGE_ERROR = "Data pass confirmation page is not displayed";
	//public static final String DATALISTPAGE_FOOTER_ERROR = "Footer is not dispalyed in data list page";
	//public static final String DATAPASS_REVIEWPAGE_FOOTER_ERROR = "Footer is not dispalyed in data pass review page";
	//public static final String DATAPASS_CONFIRMATIONPAGE_FOOTER_ERROR = "Footer is not dispalyed in data pass confirmation page";
	public static final String DATAPASS_ERROR_MESSAGE = "Data pass not added successfully";
	//public static final String INTERNATIONAL_ROAMING_DISABLE_MESSAGE_ERROR = "International roaming disable message is not displyed";
	//public static final String UNBLOCK_ROAMING_LINK_ERROR = "Unblock roaming link is not clickable";
	public static final String TERMS_CONDITIONS_TEXT = "By clicking Agree and Submit, I agree that";
	//public static final String HDDATAPASS_ERROR = "HD Data pass not added";
	//public static final String DATAPASS_CONFIRMATIONPAGE_LEGAL_COPY_ERROR = "Legal copy is not displayed in data pass confirmation page";
	public static final String NO_DATAPASS = "No Data Pass page not is displaying";
	public static final String COMPATIBLE_DATAPASS = "There are no data passes compatible with your plan at this time.";
	public static final String DATAPASS_CONFIRM_MESSAGE = "Data pass added successfully";

	// Data Passes Review Page
	public static final String VERIFY_DATAPASS_REVIEW_PAGE = "Review page is displayed";
	public static final String START_IMMEDIATELY = "Start Immediately is not displaying";
	public static final String VERIFY_START_IMMEDIATELY = "Verified Start Immediately text";
	public static final String DATAPASS_REVIEW = "Review page is not displaying";
	public static final String STARTS_ON_DATE = "Starts on with date and time not displaying";
	public static final String STARTS_AFTER_CURRENT_DATE_EXPIRES = "Starts after current data expires is not displaying";
	public static final String STARTS_AFTER_CURRENT_DATE_EXPIRES_MSG = "Verified Starts after current data expires";
	public static final String TAXES_AND_FEES_NEXT_BILL = "plus taxes and fees will be added to your next bill";
	public static final String TAXES_AND_FEES_NEXT_BILL_ERROR = "Plus taxes and fees will be added to your next bill text is not displaying";
	public static final String VERIFY_TAXES_AND_FEES_NEXT_BILL = "Plus taxes and fees will be added to your next bill text is displaying";
	public static final String DRP_LEGALESE = "DRP Legalese text is not displaying";
	public static final String VERFIY_DRP_LEGALESE = "Verified DRP Legalese text is displaying";

	// Data Passes Confirmation Page
	public static final String TAXES_AND_FEES = "Taxes and fees is not displaying";
	public static final String ORDER_COMPLETE_TEXT = "Your Order is complete text not displaying";
	public static final String DPCLEGAL_COPYTEXT = "Data pass charges: Data pass charges are one-time charges. These charges will be applied to the next monthly bill after the start date of your pass. Removed/cancelled data passes: No refunds will be issued after the start date of a pass.";
	
	//public static final String UNAV_TMOBILE_ICON_ERROR = "T-mobile is not displayed in the header";
	//public static final String UNAV_CONTACTUS_ICON_ERROR = "Contact us icon is not displayed in the header";
	//public static final String UNAV_BACKARROW_ICON_ERROR = "Back arrow is not displayed in the header";
	
	public static final String CHANGE_FROM_LASTBILL = "Change From LastBill";
	public static final String VERIFY_CHANGE_FROM_LASTBILL = "Verify Change From LastBill";
	public static final String CURRENT_CHARGES_DISPLAYED ="Current Charges are Displayed which is not as expected.";
	
	public static final String VERIFY_PROFILE_PAGE_DISPLAY ="Profile Page is displayed";
	public static final String VERIFY_LINESELECTOR_PRIVACYNOTIFICATION_PROFILEPAGE="Line selector is not displayed";
	public static final String VERIFY_LINESELECTOR_PRIVACYNOTIFICATION_PROFILEPAGE_DISPLAY="Line selector is displayed";

	public static final String RECURRING_CHARGES_DISPLAYED ="Recurring Charges are not displayed.";
	public static final String MISC_CHARGES_DISPLAYED ="Misc Charges are not displayed.";
	public static final String DETAIL_PAGE_VERIFY = "Detail page is not displayed.";
	public static final String VERIFY_SEE_TAXES_FEES = "See Taxes and Fees is not displayed.";
	public static final String VERIFY_TAXES_FEES_MODELWINDOW = "Taxes and Fees ModelWindow is not displayed.";
	public static final String VERIFY_TAXES_FEES_MODELWINDOW_HEADER = "Taxes and Fees ModelWindow Header is not displayed.";
	public static final String VERIFY_TMOBILE_FEES_AND_CHARGES_HEADER = "T-Mobile Fees and Charges Header is not displayed.";
	public static final String VERIFY_GOVERNMENT_TAXES_AND_FEES_HEADER = "Government Taxes and Fees Header is not displayed.";
	public static final String VERIFY_MODELWINDOW_DISCLAIMER = "ModelWindow Disclaimer is not displayed.";
	public static final String VERIFY_MODELWINDOW_TOTAL_AMOUNT_DISPLAYED = "ModelWindow Total Charges are not displayed.";
	public static final String VERIFY_TAXES_FEES = "Taxes and Fees Column from the current charges table are displayed.";
	
	public static final String TMOBILEONENOCREDITCHECK="T-Mobile ONE No Credit Check TE";
	public static final String VERIFYTMOBILEONENOCREDITCHECK="Verify T-Mobile One No Credit Check TE";
	
	public static final String PLAN_CONFIGURE_PAGE_ERROR="Plan configure page is not displayed";
	public static final String PLAN_COMPARISON_PAGE_ERROR="Plan comparison page is not displayed";
	public static final String VERIFY_CURRENTCHARGES_DISCLAIMER="CurrentCharges Disclaimer is not displayed";
	public static final String VERIFY_TAXES_FEES_AMOUNT_DISPLAYED="Taxes And Fees Charges are not displayed";
	
	public static final String ONE_TIME_PAYMENT_PAGE_SHOULD_LOAD = "One time Payment page should be loaded";
	public static final String VERIFY_REVIEW_PAYMENT_DISPALYED = "Review payment page is displayed";
	public static final String VERIFY_REVIEW_PAYMENT_LOADED = "Review payment page should be loaded";
	public static final String ACCOUNT_NUMBER_DISPLAYED = "Account number displayed";
	public static final String VERIFY_ACCOUNT_NUMBER = "Account number should be displayed";
	public static final String VERIFY_CREDIT_CARD_NUMBER = "Verify credit card number";
	public static final String VERIFYED_ZIP_CODE = "Verified zip code";
	
	public static final String UPDATED_CUSTOMER_EMAIL = "Customer Email should be updated.";
	public static final String VERIFY_FILE_CLAIM = "File My Claim page should be displayed.";
	public static final String VERIFY_SHIPPINGDETAILS_LINK = "Verify Shipping Details options are displayed";
	public static final String VERIFY_JUMP_CLAIM_STATUS = "Jump upgrade claim status should be displayed.";
	//public static final String ANSWER_QUESTIONS_AND_CONTINUE = "Answer all the questions and Click continue button";
	public static final String WRONG_ANSWERS_TO_QUESTIONS_AND_CONTINUE = "Select incorrect answers and Click continue button";
	public static final String FILE_A_CLAIM = "Click on File a Claim button";	

	public static final String PHP_HEADER="Premium Device Protection with Lookout Mobile Security Premium";
	public static final String VERIFY_PHP_HEADER="Verify PHP Header";
	public static final String PHP_HEADER_VERIFIED="PHP Header is verified";
	

	public static final String NO_VERIFY_EIP_BALANCE = "Eip balance not displayed";
	public static final String BILLING_PAGE_SHOULD_DISPLAYED = "bill summary page should be  displayed";
	public static final String EIP_SECTION_SHOULD_DISPLAYED = "Eip section should be displayed";
	
	public static final String EIP_SECTION_DISPLAYED = "EIP Section is displayed";
	public static final String VERIFY_ACCOUNT_HISTORY_SECTION= "Account history section should be displayed";
	public static final String ACCOUNT_HISTORY_SECTION_DISPLAYED = "Account history section is displayed";

	public static final String VERIFY_SHOP_PAGE = "Shop page is loaded";
	public static final String VERIFY_UPGRADE_ADDSERVICES = "Upgrade-Add Services page should be loaded";
	public static final String VERIFY_UPGRADE_REVIEWCART = "Upgrade-Review cart page should be loaded";
	
	public static final String TMOBILEONETEPRICE="0.00";
	public static final String ONEPLUSNCCPRICE="5.00";
	public static final String ONEPLUSINTERNATIONALTEPRICE="25.00";
	public static final String VERIFY_TMOBILEONETEPRICE="T-Mobile One TE Price has been verified";
	public static final String VERIFY_ONEPLUSNCCPRICE="One Plus NCC Price has been verified";
	public static final String VERIFY_ONEPLUSINTERNATIONALTEPRICE="One Plus International TE Price has been verified";
	public static final String DEPOSITTOTAL_HEADER="A total deposit of $25.00 is required for your voice lines.";
	public static final String VERIFY_DEPOSITTOTAL_HEADER="DepositTotal Header has been verified";

	public static final String VERIFY_PLAN_DETAIL_PAGE = "Plan Detail Page should be displayed";
	public static final String VERIFY_BUYASIM_BUTTON = "BUY A SIM Button should be displayed";
	
	 public static final String ONE_PLUS_INTERNATIONAL_SERVICE="ONE Plus International";
	 public static final String $25_ONEPLUS_SERVICE_PRICE="$25";
	 public static final String ERRORMSG_$25_ONEPLUS_SERVICE_PRICE="25$ one plus service is not displayed";
	 public static final String VERIFY_TAX_PRICE_STRIKE="Tax Price is not Strike";
	
	public static final String BILL_DUEDATE_NOTDISPLAYED="Bill due date is not displayed";
	public static final String BILL_DUEDATE_DISPLAYED="Bill due Date is displayed";
	public static final String REMAININGDAYS_DUEDATE_NOTDISPLAYED="Remaining days of due date is not displayed";
	public static final String REMAININGDAYS_DUEDATE_DISPLAYED="Remaining days of due date is displayed";
	public static final String VERIFY_BILL_SUMMARYPAGE_PAST_BILLS="Bill Summary Page Past Bills Should be displayed Grater than 12 months";
	public static final String USAGE_OVER_TIME = "Usage Over Time Should be displayed";
	
	public static final String AUTOPAYLINK="null";
	public static final String AUTOPAY_LINK_NOT_DISPLAYED="Auto Pay link is not displayed";
	public static final String AUTOPAY_PAYEMENT_SCHEDULEDDATE_NOTDISPLAYED="Autopay Payment Scheduled Date message is not displayed";
	public static final String AUTOPAY_PAYEMENT_SCHEDULEDDATE_DISPLAYED="Autopay Payment Scheduled Date message is displayed";
	
	public static final String AUTOPAY_CARD_ICON_NOTDISPLAYED="Autopay Card Icon is not displayed";
	public static final String AUTOPAY_CARD_ICON_DISPLAYED="Autopay Card Icon is displayed";
	
	public static final String AUTOPAY_ALERTS_NOTDISPLAYED="Autopay Payemnt information alerts is not displayed";
	public static final String AUTOPAY_ALERTS_DISPLAYED="Autopay Payemnt information alerts Icon is displayed";
	
	public static final String PAYMENT_DECLINE_NOTDISPALYED="Payment Declined dialog box is not displayed";
	public static final String PAYMENT_DECLINE_DISPALYED="Payment Declined dialog box is displayed";
	public static final String VERIFY_CREDITCARD_NUMBERS="Credit card number is displayed correctly";
	public static final String VERIFY_ZIPCODE_NUMBERS="Zip Code is displayed correctly is displayed";
	public static final String PAYMENT_CONFIRMATIONMESSAGE="We appreciate your business. Your payment is being processed and may take up to two hours to post to your account.";
	public static final String VERIFY_REMOVEPAYMENT_DIALOGBOX_NOTDISPLAYED="Remove payment dialog box is not displayed";
	public static final String VERIFY_REMOVEPAYMENT_DIALOGBOX_DISPLAYED="Remove payment dialog box is displayed";	

	public static final String VERIFY_MSISDN_DATA="MSISDN Data not displayed";
	public static final String VERIFY_PAYBILL_BUTTON="PAYBILL Button not displayed";
	public static final String VERIFY_VIEWBILL_LINK="VIEWBILL Link not displayed";
	public static final String VERIFY_VIEW_PLAN_DETAILS="View plan details Link not displayed";	
	
	public static final String BILLING_SUMMARYPAGE_DISPLAYED="Billing Summary page is displayed";
	public static final String EIPPAGE_NOTDISPLAYED="Equipement Installment Plan page is not displayed";
	public static final String EIPPAGE_DISPLAYED="Equipement Installment Plan page is displayed";
	public static final String EIP_ESTIMATOR_NOTDISPLAYED="EIP Estimator dialog box is not displayed";
	public static final String EIP_ESTIMATOR_DISPLAYED="EIP Estimator dialog box is displayed";

	public static final String EXTRA_DEVCIE_PAYMENT_NOTDISPLAYED="Extra Device Payment page is not displayed";
	public static final String EXTRA_DEVCIE_PAYMENT_DISPLAYED="Extra Device Payment page is displayed";
	public static final String REVIEW_DEVICE_PAYMENT_NOTDISPLAYED="Review Device payment page is not displayed";
	public static final String REVIEW_DEVICE_PAYMENT_DISPLAYED="Review device payment page is displayed";
	public static final String COMPLETED_PLAN_NOTDISPLAYED="Completed Plan is not displayed";
	public static final String COMPLETED_PLAN_DISPLAYED="Completed plan is displayed";
	public static final String PAYMENT_ARRANGEMENT_NOTDISPLAYED="Payment Arrangement is not displayed";
	public static final String PAYMENT_ARRANGEMENT_DISPLAYED="Payment Arrangement is displayed";
	public static final String PAYMENTS_DETAILS_NOTDISPLAYED="Payment Details is not displayed";
	public static final String PAYMENTS_DETAILS_DISPLAYED="Payment Details is displayed";
	public static final String REVIEW_PAYMENT_INFORMATION_NOTDISPLAYED="Review Payment information page is not displayed";
	public static final String REVIEW_PAYMENT_INFORMATION_DISPLAYED="Review payment informatio page is displayed";
	public static final String PAY_ONLINE_QUADRANT_NOTDISPLAYED="Pay online Quadrant is not displayed";
	public static final String PAY_ONLINE_QUADRANT_DISPLAYED="Pay online Quadrant is displayed";
	public static final String CALL_CARE_QUADRANT_NOTDISPLAYED="Call care Quadrant is not displayed";
	public static final String CALL_CARE_QUADRANT_DISPLAYED="Call care Quadrant is displayed";
	public static final String PAST_DUE_QUADRANT_NOTDISPLAYED="Past Due Quadrant is not displayed";
	public static final String PAST_DUE_QUADRANT_DISPLAYED="Past due Quadrant is displayed";
	public static final String CURRENT_BALANCE_QUADRANT_NOTDISPLAYED="Current Balance Quadrant is not displayed";
	public static final String CURRENT_BALANCE_QUADRANT_DISPLAYED="Currernt Balance Quadrant is displayed";
	
	public static final String VERIFY_KICKBACK_FOR_SIMPLECHOICE_PLAN="Kick Back is displayed for Simple Choice plan";
	public static final String VERIFY_COVERAGEDEVICE_TYPE="Coverage Device Type not displayed";
	public static final String VERIFY_COVERAGEDEVICE_IMEI="Coverage Device IMEI No not displayed";
	public static final String VERIFY_COVERAGEDEVICE_STATUS="Coverage Device Status not displayed";
	public static final String VERIFY_COVERAGEDEVICE_ADDRESS="Coverage Device Address not displayed";
	public static final String VERIFY_COVERAGEDEVICE_MOBILENUMBER="Coverage Device Mobile number not displayed";

	public static final String VERIFY_VISIBLE_DATES_NOTDISPLAYED="Visible dates is not displayed";
	public static final String VERIFY_VISIBLE_DATES_DISPLAYED="Visible dates is displayed";
	public static final String VERIFY_CURRENT_ACTIVITY_NOTDISPLAYED="Current Activity page is not displayed";
	public static final String VERIFY_CURRENT_ACTIVITY_DISPLAYED="Current Activity page is displayed";
	public static final String VERIFY_REMAINING_PAYMENTS_NOTDISPLAYED="Remaining Payments page is not displayed";
	public static final String VERIFY_REMAINING_PAYMENTS_DISPLAYED="Remaining Payments page is displayed";
	public static final String VERIFY_SCHEDULING_PAYMENTS_NOTDISPLAYED="Scheduling Payment Details is not displayed";
	public static final String VERIFY_SCHEDULING_PAYMENTS_DISPLAYED="Scheduling Payment Details is displayed";

	public static final String VERIFY_KICKBACK_ON_OFF="Kickback On/Off modification did not work as expected.";
	public static final String ONBOARDING_FOOTER_CHECKLIST="Onboarding Footer Checklist is visable for Retricted User";
	
	public static final String VERIFY_BALANCE_UPDATES_NOTDISPLAYED="Balance Updates is not displayed";
	public static final String VERIFY_BALANCE_UPDATES_DISPLAYED="Balance Updates is displayed";
	
	public static final String VERIFY_EASYPAY_FAILED_PAYMENT_NOTDISPLAYED="EasyPay failed payment is not displayed";
	public static final String VERIFY_EASYPAY_FAILED_PAYMENT_DISPLAYED="EasyPay failed payment is displayed";
	
	public static final String VERIFY_SUBMIT_BUTTON_NOTDISPLAYED="Submit Button is not displayed";
	public static final String VERIFY_SUBMIT_BUTTON_DISPLAYED="Submit Button is displayed";	
	public static final String VERIFY_CANCEL_BUTTON_NOTDISPLAYED="Submit Button is not displayed";
	public static final String VERIFY_CANCEL_BUTTON_DISPLAYED="Submit Button is displayed";
	public static final String VERIFY_SUBMIT_BUTTON_NOTENABLED="Submit Button is not enabled";
	public static final String VERIFY_SUBMIT_BUTTON_ENABLED="Submit Button is enabled";	

	/*public static final String ACC_ACTIVITY_TYPE_PAYMENT_CONFIRMATION = "Payment Arrangement Confirmation";
	public static final String PAYMENTS_LINK = "Payments";
	public static final String CLICK_ACCOUNT_HISTORY = "Click on Account History link in Home Page.";
	public static final String CLICK_PAYMENT_TAB = "Click on Payments tab under Account History.";
	public static final String ACC_ACTIVITY_TYPE_EIP = "eip";
	public static final String ACC_ACTIVITY_TYPE_REFUND = "Device payment refund";
	public static final String ACC_ACTIVITY_TYPE_REVERSAL = "Device payment reversal";

	public static final String ACC_ACTIVITY_TYPE = "Verified and clicked activity type";
	public static final String ACC_ACTIVITY_DESC = "Verified activity activity description";
	public static final String ACC_ACTIVITY_DETAILS = "Verified activity activity payment type, reference Id, Payment Id.";
	public static final String ACC_ACTIVITY_TYPE_EIP_DESC = "Equipment Installment Plan";
	public static final String ACC_ACTIVITY_TYPE_DEVICE_PAYMENT = "New device payment posted";
	public static final String ACC_ACTIVITY_TYPE_PAYMENT = "New payment posted";	*/
	
	public static final String T_MOBILEONEWEARABLE_TEXT="T-Mobile ONE Wearable";
	public static final String VERIFY_TMOBILEONE_WEARABLE_NOTMATCHED="T-Mobile One wearble Text is not matched";
	public static final String VERIFY_TMOBILEONE_WEARABLE_MATCHED="T-Mobile One wearble Text is matched";	
	public static final String T_MOBILEONEWEARABLE_20DOLLAR_TEXT="$20.00";
	public static final String VERIFY_TMOBILEONE_WEARABLE_20DOLLAR_NOTMATCHED="T-Mobile One wearble Text is not matched";
	public static final String VERIFY_TMOBILEONE_WEARABLE_20DOLLAR_MATCHED="T-Mobile One   Text is matched";
	public static final String VERIFY_PRIMARY_ACCOUNT_HOLDER="Primary Account Holder not displayed";
	public static final String VERIFY_KICKBACK="KickBack not displayed";
	public static final String VERIFY_RESTRICTED_ACCESS="Restricted Access not displayed";
	
	public static final String PDP_PAGE= "Navigated to Product Details Page";
	public static final String LINE_SELECTOR_PAGE= "Navigated to Line Selector Page";
	/*public static final String CART_PAGE_ERROR = "Cart page is not displayed";
	public static final String CART_PAGE_BACK_ARROW_ERROR = "Back arrow is not displayed in cart page";
	public static final String LINESELECTOR_PAGE_ERROR = "Line selector page is not displayed";
	public static final String PDP_PAGE_ERROR = "PDP page is not displayed";
	
	public static final String DEVICE_IMAGE_ERROR = "Device image is not displayed.";
	public static final String NICK_NAME_ERROR = "Nick name is not displayed in cart page";
	public static final String DEVICE_DESCRIPTION_ERROR = "Device description is not displayed.";
	public static final String DEVICE_EIP_PRICE_ERROR = "Device EIP price is not displayed in cart page";
	public static final String REMOVE_MODEL_ERROR = "Remove model is not displayed in cart page";
	public static final String REMOVE_MODEL_BODY_TEXT_ERROR = "Remove model body description is not displayed";
	public static final String EMPTY_CART_ERROR = "Empty cart text not displayed";
	public static final String REMOVE_MODEL_BODY_TEXT = "Are you sure you want to remove this item from your cart?";
	public static final String MONTHLY_CHARGES_MODEL_ERROR = "Monthly charges model is not displayed";
	public static final String REMOVE_MODEL_PREVIOUSMONTHLYCHARGES_LABEL = "Previous Monthly charges";
	public static final String REMOVE_MODEL_NEW_DEVICE_PAYMENT_LABEL = "New device payment";
	public static final String REMOVE_MODEL_TAXES_FEES_LABEL = "+Taxes and fees not already included";
	public static final String NEW_DEVICE_PAYEMENT_ERROR = "New device payment label is not displayed";
	public static final String MONTHLY_CHARGES_MODEL_TOTAL_ERROR = "Total is not displayed in Monthly charges model";
	public static final String MONTHLY_CHARGES_MODEL_TAXESANDFEES_ERROR = "Taxes and fees is nod displayed in Monthly charges model";
	
	public static final String SHIPPING_INFORMATION_SECTION_ERROR = "Shipping information section is not displayed";
	public static final String SHIPPINGDETAILS_PAGE_ERROR = "Shipping details page is not displayed";
	public static final String VERIFY_SHIPPINGDETAILS_PAGE = "Shipping details page is displayed";
	public static final String CART_PAGE_SHIPPING_METHOD_ERROR = "Shipping method date is not displayed in cart page";
	public static final String CART_PAGE_SHIPDATE_ERROR = "Ship date is not displayed in cart page";
	public static final String CART_PAGE_SHIP_ADDRESS_ERROR = "Ship address is not displayed in cart page";
	public static final String SHOP_ACCESSORIES_BUTTON_ERROR = "Shop Accessoriesis button is not displayed";
	public static final String PDPPAGE_MEMORY_HEADER_ERROR = "Memory header is not displayed";
	public static final String PDPPAGE_SEEMORE_LINK_ERROR = "See more link is not displayed";
	public static final String PDPPAGE_SEELESS_LINK_ERROR = "See less link is not displayed";
	public static final String PDPPAGE_CONTINUE_BUTTON_ERROR = "Continue button is not displayed";
	
	public static final String YOUPAYTODAY_DOWN_PAYMENT_HEADING_ERROR = "Down payment heading is not displayed";
	public static final String YOUPAYTODAY_DOWN_PAYMENT_TEXT_ERROR = "Down payment text is not displayed";
	public static final String VERIFY_CART_PAGE = "Cart page is displayed";
	public static final String LEGAL_COPY_ERROR = "Legal copy is not displayed";
	public static final String CARTPAGE_ACCEPTANDPLACEORDER_ERROR = "Accept and place order button is not displayed";
	public static final String CARTPAGE_ACCEPTANDCONTINUE_ERROR = "Accept and continue button is not displayed";
	public static final String SELECTED_SHIP_METHOD_ERROR = "Selected ship method is not displayed";
	public static final String ACCESSIBILITY_PAGE_ERROR = "Accessibility Page is not displayed";
	public static final String ACCESSIBILITY_PAGE = "Accessibility page is displayed";
	public static final String PLPPAGE_NO_ITEMS_FOUND_ERROR = "No items found is not displayed";
	public static final String PLPPAGE_NO_ITEMS_FOUND_MESSAGE_ERROR = "For more accurate results try removing some of your filters is displayed";
	
	public static final String VERIFY_DEFEAULT_SORT_OPTION = "Verify defeault option";
	public static final String VERIFY_FEATURED = "Verify featured devices";
	public static final String VERIFY_HIGH_RATING = "High rating is not in order";
	public static final String HIGH_RATING = "Highest rating";
	public static final String FEATURED = "Featured";
	public static final String PLPPAGE_LEGALTEXT_ERROR = "Legal text is not displayed";
	public static final String PDPPAGE_LEGALTEXT_ERROR = "Legal text is not displayed";
	public static final String LEGAL_TEXT_PDPPAGE = "IF YOU CANCEL WIRELESS SERVICE, REMAINING BALANCE ON PHONE BECOMES DUE. 0% APR O.A.C. for well-qualified customers.";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_HEADER_ERROR = "Down payment model header is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_TEXT_ERROR = "Down payment model text is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_NOTE_ERROR = "Down payment model note is not displayed";
	public static final String PDPPAGE_ADDTOCART_ERROR = "Add to cart button is not displayed";
	*/
	public static final String VERIFY_FEATUREPLAN_PAGE_DISPLAYED="Feature plan page is displayed";
	public static final String VERIFY_FEATUREPLAN_PAGE_NOTDISPLAYED="Feature plan page is not displayed";
	public static final String VERIFY_PLANCOMPARISION_PAGE_DISPLAYED="Plan comparision page is displayed";
	public static final String VERIFY_PLANCONFIFUREPAGE_DISPLAYED="Plan Configure page is displayed";
	public static final String VERIFY_PLANCONFIFUREPAGE_NOTDISPLAYED="Plan Configure page is displayed";
	public static final String VERIFY_MOBILEINTERNET2GB_DISPLAYED="Mobile Internet 2GB data is displayed";
	public static final String VERIFY_MOBILEINTERNET2GB_NIOTDISPLAYED="Mobile Internet 2GB data is displayed";
	public static final String VERIFY_CHANGEPLANPAGE_ISDISPLAYED="Change plan page is displayed";
	public static final String VERIFY_CHANGEPLANPAGE_NOTDISPLAYED="Change plan page is not displayed";
	public static final String VERIFY_REVIEWCHANGEPAGE_ISDISPLAYED="Review Change page is displayed";
	public static final String VERIFY_REVIEWCHANGEPAGE_NOTDISPLAYED="Review Change page is not displayed";
	public static final String MOBILE_INTERNET_2GB="Mobile Internet 2GB";
	public static final String VERIFY_MOBILE_INTERNET_2GB_ISMATCHED="Mobile Internet 2GB is Matched";
	public static final String VERIFY_INSTALLMENTPLAN_TYPE_EIP = "Installment Plan Type is not EIP";
	public static final String VERIFY_PLANSTATUS_ACTIVE = "Plan Status Shows not Active";
	public static final String VERIFY_EIP_PLANID = "EIP PlanID Shows empty";
	public static final String VERIFY_PLAN_BALANCE_AMOUNT = "PlanBalance Amount Shows empty";

	//public static final String MANAGE_MULTI_LINE_LINK_VERIFY = "Manage Multi Line settings link is not displayed";
	//public static final String MANAGE_MULTILINE_LINK_DISPLAYED = "Manage Multi Line settings link is displayed";
	
	public static final String NOT_ELIGIBLE_ADDLINE="You are not eligible to add a line.";
	public static final String VERIFY_NOTELIGIBLE_ADDLINE_MATCHED="Add a line not Eligible text is matched";
	public static final String BUYSIM_BUTTON_NOTDISPLAYED="Buy Sim button is not displayed";
	public static final String BUYSIM_BUTTON_DISPLAYED="Buy Sim button is displayed";
//	public static final String TAXES_FEES_TEXT = "TAXES & FEES";
	public static final String TAXES_FEES_TEXT = "Taxes and fees";
	public static final String VERIFY_TAXES_FEES_HIDDEN = "taxes and fees are not hidden in Current chages table";
	
	public static final String VERIFY_NEWMONTHLY_DISPLAYED="New Monthly tab is displayed";
	public static final String VERIFY_NEWMONTHLY_NOTDISPLAYED="New Monthly tab is not displayed";
	public static final String VERIFY_CURRENTMONTHLY_DISPLAYED="Current Monthly tab is displayed";
	public static final String VERIFY_CURRENTMONTHLY_NOTDISPLAYED="Current Monthly tab is not displayed";
	public static final String REMOVED_MOBILE_INTERNET_6GB="Mobile Internet 6GB";
	public static final String VERIFY_REMOVED_MOBILE_INTERNET_6GB_ISMATCHED="Mobile Internet 6GB";
	
	public static final String VERIFY_DEVICEIMAGE_DISPLAYED="Device Image is displayed";
	public static final String VERIFY_DEVICEIMAGE_NOTDISPLAYED="Device Image is not displayed";
	public static final String VERIFY_DEVICENAME_DISPLAYED="Device Name is displayed";
	public static final String VERIFY_DEVICENAME_NOTDISPLAYED="Device Name is not displayed";
	public static final String VERIFY_CAMERAFEATURE_DISPLAYED="Camera Features is displayed";
	public static final String VERIFY_CAMERAFEATURE_NOTDISPLAYED="Camera Features is not displayed";
	public static final String VERIFY_REVIEWWRITTENBLOG_DISPLAYED="Review Written blog is displayed";
	public static final String VERIFY_REVIEWWRITTENBLOG_NOTDISPLAYED="Review Written blog is not displayed";

	//public static final String CLICK_CREDITS_AND_ONE_TIME_CHARGES_LINK = "Click on Credits and One time charges link on Bill Summary page";
	
	public static final String VERIFY_SAVED_CHECKINGACCOUNT_DISPLAYED="Saved Checking Account is displayed";
	public static final String VERIFY_SAVED_CHECKINGACCOUNT_NOTDISPLAYED="Saved Checking Account is not displayed";

	public static final String PAYEMENT_ARRANGEMENT_UNSUCCESSFULL_MESSAGE="Incorrect Info";
	
	public static final String PAYEMENT_ARRANGEMENT_SUCCESSFULL_MESSAGE="Success!";
	public static final String PAYEMENT_ARRANGEMENT_SUCCESSFULLMESSAGE_MATCHED="Sucessful message is matched";
	
	public static final String PAYEMENT_ARRANGEMENT_TERMSANDCONDITION="TERMS AND CONDITIONS";
	public static final String PAYEMENT_ARRANGEMENT_TERMSANDCONDITION_SUCCESSFULLMESSAGE_MATCHED="Terms and condition message is matched";
	public static final String BALANCE_UPDATES_NOTDISPLAYED="Balance Updates is not displayed";
	public static final String BALANCE_UPDATES_DISPLAYED="Balance Updates is displayed";

	/*public static final String MESSAGE_US_AND_CALL_US_ERROR = "We're sorry, our systems are currently unavailable. Please contact us at 1-800-937-8997 or dial 611 from your T-Mobile device.";
	public static final String MESSAGE_US_ERROR = "We're sorry, Message Us is currently unavailable. Please contact us using Call Me instead!";
	public static final String CALL_US_ERROR = "We're sorry, Call Me is currently unavailable. Please contact us using Message Us instead!";

	public static final String REVIEW_SECTION_MOST_HELPFUL_ERROR = "Review section(Most Helpful) is not displayed";
	public static final String REVIEW_SECTION_NEWEST_ERROR = "Review section(Newest) is not displayed";
	public static final String REVIEW_SECTION_OLDEST_ERROR = "Review section(Oldest) is not displayed";
	public static final String REVIEW_SECTION_HIGHEST_RATING_ERROR = "Review section(Highest Rating) is not displayed";
	public static final String REVIEW_SECTION_LOWEST_RATING_ERROR = "Review section(Lowest Rating) is not displayed";
	public static final String ADD_A_LINE_LINK_ERROR = "Add a line link is not displayed";
	
	public static final String PDP_PAGE_NUMBER_OF_CUSTOMER_REVIEWS_ERROR = "Number of customer reviews are not displayed";
	public static final String PDP_PAGE_REVIEW_TAB_ERROR = "Reviews tab is not selected";
	public static final String PDP_PAGE_DEFAULT_REVIEW_SECTION_ERROR = "Default review section is not displayed";
	public static final String PDPPAGE_FEATURES_TAB_ERROR = "Features tab is not selected by default";
	public static final String PDPPAGE_FEATURES_ERROR = "Features header is not displayed";
	public static final String VERIFY_3RD_PARTY_SERVICES_SECTION = "Verify 3rd party services section is displayed in Billing Details Page.";
	
	public static final String PAYMENT_PAGE_ERROR = "Payment page is not displayed";
	public static final String VERIFY_PAYMENT_PAGE = "Verified payment page";
	public static final String CARTPAGE_ORDER_HEADING_ERROR = "Verify order confirmation heading is not displyed";
	public static final String CARTPAGE_ORDER_NUMBER_ERROR = "Verify order confirmation number is not displyed";
	public static final String CARTPAGE_ORDER_HEADING = "Verify order confirmation heading";*/
	public static final String PHP_MIGRATION_PAGE = "PHP migration page is not displayed";
	
	public static final String VERIFY_SUCCESS_MESSAGE_DISPLAYED="Success message is displayed";
	public static final String VERIFY_SUCCESS_MESSAGE_NOTDISPLAYED="Success message is not displayed";
	
	//public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE = "The Regulatory Programs (RPF) & Telco Recovery Fee (TRF) is not a government tax, but a fee collected and retained to recover government compliance costs (RPF), plus charges imposed by other carriers for calls between our customers and theirs, and by 3rd parties for network facilities and services we purchase to provide service (TRF). Of the $3.18 total charged for voice lines, $0.60 is for the RPF & $2.58 is for the TRF. Of the $1.16 total charged for data only lines, $0.15 is for the RPF & $1.01 is for the TRF.";
	//public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_SPANISH = "El cargo por Programas Reguladores (RPF) y el cargo por Recuperación de los Costos de Compañía de Telecomunicaciones (TRF) no son impuestos gubernamentales, sino cargos cobrados y retenidos para recuperar costos de cumplimiento con el gobierno (RPF), más cargos aplicados por otros proveedores por llamadas entre nuestros clientes y los suyos, y por terceros por servicios y funciones de red que compramos para proporcionar servicio (TRF). Del total de $3.18 que se cobra para líneas de voz, $0.60 es por el RPF y $2.58 es por el TRF. Del total de $1.16 que se cobra para líneas sólo de datos, $0.15 es por el RPF y $1.01 es por el TRF.";
	//public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_AAL = "For plans where taxes and fees are not included in the monthly recurring charge: Monthly Regulatory Programs (RPF) & Telco Recovery Fee (TRF) totaling $3.18 per voice line ($0.60 for RPF & $2.58 for TRF) and $1.16 per data only line ($0.15 for RPF & $1.01 for TRF) applies. Taxes approx. 6-28% of bill.";
	//public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_ERROR = "AAL - TRF and Total fees details are not updated in legal desclaimer text.";
	//public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_JUMP = "I will be charged a monthly Regulatory Programs & Telco Recovery Fee (not a government-required tax or charge) of up to $3.18 (subject to change without notice; plus tax) per line of service. This fee may not apply to certain data devices/services. International rates and roaming charges may apply. Certain rates are subject to change at any time. If I have purchased a device under EIP, I will refer to my EIP agreement for the specific terms and conditions of that program.";
	public static final String SHOP_PAGE_ERROR = "Shop page is not displayed";
	public static final String RPF_TRF_APPLIES_TEXT ="For plans where taxes and fees are not included in the monthly recurring charge: Monthly Regulatory Programs (RPF) & Telco Recovery Fee (TRF) totaling $3.18 per voice line ($0.60 for RPF & $2.58 for TRF) and $1.16 per data only line ($0.15 for RPF & $1.01 for TRF) applies";
	public static final String VERIFY_REVIEW_CHANGES_PAGE="Review changes page is not displayed";
	
	public static final String BINGE_ON_TEXT_ERROR = "Binge on text is displaying";
	
	public static final String LEASEDETAIILS_PAGE = "Verify lease details page";
	public static final String LEASEDETAIILS_PAGE_ERROR = "Lease details page is not displayed";
	public static final String DEVICEPAYMENT_PAGE = "Verify device payment page";
	public static final String DEVICEPAYMENT_PAGE_ERROR = "Device payment is not displayed";

	public static final String HIGH_RATING = "Highest rating";
	public static final String PRICE_LOW_TO_HIGH = "Price low to high";
	public static final String PRICE_HIGH_TO_LOW = "Price high to low";
	public static final String FEATURED = "Featured";

	public static final String VERIFY_DEFEAULT_SORT_OPTION = "Verify defeault option";
	public static final String VERIFY_PRICES_LOWTOHIGH = "Prices are not in the low to high order";
	public static final String VERIFY_PRICES_HIGHTOLOW = "Prices are not in the high low order";
	public static final String VERIFY_FEATURED = "Verify featured devices";
	public static final String VERIFY_HIGH_RATING = "High rating is not in order";

	public static final String UPGRADE_PAGE_ERROR = "Upgrade page is not displayed";
	public static final String UPGRADE_PLAN_PAGE_ERROR = "Upgrade plan page is not displayed";
	public static final String UPGRADE_SERVICE_PAGE_ERROR = "Upgrade service page is not displayed";

	public static final String PAGINATION_ERROR = "Pagination not displayed on PLP";
	public static final String ELLIPSIS_ERROR = "Ellipsis are not displayed on PLP";
	public static final String ROWS_ERROR = "3 rows not displayed on PLP";
	public static final String DEVICES_ERROR = "4 devices not displayed per row on PLP";

	public static final String VERIFY_PAGINATION = "Verify pagination";
	public static final String VERIFY_ELLIPSIS = "Verify ellipsis";
	public static final String VERIFY_ROWS = "Verify 3 rows displayed on PLP";
	public static final String VERIFY_DEVICES = "Verify 4 devices displayed per row on PLP";

	public static final String VERIFY_PAYMENT_ARRANGEMENT = "Verify payment arrangement";
	public static final String VERIFY_PLANS_COMPARISION_PAGE = "Chnage Plans Page is Not Displayed";

	public static final String VERIFY_AA_DEEPLINKING = "Verified Account history tabs deeplinking to Documents, Payments Tabs .";
	public static final String VERIFY_DOWNLOAD_PDF = "Verified Account history PDF to download from render.";

	//public static final String VERIFY_PAYMENT_PAGE = "Verified payment page";

	public static final String VERIFY_BANK_INFO_HEADER = "Verified Bank Information Header";
	public static final String VERIFY_CARD_INFO_HEADER = "Verified Card Information Header";
	public static final String VERIFY_NEW_BANK_ACCOUNT = "Successfully added New Bank Information";
	public static final String VERIFY_NEW_CARD_ACCOUNT = "Successfully added New Card Information";
	public static final String VERIFY_SPOKE_PAGE = "Verified payment spoke page";
	public static final String STORED_PAYMENT_REMOVED = "Successfully removed stored payment type.";
	public static final String VERIFY_INSESSION_PAYMENT = "Successfully verified insesison payment type";

	public static final String PLP_PROMOTEXT = "Promo text not displayed";
	public static final String PLP_PROMO_DIALOGBOX_DEVICENAME = "Device name is displayed";
	public static final String PLP_PROMO_DIALOGBOX_DEVICEIMG = "Device image is displayed";

	public static final String PLP_PROMO_DIALOGBOX_SUBHEADING = "Promotion subheadiung is displayed";
	public static final String PLP_PROMO_DIALOGBOX_DESCRIPTION = "Promotion description is displayed";

	public static final String PLP_PROMO_DIALOGBOX_DEVICENAME_ERROR = "Device name not displayed";
	public static final String PLP_PROMO_DIALOGBOX_DEVICEIMG_ERROR = "Device image not displayed";
	public static final String PLP_PROMO_DIALOGBOX_SUBHEADING_ERROR = "Promotion subheadiung is not displayed";
	public static final String PLP_PROMO_DIALOGBOX_DESCRIPTION_ERROR = "Promotion description is not displayed";

	public static final String PLP_LEAGALTEXT_MODEL_HEADER_ERROR = "Legal text header is not displayed";
	public static final String PLP_LEAGALTEXT_MODEL_DESCRIPTION_ERROR = "Legal text desciption is not displayed";

	public static final String PLP_LEAGALTEXT_MODEL_HEADER = "Legal text header is displayed";
	public static final String PLP_LEAGALTEXT_MODEL_DESCRIPTION = "Legal text desciption is displayed";

	public static final String PLP_SORTING_DROPDOWN_ERROR = "Sorting dropdown is not displayed";
	public static final String PLP_FILTER_DROPDOWN_ERROR = "Filter dropdown is not displayed";

	public static final String PLP_SORTING_DROPDOWN = "Sorting dropdown is not displayed";
	public static final String PLP_FILTER_DROPDOWN = "Filter dropdown is not displayed";

	public static final String VERIFY_AUTO_PAY_BUTTON = "Verify AutoPay Button";
	public static final String VERIFY_PAYMENT_PAGE = "Verified payment page";
	public static final String LINESELECTOR_PAGE = "Line selector page is displayed";
	public static final String LINESELECTOR_PAGE_ERROR = "Line selector page is not displayed";
	public static final String CART_PAGE_ERROR = "Cart page is not displayed";
	public static final String VERIFY_CART_PAGE = "Cart page is displayed";
	public static final String PDP_PAGE_ERROR = "PDP page is not displayed";
	public static final String VERIFY_PDP_PAGE = "Verify PDP page is not displayed";
	public static final String PDP_PAGE_REVIEW_SECTION_ERROR = "Review section is not displayed";
	public static final String PDP_PAGE_NUMBER_OF_CUSTOMER_REVIEWS_ERROR = "Number of customer reviews are not displayed";
	public static final String PDP_PAGE_REVIEW_TAB_ERROR = "Reviews tab is not selected";
	public static final String PDP_PAGE_DEFAULT_REVIEW_SECTION_ERROR = "Default review section is not displayed";

	public static final String PAYMENT_PAGE_ERROR = "Payment page is not displayed";

	public static final String PAYMENT_PAGE_CARDNAME_ERROR = "Card name is allowing more then 55 characters";
	public static final String PAYMENT_PAGE_CARDNUMBER_ERROR = "Card number is allowing more then 16 characters";
	public static final String PAYMENT_PAGE_CVV_ERROR = "CVV is allowing more then 4 characters";
	public static final String PAYMENT_PAGE_UPDATEBUTTON_ERROR = "Update button is not displayed";
	public static final String PAYMENT_PAGE_CANCELBUTTOM_ERROR = "Cancel button is not displayed";

	public static final String VERIFY_CARDNAME_ERROR_MESSAGE = "Verify card name error message";
	public static final String VERIFY_CC_ERROR_MESSAGE = "Verify CC error message";
	public static final String VERIFY_EXPIRYDATE_ERROR_MESSAGE = "Verify expiry date error message";
	public static final String VERIFY_CVV_ERROR_MESSAGE = "Verify cvv error message";
	public static final String VERIFY_ADDRESSONE_ERROR_MESSAGE = "Verify address one error message";
	public static final String VERIFY_CITY_ERROR_MESSAGE = "Verify city error message";
	public static final String VERIFY_ZIPCODE_ERROR_MESSAGE = "Verify zip code error message";

	public static final String CARDNAME_ERROR_MESSAGE = "Enter a valid name.";
	public static final String CC_ERROR_MESSAGE = "Enter a valid credit card number.";
	public static final String EXPIRYDATE_ERROR_MESSAGE = "Enter a valid expiration date.";
	public static final String CVV_ERROR_MESSAGE = "Enter a valid CVV number.";

	public static final String ADDRESSONE_ERROR_MESSAGE = "Please enter correct address.";
	public static final String CITY_ERROR_MESSAGE = "Enter a valid city.";
	public static final String ZIPCODE_ERROR_MESSAGE = "Enter a valid ZIP code.";

	public static final String CARTPAGE_PREORDERED_TEXT = "Pre-Ordered is not displayed";
	public static final String CARTPAGE_BACKORDERED_TEXT = "Back-Ordered is not displayed";

	public static final String PREORDERED_TEXT = "PREORDER";
	public static final String BACKORDERED_TEXT = "Backordered:";

	public static final String EMPTY_CART_ERROR = "Empty cart text not displayed";
	public static final String EMPTY_CART = "Verify Empty cart";
	public static final String SERVICE_AGREEMENT_PAGE_ERROR = "Service agreement page is not displayed";
	public static final String ELECTRONIC_SIGNATURE_TERMS_ERROR = "Electronic signature terms page is not displayed";
	public static final String SHIPPINGDETAILS_PAGE_ERROR = "Shipping details page is not displayed";
	public static final String VERIFY_SHIPPINGDETAILS_PAGE = "Shipping details page is displayed";
	public static final String SHIPPING_INFORMATION_SECTION_ERROR = "Shipping information section is not displayed";

	public static final String SERVICE_AGREEMENT_PAGE = "Service agreement page  is displayed";
	public static final String ELECTRONIC_SIGNATURE_TERMS = "Electronic signature terms page is displayed";

	public static final String IAM_SERVER_ENVIRONEMT = "iam_servers";
	public static final String VERIFY_PAYMENT_ARRANGEMENT_RECORDS = "Verified Payments tab in Account history for Payment Arrangement Records";
	public static final String VERIFY_AALSAMSON_RECORDS_PAYMENT_TAB = "Verified Payments tab in Account history for AAL SAMSON records";

	public static final String VERIFY_AA_SEDONA_RECORDS_PAYMENT_TAB = "Verified Account history for Sedona records";
	public static final String PAYMENTS_LINK = "Payments";
	public static final String DOCUMENTS_LINK = "Documents";
	public static final String IN_MOBILE = " in Mobile";

	public static final String VERIFY_CARTPAGE_UNAV_HEADERFOOTER_LINKS = "Header footer links are displayed";
	public static final String ARTPAGE_UNAV_HEADERFOOTER_LINKS = "Verify Header footer links";

	public static final String FILTER_PLPPAGE_ERROR = "Filter functionality is not working as expected";
	public static final String VERIFY_FILTER_PLPPAGE = "Verify filter functionality";

	public static final String LINESELECTOR_PAGE_BACKARROW_ERROR = "Back arrow is not displayed";
	public static final String LINESELECTOR_PAGE_BACKARROW = "Verify Back arrow";

	public static final String VERIFY_LINESELCTOR_INELIGIBILITY_MES = "Verify ineligibility message";

	public static final String PDPPAGE_DOWNPAYMENT_MODEL_HEADER_ERROR = "Down payment model header is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_TEXT_ERROR = "Down payment model text is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_NOTE_ERROR = "Down payment model note is not displayed";

	public static final String PDPPAGE_DOWNPAYMENT_MODEL_HEADER = "Verify down payment model";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_TEXT = "Verify down payment text";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_NOTE = "Verify down payment note";

	public static final String PLPPAGE_PROMOSTAMPS_ERROR = "Promotional stamps are not displayed";
	public static final String VERIFY_PLPPAGE_PROMOSTAMPS = "Verify Promotional stamps";

	public static final String CARTPAGE_ORDER_HEADING = "Verify order confirmation heading";
	public static final String CARTPAGE_ORDER_NUMBER = "Verify order confirmation number";
	public static final String CARTPAGE_ORDER_ESTIMATED_DATE = "Verify estimated date";
	public static final String CARTPAGE_TRACKORDER_HERE = "Verify track order here";
	public static final String CARTPAGE_YES_TRADEIN = "Verify yes trade in";
	public static final String CARTPAGE_TRADEIN_ERROR = "Verify Trade-In header";
	public static final String CARTPAGE_NOTHANKS = "Verify no thanks link";

	public static final String CARTPAGE_ORDER_HEADING_ERROR = "Verify order confirmation heading is not displyed";
	public static final String CARTPAGE_ORDER_NUMBER_ERROR = "Verify order confirmation number is not displyed";
	public static final String CARTPAGE_ORDER_ESTIMATED_DATE_ERROR = "Verify estimated date is not displyed";
	public static final String CARTPAGE_TRACKORDER_HERE_ERROR = "Verify track order here is not displyed";
	public static final String CARTPAGE_YES_TRADEIN_ERROR = "Verify yes trade in is not displyed";

	public static final String CARTPAGE_OUTOF_STOCK_ERROR = "Out of stock model is not displayed";
	public static final String CARTPAGE_OUTOFSTOCK = "Verify out of stock model";
	public static final String ACC_ACTIVITY_TYPE_EIP = "eip";
	public static final String ACC_ACTIVITY_TYPE_REFUND = "Device payment refund";
	public static final String ACC_ACTIVITY_TYPE_REVERSAL = "Device payment reversal";
	public static final String ACC_BILLING_BRITE = "BriteBill bill should be displayed.";
	public static final String ACTIVITY_REVERSAL_DESC = "We successfully reversed";
	public static final String ACTIVITY_REFUND_DESC = "We successfully processed a payment refund";

	public static final String ACC_ACTIVITY_TYPE = "Verified and clicked activity type";
	public static final String ACC_ACTIVITY_DESC = "Verified activity activity description";
	public static final String PRINT_RECEIPT_LINK_NOT_DISPLAYED = "Print receipt link is not displayed";

	public static final String MANAGE_MULTI_LINE_LINK_VERIFY = "Manage Multi Line settings link is not displayed";
	public static final String MANAGE_MULTILINE_LINK_DISPLAYED = "Manage Multi Line settings link is displayed";
	public static final String PAYMENT_DECLINE = "Payment decline is not displayed";
	public static final String MONTHLY_CHARGES_MODEL_ERROR = "Monthly charges model is not displayed";
	public static final String VERIFY_MONTHLY_CHARGES_MODEL = "Verify monthly charges model";
	public static final String MONTHLY_CHARGES_MODEL_TOTAL_ERROR = "Total is not displayed in Monthly charges model";
	public static final String VERIFY_MONTHLY_CHARGES_MODEL_TOTAL = "Verify total in Monthly charges model";
	public static final String MONTHLY_CHARGES_MODEL_TAXESANDFEES_ERROR = "Taxes and fees is nod displayed in Monthly charges model";
	public static final String VERIFY_MONTHLY_CHARGES_MODEL_TAXESANDFEES = "Verify taxes and fees in monthly charges model";
	public static final String ACCESSIBILITY_PAGE_ERROR = "Accessibility Page is not displayed";
	public static final String ACCESSIBILITY_PAGE = "Accessibility page is displayed";
	public static final String EXPECTED_TEST_STEPS = "Expected Test Steps:";
	public static final String ACTUAL_TEST_STEPS = "Actual Test Steps:";
	public static final String CLICK_CURRENT_CHARGES_ROW = "Click current charges row on billing page";
	public static final String CURRENT_CHARGES_HEADER_VERIFY = "Currrent charges header on bill summary page is not displayed.";
	public static final String BILLINGGROUP_BOX_ERROR = "Billing highlights group box is not displayed.";
	public static final String AUTOPAY_SEARCH = "Auto Pay";

	public static final String YOUPAYTODAY_DOWN_PAYMENT_HEADING_ERROR = "Down payment heading is not displayed";
	public static final String YOUPAYTODAY_DOWN_PAYMENT_LINK_ERROR = "Down payment link is not displayed";

	public static final String YOUPAYTODAY_DOWN_PAYMENT_TEXT_ERROR = "Down payment text is not displayed";

	public static final String YOUPAYTODAY_SHIPPING_INFO_ERROR = "Shipping information is not displayed";
	public static final String YOUPAYTODAY_ORDER_TOTAL_ERROR = "Order toral is not displayed";

	public static final String DATAPASS_CONFIRMATION_PAGE_ERROR = "Data pass confirmation page is not displayed";
	public static final String HDDATAPASS_ERROR = "HD Data pass not added";

	public static final String DATAPASS_PAGE_LEGAL_INFO_ERROR = "Legalese information is not displayed";
	public static final String DATAPASS_PAGE_LEGAL_INFO = "Verify legalese information is displayed";
	public static final String PLANPAGE_ERROR = "Plan page is not displayed";

	public static final String UNAV_TMOBILE_ICON_ERROR = "T-mobile is not displayed in the header";
	public static final String UNAV_CONTACTUS_ICON_ERROR = "Contact us icon is not displayed in the header";
	public static final String UNAV_BACKARROW_ICON_ERROR = "Back arrow is not displayed in the header";

	public static final String ACC_ACTIVITY_DETAILS = "Verified activity activity payment type, reference Id, Payment Id.";
	public static final String ACC_ACTIVITY_TYPE_EIP_DESC = "Equipment Installment Plan";
	public static final String ACC_ACTIVITY_TYPE_DEVICE_PAYMENT = "New device payment posted";
	public static final String ACC_ACTIVITY_TYPE_PAYMENT = "New payment posted";
	public static final String ACC_ACTIVITY_TYPE_BILLING = "New bill available";
	public static final String ACC_ACTIVITY_TYPE_PAYMENT_CONFIRMATION = "Payment Arrangement Confirmation";
	public static final String CLICK_ACCOUNT_HISTORY = "Click on Account History link in Home Page.";
	public static final String CLICK_PAYMENT_TAB = "Click on Payments tab under Account History.";
	public static final String CLICK_BILLING_TAB = "Click on Billing tab under Account History.";

	public static final String DATALISTPAGE_FOOTER_ERROR = "Footer is not displayed in data list page";
	public static final String DATAPASS_REVIEWPAGE_FOOTER_ERROR = "Footer is not displayed in data pass review page";
	public static final String DATAPASS_CONFIRMATIONPAGE_FOOTER_ERROR = "Footer is not displayed in data pass confirmation page";

	public static final String DATAPASS_CONFIRMATIONPAGE_LEGAL_COPY_ERROR = "Legal copy is not displayed in data pass confirmation page";

	public static final String INTERNATIONAL_ROAMING_DISABLE_MESSAGE_ERROR = "International roaming disable message is not displyed";
	public static final String UNBLOCK_ROAMING_LINK_ERROR = "Unblock roaming link is not clickable";

	public static final String PLPPAGE_LEFTARROW_ERROR = "Left arrow is not displayed in PLP page";
	public static final String PLPPAGE_RIGHTARROW_ERROR = "Right arrow is not displayed in PLP page";
	public static final String PLPPAGE_LEFTARROW_DISABLED = "Left arrow is not disabled in PLP page";
	public static final String PLPPAGE_NUMBER_ERROR = "Page number is not highlighted in PLP page";

	public static final String PLPPAGE_LEGALTEXT_ERROR = "Legal text is not displayed";
	public static final String PDPPAGE_LEGALTEXT_ERROR = "Legal text is not displayed";
	public static final String PLPPAGE_LEGALTEXT_UNDERDEVICEIMAGE_ERROR = "Page number is not highlighted in PLP page";

	public static final String PAYMENTS_LABEL = "Payment #";
	public static final String DIALTONUMBER_IMEI = "*#06#";
	public static final String CARRIER_DROPDOWN_OPTION = "ATT";
	public static final String MAKE_DROPDOWN_OPTION = "Samsung";
	public static final String MODEL_DROPDOWN_OPTION = "SGH-A127";
	public static final String SAMSUNG_MAKE_DROPDOWN_OPTION = "Samsung";
	public static final String SAMSUNG_MODEL_DROPDOWN_OPTION = "GT-I9020A Nexus S - AT&T";
	public static final String PAYMENT_TYPE_NOT_DISPLAYED = "Payment category type is not displayed under ALL tab for Account History.";
	public static final String SERVICES_TYPE_NOT_DISPLAYED = "Services category type is not displayed under ALL tab for Account History.";
	public static final String DEVICE_TYPE_NOT_DISPLAYED = "Device category type is not displayed under ALL tab for Account History.";
	public static final String DOCUMENTS_TYPE_NOT_DISPLAYED = "Documents category type is not displayed under ALL tab for Account History.";

	public static final String TRADEIN_ANOTHER_DEVICEPAGE_ERROR = "Trade in another device page is not displayed";
	public static final String DEVICE_CONDITIONPAGE_ERROR = "Device conditions page is not displayed";
	public static final String DEVICE_CONDITIONPAGE_LEGAL_COPY_ERROR = "Legal copy is not displayed";

	public static final String WHATISTHISMODEL_TITLE_ERROR = "What is this model title is not displayed";
	public static final String WHATISTHISMODEL_HEADER_ERROR = "What is this model header is not displayed";
	public static final String WHATISTHISMODEL_BODY_ERROR = "What is this model body is not displayed";
	public static final String WHATISTHISMODEL_DIANIN_NUMBER_ERROR = "What is this model dial in number is not displayed";

	public static final String DEVICE_CONDITIONS_PAGE_BACKARROW_ERROR = "Back arrow is not displayed";
	public static final String DEVICE_CONDITIONS_IMEI_FORMAT_ERROR = "IMEI format is not coorect.";
	public static final String MAKE_DROPDOWN_OPTION_ERROR = "Make dropdown options are not populated";
	public static final String MODEL_DROPDOWN_OPTION_ERROR = "Model dropdown options are not populated";

	public static final String MAKE_DROPDOWN_DEFAULTOPTION_ERROR = "Make dropdown default option is not populated";
	public static final String MODEL_DROPDOWN_DEFAULTOPTION_ERROR = "Model dropdown default option is not populated";
	public static final String CLICK_PAY_BILL = "Click Pay bill button on home page";

	public static final String BACKARROW_TRADEINDEVICEANOTHER_PAGE = "Back arrow is not displayed";

	public static final String CARRIER_DROPDOWN_ERROR = "Carrier dropdon is not displayed";
	public static final String MAKE_DROPDOWN_ERROR = "Make dropdon is not displayed";
	public static final String MODEL_DROPDOWN_ERROR = "Model dropdon is not displayed";
	public static final String CONTINUENBUTTON_DISABLED = "Continue button is enabled";
	public static final String CONTINUENBUTTON_ENABLED = "Continue button is disabled";

	public static final String IMEI_NUMBER = "22 323232 323232 3";
	public static final String CLEAR_FILTER_PLPPAGE_ERROR = "Clear filter is not displayed";

	public static final String PLPPAGE_NO_ITEMS_FOUND_ERROR = "No items found is not displayed";
	public static final String PLPPAGE_NO_ITEMS_FOUND_MESSAGE_ERROR = "For more accurate results try removing some of your filters is displayed";
	public static final String ACC_ACTIVITY_TYPE_RECEIPT = "Receipt";

	public static final String IMEI_EIP_BALANCE_ERROR_MESSAGE = "IMEI EIP balance error message is not displayed";
	public static final String IMEI_INELIGIBLE_MESSAGE = "IMEI ineligible message is not displayed";
	public static final String IMEI_INVALID_NUMBER = "IMEI invalid number message is not displayed";

	public static final String DEVICE_HAS_ISSUE_PAGE_ERROR = "Device has issue page is not displayed";
	public static final String DEVICE_HAS_ISSUE_AGREECONTINUE_ERROR = "Agree and continue is not displayed in device has issue page";
	public static final String TRADEIN_ANOTHER_DEVICEBUTTON_ERROR = "Trade-In another device button is not displayed";
	public static final String FILE_INSURANCE_CLAIM_BUTTON_ERROR = "Fila insurance claim button is not displayed";
	public static final String LEGAL_COPY_ERROR = "Legal copy is not displayed";
	public static final String CUSTOMER_ACCEPTANCE_AGREEMENT_MODEL_ERROR = "Customer acceptance agreement model is not displayed";
	public static final String CONTINUE_TRADEIN_BUTTON_ERROR = "Continue Trade-In button is not displayed";
	public static final String SKIP_TRADEIN_BUTTON_ERROR = "Skip Trade-In button is not displayed";

	public static final String TRADEIN_ANOTHER_DEVICE_BUTTON_ERROR = "Trade-In another device button is not displayed";
	public static final String IMEI_SPECIALCHARS_DATA = "22 @#$%^& 323232 *";
	public static final String INVALID_IMEI_1 = "11 111111 111111 1";
	public static final String INVALID_IMEI_2 = "222222222222222";
	public static final String INVALID_IMEI_3 = "333333333333333";
	public static final String INVALID_IMEI_5 = "555555555555555";
	public static final String VALID_IMEI = "026726404663643";
	public static final String EIP_IMEI = "354383069116616";
	public static final String INVALID_IMEI_ERROR_MESSAGE = "IMEI invalid number message is not displayed";
	public static final String INVALID_DEVICE_ERROR_MESSAGE = "This device is not eligible for Trade in error message not displayed";
	public static final String IMEI_SPECIALCHARS_DATA_ERROR = "IMEI field is not allowing special chars";

	public static final String DDEVICEHASISSUESPAGE_BACK_ARROW_ERROR = "Back arrow is not displayed";
	public static final String DEVICEHASISSUESPAGE_MAKEANDMODEL_ERROR = "Make & Model options are not displayed";
	public static final String DEVICEHASISSUESPAGE_IMEI_ERROR = "IMEI is not displayed";
	public static final String DEVICEHASISSUESPAGE_TRADEINVALUE_ERROR = "Trade-In value is not displayed";
	public static final String DEVICEHASISSUESPAGE_ALTERNATESTEPTS_ERROR = "Alternate steps section is not displayed";

	public static final String TRADEINVALUEPAGE_BACK_ARROW_ERROR = "Back arrow is not displayed";
	public static final String TRADEINVALUEPAGE_MAKEANDMODEL_ERROR = "Make & Model options are not displayed";
	public static final String TRADEINVALUEPAGE_IMEI_ERROR = "IMEI is not displayed";
	public static final String TRADEINVALUEPAGE_VIABILLCREDIT_ERROR = "Via bill credit is not displayed";
	public static final String CUSTOMER_ACCEPTANCE_AGREEMENT_ERROR = "Customer acceptance agreement is not displayed";
	public static final String TRADEINVALUEPAGE_ERROR = "Trade-In value page is not displayed";

	public static final String PLP_PAGE_JUMPTOTOP_ERROR = "Jump To Top link is not displayed";

	public static final String UPGRADE_YOUR_DEVICE_ERROR = "Upgrade your device is not displayed";
	public static final String UPGRADE_TITLE_ERROR = "Upgrade title is not displayed";
	public static final String JUMP_UPGRADE_LOGO_ERROR = "Jump upgrade log is not displayed";
	public static final String JUMP_TEXT_ERROR = "Jump text is not displayed";
	public static final String UPGRADE_BUTTON_ERROR = "Upgrade button is not displayed";
	public static final String JUMP_ELIGIBLE_TEXT_ERROR = "Jump eligible text is not displayed";

	public static final String PHP_MIGRATION_PAGE_ERROR = "PHP migration page is not displayed";
	public static final String MYTMO_CLAIM_PAGE_ERROR = "My TMO claim page is not displayed";
	public static final String CART_PAGE_BACK_ARROW_ERROR = "Back arrow is not displayed in cart page";

	public static final String CART_PAGE_SHIPPING_ERROR = "Shipping is not displayed in cart page";
	public static final String CART_PAGE_SHIPPING_METHOD_ERROR = "Shipping method date is not displayed in cart page";
	public static final String CART_PAGE_SHIPDATE_ERROR = "Ship date is not displayed in cart page";
	public static final String CART_PAGE_SHIP_ADDRESS_ERROR = "Ship address is not displayed in cart page";
	public static final String CART_PAGE_TAXES_ERROR = "Taxes label is not displayed in cart page";
	public static final String CART_PAGE_ORDERTOTAL_ERROR = "Order total label is not displayed in cart page";
	public static final String CART_PAGE_ORDERTOTAL_AMOUNT_ERROR = "Order total is not equal the amounts for 'due today', 'Shipping', and 'Taxes'";

	public static final String DEVICE_IMAGE_ERROR = "Device image is not displayed.";
	public static final String NICK_NAME_ERROR = "Nick name is not displayed in cart page";

	public static final String DEVICE_DESCRIPTION_ERROR = "Device description is not displayed.";
	public static final String DEVICE_EIP_PRICE_ERROR = "Device EIP price is not displayed in cart page";
	public static final String REMOVE_MODEL_ERROR = "Remove model is not displayed in cart page";
	public static final String REMOVE_MODEL_BODY_TEXT_ERROR = "Remove model body description is not displayed";
	public static final String REMOVE_MODEL_BODY_TEXT = "Are you sure you want to remove this item from your cart?";
	public static final String NEW_DEVICE_PAYEMENT_ERROR = "New device payment label is not displayed";
	public static final String PREMIUM_DEVICE_PROTECTION_ERROR = "Premium device protection label is not displayed";
	public static final String SHOP_ACCESSORIES_BUTTON_ERROR = "Shop Accessoriesis button is not displayed";
	public static final String PDPPAGE_MEMORY_HEADER_ERROR = "Memory header is not displayed";
	public static final String PDPPAGE_FEATURES_ERROR = "Features header is not displayed";
	public static final String PDPPAGE_FEATURES_TAB_ERROR = "Features tab is not selected by default";

	public static final String PDPPAGE_SEEMORE_LINK_ERROR = "See more link is not displayed";
	public static final String PDPPAGE_SEELESS_LINK_ERROR = "See less link is not displayed";
	public static final String PDPPAGE_PREORDER_ERROR = "Pre-ordered is not displayed";
	public static final String PDPPAGE_BACKORDER_ERROR = "Back-ordered is not displayed";
	public static final String PDPPAGE_NO_REVIEWS_ERROR = "There are no reviews for this phone text is nod displayed";
	public static final String PDPPAGE_CONTINUE_BUTTON_ERROR = "Continue button is not displayed";
	public static final String PDPPAGE_ADDTOCART_ERROR = "Add to cart button is not displayed";

	public static final String CARTPAGE_ACCEPTANDCONTINUE_ERROR = "Accept and continue button is not displayed";
	public static final String CARTPAGE_ACCEPTANDPLACEORDER_ERROR = "Accept and place order button is not displayed";

	public static final String HEADER_ERROR = "Header is displayed in shipping details page";
	public static final String FOOTER_ERROR = "Footer is displayed in shipping details page";

	public static final String MANUFACTURER_LABEL_ERROR = "Manufacturer label is not displayed";
	public static final String OPERATING_SYSTEM_ERROR = "Operating system label is nod displayed";
	public static final String BREADCRUMB_ERROR = "Breadcrumb is not displayed";
	public static final String FILTER_DEVICE_LIST_ERROR = "Filtered device is not displayed";
	public static final String KICKBACK_ERROR_MSG = "Kickback was not applied to some of your lines because it went over 2GB.";
	public static final String CLICK_CREDITS_AND_ONE_TIME_CHARGES_LINK = "Click on Credits and One time charges link on Bill Summary page";
	public static final String VERIFY_CREDITS_AND_ONE_TIME_CHARGES_MODAL = "Verify Credits and One time charges modal is displayed.";
	public static final String CREDITS_AND_ONE_TIME_CHARGES_MODAL_DISPLAYED = "Credits and One time charges modal is displayed.";
	public static final String VERIFY_NET_PRICE_LINE_ITEMS = "Verify Net Price Line Items are displayed";
	public static final String NET_PRICE_LINE_ITEMS_DISPLAYED = "Net Price Line Items are displayed";
	public static final String VERIFY_TOTAL_AMOUNT = "Verify Total Amount is displayed correctly";
	public static final String TOTAL_AMOUNT_DISPLAYED = "Total Amount is displayed correctly";

	public static final String OUTOF_STOCK_ERROR = "Out of stock is not displayed";
	public static final String ADD_TO_CART_ERROR = "Add to cart button is enabled";
	public static final String ESTIMATED_SHIP_DATE_ERROR = "Estimated ship date is displayed";

	public static final String CONFIRMATION_PAGE_ERROR = "Confirmation page is not displayed";

	public static final String CONFIRMATION_PAGE_ORDERSTATUSCOPY_ERROR = "Order status copy is not displayed";
	public static final String VERIFY_3RD_PARTY_SERVICES_SECTION = "Verify 3rd party services section is displayed in Billing Details Page.";
	public static final String VERIFY_TI_LINE_ITEM = "Verify Tax Inclusive line item is displayed.";
	public static final String VERIFY_TE_LINE_ITEM = "Verify Tax Exclusive line item is displayed.";
	public static final String CLICK_TAXES_AND_FEES_INCLUDED_LINK = "Click on Taxes and Fees Included link.";
	public static final String VERIFY_TAXES_AND_FEES_INCLUDED_MODAL = "Verify Taxes and Fees Included modal is displaeyd.";
	public static final String CUSTOMER_ACCEPTANCE_AGREEMENT_MODEL_TEXT = "By clicking Continue trade-in, you are accepting the offer price and have read and agreed to Customer Acceptance Agreement. Your device's final value will be based upon full valuation of Trade-in device upon receipt.";
	public static final String DRP_LINK_ERROR = "DRP link is not displayed in Customer acceptance agreement.";
	public static final String DRP_LINK_MODAL_ERROR = "DRP modal is not displayed";
	public static final String GENERAL_LEGAL_TERMS_ERROR = "General Legal Terms is not displayed";
	public static final String GENERAL_LEGAL_TEXT = "You can trade in another device or skip trade-in for now. You will also be able to file an insurance claim toward this device at the end of purchase transaction.";
	public static final String ESTIMATED_SHIP_DATE_FORMAT_ERROR = "Estimated ship date is not displayed with forward slashes";
	public static final String LEGAL_TEXT_PDPPAGE = "If you cancel wireless service, remaining balance on device's full retail price";

	public static final String REMOVE_MODEL_PREVIOUSMONTHLYCHARGES_LABEL = "Previous Monthly charges";
	public static final String REMOVE_MODEL_NEW_DEVICE_PAYMENT_LABEL = "New device payment";
	public static final String REMOVE_MODEL_TAXES_FEES_LABEL = "+Taxes and fees not already included";

	public static final String SELECTED_SHIP_METHOD_ERROR = "Selected ship method is not displayed";
	public static final String SHIPMETHOD_CURRENTSELECTION_ERROR = "Current shipping method is not highlighted";
	public static final String SHIPPING_METHOD_ORDER_ERROR = "Shipping method order is not displayed from lowest to higest";
	public static final String ACCEPT_AND_CONTINUE_ERROR = "Accept & continue button is enabled";

	public static final String DEVICE_MEMORY_ERROR = "Device memory is not displayed";
	public static final String DEVICE_COLOR_ERROR = "Device color is not displayed";

	public static final String CHECKORDER_STATUS_ERROR = "Check order status is not displayed";
	public static final String CONTACT_US_ERROR = "Contact us link is not displayed";
	public static final String RETURN_POLICY_ERROR = "Return policy link is not displayed";
	public static final String STORE_LOCATOR_ERROR = "Store locator link is not displayed";

	public static final String SHOP_PAGE_KEEP_YOUR_DEVICE_ERROR = "Keep your device is not displayed";
	public static final String SHOP_PAGE_BUYASIM_ERROR = "Buy a sim button is not displayed";
	public static final String SHOP_PAGE_ACCESSORIES_ERROR = "Accessories header is not displayed";
	public static final String SHOP_PAGE_SHOP_ACCESSORIES_BUTTON_ERROR = "Shop Accessories button is not displayed";
	public static final String COLOR_LABEL_ERROR = "Color label is not displayed";

	public static final String FIND_MY_IPHONE = "Find my iPhone";
	public static final String ANTI_THEFT_FEATURE_ANDROID_DEVICE = "Have you disabled the anti-theft feature?";
	public static final String DEVICE_CONDITION_RADIO_BUTTONS_ERROR = "Device Codition selection radio buttons are not displayed.";
	public static final String DEVICE_POWERS_ON = "Device powers on";
	public static final String NO_LIQUID_DAMAGE = "No liquid damage";
	public static final String NO_SCREEN_DAMAGE = "No screen damage";
	public static final String DOESNT_POWER_ON = "Doesn't power on";
	public static final String HAS_LIQUID_DAMAGE = "Has liquid damage";
	public static final String HAS_SCREEN_DAMAGE = "Has screen damage";

	public static final String GOOD_POINT_NOT_DISPLAYED = " point is not displayed under It's Good - Device Condition page";
	public static final String BAD_POINT_NOT_DISPLAYED = " point is not displayed under It's Good - Device Condition page";

	public static final String PDP_PAGE_PAYMENT_OPTION_ERROR = "Payment option is not selected";
	public static final String PDP_PAGE_PRICINGOPTION_HEADER_ERROR = "Pricing option header is not displayed";
	public static final String CVV_TOOPTIP_HEADER_ERROR = "CVV tooltip model header is not selected";
	public static final String CVV_TOOPTIP_IMAGE_ERROR = "CVV tooltip model image is not selected";
	public static final String SHOP_PAGE_LEGAL_TEXT_ERROR = "Legal text is not displayed";
	public static final String SHOP_PAGE_SELECTDEVICE_BUTTON_ERROR = "Select device button is not displayed";
	public static final String VERIFY_HOME_PAGE_DISPLAYED = "Home page is displayed";

	public static final String LINE_SELECTOR_PAGE_UPGRADE_BUTTON_ERROR = "Upgrade button is not displayed for EIP paid off Just Customer";
	public static final String LINE_SELECTOR_PAGE_EIPPAIDOFF_MESASGE_ERROR = "EIP paid off message is not displayed";
	public static final String INSURANCE_MIGRATION_PAGE_ERROR = "Insurance migration page is not displayed";

	public static final String REVIEW_SECTION_MOST_HELPFUL_ERROR = "Review section(Most Helpful) is not displayed";
	public static final String REVIEW_SECTION_NEWEST_ERROR = "Review section(Newest) is not displayed";
	public static final String REVIEW_SECTION_OLDEST_ERROR = "Review section(Oldest) is not displayed";
	public static final String REVIEW_SECTION_HIGHEST_RATING_ERROR = "Review section(Highest Rating) is not displayed";
	public static final String REVIEW_SECTION_LOWEST_RATING_ERROR = "Review section(Lowest Rating) is not displayed";
	public static final String ADD_A_LINE_LINK_ERROR = "Add a line link is not displayed";
	public static final String SCNC_CUSTOMER_ELIGIBILITY_ERROR = "SCNC is not eligible for upgrade";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE = "The Regulatory Programs (RPF) & Telco Recovery Fee (TRF) is not a government tax, but a fee collected and retained to recover government compliance costs (RPF), plus charges imposed by other carriers for calls between our customers and theirs, and by 3rd parties for network facilities and services we purchase to provide service (TRF). Of the $3.18 total charged for voice lines, $0.60 is for the RPF & $2.58 is for the TRF. Of the $1.16 total charged for data only lines, $0.15 is for the RPF & $1.01 is for the TRF.";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_SPANISH = "El cargo por Programas Reguladores (RPF) y el cargo por Recuperación de los Costos de Compañía de Telecomunicaciones (TRF) no son impuestos gubernamentales, sino cargos cobrados y retenidos para recuperar costos de cumplimiento con el gobierno (RPF), más cargos aplicados por otros proveedores por llamadas entre nuestros clientes y los suyos, y por terceros por servicios y funciones de red que compramos para proporcionar servicio (TRF). Del total de $3.18 que se cobra para líneas de voz, $0.60 es por el RPF y $2.58 es por el TRF. Del total de $1.16 que se cobra para líneas sólo de datos, $0.15 es por el RPF y $1.01 es por el TRF.";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_AAL = "For plans where taxes and fees are not included in the monthly recurring charge: Monthly Regulatory Programs (RPF) & Telco Recovery Fee (TRF) totaling $3.18 per voice line ($0.60 for RPF & $2.58 for TRF) and $1.16 per data only line ($0.15 for RPF & $1.01 for TRF) applies. Taxes approx. 6-28% of bill";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_ERROR = "AAL - TRF and Total fees details are not updated in legal desclaimer text.";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_JUMP = "I will be charged a monthly Regulatory Programs & Telco Recovery Fee (not a government-required tax or charge) of up to $3.18 (subject to change without notice; plus tax) per line of service. This fee may not apply to certain data devices/services. International rates and roaming charges may apply. Certain rates are subject to change at any time. If I have purchased a device under EIP, I will refer to my EIP agreement for the specific terms and conditions of that program.";

	public static final String MESSAGE_US_AND_CALL_US_ERROR = "We're sorry, our systems are currently unavailable. Please contact us at 1-800-937-8997 or dial 611 from your T-Mobile device.";
	public static final String MESSAGE_US_ERROR = "We're sorry, Message Us is currently unavailable. Please contact us using Call Me instead!";
	public static final String CALL_US_ERROR = "We're sorry, Call Me is currently unavailable. Please contact us using Message Us instead!";

	public static final String NEED_HELP_LINK = "Need help link is not displayed";
	public static final String DEVICE_INFORMATION = "Device information is not displayed";
	public static final String VERIFY_IMEI_CALL_TEXT = "Verify IMEI call text is not displayed";

	public static final String ACCEPT_AND_CONTINUE_BUTTON_ERROR = "Accept and continue button is not displayed";
	public static final String TRADEIN_CONFIRMATION_PAGE_ERROR = "Trade-In confirmation page is not displayed";
	public static final String EIP_STRIKE_THROUGH_AMOUNT_ERROR = "EIP strike through is not displayed";
	public static final String BILL_CREDIT_AMOUNT = "Bill credit amount is not displayed";
	public static final String CHECKOUT_PAGE_VERIFY = "Checkout page is not displayed.";
	public static final String CLICK_ON_ADDTOCART_BUTTON = "Click On Add To Cart Button";

	public static final String VERIFY_JUMP_DEVICE_VERIFICATION_PAGE = "Verify Jump device verification page.";
	public static final String CLICK_CONTINUE_BUTTON = "Click continue button";
	public static final String ANSWER_QUESTIONS_AND_CONTINUE = "Answer all the questions and Click continue button";
	public static final String OFFER_DETAILS_PAGE_ERROR = "Offer details page is not displayed";
	public static final String FREQUENTILY_ASKED_QUESTIONS_ERROR = "Frequently asked questions section is not displayed";
	public static final String REDEMPTION_SEPTS_ERROR = "Redemption steps are not displayed";
	public static final String ERROR_MESSAGE = "Error message is not displayed";
	public static final String CONTINUE_WITHOUT_THE_OFFER_ERROR = "Continue without the offer link is displayed";
	public static final String CAROUSEL_INDICATORS_ERROR = "Carousel indicators are not displayed";
	public static final String MARQUEE_BANNER_RIGHTARROW_ERROR = "Marquee banner right arrow is not displayed";
	public static final String MARQUEE_BANNER_LEFTARROW_ERROR = "Marquee banner left arrow is not displayed";
	public static final String DEVICE_LIST_SECTION_ERROR = "Device list section is not displayed";

	public static final String BILLING_ADDRESS_HEADER = "Billing address header is not displayed";

	public static final String BILLING_ADDRESS_ADDRESS_TEXTBOX = "Billing address text box is not auto populated";
	public static final String BILLING_ADDRESS_CITY_ADDRESS = "Billing address city text box is not auto populated";
	public static final String BILLING_ADDRESS_STATE_DROPDOWN = "Billing address state is not auto populated";
	public static final String BILLING_ADDRESS_ZIP_TEXTBOC = "Billing address zip code is not auto populated";

	public static final String SHIPPING_METHOD_SUBHEADER = "Shipping method sub header is not displayed";
	public static final String DRP_MODEL_ERROR = "DRP model is not displayed";
	public static final String TRADEIN_PAGES_CHAT_BOX_DISPLAYED = "Chat box is displayed in Trade in pages.";
	public static final String TRADEIN_PAGES_SEDONA_HEADER_DISPLAYED = "Sedona header is displayed in Trade in pages.";

	public static final String FRP_HOLDUP_MODEL_ERROR = "FRP hold up model is not displayed";
	public static final String JUMP_IMEI_PAGE_ERROR = "Jump IMEI page is not displayed";
	public static final String JUMP_PRODUCT__PHP_PAGE_ERROR = "Jump product php page is not displayed";
	public static final String FRP_HOLDUP_MODEL_OK_BUTTON_ERROR = "FRP hold up model ok button is not displayed";
	public static final String FRP_HOLDUP_MODEL_PROMOTION_TEXT_ERROR = "FRP hold up model promotion text is not displayed";
	public static final String DOWNLOAD_BILLING_CHARGES_VERIFY = "Error downloading the PDF file.";
	public static final String PROMOTION_MODEL_ERROR = "Promotion model is not displayed";
	public static final String PROMOTION_MODEL_CLOSE_BUTTON_ERROR = "Promotion model close button is not displayed";
	public static final String ADD_A_LINE_PAGE_ERROR = "Add a line page is not displayed";
	public static final String ADD_A_LINE_SECTION_ERROR = "Add a line section is not displayed";
	public static final String ADD_A_LINE_BUTTON_ERROR = "Add a line button is not displayed";

	public static final String JUMP_CHOICE_PAGE_ERROR = "Jump choice page is not displayed";
	public static final String JUMP_CHOICE_PAGE_DEVICE_IMAGE_ERROR = "Jump choice page device image is not displayed";
	public static final String JUMP_CHOICE_PAGE_TERMSANDCONDITIONS_ERROR = "Jump terms & conditions link is not displayed";
	public static final String JUMP_CHOICE_PAGE_GET_THE_OFFER_SECTION_ERROR = "Get the offer section is not displayed";
	public static final String JUMP_CHOICE_PAGE_USE_JUMP_ERROR = "User jump section is not displayed";

	public static final String WHY_DO_I_NEED_TO_PAY_LINK_ERROR = "Why do i need to pay link is not displayed";
	public static final String HOW_DOES_JUMP_WORK_LINK_ERROR = "How does jump work link is not displayed";

	public static final String CART_PAGE_EIP_TEXT_ERROR = "EIP text is not displayed";
	public static final String SEDONA_HEADER_ERROR = "Sedona header is not displayed";
	public static final String TRADEIN_VALUE_PAGE_PROMO_TEXT = "Trade-in value page promo text is not displayed";
	public static final String TRADEIN_VALUE_PAGE_PROMO_MESSAGE_GREEN_TICKMARK = "Trade-in value page promo message green tick mark is not displayed";
	public static final String MARQUEE_BANNER_LEGAL_TEXT = "Marquee banner legal text is not displayed";
	public static final String LEGAL_VERIBIAGE_ERROR = "Legal veribiage text is not displayed";
	public static final String LEGAL_VERIBIAGE_TEXT = "IF YOU CANCEL WIRELESS SERVICE, REMAINING BALANCE ON PHONE BECOMES DUE. 0% APR O.A.C. for well-qualified customers.";

	public static final String DRPLEGALTEXT_ERROR = "DRP Legal Text is not displayed";
	public static final String HOW_DOES_JUMP_WORK_MODEL_ERROR = "How does jump work model is not displayed";
	public static final String JUMP_PROGRAM_INFORMATION_ERROR = "Jump program information is not displayed";
	public static final String WHY_DO_I_NEED_TO_PAY_ERROR = "Why do i need to pay model is not displayed";
	public static final String REASON_TO_PAY_EIP_ERROR = "Reason to pay EIP is not displayed";
	public static final String TODAY_PRICING_USE_JUMP_SECTION_ERROR = "0 is not displayed for today pricing under use jump section";
	public static final String DEVICE_NOT_ELIGIBLE_MESSAGE_ERROR = "Device not eligible message is not displayed";

	public static final String AP_WITH_BAL_IN_BLACKOUT_PERIOD = "AutoPay will still process the full payment for your balance due on: (bill due date)";
	public static final String AP_WITH_BAL_OUT_BLACKOUT_PERIOD = "A one-time payment will be required for any outstanding balance due on: ";
	public static final String AP_WITH_BAL_OUT_BLACKOUT_PERIOD_PAST_DUE_DATE = "A one-time payment will be required for any outstanding balance due on: ";
	public static final String AP_WITH_BAL_NO_DUE_DATE = "A one-time payment will be required for any outstanding balance if you are cancelling within 2 days of your next bill due date.";
	public static final String AP_CANCEL_WARNING = "Warning: If you cancel AutoPay, you will lose your ";
	public static final String AP_CANCEL_CONFIRM_WARNING = "You will no longer receive the AutoPay discount";

	public static final String PHONE_SELECTION_PAGE_ERROR = "Phone selection page is not displayed";
	public static final String EIP_PAID_OFF_STATUS = "EIP paid off status is not displayed";
	public static final String EMAIL_NOTIFICATION_ERROR = "Email notification is not displayed";
	public static final String BILL_CREDITMESSAGE_ERROR = "Bill CreditMessage is displayed";
	public static final String FIND_MY_IPHONE_IS_TURNED_ON = "will be turned on";
	public static final String DISABLE_ANTI_THEFT_FEATURE = "Anti Theft feature not disable";

	public static final String PAYMENT_OPTION_EIP = "Monthly payments";
	public static final String PAYMENT_OPTION_FRP = "Full Retail Price";

	public static final String TOTAL_DEVICES_RESULTS = "Total No Of Devices Results not displayed";
	public static final String APPLE_DEVICES = "All are not only Apple Devises in PLP Page";
	public static final String SAMSUNG_DEVICES = "All are not only Samsung Devises in PLP Page";
	public static final String INLINE_PAYMENT_ERRORS = "We are unable to accept payments from this account.";
	public static final String INLINE_BANK_PAYMENT_ERRORS = "We are unable to accept bank account payments on your account.";
	public static final String INLINE_CARD_PAYMENT_ERRORS = "We are unable to accept card payments on your account.";

	public static final String ITS_GOOD_SECTION_ERROR = "It's good section is not displayed";
	public static final String ITS_GOT_ISSUES_SECTION_ERROR = "It's got issues section is not displayed";

	public static final String ITS_GOOD_SECTION_DEVICE_POWERS_ON_ERROR = "Device powers on point is not displayed";
	public static final String ITS_GOOD_SECTION_NO_LIQUID_DAMAGE_ERROR = "No liquid damage point is not displayed";
	public static final String ITS_GOOD_SECTION_NO_SCREEN_DAMAGE_ERROR = "No screen damage point is not displayed";

	public static final String ITS_GOT_ISSUES_SECTION_HAS_LIQUID_DAMAGE_ERROR = "Has liquid damage point is not displayed";
	public static final String ITS_GOT_ISSUES_SECTION_DOES_NOT_POWER_ON_ERROR = "Doesn't power on point is not displayed";
	public static final String ITS_GOT_ISSUES_SECTION_HAS_SCREEN_DAMAGE_ERROR = "Has screen damage point is not displayed";

	public static final String SECOND_LINE_ERROR = "Second line not added";
	public static final String AP_MSG_WITH_BAL_IN_BLACKOUT_PERIOD = "Next AutoPay payment: (bill due date + 1 calendar month - 2 days)";
	public static final String AP_MSG_WITH_BAL_OUT_BLACKOUT_PERIOD = "Next AutoPay payment: (bill due date -  2 days)";
	public static final String AP_MSG_WITHOUT_BAL_OUT_BLACKOUT_PERIOD = "Next AutoPay payment: (past bill due date + 1 calendar month - 2 days)";
	public static final String AP_MSG_WITH_INVALID_DUE_DATE = "Next AutoPay payment: (past bill due date + 1 calendar month - 2 days) ";
	public static final String AP_MSG_WITHOUT_DUE_DATE = "Recurring payments for your full balance will be processed 2 days before each bill due date.";

	public static final String TRACK_YOUR_ORDER_ERROR = "Track Your Order Button Error";
	public static final String ORDER_NUMBER_ERROR = "Order number Error";
	public static final String HOME_BUTTON_ERROR = "Home Button Error";
	public static final String ADDED_DEVICE_ERROR = "New Devece not added";

	public static final String TRADEIN_VALUE_PAGE_AGREE_AND_CONTINUE = "Agree & continue button is not displayed";
	public static final String PROMO_MONTHLYBILL_CREDIT_ERROR = "Promo MonthlyBill Credit Column is not displayed";

	public static final String PLAN_PAGE_ERROR = "plan page is not displayed";
	public static final String DEFAULT_DROPDOWN_OPTION = "Select";
	public static final String ALTERNATE_STEPS = "You may be able to file an insurance claim after you purchase your new device.";
	public static final String CREDIT_NOTIFICATION_ERROR = "Credit notification message not displayed";

	public static final String CARDNAME_LENGTH = "testtesttesttesttesttesttesttesttesttesttesttesttest";
	public static final String CARD_LENGTH = "4444444444444444444444";
	public static final String CVV_LENGTH = "12345";
	public static final String PDPPAGE_MEMORYSIZE_ORDER_ERROR = "PDP Page Memory Sizes are not ordered";


	public static final String STATE = "NY";
	public static final String LCASE_STATE = "ny";
	public static final String INVALID_STATE = "NC";
	public static final String SHIPPING_ADDRESS_ERROR = "Shipping address is not changed";
	public static final String SHIPPING_METHOD = "3-Day shipping";


	public static final String AUTOPAY_NO_DUE_DATE_OTP_CONFIRM_PAGE = "Recurring payments for your full balance will be processed 2 days before your bill due date.";
	public static final String AUTOPAY_NO_DUE_DATE_OTP_CONFIRM_PAGE_ERROR = "Generic Auto pay text is not displayed for no Due date in OTP Confirmation Page";
	public static final String OTP_CONFIRM_PAGE_AUTOPAY_ERROR = "Expected Autopay text is not displayed in OTP confirmation page.";
	public static final Object SETUP_AUTOPAY = "Save time and avoid late fees by setting up";
	public static final Object LEGAL_TNC = "By submitting, you agree to the Terms & Conditions";
	public static final String DEVICE_PROTECTION_SECTION = "Device Protection Section not displayed in New Monthly Total Breakdown";
	public static final String MINUS_SIGN_ERROR = "Minus Sign not displayed in front of Downgrade Device Protection Section";
	public static final String DRP_LINK = "DRP Customer Acceptance legal copy displayed";
	public static final String DRP_CUSTOMER_ACCEPTANCE_LINK_ERROR = "DRP Customer Acceptance link displayed";
	
	
	public static final String EIP_PAID_OFF_JUMP = "You have paid off your JUMP device. You will be taken to the Standard Upgrade flow.";	
	
	public static final String STANDALONE_SERVICES_ERROR = "Stand alone services not displayed";
	public static final String SERVICE_NOT_ADDED = "Service price not displayed";
	public static final String JUMP_BUNDLES_ERROR = "Jump bundels error";
	public static final String SERVICES_HIGH_TO_LOW_ODRER_ERROR = "Service prices are not in order";
	public static final String DEFAULT_RADIO_BUTTON = "Default radio button not selected high price";
	
	public static final String TRADEIN_PAGE_ERROR = "Tradein page is not displayed";
	public static final String CREDIT_NOTIFICATION_DISPLAYED_ERROR = "Credit notification message should not displayed";
	public static final String APPLE_IPHONE_8 = "Apple iPhone 6s";
	public static final String SAMSUNG_GALAXY_NOTE8 = "Samsung Galaxy Note8";
	public static final String LEGAL_TEXT = "On all T-mobile Plans,if congested,top 3% of data users text is not displaying";
	
	public static final String CHANGE_RATE_PLAN_ERROR = "Change rate plan is not displayed";
	public static final String PLP_JUMP_UPGRADE_PAGE_ERROR = "Jump upgrade page is not displayed";
	
	public static final String BENEFITS_PAGE_VERIFICATION = "Benefits page not displayed";
	public static final String CRAZYLEGS_BENEFITS_PAGE_LEGALEASE = "Redeeming this offer will replace your current Netflix payment method with T-Mobile. On all T-Mobile plans, if congested, customers using >50GB/mo. may notice reduced speeds due to prioritization. Video typically streams at 480p. Tethering at max 3G speeds. Int'l speeds approx. 128 kbps. Limited time offer; subject to change. Receive Netflix Standard 2-screen (up to a $10.99/mo. value) while you maintain 2+ qual'g T-Mobile ONE lines in good standing. Value may be applied to different Netflix streaming plans. Not redeemable or refundable for cash. Cancel Netflix anytime. Netflix Terms of Use apply: www.netflix.com/termsofuse. 1 offer per T-Mobile account; may take 1-2 bill cycles. Like all plans, features may change or be discontinued at any time; see T-Mobile Terms and Conditions for details.";
	public static final String VERIFY_IMPORTANT_MESSAGE = "Save up to $10 per line with KickBack™.";
	public static final String DATA_PASS_ERR ="You do not have permission to purchase data pass. Please contact your primary account holder to make a purchase.";

	public static final String DATA_PASS_INT_ERR = "International roaming is currently disabled on your account. To purchase a roaming data pass";
	public static final String PDP_FRP_PRICING_ERROR = "PDP FRP price is not matched with PLP FRP price";
	
	public static final String PUERTO_RICO_CUSTOMER_ELIGIBILITY_MESSAGE_ERROR = "Puerto rico customer eligibility message is not displayed";
	public static final String PREMIUM_DEVICE_PROTECTION_PLUS_SYMBOL_ERROR = "Plus symbol is displayed for premium device protection null amount";
	
	public static final String IPHONE7_PLP_PAGE_ERROR = "iPhone 7 is not displayed in product list page";
	public static final String PREMIUM_DEVICE_PROTECTION_YES_ERROR = "Premium device protection yes is not displayed";
	
	public static final String PLP_PAGE_FILTER_FEATURE_COLUMN_ERROR = "Feature section is displayed";
	public static final String PLP_PAGE_DEALS_COLUMN_ERROR = "Deals column not displayed";
	public static final String PLP_PAGE_ONSALE_OPTION_ERROR = "On sale option not displayed";
	public static final String PLP_PAGE_ONSALE_FILTER_DEVICES = "On sale devices not displayed";
	
	public static final String TMOBILE_QAPROD_HOME_PAGE_VERIFY = "User is not Navigated To T MOBILE Home Page.";
	public static final String CANCEL_MODAL_TEXT = "Are you sure you want to leave this page? Any changes you have made will not be saved.";
	public static final String PA_CANCEL_MODAL_TEXT = "Are you sure you want to leave this page? Any changes you have made will be lost.";
	public static final String OTP_CANCEL_MODAL_TEXT = "Are you sure you want to cancel this transaction?";
	
	//Port-In Confirmation Page
	public static final String PORTIN_CONF_HEADER = "Thanks !";
	public static final String PORTIN_CONF_SUBHEADER = "We'll submit this info for your transfer. We'll let you know if we run into any other issues.";
	
	//Port-In Input Page
	public static final String PORTIN_INPUT_HEADER = "Thanks !";
	public static final String PORTIN_INPUT_SUBHEADER = "We'll submit this info for your transfer. We'll let you know if we run into any other issues.";
	public static final String PORTIN_INPUT_HELPER = "Type a unique phoneNumber.";
	public static final String PORTIN_INPUT_PHONE_ERROR = "12345";
	public static final String PORTIN_INPUT_PHONE_ENTER = "4043696485";
	
	//Invalid Card Details
	public static final String INVALID_CARDNUMBER="5894";
	public static final String INVALID_EXPIRYDATE="06";
	public static final String INVALID_CVV="09";

	//LazerShark Auth Page
	public static final String LS_AUTH_LOGIN_USER = "brtadmin";
	public static final String LS_AUTH_LOGIN_PWD = "brtadmin";
	public static final String LS_AUTH_LEGAL_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla vitae lorem vitae";
	public static final String LS_AUTH_LEGAL_LINK = "Terms and conditions";
	public static final String LS_AUTH_LEGAL_MODEL = "Terms and conditions Agree";
	public static final String LS_AUTH_LEGAL_MODEL_BUTTON = "I Agree";
	public static final String LAZERSHARK_CARRIER_HEADER = "Who is your current wireless provider?";
	public static final String LAZERSHARK_PROVIDER = "AT&T";
	public static final String LAZERSHARK_FORM1_UR_HEADER = "How would you rate your experience?";
	public static final String LAZERSHARK_FORM1_TFD_HEADER = "Are you interested in T-Mobile for Business?";
	public static final String LAZERSHARK_FORM1_TFB_YES = "Yes";
	public static final String LAZERSHARK_FORM1_TFB_NO = "No";
	public static final String LAZERSHARK_FORM1_CONTINUE = "Continue";
	public static final String LAZERSHARK_CUST_FNERROR = "Please enter your first name";
	public static final String LAZERSHARK_CUST_LNERROR = "Please enter your last name";
	public static final String LAZERSHARK_CUST_EMAILERROR = "Please enter your email";
	public static final String LAZERSHARK_CUST_CEMAILERROR = "Please confirm your email";
	public static final String LAZERSHARK_CUST_PHERROR = "Please enter correct phone";
	public static final String LAZERSHARK_CUST_IMEITIPMESS = "You can find your IMEI by dialing *#06# from your device.";
	public static final String LAZERSHARK_CUST_IMEIERROR = "Please enter correct IMEI number";
	public static final String LAZERSHARK_CUST_CONTINUE_BUTTON = "Continue";
	public static final String LAZERSHARK_CUST_BACK_BUTTON = "Back";
	public static final String LAZERSHARK_SHIP_ADDR1ERROR = "Please enter your address";
	public static final String LAZERSHARK_SHIP_CITYERROR = "Please enter your city";
	public static final String LAZERSHARK_SHIP_STATEERROR = "Please enter your state";
	public static final String LAZERSHARK_SHIP_ZIPERROR = "Please Enter Correct Zip Code";
	public static final String LAZERSHARK_SHIP_SUBMIT_BUTTON = "Submit";
	public static final String LAZERSHARK_SHIP_BACK_BUTTON = "Back";
	public static final String LAZERSHARK_SHIP_MODAL_HEADER = "Please review and confirm your address";
	public static final String LAZERSHARK_SHIP_MODAL_BODY = "Please review and confirm your address is correct below. Please note that PO Boxes aren’t eligible. This text is being authored on dmo-author.";
	public static final String LAZERSHARK_SHIP_CONFIRM_BUTTON = "Confirm";
	public static final String LAZERSHARK_SHIP_MODAL_BACK_BUTTON = "Back";
	public static final String LAZERSHARK_PH_ERRORHEAD = "Existing T-Mobile Customer Phone Number Is Not Eligible For This Offer";
	public static final String LAZERSHARK_PH_ERRORBODY = "This offer is not applied for current T-Mobile customers. We appreciate your interest and thank you for being a valued T-Mobile customer.";
	public static final String LAZERSHARK_EMAIL_ERRORHEAD = "Existing T-Mobile Customer Email Is Not Eligible For This Offer";
	public static final String LAZERSHARK_EMAIL_ERRORBODY = "This offer is not applied for current T-Mobile customers. We appreciate your interest and thank you for being a valued T-Mobile customer.";
	public static final String LAZERSHARK_INACTIVEEMAIL_ERRORHEAD = "Inactive Customer Email Is Not Eligible For This Offer";
	public static final String LAZERSHARK_INACTIVEEMAIL_ERRORBODY = "This offer is not applied for inactive customer emails. We appreciate your interest and please apply with your new Email ID.";
	public static final String LAZERSHARK_SHIP_ERRORHEAD = "Existing T-Mobile Customer Address Is Not Eligible For This Offer";
	public static final String LAZERSHARK_SHIP_ERRORBODY = "This offer is not applied for current T-Mobile customer address. We appreciate your interest and thank you for being a valued T-Mobile customer.";
	public static final String LAZERSHARK_EXISTSHIP_ERRORHEAD = "This Offer is already concluded";
	public static final String LAZERSHARK_EXISTSHIP_ERRORBODY = "Already Ordered! We see that you’ve already received T-Mobile’s trial device. We hope you enjoyed your experience. Feel free to give the device to a friend so they can try T-Mobile’s network.";
	public static final String LAZERSHARK_SHIPPO_ERRORHEAD = "This Shipping PO is already concluded";
	public static final String LAZERSHARK_SHIPPO_ERRORBODY = "Already PO Generated! We see that you’ve already received T-Mobile’s trial device. We hope you enjoyed your experience. Feel free to give the device to a friend so they can try T-Mobile’s network.";
	public static final String LAZERSHARK_ZIP_ERRORHEAD = "Please Enter Correct Zip Code";
	public static final String LAZERSHARK_ZIP_ERRORBODY = "This Zip Code is Invalid";
	public static final String LAZERSHARK_GENERIC_ERRORHEAD = "Generic Error";
	public static final String LAZERSHARK_GENERIC_ERRORBODY = "Generic Error";
	public static final String LAZERSHARK_CLOSE_BUTTON_ERROR = "Close";
	
	//LazerShark Page
    public static final String LAZERSHARK_FORM1_HEADER = "Let’s get started.";
    public static final String LAZERSHARK_FORM1_SUBHEADER = "Current experience";
    public static final String LAZERSHARK_FORM2_HEADER = "Tell us a bit about yourself.";
    public static final String LAZERSHARK_FORM2_SUBHEADER = "Your Info";
    public static final String LAZERSHARK_FORM2_FIRSTNAME = "TMNG";
    public static final String LAZERSHARK_FORM2_INVALID_LASTNAME = "T";
    public static final String LAZERSHARK_FORM2_LASTNAME = "TMNG";
    public static final String LAZERSHARK_FORM2_INVALID_EMAIL = "TMNG";
    public static final String LAZERSHARK_FORM2_EMAIL = "tmng@yahoo.com";
    public static final String LAZERSHARK_FORM2_INVALID_CEMAIL1 = "TMNG";
    public static final String LAZERSHARK_FORM2_INVALID_CEMAIL2 = "test1@yahoo.com";
    public static final String LAZERSHARK_FORM2_CEMAIL = "TMNG@Yahoo.com";
    public static final String LAZERSHARK_FORM2_EMAILMODAL_ERROR = "sample@yahoo.com";
    public static final String LAZERSHARK_FORM2_INVALID_PHNO = "222222";
    public static final String LAZERSHARK_FORM2_ExistPHNO = "6185417934";
    public static final String LAZERSHARK_FORM2_PHNO = "2222222222";
    public static final String LAZERSHARK_FORM2_EUSER_HEADER = "Already have a device? We’ll send you a T-Mobile SIM card.";
    public static final String LAZERSHARK_FORM2_INVALID_IMEINO = "22222";
    public static final String LAZERSHARK_FORM2_IMEINO = "997381467146246";
    public static final String LAZERSHARK_FORM2_LegalTC = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla vitae lorem vitae Terms and conditions";
    public static final String LAZERSHARK_FORM3_HEADER = "Where can we ship your device?";
    public static final String LAZERSHARK_FORM3_SUBHEADER = "Shipping address";
    public static final String LAZERSHARK_FORM3_ADDRESS1 = "3625 132nd Ave SE";
    public static final String LAZERSHARK_FORM3_ADDRESS2 = "APT # ";
    public static final String LAZERSHARK_FORM3_CITY = "Bellevue";
    public static final String LAZERSHARK_FORM3_INVALID_ZIP = "23";
    public static final String LAZERSHARK_FORM3_INVALID_ZIP1 = "12345";
    public static final String LAZERSHARK_FORM3_ZIP = "98006";
    public static final String LAZERSHARK_SOFTCONFIRM_HEADER = "You're almost done.";
    public static final String LAZERSHARK_SOFTCONFIRM_SUBHEADER = "Confirmation";
    public static final String LAZERSHARK_SOFTCONFIRM_VERIFYTITLE = "Verification link sent!";	
    
    //MI Page
    public static final String MI_REGISTER_FIRSTNAME = "first16";
    public static final String MI_REGISTER_LASTNAME = "last16";
    public static final String MI_REGISTER_ADDRESS1 = "1336 139TH AVE NE";
    public static final String MI_REGISTER_ADDRESS2 = "APT # ";
    public static final String MI_REGISTER_CITY = "BELLEVUE";
    public static final String MI_REGISTER_ZIP = "98005";
    public static final String MI_REGISTER_PHNO = "4255057590";
    public static final String MI_REGISTER_EMAIL = "arun4@yahoo.com";
    public static final String MI_REGISTER_SOCIALHANDLE = "@Twitter";
    public static final String MI_REGISTER_FORM2 = "Thanks for applying!";
    
    public static final String TEST_DATA_FILTER_LOCATION_QLAB02 = "http://172.28.49.103:3000/testdata";
    public static final String TEST_DATA_FILTER_LOCATION_PROD = "http://172.28.49.103:3000/testdata";
    
    //Layer3 Page
    public static final String LAYER3_ZIP = "22202";

	//OFD-UNAV-Header Page
	public static final String UNAV_HEADER_WIRELESS = "WIRELESS";
	public static final String UNAV_HEADER_WIRELESS_MOB = "Wireless";
	public static final String UNAV_HEADER_WIRELESS_LINK = "https://www.t-mobile.com/";
	public static final String UNAV_HEADER_BUSINESS = "BUSINESS";
	public static final String UNAV_HEADER_BUSINESS_MOB = "Business";
	public static final String UNAV_HEADER_BUSINESS_LINK = "/business";
	public static final String UNAV_HEADER_PREPAID = "PREPAID";
	public static final String UNAV_HEADER_PREPAID_MOB = "Prepaid";
	public static final String UNAV_HEADER_PREPAID_LINK = "https://prepaid.t-mobile.com/home";
	public static final String UNAV_HEADER_BANKING = "BANKING";
	public static final String UNAV_HEADER_BANKING_MOB = "Banking";
	public static final String UNAV_HEADER_BANKING_LINK = "https://www.t-mobilemoney.com/en/home.html";
	public static final String UNAV_HEADER_TV = "TV";
	public static final String UNAV_HEADER_TV_LINK = "/tv";

	public static final String UNAV_HEADER_PLANS = "Plans";
	public static final String UNAV_HEADER_PLANS_LINK = "https://www.t-mobile.com/cell-phone-plans";
	public static final String UNAV_HEADER_MAGENTA = "Magenta®";
	public static final String UNAV_HEADER_MAGENTA_LINK = "/cell-phone-plans/magenta";
	public static final String UNAV_HEADER_MAGENTA_PLUS = "Magenta® Plus";
	public static final String UNAV_HEADER_MAGENTA_PLUS_LINK = "/cell-phone-plans/magenta-plus";
	public static final String UNAV_HEADER_ESSENT = "Essentials";
	public static final String UNAV_HEADER_ESSENT_LINK = "/cell-phone-plans/essentials";
	public static final String UNAV_HEADER_ULTI55 = "Unlimited 55";
	public static final String UNAV_HEADER_ULTI55_LINK = "/cell-phone-plans/unlimited-55-senior-discount-plans";
	public static final String UNAV_HEADER_MILITARY = "Military";
	public static final String UNAV_HEADER_MILITARY_LINK = "/cell-phone-plans/military-discount-plans";
	
	public static final String UNAV_HEADER_PHONES = "Phones";
	public static final String UNAV_HEADER_PHONES_LINK = "/cell-phones";
	public static final String UNAV_HEADER_CELLPHONES = "Cell phones";
	public static final String UNAV_HEADER_CELLPHONES_LINK = "/cell-phones";
	public static final String UNAV_HEADER_TABLETS = "Tablets";
	public static final String UNAV_HEADER_TABLETS_LINK = "/tablets";
	public static final String UNAV_HEADER_SMARTWATCHES = "Smartwatches";
	public static final String UNAV_HEADER_SMARTWATCHES_LINK = "/smart-watches";
	public static final String UNAV_HEADER_ACCESSORIES = "Accessories";
	public static final String UNAV_HEADER_ACCESSORIES_LINK = "/accessories";
	public static final String UNAV_HEADER_BRING_YOUR_OWNDEVICE = "Bring your own device";
	public static final String UNAV_HEADER_BRING_YOUR_OWNDEVICE_LINK = "/resources/bring-your-own-phone";
	public static final String UNAV_HEADER_DEALS = "Deals";
	public static final String UNAV_HEADER_DEALS_LINK = "/offers/deals-hub";
	public static final String UNAV_HEADER_APPLE = "Apple";
	public static final String UNAV_HEADER_APPLE_LINK = "/offers/apple-iphone-deals";
	public static final String UNAV_HEADER_SAMSUNG = "Samsung";
	public static final String UNAV_HEADER_SAMSUNG_LINK = "/offers/samsung-phone-deals";
	public static final String UNAV_HEADER_GOOGLE = "Google";
	public static final String UNAV_HEADER_GOOGLE_LINK = "/offers/google-phone-deals";
	public static final String UNAV_HEADER_LG = "LG";
	public static final String UNAV_HEADER_LG_LINK = "/offers/deals-hub";
	public static final String UNAV_HEADER_COVERAGE = "Coverage";
	public static final String UNAV_HEADER_COVERAGE_LINK = "/coverage/4g-lte-5g-networks";
	public static final String UNAV_HEADER_BENEFITS = "Benefits & more";
	public static final String UNAV_HEADER_BENEFITS_LINK = "/brand/benefits";
	public static final String UNAV_HEADER_BENEFITS1 = "Benefits";
	public static final String UNAV_HEADER_BENEFITS1_LINK = "/brand/benefits";
	public static final String UNAV_HEADER_5G = "5G vision";
	public static final String UNAV_HEADER_5G_LINK = "/5g";
	public static final String UNAV_HEADER_WHYTMOBILE = "Why T-Mobile";
	public static final String UNAV_HEADER_WHYTMOBILE_LINK = "/brand/why-t-mobile";
	public static final String UNAV_HEADER_TRAVEL = "Travel";
	public static final String UNAV_HEADER_TRAVEL_LINK = "/coverage/international-calling";

	//About Us Header
	public static final String UNAV_HEADER_OURSTORY = "Our story";
	public static final String UNAV_HEADER_OURSTORY_LINK = "/our-story";
	public static final String UNAV_HEADER_UNCARRIER = "Un-carrier history";
	public static final String UNAV_HEADER_UNCARRIER_LINK = "/our-story/un-carrier-history";
	public static final String UNAV_HEADER_NETWORK = "Network & innovation";
	public static final String UNAV_HEADER_NETWORK_LINK = "/our-story/network-and-innovation";
	public static final String UNAV_HEADER_AWARDS = "Awards";
	public static final String UNAV_HEADER_AWARDS_LINK = "/our-story/awards";
	public static final String UNAV_HEADER_LEADERSHIP = "Leadership";
	public static final String UNAV_HEADER_LEADERSHIP_LINK = "https://investor.t-mobile.com/corporate-governance/management-and-board-of-directors/";
	public static final String UNAV_HEADER_WORKINGTOGETHER = "Working together";
	public static final String UNAV_HEADER_WORKINGTOGETHER_LINK = "/our-story/working-together";
	
	public static final String UNAV_HEADER_RESPONSIBILITY = "Responsibility";
	public static final String UNAV_HEADER_RESPONSIBILITY_LINK = "/responsibility";
	public static final String UNAV_HEADER_COMMUNITY = "Community";
	public static final String UNAV_HEADER_COMMUNITY_LINK = "/responsibility/community";
	public static final String UNAV_HEADER_SUSTAINABILITY = "Sustainability";
	public static final String UNAV_HEADER_SUSTAINABILITY_LINK = "/responsibility/sustainability";
	public static final String UNAV_HEADER_CONSUMERINFO = "Consumer information";
	public static final String UNAV_HEADER_CONSUMERINFO_LINK = "/responsibility/consumer-info";
	public static final String UNAV_HEADER_PRIVACYCENTER = "Privacy center";
	public static final String UNAV_HEADER_PRIVACYCENTER_LINK = "/responsibility/privacy";
	public static final String UNAV_HEADER_LEGALCENTER = "Legal center";
	public static final String UNAV_HEADER_LEGALCENTER_LINK = "/responsibility/legal";
	
	public static final String UNAV_HEADER_NEWSROOM = "Newsroom";
	public static final String UNAV_HEADER_NEWSROOM_LINK = "/news";
	public static final String UNAV_HEADER_ARCHIVE = "Archive";
	public static final String UNAV_HEADER_ARCHIVE_LINK = "/news/archive";
	public static final String UNAV_HEADER_MEDIALIBRARY = "Media library";
	public static final String UNAV_HEADER_MEDIALIBRARY_LINK = "/news/media-library";
	public static final String UNAV_HEADER_FACTSHEETS = "Fact sheets";
	public static final String UNAV_HEADER_FACTSHEETS_LINK = "/news/fact-sheets";
	public static final String UNAV_HEADER_CONTACTUS = "Contact us";
	public static final String UNAV_HEADER_CONTACTUS_LINK = "/news/contact-us";
	
	public static final String UNAV_HEADER_INVESTORS = "Investors";
	public static final String UNAV_HEADER_INVESTORS_LINK = "https://investor.t-mobile.com/investors/default.aspx";
	public static final String UNAV_HEADER_FINPERF = "Financial performance";
	public static final String UNAV_HEADER_FINPERF_LINK = "https://investor.t-mobile.com/financial-performance/quarterly-results/default.aspx";
	public static final String UNAV_HEADER_NEWSEVENTS = "News & events";
	public static final String UNAV_HEADER_NEWSEVENTS_LINK = "https://investor.t-mobile.com/news-and-events/t-mobile-us-press-releases/default.aspx";
	public static final String UNAV_HEADER_STOCKCHART = "Stock chart";
	public static final String UNAV_HEADER_STOCKCHART_LINK = "https://investor.t-mobile.com/stock-chart/default.aspx";
	public static final String UNAV_HEADER_CORPGOV = "Corporate governance";
	public static final String UNAV_HEADER_CORPGOV_LINK = "https://investor.t-mobile.com/corporate-governance/governance-documents/default.aspx";
	public static final String UNAV_HEADER_RESOURCES = "Resources";
	public static final String UNAV_HEADER_RESOURCES_LINK = "https://investor.t-mobile.com/resources/contact-us/default.aspx";
	
	
	public static final String UNAV_HEADER_CAREERS = "Careers";
	public static final String UNAV_HEADER_CAREERS_LINK = "/careers";
	public static final String UNAV_HEADER_CUROPE = "Current openings";
	public static final String UNAV_HEADER_CUROPE_LINK = "/job-search";
	public static final String UNAV_HEADER_CLOGIN = "Log in";
	public static final String UNAV_HEADER_CLOGIN_LINK = "/careers/login";
	public static final String UNAV_HEADER_MEETOURTEAM = "Meet our teams";
	public static final String UNAV_HEADER_MEETOURTEAM_LINK = "/careers/meet-our-teams";
	public static final String UNAV_HEADER_CULTUREBEN = "Culture & benefits";
	public static final String UNAV_HEADER_CULTUREBEN_LINK = "/careers/culture-and-benefits";
	public static final String UNAV_HEADER_FORAPPLI = "For applicants";
	public static final String UNAV_HEADER_FORAPPLI_LINK = "/careers/for-applicants";	
	
	//Header-Utilities
	public static final String UNAV_HEADER_FIND_A_STORE = "Find a store";
	public static final String UNAV_HEADER_FIND_A_STORE_LINK = "/store-locator";
	public static final String UNAV_HEADER_LETS_TALK = "Let's talk";
	public static final String UNAV_HEADER_CART = "Cart";
	public static final String UNAV_HEADER_CART_LINK = "/cart";
	public static final String UNAV_HEADER_SEARCH = "Search";
	public static final String UNAV_HEADER_MYACCOUNT = "My Account";
	public static final String UNAV_HEADER_MYACCOUNT_WIRELESS = "My account";
	public static final String UNAV_HEADER_MYACCOUNT_LINK = "https://account.t-mobile.com/signin ";
	public static final String UNAV_HEADER_LOGIN = "Log In";
	public static final String UNAV_HEADER_LOGIN_WIRELESS = "Log in";
	public static final String UNAV_HEADER_LOGIN_LINK = "https://account.t-mobile.com/signin";
	public static final String UNAV_HEADER_BACKTOMYACCOUNT = "Back to My Account";
	public static final String UNAV_HEADER_BACKTOMYACCOUNT_LINK = "/home";
	public static final String UNAV_HEADER_QUICK_ACTIONS = "Quick Actions";
	public static final String UNAV_HEADER_ABOUUS_QUICK_ACTIONS = "Quick actions";
	public static final String UNAV_HEADER_BILL_PAY = "Bill pay";
	public static final String UNAV_HEADER_BILL_PAY_LINK = "/onetimepayment";
	public static final String UNAV_HEADER_ADD_DEVICE = "Add a device";
	public static final String UNAV_HEADER_ADD_DEVICE_LINK = "https://account.t-mobile.com/signin";
	public static final String UNAV_HEADER_UPGRADE = "Upgrade";
	public static final String UNAV_HEADER_UPGRADE_LINK = "https://account.t-mobile.com/signin";
	public static final String UNAV_HEADER_CHECK_ORDER = "Check order status";
	public static final String UNAV_HEADER_CHECK_ORDER_LINK = "/myphone/checkorder.html";
	public static final String UNAV_HEADER_SUPPORT = "Support";
	public static final String UNAV_HEADER_SUPPORT_LINK = "https://support.t-mobile.com/welcome";
	public static final String UNAV_HEADER_ABOUTUS_SUPPORT_LINK = "https://support.t-mobile.com/welcome.html";
	
    public static final String UNAV_FOOTER_tmobile_title = "Connect With T-Mobile";
    public static final String UNAV_FOOTER_tmobile_wireless_title = "Connect With T-Mobile";
    public static final String UNAV_FOOTER_Instagram_LINK = "https://www.instagram.com/tmobile/";
    public static final String UNAV_FOOTER_Facebook_LINK = "https://www.facebook.com/TMobile";
    public static final String UNAV_FOOTER_Facebook_MOBILE_LINK = "https://m.facebook.com/TMobile";
    public static final String UNAV_FOOTER_Twitter_LINK = "https://twitter.com/TMobile";
    public static final String UNAV_FOOTER_Twitter_MOBILE_LINK = "https://mobile.twitter.com/TMobile";
    public static final String UNAV_FOOTER_Youtube_LINK = "https://www.youtube.com/user/TMobile/custom";
    public static final String UNAV_FOOTER_Youtube_MOBILE_LINK = "https://m.youtube.com/user/TMobile/custom";
    public static final String UNAV_FOOTER_language_0 = "English";
    public static final String UNAV_FOOTER_language_0_LINK = "https://www.t-mobile.com";
    public static final String UNAV_FOOTER_language_1 = "Español";
    public static final String UNAV_FOOTER_language_1_LINK = "https://es.";
    public static final String UNAV_FOOTER_language_1_WIRELESS_LINK = "https://es.";
    //Wireless Footer-1
    public static final String UNAV_FOOTER_footerLabel_0 = "Phones & devices";
    public static final String UNAV_FOOTER_subFooterLink_0_0 = "Phones";
    public static final String UNAV_FOOTER_subFooterLink_0_0_LINK = "/cell-phones";
    public static final String UNAV_FOOTER_subFooterLink_0_1 = "Tablets";
    public static final String UNAV_FOOTER_subFooterLink_0_1_LINK = "/tablets";
    public static final String UNAV_FOOTER_subFooterLink_0_2 = "Smartwatches";
    public static final String UNAV_FOOTER_subFooterLink_0_2_LINK = "/smart-watches";
    public static final String UNAV_FOOTER_subFooterLink_0_3 = "Accessories";
    public static final String UNAV_FOOTER_subFooterLink_0_3_LINK = "/accessories";
    public static final String UNAV_FOOTER_footerLabel_1 = "Apps & connected devices";
    public static final String UNAV_FOOTER_subFooterLink_1_0 = "FamilyMode";
    public static final String UNAV_FOOTER_subFooterLink_1_0_LINK = "/offers/t-mobile-family-mode";
    public static final String UNAV_FOOTER_subFooterLink_1_1 = "DIGITS";
    public static final String UNAV_FOOTER_subFooterLink_1_1_LINK = "/offers/t-mobile-digits";
    public static final String UNAV_FOOTER_subFooterLink_1_2 = "SyncUP DRIVE";
    public static final String UNAV_FOOTER_subFooterLink_1_2_LINK = "/offers/syncup";

    public static final String UNAV_FOOTER_footerLabel_2 = "Plans & information";
    public static final String UNAV_FOOTER_subFooterLink_2_0 = "Plans home";
    public static final String UNAV_FOOTER_subFooterLink_2_0_LINK = "/cell-phone-plans";
    public static final String UNAV_FOOTER_subFooterLink_2_1 = "55+";
    public static final String UNAV_FOOTER_subFooterLink_2_1_LINK = "/cell-phone-plans/unlimited-55-senior-discount-plans";
    public static final String UNAV_FOOTER_subFooterLink_2_2 = "Military";
    public static final String UNAV_FOOTER_subFooterLink_2_2_LINK = "/cell-phone-plans/military-discount-plans";
    public static final String UNAV_FOOTER_subFooterLink_2_3 = "Prepaid";
    public static final String UNAV_FOOTER_subFooterLink_2_3_LINK = "https://prepaid.t-mobile.com/home";
    public static final String UNAV_FOOTER_subFooterLink_2_4 = "Insurance";
    public static final String UNAV_FOOTER_subFooterLink_2_4_LINK = "/devices/phone-protection-plans";
    public static final String UNAV_FOOTER_subFooterLink_2_5 = "Data pass";
    public static final String UNAV_FOOTER_subFooterLink_2_5_LINK = "https://support.t-mobile.com/docs/DOC-7261";
    public static final String UNAV_FOOTER_subFooterLink_2_6 = "Mobile Internet";
    public static final String UNAV_FOOTER_subFooterLink_2_6_LINK = "/responsibility/consumer-info/policies/internet-service";

    public static final String UNAV_FOOTER_footerLabel_3 = "Switch to T-Mobile";
    public static final String UNAV_FOOTER_subFooterLink_3_0 = "We’ll help you join";
    public static final String UNAV_FOOTER_subFooterLink_3_0_LINK = "/resources/how-to-join-us";
    //public static final String UNAV_FOOTER_subFooterLink_3_1 = "Switcher Tool";
    public static final String UNAV_FOOTER_subFooterLink_3_1 = "Savings calculator";
    public static final String UNAV_FOOTER_subFooterLink_3_1_LINK = "/switch-to-t-mobile";
    public static final String UNAV_FOOTER_subFooterLink_3_2 = "Bring your own device";
    public static final String UNAV_FOOTER_subFooterLink_3_2_LINK = "/resources/bring-your-own-phone";
    public static final String UNAV_FOOTER_subFooterLink_3_3 = "Trade-in program";
    public static final String UNAV_FOOTER_subFooterLink_3_3_LINK = "/resources/phone-trade-in";
    public static final String UNAV_FOOTER_subFooterLink_3_4 = "Number porting";
    public static final String UNAV_FOOTER_subFooterLink_3_4_LINK = "/resources/keep-your-number";

    public static final String UNAV_FOOTER_footerLabel_4 = "T-Mobile benefits";
    public static final String UNAV_FOOTER_subFooterLink_4_0 = "Magenta Plan benefits";
    public static final String UNAV_FOOTER_subFooterLink_4_0_LINK = "/brand/benefits";
    public static final String UNAV_FOOTER_subFooterLink_4_1 = "Travel";
    public static final String UNAV_FOOTER_subFooterLink_4_1_LINK = "/travel-abroad-with-simple-global";

    public static final String UNAV_FOOTER_footerLabel_5 = "Order info";
    public static final String UNAV_FOOTER_subFooterLink_5_0 = "Check order status";
    public static final String UNAV_FOOTER_subFooterLink_5_0_LINK = "https://secure-checkout.t-mobile.com/webapp/wcs/stores/servlet/TMCcoOrderStatusLookupForm?langId=-1&storeId=10551";
    public static final String UNAV_FOOTER_subFooterLink_5_1 = "View return policy";
    public static final String UNAV_FOOTER_subFooterLink_5_1_LINK = "/responsibility/legal/return-policy";
    public static final String UNAV_FOOTER_subFooterLink_5_2 = "Redeem a rebate";
    public static final String UNAV_FOOTER_subFooterLink_5_2_LINK = "https://promotions.t-mobile.com";

    public static final String UNAV_FOOTER_footerLabel_6 = "Support";
    public static final String UNAV_FOOTER_subFooterLink_6_0 = "Contact us";
    public static final String UNAV_FOOTER_subFooterLink_6_0_LINK = "/contact-us";
    public static final String UNAV_FOOTER_subFooterLink_6_1 = "Phones";
    public static final String UNAV_FOOTER_subFooterLink_6_1_LINK = "https://support.t-mobile.com/community/phones-tablets-devices";
    public static final String UNAV_FOOTER_subFooterLink_6_2 = "Plans";
    public static final String UNAV_FOOTER_subFooterLink_6_2_LINK = "https://support.t-mobile.com/community/plans-features";
    public static final String UNAV_FOOTER_subFooterLink_6_3 = "Billing";
    public static final String UNAV_FOOTER_subFooterLink_6_3_LINK = "https://support.t-mobile.com/docs/DOC-2504";
    public static final String UNAV_FOOTER_subFooterLink_6_4 = "International";
    public static final String UNAV_FOOTER_subFooterLink_6_4_LINK = "https://support.t-mobile.com/docs/DOC-2082";

    public static final String UNAV_FOOTER_footerLabel_7 = "My account";
    public static final String UNAV_FOOTER_subFooterLink_7_0 = "Pay my bill";
    public static final String UNAV_FOOTER_subFooterLink_7_0_LINK = "/onetimepayment";
    public static final String UNAV_FOOTER_subFooterLink_7_1 = "Upgrade";
    public static final String UNAV_FOOTER_subFooterLink_7_1_LINK = "https://support.t-mobile.com/docs/DOC-1674";
    public static final String UNAV_FOOTER_subFooterLink_7_2 = "Add a line";
    public static final String UNAV_FOOTER_subFooterLink_7_2_LINK = "https://support.t-mobile.com/docs/DOC-4000";

    public static final String UNAV_FOOTER_footerLabel_8 = "More than wireless";
    public static final String UNAV_FOOTER_subFooterLink_8_0 = "Business";
    public static final String UNAV_FOOTER_subFooterLink_8_0_LINK = "/business";
    public static final String UNAV_FOOTER_subFooterLink_8_1 = "Prepaid";
    public static final String UNAV_FOOTER_subFooterLink_8_1_LINK = "https://prepaid.t-mobile.com/home";
    public static final String UNAV_FOOTER_subFooterLink_8_2 = "TVision";
    public static final String UNAV_FOOTER_subFooterLink_8_2_LINK = "/tv";
    public static final String UNAV_FOOTER_subFooterLink_8_3 = "T-Mobile Money";
    public static final String UNAV_FOOTER_subFooterLink_8_3_LINK = "https://www.t-mobilemoney.com/en/home.html";
    public static final String UNAV_FOOTER_subFooterLink_8_4 = "Home Internet";
    public static final String UNAV_FOOTER_subFooterLink_8_4_LINK = "/ISP";
    public static final String UNAV_FOOTER_subFooterLink_8_5 = "IoT";
    public static final String UNAV_FOOTER_subFooterLink_8_5_LINK = "/internet-of-things";

    public static final String UNAV_FOOTER_footerLabel_9 = "About T-Mobile";
    public static final String UNAV_FOOTER_subFooterLink_9_0 = "Our story";
    public static final String UNAV_FOOTER_subFooterLink_9_0_LINK = "/our-story";
    public static final String UNAV_FOOTER_subFooterLink_9_1 = "Newsroom";
    public static final String UNAV_FOOTER_subFooterLink_9_1_LINK = "/news";
    public static final String UNAV_FOOTER_subFooterLink_9_2 = "Investor relations";
    public static final String UNAV_FOOTER_subFooterLink_9_2_LINK = "https://investor.t-mobile.com/investors/default.aspx";

    public static final String UNAV_FOOTER_footerLabel_10 = "Corporate responsibility";
    public static final String UNAV_FOOTER_subFooterLink_10_0 = "Community";
    public static final String UNAV_FOOTER_subFooterLink_10_0_LINK = "/responsibility/community";
    public static final String UNAV_FOOTER_subFooterLink_10_1 = "Sustainability";
    public static final String UNAV_FOOTER_subFooterLink_10_1_LINK = "/responsibility/sustainability";
    public static final String UNAV_FOOTER_subFooterLink_10_2 = "Privacy Center";
    public static final String UNAV_FOOTER_subFooterLink_10_2_LINK = "/responsibility/privacy";

    public static final String UNAV_FOOTER_footerLabel_11 = "Careers";
    public static final String UNAV_FOOTER_subFooterLink_11_0 = "T-Mobile careers";
    public static final String UNAV_FOOTER_subFooterLink_11_0_LINK = "/careers";
    
    //Aboutus-Footer 1
    public static final String UNAV_FOOTER_CONTACTUS_LABEL = "Contact us";
    public static final String UNAV_FOOTER_CONTACTINFO = "Contact information";
    public static final String UNAV_FOOTER_CONTACTINFO_LINK = "/contact-us";
    public static final String UNAV_FOOTER_CHECKORDERSTAT = "Check order status";
    public static final String UNAV_FOOTER_CHECKORDERSTAT_LINK = "https://secure-checkout.t-mobile.com/webapp/wcs/stores/servlet/TMCcoOrderStatusLookupForm?langId=-1&storeId=10551";
    public static final String UNAV_FOOTER_VIEWRETURN = "View return policy";
    public static final String UNAV_FOOTER_VIEWRETURN_LINK = "/responsibility/legal/return-policy";
    public static final String UNAV_FOOTER_GETAREBATE = "Get a rebate";
    public static final String UNAV_FOOTER_GETAREBATE_LINK = "https://promotions.t-mobile.com/OfferSearch";
    public static final String UNAV_FOOTER_FINDASTORE = "Find a store";
    public static final String UNAV_FOOTER_FINDASTORE_LINK = "/store-locator";
    public static final String UNAV_FOOTER_TRADEINPRG = "Trade-in program";
    public static final String UNAV_FOOTER_TRADEINPRG_LINK = "/resources/phone-trade-in";

    public static final String UNAV_FOOTER_SUPPORT_LABEL = "Support";
    public static final String UNAV_FOOTER_DEVICESUPPORT = "Device support";
    public static final String UNAV_FOOTER_DEVICESUPPORT_LINK = "https://support.t-mobile.com/community/phones-tablets-devices";
    public static final String UNAV_FOOTER_QUES = "Questions about your bill";
    public static final String UNAV_FOOTER_QUES_LINK = "https://support.t-mobile.com/community/account";
    public static final String UNAV_FOOTER_PLANSSERVICE = "Plans & services";
    public static final String UNAV_FOOTER_PLANSSERVICE_LINK = "https://support.t-mobile.com/community/plans-features";
    public static final String UNAV_FOOTER_ACTIVE = "Activate your prepaid phone or device";
    public static final String UNAV_FOOTER_ACTIVE_LINK = "https://prepaid.t-mobile.com/home";
    public static final String UNAV_FOOTER_REFILL = "Refill your prepaid account";
    public static final String UNAV_FOOTER_REFILL_LINK = "https://prepaid.t-mobile.com/direct-to-account";
    public static final String UNAV_FOOTER_INTERRATES = "International rates";
    public static final String UNAV_FOOTER_INTERRATES_LINK = "/travel-abroad-with-simple-global";
    
    public static final String UNAV_FOOTER_BUSINESS_LABEL = "T-Mobile for Business";
    public static final String UNAV_FOOTER_BUSINESSPLAN = "Business plans";
    public static final String UNAV_FOOTER_BUSINESSPLAN_LINK = "/business/wireless-business-plans";
    public static final String UNAV_FOOTER_INTERNET = "Internet of Things";
    public static final String UNAV_FOOTER_INTERNET_LINK = "https://iot.t-mobile.com/";
    
    //Footer-2
    public static final String UNAV_FOOTER2_top_0 = "ABOUT";
    public static final String UNAV_FOOTER2_top_0_LINK = "/about-us";
    public static final String UNAV_FOOTER2_top_1 = "INVESTOR RELATIONS";
    public static final String UNAV_FOOTER2_top_1_LINK = "https://investor.t-mobile.com/investors/default.aspx";
    public static final String UNAV_FOOTER2_top_2 = "PRESS";
    public static final String UNAV_FOOTER2_top_2_LINK = "/news";
    public static final String UNAV_FOOTER2_top_2_WIRELESS_LINK = "https://investor.t-mobile.com/news-and-events/t-mobile-us-press-releases/default.aspx";
    public static final String UNAV_FOOTER2_top_3 = "CAREERS";
    public static final String UNAV_FOOTER2_top_3_LINK = "/careers";
    public static final String UNAV_FOOTER2_top_4 = "DEUTSCHE TELEKOM";
    public static final String UNAV_FOOTER2_top_4_LINK = "https://www.telekom.com/en";
    public static final String UNAV_FOOTER2_top_5 = "PUERTO RICO";
    public static final String UNAV_FOOTER2_top_5_LINK = "https://www.t-mobilepr.com/";
    public static final String UNAV_FOOTER2_bottom_0 = "PRIVACY POLICY";
    public static final String UNAV_FOOTER2_bottom_0_LINK = "/responsibility/privacy/privacy-policy";
    public static final String UNAV_FOOTER2_bottom_1 = "INTEREST-BASED ADS";
    public static final String UNAV_FOOTER2_bottom_1_LINK = "/responsibility/privacy/privacy-choice/ad-options";
    public static final String UNAV_FOOTER2_bottom_2 = "PRIVACY CENTER";
    public static final String UNAV_FOOTER2_bottom_2_LINK = "/responsibility/privacy";
    public static final String UNAV_FOOTER2_bottom_3 = "CONSUMER INFORMATION";
    public static final String UNAV_FOOTER2_bottom_3_LINK = "/responsibility/consumer-info";
    public static final String UNAV_FOOTER2_bottom_4 = "PUBLIC SAFETY/911";
    public static final String UNAV_FOOTER2_bottom_4_LINK = "/responsibility/consumer-info/safety/9-1-1";
    public static final String UNAV_FOOTER2_bottom_5 = "TERMS & CONDITIONS";
    public static final String UNAV_FOOTER2_bottom_5_LINK = "/responsibility/legal/terms-and-conditions";
    public static final String UNAV_FOOTER2_bottom_6 = "TERMS OF USE";
    public static final String UNAV_FOOTER2_bottom_6_LINK = "/responsibility/consumer-info/policies/terms-of-use";
    public static final String UNAV_FOOTER2_bottom_7 = "ACCESSIBILITY";
    public static final String UNAV_FOOTER2_bottom_7_LINK = "/responsibility/consumer-info/accessibility-policy";
    public static final String UNAV_FOOTER2_bottom_8 = "OPEN INTERNET";
    public static final String UNAV_FOOTER2_bottom_8_LINK = "/responsibility/consumer-info/policies/internet-service";
    public static final String UNAV_FOOTER2_bottom_COPYRIGHT = "Copyright ©2002-2019 T-Mobile USA, INC";
    public static final String UNAV_FOOTER2_bottom_9 = "Do Not Sell My Personal Information";
    public static final String UNAV_FOOTER2_bottom_9_LINK = "/dns";
    public static final String Payments_Path=System.getProperty("user.dir")+"/src/test/resources/testdata/payments.json";
    
    //OFD UNAV MYTMO Page
    public static final String UNAV_MYTMO_HEADER_BILL = "Bill";
    public static final String UNAV_MYTMO_HEADER_USAGE = "Usage";
    public static final String UNAV_MYTMO_HEADER_ACCOUNT = "Account";
    public static final String UNAV_MYTMO_HEADER_PHONE = "My Phone";
    public static final String UNAV_MYTMO_HEADER_SHOP = "Shop";
    public static final String UNAV_MYTMO_HEADER_MANAGELINES = "Manage my lines";
    public static final String UNAV_MYTMO_HEADER_EDITPROFILE = "Edit my Profile";
    public static final String UNAV_MYTMO_HEADER_REPORTLOST = "Report a lost or stolen device";
    public static final String UNAV_MYTMO_HEADER_ADDDEVICE = "Add a person or device to my account";
    public static final String UNAV_MYTMO_HEADER_SEELATESTDEALS = "See the latest deals";
    public static final String UNAV_MYTMO_HEADER_CONTACTUS = "Contact us";
    public static final String UNAV_MYTMO_HEADER_CONTACTUS_LINK = "/contact-us";
    public static final String UNAV_MYTMO_HEADER_PROFILE = "Profile";
    public static final String UNAV_MYTMO_HEADER_PROFILE_LINK = "/profile";
    public static final String UNAV_MYTMO_HEADER_ACCOUNTHISTORY = "Account history";
    public static final String UNAV_MYTMO_HEADER_ACCOUNTHISTORY_LINK = "/accounthistory";
    public static final String UNAV_MYTMO_HEADER_LOGOUT = "Logout";
    public static final String UNAV_MYTMO_HEADER_SIGNIN_LINK = "/signin";
    public static final String UNAV_MYTMO_FOOTER_STORELOCATOR = "Store Locator";
    public static final String UNAV_MYTMO_FOOTER_COVERAGE_LINK = "/coverage-map";
    public static final String UNAV_MYTMO_FOOTER_TMOBILE = "T-Mobile.com";
    
  //LazerShark Page
    public static final String LAZERSHARK_FORM1_FIRSTNAME = "TMNG";
    public static final String LAZERSHARK_FORM1_INVALID_LASTNAME = "T";
    public static final String LAZERSHARK_FORM1_LASTNAME = "TMNG";
    public static final String LAZERSHARK_FORM1_INVALID_EMAIL = "TMNG";
    public static final String LAZERSHARK_FORM1_EMAIL = "tmng@yahoo.com";
    public static final String LAZERSHARK_FORM1_INVALID_CEMAIL1 = "TMNG";
    public static final String LAZERSHARK_FORM1_INVALID_CEMAIL2 = "test1@yahoo.com";
    public static final String LAZERSHARK_FORM1_CEMAIL = "TMNG@Yahoo.com";
    public static final String LAZERSHARK_FORM1_EMAILMODAL_ERROR = "sample@yahoo.com";
    public static final String LAZERSHARK_FORM1_INVALID_PHNO = "222222";
    public static final String LAZERSHARK_FORM1_ExistPHNO = "6185417934";
    public static final String LAZERSHARK_FORM1_PHNO = "2222222222";
    public static final String LAZERSHARK_FORM1_EUSER_HEADER = "Already have a device? We’ll send you a T-Mobile SIM card.";
    public static final String LAZERSHARK_FORM1_INVALID_IMEINO = "22222";
    public static final String LAZERSHARK_FORM1_IMEINO = "997381467146246";
    public static final String LAZERSHARK_FORM1_LegalTC = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla vitae lorem vitae Terms and conditions";
    public static final String LAZERSHARK_FORM2_ADDRESS1 = "3625 132nd Ave SE";
    public static final String LAZERSHARK_FORM2_ADDRESS2 = "APT # ";
    public static final String LAZERSHARK_FORM2_CITY = "Bellevue";
    public static final String LAZERSHARK_FORM2_INVALID_ZIP = "23";
    public static final String LAZERSHARK_FORM2_INVALID_ZIP1 = "12345";
    public static final String LAZERSHARK_FORM2_ZIP = "98006";
    
    
    private Constants() {
		
	}

}