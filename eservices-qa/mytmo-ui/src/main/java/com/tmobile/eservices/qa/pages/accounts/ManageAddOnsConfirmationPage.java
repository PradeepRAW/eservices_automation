package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class ManageAddOnsConfirmationPage extends CommonPage {

	@FindBy(css = "span[u1st-aria-label='Thanks for signing up for Netflix']")
	private WebElement thanksForSignUpMessage;

	@FindBy(linkText = "Activate Netflix")
	private WebElement activateNetflixBtn;

	@FindBy(css = "h4[ng-bind*='DP_C_OrderCompleteText']")
	private WebElement dataPassesConfirmation;

	@FindBy(css = "button[ng-click*='btnContinue_click']")
	private WebElement returnToPlanBtn;

	@FindBy(css = "div[ng-bind*='PageContent.DP_C_AmtChargeText']")
	private WebElement taxesAndFeesText;

	@FindBy(css = "div[style*='hidden'] a")
	private WebElement hiddenBackBtn;

	@FindBy(linkText = "View Details")
	private WebElement viewDetails;

	@FindBy(css = "h4[ng-if*='immediate']")
	private WebElement startsImmediatelyText;

	@FindBy(css = "span[ng-bind-html*='DP_C_LegalCopy']")
	private WebElement dPCLegalCopyText;

	@FindBy(css = "span[ng-bind-html*='PageContent.DP_C_LegalCopy']")
	private WebElement legalCopy;

	@FindBy(xpath = "//div[contains(text(), 'A receipt will be emailed to')]")
	private WebElement emailReceipt;

	@FindBy(css = "div[ng-bind*='DP_C_PaymentText']")
	private WebElement paymentText;

	@FindBy(css = "div[ng-bind*='DP_C_AuthorizationText']")
	private WebElement authorizationText;

	@FindBy(css = "div[ng-bind*='DP_C_TransactionTotalText']")
	private WebElement transactionTotalText;

	@FindBy(css = "div[ng-bind*='DP_C_PaymentTypeText']")
	private WebElement paymentTypeText;

	@FindBy(css = "div[ng-bind*='DP_C_NameOnAccountText']")
	private WebElement nameOnAccountText;

	@FindBy(css = "div[ng-bind*='DP_C_AccountNumberText']")
	private WebElement accountNumberText;

	@FindBy(css = "div.body.black.p-l-10")
	private WebElement yourAccountEligbileText;

	@FindBy(xpath = "//button[contains(text(),'NEXT STEP - SIGN UP')]")
	private WebElement netFlixSignUpBtn;

	@FindBy(css = "img.playstore-icon.pull-right")
	private WebElement Googleplay;

	@FindBy(css = "img.appstore-icon")
	private WebElement ApplePay;

	@FindBy(css = "button[class*='SecondaryCTA']")
	private WebElement ContactUS;

	@FindBy(css = "button[class*='PrimaryCTA full-btn-width']")
	private List<WebElement> btnOnConfirmationPage;

	@FindBy(css = "div.Display3.text-center")
	private List<WebElement> confirmationPageHeader;

	@FindBy(css = "span.pull-left.black.Display6")
	private WebElement orderDetailsHeader;

	@FindBy(css = "span.pull-right.cursor")
	private WebElement orderDetailsCarat;

	@FindBy(css = "span.arrow-size.arrow-back")
	private WebElement backArrow;

	@FindBy(css = "span.pull-left.H6-heading.pull-left")
	private List<WebElement> newRemovedAddOnsHeader;

	@FindBy(xpath = "//span[text()='New add-ons']//following::span[contains(@class,'Display6')]")
	private List<WebElement> newAddOnsList;

	@FindBy(css = "p.pull-right.H6-heading")
	private WebElement newAddOnsprice;

	@FindBy(css = "span.pull-right.H6-heading")
	private WebElement removedAddOnsprice;

	@FindBy(css = "p.mb-0.H5-heading")
	private WebElement textMessage;

	@FindBy(css = "p.mb-0.H5-heading")
	private WebElement btnGoToPlanBenefits;

	@FindBy(css = "div[class*='Display3'] p")
	private List<WebElement> confirmMessage;

	@FindBy(className = "PrimaryCTA")
	private WebElement nextStepSignUpBtn;

	@FindBy(css = "button[class=\"PrimaryCTA full-btn-width\"]")
	private WebElement returnToHomeCTA;

	@FindBy(xpath = "//span[contains(text(),'One last step...')]")
	private WebElement oneLastStepText;

	@FindBy(xpath = "//span[contains(text(),'To finish activating, just log in to your Netflix ')]")
	private WebElement textToFinishActivating;

	@FindBy(xpath = "//span[contains(text(),'Lorem ipsum dolor')]")
	private WebElement loremIpsumDolorText;

	@FindBy(xpath = "//a[contains(@class,'legal-link black cursor')]")
	private WebElement termsAndConditionsLink;

	@FindBy(xpath = "//button[contains(@class,'SecondaryCTA')]")
	private WebElement backToAccountOverviewCTA;

	@FindBy(xpath = "//button[contains(@class,'PrimaryCTA full-btn-width float-md-left float-lg-left float-xl-left')]")
	private WebElement goToNetflixCTA;


	public ManageAddOnsConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public ManageAddOnsConfirmationPage verifyODFConfirmationPage() {
		waitForDataPassSpinnerInvisibility();
		checkPageIsReady();
		try {
			// Assert.assertTrue(verifyElementBytext(confirmMessage, "Thanks"),"verifying
			// addon confirmation page ");
			// Assert.assertTrue(verifyElementBytext(confirmMessage, "Your order is
			// complete"),"verifying text on addon confirmation page ");
			Reporter.log("Verify add-ons confirmation page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Verify add-ons confirmation page is  displayed.");
		}
		return this;
	}


	public ManageAddOnsConfirmationPage verifyNetFilxEligibleMessage(String msg) {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader")));

		try {
			yourAccountEligbileText.getText().contains(msg);
			Reporter.log("Your account's now eligible for Netflix is  displayed");
		} catch (Exception e) {
			Assert.fail("Your account's now eligible for Netflix is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifySignUpNextFlixBtnDisplayed() {

		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader")));

		try {
			netFlixSignUpBtn.click();
			Reporter.log("Sign up for Netflix on us Btn is  displayed");
		} catch (Exception e) {
			Assert.fail("Sign up for Netflix on us Btn is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage googlePlayIcon() {

		try {
			Googleplay.isDisplayed();
			Reporter.log("Googleplay Icon is  displayed");
		} catch (Exception e) {
			Assert.fail("Googleplay Icon is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage appStoreIcon() {

		try {
			ApplePay.isDisplayed();
			Reporter.log("AppStoreIcon Icon is  displayed");
		} catch (Exception e) {
			Assert.fail("AppStoreIcon Icon is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage contactUsButton() {

		try {
			ContactUS.isDisplayed();
			Reporter.log("ContactUsButton  is  displayed");
		} catch (Exception e) {
			Assert.fail("ContactUsButton  is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage clickOnReturnHome() {

		try {
			btnOnConfirmationPage.get(0).click();
			Reporter.log("ReturnHomeButton  is  clicked");
		} catch (Exception e) {
			Assert.fail("ReturnHomeButton  is  not clicked");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyGoToPlanBenefits() {

		try {
			btnOnConfirmationPage.get(1).click();
			Reporter.log("Clicked on Go To Plan Benefits");
		} catch (Exception e) {
			Assert.fail("Unable to click on  Go To Plan Benefits");
		}

		try {
			AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
			accountoverviewpage.verifyAccountOverviewPage();
		} catch (Exception e) {
			Assert.fail("Go to Plan Benefits  button goes to the Plan Details page (/plan-details) ");
		}

		return this;
	}

	public ManageAddOnsConfirmationPage verifyThanksHeaderOnConfirmationPage() {

		try {
			confirmationPageHeader.get(0).getText().contains("Thanks");
			confirmationPageHeader.get(0).getText().contains(" for your order!");
			Reporter.log("Thanks header on confirmation page is displayed");
		} catch (Exception e) {
			Assert.fail("Thanks header on confirmation page is not  displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifySecondHeaderOnConfirmationPage(String text) {

		try {
			Assert.assertTrue(confirmationPageHeader.get(1).getText().contains(text),
					text + "is appearing on header confirmation page.");
			Reporter.log(text + "is appearing on header confirmation page.");
		} catch (Exception e) {
			Assert.fail(text + "is appearing on header confirmation page.");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifytextMessage() {

		try {
			Assert.assertTrue(textMessage.getText().contains("Sorry, we hit a snag."));
			Assert.assertTrue(textMessage.getText().contains(
					"Visit the benefit in your plan details to see if any additional steps are required to sign-up for Netflix"));
			Reporter.log(
					"Sorry, we hit a snag.  Visit the benefit in your plan details to see if any additional steps are required to sign-up for Netflix is appearing on  confirmation page.");
		} catch (Exception e) {
			Assert.fail(
					"Sorry, we hit a snag.  Visit the benefit in your plan details to see if any additional steps are required to sign-up for Netflix is appearing on confirmation page.");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyOrderDetailsDisplayed() {

		try {
			orderDetailsHeader.getText().contains("Order details");
			Reporter.log("Order Details text is diplayed");
		} catch (Exception e) {
			Assert.fail("Order Details text is not diplayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyOrderDetailsBladeDisplayedwithDownwarCarat() {
		try {
			checkPageIsReady();
			waitforSpinner();
			Assert.assertTrue(orderDetailsCarat.getAttribute("class").contains("arrowdown_rotation"),
					"rder Details blade is not displayed with downward carat");
			Reporter.log("Order Details blade is displayed with downward carat");
		} catch (Exception e) {
			Assert.fail("Order Details blade is not displayed with downward carat");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage clickOnOrderDetailsBlade() {

		try {
			orderDetailsCarat.click();
			Reporter.log("orderDetailsCarat  is  clicked");
		} catch (Exception e) {
			Assert.fail("orderDetailsCarat  is  not clicked");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyUpgradeNetflixDetailsOnOrderDetails(String removedAddOns,
			String newAddOns) {

		Double d1, d2;
		try {
			Assert.assertTrue(newRemovedAddOnsHeader.get(0).getText().equals("New add-ons"),
					"New add-ons text is Not displayed");
			Reporter.log("New add-ons text is displayed");

			Assert.assertTrue(newAddOnsList.get(0).getText().contains(newAddOns),
					newAddOns + " is not displayed under New add-ons");
			Reporter.log("Upgraded Netfix text '" + newAddOns + "'  is displayed on New add-ons");
			d1 = Double.parseDouble(getNumberValueFromString(newAddOns));

			Assert.assertTrue(newRemovedAddOnsHeader.get(1).getText().equals("Removed add-ons"),
					"Removed add-ons text is Not displayed");
			Reporter.log("Removed add-ons text is displayed");
			Assert.assertTrue(newAddOnsList.get(1).getText().contains(removedAddOns),
					removedAddOns + " is not displayed under Removed add-ons");
			Reporter.log("removed Netfix text '" + removedAddOns + "' is displayed on Removed add-ons");
			d2 = Double.parseDouble(getNumberValueFromString(removedAddOns));

			Double difference = Math.abs(d1 - d2);

			if (removedAddOnsprice.getText().contains("On us")) {
				try {
					Assert.assertTrue(
							String.valueOf(getNumberValueFromString(newAddOnsprice.getText()))
									.contains(String.valueOf(difference)),
							"Price for " + newAddOns + " under New add-ons not mached. Expected price is " + difference
									+ "" + ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(newAddOnsprice.getText())));
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is $" + difference + "/mo");
					Assert.assertTrue(removedAddOnsprice.getText().equals("On us"),
							"Price for " + removedAddOns + " under Removed add-ons not mached. Expected price is On us"
									+ ". Actual price displayed is " + removedAddOnsprice.getText());
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is On us ");
				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
				}
			} else {
				try {
					Assert.assertTrue(String.valueOf(getNumberValueFromString(newAddOnsprice.getText())).contains("7"),
							"Price for " + newAddOns + " under New add-ons not mached. Expected price is $4"
									+ ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(newAddOnsprice.getText())));
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is $7.00/mo");
					Assert.assertTrue(removedAddOnsprice.getText().contains("$4"),
							"Price for " + removedAddOns + " under Removed add-ons not mached. Expected price is $4"
									+ ". Actual price displayed is " + removedAddOnsprice.getText());
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is $4 ");
				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
				}
			}
		} catch (Exception e) {
			Assert.fail("Unable to verify add-ons inside Order details on on addons Confirmation Page");
		}

		return this;

	}

	public ManageAddOnsConfirmationPage verifyDowngradeNetflixDetailsOnOrderDetails(String removedAddOns,
			String newAddOns) {

		Double d1, d2;
		try {
			Assert.assertTrue(newRemovedAddOnsHeader.get(0).getText().equals("New add-ons"),
					"New add-ons text is Not displayed");
			Reporter.log("New add-ons text is displayed");
			Assert.assertTrue(newAddOnsList.get(0).getText().contains(newAddOns),
					newAddOns + " is not displayed under New add-ons");
			Reporter.log("Upgraded Netfix text '" + newAddOns + "'  is displayed on New add-ons");
			d1 = Double.parseDouble(getNumberValueFromString(newAddOns));
			Assert.assertTrue(newRemovedAddOnsHeader.get(1).getText().equals("Removed add-ons"),
					"Removed add-ons text is Not displayed");
			Reporter.log("Removed add-ons text is displayed");
			Assert.assertTrue(newAddOnsList.get(1).getText().contains(removedAddOns),
					removedAddOns + " is not displayed under Removed add-ons");
			Reporter.log("removed Netfix text '" + removedAddOns + "' is displayed on Removed add-ons");
			d2 = Double.parseDouble(getNumberValueFromString(removedAddOns));

			Double difference = Math.abs(d1 - d2);

			if (newAddOnsprice.getText().contains("On us")) {
				try {
					Assert.assertTrue(newAddOnsprice.getText().contains("On us"),
							"Price for " + newAddOns + " under Removed add-ons not mached. Expected price is On us"
									+ ". Actual price displayed is " + newAddOnsprice.getText());
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is On us ");
					Assert.assertTrue(
							String.valueOf(getNumberValueFromString(removedAddOnsprice.getText()))
									.contains(String.valueOf(difference)),
							"Price for " + removedAddOns + " under Remove add-ons not mached. Expected price is "
									+ difference + "" + ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(removedAddOnsprice.getText())));
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is $" + difference
							+ "/mo");

				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
				}
			} else {
				try {
					Assert.assertTrue(newAddOnsprice.getText().contains("$4"),
							"Price for " + newAddOns + " under Removed add-ons not mached. Expected price is $4"
									+ ". Actual price displayed is " + newAddOnsprice.getText());
					Reporter.log("Price for " + newAddOns
							+ " under New add-ons is displayed correctly. Price displayed is $4 ");
					Assert.assertTrue(
							String.valueOf(getNumberValueFromString(removedAddOnsprice.getText())).contains("7"),
							"Price for " + removedAddOns + " under Remove add-ons not mached. Expected price is $7.00"
									+ ". Actual price displayed is "
									+ String.valueOf(getNumberValueFromString(removedAddOnsprice.getText())));
					Reporter.log("Price for " + removedAddOns
							+ " under Removed add-ons is displayed correctly. Price displayed is $7.00/mo");

				} catch (Exception e) {
					Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
				}
			}

		} catch (Exception e) {
			Assert.fail("Unable to verify price inside Order details on on addons Confirmation Page");
		}
		return this;

	}

	public ManageAddOnsConfirmationPage verifyBackArrowIsDisabledOnConfirmationPage() {

		try {
			// backArrow;
			Reporter.log("Back Arrow is disabled");
		} catch (Exception e) {
			Assert.fail("Back Arrow is not disabled");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage clickOnNextStepSignUpBtn() {
		try {
			nextStepSignUpBtn.click();
		} catch (Exception e) {
			Assert.fail("Back Arrow is not disabled");
		}
		return this;
	}

	public String getNumberValueFromString(String string) {
		Matcher m = Pattern.compile("\\$(-?\\d+.\\d+)?.*").matcher(string);
		m.find();
		return m.group(1);
	}


	public ManageAddOnsConfirmationPage checkAndClickOnReturnToHomeCTA() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(returnToHomeCTA));
			clickElementWithJavaScript(returnToHomeCTA);
			Reporter.log("Return to Home CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Return to Home CTA is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage clickOnReturnToHomeCTA() {
		try {
			returnToHomeCTA.click();
			Reporter.log("Return to Home CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Return to Home CTA is not clicked");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyExistingNetFilxEligibleMessageIsDisplayedOrNot() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader")));
		try {
			yourAccountEligbileText.isDisplayed();
			Assert.fail("Your account's now eligible for Netflix is displayed");
		} catch (Exception e) {
			Reporter.log("Your account's now eligible for Netflix is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyExistingSignUpNextFlixBtnIsDisplayedOrNot() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader")));
		try {
			netFlixSignUpBtn.isDisplayed();
			Assert.fail("Sign up for Netflix on us Btn is displayed");
		} catch (Exception e) {
			Reporter.log("Sign up for Netflix on us Btn is not displayed");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyTextOneLastStepIsDisplayedOrNot() {
		try {
			oneLastStepText.isDisplayed();
			Reporter.log("One Last Step text is displayed on confirmation page when user opted for Netflix");
			Assert.assertEquals(oneLastStepText.getText().trim(), "One last step...",
					"Text One last step is mismatched");
		} catch (Exception e) {
			Assert.fail("One last Step text is not displayed.");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyTextBelowOneLastStepIsDisplayedOrNot() {
		try {
			String msg = "To finish activating, just log in to your Netflix account or create new one";
			textToFinishActivating.isDisplayed();
			Reporter.log("Text below One Last Step text is displayed on confirmation page when user opted for Netflix");
			Assert.assertEquals(textToFinishActivating.getText().trim(), msg, "Text below One last step is mismatched");
		} catch (Exception e) {
			Assert.fail("Text below One last Step text is not displayed.");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyTextLoremDolorIsDisplayedOrNot() {
		try {
			loremIpsumDolorText.isDisplayed();
			Reporter.log("Text Lorem dolor is displayed on confirmation page when user opted for Netflix");
			Assert.assertEquals(loremIpsumDolorText.getText().trim(), "Lorem ipsum dolor",
					"Text Lorem Ipsum dolor is mismatched");
		} catch (Exception e) {
			Assert.fail("Text Lorem Ipsum dolor is not displayed.");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyTermsAndConditionsLinkInNewNetflixComponentIsDisplayedOrNot() {
		try {
			termsAndConditionsLink.isDisplayed();
			Reporter.log("Terms and conditions link is displayed in new Netflix component when user opted for Netflix");
		} catch (Exception e) {
			Assert.fail(
					"Terms and conditions link is not displayed in new Netflix component when user opted for Netflix");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyBackToAccountOverviewCTAInNewNetflixComponentWhetherIsDisplayedOrNot() {
		try {
			backToAccountOverviewCTA.isDisplayed();
			Reporter.log(
					"Back to Account Overview CTA is displayed in new Netflix component when user opted for Netflix");
		} catch (Exception e) {
			Assert.fail(
					"Back to Account Overview CTA is not displayed in new Netflix component when user opted for Netflix");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyGoToNetflixCTAInNewNetflixComponentWhetherIsDisplayedOrNot() {
		try {
			goToNetflixCTA.isDisplayed();
			Reporter.log("Goto Netflix CTA is displayed in new Netflix component when user opted for Netflix");
		} catch (Exception e) {
			Assert.fail("Goto Netflix CTA is not displayed in new Netflix component when user opted for Netflix");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyNextStepsComponentOnConfirmationPage() {
		verifyTextOneLastStepIsDisplayedOrNot();
		verifyTextBelowOneLastStepIsDisplayedOrNot();
		verifyTextLoremDolorIsDisplayedOrNot();
		verifyTermsAndConditionsLinkInNewNetflixComponentIsDisplayedOrNot();
		verifyBackToAccountOverviewCTAInNewNetflixComponentWhetherIsDisplayedOrNot();
		verifyGoToNetflixCTAInNewNetflixComponentWhetherIsDisplayedOrNot();
		return this;
	}

	public ManageAddOnsConfirmationPage clickOnActivateNetflixBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(activateNetflixBtn));
			activateNetflixBtn.click();
			Reporter.log("Clicked on Activate Netflix button");
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("Activate Netflix button is not displaying");
		}
		return this;
	}

	public ManageAddOnsConfirmationPage verifyNetflixSigningUpMessage() {
		checkPageIsReady();
		try {
			Assert.assertTrue(thanksForSignUpMessage.isDisplayed(), "Thanks For Sign Up header text is not displaying");
			Reporter.log("Thanks For Sign Up header text message is displayed");
		} catch (Exception e) {
			Assert.fail("Thanks For Sign Up header text is not displaying");
		}
		return this;
	}

}
