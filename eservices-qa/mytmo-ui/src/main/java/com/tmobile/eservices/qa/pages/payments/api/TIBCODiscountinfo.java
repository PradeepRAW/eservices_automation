package com.tmobile.eservices.qa.pages.payments.api;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.response.Response;

public class TIBCODiscountinfo extends EOSCommonLib {

	public Response geResponseTibco_Discuntinfo(Map<Object, Object> tokenMap) throws Exception {
		Response response = null;
		String fileName = System.getProperty("user.dir")
				+ "/src/test/resources/reqtemplates/pub/GetDiscountinfomiddleware.txt";
		String requestBody = new String(Files.readAllBytes(Paths.get(fileName)));
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		RestService restService = new RestService(updatedRequest, "https://tugs-tmo.internal.t-mobile.com");
		restService.setContentType("text/xml");
		restService.addHeader("soapaction", "gs.Discount.getDiscountInfo");
		response = restService.callService("genericesp/soap", RestCallType.POST);

		return response;
	}

	public Map<Object, Object> getResponseValuesFromDiscountinfo(Response response, int lines)
			throws ParserConfigurationException, SAXException, IOException {
		Map<Object, Object> discountvals = new HashMap<Object, Object>();
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(response.asString()));

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			Double dblDiscount = 0.0;

			NodeList disc = doc.getElementsByTagName("pfx3:discount");
			for (int nde = 0; nde < disc.getLength(); nde++) {
				Node childnode = disc.item(nde);

				dblDiscount = dblDiscount + finddiscounts(childnode);

			}

			// String discount =
			// doc.getElementsByTagName("pfx3:amount").item(0).getTextContent().trim();

			// Double dblDiscount=Double.parseDouble(discount);
			// dblDiscount=5.0*lines;
			DecimalFormat df = new DecimalFormat("0.0");

			String reqdiscount = df.format(dblDiscount);
			if (dblDiscount > 0)
				discountvals.put("discounteligible", true);
			else
				discountvals.put("discounteligible", false);

			discountvals.put("discount", reqdiscount);

		} else {
			discountvals.put("discounteligible", false);
			discountvals.put("discount", "0.0");
		}

		return discountvals;
	}

	public Double finddiscounts(Node childnode) {
		String[] arraydiscountcodes = { "MEPYDIS", "MEPYDISUB", "MEPYDIS40", "MEPYDIS20", "MEPYDIS35" };
		List<String> discountcodes = Arrays.asList(arraydiscountcodes);
		Double disc = 0.0;
		NodeList discchildnodes = childnode.getChildNodes();
		for (int levelnde = 0; levelnde < discchildnodes.getLength(); levelnde++) {
			if (discchildnodes.item(levelnde).getNodeName().equalsIgnoreCase("pfx3:code")) {
				String disccode = discchildnodes.item(levelnde).getTextContent();
				if (!discountcodes.contains(disccode))
					return 0.0;
			}

			if (discchildnodes.item(levelnde).getNodeName().equalsIgnoreCase("pfx3:amount")) {
				String discval = discchildnodes.item(levelnde).getTextContent();
				return Double.parseDouble(discval);
			}
		}
		return disc;
	}

}