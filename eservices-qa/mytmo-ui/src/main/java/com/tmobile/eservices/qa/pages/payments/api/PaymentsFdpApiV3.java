package com.tmobile.eservices.qa.pages.payments.api;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class PaymentsFdpApiV3 extends ApiCommonLib {
	
	/***
	 * Summary: This API is used do the payments  through OrchestrateOrders API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-interactionId
	 * 	-workflowId
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	
	private Map<String, String> buildPaymentsFdpApiHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		//headers.put("interactionId","123456787");
		//headers.put("Authorization", checkAndGetAccessToken());
		//headers.put("cache-control", "no-cache");
		//headers.put("workflowId", "BILLPAY");

		
	
		return headers;
	}
	
	public Response PaymentsFdpApi(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildPaymentsFdpApiHeader(apiTestData);
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		tokenMap.replace("version", "v1");
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		headers.put("ban", apiTestData.getBan());
		
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/fdp", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}
	
	
	public Response PaymentsFdpApiDelete(ApiTestData apiTestData,Map<String, String> tokenMap,String accountNumber) throws Exception {
		Map<String, String> headers = buildPaymentsFdpApiHeader(apiTestData);
		headers.put("Authorization", checkAndGetAccessToken());
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/fdp/"+accountNumber, RestCallType.DELETE);
		System.out.println(response.asString());
		return response;
		
	}
	
	
}
