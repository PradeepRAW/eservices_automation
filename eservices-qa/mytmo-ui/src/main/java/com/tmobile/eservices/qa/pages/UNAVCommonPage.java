package com.tmobile.eservices.qa.pages;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.listeners.Verify;

import io.appium.java_client.AppiumDriver;

/**
 * This page contains all the UNAV Header and Footer elements and methods
 * 
 * @author sputti2
 *
 */
public class UNAVCommonPage extends CommonPage {

	private final String pageUrl = "/home";

	// UNAV Sub Header Links
	private final String billPageUrl = "/billandpay";
	private final String usagePageUrl = "/usage";
	private final String accountPageUrl = "/account-overview";
	private final String phonePageUrl = "/myphone/myphonedetails.html";
	private final String shopPageUrl = "/purchase/shop";
	private final String manageMyLinesPageUrl = "/account-overview";
	private final String editMyProfilePageUrl = "/profile";
	private final String addDevicePageUrl = "/purchase/device-intent";
	private final String seeLatestDealsPageUrl = "/purchase/productlist";

	// UNAV Header Objects - Universal Header
	@FindBy(css = "#universal-menu-0")
	private WebElement umenu0;

	@FindBy(css = "#universal-menu-1")
	private WebElement umenu1;

	@FindBy(css = "#universal-menu-2")
	private WebElement umenu2;

	@FindBy(css = "#universal-menu-3")
	private WebElement umenu3;

	@FindBy(css = "#universal-menu-4")
	private WebElement umenu4;

	// UNAV Header Objects - Sub-Header
	@FindBy(css = "#digital-header-nav-link-head-0")
	protected WebElement navLink0;

	@FindBy(css = "#digital-header-nav-link-head-1")
	protected WebElement navLink1;

	@FindBy(css = "#digital-header-nav-link-head-2")
	protected WebElement navLink2;

	@FindBy(css = "#digital-header-nav-link-head-3")
	protected WebElement navLink3;

	@FindBy(css = "#digital-header-nav-link-head-4")
	protected WebElement navLink4;

	@FindBy(css = "a[data-analytics-value=\"Let's talk\"]")
	protected WebElement letsTalkMenu;

	@FindBy(xpath = "//span[contains(text(),'Message us')]")
	protected WebElement messageUsLink;

	@FindBy(css = "span[role='heading']")
	protected WebElement lpmessageUsHeader;

	@FindBy(css = "li.nav_link_highlight div a")
	protected WebElement selectedMenuOnUNAVTMO;

	@FindBy(css = "li.nav_link_highlight_wout_dd div a")
	protected WebElement selectedMenuOnUNAVMytmo;

	// Header first dropdown for Navigation links
	@FindBy(css = "#digital-header-nav-link-category-link-0-0")
	public WebElement navItem_0_0;

	@FindBy(css = "#digital-header-nav-link-category-link-0-1")
	public WebElement navItem_0_1;

	@FindBy(css = "#digital-header-nav-link-category-link-0-2")
	public WebElement navItem_0_2;

	@FindBy(css = "#digital-header-nav-link-category-link-0-3")
	public WebElement navItem_0_3;

	@FindBy(css = "#digital-header-nav-link-category-link-0-4")
	public WebElement navItem_0_4;

	// Header second dropdown for Navigation links
	@FindBy(css = "#digital-header-nav-link-category-link-1-0")
	public WebElement navItem_1_0;

	@FindBy(css = "#digital-header-nav-link-category-link-1-1")
	public WebElement navItem_1_1;

	@FindBy(css = "#digital-header-nav-link-category-link-1-2")
	public WebElement navItem_1_2;

	@FindBy(css = "#digital-header-nav-link-category-link-1-3")
	public WebElement navItem_1_3;

	@FindBy(css = "#digital-header-nav-link-category-link-1-4")
	public WebElement navItem_1_4;

	// Header third dropdown for Navigation links
	@FindBy(css = "#digital-header-nav-link-category-link-2-0")
	public WebElement navItem_2_0;

	@FindBy(css = "#digital-header-nav-link-category-link-2-1")
	public WebElement navItem_2_1;

	@FindBy(css = "#digital-header-nav-link-category-link-2-2")
	public WebElement navItem_2_2;

	@FindBy(css = "#digital-header-nav-link-category-link-2-3")
	public WebElement navItem_2_3;

	@FindBy(css = "#digital-header-nav-link-category-link-2-4")
	public WebElement navItem_2_4;

	// Header fouth dropdown for Navigation links
	@FindBy(css = "#digital-header-nav-link-category-link-3-0")
	public WebElement navItem_3_0;

	@FindBy(css = "#digital-header-nav-link-category-link-3-1")
	public WebElement navItem_3_1;

	@FindBy(css = "#digital-header-nav-link-category-link-3-2")
	public WebElement navItem_3_2;

	@FindBy(css = "#digital-header-nav-link-category-link-3-3")
	public WebElement navItem_3_3;

	@FindBy(css = "#digital-header-nav-link-category-link-3-4")
	public WebElement navItem_3_4;

	// Header fifth dropdown for Navigation links
	@FindBy(css = "#digital-header-nav-link-category-link-4-0")
	public WebElement navItem_4_0;

	@FindBy(css = "#digital-header-nav-link-category-link-4-1")
	public WebElement navItem_4_1;

	@FindBy(css = "#digital-header-nav-link-category-link-4-2")
	public WebElement navItem_4_2;

	@FindBy(css = "#digital-header-nav-link-category-link-4-3")
	public WebElement navItem_4_3;

	@FindBy(css = "#digital-header-nav-link-category-link-4-4")
	public WebElement navItem_4_4;

	/******************** utilities page objects ***************/

	@FindBy(css = "#digital-header-utility-0")
	public WebElement utility_0;

	@FindBy(css = "#digital-header-utility-0 > .d-inline")
	public WebElement utility_0_Label;

	@FindBy(css = "#digital-header-utility-0 > span.mdi-store")
	public WebElement storeIcon;

	@FindBy(css = "#digital-header-utility-1")
	public WebElement utility_1;

	@FindBy(css = "#digital-header-utility-1 > .d-inline")
	public WebElement utility_1_Label;

	@FindBy(css = "#digital-header-utility-1 > span.mdi-cellphone")
	private WebElement phoneIcon;

	@FindBy(css = "#digital-header-utility-3")
	public WebElement utility_3;

	@FindBy(css = "#digital-header-utility-4")
	public WebElement utility_4;

	@FindBy(css = "#digital-header-utility-4 > span.mdi-magnify")
	public WebElement headersearchIcon;

	@FindBy(css = "a#cart")
	public WebElement cart;

	@FindBy(css = "#cart > .d-inline")
	public WebElement cart_label;

	@FindBy(css = "span.mdi-cart")
	public WebElement cartIcon;

	@FindBy(css = "span.mdi-cart>span")
	public WebElement cartCounter;

	@FindBy(css = "#cart > span.d-inline.show-on-sm-modal")
	public WebElement cartGetStrted;

	/***** My Account user links - page objects -Desktop *****/

	@FindBy(css = "#user-links-dropdown")
	public WebElement myAccount_desk;

	@FindBy(css = "#user-links-dropdown > span.mdi-chevron-down")
	public WebElement chevronDown_myAcc_desk;

	@FindBy(css = "#user-links-dropdown > span.mdi-chevron-up")
	public WebElement chevronUp_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-0-0")
	public WebElement logIn_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-1-0")
	public WebElement quickActions_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-0-0-0")
	public WebElement profile_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-0-0-1")
	public WebElement AccHistory_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-0-0-3")
	public WebElement logout_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-1-0-0")
	public WebElement billPay_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-1-0-1")
	public WebElement addDevice_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-1-0-2")
	public WebElement upgrade_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-1-0-3")
	public WebElement checkOrderStatus_myAcc_desk;

	@FindBy(css = "#user-link-dropdown-item-category-name-1-0-4")
	public WebElement support_myAcc_desk;

	// Account Drop down Options
	@FindBy(linkText = "Manage my lines")
	private WebElement manageMyLinesLink;

	@FindBy(linkText = "Edit my Profile")
	private WebElement editMyProfileLink;

	// Phone Drop down Options
	@FindBy(linkText = "Report a lost or stolen device")
	private WebElement reportLostDeviceLink;

	// Shop Drop down Options
	@FindBy(linkText = "Add a person or device to my account")
	private WebElement addDeviceLink;

	@FindBy(linkText = "See the latest deals")
	private WebElement seeLatestDealsLink;

	// UNAV Utilities - Objects
	@FindBy(css = "#showIosId [id*='banner_cross']")
	private WebElement tMobileAppIOSPopUp;

	@FindBy(css = "#showIosId [id*='banner_cross']")
	private WebElement mobileSearchButton;

	@FindBy(css = "#searchIconId")
	private WebElement searchIcon;

	@FindBy(css = "[aria-label*='click to clear the search']")
	private WebElement search;

	@FindBy(css = "#searchId")
	private WebElement searchTextBox;

	@FindBy(css = "a[id='addHighSpeedDataLink']")
	private List<WebElement> searchResults;

	// Account History Drop down objects
	@FindBy(css = ".gh-dropdown-toggle .gh-dropdown-name")
	private WebElement profileName;

	@FindBy(css = "div#model")
	private WebElement profileDropDown;

	@FindBy(css = ".gh-dropdown-toggle .gh-arrow-down-gray")
	private WebElement profileDropDownIcon;

	@FindBy(css = ".gh-dropdown-option [data-href*='profile']")
	private WebElement profileMenu;

	@FindBy(css = ".gh-dropdown-option [data-href*='accounthistory']")
	private WebElement accountHistoryMenu;

	@FindBy(css = ".gh-dropdown-option [data-href*='accountchooser']")
	private WebElement switchAccountMenu;

	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	private WebElement logOutMenu;

	// Home Page Objects
	@FindBy(css = ".gh-tmo-icon")
	protected WebElement tMobileIcon;

	@FindBy(css = "#doneButton")
	private WebElement announcementModalWindow;

	// UNAV Footer Page Objects
	@FindBy(css = "#digital-footer-tmobile-icon-title")
	public WebElement tmobile_title;

	@FindBy(css = "#digital-footer-tmobile-icon-0 > span")
	public WebElement socialLink0;

	@FindBy(css = "#digital-footer-tmobile-icon-1 > span")
	public WebElement socialLink1;

	@FindBy(css = "#digital-footer-tmobile-icon-2 > span")
	public WebElement socialLink2;

	@FindBy(css = "#digital-footer-tmobile-icon-3 > span")
	public WebElement socialLink3;

	@FindBy(css = "#digital-footer-tmobile-icon-4 > span")
	public WebElement socialLink4;

	@FindBy(css = "#digital-footer-language-switch-0")
	public WebElement language_0;

	@FindBy(css = "#digital-footer-language-switch-1")
	public WebElement language_1;

//*****page objects for footer labels and sub-footer link*****//

	@FindBy(css = "#digital-footer-category-name-0")
	public WebElement footerLabel_0;

	@FindBy(css = "#digital-footer-categorized-link-0-0")
	public WebElement subFooterLink_0_0;

	@FindBy(css = "#digital-footer-categorized-link-0-1")
	public WebElement subFooterLink_0_1;

	@FindBy(css = "#digital-footer-categorized-link-0-2")
	public WebElement subFooterLink_0_2;

	@FindBy(css = "#digital-footer-categorized-link-0-3")
	public WebElement subFooterLink_0_3;

	@FindBy(css = "#digital-footer-categorized-link-0-4")
	public WebElement subFooterLink_0_4;

	@FindBy(css = "#digital-footer-categorized-link-0-5")
	public WebElement subFooterLink_0_5;

	@FindBy(css = "#digital-footer-category-name-1")
	public WebElement footerLabel_1;

	@FindBy(css = "#digital-footer-categorized-link-1-0")
	public WebElement subFooterLink_1_0;

	@FindBy(css = "#digital-footer-categorized-link-1-1")
	public WebElement subFooterLink_1_1;

	@FindBy(css = "#digital-footer-categorized-link-1-2")
	public WebElement subFooterLink_1_2;

	@FindBy(css = "#digital-footer-categorized-link-1-3")
	public WebElement subFooterLink_1_3;

	@FindBy(css = "#digital-footer-categorized-link-1-4")
	public WebElement subFooterLink_1_4;

	@FindBy(css = "#digital-footer-categorized-link-1-5")
	public WebElement subFooterLink_1_5;

	@FindBy(css = "#digital-footer-category-name-2")
	public WebElement footerLabel_2;

	@FindBy(css = "#digital-footer-categorized-link-2-0")
	public WebElement subFooterLink_2_0;

	@FindBy(css = "#digital-footer-categorized-link-2-1")
	public WebElement subFooterLink_2_1;

	@FindBy(css = "#digital-footer-categorized-link-2-2")
	public WebElement subFooterLink_2_2;

	@FindBy(css = "#digital-footer-categorized-link-2-3")
	public WebElement subFooterLink_2_3;

	@FindBy(css = "#digital-footer-categorized-link-2-4")
	public WebElement subFooterLink_2_4;

	@FindBy(css = "#digital-footer-categorized-link-2-5")
	public WebElement subFooterLink_2_5;

	@FindBy(css = "#digital-footer-categorized-link-2-6")
	public WebElement subFooterLink_2_6;

	@FindBy(css = "#digital-footer-category-name-3")
	public WebElement footerLabel_3;

	@FindBy(css = "#digital-footer-categorized-link-3-0")
	public WebElement subFooterLink_3_0;

	@FindBy(css = "#digital-footer-categorized-link-3-1")
	public WebElement subFooterLink_3_1;

	@FindBy(css = "#digital-footer-categorized-link-3-2")
	public WebElement subFooterLink_3_2;

	@FindBy(css = "#digital-footer-categorized-link-3-3")
	public WebElement subFooterLink_3_3;

	@FindBy(css = "#digital-footer-categorized-link-3-4")
	public WebElement subFooterLink_3_4;

	@FindBy(css = "#digital-footer-categorized-link-3-5")
	public WebElement subFooterLink_3_5;

	@FindBy(css = "#digital-footer-category-name-4")
	public WebElement footerLabel_4;

	@FindBy(css = "#digital-footer-categorized-link-4-0")
	public WebElement subFooterLink_4_0;

	@FindBy(css = "#digital-footer-categorized-link-4-1")
	public WebElement subFooterLink_4_1;

	@FindBy(css = "#digital-footer-category-name-5")
	public WebElement footerLabel_5;

	@FindBy(css = "#digital-footer-categorized-link-5-0")
	public WebElement subFooterLink_5_0;

	@FindBy(css = "#digital-footer-categorized-link-5-1")
	public WebElement subFooterLink_5_1;

	@FindBy(css = "#digital-footer-categorized-link-5-2")
	public WebElement subFooterLink_5_2;

	@FindBy(css = "#digital-footer-category-name-6")
	public WebElement footerLabel_6;

	@FindBy(css = "#digital-footer-categorized-link-6-0")
	public WebElement subFooterLink_6_0;

	@FindBy(css = "#digital-footer-categorized-link-6-1")
	public WebElement subFooterLink_6_1;

	@FindBy(css = "#digital-footer-categorized-link-6-2")
	public WebElement subFooterLink_6_2;

	@FindBy(css = "#digital-footer-categorized-link-6-3")
	public WebElement subFooterLink_6_3;

	@FindBy(css = "#digital-footer-categorized-link-6-4")
	public WebElement subFooterLink_6_4;

	@FindBy(css = "#digital-footer-category-name-7")
	public WebElement footerLabel_7;

	@FindBy(css = "#digital-footer-categorized-link-7-0")
	public WebElement subFooterLink_7_0;

	@FindBy(css = "#digital-footer-categorized-link-7-1")
	public WebElement subFooterLink_7_1;

	@FindBy(css = "#digital-footer-categorized-link-7-2")
	public WebElement subFooterLink_7_2;

	@FindBy(css = "#digital-footer-category-name-8")
	public WebElement footerLabel_8;

	@FindBy(css = "#digital-footer-categorized-link-8-0")
	public WebElement subFooterLink_8_0;

	@FindBy(css = "#digital-footer-categorized-link-8-1")
	public WebElement subFooterLink_8_1;

	@FindBy(css = "#digital-footer-categorized-link-8-2")
	public WebElement subFooterLink_8_2;

	@FindBy(css = "#digital-footer-categorized-link-8-3")
	public WebElement subFooterLink_8_3;

	@FindBy(css = "#digital-footer-categorized-link-8-4")
	public WebElement subFooterLink_8_4;

	@FindBy(css = "#digital-footer-categorized-link-8-5")
	public WebElement subFooterLink_8_5;

	@FindBy(css = "#digital-footer-category-name-9")
	public WebElement footerLabel_9;

	@FindBy(css = "#digital-footer-categorized-link-9-0")
	public WebElement subFooterLink_9_0;

	@FindBy(css = "#digital-footer-categorized-link-9-1")
	public WebElement subFooterLink_9_1;

	@FindBy(css = "#digital-footer-categorized-link-9-2")
	public WebElement subFooterLink_9_2;

	@FindBy(css = "#digital-footer-category-name-10")
	public WebElement footerLabel_10;

	@FindBy(css = "#digital-footer-categorized-link-10-0")
	public WebElement subFooterLink_10_0;

	@FindBy(css = "#digital-footer-categorized-link-10-1")
	public WebElement subFooterLink_10_1;

	@FindBy(css = "#digital-footer-categorized-link-10-2")
	public WebElement subFooterLink_10_2;

	@FindBy(css = "#digital-footer-category-name-11")
	public WebElement footerLabel_11;

	@FindBy(css = "#digital-footer-categorized-link-11-0")
	public WebElement subFooterLink_11_0;

	@FindBy(css = "#digital-footer-categorized-link-11-1")
	public WebElement subFooterLink_11_1;

//******page objects for Footer -2******//

	@FindBy(css = "img[alt='tmobile logo link']")
	public WebElement tMoLogo_footer;

	@FindBy(css = "#digital-footer-bottom-link-top-0")
	public WebElement footer_link_top_0;

	@FindBy(css = "#digital-footer-bottom-link-top-1")
	public WebElement footer_link_top_1;

	@FindBy(css = "#digital-footer-bottom-link-top-2")
	public WebElement footer_link_top_2;

	@FindBy(css = "#digital-footer-bottom-link-top-3")
	public WebElement footer_link_top_3;

	@FindBy(css = "#digital-footer-bottom-link-top-4")
	public WebElement footer_link_top_4;

	@FindBy(css = "#digital-footer-bottom-link-top-5")
	public WebElement footer_link_top_5;

	@FindBy(css = "#digital-footer-bottom-link-bottom-0")
	public WebElement footer_link_bottom_0;

	@FindBy(css = "#digital-footer-bottom-link-bottom-1")
	public WebElement footer_link_bottom_1;

	@FindBy(css = "#digital-footer-bottom-link-bottom-2")
	public WebElement footer_link_bottom_2;

	@FindBy(css = "#digital-footer-bottom-link-bottom-3")
	public WebElement footer_link_bottom_3;

	@FindBy(css = "#digital-footer-bottom-link-bottom-4")
	public WebElement footer_link_bottom_4;

	@FindBy(css = "#digital-footer-bottom-link-bottom-5")
	public WebElement footer_link_bottom_5;

	@FindBy(css = "#digital-footer-bottom-link-bottom-6")
	public WebElement footer_link_bottom_6;

	@FindBy(css = "#digital-footer-bottom-link-bottom-7")
	public WebElement footer_link_bottom_7;

	@FindBy(css = "#digital-footer-bottom-link-bottom-8")
	public WebElement footer_link_bottom_8;

	@FindBy(css = "#digital-footer-bottom-link-bottom-10")
	public WebElement footer_link_bottom_9;

//**************Page objects for social/corporate links - Footer*******************//

	@FindBy(css = "#digital-footer-corporate-icon-0")
	public WebElement bottom_socailLink0;

	@FindBy(css = "#digital-footer-corporate-icon-1")
	public WebElement bottom_socailLink1;

	@FindBy(css = "#digital-footer-corporate-icon-2")
	public WebElement bottom_socailLink2;

	@FindBy(css = "#digital-footer-corporate-icon-3")
	public WebElement bottom_socailLink3;

	@FindBy(css = "#digital-footer-copyright")
	public WebElement copyright;

	// ****************Page objects for expand button in mobile view and tablet
	// view*****************//
	// Header

	@FindBy(css = "#digital-header-utility-mobile-2")
	public WebElement header_myAccount_icon;

	@FindBy(css = "#digital-header-utility-mobile-4")
	public WebElement header_wireless_myAccount_icon;

	@FindBy(css = ".navbar__mobile_text")
	public WebElement header_wireless_myAccount_text;

	@FindBy(css = "#digital-header-menu-button")
	public WebElement header_menu;

	@FindBy(css = "#universal-menu-mobile-0")
	public WebElement wireless_mob;

	@FindBy(css = "#universal-menu-mobile-1")
	public WebElement business_mob;

	@FindBy(css = "#universal-menu-mobile-2")
	public WebElement prepaid_mob;

	@FindBy(css = "#universal-menu-mobile-3")
	public WebElement tv_mob;

	@FindBy(css = "#universal-menu-mobile-4")
	public WebElement banking_mob;

	@FindBy(css = "#digital-header-nav-link-mobile-toggel-0>span")
	public WebElement plusExpand_minusMinimize_mob_0;

	@FindBy(css = "#digital-header-nav-link-mobile-toggel-1>span")
	public WebElement plusExpand_minusMinimize_mob_1;

	@FindBy(css = "#digital-header-nav-link-mobile-toggel-2>span")
	public WebElement plusExpand_minusMinimize_mob_2;

	@FindBy(css = "#digital-header-nav-link-mobile-toggel-3>span")
	public WebElement plusExpand_minusMinimize_mob_3;

	@FindBy(css = "#digital-header-nav-link-mobile-toggel-4>span")
	public WebElement plusExpand_minusMinimize_mob_4;

	/********************
	 * utilities page objects-Mobile and Tab specific
	 ***************/

	@FindBy(css = "#digital-header-utility-0 > span.mdi-chevron-right")
	public WebElement utility_0_rightArrow_mob;

	@FindBy(css = "#digital-header-utility-1 > span.mdi-chevron-right")
	public WebElement utility_1_rightArrow_mob;

	@FindBy(css = "#digital-header-utility-2 > span.mdi-chevron-right")
	public WebElement utility_2_rightArrow_mob;

	@FindBy(css = "#digital-header-utility-4 > span.mdi-chevron-right")
	public WebElement utility_4_rightArrow_mob;

	/***** Menu- user links - page objects - Mobile or Tablet *****/
	@FindBy(css = "#user-links-dropdown-mobile")
	public WebElement myAccount_mob;

	@FindBy(css = "#user-links-dropdown-mobile > span.mdi-chevron-down")
	public WebElement chevronDown_myAcc_mob;

	@FindBy(css = "#user-links-dropdown-mobile > span.mdi-chevron-up")
	public WebElement chevronUp_myAcc_mob;

	@FindBy(css = "a#cart > span.mdi-chevron-right")
	public WebElement cart_rightArrow_mob;

	@FindBy(css = "#user-links-dropdown-mobile")
	public WebElement myAcc_mob;

	@FindBy(css = "#user-links-dropdown-mobile > span.mdi-chevron-right")
	public WebElement myAcc_rightArrow_mob;

	@FindBy(xpath = "//*[.=\"MORE FROM TMOBILE\"]")
	public WebElement moreFromTmob_mob;

	// Footer
	@FindBy(css = "#digital-footer-panel-0 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton0;

	@FindBy(css = "#digital-footer-panel-1 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton1;

	@FindBy(css = "#digital-footer-panel-2 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton2;

	@FindBy(css = "#digital-footer-panel-3 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton3;

	@FindBy(css = "#digital-footer-panel-4 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton4;

	@FindBy(css = "#digital-footer-panel-5 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton5;

	@FindBy(css = "#digital-footer-panel-6 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton6;

	@FindBy(css = "#digital-footer-panel-7 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton7;

	@FindBy(css = "#digital-footer-panel-8 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton8;

	@FindBy(css = "#digital-footer-panel-9 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton9;

	@FindBy(css = "#digital-footer-panel-10 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton10;

	@FindBy(css = "#digital-footer-panel-11 > div > div.expansion__panel--header > button > span")
	public WebElement expandButton11;

	@FindBy(xpath = "//li/a[contains(@data-analytics-value,'Search')]")
	private List<WebElement> typeAheadSearchResults;

	@FindBy(css = "a[aria-label='Search label'] span[class*='sm-modal']")
	private WebElement searchButton;

	@FindBy(css = "div[class='navbar__search ng-star-inserted is-active'] [id='nav_search_text']")
	private WebElement searchTextBox1;

	@FindBy(css = "div[class*='navbar__search ng-star-inserted is-active'] input[placeholder='Search']")
	private WebElement searchHintText;

	@FindBy(css = "div[class*='navbar__search ng-star-inserted is-active'] [class*='search-results-container']")
	private WebElement searchDropDown;

	@FindBy(css = "a[aria-label='Search label'] span[class*='close']")
	private WebElement searchCloseButton;

	@FindBy(css = "div[class*='navbar__search ng-star-inserted is-active'] span[class*='search-input-icon']")
	private WebElement searchIcon1;

	@FindBy(xpath = "//div[contains(@class,'is-active')]/div/div/div/p[contains(.,'Quick links')]")
	private WebElement quickLinksHeadline;

	@FindBy(xpath = "//div[contains(@class,'is-active')]/div/div/div/p[contains(.,'Quick links')]/following-sibling::li")
	private List<WebElement> quickLinks;

	@FindBy(xpath = "//div[contains(@class,'is-active')]/div/div/div/p[contains(.,'Top searches')]")
	private WebElement topSearchesHeadline;

	@FindBy(xpath = "//div[contains(@class,'is-active')]/div/div/div/p[contains(.,'Top searches')]/following-sibling::li")
	private List<WebElement> topSearches;

	@FindBy(xpath = "//div[contains(@class,'is-active')]/div/div/div/p[contains(.,'Top searches')]/following-sibling::li/span")
	private List<WebElement> searchIconBesideTopSearch;

	@FindBy(css = "#user-link-dropdown-item-0-0-0")
	public WebElement backToMyAccountLink;

	/**
	 * 
	 * @param webDriver
	 */
	public UNAVCommonPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Home Page
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitforSpinner();
			if (isElementDisplayed(announcementModalWindow)) {
				announcementModalWindow.click();
			}
			// waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			// waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			waitFor(ExpectedConditions.urlContains(pageUrl));
		} catch (Exception e) {
			Assert.fail("Home page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Home Page
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyHomePage() {
		checkPageIsReady();
		waitforSpinner();
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			waitFor(ExpectedConditions.titleContains("Home"));
		} else {
			verifyPageLoaded();
		}
		Reporter.log("Home page is displayed");
		return this;
	}

	// Verify UNAV Header
	/**
	 * Verify T-Mobile Logo Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyTMobileLogo() {
		try {
			Assert.assertTrue(isElementDisplayed(tMobileIcon));
			Reporter.log("T-Mobile Logo is displayed");
		} catch (Exception e) {
			Assert.fail("T-Mobile Logo is not dispalyed");
		}
		return this;
	}

	/**
	 * Click tmobile icon.
	 */
	public UNAVCommonPage clickTMobileIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(tMobileIcon));
			clickElement(tMobileIcon);
		} catch (Exception e) {
			Reporter.log("Click on tmobile icon failed");
			Assert.fail("Click on tmobile icon failed");
		}
		return this;
	}

	/**
	 * Verify Bill Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyBillLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink0));
			Assert.assertTrue(navLink0.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_BILL));
			Reporter.log(navLink0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Bill Link
	 */
	public UNAVCommonPage clickBillLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink0));
			clickElement(navLink0);
			Reporter.log("Clicked on bill link");
		} catch (Exception e) {
			Reporter.log("Click on bill link failed");
			Assert.fail("Bill link not found");
		}
		return this;
	}

	/**
	 * Verify Bill page.
	 *
	 * @return the BillPage class instance.
	 */
	public UNAVCommonPage verifyBillPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(billPageUrl);
			Reporter.log("Bill page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Bill page not displayed");
		}
		return this;
	}

	/**
	 * Verify Usage Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyUsageLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink1));
			Assert.assertTrue(navLink1.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_USAGE));
			Reporter.log(navLink1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Usage Link
	 */
	public UNAVCommonPage clickUsageLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink1));
			navLink1.click();
			Reporter.log("Clicked on usage menu");
		} catch (Exception e) {
			Reporter.log("Click on usage menu failed");
			Assert.fail("Click on usage menu failed");
		}
		return this;
	}

	/**
	 * Verify Usage page.
	 *
	 * @return the UsagePage class instance.
	 */
	public UNAVCommonPage verifyUsagePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(usagePageUrl);
			Reporter.log("Usage page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Usage page not displayed");
		}
		return this;
	}

	/**
	 * Verify Account Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyAccountLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink2));
			Assert.assertTrue(navLink2.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_ACCOUNT));
			Reporter.log(navLink2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Account Link
	 */
	public UNAVCommonPage clickAccountLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2));
			navLink2.click();
			Reporter.log("Clicked on account menu");
		} catch (Exception e) {
			Reporter.log("Click on account menu failed");
			Assert.fail("Click on account menu failed");
		}
		return this;
	}

	/**
	 * Verify Account page.
	 *
	 * @return the AccountPage class instance.
	 */
	public UNAVCommonPage verifyAccountPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(accountPageUrl);
			Reporter.log("Account page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Account page not displayed");
		}
		return this;
	}

	/**
	 * Verify Phone Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyHeaderPhoneLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink3));
			Assert.assertTrue(navLink3.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_PHONE));
			Reporter.log(navLink3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Phone link
	 */
	public UNAVCommonPage clickHeaderPhoneLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink3));
			clickElement(navLink3);
			Reporter.log("Clicked on phone menu");
		} catch (Exception e) {
			Reporter.log("Click on phone menu failed");
			Assert.fail("Click on phone menu failed");
		}
		return this;
	}

	/**
	 * Verify Phone page.
	 *
	 * @return the Phone Page class instance.
	 */
	public UNAVCommonPage verifyHeaderPhonePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(phonePageUrl);
			Reporter.log("Phone page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Phone page not displayed");
		}
		return this;
	}

	/**
	 * Verify Shop Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyShopLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink4));
			Assert.assertTrue(navLink4.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_SHOP));
			Reporter.log(navLink4.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink4.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Shop link
	 */
	public UNAVCommonPage clickShopLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink4));
			clickElement(navLink4);
			Reporter.log("Clicked on shop menu");
		} catch (Exception e) {
			Reporter.log("Click on shop menu failed");
			Assert.fail("Click on shop menu failed");
		}
		return this;
	}

	/**
	 * Verify Shop page.
	 *
	 * @return the Shop Page class instance.
	 */
	public UNAVCommonPage verifyShopPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(shopPageUrl);
			Reporter.log("Shop page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Shop page not displayed");
		}
		return this;
	}

	/**
	 * Move to account menu and verify Manage my lines Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyManageMyLinesLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2));
			moveToElement(navLink2);
			Assert.assertTrue(isElementDisplayed(navItem_2_0));
			Assert.assertEquals(navItem_2_0.getText(), Constants.UNAV_MYTMO_HEADER_MANAGELINES);
			Reporter.log(navItem_2_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_2_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Move to account menu and verify Manage my lines link
	 */
	public UNAVCommonPage clickManageMyLineslink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2));
			moveToElement(navLink2);
			clickElement(navItem_2_0);
			Reporter.log("Clicked on Manage My Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Manage My Link Option failed");
			Assert.fail("Click on Manage My Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Manage my lines page.
	 *
	 * @return the Manage my lines Page class instance.
	 */
	public UNAVCommonPage verifyManageMyLinesPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(manageMyLinesPageUrl);
			Reporter.log("Manage My Lines Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Manage My Lines Page not displayed");
		}
		return this;
	}

	/**
	 * Move to account menu and verify Edit my Profile Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyEditProfileLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2));
			moveToElement(navLink2);
			Assert.assertTrue(isElementDisplayed(navItem_2_1));
			Assert.assertEquals(navItem_2_1.getText(), Constants.UNAV_MYTMO_HEADER_EDITPROFILE);
			Reporter.log(navItem_2_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_2_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Move to account menu and verify Edit my Profile link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickEditMyProfilelink() {
		try {
			checkPageIsReady();
			moveToElement(navLink2);
			clickElement(navItem_2_1);
			Reporter.log("Clicked on Edit my Profile Option");
		} catch (Exception e) {
			Reporter.log("Click on Edit my Profile Option failed");
			Assert.fail("Click on Edit my Profile Option failed");
		}
		return this;
	}

	/**
	 * Verify Edit my Profile page.
	 *
	 * @return the Edit my Profile Page class instance.
	 */
	public UNAVCommonPage verifyEditmyProfilePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(editMyProfilePageUrl);
			Reporter.log("Edit my Profile Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Edit my Profile Page not displayed");
		}
		return this;
	}

	/**
	 * Move to shop menu and verify Add a person or device to my account Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyAddDeviceLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink4));
			moveToElement(navLink4);
			Assert.assertTrue(isElementDisplayed(navItem_4_0));
			Assert.assertEquals(navItem_4_0.getText(), Constants.UNAV_MYTMO_HEADER_ADDDEVICE);
			Reporter.log(navItem_4_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_4_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Move to shop menu and verify Add a person or device to my account link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickAddDevicelink() {
		try {
			checkPageIsReady();
			moveToElement(navLink4);
			clickElement(navItem_4_0);
			Reporter.log("Clicked on Add a person or device to my account Option");
		} catch (Exception e) {
			Reporter.log("Click on Add a person or device to my account Option failed");
			Assert.fail("Click on Add a person or device to my account Option failed");
		}
		return this;
	}

	/**
	 * Verify Add a person or device link to my account page.
	 *
	 * @return the Add a person or device to my account Page class instance.
	 */
	public UNAVCommonPage verifyAddDevicePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(addDevicePageUrl);
			Reporter.log("Add a person or device to my account Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Add a person or device to my account Page not displayed");
		}
		return this;
	}

	/**
	 * Move to shop menu and verify See the latest deals Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifySeeLatestDealsLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink4));
			moveToElement(navLink4);
			Assert.assertTrue(isElementDisplayed(navItem_4_1));
			Assert.assertEquals(navItem_4_1.getText(), Constants.UNAV_MYTMO_HEADER_SEELATESTDEALS);
			Reporter.log(navItem_4_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_4_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Move to shop menu and verify See the latest deals link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickSeeLatestDealslink() {
		try {
			checkPageIsReady();
			moveToElement(navLink4);
			clickElement(navItem_4_1);
			Reporter.log("Clicked on See the latest deals Option");
		} catch (Exception e) {
			Reporter.log("Click on See the latest deals Option failed");
			Assert.fail("Click on See the latest deals Option failed");
		}
		return this;
	}

	/**
	 * Verify See the latest deals page.
	 *
	 * @return the See the latest deals Page class instance.
	 */
	public UNAVCommonPage verifyLatestDealsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(seeLatestDealsPageUrl);
			Reporter.log("See the latest deals Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("See the latest deals Page not displayed");
		}
		return this;
	}

	// Verify UNAV Utilities
	/**
	 * Verify Contact Us Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyHeaderContactUsLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(utility_0_Label));
			Assert.assertTrue(isElementDisplayed(utility_0_Label));
			// Assert.assertTrue(utility_0_Label.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_CONTACTUS));
			Reporter.log(utility_0_Label.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(utility_0_Label.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Contact Us link
	 */
	public UNAVCommonPage clickHeaderContactUslink() {
		try {
			clickElement(utility_0);
			Reporter.log("Clicked on Contact Us / Call Us Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Contact Us / Call Us Link Option failed");
			Assert.fail("Click on Contact Us / Call Us Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Contact Us page.
	 *
	 * @return the Contact Us Page class instance.
	 */
	public UNAVCommonPage verifyHeaderContactUsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_MYTMO_HEADER_CONTACTUS_LINK);
			Reporter.log("Contact Us Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Contact Us Page not displayed");
		}
		return this;
	}

	// Verify My Account
	/**
	 * Verify My Account Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyUtilityMyAccountLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(myAccount_desk));
			Assert.assertTrue(isElementDisplayed(myAccount_desk));
			// Assert.assertTrue(myAccount_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_MYACCOUNT));
			Reporter.log(myAccount_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(myAccount_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click My Account link
	 */
	public UNAVCommonPage clickUtilityMyAccountlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(myAccount_desk));
			clickElement(myAccount_desk);
			Reporter.log("Clicked on My Account Link Option");
		} catch (Exception e) {
			Reporter.log("Click on My Account Link Option failed");
			Assert.fail("Click on My Account Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Back to My Account Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyUtilityBacktoMyAccountLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(backToMyAccountLink));
			Assert.assertTrue(isElementDisplayed(backToMyAccountLink));
			Assert.assertTrue(backToMyAccountLink.getText().equalsIgnoreCase(Constants.UNAV_HEADER_BACKTOMYACCOUNT));
			Reporter.log(backToMyAccountLink.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(backToMyAccountLink.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click Back to My Account link
	 */
	public UNAVCommonPage clickUtilityBacktoMyAccountlink() {
		try {
			clickElement(backToMyAccountLink);
			Reporter.log("Clicked on Back to My Account Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Back to My Account Link Option failed");
			Assert.fail("Click on Back to My Account Link Option failed");
		}
		return this;
	}

	/**
	 * Verify MYTMO Home page.
	 *
	 * @return the Home Page class instance.
	 */
	public UNAVCommonPage verifyBackToHomePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_BACKTOMYACCOUNT_LINK);
			Reporter.log("MYTMO Home Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MYTMO Home Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Profile Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyProfileLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(profile_myAcc_desk));
			Assert.assertTrue(isElementDisplayed(profile_myAcc_desk));
			Assert.assertTrue(profile_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_PROFILE));
			Reporter.log(profile_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(profile_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Profile link
	 */
	public UNAVCommonPage clickProfilelink() {
		try {
			clickElement(profile_myAcc_desk);
			Reporter.log("Clicked on Profile Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Profile Link Option failed");
			Assert.fail("Click on Profile Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Profile page.
	 *
	 * @return the Profile Page class instance.
	 */
	public UNAVCommonPage verifyProfilePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_MYTMO_HEADER_PROFILE_LINK);
			Reporter.log("Profile Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Profile Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Account History Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyAccountHistoryLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(AccHistory_myAcc_desk));
			Assert.assertTrue(isElementDisplayed(AccHistory_myAcc_desk));
			Assert.assertTrue(
					AccHistory_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_ACCOUNTHISTORY));
			Reporter.log(AccHistory_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(AccHistory_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click Account History link
	 */
	public UNAVCommonPage clickAccountHistorylink() {
		try {
			clickElement(AccHistory_myAcc_desk);
			Reporter.log("Clicked on Account History Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Account History Link Option failed");
			Assert.fail("Click on AccountHistory Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Account History page.
	 *
	 * @return the Account History Page class instance.
	 */
	public UNAVCommonPage verifyAccountHistoryPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_MYTMO_HEADER_ACCOUNTHISTORY_LINK);
			Reporter.log("Account History Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Account History Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Logout Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyLogoutLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(logout_myAcc_desk));
			Assert.assertTrue(isElementDisplayed(logout_myAcc_desk));
			Assert.assertTrue(logout_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_HEADER_LOGOUT));
			Reporter.log(logout_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(logout_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click Logout link
	 */
	public UNAVCommonPage clickLogoutlink() {
		try {
			clickElement(logout_myAcc_desk);
			Reporter.log("Clicked on Logout Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Logout Link Option failed");
			Assert.fail("Click on Logout Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Signin page.
	 *
	 * @return the Signin Page class instance.
	 */
	public UNAVCommonPage verifySigninPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_MYTMO_HEADER_SIGNIN_LINK);
			Reporter.log("Signin Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Signin Page not displayed");
		}
		return this;
	}
	// Verify UNAV Footer - Top

	/**
	 * Verify Footer Title
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyFooterTitle() {
		try {
			Assert.assertTrue(isElementDisplayed(tmobile_title));
			Assert.assertTrue(tmobile_title.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_tmobile_title));
			Reporter.log(tmobile_title.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(tmobile_title.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify English Language
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyEnglishLanguage() {
		try {
			moveToElement(footer_link_top_0);
			Assert.assertTrue(isElementDisplayed(language_0));
			Assert.assertTrue(language_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_language_0));
			Reporter.log(language_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(language_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click English link
	 */
	public UNAVCommonPage clickEnglishlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(language_0));
			clickElementWithJavaScript(language_0);
			Reporter.log("Clicked on English Language Link");
		} catch (Exception e) {
			Verify.fail("Click on English link failed ");
		}
		return this;
	}

	/**
	 * Verify T-Mobile page.
	 *
	 * @return the AccountPage class instance.
	 */
	public UNAVCommonPage verifyTMobilePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_WIRELESS_LINK);
			Reporter.log("T-Mobile Home page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Home page not displayed");
		}
		return this;
	}

	/**
	 * Verify Spanish Language
	 * 
	 * @return
	 */
	public UNAVCommonPage verifySpanishLanguage() {
		try {
			moveToElement(footer_link_top_0);
			Assert.assertTrue(isElementDisplayed(language_1));
			Assert.assertTrue(language_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_language_1));
			Reporter.log(language_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(language_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Spanish link
	 */
	public UNAVCommonPage clickSpanishlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(language_1));
			clickElementWithJavaScript(language_1);
			Reporter.log("Clicked on Spanish Link");
		} catch (Exception e) {
			Verify.fail("Click on Spanish link failed ");
		}
		return this;
	}

	/**
	 * Verify Spanish T-Mobile page.
	 *
	 * @return the SpanishPage class instance.
	 */
	public UNAVCommonPage verifySpanishPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_language_1_WIRELESS_LINK);
			Reporter.log("T-Mobile Spanish page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Spanish page not displayed");
		}
		return this;
	}

	// Verify UNAV Footer - Social Links

	/**
	 * Click Instagram link
	 */
	public UNAVCommonPage clickInstagramlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(socialLink0));
			clickElementWithJavaScript(socialLink0);
			Reporter.log("Clicked on Instagram Link");
		} catch (Exception e) {
			Verify.fail("Click on Instagram link failed ");
		}
		return this;
	}

	/**
	 * Verify Instagram T-Mobile page.
	 *
	 * @return the InstagramPage class instance.
	 */
	public UNAVCommonPage verifyInstagramPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_Instagram_LINK);
			Reporter.log("T-Mobile Instagram page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Instagram page not displayed");
		}
		return this;
	}

	/**
	 * Click Facebook link
	 */
	public UNAVCommonPage clickFacebooklink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(socialLink1));
			clickElementWithJavaScript(socialLink1);
			Reporter.log("Clicked on Facebook Link");
		} catch (Exception e) {
			Verify.fail("Click on Facebook link failed ");
		}
		return this;
	}

	/**
	 * Verify Facebook T-Mobile page.
	 *
	 * @return the FacebookPage class instance.
	 */
	public UNAVCommonPage verifyFacebookPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_Facebook_LINK);
			Reporter.log("T-Mobile Facebook page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Facebook page not displayed");
		}
		return this;
	}

	/**
	 * Click Twitter link
	 */
	public UNAVCommonPage clickTwitterlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(socialLink2));
			clickElementWithJavaScript(socialLink2);
			Reporter.log("Clicked on Twitter Link");
		} catch (Exception e) {
			Verify.fail("Click on Twitter link failed ");
		}
		return this;
	}

	/**
	 * Verify Twitter T-Mobile page.
	 *
	 * @return the TwitterPage class instance.
	 */
	public UNAVCommonPage verifyTwitterPage() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				verifyPageUrl(Constants.UNAV_FOOTER_Twitter_MOBILE_LINK);
			} else {
				verifyPageUrl(Constants.UNAV_FOOTER_Twitter_LINK);
			}
			Reporter.log("T-Mobile Twitter page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Facebook page not displayed");
		}
		return this;
	}

	/**
	 * Click YouTube link
	 */
	public UNAVCommonPage clickYouTubelink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(socialLink3));
			clickElementWithJavaScript(socialLink3);
			Reporter.log("Clicked on YouTube Link");
		} catch (Exception e) {
			Verify.fail("Click on YouTube link failed ");
		}
		return this;
	}

	/**
	 * Verify YouTube T-Mobile page.
	 *
	 * @return the YouTubePage class instance.
	 */
	public UNAVCommonPage verifyYouTubePage() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				verifyPageUrl(Constants.UNAV_FOOTER_Youtube_MOBILE_LINK);
			} else {
				verifyPageUrl(Constants.UNAV_FOOTER_Youtube_LINK);
			}
			Reporter.log("T-Mobile YouTube page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile YouTube page not displayed");
		}
		return this;
	}
	// Verify UNAV Footer

	/**
	 * Verify Footer Support Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifySupportLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footerLabel_0));
			Assert.assertTrue(footerLabel_0.getText().equalsIgnoreCase(Constants.UNAV_HEADER_SUPPORT));
			Reporter.log(footerLabel_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footerLabel_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Support link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickSupportlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(footerLabel_0));
			clickElementWithJavaScript(footerLabel_0);
			Reporter.log("Clicked on Support Link");
		} catch (Exception e) {
			Reporter.log("Click on Support failed");
			Assert.fail("Click on Support failed");
		}
		return this;
	}

	/**
	 * Verify Support page.
	 *
	 * @return the Support Page class instance.
	 */
	public UNAVCommonPage verifySupportPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_ABOUTUS_SUPPORT_LINK);
			Reporter.log("Support Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Support Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Contact Us Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyContactUsLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footerLabel_1));
			Assert.assertTrue(footerLabel_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_CONTACTUS_LABEL));
			Reporter.log(footerLabel_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footerLabel_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Contact Us link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickContactUslink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(footerLabel_1));
			clickElementWithJavaScript(footerLabel_1);
			Reporter.log("Clicked on Contact Us Link");
		} catch (Exception e) {
			Reporter.log("Click on Contact Us failed");
			Assert.fail("Click on Contact Us failed");
		}
		return this;
	}

	/**
	 * Verify Contact Us page.
	 *
	 * @return the Contact Us Page class instance.
	 */
	public UNAVCommonPage verifyContactUsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_CONTACTINFO_LINK);
			Reporter.log("Contact Us Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Contact Us Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Store Locator Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyStoreLocatorLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footerLabel_2));
			Assert.assertTrue(footerLabel_2.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_FOOTER_STORELOCATOR));
			Reporter.log(footerLabel_2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footerLabel_2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Store Locator link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickStoreLocatorlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(footerLabel_2));
			clickElementWithJavaScript(footerLabel_2);
			Reporter.log("Clicked on Store Locator Link");
		} catch (Exception e) {
			Reporter.log("Click on Store Locator failed");
			Assert.fail("Click on Store Locator failed");
		}
		return this;
	}

	/**
	 * Verify Store Locator page.
	 *
	 * @return the Store Locator Page class instance.
	 */
	public UNAVCommonPage verifyStoreLocatorPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_FINDASTORE_LINK);
			Reporter.log("Store Locator Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Store Locator Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Coverage Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyCoverageLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footerLabel_3));
			Assert.assertTrue(footerLabel_3.getText().equalsIgnoreCase(Constants.UNAV_HEADER_COVERAGE));
			Reporter.log(footerLabel_3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footerLabel_3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Coverage link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickCoveragelink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(footerLabel_3));
			clickElementWithJavaScript(footerLabel_3);
			Reporter.log("Clicked on Coverage Link");
		} catch (Exception e) {
			Reporter.log("Click on Coverage failed");
			Assert.fail("Click on Coverage failed");
		}
		return this;
	}

	/**
	 * Verify Coverage page.
	 *
	 * @return the Coverage Page class instance.
	 */
	public UNAVCommonPage verifyCoveragePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_MYTMO_FOOTER_COVERAGE_LINK);
			Reporter.log("Coverage Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Coverage Page not displayed");
		}
		return this;
	}

	/**
	 * Verify T-Mobile.com Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyTMobileLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footerLabel_4));
			Assert.assertTrue(footerLabel_4.getText().equalsIgnoreCase(Constants.UNAV_MYTMO_FOOTER_TMOBILE));
			Reporter.log(footerLabel_4.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footerLabel_4.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click T-Mobile.com link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickTMobilelink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(footerLabel_4));
			clickElementWithJavaScript(footerLabel_4);
			Reporter.log("Clicked on T-Mobile.com Link");
		} catch (Exception e) {
			Reporter.log("Click on T-Mobile.com failed");
			Assert.fail("Click on T-Mobile.com failed");
		}
		return this;
	}

	// Verify UNAV Footer - Bottom

	/**
	 * Verify Footer ABOUT Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyAboutLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_top_0));
			Assert.assertTrue(footer_link_top_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_0));
			Reporter.log(footer_link_top_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_top_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our About link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickAboutlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(footer_link_top_0));
			clickElementWithJavaScript(footer_link_top_0);
			Reporter.log("Clicked on ABOUT Link");
		} catch (Exception e) {
			Reporter.log("Click on ABOUT failed");
			Assert.fail("Click on ABOUT failed");
		}
		return this;
	}

	/**
	 * Verify ABOUT page.
	 *
	 * @return the ABOUT Page class instance.
	 */
	public UNAVCommonPage verifyAboutPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_top_0_LINK);
			Reporter.log("ABOUTs Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("ABOUT Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer INVESTOR RELATIONS Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyFooterInvestorRelationsLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_top_1));
			Assert.assertTrue(footer_link_top_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_1));
			Reporter.log(footer_link_top_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_top_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our Investor Relations link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickFooterInvestorRelationslink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_top_1);
			Reporter.log("Clicked on Invester Relations Link");
		} catch (Exception e) {
			Reporter.log("Click on Invester Relations failed");
			Assert.fail("Click on Invester Relations failed");
		}
		return this;
	}

	/**
	 * Verify Investor Relations page.
	 *
	 * @return the Investor Relations Page class instance.
	 */
	public UNAVCommonPage verifyFooterInvestorRelationsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_top_1_LINK);
			Reporter.log("Investor Relations Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Investor Relations Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer PRESS Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyPressLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_top_2));
			Assert.assertTrue(footer_link_top_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_2));
			Reporter.log(footer_link_top_2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_top_2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our PRESS link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPresslink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_top_2);
			Reporter.log("Clicked on PRESS Link");
		} catch (Exception e) {
			Reporter.log("Click on PRESS failed");
			Assert.fail("Click on PRESS failed");
		}
		return this;
	}

	/**
	 * Verify PRESS page.
	 *
	 * @return the PRESS Page class instance.
	 */
	public UNAVCommonPage verifyPressPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_top_2_LINK);
			Reporter.log("PRESS Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("PRESS Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer CAREERS Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyFooterCareersLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_top_3));
			Assert.assertTrue(footer_link_top_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_3));
			Reporter.log(footer_link_top_3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_top_3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our CAREERS link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickFooterCareerslink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_top_3);
			Reporter.log("Clicked on CAREERS Link");
		} catch (Exception e) {
			Reporter.log("Click on CAREERS failed");
			Assert.fail("Click on CAREERS failed");
		}
		return this;
	}

	/**
	 * Verify CAREERS page.
	 *
	 * @return the CAREERS Page class instance.
	 */
	public UNAVCommonPage verifyFooterCareersPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_top_3_LINK);
			Reporter.log("CAREERS Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("CAREERS Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer DEUTSCHE TELEKOM Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyDeutscheTelekomLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_top_4));
			Assert.assertTrue(footer_link_top_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_4));
			Reporter.log(footer_link_top_4.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_top_4.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our DEUTSCHE TELEKOM link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickDeutscheTelekomlink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_top_4);
			Reporter.log("Clicked on DEUTSCHE TELEKOM Link");
		} catch (Exception e) {
			Reporter.log("Click on DEUTSCHE TELEKOM failed");
			Assert.fail("Click on DEUTSCHE TELEKOM failed");
		}
		return this;
	}

	/**
	 * Verify DEUTSCHE TELEKOM page.
	 *
	 * @return the DEUTSCHE TELEKOM Page class instance.
	 */
	public UNAVCommonPage verifyDeutscheTelekomPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_top_4_LINK);
			Reporter.log("DEUTSCHE TELEKOM Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("DEUTSCHE TELEKOM Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer PUERTO RICO Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyPuertoRicoLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_top_5));
			Assert.assertTrue(footer_link_top_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_5));
			Reporter.log(footer_link_top_5.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_top_5.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our PUERTO RICO link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPuertoRicolink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_top_5);
			Reporter.log("Clicked on PUERTO RICO Link");
		} catch (Exception e) {
			Reporter.log("Click on PUERTO RICO failed");
			Assert.fail("Click on PUERTO RICO failed");
		}
		return this;
	}

	/**
	 * Verify PUERTO RICO page.
	 *
	 * @return the PUERTO RICO Page class instance.
	 */
	public UNAVCommonPage verifyPuertoRicoPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_top_5_LINK);
			Reporter.log("PUERTO RICO Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("PUERTO RICO Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer PRIVACY POLICY Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyPrivacyPolicyLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_0));
			Assert.assertTrue(footer_link_bottom_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_0));
			Reporter.log(footer_link_bottom_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our PRIVACY POLICY link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPrivacyPolicylink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_0);
			Reporter.log("Clicked on PRIVACY POLICY Link");
		} catch (Exception e) {
			Reporter.log("Click on PRIVACY POLICY failed");
			Assert.fail("Click on PRIVACY POLICY failed");
		}
		return this;
	}

	/**
	 * Verify PUERTO RICO page.
	 *
	 * @return the PUERTO RICO Page class instance.
	 */
	public UNAVCommonPage verifyPrivacyPolicyPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_0_LINK);
			Reporter.log("PRIVACY POLICY Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("PRIVACY POLICY Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer INTEREST-BASED ADS Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyInterestBasedAdsLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_1));
			Assert.assertTrue(footer_link_bottom_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_1));
			Reporter.log(footer_link_bottom_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our INTEREST-BASED ADS link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickInterestBasedAdslink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_1);
			Reporter.log("Clicked on INTEREST-BASED ADS Link");
		} catch (Exception e) {
			Reporter.log("Click on INTEREST-BASED ADS failed");
			Assert.fail("Click on INTEREST-BASED ADS failed");
		}
		return this;
	}

	/**
	 * Verify INTEREST-BASED ADS page.
	 *
	 * @return the INTEREST-BASED ADS Page class instance.
	 */
	public UNAVCommonPage verifyInterestBasedAdsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_1_LINK);
			Reporter.log("INTEREST-BASED ADS Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("INTEREST-BASED ADS Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer PRIVACY CENTER Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyPrivacyLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_2));
			Assert.assertTrue(footer_link_bottom_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_2));
			Reporter.log(footer_link_bottom_2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our PRIVACY CENTER link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPrivacylink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_2);
			Reporter.log("Clicked on PRIVACY CENTER Link");
		} catch (Exception e) {
			Reporter.log("Click on PRIVACY CENTER failed");
			Assert.fail("Click on PRIVACY CENTER failed");
		}
		return this;
	}

	/**
	 * Verify PRIVACY CENTER page.
	 *
	 * @return the PRIVACY CENTER Page class instance.
	 */
	public UNAVCommonPage verifyPrivacyPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_2_LINK);
			Reporter.log("PRIVACY CENTER Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("PRIVACY CENTER Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer CONSUMER INFORMATION Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyConsumerLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_3));
			Assert.assertTrue(footer_link_bottom_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_3));
			Reporter.log(footer_link_bottom_3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our CONSUMER INFORMATION link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickConsumerlink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_3);
			Reporter.log("Clicked on CONSUMER INFORMATION Link");
		} catch (Exception e) {
			Reporter.log("Click on CONSUMER INFORMATION failed");
			Assert.fail("Click on CONSUMER INFORMATION failed");
		}
		return this;
	}

	/**
	 * Verify CONSUMER INFORMATION page.
	 *
	 * @return the CONSUMER INFORMATION Page class instance.
	 */
	public UNAVCommonPage verifyConsumerPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_3_LINK);
			Reporter.log("CONSUMER INFORMATION Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("CONSUMER INFORMATION Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer PUBLIC SAFETY/911 Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyPublicSafetyLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_4));
			Assert.assertTrue(footer_link_bottom_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_4));
			Reporter.log(footer_link_bottom_4.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_4.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our PUBLIC SAFETY/911 link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPublicSafetylink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_4);
			Reporter.log("Clicked on PUBLIC SAFETY/911 Link");
		} catch (Exception e) {
			Reporter.log("Click on PUBLIC SAFETY/911 failed");
			Assert.fail("Click on PUBLIC SAFETY/911 failed");
		}
		return this;
	}

	/**
	 * Verify PUBLIC SAFETY/911 page.
	 *
	 * @return the PUBLIC SAFETY/911 Page class instance.
	 */
	public UNAVCommonPage verifyPublicSafetyPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_4_LINK);
			Reporter.log("PUBLIC SAFETY/911 Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("PUBLIC SAFETY/911 Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer TERMS & CONDITIONS Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyTermsLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_5));
			Assert.assertTrue(footer_link_bottom_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_5));
			Reporter.log(footer_link_bottom_5.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_5.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our TERMS & CONDITIONS link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickTermslink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_5);
			Reporter.log("Clicked on TERMS & CONDITIONS Link");
		} catch (Exception e) {
			Reporter.log("Click on TERMS & CONDITIONS failed");
			Assert.fail("Click on TERMS & CONDITIONS failed");
		}
		return this;
	}

	/**
	 * Verify TERMS & CONDITIONS page.
	 *
	 * @return the TERMS & CONDITIONS Page class instance.
	 */
	public UNAVCommonPage verifyTermsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_5_LINK);
			Reporter.log("TERMS & CONDITIONS Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("TERMS & CONDITIONS Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer TERMS OF USE Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyTermsOfUseLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_6));
			Assert.assertTrue(footer_link_bottom_6.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_6));
			Reporter.log(footer_link_bottom_6.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_6.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our TERMS OF USE link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickTermsOfUselink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_6);
			Reporter.log("Clicked on TERMS OF USE Link");
		} catch (Exception e) {
			Reporter.log("Click on TERMS OF USE failed");
			Assert.fail("Click on TERMS OF USE failed");
		}
		return this;
	}

	/**
	 * Verify TERMS OF USE page.
	 *
	 * @return the TERMS OF USE Page class instance.
	 */
	public UNAVCommonPage verifyTermsOfUsePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_6_LINK);
			Reporter.log("TERMS OF USE Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("TERMS OF USE Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer ACCESSIBILITY Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyAccessibilityLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_7));
			Assert.assertTrue(footer_link_bottom_7.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_7));
			Reporter.log(footer_link_bottom_7.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_7.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our ACCESSIBILITY link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickAccessibilitylink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_7);
			Reporter.log("Clicked on ACCESSIBILITY Link");
		} catch (Exception e) {
			Reporter.log("Click on ACCESSIBILITY failed");
			Assert.fail("Click on ACCESSIBILITY failed");
		}
		return this;
	}

	/**
	 * Verify ACCESSIBILITY page.
	 *
	 * @return the ACCESSIBILITY Page class instance.
	 */
	public UNAVCommonPage verifyAccessibilityPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_7_LINK);
			Reporter.log("ACCESSIBILITY Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("ACCESSIBILITY Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer OPEN INTERNET Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyOpenInternetLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_8));
			Assert.assertTrue(footer_link_bottom_8.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_8));
			Reporter.log(footer_link_bottom_8.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_8.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our OPEN INTERNET link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickOpenInternetlink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_8);
			Reporter.log("Clicked on OPEN INTERNET Link");
		} catch (Exception e) {
			Reporter.log("Click on OPEN INTERNET failed");
			Assert.fail("Click on OPEN INTERNET failed");
		}
		return this;
	}

	/**
	 * Verify OPEN INTERNET page.
	 *
	 * @return the OPEN INTERNET Page class instance.
	 */
	public UNAVCommonPage verifyOpenInternetPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_8_LINK);
			Reporter.log("OPEN INTERNET Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("OPEN INTERNET Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Footer Do Not Sell Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyDoNotSellLabel() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_9));
			Assert.assertTrue(footer_link_bottom_9.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_9));
			Reporter.log(footer_link_bottom_9.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(footer_link_bottom_9.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Our Do Not Sell link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickDoNotSellLink() {
		try {
			checkPageIsReady();
			clickElement(footer_link_bottom_9);
			Reporter.log("Clicked on Do Not Sell Link");
		} catch (Exception e) {
			Reporter.log("Click on Do Not Sell failed");
			Assert.fail("Click on Do Not Sell failed");
		}
		return this;
	}

	/**
	 * Verify DoNotSell page.
	 *
	 * @return the DoNotSell Page class instance.
	 */
	public UNAVCommonPage verifyDoNotSellPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER2_bottom_9_LINK);
			Reporter.log("Do Not Sell Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Do Not Sell Page not displayed");
		}
		return this;
	}

	// Footer2 Social links
	/**
	 * Click Footer2 Instagram link
	 */
	public UNAVCommonPage clickFooter2Instagramlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(bottom_socailLink0));
			clickElementWithJavaScript(bottom_socailLink0);
			Reporter.log("Clicked on Instagram Link");
		} catch (Exception e) {
			Verify.fail("Click on Instagram link failed ");
		}
		return this;
	}

	/**
	 * Click Footer2 Facebook link
	 */
	public UNAVCommonPage clickFooter2Facebooklink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(bottom_socailLink1));
			clickElementWithJavaScript(bottom_socailLink1);
			Reporter.log("Clicked on footer2 Facebook Link");
		} catch (Exception e) {
			Verify.fail("Click on Footer2 Facebook link failed ");
		}
		return this;
	}

	/**
	 * Click Footer2 Twitter link
	 */
	public UNAVCommonPage clickFooter2Twitterlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(bottom_socailLink2));
			clickElementWithJavaScript(bottom_socailLink2);
			Reporter.log("Clicked on Footer 2 Twitter Link");
		} catch (Exception e) {
			Verify.fail("Click on Footer2 Twitter link failed ");
		}
		return this;
	}

	/**
	 * Click Footer2 YouTube link
	 */
	public UNAVCommonPage clickFooter2YouTubelink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(bottom_socailLink3));
			clickElementWithJavaScript(bottom_socailLink3);
			Reporter.log("Clicked on Footer2 YouTube Link");
		} catch (Exception e) {
			Verify.fail("Click on Footer2 YouTube link failed ");
		}
		return this;
	}

	/**
	 * Verify Footer2 Copy Right Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyCopyRightLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(footer_link_bottom_8));
			Assert.assertTrue(copyright.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_COPYRIGHT));
			Reporter.log(copyright.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(copyright.getText() + " is not dispalyed");
		}
		return this;
	}

	// Mobile Clicks on Home Header Page
	/**
	 * Click on Menu Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickMenuExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(header_menu);
				header_menu.click();
				Reporter.log("Clicked on Menu Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Menu Expnad Button failed");
			Assert.fail("Click on Menu Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Account Plus Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickAccountPlusExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(plusExpand_minusMinimize_mob_2);
				plusExpand_minusMinimize_mob_2.click();
				Reporter.log("Clicked on Account Plus Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Account Plus Expnad Button failed");
			Assert.fail("Click on Account Plus Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Phone Plus Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPhonePlusExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(plusExpand_minusMinimize_mob_3);
				plusExpand_minusMinimize_mob_3.click();
				Reporter.log("Clicked on Phone Plus Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Phone Plus Expnad Button failed");
			Assert.fail("Click on Phone Plus Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Shop Plus Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickShopPlusExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(plusExpand_minusMinimize_mob_4);
				plusExpand_minusMinimize_mob_4.click();
				Reporter.log("Clicked on Shop Plus Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Shop Plus Expnad Button failed");
			Assert.fail("Click on Shop Plus Expnad Button failed");
		}
		return this;
	}

	// Mobile Clicks on Home Footer Page
	/**
	 * Click on Phones and devices Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPhonesExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton0.click();
				Reporter.log("Clicked on Phone and devices Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Phone and devices Expnad Button failed");
			Assert.fail("Click on Phone and devices Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Apps And Connected devices Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickAppsExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton1.click();
				Reporter.log("Clicked on Apps And Connected devices Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Apps And Connected devices Expnad Button failed");
			Assert.fail("Click on  Apps And Connected devices Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Plans and information Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickPlansExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton2.click();
				Reporter.log("Clicked on Plans and information Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Plans  and information Expnad Button failed");
			Assert.fail("Click on Plans  and information Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Switch to T-Mobile Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickSwitchExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton3.click();
				Reporter.log("Clicked on Switch to T-Mobile Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Switch to T-Mobile Expnad Button failed");
			Assert.fail("Click on Switch to T-Mobile Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on T-Mobile Benefits Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickBenefitsExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton4.click();
				Reporter.log("Clicked on T-Mobile Benefits Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on T-Mobile Benefits Expnad Button failed");
			Assert.fail("Click on T-Mobile Benefits Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Order Info Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickOrderInfoExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton5.click();
				Reporter.log("Clicked on Order Info Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Order Info Expnad Button failed");
			Assert.fail("Click on Order Info Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Support Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickSupportExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton6.click();
				Reporter.log("Clicked on Support Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Support Expnad Button failed");
			Assert.fail("Click on Support Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on My Account Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickMyAccountExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton7.click();
				Reporter.log("Clicked on  My Account Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on  My Account Expnad Button failed");
			Assert.fail("Click on  My Account Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on More Than Wireless Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickMoreThanWirelessExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton8.click();
				Reporter.log("Clicked on  More Than Wireless Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on  More Than Wireless Expnad Button failed");
			Assert.fail("Click on  More Than Wireless Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on About T-Mobile Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickAboutTMobileExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton9.click();
				Reporter.log("Clicked on About T-Mobile Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on About T-Mobile Expnad Button failed");
			Assert.fail("Click on About T-Mobile Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Corporate Responsibility Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickCorporateResponsibilityExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton10.click();
				Reporter.log("Clicked on Corporate Responsibility Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Corporate Responsibility Expnad Button failed");
			Assert.fail("Click on Corporate Responsibility Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Careers Expand link
	 * 
	 * @return
	 */
	public UNAVCommonPage clickCareersExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton11.click();
				Reporter.log("Clicked on Careers Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Careers Expnad Button failed");
			Assert.fail("Click on Careers Expnad Button failed");
		}
		return this;
	}

	/**
	 * Verify Search Button
	 *
	 */
	public UNAVCommonPage verifySearchButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchButton));
			Assert.assertTrue(searchButton.isDisplayed());
			Reporter.log("Search button displayed successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display Search button");
		}
		return this;
	}

	/**
	 * Click Search Button
	 *
	 */
	public UNAVCommonPage clickSearchButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchButton));
			searchButton.click();
			Reporter.log("Clicked on Search button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Search button");
		}
		return this;
	}

	/**
	 * Verify Search input field
	 *
	 */
	public UNAVCommonPage verifySearchInputField() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchTextBox));
			Assert.assertTrue(searchTextBox.isDisplayed());
			Reporter.log("Search input field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Search input field");
		}
		return this;
	}

	/**
	 * Verify Search Hint text
	 *
	 */
	public UNAVCommonPage verifySearchHintText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchHintText));
			Assert.assertTrue(searchHintText.isDisplayed());
			Reporter.log("Search hint text is displayed in the text box");
		} catch (Exception e) {
			Assert.fail("Failed to display Search hint text in the text box");
		}
		return this;
	}

	/**
	 * Verify type ahead functionality
	 *
	 */
	public UNAVCommonPage verifyTypeAheadSearch(String deviceName) {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchTextBox1));
			sendTextData(searchTextBox1, deviceName);
			checkPageIsReady();
			Assert.assertTrue(typeAheadSearchResults.size() > 0);
			Reporter.log("type ahead search functionality giving suggestion");
		} catch (Exception e) {
			Assert.fail("Failed to validate type ahead search");
		}
		return this;
	}

	/**
	 * Verify Search drop down
	 *
	 */
	public UNAVCommonPage verifySearchDropDown() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchDropDown));
			Assert.assertTrue(searchDropDown.isDisplayed());
			Reporter.log("Drop down is displayed upon clicking on Search");
		} catch (Exception e) {
			Assert.fail("Failed to display Search drop down");
		}
		return this;
	}

	/**
	 * Click Search close button
	 *
	 */
	public UNAVCommonPage clickSearchCloseButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchCloseButton));
			searchCloseButton.click();
			Reporter.log("Clicked on Search close button successfully");
		} catch (Exception e) {
			Assert.fail("Failed to click on Search close button");
		}
		return this;
	}

	/**
	 * Click Search icon
	 *
	 */
	public UNAVCommonPage clickSearchIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchIcon));
			searchIcon1.click();
			Reporter.log("Clicked on Search icon");
		} catch (Exception e) {
			Assert.fail("Failed to click on Search icon");
		}
		return this;
	}

	/**
	 * Verify Quick Links Headline
	 *
	 */
	public UNAVCommonPage verifyQuickLinksHeadline() {
		try {
			waitFor(ExpectedConditions.visibilityOf(quickLinksHeadline));
			Assert.assertTrue(quickLinksHeadline.isDisplayed());
			Reporter.log("Quick links headline is displayed in the drop down");
		} catch (Exception e) {
			Assert.fail("Failed to display Quick links headline in the drop down");
		}
		return this;
	}

	/**
	 * Verify Quick Links In Drop Down
	 *
	 */
	public UNAVCommonPage verifyQuickLinksInDropDown() {
		try {
			for (WebElement qlinks : quickLinks) {
				waitFor(ExpectedConditions.visibilityOf(qlinks));
				Assert.assertTrue(qlinks.isDisplayed());
			}
			Reporter.log("Quick links are displayed in the drop down");
		} catch (Exception e) {
			Assert.fail("Failed to display Quick links in the drop down");
		}
		return this;
	}

	/**
	 * Click on Quick link
	 *
	 */
	public UNAVCommonPage clickQuickLink() {
		try {
			quickLinks.get(0).click();
			Reporter.log("Quick link is clickable");
		} catch (Exception e) {
			Assert.fail("Failed to click on Quick link");
		}
		return this;
	}

	/**
	 * Verify Top Searches Headline
	 *
	 */
	public UNAVCommonPage verifyTopSearchesHeadline() {
		try {
			waitFor(ExpectedConditions.visibilityOf(topSearchesHeadline));
			Assert.assertTrue(topSearchesHeadline.isDisplayed());
			Reporter.log("Top searches headline is displayed in the drop down");
		} catch (Exception e) {
			Assert.fail("Failed to display Top searches headline in the drop down");
		}
		return this;
	}

	/**
	 * Verify Top Searches In Drop Down
	 *
	 */
	public UNAVCommonPage verifyTopSearchesInDropDown() {
		try {
			for (WebElement topsearch : topSearches) {
				waitFor(ExpectedConditions.visibilityOf(topsearch));
				Assert.assertTrue(topsearch.isDisplayed());
			}
			Reporter.log("Top searches are displayed in the drop down");
		} catch (Exception e) {
			Assert.fail("Failed to display Top searches in the drop down");
		}
		return this;
	}

	/**
	 * Click on Top Search
	 *
	 */
	public UNAVCommonPage clickOnTopSearch() {
		try {
			topSearches.get(0).click();
			Reporter.log("Top searches is clickable");
		} catch (Exception e) {
			Assert.fail("Failed to click on Top search");
		}
		return this;
	}

	/**
	 * Verify Search Icon Beside Top Search
	 *
	 */
	public UNAVCommonPage verifySearchIconBesideTopSearch() {
		try {
			for (WebElement searchicon : searchIconBesideTopSearch) {
				waitFor(ExpectedConditions.visibilityOf(searchicon));
				Assert.assertTrue(searchicon.isDisplayed());
			}
			Reporter.log("Search icon is displayed beside each top search");
		} catch (Exception e) {
			Assert.fail("Failed to display Search icon beside each top search");
		}
		return this;
	}

	/**
	 * Verify Search Button
	 *
	 */
	public UNAVCommonPage verifyMyAccountDropDown() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchButton));
			Assert.assertTrue(searchButton.isDisplayed());
			Reporter.log("Search button displayed successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display Search button");
		}
		return this;
	}

	/**
	 * Verify My Account Label
	 * 
	 * @return
	 */
	public UNAVCommonPage verifyBackToMyAccountLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(backToMyAccountLink));
			Assert.assertTrue(isElementDisplayed(backToMyAccountLink));
			Reporter.log("Back to my account link displayed");
		} catch (Exception e) {
			Assert.fail("Back to my account link not displayed");
		}
		return this;
	}

	/**
	 * Verify Selected Menu is highlighted
	 * 
	 * @return
	 */
	public UNAVCommonPage verifySelectedMenuisHighlighted(String selectedMenu) {
		try {
			Assert.assertTrue(
					selectedMenuOnUNAVMytmo.getAttribute("data-analytics-value").equalsIgnoreCase(selectedMenu),
					"Selected Menu is not highlighted:" + selectedMenu);
			Reporter.log("Selected menu item is highlighted in UNAV: " + selectedMenu);
		} catch (Exception e) {
			Assert.fail("Failed to verify Selected Menu is highlighted");
		}
		return this;
	}

}