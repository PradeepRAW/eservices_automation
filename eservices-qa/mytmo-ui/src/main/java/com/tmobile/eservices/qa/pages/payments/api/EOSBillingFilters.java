package com.tmobile.eservices.qa.pages.payments.api;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSBillingFilters extends EOSCommonLib {

	public Response getResponsedataset(String alist[]) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("userRole", alist[4]);
			restService.addHeader("ban", alist[3]);
			restService.addHeader("msisdn", alist[0]);

			restService.addHeader("usn", "usn_100");
			restService.addHeader("unBilledCycleStartDate", "2017-10-01");
			restService.addHeader("Authorization", "Bearer " + alist[2]);
			restService.addHeader("access_token", "Bearer " + alist[1]);

			response = restService.callService("v1/billing/dataset", RestCallType.GET);

		}

		return response;
	}

	public String geteipdetails(Response response) {
		String eipdetails = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("eipDetailsSection.isEIPDetailsExists") != null) {
				return jsonPathEvaluator.get("eipDetailsSection.isEIPDetailsExists").toString();
			}
		}

		return eipdetails;
	}

	public String getjoddetails(Response response) {
		String joddetails = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("jodDetailsSection.isJumpLeaseDetailsExists") != null) {
				return jsonPathEvaluator.get("jodDetailsSection.isJumpLeaseDetailsExists").toString();
			}
		}

		return joddetails;
	}

	public String getpaeligibility(Response response) {
		String paeligibility = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("paymentArrangementDetails.paymentArrangementIndicator") != null) {
				return jsonPathEvaluator.get("paymentArrangementDetails.paymentArrangementIndicator").toString();
			}
		}

		return paeligibility;
	}

	public String getautopaystatus(Response response) {
		String autopay = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("autoPay.easyPayStatus") != null) {
				return jsonPathEvaluator.get("autoPay.easyPayStatus").toString();
			}
		}

		return autopay;
	}

	public String getdueDate(Response response) {
		String autopay = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("autoPay.dueDate") != null) {
				return jsonPathEvaluator.get("autoPay.dueDate").toString();
			}
		}

		return autopay;
	}

	public String getUserNickname(Response response) {
		String userinfo = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("loggedInUserInfo.nickName") != null) {
				return jsonPathEvaluator.get("loggedInUserInfo.nickName").toString();
			}
		}

		return userinfo;
	}

	public String getIspastdueAmount(Response response) {
		String pastdue = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("pastdueAmountDetails.pastdueAmount") != null) {

				float amt = Float.parseFloat(jsonPathEvaluator.get("pastdueAmountDetails.pastdueAmount").toString());
				if (amt > 0) {
					return "true";
				} else {
					return "false";
				}

			}
		}

		return pastdue;
	}

	public String getARbalance(Response response) {
		String arbalance = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("arBalance.balanceDue") != null) {

				float amt = Float.parseFloat(jsonPathEvaluator.get("arBalance.balanceDue").toString());
				if (amt > 0) {
					return "true";
				} else {
					return "false";
				}

			}
		}

		return arbalance;
	}

	public String getduedatetype(String duedate) {
		LocalDate dued = LocalDate.parse(duedate);
		LocalDate ldObj = LocalDate.now();
		Period period = Period.between(ldObj, dued);
		int diff = period.getDays();
		if (diff > 2)
			return "outsideblackout";
		else if (diff < 0)
			return "pastbillduedate";
		else if (diff == 0)
			return "duedate";
		else
			return "insideblackout";
	}

//pastdueAmountDetails.pastdueAmount

	/***********************************************************************************
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 ***********************************************************************************/

	public Response getResponsebillList(String alist[]) throws Exception {
		Response response = null;

		if (alist != null) {
			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("Authorization", "Bearer " + alist[2]);
			restService.addHeader("ban", alist[3]);
			response = restService.callService("v1/billing/cycles?ban=" + alist[3], RestCallType.GET);

		}

		return response;
	}

	public String getdocumentid(Response response) {
		String billtype = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusMessage") != null) {
				if (jsonPathEvaluator.get("statusMessage").toString().equalsIgnoreCase("Bill list found")) {

					if (jsonPathEvaluator.get("BillList[0].documentId") != null) {
						return jsonPathEvaluator.get("BillList[0].documentId").toString();

					}

				}

			}

		}

		return billtype;
	}

	public String getbilltype(Response response) {
		String billtype = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusMessage") != null) {
				if (jsonPathEvaluator.get("statusMessage").toString().equalsIgnoreCase("Bill list found")) {

					if (jsonPathEvaluator.get("BillList[0].documentId") != null) {
						String doc = jsonPathEvaluator.get("BillList[0].documentId").toString();
						if (doc.trim().length() > 0) {
							return "bbill";
						} else {
							return "ebill";
						}
					}

				}
				return "ebill";
			}

		}

		return billtype;
	}

	public String getebillstatemenid(Response response) {
		String billtype = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusMessage") != null) {
				if (jsonPathEvaluator.get("statusMessage").toString().equalsIgnoreCase("Bill list found")) {

					if (jsonPathEvaluator.get("BillList[0].documentId") != null) {

						List<String> arrdocids = jsonPathEvaluator.get("BillList.documentId");

						boolean ismatch = false;
						int index = 0;

						for (int i = 0; i < arrdocids.size(); i++) {
							if (arrdocids.get(i) == null) {
								index = i;
								ismatch = true;
								break;
							} else if (arrdocids.get(i).trim().equalsIgnoreCase("")) {
								index = i;
								ismatch = true;
								break;
							}
						}

						if (ismatch)
							return jsonPathEvaluator.get("BillList[" + index + "].statementId").toString();

					}

				}

			}
		}

		return billtype;
	}

	public String getebillstatementmonth(Response response) {
		String billtype = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusMessage") != null) {
				if (jsonPathEvaluator.get("statusMessage").toString().equalsIgnoreCase("Bill list found")) {

					if (jsonPathEvaluator.get("BillList[0].documentId") != null) {

						List<String> arrdocids = jsonPathEvaluator.get("BillList.documentId");

						boolean ismatch = false;
						int index = 0;

						for (int i = 0; i < arrdocids.size(); i++) {
							if (arrdocids.get(i) == null) {
								index = i;
								ismatch = true;
								break;
							} else if (arrdocids.get(i).trim().equalsIgnoreCase("")) {
								index = i;
								ismatch = true;
								break;
							}
						}

						if (ismatch)
							return jsonPathEvaluator.get("BillList[" + index + "].endDate").toString();

					}

				}

			}
		}

		return billtype;
	}

	public String checkpreviousbills(Response response) {
		String billtype = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("statusMessage") != null) {
				if (jsonPathEvaluator.get("statusMessage").toString().equalsIgnoreCase("Bill list found")) {

					if (jsonPathEvaluator.get("BillList.size()") != null) {
						if (Integer.parseInt(jsonPathEvaluator.get("BillList.size()").toString()) > 2) {
							return "true";
						} else {
							return "false";
						}

					}

				}
				return billtype;
			}

		}

		return billtype;
	}

	/***********************************************************************************
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 ***********************************************************************************/

	public Response getResponseprintsummaryBill(String alist[], String Documentid) throws Exception {
		Response response = null;

		if (alist != null) {
			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", bbenv.get(environment));
			// restService.setContentType("application/json");
			restService.addHeader("Accept", "application/json");
			restService.addHeader("ban", alist[3]);
			restService.addHeader("puid", "test@britebill.com");
			response = restService.callService("v21/tmo/documents/" + Documentid + "?mode=full", RestCallType.GET);

		}

		return response;
	}

	/***********************************************************************************
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 ***********************************************************************************/

	public Response getResponseprintdetailedBill(String alist[], String Documentid) throws Exception {
		Response response = null;

		if (alist != null) {
			// RestService restService = new RestService("",
			// "https://stage2.eos.corporate.t-mobile.com");
			RestService restService = new RestService("", bbenv.get(environment));
			restService.addHeader("Accept", "application/json");
			// restService.setContentType("application/json");
			restService.addHeader("ban", alist[3]);
			restService.addHeader("puid", "test@britebill.com");
			response = restService.callService("v21/tmo/documents/" + Documentid + "?mode=detailed", RestCallType.GET);

		}

		return response;
	}

}
