package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class EOSPaymentsV4 extends ApiCommonLib {

	/***
	 * Summary: This API is used do the payments through OrchestrateOrders API
	 * Headers:Authorization, -Content-Type, -Postman-Token, -cache-control,
	 * -interactionId -workflowId
	 * 
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */

	private Map<String, String> buildWalletApiHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("ban", apiTestData.getBan());
		headers.put("tmo-id", apiTestData.gettmoId());
		headers.put("Authorization", getAccessToken());
		return headers;
	}

	public Response verifyProcessEIPPayment(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("application_id", "STREAMLINE");
		headers.put("interactionId", "123");
		headers.put("activityId", "123456");
		headers.put("cache-control", "no-cache");
		headers.put("channel_id", "WEB");
		headers.put("msisdn", apiTestData.getMsisdn());
		
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("/payments/v4/eip", RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}

	public Response processNonSedonaOTP(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("application_id", "ESERVICE");
		headers.put("cache-control", "no-cache");
		headers.put("channel", "WEB");
		headers.put("channel_id", "WEB");
		headers.put("interactionid", "Eservice");
		headers.put("msisdn", apiTestData.getMsisdn());
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("/payments/v4/otp", RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}

	public Response processPaymentPost(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("application_id", "UUI");// MYTMO
		headers.put("activityid", "234234234");
		headers.put("channel_id", "CARE");
		headers.put("interactionId", "1231");
		headers.put("userid", "234234");
		headers.put("tmoid", "U-2c0cf5cd-8250-4fd8-a165-2e068d659413");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("/processPaymentPost", RestCallType.PUT);
		System.out.println(response.getStatusCode());
		return response;

	}

	public Response processSedonaOTP(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);

		headers.put("channel_id", "RETAIL");
		headers.put("accountNumber", apiTestData.getBan());
		headers.put("application_id", "123");
		headers.put("channelVersion", "123");
		headers.put("channel", "WEB");
		headers.put("client_id", "123");
		headers.put("interactionId", "123456787");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("walletCapacity", "10");
		headers.put("isLimitedWalletCapacity", "false");
		headers.put("usn", "123");
		headers.put("workflowId", "walletsearch");

		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("/processSedonaOTP", RestCallType.GET);
		System.out.println(response.asString());
		return response;

	}

}
