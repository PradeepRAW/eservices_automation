package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class RefillPage extends CommonPage {

	@FindBy(css = ".display4:not(.text-left)")
	private WebElement refillAccountHeader;
	
	private static final String pageUrl = "refill";

	public RefillPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Refill page.
	 */
	public RefillPage verifyRefillPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Refill page displayed");
		} catch (AssertionError e) {
			Reporter.log("Refill page not displayed");
			Assert.fail("Refill page not displayed");
		}
		return this;
	}
}
