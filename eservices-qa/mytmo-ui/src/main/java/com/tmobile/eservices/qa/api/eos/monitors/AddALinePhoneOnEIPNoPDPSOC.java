package com.tmobile.eservices.qa.api.eos.monitors;

import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AddALinePhoneOnEIPNoPDPSOC extends OrderProcess {

	/*
	 * 
	 * Steps For aalPhoneOnEIPNoPDPSOC Flow, 1. getCatalogProducts 2. Create
	 * Cart 3. invokeGetEligiblePlans 4. Update Cart 5. Create Quote 6. Update
	 * Quote 7. Make Payment 8. Place Order
	 * 
	 */

	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException{
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "createCartAAL_BYOD_EIP.txt");
			invokeCreateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void getEligiblePlans(ApiTestData apiTestData) throws ServiceFailureException {
		try {

			Response response = serviceApi.getServicesForPlan(apiTestData, "", serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "getServicesForPlan");
			if (isSuccessResponse) {				
				if (isResponseEmpty(response)) {
					addExceptionMessagesToMapAndLog(response, "Empty Response from getServicesForPlan Service ");
					throwServiceFailException();
				}
				logLargeResponse(response, "getServicesForPlan");
				getTokenMapForGetEligiblePlans(response);
			}
		} catch (Exception e) {
			Reporter.log("Service issue with getServicesForPlan : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void updateCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Plans.txt");
			invokeUpdateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue during Update Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void updateQuote(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "updateQuoteEIP_AALMonitors.txt");
			invokeUpdateQuote(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while invoking the Update Quote : " + e);
			throwServiceFailException();
		}

	}

	private void getTokenMapForGetEligiblePlans(Response response) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			if (!jsonNode.isMissingNode() && jsonNode.has("planInfo")) {
				JsonNode planInfoNode = jsonNode.path("planInfo");
				if (!planInfoNode.isMissingNode() && planInfoNode.isArray()) {
					serviceResultsMap.put("planFamilyId", getPathVal(planInfoNode.get(0), "planFamilyId"));
					serviceResultsMap.put("ratePlanSoc", getPathVal(planInfoNode.get(0), "ratePlanSoc"));
				}
			}
		}
	}

}
