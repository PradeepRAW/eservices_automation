package com.tmobile.eservices.qa.pages.shop;

import java.text.DecimalFormat;
import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 *
 */
public class AccessoriesStandAlonePDPPage extends CommonPage {

	private static final String pageUrl = "accessoriesdetails";

	@FindBy(css = "button[ng-if='!$ctrl.accessoryDetails.isOutOfStock']")
	private WebElement addToCartButton;

	@FindBy(css = ".plan-price-header-total")
	private WebElement eipDownPayment;

	@FindBy(css = ".eipprice div.EIPPayment.ng-binding")
	private WebElement eipMonthlyPaymentInPDP;

	@FindBy(css = ".eipprice .EIPInstallment.ng-binding")
	private WebElement eipLoanTermInPDP;

	@FindBy(css = ".summary-price.ng-binding.ng-scope")
	private WebElement accessoryFRPPrice;

	@FindBy(css = "div.rndtxt span.summary-price.text-magenta")
	private WebElement eipDownPaymentWith$;

	@FindBy(css = "div.ui_primary_link a")
	private WebElement backToListText;

	@FindBy(css = ".summary-price.ng-binding.ng-scope .super-decimal-price.ng-binding")
	private WebElement accessoryFRPPriceCents;

	@FindBy(css = ".eipprice div.EIPPayment.ng-binding .super-decimal-price.ng-binding")
	private WebElement eipMonthlyPaymentInPDPCents;

	@FindBy(css = "#eipPriceDetail .totalprice del")
	private WebElement accessoryFRPStrikedPrice;

	@FindBy(css = "#eipPriceDetail .totalprice del .super-decimal-price.ng-binding")
	private WebElement accessoryFRPStrikedPriceCents;

	@FindBy(css = "#one :nth-child(5) p")
	private WebElement catalogPromoPrice;

	@FindBy(css = "a[ng-bind-html*='ctrl.PageContent.acceCaliforniaProposition65LegalLink']")
	private WebElement californiaResidentswarning;

	@FindBy(css = "h2.h2-title-warning.text-bold.pull-right-md.ng-binding")
	private WebElement californiaPropositionwarningModalHeader;

	@FindBy(css = "div.body-copy-description-regular.p-l-5")
	private WebElement californiaPropositionwarningModalMessage;

	@FindBy(css = "	button[ng-click*='close($event)']")
	private WebElement closeCaliforniaPopUp;

	@FindBy(css = "[style*='margin-bottom']")
	private WebElement starImageForAccessory;

	@FindBy(css = "a[ng-attr-id*='positive']")
	private List<WebElement> positiveReviewCountAccessory;

	@FindBy(css = "a[ng-attr-id*='negative']")
	private List<WebElement> negativeReviewCountAccessory;

	@FindBy(css = "div.review-name.text-bold.ng-binding")
	private WebElement reviewTabBelowForAccessory;

	@FindBy(css = "a.review-tabs.ng-binding")
	private List<WebElement> reviewTabFilterOptions;

	@FindBy(css = "[ng-click*='ctrl.indextab']")
	private WebElement reviewCountAccessory;

	@FindBy(css = "[title*='FEATURES']")
	private WebElement featuresTab;

	@FindBy(css = "div.flexbox.specifications")
	private WebElement specificationsSection;

	@FindBy(css = "[title*='RELATED']")
	private WebElement relatedTab;

	@FindBy(css = "section#related .carouselsection")
	private WebElement relatedDevicesSection;

	@FindBy(css = "h2[ng-if*='relatedAccessories']")
	private WebElement relatedAccesoriesHeader;

	@FindBy(xpath = "//div[contains(@class,'add-to-cart') and contains(@ng-if,'!item.isOutOfStock')]/a")
	private List<WebElement> relatedAccessories;

	@FindBy(xpath = "//div[contains(@class,'add-to-cart') and contains(@ng-if,'!item.isOutOfStock')]/a/ancestor::div[@ng-repeat='item in items']/div/a/div")
	private List<WebElement> relatedAccessoriesName;
	
	@FindBy(css = "a[data-slide='next']")
	private WebElement mobileSlideCTA;
	
	
	public AccessoriesStandAlonePDPPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify Verify Accessory ProductDetails Page.
	 *
	 * @return the ShopPage class instance.
	 */
	public AccessoriesStandAlonePDPPage verifyAccessoriesStandAlonePDPPage() {
		waitforSpinner();
		try {
			verifyPageUrl();
			Reporter.log("Accessories stand alone PDP page is displayed");
			verifyBackTolistText();
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Accessories stand alone PDP page");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AccessoriesStandAlonePDPPage verifyPageUrl() {
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains(pageUrl), "Accessories PDP page is not displayed");
		} catch (Exception e) {
			Reporter.log("Accessories PDP page is not displayed");
		}
		return this;
	}

	/**
	 * Click AddToCart Button
	 */
	public AccessoriesStandAlonePDPPage clickAddToCartButton() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(addToCartButton);
			Reporter.log("Add to Cart Button Clicked on Accessory PDP");
		} catch (NoSuchElementException e) {
			Assert.fail("AddToCartButton is not Displayed to Click");
		}
		return this;
	}

	/**
	 * Verify that EIP monthly payment is displayed on PDP
	 */
	public AccessoriesStandAlonePDPPage verifyMonthlyPayment() {
		try {
			waitforSpinner();
			Assert.assertTrue(eipMonthlyPaymentInPDP.isDisplayed(), "EIP monthly Payment is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Monthly Payment");
		}
		return this;
	}

	/**
	 * Verify that EIP Loan Term is displayed on PDP
	 */
	public AccessoriesStandAlonePDPPage verifyLoanTerm() {
		try {
			waitforSpinner();
			Assert.assertTrue(eipLoanTermInPDP.isDisplayed(), "EIP loan term is not displayed in PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Loan Term");
		}
		return this;
	}

	/**
	 * Verify that EIP section is displayed
	 */
	public AccessoriesStandAlonePDPPage verifyEIPDownPayment() {
		try {
			Assert.assertTrue(eipDownPayment.isDisplayed(), "EIP downpayment is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP downpayment");
		}
		return this;
	}

	/**
	 * Verify FRP Display on PDP
	 */
	public AccessoriesStandAlonePDPPage verifyFRPinPDP() {
		waitforSpinner();
		try {
			Assert.assertTrue(accessoryFRPPrice.isDisplayed(), "FRP is not displayed in PDP");
			Reporter.log("FRP is displayed in PDP page");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP on PDP page");
		}
		return this;
	}

	/**
	 * Verify FRP PRice
	 * 
	 * @return
	 */

	public Double getAccessoryFRPPRiceInPDP() {
		String frpPrice;
		frpPrice = accessoryFRPPrice.getText();
		frpPrice = frpPrice.substring(1, frpPrice.length() - 2) + "." + frpPrice.substring(frpPrice.length() - 2);
		return Double.parseDouble(frpPrice);

	}

	/**
	 * Verify that Loan Term Length is only 9, 12, 24 or 36
	 */
	public AccessoriesStandAlonePDPPage verifyLoanTermLengthInPDP() {
		try {
			waitforSpinner();
			Assert.assertTrue(eipLoanTermInPDP.getText().contains("mos"), "EIP Loan Term is not displayed correctly");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Loan Term length");
		}
		return this;
	}

	/**
	 * Verify that FRP price from PLP page is equal to FRP from PDP
	 */
	public AccessoriesStandAlonePDPPage compareFRPPriceIsSameForPLPAndPDP(Double firstValue, Double secondValue) {
		try {

			Assert.assertEquals(firstValue, secondValue, "PDP FRP price is not matched with PLP FRP price");
			Reporter.log("PDP FRP price is matched with PLP FRP price");
		} catch (Exception e) {
			Assert.fail("Failed to compare PDP FRP price is not matched with PLP FRP price");
		}
		return this;
	}

	/**
	 * Verify that down payment from PLP page is equal to down payment from PDP
	 */
	public AccessoriesStandAlonePDPPage compareDownPaymentIsSameForPLPAndPDP(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "PDP Down payment is not matched with PLP Down payment price");
			Reporter.log("PDP Down payment is matched with PLP Down payment");
		} catch (Exception e) {
			Assert.fail("Failed to compare PDP Down payment is not matched with PLP Down payment");
		}
		return this;
	}

	/**
	 * Verify that loan term from PLP page is equal to loan term from PDP
	 */
	public AccessoriesStandAlonePDPPage compareLoanTermIsSameForPLPAndPDP(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "PDP loan term is not matched with PLP loan term price");
			Reporter.log("PDP loan term is matched with PLP loan term");
		} catch (Exception e) {
			Assert.fail("Failed to compare PDP loan term is not matched with PLP loan term");
		}
		return this;
	}

	/**
	 * Verify that monthly payment from PLP page is equal to monthly payment from
	 * PDP
	 */
	public AccessoriesStandAlonePDPPage compareMonthlyPaymentIsSameForPLPAndPDP(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue,
					"PDP monthly payment is not matched with PLP monthly payment price");
			Reporter.log("PDP monthly payment is matched with PLP monthly payment");
		} catch (Exception e) {
			Assert.fail("Failed to compare PDP monthly payment is not matched with PLP monthly payment");
		}
		return this;
	}

	/**
	 * Verify Down Payment
	 * 
	 * @return
	 */
	public String getAccessoryDownPaymentInPDP() {
		String downPayment;
		downPayment = eipDownPaymentWith$.getText();
		return downPayment;

	}

	/**
	 * Verify Loan Term
	 * 
	 * @return
	 */
	public String getAccessoryLoanTermInPDP() {
		String loanTerm;
		loanTerm = eipLoanTermInPDP.getText();
		return loanTerm;

	}

	/**
	 * Verify Monthly Payment
	 * 
	 * @return
	 */
	public String getAccessoryMonthlyPaymentInPDP() {
		String monthlyPayment;
		monthlyPayment = eipMonthlyPaymentInPDP.getText();
		return monthlyPayment;

	}

	/**
	 * Verify Back to list Text
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage verifyBackTolistText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(backToListText.isDisplayed(), "Back to list Text is not displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Back to list Text.");
		}
		return this;
	}

	/**
	 * Get FRP Price of Accessory
	 * 
	 * @return
	 */
	public Double getAccessoryFRPPRiceInPDPValue() {
		String frpPrice;
		waitFor(ExpectedConditions.visibilityOf(accessoryFRPPrice), 5);
		frpPrice = accessoryFRPPrice.getText();
		String frpWithCents = frpPrice.substring(0, frpPrice.length() - 2) + "." + accessoryFRPPriceCents.getText();
		String[] arr = frpWithCents.split("\\$", 0);
		return Double.parseDouble(arr[1]);

	}

	/**
	 * Get EIP Price of Accessory
	 * 
	 * @return
	 */
	public Double getAccessoryMonthlyPaymentInPDPValue() {
		String frpPrice;
		frpPrice = eipMonthlyPaymentInPDP.getText();
		String frpWithCents = frpPrice.substring(0, frpPrice.length() - 2) + "."
				+ eipMonthlyPaymentInPDPCents.getText();
		String[] arr = frpWithCents.split("\\$", 0);
		if (arr[1].contains("-")) {
			arr[1] = "-" + arr[1].replaceAll("-", "");
		}
		return Double.parseDouble(arr[1]);

	}

	/**
	 * Get Accessory Down Payment Price for accessory
	 *
	 */
	public Double getAccessoryDownPaymentPriceForAccessory() {

		String frpTotal;
		frpTotal = eipDownPaymentWith$.getText();
		String[] arr = frpTotal.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Accessory EIP Loan Term for accessory
	 *
	 */
	public Double getAccessoryEIPLoanTermForAccessory() {

		String loanTerm;
		loanTerm = eipLoanTermInPDP.getText().toLowerCase();
		loanTerm = loanTerm.substring(5, loanTerm.length() - 4);
		return Double.parseDouble(loanTerm);
	}

	/**
	 * Get Accessory EIP Loan Term for accessory
	 *
	 */
	public Double getAccessoryCatalogPriceForAccessory() {

		String frpTotal;
		frpTotal = catalogPromoPrice.getText();
		frpTotal = frpTotal.substring(0, frpTotal.indexOf('I'));
		String[] arr = frpTotal.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Get Accessory Striked Price for accessory
	 *
	 */
	public Double getAccessoryStrikedPriceForAccessory() {

		String frpPrice;
		frpPrice = accessoryFRPStrikedPrice.getText();
		String frpWithCents = frpPrice.substring(0, frpPrice.length() - 2) + "."
				+ accessoryFRPStrikedPriceCents.getText();
		String[] arr = frpWithCents.split("\\$", 0);
		return Double.parseDouble(arr[1]);
	}

	/**
	 * Compare EIP Price for an Accessory
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage compareEIPPrice(Double eipPriceActual, Double calculatedEIPPrice) {
		try {
			DecimalFormat df = new DecimalFormat("0.00");
			String formate = df.format(calculatedEIPPrice);
			double finalValue = Double.parseDouble(formate);
			Assert.assertEquals(eipPriceActual, finalValue,
					"EIP Actual Price PDP does not Match with Calculated EIP Price PDP");
			Reporter.log("EIP Actual price matches with Calculated EIP Price PDP");
		} catch (Exception e) {
			Assert.fail("Failed to compare EIP Actual price PDP is not matched with EIP Calculated Price PDP");
		}
		return this;
	}

	/**
	 * Verify Accessory Striked through Price is equal to Original Price minus
	 * Catalogue Promo
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage verifyAccessoryStrikedThroughPrice() {
		try {
			Assert.assertTrue(accessoryFRPStrikedPrice.isDisplayed(), "FRP Striked through Price is not displayed.");
			Double accStrikeThroughPricecalc = getAccessoryStrikedPriceForAccessory()
					- getAccessoryCatalogPriceForAccessory();
			Double accStrikeThroughPrice = getAccessoryFRPPRiceInPDPValue();
			Assert.assertEquals(accStrikeThroughPricecalc, accStrikeThroughPrice,
					"Calculated StrikeThrough price does not match with Actual Striked Through Price");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP Striked through Price.");
		}
		return this;
	}

	/**
	 * Verify california Residents warning message
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage verifyCaliforniaResidentsWarning() {
		try {
			Assert.assertTrue(californiaResidentswarning.isDisplayed(),
					"california residents warning message not displayed");
			Reporter.log("California Residents warning displayed");
		} catch (Exception e) {
			Assert.fail("California Residents warning not displayed");
		}
		return this;
	}

	/**
	 * Click on california Residents warning link
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage clickonCaliforniaPropositionLink() {
		try {
			californiaResidentswarning.click();
			Reporter.log("Clicked on california Proposition link");
		} catch (Exception e) {
			Assert.fail("california Proposition link not displayed");
		}
		return this;

	}

	/**
	 * Verify california Residents warning modal
	 */
	public AccessoriesStandAlonePDPPage verifyCaliforniaPropositionWarningModal() {
		try {
			waitforSpinner();
			Assert.assertTrue(californiaPropositionwarningModalHeader.isDisplayed(),
					"california Proposition warning modal not displayed");
					Assert.assertTrue(californiaPropositionwarningModalMessage.isDisplayed(),
					"california Proposition warning modal message not displayed");
			Reporter.log("California Residents warning modal displayed");
		} catch (Exception e) {
			Assert.fail("California Residents warning modal  not displayed");
		}
		return this;
	}

	/**
	 * Verify PDP Page after closing California Residents warning modal
	 */
	public AccessoriesStandAlonePDPPage validatePDPPageAfterClosingTheModal() {
		try {
			closeCaliforniaPopUp.click();
			Assert.assertTrue(californiaResidentswarning.isDisplayed(),
					"california residents warning message not displayed");
			Reporter.log("Clicked on Close on california Proposition PopUp and PDP page is Displayed");

		} catch (Exception e) {
			Assert.fail("california Proposition link not displayed");
		}
		return this;

	}

	/**
	 * Verify star Image for Accessory PDP
	 */
	public AccessoriesStandAlonePDPPage verifyStarImageForAccessory() {
		try {
			Assert.assertTrue(starImageForAccessory.isDisplayed(), "Star Image For Accessory is not displayed");
			Reporter.log("Star Image For Accessory is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to Verify Star Image For Accessory");
		}
		return this;

	}

	/**
	 * Verify Review Count for Accessory PDP
	 */
	public AccessoriesStandAlonePDPPage verifyPositiveReviewCountForAccessory() {
		try {
			int reviewLength = positiveReviewCountAccessory.size();
			for (int i = 0; i < reviewLength; i++) {
				Assert.assertTrue(positiveReviewCountAccessory.get(i).isDisplayed(),
						" Helpful Review Count for Accessory PDP is not displayed");
				Reporter.log("Helpful review Count for Accessory PDP is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Verifypositive  Review Count for Accessory PDP");
		}
		return this;

	}

	/**
	 * count positive Review for Accessory PDP
	 */
	public String getPositiveReviewCountForAccessory() {
		waitFor(ExpectedConditions.visibilityOf(positiveReviewCountAccessory.get(0)));
		checkPageIsReady();
		String review = positiveReviewCountAccessory.get(0).getText();
		String review1 = review.substring(9, 10);
		return review1;

	}

	/**
	 * Verify Review Count for Accessory PDP
	 */
	public AccessoriesStandAlonePDPPage verifyNegativeReviewCountForAccessory() {
		try {
			int reviewLength = negativeReviewCountAccessory.size();
			for (int i = 0; i < reviewLength; i++) {
				Assert.assertTrue(negativeReviewCountAccessory.get(i).isDisplayed(),
						"Unhelpful Review Count for Accessory PDP is not displayed");
				Reporter.log("Unhelpful review Count for Accessory PDP is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Verify negative Review Count for Accessory PDP");
		}
		return this;

	}

	/**
	 * count negative Review for Accessory PDP
	 */
	public String getNegativeReviewCountForAccessory() {
		waitFor(ExpectedConditions.visibilityOf(negativeReviewCountAccessory.get(2)));
		checkPageIsReady();

		String review = negativeReviewCountAccessory.get(2).getText();
		String review1 = review.substring(11, 12);
		System.out.println("negative review " + review1);
		return review1;

	}

	/**
	 * Review Tab Below for Accessory PDP
	 */
	public AccessoriesStandAlonePDPPage verifyReviewTabBelowForAccessory() {
		try {
			Assert.assertTrue(reviewTabBelowForAccessory.isDisplayed(),
					"Review Tab Below for Accessory PDP is not displayed");
			Reporter.log("Review Tab Below for Accessory PDP is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to Verify Review Tab Below for Accessory PDP");
		}
		return this;

	}

	/**
	 * Click Review Tab Below for Accessory PDP
	 */
	public AccessoriesStandAlonePDPPage clickReviewTabBelowForAccessory() {
		try {
			moveToElement(reviewTabBelowForAccessory);
			Assert.assertTrue(reviewTabBelowForAccessory.isDisplayed(),
					"Review Tab Below for Accessory PDP is not displayed");
			reviewTabBelowForAccessory.click();
			Reporter.log("Review Tab Below for Accessory PDP is clicked");

		} catch (Exception e) {
			Assert.fail("Failed to click on Review Tab Below for Accessory PDP");
		}
		return this;

	}

	/**
	 * Verify that review count are equal
	 */
	public AccessoriesStandAlonePDPPage compareReviewCount(String firstValue, String secondValue) {
		try {
			Assert.assertNotEquals(firstValue, secondValue, " review count is  equal");
			Reporter.log("review count are not equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare review count");
		}
		return this;
	}

	/**
	 * Verify All Filter Options For Accessory Review
	 */
	public AccessoriesStandAlonePDPPage verifyFilterOptionForAccessoryReview() {
		try {
			Assert.assertTrue(reviewTabFilterOptions.get(0).isDisplayed(),
					"All Reviews Filter Option for Review Tab On Accessory PDP is not displayed");
			Assert.assertTrue(reviewTabFilterOptions.get(1).isDisplayed(),
					"Most Helpful Filter Option for Review Tab On Accessory PDP is not displayed");
			Assert.assertTrue(reviewTabFilterOptions.get(2).isDisplayed(),
					"Newest Filter Option for Review Tab On Accessory PDP is not displayed");
			Assert.assertTrue(reviewTabFilterOptions.get(3).isDisplayed(),
					"Oldest Filter Option for Review Tab On Accessory PDP is not displayed");
			Assert.assertTrue(reviewTabFilterOptions.get(4).isDisplayed(),
					"Highest Rating Filter Option for Review Tab On Accessory PDP is not displayed");
			Assert.assertTrue(reviewTabFilterOptions.get(5).isDisplayed(),
					"Lowest Rating Filter Option for Review Tab On Accessory PDP is not displayed");
			Reporter.log("All Options Filter Options For Accessory Review are displayed");

		} catch (Exception e) {
			Assert.fail("Failed to verify All Filter Options For Accessory Review");
		}
		return this;

	}

	/**
	 * Verify Review Count for Accessory PDP Near Star Image
	 */
	public AccessoriesStandAlonePDPPage verifyReviewCountForAccessory() {
		try {
			Assert.assertTrue(reviewCountAccessory.isDisplayed(),
					"Review Count Near Star Image for Accessory PDP is not displayed");
			Reporter.log("Review Count Near Star Image for Accessory PDP is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to Verify Review Count Near Star Image for Accessory PDP");
		}
		return this;

	}

	/**
	 * Click on Features tab on Accessories PDP
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage clickFeaturesOnAccessoriesStandlonePDPPage() {
		try {
			clickElement(featuresTab);
			Reporter.log("Clicked on Features tab");
		} catch (Exception e) {
			Assert.fail("Unable to click on Features tab");
		}
		return this;
	}

	/**
	 * Verifies presence of Features Section
	 */
	public AccessoriesStandAlonePDPPage verifyFeaturesSectionOnAccessoriesStandalonePDPPage() {
		try {
			Assert.assertTrue(specificationsSection.isDisplayed(), "Features are not displayed");
			Reporter.log("Features section is available");
		} catch (Exception e) {
			Assert.fail("Features section isn't available");
		}
		return this;
	}

	/**
	 * Click the related accessory
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage clickRelatedTabOnAccessoriesStandlonePDPPage() {
		try {
			clickElement(relatedTab);
			Reporter.log("Clicked on related devices tab");
		} catch (Exception e) {
			Assert.fail("Unable to click on related devices tab");
		}
		return this;
	}

	/**
	 * Verifies related devices on Accessories Standalone PDP Page
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage verifyRelatedDevicesSectionOnAccessoriesStandalonePDPPage() {
		try {
			Assert.assertTrue(relatedAccesoriesHeader.isDisplayed(), "Related Accessories header is not displayed");
			Assert.assertTrue(relatedDevicesSection.isDisplayed(), "Related devices are not displayed");
			Reporter.log("Related devices section is available");
		} catch (Exception e) {
			Assert.fail("Related devices section isn't available");
		}
		return this;
	}

	/**
	 * Select the related accessory from list
	 * 
	 * @return
	 */
	public String selectRelatedAccessoryOnAccessoriesStandalonePage() {
		checkPageIsReady();
		String relatedAccessory = null;
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileSlideCTA.click();
				relatedAccessory = relatedAccessoriesName.get(0).getText();
				relatedAccessories.get(0).click();
				Reporter.log("Selected related device");
			} else {
				for (WebElement ele : relatedAccessories) {
					relatedAccessory = relatedAccessoriesName.get(0).getText();
					clickElement(ele);
					Reporter.log("Selected related device");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Related device isn't available");
		}
		return relatedAccessory;
	}

	/**
	 * Verify Accessory FRP greater Than $49
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage verifyAccessoryFRPGreaterThan49() {
		waitforSpinner();
		try {
			Double accessoryFRP = getAccessoryFRPPRiceInPDPValue();
			Assert.assertTrue(accessoryFRP > 49.00, "Accessory FRP Not greater than $49");
			Reporter.log("Accessory FRP is greater than $49");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory FRP is greater than $49");
		}
		return this;
	}

	/**
	 * Verify Accessory FRP Less Than $49
	 * 
	 * @return
	 */
	public AccessoriesStandAlonePDPPage verifyAccessoryFRPLessThan49() {
		try {
			checkPageIsReady();
			Double accessoryFRP = getAccessoryFRPPRiceInPDPValue();
			Assert.assertTrue(accessoryFRP < 49.00, "Accessory FRP Not less than $49");
			Reporter.log("Accessory FRP is less than $49");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory FRP is less than $49");
		}
		return this;
	}
}
