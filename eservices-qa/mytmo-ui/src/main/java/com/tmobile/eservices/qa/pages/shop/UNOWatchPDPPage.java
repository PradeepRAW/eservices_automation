package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class UNOWatchPDPPage extends CommonPage {

	@FindBy(xpath = "//span[contains(text(), 'Upgrade')] | //span[contains(text(), 'Add to Cart')]")
	private WebElement upgradeCTA;

	public UNOWatchPDPPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify UNO PLP Page
	 *
	 * @return
	 */
	public UNOWatchPDPPage verifyWatchesPDPPageLoaded() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains("smart-watch/"));
			Reporter.log("UNOPDP page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to load UNOPDP Page");
		}
		return this;
	}

	/**
	 * Click on Add to Cart CTA button
	 */
	public UNOWatchPDPPage clickOnAddToCartCTA() {
		try {
			checkPageIsReady();
			moveToElement(upgradeCTA);
			clickElementWithJavaScript(upgradeCTA);
			Reporter.log("Add to cart button is clickable");
		} catch (Exception e) {
			Assert.fail("Failed to display Add to cart cta button");
		}
		return this;
	}

}
