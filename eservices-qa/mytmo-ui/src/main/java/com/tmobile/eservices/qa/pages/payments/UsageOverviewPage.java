package com.tmobile.eservices.qa.pages.payments;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * 
 * @author pshiva
 *
 */
public class UsageOverviewPage extends CommonPage {

	@FindBy(id = "downloadCSV")
	private WebElement downloadUsageRecords;

	@FindBy(id = "viewAllUsageDetails")
	private WebElement viewAllUsageDetailsLink;

	@FindAll({ @FindBy(id = "individualName0"), @FindBy(id = "individualNameMinutes0") })
	private WebElement userName;

	@FindAll({ @FindBy(id = "individualMsisdn0"), @FindBy(id = "individualMsisdnMinutes0") })
	private WebElement msisdn;

	@FindBy(linkText = "Minutes")
	private WebElement minutes;

	@FindBy(linkText = "Messages")
	private WebElement messages;

	@FindBy(linkText = "Data")
	private WebElement data;

	// @FindBy(css = "#billCycleSelector")
	@FindAll({ @FindBy(css = "#billCycleSelector"), @FindBy(css = "div#changedropdown span") })
	private WebElement selectBillCycleSelector;

	@FindBy(css = "#di_billCycle0")
	private WebElement previousBillCycle;

	@FindBy(css = "#di_billCycleDropDown")
	private WebElement billCycleDropdown;

	@FindBys(@FindBy(css = "#di_billCycleDropDown li"))
	private List<WebElement> lineDeviceTradeInOptions;

	@FindAll({ @FindBy(css = "div.overview_Title.margin10 span.ui_headline"), @FindBy(css = "div.ui_mobile_headline") })
	private WebElement verifyUsageOverviewPage;

	@FindAll({ @FindBy(css = "div[class*=ui_caps_headline]"), @FindBy(css = "div[class*=ui_mobile_subhead]") })
	private List<WebElement> verifyTabs;

	@FindBy(id = "di_allTabs")
	private WebElement callDetailTabs;

	@FindBy(css = "div.co_calldetailrecords")
	private WebElement calldetailrecords;

	@FindBy(id = "di_datatab")
	private WebElement dataTab;

	@FindBy(id = "usageHistory")
	private WebElement yourUsageHistoryInLegacy;

	@FindBy(id = "di_voicecolumnbar")
	private WebElement usageDetailsRow;

	@FindBys(@FindBy(css = "a.ui_primary_link.data-stash-modal-link"))
	private List<WebElement> viewDataStash;

	@FindBys(@FindBy(css = "a[data-toggle='modal']"))
	private List<WebElement> viewDataStashMobile;

	@FindBy(css = "div#datastashmodal")
	private WebElement verifyDataStash;

	@FindBy(css = "div#datastashmodal button.close")
	private WebElement closeDataStash;

	@FindBy(css = "div#jodUpgradeSection_No_Redemption")
	private WebElement warningMessageJODPhone;

	@FindBy(css = "svg#SVGChart title")
	private WebElement numberOfMinutes;

	@FindBy(xpath = "//*[text()='Minutes']")
	private WebElement minutesText;

	@FindBy(xpath = "//*[text()='Messages']")
	private WebElement messagesText;

	@FindBy(xpath = "//*[text()='Data']")
	private WebElement dataText;

	@FindBy(id = "di_CDRMsisdn1")
	private WebElement lineSelectorMSISDNUsagepage;

	private By waitSpinner = By.id("di_loaderImage");

	@FindBys(@FindBy(css = ".ui_mobile_subhead"))
	private List<WebElement> mobileHeaderTabs;

	@FindBy(id = "viewAllUsageDetails")
	private WebElement viewAllUsageDetails;

	@FindBy(xpath = "//div[text()='Minutes ']")
	private WebElement viewMinutes;

	@FindBy(xpath = "//div[text()='Messages ']")
	private WebElement viewMessages;

	@FindBy(xpath = "//div[text()='Data']")
	private WebElement viewData;

	@FindBy(xpath = "//div[text()='T-Mobile Purchases']")
	private WebElement viewTmobilePurchases;

	@FindBy(xpath = "//div[text()='Third-Party Purchases']")
	private WebElement viewThirdPartyPurchases;

	@FindBy(xpath = "//span[contains(text(),'of Total on-network data')]")
	private WebElement ofTotalOnNetworkDataText;

	@FindBy(xpath = "//span[contains(text(),'of Unlimited high-speed')]")
	private List<WebElement> ofUnlimitedHighSpeedText;

	@FindAll({ @FindBy(id = "img_DropDownIcon"),
			@FindBy(css = "span.pull-right.ui_mobile_primary_link.cdrpassdropdetail") })
	private WebElement selectMonthDropDown;

	@FindAll({ @FindBy(css = "#usageChart .usage_margin2"), @FindBy(css = "#usageChart .ui_mobile_subhead") })
	private WebElement usageOverTimeHeader;

	@FindBy(css = "div[id='newPurchasesHeading']>div[class*='ui_mobile_subhead']")
	private List<WebElement> tMobilePurchasesText;

	@FindBy(css = "div[id='thirdPartyChargesHeading']>div[class*='ui_mobile_subhead']")
	private List<WebElement> thirdPartyChargesText;

	@FindBy(css = "div[id='sharedPurchases']>div[id*='sharedPurchasesLine0']>div[id*='sharedPurchases0']")
	private List<WebElement> tMobilePurchases;

	@FindBy(css = "div[id='sharedThirdPartyCharges']>div[id*='sharedThirdPartyChargesLine0']>div[id*='sharedThirdPartyCharges0']")
	private List<WebElement> individualThirdPartyCharges;

	@FindBy(css = ".ajax_loader")
	private WebElement pageSpinner;

	@FindBy(css = "#individualName0")
	private WebElement pageLoadElement;

	private final String pageUrl = "usage";

	/**
	 * 
	 * @param webDriver
	 */
	public UsageOverviewPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @return
	 */
	public UsageOverviewPage verifyUsageOverviewPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Usage overview page is displayed");
		} catch (Exception e) {
			Assert.fail("Usage overview page not displayed");
		}
		return this;
	}

	/**
	 * click View All Usage Details
	 */
	public UsageOverviewPage clickViewAllUsageDetails() {
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			checkPageIsReady();
			viewAllUsageDetailsLink.click();
			Reporter.log("Clicked on veiw all Usage details link");
		} catch (Exception e) {
			Assert.fail("View all usage details not found");
		}
		return this;
	}

	/**
	 * Verify Usage Page Heading
	 * 
	 * @return
	 */
	public UsageOverviewPage verifyUsagePageHeading() {
		try {
			waitForSpinnerInvisibility();
			verifyUsageOverviewPage.isDisplayed();
			Reporter.log("Usage page header displayed");
		} catch (Exception e) {
			Assert.fail("Usage page header not found");
		}
		return this;
	}

	/**
	 * Verify Tabs
	 * 
	 * @param tab
	 * @return
	 */
	public UsageOverviewPage verifyTabs(String tab) {
		try {
			Boolean isTabDisplayed = false;
			for (WebElement webElement : verifyTabs) {
				if (webElement.getText().equalsIgnoreCase(tab)) {
					isTabDisplayed = true;
					Reporter.log("Verified tab: " + tab);
					break;
				}
			}
			Assert.assertTrue(isTabDisplayed, "Tab: " + tab + " not found");
		} catch (Exception e) {
			Assert.fail("Tabs not found");
		}
		return this;
	}

	/**
	 * verify Data Stash
	 * 
	 * @return
	 */
	public UsageOverviewPage verifyDataStash() {
		try {
			dataStashByMobileOrDesktop(viewDataStash);
			Reporter.log("Verified data stash");
		} catch (Exception e) {
			Assert.fail("Data stash not found");
		}
		return this;
	}

	/**
	 * verify Data Stash Mobile
	 * 
	 * @return boolean
	 */
	public UsageOverviewPage verifyDataStashMobile() {
		try {
			dataStashByMobileOrDesktop(viewDataStashMobile);
			Reporter.log("Verified view Data stash mobile");
		} catch (Exception e) {
			Assert.fail("Data stash mobile not found");
		}
		return this;
	}

	/**
	 * data Stash By Mobile Or Desktop
	 * 
	 * @param elements
	 * @return
	 */
	private UsageOverviewPage dataStashByMobileOrDesktop(List<WebElement> elements) {
		checkPageIsReady();
		waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
		for (WebElement webElement : elements) {
			if (webElement.isDisplayed()) {
				webElement.click();
				waitFor(ExpectedConditions.visibilityOf(verifyDataStash));
				if (verifyDataStash.isDisplayed()) {
					closeDataStash.click();
					Reporter.log("Clicked on close data stash");
					break;
				}

			}
		}
		return this;
	}

	/**
	 * Verify Messages tab
	 * 
	 * @return boolean
	 */
	public UsageOverviewPage verifyMinutesTab() {
		try {
			waitFor(ExpectedConditions.visibilityOf(minutesText));
			minutesText.isDisplayed();
			Reporter.log("Verified minutes tab");
		} catch (Exception e) {
			Assert.fail("Minuites tab not found");
		}
		return this;
	}

	/**
	 * Verify Messages tab
	 * 
	 * @return boolean
	 */
	public UsageOverviewPage verifyMessageTab() {
		try {
			waitFor(ExpectedConditions.visibilityOf(messagesText));
			messagesText.isDisplayed();
			Reporter.log("Message Tab verified");
		} catch (Exception e) {
			Assert.fail("Message tab not found");
		}
		return this;
	}

	/**
	 * Verify Data tab
	 * 
	 * @return boolean
	 */
	public UsageOverviewPage verifyDataTab() {
		try {
			waitFor(ExpectedConditions.visibilityOf(dataText));
			dataText.isDisplayed();
			Reporter.log("Verified data tab");
		} catch (Exception e) {
			Assert.fail("Data tab not found");
		}
		return this;
	}

	/**
	 * verify Mobile Tabs
	 * 
	 * @param tab
	 * @return boolean
	 */
	public UsageOverviewPage verifyMobileTabs(String tab) {
		try {
			Boolean isTabDisplayed = false;
			checkPageIsReady();
			for (WebElement mobileTab : mobileHeaderTabs) {
				if (mobileTab.isDisplayed() && tab.contains(mobileTab.getText())) {
					isTabDisplayed = true;
					Reporter.log(tab + " tab is displayed");
					break;
				}
			}
			Assert.assertTrue(isTabDisplayed, "Mobile Tab: " + tab + " not found");
		} catch (Exception e) {
			Assert.fail("Mobile tab: " + tab + " not found");
		}
		return this;
	}

	/**
	 * download Usage Records
	 */
	public UsageOverviewPage downloadUsageRecords() {
		try {
			checkPageIsReady();
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			waitFor(ExpectedConditions.visibilityOf(downloadUsageRecords));
			Assert.assertTrue(downloadUsageRecords.isDisplayed(), "Download Usage Records is not present");
			Reporter.log("Verified download records");
		} catch (Exception e) {
			Assert.fail("Download usage records not found");
		}
		return this;
	}

	/**
	 * view All Usage Details in Usage Page
	 */
	public UsageOverviewPage verifyUsageInfoAndroid() {
		try {
			viewMinutes.isDisplayed();
			viewMessages.isDisplayed();
			viewData.isDisplayed();
		} catch (Exception e) {
			Assert.fail("usage info not found");
		}
		return this;
	}

	/**
	 * Verify Usage Over time Header
	 */
	public UsageOverviewPage getTextUsageOverTimeHeader(String text) {
		try {
			checkPageIsReady();

			waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
			scrollToElement(usageOverTimeHeader);
			Assert.assertTrue(usageOverTimeHeader.getText().contains(text), "Usage over time Header is not displayed");
			Reporter.log("Usage over time Header is displayed");
		} catch (Exception e) {
			Assert.fail("Text usage over time header not found");
		}
		return this;
	}

	/**
	 * verify TMobile Purchases Text
	 * 
	 * @return
	 */
	public boolean verifyTMobilePurchasesText() {
		boolean isElementExsist = false;
		if (!thirdPartyChargesText.isEmpty() && thirdPartyChargesText.get(0).isDisplayed()
				&& !tMobilePurchases.isEmpty()) {
			for (WebElement webElement : tMobilePurchases) {
				if (webElement.getText().contains("$0.00")) {
					isElementExsist = true;
				} else {
					isElementExsist = false;
				}
			}
		}
		return isElementExsist;
	}

	/**
	 * verify Third Party Chargess Text
	 * 
	 * @return
	 */
	public boolean verifyThirdPartyChargesText() {
		boolean isElementExsist = false;
		if (!thirdPartyChargesText.isEmpty() && thirdPartyChargesText.get(0).isDisplayed()
				&& !tMobilePurchases.isEmpty()) {
			for (WebElement webElement : individualThirdPartyCharges) {
				if (webElement.getText().contains("$0.00")) {
					isElementExsist = true;
				} else {
					isElementExsist = false;
				}
			}
		}
		return isElementExsist;
	}

	/**
	 * view All Usage Details in Usage Page
	 */
	public UsageOverviewPage verifycallDetailTabsDisplayed() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				calldetailrecords.isDisplayed();
			} else {
				callDetailTabs.isDisplayed();
			}

		} catch (Exception e) {
			Assert.fail("call details tabs not displayed");
		}
		return this;
	}

	/**
	 * select previous Bill Cycle
	 */
	public UsageOverviewPage selectPreviousBillCycle() {
		try {
			selectBillCycleSelector.click();
			waitFor(ExpectedConditions.visibilityOf(billCycleDropdown));
			Assert.assertTrue(billCycleDropdown.isDisplayed(), "Monthly drop down Section is not Displayed ");
			Reporter.log("Monthly drop down Section is Displayed ");
			previousBillCycle.click();
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
		} catch (Exception e) {
			Assert.fail("Failed to select previos bill cycle");
		}
		return this;
	}

}
