package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AccountsApiV3 extends ApiCommonLib {

	/***
	 * This is the API for EOS Account Management Service parameters: - $ref:
	 * '#/parameters/oAuth' - $ref: '#/parameters/transactionId' - $ref:
	 * '#/parameters/correlationId' - $ref: '#/parameters/applicationId' - $ref:
	 * '#/parameters/channelId' - $ref: '#/parameters/clientId' - $ref:
	 * '#/parameters/transactionBusinessKey' - $ref:
	 * '#/parameters/transactionBusinessKeyType' - $ref:
	 * '#/parameters/transactionType'
	 * 
	 * @return
	 * @throws Exception
	 */

	// Gets stored payment methods on an account. Returns collection of
	// PaymentMethod objects.

	public Response getStoredPayment(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "accounts/v3/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	// Create new payment method

	public Response createNewPaymentMethodBank(ApiTestData apiTestData, String requestBody,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "accounts/v3/" + tokenMap.get("accountNumber") + "/paymentmethods/CHECK";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		System.out.println(response.asString());
		return response;
	}

	// Create new payment method

	public Response createNewPaymentMethodCard(ApiTestData apiTestData, String requestBody,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "accounts/v3/" + tokenMap.get("ban") + "/paymentmethods/CREDIT";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		System.out.println(response.asString());
		return response;
	}

	// Create new payment method

	public Response updateStoredPaymentMethodCard(ApiTestData apiTestData, String requestBody,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "accounts/v3/" + tokenMap.get("ban") + "/paymentmethods/CREDIT";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
    	System.out.println(response.asString());
		return response;
	}

	public Response updatePaymentMethodBank(ApiTestData apiTestData, String requestBody,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "accounts/v3/" + tokenMap.get("accountNumber") + "/paymentmethods/CHECK";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		System.out.println(response.asString());
		return response;
	}

	// Delete payment method

	public Response deletePaymentMethod(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPaymentsAPIHeader(apiTestData,tokenMap);
    	String resourceURL = "accounts/v3/" + apiTestData.getBan() + "/paymentmethods/" + apiTestData.getPaymentCode();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.DELETE);
		System.out.println(response.asString());
		return response;
	}

	public Response validatePaymentMethod(ApiTestData apiTestData, String requestBody,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPlansAPIHeader1(apiTestData,tokenMap);
    	String resourceURL = "accounts/v3/validation/" + apiTestData.getBan();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildAccountPaymentsAPIHeader(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		final String accessToken = checkAndGetAccessToken();
		tokenMap.put("accessToken", accessToken);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization",accessToken);
		headers.put("ban",apiTestData.getBan());
		headers.put("channel_id","TSL");
		headers.put("eosWalletCapacity","3");
		headers.put("Content-Type","application/json");
		headers.put("interactionId","123456787");
		headers.put("isLimitedWalletCapacity","false");
		return headers;
	}

	private Map<String, String> buildAccountPlansAPIHeader() throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "Bearer " + checkAndGetAccessToken());
		headers.put("interactionId", "1222");
		headers.put("X-Auth-Originator", checkAndGetAccessToken());
		headers.put("Accept", "application/json");
		headers.put("activity-id", "121212");
		headers.put("application-id", "333");
		headers.put("channel-id	", "444");
		headers.put("dealercode", "123");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid","PaymentsAPITest");

		return headers;
	}
	
	private Map<String, String> buildAccountPlansAPIHeader1(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("clientid","ESERVICE");
		headers.put("applicationId","MYTMO");
		headers.put("transactionType","UPGRADE");
		headers.put("transactionId","5d96155b-67b0-40df-947f-7eba582efafe");
		headers.put("channelId","DESKTOP");
		headers.put("channel_id","WEB");
		headers.put("usn","1000021111");
		headers.put("application_id","MYTMO");
		headers.put("Content-Type","application/json");
		headers.put("interactionId","123123123");
		headers.put("phoneNumber","4042502893");
		headers.put("transactionBusinesKey","BAN");
		headers.put("transactionBusinesKeyType","897132371");
		headers.put("Authorization",checkAndGetAccessToken());

		return headers;
	}

	// Delete payment method

	public Response getAccountPlansMethod(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildAccountPlansAPIHeader();
    	String resourceURL = "billing-experience/v3/recurring-items/OTYwODQ5ODcy";
    	Response response = invokeRestServiceCall(headers, "https://tmobileqat-qat03-pro.apigee.net", resourceURL, "" ,RestServiceCallType.GET);
		System.out.println(response.asString());
		return response;
	}

}
