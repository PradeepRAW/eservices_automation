package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author pshiva
 *
 */
public class EditCardPage1_5 extends CommonPage {

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	private By waitSpinner = By.cssSelector("div.circle");

	@FindBy(css = "span[tooltip-position='top']")
	private WebElement toolTipText;

	@FindBy(linkText = "What's this?")
	private WebElement whatIsThisCvvlink;

	@FindBy(css = "i.tooltip-icon")
	private WebElement toolTip;

	@FindBy(className = "cvv_visaImg")
	private WebElement cvvModel;

	@FindBy(className = "Display3")
	private WebElement pageHeader;

	@FindAll({ @FindBy(id = "name_on_card"), @FindBy(id = "cardName") })
	private WebElement cardName;

	@FindAll({ @FindBy(id = "card_number"), @FindBy(id = "creditCardNumber") })
	private WebElement cardNumber;

	@FindBy(id = "expirationDate")
	private WebElement expiryYearAndMonth;

	@FindAll({ @FindBy(id = "CVV"), @FindBy(id = "cvvNumber") })
	private WebElement cvvCode;

	@FindAll({ @FindBy(id = "Zip"), @FindBy(id = "zipCode") })
	private WebElement zip;

	@FindAll({ @FindBy(css = "div.card-information-title h3"), @FindBy(className = "Display3") })
	private WebElement cardHeader;

	@FindBy(css = "div.card-information-title h4")
	private WebElement cardHeaderMobile;

	@FindAll({ @FindBy(css = "div#continue button"), @FindBy(css = "button.PrimaryCTA") })
	private WebElement continueAddCard;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement cancelBtn;

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button.SecondaryCTA") })
	private WebElement backBtn;

	@FindAll({ @FindBy(css = "label[for='name_on_card']"), @FindBy(css = "label[for='cardName']") })
	private WebElement nameOnCardTitle;

	@FindAll({ @FindBy(css = "label[for='card_number']"), @FindBy(css = "label[for='creditCardNumber']") })
	private WebElement creditCardNoTitle;

	@FindAll({ @FindBy(css = "label[for='expiration_date']"), @FindBy(css = "label[for='expirationDate']") })
	private WebElement expDateTitle;

	@FindAll({ @FindBy(css = "label[for='CVV']"), @FindBy(css = "label[for='cvvNumber']") })
	private WebElement cvvTitle;

	@FindAll({ @FindBy(css = "label[for='ZIP']"), @FindBy(css = "label[for='zipCode']") })
	private WebElement zipCodeTitleSoftGoods;

	@FindAll({ @FindBy(css = "label[for='ZIP']"), @FindBy(css = "label[for='zipCode']") })
	private WebElement zipCodeTitle;

	@FindBy(css = "label[for='zip']")
	private WebElement zipCodeTitleHardGoods;

	@FindBy(css = "input#zipCode")
	private WebElement zipCode;

	@FindBy(css = "#pageContainer")
	private WebElement pageSpinner;

	@FindBy(css = "input#cardNickname")
	private WebElement nickName;

	@FindAll({ @FindBy(css = "label[for='nickName']"), @FindBy(css = "label[for='nickName']") })
	private WebElement nickNameLabel;

	@FindBy(css = "label[for = 'defaultCheckbox']>div>span")
	private WebElement defaultCheckbox;

	@FindBy(xpath = "//p[contains(text(),'Set as default')]")
	private WebElement defaultCheckboxLabel;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_name')]"), @FindBy(css = "label[for='accountName']") })
	private WebElement nameOnAccountHeader;

	private final String pageUrl = "/updatecard";

	@FindBy(css = "button.PrimaryCTA")
	private WebElement saveChangesBtn;
	
	@FindBy(id="deleteModelLink")
	private WebElement removeFromMyWalletLink;
	
	@FindBy(id="deleteModal")
	private WebElement deleteModal;

	@FindBy(css="#deleteModal div span.Display3")
	private WebElement deleteModalHeader;

	@FindBy(css="#deleteModal div.padding-top-medium span")
	private WebElement deleteModalContent;

	@FindBy(css="#deleteModal button.PrimaryCTA-accent")
	private WebElement deleteModalYesBtn;

	@FindBy(css="#deleteModal #deleteModalCancelBtn")
	private WebElement deleteModalCancelBtn;
	
	
	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public EditCardPage1_5(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EditCardPage1_5 verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * verify card information header
	 *
	 * @return boolean
	 */
	public EditCardPage1_5 verifyCardPageLoaded(String msg) {
		try {

			checkPageIsReady();
			// waitFor(ExpectedConditions.visibilityOf(pageSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(backBtn));
			verifyPageUrl();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(cardHeaderMobile.getText().trim().equals(msg));

			} else {
				Assert.assertTrue(cardHeader.getText().trim().equals(msg));
			}
			Reporter.log("Verified the Card info Header details");
		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	public EditCardPage1_5 clickBackBtn() {
		try {
			backBtn.click();
		} catch (Exception e) {
			Assert.fail("Failed to click back button");
		}
		return this;
	}

	/**
	 * click continue button for add Card
	 */
	public EditCardPage1_5 clickContinueAddCard() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(continueAddCard));
			clickElementWithJavaScript(continueAddCard);
			Reporter.log("Clicked on Continue button");

		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	/**
	 * Verify Card Number place Holder
	 * 
	 * @return true/false
	 */
	public EditCardPage1_5 verifyCardNumberplaceHolder(String msg) {
		try {
			getplaceHolderValue(cardNumber, msg);
			Reporter.log("Verified card number place holder");
		} catch (Exception e) {
			Verify.fail("Failed to verify card Number place holder");
		}

		return this;
	}

	/**
	 * Verify Card Name place Holder
	 * 
	 * @return true/false
	 */
	public EditCardPage1_5 verifyCardNameplaceHolder(String msg) {
		try {
			getplaceHolderValue(cardName, msg);
			Reporter.log("Verified the card Name place holder");
		} catch (Exception e) {
			Verify.fail("Failed to verify Card Name place holder");
		}
		return this;
	}

	/**
	 * Verify Card Cvv Code place Holder
	 * 
	 * @return true/false
	 */
	public EditCardPage1_5 verifyCardCVVCodeplaceHolder(String msg) {
		try {
			getplaceHolderValue(cvvCode, msg);
			Reporter.log("CVV code place holder verified");
		} catch (Exception e) {
			Verify.fail("Failed to verify card CVV code place holder");
		}
		return this;
	}

	/**
	 * Verify Card ZipCode place Holder
	 * 
	 * @return true/false
	 */
	public EditCardPage1_5 verifyCardZipCodeplaceHolder(String msg) {
		try {
			getplaceHolderValue(zip, msg);
			Reporter.log("Verified the card zip code place holder value");
		} catch (Exception e) {
			Verify.fail("Failed to verify card zip code place holder value");
		}
		return this;
	}

	public void getplaceHolderValue(WebElement element, String placeHolder) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element));
			element.getAttribute("placeholder").contains(placeHolder);
		} catch (Exception e) {
			Verify.fail(placeHolder + " Place holder not found");
		}
	}

	/**
	 * verify Name on card title and field are displayed
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyNameOnCardHeader() {
		try {
			nameOnCardTitle.isDisplayed();
			cardName.isDisplayed();
			Reporter.log("Verified the Name on Card Header");
		} catch (Exception e) {
			Verify.fail("Failed to verify the Name on Card Header");
		}
		return this;
	}

	/**
	 * verify credit card number title and field are displayed
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyCreditCardNoHeader() {
		try {
			creditCardNoTitle.isDisplayed();
			cardNumber.isDisplayed();
			Reporter.log("Verified the Credit Card No. Header");
		} catch (Exception e) {
			Verify.fail("Fail to verify the Credit Card No. Header");
		}

		return this;
	}

	/**
	 * verify Exp date title and field are displayed
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyExpDateHeader() {
		try {
			expDateTitle.isDisplayed();
			Reporter.log("Verified the Exp Date Header");
		} catch (Exception e) {
			Verify.fail("Failed to verify the Exp Date Header");
		}
		return this;
	}

	/**
	 * verify CVV title and field are displayed
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyCVVHeader() {
		try {
			cvvTitle.isDisplayed();
			cvvCode.isDisplayed();
			Reporter.log("Verified the CVV Header");
		} catch (Exception e) {
			Verify.fail("Fail to verify the CVV Header");
		}
		return this;
	}

	/**
	 * verify Exp date title and field are displayed
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyZipeHeader() {
		try {
			zipCodeTitle.isDisplayed();
			zipCode.isDisplayed();

			Reporter.log("Verified the Exp Date Header");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Exp Date Header");
		}
		return this;
	}

	/**
	 * verify Page header is displayed or not
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyPageHeader(String pageHeaderText) {
		try {
			pageHeader.isDisplayed();
			Assert.assertTrue(pageHeader.getText().equals(pageHeaderText), "page header is not found");
			Reporter.log("Page header is displayed");
		} catch (Exception e) {
			Assert.fail("page header is not found");
		}
		return this;
	}

	/**
	 * verify continue button is displayed or not
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyContinueBtn() {
		try {
			continueAddCard.isDisplayed();

			Reporter.log("continue button is displayed");
		} catch (Exception e) {
			Assert.fail("continue button is not found");
		}
		return this;
	}

	/**
	 * verify cancel button is displayed or not
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyCancelBtn() {
		try {
			cancelBtn.isDisplayed();

			Reporter.log("cancel button is displayed");
		} catch (Exception e) {
			Assert.fail("cancel button is not found");
		}
		return this;
	}

	/**
	 * click what is this? link for cvv model
	 */
	public EditCardPage1_5 clickWhatIsThisCvvLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(whatIsThisCvvlink));
			whatIsThisCvvlink.isDisplayed();
			whatIsThisCvvlink.click();
			Reporter.log("Clicked on what is this? link");

		} catch (Exception e) {
			Assert.fail("what is this? link is not found");
		}
		return this;
	}

	/**
	 * verify cvv model is displayed or not
	 */
	public EditCardPage1_5 verifyCvvModel() {
		try {

			cvvModel.isDisplayed();

			Reporter.log("cvv model is displayed");

		} catch (Exception e) {
			Assert.fail("cvv model is not displayed");
		}
		return this;
	}

	public EditCardPage1_5 mouseHoverOnToolTip() {
		try {
			Actions builder = new Actions(getDriver());
			Action mouseOverHome = builder.moveToElement(toolTip).build();
			mouseOverHome.perform();
			Reporter.log("mouse hover on tool tip");
		} catch (Exception e) {
			Assert.fail("tool tip is not found");
		}
		return this;
	}

	/**
	 * verify text on tool tip
	 */
	public EditCardPage1_5 verifyTextOnToolTip(String toolTiptest) {
		try {
			toolTipText.isDisplayed();
			Assert.assertTrue(toolTipText.getAttribute("tooltip-text").equals(toolTiptest));
			Reporter.log("text on tool tip is verified");
		} catch (Exception e) {
			Assert.fail("text on tool tip not found");
		}
		return this;
	}

	/**
	 * verify field label is displayed or not
	 * 
	 * @return boolean
	 */
	public EditCardPage1_5 verifyAddCardFieldLabels() {
		try {
			verifyNameOnCardHeader();
			Assert.assertTrue(nameOnCardTitle.getText().equals("Name on card"), "Name on card is not found");
			verifyCreditCardNoHeader();
			Assert.assertTrue(creditCardNoTitle.getText().equals("Credit card number"),
					"Credit card number is not found");
			verifyExpDateHeader();
			Assert.assertTrue(expDateTitle.getText().equals("Expiration date"), "Expiration date is not found");
			verifyCVVHeader();
			Assert.assertTrue(cvvTitle.getText().equals("CVV"), "CVV is not found");
			verifyZipeHeader();
			Assert.assertTrue(zipCodeTitle.getText().equals("Zip"), "ZIP is not found");
			/*
			 * verifyNickNameHeader();
			 * Assert.assertTrue(nickNameLabel.getText().equals("NickName"),
			 * "NickName is not found");
			 */
			Reporter.log("Field labels is displayed");
		} catch (Exception e) {
			Assert.fail("Field labels  is not found");
		}
		return this;
	}

	public void verifyInSessionFieldValues(Payment payment) {
		try {
			Assert.assertTrue(cardName.getAttribute("value").equals(payment.getNameOnCard()), "Name is not matching");
			Assert.assertTrue(cardNumber.getAttribute("value").replaceAll("-", "").equals(payment.getCardNumber()),
					"Card Number is not matching");
			Assert.assertTrue(zipCode.getAttribute("value").equals(payment.getZipCode()), "ZipCode is not matching");
			Assert.assertTrue(expiryYearAndMonth.getAttribute("value").replaceAll("[^\\d.]", "")
					.equals(payment.getExpiryMonthYear()), "Expiry Month is not matching");
		} catch (Exception e) {
			Assert.fail("Failed verifying In session Field values for Card");
		}
	}

	public void enterCardNumber(String cardNum) {
		try {
			while (!cardNumber.getAttribute("value").isEmpty()) {
				cardNumber.sendKeys(Keys.BACK_SPACE);
			}
			cardNumber.sendKeys(cardNum);
		} catch (Exception e) {
			Assert.fail("Failed entering Card number");
		}
	}

	public void enterCVVCode(String cvv) {
		try {
			cvvCode.sendKeys(cvv);
		} catch (Exception e) {
			Assert.fail("Failed entering CVV code");
		}
	}

	/**
	 * verify NickName Field is displayed or not
	 * 
	 *
	 */
	public EditCardPage1_5 verifyNickNameHeader() {
		try {
			nickNameLabel.isDisplayed();
			nickName.isDisplayed();
			Reporter.log("nickName field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify the nickName Header ");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public EditCardPage1_5 verifyNickNameDisplay() {
		try {
			if (nickName.isDisplayed()) {
				Reporter.log("nickName field is displayed");
			} else {
				Reporter.log("nickName field is Surpressed");
			}

		} catch (Exception e) {
			Assert.fail("nickName Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field is displayed or not
	 * 
	 *
	 */
	public EditCardPage1_5 verifyDefaultCheckBox() {
		try {
			if (defaultCheckbox.isDisplayed()) {
				Reporter.log("Default Check Box field is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Default checkbox Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field label is displayed or not
	 * 
	 *
	 */
	public EditCardPage1_5 verifyDefaultCheckBoxLabel() {
		try {
			if (defaultCheckboxLabel.isDisplayed()) {
				Reporter.log("Default Check Box field Label is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field Label is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Default checkbox Field Label is not Found ");
		}
		return this;
	}

	/**
	 * Click checkbox Field is Enabled or not
	 * 
	 *
	 */
	public EditCardPage1_5 ClickDefaultCheckBox() {
		try {
			defaultCheckbox.click();
			Reporter.log("default Check box field is Clicked");

		} catch (Exception e) {
			Assert.fail("default CheckBox is not Found ");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public EditCardPage1_5 verifyNoNickNameandDefaultCheckBoxDisplay() {
		try {
			if (nickName.isDisplayed() && defaultCheckboxLabel.isDisplayed() && defaultCheckbox.isDisplayed()) {
				Assert.fail("nickName Field,Default Check Box and Default CheckBox Label is Displayed");
			}

		} catch (Exception e) {
			Reporter.log("nickName Field,Default Check Box and Default CheckBox Label is not found");
		}
		return this;
	}

	public EditCardPage1_5 verifyNameOnAccountHeader() {
		try {
			Verify.assertTrue(nameOnAccountHeader.isDisplayed(), "Name on Account header is not displayed");
			Verify.assertTrue(nameOnAccountHeader.getText().equals("Name on Account"),
					"Name on Account header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Name on Account header missing");
		}
		return this;
	}

	public EditCardPage1_5 getPlaceholder(WebElement element, String msg) {
		try {
			Verify.assertTrue(element.getAttribute("placeholder").equals(msg),
					"Placeholder text is incorrect for " + element.getAttribute("placeholder"));
		} catch (Exception e) {
			Verify.fail("Place holder not found");
		}
		return this;
	}

	/**
	 * verify NickName Field Label, Header, Field
	 */
	public EditCardPage1_5 verifyNickNameFieldElements() {
		try {
			verifyNameOnAccountHeader();
			getPlaceholder(nickNameLabel, "NickName");

		} catch (Exception e) {
			Verify.fail("Failed to verify all NickName elements");

		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public EditCardPage1_5 verifyNickNameSurpess() {
		try {
			if (nickName.isEnabled()) {
				Reporter.log("nickName field is Enabled");
			} else {
				Reporter.log("nickName field is Surpressed");
			}

		} catch (Exception e) {
			Assert.fail("nickName Field is not Found ");
		}
		return this;
	}

	/**
	 * Enter Nick Name in card Page
	 *
	 * @param payment
	 */
	public void enterNickName(String nicName) {
		try {
			checkPageIsReady();
			nickName.clear();
			nickName.sendKeys(nicName);
			Reporter.log("Nick Name is Enterd successfully");
		} catch (Exception e) {
			Assert.fail("Failed to fill the Nick Name");
		}

	}

	/**
	 * verify all page elements for edit card
	 * 
	 * @param storedCardNo
	 */
	public void verifyEditCardPageElements(String storedCardNo) {
		try {
			verifyNameOnCardNotNull();
			verifyExpiryDateNotNull();
			verifyZipCodeNotNull();
			verifyMaskedCardNumberForEditCard(storedCardNo);
			verifyBackBtn();
			verifySaveChangesBtn();
			Reporter.log("Verified Edit card page elements");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit card page elements");
		}
	}

	public void verifyNameOnCardNotNull() {
		try {
			Assert.assertFalse(cardName.getAttribute("value").isEmpty(), "Card name is empty");
			Reporter.log("Card Name is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify card name on Edit card page");
		}
	}

	public void verifyExpiryDateNotNull() {
		try {
			Assert.assertFalse(expiryYearAndMonth.getAttribute("value").isEmpty(), "Expiration Date is empty");
			Reporter.log("Expiration date is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Expiration date on Edit card page");
		}
	}

	public void verifyZipCodeNotNull() {
		try {
			Assert.assertFalse(zipCode.getAttribute("value").isEmpty(), "Zipcode is empty");
			Reporter.log("Zipcode is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Zipcode on Edit card page");
		}
	}

	public void verifyMaskedCardNumberForEditCard(String storedCardNo) {
		try {
			Assert.assertTrue(cardNumber.getAttribute("value").contains(storedCardNo), "Last 4 digits of Card number is not displayed");
			Assert.assertFalse(cardNumber.getAttribute("value").isEmpty(), "Card number is empty");
			Reporter.log("Card number is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Card number on Edit card page");
		}
	}

	public void verifyBackBtn() {
		try {
			backBtn.isDisplayed();
			Reporter.log("Back button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify back button");
		}
	}

	public void verifySaveChangesBtn() {
		try {
			saveChangesBtn.isDisplayed();
			Reporter.log("Save changes button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save changes button");
		}
	}

	public void updateCardDetailsAndSave(Payment payment) {
		try {
			cardName.clear();
			cardName.sendKeys(payment.getNameOnCard());
			expiryYearAndMonth.clear();
			expiryYearAndMonth.sendKeys(payment.getExpiryMonthYear());
			cvvCode.sendKeys(payment.getCvv());
			nickName.clear();
			nickName.sendKeys(payment.getNickName());
			saveChangesBtn.click();
			Reporter.log("Updated new card details and clicked on save");
		} catch (Exception e) {
			Assert.fail("Failed to update card details");
		}
		
	}

	public void verifyUpdatedCardDetails(Payment payment) {
		try {
			Assert.assertTrue(cardName.getAttribute("value").equals(payment.getNameOnCard()));
			Assert.assertTrue(nickName.getAttribute("value").equals(payment.getNickName()));
			Reporter.log("Verified the updated Card details");
		} catch (Exception e) {
			Assert.fail("Failed to verify updated card details");
		}
	}
	
	public EditCardPage1_5 clickRemoveFromMyWalletLink() {
		try {
			removeFromMyWalletLink.click();
			Reporter.log("Clicked on Remove from my Wallet link");
		} catch (Exception e) {
			Assert.fail("Failed to click on Remove from my Wallet link");
		}
		return this;
	}

	public EditCardPage1_5 verifyRemoveFromMyWalletModal() {
		try {
			deleteModal.isDisplayed();
			deleteModalHeader.isDisplayed();
			deleteModalContent.isDisplayed();
			deleteModalYesBtn.isDisplayed();
			deleteModalCancelBtn.isDisplayed();
			Reporter.log("Verified Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify Delete modal");
		}
		return this;
	}

	public EditCardPage1_5 clickYesDeleteOnModal() {
		try {
			deleteModalYesBtn.click();
			Reporter.log("Clicked 'Yes, Delete button' on Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Yes, Delete button' on Delete modal");
		}
		return this;
	}
	
	public EditCardPage1_5 clickCancelOnModal() {
		try {
			deleteModalCancelBtn.click();
			Reporter.log("Clicked 'Cancel button' on Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Cancel button' on Delete modal");
		}
		return this;
	}
	
}