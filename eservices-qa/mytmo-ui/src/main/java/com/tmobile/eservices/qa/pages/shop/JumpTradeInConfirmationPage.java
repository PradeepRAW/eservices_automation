/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class JumpTradeInConfirmationPage extends CommonPage {
	
	public static final String JUMP_TRADEIN_CONFIRMATION_PAGEURL = "purchase-jump/tradeinconfirmation";
	
	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement acceptAndContinueButton;
	
		
	public JumpTradeInConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Jump PLP Page
	 * 
	 * @return
	 */
	public JumpTradeInConfirmationPage verifyPageUrl() {
		
		waitforSpinner();
		checkPageIsReady();
		try{
			getDriver().getCurrentUrl().contains(JUMP_TRADEIN_CONFIRMATION_PAGEURL);
		}catch(Exception e){
			Assert.fail("Jump TradeIn confirmation page is not displayed");
		}
		return this;
	}
	
	
	/**
	 * Click Accept And Continue Button
	 * @return
	 */
	public JumpTradeInConfirmationPage clickAcceptAndContinueButton() {
		try {
			acceptAndContinueButton.click();
			Reporter.log("ContinueButton is displayed");
		} catch (Exception e) {
			Assert.fail("ContinueButton is not displayed to click");
		}
		return this;
	}
	
	
}
