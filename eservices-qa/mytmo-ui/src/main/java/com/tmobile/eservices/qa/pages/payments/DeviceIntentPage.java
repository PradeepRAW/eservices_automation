package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class DeviceIntentPage extends CommonPage {
	private final String pageUrl = "purchase/device-intent";
	
	@FindBy(css = "p.Display4")
	private WebElement pageHeader;

	@FindBy(css = "img.phone-image")
	private WebElement phoneImage;
	
	@FindBy(css = "p.Display6")
	private WebElement friendlyName;
	
	@FindBy(css = "p.legal.padding-top-xsmall")
	private WebElement msisdn;
	
	@FindBy(css = "p.body.padding-top-xsmall-xs")
	private WebElement deviceName;
	
	@FindBy(css = "div.d-inline-block.pr-3.body-bold.black>span.Display5")
	private WebElement dueAmount;
	
	@FindBy(css = "span.H5-heading")
	private WebElement monthlyPaymentAmount;

	public DeviceIntentPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public DeviceIntentPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}
	
	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public DeviceIntentPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Device Intenet Page is loaded.");
		} catch (Exception e) {
			Assert.fail("Device Intenet page is not loaded.");
		}
		return this;
	}

	
	
}
