package com.tmobile.eservices.qa.pages.tmng.functional;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

public class TPPNoOfferPage extends TmngCommonPage {
	
	private final String noOfferPageURL = "pre-screen/no-offer-hcc";


    @FindBy(css = "tmo-pre-screen-no-offer-hcc>div>h2")
    private WebElement preScreenNoOfferText;

    @FindBy(css = " tmo-pre-screen-no-offer-hcc div p")
    private WebElement preScreenNoOfferTextFields;
     
    @FindBy(css = "//*[contains(.,'Continue shopping')]/following::*[contains(@href,'store-locator')]")
    private WebElement scheduleAnInStoreVisitCTA;

    @FindBy(xpath = "//button[contains(.,'Continue shopping')]")
    private WebElement continueShoppingCta;


    public TPPNoOfferPage(WebDriver webDriver) {
        super(webDriver);
    }
    
  
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TPPNoOfferPage verifyPreScreenNoOfferPageURL() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(noOfferPageURL),60);
			Reporter.log("Pre screen No Offer page url is url verified successfully");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Pre screen No Offer page url");
		}
		return this;
	}

    /**
     * Verifies prescreen no offer page
     *
     * @return
     */
    public TPPNoOfferPage verifyPrescreenNoOfferPage() {
        try {
        	waitFor(ExpectedConditions.urlContains(noOfferPageURL),60);
            Assert.assertTrue(preScreenNoOfferText.isDisplayed(), "No offer page is not displayed during TPP pre screen");
            Reporter.log("No offer page is displayed during TPP pre-screen");
        } catch (Exception e) {
            Assert.fail("Failed to verify No offer page during TPP prescreen Flow ");
        }
        return this;
    }
   
    
    /**
     * Verifies prescreen no offer page
     *
     * @return
     */
    public TPPNoOfferPage verifyPrescreenNoOfferPageTextFields() {
        try {
            Assert.assertTrue(preScreenNoOfferTextFields.isDisplayed(), "No offer page Two Text Fields is not displayed during TPP pre screen");
            Reporter.log("No offer page Two Text Fields are displayed during TPP pre-screen");
        } catch (Exception e) {
            Assert.fail("Failed to verify No offer page Two Text Fields");
        }
        return this;
    }


    /**
     * Verify Schedule an In-Store Visit CTA in Mutual Review or Fraud Detection Page
     */
    public TPPNoOfferPage verifyScheduleAnInStoreVisitCTA() {
        try {
            Assert.assertTrue(scheduleAnInStoreVisitCTA.isDisplayed(), "Schedule an In-Store Visit CTA is not displayed on No offer Page");
            Reporter.log("Schedule an In-Store Visit CTA is displayed on  No offer Page");
        } catch (Exception e) {
            Assert.fail("Failed to Verify Schedule an In-Store Visit CTA on  No offer Page");
        }
        return this;
    }

    /**
     * Click Schedule an In-Store Visit CTA in Mutual Review or Fraud Detection Page
     */
    public TPPNoOfferPage clickScheduleAnInStoreVisitCTA() {
        try {
        	scheduleAnInStoreVisitCTA.click();
            Reporter.log("Clicked on Schedule an In-Store Visit CTA in  No offer Page");
        } catch (Exception e) {
            Assert.fail("Failed to click Schedule an In-Store Visit CTA in  No offer Page");
        }
        return this;
    }

    /**
     * Verifies Continue Shopping cta
     *
     * @return
     */
    public TPPNoOfferPage verifyContinueShoppingCTA() {
        try {
            Assert.assertTrue(continueShoppingCta.isDisplayed(), "Continue Shopping Cta is not displayed on No offer Page");
            Reporter.log("Continue Shopping Cta is displayed on No offer Page");
        } catch (Exception e) {
            Assert.fail("Failed to Verify Continue Shopping Cta on No offer Page");
        }
        return this;
    }

    /**
     * Click on Continue Shopping cta
     *
     * @return
     */
    public TPPNoOfferPage clickContinueShoppingCta() {
        try {
        	continueShoppingCta.click();
            Reporter.log("Clicked on Continue Shopping Cta on No offer Page");
        } catch (Exception e) {
            Assert.fail("Failed to on Continue Shopping Cta on No offer Page");
        }
        return this;
    }
}
