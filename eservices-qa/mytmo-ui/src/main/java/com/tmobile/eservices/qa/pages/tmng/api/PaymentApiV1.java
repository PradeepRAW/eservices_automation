package com.tmobile.eservices.qa.pages.tmng.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PaymentApiV1 extends ApiCommonLib {

	public Response getPublicKeyForTMNG(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildPublicKeyAPIHeader(apiTestData);
		String resourceURL = "shop/v1/publickey";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.GET);
		// System.out.println(response.asString());
		return response;
	}

	public Response getShopPublicKeyForCreditCheck(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildShopPublicKeyForCreditCheck(apiTestData);
		String resourceURL = "shop/v1/publickey" + "?keyType=" + apiTestData.getKeyType();
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.GET);
		// System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildPublicKeyAPIHeader(ApiTestData apiTestData) throws Exception {

		Map<String, String> headers = new HashMap<String, String>();

		headers.put("Authorization", checkAndGetTMNGOauthToken());
		headers.put("correlationId", "publickey_shop_1111");
		headers.put("application_id", "MYTMO");
		headers.put("channel_id", "WEB");
		headers.put("clientId", "12345");
		headers.put("transactionId", "1111111");
		headers.put("transactionType", "ACTIVATION");
		headers.put("Cache-Control", "no-cache");
		headers.put("Content-type", "application/json");

		return headers;
	}

	private Map<String, String> buildShopPublicKeyForCreditCheck(ApiTestData apiTestData) throws Exception {

		Map<String, String> headers = new HashMap<String, String>();

		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("correlationId", "publickey_shop_1111");
		headers.put("applicationId", "MYTMO");
		headers.put("channelId", "WEB");
		headers.put("clientId", "12345");
		headers.put("transactionId", "txn_id");
		headers.put("transactionType", "AAL");
		headers.put("Cache-Control", "no-cache");
		headers.put("Content-type", "application/json");
		headers.put("usn", "56eefaef5c4fa291");

		return headers;
	}
}
