package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author rnallamilli
 *
 */
public class NewChooseAccountPage extends CommonPage {

	public static final String chooseAccountPageUrl = "/oauth2/v1/forgotpassword";
	public static final String chooseAccountPageLoadText = "Reset Your Password";

	private static final String pageUrl = "accountchooser";

	private int timeout = 15;

	@FindBy(css = "div[class='ui_subhead']")
	private WebElement accountHeaderText;

	@FindBy(css = "section#container div[class='ui_mobile_subhead']")
	private WebElement deviceAccountHeaderText;

	@FindBy(css = "div#container ul.co_select-account-list.unstyled.pt20.mb25")
	private List<WebElement> listOfAccounts;

	@FindBy(css = "div[id='#account2']")
	private WebElement switchToSecondAccount;

	@FindBy(id = "#account1")
	private WebElement switchToFirstAccount;

	@FindBy(css = ".ban_tmobile_heading h1")
	private List<WebElement> allAccountsMsisdns;

	@FindBy(xpath = "//*[text()='Select account']")
	private WebElement selectAccount;

	@FindBy(css = "input#search")
	private WebElement searchBox;

	@FindBy(xpath = "//div[@class='ban_tmobile_heading']/h1[2]")
	private WebElement searchResultNumber;

	/**
	 * 
	 * @param webDriver
	 */
	public NewChooseAccountPage(WebDriver webDriver) {
		super(webDriver);
	}

	public NewChooseAccountPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(chooseAccountPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Change Password class instance.
	 */
	public NewChooseAccountPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(chooseAccountPageUrl);
		return this;
	}

	/**
	 * Choose Login Msisdn
	 * 
	 * @param msisdn
	 */
	public NewChooseAccountPage chooseOtherAccount(String msisdn) {
		try {
			checkPageIsReady();
			if (CollectionUtils.isNotEmpty(allAccountsMsisdns) && !CollectionUtils.sizeIsEmpty(allAccountsMsisdns))
				for (WebElement accMsisdn : allAccountsMsisdns) {
					String chooseAccount = accMsisdn.getText().trim().replace("(", "").replace(")", "").replace("-", "")
							.replace(" ", "");
					if (chooseAccount.contains(msisdn.trim())) {
						checkPageIsReady();
						getDriver()
								.findElement(
										By.xpath("//*[contains(text(),'" + accMsisdn.getText() + "')]/../../../..//a"))
								.click();
						checkPageIsReady();
						break;
					}
				}
		} catch (Exception e) {
			Assert.fail("Choose an other account failed");
		}
		return this;
	}

	/**
	 * Verify NewChooseAccount page.
	 *
	 * @return the NewChooseAccountPage class instance.
	 */
	public NewChooseAccountPage verifyChooseAccountPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Choose account page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Choose account page not displayed");
		}
		return this;

	}

	/**
	 * Verify search box.
	 *
	 * @return the NewChooseAccountPage class instance.
	 */
	public NewChooseAccountPage verifySearchBox(MyTmoData myTmoData) {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			searchBox.sendKeys(myTmoData.getLoginEmailOrPhone());
			String s = searchResultNumber.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			Assert.assertEquals(s, myTmoData.getLoginEmailOrPhone().trim(),
					"Search result not matched with search term");
			Reporter.log("Search result matched with search term");
		} catch (NoSuchElementException e) {
			Assert.fail("Search result not matched with search term");
		}
		return this;

	}

}