/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author csudheer
 *
 */
public class CoverageDevicePage extends CommonPage {

	public CoverageDevicePage(WebDriver webDriver) {
		super(webDriver);
	}

	private static final String pageUrl = "profile/coverage";

	@FindBy(css = ".body-link.cursor")
	private WebElement signalBoosterLink;

	@FindBy(xpath = "//p[text()='Assigned Mobile Number']")
	private WebElement assignedMobileNumberLink;

	@FindBy(css = "span.body.black.pull-left")
	private WebElement linkedMobileNumber;

	@FindBy(xpath = "//p[text()='Device Address']")
	private WebElement deviceAddressLink;

	@FindBy(css = "input#address1")
	private WebElement addressLine1;

	@FindBy(css = "input#address2")
	private WebElement addressLine2;

	@FindBy(css = ".PrimaryCTA")
	private WebElement saveChangesBtn;

	@FindBy(css = ".PrimaryCTA")
	private WebElement errorMessage;

	/**
	 * Verify CoverageDevice Page.
	 *
	 * @return the CoverageDevicePage class instance.
	 */
	public CoverageDevicePage verifyCoverageDevicePage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Coverage device page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Coverage device page not displayed");
		}
		return this;

	}

	/**
	 * Click 4GLTESignalBoosterLink.
	 */
	public CoverageDevicePage click4GLTESignalBoosterLink() {
		try {
			signalBoosterLink.click();
			Reporter.log("Clicked on 4GLTESignalBooster link");
		} catch (Exception e) {
			Assert.fail("Click on 4GLTESignalBooster link failed");
		}
		return this;
	}

	/**
	 * Click Assigned mobile number link.
	 */
	public CoverageDevicePage clickAssignedMobileNumberLink() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(assignedMobileNumberLink));
			assignedMobileNumberLink.click();
			Reporter.log("Clicked on assigned mobile number link");
		} catch (Exception e) {
			Assert.fail("Click on assigned mobile number link failed");
		}
		return this;
	}

	/**
	 * Verify Missdn number.
	 */
	public CoverageDevicePage verifyMissdnNumber(String phoneNumber) {
		try {
			String splittedMobileNumber[] = linkedMobileNumber.getText().split(",");
			String s = splittedMobileNumber[1].replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			Assert.assertEquals(s, phoneNumber, "Linked phone number not matched with logged missdn");
			Reporter.log("Linked phone number matched with logged missdn");
		} catch (Exception e) {
			Assert.fail("Linked phone number not matched with logged missdn");
		}
		return this;
	}

	/**
	 * Click device address link.
	 */
	public CoverageDevicePage clickDeviceAddressLink() {
		try {
			waitforSpinnerinProfilePage();
			deviceAddressLink.click();
			Reporter.log("Clicked on device address link");
		} catch (Exception e) {
			Assert.fail("Click on device address link failed");
		}
		return this;
	}

	/**
	 * Click update device address.
	 */
	public CoverageDevicePage updateDeviceAddress() {
		try {
			waitforSpinnerinProfilePage();
			if (addressLine1.getAttribute("value").contains("88")) {
				sendTextData(addressLine1, "75 PARKGATE DR APT 87");
			} else {
				sendTextData(addressLine1, "75 PARKGATE DR APT 88");
			}
			saveChangesBtn.click();
			Reporter.log("Updated device address and clicked on save changes button");
		} catch (Exception e) {
			Assert.fail("Updating device address failed");
		}
		return this;
	}

	/**
	 * Click update device address.
	 */
	public CoverageDevicePage updateAddressWithOutChanges() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(deviceAddressLink));
			String currentAddress = addressLine1.getText();
			sendTextData(addressLine1, currentAddress);
			Reporter.log("Updated same address with out any changes");
		} catch (Exception e) {
			Assert.fail("Updating same address with out any changes failed");
		}
		return this;
	}

	/**
	 * verify save changes button state
	 */
	public CoverageDevicePage verifySaveChangesBtnState() {
		try {
			Assert.assertFalse(saveChangesBtn.isEnabled());
			Reporter.log("Save changes button is in disabled state");
		} catch (Exception e) {
			Assert.fail("Save changes button is in enabled state");
		}
		return this;
	}

}
