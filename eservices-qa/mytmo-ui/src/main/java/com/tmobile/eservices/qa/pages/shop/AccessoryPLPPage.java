/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import java.text.DecimalFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 *
 */
public class AccessoryPLPPage extends CommonPage {

	public static final int ACCESSORYD_DEVICE_COUNT = 10;

	@FindBy(css = "button[ng-click*='ctrl.closeModalContinuePlp'] p")
	private WebElement okButton;

	@FindBy(css = "span[ng-show='!$ctrl.hasPromo']")
	private WebElement accessoryPromo;

	@FindBy(css = "manufactureLabel")
	private WebElement manufactureLabel;

	@FindBy(css = "deviceName")
	private WebElement deviceName;

	@FindBy(css = "div[ng-repeat*='accessory'] div.md-container.md-ink-ripple")
	private List<WebElement> accessoryList;

	@FindBy(css = "span.payMonthlyActive")
	private WebElement payInMonthly;

	@FindBy(css = "div.container.no-padding")
	private WebElement sedonaHeader;

	@FindBy(css = "div.row.hidden-xs i.ico-closeIcon")
	private WebElement closeModalDialogWindow;

	@FindBy(css = "*[ng-show*='priceLockupBarContext.showFooterUnder69'] button")
	private WebElement continueButtonEIPeligible;

	@FindBy(css = "div[ng-show*='ctrl.priceLockupBarContext.showBadCreditLockupBar'] button")
	private WebElement continueButtonBadCredit;

	@FindBy(xpath = "//div[@aria-hidden='false']//p[contains(text(), 'Continue')]")
	private WebElement continueButton;

	@FindBy(css = "button.PrimaryCTA-Normal-shipping")
	private List<WebElement> continueBtn;

	@FindBy(css = "div[ng-show='$ctrl.priceLockupBarContext.showFooterUnder69'] button")
	private WebElement continueButtonFRP;

	@FindBy(css = "div.md-icon")
	private List<WebElement> selectedAccessoryCheckbox;

	@FindBy(css = "img.imgsize-xs")
	private List<WebElement> accessoryListImages;

	@FindBy(css = "h4.skip_style.text-black a[ng-click*='$ctrl.skipAccessory()']")
	private WebElement skipAccessoriesLink;

	@FindBy(css = "[ng-show*='priceLockupBarContext.showPriceLockupBar']")
	private WebElement stickyBanner;

	@FindBy(css = "*[ng-show*='priceLockupBarContext.showFooterUnder69']")
	private WebElement stickyBannerInAccessoryListPage;

	@FindBy(css = "md-checkbox[ng-checked='accessory.isSelected'] div.md-ripple-container")
	private WebElement deselectAccessoryDevice;

	@FindBy(css = "i.fa.fa-close.uib-close")
	private List<WebElement> payMonthlyPopupCloseButton;

	@FindBy(css = "button[ng-click*='ctrl.closeModalContinuePlp'] p")
	private List<WebElement> payMonthlyPopupOkButton;

	@FindBys(@FindBy(css = "p.tr-ht-sp.text-black"))
	private List<WebElement> accesoriesList;

	@FindBy(css = "p.price-month.gray.p-t-10.p-t-5-xs.ng-binding")
	private List<WebElement> verifyFRP;

	@FindBy(css = "div[ng-click*='navigateToPDPPage'] span[ng-bind-html*='strikeText']")
	private List<WebElement> verifyEIP;

	@FindBy(css = "div[ng-click*='$ctrl.navigateToPDPPage(accessory)'] span.fine-print-body.gray.term-css.ng-binding")
	private List<WebElement> eipMonthTerms;

	@FindBy(xpath = "//p[contains(.,'Down + tax')]")
	private List<WebElement> eipDownPayment;

	@FindBy(css = "div[ng-click=\"$ctrl.payMonthlyFunction()\"] span[ng-class=\"$ctrl.payMonthlyActive ? 'payMonthlyActive' : 'pay-monthly-inactive'\"]")
	private WebElement payMonthly;

	@FindBy(css = "p.font-16.p-t-5.color-262626.ng-binding")
	private WebElement monthlyAmount;

	@FindBy(css = "div[ng-show*='showPriceLockupBar'] div[ng-click*='payFullFunction()'] p")
	private WebElement payInFullAmountWithEIP;

	@FindBy(css = "div[ng-show*='showBadCreditLockupBar'] div[ng-click*='payFullFunction()'] p.font-16")
	private WebElement payInFullAmountBadCredit;

	@FindBy(css = "div[ng-show*='showFooterUnder69'] div[ng-click*='payFullFunction()'] p.font-16")
	private WebElement payInFullAmountNoEIP;

	@FindBy(css = "p.body-copy-description.hidden-xs.ng-binding")
	private WebElement eipTotalDownPayment;

	@FindBy(css = ".text-magenta[aria-hidden = 'false']")
	private List<WebElement> accessoryCatalogPromotion;

	@FindBy(xpath = "//p[@class = 'text-magenta disc ng-binding']/following-sibling::*[contains(@class,'price-month')]")
	private List<WebElement> accessoryCatalogPromotionFRP;

	@FindBy(xpath = "//div[contains(@ng-click,'navigateToPDPPage')]//del//..//..//span[contains(@class,'fine-print-body')]")
	private List<WebElement> accessoryCatalogPromotionEIPTermPayment;

	@FindBy(css = "div[ng-hide*='!$ctrl.hasPromo']")
	private WebElement promoBanner;

	@FindBy(css = "a[ng-click*='$ctrl.openSaveOnAccessoriesModal']")
	private WebElement seeDetailsLinkInPromoBanner;

	@FindBy(css = "[ng-if='!$ctrl.isAccessoryLegalEnabled']")
	private WebElement legalDisclousure;

	@FindBy(css = "div[ng-if*='$ctrl.isAccessoryLegalEnabled && ($ctrl.finalAccessoryData[$index].isEMIAvailable || $ctrl.finalAccessoryData[$index-1].isEMIAvailable)']")
	private List<WebElement> legalDisclousureBottom;

	@FindBy(css = "p[ng-if='$ctrl.payMonthlyActive ']")
	private WebElement iOSlegalDisclousureBottom;

	@FindBy(css = "p[ng-show*='$ctrl.payMonthlyActive']")
	private WebElement cancellationLegalText;

	public AccessoryPLPPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AccessoryPLPPage verifyAccessoryPLPPage() {
		try {
			checkPageIsReady();
			//verifyDuplicatedElements(this.getClass());
			waitForSpinnerInvisibility();
			getDriver().getCurrentUrl().contains("accessorylist");
			Reporter.log("Accessory product list page is displayed");
		} catch (Exception e) {
			Assert.fail("Accessory list page URL is not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL and return true.
	 */
	public boolean verifyAccessoryPLPPageURL() {
		waitforSpinner();
		checkPageIsReady();
		return getDriver().getCurrentUrl().contains("accessorylist");
	}

	public AccessoryPLPPage clickOkButton() {
		try {
			waitforSpinner();
			waitforSpinner();
			clickElementWithJavaScript(okButton);
		} catch (Exception e) {
			Assert.fail("OkButton is not available to click");
		}
		return this;
	}

	/**
	 * Verify Accessory Promo Text
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyAccessoryPromoText() {
		try {
			accessoryPromo.isDisplayed();
			Reporter.log("PromoText is displayed");
		} catch (Exception e) {
			Assert.fail("PromoText is not displayed");
		}
		return this;
	}

	/**
	 * verify selected Accessory check box state
	 * 
	 */
	public AccessoryPLPPage verifySelectedAccessoryCheckbox() {
		try {
			waitforSpinner();
			selectedAccessoryCheckbox.get(0).isEnabled();
			Reporter.log("CheckBox is available to select");
		} catch (Exception e) {
			Assert.fail("CheckBox is not available to select");
		}
		return this;
	}

	public AccessoryPLPPage verifyPayInMonthly() {
		try {
			payInMonthly.isDisplayed();
			Reporter.log("PayInMonthly is displayed");
		} catch (Exception e) {
			Assert.fail("PayInMonthly is not displayed");
		}
		return this;
	}

	/**
	 * Click Skip Accessories CTA
	 * 
	 * @return
	 */
	public AccessoryPLPPage clickSkipAccessoriesCTA() {
		checkPageIsReady();
		waitforSpinner();
		try {
			clickElement(skipAccessoriesLink);
			Reporter.log("SkipAccessoriesCTA is available to click");
		} catch (Exception e) {
			Assert.fail("SkipAccessoriesCTA is not available to click");
		}
		return this;
	}

	/**
	 * Close ModalDialog Window
	 */
	public AccessoryPLPPage clickCloseModalDialogWindow() {
		try {
			checkPageIsReady();
			closeModalDialogWindow.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("whycantisee")));
			Reporter.log("ModalDialogWindow is isplayed to click");
		} catch (Exception e) {
			Assert.fail("ModalDialogWindow is not displayed to click");
		}
		return this;
	}

	/**
	 * Click PriceLockupBar Continue Button
	 */
	public AccessoryPLPPage clickPriceLockupBarContinueButton() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.elementToBeClickable(continueButtonEIPeligible));
			clickElementWithJavaScript(continueButtonEIPeligible);
			Reporter.log("Continue button from Sticky Banner is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Continue button in Sticky Banner");
		}
		return this;
	}

	/**
	 * Click First Accessory
	 */
	public AccessoryPLPPage clickFirstAccesory() {
		try {
			clickElementWithJavaScript(accessoryListImages.get(1));
			Reporter.log("Clicked on first accessory image");
		} catch (Exception e) {
			Reporter.log("AccessoryListImages is not available to click");
		}
		return this;
	}

	/**
	 * Click Continue Button
	 * 
	 * @return
	 */
	public AccessoryPLPPage clickContinueButton() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(continueButton), 7);
			clickElementWithJavaScript(continueButton);
			Reporter.log("Continue Button is available to click");
		} catch (Exception e) {
			Assert.fail("Continue Button is not available to click");
		}
		return this;
	}

	/**
	 * Click Continue Button in accessories list page
	 */
	public AccessoryPLPPage continueButton() {
		try {
			checkPageIsReady();
			for (WebElement element : continueBtn) {
				if (element.isDisplayed()) {
					element.click();
				}
			}

			Reporter.log("Clicked on Continue Button on Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("continueButtonFRP is not available to click");
		}
		return this;
	}

	/**
	 * Click SkipAccessories Link
	 */
	public AccessoryPLPPage clickSkipAccessoriesLink() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(skipAccessoriesLink);
			Reporter.log("SkipAccessoriesLink is available to click");
		} catch (Exception e) {
			Assert.fail("SkipAccessoriesLink is not available to click");
		}
		return this;
	}

	/**
	 * Deselect Accessory device
	 */
	public AccessoryPLPPage deselectAccessoryDevice() {
		try {
			deselectAccessoryDevice.click();
			Reporter.log("AccessoryDevice is available to Deslect");
		} catch (Exception e) {
			Assert.fail("AccessoryDevice is not available to Deslect");
		}
		return this;
	}

	/**
	 * Click Pay Monthly Popup Close Button
	 * 
	 * @return
	 */
	public AccessoryPLPPage clickPayMonthlyPopupCloseButton() {
		checkPageIsReady();
		try {			
			waitforSpinner();
			if (!payMonthlyPopupCloseButton.isEmpty()) {
				payMonthlyPopupCloseButton.get(0).click();
				Reporter.log("PayMonthlyPopupCloseButton is available");
			}
		} catch (Exception e) {
			Assert.fail("Pay Monthly Popup Close Button is not available to click");
		}
		return this;
	}

	/**
	 * Click Pay Monthly Pop-Up Ok Button
	 * 
	 * @return
	 */
	public AccessoryPLPPage clickPayMonthlyPopupOkButton() {
		try {
			Thread.sleep(10000);
			waitforSpinner();
			if (!payMonthlyPopupOkButton.isEmpty()) {
				payMonthlyPopupOkButton.get(0).click();
				Reporter.log("PayMonthly Popup Ok Button is available");
			}
		} catch (Exception e) {
			Assert.fail("Pay Monthly Popup Ok Button is not available to click");
		}
		return this;
	}

	/**
	 * Click Accessory device by name
	 */
	public AccessoryPLPPage clickAccesoriesDeviceByName(String accesoriesName) {
		try {
			waitforSpinner();
			if (getDriver().getCurrentUrl().contains("accessorylist")) {
				for (WebElement webElement : accesoriesList) {
					if (webElement.getText().contains(accesoriesName)) {
						clickElementWithJavaScript(webElement);
						break;
					} else {
						accesoriesList.get(0).click();
						break;
					}
				}
			}
			Reporter.log("AccesoriesDeviceByName is available to click");
		} catch (Exception e) {
			Assert.fail("AccesoriesDeviceByName is not available to click");
		}
		return this;
	}

	/**
	 * Click Accessories Continue Button in accessories list page
	 */
	public AccessoryPLPPage clickAccessoriesContinueButton() {
		try {
			checkPageIsReady();
			if (continueButtonFRP.isDisplayed()) {
				clickElementWithJavaScript(continueButtonFRP);
			} else if (continueButtonEIPeligible.isDisplayed()) {
				clickElementWithJavaScript(continueButtonEIPeligible);
			} else if (continueButtonBadCredit.isDisplayed()) {
				clickElementWithJavaScript(continueButtonBadCredit);
			}
			Reporter.log("Clicked on Continue Button on Accessories PLP page");
		} catch (Exception e) {
			Assert.fail("continueButtonFRP is not available to click");
		}
		return this;
	}

	/**
	 * Verify FRP Price Text on accessories PLP
	 */
	public AccessoryPLPPage verifyFRPPrice() {
		try {
			waitforSpinner();
			for (WebElement FRPPriceText : verifyFRP) {
				Assert.assertTrue(FRPPriceText.getText().contains("Total"), "FRP price text is not displayed");
				Assert.assertTrue(FRPPriceText.getText().contains("$"), "FRP price is not displayed");
			}
			Reporter.log("FRP Price and Text is displayed for all the devices");
		} catch (Exception e) {
			Assert.fail("Failed to display FRP Price and Text");
		}
		return this;
	}

	/**
	 * Verify EIP Price on accessories PLP
	 */
	public AccessoryPLPPage verifyEIPPrice() {
		try {

			waitforSpinner();
			for (WebElement EIPPrice : verifyEIP) {
				Assert.assertTrue(EIPPrice.getText().contains("$"), "EIP price is not displayed");
			}
			Reporter.log("EIP Price is displayed for all the devices");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP Price");
		}
		return this;
	}

	/**
	 * Verify EIP Month Terms on accessories PLP
	 */
	public AccessoryPLPPage verifyEIPMonthTerms() {
		try {

			waitforSpinner();
			for (WebElement EIPMonthTerms : eipMonthTerms) {
				Assert.assertTrue(EIPMonthTerms.isDisplayed(), "EIP Monthly terms are not displayed");
				Assert.assertTrue(
						EIPMonthTerms.getText().contains("9")
								| EIPMonthTerms.getText().contains("12")
								| EIPMonthTerms.getText().contains("24")
								| EIPMonthTerms.getText().contains("36"),
						"EIP Monthly terms are not correct for some devices in accessories PLP page");
			}
			Reporter.log("EIP Monthly terms are present for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP Monthly Terms");
		}
		return this;
	}

	/**
	 * Verify EIP DownPayment for upgrade users
	 */
	public AccessoryPLPPage verifyDownPaymentPriceForAllAccessoriesInPLP() {
		try {
			waitforSpinner();
			for (WebElement EIPPrice : eipDownPayment) {
				Assert.assertTrue(EIPPrice.getText().contains("$"), "EIP DownPayment price is not displayed.");
				Assert.assertTrue(EIPPrice.getText().toLowerCase().contains("down"),
						"EIP DownPayment text is not displayed");
			}
			Reporter.log("EIP DownPayment Price is displayed for all the devices.");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP DownPayment Price.");
		}
		return this;
	}

	/**
	 * Click Pay in Monthly Button in accessories list page
	 */
	public AccessoryPLPPage clickOnPayMonthly() {
		try {
			waitforSpinner();
			payMonthly.click();
			Reporter.log("Pay Monthly button is available");
		} catch (Exception e) {
			Assert.fail("Pay Monthly Button is not available to click");
		}
		return this;
	}

	/**
	 * Get Monthly Amount in accessories list page
	 */
	public String getMomnthlyAmount() {
		checkPageIsReady();
		String amount;
		amount = monthlyAmount.getText().replace("$", "");
		return amount;
	}

	/**
	 * Verify EIP Month Terms NOT displayed on accessories PLP
	 */
	public AccessoryPLPPage verifyPayMonthlyMonthsTermsNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(monthlyAmount.getText().contains("x mos"),
					"Pay Monthly month terms are displayed accessories PLP page");
			Reporter.log("Month terms are not displayed in Pay Monthly section");
		} catch (Exception e) {
			Assert.fail("Pay Monthly Month terms are displayed");
		}
		return this;
	}

	/**
	 * 
	 * Get Monthly Amount Double format in accessories list page
	 */
	public Double getMonthlyAmountDouble() {
		String amount;
		amount = monthlyAmount.getText();
		String[] arr = amount.split("\\$", 0);
		return Double.parseDouble(arr[1]);

	}

	/**
	 * Select Accessory
	 * 
	 * @param numberOfAccessories
	 * @return
	 */
	public AccessoryPLPPage selectNumberOfAccessories(String numberOfAccessories) {
		checkPageIsReady();
		waitForSpinnerInvisibility();
		waitFor(ExpectedConditions.elementToBeClickable(skipAccessoriesLink));
		try {
			waitForSpinnerInvisibility();
			int numberOfAccessoryToSelect = Integer.parseInt(numberOfAccessories);
			if (numberOfAccessoryToSelect > 0) {
				for (int i = 0; i < numberOfAccessoryToSelect; i++) {
					waitFor(ExpectedConditions.elementToBeClickable(accessoryList.get(i)));
					moveToElement(accessoryList.get(i));
					clickElement(accessoryList.get(i));
					waitforSpinner();
					Reporter.log("Accessory is available to select");
				}
			} else {
				clickElementWithJavaScript(accessoryList.get(0));
				Reporter.log("Selected 1st accessory");
			}
		} catch (Exception e) {
			Assert.fail("Failed to select Accessory");
		}
		return this;
	}

	/*
	 * Verify Sticky Banner
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyStickyBanner() {
		waitforSpinner();
		try {
			waitFor(ExpectedConditions.visibilityOf(stickyBanner));
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(stickyBanner.isDisplayed(), "Sticky banner is not displayed");
				Reporter.log("Sticky banner is displayed");
				//stickyBannerInAccessoryListPage
			} else {
				Assert.assertTrue(stickyBanner.isDisplayed(), "Sticky banner is not displayed");
				Reporter.log("Sticky banner is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify sticky banner");
		}
		return this;

	}

	/**
	 * Get eip total Amount in accessories list page
	 */
	public String getPayInFullPrice() {
		String eipTotal = null;
		if (payInFullAmountWithEIP.isDisplayed()) {
			eipTotal = payInFullAmountWithEIP.getText();
		} else if (payInFullAmountBadCredit.isDisplayed()) {
			eipTotal = payInFullAmountBadCredit.getText();
		} else if (payInFullAmountNoEIP.isDisplayed()) {
			eipTotal = payInFullAmountNoEIP.getText();
		}
		return eipTotal;
	}

	/**
	 * 
	 * Get EIP Total Amount integer format in accessories list page
	 */
	public Double getEIPTotalPriceInteger() {
		String[] arr = getPayInFullPrice().split("\\$", 0);
		return Double.parseDouble(arr[1]);

	}

	/**
	 * Verify if the EIP total price is greater than $69
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyEIPTotalPrice(Double value) {
		try {
			if (value > 69.99) {
				Reporter.log("Total price is greater than $69.99");
			} else {
				Assert.fail("Total price is less than 69. Add accessories to see EIP options");
			}
		} catch (Exception e) {
			Assert.fail("This device dont have EIP eligible accessories");
		}
		return this;
	}

	/**
	 * Verify Dollar sign in Total Price
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyTotalPriceDollarSign() {
		try {
			waitforSpinner();
			Assert.assertTrue(getPayInFullPrice().contains("$"), "EIP Total price is not displayed with $");
			Reporter.log("EIP Total Price is displayed with $");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Total Price with $");
		}
		return this;
	}

	/**
	 * Verify Total Monthly Payment
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyTotalMonthlyPayment() {
		try {
			Assert.assertTrue(monthlyAmount.isDisplayed(), "Total Monthly Payment is not displayed.");
			Assert.assertTrue(monthlyAmount.getText().contains("$"), "Total Monthly Payment is not displayed with $");
			Reporter.log("Total Monthly Payment is displayed with dollar symbol.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Total Monthly Payment with dollar symbol.");
		}
		return this;
	}

	/**
	 * Get Accessory Eip Monthly amount Integer format
	 * 
	 * @return
	 */
	public Double getSumOfAccessoriesDueMonthlyInteger(String numberOfAccessories) {
		Double sum = 0.00;
		int count = 0;
		int numberOfAccessoryToSelect = Integer.parseInt(numberOfAccessories);
		try {
			String eipMonthlyPayment = null;
			for (WebElement eipMonthly : verifyEIP) {
				eipMonthlyPayment = eipMonthly.getText();
				eipMonthlyPayment = eipMonthlyPayment.substring(eipMonthlyPayment.lastIndexOf("$") + 1);
				sum = sum + Double.parseDouble(eipMonthlyPayment);
				count++;
				if (count == numberOfAccessoryToSelect)
					break;
			}
			Reporter.log("Calculated Eip Monthly Total is " + sum);
		} catch (Exception e) {
			Assert.fail("Failed to identify Accessory Eip Monthly Payment Summary.");
		}
		return sum;
	}

	/**
	 * Get Total Monthly Payment Integer format
	 * 
	 * @return
	 */
	public Double getTotalMonthlyPaymentIntegerCartPage() {
		String monthlyPayment;
		monthlyPayment = monthlyAmount.getText();
		String monthlyPaymentSum = monthlyPayment.substring(monthlyPayment.lastIndexOf("$") + 1);
		Reporter.log("Total monthly sum is " + monthlyPaymentSum);
		return Double.parseDouble(monthlyPaymentSum);
	}

	/**
	 * Get Due Today Total amount Integer format
	 * 
	 * @return
	 */
	public Double getTotalDownIntegerCartPage() {
		String dueTodayPayment;
		dueTodayPayment = eipTotalDownPayment.getText();
		int dollarIndex = dueTodayPayment.indexOf("$");
		int downIndex = dueTodayPayment.indexOf("d");
		dueTodayPayment = dueTodayPayment.substring(dollarIndex + 1, downIndex - 1);
		return Double.parseDouble(dueTodayPayment);
	}

	/**
	 * Get Accessory Due Today amount Integer format
	 * 
	 * @return
	 */
	public Double getSumOfAccessoriesDownPaymentInteger(String numberOfAccessories) {
		Double sumdownPayment = 0.00;
		int count = 0;
		int numberOfAccessoryToSelect = Integer.parseInt(numberOfAccessories);
		try {
			String downPayment = null;
			for (WebElement duedownPayment : eipDownPayment) {
				if (count == numberOfAccessoryToSelect)
					break;
				downPayment = duedownPayment.getText();
				int dollarIndex = downPayment.indexOf("$");
				int downIndex = downPayment.indexOf("D");
				downPayment = downPayment.substring(dollarIndex + 1, downIndex - 1);
				sumdownPayment = sumdownPayment + Double.parseDouble(downPayment);
				count++;
			}
			Reporter.log("Calculated Total sum of Downpayment is " + sumdownPayment);
		} catch (Exception e) {
			Assert.fail("Failed to identify Accessory Eip Monthly Payment Summary.");
		}
		return sumdownPayment;
	}

	/**
	 * Verify that monthly amount from accessory page is equal to Total monthly
	 * amount from Cart page
	 */
	public AccessoryPLPPage compareAccessoryDownpaymentAmounts(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue,
					" Total Downpayment amount from Accessory is not matched with Downpayment amount.");
			Reporter.log("Downpayment amount from Accessory is matched with Total Downpayment amount.");
		} catch (Exception e) {
			Assert.fail("Failed to compare Total Downpayment Amount with Downpayment from accesory page.");
		}
		return this;
	}

	/**
	 * Verify that monthly amount from accessory page is equal to Total monthly
	 * amount from Cart page
	 */
	public AccessoryPLPPage compareAccessoryDueMonthlyAmounts(double firstValue, double secondValue) {
		try {

			DecimalFormat df = new DecimalFormat("0.00");
			String formate = df.format(secondValue);
			double finalValue = Double.parseDouble(formate);

			Assert.assertEquals(firstValue, finalValue,
					" Total Monthly amount from Accessory is not matched with EIP Monthly Total amount from cart.");
			Reporter.log("EIP Monthly amount from Accessory is matched with Total Monthly amount from cart.");
		} catch (Exception e) {
			Assert.fail("Failed to compare Total Monthly Amount with EIP Monthly from accesory page.");
		}
		return this;
	}

	/**
	 * Select number of accessories to be EIP eligible
	 * 
	 * @return AccessoryPLPPage
	 */
	public AccessoryPLPPage selectNumberOfAccessoriesToBeEIPeligible() {
		try {
			waitforSpinner();
			for (int i = 0; i <= accessoryList.size(); i++) {
				scrollToElement(accesoriesList.get(i));
				accessoryList.get(i).click();
				if (getEIPTotalPriceInteger() > 69.99) {
					break;
				}
				int accessoryNumber = i + 1;
				Reporter.log("Accessory #" + accessoryNumber + " have been selected");
			}
		} catch (Exception e) {
			Assert.fail("Failed to select Accessory");
		}
		return this;
	}

	/**
	 * // * Verify Accessory with Promotion // * // * @return //
	 */
	public AccessoryPLPPage verifyAccessoryWithCatalogPromotion() {
		try {
			waitforSpinner();
			checkPageIsReady();
			if (accessoryCatalogPromotion.size() > 0) {
				Assert.assertTrue(accessoryCatalogPromotion.get(0).isDisplayed(),
						"Accessory with Catalog Promomtion is not displayed.");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display Accessory with Catalog Promomtion");
		}
		return this;
	}

	/**
	 * // * Verify Accessory with Promotion // * // * @return //
	 */
	public AccessoryPLPPage verifyFRPOfAccessoryWithCatalogPromotion() {
		try {
			if (accessoryCatalogPromotionFRP.size() > 0) {
				Assert.assertTrue(accessoryCatalogPromotionFRP.get(0).isDisplayed(),
						"Accessory with Catalog Promomtion is not displayed.");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display Accessory with Catalog Promomtion");
		}
		return this;
	}

	/**
	 * // * Get Accessory FRP with Promotion // * // * @return //
	 */
	public Double getFRPOfAccessoryWithCatalogPromotion() {

		String promoPriceText = accessoryCatalogPromotionFRP.get(0).getText();
		String arr[] = promoPriceText.split(" ");
		String promoPrice = arr[3].substring(1, arr[3].length());
		return Double.parseDouble(promoPrice);
	}

	/**
	 * // * Verify Accessory Downpayment with Promotion // * // * @return //
	 */
	public AccessoryPLPPage verifyDownPaymentOfAccessoryWithCatalogPromotion() {
		try {
			if (eipDownPayment.size() > 0) {
				Assert.assertTrue(eipDownPayment.get(0).isDisplayed(),
						" Downpayment of Accessory with Catalog Promomtion is not displayed.");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display downpayment Accessory with Catalog Promomtion");
		}
		return this;
	}

	/**
	 * // * Get Accessory FRP with Promotion // * // * @return //
	 */
	public Double getDownPaymentOfAccessoryWithCatalogPromotion() {

		String promoDPPricetext = eipDownPayment.get(0).getText();
		String arr[] = promoDPPricetext.split(" ");
		String promoDPPrice = arr[0].substring(1, arr[0].length());
		return Double.parseDouble(promoDPPrice);

	}

	/**
	 * // * Verify Accessory Downpayment with Promotion // * // * @return //
	 */
	public AccessoryPLPPage verifyEIPPaymentOfAccessoryWithCatalogPromotion() {
		try {
			if (verifyEIP.size() > 0) {
				Assert.assertTrue(verifyEIP.get(0).isDisplayed(),
						" EIP of Accessory with Catalog Promomtion is not displayed.");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display EIP of Accessory with Catalog Promomtion");
		}
		return this;
	}

	/**
	 * // * Get Accessory FRP with Promotion // * // * @return //
	 */
	public Double getEIPPaymentOfAccessoryWithCatalogPromotion() {

		String promoEIPPricetext = verifyEIP.get(0).getText();

		String arr[] = (promoEIPPricetext.split("\\s"));
		String promoEIPPrice = arr[1].substring(1, arr[0].length());
		return Double.parseDouble(promoEIPPrice);
	}

	/**
	 * // * Verify Accessory Downpayment with Promotion // * // * @return //
	 */
	public AccessoryPLPPage verifyEIPTermOfAccessoryWithCatalogPromotion() {
		try {
			if (accessoryCatalogPromotionEIPTermPayment.size() > 0) {
				Assert.assertTrue(accessoryCatalogPromotionEIPTermPayment.get(0).isDisplayed(),
						" EIP Term of Accessory with Catalog Promomtion is not displayed.");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display EIP Term of Accessory with Catalog Promomtion");
		}
		return this;
	}

	/**
	 * // * Get Accessory EIP Term with Promotion // * // * @return //
	 */
	public Double getEIPTermOfAccessoryWithCatalogPromotion() {

		String promoEIPTermPricetext = accessoryCatalogPromotionEIPTermPayment.get(0).getText();
		String arr[] = promoEIPTermPricetext.split(" ");
		return Double.parseDouble(arr[1]);

	}

	/**
	 * Click First Promo Accessory device by
	 */
	public AccessoryPLPPage clickFirstPromoAccessory() {
		try {
			clickElementWithJavaScript(accessoryCatalogPromotion.get(0));
			Reporter.log("Promo AccesoriesDevice is available to click");
		} catch (Exception e) {
			Assert.fail("Promo AccesoriesDevice is not available to click");
		}
		return this;
	}

	/**
	 * Verify Promo Banner
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyPromoBanner() {
		try {
			checkPageIsReady();
			if (!promoBanner.isDisplayed()) {
				Reporter.log("Promo banner is not displayed");
			} else {
				Assert.assertTrue(seeDetailsLinkInPromoBanner.isDisplayed(),
						"'See details' link is not displayed in promo banner");
				Reporter.log("Promo banner is displayed");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Promo Banner");
		}
		return this;
	}

	/**
	 * Verify legal disclousure under each accessory is removed
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyLegalDisclousureforEachAccessoryRemoved() {

		try {
			checkPageIsReady();
			if (legalDisclousure.isDisplayed()) {
				Assert.fail("legal disclousure is displayed -failed");

			}
		} catch (Exception e) {
			Reporter.log("legal disclousure is not displayed ");

		}
		return this;
	}

	/**
	 * Verify legal disclousure at bottom of page
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyLegalDisclousureatEachRow() {

		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				iOSlegalDisclousureBottom.isDisplayed();
				Reporter.log("legal disclousure  at bottom   displayed");
			} else {
				int counter = legalDisclousureBottom.size();
				for (int i = 0; i < counter; i++) {
					legalDisclousureBottom.get(i).isDisplayed();
					String legalText = legalDisclousureBottom.get(i).getText();
					Assert.assertTrue(legalText.contains("If you cancel wireless service"),
							"legal disclousure at bottom  not displayed.");
					Reporter.log("legal disclousure  at bottom   displayed ");
				}
			}
		} catch (Exception e) {
			Assert.fail("legal disclousure  at bottom  not  displayed -failed");
		}
		return this;
	}

	/**
	 * Click Accessory device by name
	 */
	public AccessoryPLPPage verifyDeviceNameAndManufacturerName() {
		try {
			waitforSpinner();
			if (getDriver().getCurrentUrl().contains("accessories")) {
				for (WebElement webElement : accesoriesList) {
					// webElement.findElement(By.cssSelector("accesoriesName")).getText().contains(accesoriesName);
					if (webElement.getText() != null) {
						Reporter.log("Manufacturer Name and Device Name are displayed for One Accessory");
					} else {
						Assert.fail("Manufacturer Name and Device Name are Not displayed for One Accessory");
					}
				}
			}
			Reporter.log("Manufacturer Name and Device Name are displayed for All Accessories");
		} catch (Exception e) {
			Assert.fail("Falied to verify Manufacturer Name and Device Name displayed for All Accessories");
		}
		return this;
	}

	/*
	 * Verify Cancellation Legal Text
	 * 
	 * @return
	 */
	public AccessoryPLPPage verifyCancellationLegalText() {
		waitforSpinner();
		try {
			waitFor(ExpectedConditions.visibilityOf(cancellationLegalText));
			Assert.assertTrue(cancellationLegalText.isDisplayed(), "Cancellation Legal Text is not displayed");
			Reporter.log("Cancellation Legal Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Cancellation Legal Text");
		}
		return this;
	}

	/**
	 * Select number of accessories greater than $49
	 * 
	 * @return AccessoryPLPPage
	 */
	public AccessoryPLPPage selectNumberOfAccessoriesMoreThan49() {
		try {
			waitforSpinner();
			for (int i = 0; i <= accessoryList.size(); i++) {
				scrollToElement(accesoriesList.get(i));
				accessoryList.get(i).click();
				if (getEIPTotalPriceInteger() > 49) {
					break;
				}
				int accessoryNumber = i + 1;
				Reporter.log("Accessory #" + accessoryNumber + " have been selected");
			}
		} catch (Exception e) {
			Assert.fail("Failed to select Accessory");
		}
		return this;
	}
}
