package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class CheckOutTheCoveragePage extends TmngCommonPage{
private final String pageUrl = "/coverage";
	
	public CheckOutTheCoveragePage(WebDriver webDriver) {
		super(webDriver);
	}

	public CheckOutTheCoveragePage verifyCheckOutTheCoveragePage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(pageUrl),60);
		} catch (Exception e) {
			Assert.fail("CheckOutTheCoverage URL is not displayed");
		}
		return this;
		
	}
	
	
}
