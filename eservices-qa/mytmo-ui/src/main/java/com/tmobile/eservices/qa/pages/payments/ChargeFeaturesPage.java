package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class ChargeFeaturesPage extends CommonPage {

	private final String pageUrl = "charges/category/charge/Features";

	@FindBy(css = ".d-none.d-lg-inline-block")
	private WebElement backToSummaryLink;

	@FindBy(css = "img[alt='back']")
	private WebElement mobileBackToSummaryLink;

	@FindBy(css = "i.fa.fa-angle-left")
	private WebElement arrowLeft;

	@FindBy(css = "i.fa.fa-angle-right")
	private WebElement arrowRight;

	@FindBy(css = "bb-slider div.bb-slider-item-amount")
	private List<WebElement> amountBlock;

	@FindBy(css = "img.loader-icon")
	private WebElement pageSpinner;

	@FindBy(css = "span.bb-back")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//bb-slider//div[contains(text(),'Services')]")
	private WebElement servicesTabInSlider;

	@FindBy(css = "a.bb-taxes-link")
	private WebElement viewIncludedTaxesAndFeesLink;

	@FindBy(css = "div.bb-taxes-title")
	private WebElement includedTaxesAndFeesModal;

	@FindBy(css = "img.bb-close-image")
	private WebElement includedTaxesAndFeesModalCloseBtn;

	@FindBy(css = "div.bb-charge-details")
	private WebElement ServiceLineDetails;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public ChargeFeaturesPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ChargeFeaturesPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @throws InterruptedException
	 */
	public ChargeFeaturesPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			// waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Fail to load Charge Feature page");
		}
		return this;
	}

	/**
	 * verify that Services is displayed on the slider
	 * 
	 * @return
	 */
	public ChargeFeaturesPage verifyServicesInSlider() {
		try {
			servicesTabInSlider.isDisplayed();
			Reporter.log("One Time Charge slider is displayed");
		} catch (Exception e) {
			Assert.fail("One Time Charge slider is not present");
		}
		return this;
	}

	/**
	 * Click on View included taxes and fees link.
	 *
	 * @throws InterruptedException
	 */
	public ChargeFeaturesPage clickViewInlcudedTaxesFeesLink() {
		try {
			viewIncludedTaxesAndFeesLink.click();
			Reporter.log("Clicked on View included taxes and fees link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on View included taxes and fees link.");
		}
		return this;
	}

	/**
	 * Verify that the Included Taxes And Fees Modal loaded completely.
	 *
	 */
	public ChargeFeaturesPage verifyIncludedTaxesAndFeesModal() {
		try {
			includedTaxesAndFeesModal.isDisplayed();
			Assert.assertTrue(includedTaxesAndFeesModal.getText().equals("Included taxes & fees"),
					"Expected text is not displayed on modal header");
			Reporter.log("Included Taxes And Fees Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Included Taxes And Fees Modal is not present");
		}
		return this;
	}

	/**
	 * Verify that the Included Taxes And Fees Modal loaded completely.
	 *
	 */
	public ChargeFeaturesPage verifyExcludedTaxesAndFeesModal() {
		try {
			includedTaxesAndFeesModal.isDisplayed();
			Assert.assertTrue(includedTaxesAndFeesModal.getText().equals("Excluded government taxes & fees"),
					"Expected text is not displayed on modal header");
			Reporter.log("Excluded government taxes & fees Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Excluded government taxes & fees modal is not found");
		}
		return this;
	}

	/**
	 * Verify if Services details assigned to Lines are displayed
	 *
	 */
	public ChargeFeaturesPage verifyServiceDetailsofAllLines() {
		try {
			ServiceLineDetails.isDisplayed();
			Reporter.log("Service Line Details are displayed");
		} catch (Exception e) {
			Assert.fail("Service Line Details are not found");
		}
		return this;
	}

	/**
	 * Click on Included taxes and fees modal close button.
	 *
	 */
	public ChargeFeaturesPage clickInlcudedTaxesFeesModalCloseBtn() {
		try {
			includedTaxesAndFeesModalCloseBtn.click();
			Reporter.log("Clicked on included taxes and fees Modal close Button.");
		} catch (Exception e) {
			Assert.fail("Fail to click on included taxes and fees Modal close Button.");
		}
		return this;
	}

	/**
	 * Verify page elements
	 *
	 * @throws InterruptedException
	 */
	public ChargeFeaturesPage verifyChargeFeaturePageElements() {
		try {
			Assert.assertTrue(backToSummaryLink.isDisplayed(), "Back To Summary link is not displayed");
			Assert.assertTrue(servicesTabInSlider.isDisplayed(), "Equipment header is not displayed");
			Assert.assertTrue(arrowLeft.isDisplayed(), "Arrow left is not displayed");
			Assert.assertTrue(arrowRight.isDisplayed(), "Arrow right is not displayed");
			for (WebElement amount : amountBlock) {
				Assert.assertTrue(amount.getText().contains("$"), "Dollar sign is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Fail to verify Charge Feature page elements");
		}
		return this;
	}

	/**
	 * Verify the slider actions in page
	 *
	 * @throws InterruptedException
	 * 
	 */
	public ChargeFeaturesPage verifyPageSliderActions() {
		try {
			clickElement(arrowRight);
			clickElement(arrowLeft);
			Assert.assertTrue(servicesTabInSlider.isDisplayed(), "Failed to verify slider actions");
		} catch (Exception e) {
			Assert.fail("Failed to verify the slider actions(Left Arrow/Right Arrow)");
		}
		return this;
	}

	/**
	 * Click Back to Summary page link
	 *
	 */
	public ChargeFeaturesPage clickBackToSummaryLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileBackToSummaryLink.click();
			} else {
				backToSummaryLink.click();
			}
			Reporter.log("Clicked on Back To Summary Link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on Back To Summary Link.");
		}
		return this;
	}
}
