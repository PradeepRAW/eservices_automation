/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class ChangeSimPage extends CommonPage {

	@FindBy(css = "div.padding-vertical-large h6.text-gray-dark")
	private WebElement changeSimPage;


	public ChangeSimPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Change SIM Page
	 * 
	 * @return
	 */
	public ChangeSimPage verifyChangeSimPage() {
		try {
			changeSimPage.isDisplayed();
			Reporter.log("Navigated to Change Sim Page");
		} catch (Exception e) {
			Assert.fail("Change Sim Page is Not Displayed");
		}
		return this;
	}


}
