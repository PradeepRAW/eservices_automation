package com.tmobile.eservices.qa.api.unlockdata;

/**
 * @author blakshminarayana
 *
 */
public interface PDFService {
	void savePDF(String pdfURL, String filename);

	boolean validateContentInPDF(String filename, String content);
}
