/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class FamilyWherePage extends CommonPage {

	private static final String pageUrl = "familywhere";
	private static final String thirdPartyUrl = "https://t-mobilefamilywhere.com/tmoRegistration.";

	public FamilyWherePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify FamilyWhere Page.
	 *
	 * @return the FamilyWhere page class instance.
	 */
	public FamilyWherePage verifyFamilyWherePage() {
		try {
			verifyPageUrl(pageUrl);
			Reporter.log("FamilyWhere page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("FamilyWhere page not displayed");
		}
		return this;
	}
	
	/**
	 * Verify FamilyWhere Page navigated to Third party URL.
	 *
	 * @return the FamilyWhere page class instance.
	 */
	public FamilyWherePage verifyFamilyWherePageNavigatedToThirdPartyUrl() {
		try {
			verifyCurrentPageURL("mobilefamilywhere", "mobilefamilywhere");
		//	verifyPageUrl(thirdPartyUrl);
			Reporter.log("Navigated to third party Url");
		} catch (NoSuchElementException e) {
			Assert.fail("unable to validate the third party url");
		}
		return this;
	}
}
