
/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author anandgarpawar
 *
 */
public class ChangePlanDeviceBreakdownPage extends CommonPage {

	// Headers
	@FindBy(css = "span[aria-label='Your Devices']")
	private WebElement headingOfDeviceBreakDownPage;

	@FindBy(css = ".body.padding-top-small")
	private WebElement subHeadingOfDeviceBreakDownPage;

	@FindBy(css = "div[aria-label='Total monthly device cost']")
	private WebElement textTotalMonthlyDeviceCost;

	@FindBy(css = "p.Display5")
	private WebElement totalMonthlyDeviceCostAmount;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement lineNameOnDeviceBreakdownPage;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement lineNumberOnDeviceBreakdownPage;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement priceOnLineLevelOnDeviceBreakdownPage;

	@FindBy(xpath = "//*[@class='SecondaryCTA blackCTA text-balck full-btn-width-xs']")
	private WebElement backToSummaryCTAOnDeviceBreakdownPage;

	@FindBy(css = "p.Display6")
	private List<WebElement> amountsAtEachLineLevel;

	@FindBy(xpath = "//*[@class=' black float-right']")
	private WebElement totalMonthlyCost;

	@FindBy(css = "p.black.Display4")
	private List<WebElement> namesOfEachLine;

	@FindBy(css = "p.H6-heading")
	private List<WebElement> msisdnOfEachLine;

	/**
	 * 
	 * @param webDriver
	 */
	public ChangePlanDeviceBreakdownPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Check header of Plans Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanDeviceBreakdownPage checkHeaderOfPlansBreakDownPage() {
		try {
			Assert.assertTrue(headingOfDeviceBreakDownPage.isDisplayed(),
					"Header of Device Breakdown page is mismatched");
			Reporter.log("Header of Device details page is matched.");
		} catch (Exception e) {
			Assert.fail("Header of Device Details page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Subheader of Plans Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanDeviceBreakdownPage checkSubHeaderOfPlansBreakDownPage() {
		try {
			Assert.assertTrue(
					subHeadingOfDeviceBreakDownPage.getText()
							.contains("Here’s the cost breakdown for the devices on your account"),
					"SubHeader of Device details Breakdown page is mismatched");
			Reporter.log("Sub Header of device details page is matched.");
		} catch (Exception e) {
			Assert.fail("Sub Header of Device details page is not displayed.");
		}
		return this;
	}

	/**
	 * Check text Total Monthly Cost on Device Breakdown page
	 * 
	 * @return
	 */
	public ChangePlanDeviceBreakdownPage checkTotalMonthlyDeviceCostText() {
		try {
			Assert.assertTrue(textTotalMonthlyDeviceCost.getText().contains("Total monthly device cost"),
					"Text Total Monthly Device cost on Device details Breakdown page is mismatched");
			Reporter.log("Text Total Monthly Device cost on Device details Breakdown page is matched");
		} catch (Exception e) {
			Assert.fail("Text Total Monthly Device cost on Device details Breakdown page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Total cost for each line
	 * 
	 * @return
	 */
	public double calculateTotalCostForEachLine() {
		double sum = 0;
		try {

			ArrayList<Double> lineCosts = getNumbersFromList(amountsAtEachLineLevel);
			for (int counter = 0; counter < lineCosts.size(); counter++) {
				sum = sum + lineCosts.get(counter);
			}

		} catch (Exception e) {
			Assert.fail("Not able to calculate cost.");
		}
		return sum;
	}

	/**
	 * Check Total Monthly Cost
	 * 
	 * @return
	 */
	public Double calculateTotalMonthlyCostForSingleDevice() {

		String monthlyDeviceCost;
		Double totalMonthlyCostValue = null;
		try {
			monthlyDeviceCost = totalMonthlyDeviceCostAmount.getText();
			monthlyDeviceCost = monthlyDeviceCost.substring(1, monthlyDeviceCost.length());
			totalMonthlyCostValue = Double.parseDouble(monthlyDeviceCost);

		} catch (Exception e) {
			Assert.fail("Not able to calculate cost.");
		}
		return totalMonthlyCostValue;
	}

	/**
	 * Read Total Monthly Cost only
	 * 
	 * @return
	 */
	public double verifyTotalMonthlyCost() {
		double totalMonthlyCostValue = 0;
		try {
			String monthlyCost = totalMonthlyDeviceCostAmount.getText();
			monthlyCost = monthlyCost.substring(1, monthlyCost.length());
			totalMonthlyCostValue = Double.parseDouble(monthlyCost);
		} catch (Exception e) {
			Assert.fail("Not able to calculate Total Monthly cost.");
		}
		return totalMonthlyCostValue;
	}

	// Verify Total Monthly cost
	public ChangePlanDeviceBreakdownPage verifyAndCheckTotalMonthlyCost() {
		double totalMonthlyCost = 0;
		double totalCostAtLineLevel = 0;

		totalMonthlyCost = verifyTotalMonthlyCost();
		totalCostAtLineLevel = calculateTotalCostForEachLine();

		if (totalMonthlyCost == totalCostAtLineLevel)
			Reporter.log("Prices are matching in Total Monthly section");
		else
			Assert.fail("Prices are not matching in Total Monthly Section");
		return this;
	}

	// Verify Total Monthly cost for Single EIP Device
	public ChangePlanDeviceBreakdownPage verifyAndCheckTotalMonthlyCostWhenAccountHasOnlyOneDeviceOnEIP() {
		double totalMonthlyCost = 0;
		double totalCostAtLineLevel = 0;

		totalMonthlyCost = verifyTotalMonthlyCost();

		totalCostAtLineLevel = calculateTotalMonthlyCostForSingleDevice();

		if (totalMonthlyCost == totalCostAtLineLevel)
			Reporter.log("Prices are matching in Total Monthly section");
		else
			Assert.fail("Prices are not matching in Total Monthly Section");

		return this;
	}

	// Check Name of each line
	public ChangePlanDeviceBreakdownPage verifyNamesOfEachLine() {
		String lineName = null;
		int count = namesOfEachLine.size();
		for (int i = 0; i < count; i++) {
			lineName = namesOfEachLine.get(i).getText();
			if (lineName.equals(""))
				Assert.fail("Name is not displayed for line");
			else
				Reporter.log("Name displayed for line");
		}
		return this;
	}

	// Check MSISDN of each line
	public ChangePlanDeviceBreakdownPage verifyMSISDNOfEachLine() {
		String lineNumber = null;
		int count = msisdnOfEachLine.size();
		for (int i = 0; i < count; i++) {
			lineNumber = msisdnOfEachLine.get(i).getText();
			if (lineNumber.equals(""))
				Assert.fail("MSISDN is not displayed for line");
			else
				Reporter.log("MSISDN  displayed for line");
		}
		return this;
	}

	// Check MSISDN of each line
	public ChangePlanDeviceBreakdownPage verifyNameAndMSISDNOfEachLine() {
		verifyNamesOfEachLine();
		verifyMSISDNOfEachLine();
		return this;
	}

	/**
	 * Click on Back to Summary CTA
	 * 
	 * @return
	 */
	public ChangePlanDeviceBreakdownPage clickOnBackToSummaryCTA() {
		try {
			backToSummaryCTAOnDeviceBreakdownPage.isDisplayed();
			backToSummaryCTAOnDeviceBreakdownPage.click();
			Reporter.log("Back to Summary CTA is clicked on Device Breakdown page.");
		} catch (Exception e) {
			Assert.fail("Back to Summary CTA is not displayed on Device Breakdown page.");
		}
		return this;
	}

}
