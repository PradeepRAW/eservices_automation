package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class DCPTokenApi extends ApiCommonLib{
	
	/***
	 *  summary: post the request to create a new token
	 *  description: 'This method is used to create DCP Token '
	 *  Headers:*  Authorization
	 *  accept
	 *  applicationid
	 *  cache-control
	 *  channelid
	 *  clientid
	 *  content-type
	 *  usn
	 *  interactionid
	 *  transactionBusinessKey
	 *  phoneNumber
	 *  transactionbusinesskeytype
	 *  correlationid
	 *  transactionid
	 *  transactiontype
	 *  
	 * @return
	 * @throws Exception
	 */
	public String createPlattoken(ApiTestData apiTestData) throws Exception {

		try {
			String requestBody = "{\"accountNumber\":\"" + apiTestData.getBan() + "\",\"phoneNumber\":\""
					+ apiTestData.getMsisdn() + "\"}";
			String correlationid = checkAndGetAccessToken();
			Map<String, String> headers = buildDCPTokenApiHeader(apiTestData);
			String resourceURL = "v1/initialize/plattoken/create";
	    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {	
				return correlationid;
			}

		} catch (Exception e) {
		}
		return null;

	}
	
	public String deletePlattoken(ApiTestData apiTestData) throws Exception {

		try {
			String requestBody = "{\"accountNumber\":\"" + apiTestData.getBan() + "\",\"phoneNumber\":\""
					+ apiTestData.getMsisdn() + "\"}";
			Map<String, String> headers = buildDCPTokenApiHeader(apiTestData);
			String resourceURL = "v1/initialize/plattoken/delete";
	    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.DELETE);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				return "Success";
			}

		} catch (Exception e) {
		}
		return null;

	}
	
    private Map<String, String> buildDCPTokenApiHeader(ApiTestData apiTestData) throws Exception {
    	    	
		Map<String, String> headers = new HashMap<String, String>();
		String correlationid = checkAndGetAccessToken();
		
		headers.put("Authorization", getAuthHeader());
		headers.put("accept", "application/json");
		headers.put("applicationid", "MYTMO");//
		headers.put("cache-control", "no-cache");
		headers.put("channelid", "WEB");
		headers.put("clientid", "ESERVICE");
		headers.put("content-type", "application/json");//
		headers.put("usn", "testUSN");
		headers.put("interactionid", "api-testing");
		headers.put("transactionBusinessKey", apiTestData.getBan());
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("transactionbusinesskeytype", "BAN");
		headers.put("correlationid", correlationid);
		headers.put("transactionid", "xasdf1234asa4444");
		headers.put("transactiontype", "UPGRADE");
		return headers;
	}

}
