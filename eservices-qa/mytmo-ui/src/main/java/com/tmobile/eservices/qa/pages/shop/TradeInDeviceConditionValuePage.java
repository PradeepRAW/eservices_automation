/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class TradeInDeviceConditionValuePage extends CommonPage {
	
	public static final String pageUrl = "tradeInDeviceConditionValue";
	public static final String deviceGood = "itsGood";
	public static final String deviceBad = "itsGotIssues";

	@FindBy(css = "h4.h3-title.h4-title-mobile")
	private WebElement deviceConditionsPage;

	@FindBy(css = "div#good p.Display5")
	private WebElement itsGood;

	@FindBy(css = "div#bad")
	private WebElement itsGotIsues;
	
	@FindBy(xpath = "//div[contains(@id,'good')]//p[contains(.,'good condition')]")
	private WebElement itsGoodConditionBox;


	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement acceptAndContinueButton;
	
	@FindBy(css = "div#bad")
	private WebElement itsGotIssuesRadioBtn;

	@FindBy(css = "div#good")
	private WebElement itsGoodRadioBtn;

	@FindBy(css = "div.padding-bottom-medium-sm li.body-copy-description.gray-light")
	private List<WebElement> badDevicePoints;

	@FindBy(css = "div.col-sm-offset-1 li.body-copy-description.gray-light")
	private List<WebElement> goodDevicePoints;

	@FindBy(css = "div.body-copy-highlight")
	private WebElement deviceDescription;

	@FindBy(css = "span.radio-ht")
	private List<WebElement> radioButtons;
	
	@FindBy(css = "div#tradeValue button[ng-click*='accept']")
	private WebElement continueTradeIn;

	@FindBy(css = "p.Display2.text-black")
	private WebElement eipBalance;

	@FindBy(css = "span.fine-print-body")
	private WebElement tradeInValueInDescription;
	
	@FindBy(css = "p.Display2")
	private WebElement tradeInValueHeader;
	
	@FindBy(css = "[ng-if*='selectedBulletPoints']")
	private WebElement bulletinFormatDiv;
	
	@FindBy(css = "div[ng-if*='isFirstPage'] div p")
	private List<WebElement> deviceConditionTexts;

	@FindBy(css = "[ng-click*='ctrl.phoneModal()']")
	private WebElement notSureWhatToSelectHyperlink;

	@FindBy(css = ".ico.ico-closeIcon.ng-scope")
	private WebElement popUpModal;

	@FindBy(css = ".text-bold.text-center.ng-binding")
	private WebElement popupModalHeader;

	@FindBy(css = ".ico-closeIcon.ng-scope")
	private WebElement modalCloseIcon;

	@FindBy(css = "[ng-repeat='list in $ctrl.modalPoints']")
	private List<WebElement> modalDescription;

	@FindBy(css = "h1.Display2")
	private WebElement tellUsConditionHeader;

	@FindBy(css = "[id='good'] img")
	private WebElement itsGoodImage;

	@FindBy(css = "[id='bad'] img")
	private WebElement itsGotIsuesImage;

	@FindBy(css = "div#good div p:first-child")
	private WebElement goodConditiontext;

	@FindBy(css = ".btn.PrimaryCTA-Normal")
	private WebElement ContinueButton;
	@FindBy(css = "div.row.no-margin div:nth-child(2) i")
	private WebElement deviceAcceptableLCDDisplay;

	@FindBy(xpath = "//div[contains(@id,'good')]//p[contains(.,'good condition')]")
	private WebElement goodConditionTitleAAL;
	
	@FindBy(xpath = "//div[contains(@id,'bad')]//p[contains(.,'It has issues')]")
	private WebElement badConditionTitleAAL;
	
	@FindBy(css = "div[id='good'] img[class*='img_width']")
	private WebElement goodDeviceImageAAL;
	
	@FindBy(css = "div[id='bad'] img[class*='img_width']")
	private WebElement badDeviceImageAAL;
	
	@FindBy(css = "div#good p[ng-repeat*='tradeInDeviceCondition']")
	private List<WebElement> goodConditionTextAAL;
	
	@FindBy(css = "div#bad p[ng-repeat*='tradeInDeviceCondition']")
	private List<WebElement> badConditionTextAAL;	
	
	@FindBy(css = "button[ng-click*='$ctrl.close()']")
	private WebElement notSureWindowCloseAAL;
	
	@FindBy(css = "div#tradeInQuestion span")
	private WebElement titleText;
	
	@FindBy(css = "div[ng-repeat*='tradeInDetail'] [tabindex='0']")
	private List<WebElement> assuredQuestions;
	
	@FindBy(css = "div[ng-repeat*='tradeInDetail'] [class*='SecondaryCTA ']")
	private List<WebElement> assuredQuestionsYesNo;
	
	@FindBy(id = "continueBtn")
	private WebElement continueCTANew;
	
	@FindBy(xpath="//button[@id='btnNo0']/../../div/div[2]")
	private WebElement locationQuestion;
	    
	@FindBy(xpath="//button[@id='btnNo1']/../../div/div[2]")
	private WebElement crackedScreenQuestion;
	    
	@FindBy(xpath="//button[@id='btnNo3']/../../div/div[2]")
	private WebElement batteryQuestion;
	    
	@FindBy(xpath="//button[@id='btnNo2']/../../div/div[2]")
	private WebElement waterDropQuestion;
	
	@FindBy(id="btnYes0")
	private WebElement findMyIphoneYesButton;
	
	@FindBy(id="btnNo0")
	private WebElement findMyIphoneNoButton;
	
	@FindBy(id="btnYes1")
	private WebElement acceptableLCDYesButton;
	
	@FindBy(id="btnNo1")
	private WebElement acceptableLCDNoButton;
	
	@FindBy(id="btnNo2")
	private WebElement waterDamageNoButton;
	
	@FindBy(id="btnYes2")
	private WebElement waterDamageYesButton;
	
	@FindBy(id="btnYes3")
	private WebElement devicePowerONYesButton;
	
	@FindBy(id="btnNo3")
	private WebElement devicePowerONNoButton;
	
	/**
	 * 
	 * @param webDriver
	 */
	public TradeInDeviceConditionValuePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Device Conditions Page
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyTradeInDeviceConditionValuePage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			getDriver().getCurrentUrl().contains(pageUrl);
			Reporter.log("Navigated to tradeInDeviceConditionValue page");
		} catch (Exception e) {
			Assert.fail("tradeInDeviceConditionValue page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Condition [Its Good]
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDeviceConditionAsItsGood() {
		try {
			Assert.assertTrue(itsGood.getText().contains("It's in good"));
			Reporter.log("It's good section is displayed");
		} catch (Exception e) {
			Assert.fail("It's good section is not displayed");
		}

		return this;
	}

	/**
	 * Verify Device Condition [Its Got Issues]
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDeviceConditionAsItsGotIssues() {
		try {
			Assert.assertTrue(itsGotIsues.isDisplayed(), "It's Got Issues section is not displayed");
			Reporter.log("It's Got Issues section is displayed");
		} catch (Exception e) {
			Assert.fail("t's Got Issues section is not displayed");
		}
		return this;
	}

	/**
	 * Click On Its Good Condition
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickOnItsGoodCondition() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(itsGoodConditionBox);
			Reporter.log("Clicked on Its Good Condition");
		} catch (Exception e) {
			Assert.fail("Its Good Condition is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Accept and continue Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickAcceptAndContinueButton() {
		waitforSpinner();
		try {
			acceptAndContinueButton.click();
			Reporter.log("Clicked on acceptAndContinueButton");
		} catch (Exception e) {
			Assert.fail("Clicked on acceptAndContinueButton");
		}
		return this;
	}
	
	/**
	 * Click Its Got Issues Radio Button
	 */
	public TradeInDeviceConditionValuePage clickItsGotIssuesRadioBtn() {
		try {
			clickElementWithJavaScript(itsGotIssuesRadioBtn);
			Reporter.log("Clicked on IssueRadiButton");
		} catch (Exception e) {
			Assert.fail("IssueRadiButton is not displayed to click.");
		}
		return this;
	}

	/**
	 * Click Its Good Radio Button
	 */
	public TradeInDeviceConditionValuePage clickItsGoodRadioBtn() {
		try {
			clickElementWithJavaScript(itsGoodRadioBtn);
			Reporter.log("Clicked on Good Radio Button.");
		} catch (Exception e) {
			Assert.fail("Good Radio Button is not displayed to Click.");
		}
		return this;
	}

	/**
	 * verify good device points are displayed
	 * 
	 * @param point
	 * @return true/false
	 */
	public TradeInDeviceConditionValuePage verifyGoodDevicePoints(String point) {
		try {
			boolean isGoodDevicePoints = false;
			for (WebElement points : goodDevicePoints) {
				if (points.isDisplayed() && points.getText().equals(point)) {
					isGoodDevicePoints = true;
				}
			}
			Assert.assertTrue(isGoodDevicePoints, "Anti Theft feature Message is not displayed for Android device.");
			Reporter.log("Anti Theft feature point is displayed");
		} catch (Exception e) {
			Assert.fail("Anti Theft feature Message is not displayed for Android device.");
		}
		return this;
	}

	/**
	 * Get the Device name
	 * 
	 * @return deviceName
	 */
	public String getDeviceDescription() {
		return deviceDescription.getText();
	}

	/**
	 * verify device condition radio buttons
	 * 
	 * @return true/false
	 */
	public TradeInDeviceConditionValuePage verifyDeviceCondtionRadioButtons() {
		try {
			boolean isDisplayed = false;
			for (WebElement radioBtn : radioButtons) {
				if (radioBtn.isDisplayed()) {
					isDisplayed = true;
				}
			}
			Assert.assertTrue(isDisplayed, "Device Condition Radio buttons is not displayed");
			Reporter.log("Device Condition Radio buttons is displayed");
		} catch (Exception e) {
			Assert.fail("Device Condition Radio buttons is not displayed");
		}
		return this;
	}

	/**
	 * verify bad device points are displayed
	 * 
	 * @param point
	 * @return true/false
	 */
	public TradeInDeviceConditionValuePage verifybadDevicePoints(String point) {
		try {
			boolean isBadDevicepoints = false;
			for (WebElement points : badDevicePoints) {
				if (points.isDisplayed() && points.getText().equals(point)) {
					isBadDevicepoints = true;
				}
			}
			Assert.assertTrue(isBadDevicepoints, "Bad device points are not displayed");
			Reporter.log("Bad device points are displayed");
		} catch (Exception e) {
			Assert.fail("Bad device points are not displayed");
		}
		return this;
	}
	
	/**
	 * Click on Close button on the modal
	 */
	public TradeInDeviceConditionValuePage clickContinueTradeInButton() {
		try {
			clickElementWithJavaScript(continueTradeIn);
			Reporter.log("Clicked on Continue Trade in Button on New trade in Value Page");
		} catch (Exception e) {
			Assert.fail("Continue Trade in Button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Primary CTA [continue TradeIn]
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyContinueTradeInButton() {
		try {
			Assert.assertTrue(continueTradeIn.isDisplayed(),"Continue Trade in CTA is not displayed");
			Reporter.log("continue TradeIn CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display continue TradeIn CTA");
		}

		return this;
	}

	/**
	 * Verify EIP balance
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyEIPBalance() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(eipBalance), "EIP balance in description not displayed");
			Reporter.log("EIP balance in description displayed");
		} catch (AssertionError e) {
			Assert.fail("EIP balance in description not displayed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify trade in value in description
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyTradeInValueInDescription() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(tradeInValueHeader),
					"Trade in value in description not displayed");
			Reporter.log("Trade in value in description displayed");
		} catch (AssertionError e) {
			Assert.fail("Trade in value in description not displayed " + e.getMessage());
		}
		return this;
	}
	
	/**
	 * Click 'Want to trade in a different phone?' link
	 */
	public TradeInDeviceConditionValuePage verifyTradeInValue(String lineSelectorTardeInValue) {
		try {
			waitforSpinner();
			Assert.assertEquals(lineSelectorTardeInValue, getPriceValue(tradeInValueHeader), "Trade in value not same in both pages");
			Reporter.log("Trade in value is same in both pages");
		} catch (AssertionError e) {
			Assert.fail("Trade in value not same in both pages " + e.getMessage());
		}
		return this;
	}
	
	/**
	 * Verify trade in value bulletin format divison
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyTradeInValueBulletinFormatDiv() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(bulletinFormatDiv),
					"Trade in value bulletin format divison not displayed");
			Reporter.log("Trade in value bulletin format divison displayed");
		} catch (AssertionError e) {
			Assert.fail("Trade in value bulletin format divison not displayed " + e.getMessage());
		}
		return this;
	}
	
	
	/**
	 * click Continue Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickContinueButton() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(ContinueButton));
			clickElementWithJavaScript(ContinueButton);
		} catch (Exception e) {
			Assert.fail("Continue Button is not Clickable");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TradeInDeviceConditionValuePage verifyPageUrl() {
		try{
			getDriver().getCurrentUrl().contains(pageUrl);
		}catch(Exception e){
			Assert.fail(" page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Trade in device condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyTradeInDeviceCondition() {

		try {
			checkPageIsReady();
			waitforSpinner();
			verifyPageUrl();
			Reporter.log("New Trade in Device Condition Page is loaded");
		} catch (Exception e) {
			Assert.fail("New Trade in Device Condition Page is Not Loaded");
		}
		return this;

	}

	/**
	 * Click on Not Sure What To Select Hyper Link
	 */
	public TradeInDeviceConditionValuePage clickNotSureWhatToSelectHyperLink() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(notSureWhatToSelectHyperlink);
			Reporter.log("Clicked on Not Sure What To Select Hyper Link on New Trade in Device Condition Page.");
		} catch (Exception e) {
			Assert.fail("Not Sure What To Select Hyper Link is not clickable.");
		}
		return this;
	}

	/**
	 * Verify Not Sure What To Select Hyper Link is displayed
	 */
	public TradeInDeviceConditionValuePage verifyNotSureWhatToSelectHyperLink() {
		try {
			waitforSpinner();
			Assert.assertTrue(notSureWhatToSelectHyperlink.isDisplayed(),
					"Not Sure What To Select Hyper Link on New Trade in Device Condition Page is not displaying");
			Reporter.log("Not Sure What To Select Hyper Link on New Trade in Device Condition Page is Displaying.");
		} catch (Exception e) {
			Assert.fail("Not Sure What To Select Hyper Link is not Displaying.");
		}
		return this;
	}

	/**
	 * Verify Popup modal is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyPopupModalIsDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(popUpModal.isDisplayed(), "Popup modal is not displayed");
			Reporter.log("Popup modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Popup modal");
		}
		return this;
	}

	/**
	 * Verify Popup modal Header is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyPopupModalHeaderIsDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(popupModalHeader.isDisplayed(), "Popup modal Header is not displayed");
			Reporter.log("Popup modal Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Popup modal Header");
		}
		return this;
	}

	/**
	 * Verify Popup modal Header is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyPopupModalDescriptionIsDisplayed() {
		try {

			waitforSpinner();
			for (WebElement description : modalDescription) {
				Assert.assertTrue(description.isDisplayed(), "Popup modal Description is not displayed");
			}
			Reporter.log("Popup modal Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Popup modal Description");
		}
		return this;
	}

	/**
	 * Click on Close button on the modal
	 */
	public TradeInDeviceConditionValuePage clickCloseButtonOfModal() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(modalCloseIcon);
			Reporter.log("Clicked on Close button on the modal on New Trade in Device Condition Page");
		} catch (Exception e) {
			Assert.fail("Close button on the modal is not clickable");
		}
		return this;
	}


	/**
	 * Verify Device Condition [Its Got Issues]
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDeviceConditionAsItsBad() {
		try {
			Assert.assertTrue(itsGotIsues.isDisplayed(), "It's Got Issues section is not displayed");
			Reporter.log("It's Got Issues section is displayed.");
		} catch (Exception e) {
			Assert.fail("It's Got Issues section is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Condition [Its Got Issues]
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickDeviceConditionAsItsBad() {
		try {
			clickFindMyIphoneYesButton();
			clickAcceptableLCDNoButton();
			clickWaterDamageYesButton();
			clickDevicePowerONNoButton();		
		} catch (Exception e) {
			Assert.fail("t's Got Issues section is not clickable");
		}
		return this;
	}

	/**
	 * Verify Page header Tell us Phone condition is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyTellUsConditionHeaderIsDisplayed() {
		try {
			Assert.assertTrue(tellUsConditionHeader.isDisplayed(),
					"Page header Tell us Phone condition is not displayed.");
			Assert.assertTrue(tellUsConditionHeader.getText().contains("Tell us about the phone"));
			Reporter.log("Page header Tell us Phone condition is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Page header Tell us Phone condition.");
		}
		return this;
	}

	/**
	 * Verify Device Condition [Its Goood] Image
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDeviceConditionAsitsGoodImage() {
		try {
			Assert.assertTrue(itsGoodImage.isDisplayed(), "It's Good image section image is not displayed");
			Reporter.log("It's Goood section image is displayed.");
		} catch (Exception e) {
			Assert.fail("It's Good section image is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Condition [Its Got Issues] Image
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDeviceConditionAsItsBadImage() {
		try {
			Assert.assertTrue(itsGotIsuesImage.isDisplayed(), "It's Got Issues section image is not displayed");
			Reporter.log("It's Got Issues section image is displayed.");
		} catch (Exception e) {
			Assert.fail("t's Got Issues section image is not displayed");
		}
		return this;
	}

	/**
	 * Verify All below are true text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyAllTrueIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "All below are true"),
					"All below are true text is not displayed");
			Reporter.log("All below are true text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display All below are true text.");
		}
		return this;
	}

	/**
	 * Verify Device Powers on text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDevicePowerOnIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "Device Powers on"),
					"Device Powers on text is not displayed");
			Reporter.log("Device Powers on text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Device Powers on text.");
		}
		return this;
	}

	/**
	 * Verify No screen damage text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyNoScreenDamageIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "No screen damage"),
					"No screen damage text is not displayed");
			Reporter.log("No screen damage text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display No screen damage text");
		}
		return this;
	}

	/**
	 * Verify No liquid damage text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyNoLiquidDamageIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "No liquid damage"),
					"No liquid damage text is not displayed");
			Reporter.log("No liquid damage text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display No liquid damage text");
		}
		return this;
	}

	/**
	 * Verify Any of the below applies text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyAnyOfTheBelowIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "Any of the below applies"),
					"Any of the below applies text is not displayed");
			Reporter.log("Any of the below applies text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Any of the below applies");
		}
		return this;
	}

	/**
	 * Verify Doesnt Power On text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDoesntPowerOnIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "power on"),
					"Doesnt Power On text is not displayed");
			Reporter.log("Doesnt Power On text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Doesnt Power On text");
		}
		return this;
	}

	/**
	 * Verify Has screen damage text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyHasScreenDamageIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "Has screen damage"),
					"Has screen damage text is not displayed");
			Reporter.log("Has screen damage text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Has screen damage text");
		}
		return this;
	}

	/**
	 * Verify Has Liquid Damage text is displayed
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyHasLiquidDamageIsDisplayed() {
		try {
			Assert.assertTrue(verifyElementBytext(deviceConditionTexts, "liquid damage"),
					"Has Liquid Damage text is not displayed");
			Reporter.log("Has Liquid Damage text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Has Liquid Damage text");
		}
		return this;
	}

	/**
	 * Click on Good condition
	 */
	public TradeInDeviceConditionValuePage clickOnGoodCondition() {
		try {
			waitforSpinner();
			Assert.assertTrue(goodConditiontext.isDisplayed(), "Good condition divison not displayed");
			goodConditiontext.click();
		} catch (Exception e) {
			Assert.fail("Click on good condition divison failed " + e.getMessage());
		}
		return this;
	}
	
	/**
	 * Verify good title text on device condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyGoodTitleTextOnDeviceConditionAAL() {
		try {
			waitFor(ExpectedConditions.visibilityOf(goodConditionTitleAAL));
			Assert.assertTrue(goodConditionTitleAAL.isDisplayed(),"Good condition title text not present on deviceCondition");
			Reporter.log("Good condition title text present  on deviceCondition");
		} catch (Exception e) {
			Assert.fail("Failed to display good condition title on device condition");
		}
		return this;
	}

	/**
	 * Verify bad title text on device condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyBadTitleTextOnDeviceConditionAAL() {
		try {
			waitFor(ExpectedConditions.visibilityOf(badConditionTitleAAL));
			Assert.assertTrue(badConditionTitleAAL.isDisplayed(),"Bad condition title text not present on deviceCondition");
			Reporter.log("Bad condition title text present  on deviceCondition");
		} catch (Exception e) {
			Assert.fail("Failed to display bad condition title on device condition");
		}
		return this;
	}
	
	/**
	 * Verify bad device image on device condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyBadImageOnDeviceConditionAAL() {
		try {
			waitFor(ExpectedConditions.visibilityOf(badDeviceImageAAL));
			Assert.assertTrue(badDeviceImageAAL.isDisplayed(),"Bad device image not present on deviceCondition");
			Reporter.log("Bad device image present on deviceCondition");
		} catch (Exception e) {
			Assert.fail("Failed to display bad device image on device condition");
		}
		return this;
	}
	
	/**
	 * Verify good device image on device condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyGoodImageOnDeviceConditionAAL() {
		try {
			waitFor(ExpectedConditions.visibilityOf(goodDeviceImageAAL));
			Assert.assertTrue(goodDeviceImageAAL.isDisplayed(),"Good device image not present on deviceCondition");
			Reporter.log("Good device image present  on deviceCondition");
		} catch (Exception e) {
			Assert.fail("Failed to display good device image on device condition");
		}
		return this;
	}
	
	/**
	 * Verify text powers on for good condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDevicePowerOnTextForGoodConditionAAL() {
		try {
			Assert.assertTrue(verifyElementBytext(goodConditionTextAAL, "Device powers on"),
					"Power On text is not displayed for good condition");
			Reporter.log(" Power On text is displayed for good condition");
		} catch (Exception e) {
			Assert.fail("Failed to display text powers on for good condition");
		}
		return this;
	}
	
	/**
	 * Verify text powers on for bad condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyDevicePowerOnTextForBadConditionAAL() {
		try {
			Assert.assertTrue(verifyElementBytext(badConditionTextAAL, "Doesn't power on"),
					"Power On text is not displayed for bad condition");
			Reporter.log(" Power On text is displayed for bad condition");
		} catch (Exception e) {
			Assert.fail("Failed to display text powers on for bad condition");
		}
		return this;
	}
	
	/**
	 * Verify text screen damage for bad condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyNoScreenImageTextForBadConditionAAL() {
		try {
			Assert.assertTrue(verifyElementBytext(badConditionTextAAL, "Has screen damage"),
					"Screen damage is not displayed for bad condition");
			Reporter.log(" Screen damage text is displayed for bad condition");
		} catch (Exception e) {
			Assert.fail("Failed to display text screen damage for bad condition");
		}
		return this;
	}
	
	/**
	 * Verify text screen damage for good condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyNoScreenImageTextForGoodConditionAAL() {
		try {
			Assert.assertTrue(verifyElementBytext(goodConditionTextAAL, "No screen damage"),
					"Screen damage is not displayed for good condition");
			Reporter.log(" Screen damage text is displayed for good condition");
		} catch (Exception e) {
			Assert.fail("Failed to display text screen damage for good condition");
		}
		return this;
	}
	
	/**
	 * Verify text Has liquid damage for good condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyLiquidDamageTextForGoodConditionAAL() {
		try {
			Assert.assertTrue(verifyElementBytext(goodConditionTextAAL, "No liquid damage"),
					"Has liquid damage is not displayed for good condition");
			Reporter.log("Has liquid damage text is displayed for good condition");
		} catch (Exception e) {
			Assert.fail("Failed to display text liquid damage for good condition");
		}
		return this;
	}
	
	/**
	 * Verify text Has liquid damage for bad condition
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyLiquidDamageTextForBadConditionAAL() {
		try {
			Assert.assertTrue(verifyElementBytext(badConditionTextAAL, "Has liquid damage"),
					"Has liquid damage is not displayed for bad condition");
			Reporter.log("Has liquid damage text is displayed for bad condition");
		} catch (Exception e) {
			Assert.fail("Failed to display text liquid damage for bad condition");
		}
		return this;
	}		
	
	/**
	 * click close on device condition modal window
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickDeviceConditionModalCloseAAL() {
		try {
			waitFor(ExpectedConditions.visibilityOf(notSureWindowCloseAAL));
			notSureWindowCloseAAL.click();
			Reporter.log("Device Condition modal window is closed");
		} catch (Exception e) {
			Assert.fail("Failed to click  device condition modal close button");
		}
		return this;
	}
	
	/**
	 * Verify Title Text is displayed 
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage titleTextDisplayed() {
		try {
			Assert.assertTrue(titleText.isDisplayed(),"Title text is not displayed ");
			Assert.assertTrue(titleText.getText().equals("Tell us about the condition of your old phone."),"Title text is not displayed ");
			Reporter.log("Title text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Title text");
		}
		return this;
	}
	
	/**
	 * Verify Title Text is displayed 
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyNewContinueButton() {
		try {
			Assert.assertTrue(continueCTANew.isDisplayed(),"Continue button is not displayed ");
			Reporter.log("Continue button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Continue button");
		}
		return this;
	}
	
	/**
	 * Verify Assured Questions are displayed 
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyAssuredQuestionsDisplayed() {
		try {
			for(WebElement question : assuredQuestions) {
				Assert.assertTrue(question.isDisplayed(),"Assured Question is not displayed ");
				Assert.assertTrue(question.getText().contains("?"),"Assured Question is not displayed ");
			}
			Reporter.log("Assured Questions are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Assured Questions");
		}
		return this;
	}
	
	/**
	 * Verify Assured Questions Yes No Box are displayed 
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage verifyAssuredQuestionsYesNoDisplayed() {
		try {
			for(WebElement yesNo : assuredQuestionsYesNo) {
				Assert.assertTrue(yesNo.isDisplayed(),"Assured Question Yes/No Box is not displayed ");
				Assert.assertTrue(yesNo.getText().equals("Yes")||yesNo.getText().equals("No"),"Assured Question Yes/No Box is not displayed ");
			}
			Reporter.log("Assured Questions Yes/No Box are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Assured Questions Yes/No Box");
		}
		return this;
	}
	
	  /**
     * Verify Battery question displayed
     *
     * @return
     */
    public TradeInDeviceConditionValuePage verifyBatteryQuestionDisplayed() {
    try {
        Assert.assertTrue(batteryQuestion.isDisplayed(), "Battery question not displayed");
        Reporter.log("Battery question displayed");
    }catch(Exception e) {
    Assert.fail("Failed to display Battery question");
    }
    return this;
    }
    
    
    /**
     * Verify Water drop question display
     */
    public TradeInDeviceConditionValuePage verifyWaterDropQuestionDisplayed() {
        try {
            Assert.assertTrue(waterDropQuestion.isDisplayed(), "Water drop question not display");
            Reporter.log("Water drop question display");
        } catch (Exception e) {
            Assert.fail("Failed to display Water drop question.");
        }
        return this;
    }
    
    /**
     * Verify Cracked screen question display
     */
    public TradeInDeviceConditionValuePage verifyCrackedScreenQuestionDisplay() {
        try {
            Assert.assertTrue(crackedScreenQuestion.isDisplayed(), "Cracked screen question not display");
            Reporter.log("Cracked screen question display");
        } catch (Exception e) {
            Assert.fail("Failed to display Cracked screen question");
        }return this;
    }
    
    /**
     * Verify Location question display
     */
    public TradeInDeviceConditionValuePage verifyLocationQuestionDisplay() {
        try {
            Assert.assertTrue(locationQuestion.isDisplayed(), "Location question not display");
            Reporter.log("Location question display");
        } catch (Exception e) {
            Assert.fail("Failed to display Location question");
        }return this;
    }


    /**
	 * Click on Find My Iphone YesButton
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickFindMyIphoneYesButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(findMyIphoneYesButton));
			clickElementWithJavaScript(findMyIphoneYesButton);
			Reporter.log("Find My Iphone YesButton is clickable");
		} catch (Exception e) {
			Assert.fail("Find My Iphone YesButton is not clickable");
		}
		return this;
	}
	
	
	/**
	 * Click on Acceptable LCD YesButton
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickAcceptableLCDYesButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(acceptableLCDYesButton));
			clickElementWithJavaScript(acceptableLCDYesButton);
			Reporter.log("Acceptable LCD YesButton is clickable");
		} catch (Exception e) {
			Assert.fail("Acceptable LCD YesButton is not clickable");
		}
		return this;
	}
	
	
	/**
	 * Click on Water Damage NoButton
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickWaterDamageNoButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(waterDamageNoButton));
			clickElementWithJavaScript(waterDamageNoButton);
			Reporter.log("Water Damage NoButton is clickable");
		} catch (Exception e) {
			Assert.fail("Water Damage NoButton is not clickable");
		}
		return this;
	}
	
	/**
	 * Click on Device Power ON YesButton
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickDevicePowerONYesButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(devicePowerONYesButton));
			clickElementWithJavaScript(devicePowerONYesButton);
			Reporter.log("Device Power ON YesButton is clickable");
		} catch (Exception e) {
			Assert.fail("Device Power ON YesButton is not clickable");
		}
		return this;
	}
	
	
	/**
	 * Click on ContinueCTA New Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickContinueCTANew() {
		try {
			waitFor(ExpectedConditions.visibilityOf(continueCTANew));
			clickElementWithJavaScript(continueCTANew);
			Reporter.log("ContinueCTA is clickable");
		} catch (Exception e) {
			Assert.fail("ContinueCTA is not clickable");
		}
		return this;
	}
	
	/**
	 * Click on Device Power ON No Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickDevicePowerONNoButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(devicePowerONNoButton));
			clickElementWithJavaScript(devicePowerONNoButton);
			Reporter.log("Device Power ON No Button is clickable");
		} catch (Exception e) {
			Assert.fail("Device Power ON No Button is not clickable");
		}
		return this;
	}
	/**
	 * Click on Acceptable LCD No Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickAcceptableLCDNoButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(acceptableLCDNoButton));
			clickElementWithJavaScript(acceptableLCDNoButton);
			Reporter.log("Acceptable LCD No Button is clickable");
		} catch (Exception e) {
			Assert.fail("Acceptable LCD No Button is not clickable");
		}
		return this;
	}
	/**
	 * Click on Water Damage Yes Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickWaterDamageYesButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(waterDamageYesButton));
			clickElementWithJavaScript(waterDamageYesButton);
			Reporter.log("Water Damage Yes Button is clickable");
		} catch (Exception e) {
			Assert.fail("Water Damage Yes Button is not clickable");
		}
		return this;
	}
	/**
	 * Click on find My Iphone No Button
	 * 
	 * @return
	 */
	public TradeInDeviceConditionValuePage clickFindMyIphoneNoButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(findMyIphoneNoButton));
			clickElementWithJavaScript(findMyIphoneNoButton);
			Reporter.log("find My I phone No Button is clickable");
		} catch (Exception e) {
			Assert.fail("find My I phone No Button is not clickable");
		}
		return this;
	}
}
