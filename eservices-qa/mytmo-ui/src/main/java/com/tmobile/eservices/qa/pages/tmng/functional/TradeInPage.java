/**
 * 
 */
package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author ksrivani
 *
 */
public class TradeInPage extends TmngCommonPage {

	public TradeInPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	private final String pageUrl = "trade-in";

	@FindBy(xpath = "//h2[contains(text(),'Find out what your phone is worth.')]")
	private WebElement tradeInDescription;

	@FindBy(css = "input[placeholder='Device carrier']")
	private WebElement deviceCarrierPlaceholderText;

	@FindBy(css = "input[placeholder='Device manufacturer']")
	private WebElement deviceManufacturerPlaceholderText;

	@FindBy(css = "input[placeholder='Device model']")
	private WebElement deviceModelPlaceholderText;

	@FindBy(css = "input[name='deviceCarrier']")
	private WebElement deviceCarrierTextBox;

	@FindBy(css = "input[name='deviceManufacturer']")
	private WebElement deviceManufacturerTextBox;

	@FindBy(css = "input[name='deviceModel']")
	private WebElement deviceModelTextBox;
	
	@FindBy(xpath = "//div[@id='deviceCarrier']/fuse-search/ul/li/a")
	private List<WebElement> suggestionListOfDeviceCarrierSelectorSearchBar;
	
	@FindBy(xpath = "//div[@id='deviceManufacturer']/fuse-search/ul/li/a")
	private List<WebElement> suggestionListOfDeviceManufacturerSelectorSearchBar;
	
	@FindBy(xpath = "//div[@id='deviceModel']/fuse-search/ul/li/a")
	private List<WebElement> suggestionListOfDeviceModelSelectorSearchBar;
	
	@FindBy(xpath = "//button[contains(text(),'Get estimate')]")
	private WebElement getEstimateButton;

	@FindBy(css = "span[id='estimate-header']")
	private WebElement estimatesSuccessHeader;
	
	@FindBy(css = "span[class*='estimate-value']")
	private WebElement estimateValue;
	
	@FindBy(css = "span[id='estimate-sub-header']")
	private WebElement successDescription;
	
	@FindBy(xpath = "//div[contains(@class,'tradein-conditions')]/table/tbody/tr/td/p[contains(text(), 'Actual value is determined')]")
	private WebElement conditionTerms;
	
	@FindBy(css = "button[class*='shop-now']")
	private WebElement shopNowButton;
	
	private final String productListingPageUrl = "/cell-phones";
	
	@FindBy(xpath = "//a[text()='See full terms']")
	private WebElement seeFullTermsLink;
	
	@FindBy(xpath = "//h3[contains(text(),'Find out what your phone is worth.')]")
	private WebElement fullTermsOverlay;
	
	@FindBy(xpath = "//button[contains(text(),'Check another device')]")
	private WebElement checkAnotherDeviceButton;
	
	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the TravelAbroadPage class instance.
	 */
	public TradeInPage verifyTradeInDevicePage() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		Reporter.log("Trade In device page is displayed");
		return this;
	}
	

	/***
	 * Verify the display of description on Trade In page
	 * 
	 * @return
	 */
	public TradeInPage verifyTradeInHeaderDescription() {
		try {
			checkPageIsReady();
			Assert.assertTrue(tradeInDescription.isDisplayed());
			Reporter.log("Trade In Header should be displayed successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to display the Trade In Header");
		}
		return this;
	}

	/***
	 * Verify the display of Device Carrier text box
	 * 
	 * @return
	 */
	public TradeInPage verifyDeviceCarrierTextBox() {
		try {
			checkPageIsReady();
			JavascriptExecutor je = (JavascriptExecutor) getDriver();
			je.executeScript("scroll(0, 250);");
			Assert.assertTrue(deviceCarrierTextBox.isDisplayed());
			Reporter.log("Device Carrier text box is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to display the Device Carrier text box");
		}
		return this;
	}

	/***
	 * Verify the display of Device Manufacturer text box
	 * 
	 * @return
	 */
	public TradeInPage verifyDeviceManufacturerTextBox() {
		try {
			checkPageIsReady();
			Assert.assertTrue(deviceManufacturerTextBox.isDisplayed());
			Reporter.log("Device Manufacturer text box is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to display the Device Manufacturer text box");
		}
		return this;
	}

	/***
	 * Verify the display of Device Carrier text box
	 * 
	 * @return
	 */
	public TradeInPage verifyDeviceModelTextBox() {
		try {
			checkPageIsReady();
			Assert.assertTrue(deviceModelTextBox.isDisplayed());
			Reporter.log("Device Model text box is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Unable to display the Device Model text box");
		}
		return this;
	}

	/***
	 * Enter text in the Device carrier selector text field
	 * 
	 * @param value
	 * @return
	 */
	public TradeInPage setTextForDeviceCarrierSelectorSearchTextBar(String value) {
		try {
			checkPageIsReady();
			sendTextData(deviceCarrierTextBox, value);
			Reporter.log("Entered '" + value + "' in the Device carrier text box");
		} catch (Exception e) {
			Assert.fail("failed to set text in the Device carrier selector field" + e.toString());
		}
		return this;
	}

	/***
	 * enter text in the Device manufacturer selector text field
	 * 
	 * @param value
	 * @return
	 */
	public TradeInPage setTextForDeviceManufacturerSelectorSearchTextBar(String value) {
		try {
			checkPageIsReady();
			sendTextData(deviceManufacturerTextBox, value);
			Reporter.log("Entered '" + value + "' in the Device manufacturer text box");
		} catch (Exception e) {
			Assert.fail("failed to set text in the Device manufacturer selector field" + e.toString());
		}
		return this;
	}

	/***
	 * enter text in the Device manufacturer selector text field
	 * 
	 * @param value
	 * @return
	 */
	public TradeInPage setTextForDeviceModelSelectorSearchTextBar(String value) {
		try {
			checkPageIsReady();
			sendTextData(deviceModelTextBox, value);
			Reporter.log("Entered '" + value + "' in the Device model text box");
		} catch (Exception e) {
			Assert.fail("failed to set text in the Device model selector field" + e.toString());
		}
		return this;
	}

	/***
	 * verify the auto complete functionality for Device carrier selector text
	 * field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public TradeInPage verifyAutoCompleteSearchResultsForDeviceCarrierSelectorField(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			setTextForDeviceCarrierSelectorSearchTextBar(expectedValue);
			for (WebElement ele : suggestionListOfDeviceCarrierSelectorSearchBar) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					actListOfValuesThatMatchedTheSearchValue++;
					}
			}
			Assert.assertEquals(suggestionListOfDeviceCarrierSelectorSearchBar.size(),
					actListOfValuesThatMatchedTheSearchValue);
			Reporter.log("Autocomplete search functionality works for Device carrier selector search field");
		} catch (Exception e) {
			Assert.fail(
					"failed to verify the Autocomplete search functionality for Device carrier selector search field"
							+ e.toString());
		}
		return this;
	}
	
	/***
	 * verify the auto complete functionality for Device manufacturer selector text
	 * field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public TradeInPage verifyAutoCompleteSearchResultsForDeviceManufacturerSelectorField(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			setTextForDeviceManufacturerSelectorSearchTextBar(expectedValue);
			for (WebElement ele : suggestionListOfDeviceManufacturerSelectorSearchBar) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					actListOfValuesThatMatchedTheSearchValue++;
				}
			}
			Assert.assertEquals(suggestionListOfDeviceManufacturerSelectorSearchBar.size(),
					actListOfValuesThatMatchedTheSearchValue);
			Reporter.log("Autocomplete search functionality works for Device Manufacturer selector search field");
		} catch (Exception e) {
			Assert.fail(
					"failed to verify the Autocomplete search functionality for Device Manufacturer selector search field"
							+ e.toString());
		}
		return this;
	}
	
	/***
	 * verify the auto complete functionality for Device model selector text
	 * field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public TradeInPage verifyAutoCompleteSearchResultsForDeviceModelSelectorField(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			setTextForDeviceModelSelectorSearchTextBar(expectedValue);
			for (WebElement ele : suggestionListOfDeviceModelSelectorSearchBar) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					actListOfValuesThatMatchedTheSearchValue++;
				}
			}
			Assert.assertEquals(suggestionListOfDeviceModelSelectorSearchBar.size(),
					actListOfValuesThatMatchedTheSearchValue);
			Reporter.log("Autocomplete search functionality works for Device Model selector search field");
		} catch (Exception e) {
			Assert.fail(
					"failed to verify the Autocomplete search functionality for Device Model selector search field"
							+ e.toString());
		}
		return this;
	}

	/***
	 * Verify Removing A Character From Device Carrier
	 * 
	 */
	public TradeInPage verifyRemovingACharacterFromDeviceCarrier() {
		checkPageIsReady();
		try {
			deviceCarrierTextBox.sendKeys(Keys.BACK_SPACE);
			Reporter.log("Successfully removed a character from entered Device carrier inorder to view typeahead list");
		} catch (Exception e) {
			Assert.fail("failed to remove a character from entered Device carrier");
		}
		return this;
	}
	
	/***
	 * Verify and select from Type ahead list of Device carrier
	 * 
	 */
	public TradeInPage verifyandSelectFromTypeAheadListOfDeviceCarrier() {
		checkPageIsReady();
		try {
			suggestionListOfDeviceCarrierSelectorSearchBar.get(0).click();
			Reporter.log("Selected the Device carrier from the type ahead list");
		} catch (Exception e) {
			Assert.fail("failed to select Device carrier from the type ahead list");
		}
		return this;
	}
	
	/***
	 * Verify Removing A Character From Device Manufacturer
	 * 
	 */
	public TradeInPage verifyRemovingACharacterFromDeviceManufacturer() {
		checkPageIsReady();
		try {
			deviceManufacturerTextBox.sendKeys(Keys.BACK_SPACE);
			Reporter.log("Successfully removed a character from entered Device manufacturer inorder to view typeahead list");
		} catch (Exception e) {
			Assert.fail("failed to remove a character from entered Device manufacturer");
		}
		return this;
	}
	
	/***
	 * Verify and select from Type ahead list of Device manufacturer
	 * 
	 */
	public TradeInPage verifyandSelectFromTypeAheadListOfDeviceManufacturer() {
		checkPageIsReady();
		try {
			suggestionListOfDeviceManufacturerSelectorSearchBar.get(0).click();
			Reporter.log("Selected the Device manufacturer from the type ahead list");
		} catch (Exception e) {
			Assert.fail("failed to select Device manufacturer from the type ahead list");
		}
		return this;
	}
	
	/***
	 * Verify Removing A Character From Device Model
	 * 
	 */
	public TradeInPage verifyRemovingACharacterFromDeviceModel() {
		checkPageIsReady();
		try {
			deviceModelTextBox.sendKeys(Keys.BACK_SPACE);
			Reporter.log("Successfully removed a character from entered Device model inorder to view typeahead list");
		} catch (Exception e) {
			Assert.fail("failed to remove a character from entered Device model");
		}
		return this;
	}
	
	/***
	 * Verify and select from Type ahead list of Device model
	 * 
	 */
	public TradeInPage verifyandSelectFromTypeAheadListOfDeviceModel() {
		checkPageIsReady();
		try {
			suggestionListOfDeviceModelSelectorSearchBar.get(0).click();
			Reporter.log("Selected the Device model from the type ahead list");
		} catch (Exception e) {
			Assert.fail("failed to select Device model from the type ahead list");
		}
		return this;
	}
	
	/**
	 * It will retrieve the selected value from Device Carrier drop down
	 *  
	 * @return
	 */
	public String retrieveDeviceCarrier() {
		return deviceCarrierTextBox.getAttribute("value");
		}
	
	/**
	 * It will retrieve the selected value from Device Manufacturer drop down
	 *  
	 * @return
	 */
	public String retrieveDeviceManufacturer() {
		return deviceManufacturerTextBox.getAttribute("value");
	}
	
	/**
	 * It will retrieve the selected value from Device Model drop down
	 *  
	 * @return
	 */
	public String retrieveDeviceModel() {
		return deviceModelTextBox.getAttribute("value");
	}
	
	/***
	 * Verify the display of Go button
	 * 
	 * @return
	 */
	public TradeInPage verifyGoButton() {
		try {
			checkPageIsReady();
			Assert.assertTrue(getEstimateButton.isDisplayed());
			Assert.assertTrue(getEstimateButton.isEnabled());
			Reporter.log("Go button is enabled");
		} catch (Exception e) {
			Assert.fail("Unable to display the Go button");
		}
		return this;
	}
	
	/***
	 * Click Get estimate button
	 * 
	 * @return
	 */
	public TradeInPage clickGetEstimateButton() {
		try {
			checkPageIsReady();
			getEstimateButton.click();
			Reporter.log("Clicked on Get estimate button");
		} catch (Exception e) {
			Assert.fail("Unable to click Get estimate button");
		}
		return this;
	}
	
	/***
	 * Verify the display of success Header with estimations
	 * 
	 * @return
	 */
	public TradeInPage verifyEstimatesInSuccessHeader() {
		try {
			checkPageIsReady();
			estimatesSuccessHeader.isDisplayed();
			estimateValue.isDisplayed();
			Reporter.log("Success header with the estimated offer is displayed: "+estimatesSuccessHeader.getText()+estimateValue.getText());
			} catch (Exception e) {
			Assert.fail("No header is displayed with the estimations");
		}
		return this;
	}
	
	/***
	 * Verify the display of success Description
	 * 
	 * @return
	 */
	public TradeInPage verifySuccessDescription() {
		try {
			checkPageIsReady();
			successDescription.isDisplayed();
			Reporter.log("Success description is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display the success description");
		}
		return this;
	}
	
	/***
	 * Verify the display of Condition terms when estimated value is greater than Zero
	 * 
	 * @return
	 */
	public TradeInPage verifyConditionTerms() {
		try {
			checkPageIsReady();
			int estValue = Integer.parseInt(estimateValue.getText().substring(1));
			if (estValue > 0) {
				Assert.assertTrue(conditionTerms.isDisplayed());
				Reporter.log("Conditions terms are displayed, when estimated value is greater than Zero");
						}
			else{
				Reporter.log("No conditions will be displayed, as the estimated offer is Zero");
			}
			} catch (Exception e) {
			Assert.fail("Unable to display the Conditions terms, when estimated value is greater than Zero");
		}
		return this;
	}
	
	/***
	 * Click Shop Now button
	 * 
	 * @return
	 */
	public TradeInPage clickShopNowButton() {
		try {
			checkPageIsReady();
			shopNowButton.click();
			Reporter.log("Clicked on Shop Now button");
		} catch (Exception e) {
			Assert.fail("Unable to click Shop Now button");
		}
		return this;
	}
	
	/***
	 * Verify the display of Product Listing page
	 * 
	 * @return
	 */
	public TradeInPage verifyProductListingPage() {
			checkPageIsReady();
			try {
				ArrayList<String> tradeInWindows = new ArrayList<String>(getDriver().getWindowHandles());
				getDriver().switchTo().window(tradeInWindows.get(1));
				waitFor(ExpectedConditions.urlContains(productListingPageUrl),60);
				Reporter.log("Product Listing page is displayed");
				} catch (Exception e) {
				Assert.fail("Product Listing page is not displayed");
			}
			return this;
		}
	
	/***
	 * Click Shop Now button
	 * 
	 * @return
	 */
	public TradeInPage clickSeeFullTermsLink() {
		try {
			checkPageIsReady();
			JavascriptExecutor je = (JavascriptExecutor) getDriver();
			je.executeScript("scroll(0, 250);");
			seeFullTermsLink.click();
			Reporter.log("Clicked on Full Terms links successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to click on Full Terms Link");
		}
		return this;
	}
	
	/***
	 * Click Shop Now button
	 * 
	 * @return
	 */
	public TradeInPage verifyFullTermsOverlay() {
		try {
			checkPageIsReady();
			fullTermsOverlay.isDisplayed();
			Reporter.log("Full Terms overlay is loaded with content successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to load Full Terms overlay with content");
		}
		return this;
	}
	
	/***
	 * Click Go button
	 * 
	 * @return
	 */
	public TradeInPage clickCheckAnotherDeviceButton() {
		try {
			checkPageIsReady();
			checkAnotherDeviceButton.click();
			Reporter.log("Clicked on Check Another Device button");
		} catch (Exception e) {
			Assert.fail("Unable to click Check Another Device button");
		}
		return this;
	}
	
	/***
	 * Verify the display of description on Trade In page
	 * 
	 * @return
	 */
	public TradeInPage verifyTradeInDescriptionNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertFalse(tradeInDescription.isDisplayed());
			Reporter.log("Trade In description is hidden, once clicked on Go button");
		} catch (Exception e) {
			Assert.fail("Trade In description is displayed");
		}
		return this;
	}
	
	/***
	 * Verify the success result format
	 * 
	 * @return
	 */
	public TradeInPage verifySuccessResultFormat(String deviceCarrier, String deviceManufacturer, String deviceModel) {
		try {
			checkPageIsReady();
				Assert.assertEquals(successDescription.getText(), deviceManufacturer + " " + deviceModel);
				Reporter.log("Success Result format is verified successfully as: " + deviceManufacturer + " " + deviceModel);
			} catch (Exception e) {
			Assert.fail("Could not verify the success header description format");
		}
		return this;
	}
}