package com.tmobile.eservices.qa.api.unlockdata;

/**
 * @author blakshminarayana
 * This class has all the common properties 
 */
public final  class Constants {
	public static final String MACID ="MacId";
	public static final String GENERATE_SOAP_ACTION="generateToken";
	public static final String AUTHORIZATION_RESPONE_TOKEN="token";
	public static final String PAYMENT_HISTORY_RESPONE_TOKEN="eclb";
	public static final String ACCOUNT_PAYMENT_RESPONE_TOKEN="eipEquipmentId";
	public static final String INSTALLMENT_HISTORY_SOAP_ACTION= "getInstallmentHistory";
	public static final String PAYMENT_SOAP_ACTION= "makePayment";
	public static final String APPLICATION_PROPERTY_FILE="utils.properties";
	public static final String AUTHORIZATION_FILE="cutomer.xml";
	public static final String AGENCYMODEL_FILE="agencyModel.xml";
	public static final String COVERAGEDEVICEDASHBOARD_FILE="coverageDeviceDashboard.xml";
	public static final String INSTALLMENTACCOUNTPAYMENT_FILE="installmentAccountPayment.xml";
	public static final String INSTALLMENTHISTORY_FILE="installmentHistory.xml";
	public static final String NONAGENCYMODEL_FILE="nonAgencyModel.xml";
	public static final String COVERAGE_SELECTION_FILE="coverageSelection.xml";
	public static final String WARRANTY_EXCHANGE_FULFILLMENT_FILE="warrantyFulfillment.xml";
	public static final String WARRANTY_EXCHANGE_INITIATION_FILE="warrantyInitiation.xml";
	public static final String AUTHORIZATION_TOKEN_END_POINT_URL=".soap.generatetoken.endpoint";
	public static final String INSTALLMENT_HISTORY_END_POINT_URL=".soap.installment.history.endpoint";	
	public static final String INSTALLMENT_ACCOUNT_END_POINT_URL=".soap.installment.accpayment.endpoint";
	public static final String SOAPHANDLER_SOAPDIR="soap/";
	public static final String PDFHANDLER_DIR="pdf/";

	//	data base information
	public static final String JDBC_DRIVER_URL=".jdbc.driver.url";
	public static final String JDBC_USER_ID=".jdbc.driver.userID";
	public static final String JDBC_USER_PAASWORD=".jdbc.driver.password";
	
//	EIP data base information
	public static final String EIP_URL=".eip.url";
	public static final String EIP_USER_ID=".eip.userID";
	public static final String EIP_USER_PAASWORD=".eip.password";
	//Rest service end points
	public static final String GET_ACCESS_TOKEN=".soap.accesstoken.endpoint"; 
	public static final String SENT_PIN_TO_MSISDN=".soap.sendpintomsisdn.endpoint";
	public static final String 	AUTHORIZATION=".authorization";
	public static final String 	PROFILE_END_POINT=".soap.profile.endpoint";
	public static final String 	ENCRYPTION=".soap.encrypt.endpoint";
	public static final String 	DECRYPTION=".soap.decrypt.endpoint";
	
	public static final String 	GET_CLEAR_COUNTERS=".soap.clearcounters.endpoint";
	
	public static final String  CHANGE_PROFILE_PASSWORD =".rest.changePassword.endpoint";
	
	public static final String  RESET_PROFILE_PASSWORD =".rest.resetPassword.endpoint";
	
	public static final String ASOP_PROCESSQUOTE_ENDPOINT_URL=".asop.processQuote";
	public static final String ASOP_SYNCQUOTE_ENDPOINT_URL=".asop.syncQuote";
	public static final String ASOP_JSON_FILE_DIR="json/";
	public static final String ASOP_STANDARD_ORDER_JSON_FILE="standard.json";
	
	public static final String 	JUMP_SERVICE_SERVICE=".rest.jumpservice.endpoint";
	//User Registration soap xml files
	public static final String MANAGE_PROFILE_BETA="manageprofile-beta.xml";
	public static final String MANAGE_PROFILE_PARTIAL="manageprofile-partial.xml";
	public static final String MANAGE_PROFILE_FULL="manageprofile-full.xml";
	public static final String MANAGE_PROFILE_END_POINT_URL=".soap.registarion.endpoint";
	public static final String PROFILE_NEW_PASSWORD=".profile.password";
	public static final String MANAGE_PROFILE_SOAP_ACTION="manageProfile";
	public static final String MSISDNTAG = "MSISDN";
	public static final String ERRORTAG = "";
	public static final String  NUMLINES = "";
	public static final String  NUMLINESTAG = "role";
	public static final String SECOND_FACTOR_AUTH=".soap.generatetemppin.endpoint";
	
	/**  Test Data service end point*/
	public static final String PAYMENT_SERVICE=".service.soap.payment.service";
	private Constants() {
		
	}
}
