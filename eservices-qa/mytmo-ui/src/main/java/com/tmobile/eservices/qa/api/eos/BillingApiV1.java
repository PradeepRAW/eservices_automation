package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class BillingApiV1 extends ApiCommonLib {

	/***
	 * T-Mobile BillingV1 API’s. parameters: - $ref: '#/parameters/oAuth' - $ref:
	 * '#/parameters/Accept' - $ref: '#/parameters/interactionid'
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getDataSet(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildBillingAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/billing/dataset";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.GET);
		return response;
	}

	public Response getBillList(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildBillingAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/billing/cycles?ban=" + apiTestData.getBan();
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.GET);
		return response;
	}

	private Map<String, String> buildBillingAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		// tokenMap.put("version", "v1");
		JWTTokenApi jwtToken = new JWTTokenApi();
		Map<String, String> jwtTokens = jwtToken.checkAndGetJWTToken(apiTestData, tokenMap);
		Map<String, String> headers = new HashMap<String, String>();
		// jwtToken.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"),
		// tokenMap);
		// jwtToken.registerJWTTokenforAPIGEE("ONPREM", jwtTokens.get("jwtToken"),
		// tokenMap);
		headers.put("Authorization", "Bearer " + jwtTokens.get("jwtToken"));
		headers.put("Content-Type", "application/json");
		headers.put("Accept", " application/json");
		headers.put("access_token", jwtTokens.get("accessToken"));
		headers.put("ban", apiTestData.getBan());
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("unBilledCycleStartDate", "2017-10-01");
		headers.put("easyPayStatus", "true");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid", "PaymentsAPITest");
		headers.put("applicationId", "MyTMO");
		headers.put("channelId", "web");
		headers.put("channelVersion", "213123");
		headers.put("clientId", "e-servicesUI");
		headers.put("transactionBusinessKey", "MSISDN");
		headers.put("transactionBusinessKeyType", apiTestData.getMsisdn());
		headers.put("transactionType", "billing");
		headers.put("userRole", "PAH");
		headers.put("billingCycleEndDate", "10/12/2018");
		headers.put("easyPayIndicator", "true");
		return headers;
	}
}
