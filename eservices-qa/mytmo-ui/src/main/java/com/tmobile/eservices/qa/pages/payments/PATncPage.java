package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * URL : onetimepayment/amount
 * 
 * @author pshiva
 *
 */
public class PATncPage extends CommonPage {

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	@FindAll({ @FindBy(css = "div[ng-click*='Amount']"), @FindBy(xpath = "//span[contains(text(),'Amount')]") })
	private WebElement amountIcon;

	@FindBy(css = "[ng-click='vm.updateAmount()']")
	private WebElement updateAmount;

	@FindBy(css = "div.other-amount-alert")
	private List<WebElement> otherAmountAlert;

	@FindBy(css = "div#continueAmountAlert button")
	private WebElement yesOtherAmountAlert;

	private final String pageUrl = "paymentarrangement/tnc";

	// termsAndConditions
	/**
	 * page constructor
	 * 
	 * @param webDriver
	 */
	public PATncPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PATncPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PATncPage verifyPageLoaded() {
		try {
			checkPageIsReady();

			verifyPageUrl();

			Reporter.log("Verified PA Terms and conditions  displyaed");
		} catch (Exception e) {
			Assert.fail("Terms and conditions page is not displayed - PA");
		}

		return this;
	}

}