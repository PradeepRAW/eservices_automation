/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class AutoPayPage extends CommonPage {

	@FindBy(xpath = "//p[contains(text(),'Bill due')]")
	private WebElement autoPayScheduledDate;

	@FindBy(css = "div.text-center.icon")
	private WebElement autoPayLogo;

	@FindBy(css = "button.glueButton.glueButtonSecondary")
	private WebElement cancelAutoPayLink;

	@FindBy(css = "button.btn.btn-primary.padding-bottom-large.PrimaryCTA.no-padding.width-md.glueButton")
	private WebElement yesCancelAutopay;

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	private static final String pageUrl = "autopay";

	public AutoPayPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify AutoPay Page
	 * 
	 * @return
	 */
	public AutoPayPage verifyAutoPayPage() {

		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("AutoPay page displayed");
		} catch (Exception e) {
			Assert.fail("AutoPay page not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AutoPayPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * click cancel auto pay link
	 */
	public AutoPayPage clickCancelButton() {
		try {
			checkPageIsReady();
			cancelAutoPayLink.click();
			Reporter.log("Clicked on CancelAuto pay Link");
		} catch (Exception e) {
			Verify.fail("Failed to click cancel Auto pay Link");
		}
		return this;
	}

	/**
	 * click yes button for cancel auto pay
	 */
	public AutoPayPage clickYesButton() {
		try {
			yesCancelAutopay.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Yes Cancel Auto Pay");
		} catch (Exception e) {
			Assert.fail("Fail to click on Yes Cancel Auto pay");
		}
		return this;

	}

}
