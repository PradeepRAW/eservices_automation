package com.tmobile.eservices.qa.api.eos;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

public class DeviceTradeInApiV3 extends ApiCommonLib{
	
	/***
	 * The term ‘Loan’ used as micro-service name and its operations reflect the point-of-view of a T-Mobile customer.
	 * The term ‘loan’ used in the context of OFSLL or the EIP system represents the indvidual items being financed.
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
	//Gets a list of devices that can be traded in. Gets an array of carriers, array of makes for each carrier and an array of model
	//for each make.
    public Response getTradeInDevices(ApiTestData apiTestData, String requestBody,Map<String, String> tokenMap ) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData,tokenMap);
		String resourceURL = "v3/tradein/devices";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
         return response;
    }
    
    //Checks if a device is eligible for tradein
    public Response checkDeviceEligibleForTradeIn(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v3/tradein/eligibility";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    }
    
    //Gets questions regarding the condition of the device based on carrier/make/model. This service gets the questions 
    //and answer options that we need to check regarding the physical condition of the device.
    public Response getQuestionAndAnswersOfDevice(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData, tokenMap);
    	Map<String, String> queryParams = buildGetQuestionAndAnswersOfDeviceQueryparams(apiTestData);
    	RestService service = new RestService(requestBody, eos_base_url);
    	System.out.println(service);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			reqSpec.addQueryParams(queryParams);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/questions", RestCallType.GET);
 			Response response = service.callService("v3/tradein/questions", RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    //Creates a quote based on device information
    public Response createQuoteTradeIn(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v3/tradein/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         return response;
    }
    
    
    //Submit Quote
    public Response submitQuoteTradeIn(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v3/tradein/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
         return response;
    }
    
  //Get a trade in request
    public Response getTradeInBasedOnQuotedId(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v3/tradein/"+getToken("quoteId");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
         return response;
    }
    
    
    //Get a trade in request
    public Response getShippingLabelBasedOnRMANumber(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData, tokenMap);
    	Map<String, String> queryParams = buildGetShippingLabelBasedOnRMANumberQueryparams(apiTestData);
    	
    	RestService service = new RestService(requestBody, eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
         	reqSpec.addQueryParams(queryParams);
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         //Response response = service.callService("v2/tradein/"+getToken("rmaNumber")+"/shippinglabel", RestCallType.GET);
 			Response response = service.callService("v3/tradein/"+getToken("rmaNumber")+"/shippinglabel", RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    //Gets lookup data for tradein flow
    public Response getLookUpDataForTradeInFlow(ApiTestData apiTestData, String codeType, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v3/tradein/codes?codeType="+codeType;
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
         return response;
    }
    
    //list of devices on the account that are eligible for trade-in
    //This service gets the list of devices associated with each line on the account that are eligible for 
    //a trade-in during a qualifying transaction.
    public Response getAccountDeviceListEligibleForTradeIn(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildDeviceTradeInAPIHeader(apiTestData,tokenMap);
		String resourceURL = "v3/tradein/accountdevices";
    	Response response = invokeRestServiceCall(headers, "https://qlab02.eos.corporate.t-mobile.com/", resourceURL, requestBody  ,RestServiceCallType.POST);
         return response;	
    }
    
        
    private Map<String, String> buildDeviceTradeInAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	//clearCounters(apiTestData);
		Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		
		Map<String, String> headers = new HashMap<String, String>();
		
		headers.put("Content-Type","application/json");
		headers.put("Authorization",jwtTokens.get("jwtToken"));
		headers.put("correlationId",checkAndGetPlattokenJWT(apiTestData, jwtTokens.get("jwtToken")));
		headers.put("applicationId","MyTMO");
		headers.put("channelId","Web");
		headers.put("Connection","keep-alive");
		headers.put("clientId","e-servicesUI");
		headers.put("transactionBusinessKey", "BAN");
		headers.put("transactionBusinessKeyType",apiTestData.getBan());
		headers.put("transactionType","Tradein");
		headers.put("Cache-Control","no-cache");
		headers.put("usn","testUSN");
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionid","6cf0b4ca-4e94-421e-a8d9-4c64e03f1042");
		headers.put("transactionType", "Tradein");
		return headers;
	}
    
    private Map<String, String> buildGetQuestionAndAnswersOfDeviceQueryparams(ApiTestData apiTestData) throws Exception {
    	Map<String, String> headers = new HashMap<String, String>();
		headers.put("carrierName","T-Mobile");
		headers.put("makeName","Apple");
		headers.put("modelName","iPhone%205%2032GB%20black%20-%20T-Mo%20-%20ME488LL/A");
		return headers;
	}

    private Map<String, String> buildGetShippingLabelBasedOnRMANumberQueryparams(ApiTestData apiTestData) throws Exception {
    	Map<String, String> headers = new HashMap<String, String>();
		headers.put("banNumber",apiTestData.getBan());
		headers.put("language","en");
		return headers;
	}
}
