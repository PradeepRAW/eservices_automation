package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class PreviousChargePage extends CommonPage {

	@FindBy(css = ".d-none.d-lg-inline-block")
	private WebElement backToSummaryLink;
	
	@FindBy(css = "i.fa.fa-angle-left")
	private WebElement arrowLeft;
	
	@FindBy(css = "i.fa.fa-angle-right")
	private WebElement arrowRight;
	
	@FindBy(css = "bb-slider div.bb-slider-item-amount")
	private List<WebElement> amountBlock;
	
	@FindBy(css = "img.loader-icon")
	private WebElement pageSpinner;

	@FindBy(css = "span.bb-back")
	private WebElement pageLoadElement;

	private final String pageUrl = "charge/previousCharge";

	@FindBy(xpath = "//bb-slider//div[contains(text(),'Balance')]")
	private WebElement balanceTabInSlider;
	
	@FindBy(css = "a.bb-taxes-link")
	private WebElement viewIncludedTaxesAndFeesLink;
	
	@FindBy(css = "div.bb-taxes-title")
	private WebElement includedTaxesAndFeesModal;
	
	@FindBy(css = "img.bb-close-image")
	private WebElement includedTaxesAndFeesModalCloseBtn;
	
	@FindBy(css ="div.bb-charge-detail-total")
	private WebElement totalAmountLabel;
	
	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public PreviousChargePage(WebDriver webDriver) {
		super(webDriver);
	}
	/**
     * Verify that current page URL matches the expected URL.
     */
    public PreviousChargePage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public PreviousChargePage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
    		//waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
	    	waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("Plan installment page not loaded");
    	}
        return this;
    }

    /**
	 * verify that Balance is displayed on the slider
	 * @return
	 */
	public PreviousChargePage verifyBalanceInSlider() {
		try {
			balanceTabInSlider.isDisplayed();
			Reporter.log("One Time Charge slider is displayed");
		} catch (Exception e) {
			Assert.fail("One Time Charge slider is not present");
		}
		return this;
	}
    
	/**
	 * Click on View included taxes and fees link.
	 *
	 * @throws InterruptedException
	 */
	public PreviousChargePage clickViewInlcudedTaxesFeesLink() {
		try {
			viewIncludedTaxesAndFeesLink.click();
			Reporter.log("Clicked on View included taxes and fees link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on View included taxes and fees link.");
		}
		return this;
	}

	/**
	 * Verify that the Included Taxes And Fees Modal loaded completely.
	 *
	 */
	public PreviousChargePage verifyIncludedTaxesAndFeesModal() {
		try {
			includedTaxesAndFeesModal.isDisplayed();
			Assert.assertTrue(includedTaxesAndFeesModal.getText().equals("Included taxes & fees"), "Expected text is not displayed on modal header");
			Reporter.log("Included Taxes And Fees Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Included Taxes And Fees Modal is not present");
		}
		return this;
	}

	/**
	 * Click on Included taxes and fees modal close button.
	 *
	 */
	public PreviousChargePage clickInlcudedTaxesFeesModalCloseBtn() {
		try {
			includedTaxesAndFeesModalCloseBtn.click();
			Reporter.log("Clicked on included taxes and fees Modal close Button.");
		} catch (Exception e) {
			Assert.fail("Fail to click on included taxes and fees Modal close Button.");
		}
		return this;
	}
	
	/**
     * Verify page elements
     *
     * @throws InterruptedException
     */
    public PreviousChargePage verifyChargePlanPageElements() {
        try {
        	Assert.assertTrue(backToSummaryLink.isDisplayed(), "Back To Summary link is not displayed");
        	Assert.assertTrue(balanceTabInSlider.isDisplayed(), "Equipment header is not displayed");
        	Assert.assertTrue(arrowLeft.isDisplayed(), "Arrow left is not displayed");
        	Assert.assertTrue(arrowRight.isDisplayed(), "Arrow right is not displayed");
        	for (WebElement amount : amountBlock) {
        		Assert.assertTrue(amount.getText().contains("$"), "Dollar sign is not displayed");
        	}
        } catch (Exception e) {
            Assert.fail("Fail to verify previous Charge page elements");
        }
        return this;
    }
    
	/**
	 * Click Back to Summary page link
	 *
	 */
	public PreviousChargePage clickBackToSummaryLink() {
		try {
			backToSummaryLink.click();
			Reporter.log("Clicked on Back To Summary Link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on Back To Summary Link.");
		}
		return this;
	}
	/**
     * Verify Total Amount label
     *
     * @throws InterruptedException
     */
    public PreviousChargePage verifyTotalAmountLabel(){
        	try {
            	totalAmountLabel.isDisplayed();
    			Reporter.log("Total Amount Label is displayed");
    		} catch (Exception e) {
    			Assert.fail("Total Amount Label is not present");
    		}
    		return this;
    }
}
