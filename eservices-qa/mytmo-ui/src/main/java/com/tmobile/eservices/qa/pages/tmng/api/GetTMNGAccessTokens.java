package com.tmobile.eservices.qa.pages.tmng.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Headers;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;

import io.restassured.response.Response;

public class GetTMNGAccessTokens extends ApiCommonLib {

	public String environment;
	public String iam_server;
	public String tmng_api_base_url;
	public String tmng_oauth_base_url;
	public String authheader;

	public GetTMNGAccessTokens() {
		if (StringUtils.isNoneEmpty(System.getProperty("environment"))) {
			environment = System.getProperty("environment");
		} else {
			Assert.fail("Please check your runtime environment variable");
		}

		if (StringUtils.isNoneEmpty(System.getProperty("iam_servers"))) {
			iam_server = System.getProperty("iam_servers");
		} else {
			Assert.fail("Please check your runtime iam_servers variable");
		}
		tmng_api_base_url = getEOSEndPoint("ms" + "_" + iam_server);
	}
	/*
	 * public void setUpEnv() {
	 * 
	 * }
	 */

	public String getTmngAccessToken(String iam_server, String baseUrl) throws Exception {
		String accessTokenTmng = "";
		List<Header> headerList = new LinkedList<Header>();
		HashMap<String, String> formParametersMap = new HashMap<String, String>();
		Header header1 = new Header("Content-Type", "application/x-www-form-urlencoded");
		headerList.add(header1);
		Headers headers = new Headers(headerList);
		formParametersMap.put("grant_type", "client_credentials");
		formParametersMap.put("client_id", tmngApiClientIDMap.get(iam_server));
		// String requestURL =
		// eos_clear_token_url+"/oauth2/v1/token?scope=TMO_ID_profile
		// token_validation&grant_type=client_credentials";
		// String requestURL = "https://api.t-mobile.com/oauth/v1/access";
		String requestURL = baseUrl + "/oauth/v1/access";
		Reporter.log(requestURL.trim());
		com.jayway.restassured.response.Response response = null;
		response = RestAssured.given().config(new RestAssuredConfig()).headers(headers)
				.contentType("application/x-www-form-urlencoded").formParameters(formParametersMap).when()
				.relaxedHTTPSValidation().post(requestURL.trim()).then().extract().response();

		// System.out.println("Clear Counter AccessToken : "+response.statusCode()+"
		// "+response.body().asString());

		if (response.statusCode() == 200) {
			Reporter.log("<b>" + "clearcounter" + " Response : </b>" + response.body().asString());
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = null;
			try {
				jsonNode = mapper.readTree(response.asString());
			} catch (JsonProcessingException e) {
				Assert.fail("<b>JsonProcessingException Response :</b> " + e);
				Reporter.log(" <b>JsonProcessingException Response :</b> ");
				Reporter.log(" " + e);
				e.printStackTrace();
			} catch (IOException e) {
				Assert.fail("<b>IOException Response :</b> " + e);
				Reporter.log(" <b>IOException Response :</b> ");
				Reporter.log(" " + e);
				e.printStackTrace();
			}
			if (!jsonNode.isMissingNode()) {

				accessTokenTmng = getPathVal(jsonNode, "access_token");
				if (StringUtils.isNoneEmpty(accessTokenTmng)) {
					return accessTokenTmng;
				}
			}

		} else {
			Reporter.log("<b>" + "clearcounter" + " Exception Response : </b>" + response.body().asString());
			Assert.fail("<b>Failed to Fetch Access Token:</b> " + response.body().asString());

		}
		return accessTokenTmng;

	}

	public String getOAuthTmngToken() throws Exception {
		// String baseURI = oAuth.get(environment);
		tmng_oauth_base_url = oAuth.get(iam_server);
		authheader = ah.get(iam_server);
		RestService rest = new RestService("", tmng_oauth_base_url);
		// System.out.println("Auth Base URL : " + tmng_oauth_base_url);
		rest.addHeader("Authorization", authheader);
		rest.addHeader("Accept", "application/json");
		Response response = rest.callService("oauth2/v4/tokens", BaseService.RestCallType.POST);
//		System.out.println("REST " + environment + " OAuth Response::" + response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			String access_token = response.path("access_token").toString();
			if (null == tokenMapCommon) {
				tokenMapCommon = new HashMap<String, String>();
			}
			// tokenMapCommon.put("accessToken", access_token);
			tokenMapCommon.put("oAuthAccessToken", access_token);
//			System.out.println("Access token : " + access_token);
			return access_token;
		}
		return null;

	}

}
