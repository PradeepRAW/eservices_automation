/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Abhinav Shingate
 *
 */
public class JumpCartPage extends CommonPage {

	public static final String pageUrl = "jump/cart";

	@FindAll({ @FindBy(css = ".h4-title.title_color"), @FindBy(css = "#pageContainerId") })
	private WebElement jumpCartPage;

	@FindBy(css = "div.modal-background-mobile.ipad-margin-content")
	private WebElement breakdownModal;

	@FindBy(css = "p[ng-bind*=\"$ctrl.jumpCartModel.defaultShipMethod[0]\"]")
	private WebElement shippingPrice;

	@FindBy(css = "a2 a2")
	private WebElement shippingAddress;

	@FindBy(css = "#acsMainInvite .acsCloseButton--container>span>a")
	private List<WebElement> mobileFeedBackAlert;

	@FindBy(css = "input[ng-model*='$ctrl.jumpCartModel.nameOnCard']")
	private WebElement nameOnCard;

	@FindBy(css = "input[ng-model*='$ctrl.jumpCartModel.cardNumber']")
	private WebElement cardNumber;

	@FindBy(css = "input[ng-model*='$ctrl.jumpCartModel.expiryDate']")
	private WebElement expirationDate;

	@FindBy(css = "input[ng-model*='$ctrl.jumpCartModel.cvv']")
	private WebElement cvv;

	@FindBy(css = "span.check1")
	private WebElement billingAddressCheckBox;

	@FindBy(css = "input[ng-model*='$ctrl.jumpCartModel.billingAddress.addressLine1']")
	private WebElement billingAddressLineOne;

	@FindBy(css = "input[ng-model*='$ctrl.jumpCartModel.billingAddress.city']")
	private WebElement billingAddressCity;

	@FindBy(xpath = "//div/*[contains(@ng-bind,'$ctrl.jumpCartModel.contentData.stateText')]/following-sibling::div//li/a")
	private List<WebElement> billingState;

	@FindBy(css = "input[ng-model*='$ctrl.jumpCartModel.billingAddress.zipCode']")
	private WebElement billingZipCodetxt;

	@FindBy(xpath = "//div/*[contains(@ng-bind,'$ctrl.jumpCartModel.contentData.stateText')]/following-sibling::div//i")
	private WebElement clickOnBillingState;

	@FindBy(css = "button[ng-click*='$ctrl.checkForNegativeFile()']")
	private WebElement acceptAndContinue;

	@FindBy(css = "p[ng-bind*=\"$ctrl.jumpCartModel.cartDetails.taxesAndFees\"]")
	private WebElement taxesAndFeesPrice;

	@FindBy(css = "p[ng-bind*=\"$ctrl.jumpCartModel.cartDetails.newDevice.downPayment\"]")
	private WebElement downPaymentPrice;

	@FindBy(css = "h4[ng-bind*=\"$ctrl.jumpCartModel.cartDetails.orderTotal\"]")
	private WebElement orderTotalPrice;

	@FindBy(css = "span[ng-bind-html*='eIPLegalText']")
	private WebElement legalText;

	@FindBy(xpath = "//div/*[contains(@ng-bind,'selectShippingMethodText')]/following-sibling::div//li/a")
	private List<WebElement> shippingMethods;

	@FindBy(xpath = "//div/*[contains(@ng-bind,'selectShippingMethodText')]/following-sibling::div/div")
	private WebElement shippingMethodOption;

	@FindBy(xpath = "//*[contains(@ng-bind,'estimatedShipDateText')]")
	private WebElement estimateShippingText;

	@FindBy(xpath = "//*[contains(@ng-bind,'estimatedShipDate')]/following-sibling::p")
	private WebElement estimateShippingDate;

	@FindBy(css = "span.fine-print-body-new.margin-left")
	private WebElement fullRetailprice;

	String maskedElement = null;

	public JumpCartPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Jump Cart Page URL
	 * 
	 * @return
	 */
	public JumpCartPage verifyJumpCartPageURL() {
		waitforSpinner();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				if (mobileFeedBackAlert.size() == 1) {
					mobileFeedBackAlert.get(0).click();
				}
				getDriver().getCurrentUrl().contains(pageUrl);
			} else {
				getDriver().getCurrentUrl().contains(pageUrl);
			}

		} catch (Exception e) {
			Assert.fail("Jump cart page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Jump Cart Page
	 * 
	 * @return
	 */
	public JumpCartPage verifyJumpCartPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyJumpCartPageURL();
			waitFor(ExpectedConditions.visibilityOf(jumpCartPage));
			Assert.assertTrue(jumpCartPage.isDisplayed(), "Jump Cart Page is not displayed");
			Reporter.log("JumpCartPage is displayed and Verified");
		} catch (Exception e) {
			Assert.fail("Failed to Verify JumpCartPage");
		}
		return this;
	}

	/**
	 * Verify Shipping address
	 * 
	 * @return
	 */
	public JumpCartPage verifyShippingAddress() {
		waitforSpinner();
		try {
			Assert.assertTrue(shippingAddress.isDisplayed(), "Shipping address is not displayed");
			Reporter.log("Shipping address is displayed and Verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify Shipping address");
		}
		return this;
	}

	/**
	 * Verify Shipping Price is displayed
	 * 
	 * @return
	 */
	public JumpCartPage verifyShippingPriceIsDisplayed() {
		waitforSpinner();
		try {
			Assert.assertTrue(shippingPrice.isDisplayed(), "Shipping price is not displayed");
			Assert.assertTrue(shippingPrice.getText().contains("$"), "$ is not present in Shipping price.");
			Reporter.log("Shipping price is displayed  and Verified with $");
		} catch (Exception e) {
			Assert.fail("Failed to display Shipping price with $");
		}
		return this;
	}

	/**
	 * Get Down Payment Integer format
	 * 
	 * @return
	 */
	public Double getDownPaymentIntegerJumpCartPage() {
		String downPayment;
		try {
			Assert.assertTrue(downPaymentPrice.isDisplayed(), "Downpayment amount is not available");
			moveToElement(orderTotalPrice);
			downPayment = downPaymentPrice.getText();
			String[] arr = downPayment.split("\\$", 0);
			return Double.parseDouble(arr[1]);
		} catch (Exception e) {
			Reporter.log("Unable to get downpayment data");
		}
		return 0.00;
	}

	/**
	 * Get Shipping price Integer format
	 * 
	 * @return
	 */
	public Double getShippingpriceIntegerJumpCartPage() {
		String shipping;
		try {
			moveToElement(shippingPrice);
			shipping = shippingPrice.getText();
			String[] arr = shipping.split("\\$", 0);
			return Double.parseDouble(arr[1]);
		} catch (Exception e) {
			Reporter.log("Unable to get shipping price data");
		}
		return 0.00;
	}

	/**
	 * Get Taxes and Fees price Integer format
	 * 
	 * @return
	 */
	public Double getTaxesFeesAndPriceIntegerJumpCartPage() {
		String taxesAndFees;
		try {
			moveToElement(taxesAndFeesPrice);
			taxesAndFees = taxesAndFeesPrice.getText();
			String[] arr = taxesAndFees.split("\\$", 0);
			return Double.parseDouble(arr[1]);
		} catch (Exception e) {
			Reporter.log("Unable to get taxes and fees price data");
		}
		return 0.00;
	}

	/**
	 * Get Order total price Integer format
	 * 
	 * @return
	 */
	public Double getOrderTotalPriceIntegerJumpCartPage() {
		String orderTotal;
		try {
			moveToElement(orderTotalPrice);
			orderTotal = orderTotalPrice.getText();
			String[] arr = orderTotal.split("\\$", 0);
			return Double.parseDouble(arr[1]);
		} catch (Exception e) {
			Reporter.log("Unable to get Total price data");
		}
		return 0.00;
	}

	/**
	 * Verify FrP Separated Legal text
	 */
	public JumpCartPage verifyFRPseperatedfromLegalText() {
		try {
			waitforSpinner();
			Assert.assertFalse(legalText.getText().contains("full retail price of"),
					"FRP not seperated from legal text");
			Reporter.log("FRP is seperated from legal text");
		} catch (Exception e) {
			Assert.fail("FRP not seperated from legal text");
		}
		return this;
	}

	/**
	 * Verify Full retail price
	 */
	public JumpCartPage verifyFullRetailPrice() {
		try {
			waitforSpinner();
			fullRetailprice.isDisplayed();
			Reporter.log("FRP is displayed");
		} catch (Exception e) {
			Assert.fail("FRP is not displayed");
		}
		return this;
	}

	/**
	 * Verify Shipping methods
	 */
	public JumpCartPage verifyShippingMethods() {
		waitforSpinner();
		try {
			clickShippingMethodOption();
			Assert.assertTrue(shippingMethods.get(0).getText().contains("Ground"),
					"Ground Shipping method is not available");
			Assert.assertTrue(shippingMethods.get(1).getText().contains("Two Day"), "Two Day method is not available");
			Assert.assertTrue(shippingMethods.get(2).getText().contains("Next Day"),
					"Next Day method is not available");
			clickShippingMethodOption();
			Reporter.log("Ground Shipping Method is Displayed and Verified");
			Reporter.log("Two Day Shipping Method is Displayed and Verified");
			Reporter.log("Next Day Shipping Method is Displayed and Verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify Shipping methods");
		}
		return this;
	}

	/**
	 * Verify Shipping methods
	 */
	public JumpCartPage verifyJumpShippingMethods() {
		waitforSpinner();
		try {
			clickShippingMethodOption();
			Assert.assertTrue(shippingMethods.get(0).getText().contains("Two Day"), "Two Day method is not available");
			Assert.assertTrue(shippingMethods.get(1).getText().contains("Next Day"),
					"Next Day method is not available");
			clickShippingMethodOption();
			Reporter.log("Two Day Shipping Method is Displayed and Verified");
			Reporter.log("Next Day Shipping Method is Displayed and Verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify Shipping methods");
		}
		return this;
	}

	/**
	 * Verify Estimate shipping details
	 */
	public JumpCartPage verifyEstimateShippingDetails() {
		waitforSpinner();
		try {
			Assert.assertTrue(estimateShippingText.isDisplayed(), "Estimate Shipping Text is not available");
			Assert.assertTrue(estimateShippingDate.isDisplayed(), "Estimate Shipping Date is not available");
			Reporter.log("Estimated Shipping Text Is displayed and Verified");
			Reporter.log("Estimated Shipping Date Is displayed and Verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify estimated shipping details");
		}
		return this;
	}

	/**
	 * Verifies due today total prices
	 *
	 * @param dueTodayTotal
	 * @param totalPrice
	 * @return
	 */
	public JumpCartPage verifyTotalDueTodayPrices(Double dueTodayTotal, Double totalPrice) {
		try {
			String dueToday = dueTodayTotal.toString();
			String totalTodayPrice = totalPrice.toString();
			Assert.assertTrue(totalTodayPrice.contains(dueToday), "Today due today prices are not matching");
			Reporter.log("Today due today prices are verified In Cart");
		} catch (Exception e) {
			Assert.fail("Failed to verify Due today total on cart");
		}
		return this;
	}

	/**
	 * Click on Shipping method option
	 * 
	 * @return
	 */
	public JumpCartPage clickShippingMethodOption() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(shippingMethodOption), 20);
			moveToElement(shippingMethodOption);
			shippingMethodOption.click();
			Reporter.log("Clicked on Shipping method option");
		} catch (Exception e) {
			Assert.fail("Unbale to click on Shipping method option");
		}
		return this;
	}

	public JumpCartPage enterCardHoldername(String firstName) {
		waitforSpinner();
		checkPageIsReady();
		try {
			sendTextData(nameOnCard, firstName);

		} catch (Exception e) {
			Assert.fail("name is not displayed");
		}
		return this;

	}

	public JumpCartPage enterCardNumber(String creditCardNumber) {
		try {

			sendTextData(cardNumber, creditCardNumber);
			cardNumber.sendKeys(Keys.TAB);
			waitforSpinner();
			Reporter.log("Entered Card Number");

		} catch (Exception e) {
			Assert.fail("Card holder number field is not displayed");
		}
		return this;
	}

	public JumpCartPage enterCardExpiryDate(String expiryDate) {
		waitforSpinner();
		try {
			waitFor(ExpectedConditions.elementToBeClickable(expirationDate), 10);
			expirationDate.sendKeys(Keys.ENTER);
			Reporter.log("Hereeeee");

			sendTextData(expirationDate, expiryDate);
			waitforSpinner();
			Reporter.log("Entered Expiry Date Number");

		} catch (Exception e) {
			Assert.fail("Card expiry field is not displayed");
		}
		return this;
	}

	public JumpCartPage enterCVV(String cardCvv) {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.elementToBeClickable(cvv), 10);
			sendTextData(cvv, cardCvv);
			cvv.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Assert.fail("CVV field is not displayed");
		}
		return this;
	}

	/**
	 * Click Billing And Shipping CheckBox
	 */
	public JumpCartPage clickBillingAndShippingCheckBox() {
		try {
			moveToElement(billingAddressCheckBox);
			clickElementWithJavaScript(billingAddressCheckBox);
			Reporter.log("Clicked on Billing and shipping check box");

		} catch (Exception e) {
			Assert.fail("User can not click Billing and shipping check box");
		}
		return this;
	}

	/**
	 * Fill Shipping Address Information
	 */
	public JumpCartPage fillBillingAddressInfo() {
		try {
			setAddressOneForBilling(Constants.Real_ADDRESSLINE1);
			setCityForBilling(Constants.Real_CITY);
			selectStateForBilling(Constants.Real_State);
			setZipCodeForBilling(Constants.Real_ZIP_CODE);
		} catch (Exception e) {
			Assert.fail("Billing address information is not set");
		}
		return this;
	}

	private JumpCartPage setAddressOneForBilling(String address) {
		try {
			sendTextData(billingAddressLineOne, address);
			Reporter.log("Address line one is displayed In Payment Page Billing");

		} catch (Exception e) {
			Assert.fail("Address line one is not displayed In Payment Page Billing");
		}
		return this;
	}

	private JumpCartPage setCityForBilling(String city) {
		try {
			sendTextData(billingAddressCity, city);
			Reporter.log("City name is displayed");
		} catch (Exception e) {
			Assert.fail("City name is not displayed");
		}
		return this;
	}

	public JumpCartPage selectStateForBilling(String option) {
		waitforSpinner();
		try {
			clickOnBillingState.click();
			for (WebElement webElement : billingState) {
				if (webElement.getText().equalsIgnoreCase(option)) {
					webElement.click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Selecting State  Is Failed");
		}
		return this;
	}

	private JumpCartPage setZipCodeForBilling(String zipCode) {
		try {
			sendTextData(billingZipCodetxt, zipCode);
			Reporter.log("Zip code is displayed");
		} catch (Exception e) {
			Assert.fail("Zip code is not displayed");
		}
		return this;
	}

	/**
	 * Click Accept And Place Order
	 */
	public JumpCartPage clickAcceptAndPlaceOrder() {
		try {
			waitforSpinner();
			moveToElement(acceptAndContinue);
			clickElementWithJavaScript(acceptAndContinue);
			Reporter.log("Accept and Place order button is clickable");
		} catch (Exception e) {
			Assert.fail("Accept and Place order button is not clickable");
		}
		return this;
	}
}
