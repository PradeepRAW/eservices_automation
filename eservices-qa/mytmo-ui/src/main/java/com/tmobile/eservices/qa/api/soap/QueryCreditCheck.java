package com.tmobile.eservices.qa.api.soap;

import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.service.SoapService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;


public class QueryCreditCheck extends ApiCommonLib{

	
	public Response getCRP(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap){
		Response response = null;
		String soapAction = soapActions.get("crp_action");
		Reporter.log(soapUrl.get("creditCheckRequest"));
		Reporter.log("Request Type : GET");
		try{
		SoapService soapService = new SoapService(requestBody, "", soapUrl.get("creditCheckRequest"));
		soapService.addHeader("Content-Type", "text/xml");

		response = soapService.callService();
		System.out.println(response.body().asString());

		}catch(Exception e){
			Assert.fail("<b>Query Credit Check Exception Response :</b> " + e);
			Reporter.log(" <b>Query Credit Check Exception Response :</b> ");
			Reporter.log(" " + e);
		}
		return response;
	}
	
}
