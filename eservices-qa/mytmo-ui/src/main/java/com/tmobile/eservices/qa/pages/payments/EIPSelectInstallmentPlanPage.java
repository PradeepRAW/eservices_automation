package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class EIPSelectInstallmentPlanPage extends CommonPage {

	private final String pageUrl = "/selectInstallmentplan";

	@FindBy(css = "span.Display3")
	private WebElement pageHeader;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement cancelBtn;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement selectBtn;

	@FindBy(css = "input.check")
	private List<WebElement> eipRadioInputs;

	@FindBy(css = "input.check + label")
	private List<WebElement> eipRadioBtns;

	@FindBy(css = "span.pull-left.Display6")
	private List<WebElement> eipFriendlyNames;

	@FindBy(css = "span.pull-left.legal")
	private List<WebElement> eipMsisdns;

	@FindBy(css = "span.pull-left.body.black")
	private List<WebElement> eipInfoHeaders;

	@FindBy(css = "span.pull-right.body.black")
	private List<WebElement> eipInfoValues;

	public EIPSelectInstallmentPlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EIPSelectInstallmentPlanPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public EIPSelectInstallmentPlanPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Select Installment Plan Page is loaded.");
		} catch (Exception e) {
			Assert.fail("Select Installment Plan page is not loaded");
		}
		return this;
	}

	public void clickCancelBtn() {
		try {
			cancelBtn.click();
			Reporter.log("Clicked on Cancel Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Cancel button");
		}

	}

	/**
	 * click on Select button
	 */
	public void clickSelectBtn() {
		try {
			selectBtn.click();
			Reporter.log("Clicked on Select Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Select button");
		}
	}

	/**
	 * verify cancel button is displayed
	 */
	public void verifyCancelBtn() {
		try {
			cancelBtn.isDisplayed();
			Reporter.log("Cancel Button is displayed");
		} catch (Exception e) {
			Assert.fail("Cancel button is not found");
		}
	}

	/**
	 * select other eip plan
	 */
	public void selectOtherEipPlan() {
		try {
			for (int i = 0; i < eipRadioInputs.size(); i++) {
				if (!eipRadioInputs.get(i).isSelected()) {
					eipRadioBtns.get(i).click();
					Reporter.log("Other EIP Plan is selected");
				}
			}
		} catch (Exception e) {
			Assert.fail("Error: Unable to select Other EIP Plan");
		}
	}

	/**
	 * Verify all page elements are displayed
	 */
	public void verifyPageElements() {
		try {
			for (int i = 0; i < eipFriendlyNames.size(); i++) {
				Assert.assertTrue(eipFriendlyNames.get(i).isDisplayed());
				Assert.assertTrue(eipMsisdns.get(i).isDisplayed());
			}
			for (int j = 0; j < eipInfoHeaders.size(); j++) {
				Assert.assertTrue(eipInfoHeaders.get(j).isDisplayed());
				Assert.assertTrue(eipInfoValues.get(j).isDisplayed());
			}
			Reporter.log("Verified all EIP information is displayed");
		} catch (Exception e) {
			Assert.fail("Error while Verifying all EIP information");
		}
	}
}
