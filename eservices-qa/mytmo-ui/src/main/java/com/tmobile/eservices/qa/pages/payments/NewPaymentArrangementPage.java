package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author pshiva
 *
 */
public class NewPaymentArrangementPage extends CommonPage {

	private final String pageUrl = "payments/paymentarrangement";

	private By spinnerPA = By.cssSelector("div.circle");

	@FindBy(css = "p.Display3")
	private WebElement pageLoadElement;

	@FindBy(css = "div.TMO-PAYMENT-BLADE-PAGE span#paymentLable")
	private WebElement paymentMethodBlade;

	@FindBy(id = "acceptButton")
	private WebElement agreeAndSubmitCTA;

	@FindBy(id = "paymentNumber")
	private WebElement paymentNumberOnBlade;

	/**
	 * 
	 * @param webDriver
	 */
	public NewPaymentArrangementPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewPaymentArrangementPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewPaymentArrangementPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
		} catch (Exception e) {
			Assert.fail("Payment Arrangement page not loaded");
		}
		return this;
	}

	/**
	 * click payment method blade
	 */
	public void clickPaymentMethodBlade() {
		try {
			paymentMethodBlade.click();
			Reporter.log("Clicked on Payment Method Blade");
		} catch (Exception e) {
			Assert.fail("Failed to click on payment method blade");
		}

	}

	/**
	 * click agree & submit CTA
	 * 
	 * @return
	 */
	public NewPaymentArrangementPage clickAgreeAndSubmitCTA() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(agreeAndSubmitCTA));
			clickElementWithJavaScript(agreeAndSubmitCTA);
			Reporter.log("Clicked on Agree And Submit CTA");
			waitFor(ExpectedConditions.invisibilityOfElementLocated(spinnerPA));
		} catch (Exception e) {
			Assert.fail("Failed to click on Agree and Submit CTA");
		}
		return this;
	}

	/**
	 * verify Agree and Submit CTA is active and enabled
	 * 
	 * @return
	 */
	public NewPaymentArrangementPage verifyAgreeAndSubmitCTAisActive() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(agreeAndSubmitCTA));
			Assert.assertTrue(agreeAndSubmitCTA.isEnabled());
			Reporter.log(" AgreeAndSubmit CTA is Active");
		} catch (Exception e) {
			Assert.fail("Agree and Submit button not found");
		}
		return this;
	}

	/**
	 * verify selected payment method on payment method blade on landing page [Bank
	 * Num/Card Num/None Provided]
	 * 
	 * @param paymentNum
	 */
	public void verifySelectedPaymentMethodOnBlade(String paymentNum) {
		try {
			Assert.assertTrue(paymentNumberOnBlade.getText().contains(paymentNum),
					"Mismatch of selected payment number and actual displayed on blade");
			Reporter.log(
					"Verified that the selected payment method is displayed on payment method blade: " + paymentNum);
		} catch (Exception e) {
			Assert.fail("Failed to verify selected payment method on blade");
		}
	}

}