
package com.tmobile.eservices.qa.api.unlockdata;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author blakshminarayana
 *
 */
public class EmailServiceImpl implements EmailService {
	private static Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
	private static final String EMAIL_EXCEPTION="unable to connect the email server";
	private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	@Override
	public Message[] readEmail(String username, String password) throws MessagingException {
		logger.info("readEmail method called in EmailServiceImpl");
		Message[] messages=null;
		Folder inboxFolder=null;
		Store store =null;
		try {
			store = getStore(username, password);
			inboxFolder = store.getFolder("INBOX");
			inboxFolder.open(Folder.READ_ONLY);
			logger.info("unread message count in email inbox{}",inboxFolder.getUnreadMessageCount());
			messages = inboxFolder.search(new FlagTerm(new Flags(Flags.Flag.RECENT), false));
		} catch (MessagingException e) {
			throw new ServiceException(EMAIL_EXCEPTION, e);
		}
		return messages;
	}

	@Override
	public boolean deleteEmails(String username, String password) throws MessagingException {
		logger.info("deleteEmails method called in EmailServiceImpl");
		Folder inboxFolder=null;
		Store store =null;
		boolean verifyMailDeleted= false;
		try {
			store = getStore(username, password);
			inboxFolder = store.getFolder("INBOX");
			inboxFolder.open(Folder.READ_WRITE);
			logger.info("unread message count in email inbox{}",inboxFolder.getUnreadMessageCount());
			Message[] messages = inboxFolder.getMessages();
			for (Message message : messages) {
				message.setFlag(Flags.Flag.DELETED, true);
				verifyMailDeleted = true;
			}
		} catch (MessagingException e) {
			throw new ServiceException(EMAIL_EXCEPTION, e);
		}finally {
			if(null != inboxFolder){
				inboxFolder.close(true);
				store.close();
			}
		}
		return verifyMailDeleted;
	}
	/**
	 * it is common method for get messages and delete messages in email server
	 * @param username
	 * @param password
	 * @return
	 */
	private Store getStore(String username, String password){
		logger.info("getStore method called in EmailServiceImpl");
		Store store =null;
		Properties props = System.getProperties();
		props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.pop3.socketFactory.fallback", "false");
		props.setProperty("mail.pop3.port", "995");
		props.setProperty("mail.pop3.socketFactory.port", "995");
		props.put("mail.pop3.host", "pop.gmail.com");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator(){
			@Override
			 protected PasswordAuthentication getPasswordAuthentication() {
                 return new PasswordAuthentication(username,password);
             }
		});
		try {
			store = session.getStore("imaps");
			store.connect("smtp.gmail.com", username, password);
		} catch (MessagingException e) {
			throw new ServiceException("unable to connect the email server", e);
		}
		return store;
	}

}
