package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class UpdateBankPage extends CommonPage {

	@FindAll({ @FindBy(css = "div h4"), @FindBy(css = "p.Display3"), @FindBy(css = "div.bank-heading span") })
	private WebElement bankHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_name')]"), @FindBy(css = "label[for='accountName']") })
	private WebElement nameOnAccountHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'routing_number')]"), @FindBy(css = "label[for='routingNum']") })
	private WebElement routingNumberHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_number')]"), @FindBy(css = "label[for='accountNum']") })
	private WebElement accountNumberHeader;

	@FindAll({ @FindBy(css = "#account_name"), @FindBy(css = "#accountName"),
			@FindBy(css = "[name='nameOnaAccount']") })
	private WebElement accountName;

	@FindAll({ @FindBy(css = "#routing_number"), @FindBy(css = "#routingNum") })
	private WebElement routingNum;

	@FindAll({ @FindBy(css = "#account_number"), @FindBy(css = "#accountNum") })
	private WebElement bankAccountNumber;

	@FindBy(linkText = "How to find my routing or account number")
	private WebElement howToFindLink;

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button.SecondaryCTA"),
			@FindBy(css = "button.secondaryCTA") })
	private WebElement backBtn;

	private final String pageUrl = "/editbank";

	@FindAll({ @FindBy(css = "input#nickName"), @FindBy(css = "input#nickname") })
	private WebElement nickName;

	@FindAll({ @FindBy(css = "label[for='nickName']") })
	private WebElement nickNameLabel;

	@FindAll({ @FindBy(css = "a[aria-label='optional field']") })
	private WebElement optionalLabel;

	@FindBy(css = "div.legal.indicator>span")
	private WebElement defaultCheckbox;

	@FindBy(xpath = "//span[contains(text(),'Set as default')]")
	private WebElement defaultCheckboxLabel;

	@FindAll({ @FindBy(id = "acceptButton"), @FindBy(id = "addbankContinueButton"), @FindBy(id = "continue") })
	private WebElement saveChangesCTA;

	@FindAll({ @FindBy(id = "deleteModelLink"), @FindBy(css = "#confirmModalButton span") })
	private WebElement removeFromMyWalletLink;

	@FindAll({ @FindBy(id = "deleteModal"), @FindBy(id = "confirmModal") })
	private WebElement deleteModal;

	@FindAll({ @FindBy(css = "#deleteModalheader"), @FindBy(css = "#confirmModal div.modal-heading-font span") })
	private WebElement deleteModalHeader;

	@FindAll({ @FindBy(css = "#deleteModal p.body.padding-top-medium"),
			@FindBy(css = "#confirmModal div.modal-body-font span") })
	private WebElement deleteModalContent;

	@FindAll({ @FindBy(css = "#deleteModal button.PrimaryCTA-accent"),
			@FindBy(css = "#confirmModal button.btn-primary") })
	private WebElement deleteModalYesBtn;

	@FindAll({ @FindBy(css = "#deleteModal #deleteModalCancelBtn"),
			@FindBy(css = "#confirmModal button.btn-secondary") })
	private WebElement deleteModalCancelBtn;

	/**
	 * Constructor
	 *
	 * @param webDriver
	 */
	public UpdateBankPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public UpdateBankPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @param pageHeaderText
	 * 
	 * @throws InterruptedException
	 */
	public UpdateBankPage verifyBankPageLoaded(String pageHeaderText) {
		try {
			checkPageIsReady();
			verifyPageUrl();
			// waitFor(ExpectedConditions.visibilityOf(paymentProcessing));
			waitFor(ExpectedConditions.elementToBeClickable(backBtn));
			bankHeader.isDisplayed();
			Assert.assertTrue(bankHeader.getText().equalsIgnoreCase(pageHeaderText), "Bank header mismatch");
			Reporter.log("Bank page is loaded and header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page is  not loaded and Header is failed to verified");

		}
		return this;
	}

	/**
	 * Verify bank page header is displayed or not.
	 * 
	 */
	public UpdateBankPage verifyBankPageHeader() {
		try {

			bankHeader.isDisplayed();
			Assert.assertTrue(bankHeader.getText().equals("Bank information"));
			Reporter.log("Bank page header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page header is failed to verified");
		}
		return this;
	}

	/**
	 * click add bank icon
	 */
	public UpdateBankPage clickBackButton() {
		try {
			backBtn.click();
			Reporter.log("Clicked on back button");
		} catch (Exception e) {
			Verify.fail("Failed to click on back button");
		}
		return this;

	}

	/**
	 * verify Account Name Placeholder
	 * 
	 * @param namePlaceHolder
	 * @return
	 */
	public UpdateBankPage verifyAccountNamePlaceholder(String namePlaceHolder) {
		try {
			getPlaceholder(accountName, namePlaceHolder);
			Reporter.log("Verified account name place holder");
		} catch (Exception e) {
			Verify.fail("Account name place holder is not displayed");
		}
		return this;
	}

	/**
	 * verify Routing Number Placeholder
	 * 
	 * @param routingNoPLaceHolder
	 * @return
	 */
	public UpdateBankPage verifyRoutingNumberPlaceholder(String routingNoPLaceHolder) {
		try {
			getPlaceholder(routingNum, routingNoPLaceHolder);
			Reporter.log("Verified Routing number place holder");
		} catch (Exception e) {
			Verify.fail("Routing number place holder is not displayed");
		}
		return this;
	}

	/**
	 * verify bank account number place holder
	 * 
	 * @param accNumberPlaceHolder
	 * @return
	 */
	public UpdateBankPage verifyBankAccNumberPlaceholder(String accNumberPlaceHolder) {
		try {
			getPlaceholder(bankAccountNumber, accNumberPlaceHolder);
			Reporter.log("Verified bank acc number place holder");
		} catch (Exception e) {
			Verify.fail("Bank Account number place holder is not displayed");
		}
		return this;
	}

	public UpdateBankPage getPlaceholder(WebElement element, String msg) {
		try {
			Verify.assertTrue(element.getAttribute("placeholder").equals(msg),
					"Placeholder text is incorrect for " + element.getAttribute("placeholder"));
		} catch (Exception e) {
			Verify.fail("Place holder not found");
		}
		return this;
	}

	public UpdateBankPage verifyNameOnAccountHeader() {
		try {
			Verify.assertTrue(nameOnAccountHeader.isDisplayed(), "Name on Account header is not displayed");
			Verify.assertTrue(nameOnAccountHeader.getText().equals("Name on Account"),
					"Name on Account header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Name on Account header missing");
		}
		return this;
	}

	public UpdateBankPage verifyRoutingNumberHeader() {
		try {
			Verify.assertTrue(routingNumberHeader.isDisplayed(), "Routing Number header is not displayed");
			Verify.assertTrue(routingNumberHeader.getText().equals("Routing Number"),
					"Routing Number header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Routing number header not found");
		}
		return this;
	}

	public UpdateBankPage verifyAccountNumberHeader() {
		try {
			Verify.assertTrue(accountNumberHeader.isDisplayed(), "Account Number header is not displayed");
			Verify.assertTrue(accountNumberHeader.getText().equals("Account Number"),
					"Account Number header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Account number header not found");
		}
		return this;
	}

	/**
	 * Verify add bank labels is displayed or not
	 */
	public UpdateBankPage verifyAddBanklabels() {
		try {
			verifyNameOnAccountHeader();
			verifyRoutingNumberHeader();
			verifyAccountNumberHeader();

		} catch (Exception e) {
			Assert.fail("add bank labels are not found");

		}
		return this;
	}

	/**
	 * verify Back CTA is displayed
	 *
	 * @return boolean
	 */
	public UpdateBankPage verifyBackCTA() {
		try {
			Verify.assertTrue(backBtn.isDisplayed(), "Back CTA is not displayed");
		} catch (Exception e) {
			Verify.fail("Fail verifying the CTA Back button");
		}
		return this;
	}

	public UpdateBankPage setAccountName(String accName) {
		try {
			accountName.sendKeys(accName);
		} catch (Exception e) {
			Assert.fail("Account Name field not found");
		}
		return this;
	}

	public UpdateBankPage setRoutingNumber(String routingNumber) {
		try {
			routingNum.clear();
			routingNum.sendKeys(routingNumber);
		} catch (Exception e) {
			Assert.fail("Fail to set the Routing Number");
		}
		return this;
	}

	public UpdateBankPage setAccountNumber(String accNumber) {
		try {
			bankAccountNumber.clear();
			bankAccountNumber.sendKeys(accNumber);
		} catch (Exception e) {
			Assert.fail("Fail to set the Account number");
		}
		return this;
	}

	/**
	 * verify how to find my routing or account number is displayed or not
	 *
	 */
	public UpdateBankPage verifyHowToFindLink() {
		try {
			howToFindLink.isDisplayed();
			Assert.assertTrue(howToFindLink.getText().equals("How to find my routing or account number"),
					"how to find link is not found");

		} catch (Exception e) {
			Assert.fail("Fail to verify the how to find link");
		}
		return this;
	}

	/**
	 * click how to find my routing or account number link
	 *
	 */
	public UpdateBankPage clickHowToFindLink() {
		try {
			howToFindLink.isDisplayed();
			howToFindLink.click();
			Reporter.log("how to find link is clicked");

		} catch (Exception e) {
			Assert.fail("Fail to verify the how to find link");
		}
		return this;
	}

	/**
	 * verify NickName Field is displayed or not
	 * 
	 *
	 */
	public UpdateBankPage verifyNickNameHeader() {
		try {
			nickNameLabel.isDisplayed();
			nickName.isDisplayed();
			optionalLabel.isDisplayed();
			Reporter.log("nickName field,label,Optional Label is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify the nickName Header elements");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public UpdateBankPage verifyNickNameDisplay() {
		try {
			if (nickName.isDisplayed()) {
				Reporter.log("nickName field is displayed");
			} else {
				Reporter.log("nickName field is Surpressed");
			}

		} catch (Exception e) {
			Assert.fail("nickName Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field is displayed or not
	 * 
	 *
	 */
	public UpdateBankPage verifyDefaultCheckBox() {
		try {
			if (defaultCheckbox.isDisplayed()) {
				Reporter.log("Default Check Box field is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Default checkbox Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field label is displayed or not
	 * 
	 *
	 */
	public UpdateBankPage verifyDefaultCheckBoxLabel() {
		try {
			if (defaultCheckboxLabel.isDisplayed()) {
				Reporter.log("Default Check Box field Label is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field Label is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Default checkbox Field Label is not Found ");
		}
		return this;
	}

	/**
	 * Click checkbox Field is Enabled or not
	 * 
	 *
	 */
	public UpdateBankPage ClickDefaultCheckBox() {
		try {
			defaultCheckbox.click();
			Reporter.log("default Check box field is Clicked");

		} catch (Exception e) {
			Assert.fail("default CheckBox is not Found ");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public UpdateBankPage verifyNoNickNameandDefaultCheckBoxDisplay() {
		try {
			if (nickName.isDisplayed() && defaultCheckboxLabel.isDisplayed() && defaultCheckbox.isDisplayed()) {
				Assert.fail("nickName Field,Default Check Box and Default CheckBox Label is Displayed");
			}

		} catch (Exception e) {
			Reporter.log("nickName Field,Default Check Box and Default CheckBox Label is not found");
		}
		return this;
	}

	/**
	 * Enter Nick Name in Bank Page
	 *
	 * @param payment
	 */
	public void enterNickName(String nicName) {
		try {
			checkPageIsReady();
			nickName.clear();
			nickName.sendKeys(nicName);
			Reporter.log("Nick Name is Enterd successfully");
		} catch (Exception e) {
			Assert.fail("Failed to fill the Nick Name");
		}

	}

	public void verifyEditBankPageElements() {
		try {
			verifyNameOnAcocuntNotNull();
			verifyRoutingNumberMasked();
			verifyAcocuntNumberMasked();
			verifyBackCTA();
			// removed for CCS phase 1
			// verifySaveChangesCTA();
			Reporter.log("Verified Edit bank page elements");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit bank page elements");
		}
	}

	private void verifyAcocuntNumberMasked() {
		try {
			bankAccountNumber.getAttribute("value").matches("[*]+");
			Reporter.log("Verified that the acocunt number is masked");
		} catch (Exception e) {
			Assert.fail("Failed to verify that the account number is masked");
		}
	}

	private void verifyRoutingNumberMasked() {
		try {
			routingNum.getAttribute("value").matches("[*]+");
			Reporter.log("Verified that the routing number is masked");
		} catch (Exception e) {
			Assert.fail("Failed to verify that the routing number is masked");
		}
	}

	private void verifyNameOnAcocuntNotNull() {
		try {
			Assert.assertTrue(accountName.isDisplayed(), "Account Name is NULL");
			Assert.assertTrue(accountName.isEnabled(), "Account Name is Disabled");
			Assert.assertTrue(!accountName.getAttribute("value").isEmpty(), "Account Name is NULL");
			Reporter.log("Verified that the Account Name is not NULL");
		} catch (Exception e) {
			Assert.fail("Failed to verify that the Account Name is not NULL");
		}
	}

	public String updateBankDetailsAndSave(Payment payment) {
		String nick = null;
		try {
			accountName.clear();
			accountName.sendKeys(payment.getCheckingName());
			nickName.clear();
			nick = generateRandomString();
			nickName.sendKeys(nick);
			saveChangesCTA.click();
			Reporter.log("Updated new Bank details and clicked on save");
		} catch (Exception e) {
			Assert.fail("Failed to update Bank details");
		}
		return nick;
	}

	public void verifyUpdatedBankDetails(Payment payment, String nick) {
		try {
			Assert.assertTrue(accountName.getAttribute("value").equals(payment.getCheckingName()),
					"Actual: " + accountName.getAttribute("value") + " & Expected: " + payment.getCheckingName());
			Assert.assertTrue(nickName.getAttribute("value").equals(nick),
					"Actual: " + nickName.getAttribute("value") + " & Expected: " + nick);
			Reporter.log("Verified the updated bank details");
		} catch (Exception e) {
			Assert.fail("Failed to verify updated Bank details");
		}
	}

	public UpdateBankPage clickRemoveFromMyWalletLink() {
		try {
			removeFromMyWalletLink.click();
			Reporter.log("Clicked on Remove from my Wallet link");
		} catch (Exception e) {
			Assert.fail("Failed to click on Remove from my Wallet link");
		}
		return this;
	}

	public UpdateBankPage verifyRemoveFromMyWalletModal() {
		try {
			waitFor(ExpectedConditions.visibilityOf(deleteModal));
			deleteModal.isDisplayed();
			deleteModalHeader.isDisplayed();
			deleteModalContent.isDisplayed();
			deleteModalYesBtn.isDisplayed();
			deleteModalCancelBtn.isDisplayed();
			Reporter.log("Verified Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify Delete modal");
		}
		return this;
	}

	public UpdateBankPage clickYesDeleteOnModal() {
		try {
			deleteModalYesBtn.click();
			Reporter.log("Clicked 'Yes, Delete button' on Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Yes, Delete button' on Delete modal");
		}
		return this;
	}

	public UpdateBankPage clickCancelOnModal() {
		try {
			deleteModalCancelBtn.click();
			Reporter.log("Clicked 'Cancel button' on Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Cancel button' on Delete modal");
		}
		return this;
	}

}
