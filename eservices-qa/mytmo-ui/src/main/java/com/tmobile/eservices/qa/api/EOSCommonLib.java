package com.tmobile.eservices.qa.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;

import groovy.json.StringEscapeUtils;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSCommonLib extends ServiceTest {
	public Map<String, String> eep;
	public Map<String, String> bbenv;
	public Map<String, String> mep;
	public static Map<String, String> ah;
	public static Map<String, String> oAuth;
	public static Map<String, String> jwt;
	public static Map<String, String> lbill;
	public static Map<String, String> lbillauth;
	public static String eos_base_url;
	public static String meezo_base_url;
	public static String backend = null;
	public static String environment = System.getProperty("environment");
	public static String environment1 = System.getProperty("environment1");
	public static String url = System.getProperty("url");

	/***
	 * Map for EOS end points
	 */
	public EOSCommonLib() {

		if (environment.contains(".com")) {

			if (environment.trim().substring(environment.length() - 1).equalsIgnoreCase("/")) {
				environment = environment.substring(0, environment.length() - 1);
			}

			url = environment;

			String[] stageenv = { "https://e1.my.t-mobile.com", "https://e3.my.t-mobile.com",
					"https://e8.my.t-mobile.com", "https://e9.my.t-mobile.com", "https://reg9.eservice.t-mobile.com" };
			String[] qlab03env = { "https://e4.my.t-mobile.com", "https://qata03.eservice.t-mobile.com" };
			String[] qlab02env = { "https://e2.my.t-mobile.com" };
			String[] prodenv = { "https://my.t-mobile.com" };
			if (Arrays.binarySearch(stageenv, environment.trim().toLowerCase()) > -1)
				environment = "stage";
			else if (Arrays.binarySearch(qlab03env, environment.trim().toLowerCase()) > -1)
				environment = "qlab03";
			else if (Arrays.binarySearch(qlab02env, environment.trim().toLowerCase()) > -1)
				environment = "qlab02";
			else
				environment = "prod";
		}

		// String environment="prod";
		// environment = System.getProperty("environment");
		eep = new HashMap<String, String>();
		eep.put("qat03", "https://qat03.pd.api.t-mobile.com");
		eep.put("stage", "https://stage.eos.corporate.t-mobile.com");
		eep.put("stage2", "https://stage2.eos.corporate.t-mobile.com");
		eep.put("qlab02", "https://qlab02.eos.corporate.t-mobile.com");
		eep.put("qlab03", "https://qlab03.eos.corporate.t-mobile.com");
		eep.put("qlab06", "https://qlab06.eos.corporate.t-mobile.com");
		eep.put("qlab07", "https://qlab07.eos.corporate.t-mobile.com");
		eep.put("prod", "https://eos.corporate.t-mobile.com");
		eep.put("AggregateAPIprod", "https://api.t-mobile.com");
		eep.put("dev", "https://dev.eos.corporate.t-mobile.com");
		eep.put("ilab02", "https://ilab02.core.op.api.t-mobile.com");

		bbenv = new HashMap<String, String>();
		bbenv.put("qlab03", "https://UAT05.qa.tmobile.britebill.com");
		bbenv.put("qlab02", "https://UAT04.qa.tmobile.britebill.com");

		oAuth = new HashMap<String, String>();
		oAuth.put("qlab02", "https://qlab02.core.op.api.t-mobile.com");
		oAuth.put("qlab03", "https://qlab03.core.op.api.t-mobile.com");
		oAuth.put("stage", "https://core.op.api.internal.t-mobile.com");
		oAuth.put("stage2", "https://core.op.api.internal.t-mobile.com");
		oAuth.put("prod", "https://core.op.api.internal.t-mobile.com");
		oAuth.put("dev", "https://qlab02.core.op.api.t-mobile.com");
		oAuth.put("qlab01", "https://qlab01.core.op.api.t-mobile.com");
		oAuth.put("qlab06", "https://qlab06.core.op.api.geo.t-mobile.com");
		oAuth.put("qlab07", "https://dlab03.core.op.api.t-mobile.com");
		oAuth.put("ilab02", "https://ilab02.core.op.api.t-mobile.com");

		jwt = new HashMap<String, String>();
		jwt.put("qlab02", "https://uat.brass.account.t-mobile.com");
		jwt.put("stage", "https://brass.account.t-mobile.com");
		jwt.put("stage2", "https://brass.account.t-mobile.com");
		jwt.put("prod", "https://brass.account.t-mobile.com");
		jwt.put("qlab03", "https://qat.brass.account.t-mobile.com");

		lbill = new HashMap<String, String>();
		lbill.put("stage", "http://stgtleims0003.unix.gsm1900.org:10002");
		lbill.put("stage2", "http://stgtleims0003.unix.gsm1900.org:10003");
		lbill.put("prod", "https://ebillservice.internal.t-mobile.com");
		lbill.put("qlab03", "https://qatpleims0003.unix.gsm1900.org:7603");
		lbill.put("qlab02", "https://qatpleims0002.unix.gsm1900.org:7613");

		/***
		 * Map for basic headers
		 */
		ah = new HashMap<String, String>();
		ah.put("dev", "Basic NU1vSDJ0VlBEOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==");
		ah.put("qlab01", "Basic NU1vSDJ0VlBEOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==");
		ah.put("qlab02", "Basic NU1vSDJ0VlBEOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==");
		// ah.put("qlab03","Basic
		// Y1V0aHdHTGoyWEZ5b0RpMmRoQUZ5YUVFQVRQUEZ0anM6ckR2STgzQUlBTlhuQWt4SA==");
		ah.put("qlab03", "Basic R3FiNUNmTUpQMThuUW80TWNZSlV5TUdKTzk3ZTg0YWU6b3pQWGFkNUM5WFI3dnIxMg==");
		ah.put("qlab06", "Basic R3FiNUNmTUpQMThuUW80TWNZSlV5TUdKTzk3ZTg0YWU6b3pQWGFkNUM5WFI3dnIxMg==");
		ah.put("qlab07", "Basic T2NBY0g5N3Q0Tm50VDJtYWl1dFhiaVdqa09rTHZwcDg6M2hMTXFzOGpqR3g5dTRCeg==");
		ah.put("stage", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("stage2", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("prod", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("ilab02", "Basic TjI3c01HYjNYU3J0STAwcVpTS1c4TlU1SXEyMVhGU1M6N1FjeHhnVXBucHlSb0tPaw==");

		lbillauth = new HashMap<String, String>();
		lbillauth.put("stage", "Basic ZWJpbGw6cGE4OGIxbGx6");
		lbillauth.put("stage2", "Basic ZWJpbGw6cGE4OGIxbGx6");
		lbillauth.put("prod", "Basic ZWJpbGw6cGE4OGIxbGx6");
		lbillauth.put("qlab02", "Basic Basic ZWJpbGw6RG9udDRHZXQ=");
		lbillauth.put("qlab03", "Basic ZWJpbGw6cGE4OGIxbGx6");

		/***
		 * Map for meezo end points
		 */
		mep = new HashMap<String, String>();
		// #ppd
		mep.put("meezo_usage_ppd", "https://mezppdusag.mezzo.eservice.t-mobile.com:8082/ws/usageservice_wsdl");
		mep.put("meezo_phone_ppd", "https://mezppdphon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("meezo_plan_ppd", "https://mezppdplan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("meezo_profile_ppd", "https://mezppdprof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("meezo_billing_ppd", "https://mezppdbill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("meezo_shop_ppd", "https://mezppdshop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #PROD
		mep.put("mz_prod_usage", "https://usag_mezzo_eservice_t-mobile_com/ws/usageservice");
		mep.put("mz_prod_phone", "https://phon_mezzo_eservice_t-mobile_com/ws/phoneservice");
		mep.put("mz_prod_plan", "https://plan_mezzo_eservice_t-mobile_com/ws/planservice");
		mep.put("mz_prod_profile", "https://prof_mezzo_eservice_t-mobile_com/ws/profileservice");
		mep.put("mz_prod_billing", "https://bill_mezzo_eservice_t-mobile_com/ws/billingservice");
		mep.put("mz_prod_shop", "https://shop_mezzo_eservice_t-mobile_com/ws/shopservice");

		// #qlab02
		mep.put("mz_qlab02_usage", "http://mezqlab02usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab02_phone", "http://mezqlab02phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab02_plan", "http://mezqlab02plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab02_profile", "http://mezqlab02prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab02_billing", "http://mezqlab02bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab02_shop", "http://mezqlab02shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #qlab03
		mep.put("mz_qlab03_usage", "http://mezqlab03usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab03_phone", "http://mezqlab03phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab03_plan", "http://mezqlab03plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab03_profile", "http://mezqlab03prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab03_billing", "http://mezqlab03bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab03_shop", "http://mezqlab03shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #dev2
		mep.put("mz_dev2_usage", "https://mezdev2usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_dev2_phone", "https://mezdev2phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_dev2_plan", "https://mezdev2plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_dev2_profile", "https://mezdev2prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_dev2_billing", "https://mezdev2bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_dev2_shop", "https://mezdev2shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #dev
		mep.put("mz_dev_usage", "https://mezdev2usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_dev_phone", "https://mezdev2phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_dev_plan", "https://mezdev2plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_dev_profile", "https://mezdev2prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_dev_billing", "https://mezdev2bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_dev_shop", "https://mezdev2shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #qlab01
		mep.put("mz_qlab01_usage", "http://mezqlab06usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab01_phone", "http://mezqlab06phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab01_plan", "http://mezqlab06plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab01_profile", "http://mezqlab06prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab01_billing", "http://mezqlab06bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab01_shop", "http://mezqlab06shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #qlab06
		mep.put("mz_qlab06_phone", "http://mezqlab07phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab06_plan", "http://mezqlab07plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab06_profile", "http://mezqlab07prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab06_usage", "http://mezqlab07usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab06_billing", "http://mezqlab07bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab06_shop", "http://mezqlab07shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #qlab07
		mep.put("mz_qlab07_phone", "http://mezqlab01phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab07_plan", "http://mezqlab01plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab07_profile", "http://mezqlab01prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab07_usage", "http://mezqlab01usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab07_billing", "http://mezqlab01bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab07_shop", "http://mezqlab01shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");
	}

	/***
	 * Get user token for Jwt Token
	 * 
	 * @param msdn
	 * @param pswd
	 * @return
	 * @throws Exception
	 */
	public Response getauthcoderequest(String misdin, String pwd) throws Exception {

		JSONObject requestParams = new JSONObject();

		requestParams.put("user_id", misdin);
		requestParams.put("password", pwd);
		requestParams.put("lang", "en");
		requestParams.put("trans_id", "Acceptance-Testing");
		requestParams.put("redirect_uri", url);
		requestParams.put("response_type", "code");
		requestParams.put("re_auth", "auto");
		requestParams.put("scope",
				"TMO_ID_profile associated_lines billing_information associated_billing_accounts extended_lines token openid");
		requestParams.put("access_type", "online");
		requestParams.put("approval_prompt", "auto");
		// System.out.println("apiURL is :"+url);
		// System.out.println("apienv is :"+environment);

		if (environment.equals("prod") || environment.equals("stage") || environment.equals("stage2")) {
			requestParams.put("client_id", "MYTMO");
		} else {
			requestParams.put("client_id", "MYTMO");
		}

		RestService restService = new RestService(requestParams.toString(), jwt.get(environment));
		restService.setContentType("application/json");
		Response response = restService.callService("ras/v1/code", RestCallType.POST);
		return response;
	}

	public String getCodefromauthtokenresponse(String misdin, String pwd) {
		String code = null;

		try {

			Response response = getauthcoderequest(misdin, pwd);
			if (response.body().asString() != null && response.getStatusCode() == 200) {
				JsonPath jsonPathEvaluator = response.jsonPath();
				if (jsonPathEvaluator.get("code") != null) {
					return jsonPathEvaluator.get("code").toString();
				}

			} else {
				// System.out.println(response.body().asString());
			}

		} catch (Exception e) {

		}

		return code;
	}

	public Response getusertokenrequest(String code) throws Exception {

		JSONObject requestParams = new JSONObject();

		requestParams.put("trans_id", "Acceptance-Testing");
		requestParams.put("code", code);
		requestParams.put("redirect_uri", url);
		requestParams.put("response_selection", "id_token.direct userinfo");
		requestParams.put("scopes", "TMO_ID_profile associated_lines extended_lines");

		if (environment.equals("prod") || environment.equals("stage") || environment.equals("stage2")) {
			requestParams.put("client_id", "MYTMO");
			requestParams.put("client_secret", "tFKWQodJkjqcEx7VeHVP3eZrvqo8mVYzq9ed6P37P5F2PUFujn");
		} else {
			requestParams.put("client_id", "MYTMO");
			requestParams.put("client_secret", "MYTMOSecret");
		}

		RestService restService = new RestService(requestParams.toString(), jwt.get(environment));
		restService.setContentType("application/json");
		Response response = restService.callService("tms/v3/usertoken", RestCallType.POST);
		return response;
	}

	public String[] getparametersfromusertokenresponse(String code, String misdin) {
		// ArrayList<String> alist=new ArrayList<String>();
		String[] alist = new String[11];
		try {
			Response response = getusertokenrequest(code);
			if (response.body().asString() != null && response.getStatusCode() == 200) {
				JsonPath jsonPathEvaluator = response.jsonPath();

				if (jsonPathEvaluator.get("access_token") != null) {

					alist[0] = misdin;
					alist[1] = jsonPathEvaluator.get("access_token").toString();
					alist[2] = jsonPathEvaluator.get("id_tokens[0].'id_token.direct'").toString();
					alist[3] = jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin
							+ "'}[0].primaryBillingAccountCode").toString();
					if (jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin
							+ "'}[0].multiLineCustomerType") != null) {
						alist[4] = jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin
								+ "'}[0].multiLineCustomerType").toString();
					} else {
						alist[4] = jsonPathEvaluator.get("userInfo.extendedLines[0].contracts.findAll{it.msisdn=='"
								+ misdin + "'}[0].multiLineCustomerType").toString();
					}
					alist[5] = jsonPathEvaluator.get("userInfo.iamProfile.email").toString();
					alist[6] = jsonPathEvaluator.get("userInfo.iamProfile.firstname").toString();
					alist[7] = jsonPathEvaluator.get("userInfo.iamProfile.lastname").toString();
					alist[8] = jsonPathEvaluator
							.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].accountTypeSubType")
							.toString();
					if (jsonPathEvaluator
							.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].status") != null) {
						alist[9] = jsonPathEvaluator
								.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].status")
								.toString();
					} else {
						alist[9] = jsonPathEvaluator.get(
								"userInfo.extendedLines[0].contracts.findAll{it.msisdn=='" + misdin + "'}[0].status")
								.toString();
					}

					if (jsonPathEvaluator.get("userInfo.extendedLines.contracts[0].multiLineCustomerType").toString()
							.indexOf(",") < 0) {
						alist[10] = "1";
					} else {
						alist[10] = Integer.toString(
								jsonPathEvaluator.get("userInfo.extendedLines.contracts[0].multiLineCustomerType")
										.toString().split(",").length);
					}

					return alist;
				}

			}

		} catch (Exception e) {

		}

		return null;
	}

	public Response getResponseOnpermregistration(String[] alist) throws Exception {

		RestService restService = new RestService("", eep.get(environment));
		restService.setContentType("application/json");
		restService.addHeader("Authorization", alist[2]);
		restService.addHeader("apigee-proxy", "ONPREM");
		Response response = restService.callService("v1/eosoauthmanager/usertoken", RestCallType.POST);
		return response;
	}

	public String getonpermsuccess(String alist[]) {
		String status = null;

		try {

			Response response = getResponseOnpermregistration(alist);
			if (response.body().asString() != null && response.getStatusCode() == 200) {
				JsonPath jsonPathEvaluator = response.jsonPath();
				if (jsonPathEvaluator.get("status") != null) {
					return jsonPathEvaluator.get("status").toString();
				}

			} else {
				// System.out.println(response.body().asString());
			}

		} catch (Exception e) {

		}

		return status;
	}

	public Response getResponseSAASregistration(String[] alist) throws Exception {

		RestService restService = new RestService("", eep.get(environment));
		restService.setContentType("application/json");
		restService.addHeader("Authorization", alist[2]);
		restService.addHeader("apigee-proxy", "SAAS");
		Response response = restService.callService("v1/eosoauthmanager/usertoken", RestCallType.POST);
		return response;
	}

	public String getSAASsuccess(String alist[]) {
		String status = null;

		try {

			Response response = getResponseSAASregistration(alist);
			if (response.body().asString() != null && response.getStatusCode() == 200) {
				JsonPath jsonPathEvaluator = response.jsonPath();
				if (jsonPathEvaluator.get("status") != null) {
					return jsonPathEvaluator.get("status").toString();
				}

			} else {
				// System.out.println(response.body().asString());
			}

		} catch (Exception e) {

		}

		return status;
	}

	public String[] getauthorizationandregisterinapigee(String misdin, String pwd) {
		String code = getCodefromauthtokenresponse(misdin, pwd);
		if (code != null) {
			String alist[] = getparametersfromusertokenresponse(code, misdin);
			if (alist != null) {
				// return alist;
				if (getonpermsuccess(alist).equalsIgnoreCase("success")) {

					if (getSAASsuccess(alist).equalsIgnoreCase("success")) {
						return alist;
					}
				}

			}

		}

		return null;
	}

	public String getjsondataformisdin(String misdin) throws Exception {
		String getval = "false";

		// RestService restService = new RestService("",
		// "http://localhost:3000/testdata");
		RestService restService = new RestService("", "http://172.28.49.103:3000/testdata");
		Response response = restService.callService(misdin, RestCallType.GET);
		if (response.statusCode() == 200) {
			return "true";
		}
		return getval;
	}

	public String getbanfromstoreddata(String misdin) throws Exception {
		String getval = "false";

		// RestService restService = new RestService("",
		// "http://localhost:3000/testdata");
		RestService restService = new RestService("", "http://172.28.49.103:3000/testdata");
		Response response = restService.callService(misdin, RestCallType.GET);
		if (response.statusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();
			if (jsonPathEvaluator.get("ban") != null) {
				return jsonPathEvaluator.get("ban").toString();
			}

		}
		return getval;
	}

	public String getjsondataformisdinShop(String misdin) throws Exception {
		String getval = "false";

		// RestService restService = new RestService("",
		// "http://localhost:3000/testdata");
		RestService restService = new RestService("", "http://172.28.49.103:3000/qlab02/testdata");
		Response response = restService.callService(misdin, RestCallType.GET);
		if (response.statusCode() == 200) {
			return "true";
		}
		return getval;
	}

	public String putjsondataformisdin(String misdin, String payload) throws Exception {
		String getval = "false";
		// RestService restService = new RestService(payload,
		// "http://localhost:3000/testdata");
		RestService restService = new RestService(payload, "http://172.28.49.103:3000/testdata");
		restService.setContentType("application/json");
		Response response = restService.callService(misdin, RestCallType.PUT);
		if (response.statusCode() == 200) {
			return "true";
		}
		return getval;
	}

	public String putjsondataformisdinShop(String misdin, String payload) throws Exception {
		String getval = "false";
		// RestService restService = new RestService(payload,
		// "http://localhost:3000/testdata");
		RestService restService = new RestService(payload, "http://172.28.49.103:3000/qlab02/testdata");
		restService.setContentType("application/json");
		Response response = restService.callService(misdin, RestCallType.PUT);
		if (response.statusCode() == 200) {
			return "true";
		}
		return getval;
	}

	public String postjsondataformisdin(String misdin, String payload) throws Exception {
		String getval = "false";

		// RestService restService = new RestService(payload,
		// "http://localhost:3000/testdata");
		RestService restService = new RestService(payload, "http://172.28.49.103:3000/testdata");
		restService.setContentType("application/json");
		Response response = restService.callService("", RestCallType.POST);
		if (response.statusCode() == 201) {
			return "true";
		}
		return getval;
	}

	public String postjsondataformisdinShop(String misdin, String payload) throws Exception {
		String getval = "false";

		// RestService restService = new RestService(payload,
		// "http://localhost:3000/testdata");
		RestService restService = new RestService(payload, "http://172.28.49.103:3000/qlab02/testdata");
		restService.setContentType("application/json");
		Response response = restService.callService("", RestCallType.POST);
		if (response.statusCode() == 201) {
			return "true";
		}
		return getval;
	}

	public String getaccesscode() throws Exception {
		String accesscode = "false";
		RestService restService = new RestService("", "https://core.op.api.internal.t-mobile.com");
		restService.setContentType("application/json");
		restService.addHeader("authorization", ah.get("prod"));
		restService.addHeader("accept", "application/json");
		Response response = restService.callService(
				"v1/oauth2/accesstoken?grant_type=client_credentials&transactiontype=POSTPAID", RestCallType.GET);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();
			if (jsonPathEvaluator.get("access_token") != null) {
				return jsonPathEvaluator.get("access_token").toString();
			}
		}
		return accesscode;
	}

	@SuppressWarnings("rawtypes")
	public String prepareRequestParam(String requestParam, Map<Object, Object> tokenMap) {
		String replacedRequestParam = requestParam;
		if (requestParam != null) {
			if (tokenMap != null) {
				Iterator<?> it = tokenMap.entrySet().iterator();
				while (it.hasNext()) {

					Map.Entry pair = (Map.Entry) it.next();
					String key = pair.getKey().toString();
					String depVal = pair.getValue().toString();
					depVal = StringEscapeUtils.escapeJava(depVal);
					String frameworkKey = key + "_frkey";

					if (replacedRequestParam.contains(frameworkKey)) {
						replacedRequestParam = replacedRequestParam.replace(frameworkKey, depVal);

					}
				}
			}
		}
		return replacedRequestParam;
	}

}
