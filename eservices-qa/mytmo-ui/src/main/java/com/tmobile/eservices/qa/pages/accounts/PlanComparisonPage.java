package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author pshiva
 *
 */
public class PlanComparisonPage extends CommonPage {

	// Headers

	private final String pageUrl = "/plan-comparison";
	
	@FindAll({ @FindBy(css = ".ui_headline.planHeadline"),
			@FindBy(css = "div#PlanServicesDiv_id div.ui_mobile_headline") })
	private WebElement planPageHeader;

	@FindBy(css = "div.Display3")
	private WebElement accountHeader;

	@FindBy(css = "div.H6-heading")
	private List<WebElement> monthlyTotalDetails;

	// Buttons
	@FindBy(css = "div[class*='dropdown-right'] button[aria-label='Get Verified']")
	private WebElement futurePlanGetVerifiedBtn;

	@FindBy(css = "div[class*='dropdown-left'] button[aria-label='Get Verified']")
	private WebElement currentPlanGetVerifiedBtn;

	@FindBy(css = "button.PrimaryCTA")
	private List<WebElement> selectPlanCta;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement backCta;

	@FindBy(css = "button.PrimaryCTA-accent.pull-left")
	private WebElement closeModal;

	@FindBy(id = "ddLeft")
	private WebElement currentPLanDropDown;

	@FindBy(id = "ddRight")
	private WebElement futurePLanDropDown;

	@FindBy(css = "div[id='ddLeft'] a[role='menuitem']")
	private List<WebElement> currentPlanValues;

	@FindBy(css = "div#ddRight a[role='menuitem']")
	private List<WebElement> rightDropDownValuesList;

	@FindBy(xpath = "//span[contains(text(),'Choose this plan')]//following::button[contains(text(),'Select Plan')]")
	private WebElement selectCta;

	// dropdown

	@FindBy(css = "div#ddRight")
	private WebElement rightDropDown;

	@FindBy(xpath = "//button[@aria-label='Select plan']//..//..//..//..//p[@class='Display4']//span")
	private WebElement magentaSelectPlanButtonWithPrice;

	// Others
	@FindBy(css = "span[class*='body'] img")
	private WebElement planName;

	@FindBy(css = "div.col-12.Display3")
	private WebElement verifyExplore;

	@FindBy(css = "span.legal")
	private WebElement legalText;

	@FindBy(css = "span.legal-bold-link.black")
	private WebElement seeFullDetails;

	@FindBy(css = "div.Display5")
	private WebElement verifyLegalPage;

	@FindBy(css = "div.row.cursor.padding-top-small")
	private List<WebElement> clickOnFeaturePlan;

	@FindBy(css = "p.Display3.padding-top-small.text-center")
	private WebElement comparePlan;

	@FindBy(xpath = "//span[contains(text(),'More Details')]")
	private WebElement moreDetails;

	@FindBy(css = "span.Display6.padding-bottom-xsmall.cursor")
	private List<WebElement> featurePlan;

	@FindBy(xpath = "//span[@ng-model=\"selectedVal\"]")
	private List<WebElement> selectedPlan;

	@FindBy(xpath = "//span[contains(text(),'Choose this plan')]//following::button[contains(text(),'Contact Us')]")
	private WebElement contact;

	@FindBy(xpath = "//span[contains(text(),' Plan you selected is no longer available, please find available plans')]")
	private WebElement notificationforWrongPlan;

	@FindBy(css = "span[class*='Display6']")
	private List<WebElement> listOfMoreDetailsHeaders;

	public PlanComparisonPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Plan page
	 * 
	 * @return
	 */
	public PlanComparisonPage verifyPlanComparisionPage() {
		try {
			checkPageIsReady();
			waitForImageSpinnerInvisibility();
			verifyPageUrl(pageUrl);
			Reporter.log("Plan-Comparision Page is dispalyed");
		} catch (Exception e) {
			Assert.fail("Plan-Comparision Page not Loaded");
		}
		return this;
	}
	public PlanComparisonPage errorMessageforUnAvaiablePlan() {
		checkPageIsReady();
		try {
			Assert.assertTrue(notificationforWrongPlan.isDisplayed());
			Reporter.log(notificationforWrongPlan.getText() + "is Dipslayed");
		} catch (Exception e) {
			Reporter.log("Expected Error message is not Displayed ");
		}
		return this;
	}

	public PlanComparisonPage deeplinkNewPlanComparisonPageforBestPlan() {

		String currentURL[] = getDriver().getCurrentUrl().split("home");
		try {
			currentURL[0] = currentURL[0] + "change-plan/plan-comparison/#UNLMTT";
			getDriver().get(currentURL[0]);
			checkPageIsReady();
			Assert.assertTrue(currentURL[0].contains("/change-plan/explore-plans"));
			Reporter.log("Explore Plan page is loaded");

		} catch (Exception e) {
			Assert.fail("Explore Plan page is not loaded");
		}
		return this;
	}

	public PlanComparisonPage verifyContactCtabeowChoosePlan() {
		try {
			checkPageIsReady();
			Assert.assertTrue(contact.isDisplayed());
			Reporter.log("ContactUs cta is available of PlanComparison Page");
			contact.click();
			verifyContactUsPage();
		} catch (Exception e) {
			Reporter.log("ContactUs cta Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage verifythatSamePlanisPresented() {
		try {
			checkPageIsReady();
			String name1 = selectedPlan.get(1).getText();
			Reporter.log("The Selected Plan is : " + name1);
			Assert.assertTrue(clickOnFeaturePlan.get(1).getText().contains(name1));
			Reporter.log("Same Selected Plan is available on Plan Comparison page");
		} catch (Exception e) {
			Reporter.log("Same Selected Plan is available on Plan Comparison page");
		}
		return this;
	}

	public PlanComparisonPage verifyExplorePageafterRefersh() {
		try {
			checkPageIsReady();
			getDriver().navigate().refresh();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/explore-plans"));
			Reporter.log("ExplorePlans Page is loaded");
			Assert.assertTrue(verifyExplore.getText().contains("Explore plans"));
			Reporter.log("ExplorePlane Heading is available");
		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlaSectionforTalkandText() {
		try {
			checkPageIsReady();
			currentPLanDropDown.click();
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("Magenta");
			plans.add("Magenta® Plus Military");
			plans.add("T-Mobile Essentials");
			plans.add("Unlimited Talk + Text");
			plans.add("Magenta™ Plus");
			plans.add("Magenta™ Military");
			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(currentPlanValues.get(i).getText())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}
		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlaSectionforTMobileONEUnlimited55() {
		try {
			checkPageIsReady();
			Actions action = new Actions(getDriver());
			action.moveToElement(currentPLanDropDown);
			currentPLanDropDown.click();
			List<WebElement> option = currentPlanValues;
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("T-Mobile ONE Unlimited 55");
			plans.add("T-Mobile ONE with ONE Plus");
			plans.add("T-Mobile ONE");
			plans.add("T-Mobile ONE Military");
			plans.add("T-Mobile ONE w/ ONE Plus Family Unlimited 55");
			plans.add("T-Mobile Essentials");
			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(option.get(i).getText())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}
		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlaSectionforTMobileONEMilitary() {
		try {
			checkPageIsReady();
			currentPLanDropDown.click();
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("Magenta Military");
			plans.add("Magenta");
			plans.add("T-Mobile ONE Military");
			plans.add("Magenta Plus");
			plans.add("T-Mobile Essentials");
			plans.add("Magenta Plus Military");
			plans.add("Magenta (Current Plan)");

			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(
						currentPlanValues.get(i).getText().replaceAll("[^\\x00-\\x7f]", "").toString().trim())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}
		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlaSectionforTMobileEssentials() {
		try {
			checkPageIsReady();
			Actions action = new Actions(getDriver());
			action.moveToElement(currentPLanDropDown);
			currentPLanDropDown.click();
			ArrayList<String> plans = new ArrayList<>();
			plans.add("T-Mobile Essentials");
			plans.add("Magenta Unlimited 55");
			plans.add("Magenta Plus Military");
			plans.add("Magenta Military");
			plans.add("Magenta");
			boolean isExist = false;
			for (WebElement planName : currentPlanValues) {
				String planNAme = planName.getText().replaceAll("[^\\x00-\\x7f]", "").toString();
				for (String plan : plans) {
					planNAme.contains(plan);
					isExist = true;
					break;
				}
				if (!isExist) {
					Assert.fail("Plan name is not displayed: " + planNAme);
				}
				Reporter.log(planName.getText() + " Plan is Presented ");
			}
		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlaSectionforTMobileONEPlus() {
		try {
			checkPageIsReady();
			Actions action = new Actions(getDriver());
			action.moveToElement(currentPLanDropDown);
			currentPLanDropDown.click();
			List<WebElement> option = currentPlanValues;
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("T-Mobile ONE Unlimited 55+ with ONE Plus");
			plans.add("T-Mobile ONE with ONE Plus");
			plans.add("T-Mobile ONE Unlimited 55");
			plans.add("T-Mobile ONE");
			plans.add("T-Mobile Essentials");
			plans.add("T-Mobile ONE Military");
			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(option.get(i).getText())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}

		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlanSectionforTMobileONE() {
		try {
			checkPageIsReady();
			Actions action = new Actions(getDriver());
			action.moveToElement(currentPLanDropDown);
			currentPLanDropDown.click();
			List<WebElement> option = currentPlanValues;
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("Magenta™ Military");
			plans.add("T-Mobile ONE");
			plans.add("Magenta™ Plus");
			plans.add("T-Mobile Essentials");
			plans.add("Magenta® Plus Military");
			plans.add("Magenta");

			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(option.get(i).getText())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}

		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlanSectionforSCNA() {
		try {
			checkPageIsReady();
			currentPLanDropDown.click();
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("T-Mobile ONE Unlimited 55+ with ONE Plus");
			plans.add("T-Mobile ONE with ONE Plus");
			plans.add("Simple Choice North America");
			plans.add("T-Mobile ONE Unlimited 55");
			plans.add("T-Mobile ONE");
			plans.add("T-Mobile Essentials");
			plans.add("T-Mobile ONE Military");

			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(currentPlanValues.get(i).getText())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}
		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlanSectionforSCNANCC() {
		try {
			checkPageIsReady();
			currentPLanDropDown.click();
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("T-Mobile ONE with ONE Plus No Credit Check");
			plans.add("T-Mobile ONE No Credit Check");
			plans.add("T-Mobile Essentials No Credit Check");
			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(currentPlanValues.get(i).getText())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}

		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage checkEligibleplansfromCurrentPlanSection() {
		try {
			checkPageIsReady();
			Actions action = new Actions(getDriver());
			action.moveToElement(currentPLanDropDown);
			currentPLanDropDown.click();
			List<WebElement> option = currentPlanValues;
			ArrayList<String> plans = new ArrayList<String>();
			plans.add("T-Mobile ONE with ONE Plus No Credit Check");
			plans.add("T-Mobile ONE No Credit Check");
			plans.add("T-Mobile Essentials No Credit Check");
			for (int i = 0; i < plans.size(); i++) {
				if (plans.contains(option.get(i).getText())) {
					Reporter.log(plans.get(i) + " Plan is Presented ");
				} else {
					Reporter.log(plans.get(i) + " Plan is not Presented ");
				}
			}

		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public PlanComparisonPage verifySelectPlanCtabeowChoosePlan() {
		try {
			checkPageIsReady();
			Assert.assertTrue(selectCta.isDisplayed(), "Select Plan Cta is not Available below Choose Plan section");
			Reporter.log("Select Plan Cta is Available below Choose Plan section");
		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section");
		}
		return this;
	}

	public PlanComparisonPage select55PlusPlanfromCurrentPlanDropDown() {
		try {
			checkPageIsReady();

			Actions action = new Actions(getDriver());
			action.moveToElement(currentPLanDropDown);
			List<WebElement> option = currentPlanValues;
			for (int i = 0; i < option.size(); i++) {
				if (option.get(i).getText().equals("T-Mobile ONE Unlimited 55")) {
					option.get(i).click();
					break;
				}
			}
			Reporter.log("Selected the T-Mobile ONE Unlimited 55 from current plan dropdown");
		} catch (Exception e) {
			Reporter.log("T-Mobile ONE Unlimited 55 plan is not available to slect from CurrentPlan drop down ");
		}
		return this;
	}

	public PlanComparisonPage verifyFuturePlanGetVerifiedBtn() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementByText(futurePlanGetVerifiedBtn, "Get Verified"));
			Reporter.log("Get Verifiedis available on PlanComparision Page");
			futurePlanGetVerifiedBtn.click();
		} catch (Exception e) {
			Assert.fail("Get Verified is not available on PlanComparision Page");
		}
		return this;
	}

	public PlanComparisonPage verifySelectPlanCta(int index) {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementByText(selectPlanCta.get(index), "Select plan"));
			Reporter.log("SelectPlan CTA is available on PlanComparision Page");

		} catch (Exception e) {
			Reporter.log("SelectPlan CTA is not available on PlanComparision Page");
		}
		return this;
	}

	public PlanComparisonPage clickOnSelectPlanCta() {
		try {
			checkPageIsReady();
			selectPlanCta.get(0).click();
			Reporter.log("Clicked on selectPlan cta and navigated to Review Page");
		} catch (Exception e) {
			Reporter.log("selectPlan cta is not available to click in planComparionPage");
		}
		return this;
	}

	public PlanComparisonPage verifyComparePlan() {
		try {
			waitforSpinner();
			checkPageIsReady();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/plan-comparison"), "PlanCompariosn Page is loaded");
			Assert.assertTrue(verifyElementByText(comparePlan, "Compare plans"));
			Reporter.log("Compare plans page loaded");
		} catch (Exception e) {
			Reporter.log("Compare plans page is not loaded");
		}
		return this;
	}

	public PlanComparisonPage verifyBackCta() {
		try {
			checkPageIsReady();
			Assert.assertTrue(backCta.getText().contains("Back"));
			Reporter.log("backCta is available on legal page");
			backCta.click();
			Reporter.log("Clciked on backCta and navigated back to Previous page");

		} catch (Exception e) {
			Reporter.log("backCta is available on legal page");
		}
		return this;
	}

	public PlanComparisonPage verifyThatLegalPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyLegalPage.getText().contains("Important Rate Plan Information"));
			Reporter.log("Legal page is loaded successfully");
		} catch (Exception e) {
			Reporter.log("Legal page is NOT loaded successfully");
		}
		return this;
	}

	public PlanComparisonPage seeFullDetailsLinkClick() {
		try {
			checkPageIsReady();
			Assert.assertTrue(seeFullDetails.getText().contains("See full details"));
			Reporter.log("See Full Details link is available to click");
			clickElementWithJavaScript(seeFullDetails);
			Reporter.log("See Full Details link is clicked and navigated to Legal page");
		} catch (Exception e) {
			Reporter.log("See Full Details link is NOT available to click");
		}
		return this;
	}

	public PlanComparisonPage verifyLegalTextOnExplore() {
		try {
			checkPageIsReady();
			legalText.isDisplayed();
			Reporter.log("Legal text in Explore page is available");
		} catch (Exception e) {
			Reporter.log("Legal text in Explore page is NOT available");
		}
		return this;
	}

	public PlanComparisonPage verifyContactUsPage() {
		try {
			checkPageIsReady();
			checkPageIsReady();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/contact-us"));
			Reporter.log("Contact Page is loaded Successfully");
		} catch (Exception e) {
			Assert.fail("Contact Page is not loaded Successfully");
		}
		return this;

	}

	public PlanComparisonPage choosePlanFromChoosePlanSection(String planName) {
		try {
			checkPageIsReady();
			futurePLanDropDown.click();

			for (WebElement webElement : rightDropDownValuesList) {
				String value = webElement.getText().replaceAll("[^\\x00-\\x7f]", "").toString();
				if (value.equalsIgnoreCase(planName)) {
					webElement.click();
				}
			}

		} catch (Exception e) {
			Reporter.log("Select Plan Cta is not Available below Choose Plan section ");
		}
		return this;
	}

	public List<String> listOfMoreDetailsHeaders() {

		List<String> slist = new ArrayList<>();
		slist.add("No annual service contract");
		slist.add("Wi-Fi calling");
		slist.add("Taxes and fees included");
		slist.add("Netflix™");
		slist.add("HD video streaming");
		slist.add("Optimized video streaming");
		slist.add("Unlimited international texting from home");
		slist.add("Simple Global (texting and data abroad)");
		slist.add("Overseas voice calling");
		slist.add("Use your device in Mexico and Canada");
		slist.add("Gogo in-flight benefits");
		slist.add("Family Allowances");
		slist.add("T-Mobile Family Mode");
		slist.add("Voicemail to Text");
		slist.add("Name ID");
		slist.add("AutoPay discount");
		return slist;
	}

	public PlanComparisonPage clickOnChooseThisPlanSection() {
		try {
			rightDropDown.click();
		} catch (Exception e) {
			Assert.fail("Failed to click on Monthly total");
		}
		return this;
	}

	public PlanComparisonPage clickOnChooseThisPlanByName(String dropDownName) {
		try {
			clickElementBytext(rightDropDownValuesList, dropDownName);
		} catch (Exception e) {
			Assert.fail("Failed to click on Monthly total");
		}
		return this;
	}

	public PlanComparisonPage verifyPlanByPrice(String dropDownName) {
		try {
			verifyElementByText(magentaSelectPlanButtonWithPrice, dropDownName);
		} catch (Exception e) {
			Assert.fail("Failed to click on Monthly total");
		}
		return this;
	}

}