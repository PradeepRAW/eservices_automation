/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author csudheer
 *
 */
public class PromotionsPage extends CommonPage {

	ArrayList<String> accTypelist = new ArrayList<>();
	ArrayList<String> accTypelist1 = new ArrayList<>();
	ArrayList<String> accStatuslist = new ArrayList<>();

	@FindBy(css = "p[class*='Display3']")
	private WebElement myPromotionsHeader;

	@FindBy(css = "div[class*='col-12 col'] span.arrow-right")
	private List<WebElement> promotionsRightDownBtn;

	@FindBy(css = "div[id='myPromotionsPage'] i[class*='right arrow-down']")
	private WebElement promotionRightDownBtn;

	@FindBy(css = "div[id='myPromotionsPage'] i[class*='right arrow-up']")
	private WebElement promotionsRightUpBtn;

	@FindBy(css = "div[class*='content-open'] div[class='H6-heading']")
	private WebElement firstPromoEligibilityCriteriaText;

	@FindBy(css = "div[class*='content-open'] div[class='H6-heading']")
	private List<WebElement> listOfEligibilityCriteria;

	@FindAll({ @FindBy(css = "div[class*='content-open'] i[class*='check-circle']"),
			@FindBy(css = "div[class*='content-open'] i[class*='circle-one']") })
	private List<WebElement> completedORIncompleteEligibilityCriteria;

	@FindBy(css = "div[class*='content-open'] i[class*='circle-none']")
	private List<WebElement> incompleteEligibilityCriteriaList;

	@FindBy(css = "div[class*='content-open'] i")
	private List<WebElement> completedORIncompleteEligibilityCriteriaList;

	@FindBy(xpath = "//*[@id=\"promonull\"]")
	private WebElement accountHolderType;

	@FindBy(xpath = "//div[contains(@class,'content-open')]//..//p[@class='body ']")
	private WebElement promoStatus;

	@FindBy(xpath = "//div[contains(@class,'content-open')]//..//p[@class='body ']/span")
	private WebElement pendingStatusDetails;

	@FindBy(linkText = "See offer details")
	private WebElement seeOfferDetailsLink;

	@FindAll({ @FindBy(linkText = "Promo terms & conditions"), @FindBy(linkText = "Terms & Conditions") })
	private WebElement termsAndConditionsLink;

	@FindBy(css = "div[class*='content-open'] div[class='legal']")
	private WebElement legalText;

	@FindBy(css = "div[class*='content-open'] div[class*='padding-top-medium'] div[class*='H6-heading']")
	private WebElement creditYouWillGetText;

	@FindBy(css = "div[class*='content-open'] p[class='body'] span")
	private List<WebElement> promotionalSavings;

	@FindBy(css = "div[class*='content-open'] div span")
	private List<WebElement> promoCode;

	@FindBy(css = "div.legal span")
	private List<WebElement> limitedTimeOfferText;

	@FindBy(css = "div[class*='content-open'] div[class='legal']")
	private WebElement legalHeaderText;

	@FindBy(css = "span[class*='Display4']")
	private List<WebElement> tclegalHeaderText;

	@FindBy(css = "//div[text()='Print this page']")
	private WebElement printThisPage;

	@FindBy(xpath = "//div[contains(@class,'content-open')]//..//i[@class='info-circle']")
	private List<WebElement> infoIcon;

	@FindBy(css = "span[class*='Display5']")
	private WebElement promoInfoInIcon;

	@FindBy(css = "button.PrimaryCTA-accent")
	private WebElement okButton;

	@FindBy(css = "img[src*='images/icons']")
	private WebElement printThisPageLink;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement backButton;

	@FindBy(css = "div[class*='content-open'] div[class*='top-medium'] div.H6-heading")
	private List<WebElement> creditDetailsText;

	@FindBy(css = "p.body")
	private List<WebElement> creditDetailsTextsList;

	@FindBy(css = "div[class*='content-open'] div p")
	private WebElement promoCriteriaText;

	@FindBy(xpath = "/html/body/acn-root/div[2]/app-my-promotions/div/div/div[1]/div[2]/p")
	private WebElement viewDetailsPendingActiveAndPastPromotionsText;

	@FindBy(xpath = "//div[contains(@class,'content-open')]//button")
	private List<WebElement> changePlanBtn;

	@FindBy(css = "span[class*='arrow-down']")
	private WebElement selectLineArrowDown;

	@FindBy(css = "ul[class*='dropdown-menu show'] a")
	private List<WebElement> selectLineDropDownList;

	@FindBy(xpath = "//p[contains(text(),'View Past Promotions')]")
	private WebElement viewPastPromotionsLink;

	@FindBy(css = "p[class*='text-uppercase']")
	private WebElement pendingText;

	@FindBy(css = "span[class*='arrow-right']")
	private List<WebElement> listOfPromotionsRightArrow;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement goToMyPromotionsBtn;

	@FindBy(css = ("p[class*='Display3']"))
	private WebElement pastpromotionsText;

	@FindBy(xpath = "//p[contains(text(),'Showing promotions')]")
	private WebElement showingPromotionsText;

	@FindBy(xpath = "//span[contains(@class,'arrow-right')]/../../div/p")
	private List<WebElement> pastPromotionsList;

	@FindBy(css = "p[class*='padding-vertical-small']")
	private WebElement selectedPastPromotionPageHeader;

	@FindBy(css = "p[class*='padding-bottom-large']")
	private WebElement promotionStatusText;

	@FindBy(xpath = "//p[contains(text(),'There are no past promotions')]")
	private WebElement thereAreNoPastPromotionsText;

	public PromotionsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Promotions Header
	 * 
	 * @return
	 */

	public PromotionsPage verifyMyPromotionsPage(String msg) {
		checkPageIsReady();
		try {
			Assert.assertTrue(myPromotionsHeader.getText().contains(msg), "Promotions Page is not displayed");
			Reporter.log("Verified My Promotions page header");
		} catch (Exception e) {
			Assert.fail("Promotions Page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promotions page URL
	 * 
	 * @return
	 */
	public PromotionsPage verifyMyPromotionsPageURL() {
		try {
			verifyCurrentPageURL("My Promotions", "promotions");
			Reporter.log("Verified My Promotions URL");
		} catch (Exception e) {
			Assert.fail("My Promotions Page is not displayed");
		}
		return this;

	}

	/**
	 * Click On Promotions Right Down Button
	 * 
	 * @return
	 */
	public PromotionsPage clickOnFirstPromotionsRightDownBtn() {
		try {
			promotionsRightDownBtn.get(1).click();
			Reporter.log("Promotions Right CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Promotions right down CTA is not clicked");
		}
		return this;
	}

	/**
	 * Click On Promotions Right Down Button
	 * 
	 * @return
	 */
	public PromotionsPage clickOnRadndomPromotionsRightDownBtn() {
		try {
			for (int i = 0; i < promotionsRightDownBtn.size(); i++) {
				waitFor(ExpectedConditions.visibilityOfAllElements(promotionsRightDownBtn));
				promotionsRightDownBtn.get(i).click();
				if (listOfEligibilityCriteria != null && !listOfEligibilityCriteria.isEmpty()) {
					break;
				}
				clickOnFirstPromotionsRightUpBtn();
				Reporter.log("Clicked on Promotion right Down button");
			}
		} catch (Exception e) {
			Assert.fail("Not clicked on Promotion Rright Down button");
		}
		return this;
	}

	/**
	 * Click On Promotions Right Down change Plan Button
	 * 
	 * @return
	 */
	public PromotionsPage clickOnRadndomPromotionsChangePlanBtn() {
		try {
			for (int i = 0; i < promotionsRightDownBtn.size(); i++) {
				waitFor(ExpectedConditions.visibilityOfAllElements(promotionsRightDownBtn));
				promotionsRightDownBtn.get(i).click();
				if (changePlanBtn != null && !changePlanBtn.isEmpty()) {
					break;
				}
				clickOnFirstPromotionsRightUpBtn();
				Reporter.log("Clicked on Promotion right Down button");
			}
		} catch (Exception e) {
			Assert.fail("Promotion right Down button is not clicked");
		}
		return this;
	}

	/**
	 * Click On Promotions Right Down credit Details List Text
	 * 
	 * @return
	 */
	public PromotionsPage clickOnRadndomPromotionsCreditDetailsListTexts() {
		try {
			for (int i = 0; i < promotionsRightDownBtn.size(); i++) {
				waitFor(ExpectedConditions.visibilityOfAllElements(promotionsRightDownBtn));
				promotionsRightDownBtn.get(i).click();

				if (creditDetailsText != null && !creditDetailsTextsList.isEmpty()) {
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Promotion right Down button is not clicked");
		}
		return this;
	}

	/**
	 * Click On Promotions Right Up Button
	 * 
	 * @return
	 */
	public PromotionsPage clickOnFirstPromotionsRightUpBtn() {
		try {
			promotionsRightUpBtn.click();
			Reporter.log("Promotions right CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Promotions right CTA is not clicked");
		}
		return this;
	}

	/**
	 * Verify Eligibility Criteria text
	 * 
	 * @return
	 */
	public PromotionsPage verifyFirstPromoEligibilityCriteriaText(String msg) {
		try {
			Assert.assertTrue(firstPromoEligibilityCriteriaText.getText().contains(msg),
					"Eligibility Criteria is not displayed");
			Reporter.log("Expanded Promotion section and verified Eligibility Criteria");
		} catch (Exception e) {
			Assert.fail("Eligibility Criteria is not displayed");
		}
		return this;
	}

	/**
	 * Verify Eligibility Criteria List
	 * 
	 * @return
	 */
	public PromotionsPage verifyEligibilityCriteriaList() {
		try {
			Assert.assertFalse(listOfEligibilityCriteria.isEmpty(), "Eligibility Criteria list is not displayed");
		} catch (Exception e) {
			Assert.fail("Eligibility Criteria list is not displayed");
		}
		return this;
	}

	/**
	 * Verify Eligibility Criteria List
	 * 
	 * @return
	 */
	public PromotionsPage verifyCompletedORIncompleteEligibilityCriteria() {
		try {
			Assert.assertFalse(completedORIncompleteEligibilityCriteria.isEmpty(),
					"Completed Or Incomplete Eligibility Criteria is not displayed");
		} catch (Exception e) {
			Assert.fail("Completed Or Incomplete Eligibility Criteria is not displayed");
		}
		return this;
	}

	/**
	 * Verify Eligibility Criteria List
	 * 
	 * @return
	 */
	public boolean verifyChangePlanButtonRestrictedCustomer() {

		if (incompleteEligibilityCriteriaList.isEmpty()) {
			return verifyNoChangePlanBtn();
		} else {
			return verifyChangePlanBtn();
		}
	}

	/**
	 * Verify account Holder Type
	 * 
	 * @return
	 */
	public PromotionsPage verifyAccountHolderType() {
		try {
			Assert.assertTrue(accountHolderType.isDisplayed());
			Reporter.log("Account Holder Type is displayed.");
		} catch (Exception e) {
			Assert.fail("Account Holder Type is not displayed");
		}
		return this;

		/*
		 * accTypelist1.add("Primary Account Holder"); accTypelist1.add(
		 * "Restricted Access"); accTypelist1.add("Full Access");
		 * 
		 * String acType = getText(accountHolderType); boolean isEleExist = false;
		 * 
		 * if (accountHolderType.isDisplayed()) { for (String getText : accTypelist1) {
		 * isEleExist = acType.contains(getText); if (isEleExist) { break; } } } else {
		 * return isEleExist; } return isEleExist;
		 */
	}

	/**
	 * Verify promo Status and details
	 * 
	 * @return
	 */
	public PromotionsPage verifyPromoStatusAndDetails() {

		boolean isEleExist = false;

		accStatuslist.add("Expired");
		accStatuslist.add("Inactive");
		accStatuslist.add("Complete");
		accStatuslist.add("enrolled");
		accStatuslist.add("Enrolled");
		accTypelist.add("Eligible");
		accTypelist.add("In Process");
		accTypelist.add("Pending");

		String[] promoState = promoStatus.getText().split(":");
		String promoStatusDetails = pendingStatusDetails.getText();
		if (!promoState[0].isEmpty())
			for (String ele : accTypelist) {
				if (promoState[0].trim().equalsIgnoreCase(ele)) {
					Assert.assertTrue(promoStatusDetails.contains("Finish by"),
							"Promo Status And Details are not displaying ");
					Reporter.log("Promo Status And Details are displayed");
				}
			}
		else if (accStatuslist.contains(promoState)) {
			isEleExist = true;
		}
		Assert.assertTrue(isEleExist, "Promo Status And Details are not displaying ");
		Reporter.log("Promo Status And Details are displayed");
		return this;
	}

	/**
	 * Verify credit You Will Get Text
	 * 
	 * @return
	 */
	public PromotionsPage verifyCreditYouWillGetText() {
		try {
			Assert.assertTrue(creditYouWillGetText.isDisplayed());
			Reporter.log("Credit You Will Get text is displayed.");
		} catch (Exception e) {
			Assert.fail("Credit You Will Get text is not displayed.");
		}
		return this;
	}

	/**
	 * Verify Promo Code
	 * 
	 * @return
	 */
	public PromotionsPage verifyPromoCode() {
		try {
			Assert.assertTrue(verifyElementBytext(promoCode, "Promo code"), "Promo Code is not displayed");
			Reporter.log("Verified Promo Code");
		} catch (Exception e) {
			Assert.fail("Promo code is not displayed");
		}
		return this;
	}

	/**
	 * Click on See Promotions Details
	 * 
	 * @return
	 */
	public PromotionsPage clickOnSeePromotionsDetailsLink() {
		try {
			clickElementWithJavaScript(seeOfferDetailsLink);
			Reporter.log("Clicked on See Promotions Details Link");
		} catch (Exception e) {
			Assert.fail("See Promotions Details Link is not clicked");
		}
		return this;
	}

	/**
	 * Verify promotional offers url
	 * 
	 * @return
	 */
	public PromotionsPage verifyPromotionsDetailsUrl() {
		try {
			verifyCurrentPageURL("promotionaloffers", "promotionaloffers");
			Reporter.log("Promotional offers page is displayed");
		} catch (Exception e) {
			Assert.fail("Promotional offers page is not displayed");
		}
		return this;
	}

	/**
	 * Verify limited Time Offer Text
	 * 
	 * @return
	 */
	public PromotionsPage verifyLimitedTimeOfferText() {
		try {
			Assert.assertTrue(verifyElementBytext(limitedTimeOfferText, "Limited time offer and subject to change"));
			Reporter.log("Limited Time Offer Text is displayed");
		} catch (Exception e) {
			Assert.fail("Limited Time Offer Text is not displaye");
		}
		return this;
	}

	/**
	 * Click on terms And Conditions Link
	 * 
	 * @return
	 */
	public PromotionsPage clickOnTermsAndConditionsLink() {
		try {
			switchToHomeWindow();
			checkPageIsReady();
			termsAndConditionsLink.click();
			Reporter.log("Terms & Conditions link is clicked");
		} catch (Exception e) {
			Assert.fail("Terms and conditions link is not found");
		}
		return this;
	}

	/**
	 * Verify legal Text
	 * 
	 * @return
	 */
	public boolean verifyLegalTextIsEmpty() {
		return legalText.getText().isEmpty();
	}

	/**
	 * Verify Terms And Conditions text
	 */
	public PromotionsPage verifyTCLegalHeaderText() {
		try {
			checkPageIsReady();
			switchToSecondWindowInMobile();
			verifyCurrentPageURL("TermsAndConditions", "terms-and-conditions");
			Reporter.log("Verified Terms & conditions page is displayed");
		} catch (Exception e) {
			Assert.fail("Verified Terms & conditions page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promotions Details Terms And Conditions text
	 */
	public PromotionsPage verifyPromotionsDetailsTCLegalHeaderText() {
		try {
			checkPageIsReady();
			switchToSecondWindowInMobile();
			boolean isElementExsist = false;
			if (verifyPromotionsDetailsUrl() != null) {
				isElementExsist = true;
			} else {
				isElementExsist = printThisPage.isDisplayed();
			}
			Assert.assertTrue(isElementExsist, "Promotions Details page is displayed");
		} catch (Exception e) {
			Assert.fail("Promotions Details page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Promotions Details Terms And Conditions text
	 */
	public PromotionsPage closeAllOtherWindows() {
		try {
			checkPageIsReady();
			for (String handle : getDriver().getWindowHandles()) {
				getDriver().switchTo().window(handle);
				String title = getDriver().switchTo().window(handle).getTitle();
				if (!(title.contains("My T-Mobile")))
					getDriver().switchTo().window(handle).close();
			}

		} catch (Exception e) {
			Assert.fail(" page is not closed");
		}
		return this;
	}

	/**
	 * Click on info Icon
	 * 
	 * @return
	 */
	public PromotionsPage clickOnInfoIcon() {
		try {
			infoIcon.get(0).click();
			Reporter.log("Info icon is clicked");
		} catch (Exception e) {
			Assert.fail("Info icon is not clicked");
		}
		return this;
	}

	public PromotionsPage verifyPromoInfoInIcon() {
		try {
			Assert.assertTrue(promoInfoInIcon.isDisplayed());
			Reporter.log("Promotion Info Icon is displayed.");
		} catch (Exception e) {
			Assert.fail("Promotion Info Icon is not displayed.");
		}
		return this;
	}

	/**
	 * Click on Ok button
	 * 
	 * @return
	 */
	public PromotionsPage clickOkButton() {
		try {
			okButton.click();
			Reporter.log("OK CTA is clicked");
		} catch (Exception e) {
			Assert.fail("OK CTA is not clicked");
		}
		return this;
	}

	/**
	 * Verify Credit Details Text
	 * 
	 * @return
	 */
	public PromotionsPage verifyCreditDetailsText() {
		try {
			Assert.assertTrue(verifyElementBytext(creditDetailsText, "Credit details"),
					"Credit Details text is not displayed");
			Reporter.log("Credit Details is displayed");
		} catch (Exception e) {
			Assert.fail("Credit Details text is not displayed");
		}
		return this;
	}

	/**
	 * Verify promo Criteria Text
	 * 
	 * @return
	 */
	public PromotionsPage verifyPromoCriteriaText() {
		try {
			Assert.assertTrue(printThisPageLink.isDisplayed());
			Reporter.log("Print This page link is displayed.");
		} catch (Exception e) {
			Assert.fail("Print This page link is not displayed.");
		}
		return this;
	}

	/**
	 * Click on change Plan button
	 * 
	 * @return
	 */
	public PromotionsPage clickOnChangePlanBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(changePlanBtn));
			changePlanBtn.get(0).click();
		} catch (Exception e) {
			Assert.fail("Change Plan button is not Clicked");
		}
		return this;
	}

	/**
	 * Verify no change Plan button
	 * 
	 * @return
	 */
	public boolean verifyNoChangePlanBtn() {
		return changePlanBtn.isEmpty();
	}

	/**
	 * Verify change Plan button
	 * 
	 * @return
	 */
	public boolean verifyChangePlanBtn() {
		return changePlanBtn.get(0).isDisplayed();
	}

	/**
	 * Verify plans comparison
	 * 
	 */
	public PromotionsPage verifyPlansComparisonPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("modal_message_loading")));
			verifyCurrentPageURL("plans-comparison", "plans-comparison");
		} catch (Exception e) {
			Assert.fail("Verified Plans comparision Page is not displayed");
		}
		return this;
	}

	public boolean verifyCreditDetailsList() {
		boolean isElementExsist = false;
		for (int i = 0; i < creditDetailsTextsList.size(); i++) {

			if (verifyElementBytext(creditDetailsTextsList, "Total monthly credit")
					&& verifyElementBytext(creditDetailsTextsList, "Duration")
					&& verifyElementBytext(creditDetailsTextsList, "Total credit amount")
					&& verifyElementBytext(creditDetailsTextsList, "Number of credits applied"))
				isElementExsist = true;
		}
		return isElementExsist;
	}

	/**
	 * Click on select Line Arrow Down
	 * 
	 * @return
	 */
	public PromotionsPage clickSelectLineArrowDown() {
		try {
			selectLineArrowDown.click();
			Reporter.log("Select Line Arrow CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Select line arrow is not found");
		}
		return this;
	}

	public PromotionsPage selectLineDropDown() {
		try {
			selectLineDropDownList.get(1).click();
			Reporter.log("Select Line Drop down clicked");
		} catch (Exception e) {
			// Assert.fail("Select line drop down is not found");
		}
		return this;
	}

	public PromotionsPage verifyViewPastPromotionsLink() {
		try {
			Assert.assertTrue(viewPastPromotionsLink.isDisplayed());
			Reporter.log("View Past Promotions link is displayed.");
		} catch (Exception e) {
			Assert.fail("View Past Promotion link is not displayed.");
		}
		return this;
	}

	public PromotionsPage verifyPendingText(String msg) {
		try {
			Assert.assertTrue(pendingText.getText().contains(msg), "Pedning Promotions are not displayed");
		} catch (Exception e) {
			Assert.fail("Pedning Promotions are not displayed");
		}
		return this;
	}

	public PromotionsPage clickOnPromotion() {

		try {
			listOfPromotionsRightArrow.get(0).click();
			Reporter.log("Right arrow for list of promotions is clicked");
		} catch (Exception e) {
			Assert.fail("Right arrow for list of promotions is not found");
		}
		return this;
	}

	public PromotionsPage verifyGoToMyPromotionsBtn() {
		try {
			Assert.assertTrue(goToMyPromotionsBtn.isDisplayed());
			Reporter.log("Goto My Promotion button is displayed.");
		} catch (Exception e) {
			Assert.fail("Goto My Promotion button is not displayed.");
		}
		return this;
	}

	public PromotionsPage clickOnViewPastPromotionsLink() {
		try {
			viewPastPromotionsLink.click();
			Reporter.log("View Past Promotions link is clicked");
		} catch (Exception e) {
			Assert.fail("View Past Promotions link is not clicked");
		}
		return this;
	}

	public PromotionsPage verifyPastPromotionsPage(String msg) {
		try {
			checkPageIsReady();
			Thread.sleep(1000);
			Assert.assertTrue(pastpromotionsText.getText().contains(msg), "Past Promotions page is not displayed");
			Reporter.log("Past Promotions page is displayed");
		} catch (Exception e) {
			Assert.fail("Past promotions page is not displayed");
		}
		return this;
	}

	public PromotionsPage verifyShowingPromotionsText(String msg) {
		try {
			Assert.assertTrue(showingPromotionsText.getText().contains(msg),
					"Showing promotions for the past 6 months text is displayed");
		} catch (Exception e) {
			Assert.fail("Showing promotions for the past 6 months text is not displayed");
		}
		return this;
	}

	public String getPromotionName() {
		return pastPromotionsList.get(0).getText();
	}

	public PromotionsPage clickGoToMyPromotionsBtn() {
		try {
			goToMyPromotionsBtn.click();
			Reporter.log("Clicked on got to my promotions button");
		} catch (Exception e) {
			Assert.fail("Go to my promotion button is not clicked");
		}
		return this;
	}

	public PromotionsPage verifyNoPendingOrActivePromotionsText() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(promotionStatusText));
			Assert.assertTrue(promotionStatusText.getText().contains("No promotions available at this time"),
					"No promotions available at this time is not displayed");
			Reporter.log("There are no pending or active promotions. text is displayed");
		} catch (Exception e) {
			Assert.fail("No promotions available at this time is not displayed");
		}

		return this;
	}

	public PromotionsPage verifyNoPromotionsText() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(promotionStatusText));
			Assert.assertEquals(promotionStatusText.getText(), "No promotions available at this time.",
					"No promotions available at this time. text is not displayed");
			Reporter.log("No promotions available at this time. text is displayed");
		} catch (Exception e) {
			Assert.fail("No promotions available at this time. is not displayed");
		}

		return this;
	}

	public PromotionsPage verifyNoActivePromotionsText() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(promotionStatusText));
			Assert.assertEquals(promotionStatusText.getText(), "No active promotions available at this time.",
					"No active promotions available at this time. text is not displayed");
			Reporter.log("No active promotions available at this time. text is displayed");
		} catch (Exception e) {
			Assert.fail("No active promotions available at this time. is not displayed");
		}

		return this;
	}

	public PromotionsPage verifyPendingHeaderIsNotDisplayed() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(promotionStatusText));
			Assert.assertFalse(promotionStatusText.isDisplayed(), "Pending header is displayed");
			Reporter.log("Pending header is not displayed when there are no pending promotions");
		} catch (Exception e) {
			Assert.fail("Pending header is displayed");
		}

		return this;
	}
}
