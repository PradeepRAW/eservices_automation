package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class DeviceProtectionPage extends CommonPage {

	private static final String pageUrl = "deviceprotection";

	@FindBy(css = ".btn.btn-primary.PrimaryCTA")
	private WebElement continuebuttonInsurence;

	@FindBy(css = "button.btn.btn-secondary.padding-bottom-large.text-black.ng-binding")
	private WebElement insuranceDeclineButton;

	@FindBy(css = "div.modal-background")
	private WebElement insuranceNotificationModalPopWindow;

	@FindBy(css = "button[ng-click*='$ctrl.onclickOKRemovePHP()']")
	private WebElement insuranceModalPopupWindowOkButton;

	@FindBy(css = "h4[ng-bind*='vm.phpModel.phpAuthorContent.modalWindowHeader']")
	private WebElement removeInsuranceWindowHeaderMessage;

	@FindBy(css = "p[ng-bind*='vm.phpModel.phpAuthorContent.modalWindowConfirmationMessage']")
	private WebElement removeInsuranceModalWindowBodyMessage;

	@FindBy(css = "p[ng-bind*='vm.phpModel.phpAuthorContent.modalWindowContinueBtnLabel']")
	private WebElement removeInsuranceModalContinueButton;

	@FindBy(css = "div[ng-repeat*='phpSocList'] label[ng-bind*='socName']")
	private List<WebElement> socLists;
	
	@FindBy(xpath = "//label[contains(text(), 'need protection')]")
	private WebElement iDontNeedProtectionSOC;

	@FindBy(css = "div[ng-repeat*='phpSocList'] li[class*='description']")
	private List<WebElement> socDescriptionList;

	@FindBy(css = "input[checked*='checked']")
	private WebElement defaultCheckedSOC;

	@FindBy(css = "p[ng-bind*='vm.phpModel.phpAuthorContent.title']")
	private WebElement insuranceHeader;

	@FindBy(css = "p[ng-bind*='vm.phpModel.activeSoc.shortDescription']")
	private WebElement protection360SOC;

	@FindBy(xpath = "//label[contains(text(), 'Protection<360>™')]//../ul")
	private WebElement protection360SOCDescription;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement noIllTakeMyChancesButton;

	@FindBy(css = "button.gluePrimaryCTAAccent")
	private WebElement yesIamSureButton;

	@FindBy(css = "a[ng-bind*='legalTextLinkLabel']")
	private WebElement legalTextFornonNYCustomers;

	@FindBy(css = "p[ng-bind*='vm.phpModel.phpAuthorContent.previousSocTagLabel']")
	private WebElement previousSOCStatus;

	@FindBy(css = "[ng-bind*='vm.phpModel.previousSoc.socName']")
	private WebElement previousSOCName;

	@FindBy(css = "[ng-bind*='previousSoc.socCode']")
	private WebElement previousSOCPrice;

	@FindBy(css = "#pageContainer .check")
	private List<WebElement> radioButtons;

	@FindBy(css = "div.glueDialog.glueAnimationOut.glueAnimationIn")
	private WebElement declineModal;

	@FindBy(xpath = "//label[contains(text(),'need protection for my phone')]//../../div[2]/div/div/label")
	private WebElement dontNeedProtectionRadioButtonForNY;

	@FindBy(css = "#IDONT_SOC")
	private WebElement dontNeedOtherProtectionRadioButton;

	@FindBy(css = "p[ng-bind*='vm.phpModel.phpAuthorContent.recommendedSocTagLabel']")
	private WebElement bestValue;

	@FindBy(css = "label[ng-bind*='socName']")
	private List<WebElement> allSOCName;

	@FindBy(css = "p[class*='body-copy-description-regular p-b-4']")
	private List<WebElement> allSOCPrice;

	@FindBy(css = "ul.p-t-8")
	private WebElement socDescription;

	@FindBy(css = "body-copy-description-regular text-black ng-binding list-none")
	private WebElement protection360Text;

	@FindBy(css = "[ng-click*='ctrl.showPHPQuestions()']")
	private WebElement yesProtectMyPlanCTA;

	@FindBy(css = "[ng-bind-html*='$ctrl.legalFooterTextForRedesignForNY | displayHTML']")
	private WebElement legalTextNYUsers;

	@FindBy(css = "[ng-bind-html*='$ctrl.legalFooterTextForRedesign | displayHTML']")
	private WebElement legalTextNonNYUsers;

	@FindBy(css = ".legal.gray.p-t-24.ng-binding")
	private WebElement newPlanText;

	@FindBy(css = "div.col-12 p.body-copy-description-regular.text-black.ng-binding")
	private WebElement NewPlanPrice;

	@FindBy(css = ".body-copy-description-regular.gray")
	private WebElement oldPlanPrice;

	@FindBy(css = "[ng-click*='ctrl.showPHPQuestions()']")
	private WebElement protectPlanCTA;

	@FindBy(css = ".Display2.text-bold.p-t-64-sm.Display2")
	private WebElement protectionHeader;

	@FindBy(css = ".legal.gray.text-bold.p-t-24-xs")
	private WebElement previousPlanText;

	@FindBy(css = "")
	private WebElement titleDescription;

	@FindBy(css = "span[ng-click*='showMoreOptions']")
	private WebElement seeMoreOptionsLink;

	@FindBy(css = "")
	private WebElement toggleButton;

	@FindBy(css = "ineligibleProtectionMessageHeader")
	private WebElement ineligibleProtectionMessageHeader;

	@FindBy(css = "protectionServiceOptionCheckBoxUnChecked")
	private WebElement protectionServiceOptionCheckBoxUnChecked;

	@FindBy(css = "newTextVerbiage")
	private WebElement newTextVerbiage;
	
	
	public DeviceProtectionPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify change New Protection Plan
	 * 
	 * @return
	 */
	public boolean verifyDeviceProtectionURL() {
		checkPageIsReady();
		boolean isDeviceProtectionPageDisplayed = false;
		try {
			waitforSpinner();
			if (getDriver().getCurrentUrl().contains("deviceprotection")) {
				isDeviceProtectionPageDisplayed = true;
				Reporter.log("Device protection page is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Device protection page is displayed");
		}
		return isDeviceProtectionPageDisplayed;
	}

	/**
	 * Verify Insurance Migration Page
	 * 
	 * @return
	 */
	public DeviceProtectionPage verifyDeviceProtectionPage() {
		waitforSpinner();
		//verifyDuplicatedElements(this.getClass());
		try {
			verifyDeviceProtectionPageURL();
			Reporter.log("Navigated to Device Protection Page");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Device Protection Page");
		}
		return this;
	}


	/**
	 * Verify Device Protection page url
	 * 
	 * @return
	 */
	public DeviceProtectionPage verifyDeviceProtectionPageURL() {
		try {
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains(pageUrl),
					"URL for Device Protection Page is not correct");
		} catch (Exception e) {
			Assert.fail("Device protection page is not displayed");
		}
		return this;
	}

	/**
	 * Click Continue Button
	 */
	public DeviceProtectionPage clickContinueButton() {
		waitforSpinner();
		try {
			moveToElement(continuebuttonInsurence);
			clickElementWithJavaScript(continuebuttonInsurence);
			Reporter.log("Clicked on Continue Button on PHP");
		} catch (Exception e) {
			Assert.fail("Continue Button in Device Protection Page is not displayed to click");
		}
		return this;
	}


	/**
	 * Click Insurance Decline Button
	 */
	public DeviceProtectionPage clickInsuranceDeclineButton() {
		try {
			waitforSpinner();
			insuranceDeclineButton.click();
			Reporter.log("Clicked on Insurance Decline Button");

		} catch (Exception e) {
			Assert.fail("Insuracne Decline Button is not displayed to click");
		}
		return this;
	}

	/**
	 * Verify Insurance Notification Modal Pop Window
	 */
	public DeviceProtectionPage verifyInsuranceNotificationModalPopWindow() {
		try {
			insuranceNotificationModalPopWindow.isDisplayed();
			Reporter.log("Insurance notification modal pop up window is displayed");
		} catch (Exception e) {
			Assert.fail("Insurance Notification Modal popup Window is not displayed");
		}
		return this;

	}

	/**
	 * Click Insurance Modal Popup Window Ok Button
	 */
	public DeviceProtectionPage clickInsuranceModalPopupWindowOkButton() {
		try {
			insuranceModalPopupWindowOkButton.click();
			Reporter.log("Clicked on Insurance Model Pop Up OK Button");

		} catch (Exception e) {
			Assert.fail("Insuracne Notification Modal popup Window is not displayed");
		}
		return this;
	}

	/**
	 * Verify Remove Insurance Modal Window Header message
	 */
	public DeviceProtectionPage verifyRemoveInsuranceModalWindowHeader() {
		try {
			Assert.assertTrue(removeInsuranceWindowHeaderMessage.getText().equalsIgnoreCase("I will take my Chances"));
			Reporter.log("Remove Insurance modal header message is displayed");
		} catch (Exception e) {
			Assert.fail("Remove Insurance modal header message is not displayed");
		}
		return this;
	}

	/**
	 * Verify Remove Insurance Modal Window Body Message
	 */
	public DeviceProtectionPage verifyRemoveInsuranceModalWindowBodyMessage() {
		try {
			Assert.assertTrue(
					removeInsuranceModalWindowBodyMessage.getText().contains("You will not get any Protection Plans."));
			Reporter.log("Remove Insurance modal body message is displayed");

		} catch (Exception e) {
			Assert.fail("Remove Insurance modal body message is not displayed");
		}
		return this;
	}

	/**
	 * click Remove insurance model continue button
	 */
	public DeviceProtectionPage clickRemoveInsuranceModalContinueButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(removeInsuranceModalContinueButton));
			clickElementWithJavaScript(removeInsuranceModalContinueButton);
			Reporter.log("clicked Remove insurance model continue button");
		} catch (Exception e) {
			Assert.fail("Failed to click Remove insurance model continue button");
		}
		return this;
	}

	/**
	 * Verify 360 protection SOC displayed
	 */
	public DeviceProtectionPage verify360SOCName() {
		try {
			waitforSpinner();
			Assert.assertTrue(verifyElementBytext(socLists, "Protection<360>™"),
					"360 protection SOC name is not displayed");
			Reporter.log("360 protection SOC name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay 360 protection SOC");
		}
		return this;
	}

	/**
	 * verify Protection 360 SOC
	 */
	public DeviceProtectionPage verifyProtection360SOC() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(protection360SOC));
			Assert.assertTrue(protection360SOC.isDisplayed(), "360 protection SOC name is not displayed");
			Reporter.log("Protection 360 SOC name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay 360 protection SOC");
		}
		return this;
	}

	/**
	 * verify Protection 360 SOC description
	 */
	public DeviceProtectionPage verifyProtection360SOCDescription() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(protection360SOCDescription));
			Assert.assertTrue(protection360SOCDescription.isDisplayed(),
					"protection 360 SOC description is not displayed");
			Reporter.log("Protection 360 SOC description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display protection 360 SOC description");
		}
		return this;
	}

	/**
	 * verify No, I'll take my chances button
	 */
	public DeviceProtectionPage verifyNoIWillTakeChancesButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(noIllTakeMyChancesButton));
			Assert.assertTrue(noIllTakeMyChancesButton.isDisplayed(),
					"No I ll take my chances button is not displayed");
			Reporter.log("No I ll take my chances button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify No I ll take my chances button");
		}
		return this;
	}

	/**
	 * verify Yes protect my phone button
	 */
	public DeviceProtectionPage verifyYesProtectMyPhoneButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(continuebuttonInsurence));
			Assert.assertTrue(continuebuttonInsurence.isDisplayed(), "Yes protect my phone button is not displayed");
			Reporter.log("Yes protect my phone button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Yes protect my phone button");
		}
		return this;
	}

	/**
	 * click on yes protect my phone button
	 */
	public DeviceProtectionPage clickOnYesProtectMyPhoneButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(continuebuttonInsurence));
			continuebuttonInsurence.click();
			Reporter.log("Yes protect my phone button is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Yes protect my phone button");
		}
		return this;
	}

	/**
	 * Verify Soc Description Text
	 */
	public DeviceProtectionPage verifySocDescription() {
		try {
			Assert.assertTrue(socDescription.isDisplayed(), "Soc Description Text is not displayed.");
			Reporter.log("Soc Description Text is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Soc Description Text.");
		}
		return this;
	}

	/**
	 * click on No, I'll take my chances button
	 *
	 */
	public DeviceProtectionPage clickOnNoIWillTakeChancesButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(noIllTakeMyChancesButton));
			noIllTakeMyChancesButton.click();
			Reporter.log("Clicked on No I ll take my chances button");
		} catch (Exception e) {
			Assert.fail("Failed to click on No I ll take my chances button");
		}
		return this;
	}

	public DeviceProtectionPage clickYesIamSureButton() {
		checkPageIsReady();
		try {
			clickElement(yesIamSureButton);
		} catch (Exception e) {
			Assert.fail("Yes I am Sure button not displayed");
		}
		return this;
	}

	/**
	 * verify Other protection option display
	 */
	public DeviceProtectionPage verifyOtherProtectionOption() {
		try {
			waitFor(ExpectedConditions.visibilityOf(seeMoreOptionsLink));
			Assert.assertTrue(seeMoreOptionsLink.isDisplayed(), "Other protection option is not displayed");
			Reporter.log("Other protection option is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Other protection option");
		}
		return this;
	}

	/**
	 * verify legal text for non NY customers
	 */
	public DeviceProtectionPage verifyLegalTextFornonNYCustomers() {
		try {
			waitFor(ExpectedConditions.visibilityOf(legalTextFornonNYCustomers));
			Assert.assertTrue(legalTextFornonNYCustomers.isDisplayed(), "legal text for NY customers is not displayed");
			Reporter.log("legal text for NY customers is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify legal text for NY customers");
		}
		return this;
	}

	/**
	 * Verify 360 protection SOC price displayed
	 */
	public DeviceProtectionPage verify360SOCPrice() {
		try {
			Assert.assertTrue(allSOCPrice.get(0).isDisplayed(), "360 protection SOC price is not displayed");
			Assert.assertTrue(allSOCPrice.get(0).getText().contains("$"),
					"360 protection SOC price dont have '$' in it");
			Assert.assertTrue(allSOCPrice.get(0).getText().contains("/mo"),
					"360 protection SOC price dont have '/mo' in it");
			Reporter.log("360 protection SOC price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay 360 protection SOC price");
		}
		return this;
	}

	/**
	 * Verify 360 protection SOC description displayed
	 */
	public DeviceProtectionPage verify360SOCDescription() {
		try {
			Assert.assertTrue(socDescriptionList.get(0).isDisplayed(),
					"360 protection SOC description is not displayed");
			Reporter.log("360 protection SOC description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay 360 protection SOC description");
		}
		return this;
	}

	/**
	 * Verify 360 protection SOC is selected
	 */

	public DeviceProtectionPage verify360SOCIsSelected() {
		try {
			Assert.assertTrue(defaultCheckedSOC.isEnabled(), "360 protection SOC is not selected");
			Reporter.log("360 protection SOC is selected");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay 360 protection SOC selected");
		}
		return this;
	}

	/**
	 * verify header
	 */
	public DeviceProtectionPage verifyInsuranceHeader() {
		try {
			Assert.assertTrue(insuranceHeader.isDisplayed(), "Insurance header is not displayed");
			Reporter.log("Insurance Header displayed as expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Insurance header");
		}
		return this;
	}

	/**
	 * verify header for NY
	 */
	public DeviceProtectionPage verifyInsuranceHeaderForNY() {
		try {
			Assert.assertTrue(insuranceHeader.isDisplayed(), "Insurance header is not displayed");
			Assert.assertTrue(insuranceHeader.getText().contains("How do you want to protect your phone?"),
					"Insurance Header not displayed as expected");
			Reporter.log("Insurance Header displayed as expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Insurance header");
		}
		return this;
	}
	
	/**
	 * Verify Jump Protection page
	 */
	public boolean verifyJumpProtection() {
		try {
			waitforSpinner();
			Assert.assertTrue(continuebuttonInsurence.isDisplayed(),
					"Continue CTA is not displaying in Jump Protection page");
			Reporter.log("Continue CTA is displaying in Jump Protection page");
			return true;
		} catch (Exception e) {
			Reporter.log("Failed to dipslay Continue CTA in jump protection page");
		}
		return false;
	}

	/**
	 * click Jump Protection page
	 */
	public DeviceProtectionPage clickJumpProtection() {
		waitforSpinner();
		try {
			clickElementWithJavaScript(continuebuttonInsurence);
			Reporter.log("Clickable continue cta");
		} catch (Exception e) {
			Assert.fail("Failed to clickable Continue Cta");
		}
		return this;
	}

	/**
	 * Verify High level Protection title
	 */
	public DeviceProtectionPage verifyHighLevelProtectionTitle() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(insuranceHeader),
					"High level Protection title  is not displaying in  Protection page");
			Reporter.log("High level Protection title  is displaying in  Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay High level Protection title  in jump protection page");
		}
		return this;
	}

	/**
	 * Verify previous SOC Status
	 */
	public DeviceProtectionPage verifyPrevoiusSOCStatus() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(previousSOCStatus),
					"previous SOC Status  is not displaying in  Protection page");
			Reporter.log("previous SOC Status  is displaying in  Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay previous SOC Status in jump protection page");
		}
		return this;
	}

	/**
	 * Verify previous SOC Price
	 */
	public DeviceProtectionPage verifyPrevoiusSOCPrice() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(previousSOCPrice),
					"previous SOC Price is not displaying in  Protection page");
			Reporter.log("previous SOC Price is displaying in Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to display previous SOC Price in protection page");
		}
		return this;
	}

	/**
	 * Verify previous SOC Name
	 */
	public DeviceProtectionPage verifyPrevoiusSOCName() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(previousSOCName),
					"previous SOC Name  is not displaying in  Protection page");
			Reporter.log("previous SOC Name is displaying in Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to display previous SOC Name in protection page");
		}
		return this;
	}

	/**
	 * Verify I dont need protection SOC
	 */
	public DeviceProtectionPage verifyIDOntNeedProtectionSOC() {
		try {
			//Assert.assertTrue(socLists.get(2).getText().contains("need protection"),
				//	"I dont need protection SOC is not displaying in  Protection page");
			Assert.assertTrue(iDontNeedProtectionSOC.isDisplayed(), "I Don't need protection SOC not displayed");
			Reporter.log("I dont need protection SOC is displaying in Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to display I dont need protection SOC in protection page");
		}
		return this;
	}

	/**
	 * Verify Radio Buttons present on protection page
	 */
	public DeviceProtectionPage verifyRadioButtonsPresent() {
		try {
			for (WebElement webElement : radioButtons) {
				waitforSpinner();
				Assert.assertTrue(isElementDisplayed(webElement),
						"Radio Buttons is not displaying in  Protection page");
				Reporter.log("Radio Buttons is displaying in Protection page");
			}

		} catch (Exception e) {
			Assert.fail("Failed to display Radio Buttons in protection page");
		}
		return this;
	}

	/**
	 * verify legal text for NY customers
	 */
	public DeviceProtectionPage verifyDeclineModalPopUpWindow() {
		try {
			waitFor(ExpectedConditions.visibilityOf(declineModal));
			Assert.assertTrue(declineModal.isDisplayed(), "Decline Modal Pop Up window is not present");
			Reporter.log("Decline Modal Pop Up window is present");
		} catch (Exception e) {
			Assert.fail("Failed to display Decline Modal Pop Up window");
		}
		return this;
	}

	/**
	 * verify legal text for NY customers
	 */
	public DeviceProtectionPage verifyLegalTextForNYCustomers() {
		try {
			waitFor(ExpectedConditions.visibilityOf(legalTextFornonNYCustomers));
			Assert.assertTrue(legalTextFornonNYCustomers.isDisplayed(), "legal text for NY customers is not displayed");
			Reporter.log("legal text for NY customers is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify legal text for NY customers");
		}
		return this;
	}

	/**
	 * Click on don't need protection radio button
	 */
	public DeviceProtectionPage clickDontNeedProtectionRadioButton() {
		try {
			clickElementWithJavaScript(dontNeedOtherProtectionRadioButton);
			Reporter.log("Clicked on Dont Need Protection Radio Button");
		} catch (Exception e) {
			Assert.fail("Failed to click Dont Need Protection Radio Button");
		}
		return this;
	}

	/**
	 * verify dont need protection radio button for NY
	 */
	public DeviceProtectionPage verifyDontNeedProtectionRadioButtonForNY() {
		try {
			Assert.assertTrue(dontNeedProtectionRadioButtonForNY.isDisplayed(),
					"I Dont need protection radio button not displayed");
			Reporter.log("I Dont need protection radio button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify I Dont need protection radio button");
		}
		return this;
	}

	/**
	 * Click on dont need protection radio button for NY
	 */
	public DeviceProtectionPage clickDontNeedProtectionRadioButtonForNY() {
		try {
			clickElementWithJavaScript(dontNeedProtectionRadioButtonForNY);
			Reporter.log("DontNeedProtectionRadioButton is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click DontNeedProtectionRadioButton");
		}
		return this;
	}

	/**
	 * click Other protection option display
	 */
	public DeviceProtectionPage clickOtherProtectionOption() {
		try {
		
			clickElementWithJavaScript(seeMoreOptionsLink);
			Reporter.log("Other protection option is clicked");			
		} catch (Exception e) {
			Assert.fail("Failed to click Other protection option");
		}
		return this;
	}

	/**
	 * verify legal text for best value
	 */
	public DeviceProtectionPage verifyLegalTextForBestValue() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(bestValue));
			Assert.assertTrue(bestValue.isDisplayed(), "legal text for non NY customers best value is not displayed");
			Reporter.log("legal text for NON NY customers best value is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify legal textfor NON NY customers best valu");
		}
		return this;
	}

	/**
	 * verify basic soc name on other protection page
	 */
	public DeviceProtectionPage verifyProtectionBasicSOCName() {
		try {
			waitforSpinner();
			Assert.assertTrue(socLists.get(1).isDisplayed(),
					"basic soc name on other protection page  is not displayed");
			Reporter.log("basic soc name on other protection page  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay basic soc name on other protection page ");
		}
		return this;
	}

	/**
	 * verify basic soc price on other protection page
	 */
	public DeviceProtectionPage verifyProtectionBasicSOCPrice() {
		try {

			waitFor(ExpectedConditions.visibilityOf(allSOCPrice.get(1)));
			Assert.assertTrue(allSOCPrice.get(1).isDisplayed(),
					"basic soc price on other protection page is not displayed");
			Reporter.log("basic soc price on other protection page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay basic soc price on other protection page");
		}
		return this;
	}

	/**
	 * verify basic soc description on other protection page
	 */
	public DeviceProtectionPage verifyProtectionBasicSOCDescription() {
		try {
			waitFor(ExpectedConditions.visibilityOf(socDescriptionList.get(1)));
			Assert.assertTrue(socDescriptionList.get(1).isDisplayed(),
					"basic soc description on other protection page  is not displayed");
			Reporter.log("basic soc description on other protection pag");
		} catch (Exception e) {
			Assert.fail("Failed to display basic soc description on other protection pag");
		}
		return this;
	}

	/**
	 * Click on dont need protection radio button for other protection page
	 */

	public DeviceProtectionPage clickIDontNeedProtectionRadioButton() {
		try {
			clickElementWithJavaScript(dontNeedOtherProtectionRadioButton);
			Reporter.log("DontNeedProtectionRadioButton is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click DontNeedProtectionRadioButton");
		}
		return this;
	}
	
	
	/**
	 * verify on dont need protection radio button for other protection page
	 */

	public DeviceProtectionPage verifyIDontNeedProtectionRadioButton() {
		try {
			Assert.assertTrue(dontNeedOtherProtectionRadioButton.isDisplayed(), "Radio Button not displayed");
			Reporter.log("DontNeedProtectionRadioButton is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify DontNeedProtectionRadioButton");
		}
		return this;
	}

	/**
	 * verify All all NY SOCs are present
	 */
	public DeviceProtectionPage verifyAllNYSOCPresent() {
		try {
			Assert.assertTrue(socLists.get(1).getText().contains("Service Contract Device Protection Plan"));
			Assert.assertTrue(socLists.get(2).getText().contains("Insurance Device Protection Plan"));
			Assert.assertTrue(socLists.get(3).getText().contains(
					"Insurance Device Protection Plan (Service Contract purchased separately from another provider)"));
			Reporter.log("All NY Socks have been displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display all NY SOCs");
		}
		return this;
	}

	/**
	 * verify Name of Extended Warranty SOC
	 */
	public DeviceProtectionPage verifyExtendedWarrantySOCPresent() {
		try {
			Assert.assertTrue(verifyElementBytext(socLists, "Service Contract Device Protection Plan"),
					"Extended Warranty SOC name is not displayed");
			Reporter.log("Extended Warranty SOC Name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Extended Warranty SOC Name");
		}
		return this;
	}

	/**
	 * verify description of Extended Warranty SOC
	 */
	public DeviceProtectionPage verifyExtendedWarrantySOCDescriptionPresent() {
		try {
			Assert.assertTrue(allSOCName.get(1).isDisplayed());
			Reporter.log("Extended Warranty SOC Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Extended Warranty SOC Description");
		}
		return this;
	}

	/**
	 * verify name of Insurance Only SOC
	 */
	public DeviceProtectionPage verifyInsuranceOnlySOCPresent() {
		try {
			Assert.assertTrue(verifyElementBytext(socLists, "Insurance Device Protection Plan"),
					"Insurance Only SOC name is not displayed");
			Reporter.log("Insurance Only SOC Name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Insurance Only SOC Name");
		}
		return this;
	}

	/**
	 * verify description of Insurance Only SOC
	 */
	public DeviceProtectionPage verifyInsuranceOnlySOCDescriptionPresent() {
		try {
			Assert.assertTrue(allSOCName.get(2).isDisplayed());
			Reporter.log("Insurance Only SOC Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Insurance Only SOC Description");
		}
		return this;
	}

	/**
	 * verify Description of Extended Warranty and Insurance SOC
	 */
	public DeviceProtectionPage verifyExtendedWarrantyAndInsuranceSOCDescriptionPresent() {
		try {
			Assert.assertTrue(allSOCName.get(3).isDisplayed());
			Reporter.log("Extended Warranty and Insurance SOC Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Extended Warranty and Insurance SOC Description");
		}
		return this;
	}

	/**
	 * verify Price of Extended Warranty SOC
	 */
	public DeviceProtectionPage verifyExtendedWarrantySOCPricePresent() {
		try {
			Assert.assertTrue(allSOCPrice.get(1).isDisplayed(), "Price is not displayed");
			String arr[] = allSOCPrice.get(1).getText().split("\\.");
			String arr1[] = arr[1].split("/");
			int decimalValue = arr1[0].length();
			if (decimalValue == 2) {
				Reporter.log("Extended Warranty SOC Price is displayed with 2 decimal");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Extended Warranty SOC Price");
		}
		return this;
	}

	/**
	 * verify Price of Insurance Only SOC
	 */
	public DeviceProtectionPage verifyInsuranceOnlySOCPricePresent() {
		try {
			Assert.assertTrue(allSOCPrice.get(2).isDisplayed());
			String arr[] = allSOCPrice.get(2).getText().split("\\.");
			String arr1[] = arr[1].split("/");
			int decimalValue = arr1[1].length();
			if (decimalValue == 2) {
				Reporter.log("Insurance Only SOC Price is displayed with 2 decimal");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Insurance Only SOC Price");
		}
		return this;
	}

	/**
	 * verify Price of Extended Warranty and Insurance SOC
	 */
	public DeviceProtectionPage verifyExtendedWarrantyAndInsuranceSOCPricePresent() {
		try {
			Assert.assertTrue(allSOCPrice.get(3).isDisplayed());
			String arr[] = allSOCPrice.get(3).getText().split("\\.");
			String arr1[] = arr[1].split("/");
			int decimalValue = arr1[1].length();
			if (decimalValue == 2) {
				Reporter.log("Extended Warranty and Insurance SOC Price is displayed with 2 decimal");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Extended Warranty and Insurance SOC Price");
		}
		return this;
	}

	/**
	 * Verify Protection 360SOC Name
	 */
	public DeviceProtectionPage verifyProtection360SOCName() {
		try {
			waitforSpinner();
			boolean protection360SocSocName = false;
			for (WebElement webElement : socLists) {
				if (webElement.getText().equalsIgnoreCase("Protection<360>™")) {
					protection360SocSocName = true;
					break;
				}
			}
			Assert.assertTrue(protection360SocSocName, "Protection 360 Soc Name is not displayed");
			Reporter.log("Protection 360 Soc Name is displayed");
		} catch (Exception e) {
			Assert.fail("Protection 360 Soc Name is not displayed");
		}
		return this;
	}

	/**
	 * Verify Protection 360 Soc Price
	 */
	public DeviceProtectionPage verifyProtection360SOCPrice() {
		try {
			Assert.assertTrue(allSOCPrice.get(0).isDisplayed());
			Reporter.log("Protection 360 Soc price is displayed");
		} catch (Exception e) {
			Assert.fail("Protection 360 Soc price is not displayed");
		}
		return this;
	}

	/**
	 * verify Previous And Current SOC Are Of Same Category
	 */
	public DeviceProtectionPage verifyPreviousAndCurrentSOCAreOfSameCategory() {
		try {
			String previousSOC = previousSOCName.getText();
			String currentSOCName = socLists.get(0).getText();
			Assert.assertTrue(currentSOCName.equalsIgnoreCase(previousSOC), "Previous and current SOCs are not same");
			Reporter.log("customer migrated to the same version/category of SOC ");
		} catch (Exception e) {
			Assert.fail("customer not migrated to the same version/category of SOC");
		}
		return this;
	}

	/**
	 * Verify current SOC is selected by default
	 */

	public DeviceProtectionPage verifyCurrentSOCIsSelected() {
		try {
			Assert.assertTrue(socLists.get(0).isEnabled(), "Current SOC is not selected");
			Reporter.log("Current protection SOC is selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify the selection of Current protection SOC");
		}
		return this;
	}

	/**
	 * verify Previous And Current SOC Are Of Different Category
	 */
	public DeviceProtectionPage verifyPreviousAndCurrentSOCAreOfDifferentCategory() {
		try {
			String previousSOC = previousSOCName.getText();
			String currentSOCName = socLists.get(0).getText();
			Assert.assertFalse(currentSOCName.equalsIgnoreCase(previousSOC), "Previous and current SOCs are same");
			Reporter.log("Customer migrated to the different version/category of SOC ");
		} catch (Exception e) {
			Assert.fail("Customer failed to migrate to the different version/category of SOC");
		}
		return this;
	}

	/**
	 * Verify header 'Your new phone requires higher level of protection.
	 */
	public DeviceProtectionPage verifyProtectionHeader() {
		try {
			Assert.assertTrue(protectionHeader.isDisplayed(),
					"'Your new phone requires higher level of protection. Let's keep it protected.' is not displayed");
			Reporter.log("D'Your new phone requires higher level of protection. Let's keep it protected.'is displayed");
		} catch (Exception e) {
			Assert.fail(
					"Failed to dipslay 'Your new phone requires higher level of protection. Let's keep it protected.");
		}
		return this;
	}

	/**
	 * Verify previous Plan Text
	 */
	public DeviceProtectionPage verifyPreviousPlanText() {
		try {
			Assert.assertTrue(previousPlanText.isDisplayed(), "Previous Plan Text is not displayed");
			Reporter.log("Previous Plan Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Previous Plan Text");
		}
		return this;
	}

	/**
	 * Verify New Plan Text
	 */
	public DeviceProtectionPage verifyNewPlanText() {
		try {
			Assert.assertTrue(newPlanText.isDisplayed(), "New Plan Text is not displayed");
			Reporter.log("New Plan Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay New Plan Text");
		}
		return this;
	}

	/**
	 * Verify Protection 360 text
	 */
	public DeviceProtectionPage verifyProtection360Text() {
		try {
			Assert.assertTrue(protection360Text.isDisplayed(), "Protection 360 text is not displayed");
			Reporter.log("Protection 360 text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Protection 360 text.");
		}
		return this;
	}

	/**
	 * Verify legal Text NY Users
	 */
	public DeviceProtectionPage verifyLegalTextForNYUsers() {
		try {
			waitFor(ExpectedConditions.visibilityOf(legalTextNYUsers));
			Assert.assertTrue(legalTextNYUsers.isDisplayed(), "Legal Text For NY Users is not displayed");
			Reporter.log("Legal Text For NY Users is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Legal Text For NY Users.");
		}
		return this;
	}

	/**
	 * Verify legal Text Non NY Users
	 */
	public DeviceProtectionPage verifyLegalTextForNonNYUsers() {
		try {
			waitFor(ExpectedConditions.visibilityOf(legalTextNonNYUsers));
			Assert.assertTrue(legalTextNonNYUsers.isDisplayed(), "Legal Text For Non NY Users is not displayed");
			Reporter.log("Legal Text For Non NY Users is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Legal Text For Non NY Users.");
		}
		return this;
	}

	/**
	 * Verify New Plan Price
	 */
	public DeviceProtectionPage verifyNewPlanPrice() {
		try {
			Assert.assertTrue(NewPlanPrice.isDisplayed(), "New Plan Price is not displayed");
			Reporter.log("New Plan Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay New Plan Price.");
		}
		return this;
	}

	/**
	 * Verify Old Plan Price
	 */
	public DeviceProtectionPage verifyOldPlanPrice() {
		try {
			Assert.assertTrue(oldPlanPrice.isDisplayed(), "Old Plan Price is not displayed");
			Reporter.log("Old Plan Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Old Plan Price.");
		}
		return this;
	}

	/**
	 * Click protect plan cta
	 */

	public DeviceProtectionPage clickProtectPlanCTA() {
		try {
			waitforSpinner();
			protectPlanCTA.click();
			Reporter.log("Protect Plan CTA is clicked.");
		} catch (Exception e) {
			Assert.fail("Failed to click protect plan cta");
		}
		return this;
	}

	/**
	 * Click on 'Yes, protect my plan' CTA
	 */
	public DeviceProtectionPage clickYesProtectMyPlanCTA() {
		try {
			clickElementWithJavaScript(yesProtectMyPlanCTA);
			Reporter.log("Clicked on 'Yes, protect my plan' CTA.");

		} catch (Exception e) {
			Assert.fail("Failed to click on 'Yes, protect my plan' CTA.");
		}
		return this;
	}

	/**
	 * verify title Description
	 */
	public DeviceProtectionPage verifyTitleDescription() {
		try {
			Assert.assertTrue(titleDescription.isDisplayed());
			Reporter.log("Title Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display title Description");
		}
		return this;
	}

	/**
	 * Click on see more options
	 */
	public DeviceProtectionPage clickOnSeeMoreOptions() {
		try {
			seeMoreOptionsLink.click();
			Reporter.log("Clicked on see More Options Link");
		} catch (Exception e) {
			Assert.fail("Failed to click on see More Options Link");
		}
		return this;
	}

	/**
	 * Verifies Sorting order
	 */
	public DeviceProtectionPage verifySOCOrder() {
		try {
			Assert.assertTrue(allSOCName.get(0).isDisplayed());
			Assert.assertTrue(allSOCName.get(1).isDisplayed());
			Assert.assertTrue(allSOCName.get(2).isDisplayed());
			Assert.assertTrue(allSOCName.get(0).getText().trim().contains("protection"),"SOc is displayed");
			Assert.assertTrue(allSOCName.get(1).getText().trim().contains("protection"),"SOc is displayed");
			Assert.assertTrue(allSOCName.get(2).getText().trim().contains("protection"),"SOc is displayed");
			Reporter.log("Order of SOC: " +allSOCName.get(0)+ ", " +allSOCName.get(1)+ ", " +allSOCName.get(0));
			Reporter.log("Verified SOC Order");

		} catch (Exception e) {
			Assert.fail("Highest Price order is not displayed");
		}
		return this;
	}

	/**
	 * Click and verifies toggle button
	 */
	public DeviceProtectionPage clickAndVerifyToggleButton() {
		checkPageIsReady();
		try {
			toggleButton.click();
			Assert.assertTrue(isDisplayed(), "");
			Reporter.log("Verified toggle button");

		} catch (Exception e) {
			Assert.fail("Toggle button isn't working correctly");
		}
		return this;

	}

	/**
	 * Verifies default SOC selection
	 */
	public void verifyDefaultSOCSelection() {
		try {
			Assert.assertTrue(isDisplayed(), "");
			Reporter.log("Verified default SOC selection");

		} catch (Exception e) {
			Assert.fail("Default SOC selection isn't shown correctly");
		}
	}

	/**
	 * verify Previous And Current SOC Are Of Same Category
	 */
	public DeviceProtectionPage selectNewSOCdifferentFromPrevious() {
		try {
			String previousSOC = previousSOCName.getText();
			for (WebElement soc : socLists) {
				String currentSOCName = soc.getText();
				if (!currentSOCName.contains(previousSOC)) {
					clickElementWithJavaScript(soc);
					break;
				}
			}
			Reporter.log("Selected new insurance SOC different from previous");
		} catch (Exception e) {
			Assert.fail("Failed to select SOC");
		}
		return this;
	}

	/**
	 * Verifies Ineligible Message Header
	 */
	public DeviceProtectionPage verifyIneligiblProtectionMessageHeader() {
		try {
			Assert.assertTrue(ineligibleProtectionMessageHeader.isDisplayed());
			Reporter.log("Ineligible Protection Message Header is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display Ineligible protection Message Header");
		}
		return this;
	}

	/**
	 * Verifies Protection Service Option Check Box UnChecked
	 */
	public DeviceProtectionPage verifyProtectionServiceOptionCheckBoxUnChecked() {
		try {
			Assert.assertFalse(protectionServiceOptionCheckBoxUnChecked.isSelected());
			Reporter.log("Protection Service Option Check Box is UnChecked");

		} catch (Exception e) {
			Assert.fail("Failed to Uncheck Protection Service Option CheckBox");
		}
		return this;
	}

	/**
	 * Verifies NewTextVerbiage
	 */
	public DeviceProtectionPage verifyNewTextVerbiage() {
		try {
			Assert.assertFalse(newTextVerbiage.isDisplayed());
			Reporter.log("New Text Verbiage is not displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display New verbiage text");
		}
		return this;
	}
	
	
	
	/**
	 * Get Name for default selected SOC
	 */
	public String getNameforDefaultSelectedSOC() {
		String name = null;
				try {
			Assert.assertTrue(socLists.get(0).isDisplayed(), "Default Selected SOC name is not displayed");
			name = socLists.get(0).getText();
			Reporter.log("New Plan Name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay New Plan Price.");
		}
		return name;
	}
	
	
	/**
	 * Get Name for default selected SOC
	 */
	public String getPriceforDefaultSelectedSOC() {
		String price = null;
		String price1[] = new String[0];
		try {
			Assert.assertTrue(allSOCPrice.get(0).isDisplayed(), "Default Selected SOC price is not displayed");
			price = allSOCPrice.get(0).getText().replace(" ", "");
			price1 = price.split("/");
			Reporter.log("New Plan Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay New Plan Price.");
		}
		return price1[0];
	}

	/**
	 * verify All all Tier6Soc Non NY present
	 */
	public DeviceProtectionPage verifyTier6SOCNonNYPresent() {
		try {
			Assert.assertTrue(socLists.get(0).getText().contains("Protection<360>™"));
			Assert.assertTrue(socLists.get(1).getText().contains("Device Protection"));
			Reporter.log("All Tier6Soc Non NY have been displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display all Tier6Soc Non NY");
		}
		return this;
	}
	
	/**
	 * select Tier6 Soc Non NY
	 */
	public DeviceProtectionPage selectTier6SocNonNY() {
		try {
			for (WebElement soc : socLists) {
				String currentSOCName = soc.getText();
				if (currentSOCName.contains("Protection")) {
					clickElementWithJavaScript(soc);
					break;
				}
			}
			Reporter.log("Selected Tier 6 Soc For Non NY");
		} catch (Exception e) {
			Assert.fail("Failed to select Tier 6 Soc For Non NY");
		}
		return this;
	}
	
	/**
	 * select Tier6 Soc NY
	 */
	public DeviceProtectionPage selectTier6SocNY() {
		try {
			for (WebElement soc : socLists) {
				String currentSOCName = soc.getText();
				if (currentSOCName.contains("Service Contract")) {
					clickElementWithJavaScript(soc);
					break;
				}
			}
			Reporter.log("Selected Tier 6 Soc For NY");
		} catch (Exception e) {
			Assert.fail("Failed to select Tier 6 Soc For NY");
		}
		return this;
	}
	
	
	
	/**
	 * Get previous SOC Name
	 */
	public String getPrevoiusSOCName() {
		String name = null;
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(previousSOCName),
					"previous SOC Name  is not displaying in  Protection page");
			name = previousSOCName.getText();
			Reporter.log("previous SOC Name is displaying in Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to Get previous SOC Name in protection page");
		}
		return name;
	}
	
	
	/**
	 * Get previous SOC Price
	 */
	public String getPrevoiusSOCPrice() {
		String price = null;
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(previousSOCPrice),
					"previous SOC Price is not displaying in  Protection page");
			price = previousSOCPrice.getText();
			Reporter.log("previous SOC Price is displaying in Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to display previous SOC Price in protection page");
		}
		return price;
	}
	
	
	
	/**
	 * Verify previous SOC Name is Tier 6 SOC
	 */
	public String verifyPrevoiusSOCNameTier6() {
		String name = null;
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(previousSOCName),
					"previous SOC Name  is not displaying in  Protection page");
			Assert.assertTrue(previousSOCName.getText().contains("Device Protection"),"Tier 6 SOC is not displayed in previous");
			Reporter.log("previous SOC Name is Tier 6 displaying in Protection page");
		} catch (Exception e) {
			Assert.fail("Failed to verify previous SOC Name in protection page");
		}
		return name;
	}
	
	/**
	 * select Tier6 Soc NY
	 */
	public DeviceProtectionPage selectTier6SocNYBasic() {
		try {
			for (WebElement soc : socLists) {
				String currentSOCName = soc.getText();
				if (currentSOCName.contains("Insurance Device Protection Plan (Service Contract purchased separately from another provider)")) {
					clickElementWithJavaScript(soc);
					break;
				}
			}
			Reporter.log("Selected Tier 6 Soc For NY");
		} catch (Exception e) {
			Assert.fail("Failed to select Tier 6 Soc For NY");
		}
		return this;
	}
	
	/**
	 * select Tier6 Soc Non NY
	 */
	public DeviceProtectionPage selectTier6SocNonNYBasic() {
		try {
			for (WebElement soc : socLists) {
				String currentSOCName = soc.getText();
				if (currentSOCName.contains("Protection<360>™")) {
					clickElementWithJavaScript(soc);
					break;
				}
			}
			Reporter.log("Selected Tier 6 Soc For Non NY");
		} catch (Exception e) {
			Assert.fail("Failed to select Tier 6 Soc For Non NY");
		}
		return this;
	}
	
}
