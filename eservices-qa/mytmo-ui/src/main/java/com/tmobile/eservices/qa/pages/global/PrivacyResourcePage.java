package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class PrivacyResourcePage extends CommonPage {

	public static final String privacyResourcePageUrl = "/responsibility/privacy";
	public static final String privacyResourcePageLoadText = "Privacy & Security Resources";


	@FindBy(xpath = "//*[contains(text(),'Privacy Center')]")
	private WebElement privacyResourcePageHeader;

	/**
	 * 
	 * @param webDriver
	 */
	public PrivacyResourcePage(WebDriver webDriver) {
		super(webDriver);
	}
	
	

	/**
	 * Verify Privacy Policy Page
	 * 
	 * @return
	 */
	public PrivacyResourcePage verifyPrivacyResourcePage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(privacyResourcePageUrl);
			Reporter.log("Privacy Resource page is displayed");
		} catch (Exception e) {
			Assert.fail("Privacy Resource page not displayed");
		}
		return this;
	}
}