/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 * 
 */
public class AccessoriesStandAloneReviewCartPage extends CommonPage {

	@FindBy(id = "checkoutCTAURL")
	private WebElement checkoutBtn;

	@FindBy(id = "restrictedUser")
	private WebElement checkOutBtnMsg;
	
	@FindBy(css = "div.xs-style input.btn.btn-secondary")
	private WebElement continueShoppingButton;
	
	@FindBy(css = "div.ui_mobile_secondary_link")
	private WebElement continueShoppingButtonMobile;

	@FindBy(css = "#eipTermsFlagTrue")
	private WebElement taxesTerm;
	
	@FindBy(css = ".finalCart #upfrontPaymentAmnt")
	private List<WebElement> eipUpfrontUpfrontText;
	
	@FindBy(css =".finalCart #upfront-monthly-pay-amt")
	private List<WebElement> eipDueMonthlyPrice;
	
	@FindBy(css = ".finalCart span.unitPrice")
	private List<WebElement> eipDueTodayPrice;
	
	@FindBy(css ="th#details_coloumn")
	private WebElement detailsLabel;
	
	@FindBy(css ="th#duemonthly_coloumn")
	private WebElement dueMonthlyLabel;
	
	@FindBy(css ="tr > th:nth-child(4)")
	private WebElement dueTodayLabel;
	
	@FindBy(css ="span#dueGrandTotToday")
	private WebElement dueTodayTotalAmt;
	
	@FindBy(css ="#dueTotMonthly")
	private WebElement dueMonthlyTotalAmt;
	
	@FindBy(css ="#rd2")
	private WebElement onePaymentRadioBtn;
	
	@FindBy(css ="#rd1")
	private WebElement monthlyPaymentRadioBtn;
		
	@FindBy(xpath="//div[contains(text(),'Review Cart')]")
	private WebElement reviewCartText;
	
	@FindBy(css ="#hideForFRP")
	private WebElement legalText;
	
	@FindBy(css =".container-curve")
	private WebElement table;
	
	@FindBy(css ="#shippingCost")
	private WebElement shippingAmt;
		
	@FindBy(css = "div a.removeCartCTA")
	private WebElement removeAccessoryDeviceFromCart;
	
	@FindBy(css = "#totalFinancedAmountValue")
	private WebElement totalFinancedAmountValue;
	
	@FindBy(css = "#totalFinancedAmountLabel")
	private WebElement totalFinancedAmountLabel;
	
	@FindBy(css = ".finalCart div.heading")
	private List<WebElement> accessoryNameInCart;

	@FindBy(id = "pastDueStickyBanner")
	private WebElement pastDueStickyBanner;

	@FindBy(id = "pastDueStickyTitle")
	private WebElement pastDueMessageTitle;

	@FindBy(id = "pastDueStickyBody")
	private WebElement pastDueMessageBody;

	@FindBy(css = ".close.no-background")
	private WebElement popupCloseIcon;

	@FindBy(css = "p[class*='modal-title no-margin']")
	private WebElement popUpModalHeader;

	@FindBy(css = "p[class*='padding-top-medium']")
	private WebElement popUpModalMessage;

	@FindBy(css = "[onclick*='makePayment']")
	private WebElement popUpModalMakePaymentCTA;


	/**
	 * @param webDriver
	 */
	public AccessoriesStandAloneReviewCartPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Accessories Stand Alone Review Cart Page
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyAccessoriesStandAloneReviewCartPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("Accessories stand alone review cart page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Accessories stand alone review cart page not displayed");
		}
		return this;
	}
	
	 /**
     * Verify that current page URL matches the expected URL.
     */
    public AccessoriesStandAloneReviewCartPage verifyPageUrl() {
        try{
    		getDriver().getCurrentUrl().contains("cart");
    		Reporter.log("Landed on Review Cart page");
		}catch (Exception e){
    		Assert.fail("Unable to navigate to Review cart page");
		}
        return this;
    }

	/**
	 * verify Check Out Button State
	 */
	public AccessoriesStandAloneReviewCartPage verifyCheckOutButtonState() {
		getDriver().navigate().refresh();
		checkPageIsReady();	
		System.out.println("ddd"+checkoutBtn.isEnabled());
		if (!checkoutBtn.isEnabled()) {
			//Assert.assertTrue(condition);
			Reporter.log("Check out button is in disabled state");
		} else {
			Assert.fail("Check out button is not in disabled state");
		}
		return this;
	}

	/**
	 * Click CheckOut Button
	 */
	public AccessoriesStandAloneReviewCartPage VerifyCheckOutButtonMessage() {
		checkPageIsReady();	
		if (checkOutBtnMsg.getText().contains("Please contact your primary account holder to purchase an accessory")) {
			Reporter.log("Text message for disabled cta displayed");
		} else {
			Assert.fail("Text message for disabled cta is not displayed");
		}
		return this;
	}
	
	
	
	/**
	 * Click continue to shopping
	 */
	public AccessoriesStandAloneReviewCartPage clickContinueToShoppingButton() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.elementToBeClickable(continueShoppingButton));
			continueShoppingButton.click();
			Reporter.log("Continue To Shopping Button is clicked");
		} catch (Exception e) {
			Assert.fail("Continue To Shopping Button is not clickable");
		}
		return this;
	}
	
	

	
	/**
	 * Verify that Taxes Term contains + Taxes
	 */

	public AccessoriesStandAloneReviewCartPage verifyTaxesTerm() {
		try {
			waitforSpinner();
			Assert.assertTrue(taxesTerm.getText().contains("+ taxes"),"Taxes term does not contain + taxes text");
			Reporter.log("Taxes term contains + taxes");
		} catch (Exception e) {
			Assert.fail("Failed to display '+ Taxes' in EIP taxes terms");
		}
		return this;
	}
	
	
	/**
	 * Verify that EIP Upfront Payment price,Dollar Symbol, Month and Length at
	 * Accessory Review Cart page
	 */
	public AccessoriesStandAloneReviewCartPage verifyEipUpfrontPaymentPriceAtAccessoryCartPage() {
		try {
			waitforSpinner();
			for (WebElement eipUpfrontParaPrice : eipUpfrontUpfrontText) {
				Assert.assertTrue(eipUpfrontParaPrice.isDisplayed(), "EIP Upfront Payment price is not displaying.");
				Assert.assertTrue(eipUpfrontParaPrice.getText().contains("$"),
						"$ is not displayed for all Accessories EIP pricing.");
			}
			Reporter.log("EIP Upfront Payment price is present and contain $ for Accessories.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Upfront Price at Accessory Review cart page.");
		}
		return this;
	}
	
	
	/**
	 * Verify EIP Due Monthly price and Dollar Symbol at Accessory Review Cart page
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyEipDueMonthlyForAccesories() {
		try {
			waitforSpinner();
			for (WebElement DueMonthlyPrice : eipDueMonthlyPrice) {
				Assert.assertTrue(DueMonthlyPrice.isDisplayed(),
						"EIP Due Monthly is not displayed for Accessories pricing.");
				Assert.assertTrue(DueMonthlyPrice.getText().contains("$"),
						"$ is not displayed for all Accessories EIP Due Monthly pricing.");
			}
			Reporter.log("EIP Due Monthly Price is present and contains $ for all Accessories.");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Due Monthly price and $ for all Accessories.");
		}
		return this;
	}
	
	/**
	 * Verify EIP Due Today Price & Dollar Symbol at Accessory Review Cart page
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyEipDueTodayForAccesories() {
		try {
			waitforSpinner();
			for (WebElement DueTodayPrice : eipDueTodayPrice) {
				Assert.assertTrue(DueTodayPrice.isDisplayed(),
						"Due Today Price is not displayed for Accessories pricing.");
				Assert.assertTrue(DueTodayPrice.getText().contains("$"),
						"$ is not displayed for all Accessories Due Today pricing.");
			}
			Reporter.log("Due Today Price is present and contain $ for all Accessories.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Due Today Price and $ for all Accessories");
		}
		return this;
	}
	
	/**
	 * Verify EIP Due Today Total Price & Dollar Symbol at Accessory Review Cart page
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyEipDueTodayTotalPriceForAccesories() {
		try {
			waitforSpinner();
			Assert.assertTrue(dueTodayTotalAmt.isDisplayed(),
					"EIP Due Today Total Price is not displayed for Accessories pricing.");
			Reporter.log("EIP Due Today Total Price is present for all Accessories.");
			Assert.assertTrue(dueTodayTotalAmt.getText().contains("$"),
					"$ is not displayed for Due Today Total Price.");
			Reporter.log("EIP Due Today Total $ symbol is present for all Accessories");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Due Today Total Price and $ for all Accessories");
		}
		return this;
	}
	

	/**
	 * Verify EIP Due Monthly Total Price & Dollar Symbol at Accessory Review Cart
	 * page
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyEipDueMonthlyTotalPriceForAccesories() {
		try {
			waitforSpinner();
			Assert.assertTrue(dueMonthlyTotalAmt.isDisplayed(),	"EIP Due Monthly Total Price is not displayed for Accessories pricing.");
			Reporter.log("EIP Due Monthly Total Price is present for Accessories.");
			Assert.assertTrue(dueMonthlyTotalAmt.getText().contains("$"), "$ is not displayed for Due Monthly Total Price.");
			Reporter.log("EIP Due Monthly Total Price $ symbol is present for Accessories.");
			Assert.assertTrue(dueMonthlyTotalAmt.getText().contains("mo"), " Mo text is not displayed for is not displayed for Due Monthly Total Price.");
			Reporter.log("EIP Due Monthly Total Price mo text is present for Accessories.");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Due Monthly Total Price and $ for Accessories.");
		}
		return this;
	}
	
	
	
	
	
	/**
	 * Verify Details Label
	 */
	public AccessoriesStandAloneReviewCartPage verifyDetailsLabel() {
		try {
			waitforSpinner();
			Assert.assertTrue(detailsLabel.isDisplayed(), "Details label is not displayed.");
			Reporter.log("Details label is displayed.");
		} catch (Exception e) {
			Assert.fail("Details label is not displayed.");
		}
		return this;
	}
	
	/**
	 * Verify Due Monthly Label
	 */
	public AccessoriesStandAloneReviewCartPage verifyDueMonthlyLabel() {
		try {
			waitforSpinner();
			Assert.assertTrue(dueMonthlyLabel.getText().equalsIgnoreCase("DUE MONTHLY"), "Due Monthly label is not correct.");
			Reporter.log("Due Monthly Label is displayed.");
		} catch (Exception e) {
			Assert.fail("Due Monthly is not displayed.");
		}
		return this;
	}

	/**
	 * Verify Due Monthly Label is not displaying
	 */
	public AccessoriesStandAloneReviewCartPage verifyDueMonthlyLabelNotDisplay() {
		try {
			waitforSpinner();
			Assert.assertTrue(dueMonthlyLabel.getText().equalsIgnoreCase(""), "Due Monthly label is present");
			Reporter.log("Due Monthly Label is not displayed.");
		} catch (Exception e) {
			Assert.fail("Due Monthly Label is displayed.");
		}
		return this;
	}
	/**
	 * Verify Due Today Label
	 */
	public AccessoriesStandAloneReviewCartPage verifyDueTodayLabel() {
		try {
			waitforSpinner();
			Assert.assertTrue(dueTodayLabel.getText().equalsIgnoreCase("DUE TODAY"), "Due Today label is not correct.");
			Reporter.log("Due Today Label is displayed.");
		} catch (Exception e) {
			Assert.fail("Due Today Label is not displayed");
		}
		return this;
	}

	/**
	 * Click Monthly Radio Button
	 */
	public AccessoriesStandAloneReviewCartPage clickMonthlyRadioButton() {
		try {
			clickElementWithJavaScript(monthlyPaymentRadioBtn);
			Reporter.log("Monthly Radio Button is clickable.");
		} catch (Exception e) {
			Assert.fail("Monthly Radio Button is not clickable.");
		}
		return this;
	}

	/**
	 * Click One payment Radio
	 */
	public AccessoriesStandAloneReviewCartPage clickOnePaymentRadioButton() {
		try {
			clickElementWithJavaScript(onePaymentRadioBtn);
			Reporter.log("One payment Radio Button is clickable.");
		} catch (Exception e) {
			Assert.fail("One payment Radio Button is not clickable.");
		}
		return this;
	}

	/**
	 * Verify Review Cart Text
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyReviewCartText(){
		checkPageIsReady();
		waitforSpinner();
		try {
			Assert.assertTrue(reviewCartText.isDisplayed(), "Review Cart Text is not displayed.");
			Reporter.log("Review Cart Text displayed. ");
		} catch (Exception e) {
			Assert.fail("Failed to display Review Cart Text.");
		}
		return this;
	}
	
	/**
	 * Verify  Due Monthly amount for each accessory
	 */
	public AccessoriesStandAloneReviewCartPage verifyDueMonthlyAmtForEachAccessory(int DueMonthlyAmt) {
		try {
			waitforSpinner();
			for (int i = 1; i <= DueMonthlyAmt; i++) {
				Assert.assertTrue(eipDueMonthlyPrice.get(i).isDisplayed(), "Due Monthly amount for each accessory is not displayed");	
			}
			Reporter.log("Due Monthly amount for each accessory is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Due Monthly amount for each accessory.");
		}
		return this;
	}
	/**
	 * Verify Legal Text
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyLegalText(){
		try {
			waitforSpinner();
			Assert.assertTrue(legalText.isDisplayed(), "Legal Text is not displayed.");
			Assert.assertTrue(legalText.getText().contains("IF YOU CANCEL WIRELESS SERVICE, REMAINING BALANCE ON ACCESSORIES BECOMES DUE. 0% APR. Qual’g credit & service req’d. Monthly charges for items in this cart; will change upon full repayment of any financed item."), "In-correct Legal Text is displaying.");
			Reporter.log("Legal Text displayed. ");
		} catch (Exception e) {
			Assert.fail("Failed to display Legal Text.");
		}
		return this;
	}
	
	/**
	 * Verify Legal Text is not displaying
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyLegalTextNotDisplay(){
		try {
			waitforSpinner();
			Assert.assertFalse(legalText.isDisplayed(), "Legal Text displayed.");
			Reporter.log("Legal Text is not displayed. ");
		} catch (Exception e) {
			Assert.fail("Legal Text displayed.");
		}
		return this;
	}
	/**
	 * Verify Table is displayed
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyTable(){
		try {
			waitforSpinner();
			Assert.assertTrue(table.isDisplayed(), "Table is not displayed.");
			Reporter.log("Table is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Table.");
		}
		return this;
	}
	
	/**
	 * Get Accessory Due Monthly amount Integer format
	 * 
	 * @return
	 */	
	public Double getAccessoryDueMonthlyInteger(Integer NumberOfAccessories) {
		Double dueMonthlySum = 0.00;
		try {
			String dueMonthlyPayment = null;
			for (int i = 0; i < NumberOfAccessories; i++)  {
				dueMonthlyPayment = eipDueMonthlyPrice.get(i).getText();
				dueMonthlyPayment = dueMonthlyPayment.substring(dueMonthlyPayment.lastIndexOf("$") + 1);
				dueMonthlySum = dueMonthlySum + Double.parseDouble(dueMonthlyPayment);
			}
		} catch (Exception e) {
			Assert.fail("Failed to identify Accessory Due Monthly amount Summary");
		}
		return dueMonthlySum;
	}
	/**
	 * Get Accessory Due Today amount Integer format
	 * 
	 * @return
	 */	
	public Double getSumOfAccessoriesDueTodayInteger(Integer NumberOfAccessories) {
		Double dueSum = 0.00;
		try {
			String dueTodayPayment = null;
			for (int i = 0; i < NumberOfAccessories; i++)  {
				dueTodayPayment = eipDueTodayPrice.get(i).getText();
				dueTodayPayment = dueTodayPayment.substring(dueTodayPayment.lastIndexOf("$") + 1);
				dueSum = dueSum + Double.parseDouble(dueTodayPayment);
			}
		} catch (Exception e) {
			Assert.fail("Failed to identify Accessory Due Today Summary.");
		}
		return dueSum;
	}

	/**
	 * Verify that monthly amount from accessory page is equal to  Total monthly amount from Cart page
	 */
	public AccessoriesStandAloneReviewCartPage compareAccessoryDueMonthlyAmounts(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Due Monthly amount from Accessory is not matched with Due Monthly Total amount from cart.");
			Reporter.log("Due Monthly amount from Accessory is matched with Total Monthly amount from cart.");
		} catch (Exception e) {
			Assert.fail("Failed to compare Due Monthly Total Amount with Due Monthly from accesory page.");
		}
		return this;
	}
	/**
	 * Verify that Due Today amount from accessory page is equal to  Total monthly amount from Cart page
	 */
	public AccessoriesStandAloneReviewCartPage compareAccessoryDueTodayAmounts(double todayValue1, double todayValue2) {
		try {
			Assert.assertEquals(todayValue1, todayValue2, " Due Today amount from Accessory is not matched with Due Today Total amount from cart.");
			Reporter.log("Due Today amount from Accessory is matched with Total Today amount from cart.");
		} catch (Exception e) {
			Assert.fail("Failed to compare Due Today Total Amount with Due Today at accesory review cart page.");
		}
		return this;
	}
	
	/**
	 * Get Due Today Total amount Integer format
	 * 
	 * @return
	 */
	public Double getDueTodayTotalIntegerCartPage() {
		String dueTodayPayment;
		dueTodayPayment = dueTodayTotalAmt.getText();
		String dueToday = dueTodayPayment.substring(dueTodayPayment.lastIndexOf("$")+1);
		return Double.parseDouble(dueToday);
	}
	
	/**
	 * Get Due Monthly Total amount Integer format
	 * 
	 * @return
	 */
	public Double getDueMonthlyTotalIntegerCartPage() {
		String monthlyTotal;
		monthlyTotal = dueMonthlyTotalAmt.getText();
		int dollarIndex=monthlyTotal.indexOf("$");
		int moIndex=monthlyTotal.indexOf("/");
		String dueMonthlyTotal = monthlyTotal.substring(dollarIndex+1, moIndex);
		return Double.parseDouble(dueMonthlyTotal);
	}
	/**
	 * Get shipping amount Integer format
	 * 
	 * @return
	 */
	public Double getshippingIntegerCartPage() {
		String shippingPayment;
		shippingPayment = shippingAmt.getText();
		String dueShippingPayment = shippingPayment.substring(shippingPayment.lastIndexOf("$")+1);
		return Double.parseDouble(dueShippingPayment);
	}
	
	/**
	 * Click Check out button
	 */
	public AccessoriesStandAloneReviewCartPage clickCheckOutButton() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.elementToBeClickable(checkoutBtn));
			clickElementWithJavaScript(checkoutBtn);
			Reporter.log("Check out Button is clickable.");
		} catch (Exception e) {
			Assert.fail("Check out Button is not clickable.");
		}
		return this;
	}
	
	/**
	 * Click Continue Shopping button
	 */
	public AccessoriesStandAloneReviewCartPage clickContinueShoppingButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(continueShoppingButtonMobile);
				(continueShoppingButtonMobile).click();
				Reporter.log("Continue shopping Button is clickable.");
			} else {
				(continueShoppingButton).click();
				Reporter.log("Continue shopping Button is clickable.");
			}
			
		} catch (Exception e) {
			Assert.fail("Continue shopping Button is clickable");
		}
		return this;
	}
	
	/**
	 * Click Remove Accessory Device From Cart
	 */
	public AccessoriesStandAloneReviewCartPage clickRemoveAccessoryDeviceFromReviewCart() {
		try {
			(removeAccessoryDeviceFromCart).click();
			Reporter.log("Remove Accessory Device is clickable from ReviewCart");
		} catch (Exception e) {
			Assert.fail("Remove Accessory Device is not clickable from ReviewCart");
		}
		return this;
	}
	
	/**
	 * Verify that Total Financed Amount is displayed
	 */

	public AccessoriesStandAloneReviewCartPage verifyTotalFinancedAmountValueisDisplayed() {
		try {
			waitforSpinner();
			Assert.assertTrue(totalFinancedAmountLabel.isDisplayed(), "Total Financed Amount Label is not displayed");
			Assert.assertTrue(totalFinancedAmountValue.isDisplayed(), "Total Financed Amount Value is not displayed");
			Reporter.log("Total Financed Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Total Financed Amount");
		}
		return this;
	}
	
	/**
	 * Get Total Financed Amount 
	 * 
	 * @return
	 */
	public Double getAccessoryTFAInCart() {
		String tFAPrice;
		tFAPrice = totalFinancedAmountValue.getText();
		tFAPrice = tFAPrice.substring(1, tFAPrice.length());
		return Double.parseDouble(tFAPrice);
	}
	
	/**
	 * Verify that Total Financed Amount from Accessory PDP page is equal to  Total Financed Amount from Cart page
	 */
	public AccessoriesStandAloneReviewCartPage compareAccessoryTFAmounts(Double value1, Double value2) {
		try {
			Assert.assertEquals(value1, value2, "Total Financed Amount from Accessory PDP is not matched with Total Financed Amount from cart.");
			Reporter.log("Total Financed Amount from Accessory PDP is matched with Total Financed Amount from cart.");
		} catch (Exception e) {
			Assert.fail("Failed to compare Total Financed Amount from Accessory PDP and Total Financed Amount from cart.");
		}
		return this;
	}
	
	/**
	 * Verify that Total Financed Amount is Not displayed
	 */

	public AccessoriesStandAloneReviewCartPage verifyTotalFinancedAmountValueisNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(totalFinancedAmountLabel.isDisplayed(), "Total Financed Amount Label is displayed. But should not");
			Assert.assertFalse(totalFinancedAmountValue.isDisplayed(), "Total Financed Amount Value is displayed. But should not");
			Reporter.log("Total Financed Amount is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Total Financed Amount");
		}
		return this;
	}
	
	/**
	 * Verify that item is present on the cart
	 */
	public AccessoriesStandAloneReviewCartPage verifyAccessoryIsPresentOnCartPage(MyTmoData myTmoData) {
		try {
			boolean isPresent = false;
			waitforSpinner();
			Thread.sleep(5000);
			waitFor(ExpectedConditions.visibilityOf(accessoryNameInCart.get(0)));
			for (WebElement item : accessoryNameInCart) {
				if(item.getText().contains(myTmoData.getAccessoryName())) {
					isPresent = true;
				}
			}
			Assert.assertTrue(isPresent, "Selected Accessory NOT present on AccessoryStandalone Cart Page");
			Reporter.log("Selected Accessory present on AccessoryStandalone Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory name");
		}
		return this;
	}
	
	/**
	 * Verify Eip Amount Displayed With format
	 */
	public AccessoriesStandAloneReviewCartPage verifyEipAmountDisplayedWithformat() {
		try {
			for (WebElement DueMonthlyPrice : eipDueMonthlyPrice) {
				Assert.assertTrue(DueMonthlyPrice.getText().contains("/mo"),
					"/mo is not displayed for all Accessories EIP Due Monthly pricing.");
				}
			Reporter.log("/mo is displayed for all Accessories EIP Due Monthly pricing.");
			} catch (Exception e) {
				Assert.fail("Failed to verify EIP Due Monthly price format /mo for Accessories.");
			}
		return this;
		}

	/**
	 * Verifies Added accessory on Cart Page
	 * @param relatedAccessory
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyAddedAccessoryIsAvailableOnReviewCartPage(String relatedAccessory) {
		try {
			boolean isPresent = false;
			waitforSpinner();
			Thread.sleep(5000);
			waitFor(ExpectedConditions.visibilityOf(accessoryNameInCart.get(0)));
			for (WebElement item : accessoryNameInCart) {
				if(item.getText().contains(relatedAccessory)) {
					isPresent = true;
				}
			}
			Assert.assertTrue(isPresent, "Selected Accessory NOT present on AccessoryStandalone Cart Page");
			Reporter.log("Selected Accessory present on AccessoryStandalone Cart Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory name");
		}
		return this;
	}

	/**
	 * Verify that Past Due Message is displayed
	 */

	public AccessoriesStandAloneReviewCartPage verifyPastDueMessageIsDisplayed() {
		checkPageIsReady();

		try {
			waitFor(ExpectedConditions.visibilityOf(pastDueStickyBanner));
			Assert.assertTrue(pastDueMessageTitle.isDisplayed(), "Past Due Message Title is not displayed");
			Assert.assertTrue(pastDueMessageBody.isDisplayed(), "Past Due Message Body is not displayed");
			Reporter.log("Past Due Message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Past Due Message");
		}
		return this;
	}

	/**
	 * Verify that Past Due Price is displayed in Sticky Banner
	 */

	public AccessoriesStandAloneReviewCartPage verifyPastDuePriceIsDisplayed() {
		try {
			Assert.assertTrue(pastDueMessageTitle.getText().contains("$"), "Past Due Price is not displayed");
			Reporter.log("Past Due Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Past Due Price");
		}
		return this;
	}

	/**
	 * Verify that Checkout Button is disabled
	 */

	public AccessoriesStandAloneReviewCartPage verifyCheckOutButtonIsDisabled() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(checkoutBtn));
			Assert.assertFalse(checkoutBtn.isEnabled(), "CheckOut Button is enabled");
			Reporter.log("CheckOut Button is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify CheckOut Button disabled");
		}
		return this;
	}


	/**
	 * Verify that COntinue to SHipping Button is enabled
	 */

	public AccessoriesStandAloneReviewCartPage verifyContinueToShoppingEnabled() {
		try {
			if(getDriver() instanceof AppiumDriver)
			{
			Assert.assertTrue(continueShoppingButtonMobile.isEnabled(), "Continue To Shipping Button is disabled");
			}
			else
				Assert.assertTrue(continueShoppingButton.isEnabled(), "Continue To Shipping Button is disabled");
			Reporter.log("Continue To Shipping Button is enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Continue To Shipping Button enabled");
		}
		return this;
	}


	/**
	 * Click on Sticky Banner
	 */

	public AccessoriesStandAloneReviewCartPage clickOnStickyBanner() {
		try {
			pastDueStickyBanner.click();
			Reporter.log("Clicked on Sticky Banner");
		} catch (Exception e) {
			Assert.fail("Failed to click on Sticky Banner");
		}
		return this;
	}


	/**
	 * Click on Close Icon on PopUp
	 */

	public AccessoriesStandAloneReviewCartPage clickOnPopUpCloseIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(popupCloseIcon));
			clickElementWithJavaScript(popupCloseIcon);
			Reporter.log("Clicked on Close Icon");
		} catch (Exception e) {
			Assert.fail("Failed to click on Close Icon");
		}
		return this;
	}

	/**
	 * Verify Pop Up Modal header iS displayed
	 */

	public AccessoriesStandAloneReviewCartPage verifyPopUpModalHeaderIsDisplayed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(popUpModalHeader));
			Assert.assertTrue(popUpModalHeader.isDisplayed(), "Pop Up Modal Header is not displayed");
			Reporter.log("Pop Up Modal Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pop Up Modal Header");
		}
		return this;
	}

	/**
	 * Verify make Payment CTA displayed
	 */

	public AccessoriesStandAloneReviewCartPage verifyMakePaymentCTAIsDisplayed() {
		try {
			Assert.assertTrue(popUpModalMakePaymentCTA.isDisplayed(), "Make Payment CTA is not displayed");
			Reporter.log("PMake Payment CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Make Payment CTA");
		}
		return this;
	}


	/**
	 * Verify and click on make Payment CTA displayed
	 */

	public AccessoriesStandAloneReviewCartPage clickMakePaymentCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(popUpModalMakePaymentCTA));
			popUpModalMakePaymentCTA.click();
			Reporter.log("PMake Payment CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Make Payment CTA");
		}
		return this;
	}
	
	/**
	 * verify Puerto Rico banner 
	 */
	public AccessoriesStandAloneReviewCartPage verifyPuretoRicoStickyBanner() {
		try {
			waitFor(ExpectedConditions.visibilityOf(pastDueMessageBody));
			Assert.assertTrue(pastDueMessageBody.isDisplayed(), "Pureto rico banner is not displayed");
			Reporter.log("Pureto rico banner  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pureto rico banner ");
		}
		return this;
	}
	
	/**
	 *  click on make Payment CTA displayed
	 */

	public AccessoriesStandAloneReviewCartPage clickPuretoRicoStickyBanner() {
		try {
			waitFor(ExpectedConditions.visibilityOf(pastDueMessageBody));
			pastDueMessageBody.click();
			Reporter.log("Pureto rico banner is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Pureto rico banner");
		}
		return this;
	}
	
	/**
	 * Verify Accessories Stand Alone Checkout Page
	 * 
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage verifyAccessoriesStandAloneCheckOutPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			getDriver().getCurrentUrl().contains("secure-checkout");
			Reporter.log("Accessories stand alone Check Out page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Accessories stand alone Check Out page not displayed");
		}
		return this;
	}
	
}
