/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.SignUpData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class MissingSecurityQuestionsPage extends CommonPage {

	private static final String pageUrl = "security";

	@FindBy(css = ".mobile-padding-bottom20.mobile-padding-left20.ng-binding")
	private WebElement signupTextHeader;

	@FindBy(id = "question_1")
	private WebElement securityQuestion1;

	@FindBy(id = "securitytext_1")
	private WebElement securityQuestion1Answer;

	@FindBy(id = "question_2")
	private WebElement securityQuestion2;

	@FindBy(id = "securitytext_2")
	private WebElement securityQuestion2Answer;

	@FindBy(id = "question_3")
	private WebElement securityQuestion3;

	@FindBy(id = "securitytext_3")
	private WebElement securityQuestion3Answer;

	@FindBy(css = "input[value='Next']")
	private WebElement nextButton;

	/**
	 * 
	 * @param webDriver
	 */
	public MissingSecurityQuestionsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * select question 1 and enter answer
	 * 
	 * @param value
	 */
	private MissingSecurityQuestionsPage selectQuestionAndEnterAnswer1(String value) {

		try {
			selectElementFromDropDown(securityQuestion1, Constants.SELECT_BY_INDEX, "1");
			sendTextData(securityQuestion1Answer, value);
			Reporter.log("Successfully sent security answer");
		} catch (Exception e) {
			Reporter.log("Sending security answer was unsuccessful." + e.getMessage());
		}
		return this;
	}

	/**
	 * select question 2 and enter answer
	 * 
	 * @param value
	 */
	private MissingSecurityQuestionsPage selectQuestionAndEnterAnswer2(String value) {
		try {
			selectElementFromDropDown(securityQuestion1, Constants.SELECT_BY_INDEX, "2");
			sendTextData(securityQuestion2Answer, value);
			Reporter.log("Successfully sent security answer");
		} catch (Exception e) {
			Reporter.log("Sending security answer was unsuccessful." + e.getMessage());
		}
		return this;
	}

	/**
	 * select question 3 and enter answer
	 * 
	 * @param value
	 */
	private MissingSecurityQuestionsPage selectQuestionAndEnterAnswer3(String value) {
		try {
			selectElementFromDropDown(securityQuestion1, Constants.SELECT_BY_INDEX, "3");
			sendTextData(securityQuestion3Answer, value);
			Reporter.log("Successfully sent security answer");
		} catch (Exception e) {
			Reporter.log("Sending security answer was unsuccessful." + e.getMessage());
		}
		return this;
	}

	/**
	 * click next button
	 */
	private MissingSecurityQuestionsPage clickNextButton() {
		try {
			nextButton.click();
			Reporter.log("Clicked on next button");
		} catch (Exception e) {
			Assert.fail("Click on next button failed" + e.getMessage());
		}
		return this;
	}

	/**
	 * Fill Security Questions And Submit
	 * 
	 * @param signUpData
	 */
	public MissingSecurityQuestionsPage answerSecurityQuestionsAndSubmit(SignUpData signUpData) {
		try {
			if (!signUpData.getSecurityQuestionAns1().isEmpty()) {
				selectQuestionAndEnterAnswer1(signUpData.getSecurityQuestionAns1());
			} else if (!signUpData.getSecurityQuestionAns2().isEmpty()) {
				selectQuestionAndEnterAnswer2(signUpData.getSecurityQuestionAns2());
			} else if (!signUpData.getSecurityQuestionAns3().isEmpty()) {
				selectQuestionAndEnterAnswer3(signUpData.getSecurityQuestionAns3());
			}
			clickNextButton();
			Reporter.log("Security questions updated succesfully");
		} catch (Exception e) {
			Assert.fail("Security questions not updated succesfully" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify MissingSecurityQuestions Page.
	 *
	 * @return the MissingSecurityQuestions Page class instance.
	 */
	public MissingSecurityQuestionsPage verifyMissingSecurityQuestionsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("MissingSecurityQuestions Page page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MissingSecurityQuestions Page not displayed");
		}
		return this;

	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public MissingSecurityQuestionsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

}
