package com.tmobile.eservices.qa.api.exception;

public class InvalidAuthTokenException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidAuthTokenException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }	

}
