package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PaymentMicroserviceApiV1 extends ApiCommonLib {

	/***
	 * Summary: Process a new payment
	 * Headers:interactionId ,
	 * 	-workflowId 
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response payments(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildPaymentsHeader(apiTestData);
		String resourceURL = "v1/payments";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.POST);
		return response;
	}
    
	/***
	 * Summary: Process a new payment
	 * Headers:interactionId ,
	 * 	-workflowId 
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response paymentValidation(ApiTestData apiTestData, String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildShopValidationOfPaymentToken(apiTestData,tokenMap);
		String resourceURL = "accounts/v3/validation/"+tokenMap.get("ban");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}
	
	/***
	 * Summary: Process a new payment
	 * Headers:interactionId ,
	 * 	-workflowId 
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response paymentsRefund(ApiTestData apiTestData,String transactionId) throws Exception {
		Map<String, String> headers = buildPaymentsHeader(apiTestData);
		String resourceURL = "v1/payments/"+transactionId+"/refund";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.POST);
		return response;
	}
	/***
	 * Summary: Process a new payment
	 * Headers:interactionId ,
	 * 	-workflowId 
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getCustomErrorInfo(ApiTestData apiTestData,String transactionId,String code) throws Exception {
		Map<String, String> headers = buildPaymentsHeader(apiTestData);
		String resourceURL = "v1/payments/"+transactionId+"/errors/"+code;
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.POST);
		return response;
	}
	private Map<String, String> buildPaymentsHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		//final String dcpToken = createPlattoken(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();
		//headers.put("Authorization", dcpToken);
		headers.put("Content-Type", "application/json");
		headers.put("interactionId ", "interactionId ");
		headers.put("workflowId ","workflowId ");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid","PaymentsAPITest");
		return headers;
	}
   
	
	  private Map<String, String> buildShopValidationOfPaymentToken(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

			String transactionType = "UPGRADE";
			//Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
			Map<String, String> headers = new HashMap<String, String>();
			if (StringUtils.isNoneEmpty(tokenMap.get("addaline"))) {
				transactionType = "ADDALINE";
			}
			headers.put("clientid","ESERVICE");
			headers.put("applicationId","MYTMO");
			headers.put("transactionType",transactionType);
			headers.put("transactionId","5d96155b-67b0-40df-947f-7eba582efafe");
			headers.put("channelId","DESKTOP");
			headers.put("channel_id","WEB");
			headers.put("usn","1000021111");
			headers.put("application_id","MYTMO");
			headers.put("Content-Type","application/json");
			headers.put("interactionId","123123123");
			headers.put("phoneNumber",apiTestData.getMsisdn());
			headers.put("transactionBusinesKey","BAN");
			headers.put("transactionBusinesKeyType",apiTestData.getBan());
			headers.put("Authorization",checkAndGetAccessToken());
			return headers;
			

		}

}


