package com.tmobile.eservices.qa.pages.tmng.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class AddressProfileV1 extends ApiCommonLib {

	/***
	 * This is the API specification for the EOS AddressValidation . The EOS API
	 * will inturn invoke DCP apis to execute the quote functions. parameters: -
	 * $ref: '#/parameters/oAuth' - $ref: '#/parameters/transactionId' - $ref:
	 * '#/parameters/correlationId' - $ref: '#/parameters/applicationId' - $ref:
	 * '#/parameters/channelId' - $ref: '#/parameters/clientId' - $ref:
	 * '#/parameters/transactionBusinessKey' - $ref:
	 * '#/parameters/transactionBusinessKeyType' - $ref:
	 * '#/parameters/transactionType'
	 * 
	 * @return
	 * @throws Exception
	 */

	public Response getProfileIdForCreditCheck(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildProfileIdForCreditCheckAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, "https://tmobileqat-qat03-pro.apigee.net");
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("commerce/v3/tokens", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildProfileIdForCreditCheckAPIHeader(ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", "Bearer " + checkAndGetTMNGUNOToken());
		headers.put("correlationId", "prescreen_99");
		headers.put("ApplicationId", "TMO");
		headers.put("channelId", "WEB");
		headers.put("interactionid", "xasdf1234assadia");
		headers.put("transactionType", "ACTIVATION");
		headers.put("Cache-Control", "no-cache");
		headers.put("Content-type", "application/json");
		headers.put("X-Auth-Originator", checkAndGetTMNGUNOToken());
		return headers;
	}

}
