package com.tmobile.eservices.qa.pages.shop;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.ios.IOSDriver;

public class UNOWatchPLPPage extends CommonPage {

	@FindBy(css = "p.product-name")
	private List<WebElement> productName;

	@FindBy(css = "div.manufacturer span[data-e2e='product-card-manufacturer']")
	private List<WebElement> productManufacturer;

	public UNOWatchPLPPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify UNO PLP Page
	 *
	 * @return
	 */
	public UNOWatchPLPPage verifyWatchesPageLoaded() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains("/smart-watches"));
			Reporter.log(Constants.PLP_PAGE_HEADER);
		} catch (Exception e) {
			Assert.fail("Product List Page is Not Loaded");
		}
		return this;
	}

	/**
	 * Click device containing availability
	 *
	 * @param pName
	 *
	 */
	public void clickDeviceByName(String pName) {
		try {
			pName = pName.trim();
			Boolean elementClick = true;
			do {
				for (int i = 0; i < productName.size(); i++) {
					String productNameWithManufacturer = productManufacturer.get(i).getText() + " "
							+ productName.get(i).getText();
					if (productName.get(i).getText().trim().endsWith(pName)
							|| productNameWithManufacturer.endsWith(pName)) {
						String deviceName = productName.get(i).getText();
						// moveToElement(productName.get(i));
						clickElementWithJavaScript(productName.get(i));
						Reporter.log("Clicked on product: " + deviceName);
						elementClick = false;
						break;
					}
				}
				waitForSpinnerInvisibility();
				if (getDriver().getCurrentUrl().contains("cell-phones")
						| getDriver().getCurrentUrl().contains("smart-watches")
						| getDriver().getCurrentUrl().contains("accessories")
						| getDriver().getCurrentUrl().contains("tablets")) {
					scrollToNextLine();
				} else {
					break;
				}
			} while (elementClick);
		} catch (Exception e) {
			Assert.fail("Fail to click on next device: " + pName);
		}
	}

	/**
	 * Scroll to next line in PLP
	 *
	 */
	public void scrollToNextLine() {
		try {
			if (getDriver() instanceof IOSDriver) {
				scrollDownMobileIOS();
			} else {
				((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
			}
		} catch (Exception e) {
			Assert.fail("Failed to scroll to next line");
		}
	}

	public void scrollDownMobileIOS() {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		js.executeScript("mobile: scroll", scrollObject);

	}

}
