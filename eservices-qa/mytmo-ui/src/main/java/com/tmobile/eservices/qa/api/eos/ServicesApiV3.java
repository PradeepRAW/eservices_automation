package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class ServicesApiV3 extends ApiCommonLib {
	public static String accessToken;
	Map<String, String> headers;
	/***
	 * Summary: Retreive the Service details (SOC details) for sale and conflicts
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getServicesForSale(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildGetSocHeader(apiTestData,  tokenMap);
		String resourceURL = "v3/service/"+tokenMap.get("cartId")+"/socs";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
	}
	
	/***
	 * Summary: Retreive the Service plan details (plan details) for sale and conflicts
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 * 
	 * @return
	 * @throws Exception
	 */

	public Response getServicesForPlan(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception {	    
	       Map<String, String> headers = buildGetSocHeader(apiTestData, tokenMap);
			String resourceURL = "v3/service/"+tokenMap.get("cartId")+"/plans?msisdn="+apiTestData.getMsisdn();
	    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
	       return response;
	}
	
	public Response getOptionalServicesResponse(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception {	    
	       Map<String, String> headers = buildGetServiceHeader(apiTestData, tokenMap);
			String resourceURL = "v3/service/"+tokenMap.get("cartId")+"/socs";
	    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
	       return response;
	}

	private Map<String, String> buildGetSocHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
	//	tokenMap.put("version", "v1");
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
    	jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"),tokenMap);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept","application/json");
		headers.put("applicationid","MYTMO");
		headers.put("cache-control","no-cache");
		headers.put("channelid","WEB");
		headers.put("clientid","233444");
		headers.put("content-type","application/json");
		headers.put("correlationid",checkAndGetPlattokenJWT(apiTestData,jwtTokens.get("jwtToken")));
		headers.put("transactionid","xasdf1234asa4444");
		headers.put("transactionbusinesskeytype",apiTestData.getBan());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionType","ADDALINE");
		headers.put("usn","testUSN");
		headers.put("version","2");
		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
		headers.put("authorization",jwtTokens.get("jwtToken"));
		headers.put("interactionid","IID");
		return headers;
	}
	
    public Response getServicesForWithOutAutopay(ApiTestData apiTestData, Map<String,String> tokenMap) throws Exception{
        //accessToken = createPlattoken(apiTestData);
       Map<String, String> headers = buildGetSocHeader(apiTestData, tokenMap);
		String resourceURL = "v3/service/"+tokenMap.get("cartId")+"/plans?msisdn="+apiTestData.getMsisdn();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
       return response;
    }  
    
    private Map<String, String> buildGetServiceHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
  
    		JWTTokenApi jwtTokenApi = new JWTTokenApi();
        	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
        	jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"),tokenMap);
    		Map<String, String> headers = new HashMap<String, String>();
    		headers.put("accept","application/json");
    		headers.put("applicationid","MYTMO");
    		headers.put("cache-control","no-cache");
    		headers.put("channelid","WEB");
    		headers.put("clientid","233444");
    		headers.put("content-type","application/json");
    		headers.put("correlationid",checkAndGetPlattokenJWT(apiTestData,jwtTokens.get("jwtToken")));
    		headers.put("transactionid","xasdf1234asa4444");
    		headers.put("transactionbusinesskeytype",apiTestData.getBan());
    		headers.put("phoneNumber",apiTestData.getMsisdn());
    		headers.put("transactionType","ADDALINE");
    		headers.put("usn","sampleUsn12");
    		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
    		headers.put("Authorization",jwtTokens.get("jwtToken"));
    
    		return headers;
    	}

}


