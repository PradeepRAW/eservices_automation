package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class PaymentsWalletsV4 extends ApiCommonLib {

	/***
	 * Summary: This API is used do the payments  through OrchestrateOrders API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-interactionId
	 * 	-workflowId
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */

	private Map<String, String> buildWalletApiHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("ban", apiTestData.getBan());
		headers.put("tmo-id", apiTestData.gettmoId());
		headers.put("Authorization", getAccessToken());
		return headers;
	}

	public Response verifypaymentmethod(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("application_id", "123");
		headers.put("cache-control", "no-cache");
		headers.put("channel_id", "CARE");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v4/spm/verifyPaymentMethod", RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}

	public Response WalletCreation(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("accountNumber", apiTestData.getBan());
		headers.put("activityid", "23432324");
		headers.put("application_id", "UUI");
		headers.put("cache-control", "no-cache");
		headers.put("channel_id", "CARE");
		headers.put("interactionid", "12335");
		headers.put("userid", "23423423");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v4/mpi", RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}
	
	public Response walletUpdate(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("application_id", "UUI");//MYTMO
		headers.put("activityid", "234234234");
		headers.put("channel_id", "CARE");
		headers.put("interactionId","1231");
		headers.put("userid", "234234");
		headers.put("tmoid", "U-2c0cf5cd-8250-4fd8-a165-2e068d659413");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v4/mpi", RestCallType.PUT);
		System.out.println(response.getStatusCode());
		return response;

	}
	
	public Response walletSearch(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		
		headers.put("channel_id", "RETAIL");
		headers.put("accountNumber", apiTestData.getBan());
		headers.put("application_id", "123");
		headers.put("channelVersion", "123");
		headers.put("channel", "WEB");
		headers.put("client_id", "123");
		headers.put("interactionId", "123456787");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("walletCapacity", "10");
		headers.put("isLimitedWalletCapacity", "false");
		headers.put("usn", "123");
		headers.put("workflowId", "walletsearch");
		
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v4/spm", RestCallType.GET);
		System.out.println(response.asString());
		return response;

	}

	public Response walletDelete(ApiTestData apiTestData,String requestBody) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("application_id", "UUI");
		headers.put("channel", "MWEB");
		headers.put("channelVersion", "1");
		headers.put("channel_id", "CARE");
		headers.put("client_id", "TMOID");
		headers.put("interactionId", "12345");
		headers.put("usn", "4253452642364");
		headers.put("workflowId", "235232872863262");
/*		headers.put("Host", "qlab03.eos.corporate.t-mobile.com");
		headers.put("accept","application/json");
		headers.put("accept-encoding","gzip, deflate");*/
		
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v4/mpi", RestCallType.DELETE);
		System.out.println(response.asString());
		return response;

	}

	public Response validatePaymentMethod(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);

		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("interactionId", "123456787");
		headers.put("channel_id", "TSL");
		headers.put("application_id", "MYTMO");

		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v4/validatePaymentMethod", RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}

	public Response validateRoutingNumber(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("interactionId", "123456787");
		headers.put("channel_id", "RETAIL");
		headers.put("application_id", "ESERVICE");
		
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v4/validateRoutingNumber/"+apiTestData.getRoutingNo(), RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}
}
