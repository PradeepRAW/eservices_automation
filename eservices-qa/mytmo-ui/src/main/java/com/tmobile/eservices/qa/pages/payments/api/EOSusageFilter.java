package com.tmobile.eservices.qa.pages.payments.api;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSusageFilter extends  EOSCommonLib {

	




/***********************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 ***********************************************************************************/

public Response getResponseusagesummary(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	    	 JSONObject rootval = new JSONObject();
        	JSONArray custinfo=new JSONArray();
	    	JSONObject requestParams = new JSONObject();
	    	
	    	
	 		requestParams.put("msisdn", alist[0]);
	 		requestParams.put("name", "Test");
	 		rootval.put("customerInfo",custinfo);
	 		custinfo.put(requestParams);
	 		
	 		
	 	RestService restService = new RestService(rootval.toString(), eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization", "Bearer "+alist[2]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("userRole", alist[4]);
	    restService.addHeader("msisdn", alist[0]);
	    response = restService.callService("v1/usage/summary",RestCallType.POST);
	    
	}

	    return response;
}


public String getuseddata(Response response) {
	String data=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalData")!=null) {
			List<String> vals=jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalData");
			for(String s:vals) {
				if(!s.toString().trim().equalsIgnoreCase("0.00")) {
					Reporter.log("rettieved data "+s);
					return "true";
				}
			}
	        return "false";
	

}

}
	return data;
}



//getusedmessages

public String getusedmessages(Response response) {
	String messages=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalMessages")!=null) {
			List<String> vals=jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalMessages");
			for(String s:vals) {
				if(!s.toString().trim().equalsIgnoreCase("0")) return "true";
			}
	        return "false";
	

}

}
	return messages;
}




//getusedcalls

public String getusedcalls(Response response) {
	String calls=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalCalls")!=null) {
			List<String> vals=jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalCalls");
			for(String s:vals) {
				if(!s.toString().trim().equalsIgnoreCase("0")) return "true";
			}
	        return "false";
	

}

}
	return calls;
}

//3rdparty purchases

public String getthirdpartypurchases(Response response) {
	String thirdpartypurchases=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalThirdPartyPurchases")!=null) {
			List<String> vals=jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalThirdPartyPurchases");
			for(String s:vals) {
				if(!s.toString().trim().equalsIgnoreCase("0.00")) return "true";
			}
	        return "false";
	

}

}
	return thirdpartypurchases;
}


//t-mobile purchases

public String gettmobilepurchases(Response response) {
	String tmobilepurchases=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalTMobilePurchases")!=null) {
			List<String> vals=jsonPathEvaluator.get("usageDataByLine[0].totalUsageByBillCycle.totalTMobilePurchases");
			for(String s:vals) {
				if(!s.toString().trim().equalsIgnoreCase("0.00")) return "true";
			}
	        return "false";
	

}

}
	return tmobilepurchases;
}



/***********************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 ***********************************************************************************/

public Response getResponseusagesummarydatastash(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	    	 JSONObject rootval = new JSONObject();
        	JSONArray custinfo=new JSONArray();
	    	JSONObject requestParams = new JSONObject();
	    	
	    	
	 		requestParams.put("msisdn", alist[0]);
	 		requestParams.put("name", "Test");
	 		rootval.put("customerInfo",custinfo);
	 		custinfo.put(requestParams);
	 		
	 		
	 	RestService restService = new RestService(rootval.toString(), eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization", "Bearer "+alist[2]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("userRole", alist[4]);
	    restService.addHeader("msisdn", alist[0]);
	    response = restService.callService("v1/usage/datastash",RestCallType.POST);
	    
	}

	    return response;
}
/*******************************************************************************************
 * 
 */


public Response getUsagedetailseresponsebill(String alist[],String statementid) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("msisdn", alist[0]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("userRole", alist[4]);
	    response = restService.callService("v1/usage/details?statementId="+statementid,RestCallType.GET);
	    
	}

	    return response;
}


/*******************************************************************************************
 * 
 */


public Response getUsagedetailsresponsebbill(String alist[],String docid) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization","Bearer "+alist[2]);
	    restService.addHeader("msisdn", alist[0]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("userRole", alist[4]);
	    response = restService.callService("v1/usage/details?documentId="+docid,RestCallType.GET);
	    
	}

	    return response;
}







}

