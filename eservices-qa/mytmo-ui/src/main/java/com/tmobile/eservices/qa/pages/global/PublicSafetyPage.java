package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class PublicSafetyPage extends CommonPage {

	private static final String pageUrl = "safety";

	public PublicSafetyPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Public safety page.
	 */
	public PublicSafetyPage verifyPublicSafetyPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Public safety page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Public safety page not displayed");
		}
		return this;
	}
}
