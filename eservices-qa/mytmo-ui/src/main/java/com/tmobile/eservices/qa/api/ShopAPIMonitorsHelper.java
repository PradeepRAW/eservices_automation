package com.tmobile.eservices.qa.api;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CartApiV4;
import com.tmobile.eservices.qa.api.eos.CartApiV5;
import com.tmobile.eservices.qa.api.eos.CatalogApiV3;
import com.tmobile.eservices.qa.api.eos.CatalogApiV4;
import com.tmobile.eservices.qa.api.eos.DeviceTradeInApiV3;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.api.eos.OrderApi;
import com.tmobile.eservices.qa.api.eos.PromotionApiV2;
import com.tmobile.eservices.qa.api.eos.QuoteApi;
import com.tmobile.eservices.qa.api.eos.QuoteApiV5;
import com.tmobile.eservices.qa.api.eos.ServicesApiV2;
import com.tmobile.eservices.qa.api.eos.ServicesApiV3;
import com.tmobile.eservices.qa.api.exception.InvalidAuthTokenException;
import com.tmobile.eservices.qa.api.exception.InvalidServiceResultException;
import com.tmobile.eservices.qa.api.soap.QueryCreditCheck;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class ShopAPIMonitorsHelper extends ApiCommonLib {

	public Response response = null;
	public String requestBody = null;
	public String updatedRequest = "";
	public JWTTokenApi jwtTokenApi;

	public ShopAPIMonitorsHelper() {
		jwtTokenApi = new JWTTokenApi();
	}

	public Map<String, String> prepareTokenMap(String requestName, Response response, Map<String, String> tokenMap)
			throws Exception {

		switch (requestName) {

		case "getCRP":
			tokenMap = getTokenMapForCreditCheck(response, tokenMap);
			break;
		case "retrieveGroupedProductsUsingPOST":
			tokenMap = getTokenMapForProductsFamily(response, tokenMap);
			break;
		case "createCartAPI":
			tokenMap = getTokenMapForCreateCartAPI(response, tokenMap);
			break;
		case "CreateQuoteAPI":
			tokenMap = getTokenMapForCreateQuoteAPI(response, tokenMap);
			break;
		case "UpdateQuoteAPI":
			tokenMap = getTokenMapForUpdateQuoteAPI(response, tokenMap);
			break;
		case "UpdatePayment":
			tokenMap = getTokenMapForUpdatePayment(response, tokenMap);
			break;
		case "OrderAPI":
			tokenMap = getTokenMapForOrderAPI(response, tokenMap);
		case "getSOCForSale":
			tokenMap = getTokenMapForSocForSale(response, tokenMap);
			break;
		case "updateCartAPI":
			tokenMap = getTokenMapForUpdateCartAPI(response, tokenMap);
			break;
		case "getAccessories":
			tokenMap = getTokenMapForGetAccessories(response, tokenMap);
			break;
		case "getAccountDevicesEligibleForTradeIn":
			tokenMap = getAccountDevicesEligibleForTradeIn(response, tokenMap);
			break;
		case "createQuoteTradeIn":
			tokenMap = getcreateQuoteTradeIn(response, tokenMap);
			break;
		case "getEligiblePlans":
			tokenMap = getEligiblePlans(response, tokenMap);
			break;
		case "searchPromotions":
			tokenMap = getsearchPromotions(response, tokenMap);
			break;
			
		}
		return tokenMap;
	}

	public Map<String, String> getsearchPromotions(Response response, Map<String, String> tokenMap) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (!jsonNode.isMissingNode() && jsonNode.has("promotions")) {
			JsonNode parentNode = jsonNode.path("promotions");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					JsonNode node = parentNode.get(i);
					if (node.isObject()) {
						String promoName = getPathVal(node, "promoName");
						//String promoCustomerFacingName = getPathVal(node, "promoCustomerFacingName");
						tokenMap.put("promoName", promoName);
						//tokenMap.put("promoCustomerFacingName", promoName);
					}
				}
			}
		}else if (!jsonNode.isMissingNode() && jsonNode.has("message")){
			failAndLogResponse(response, "serachPromotions");
		}
		
		return tokenMap;
	}

	private Map<String, String> getEligiblePlans(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			if (!jsonNode.isMissingNode() && jsonNode.has("planInfo")) {
				JsonNode planInfoNode = jsonNode.path("planInfo");
				if (!planInfoNode.isMissingNode() && planInfoNode.isArray()) {
					tokenMap.put("planFamilyId", getPathVal(planInfoNode.get(0), "planFamilyId"));
					tokenMap.put("ratePlanSoc", getPathVal(planInfoNode.get(0), "ratePlanSoc"));
				}
			}
		}
		return tokenMap;
	}

	private Map<String, String> getcreateQuoteTradeIn(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			tokenMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));
			tokenMap.put("deviceIMEI", getPathVal(jsonNode, "device.imei"));
			tokenMap.put("deviceName", getPathVal(jsonNode, "device.carrier[0].make[0].model[0].modelName"));
			tokenMap.put("deviceCarrier", getPathVal(jsonNode, "device.carrier[0].carrierName"));
			tokenMap.put("deviceMake", getPathVal(jsonNode, "device.carrier[0].make[0].makeName"));
		}
		return tokenMap;
	}

	private Map<String, String> getAccountDevicesEligibleForTradeIn(Response response, Map<String, String> tokenMap) {

		JsonNode jsonNode = getParentNodeFromResponse(response);

		if (!jsonNode.isMissingNode() && jsonNode.has("accountDevices")) {
			JsonNode parentNode = jsonNode.path("accountDevices");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					if ("true".equalsIgnoreCase(parentNode.get(i).get("isEligible").asText())
							&& (null != parentNode.get(i).get("device")
									&& (!parentNode.get(i).get("device").isMissingNode()))) {
						tokenMap.put("isEligibleForTradeIn", getPathVal(parentNode.get(i), "isEligible"));
						tokenMap.put("imei", getPathVal(parentNode.get(i), "imei"));
						tokenMap.put("displayName", getPathVal(parentNode.get(i), "displayName"));
						tokenMap.put("makeName", getPathVal(parentNode.get(i), "device.carrier[0].make[0].makeName"));
						tokenMap.put("TradeInModelName",
								getPathVal(parentNode.get(i), "device.carrier[0].make[0].model[0].modelName"));
					}
				}
			}

		}

		return tokenMap;
	}

	private Map<String, String> getTokenMapForGetAccessories(Response response, Map<String, String> tokenMap)
			throws Exception {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		boolean accessoryFound = false;
		String prodStatus = "";
		if (!jsonNode.isMissingNode() && jsonNode.has("data")) {

			JsonNode parentNode = jsonNode.path("data");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					// JsonNode dataNode = parentNode.get(i);
					if (null != parentNode.get(i).get("productAvailability")) {
						if ("Available".equalsIgnoreCase(
								parentNode.get(i).get("productAvailability").get("availabilityStatus").asText())) {

							tokenMap.put("skuAccessory", getPathVal(parentNode.get(i), "skuNumber"));
							tokenMap.put("FRPAccessory", getPathVal(parentNode.get(i), "pricing.offerPrice"));
							tokenMap.put("estimatedShipFromDateAccessory",
									getPathVal(parentNode.get(i), "productAvailability.estimatedShipDateFrom"));
							tokenMap.put("ShipDateToAccessory",
									getPathVal(parentNode.get(i), "productAvailability.estimatedShipDateTo"));
							accessoryFound = true;
							break;
						}
					} else {
						throw new Exception(
								"productAvailability attribute in GetAccessories Response is null or empty");
					}
					prodStatus = parentNode.get(i).get("productAvailability").get("availabilityStatus").asText();
				}

				if (!accessoryFound) {
					tokenMap.put("skuAccessory", "");
					tokenMap.put("FRPAccessory", "");
					tokenMap.put("estimatedShipFromDateAccessory", "");
					tokenMap.put("ShipDateToAccessory", "");
					tokenMap.put("exceptionMessage", "No Valid SKU found in AccessoryList because of " + prodStatus);
					throw new Exception("No Valid SKU with Available status was found in AccessoryList");
				}

			}
		} else {
			// failAndLogResponse(response, "getAccessories");
			throw new Exception("Empty Response from getAccessories");
		}

		return tokenMap;
	}

	private Map<String, String> getTokenMapForUpdateCartAPI(Response response, Map<String, String> tokenMap) {

		return tokenMap;
	}

	private Map<String, String> getTokenMapForSocForSale(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);

		tokenMap.put("socCode", getPathVal(jsonNode, "socForSale[0].socCode"));
		tokenMap.put("socName", getPathVal(jsonNode, "socForSale[0].socName"));
		tokenMap.put("socPrice", getPathVal(jsonNode, "socForSale[0].price"));
		return tokenMap;
	}

	private Map<String, String> getTokenMapForCreditCheck(Response response, Map<String, String> tokenMap) {

		tokenMap.put("CRPID", response.xmlPath()
				.get("Envelope.Body.queryCreditCheckResponse.creditCheckResponse.creditRiskProfile.identifier"));
		return tokenMap;
	}

	private Map<String, String> getTokenMapForProductsFamily(Response response, Map<String, String> tokenMap)
			throws Exception {
		String sku = tokenMap.get("sku");
		JsonNode jsonNode = getParentNodeFromResponse(response);

		tokenMap = findAvailableSkuInFamilyProducts(jsonNode, sku, tokenMap);

		return tokenMap;
	}

	private Map<String, String> findAvailableSkuInFamilyProducts(JsonNode jsonNode, String skuFromTestData,
			Map<String, String> tokenMap) throws Exception {

		JsonNode availableDeviceInfoNode = null;
		boolean tokenMapUpdated = false;
		// String outOfStock = "OUT OF STOCK";
		String outOfStock = "AVAILABLE";
		int jVal = -1;
		if (!jsonNode.isMissingNode() && jsonNode.has("deviceFamilyInfo")) {
			JsonNode deviceFamilyInfoPathNode = jsonNode.path("deviceFamilyInfo");
			JsonNode parentNode = deviceFamilyInfoPathNode.path("data");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					JsonNode dataNode = parentNode.get(i);
					if (dataNode.isObject()) {
						JsonNode devicesInfoPath = dataNode.path("devicesInfo");
						if (!devicesInfoPath.isMissingNode() && devicesInfoPath.isArray()) {
							for (int j = 0; j < devicesInfoPath.size(); j++) {
								if (outOfStock.equalsIgnoreCase(devicesInfoPath.get(j).get("productStatus").asText())) {
									/*if (StringUtils.isNoneEmpty(skuFromTestData)) {
										if (skuFromTestData
												.equalsIgnoreCase(devicesInfoPath.get(j).get("skuNumber").asText())) {
											jVal = j;
											tokenMap = prepareTokenMapWithAvailableDevice(devicesInfoPath, tokenMap,
													jVal);
											tokenMapUpdated = true;
											return tokenMap;
											// break;
										}
									}*/
									jVal = j;
									availableDeviceInfoNode = devicesInfoPath;
									tokenMap = prepareTokenMapWithAvailableDevice(availableDeviceInfoNode, tokenMap, jVal);
									tokenMapUpdated = true;
									return tokenMap;
								}
							}
						}
					}
				}
				if (!tokenMapUpdated) {
					throw new Exception("No Valid SKU found in Products to proceed for Cart Creation");
				}
			}
		} else {
			throw new InvalidServiceResultException("No deviceFamilyInfo in Service Response",
					new Exception("No deviceFamilyInfo in Service Response"));
		}

		return tokenMap;

	}

	private Map<String, String> prepareTokenMapWithAvailableDevice(JsonNode devicesInfoPath,
			Map<String, String> tokenMap, int jVal) {

		tokenMap.put("sku", getPathVal(devicesInfoPath.get(jVal), "skuNumber"));
		tokenMap.put("color", getPathVal(devicesInfoPath.get(jVal), "color"));
		tokenMap.put("deviceId", getPathVal(devicesInfoPath.get(jVal), "deviceId"));
		tokenMap.put("colorSwatch", getPathVal(devicesInfoPath.get(jVal), "swatch"));
		tokenMap.put("description", getPathVal(devicesInfoPath.get(jVal), "description"));
		tokenMap.put("memory", getPathVal(devicesInfoPath.get(jVal), "memory"));
		tokenMap.put("memoryUom", getPathVal(devicesInfoPath.get(jVal), "memoryUOM"));
		tokenMap.put("availabilityStatus",
				getPathVal(devicesInfoPath.get(jVal), "productAvailability.availabilityStatus"));
		tokenMap.put("estimatedShipDateTo",
				getPathVal(devicesInfoPath.get(jVal), "productAvailability.estimatedShipDateTo"));
		tokenMap.put("estimatedShipDateFrom",
				getPathVal(devicesInfoPath.get(jVal), "productAvailability.estimatedShipDateFrom"));
		tokenMap.put("fullRetailPrice", getPathVal(devicesInfoPath.get(jVal), "pricing.offerPrice"));
		tokenMap.put("imageUrl", getPathVal(devicesInfoPath.get(jVal), "images[0].url"));
		tokenMap.put("familyId", getPathVal(devicesInfoPath.get(jVal), "family"));
		// tokenMap.put("manufacturerType", getPathVal(devicesInfoPath.get(j),
		// "manufacturerType"));
		tokenMap.put("modelName", getPathVal(devicesInfoPath.get(jVal), "modelName"));
		/*
		 * tokenMap.put("payNowAmount", getPathVal(devicesInfoPath.get(jVal),
		 * "pricing.loans.financeDetails[0].eipInfo.dueTodayAmount.value"));
		 * tokenMap.put("monthlyAmount", getPathVal(devicesInfoPath.get(jVal),
		 * "pricing.loans.financeDetails[0].eipInfo.financeAmount.value"));
		 * tokenMap.put("monthlynoOfMonths",
		 * getPathVal(devicesInfoPath.get(jVal),
		 * "pricing.loans.configuredTerm"));
		 */
		tokenMap.put("payNowAmount", "125.0");
		tokenMap.put("monthlyAmount", "31.25");
		tokenMap.put("monthlynoOfMonths", "24");
		return tokenMap;
	}

	private Map<String, String> getTokenMapForCreateCartAPI(Response response, Map<String, String> tokenMap) {

		JsonNode jsonNode = getParentNodeFromResponse(response);

		if (!jsonNode.isMissingNode()) {
			tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
		}

		return tokenMap;
	}

	private Map<String, String> getTokenMapForCreateQuoteAPI(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {

			tokenMap.put("orderId", getPathVal(jsonNode, "orderId"));
			tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));

			Double totaltax = Double.parseDouble(getPathVal(jsonNode, "taxCollection.totalTax"));
			if (StringUtils.isBlank(tokenMap.get("payNowAmount"))) {
				exceptionMsgs.put(ShopConstants.ERROR_MSG,
						"Seems like a Data issue. Please check Product catalog service Response");
				throw new IllegalArgumentException(
						"Cannot proceed due to Empty payNowAmount from product catalog service Response");
			}
			Double payNowAmount = Double.parseDouble(tokenMap.get("payNowAmount"));
			Double chargeAmount = totaltax + 6.99 + payNowAmount;
			DecimalFormat df = new DecimalFormat("###.##");

			tokenMap.put("chargeAmount", df.format(chargeAmount));

			tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
			tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			tokenMap.put("addressLine1", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressLine1"));
			tokenMap.put("cityName", getPathVal(jsonNode, "shippingDetails[0].shipTo.cityName"));
			tokenMap.put("stateCode", getPathVal(jsonNode, "shippingDetails[0].shipTo.stateCode"));
			tokenMap.put("zip", getPathVal(jsonNode, "shippingDetails[0].shipTo.zip"));
			tokenMap.put("countryCode", getPathVal(jsonNode, "shippingDetails[0].shipTo.countryCode"));
			tokenMap.put("tax", getPathVal(jsonNode, "taxCollection.totalTax"));
			tokenMap.put("firstName", getPathVal(jsonNode, "shippingDetails[0].shipTo.firstName"));
			tokenMap.put("familyName", getPathVal(jsonNode, "shippingDetails[0].shipTo.familyName"));
		}
		return tokenMap;
	}

	private Map<String, String> getTokenMapForUpdateQuoteAPI(Response response, Map<String, String> tokenMap) {
		// TODO Auto-generated method stub
		return tokenMap;
	}

	private Map<String, String> getTokenMapForUpdatePayment(Response response, Map<String, String> tokenMap) {
		// TODO Auto-generated method stub
		return tokenMap;
	}

	private Map<String, String> getTokenMapForOrderAPI(Response response, Map<String, String> tokenMap) {
		// TODO Auto-generated method stub
		return tokenMap;
	}

	public void invokeFilterApi(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		// TODO Auto-generated method stub

		String operationName = "testCatalogFilterPhoneManuFacturerV4";
		// CatalogApiV3 catalogapiv3 = new CatalogApiV3();
		CatalogApiV4 catalogapiv4 = new CatalogApiV4();
		// Response response =catalogapiv3.getFilterOptionsUsingGET(apiTestData,
		// "PHONES", "" , tokenMap);
		// Response response =catalogapiv4.getFilterOptionsUsingGET(apiTestData,
		// "PHONES", "" , tokenMap);
		Response response = catalogapiv4.getFilterOptionsUsingGET(apiTestData, "PHONES", "", tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log("Response Status:" + response.statusCode());
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				// tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {

			failAndLogResponse(response, operationName);
		}

	}

	public Map<String, String> invokeCreateCart(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap, CartApiV5 cartApi, String version) throws Exception {

		updatedRequest = prepareRequestParam(requestBody, tokenMap);

		if ("v5".equalsIgnoreCase(version)) {
			response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);
		} else if ("v4".equalsIgnoreCase(version)) {
			CartApiV4 cartApiV4 = new CartApiV4();
			response = cartApiV4.createCart(apiTestData, updatedRequest, tokenMap);
		}

		logRequest(updatedRequest, "createCartAPI");
		// response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "createCartAPI");
			tokenMap = prepareTokenMap("createCartAPI", response, tokenMap);
		} else {
			failAndLogResponse(response, "createCartAPI");
		}
		return tokenMap;
	}

	public Map<String, String> invokeUpdateCart(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap, CartApiV5 cartApi, String version) throws Exception {

		updatedRequest = prepareRequestParam(requestBody, tokenMap);

		if ("v5".equalsIgnoreCase(version)) {
			response = cartApi.updateCart(apiTestData, updatedRequest, tokenMap);
		} else if ("v4".equalsIgnoreCase(version)) {
			CartApiV4 cartApiV4 = new CartApiV4();
			response = cartApiV4.updateCart(apiTestData, updatedRequest, tokenMap);
		}

		logRequest(updatedRequest, "updateCartAPI");
		// response = cartApi.updateCart(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "updateCartAPI");
			tokenMap = prepareTokenMap("updateCartAPI", response, tokenMap);
		} else {
			failAndLogResponse(response, "updateCartAPI");
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetSOCForSale(ApiTestData apiTestData, Map<String, String> tokenMap,
			String version) throws Exception {

		requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_MONITORS + "getSOCforSale_req_forMonitor.txt");
		updatedRequest = prepareRequestParam(requestBody, tokenMap);

		if ("v2".equalsIgnoreCase(version)) {
			ServicesApiV2 serviceApiV2 = new ServicesApiV2();
			response = serviceApiV2.getServicesForSale(apiTestData, updatedRequest);
		} else if ("v3".equalsIgnoreCase(version)) {
			ServicesApiV3 serviceApiV3 = new ServicesApiV3();
			response = serviceApiV3.getServicesForSale(apiTestData, updatedRequest, tokenMap);
		}

		logRequest(updatedRequest, "getSOCForSale");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getSOCForSale");
			if (StringUtils.isBlank(response.body().asString()) || "{}".equalsIgnoreCase(response.body().asString())) {
				throw new Exception("getSOCForSale Response is Empty :" + response.body().asString());
			}
			tokenMap = prepareTokenMap("getSOCForSale", response, tokenMap);
		} else {
			failAndLogResponse(response, "getSOCForSale");
		}

		return tokenMap;
	}

	public Map<String, String> invokeQueryCreditCheck(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		QueryCreditCheck creditCheck = new QueryCreditCheck();
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "getCRP_XMLRequest.txt");
		logXMLRequest(requestBody, "QueryCreditCheck");

		response = creditCheck.getCRP(apiTestData, requestBody, tokenMap);
		logLargeResponse(response, "QueryCreditCheck");
		tokenMap = prepareTokenMap("getCRP", response, tokenMap);
		return tokenMap;
	}

	public Map<String, String> invokeOrder(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "OrderAPI");
		OrderApi createOrder = new OrderApi();
		response = createOrder.createOrder(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "OrderAPI");
		} else {
			failAndLogResponse(response, "OrderAPI");
		}
		return tokenMap;
	}

	public Map<String, String> invokeUpdatePayment(ApiTestData apiTestData, QuoteApi quoteApi,
			Map<String, String> tokenMap, String version) throws Exception {
		String encryptedCardNum = getEncryptedCardNumber(apiTestData, tokenMap);
		String lastFourDigits = apiTestData.getCardNumber().substring(apiTestData.getCardNumber().length() - 4);
		tokenMap.put("encryptedCardNum", encryptedCardNum);
		tokenMap.put("lastFourDigits", lastFourDigits);
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "updatePayment_req.txt");
		updatedRequest = prepareRequestParam(requestBody, tokenMap);

		if ("v3".equalsIgnoreCase(version)) {
			response = quoteApi.updatePayment(apiTestData, updatedRequest, tokenMap);
		} else if ("v5".equalsIgnoreCase(version)) {
			QuoteApiV5 quoteV5 = new QuoteApiV5();
			response = quoteV5.updatePayment(apiTestData, updatedRequest, tokenMap);
		}

		logRequest(updatedRequest, "UpdatePayment");
		// response = quoteApi.updatePayment(apiTestData, updatedRequest,
		// tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "UpdatePayment");
		} else {
			failAndLogResponse(response, "UpdatePayment");
		}
		return tokenMap;
	}

	public Map<String, String> invokeUpdateQuote(ApiTestData apiTestData, String requestBody, QuoteApi quoteApi,
			Map<String, String> tokenMap, String version) throws Exception {
		updatedRequest = prepareRequestParam(requestBody, tokenMap);

		if ("v3".equalsIgnoreCase(version)) {
			response = quoteApi.updateQuote(apiTestData, updatedRequest, tokenMap);
		} else if ("v5".equalsIgnoreCase(version)) {
			QuoteApiV5 quoteV5 = new QuoteApiV5();
			response = quoteV5.updateQuote(apiTestData, updatedRequest, tokenMap);
		}

		logRequest(updatedRequest, "UpdateQuoteAPI");
		// response = quoteApi.updateQuote(apiTestData, updatedRequest,
		// tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			// nothing to write in token map
			logSuccessResponse(response, "UpdateQuoteAPI");
		} else {
			failAndLogResponse(response, "UpdateQuoteAPI");
		}
		return tokenMap;
	}

	public Map<String, String> invokeUpdateAddress(ApiTestData apiTestData, String requestBody, QuoteApi quoteApi,
			Map<String, String> tokenMap, String version) throws Exception {
		updatedRequest = prepareRequestParam(requestBody, tokenMap);

		if ("v3".equalsIgnoreCase(version)) {
			response = quoteApi.updateQuote(apiTestData, updatedRequest, tokenMap);
		} else if ("v5".equalsIgnoreCase(version)) {
			QuoteApiV5 quoteV5 = new QuoteApiV5();
			response = quoteV5.updateAddress(apiTestData,
					ShopConstants.SHOP_QUOTE + "updateQuoteAddressV5.txt", tokenMap);
		}

		logRequest(updatedRequest, "UpdateQuoteShippingAddressAPI");
		// response = quoteApi.updateQuote(apiTestData, updatedRequest,
		// tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			// nothing to write in token map
			logSuccessResponse(response, "UpdateQuoteShippingAddressAPI");
		} else {
			failAndLogResponse(response, "UpdateQuoteShippingAddressAPI");
		}
		return tokenMap;
	}

	public Map<String, String> invokeCreateQuote(ApiTestData apiTestData, QuoteApi quoteApi,
			Map<String, String> tokenMap, String version) throws Exception {

		// updatedRequest = prepareRequestParam(requestBody, tokenMap);

		if ("v3".equalsIgnoreCase(version)) {
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "CreateQuote_req.txt");
			updatedRequest = prepareRequestParam(requestBody, tokenMap);
			response = quoteApi.createQuote(apiTestData, updatedRequest, tokenMap);
		} else if ("v5".equalsIgnoreCase(version)) {
			QuoteApiV5 quoteV5 = new QuoteApiV5();
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "CreateQuote_V5.txt");
			updatedRequest = prepareRequestParam(requestBody, tokenMap);
			response = quoteV5.createQuote(apiTestData, updatedRequest, tokenMap);
		}

		logRequest(updatedRequest, "CreateQuoteAPI");
		// response = quoteApi.createQuote(apiTestData, updatedRequest,
		// tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "CreateQuoteAPI");
			tokenMap = prepareTokenMap("CreateQuoteAPI", response, tokenMap);
		} else {
			failAndLogResponse(response, "CreateQuoteAPI");
		}
		return tokenMap;
	}

	public Map<String, String> invokeFamilyProducts(ApiTestData apiTestData, Map<String, String> tokenMap,
			String version) throws Exception {
		if ("v3".equalsIgnoreCase(version)) {
			CatalogApiV3 catalogApi = new CatalogApiV3();
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "familyIdProductPostAPI_EIP.txt");
			response = catalogApi.retrieveGroupedProductsUsingPOST(apiTestData, requestBody, tokenMap);
		} else if ("v4".equalsIgnoreCase(version)) {
			CatalogApiV4 catalogApi = new CatalogApiV4();
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "familyIdProductPostAPI_EIP_v4.txt");
			response = catalogApi.retrieveGroupedProductsUsingPOST(apiTestData, requestBody, tokenMap);
		}
		// requestBody = new
		// ServiceTest().getRequestFromFile("familyIdProductPostAPI_EIP.txt");
		logRequest(requestBody, "FamilyProducts");
		// response = catalogApiV3.retrieveGroupedProductsUsingPOST(apiTestData,
		// requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			if (StringUtils.isBlank(response.body().asString()) || "{}".equalsIgnoreCase(response.body().asString())) {
				throw new Exception("FamilyProducts Response is Empty :" + response.body().asString());
			}
			logLargeResponse(response, "FamilyProducts");
			tokenMap = prepareTokenMap("retrieveGroupedProductsUsingPOST", response, tokenMap);
		} else {
			failAndLogResponse(response, "FamilyProducts");
		}
		return tokenMap;
	}

	public Map<String, String> invokeCatalogForProducts(ApiTestData apiTestData, Map<String, String> tokenMap,
			String version) {
		boolean emptyResponseFlag = false;
		try {
			CatalogApiV4 catalogApi = new CatalogApiV4();
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "familyIdProducts_BYOD_SimStarterKit.txt");
			// requestBody = new
			// ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS+"familyIdProducts_BYOD_SimStarterKit.txt");
			updatedRequest = prepareRequestParam(requestBody, tokenMap);
			response = catalogApi.retrieveGroupedProductsUsingPOST(apiTestData, updatedRequest, tokenMap);
			logLargeRequest(updatedRequest, "FamilyProducts");
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				if (StringUtils.isBlank(response.body().asString())
						|| "{}".equalsIgnoreCase(response.body().asString())) {
					emptyResponseFlag = true;
					throw new Exception("FamilyProducts Response is Empty :" + response.body().asString());
				}
				logLargeResponse(response, "FamilyProducts");
				tokenMap = prepareTokenMap("retrieveGroupedProductsUsingPOST", response, tokenMap);
			} else {
				failAndLogResponse(response, "FamilyProducts");
			}
		} catch (Exception e) {
			if (emptyResponseFlag) {
				throw new InvalidServiceResultException(
						"FamilyProducts Response is Empty :" + response.body().asString(), e);
			}
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetAccessories(ApiTestData apiTestData, Map<String, String> tokenMap,
			String version) throws Exception {
		if ("v3".equalsIgnoreCase(version)) {

			String requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "getAccessoryListForMonitors.txt");
			updatedRequest = prepareRequestParam(requestBody, tokenMap);
			logRequest(updatedRequest, "getAccessories");
			CatalogApiV3 catalogApiV3 = new CatalogApiV3();
			response = catalogApiV3.retrieveProductsUsingPOST(apiTestData, updatedRequest, tokenMap);

		} else if ("v4".equalsIgnoreCase(version)) {

			String requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "getAccessoryListV4ForMonitors.txt");
			updatedRequest = prepareRequestParam(requestBody, tokenMap);
			logRequest(updatedRequest, "getAccessories");
			CatalogApiV4 catalogApiV4 = new CatalogApiV4();
			response = catalogApiV4.retrieveProductsUsingPOST(apiTestData, updatedRequest, tokenMap);
		}
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			if (response.getBody().asString().equals("{ }")) {
				failAndLogResponse(response, "getAccessories");
				throw new Exception("Empty Response from getAccessories");
			}
			logSuccessResponse(response, "getAccessories");
			tokenMap = prepareTokenMap("getAccessories", response, tokenMap);
		} else {
			failAndLogResponse(response, "getAccessories");
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetAccountDevicesEligibleForTradeIn(ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_MONITORS + "getAccountDeviceListForTradeinAPI.txt");
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "getAccountDevicesEligibleForTradeIn");
		DeviceTradeInApiV3 deviceTradeInApi = new DeviceTradeInApiV3();
		response = deviceTradeInApi.getAccountDeviceListEligibleForTradeIn(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getAccountDevicesEligibleForTradeIn");
			tokenMap = prepareTokenMap("getAccountDevicesEligibleForTradeIn", response, tokenMap);
		} else {
			failAndLogResponse(response, "getAccountDevicesEligibleForTradeIn");
		}
		return tokenMap;

	}

	public Map<String, String> invokeTradeIn(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_MONITORS + "tradeIn_req_eip_monitors.txt");
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "createQuoteTradeIn");
		DeviceTradeInApiV3 deviceTradeInApi = new DeviceTradeInApiV3();
		response = deviceTradeInApi.createQuoteTradeIn(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "createQuoteTradeIn");
			tokenMap = prepareTokenMap("createQuoteTradeIn", response, tokenMap);
		} else {
			failAndLogResponse(response, "createQuoteTradeIn");
		}

		return tokenMap;
	}

	public void checkEmptyValuesInTokenMap(Map<String, String> tokenMap) throws Exception {
		Iterator<Map.Entry<String, String>> itr = tokenMap.entrySet().iterator();
		StringBuilder exceptionMessage = new StringBuilder();
		exceptionMessage = new StringBuilder();
		if (StringUtils.isNoneEmpty(tokenMap.get("exceptionMessage"))) {
			exceptionMessage.append(tokenMap.get("exceptionMessage"));
		}
		while (itr.hasNext()) {
			Map.Entry<String, String> curr = itr.next();
			if (StringUtils.isEmpty(curr.getValue())) {
				exceptionMessage = exceptionMessage.append(" " + curr.getKey() + " is Empty " + "\n");
			}
		}
		if (StringUtils.isNoneEmpty(exceptionMessage)) {
			throw new Exception(exceptionMessage.toString());
		}

	}

	public void setBanMsisdnFromFilter(ApiTestData apiTestData) throws Exception {
		String misdinpwd = getMsisdnAndPaswordFromFilter(apiTestData);
		if (misdinpwd == null) {
			Assert.fail("No valid data, Msisdn and Password identified in Test Data Filter");
		}
		String misdin = misdinpwd.split("/")[0];
		String pwd = misdinpwd.split("/")[1];
		apiTestData.setMsisdn(misdin);
		apiTestData.setPassword(pwd);
		System.out.println("Msisdn From Filter : " + apiTestData.getMsisdn());
		System.out.println("Password from Filter : " + apiTestData.getPassword());
	}

	public void standardUpgradeFRPFlow(ApiTestData apiTestData) {			
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Standard Upgrade Flow FRP</b> ");

			// invokeClearCounters(apiTestData);
			// setBanMsisdnFromFilter(apiTestData);

			// 1. Invoke getCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke FilterApi

			// invokeFilterApi(apiTestData,tokenMap);

			// 2. Invoke familyIdProductPostAPI_FRP
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));

			// checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke createCartAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_FRP_Order.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 4. Invoke CreateQuoteAPI FRP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 5. Invoke UpdateQuoteAPI FRP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_FRP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 6. Invoke UpdatePayment FRP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 7. Invoke OrderAPI FRP
			/*
			 * requestBody = new
			 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_ORDER+
			 * "order_FRP_req.txt"); invokeOrder(apiTestData, requestBody,
			 * tokenMap);
			 */

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void standardUpgradeEIPFlow(ApiTestData apiTestData) {
		try {

			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getMsisdn()+"@yopmail.com");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Standard Upgrade Flow EIP</b> ");

			// invokeClearCounters(apiTestData);

			// 1. Invoke GetCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));

			// 3. Invoke createCartAPI EIP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. Invoke CreateQuoteAPI EIP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// 5. Invoke UpdateQuoteAPI EIP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_EIP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// 6. Invoke UpdateAddress with Update Quote
			/*requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "updateQuoteAddressV5.txt");
			invokeUpdateAddress(apiTestData, requestBody, quoteApi, tokenMap, "v5");*/

			// 6. Invoke UpdatePayment EIP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// 7. Invoke OrderAPI EIP

			/*
			 * requestBody = new
			 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_ORDER+
			 * "order_EIP_req.txt"); invokeOrder(apiTestData, requestBody,
			 * tokenMap);
			 */

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void standardUpgradeAddSocsEIPFlow(ApiTestData apiTestData) {
		try {

			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Standard Upgrade Flow EIP</b> ");

			// invokeClearCounters(apiTestData);

			// 1. Invoke GetCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));

			// 3. Invoke createCartAPI EIP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. Invoke GetSocForSale
			invokeGetSOCForSale(apiTestData, tokenMap, "v3");

			// 5. invoke Update Cart
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_PHPSOC_EIP_req.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 6. Invoke CreateQuoteAPI EIP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// 7. Invoke UpdateQuoteAPI EIP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_EIP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// 8. Invoke UpdatePayment EIP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// 9. Invoke OrderAPI EIP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_EIP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void standardUpgradeAddAccessoriesEIPFlow(ApiTestData apiTestData) {
		try {

			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Standard Upgrade Flow EIP</b> ");

			// invokeClearCounters(apiTestData);

			// 1. Invoke GetCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));

			// 3. Invoke createCartAPI EIP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. invoke getAccessories
			invokeGetAccessories(apiTestData, tokenMap, "v4");

			// checkEmptyValuesInTokenMap(tokenMap);
			// 5. invoke Update Cart
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_Accessories_EIP_req.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 6. Invoke CreateQuoteAPI EIP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// 7. Invoke UpdateQuoteAPI EIP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_EIP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// 8. Invoke UpdatePayment EIP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// 9. Invoke OrderAPI EIP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_EIP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void standardUpgradeTradeInEIPFlow(ApiTestData apiTestData) {
		try {

			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getMsisdn()+"@yopmail.com");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Standard Upgrade Flow EIP</b> ");

			// invokeClearCounters(apiTestData);

			// 1. Invoke GetCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));

			// 3. Invoke createCartAPI EIP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. invoke account Devices for TradeIn

			// invokeGetAccountDevicesEligibleForTradeIn(apiTestData, tokenMap);

			// 5. invoke Trade-In

			invokeTradeIn(apiTestData, tokenMap);

			// 6. invoke Update Cart Trade-In
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "updateCartDeviceTradeIn_Monitors.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 7. Invoke CreateQuoteAPI EIP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// 8. Invoke UpdateQuoteAPI EIP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_EIP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// 9. Invoke UpdatePayment EIP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// 10. Invoke OrderAPI EIP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_EIP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void aalPhoneOnEIPNoPDPSOC(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			tokenMap.put("addaline", "ADDALINE");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>AAL Phone on EIP no PDP(soc) [AAL + NBYOD +  EIP]</b> ");

			// invokeClearCounters(apiTestData);

			// 1. Invoke getCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI_FRP
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));
			// checkEmptyValuesInTokenMap(tokenMap);
			
			
			//invokeTradeInPromotions(apiTestData, requestBody, tokenMap);
			

			// 3. Invoke createCartAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAAL_BYOD_EIP.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// fetch eligible plans

			invokeGetEligiblePlans(apiTestData, requestBody, tokenMap);

			// 4. Update Cart for Plans
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Plans.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 5. Invoke CreateQuoteAPI FRP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// 5. Invoke UpdateQuoteAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "updateQuoteEIP_AALMonitors.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// 6. Invoke UpdatePayment FRP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// 7. Invoke OrderAPI FRP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_FRP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}


	public void invokeGetEligiblePlans(ApiTestData apiTestData, String updatedRequest, Map<String, String> tokenMap) {
		try {
			ServicesApiV3 serviceApiV3 = new ServicesApiV3();
			Response response = serviceApiV3.getServicesForPlan(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				if (StringUtils.isBlank(response.body().asString())
						|| "{}".equalsIgnoreCase(response.body().asString())) {
					logLargeResponse(response, "GetEligiblePlans");
					throw new Exception("GetEligiblePlans Response is Empty :" + response.body().asString());
				}
				logLargeResponse(response, "GetEligiblePlans");
				tokenMap = prepareTokenMap("getEligiblePlans", response, tokenMap);
			} else {
				failAndLogResponse(response, "GetEligiblePlans");
			}

		} catch (Exception e) {

		}

	}
	
	public void invokeTradeInPromotions(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) {
		try {
			PromotionApiV2 promoApiV2 = new PromotionApiV2();
			String expSkuVal = apiTestData.getSku();
			String operationName="searchPromotions";
			requestBody = "{ \"shopper\": { \"salesChannel\": \"WEB\", \"accountType\": \"I\", \"accountSubType\": \"R\", \"crpid\": \"CRP6\" }, \"products\": [ { \"productType\": \"DEVICE\", \"sku\": [ \""+expSkuVal+"\" ] } ], \"promotions\": { \"transactionType\": \"UPGRADE\" } }";

			 response = promoApiV2.search_promotions(apiTestData, requestBody, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				if (StringUtils.isBlank(response.body().asString())
						|| "{}".equalsIgnoreCase(response.body().asString())) {
					logLargeResponse(response, operationName);
					throw new Exception(operationName+" Response is Empty :" + response.body().asString());
				}
				logLargeResponse(response, operationName);
				tokenMap = prepareTokenMap(operationName, response, tokenMap);
			} else {
				failAndLogResponse(response, operationName);
			}

		} catch (Exception e) {

		}
		
	}


	public void aal_FRP_PDP_NY_WithDeposit(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			tokenMap.put("addaline", "ADDALINE");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log(
					"<b>Add A Line, one FRP phone + PDP NY (includes SIM) (no credit customer) with deposit’</b> [AAL + NBYOD +  FRP] ");

			// invokeClearCounters(apiTestData);

			// 1. Invoke getCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI_FRP
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));
			// checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke createCartAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAAL_NBYOD_FRP.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 4. Update Cart for Plans
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Plans.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// GetSoc

			// invokeGetSOCForSale(apiTestData, tokenMap, "v3");

			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Services.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. Invoke CreateQuoteAPI FRP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 5. Invoke UpdateQuoteAPI FRP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_FRP_V5.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 6. Invoke UpdatePayment FRP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 7. Invoke OrderAPI FRP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_FRP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void aal_EIP_PDP_WithSecurityDeposit(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			tokenMap.put("addaline", "ADDALINE");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>AAL Phone on EIP no PDP(soc) [AAL + NBYOD +  EIP]</b> ");

			// invokeClearCounters(apiTestData);

			// 1. Invoke getCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI_FRP
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));

			// checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke createCartAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAAL_BYOD_EIP.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. Update Cart for Plans
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Plans.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// GetSoc

			// invokeGetSOCForSale(apiTestData, tokenMap, "v3");

			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Services.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 5. Invoke CreateQuoteAPI FRP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// 5. Invoke UpdateQuoteAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "updateQuoteEIP_AALMonitors.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// 6. Invoke UpdatePayment FRP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// 7. Invoke OrderAPI FRP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_FRP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void AAL_BYOD_SIMKIT(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			tokenMap.put("addaline", "ADDALINE");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>AAL BYOD SIM Kit: use my phone</b> ");

			// 1. Invoke getCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI_FRP
			invokeFamilyProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));
			// checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke createCartAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAAL_BYOD_FRP.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// update cart for Plans
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Plans.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// update cart for sim

			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_sim.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. Invoke CreateQuoteAPI FRP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 5. Invoke UpdateQuoteAPI FRP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_FRP_V5.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 6. Invoke UpdatePayment FRP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 7. Invoke OrderAPI FRP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_FRP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	public void BYODFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			// tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>BYOD Flow</b> ");

			getJWTToken(apiTestData, tokenMap);

			// 1. Invoke getCRP (QueryCreditCheck)
			// invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke FilterApi

			// invokeFilterApi(apiTestData,tokenMap);

			// 2. Invoke familyIdProductPostAPI_FRP
			invokeCatalogForProducts(apiTestData, tokenMap, "v4");
			tokenMap.put("email", tokenMapCommon.get("emailVal"));

			// checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke createCartAPI FRP
			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_BYOD_For_Monitors.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_BYOD_Monitors.txt");
			invokeUpdateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 4. Invoke CreateQuoteAPI FRP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 5. Invoke UpdateQuoteAPI FRP
			requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_FRP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 6. Invoke UpdatePayment FRP
			// invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// checkEmptyValuesInTokenMap(tokenMap);

			// 7. Invoke OrderAPI FRP
			// requestBody = new
			// ServiceTest().getRequestFromFile("order_FRP_req.txt");
			// invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re + " " + exceptionMsgs.get(ShopConstants.ERROR_MSG));
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
			Assert.fail("<b>Exception Response :</b> " + e);
		}
	}

	public void getJWTToken(ApiTestData apiTestData, Map<String, String> tokenMap) {

		Map<String, String> jwtTokenMap = new HashMap<String, String>();

		try {
			jwtTokenMap = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
			if (jwtTokenMap.isEmpty()) {
				throw new Exception("Empty Jwt Token Map.");
			}

			if (StringUtils.isNoneEmpty(jwtTokenMap.get("jwtToken"))) {
				tokenMap.put("jwtToken", jwtTokenMap.get("jwtToken"));
				tokenMap.put("accessToken", jwtTokenMap.get("accessToken"));
				tokenMap.put("emailVal", jwtTokenMap.get("emailVal"));
				tokenMap.put("code", jwtTokenMap.get("code"));
			}

		} catch (Exception e) {
			if (StringUtils.isEmpty(jwtTokenMap.get("jwtToken"))) {
				throw new InvalidAuthTokenException("Could not fetch JWT Token.", e);

			}
		}

	}
}
