package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class FAQPage extends CommonPage {
	
	@FindBy(css = "autopay-faq")
	private WebElement faqModal;
	
	@FindBy(css = "autopay-faq h4")
	private WebElement faQHeader;
	
	@FindBy(css = "autopay-faq button")
	public WebElement faqBackBtn;
	
	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	@FindBy(css = "i.fa.fa-spin.fa-spinner.interceptor-spinner")
	private WebElement pageSpinner;

	@FindBy(css = ".btn.btn-secondary.SecondaryCTA")
	private WebElement pageLoadElement;

	private final String pageUrl = "/faq";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public FAQPage(WebDriver webDriver) {
		super(webDriver);
	}
	/**
     * Verify that current page URL matches the expected URL.
     */
    public FAQPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public FAQPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
    		waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
	    	waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("FAQ page not loaded");
    	}
        return this;
    }
    
    /**
	 * validate Autopay FAQ modal
	 */
	public FAQPage validateFAQPage() {
	    try{
            Assert.assertTrue(faqModal.isDisplayed(), "Auto Pay FAQ modal is not displayed");
            Assert.assertTrue(faQHeader.getText().equals("Frequently Asked Questions"),
                    "Auto Pay FAQ header is not displayed");
            Assert.assertTrue(faqBackBtn.isDisplayed(), "Back button is not displayed on FAQ modal");
            Reporter.log("Auto Pay FAQ page successfully validated");
        }catch (Exception e){
	        Assert.fail("Fail to Validate FAQ Model");
        }
		return this;
	}
	
	/***
	 * Click FAQ button
	 * @return
	 */

	public FAQPage clickFAQBackButton() {
	    try{
            waitFor(ExpectedConditions.elementToBeClickable(faqBackBtn));
            clickElementWithJavaScript(faqBackBtn);
            waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
            Reporter.log("Clicked on FQA Back Button");
        }catch (Exception e){
	        Assert.fail("Fail to Click FAQ Back Button");
        }
	 return this;
	}

}
