package com.tmobile.eservices.qa.pages.tmng.functional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

public class CheckOutPage extends TmngCommonPage {

	public CheckOutPage(WebDriver webDriver) {
		super(webDriver);
	}

	public static final String INVALID_ADDRESS_LINE_1 = "A-105";
	public static final String INVALID_CITY = "Pune";
	public static final String INVALID_STATE = "ME";
	public static final String INVALID_ZIPCODE = "41105";

	static JavascriptExecutor javaScript;

	private final String pageUrl = "/checkout";

	@FindBy(id = "userInfoHeader")
	private WebElement personalInfoText;

	@FindBy(css = "button[aria-label='Add to cart']")
	private WebElement addToCartBtn;

	@FindBy(css = "#shipState")
	private WebElement shipstate;

	@FindBy(css = "input#shipCity")
	private WebElement city;

	@FindBy(css = "input#shipZip")
	private WebElement zipCode;

	@FindBy(css = "input#shipAddress1")
	private WebElement address1;

	@FindBy(css = "input[placeholder = 'Enter shipping address']")
	private WebElement addressTooltip;

	@FindBy(css = "input#firstName")
	private WebElement firstName;

	@FindBy(css = "label[id='Up to 5 business days']")
	private WebElement shippingOptionGround;

	@FindBy(css = "[name='billDisclaimer']")
	private WebElement useSameAddressForBilling;

	@FindBy(css = "[name='e911Disclaimer']")
	private WebElement useSameAddressForE911;

	@FindBy(css = "[id='paymentHeader']")
	private WebElement paymentSectionCheckPage;

	@FindBy(css = "label[id='2 business days']")
	private WebElement shippingOptionTwoDay;

	@FindBy(css = "label[id='1 business day']")
	private WebElement shippingOptionNextDay;

	@FindBy(css = "[id='infoAndShippingfirstNameLabel']")
	private WebElement firstNameLabelText;

	@FindBy(css = "[id='infoAndShippinglastNameLabel']")
	private WebElement lastNameLabelText;

	@FindBy(css = "[id='infoAndShippingemailLabel']")
	private WebElement emailLabelText;

	@FindBy(css = "[id='infoAndShippingphoneNumberLabel']")
	private WebElement phoneNumberLabelText;

	@FindBy(css = "[id='shipAddress1Label']")
	private WebElement shippingAddressLine1LabelText;

	@FindBy(css = "[id='shipCityLabel']")
	private WebElement cityLabelText;

	@FindBy(css = "[id='shipStateLabel']")
	private WebElement stateLabelText;

	@FindBy(css = "[id='shipZipLabel']")
	private WebElement zipCodeLabelText;

	@FindBy(css = "p[aria-label='First Name is Required']")
	private WebElement firstNameErrorMessage;

	@FindBy(css = "p[aria-label*='Last Name']")
	private WebElement lastNameErrorMessage;

	@FindBy(css = "p[aria-label*='Phone number']")
	private WebElement phoneNumberErrorMessage;

	@FindBy(css = "p[aria-label*='enter the address']")
	private WebElement addressErrorMessage;

	@FindBy(css = "p[aria-label = 'Please enter the city']")
	private WebElement cityErrorMessage;

	@FindBy(css = "#residentialZipError p")
	private WebElement zipCodeErrorMessage;
	
	@FindBy(css = "div#shipZipError p")
	private WebElement zipCodeErrorMessageSub;

	@FindBy(css = "p[aria-label = 'Please enter the state']")
	private WebElement stateErrorMessage;

	@FindBy(css = "p[aria-label*='email']")
	private WebElement emailErrorMessage;

	@FindBy(css = "input[placeholder = 'Please Enter First Name']")
	private WebElement firstNameTooltip;

	@FindBy(css = "input[placeholder = 'Please Enter Last Name']")
	private WebElement lastNameTooltip;

	@FindBy(css = "input[placeholder = 'Please enter email address']")
	private WebElement emailTooltip;

	@FindBy(css = "input[placeholder = 'Enter city']")
	private WebElement cityTooltip;

	@FindBy(css = "input[placeholder = 'Enter zip code']")
	private WebElement zipCodeTooltip;

	@FindBy(css = "input[placeholder = 'Phone Number: (___) ___ - ____']")
	private WebElement phoneNumberTooltip;

	@FindBy(css = "input#infoAndShippinglastName")
	private WebElement lastName;

	@FindBy(css = "input#infoAndShippingemail")
	private WebElement email;

	@FindBy(css = "input#infoAndShippingphoneNumber")
	private WebElement phoneNumber;

	@FindBy(css = "input#infoAndShippingpersonalDisclaimer")
	private WebElement termsNConditions;

	@FindBy(css = "#infoNextButton")
	private WebElement nextBtn;

	@FindBy(css = "button#paymentCreditNextButton")
	private WebElement agreeandNextBtn;

	@FindBy(css = "#disclaimer")
	private WebElement agreeCheckBoxInReviewSubmitTab;

	@FindBy(css = "#submitOrder")
	private WebElement agreeandSubmitBtn;

	@FindBy(css = "select#state")
	private WebElement idType;

	@FindBy(css = "select#state")
	private WebElement idState;

	@FindBy(css = "[id='shipState']")
	private WebElement state;

	@FindBy(id = "ssn")
	private WebElement ssn;

	@FindBy(css = "input#dob")
	private WebElement dob;

	@FindBy(css = "#dobError")
	private WebElement dobError;

	@FindBy(css = "input#portOutPin")
	private WebElement securityPin;

	@FindBy(css = "input#expiryDate")
	private WebElement expiryDate;

	@FindBy(css = "input#idNumber")
	private WebElement idNumber;

	@FindBy(css = "input#nameOnCard")
	private WebElement nameOnCard;

	@FindBy(css = "input#creditCardNumber")
	private WebElement creditCardNumber;

	@FindBy(css = "input#cardExpiryDate")
	private WebElement cardExpiryDate;

	@FindBy(css = "input[type='submit']")
	private WebElement continueCTAOnPaymentTab;

	@FindBy(css = "input#securitycode")
	private WebElement cvv;

	/*
	 * @FindBy(id = "reviewOrderHeader") private WebElement reviewOrderHeader;
	 */

	@FindBy(id = "reviewOrderHeader")
	private WebElement reviewOrderHeader;

	@FindBy(id = "reviewOrderHeader")
	private WebElement reviewOrderTitle;

	@FindBy(css = "button[aria-label='Got it']")
	private WebElement softStopGotitBtn;

	@FindBy(css = "div#shortestEIPTermText p")
	private WebElement legalText;

	@FindBy(css = "p.priceBreakdown-whiteSpace > div")
	private WebElement eipLoanTermLength;

	@FindBy(css = "a#editInCart")
	private WebElement editInCartCTA;

	@FindBy(xpath = "//div[contains(@class,'modal-wrapper')]//span//p[contains(@class,'priceBreakdown-whiteSpace')]/div")
	private WebElement totalEIPFinanceAmount;

	@FindBy(xpath = "//span[contains(text(),'Final List Price')]/../span[2]/div")
	private WebElement frpPrice;

	@FindBy(css = "p#monthlyPrice span")
	private WebElement legalTextProductTile;

	@FindBy(xpath = "//span[contains(text(),'Financed Amount')]")
	private WebElement totalFinanceAmountText;

	@FindBy(css = "div[ng-if*='simKitDetails.listPrice']")
	private WebElement simStarterKitPrice;

	@FindBy(css = "p[id='simKitPayNowPrice'] div[class*='price-lockup-wrapper']")
	private WebElement simStarterKitPriceInReviewAndSubmitPage;

	@FindBy(css = "[id = 'shipState'] option")
	private List<WebElement> stateList;

	@FindBy(css = "div[ng-if*='sectionToBeDisplayed'] div[class*='price-lockup-wrapper']")
	private WebElement paymentEIPPrice;

	@FindBy(css = ".modal-dialog div[right-text='Monthly']")
	private List<WebElement> todayPriceAtCheckOutPage;

	@FindBy(css = "[size-class='price-lockup-large']")
	private WebElement totalPriceOfAccessoryAtCheckOutPage;

	@FindBy(id = "oneTimeFeeAmt")
	private WebElement oneTimeFeeAmt;

	@FindBy(id = "salesTaxAmt")
	private WebElement salesTaxAmt;

	@FindBy(id = "shippingFeeAmt")
	private WebElement shippingFeeAmt;

	@FindBy(css = "[ng-if*='isFRPPayment']")
	private WebElement todayPriceAmount;

	@FindBy(css = "div[class*='price-lockup'][size-class='price-lockup-large']")
	private WebElement totalTodayAmount;

	@FindBy(css = "div.border.top.right.bottom>p")
	private WebElement tradeInTile;

	@FindBy(css = ".modal-dialog span[ng-if*='autoPayDiscount']")
	private List<WebElement> autoPayTextForEachLineOrderDetailsCartPage;

	@FindBy(css = "div[ng-if*='isAutoPayOn']")
	private WebElement totalAutoPayDiscountBannerOrderDetailsCartPage;

	@FindBy(id = "progressBtn1")
	private WebElement personalInfoAndShippingTab;

	@FindBy(css = "#itemPrice [class*='price-lockup-wrapper']")
	private WebElement deviceTotalPrice;

	@FindBy(xpath = "//p[contains(@class,'priceBreakdown-whiteSpace')]/..//div[contains(@class,'price-lockup-wrapper ng-isolate-scope')]")
	private WebElement devicePrice;

	@FindBy(css = "[aria-labelledby*='0price3'] [class*='price-lockup-wrapper']")
	private WebElement refundableAmount;

	@FindBy(css = "#confirmationHeader")
	private WebElement confirmationHeader;

	@FindBy(css = "p[ng-bind-html*='pbReinforceMessageTablet']")
	private WebElement tabletLineDiscount;

	@FindBy(css = "#creditCheckHeader")
	private WebElement letsCheckYourCreditHeader;

	@FindBy(css = "#residentialAddress1")
	private WebElement addressLine1RACreditCheck;

	@FindBy(css = "#residentialCity")
	private WebElement cityRACreditCheck;

	@FindBy(css = "#residentialState")
	private WebElement stateRACreditCheck;

	@FindBy(css = "#residentialZip")
	private WebElement zipCodeRACreditCheck;

	@FindBy(css = "#dobCreditCheck")
	private WebElement DOBCreditCheck;

	@FindBy(css = "#ssnCreditCheck")
	private WebElement SSNNumberCreditCheck;

	@FindBy(css = "#idTypeCreditCheck")
	private WebElement idTypeCreditCheck;

	@FindBy(css = "#expiryDate")
	private WebElement expiryDateCreditCheck;
	
	@FindBy(css = "#stateLabel")
	private WebElement stateIdentityLable;

	@FindBy(css = "#state")
	private WebElement stateIdentityVerificationCreditCheck;

	@FindBy(css = "[class*='indicator required']")
	private WebElement creditCheckBoxRequired;

	@FindBy(css = "#creditCheckTppNextButton")
	private WebElement runCreditCheckButton;

	@FindBy(css = "#orderModal")
	private WebElement creditCheckOrderModal;

	@FindBy(css = "[ng-click*='vm.primaryCTA'][aria-label*='Check out now']")
	private WebElement checkOutNowCTA;

	@FindBy(css = "#creditCardNumberError")
	private WebElement creditCardNumberErrorMessage;

	@FindBy(css = "#expiryDateError p")
	private WebElement expirationDateErrorMessage;

	@FindBy(css = "#securityCodeError")
	private WebElement cvvErrorMessage;

	@FindBy(css = "#cardExpiryDateError")
	private WebElement expirationDateErrorMessageCC;

	@FindBy(css = "#infoNextButton")
	private WebElement nextBtnTPP;

	@FindBy(css = "#shippingInfoHeader")
	private WebElement shippingHeader;

	@FindBy(css = "span[aria-label='Accessories details']")
	private WebElement accessoriesInPricingModal;

	@FindBy(css = "[ng-click*='vm.showAccessoriesDetails = true']")
	private WebElement downcaratButtonForAccessories;

	@FindBy(css = "[ng-class*='.amountDisplay, vm.orderDetails.cartObj.accessories']")
	private List<WebElement> accessoriesPricingDisplay;

	@FindBy(css = "#dobErrorCreditCheck")
	private WebElement dobErrorMessage;

	@FindBy(css = "#ssnErrorCreditCheck p")
	private WebElement ssnErrorMessage;

	@FindBy(css = "#idNumberErrorMessage p")
	private WebElement idNumberErrorMessage;

	@FindBy(css = "#page-name-4")
	private WebElement page4Name;

	@FindBy(css = "#lineNumber")
	private WebElement lineNumber;

	@FindBy(css = "#familyName")
	private WebElement sku;

	@FindBy(css = "[ng-bind-html*='planNameDevice || item.lineItemDetails']")
	private WebElement planName;

	@FindBy(xpath = "//*[@id=\"itemPrice\"]//span[@ng-bind-html=\"leftText\"]")
	private WebElement dueToday;

	@FindBy(xpath = "//*[@id=\"itemPrice\"]//span[@ng-bind-html=\"rightText\"]")
	private WebElement dueMonthly;

	@FindBy(css = "#collapseIcon")
	private WebElement expandingCarrot;

	@FindBy(css = "li[ng-class*='priceBreakdown']")
	private WebElement planNameAndDiscount;

	@FindBy(css = "#deviceFamilyName")
	private WebElement skuName;

	@FindBy(css = "#memory")
	private WebElement memory;

	@FindBy(css = "[ng-bind-html*='deviceLegalText']")
	private WebElement legaltext;

	@FindBy(css = "#promoNameLink")
	private WebElement promoText;

	@FindBy(css = "#subTotalHeader")
	private WebElement subTotalHeaderText;

	@FindBy(css = "#oneTimeFee")
	private WebElement OneTimeFeesText;

	@FindBy(css = "#salesTax")
	private WebElement salesTaxText;

	@FindBy(css = "#shippingLabel")
	private WebElement shippingText;

	@FindBy(css = "#totalHeader")
	private WebElement total;

	@FindBy(xpath = "//*[@id=\"tmobileApp\"]//div[@class=\"cost text-magenta\"]")
	private List<WebElement> todayText;

	@FindBy(xpath = "//span[contains(text(),'Monthly')]")
	private List<WebElement> monthlyText;

	@FindBy(css = "#shippingHeader")
	private WebElement shippingReviewHeader;

	@FindBy(css = "#editShippingLink")
	private WebElement editShippingLink;

	@FindBy(css = "#fullName")
	private WebElement shippingAddressLine1;

	@FindBy(css = "#paymentLabel")
	private WebElement paymentHeader;

	@FindBy(css = "#editPayment")
	private WebElement editPaymentLink;

	@FindBy(css = "#billingCityStateZip")
	private WebElement paymentCity;

	@FindBy(css = "#billingCityStateZip")
	private WebElement checkBoxOnReviewAndSubmit;

	@FindBy(css = "[class='imgFocus']")
	private WebElement privacyImage;

	@FindBy(css = "[href*='privacypolicy']")
	private List<WebElement> privacyPolicyLink;

	@FindBy(css = "[class*='MsoNoSpacing']")
	private List<WebElement> privacyPolicyText;

	@FindBy(css = "#termsCon")
	private WebElement termsAndConditions;

	@FindBy(css = "[class*='fixed-banner-bottom'] [ng-bind-html*='stickyBanner.orderDetailsLinkTxt']")
	private WebElement viewOrderDetailsLink;

	@FindBy(css = "[style='height: auto;']")
	private WebElement carrotExpansion;

	@FindBy(css = "[aria-label*='Not Collapsed']")
	private List<WebElement> caretExpansion;

	@FindBy(css = "")
	private WebElement orderDetailsModalAuthorableHeader;

	@FindBy(css = "")
	private WebElement orderDetailsModalToday;

	@FindBy(css = "")
	private WebElement orderDetailsModalMonthly;

	@FindBy(css = "")
	private WebElement orderDetailsModalEIPTerm;

	@FindBy(css = "")
	private WebElement orderDetailsModalCreditClass;

	@FindBy(css = "#creditCheckHeader")
	private WebElement creditCheckHeader;

	@FindBy(css = "#creditCheckSubHeader")
	private WebElement creditCheckSubHeader;

	@FindBy(css = "h4[ng-bind*='vm.creditCheckAuthorValue.ssnHeader']")
	private WebElement personalInformationHeader;

	@FindBy(css = "#creditCheckTppNextButton")
	private WebElement verifyIDNextButton;

	@FindBy(css = "[ng-bind-html*='authenticationPrompt.header']")
	private WebElement existingCustomerHeader;

	@FindBy(css = "#loginBUtton")
	private WebElement loginbuttonOnExistingCustomer;

	@FindBy(css = "[class*='cross']")
	private WebElement closeIconOnExistingCustomer;

	@FindBy(css = "div[ng-if*='finalShippingDate']")
	private WebElement shippingdateFrom;

	@FindBy(css = "span[ng-if*='finalShippingDate']")
	private WebElement shippingDateTo;

	@FindBy(css = "[class*='section-1 padding-horizontal']")
	private WebElement creditCheckOrderModalHeader;

	@FindBy(xpath = "//*[@id='orderModal']//div[@class='text-magenta']//span[@class='ng-binding']")
	private WebElement totalTodayAtCreditOrderModal;

	@FindBy(xpath = "//*[@id='orderModal']//div[@class='text-gray-darker'] //div //span")
	private WebElement totalMonthlyAtCreditOrderModal;

	@FindBy(xpath = "//*[@id='orderModal']//div[@class='description-bold ng-binding']")
	private WebElement EIPTermUnderMonthly;

	@FindBy(css = "[class*='section-2 padding-horizontal']")
	private WebElement textYoullNowSeeVerifiedPricing;

	@FindBy(css = "[aria-label*='Have questions']")
	private WebElement chatWithUsCTAOnOrderModal;

	@FindBy(css = "[aria-label*='Edit your cart']")
	private WebElement editYourCartCTAOnOrderModal;

	@FindBy(css = "span[ng-bind-html*='vm.cartManualReview.header']")
	private WebElement fraudReviewModalHeader;

	@FindBy(css = "div[ng-bind-html*='vm.cartManualReview.headerText']")
	private WebElement fraudReviewModalText;

	@FindBy(css = "button[class*='btn-primary-big-order']")
	private WebElement callCta;

	@FindBy(css = "button[class*='btn-secondary-big-order']")
	private WebElement storeVisitCta;

	@FindBy(css = "div[pdl-event='modal']")
	private WebElement tppFraudModal;

	@FindBy(css = "div[class*='close-position'] span[ng-click*='vm.close()']")
	private WebElement fraudModalCloseIcon;
	
	@FindBy(css = "span[ng-click='vm.close()'] span")
	private WebElement fraudModalCloseIconMobile;

	@FindBy(css = "div[class*='border top bottom'] [class*='price-lockup-wrapper']")
	private WebElement lineTotalPrice;

	@FindBy(css = "#oneTimeFeeAmt")
	private WebElement oneTimeFees;

	@FindBy(css = "#salesTaxAmt span")
	private WebElement salesTax;

	@FindBy(css = "#shippingFeeAmt span")
	private WebElement shippingAmount;

	@FindBy(css = "[ng-click*='vm.showLineDetails']")
	private List<WebElement> caretIconOnOrderModal;

	@FindBy(css = "[aria-label*='emi']")
	private List<WebElement> monthlyPriceOnOrderModal;

	@FindBy(css = "[aria-label*='downpayment']")
	private List<WebElement> todayPriceOnOrderModal;

	@FindBy(xpath = "//*[@id='orderModal']//span[@class='line-text']")
	private List<WebElement> todayPriceOnOrderModalEmpty;

	@FindBy(css = "[ng-if*='data.lineItemDetails.planDetails.familyName']")
	private List<WebElement> planNameOnOrderModal;

	@FindBy(css = "[id*='orderModal'] div[class*='row padding'] div[class*='2'] span")
	private WebElement totalMonthlyPricingOnOrderModal;

	@FindBy(css = "#itemPrice [class*='price-lockup-wrapper']")
	private List<WebElement> lineItems;

	@FindBy(css = "#infoAndShippingmiddleName")
	private WebElement middleName;

	@FindBy(css = "p[ng-bind*='ssnMismatchError']")
	private WebElement errorSSNMismatch;

	@FindBy(css = "#simKitPayNowPrice .price-lockup-wrapper")
	private WebElement miSimKitPrice;

	@FindBy(css = "#simKitDisplayName")
	private WebElement miSimKitName;

	@FindBy(css = "a[aria-live='assertive'] img[alt='details']")
	private WebElement detailsLinkOnPricingModal;

	@FindBy(css = "div[aria-live='assertive'][ng-if*='data.simKitDetails.isBYODAdded']")
	private WebElement simNameOnPricingModal;

	@FindBy(css = "div[aria-live='assertive'][ng-if*='data.simKitDetails.isBYODAdded'] div span")
	private List<WebElement> simDetailsOnPricingModal;

	@FindBy(css = "div[id='shippingFeeAmt'] span")
	private WebElement reviewSubmitShippingPrice;

	@FindBy(css = "div[id='salesTaxAmt'] span")
	private WebElement reviewSubmitSalesTaxPrice;

	@FindBy(css = "div[id='oneTimeFeeAmt']")
	private WebElement reviewSubmitOneTimeFeesPrice;

	@FindBy(css = "p[ng-bind-html*='vm.breakdownAuthorValue']")
	private List<WebElement> reviewSubmitTodayOrMonthlyBelowPricesLabel;

	@FindBy(css = "[aria-label*='SIM Starter Kit']")
	private WebElement simStarterKitText;

	@FindBy(xpath = "//span[contains(text(),'Total')]/../../div[@class='col-xs-2']/span")
	private WebElement lineTotalPriceForPricingModal;

	@FindBy(xpath = "//a[@id='viewOrderDetailsLinkDevice']")
	private WebElement viewOrderDetailsCheckout;

	@FindBy(css = "#dialogTitle")
	private WebElement orderDetailsModal;

	@FindBy(css = "span[ng-bind='priceBreakdownCtrl.totalFinancedAmount']")
	private WebElement totalFinanceAmount;

	@FindBy(css = "div.pricing-break-modal-content > div .price-lockup-wrapper")
	private WebElement orderCheckoutEIPPrice;

	@FindBy(css = "button.close")
	private WebElement orderDetailsCloseIcon;

	@FindBy(xpath = "//span[contains(text(),'Total')]/../../div[@class='col-xs-3']/span")
	private WebElement lineTotalPriceForPricingModalToday;

	@FindBy(css = "#idTypeErrorMessage p")
	private WebElement idTypeErrorMessage;

	@FindBy(css = "[ng-class*='.amountDisplay, vm.orderDetails.cartObj.accessories'] span[class*='description ']")
	private List<WebElement> accessoriesNameDisplay;

	@FindBy(css = "[ng-class*='.amountDisplay, vm.orderDetails.cartObj.accessories'] [class*='line-text']")
	private List<WebElement> accessoriesTodayPricebreakdown;

	@FindBy(css = "span[ng-bind-html='leftText']")
	private List<WebElement> todayTextInCheckOutPage;

	@FindBy(css = "span[class*='description-bold']")
	private List<WebElement> todayMonthlyLabelInOrderModel;

	@FindBy(css = ".dialog")
	private WebElement hccResultModal;

	@FindBy(css = "#creditHeader")
	private WebElement securityPinSection;

	@FindBy(css = "#autopaydescription2")
	private WebElement autoPay;

	@FindBy(css = "p[ng-bind*='invalidAddressErrorMessage']")
	private WebElement invalidAddressMessage;

	@FindBy(css = "input[aria-describedby*='residentialAddress1Message'][class$='ng-invalid']")
	private WebElement invalidAddressForAddressLine1;

	@FindBy(css = "input[aria-describedby*='residentialCityError'][class$='ng-invalid']")
	private WebElement invalidAddressForCity;

	@FindBy(css = "select[aria-describedby*='residentialStateError'][class$='ng-invalid']")
	private WebElement invalidAddressForState;

	@FindBy(css = "input[aria-describedby*='residentialZipError'][class$='ng-invalid']")
	private WebElement invalidAddressForZipcode;

	@FindBy(css = "input[aria-describedby*='residentialAddress2Message'][class*='ng-invalid']")
	private WebElement invalidAddressForAddressLine2;

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the Lte4gNetworkPage class instance.
	 */
	public CheckOutPage verifyCheckOutPageLoaded() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			verifyCheckOutPageUrl();
			Reporter.log("Navigated to CheckOut page");
		} catch (NoSuchElementException e) {
			Assert.fail("CheckOut Page not loaded");
		}

		return this;
	}

	/**
	 * Compare total Price of Accessory in Cart and CheckOutPage
	 * 
	 */
	public CheckOutPage comparePricesOfAccessoryInCartPageAndCheckoutPage(String priceOnCart,
			String priceOnCheckOutPage) {
		try {

			Assert.assertEquals(priceOnCart, priceOnCheckOutPage,
					"Total Price of accessory is not matching in Cart Page and Checkoutpage.");
			Reporter.log("Total Price of accessory is matching in Cart Page and Checkoutpage");
		} catch (Exception e) {
			Assert.fail("Failed to compare total price of Accessory in cart page and Checkout pages");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Lte4gNetworkPage class instance.
	 */
	public CheckOutPage verifyCheckOutPageUrl() {
		try {
			waitFor(ExpectedConditions.urlContains(pageUrl), 60);
		} catch (Exception e) {
			Assert.fail("Failed to verify Checkout page URL");
		}

		return this;
	}

	/**
	 * Verify Error Message For FirstName without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForFirstName() {

		try {
			Assert.assertTrue(isElementDisplayed(firstName), "First Name is not found");
			firstName.click();
			lastName.click();
			Assert.assertTrue(isElementDisplayed(firstNameErrorMessage),
					"Error message is not displyed for First Name filed");
			Reporter.log("Verified Error Message For FirstName without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For FirstName without filling data");
		}
		return this;
	}

	/**
	 * Verify different shipping options in Checkout Page - Personal info &Shipping
	 *
	 */
	public CheckOutPage verifyShippingAddressOptions() {

		try {
			Assert.assertTrue(isElementDisplayed(shippingOptionGround), "Ground Shipping Option is not available");
			Assert.assertTrue(isElementDisplayed(shippingOptionNextDay), "Next Day Shipping Option is not available");
			Assert.assertTrue(isElementDisplayed(shippingOptionTwoDay), "TwoDay Shipping Option is not available");
			Reporter.log("Different shipping options are available - Ground, TwoDay and NextDay");
		} catch (Exception e) {
			Assert.fail("Failed to Verify different shipping options in Checkout Page - Personal info &Shipping tab");
		}
		return this;
	}

	/**
	 * Verify Use same address for billing checkBox
	 *
	 */
	public CheckOutPage verifySameForBillingAddress() {

		try {
			Assert.assertTrue(isElementDisplayed(useSameAddressForBilling),
					"Use same address for billing checkBox Option is not available");
			Reporter.log("Verified Use same address for billing checkBox option");
		} catch (Exception e) {
			Assert.fail("Failed to verify Use same address for billing checkBox");
		}
		return this;
	}

	/**
	 * Verify Use this address for E911 address checkBox
	 *
	 */
	public CheckOutPage verifyE911Address() {

		try {
			Assert.assertTrue(isElementDisplayed(useSameAddressForE911),
					"Use this address for E911 address checkBox Option is not available");
			Reporter.log("Verified Use this address for E911 address checkBox option");
		} catch (Exception e) {
			Assert.fail("Failed to verify Use this address for E911 address checkBox");
		}
		return this;
	}

	/**
	 * Verify Payment section loading
	 *
	 */
	public CheckOutPage verifyPaymentSectionloaded() {

		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(isElementDisplayed(paymentSectionCheckPage), "Payment section not loaded");
			Reporter.log("Verified Payment section in check out page");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Payment section in checkOutPage");
		}
		return this;
	}

	/**
	 * Verify label text For FirstName
	 *
	 */
	public CheckOutPage verifyLabelTextForFirstName() {

		try {
			Assert.assertTrue(isElementDisplayed(firstName), "First Name is not found");
			Assert.assertTrue(isElementDisplayed(firstNameLabelText),
					"Label Text is not displyed for First Name filed");
			Reporter.log("Verified Label TextFor FirstName without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For FirstName without filling data");
		}
		return this;
	}

	/**
	 * Verify label text For LastName
	 *
	 */
	public CheckOutPage verifyLabelTextForLastName() {

		try {
			Assert.assertTrue(isElementDisplayed(lastName), "Last Name is not found");
			Assert.assertTrue(isElementDisplayed(lastNameLabelText), "Label Text is not displyed for Last Name filed");
			Reporter.log("Verified Label Text For Last Name.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For Last name");
		}
		return this;
	}

	/**
	 * Verify label text For Email
	 *
	 */
	public CheckOutPage verifyLabelTextForEmail() {

		try {
			Assert.assertTrue(isElementDisplayed(email), "Email is not found");
			Assert.assertTrue(isElementDisplayed(emailLabelText), "Label Text is not displyed for Email filed");
			Reporter.log("Verified Label Text For Email");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For Email");
		}
		return this;
	}

	/**
	 * Verify label text For Phone Number
	 *
	 */
	public CheckOutPage verifyLabelTextForPhoneNumber() {

		try {
			Assert.assertTrue(isElementDisplayed(phoneNumber), "Phone Number is not found");
			Assert.assertTrue(isElementDisplayed(phoneNumberLabelText),
					"Label Text is not displyed for Phone Number filed");
			Reporter.log("Verified Label Text For Phone Number");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For Phone Number");
		}
		return this;
	}

	/**
	 * Verify label text For ShippingAddressLine1
	 *
	 */
	public CheckOutPage verifyLabelTextForShippingAddressLine1() {

		try {
			Assert.assertTrue(isElementDisplayed(address1), "ShippingAddressLine1 is not found");
			Assert.assertTrue(isElementDisplayed(shippingAddressLine1LabelText),
					"Label Text is not displyed for ShippingAddressLine1 filed");
			Reporter.log("Verified LabelText For ShippingAddressLine1");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For ShippingAddressLine1");
		}
		return this;
	}

	/**
	 * Verify label text For City
	 *
	 */
	public CheckOutPage verifyLabelTextForCity() {

		try {
			Assert.assertTrue(isElementDisplayed(city), "Cityis not found");
			Assert.assertTrue(isElementDisplayed(cityLabelText), "Label Text is not displyed for City filed");
			Reporter.log("Verified LabelText For City.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For City");
		}
		return this;
	}

	/**
	 * Verify label text For State
	 *
	 */
	public CheckOutPage verifyLabelTextForState() {

		try {
			Assert.assertTrue(isElementDisplayed(state), "State is not found");
			Assert.assertTrue(isElementDisplayed(stateLabelText), "Label Text is not displyed for State filed");
			Reporter.log("Verified LabelText For State");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For State");
		}
		return this;
	}

	/**
	 * Verify label text For Zip code
	 *
	 */
	public CheckOutPage verifyLabelTextForZipcode() {

		try {
			Assert.assertTrue(isElementDisplayed(zipCode), "Zipcode is not found");
			Assert.assertTrue(isElementDisplayed(zipCodeLabelText), "Label Text is not displyed for Zipcode filed");
			Reporter.log("Verified LabelText For Zipcode");
		} catch (Exception e) {
			Assert.fail("Failed to verify Label Text For Zipcode");
		}
		return this;
	}

	/**
	 * Verify Tool-tip For FirstName without filling data
	 *
	 */
	public CheckOutPage verifyTooltipForFirstName() {

		try {
			Assert.assertTrue(isElementDisplayed(firstName), "First Name is not found");
			Assert.assertTrue(isElementDisplayed(firstNameTooltip), "Tool tip is not displyed for First Name filed");
			Reporter.log("Verified Tool tip Message For FirstName");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool top For FirstName.");
		}
		return this;
	}

	/**
	 * Verify Error Message For LastName without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForLastName() {

		try {
			Assert.assertTrue(isElementDisplayed(lastName), "Last Name is not found");
			lastName.click();
			email.click();
			Assert.assertTrue(isElementDisplayed(lastNameErrorMessage),
					"Error message is not displyed for LastName filed");
			Reporter.log("Verified Error Message For LastName without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For LastName without filling data");
		}
		return this;
	}

	/**
	 * Verify Tool-tip For Email without filling data
	 *
	 */
	public CheckOutPage verifyTooltipForEmail() {

		try {
			Assert.assertTrue(isElementDisplayed(email), "EMail is not found");
			Assert.assertTrue(isElementDisplayed(emailTooltip), "Tool tip is not displyed for EMail filed");
			Reporter.log("Verified Tool tip Message For EMail");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool top For EMail.");
		}
		return this;
	}

	/**
	 * Verify the Hard stop page.
	 *
	 */
	public CheckOutPage verifyHardStopPage() {

		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("hard-stop"), 60);
			Reporter.log("Navigated to Hard stop page");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Hard stop Page.");
		}

		return this;
	}

	/**
	 * Verify Error Message For Email without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForEmail() {

		try {
			Assert.assertTrue(isElementDisplayed(email), "Email is not found");
			email.click();
			phoneNumber.click();
			Assert.assertTrue(isElementDisplayed(emailErrorMessage), "Error message is not displyed for Email filed");
			Reporter.log("Verified Error Message For Email without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For email without filling data");
		}
		return this;
	}

	/**
	 * Verify Tool-tip For PhoneNumber without filling data
	 *
	 */
	public CheckOutPage verifyTooltipForPhoneNumber() {

		try {
			Assert.assertTrue(isElementDisplayed(phoneNumber), "phoneNumber is not found");
			Assert.assertTrue(isElementDisplayed(phoneNumberTooltip), "Tool tip is not displyed for PhoneNumber filed");
			Reporter.log("Verified Tool tip Message For PhoneNumber");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool top For PhoneNumber.");
		}
		return this;
	}

	/**
	 * Verify Error Message For PhoneNumber without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForPhoneNumber() {

		try {
			if(getDriver() instanceof IOSDriver){
				Assert.assertTrue(isElementDisplayed(phoneNumber), "PhoneNumber is not found");
				phoneNumber.click();
				phoneNumber.sendKeys(Keys.TAB);
				email.click();
				Assert.assertTrue(isElementDisplayed(phoneNumberErrorMessage),
						"Error message is not displyed for PhoneNumber filed");
				Reporter.log("Verified Error Message For PhoneNumber without filling data");
			}else {
				
			Assert.assertTrue(isElementDisplayed(phoneNumber), "PhoneNumber is not found");
			phoneNumber.click();
			phoneNumber.sendKeys(Keys.TAB);
			Assert.assertTrue(isElementDisplayed(phoneNumberErrorMessage),
					"Error message is not displyed for PhoneNumber filed");
			Reporter.log("Verified Error Message For PhoneNumber without filling data");
		}
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For PhoneNumber without filling data");
		}
		return this;
	}

	/**
	 * Verify Tool-tip For LastName without filling data
	 *
	 */
	public CheckOutPage verifyTooltipForLastName() {

		try {
			Assert.assertTrue(isElementDisplayed(lastName), "Last Name is not found");
			Assert.assertTrue(isElementDisplayed(lastNameTooltip), "Tool tip is not displyed for last Name filed");
			Reporter.log("Verified Tool tip Message For LastName");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool top For LastName.");
		}
		return this;
	}

	/**
	 * Verify Personal Info Text in CheckOut
	 *
	 */
	public CheckOutPage verifyPersonalInfo() {

		try {
			Assert.assertTrue(isElementDisplayed(personalInfoText), "Personal Info Text is not found");
			Reporter.log("Personal Info Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Personal Info Text in CheckOutPage");
		}
		return this;
	}

	/**
	 * Verify Tool-tip For ShippingAddressLine1 without filling data
	 *
	 */
	public CheckOutPage verifyTooltipForShippingAddressLine1() {

		try {
			Assert.assertTrue(isElementDisplayed(address1), "ShippingAddressLine1 is not found");
			Assert.assertTrue(isElementDisplayed(addressTooltip),
					"Tool tip is not displyed for ShippingAddressLine1 filed");
			Reporter.log("Verified Tool tip Message For ShippingAddressLine1");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool top For ShippingAddressLine1.");
		}
		return this;
	}

	/**
	 * Verify Error Message For ShippingAddressLine1 without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForShippingAddressLine1() {

		try {
			Assert.assertTrue(isElementDisplayed(address1), "ShippingAddressLine1 is not found");
			moveToElement(address1);
			if(getDriver() instanceof AppiumDriver) {
				address1.click();
				city.click();
			}else {
				clickElementWithJavaScript(address1);
				address1.sendKeys(Keys.TAB);
			}
			
			Assert.assertTrue(isElementDisplayed(addressErrorMessage),
					"Error message is not displyed for ShippingAddressLine1 filed");
			Reporter.log("Verified Error Message For ShippingAddressLine1 without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For ShippingAddressLine1 without filling data");
		}
		return this;
	}

	/**
	 * Verify Tool-tip For City without filling data
	 *
	 */
	public CheckOutPage verifyTooltipForCity() {

		try {
			Assert.assertTrue(isElementDisplayed(city), "city element is not found");
			Assert.assertTrue(isElementDisplayed(cityTooltip), "Tool tip is not displyed for city filed");
			Reporter.log("Verified Tool tip Message For city");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool top For city.");
		}
		return this;
	}

	/**
	 * Verify Error Message For City without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCity() {

		try {
			Assert.assertTrue(isElementDisplayed(city), "city element is not found");
			city.click();
			state.click();
			Assert.assertTrue(isElementDisplayed(cityErrorMessage), "Error message is not displyed for city filed");
			Reporter.log("Verified Error Message For city without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For city without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For State without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForState() {

		try {
			Assert.assertTrue(isElementDisplayed(state), "state element is not found");
			state.click();
			zipCode.click();
			Assert.assertTrue(isElementDisplayed(stateErrorMessage), "Error message is not displyed for State filed");
			Reporter.log("Verified Error Message For State without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For State without filling data");
		}
		return this;
	}

	/**
	 * Verify Tool-tip For Zipcode without filling data
	 *
	 */
	public CheckOutPage verifyTooltipForZipcode() {

		try {
			Assert.assertTrue(isElementDisplayed(zipCode), "Zipcode element is not found");
			Assert.assertTrue(isElementDisplayed(zipCodeTooltip), "Tool tip is not displyed for Zipcode filed");
			Reporter.log("Verified Tool tip Message For Zipcode");
		} catch (Exception e) {
			Assert.fail("Failed to verify Tool top For Zipcode.");
		}
		return this;
	}

	/**
	 * Verify Error Message For Zipcode without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForZipcode() {

		try {
			Assert.assertTrue(isElementDisplayed(zipCode), "Zipcode element is not found");
			zipCode.click();
			state.click();
			if(isElementDisplayed(zipCodeErrorMessageSub)) {
				Assert.assertTrue(isElementDisplayed(zipCodeErrorMessageSub),
						"Error message is not displyed for Zipcode filed");
			}else {
				Assert.assertTrue(isElementDisplayed(zipCodeErrorMessage),
						"Error message is not displyed for Zipcode filed");
			}
			
			Reporter.log("Verified Error Message For Zipcode without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Zipcode without filling data");
		}
		return this;
	}

	/**
	 * Fill personal info
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillPersonalInfo(TMNGData tmngData) {
		try {
			waitForSpinnerInvisibility();
			firstName.sendKeys(tmngData.getFirstName());
			middleName.sendKeys(tmngData.getMiddleName());
//			String s = tmngData.getLastName() + RandomStringUtils.randomAlphabetic(8).toLowerCase(); 
//			lastName.sendKeys(s);
			lastName.sendKeys(tmngData.getLastName());
			email.clear();
			email.sendKeys(tmngData.getEmail());
			phoneNumber.sendKeys(tmngData.getPhoneNumber());
			Reporter.log("Filled personal info");
		} catch (Exception e) {
			Assert.fail("Failed to fill personal info");
		}
		return this;
	}

	/**
	 * Fill personal info
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillPersonalInfoWithoutMiddleName(TMNGData tmngData) {
		try {
			waitForSpinnerInvisibility();
			firstName.sendKeys(tmngData.getFirstName());
			lastName.sendKeys(tmngData.getLastName());
			email.clear();
			email.sendKeys(tmngData.getEmail());
			phoneNumber.sendKeys(tmngData.getPhoneNumber());
			Reporter.log("Filled personal info");
		} catch (Exception e) {
			Assert.fail("Failed to fill personal info");
		}
		return this;
	}

	/**
	 * Fill shipping address
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillShippingAddress(TMNGData tmngData) {
		try {
			waitFor(ExpectedConditions.visibilityOf(address1), 60);
			address1.sendKeys(tmngData.getShippingAddress());
			city.sendKeys(tmngData.getCity());
			setValueFromDropdown(tmngData.getState(), shipstate);
			zipCode.sendKeys(tmngData.getZipcode());
			Reporter.log("Filled shipping address");
		} catch (Exception e) {
			Assert.fail("Failed to fill shipping address");
		}
		return this;
	}

	/**
	 * Click terms and conditions
	 */
	public CheckOutPage clickTermsNConditions() {
		try {
			if(getDriver() instanceof IOSDriver){
				waitFor(ExpectedConditions.elementToBeClickable(termsNConditions), 60);
				termsNConditions.sendKeys(Keys.TAB);
				Reporter.log("Clicked on terms and conditions");
			}else{
				waitFor(ExpectedConditions.elementToBeClickable(termsNConditions), 60);
				if (!termsNConditions.isSelected()) {
					clickElementWithJavaScript(termsNConditions);
				}
				termsNConditions.sendKeys(Keys.TAB);
				Reporter.log("Clicked on terms and conditions");
			}
		} catch (Exception e) {
			Assert.fail("Failed to clock terms and conditions ");
		}
		return this;
	}

	/**
	 * Click nextbutton
	 */
	public CheckOutPage clickNextBtn() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(nextBtn), 60);
			clickElementWithJavaScript(nextBtn);
			Reporter.log("Clicked on next button");
		} catch (Exception e) {
			Assert.fail("Failed to click Next button.");
		}
		return this;
	}

	/**
	 * Click agree and agree and nextbutton
	 */
	public CheckOutPage clickAgreeandNextBtn() {
		try {
			// checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(agreeandNextBtn), 60);
			clickElementWithJavaScript(agreeandNextBtn);
			Reporter.log("Clicked on Agree and nextbutton");
		} catch (Exception e) {
			Assert.fail("Failed to click Agree and next btn");
		}
		return this;
	}

	/**
	 * Click agree and agree and submit btn
	 */
	public CheckOutPage clickAgreeAndSubmitBtnInReviewSubmit() {
		try {
			checkPageIsReady();
			if(getDriver() instanceof AppiumDriver) {
				Reporter.log("Mobile view does not require to click on 'Review and submit' checkbox");
			} else {
				waitFor(ExpectedConditions.elementToBeClickable(agreeCheckBoxInReviewSubmitTab), 60);
				clickElementWithJavaScript(agreeCheckBoxInReviewSubmitTab);
				Reporter.log("Clicked on Agree with Terms and Conditions checkbox");
			}
			clickElementWithJavaScript(agreeandSubmitBtn);
			Reporter.log("Clicked on Agree and submit button");
		} catch (Exception e) {
			Assert.fail("Failed to click Agree and Submit btn in Review and submit tab of CheckOutPage");
		}
		return this;
	}

	/**
	 * Fill payment info
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillPaymentInfo(TMNGData tmngData) {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(creditCardNumber), 60);
			creditCardNumber.clear();
			creditCardNumber.sendKeys(tmngData.getCardNumber());
			cardExpiryDate.clear();
			cardExpiryDate.sendKeys(tmngData.getExpiryDate());
			cvv.clear();
			cvv.sendKeys(tmngData.getCvv());
			cvv.sendKeys(Keys.TAB);
			Reporter.log("Filled  payment info");
		} catch (Exception e) {
			Assert.fail("Failed to fill  payment info");
		}
		return this;
	}

	/**
	 * Fill payment info
	 *
	 * @param tmngData
	 */
	public CheckOutPage VerifyPaymentInfoNotPrePopulated() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(creditCardNumber), 60);
			Assert.assertTrue(creditCardNumber.getAttribute("class").contains("ng-empty"),
					"creditCardNumber is pre-populated");
			Assert.assertTrue(cardExpiryDate.getAttribute("class").contains("ng-empty"),
					"cardExpiryDate is  pre-populated");
			Assert.assertTrue(cvv.getAttribute("class").contains("ng-empty"), "cvv is  pre-populated");
			Reporter.log("Payment info is not pre populated in credit card number,card expriy date,cvv fields");
		} catch (Exception e) {
			Assert.fail("Failed to veridy  payment info");
		}
		return this;
	}

	/**
	 * Fill payment info
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillPaymentInfoNameOnCard(TMNGData tmngData) {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(creditCardNumber), 60);
			nameOnCard.clear();
			nameOnCard.sendKeys(tmngData.getNameOnCard());
			Reporter.log("Filled  payment info");
		} catch (Exception e) {
			Assert.fail("Failed to fill  payment info");
		}
		return this;
	}

	/**
	 * click continueCTA On PaymentTab
	 *
	 * @param tmngData
	 */
	public CheckOutPage clickcontinueCTAOnPaymentTab() {
		try {
			clickElementWithJavaScript(continueCTAOnPaymentTab);
			Reporter.log("Clicked on continueCTA on PaymentTab");
		} catch (Exception e) {
			Assert.fail("Failed to Click continueCTA on PaymentTab");
		}
		return this;
	}

	/**
	 * Fill identity info
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillIdentityInfo(TMNGData tmngData) {

		try {
			idType.sendKeys(tmngData.getIdType());
			idNumber.clear();
			idNumber.sendKeys(tmngData.getIdNumber());
			expiryDate.clear();
			expiryDate.sendKeys(tmngData.getIdExpiryDate());
			ssn.clear();
			ssn.sendKeys(tmngData.getSSN());
			dob.clear();
			dob.sendKeys(tmngData.getDOB());
			securityPin.clear();
			securityPin.sendKeys(tmngData.getsecurityPin());
			Thread.sleep(6000);
			if (isElementDisplayed(dobError)) {
				Reporter.log("DOB error is displayed");
				dob.clear();
				dob.sendKeys(tmngData.getDOB());
			}
			Reporter.log("Filled  identity info");
		} catch (Exception e) {
			Assert.fail("Failed to fill  identity info");
		}
		return this;
	}

	/**
	 * Fill identity info with no credit check
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillIdentityInfoWithNoCreditCheck(TMNGData tmngData) {

		try {
			checkPageIsReady();
			idType.sendKeys(tmngData.getIdType());
			idNumber.clear();
			idNumber.sendKeys(tmngData.getIdNumber());
			expiryDate.clear();
			expiryDate.sendKeys(tmngData.getIdExpiryDate());
			dob.clear();
			dob.sendKeys(tmngData.getDOB());
			securityPin.clear();
			securityPin.sendKeys(tmngData.getsecurityPin());
			ssn.clear();
			ssn.sendKeys(tmngData.getSSN());
			Reporter.log("Filled  identity info");
		} catch (Exception e) {
			Assert.fail("Failed to fill  identity info");
		}
		return this;
	}

	/**
	 * Verify Review and Submit Page
	 * 
	 * @return
	 */
	public CheckOutPage verifyReviewandSubmitPage() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(reviewOrderHeader), 60);
			Assert.assertTrue(reviewOrderHeader.isDisplayed(), "Review and submit page is not dispalyed");
			Reporter.log("Review and submit page is dispalyed");
		} catch (Exception e) {
			Assert.fail("Failed to verify review and submit page");
		}
		return this;
	}

	/**
	 * Verify Review and Submit Page
	 * 
	 * @return
	 */
	public CheckOutPage verifyReviewandSubmitPageDisplayed() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			Assert.assertTrue(reviewOrderTitle.isDisplayed(), "Review and submit page is not dispalyed");
			Reporter.log("Review and submit page is dispalyed");
		} catch (Exception e) {
			Assert.fail("Failed to verify review and submit page");
		}
		return this;
	}

	/**
	 * Verify SoftStop modal pop up.
	 * 
	 * @return
	 */
	public CheckOutPage verifySoftStopModalAndClickgotit() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			Assert.assertTrue(softStopGotitBtn.isDisplayed(), "SoftStop modal pop up is not displayed");
			softStopGotitBtn.click();
			Reporter.log("SoftStop modal pop up is displayed and clicked on Got it CTA");
		} catch (Exception e) {
			Assert.fail("Failed to Verify SoftStop modal pop up.");
		}
		return this;
	}

	/**
	 * Verify EIP Loan Term Length
	 * 
	 * @return
	 */
	public CheckOutPage verifyEipLoanTermLength() {
		try {
			checkPageIsReady();
			String eipAmount = eipLoanTermLength.getAttribute("left-text");
			Assert.assertFalse(eipAmount.isEmpty(), "EIP loan term is not displayed");
			Reporter.log("Eip loan term length is dispalyed");
		} catch (Exception e) {
			Assert.fail("Failed to verify eip loan term length");
		}
		return this;
	}

	/**
	 * Verify Edit in cart CTA in Review/Submit Tab of checkout page.
	 * 
	 * @return
	 */
	public CheckOutPage verifyEditInCartCTA() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(editInCartCTA), 60);
			Assert.assertTrue(isElementDisplayed(editInCartCTA),
					"Edit In Cart CTA is not displayed in Review/Submit Tab of checkout page ");
			Reporter.log("Edit In Cart CTA is displayed in Review/Submit Tab of checkout page ");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit in Cart CTA in Review/Submit Tab of checkout page.");
		}
		return this;
	}

	/**
	 * Click Edit in cart CTA in Review/Submit Tab of checkout page.
	 * 
	 * @return
	 */
	public CheckOutPage clickEditInCartCTA() {
		try {
			checkPageIsReady();
			editInCartCTA.click();
			Reporter.log("Edit In Cart CTA is Clicked in Review/Submit Tab of checkout page ");
		} catch (Exception e) {
			Assert.fail("Failed to click Edit in Cart CTA in Review/Submit Tab of checkout page.");
		}
		return this;
	}

	/**
	 * Verify Legal Text Review and Submit page
	 * 
	 * @return
	 */
	public CheckOutPage verifyLegalTextReviewandSubmitPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(legalText.isDisplayed(), "Legal text is notdisplayed");
			Assert.assertTrue(
					legalText.getText().contains(
							"Monthly payment is calculated based on the shortest financial term for the loan "),
					"Text in legal doesnt match");
			Reporter.log("Legal text is dispalyed");
		} catch (Exception e) {
			Assert.fail("Failed to verify legal text in Review and Submit page");
		}
		return this;
	}

	/**
	 * Verify Legal Text not displayed and Review Submit Page
	 * 
	 * @return
	 */
	public CheckOutPage verifyLegalTextNotDisplayedReviewandSubmitPage() {
		try {
			checkPageIsReady();
			Assert.assertFalse(legalText.isDisplayed(), "Legal text is displayed");
			Reporter.log("Legal text is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify legal text -failed");

		}
		return this;
	}

	/**
	 * Verify EIP Down Payment on Checkout - Review and Submit screen
	 *
	 */
	public CheckOutPage verifyEipDownPayment() {
		try {
			String eipAmount = totalTodayAmount.getAttribute("left-amount");
			Assert.assertFalse(eipAmount.isEmpty(), "EIP down payment is not displayed");
			Reporter.log("Eip down payment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Eip down payment");
		}
		return this;
	}

	/**
	 * Verify EIP Monthly Payment on Checkout - Review and Submit screen
	 *
	 */
	public CheckOutPage verifyEipMonthlyPayment() {
		try {
			String eipMonthlyAmount = totalTodayAmount.getAttribute("right-amount");
			Assert.assertFalse(eipMonthlyAmount.isEmpty(), "EIP monthly payment is not displayed");
			Reporter.log("Eip monthly payment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Eip monthly payment");
		}
		return this;
	}

	/**
	 * Verify FRP payment on Checkout - Review and Submit screen
	 *
	 */
	public CheckOutPage verifyFRPPayment() {
		try {

			Assert.assertTrue(frpPrice.getText().contains("$"), "FRP payment not displayed");
			Reporter.log("FRP payment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display FRP payment");
		}
		return this;
	}

	/**
	 * Verify Legal Text on Product Tile of Checkout - Review and Submit screen
	 *
	 */
	public CheckOutPage verifyLegalTextReviewandSubmitPageOnProductTile() {
		try {
			Assert.assertTrue(isElementDisplayed(legalTextProductTile), "Legal text is not displayed");
			Reporter.log("Legal text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Legal text on product tile level");
		}
		return this;
	}

	/**
	 * Get Total price of accessories
	 */
	public String getTotalPriceOfAccessory() {
		String titalPrice = null;
		try {
			titalPrice = totalPriceOfAccessoryAtCheckOutPage.getAttribute("left-amount");
			Reporter.log("Total Price of accessory in check out page is " + titalPrice);
		} catch (Exception e) {
			Assert.fail("Total Price of accessory at CheckOut Page isn't available");
		}
		return titalPrice;
	}

	/**
	 * Get Total Monthly price
	 * 
	 * @return
	 */
	public Double getMonthlyPriceAtCheckOutReviewPage() {
		String monthlyprice = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(totalTodayAmount), 10);
			monthlyprice = totalTodayAmount.getAttribute("right-amount");
			Reporter.log("Monthly Price at check out page is " + monthlyprice);
		} catch (Exception e) {
			Assert.fail("Monthly Price At CheckOut Page isn't available");
		}
		return Double.parseDouble(monthlyprice);
	}

	/**
	 * Get Today Down Payment Integer format
	 * 
	 * @return
	 */
	public Double getTodayPriceAtCheckOutReviewPage() {

		String todayPrices = null;
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(todayPriceAmount), 5);
			todayPrices = todayPriceAmount.getAttribute("left-amount");
			Reporter.log("Today Down Payment at check out is " + todayPrices);
		} catch (Exception e) {
			Assert.fail("Today Down Payment at checkout Page isn't available");
		}
		return Double.parseDouble(todayPrices);
	}

	/**
	 * Get Today Down Payment Integer format
	 * 
	 * @return
	 */
	public Double getTodayDownpaymentPrice() {
		double todayPrices = 0;
		try {
			checkPageIsReady();
			String oneTimeFeeAmount = oneTimeFeeAmt.getText().replace("$", " ");
			String salesTaxAmount = salesTaxAmt.getText().replace("$", "");
			String shippingFeeAmount = shippingFeeAmt.getText().replace("$", "");

			todayPrices = Double.parseDouble(oneTimeFeeAmount) + Double.parseDouble(salesTaxAmount)
					+ Double.parseDouble(shippingFeeAmount) + getTodayPriceAtCheckOutReviewPage();
			// todayPrices = Math.round(todayPrices * 100) / 100;
			BigDecimal bd = new BigDecimal(todayPrices).setScale(2, RoundingMode.HALF_UP);
			todayPrices = bd.doubleValue();

			Reporter.log("Today Down Payment at check out is " + todayPrices);
		} catch (Exception e) {
			Assert.fail("Today Down Payment at checkout Page isn't available");
		}
		return todayPrices;
	}

	/**
	 * Get Today Down Payment Integer format
	 * 
	 * @return
	 */
	public Double getTotalTodayAmount() {
		Double totalTodayPrice = null;
		try {
			String sales = salesTax.getText();
			String[] arr1 = sales.split("\\$", 0);
			Double salesTaxes = Double.parseDouble(arr1[1]);

			String totalToday = totalTodayAmount.getAttribute("left-amount");
			Double totalTodayPriceWithTaxes = Double.parseDouble(totalToday);

			totalTodayPrice = totalTodayPriceWithTaxes - salesTaxes;
			Reporter.log("Today Down Payment at check out is " + totalToday + ", and taxes are " + sales);
		} catch (Exception e) {
			Assert.fail("Today Down Payment at checkout Page isn't available");
		}
		return totalTodayPrice;
	}

	/**
	 * Get Sim Starter Kit Price Integer format
	 * 
	 * @return
	 */
	public Double getSimStarterKitPrice() {
		String simStarterKit = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(simStarterKitPriceInReviewAndSubmitPage), 5);
			simStarterKit = simStarterKitPriceInReviewAndSubmitPage.getAttribute("left-amount");
			Reporter.log("Sim Starter Kit Price at check out is " + simStarterKit);
		} catch (Exception e) {
			Assert.fail("Sim Starter Kit Price at checkout Page isn't available");
		}
		return Double.parseDouble(simStarterKit);
	}

	/**
	 * Verify that Today price = (EIP Downpayment + SSK price)'
	 */
	public CheckOutPage compareTwoPrices(Double firstValue, Double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, "Both the Prices are not Matched");
			Reporter.log("Both the Prices are Matched.");
		} catch (Exception e) {
			Assert.fail("Failed to compare both the price.");
		}
		return this;
	}

	/**
	 * Selecting State from drop down
	 * 
	 */
	public void setValueFromDropdown(String dropDownValue, WebElement dropDownWebelement) {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(dropDownWebelement), 60);
			Select stateValue = new Select(dropDownWebelement);
			stateValue.selectByVisibleText(dropDownValue);
			dropDownWebelement.sendKeys(Keys.TAB);
		} catch (Exception e) {
			Assert.fail("Failed to select state from dropdown.");
		}

	}

	/**
	 * Get Personal EIP Price
	 * 
	 * @return
	 */
	public Double getPersonalEIPPriceCheckout() {
		Double amount = 0.0;
		try {
			waitForSpinnerInvisibility();
			amount = Double.parseDouble(paymentEIPPrice.getAttribute("left-amount"));
			Reporter.log("EIPPrice is visble on check out page .");
		} catch (Exception e) {
			Assert.fail("Failed to Get Personal EIP Price");
		}

		return amount;
	}

	/**
	 * Get Personal Monthly Price
	 * 
	 * @return
	 */
	public Double getPersonalMonthlyPriceCheckout() {
		Double monthlyPrice = 0.0;
		try {
			waitForSpinnerInvisibility();
			monthlyPrice = Double.parseDouble(paymentEIPPrice.getAttribute("right-amount"));
			Reporter.log("MonthlyPrice is visble on check out page .");
		} catch (Exception e) {
			Assert.fail("Failed to Personal Monthly Price");
		}

		return monthlyPrice;
	}

	/**
	 * Verify Trade-In Device value Tile
	 * 
	 * @return
	 */
	public CheckOutPage verifyTradeInDeviceValueTile() {

		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(tradeInTile), "Trade-In tile is not Displayed");
			Reporter.log("Trade-in tile is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Trade-in tile");
		}
		return this;

	}

	/**
	 * Verify EIP Loan Term Length
	 */
	public CheckOutPage verifyEipLoanTermLength(String loanTermLengthAtDeviceLevel) {
		try {
			checkPageIsReady();
			String eipAmount = eipLoanTermLength.getAttribute("left-text");
			Assert.assertTrue(eipAmount.contains(loanTermLengthAtDeviceLevel), "eip loan term length is varying");
			Reporter.log("Eip loan term length is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify eip loan term length.");
		}
		return this;
	}

	/**
	 * Verify that Total price
	 */
	public CheckOutPage compareTodayTotalPrice() {
		try {
			double result = getTodayDownpaymentPrice() - getTotalTodayAmount();
			Assert.assertTrue(result < 1, "Both the Prices are not Matched");
			Reporter.log("Both the Prices are Matched.");
		} catch (Exception e) {
			Assert.fail("Failed to compare both the price.");
		}
		return this;
	}

	/**
	 * Get Monthly price at Order Details CheckOut page
	 */
	public CheckOutPage verifyMonthlyPriceIsCorrectinOrderDetailsCheckOutPage() {
		try {
			String monthlyprice;
			for (int i = 0; i < todayPriceAtCheckOutPage.size() - 1; i++) {
				monthlyprice = todayPriceAtCheckOutPage.get(i).getAttribute("right-amount");
				if (i == 0) {
					Assert.assertTrue(monthlyprice.equals("70.00"),
							"Price of first line is not = $70 in Order Details CheckOut Page");
				}
				if (i == 1) {
					Assert.assertTrue(monthlyprice.equals("50.00"),
							"Price of second line is not = $50 in Order Details CheckOut Page");
				}
				if (i > 1) {
					Assert.assertTrue(monthlyprice.equals("20.00"),
							"Price of " + i + " line is not = $20 in Order Details v Page");
				}
			}
			Reporter.log("Plan price for each line in Order Details pageis correct");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly prices for services in Order Details CheckOut Page");
		}
		return this;
	}

	/**
	 * Verify Total AutoPay discount price is correct at CheckOutPage
	 */
	public CheckOutPage verifyTotalAutoPayDiscountIsCorrectInOrderDetailCheckOutPage() {
		try {
			String totalAutoPayText = autoPayTextForEachLineOrderDetailsCartPage
					.get(autoPayTextForEachLineOrderDetailsCartPage.size() - 1).getText().substring(1);
			Integer totalAutoPay = Integer.parseInt(totalAutoPayText);
			Integer autoPaySumByLines = (autoPayTextForEachLineOrderDetailsCartPage.toArray().length - 1) * 5;

			String totalAutoPayBannerText = totalAutoPayDiscountBannerOrderDetailsCartPage.getText();
			totalAutoPayBannerText = totalAutoPayBannerText.substring(totalAutoPayBannerText.lastIndexOf("$") + 1);
			Integer totalAutoPayBanner = Integer.parseInt(totalAutoPayBannerText);

			Assert.assertTrue(totalAutoPay.equals(autoPaySumByLines) && totalAutoPayBanner.equals(autoPaySumByLines),
					"Total AutoPay discount (from bottom of Order Details CheckOut Page) doesnt match summary of discounts");
			Reporter.log("AutoPay discount summary is matching total in Order Details CheckOut Page");
		} catch (Exception e) {
			Assert.fail("Failed to verify AutoPay discount price at Order Details CheckOut page");
		}
		return this;
	}

	/**
	 * Click On Personal Info And Shipping Tab
	 */
	public CheckOutPage clickOnPersonalInfoAndShippingTab() {
		try {
			personalInfoAndShippingTab.click();
			Reporter.log("PersonalInfo and Shipping tab should be clickable");
		} catch (Exception e) {
			Assert.fail("Failed to PersonalInfo and Shipping tab");
		}
		return this;
	}

	/**
	 * Verify User Consent Check Box UnSelected
	 */
	public CheckOutPage verifyUserConsentCheckBoxSelected() {
		try {
			waitforSpinner();
			if (termsNConditions.getAttribute("aria-checked") == "false") {
				Assert.fail("User consent check box is unselected");
			} else {
				Reporter.log("User consent box is in selected state");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Verify User Consent Check Box UnSelected");
		}
		return this;
	}

	/**
	 * Select idType
	 *
	 * @param tmngData
	 */
	public CheckOutPage selectIdType(String idTypeText) {

		try {
			checkPageIsReady();
			moveToElement(creditCheckBoxRequired);
			setValueFromDropdown(idTypeText, idTypeCreditCheck);
			Reporter.log("Id Type is selected");
		} catch (Exception e) {
			Assert.fail("Failed to select Id Type");
		}
		return this;
	}

	/**
	 * Selecting State for Id from drop down
	 *
	 */
	public CheckOutPage selectStateForId(String state) {
		try {			
			setValueFromDropdown(state, idState);
			Reporter.log("State is Selected from dropdown for ID");
		} catch (Exception e) {
			Assert.fail("Failed to select state from dropdown for Id");
		}
		return this;
	}

	/**
	 * Verify State dropdown for Id is disabled
	 *
	 */
	public CheckOutPage verifyStateDropdownDisabled() {
		try {
			Assert.assertFalse(idState.isEnabled(), "State dropdown for Id is enabled");
//			Assert.assertTrue(idState.getText().equalsIgnoreCase("Please enter the state"),
//					"Default value is not set in State Dropdown");
			Reporter.log("State dropdown for Id is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify if State dropdown for Id is disabled");
		}
		return this;
	}

	/**
	 * Verify Total Today Payment for Device on Checkout - Review and Submit screen
	 *
	 */
	public CheckOutPage verifyTotalTodayPayment() {
		try {
			String todayAmount = deviceTotalPrice.getAttribute("left-amount");
			Assert.assertFalse(todayAmount.isEmpty(), "Today total payment is not displayed");
			Reporter.log("Today total payment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Today Total payment");
		}
		return this;
	}

	/**
	 * Get Device Price on checkout page
	 * 
	 * @return
	 */
	public Double getDevicePriceCheckout() {
		Double deviceAmount = 0.0;
		try {
			waitForSpinnerInvisibility();
			deviceAmount = Double.parseDouble(devicePrice.getAttribute("left-amount"));
			Reporter.log("Device Price is visble on check out page .");
		} catch (Exception e) {
			Assert.fail("Failed to Get Device Price");
		}

		return deviceAmount;
	}

	/**
	 * Get Refundable Amount on checkout page
	 * 
	 * @return
	 */
	public Double getRefundableAmountCheckout() {
		Double refundAmount = 0.0;
		try {
			waitForSpinnerInvisibility();
			if (isElementDisplayed(refundableAmount)) {
				refundAmount = Double.parseDouble(refundableAmount.getAttribute("left-amount"));
				Reporter.log("Refundable Amount is visble on check out page .");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Get Refundable Amount");
		}

		return refundAmount;
	}

	/**
	 * Get Today total Amount on checkout page
	 * 
	 * @return
	 */
	public Double getTodayTotalAmountCheckout() {
		Double todayAmount = 0.0;
		try {
			waitForSpinnerInvisibility();
			todayAmount = Double.parseDouble(deviceTotalPrice.getAttribute("left-amount"));
			Reporter.log("Today Amount is visble on check out page .");
		} catch (Exception e) {
			Assert.fail("Failed to Get Today Amount");
		}

		return todayAmount;
	}

	/**
	 * Verify Total Today Payment for Device on Checkout - Review and Submit screen
	 *
	 */
	public CheckOutPage verifySumOfTodayTotalAmount() {
		try {
			Double simAmt = getSimStarterKitPrice();
			Double devicePrice = getDevicePriceCheckout();
			Double refundableAmt = getRefundableAmountCheckout();
			Double totaltodayAmt = getTodayTotalAmountCheckout();

			Double total = simAmt + devicePrice + refundableAmt;
			Assert.assertEquals(total, totaltodayAmt,
					"Total of Device, Sim & Refundable Amount is not equal to total amount.");
			Reporter.log("Today total payment is verified successfully");

		} catch (Exception e) {
			Assert.fail("Failed to verify the today total Amount.");
		}
		return this;
	}

	/**
	 * verify Confirmation header is present
	 *
	 */
	public CheckOutPage verifyConfirmationPageHeaderContainsName(String name) {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(confirmationHeader), 60);
			Assert.assertTrue(confirmationHeader.getText().contains(name),
					"Name is not present in confirmation header");
			Reporter.log("Confirmation Page is present and title contains name");
		} catch (AssertionError e) {
			Assert.fail("Failed to verify Confirmation Header");

		}
		return this;
	}

	/**
	 * Verify Tablet Line Discount on Review & Submit page
	 */
	public CheckOutPage verifyTabletLineDiscountOnReviewPage() {
		try {
			Assert.assertTrue(tabletLineDiscount.getText().contains("$45.00 tablet line discount"),
					"Tablet line discount is displayed incorrectly");
			Reporter.log("Tablet line discount is displayed correct");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Tablet line discount");
		}
		return this;
	}

	/**
	 * Verify Personal Info Text in CheckOut
	 *
	 */
	public CheckOutPage verifyLetsCheckYourCreditHeader() {

		try {
			waitFor(ExpectedConditions.visibilityOf(letsCheckYourCreditHeader), 60);
			Assert.assertTrue(letsCheckYourCreditHeader.isDisplayed(), "Credit Check is not displayed");
			Reporter.log("Credit Check is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Credit Check Header");
		}
		return this;
	}

	/**
	 * Fill Residential Address for Credit Check
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillResidentialAddressforCreditCheck(TMNGData tmngData) {

		try {
			addressLine1RACreditCheck.sendKeys(tmngData.getShippingAddress());
			cityRACreditCheck.sendKeys(tmngData.getCity());

			setValueFromDropdown(tmngData.getState(), stateRACreditCheck);

			/*
			 * stateRACreditCheck.click(); stateRACreditCheck.sendKeys(tmngData.getState());
			 * stateRACreditCheck.sendKeys(Keys.TAB);
			 * 
			 * stateRACreditCheck.click(); stateRACreditCheck.sendKeys(tmngData.getState());
			 * stateRACreditCheck.sendKeys(Keys.TAB);
			 */

			zipCodeRACreditCheck.sendKeys(tmngData.getZipcode());
			sendKeysWithWait(DOBCreditCheck, tmngData.getDOB());
			sendKeysWithWait(SSNNumberCreditCheck, tmngData.getSSN());
//			DOBCreditCheck.sendKeys(tmngData.getDOB());
//			SSNNumberCreditCheck.sendKeys(tmngData.getSSN());

			setValueFromDropdown(tmngData.getIdType(),idTypeCreditCheck);

			idNumber.clear();
			idNumber.sendKeys(tmngData.getIdNumber());

			expiryDateCreditCheck.sendKeys(tmngData.getExpiryDate());

			while (dobErrorMessage.isDisplayed()) {
				DOBCreditCheck.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
				DOBCreditCheck.sendKeys(tmngData.getDOB());
			}

			while (!SSNNumberCreditCheck.getAttribute("value").replace("-", "").equals(tmngData.getSSN())) {
				SSNNumberCreditCheck.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
				SSNNumberCreditCheck.sendKeys(tmngData.getSSN());
			}

			Reporter.log("Filled  credit Check Details");

		} catch (Exception e) {
			Assert.fail("Failed to fill  identity info");
		}
		return this;
	}

	/**
	 * Selecting State for Id from drop down
	 *
	 */
	public CheckOutPage selectStateForIdIfEnabled(String state) {
		try {
			if (idState.isEnabled()) {
				idState.click();
				idState.sendKeys(state);
				idState.sendKeys(Keys.TAB);
			}
			Reporter.log("State is Selected from dropdown for ID");
		} catch (Exception e) {
			Assert.fail("Failed to select state from dropdown for Id");
		}
		return this;
	}

	/**
	 * Click Required check box for Credit Check
	 *
	 */
	public CheckOutPage clickRequiredCheckBoxForCreditCheck() {

		try {
			if(getDriver() instanceof IOSDriver){
				creditCheckBoxRequired.sendKeys(Keys.TAB);
			}else {
			
			clickElementWithJavaScript(creditCheckBoxRequired);	
			}
		
			Reporter.log("Clicked on Required Credit Check Box");
		} catch (Exception e) {
			Assert.fail("Failed to clicked on Required Credit Check Box");
		}
		return this;
	}

	/**
	 * Click Run Credit Check Credit Check
	 *
	 */
	public CheckOutPage clickRunCreditCheck() {

		try {
			waitFor(ExpectedConditions.elementToBeClickable(runCreditCheckButton), 60);
			clickElementWithJavaScript(runCreditCheckButton);
			Reporter.log("Clicked on Run Credit Check Button");
		} catch (Exception e) {
			Assert.fail("Failed to clicked on Run Credit Check Button");
		}
		return this;
	}

	/**
	 * Verify Credit Check Order Modal
	 *
	 */
	public CheckOutPage verifyOrderModalForCreditCheck() {
		try {
			waitFor(ExpectedConditions.visibilityOf(hccResultModal));
			if (isElementDisplayed(creditCheckOrderModal)) {
				Reporter.log("Credit Check Order Modal is displayed");
			} else {
				verifyCallCta();
				Assert.fail("Manual Review pop up is displayed");
			}
			Reporter.log("Credit Check Order Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Credit Check Order Modal");
		}
		return this;
	}

	/**
	 * click Check out Now CTA on Credit Check Order Modal
	 *
	 */
	public CheckOutPage clickCheckOutNowCTAOrderModal() {

		try {
			waitFor(ExpectedConditions.visibilityOf(checkOutNowCTA), 60);
			moveToElement(checkOutNowCTA);
			checkOutNowCTA.click();
			Reporter.log("Check Out Now CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Check Out Now CTACredit Check Order Modal");
		}
		return this;
	}

	/**
	 * Verify Pricing at the top sticky banner
	 *
	 */
	public CheckOutPage verifyPricingAtCheckOutStickyBanner() {

		try {
			Assert.assertFalse(totalTodayAmount.getAttribute("left-amount").isEmpty(), "Pricing is not displayed");
			Reporter.log("Pricing section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pricing section");
		}
		return this;
	}

	/**
	 * Verify Agree and Next Button Disabled on Shipping and Payment Tab
	 */
	public CheckOutPage verifyAgreeandNextBtnDisabled() {
		try {
			waitFor(ExpectedConditions.visibilityOf(address1), 60);
			// Assert.assertTrue(agreeandNextBtn.getAttribute("disabled").contains("disabled"),"Agree
			// and Next Button is Enabled");
			Assert.assertFalse(agreeandNextBtn.isEnabled(), "Agree and Next Button is Enabled");
			Reporter.log("Agree and Next Button is not enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify agree and next button");
		}
		return this;
	}

	/**
	 * Verify Error Message For Credit Card without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCreditCardNumber() {

		try {
			Assert.assertTrue(isElementDisplayed(creditCardNumber), "Credit Card Number Text Box not present");
			clickElementWithJavaScript(creditCardNumber);
			creditCardNumber.sendKeys(Keys.TAB);
			cardExpiryDate.click();
			Assert.assertTrue(isElementDisplayed(creditCardNumberErrorMessage),
					"Error message is not displyed for Credit Card field");
			Reporter.log("Error message is displyed for Credit Card field");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Credit Card without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For Expiry Date without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForExpiryDate() {

		try {
			Assert.assertTrue(isElementDisplayed(cardExpiryDate), "Expiry Date Text Box not present");
			cardExpiryDate.click();
			cardExpiryDate.sendKeys(Keys.TAB);
			Assert.assertTrue(isElementDisplayed(expirationDateErrorMessage),
					"Error message is not displyed for Expiry Date field");
			Reporter.log("Error message is displyed for Expiry Date field");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Expiry Date without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For Expiry Date without filling data for Credit Check
	 *
	 */
	public CheckOutPage verifyErrorMessageForExpiryDateCC() {

		try {
			if((getDriver() instanceof AppiumDriver)) {
			
			Assert.assertTrue(isElementDisplayed(cardExpiryDate), "Expiry Date Text Box not present");
			cardExpiryDate.click();
			cardExpiryDate.sendKeys(Keys.TAB);
			cvv.click();
			Assert.assertTrue(isElementDisplayed(expirationDateErrorMessageCC),
					"Error message is not displyed for Expiry Date field");
			Reporter.log("Error message is displyed for Expiry Date field");
			}else {
				Assert.assertTrue(isElementDisplayed(cardExpiryDate), "Expiry Date Text Box not present");
				cardExpiryDate.click();
				cardExpiryDate.sendKeys(Keys.TAB);
				Assert.assertTrue(isElementDisplayed(expirationDateErrorMessageCC),
						"Error message is not displyed for Expiry Date field");
				Reporter.log("Error message is displyed for Expiry Date field");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Expiry Date without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For CVV without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCVV() {

		try {
			if(getDriver() instanceof IOSDriver){
				Assert.assertTrue(isElementDisplayed(cvv), "CVV Text Box not present");
				clickElementWithJavaScript(cvv);
				cvv.sendKeys(Keys.TAB);
				cardExpiryDate.click();
				Assert.assertTrue(isElementDisplayed(cvvErrorMessage), "Error message is not displyed for CVV");
				Reporter.log("Error message is displyed for CVV");
			}else {
				Assert.assertTrue(isElementDisplayed(cvv), "CVV Text Box not present");
				clickElementWithJavaScript(cvv);
				cvv.sendKeys(Keys.TAB);
				Assert.assertTrue(isElementDisplayed(cvvErrorMessage), "Error message is not displyed for CVV");
				Reporter.log("Error message is displyed for CVV");	
			}
			
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For CVV without filling data");
		}
		return this;
	}

	/**
	 * Click nextbutton TPP
	 */
	public CheckOutPage clickNextBtnTPP() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(nextBtnTPP), 60);
			clickElementWithJavaScript(nextBtnTPP);
			Reporter.log("Clicked on next button");
		} catch (Exception e) {
			Assert.fail("Failed to click Next button.");
		}
		return this;
	}

	/**
	 * Verify Shipping Header
	 */
	public CheckOutPage verifyShippingHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(shippingHeader), 60);
			Assert.assertTrue(shippingHeader.isDisplayed(), "Shiiping header is displayed");
			Reporter.log("Shipping Header Verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify shipping header");
		}
		return this;
	}

	/**
	 * Fill security pin
	 *
	 * @param tmngData
	 */
	public CheckOutPage setSecurityPin(TMNGData tmngData) {
		try {
			securityPin.clear();
			securityPin.sendKeys(tmngData.getsecurityPin());
			securityPin.sendKeys(Keys.TAB);
			Reporter.log("Security Pin updated");
		} catch (Exception e) {
			Assert.fail("Failed to fill  security pin");
		}
		return this;
	}

	/**
	 * Verify Accessories on Updated Pricing Modal
	 *
	 */
	public CheckOutPage verifyAccessoriesOnUpdatePricingModal() {

		try {
			waitFor(ExpectedConditions.visibilityOf(accessoriesInPricingModal), 60);
			Assert.assertTrue(accessoriesInPricingModal.isDisplayed(), "Accessories are not displayed");
			Reporter.log("Accessories is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessories");
		}
		return this;
	}

	/**
	 * Click on down carat for view Accessories
	 */
	public CheckOutPage clickdowncaratButtonForAccessories() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(downcaratButtonForAccessories), 60);
			clickElementWithJavaScript(downcaratButtonForAccessories);
			Reporter.log("Clicked on down carat button for accessories show");
		} catch (Exception e) {
			Assert.fail("Failed to click on down carat button for view accessories");
		}
		return this;
	}

	/**
	 * verify Accessories Pricing display
	 */
	public CheckOutPage verifyAccessoriesPricingAreDisplayed() {
		try {
			waitforSpinner();
			for (WebElement webElement : accessoriesPricingDisplay) {
				Assert.assertTrue(webElement.isDisplayed(), "Accessories pricing are not displayed");
			}
			Reporter.log("Accessories pricing are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessories pricing on pricing modal");

		}
		return this;
	}

	/**
	 * Verify Error Message For Credit Check AddressLine1 without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCreditCheckAddressLine1() {

		try {
			Assert.assertTrue(isElementDisplayed(addressLine1RACreditCheck), "Credit Check AddressLine1 is not found");
			addressLine1RACreditCheck.click();
			addressLine1RACreditCheck.sendKeys(Keys.TAB);
			cityRACreditCheck.click();
			Assert.assertTrue(isElementDisplayed(addressErrorMessage),
					"Error message is not displayed for Credit Check Line1 field");
			Reporter.log("Verified Error Message For Credit Check Line1 without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Credit Check Line1 without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For City without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCreditCheckCity() {

		try {
			Assert.assertTrue(isElementDisplayed(cityRACreditCheck), "City element is not found");
			cityRACreditCheck.click();
			cityRACreditCheck.sendKeys(Keys.TAB);
			stateRACreditCheck.click();
			Assert.assertTrue(isElementDisplayed(cityErrorMessage), "Error message is not displayed for city field");
			Reporter.log("Verified Error Message For city without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For city without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For State without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCreditCheckState() {

		try {
			Assert.assertTrue(isElementDisplayed(stateRACreditCheck), "state element is not found");
			stateRACreditCheck.click();
			cityRACreditCheck.click();
			Assert.assertTrue(isElementDisplayed(stateErrorMessage), "Error message is not displayed for State field");
			Reporter.log("Verified Error Message For State without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For State without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For Zipcode without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCreditCheckZipcode() {

		try {
			Assert.assertTrue(isElementDisplayed(zipCodeRACreditCheck), "Zipcode element is not found");
			zipCodeRACreditCheck.click();
			stateRACreditCheck.click();
			Assert.assertTrue(isElementDisplayed(zipCodeErrorMessage),
					"Error message is not displayed for Zipcode field");

			Reporter.log("Verified Error Message For Zipcode without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Zipcode without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For DOB without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForDOB() {

		try {
			Assert.assertTrue(isElementDisplayed(DOBCreditCheck), "DOB element is not found");
			clickElementWithJavaScript(DOBCreditCheck);
			DOBCreditCheck.sendKeys(Keys.TAB);
			SSNNumberCreditCheck.click();
			Assert.assertTrue(isElementDisplayed(dobErrorMessage), "Error message is not displayed for DOB field");
			Reporter.log("Verified Error Message For DOB without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For DOB without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For SSN number without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForSSN() {

		try {
			Assert.assertTrue(isElementDisplayed(SSNNumberCreditCheck), "SSN element is not found");
			SSNNumberCreditCheck.click();
			idNumber.click();			
			Assert.assertTrue(isElementDisplayed(ssnErrorMessage), "Error message is not displayed for SSN field");
			Reporter.log("Verified Error Message For SSN without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For SSN without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For ID number without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForIDNumber() {

		try {
			if(getDriver() instanceof IOSDriver){
				Assert.assertTrue(isElementDisplayed(idNumber), "ID number element is not found");
				idNumber.click();
				expiryDateCreditCheck.click();
			}else {
			Assert.assertTrue(isElementDisplayed(idNumber), "ID number element is not found");
			idNumber.click();
			expiryDateCreditCheck.click();
			}
			Assert.assertTrue(isElementDisplayed(idNumberErrorMessage), "Error message is not displayed for ID number");
			Reporter.log("Verified Error Message For ID number without filling data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For ID number without filling data");
		}
		return this;
	}

	/**
	 * Verify Error Message For Expiry Date without filling data
	 *
	 */
	public CheckOutPage verifyErrorMessageForCreditCheckExpiryDate() {

		try {
			if(getDriver() instanceof IOSDriver){
				Assert.assertTrue(isElementDisplayed(expiryDateCreditCheck), "Expiry Date element is not found");
				expiryDateCreditCheck.click();
				expiryDateCreditCheck.sendKeys(Keys.TAB);
				idNumber.click();
				Assert.assertTrue(isElementDisplayed(expirationDateErrorMessage),
						"Error message is not displayed for Expiry Date");
				Reporter.log("Verified Error Message For Expiry Date without filling data");
			}else {
			Assert.assertTrue(isElementDisplayed(expiryDateCreditCheck), "Expiry Date element is not found");
			expiryDateCreditCheck.click();
			expiryDateCreditCheck.sendKeys(Keys.TAB);
			Assert.assertTrue(isElementDisplayed(expirationDateErrorMessage),
					"Error message is not displayed for Expiry Date");
			Reporter.log("Verified Error Message For Expiry Date without filling data");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For Expiry Date without filling data");
		}
		return this;
	}

	/**
	 * Verify Verify Identity page.
	 * 
	 * @return
	 */
	public CheckOutPage verifyVerifyIdentityPage() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(creditCheckHeader), 60);
			Assert.assertTrue(creditCheckHeader.getText().contains("verify your identity"),
					"Verify identity page is not displayed");
			Reporter.log("Verify Identity page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Verify Identity Page");
		}
		return this;
	}

	/**
	 * Fill Residential Address for Credit Check
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillIdentityVerificationForVerifyID(TMNGData tmngData) {

		try {
			checkPageIsReady();
			idTypeCreditCheck.click();
			idTypeCreditCheck.sendKeys(tmngData.getIdType());
			idTypeCreditCheck.sendKeys(Keys.TAB);

			idNumber.clear();
			idNumber.sendKeys(tmngData.getIdNumber());
			expiryDateCreditCheck.sendKeys(tmngData.getExpiryDate());

			Reporter.log("Filled  Identity verification Details");

		} catch (Exception e) {
			Assert.fail("Failed to fill  identity info");
		}
		return this;
	}

	/**
	 * Click nextbutton on Verify ID Page
	 */
	public CheckOutPage clickNextBtnInVerifyIDPage() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(verifyIDNextButton);
			Reporter.log("Clicked on next button");
			// Thread.sleep(5000);
		} catch (Exception e) {
			Assert.fail("Failed to click Next button.");
		}
		return this;
	}

	/**
	 * Verify Error Message For SSN number without filling data
	 *
	 */
	public CheckOutPage fillSSN(TMNGData tmngData) {

		try {
			Assert.assertTrue(isElementDisplayed(SSNNumberCreditCheck), "SSN element is not found");
			SSNNumberCreditCheck.sendKeys(tmngData.getSSN());
			Reporter.log("SSN Entered");
		} catch (Exception e) {
			Assert.fail("Failed to enter SSN");
		}
		return this;
	}

	/**
	 * Fill personal info
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillPersonalInfoAfterpreScreen(TMNGData tmngData) {
		try {
			waitForSpinnerInvisibility();
			phoneNumber.sendKeys(tmngData.getPhoneNumber());
			Reporter.log("Filled personal info");
		} catch (Exception e) {
			Assert.fail("Failed to fill personal info");
		}
		return this;
	}

	/**
	 * Verify Line - 'Line number',SKU Name and Plan, due Today and due
	 * Monthly,Expandable Carrot
	 * 
	 * @return
	 */
	public CheckOutPage verifyLineOnReviewAndSubmit() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			Assert.assertTrue(lineNumber.isDisplayed(), "Line Number not displayed");
			Assert.assertTrue(sku.isDisplayed(), "Sku Name not displayed");
			Assert.assertTrue(planName.isDisplayed(), "Plan Name not displayed");
			Assert.assertTrue(dueToday.isDisplayed(), "Due Today not displayed");
			Assert.assertTrue(dueMonthly.isDisplayed(), "Monthly not displayed");
			Assert.assertTrue(expandingCarrot.isDisplayed(), "Expanding carrot not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Verify Review and Submit Page");
		}
		return this;
	}

	/**
	 * click on expandable carrot icon
	 *
	 */
	public CheckOutPage clickCarrot() {

		try {
			clickElementWithJavaScript(expandingCarrot);
			Reporter.log("Carrot Icon clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on expandable carrot");
		}
		return this;
	}

	/**
	 * Verify Plan name and discount
	 * 
	 * @return
	 */
	public CheckOutPage verifyPlanNameAndDiscount() {

		try {
			waitFor(ExpectedConditions.visibilityOf(planNameAndDiscount), 60);
			Assert.assertTrue(planNameAndDiscount.isDisplayed(), "Plan name and Discount not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Plan and discount");
		}
		return this;
	}

	/**
	 * Verify Sku Details
	 * 
	 * @return
	 */
	public CheckOutPage verifySKUDetails() {

		try {
			Assert.assertTrue(skuName.isDisplayed(), "Sku Name not displayed");
			Assert.assertTrue(memory.isDisplayed(), "Plan Name not displayed");
			Reporter.log("Sku Deatils are verified");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify SKU Details");
		}
		return this;
	}

	/**
	 * Verify Promo text for device
	 * 
	 * @return
	 */
	public CheckOutPage verifyPromoTextForDevice() {

		try {
			Assert.assertTrue(promoText.isDisplayed(), "Promo text not displayed");
			Reporter.log("Promo text are verified");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Promo text");
		}
		return this;
	}

	/**
	 * Verify Legal text for device
	 * 
	 * @return
	 */
	public CheckOutPage verifyLegalTextForDevice() {

		try {
			Assert.assertTrue(legaltext.isDisplayed(), "Legal text not displayed");
			Reporter.log("Legal text are verified");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Legal text");
		}
		return this;
	}

	/**
	 * Verify Sub Total Section
	 * 
	 * @return
	 */
	public CheckOutPage verifySubTotalSection() {

		try {
			Assert.assertTrue(subTotalHeaderText.isDisplayed(), "Sub Total Header  not displayed");
			Assert.assertTrue(OneTimeFeesText.isDisplayed(), "One Time Fees not displayed");
			Assert.assertTrue(salesTaxText.isDisplayed(), "Sales tax not displayed");
			Assert.assertTrue(shippingText.isDisplayed(), "Shipping not displayed");
			Reporter.log("Sub Total Section displayed correctly");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sub Total Section");
		}
		return this;
	}

	/**
	 * Verify Sub Total Section
	 * 
	 * @return
	 */
	public CheckOutPage verifyTotal() {

		try {
			Assert.assertTrue(total.isDisplayed(), "Sub Total Header  not displayed");
			Reporter.log("Sub Total Section displayed correctly");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sub Total Section");
		}
		return this;
	}

	/**
	 * Verify Sub Total Section
	 * 
	 * @return
	 */
	public CheckOutPage verifyTodayAndMonthly() {

		try {
			Assert.assertTrue(todayText.get(1).isDisplayed(), "Today Text  not displayed");
			Assert.assertTrue(monthlyText.get(1).isDisplayed(), "Monthly Text not displayed");
			Reporter.log("Today and Monthly displayed correctly");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Today and Monthly");
		}
		return this;
	}

	/**
	 * Verify Shipping Review shipping header
	 * 
	 * @return
	 */
	public CheckOutPage verifyShippingReviewHeader() {

		try {
			Assert.assertTrue(shippingReviewHeader.isDisplayed(), "Shipping header is not displayed");
			Reporter.log("Shipping header is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Shipping header");
		}
		return this;
	}

	/**
	 * Verify Shipping Review edit CTA
	 * 
	 * @return
	 */
	public CheckOutPage verifyShippingReviewEditCTA() {

		try {
			Assert.assertTrue(editShippingLink.isDisplayed(), "Shipping edit CTA is not displayed");
			Reporter.log("Shipping edit CTA edit CTA is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Shipping edit CTA");
		}
		return this;
	}

	/**
	 * Verify Shipping Address
	 * 
	 * @return
	 */
	public CheckOutPage verifyIfShippingAddressIsCorrect(TMNGData tmngData) {
		try {
			Assert.assertTrue(isElementDisplayed(shippingAddressLine1),
					"Shipping address is not getting displayed correctly");
			String addresstext = shippingAddressLine1.getText();
			Assert.assertTrue(addresstext.contains(tmngData.getLastName()),
					"Shipping address is not displayed correctly");
			Reporter.log("Shipping address is displayed correctly");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Shipping address");
		}
		return this;
	}

	/**
	 * Verify payment Review shipping header
	 * 
	 * @return
	 */
	public CheckOutPage verifyPaymentReviewHeader() {

		try {
			Assert.assertTrue(paymentHeader.isDisplayed(), "payment header is not displayed");
			Reporter.log("payment header is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Payment header");
		}
		return this;
	}

	/**
	 * Verify payment edit CTA
	 * 
	 * @return
	 */
	public CheckOutPage verifyPaymentReviewEditCTA() {

		try {
			Assert.assertTrue(editPaymentLink.isDisplayed(), "payment edit CTA is not displayed");
			Reporter.log("payment edit CTA edit CTA is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify payment edit CTA");
		}
		return this;
	}

	/**
	 * Verify Payment Address
	 * 
	 * @return
	 */
	public CheckOutPage verifyIfPaymentAddressIsCorrect(TMNGData tmngData) {
		try {
			Assert.assertTrue(paymentCity.getText().contains(tmngData.getCity()),
					"Payment address is not displayed correctly");
			Reporter.log("Payment address is displayed correctly");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Payment address");
		}
		return this;
	}

	/**
	 * Verify Privacy Policy section
	 * 
	 * @return
	 */
	public CheckOutPage verifyPrivacyImageAndPolicy() {
		try {
			Assert.assertTrue(privacyImage.isDisplayed(), "Privacy Image is not displayed correctly");
			Assert.assertTrue(privacyPolicyLink.get(0).isDisplayed(), "Privacy Policy Link is not displayed correctly");
			Assert.assertTrue(privacyPolicyText.get(0).isDisplayed(), "Privacy Policy Text is not displayed correctly");
			Reporter.log("Privacy Image and Policy is displayed correctly");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Privacy Image and policy");
		}
		return this;
	}

	/**
	 * Verify Terms and COnditions
	 * 
	 * @return
	 */
	public CheckOutPage verifyTermsAndConditions() {
		try {
			Assert.assertTrue(termsAndConditions.isDisplayed(), "Terms and COnditions is not displayed correctly");
			Reporter.log("Terms and COnditions is displayed correctly");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Terms and COnditions");
		}
		return this;
	}

	/**
	 * Verify Run Credit Check Button Disabled on Credit Check Tab
	 */
	public CheckOutPage verifyRunCreditCheckBtnDisabled() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertFalse(runCreditCheckButton.isEnabled(), "Run Credit Check Button is Enabled");
			Reporter.log("Run Credit Check Button is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Run Credit Check button");
		}
		return this;
	}

	/**
	 * Verify Run Credit Check Button Disabled on Credit Check Tab
	 */
	public CheckOutPage verifyRunCreditCheckBtnEnabled() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertTrue(runCreditCheckButton.isEnabled(), "Run Credit Check Button is Disabled");
			Reporter.log("Run Credit Check Button is Enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Run Credit Check button");
		}
		return this;
	}

	/**
	 * Fill DOB details for Credit Check
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillDOBforCreditCheck() {

		try {
			checkPageIsReady();
			DOBCreditCheck.clear();
			DOBCreditCheck.sendKeys("06221999");

			Reporter.log("Filled DOB Details");

		} catch (Exception e) {
			Assert.fail("Failed to fill DOB info");
		}
		return this;
	}

	/**
	 * Verify view order details link is not displayed
	 *
	 */
	public CheckOutPage verifyViewOrderDetailsLinkNotDisplayed() {

		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(viewOrderDetailsLink), "Order details link is diplayed");
			Reporter.log("View order details link is not displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display View order details link.");
		}
		return this;
	}

	/**
	 * Verify carrot collapse
	 */
	public CheckOutPage verifyCarrotCollapsed() {
		try {
			Assert.assertEquals(caretExpansion.size(), 0, "Carrot collapse is not working");
			Assert.assertFalse(isElementDisplayed(carrotExpansion), " Carrot collapse is not working");
			Reporter.log("Carrot collpased");
		} catch (Exception e) {
			Assert.fail("Failed to verify if carrot is collapsed");
		}
		return this;
	}

	/**
	 * Verify Verify Identity page Sub Header.
	 * 
	 * @return
	 */
	public CheckOutPage verifyVerifyIdentityPageSubHeader() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			Assert.assertTrue(creditCheckSubHeader.getText().contains("make sure your Social Security Number matches"),
					"Verify identity page sub header is not displayed");
			Reporter.log("Verify Identity page sub header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Verify Identity Page sub header");
		}
		return this;
	}

	/**
	 * Verify Verify Identity page Personal Information header.
	 * 
	 * @return
	 */
	public CheckOutPage verifyVerifyIdentityPersonalInfoHeader() {

		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			Assert.assertTrue(personalInformationHeader.getText().contains("Personal Information"),
					"Personal Information Header is not displayed");
			Reporter.log("Personal Information Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Personal Information Header");
		}
		return this;
	}

	/**
	 * Verify SSN text field
	 * 
	 * @return
	 */
	public CheckOutPage verifySSNTextfield() {

		try {
			Assert.assertTrue(SSNNumberCreditCheck.isDisplayed(), "SSN text field is not displayed");
			Reporter.log("SSN text field is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify SSN text field");
		}
		return this;
	}

	/**
	 * Verify ID Type drop down field
	 * 
	 * @return
	 */
	public CheckOutPage verifyidTypefield() {

		try {
			Assert.assertTrue(idTypeCreditCheck.isDisplayed(), "ID type field is not displayed");
			Reporter.log("ID type field is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify ID type field");
		}
		return this;
	}

	/**
	 * Verify ID number text field
	 * 
	 * @return
	 */
	public CheckOutPage verifyIDNumberTextfield() {

		try {
			Assert.assertTrue(idNumber.isDisplayed(), "ID number field is not displayed");
			Reporter.log("ID number text field is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify ID number text field");
		}
		return this;
	}

	/**
	 * Verify Expiration date text field
	 * 
	 * @return
	 */
	public CheckOutPage verifyExpirationDateTextfield() {

		try {
			Assert.assertTrue(expiryDateCreditCheck.isDisplayed(), "Expiration Date text field is not displayed");
			Reporter.log("Expiration Date text field is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify Expiration Date text field");
		}
		return this;
	}

	/**
	 * Verify state drop down field
	 * 
	 * @return
	 */
	public CheckOutPage verifyVerifyIDStatefield() {

		try {
			Assert.assertTrue(SSNNumberCreditCheck.isDisplayed(), "SSN text field is not displayed");
			Reporter.log("SSN text field is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify SSN text field");
		}
		return this;
	}

	/**
	 * Verify Next CTA on Verify ID page
	 * 
	 * @return
	 */
	public CheckOutPage verifyNextCTAfield() {

		try {
			Assert.assertTrue(verifyIDNextButton.isDisplayed(), "Next button is not displayed");
			Reporter.log("Next button is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify Next button");
		}
		return this;
	}

	/**
	 * Verify Existing Customer Header
	 *
	 */
	public CheckOutPage verifyExistingCustomerHeader() {

		try {
			waitFor(ExpectedConditions.visibilityOf(existingCustomerHeader), 30);
			Assert.assertTrue(existingCustomerHeader.isDisplayed(), "Existing Customer Header is not displayed");
			Assert.assertTrue(existingCustomerHeader.getText().contains("already a T-Mobile customer"),
					"Existing Customer Header is not displayed");
			Reporter.log("Existing Customer Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Existing Customer Header");
		}
		return this;
	}

	/**
	 * Verify Login Button on Existing Customer Page
	 * 
	 * @return
	 */
	public CheckOutPage verifyLoginCTAOnExistingCustomer() {
		try {
			Assert.assertTrue(loginbuttonOnExistingCustomer.isDisplayed(),
					"Login Button on Existing Customer Page is not displayed");
			Reporter.log("Login Button on Existing Customer Page is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify Login Button on Existing Customer Page");
		}
		return this;
	}

	/**
	 * click Login Button on Existing Customer Page
	 * 
	 * @return
	 */
	public CheckOutPage clickLoginCTAOnExistingCustomer() {
		try {
			clickElementWithJavaScript(loginbuttonOnExistingCustomer);
			Reporter.log("Clicked on Login Button on Existing Customer Page");

		} catch (Exception e) {
			Assert.fail("Failed to click on Login Button on Existing Customer Page");
		}
		return this;
	}

	/**
	 * Verify Close Button on Existing Customer Page
	 * 
	 * @return
	 */
	public CheckOutPage verifyCloseCTAOnExistingCustomer() {
		try {
			Assert.assertTrue(closeIconOnExistingCustomer.isDisplayed(),
					"Close Button on Existing Customer Page is not displayed");
			Reporter.log("Close Button on Existing Customer Page is displayed");
			;
		} catch (Exception e) {
			Assert.fail("Failed to verify Close Button on Existing Customer Page");
		}
		return this;
	}

	/**
	 * click Close Button on Existing Customer Page
	 * 
	 * @return
	 */
	public CheckOutPage clickCloseCTAOnExistingCustomer() {
		try {
			clickElementWithJavaScript(closeIconOnExistingCustomer);
			Reporter.log("Clicked on close Button on Existing Customer Page");

		} catch (Exception e) {
			Assert.fail("Failed to click on close Button on Existing Customer Page");

		}
		return this;

	}

	/**
	 * Verify Error Message For DOB when entered DOB < 18 years
	 *
	 */
	public CheckOutPage verifyErrorMessageForDOBLessThan18Years() {

		try {
			waitFor(ExpectedConditions.visibilityOf(dobErrorMessage), 30);
			Assert.assertTrue(dobErrorMessage.getText().equalsIgnoreCase("You must be at least 18 years of age."),
					"Less than 18 years validation message not displayed");
			Reporter.log("Verified Less than 18 years validation message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Less than 18 years validation message");

		}
		return this;
	}

	/**
	 * Verify Shipping Date displayed and Format
	 * 
	 * @return
	 */
	public CheckOutPage verifyShippingDateDisplayedAndFormat() {
		try {
			checkPageIsReady();
			Assert.assertTrue(shippingdateFrom.isDisplayed(), "Shipping Date from is not displayed");
			Assert.assertTrue(shippingDateTo.isDisplayed(), "Shipping Date to is not displayed");
			String[] DateFromArr = shippingdateFrom.getText().split(":");
			String[] DateFromArr2 = DateFromArr[1].split("-");
			String DateFrom = DateFromArr2[1].replace(" ", "");
			String[] DateToArr = shippingdateFrom.getText().split("-");
			String DateTo = DateToArr[1].replace(" ", "");
			Assert.assertTrue(DateFrom.matches("^(0*([1-9]|1[0-2]))/(0*([1-9]|[12][0-9]|3[01]))$"),
					"From Date is not in format");
			Assert.assertTrue(DateTo.matches("^(0*([1-9]|1[0-2]))/(0*([1-9]|[12][0-9]|3[01]))$"),
					"To Date is not in format");
			Reporter.log("Verified Shipping Date displayed and Format");
		} catch (Exception e) {
			Assert.fail("Failed to verify Shipping Date displayed and Format");
		}
		return this;
	}

	/**
	 * Verify Credit Check Order Modal Header
	 *
	 */
	public CheckOutPage verifyOrderModalForCreditCheckHeader(TMNGData tmngData) {

		try {
			waitFor(ExpectedConditions.visibilityOf(creditCheckOrderModal), 60);
			Assert.assertTrue(creditCheckOrderModalHeader.isDisplayed(),
					"Credit Check Order Modal Header is not displayed");
			Assert.assertTrue(creditCheckOrderModalHeader.getText().contains(tmngData.getFirstName()),
					"Credit Check is not displayed");
			Reporter.log("Credit Check Order Modal Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Credit Check Order Modal Header");
		}
		return this;
	}

	/**
	 * Verify Credit Check OrderTotal Pricing for Order Modal
	 *
	 */
	public CheckOutPage verifyOrderTotalPricingForOrderModal() {

		try {
			Assert.assertTrue(totalTodayAtCreditOrderModal.isDisplayed(),
					"OrderTotal Pricing for Order Modal is not displayed");
			Reporter.log("OrderTotal Pricing for Order Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify OrderTotal Pricing for Order Modal");
		}
		return this;
	}

	/**
	 * Get Credit Check OrderTotal Pricing for Order Modal
	 *
	 */
	public String getTodayTotalPricingAtCreditCheckModal() {
		String totalTodayAtCreditCheckModal = "";
		try {
			totalTodayAtCreditCheckModal = totalTodayAtCreditOrderModal.getAttribute("aria-label").split(" ")[0]
					.replace("$", "");
			Reporter.log("Get Credit Check OrderTotal Pricing for Order Modal");
		} catch (Exception e) {
			Assert.fail("Failed to Get Credit Check OrderTotal Pricing for Order Modal");
		}
		return totalTodayAtCreditCheckModal;
	}

	/**
	 * Verify Credit Check Order Monthly Pricing for Order Modal
	 *
	 */
	public CheckOutPage verifyOrderMonthlyPricingForOrderModal() {

		try {
			Assert.assertTrue(totalMonthlyAtCreditOrderModal.isDisplayed(),
					"Order Monthly Pricing for Order Modal is not displayed");
			Reporter.log("Order Monthly Pricing for Order Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Order Monthly Pricing for Order Modal");
		}
		return this;
	}

	/**
	 * Get Credit Check Order Monthly Pricing for Order Modal
	 *
	 */
	public String getMonthlyTotalPricingAtCreditCheckModal() {
		String totalMonthlyAtCreditModal = "";
		try {
			totalMonthlyAtCreditModal = totalMonthlyAtCreditOrderModal.getAttribute("aria-label").split(" ")[0]
					.replace("$", "");
			Reporter.log("Get Credit Check Order Monthly Pricing for Order Modal");
		} catch (Exception e) {
			Assert.fail("Failed to Get Credit Check Order Monthly Pricing for Order Modal");
		}
		return totalMonthlyAtCreditModal;
	}

	/**
	 * Verify Credit Check EIP term Under Monthly Order Modal
	 *
	 */
	public CheckOutPage verifyEIPTermUnderMonthlyOrderModal() {

		try {
			Assert.assertTrue(EIPTermUnderMonthly.isDisplayed(),
					"EIP term Under Monthly for Order Modal is not displayed");
			Reporter.log("EIP term Under Monthly for Order Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP term Under Monthly for Order Modal");
		}
		return this;
	}

	/**
	 * Verify Dynamic Text You will now see Verified Pricing Order Modal
	 *
	 */
	public CheckOutPage verifyDynamicTextYouWillNowSeeVerifiedPricingOrderModal() {

		try {
			Assert.assertTrue(textYoullNowSeeVerifiedPricing.isDisplayed(),
					"Dynamic Text You will now see Verified Pricin for Order Modal is not displayed");
			Reporter.log("Dynamic Text You will now see Verified Pricin for Order Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Dynamic Text You will now see Verified Pricin for Order Modal");
		}
		return this;
	}

	/**
	 * Verify Check out Now CTA on Credit Check Order Modal
	 *
	 */
	public CheckOutPage verifyCheckOutNowCTAOrderModal() {

		try {
			Assert.assertTrue(checkOutNowCTA.isDisplayed(),
					"Check out Now CTA on Credit Check Order Modal is not displayed");
			Reporter.log("Check out Now CTA on Credit Check Order Modal is Verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Check Out Now CTA Credit Check Order Modal");
		}
		return this;
	}

	/**
	 * Verify Chat With us CTA on Credit Check Order Modal
	 *
	 */
	public CheckOutPage verifyChatWithUsCTAOrderModal() {

		try {
			Assert.assertTrue(chatWithUsCTAOnOrderModal.isDisplayed(),
					"Chat With us CTA on Credit Check Order Modal is not displayed");
			Reporter.log("Chat With us CTA on Credit Check Order Modal is Verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Chat With us CTA Credit Check Order Modal");
		}
		return this;
	}

	/**
	 * Verify Edit Your Cart CTA on Credit Check Order Modal
	 *
	 */
	public CheckOutPage verifyEditYourCartCTAOrderModal() {

		try {
			Assert.assertTrue(editYourCartCTAOnOrderModal.isDisplayed(),
					"Edit Your Cart CTA on Credit Check Order Modal is not displayed");
			Reporter.log("Edit Your Cart CTA on Credit Check Order Modal is Verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit Your Cart CTA Credit Check Order Modal");
		}
		return this;
	}

	/**
	 * Verify Tpp Fraud Modal
	 * 
	 * @return
	 */
	public CheckOutPage verifyTppFraudModal() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(tppFraudModal), 60);
			Assert.assertTrue(tppFraudModal.isDisplayed(), "Fraud Modal is not displayed");
			Reporter.log("Fraud Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display fraud modal");
		}
		return this;
	}

	/**
	 * Verify Tpp Fraud Review Modal Header
	 * 
	 * @return
	 */
	public CheckOutPage verifyTppFraudReviewModalHeader() {
		try {
			waitforSpinner();
			Assert.assertTrue(fraudReviewModalHeader.isDisplayed(), "Fraud Review Modal Header is not displayed");
			Reporter.log("Fraud Review Modal Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Fraud Review Modal Header");
		}
		return this;
	}

	/**
	 * Verify Tpp Fraud Review Modal Text
	 * 
	 * @return
	 */
	public CheckOutPage verifyTppFraudReviewModalText() {
		try {
			waitforSpinner();
			Assert.assertTrue(fraudReviewModalText.isDisplayed(), "Fraud Review Modal Text is not displayed");
			Reporter.log("Fraud Review Modal Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Fraud Review Modal Text");
		}
		return this;
	}

	/**
	 * Verify Call 1800 CTA
	 * 
	 * @return
	 */
	public CheckOutPage verifyCallCta() {
		try {
			waitforSpinner();
			Assert.assertTrue(callCta.isDisplayed(), "Call CTA is not displayed");
			Reporter.log("Call CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Call CTA");
		}
		return this;
	}

	/**
	 * Verify visit store CTA
	 * 
	 * @return
	 */
	public CheckOutPage verifyStoreVisitCta() {
		try {
			waitforSpinner();
			Assert.assertTrue(storeVisitCta.isDisplayed(), "Store Visit CTA is not displayed");
			Reporter.log("Store Visit CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Store Visit");
		}
		return this;
	}

	/**
	 * Click on fraud modal close CTA
	 * 
	 * @return
	 */
	public CheckOutPage clickOnFraudModalCloseCTA() {
		try {
			if((getDriver() instanceof AppiumDriver)) {
				waitFor(ExpectedConditions.visibilityOf(fraudModalCloseIconMobile),60);
				fraudModalCloseIconMobile.click();
			}else {
				waitFor(ExpectedConditions.visibilityOf(fraudModalCloseIcon),60);
				clickElementWithJavaScript(fraudModalCloseIcon);
				
			}
			Reporter.log("Fraud Modal Close CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Fraud Modal Close CTA");
		}
		return this;
	}

	/**
	 * Verify Line total at Check Out
	 */
	public CheckOutPage verifyLineTotalCheckout() {

		try {

			Double lineTotalAmount = Double.parseDouble(lineTotalPrice.getAttribute("left-amount"));

			double sum = 0;
			for (int i = 0; i < lineItems.size(); i++) {

				Double maskedElement = Double.parseDouble(lineItems.get(i).getAttribute("left-amount"));
				scrollDownToelement(lineItems.get(i));
				sum = sum + maskedElement;

			}

			String oneTime;
			oneTime = oneTimeFees.getText();
			String[] arr = oneTime.split("\\$", 0);
			Double OTF = Double.parseDouble(arr[1]);

			String sales;
			sales = salesTax.getText();
			String[] arr1 = sales.split("\\$", 0);
			Double salesT = Double.parseDouble(arr1[1]);

			String shipping;
			shipping = shippingAmount.getText();
			String[] arr2 = shipping.split("\\$", 0);
			Double shippingT = Double.parseDouble(arr2[1]);

			Double calculateTotal = (double) (Math.round((OTF + salesT + shippingT + sum)));

			scrollDownToelement(lineTotalPrice);
			Double lineTotal = (double) Math.round(lineTotalAmount);

			Assert.assertEquals(calculateTotal, lineTotal, "Line total is not equal");
			Reporter.log("Line total is Equal");
		} catch (Exception e) {
			Assert.fail("failed to check Line total Amount");
		}
		return this;
	}

	/**
	 * Verify Name On Card Is displayed and Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyNameOnCardIsDisplayedAndEditable() {
		try {
			waitforSpinner();
			Assert.assertTrue(nameOnCard.getAttribute("class").contains("ng-not-empty"),
					"Name on Card is not displayed");
			Assert.assertTrue(nameOnCard.isEnabled(), "Name on Card is not editable");
			Assert.assertTrue(nameOnCard.getAttribute("role").equals("textbox"), "Name on Card is not editable");
			Reporter.log("Name on Card is displayed and is editable");
		} catch (Exception e) {
			Assert.fail("Failed to display Name on Card and check editable");
		}
		return this;
	}

	/**
	 * Click Caret Icon on Order Modal
	 *
	 */
	public CheckOutPage clickCaretIconToExpandAllLine() {

		try {
			waitFor(ExpectedConditions.elementToBeClickable(caretIconOnOrderModal.get(0)), 60);
			for (WebElement caret : caretIconOnOrderModal) {
				clickElementWithJavaScript(caret);
			}
			Reporter.log("Clicked on Caret Icon on Order Modal");
		} catch (Exception e) {
			Assert.fail("Failed to clicked on Caret Icon on Order Modal");
		}
		return this;
	}

	/**
	 * Verify Plan Name On Order Modal
	 *
	 */
	public CheckOutPage verifyPlanNameOnOrderModalForAllLine() {
		try {
			for (WebElement planName : planNameOnOrderModal) {
				Assert.assertTrue(planName.isDisplayed(), "Plan Name on Ordar Modal is not displayed");
			}
			Reporter.log("Plan Name On Order Modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Plan Name On Order Modal");
		}
		return this;
	}

	/**
	 * Get Monthly price of device on order modal
	 */
	public float getMonthlyPriceForDeviceAndTablet() {
		float finalPrice = 0;
		float monthlyPriceDevice = 0;
		try {
			monthlyPriceDevice = Float
					.parseFloat(monthlyPriceOnOrderModal.get(0).getText().replace("$", " ").replace(" ", ""));
			// monthlyPriceDevice1 =
			// Float.parseFloat(monthlyPriceOnOrderModal.get(0).getText().replace("$",
			// " ").replace(" ", ""));
			for (WebElement price : monthlyPriceOnOrderModal) {
				monthlyPriceDevice = Float.parseFloat(price.getText().replace("$", " ").replace(" ", ""));
				finalPrice = monthlyPriceDevice + finalPrice;
			}
			Reporter.log("Monthly price of device on order modal is " + monthlyPriceDevice);
		} catch (Exception e) {
			Assert.fail("Monthly price of device on order modal isn't available");
		}
		return finalPrice;
	}

	/**
	 * Get Monthly price of Plan on order modal
	 */
	public float getMonthlyPriceForPlan() {
		float monthlyPricePlan = 0;
		try {
			monthlyPricePlan = Float
					.parseFloat(monthlyPriceOnOrderModal.get(1).getText().replace("$", " ").replace(" ", ""));
			Reporter.log("Monthly price of plan on order modal is " + monthlyPricePlan);
		} catch (Exception e) {
			Assert.fail("Monthly price of plan on order modal isn't available");
		}
		return monthlyPricePlan;
	}

	/**
	 * Get total Monthly price of Plan and device on order modal
	 */
	public float gettotalMonthlyPricingPlanAndDeviceOnOrderModal() {
		float monthlyPricePlan = 0;
		try {
			monthlyPricePlan = Float
					.parseFloat(totalMonthlyPricingOnOrderModal.getText().replace("$", " ").replace(" ", ""));
			Reporter.log("total Monthly price of Plan and device on order modal is " + monthlyPricePlan);
		} catch (Exception e) {
			Assert.fail("total Monthly price of Plan and device on order modal isn't available");
		}
		return monthlyPricePlan;
	}

	/**
	 * Compare total Price of Accessory in Cart and CheckOutPage
	 * 
	 */
	public CheckOutPage compareTwoFloatValues(Float price1, Float Price2) {
		try {

			Assert.assertEquals(price1, Price2, "Both Float values does not match");
			Reporter.log("Both Float values matched");
		} catch (Exception e) {
			Assert.fail("Failed to compare both float values");
		}
		return this;
	}

	/**
	 * verify Confirmation header is present
	 *
	 */
	public CheckOutPage verifyConfirmationPageHeader() {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(confirmationHeader), 60);
			Assert.assertTrue(confirmationHeader.isDisplayed(), "Confirmation Header is displayed");
			Reporter.log("Confirmation Page is present and header is displayed");
		} catch (AssertionError e) {
			Assert.fail("Failed to verify Confirmation Header");
		}
		return this;
	}

	/**
	 * Verify Sales Tax calculation
	 * 
	 * 
	 */
	public CheckOutPage verifySalesTaxCalculation() {
		try {
			waitforSpinner();
			Assert.assertTrue(salesTax.isDisplayed(), "Sales Tax calculation is not displayed");
			Reporter.log("Sales Tax calculation is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Sales Tax calculation");
		}
		return this;
	}

	/**
	 * Verify total today amount calculation
	 * 
	 * @return
	 */
	public CheckOutPage verifyTotalTodayCalculation() {
		try {
			waitforSpinner();
			Assert.assertTrue(totalTodayAmount.isDisplayed(), "total today amount calculation is not displayed");
			Reporter.log("total today amount calculation is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display total today amount calculation");
		}
		return this;
	}

	/**
	 * Verify Today Down Payment display on Sticky Banner
	 * 
	 * @return
	 */
	public CheckOutPage verifyTotalTodayAmount() {
		try {
			Assert.assertFalse(totalTodayAmount.getAttribute("left-amount").isEmpty(),
					"Total Down Payment is not displayed");
			Reporter.log("Today Down Payment at check out is displayed");
		} catch (Exception e) {
			Assert.fail("Today Down Payment at checkout Page isn't available");
		}
		return this;
	}

	/**
	 * Verify Monthly Payment display on Sticky Banner
	 * 
	 * @return
	 */
	public CheckOutPage verifyMonthlyAmount() {
		try {
			Assert.assertFalse(totalTodayAmount.getAttribute("right-amount").isEmpty(),
					"Monthly amount is not displayed");
			Reporter.log("Monthly amount at check out is displayed " /* + totalTodayPrice */);
		} catch (Exception e) {
			Assert.fail("Monthly amount at checkout Page isn't available");
		}
		return this;
	}

	/**
	 * Verify First Name Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyFirstnameEditable() {
		try {
			waitforSpinner();
			Assert.assertTrue(firstName.getAttribute("class").contains("ng-empty"), "First Name is Not Editable");
			Reporter.log("First Name isEditable");
		} catch (Exception e) {
			Assert.fail("Failed to verify First Name Field");
		}
		return this;
	}

	/**
	 * Verify Last Name Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyLastnameEditable() {
		try {
			waitforSpinner();
			Assert.assertTrue(lastName.getAttribute("class").contains("ng-empty"), "Last Name is Not Editable");
			Reporter.log("Last Name is Editable");
		} catch (Exception e) {
			Assert.fail("Failed to verify Last Name Field");
		}
		return this;
	}

	/**
	 * Verify Middle Name Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyMiddlenameEditable() {
		try {
			waitforSpinner();
			Assert.assertTrue(middleName.getAttribute("class").contains("ng-empty"), "Middle Name is Not Editable");
			Reporter.log("Middle Name is Editable");
		} catch (Exception e) {
			Assert.fail("Failed to verify Middle Name Field");
		}
		return this;
	}

	/**
	 * Verify Email Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyEmailEditable() {
		try {
			waitforSpinner();
			Assert.assertTrue(email.getAttribute("class").contains("ng-not-empty"), "Email is Not Editable");
			Reporter.log("Email is Editable");
		} catch (Exception e) {
			Assert.fail("Failed to verify Email Field");
		}
		return this;
	}

	/**
	 * Verify Phone number is null
	 * 
	 * @return
	 */
	public CheckOutPage verifyPhoneNumberIsNull() {
		try {
			waitforSpinner();
			Assert.assertTrue(phoneNumber.getText().contains(""), "Phone number is not null");
			Reporter.log("Phone number is null");
		} catch (Exception e) {
			Assert.fail("Failed to verify Phone number Field");
		}
		return this;
	}

	/**
	 * Verify Error Message For SSN mismatch
	 *
	 */
	public CheckOutPage verifyErrorMessageForSSNMismatch() {

		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertTrue(
					errorSSNMismatch.getText().equalsIgnoreCase("Last 4 of SSN does not match previous entry"),
					"SSN mismatch error message not displayed");
			Reporter.log("SSN mismatch error message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify SSN mismatch error validation message");

		}
		return this;
	}

	/**
	 * Verify Next Button Disabled on Personal Info Tab
	 */
	public CheckOutPage verifyNextBtnDisabled() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertFalse(nextBtnTPP.isEnabled(), "Next Button is Enabled for Personal Info tab");
			Reporter.log("Next Button is not enabled for Personal Info tab");
		} catch (Exception e) {
			Assert.fail("Failed to verify Next button for Personal Info tab");
		}
		return this;
	}

	/**
	 * Fill DOB details for Credit Check Less than 18 years
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillDOBforCreditCheckLessThan18Years() {

		try {
			checkPageIsReady();
			DOBCreditCheck.clear();
			DOBCreditCheck.sendKeys("06222006");

			Reporter.log("Filled DOB Details");

		} catch (Exception e) {
			Assert.fail("Failed to fill DOB info");
		}
		return this;
	}

	/**
	 * Verify First Name is pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyFirstnamePrePopulated() {
		try {
			waitforSpinner();
			Assert.assertTrue(firstName.getAttribute("class").contains("ng-not-empty"),
					"First Name is not pre-populated");
			Reporter.log("First Name is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify First Name Field");
		}
		return this;
	}

	/**
	 * Verify Last Name pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyLastnamePrePopulated() {
		try {
			waitforSpinner();
			Assert.assertTrue(lastName.getAttribute("class").contains("ng-not-empty"),
					"Last Name is not pre-populated");
			Reporter.log("Last Name is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify Last Name Field");
		}
		return this;
	}

	/**
	 * Verify Email pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyEmailPrePopulated() {
		try {
			waitforSpinner();
			Assert.assertTrue(email.getAttribute("class").contains("ng-not-empty"), "Email is Not pre-populated");
			Reporter.log("Email is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify Email Field");
		}
		return this;
	}

	/**
	 * Verify Phone number is pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyPhoneNumberPrePopulated() {
		try {
			waitforSpinner();
			Assert.assertTrue(phoneNumber.getAttribute("class").contains("ng-not-empty"),
					"Phone number is not pre-populated");
			Reporter.log("Phone number is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify Phone number field");
		}
		return this;
	}

	/**
	 * Verify Address Line1r is pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyAddressLine1PrePopulated() {
		try {
			Assert.assertTrue(addressLine1RACreditCheck.getAttribute("class").contains("ng-not-empty"),
					"Address Line1 is not pre-populated");
			Reporter.log("AddressLine1 is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify AddressLine1");
		}
		return this;
	}

	/**
	 * Verify City is pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyCityPrePopulated() {
		try {
			Assert.assertTrue(cityRACreditCheck.getAttribute("class").contains("ng-not-empty"),
					"City is not pre-populated");
			Reporter.log("City is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify City");
		}
		return this;
	}

	/**
	 * Verify Zipcode is pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyZipcodePrePopulated() {
		try {
			Assert.assertTrue(zipCodeRACreditCheck.getAttribute("class").contains("ng-not-empty"),
					"Zipcode is not pre-populated");
			Reporter.log("Zipcode is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify Zipcode");
		}
		return this;
	}

	/**
	 * Verify State is pre-populated
	 * 
	 * @return
	 */
	public CheckOutPage verifyStatePrePopulated() {
		try {
			Assert.assertTrue(stateRACreditCheck.getAttribute("class").contains("ng-not-empty"),
					"State is not pre-populated");
			Reporter.log("State is pre-populated");
		} catch (Exception e) {
			Assert.fail("Failed to verify State");
		}
		return this;
	}

	/**
	 * 
	 * Verify Sim price on review order page
	 */
	public CheckOutPage verifyMiSimKitPriceDisplayed() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(miSimKitPrice), 60);
			Assert.assertTrue(miSimKitPrice.isDisplayed(), "Mi Sim kit price Not Displayed");
			Reporter.log("Mi Sim kit price Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Mi Sim kit price");
		}
		return this;
	}

	/**
	 * 
	 * Verify MI Sim Kit name displayed
	 */
	public CheckOutPage verifyMiSimKitNameDisplayed() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(miSimKitName), 60);
			Assert.assertTrue(miSimKitName.isDisplayed(), "MI Sim Kit name is not displayed");
			Reporter.log("MI Sim kit name is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display MI Sim Kit name");
		}
		return this;
	}

	/**
	 * Click Caret Icon on Order Modal
	 *
	 */
	public CheckOutPage clickCaretIconToExpandLine() {

		try {
			waitFor(ExpectedConditions.elementToBeClickable(caretIconOnOrderModal.get(0)), 60);
			clickElementWithJavaScript(caretIconOnOrderModal.get(0));
			Reporter.log("Clicked on Caret Icon on Order Modal");
		} catch (Exception e) {
			Assert.fail("Failed to clicked on Caret Icon on Order Modal");
		}
		return this;
	}

	/**
	 * 
	 * verify Sku Monthly and Today price is displayed
	 */
	public CheckOutPage verifySkuPricingOnOrderModal() {
		try {
			waitforSpinner();
			Assert.assertTrue(monthlyPriceOnOrderModal.get(0).getText().contains("$"),
					"Sku Monthly price is not displayed");
			Assert.assertTrue(todayPriceOnOrderModal.get(0).getText().contains("$"),
					"Sku Today price is not displayed");
			Reporter.log("Sku Monthly and Today price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sku Monthly and Today price is displayed");
		}
		return this;
	}

	/**
	 * 
	 * verify Sku Plan Monthly and Today price is displayed
	 */
	public CheckOutPage verifySkuPlanPricingOnOrderModal() {
		try {
			waitforSpinner();
			Assert.assertTrue(monthlyPriceOnOrderModal.get(1).getText().contains("$"),
					"Sku Plan Monthly price is not displayed");
			Assert.assertTrue(todayPriceOnOrderModalEmpty.get(0).getText().equals(""),
					"Sku Plan Today price is not displayed");
			Reporter.log("Sku plan Monthly and Today price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sku Plan Monthly and Today price is displayed");
		}
		return this;
	}

	/**
	 * Click details Icon on pricing Modal
	 *
	 */
	public CheckOutPage clickDetailsLinkOnPricingModal() {

		try {
			waitFor(ExpectedConditions.elementToBeClickable(detailsLinkOnPricingModal), 60);
			clickElementWithJavaScript(detailsLinkOnPricingModal);
			Reporter.log("Clicked on details link on Pricing Modal");
		} catch (Exception e) {
			Assert.fail("Failed to click on details link on Pricing Modal");
		}
		return this;
	}

	/**
	 * 
	 * Verify Sim Kit name displayed on pricing modal
	 */
	public CheckOutPage verifySimKitNameDisplayedOnPricingModal() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(simNameOnPricingModal), 60);
			Assert.assertTrue(simDetailsOnPricingModal.get(0).isDisplayed(),
					"Sim Kit name is not displayed on pricing modal");
			Reporter.log(" Sim kit name is displayed on pricing modal");
		} catch (Exception e) {
			Assert.fail("Failed to display Sim Kit name on pricing modal");
		}
		return this;
	}

	/**
	 * 
	 * Verify Sim Kit price displayed on pricing modal
	 */
	public CheckOutPage verifySimKitPriceDisplayedOnPricingModal() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(simNameOnPricingModal), 60);
			Assert.assertTrue(simDetailsOnPricingModal.get(1).isDisplayed(),
					"Sim Kit price is not displayed on pricing modal");
			Reporter.log(" Sim kit price is displayed on pricing modal");
		} catch (Exception e) {
			Assert.fail("Failed to display Sim Kit price on pricing modal");
		}
		return this;
	}

	/**
	 * Get total Price for SIM on review Order page
	 * 
	 * @return
	 */
	public Double getTotalSimPriceOnReviewOrder() {
		Double totalSimPrice = 0.0;
		try {
			CheckOutPage checkOutPage = new CheckOutPage(getDriver());
			Double V2 = checkOutPage.getReviewSubmitSalesTaxPrice();
			Double V3 = checkOutPage.getReviewSubmitShippingPrice();
			Double V4 = checkOutPage.getReviewSubmitSimPrice();
			totalSimPrice = V2 + V3 + V4;
			Reporter.log("Sim Price at review order page is " + totalSimPrice);
		} catch (Exception e) {
			Assert.fail("Sim Price At Review Order Page isn't available");
		}
		return (totalSimPrice);
	}

	/**
	 * Get Total today price on review order page
	 * 
	 * @return
	 */
	public Double getReviewSubmitTotalTodayPrice() {
		String todayprice = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(totalTodayAmount), 60);
			todayprice = totalTodayAmount.getAttribute("left-amount");
			Reporter.log("Today Price at check out page is " + todayprice);
		} catch (Exception e) {
			Assert.fail("Today Price At CheckOut Page isn't available");
		}
		return Double.parseDouble(todayprice);
	}

	/**
	 * Get Total shipping price on review order page
	 * 
	 * @return
	 */
	public Double getReviewSubmitShippingPrice() {
		String shipping = null;
		String shippingPrice = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(reviewSubmitShippingPrice), 60);
			shipping = reviewSubmitShippingPrice.getText();
			shippingPrice = shipping.substring(shipping.lastIndexOf("$") + 1);
			Reporter.log("Shipping Price at check out page is " + shipping);
		} catch (Exception e) {
			Assert.fail("Shipping price At CheckOut Page isn't available");
		}
		return Double.parseDouble(shippingPrice);
	}

	/**
	 * 
	 * Complete ChekOutProcess For Devices for HCC User
	 */
	public CheckOutPage completeCheckOutProcessForDevicesHCC(TMNGData tMNGData) {
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();

		return this;
	}

	/**
	 * 
	 * Complete ChekOutProcess For Devices for SCC User
	 */
	public CheckOutPage completeCheckOutProcessForDevicesSCC(TMNGData tMNGData) {
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfoAfterpreScreen(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyVerifyIdentityPage();
//		checkOutPage.fillSSN(tMNGData);
		sendKeysWithWait(SSNNumberCreditCheck, tMNGData.getSSN());
		while (!SSNNumberCreditCheck.getAttribute("value").replace("-", "").equals(tMNGData.getSSN())) {
			SSNNumberCreditCheck.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
			SSNNumberCreditCheck.sendKeys(tMNGData.getSSN());
		}
		checkOutPage.fillIdentityVerificationForVerifyID(tMNGData);
		checkOutPage.clickRunCreditCheck();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();

		return this;
	}

	/**
	 * Verify sim Starter Kit Text is Not displayed
	 *
	 */
	public CheckOutPage verifySimStarterKitTextNotDisplayed() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertFalse(isElementDisplayed(miSimKitName), "Sim Starter Kit Text is Displayed");
			Reporter.log("Sim Starter Kit Text is Not Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Sim Starter Kit Text.");
		}
		return this;
	}

	/**
	 * Click View Order details Check out
	 * 
	 * @return
	 */
	public CheckOutPage clickViewOrderDetailsCheckout() {
		try {
			checkPageIsReady();
			JavascriptExecutor executor = (JavascriptExecutor) getDriver();
			executor.executeScript("arguments[0].click();", viewOrderDetailsCheckout);
			Reporter.log("order details is clickable on check out page .");
		} catch (Exception e) {
			Assert.fail("order details is not clickable on check out page ");
		}
		return this;
	}

	/**
	 * Verify view order details modal is displayed
	 *
	 */
	public CheckOutPage verifyViewOrderDetailsModalDisplayed() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(orderDetailsModal), "Order modal is not diplayed");
			Reporter.log("View order details Modal is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display View order details Modal.");
		}
		return this;
	}

	/**
	 * verify total finance amount for EIP
	 *
	 */
	public CheckOutPage verifyTotalFinanceAmount() {
		try {
			Assert.assertTrue(totalFinanceAmount.isDisplayed(), "Total finance amount is not displayed");
			Assert.assertTrue(totalFinanceAmountText.isDisplayed(), "Total finance amount text is not displayed");
			Reporter.log("Total finance amount text and amount are displayed");
		} catch (AssertionError e) {
			Assert.fail("Failed to verify Total finance amount text and amount On order detailed page for EIP");

		}
		return this;
	}

	/**
	 * Total finance amount valuation for EIP
	 *
	 */
	public CheckOutPage financeAmountValuationForEIP() {
		try {
			waitFor(ExpectedConditions.visibilityOf(totalEIPFinanceAmount), 60);
			String totalEIPmonthsString = totalEIPFinanceAmount.getAttribute("left-text");
			String amountPerMonthString = totalEIPFinanceAmount.getAttribute("left-amount");
			String totalFinancedAmountDisplayedStr = totalFinanceAmount.getText();
			totalEIPmonthsString = totalEIPmonthsString.substring(totalEIPmonthsString.lastIndexOf("X") + 2,
					totalEIPmonthsString.lastIndexOf("m") - 1);
			String eipMonthsArray[] = totalEIPmonthsString.split("x");
			String eipMonthsStr = eipMonthsArray[1];
			double eipMonths = Double.parseDouble(eipMonthsStr.trim());
			double eipAmountPerMonth = Double.parseDouble(amountPerMonthString.trim());
			double totalFinancedAmountDisplayed = Double.parseDouble(totalFinancedAmountDisplayedStr);
			double totalFinacedAmountCalilulated = (double) (Math.round((eipMonths * eipAmountPerMonth) * 100) / 100);
			Assert.assertEquals(totalFinacedAmountCalilulated, totalFinancedAmountDisplayed,
					"Total finance amount not displayed");

		} catch (AssertionError e) {
			Assert.fail("Total finance amount not displayed ");

		}
		return this;
	}

	/**
	 * verify total finance amount for FRP
	 *
	 */
	public CheckOutPage verifyTotalFinanceAmountForFRP() {
		try {
			Assert.assertFalse(isElementDisplayed(totalFinanceAmount), "Total finance amount displayed");
			Reporter.log("Total Financial amount is not displayed on order detailed page for FRP");
		} catch (AssertionError e) {
			Assert.fail("Failed to verify Total finance amount on FRP");
		}
		return this;
	}

	/**
	 * Verify View EIP Price Check Out
	 */
	public CheckOutPage verifyViewEIPPriceCheckout(Double eIPPrice, Double monthlyPrice) {
		try {
			waitForSpinnerInvisibility();
			Double totalEIPPrice = Double.parseDouble(orderCheckoutEIPPrice.getAttribute("left-amount"));
			Double totalMonthlyPrice = Double.parseDouble(orderCheckoutEIPPrice.getAttribute("right-amount"));
			Assert.assertEquals(eIPPrice, totalEIPPrice);
			Assert.assertEquals(monthlyPrice, totalMonthlyPrice,
					"failed to  check   details total amount is not visble ");
			Reporter.log("On check out  details total amount is visble ");
		} catch (Exception e) {
			Assert.fail("failed to  check   details total amount is not visble ");
		}
		return this;
	}

	/**
	 * Close Order Details modal
	 *
	 */
	public CheckOutPage clickCloseOrderDetailsModalIcon() {
		try {
			clickElementWithJavaScript(orderDetailsCloseIcon);
			Reporter.log("Clicked on Order Details close icon");
		} catch (Exception e) {
			Assert.fail("Failed to click on Close icon");
		}
		return this;
	}

	/**
	 * 
	 * verify Sku Plan Monthly and Today price is displayed
	 */
	public CheckOutPage verifyAccessoriesLineSuppressedOnOrderModal(int count) {
		try {
			Assert.assertEquals(todayPriceOnOrderModal.size(), count, "Accessories Line does not have Monthly price");
			Assert.assertEquals(monthlyPriceOnOrderModal.size(), count, "Accessories Line does not have Monthly price");
			Reporter.log("Accessories lIne is suppressed");
		} catch (Exception e) {
			Assert.fail("Failed to verify if accessries line is suppressed");
		}
		return this;
	}

	/**
	 * Verify sim Starter Kit Text is displayed
	 *
	 */
	public CheckOutPage verifySimStarterKitTextDisplayed() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(isElementDisplayed(simStarterKitText), "Sim Starter Kit Text is not Displayed");
			Reporter.log("Sim Starter Kit Text is Displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to display Sim Starter Kit Text.");
		}
		return this;
	}

	/**
	 * Get Total Monthly Device Price
	 * 
	 * @return
	 */
	public Double getTotalMonthlyPricePricingModal() {
		Double devicePriceDigit = null;
		try {
			String totalmonthlyDevicePrice;
			totalmonthlyDevicePrice = lineTotalPriceForPricingModal.getAttribute("aria-label");
			String devicePrice = totalmonthlyDevicePrice.substring(totalmonthlyDevicePrice.lastIndexOf("$") + 1);
			devicePriceDigit = Double.parseDouble(devicePrice);
			Reporter.log("Total Monthly Price Retrieved ");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve total Monthly Price");
		}
		return devicePriceDigit;
	}

	/**
	 * Get Total DownPayment Device Price
	 * 
	 * @return
	 */
	public Double getTotalDownPaymentPricePricingModal() {
		Double devicePriceDigit = null;
		try {
			String totaldownPaymentDevicePrice;
			totaldownPaymentDevicePrice = lineTotalPriceForPricingModalToday.getAttribute("aria-label");
			String devicePrice = totaldownPaymentDevicePrice
					.substring(totaldownPaymentDevicePrice.lastIndexOf("$") + 1);
			devicePriceDigit = Double.parseDouble(devicePrice);
			Reporter.log("Total Downpayment Price Retrieved ");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve total DownPayment Price");
		}
		return devicePriceDigit;
	}

	/**
	 * Verify Error Message For ID type without selecting data
	 *
	 */
	public CheckOutPage verifyErrorMessageForIDType() {
		try {
			Assert.assertTrue(isElementDisplayed(idTypeCreditCheck), "ID number element is not found");
			idTypeCreditCheck.click();
			idNumber.click();
			Assert.assertTrue(isElementDisplayed(idTypeErrorMessage), "Error message is not displayed for ID type");
			Reporter.log("Verified Error Message For ID type without selecting data");
		} catch (Exception e) {
			Assert.fail("Failed to verify Error Message For ID type without selecting data");
		}
		return this;
	}

	/**
	 * verify Accessories Pricing display
	 */
	public CheckOutPage verifyAccessoriesAreDisplayed() {
		try {
			waitforSpinner();
			for (WebElement webElement : accessoriesNameDisplay) {
				Assert.assertTrue(webElement.isDisplayed(), "Accessories pricing are not displayed");
			}
			Reporter.log("Accessories pricing are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessories pricing on pricing modal");
		}
		return this;
	}

	/**
	 * verify Accessories Pricing display
	 */
	public CheckOutPage verifyAccessoriesTodayPricingBreakdownDisplayed() {
		try {
			waitforSpinner();
			for (WebElement webElement : accessoriesTodayPricebreakdown) {
				Assert.assertTrue(webElement.isDisplayed(), "Accessories today price break down are not displayed");
			}
			Reporter.log("Accessories today price break down are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessories today price break down");
		}
		return this;
	}

	/**
	 * Verify First Name Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyFirstnameNotEditableAndLocked() {
		try {
			waitforSpinner();
			Assert.assertTrue(firstName.getAttribute("class").contains("ellipsis"),
					"First Name is Editable and unlocked");
			Assert.assertFalse(firstName.isEnabled(), "First Name is Editable and unlocked");
			Reporter.log("First Name is Not Editable and locked");
		} catch (Exception e) {
			Assert.fail("Failed to verify First Name Field");
		}
		return this;
	}

	/**
	 * Verify Last Name Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyLastnameNotEditableAndLocked() {
		try {
			waitforSpinner();
			Assert.assertTrue(lastName.getAttribute("class").contains("ellipsis"),
					"Last Name is Editable and unlocked");
			Assert.assertFalse(lastName.isEnabled(), "Last Name is Editable and unlocked");
			Reporter.log("Last Name is Not Editable and locked");
		} catch (Exception e) {
			Assert.fail("Failed to verify Last Name Field");
		}
		return this;
	}

	/**
	 * Verify today&monthly label in check out pages
	 * 
	 * @return
	 */
	public CheckOutPage verifyTodayAndMonthlyLabelInAllCheckOutPages() {
		try {
			Assert.assertTrue(todayTextInCheckOutPage.get(1).getText().contains("Today"),
					"Today label not displayed in checkout pages");
			Assert.assertTrue(monthlyText.get(1).getText().contains("Monthly"),
					"Monthly label not displayed in all checkout pages");
			Reporter.log("Today and Monthly  label displayed correctly");
			;
		} catch (Exception e) {
			Assert.fail("Failed to Verify Today and Monthly");
		}
		return this;
	}

	/**
	 * Verify today&monthly label in review order details page
	 *
	 */
	public CheckOutPage verifyTodayOrMonthlyLabelBelowPricesInReviewSubmitPage() {
		try {
			Assert.assertTrue(reviewSubmitTodayOrMonthlyBelowPricesLabel.get(0).getText().contains("Today"),
					"Today label not displayed below prices");
			Assert.assertTrue(reviewSubmitTodayOrMonthlyBelowPricesLabel.get(1).getText().contains("Monthly"),
					"Monthly label not displayed below prices");
			Reporter.log("Today/Monthly label displayed below prices in review&submit order details page");
		} catch (Exception e) {
			Assert.fail("Failed to verify Today/Monthly label");
		}
		return this;
	}

	/**
	 * Verify today&monthly label in order details model
	 * 
	 * @return
	 */
	public CheckOutPage verifyTodayAndMonthlyLabelInOrderModel() {
		try {
			Assert.assertTrue(todayMonthlyLabelInOrderModel.get(0).getText().contains("Today"),
					"Today label not displayed in order model");
			Assert.assertTrue(todayMonthlyLabelInOrderModel.get(1).getText().contains("Monthly"),
					"Monthly label not displayed in order model");
			Reporter.log("Today and Monthly  label displayed correctly");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Today and Monthly");
		}
		return this;
	}

	/**
	 * Verify Name On Card Is displayed and not Editable
	 * 
	 * @return
	 */
	public CheckOutPage verifyNameOnCardIsDisplayedIsNotEditable() {
		try {
			waitforSpinner();
			Assert.assertTrue(nameOnCard.getAttribute("class").contains("ng-not-empty"),
					"Name on Card is not displayed");
			Assert.assertTrue(nameOnCard.getAttribute("role").equals("textbox"), "Name on Card is not editable");
			Reporter.log("Name on Card is displayed and is editable");
		} catch (Exception e) {
			Assert.fail("Failed to display Name on Card and check editable");
		}
		return this;
	}

	/**
	 * Get Total sales tax price on review order page
	 * 
	 * @return
	 */
	public Double getReviewSubmitSalesTaxPrice() {
		String salesTax = null;
		String salesTaxPrice = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(reviewSubmitSalesTaxPrice), 60);
			salesTax = reviewSubmitSalesTaxPrice.getText();
			salesTaxPrice = salesTax.substring(salesTax.lastIndexOf("$") + 1);
			Reporter.log("sales tax Price at check out page is " + salesTax);
		} catch (Exception e) {
			Assert.fail("sales tax price At CheckOut Page isn't available");
		}
		return Double.parseDouble(salesTaxPrice);
	}

	/**
	 * Get SIM price on review order page
	 * 
	 * @return
	 */
	public Double getReviewSubmitSimPrice() {
		String simprice = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(miSimKitPrice), 60);
			simprice = miSimKitPrice.getAttribute("left-amount");
			Reporter.log("Today Price at check out page is " + simprice);
		} catch (Exception e) {
			Assert.fail("Today Price At CheckOut Page isn't available");
		}
		return Double.parseDouble(simprice);
	}

	/**
	 * Compare two Double Values
	 * 
	 */
	public CheckOutPage compareTwoDoubleValuesNotEqual(Double price1, Double Price2) {
		try {

			Assert.assertNotEquals(price1, Price2, "Both Double values match");
			Reporter.log("Both Double values don't matched");
		} catch (Exception e) {
			Assert.fail("Failed to compare both Double values");
		}
		return this;
	}

	/**
	 * AutoPay Section not dispalyed
	 * 
	 * @return
	 */
	public CheckOutPage verifyAutoPaySectionNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(autoPay), "AutoPay is displayed");
			Reporter.log("AutoPay is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify lAutoPay -failed");

		}
		return this;
	}

	/**
	 * Security Pin Section not dispalyed
	 * 
	 * @return
	 */
	public CheckOutPage verifySecurityPinSectionNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(securityPinSection), "Security Pin is displayed");
			Reporter.log("Security Pin is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Security Pin -failed");

		}
		return this;
	}

	/**
	 * Fill Invalid Residential Address for Credit Check
	 *
	 * @param tmngData
	 */
	public CheckOutPage fillInvalidAddressforCreditCheck(TMNGData tmngData) {

		try {
			checkPageIsReady();
			addressLine1RACreditCheck.sendKeys(INVALID_ADDRESS_LINE_1);
			cityRACreditCheck.sendKeys(INVALID_CITY);
			stateRACreditCheck.click();
			stateRACreditCheck.sendKeys(INVALID_STATE);
			stateRACreditCheck.sendKeys(Keys.TAB);

			zipCodeRACreditCheck.sendKeys(INVALID_ZIPCODE);
			sendKeysWithWait(DOBCreditCheck, tmngData.getDOB());
			sendKeysWithWait(SSNNumberCreditCheck, tmngData.getSSN());

			idTypeCreditCheck.click();
			idTypeCreditCheck.sendKeys(tmngData.getIdType());
			idTypeCreditCheck.sendKeys(Keys.TAB);

			idNumber.clear();
			idNumber.sendKeys(tmngData.getIdNumber());

			expiryDateCreditCheck.sendKeys(tmngData.getExpiryDate());

			while (dobErrorMessage.isDisplayed()) {
				DOBCreditCheck.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
				DOBCreditCheck.sendKeys(tmngData.getDOB());
			}

//			while (!SSNNumberCreditCheck.getAttribute("value").replace("-", "").equals(tmngData.getSSN())) {
//				SSNNumberCreditCheck.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
//				SSNNumberCreditCheck.sendKeys(tmngData.getSSN());
//			}

			Reporter.log("Filled  credit Check Details");

		} catch (Exception e) {
			Assert.fail("Failed to fill  identity info");
		}
		return this;
	}

	/**
	 * invalid address message displayed
	 * 
	 * @return
	 */
	public CheckOutPage verifyInvalidAddressMessage() {
		try {
			Assert.assertTrue(isElementDisplayed(invalidAddressMessage), "Invalid address message is not displayed");
			Assert.assertTrue(invalidAddressMessage.getText().contains("Please check that the provided"),
					"Invalid address message text is not matched");
			Reporter.log("Invalid address message is displayed and text also matched.");
		} catch (Exception e) {
			Assert.fail("Failed to verify invalid address message");
		}
		return this;
	}

	/**
	 * invalid address highlighted
	 * 
	 * @return
	 */
	public CheckOutPage verifyHighlightedFieldsForInvalidAddress() {
		try {
			Assert.assertTrue(invalidAddressForAddressLine1.isDisplayed(),
					"Invalid address line 1 is not highlighted in red");
			Assert.assertTrue(invalidAddressForAddressLine2.isDisplayed(),
					"Invalid address line 2 is not highlighted in red");
			Assert.assertTrue(invalidAddressForCity.isDisplayed(),
					"Invalid address for city is not highlighted in red");
			Assert.assertTrue(invalidAddressForState.isDisplayed(),
					"Invalid address for city is not highlighted in red ");
			Assert.assertTrue(invalidAddressForZipcode.isDisplayed(),
					"Invalid address for zip code is not highlighted in red");
			Reporter.log("Invalid address is highlighted ");
		} catch (Exception e) {
			Assert.fail("Failed to verify invalid address");
		}
		return this;
	}

	/**
	 * invalid address removed and enter valid address again
	 * 
	 * @return
	 */
	public CheckOutPage fillValidAddressForInvalidFields(TMNGData tmngData) {
		try {
			checkPageIsReady();
			invalidAddressForAddressLine1.clear();
			addressLine1RACreditCheck.sendKeys(tmngData.getShippingAddress());
			invalidAddressForAddressLine2.clear();
			invalidAddressForCity.clear();
			cityRACreditCheck.sendKeys(tmngData.getCity());
			stateRACreditCheck.click();
			stateRACreditCheck.sendKeys(tmngData.getState());
			invalidAddressForZipcode.clear();
			zipCodeRACreditCheck.sendKeys(tmngData.getZipcode());
			zipCodeRACreditCheck.sendKeys(Keys.TAB);
			Reporter.log("Filled  credit Check Details");

		} catch (Exception e) {
			Assert.fail("Failed to fill  identity info");
		}
		return this;
	}
}
