
package com.tmobile.eservices.qa.pages.global;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.google.common.collect.Ordering;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author ksrivani
 *
 */
public class FamilyAllowancesPage extends CommonPage {

	private String lineDashboardPageUrl = "dashboard";
	private String viewListOfLinesPageUrl = "list-of-all-lines";
	private static final String missdnAttribute = "pid";
	private static final String pageUrl = "dashboard";
	private static final String dashBoardBookMarkUrl = "/profile/family_controls/allowances/dashboard";
	private static final String historyOfAllLinesBookMarkUrl = "/profile/family_controls/allowances/history-of-all-lines";
	private static final String familyAllowancesPageUrl = "familyallowances";
	private String toggleUpArrow = "arrow-up-rotation";
	private String helpTab = "https://support.t-mobile.com/docs/DOC-1725";
	private String lineHistory = "history";
	private String accordionCollapseStatus = "arrow-down-rotation";
	private String accordianExpandStatus = "arrow-up-rotation";
	private String expectedMinutesAlertMessage1WithAllowance = "This will disable calls for this line until you set the limit above 0. Always Allowed numbers can still be reached.";
	private String expectedMinutesAlertMessage2WithAllowance = "You have reached or exceeded your total allowance set.";
	private String expectedMessagesAlertMessage1WithAllowance = "Messaging will be blocked for this line until you set the allowance above 0. Always Allowed numbers can still be reached.";
	private String expectedMessagesAlertMessage2WithAllowance = "You have exceeded your total message allowance.";
	private String expectedDownloadsAlertMessageWithAllowance = "This line will not be able to purchase downloads until you set a dollar amount.";
	private String lineDashBoardPage = "dashboard";
	private String familyControlsPage = "family_controls";
	private String expectedBreadCrumbInFamilyAllowance = "Profile > Family Controls > Family Allowances";
	private String expectedBreadCrumbInViewListOfAllLines = "Profile > .... > Family Allowances > List of All Lines";
	private String expectedBreadCrumbInHistoryOfAllLines = "Profile > .... > Family Allowances > History of All Lines";
	private String lineSettingsUrl = "profile/line_settings";

	@FindBy(xpath = "((//div[text()='What was changed:']/../div)[9]/div/span)[2]")
	private WebElement downloadLimit;

	@FindBy(css = "span[aria-label='Grant users full access.']")
	private WebElement grantUserAccessCTA;

	@FindBy(xpath = "//a[contains(text(),'The person authorized to create and modify allowances. Click below to grant full access permission to other users.')]")
	private WebElement textBelowParentDropDown;

	@FindBy(xpath = "//div[@class='padding-top-small']")
	private List<WebElement> listOfMissdns;

	@FindBy(xpath = "//span[contains(text(),\"We're having some trouble.\")]")
	private WebElement errorMessageOnSave;

	@FindBy(xpath = "//span[contains(text(),'This number already exists in your Always Allowed list')]")
	private WebElement errorMessageInAllowedNumbers;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	private WebElement continueButton;

	@FindBy(xpath = "//span[contains(text(),'You have made changes to your account')]")
	private WebElement accountChangedPopUp;

	@FindBy(xpath = "//button[contains(text(),'Ok')]")
	private WebElement okButton;

	@FindBy(xpath = "//a[contains(text(), 'None')]")
	private WebElement noneOption;

	@FindBy(xpath = "//div[@id='parentLine']/ul/li/a")
	private List<WebElement> parentLineSelectorValuesList;

	@FindBy(xpath = "//div[@id='parentLine']/ul/li[@class='dropdown-option body-copy active']/a")
	private WebElement noneOptionDisplayAfterSave;

	@FindBy(xpath = "//li[@class='breadcrumb-item']/a[text()='Profile']")
	private WebElement breadCrumbProfile;

	@FindBy(xpath = "//li[@class='breadcrumb-item']/a[text()='Family Controls']")
	private WebElement breadCrumbFamilyControls;

	@FindBy(xpath = "//li[@class='breadcrumb-item']/span[text()='Family Allowances']|//li[@class='breadcrumb-item']/a[text()='Family Allowances']")
	private WebElement breadCrumbFamilyAllowance;

	@FindBy(xpath = "//li[@class='breadcrumb-item']/span[text()='List of All Lines']")
	private WebElement breadCrumbViewListOfAllLines;

	@FindBy(xpath = "//span[contains(@class,'breadcrumb-item')]")
	private WebElement breadCrumbHistoryOfAllLines;

	@FindBy(xpath = "//span[contains(text(),'List of all lines')]|//a[contains(text(),'List of all lines')]")
	private WebElement viewListOfLinesPage;

	@FindBy(xpath = "//span[text()='change history']")
	private WebElement historyOfLinesPage;

	@FindBy(xpath = "//span[text()='Minutes']/../following-sibling::*/div/p/span/span[contains(text(),'Note')]")
	private WebElement minutesNote;

	@FindBy(xpath = "//span[text()='Minutes']/../following-sibling::*/div/p/span/span[contains(text(),'Due to recent changes to your services, your remaining balance may not display correctly in this tool until the next billing cycle.')]")
	private WebElement minutesNoteText;

	@FindBy(xpath = "//span[text()='Messages']/../following-sibling::*/div/p/span/span[contains(text(),'Note')]")
	private WebElement messagesNote;

	@FindBy(xpath = "//span[text()='Messages']/../following-sibling::*/div/p/span/span[contains(text(),'Due to recent changes to your services, your remaining balance may not display correctly in this tool until the next billing cycle.')]")
	private WebElement messagesNoteText;

	@FindBy(xpath = "//span[text()='Downloads']/../following-sibling::*/div/p/span/span[contains(text(),'Note')]")
	private WebElement downloadsNote;

	@FindBy(xpath = "//span[text()='Downloads']/../following-sibling::*/div/p/span/span[contains(text(),'Due to recent changes to your services, your remaining balance may not display correctly in this tool until the next billing cycle.')]")
	private WebElement downloadsNoteText;

	@FindBy(xpath = "//span[text()='You have made changes to your account']")
	private WebElement modalMessage;

	@FindBy(xpath = "//button[text()='Ok']")
	private WebElement modalMessageOkButton;

	@FindBy(xpath = "//p[text()='Family Controls']")
	private WebElement familyControlsBlade;

	@FindBy(xpath = "//a[contains(text(),'View list of all lines')]")
	private WebElement viewListOfLines;

	@FindBy(xpath = "//a[contains(text(),'View list of all lines')]")
	private WebElement viewListOfLinesFromLineDashBoard;

	@FindBy(xpath = "(//span[@class='arrow-down float-right'])[2]")
	private WebElement changeHistoryBillingCycleDropDown;

	@FindBy(xpath = "//span[text()='Change history']/../../following-sibling::*/div[contains(@class,'padding-top-large')]/app-dropdown-selector/div/div/div")
	private WebElement changeHistoryBillingCycleDropDownField;

	@FindBy(xpath = "//div[@class='padding-bottom-xxl']/div/div/a")
	private List<WebElement> suggestionListOfLines;

	@FindBy(css = "div[class*='suggestions'] ul li")
	private List<WebElement> suggestionListOfLineSelectorSearchBar;

	@FindBy(xpath = "//span[text()='9AM - 2PM']/../label/div/span")
	private WebElement scheduleCheckbox;

	@FindBy(xpath = "//div[@aria-label='9AM - 2PM']//..//input")
	private WebElement scheduleFieldMontoFri;

	@FindBy(xpath = "(//span[contains(text(),'Monday')]/../span/label/input)[1]")
	private WebElement scheduleCheckBoxStatus;

	@FindBy(css = "input[name='inputText']")
	private WebElement listOfLinesTextBox;

	@FindBy(xpath = "//a[contains(text(),'Autog, (325) 320-9002')]")
	private WebElement listOfLinesMssdn;

	@FindBy(xpath = "//p[text()='Family Allowances']")
	private WebElement familyAllowanceBlade;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/div/div/input")
	private WebElement currentAllowanceEnteredValue;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::span[contains(text(),'Please enter limit')]")
	private WebElement fieldValidationErrorMessage;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/span[contains(text(),'No allowance set')]")
	private WebElement remainingDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/span[contains(text(),'Used')]/following::span[1]")
	private WebElement usedValue;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/span[contains(text(),'Remaining')]")
	private WebElement remainingLabel;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/span[contains(text(),'Used')]")
	private WebElement usedLabel;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/span[contains(text(),'Current allowance')]")
	private WebElement currentAllowanceLabel;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/span[text()= 'No allowance']")
	private WebElement currentAllowanceDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following::div/div/span/label/input")
	private WebElement checkOrUncheckNoAllowanceCheckBox;

	@FindBy(xpath = "//span[text()='Parent Line']/../../tmo-dropdown-selector/div/div/div/div/span[@ng-model='selectedVal']")
	private WebElement lineDashBoardParentLine;

	@FindBy(xpath = "//span[text()='Parent Line']/../../tmo-dropdown-selector/div/div/div/ul/li/a[contains(text(),'Nagesh')]")
	private WebElement lineDashBoardselectParentValue;

	@FindBy(xpath = "//input[contains(@placeholder,'Find')]")
	private WebElement lineDashBoardLineSelector;

	@FindBy(xpath = "//span[text()='Minutes']/following-sibling::span")
	private WebElement minutesAccordian;

	@FindBy(xpath = "//span[text()='Messages']/following-sibling::span")
	private WebElement messagesAccordian;

	@FindBy(xpath = "//span[text()='Downloads']/following-sibling::span")
	private WebElement downloadsAccordian;

	@FindBy(xpath = "//span[text()='Limit device access']/following-sibling::span")
	private WebElement scheduleAccordian;

	@FindBy(xpath = "//span[text()='Change History']/following-sibling::span")
	private WebElement changeHistoryAccordian;

	@FindBy(xpath = "//span[contains(text(),'Minutes')]")
	private WebElement minutesHeader;

	@FindBy(xpath = "//span[contains(text(),'Minutes')]/../following-sibling::div/div")
	private List<WebElement> minutesDescription;

	@FindBy(xpath = "//span[contains(text(),'Minutes')]/following-sibling::span[contains(@class,'arrow')]")
	private WebElement minutesToggleButton;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span/span[contains(text(),'No allowance')]")
	private WebElement minutesNoAllowanceCheckboxText;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span/label/div/span[contains(@class, 'legal check')]")
	private WebElement minutesNoAllowanceCheckbox;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span/i[contains(@class,'tooltip-icon')]")
	private WebElement minutesNoAllowanceTooltipIcon;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[contains(@tooltip-text,'Setting No Allowance will allow this line to talk without restriction. If you do not have an Unlimited Plan, this could result in overage charges on your bill. You can also restrict calling on this line by using the Schedule and Never Allow page.')]")
	private WebElement minutesNoAllowanceTooltipMessage;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span/span[contains(text(),'line has no allowance')]/preceding-sibling::span")
	private WebElement minutesNoAllowanceAlertText;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span/span[contains(text(),'line has no allowance')]")
	private WebElement minutesNoAllowanceAlertMessage;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span/span[contains(text(),'This will disable calls for this line until you set the limit above 0')]/preceding-sibling::span")
	private WebElement minutesAlert1TextWithAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span/span[contains(text(),'This will disable calls for this line until you set the limit above 0. Always Allowed numbers can still be reached.')]")
	private WebElement minutesAlertMessage1WithAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span/span[contains(text(),'You have reached or exceeded your total allowance set.')]/preceding-sibling::span")
	private WebElement minutesAlert2TextWithAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span/span[contains(text(),'You have reached or exceeded your total allowance set.')]")
	private WebElement minutesAlertMessage2WithAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span/i[contains(@class,'tooltip-icon')]")
	private WebElement minutesAlertTooltipIcon;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/p/span[contains(@tooltip-text,'Your usage may have reached or exceeded your set allowance due to calls to or from Always Allowed numbers, calls made while roaming, or changes made to your allowances.')]")
	private WebElement minutesAlertTooltipMessage;

	@FindBy(xpath = "//span[text()= 'Minutes']//..//..//input[@type='checkbox']")
	private WebElement checkOrUncheckMinutesNoAllowanceCheckBox;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[contains(text(),'Current allowance')]")
	private WebElement minutesCurrentAllowanceLabel;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[contains(text(),'Used')]")
	private WebElement minutesUsedLabel;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[contains(text(),'Remaining')]")
	private WebElement minutesRemainingLabel;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[text()='No allowance']")
	private WebElement minutesCurrentAllowanceDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[contains(text(),'Used')]/following-sibling::span")
	private WebElement minutesUsedValue;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[contains(text(), 'No allowance set')]")
	private WebElement minutesRemainingDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div//input[@name='inputText']")
	private WebElement minutesCurrentAllowanceTextField;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div//p/span[contains(text(),'Please enter limit')]")
	private WebElement minutesFieldValidationErrorMessage;

	@FindBy(xpath = "//span[text()= 'Minutes']/../following-sibling::div/div/span[contains(text(),'Remaining')]/following-sibling::span")
	private WebElement minutesRemainingValueWithAllowance;

	@FindBy(css = ".ng-dirty.ng-valid")
	private WebElement minutesCurrentAllowanceEnteredValue;

	@FindBy(css = "span[ng-model='selectedVal']")
	private WebElement lineDashBoardDefaultParentLine;

	@FindBy(xpath = "//span[text()='allowances']/preceding-sibling::*")
	private WebElement lineDashBoardLineHolderName;

	@FindBy(xpath = "//div[text()='View your schedule and allowances of minutes, messages, and downloads for the current billing cycle']")
	private WebElement lineDashBoardBodyText;

	@FindBy(xpath = "//a[text()='Help']")
	private WebElement lineDashBoardHelp;

	@FindBy(xpath = "//span[starts-with(text(),'Billing cycle')]/following-sibling::*")
	private WebElement lineDashBoardBillingCycle;

	@FindBy(xpath = "//div[@id='parentLine']/div/span[@class='arrow-down float-right']")
	private WebElement lineDashBoardParentListDropDown;

	@FindBy(css = "span[class='slider round']")
	private WebElement lineDashBoardPhoneStatusToggle;

	@FindBy(css = "i[class='tooltip-icon']")
	private WebElement lineDashBoardVerifyParentToolTopIcon;

	@FindBy(css = "span[tooltip-text*='The designated Parent can set allowances for the other lines on the account. Only lines with Full Access permissions can be changed by going to Profile > Line Settings > Permissions. Please select a line to serve as Parent.']")
	private WebElement lineDashBoardVerifyParentToolTopText;

	@FindBy(xpath = "//span[contains(text(),'Phone Status')]/../span[4]")
	private WebElement lineDashBoardCurrentPhoneStatus;

	@FindBy(css = "input[name='checkbox2']")
	private WebElement lineDastBoardCurrentCheckBoxStatus;

	@FindBy(xpath = "//button[text()='Yes']")
	private WebElement lineDashBoardClickYes;

	@FindBy(xpath = "//button[text()='No']")
	private WebElement lineDashBoardClickNo;

	@FindBy(xpath = "//span[contains(text(),'Notify account owner')]/../label/span/span[@class='check1 inline-block']")
	private WebElement lineDashBoardNotificationCheckBox;

	@FindBy(xpath = "//button[contains(text(),'Save')]")
	private WebElement saveButton;

	@FindBy(xpath = "//button[contains(text(),'Discard Changes')]")
	private WebElement discardChangesButton;

	@FindBy(xpath = "//span[contains(text(),'Apply allowance')]")
	private WebElement applyAllowanceChangesToAllLinesCheckboxText;

	@FindBy(xpath = "//span[contains(text(),'Apply allowance')]/../label/div/span")
	private WebElement applyAllowanceChangesToAllLinesCheckbox;

	@FindBy(xpath = "//div[@aria-label= 'Apply allowance changes to all lines']")
	private WebElement checkOrUncheckApplyAllowanceChangesToAllLinesCheckBox;

	@FindBy(xpath = "//span[contains(text(),'Messages')]")
	private WebElement messagesHeader;

	@FindBy(xpath = "//span[contains(text(),'Messages')]/../following-sibling::div/div[contains(text(), 'View and set calling allowances on managed lines for the current billing cycle.')]")
	private WebElement messagesDescription;

	@FindBy(xpath = "//span[contains(text(),'Messages')]/following-sibling::span[contains(@class,'arrow')]")
	private WebElement messagesToggleButton;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span/span[contains(text(),'No allowance')]")
	private WebElement messagesNoAllowanceCheckboxText;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span/label/div/span[contains(@class, 'legal check')]")
	private WebElement messagesNoAllowanceCheckbox;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span/i[contains(@class,'tooltip-icon')]")
	private WebElement messagesNoAllowanceTooltipIcon;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[contains(@tooltip-text,'Setting No Allowance will allow this line to message without restriction. If you do not have an Unlimited Plan this could result in overage charges on your bill. You can also restrict messaging on this line by using the Schedule and Never Allow page.')]")
	private WebElement messagesNoAllowanceTooltipMessage;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p/span/span[contains(text(),'line has no allowance')]/preceding-sibling::span")
	private WebElement messagesNoAllowanceAlertText;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p/span/span[contains(text(),'line has no allowance')]")
	private WebElement messagesNoAllowanceAlertMessage;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p/span/span[contains(text(),'Messaging will be blocked')]/preceding-sibling::span")
	private WebElement messagesAlert1TextWithAllowance;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p/span/span[contains(text(),'Messaging will be blocked for this line until you set the allowance above 0. Always Allowed numbers can still be reached.')]")
	private WebElement messagesAlertMessage1WithAllowance;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p[2]/span/span[contains(text(),'You have exceeded your total message allowance.')]/preceding-sibling::span")
	private WebElement messagesAlert2TextWithAllowance;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p[2]/span/span[contains(text(),'You have exceeded your total message allowance.')]")
	private WebElement messagesAlertMessage2WithAllowance;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p/span/i[contains(@class,'tooltip-icon')]")
	private WebElement messagesAlertTooltipIcon;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/p/span[contains(@tooltip-text,'Your usage may have reached or exceeded your set allowance due to messages sent to or from Always Allowed numbers, messages while roaming, or changes made to your allowances.')]")
	private WebElement messagesAlertTooltipMessage;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div//input[@type='checkbox']")
	private WebElement checkOrUncheckMessagesNoAllowanceCheckBox;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[contains(text(),'Current allowance')]")
	private WebElement messagesCurrentAllowanceLabel;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[contains(text(),'Used')]")
	private WebElement messagesUsedLabel;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[contains(text(),'Remaining')]")
	private WebElement messagesRemainingLabel;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[text()='No allowance']")
	private WebElement messagesCurrentAllowanceDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[contains(text(),'Used')]/following-sibling::span")
	private WebElement messagesUsedValue;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[contains(text(), 'No allowance set')]")
	private WebElement messagesRemainingDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div//input[@name='inputText']")
	private WebElement messagesCurrentAllowanceTextField;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/div/p/span[contains(text(),'Please enter limit')]")
	private WebElement messagesFieldValidationErrorMessage;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span[contains(text(),'Remaining')]/following-sibling::span")
	private WebElement messagesRemainingValueWithAllowance;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/div/div/input")
	private WebElement messagesCurrentAllowanceEnteredValue;

	@FindBy(xpath = "//span[contains(text(),'History')]")
	private WebElement changeHistoryBlade;

	@FindBy(xpath = "//span[text()='Billing cycle']/following-sibling::*")
	private WebElement lineDashboardBillingcycle;

	@FindBy(xpath = "(//a[text()='View history of all lines'])[2]")
	private WebElement changeHistoryViewListOfLines;

	@FindBy(xpath = "(//ul[@class=\"dropdown-menu\"])[2]/li/a")
	private List<WebElement> changeHistoryPastBillingCycle;

	@FindBy(xpath = "//span[text()='Limit device access']/../span[contains(@class,'arrow-down')]")
	private WebElement schedule;

	@FindBy(xpath = "//span[text()='Decide when managed lines can access their devices using blocks of predefined time periods throughout the week.']")
	private WebElement scheduleHeading;

	@FindBy(xpath = "//span[text()='Limit device access']/../../div[@class='acc-content accordian-in']/div/span[@class='body-copy outline-none']")
	private WebElement scheduleHeadingToolTip;

	@FindBy(xpath = "//span[text()='Decide when managed lines can access their devices using blocks of predefined time periods throughout the week.']/../span[@class='body-copy outline-none']")
	private WebElement scheduleHeadingToolTipText;

	@FindBy(xpath = "//span[contains(text(),'Downloads')]")
	private WebElement downloadsHeader;

	@FindBy(xpath = "//span[contains(text(),'Downloads')]/../following-sibling::div/div[contains(text(), 'View and set calling allowances on managed lines for the current billing cycle.')]")
	private WebElement downloadsDescription;

	@FindBy(xpath = "//span[contains(text(),'Downloads')]/following-sibling::span[contains(@class,'arrow')]")
	private WebElement downloadsToggleButton;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span/span[contains(text(),'No allowance')]")
	private WebElement downloadsNoAllowanceCheckboxText;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span/label/div/span[contains(@class, 'legal check')]")
	private WebElement downloadsNoAllowanceCheckbox;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span/i[contains(@class,'tooltip-icon')]")
	private WebElement downloadsNoAllowanceTooltipIcon;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[contains(@tooltip-text,'Setting No Allowance will allow this line to download without restriction. This could result in overage charges on your bill.')]")
	private WebElement downloadsNoAllowanceTooltipMessage;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/p/span/span[contains(text(),'line has no allowance')]/preceding-sibling::span")
	private WebElement downloadsNoAllowanceAlertText;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/p/span/span[contains(text(),'line has no allowance')]")
	private WebElement downloadsNoAllowanceAlertMessage;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/p/span/span[contains(text(),'line will not be able to purchase downloads')]/preceding-sibling::span")
	private WebElement downloadsAlertTextWithAllowance;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/p/span/span[contains(text(),'This line will not be able to purchase downloads until you set a dollar amount.')]")
	private WebElement downloadsAlertMessageWithAllowance;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span/label/input[@type='checkbox']")
	private WebElement checkOrUncheckDownloadsNoAllowanceCheckBox;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[contains(text(),'Current allowance')]")
	private WebElement downloadsCurrentAllowanceLabel;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[contains(text(),'Used')]")
	private WebElement downloadsUsedLabel;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[contains(text(),'Remaining')]")
	private WebElement downloadsRemainingLabel;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[text()='No allowance']")
	private WebElement downloadsCurrentAllowanceDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[contains(text(),'Used')]/following-sibling::span")
	private WebElement downloadsUsedValue;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[contains(text(), 'No allowance set')]")
	private WebElement downloadsRemainingDefaultValueWithoutAllowance;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/div/div/input[@name='inputText']")
	private WebElement downloadsCurrentAllowanceTextField;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/div/p/span[contains(text(),'Please enter limit')]")
	private WebElement downloadsFieldValidationErrorMessage;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/span[contains(text(),'Remaining')]/following-sibling::span")
	private WebElement downloadsRemainingValueWithAllowance;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/div/div/input")
	private WebElement downloadsCurrentAllowanceEnteredValue;

	@FindBy(xpath = "//div[contains(@class,'padding-top-small')]/a[contains(text(),'View history of all lines')]")
	private WebElement viewHistoryOfAllLines;

	@FindBy(css = "div[class*='suggestions'] ul li")
	private List<WebElement> suggestionListOfHistoryLineSelectorSearchBar;

	private String historyOfAllLinesPageUrl = "history-of-all-lines";

	@FindBy(xpath = "//span[contains(text(),'change history')]/preceding-sibling::span")
	private WebElement historyOfAllLinesHeader;

	@FindBy(xpath = "//span[contains(text(), 'Listed below are all changes to the Allowances feature for all lines by billing cycle.')]")
	private WebElement historyOfAllLinesDescription;

	@FindBy(xpath = "//span[text()= 'change history']/../following-sibling::span/a[text()='Help']")
	private WebElement historyOfLinesHelp;

	@FindBy(css = "input[placeholder*='Find']")
	private WebElement historyLineSelector;

	@FindBy(css = "div[class*='suggestions'] ul li")
	private List<WebElement> suggestionListOfFindALineSelectorSearchBar;

	@FindBy(css = "div[class*='search-menu'] ul")
	private WebElement suggestionListOfFindALineSelector;

	@FindBy(css = "i[class*='search-icon']")
	private WebElement searchIconOnHistoryLineSelectorSearchTextBar;

	@FindBy(css = "i[class*='clear-icon']")
	private WebElement historyLineSelectorClearIon;

	@FindBy(css = "#inputText")
	private WebElement historyLineSelectorSearchTextBar;

	@FindBy(xpath = "//a[contains(text(),'Billing cycle')]")
	private WebElement historyOfLinesBillingCycleLabel;

	@FindBy(xpath = "//div[contains(text(),'Billing cycle')]/../../app-dropdown-selector/div/div/div/div/span[@class='body-copy']")
	private WebElement changeHistoryBillingcycle;

	@FindBy(xpath = "//a[contains(text(),'Billing cycle')]/../div/div/span")
	private WebElement historyOfLinesBillingcycle;

	@FindBy(xpath = "//ul[@class='dropdown-menu']")
	private WebElement historyOfLinesBillingcycleDropDown;

	@FindBy(css = "div[class*='suggestions'] ul li")
	private List<WebElement> suggestionListOfBillingCycleSelector;

	@FindBy(xpath = "//div[@class='TMO-DROPDOWN-SELECTOR']//following-sibling::ul/li/a")
	private List<WebElement> historyOfLinesPastBillingCycle;

	@FindBy(xpath = "//a[contains(text(),'View list of all lines')]")
	private WebElement viewListOfAllLines;

	@FindBy(css = "div[class*='show-recent-search'] ul li")
	private List<WebElement> recentHistoryOfHistoryLineSelectorSearchBar;

	@FindBy(xpath = "//div[contains(text(),'Date/Time')]")
	private WebElement dateTimeLabel;

	@FindBy(xpath = "//div[contains(text(),'What Was Changed:')]")
	private WebElement whatWasChangedLabel;

	@FindBy(xpath = "//div[contains(text(),'Date/Time')]/../following-sibling::div/div[@class='body-copy ']")
	private List<WebElement> dateTimeValue;

	@FindBy(xpath = "//div[contains(text(),'Date/Time')]/../following-sibling::div/div[@class='body-copy padding-bottom-small']")
	private List<WebElement> whatWasChangedValue;

	@FindBy(xpath = "//span[text()='Allowed numbers']")
	private WebElement allowedNumbersHeader;

	@FindBy(xpath = "//span[text()='Allowed numbers']/../following-sibling::div/div/span[contains(text(), 'Manage numbers that are Always Allowed or Never Allowed for each line.')]")
	private WebElement allowedNumbersDescription;

	@FindBy(xpath = "//span[contains(text(),'Allowed numbers')]/following-sibling::span[contains(@class,'arrow')]")
	private WebElement allowedNumbersAccordian;

	@FindBy(xpath = "//span[text()='Always allowed']")
	private WebElement alwaysAllowed;

	@FindBy(xpath = "//span[text()='Always allowed']/following-sibling::span[contains(text(),'Always Allowed numbers')]")
	private WebElement alwaysAllowedDescription;

	@FindBy(xpath = "//span[text()='Always allowed']/following-sibling::p/span[contains(text(),'Regardless of restrictions or allowance limits on the line, the numbers below can be reached.')]")
	private WebElement alwaysAllowedDescription2;

	@FindBy(xpath = "//span[text()= 'Always allowed']/../following-sibling::div/p/span/i[contains(@class,'tooltip-icon')]")
	private WebElement alwaysAllowedTooltipIcon;

	@FindBy(xpath = "//span[text()='Always allowed']/following-sibling::p/span[contains(@tooltip-text,'Accept calls and message from specific numbers regardless of restrictions or allowance limits.')]")
	private WebElement alwaysAllowedTooltipMessage;

	@FindBy(xpath = "//span[text()='Always allowed']/following-sibling::span[contains(text(),'Number')]")
	private WebElement alwaysAllowedNumberLabel;

	@FindBy(xpath = "//span[text()='Always allowed']/following-sibling::span[contains(text(),'Description')]")
	private WebElement alwaysAllowedDescriptionLabel;

	@FindBy(xpath = "//span[text()='Always allowed']/following-sibling::span[contains(text(),'Remove')]")
	private WebElement alwaysAllowedRemoveLabel;

	@FindBy(xpath = "//span[text()='Always allowed']/../following::div/span[contains(text(),'911')]")
	private WebElement emergencyServicesNumber;

	@FindBy(xpath = "//span[text()='Always allowed']/../following::div/span[contains(text(),'1-877-453-1304 or 611')]")
	private WebElement tMobileCustomerCareNumber;

	@FindBy(xpath = "//span[text()='Always allowed']/../following::div/span[contains(text(),'Emergency Services')]")
	private WebElement emergencyServicesNumberDescription;

	@FindBy(xpath = "//span[text()='Always allowed']/../following::div/span[contains(text(),'T-Mobile Customer Care')]")
	private WebElement tMobileCustomerCareNumberDescription;

	@FindBy(xpath = "//span[text()='Always allowed']/../following::div/span[contains(text(),'911')]/../p/span[contains(@aria-label,'')]")
	private WebElement noRemoveButtonForEmergencyServices;

	@FindBy(xpath = "//span[text()='Always allowed']/../following::div/span[contains(text(),'1-877-453-1304 or 611')]/../p/span[contains(@aria-label,'')]")
	private WebElement noRemoveButtonForTMobileCustCare;

	@FindBy(xpath = "//input[@name='alwaysAllowed']/../following-sibling::*/span[text()='Add']")
	private WebElement addButtonForAlwaysAllowed;

	@FindBy(css = "input[name='alwaysAllowed']")
	private WebElement addNewNumberForAlwaysAllowed;

	@FindBy(xpath = "//span[@aria-label='Remove icon']/../../span[1]")
	private List<WebElement> newlyAddedNumberForAlwaysAllowed;

	@FindBy(xpath = "//span[@aria-label='Remove icon']")
	private List<WebElement> removeIconForNewlyAddedNumberUnderAlwaysAllowed;

	@FindBy(xpath = "//span[contains(text(),'Note: Use of the Always Allowed numbers feature could result in overage charges on your bill.')]")
	private WebElement alwaysAllowedFootNote;

	@FindBy(xpath = "//p[@class=' text-left d-flex justify-content-between']/span[contains(text(),'ERROR: Please enter a valid phone number (no international numbers or 900 numbers).')]")
	private WebElement alwaysAllowedNumberFieldValidationErrorMessage;

	@FindBy(xpath = "//span[contains(text(),'This number already exists in your Always Allowed list')]")
	private WebElement duplicateNumberErrorMessageUnderAlwaysAllowed;

	@FindBy(xpath = "//span[text()='Never allowed']")
	private WebElement neverAllowed;

	@FindBy(xpath = "//span[text()='Never allowed']/following-sibling::p/span[contains(text(),'The numbers below are blocked for this line. Calls and messages can not be sent or received to these numbers.')]")
	private WebElement neverAllowedDescription;

	@FindBy(xpath = "//span[text()='Never allowed']/following-sibling::p/span/i[contains(@class,'tooltip-icon')]")
	private WebElement neverAllowedTooltipIcon;

	@FindBy(xpath = "//span[text()='Never allowed']/following-sibling::p/span[contains(@tooltip-text,'Block numbers from calling or messaging lines on your account. However, even if a number is on your Never Allowed list, it may still be reachable if you�re roaming on any domestic or international networks (and not on the T-Mobile USA network). International numbers, 800 numbers, and 900 numbers are not eligible for the Never Allowed list.')]")
	private WebElement neverAllowedTooltipMessage;

	@FindBy(xpath = "//span[text()='Never allowed']/following-sibling::span[contains(text(),'Number')]")
	private WebElement neverAllowedNumberLabel;

	@FindBy(xpath = "//span[text()='Never allowed']/following-sibling::span[contains(text(),'Remove')]")
	private WebElement neverAllowedRemoveLabel;

	@FindBy(xpath = "//span[contains(text(),'411 (Information) You may also want to block.')]/../span/label/span/span[contains(@class,'check1 inline-block')]")
	private WebElement neverAllowed411NumberCheckbox;

	@FindBy(xpath = "//span[contains(text(),'411 (Information) You may also want to block.')]/../span/label/input")
	private WebElement neverAllowed411NumberAddCheckbox;

	@FindBy(xpath = "//span[contains(text(),'411 (Information) You may also want to block.')]")
	private WebElement neverAllowed411NumberCheckboxText;

	@FindBy(css = "input[name='neverAllowed']")
	private WebElement addNewNumberForNeverAllowed;

	@FindBy(xpath = "//input[@name='neverAllowed']/../following-sibling::*/span[text()='Add']")
	private WebElement addButtonForNeverAllowed;

	@FindBy(xpath = "//span[@aria-label='remove icon']/../../span")
	private List<WebElement> newlyAddedNumberForNeverAllowed;

	@FindBy(xpath = "//span[@aria-label='remove icon']")
	private List<WebElement> removeIconForNewlyAddedNumberUnderNeverAllowed;

	@FindBy(xpath = "//span[@class='legal red'][contains(text(),'ERROR: Please enter a valid phone number (no international numbers, 900 numbers, or 800 numbers - including 866, 877, and 888).')]")
	private WebElement neverAllowedNumberFieldValidationErrorMessage;

	@FindBy(xpath = "//span[contains(text(),'This number already exists in your Never Allowed list')]")
	private WebElement duplicateNumberErrorMessageUnderNeverAllowed;

	@FindBy(css = "span[class='display6']")
	private List<WebElement> listOfSubModules;

	@FindBy(xpath = "//span[contains(text(),'Notify account owner and line user')]//following::p[1]/span/span[1]")
	private WebElement alertLabelWhenPhoneStatusChangingFromOnToOff;

	@FindBy(xpath = "//span[contains(text(),'Notify account owner and line user')]//following::p[1]/span/span[2]")
	private WebElement alertMsgWhenPhoneStatusChangingFromOnToOff;

	@FindBy(xpath = "//span[text()= 'Downloads']/../following-sibling::div/div/p[2]/span/span[contains(text(),'You have exceeded your total message allowance.')]")
	private WebElement downloadsAlertMessage2WithAllowance;

	@FindBy(xpath = "//span[text()= 'Minutes']//following::p[1]/span/span")
	private WebElement alertMsgLabelWithNoAllowanceForMinutesWhenPhoneStatusIsOff;

	@FindBy(xpath = "//span[text()= 'Minutes']//following::p[1]/span/span[contains(text(),'This line has no allowance set, meaning that this line�s user can talk without restrictions.') and contains(text(),'This could result in overage charges on your bill.')]")
	private WebElement alertMsgWithNoAllowanceForMinutesWhenPhoneStatusIsOff;

	@FindBy(xpath = "//span[text()= 'Minutes']//following::p[1]/span/span")
	private WebElement alertMsgLabelWithNoAllowanceForMessagesWhenPhoneStatusIsOff;

	@FindBy(xpath = "//span[text()= 'Messages']//following::p[1]/span/span[contains(text(),'This line has no allowance set, meaning that this line�s user can message without restrictions. If you do not have an Unlimited Plan, this could result in overage charges on your bill.')]")
	private WebElement alertMsgWithNoAllowanceForMessagesWhenPhoneStatusIsOff;

	@FindBy(xpath = "//span[text()= 'Minutes']//following::p[1]/span/span")
	private WebElement alertMsgLabelWithNoAllowanceForDownloadsWhenPhoneStatusIsOff;

	@FindBy(xpath = "//span[text()= 'Downloads']//following::p[1]/span/span[contains(text(),'This line has no allowance set, meaning that this line�s user can download without restrictions.')]")
	private WebElement alertMsgWithNoAllowanceForDownloadsWhenPhoneStatusIsOff;

	@FindBy(css = "input[name='inputText']")
	private WebElement missdninFamilyControlsPage;

	@FindBy(css = "span[ng-model='selectedVal']")
	private WebElement missdninProfilePage;

	@FindBy(css = "input[name='inputText']")
	private WebElement missdninFamilyAllowancesPage;

	@FindBy(xpath = "//span[contains(text(),'Downloads')]/../../div[contains(@class,'accordian-in')]/div/span/label/div/span")
	private WebElement downloadsToggleCheckBox;

	@FindBy(css = ".red")
	private WebElement downloadsDivAlertText;

	@FindBy(xpath = "//span[text()= 'Messages']/../following-sibling::div/div/span/label/input")
	private WebElement messagesNoAllowanceCheckboxStatus;

	public FamilyAllowancesPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(css = "div[class='spinner-loader-background']")
	private List<WebElement> spinner;

	/**
	 * Verify FamilyAllowances line dashboard Page.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyLineDashboardPage() {
		try {
			checkPageIsReady();
			verifyCurrentPageURL("Family Allowance page", lineDashboardPageUrl);
		} catch (Exception e) {
			Assert.fail("LineDashboard page is not displayed");
		}
		return this;
	}

	/**
	 * Verify FamilyAllowances line dashboard Page.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyViewListOfLinesPage() {
		try {
			checkPageIsReady();
			verifyCurrentPageURL("View list of lines page", viewListOfLinesPageUrl);
		} catch (Exception e) {
			Assert.fail("viewListOfLines page is not displayed");
		}
		return this;
	}

	public FamilyAllowancesPage navigateToViewListOfLinesFromFamilyControls() {
		try {
			checkPageIsReady();
			clickElement(viewListOfLinesFromLineDashBoard);
			Reporter.log("navigated to view list of lines page from family controls page");
		} catch (Exception e) {
			Assert.fail("unable to navigate to list of lines page from family controls page");
		}
		return this;
	}

	/**
	 * Verify FamilyAllowances line dashboard Page.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyViewListOfLinesIsEditable() {
		try {
			waitFor(ExpectedConditions.visibilityOf(viewListOfLines));
			clickElement(viewListOfLines);
			Reporter.log("View list of lines is clickable");
			waitforSpinnerinProfilePage();
			Reporter.log("LineDashboard page is displayed");
		} catch (Exception e) {
			Assert.fail("LineDashboard page is not displayed");
		}
		return this;
	}

	/**
	 * Verify minutes no allowance check box is in readable (not editable)
	 * format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMinutesNoAllowanceCheckboxIsReadable() {
		checkPageIsReady();
		try {
			String actualCheckBoxStatusBefore = minutesNoAllowanceCheckbox.getAttribute("value");
			clickElement(minutesNoAllowanceCheckbox);
			String actualCheckBoxStatusAfter = minutesNoAllowanceCheckbox.getAttribute("value");
			Assert.assertEquals(actualCheckBoxStatusBefore, actualCheckBoxStatusAfter);
			Reporter.log("minutes no allowance  field is readable");
		} catch (Exception e) {
			Assert.fail("minutesNoAllowanceCheckbox page is not validated");
		}
		return this;
	}

	/**
	 * Verify messages no allowance check box is in readable (not editable)
	 * format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesNoAllowanceCheckboxIsReadable() {
		try {
			checkPageIsReady();
			String actualCheckBoxStatusBefore = messagesNoAllowanceCheckbox.getAttribute("value");
			clickElement(messagesNoAllowanceCheckbox);
			String actualCheckBoxStatusAfter = messagesNoAllowanceCheckbox.getAttribute("value");
			Assert.assertEquals(actualCheckBoxStatusBefore, actualCheckBoxStatusAfter);
			Reporter.log("messages no allowance  field is readable");
		} catch (Exception e) {
			Assert.fail("minutesNoAllowanceCheckbox field  is not validated");
		}
		return this;
	}

	/**
	 * Verify messages no allowance check box is in readable (not editable)
	 * format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDownloadsNoAllowanceCheckboxIsReadable() {
		try {
			checkPageIsReady();
			String actualCheckBoxStatusBefore = downloadsNoAllowanceCheckbox.getAttribute("value");
			clickElement(downloadsNoAllowanceCheckbox);
			String actualCheckBoxStatusAfter = downloadsNoAllowanceCheckbox.getAttribute("value");
			Assert.assertEquals(actualCheckBoxStatusBefore, actualCheckBoxStatusAfter);
			Reporter.log("downloads no allowance  field is readable");
		} catch (Exception e) {
			Assert.fail("downloads NoAllowanceCheckbox field  is not  validated");
		}
		return this;
	}

	/**
	 * Verify minutes CurrentAllowanceTextField is in readable (not editable)
	 * format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage minutesCurrentAllowanceTextFieldIsReadable() {
		try {
			checkPageIsReady();
			Assert.assertTrue(!minutesCurrentAllowanceTextField.isEnabled());
			Reporter.log("minutesCurrentAllowanceTextField field is readable");
		} catch (Exception e) {
			Assert.fail("minutes current allowance text field is not validated");
		}
		return this;
	}

	/**
	 * Verify minutes CurrentAllowanceTextField is in readable (not editable)
	 * format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage messagesCurrentAllowanceTextFieldIsReadable() {
		try {
			checkPageIsReady();
			Assert.assertTrue(!messagesCurrentAllowanceTextField.isEnabled());
			Reporter.log("messagesCurrentAllowanceTextField field is readable");
		} catch (Exception e) {
			Assert.fail("messages current allowance text field is not validated");
		}
		return this;
	}

	/**
	 * Verify minutes CurrentAllowanceTextField is in readable (not editable)
	 * format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage downloadsCurrentAllowanceTextFieldIsReadable() {
		try {
			checkPageIsReady();
			Assert.assertTrue(!downloadsCurrentAllowanceTextField.isEnabled());
			Reporter.log("downloads CurrentAllowanceTextField field is readable");
		} catch (Exception e) {
			Assert.fail("downloads current allowance text field is not validated");
		}
		return this;
	}

	/**
	 * Verify schedule field is in readable (not editable) format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage scheduleFieldIsReadable() {
		try {
			checkPageIsReady();
			String actualCheckBoxStatusBefore = scheduleCheckbox.getAttribute("value");
			clickElement(scheduleCheckbox);
			String actualCheckBoxStatusAfter = scheduleCheckbox.getAttribute("value");
			Assert.assertEquals(actualCheckBoxStatusBefore, actualCheckBoxStatusAfter);
			Reporter.log("schedule checkbox field is readable");
		} catch (Exception e) {
			Assert.fail("schedule field is not readable");
		}
		return this;
	}

	/**
	 * Verify change history billing cycle field is in readable (not editable)
	 * format
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyChangeHistoryBillingCycleDropDownIsReadable() {
		try {
			checkPageIsReady();
			Assert.assertTrue(changeHistoryBillingCycleDropDownField.getAttribute("class").contains("disabled"));
			Reporter.log("change history billing cycle  field is readable and not editble");
		} catch (Exception e) {
			Assert.fail("change history billing cycle  field is not readable");
		}
		return this;
	}

	/**
	 * Verify FamilyAllowances Page.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyFamilyAllowancesPage() {
		try {
			verifyPageUrl(pageUrl);
			if (isElementDisplayed(accountChangedPopUp)) {
				Reporter.log("Account changed pop up is displayed");
				okButton.click();
				Reporter.log("Clicked on Ok button in Pop up");
				Reporter.log("FamilyAllowances page is displayed");
			} else {
				Reporter.log("Family allowances page is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Family allowances page not displayed");
		}
		return this;
	}

	/**
	 * Verify the display of Minutes blade header.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinutesBladeHeader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(minutesHeader));
			Reporter.log("Minutes blade header is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display the Minutes blade");
		}
		return this;
	}

	/**
	 * Click on Minutes blade.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clickOnMinutesBlade() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			if (minutesToggleButton.getAttribute("Class").contains(toggleUpArrow)) {
				Reporter.log("Minutes blade is already expanded");
			} else {
				elementClick(minutesHeader);
				Reporter.log("Minutes blade is expanded successfully");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Minutes blade");
		}
		return this;
	}

	/**
	 * Verify the display of Minutes blade description.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinutesBladeDescription() {
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementBytext(minutesDescription,
					"View and set calling allowances on managed lines for the current billing cycle."));
			Reporter.log("Content below Minutes blade header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Content below Minutes blade header");
		}
		return this;
	}

	/**
	 * Navigate Back
	 */
	/***
	 * verify toggle down arrow is clickable
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMinutesToggleArrow() {
		try {
			checkPageIsReady();
			Assert.assertTrue(minutesToggleButton.isDisplayed() && minutesToggleButton.isEnabled());
			Reporter.log("Verified Minutes toggle down arrow is displayed and is enabled");
		} catch (Exception e) {
			Assert.fail("Unable to display the Minutes toggle down arrow");
		}
		return this;
	}

	/***
	 * verify the display of No allowance text
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMinutesNoAllowanceCheckBox() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(minutesNoAllowanceCheckboxText), "No Allowance text is not displayed");
			Reporter.log("No Allowance text is displayed");
			Assert.assertTrue(isElementDisplayed(minutesNoAllowanceCheckbox),
					"No Allowance check box is not displayed");
			Reporter.log("No Allowance check box is displayed");
			Assert.assertTrue(isElementEnabled(minutesNoAllowanceCheckbox), "No Allowance check box is not enabled");
			Reporter.log("No Allowance checkbox is enabled");
		} catch (Exception e) {
			Assert.fail("No Allowance checkbox is not clickable");
		}
		return this;
	}

	/**
	 * Verify the display of Tool tip Icon.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinutesTooltipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(minutesNoAllowanceTooltipIcon));
			Reporter.log("Tooltip icon is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip icon");
		}
		return this;
	}

	/**
	 * Verify the display of Minutes Tool tip message.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinutesTooltipMessage() {
		try {
			checkPageIsReady();
			Actions Tooltip = new Actions(getDriver());
			Tooltip.clickAndHold(minutesNoAllowanceTooltipIcon).perform();
			Assert.assertTrue(isElementDisplayed(minutesNoAllowanceTooltipMessage));
			Reporter.log(" Tooltip message is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip message for Minutes");
		}
		return this;
	}

	/**
	 * Verify the display of alert message for Minutes without allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWithoutAllowanceForMinutes() {
		try {
			checkPageIsReady();
			String s1 = minutesNoAllowanceAlertText.getText();
			String s = minutesNoAllowanceAlertMessage.getText();
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(s,
					"This line has no allowance set, meaning that this line�s user can talk without restrictions.  This could result in overage charges on your bill.");
			Reporter.log("Successfully verified the Alert message for Minutes without allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the Alert message for Minutes");
		}
		return this;
	}

	/**
	 * Clear current allowance text field for Minutes with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clearCurrentAllowanceTextFieldUnderMinutes() {
		try {
			checkPageIsReady();
			elementClick(minutesCurrentAllowanceTextField);
			Reporter.log("Clicked on Current allowance text field");
			minutesCurrentAllowanceTextField.clear();
			minutesCurrentAllowanceTextField.sendKeys(Keys.ENTER);
			Reporter.log("Cleared Current allowance text field");
		} catch (Exception e) {
			Assert.fail("Failed to clear the Current allowance text field Under Minutes");
		}
		return this;
	}

	/**
	 * Verify the display of alert message1 for Minutes with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage1WithAllowanceForMinutes() {
		try {
			checkPageIsReady();
			String s1 = minutesAlert1TextWithAllowance.getText();
			String text = minutesAlertMessage1WithAllowance.getAttribute("innerHTML");
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(text, expectedMinutesAlertMessage1WithAllowance);
			Reporter.log(
					"Successfully verified the Alert message '!Alert: This will disable calls for this line until you set the limit above 0. Always Allowed numbers can still be reached.' for Minutes with allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the first Alert message for Minutes");
		}
		return this;
	}

	/**
	 * Verify the display of alert message2 for Minutes with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage2WithAllowanceForMinutes() {
		try {
			checkPageIsReady();
			String s1 = minutesAlert2TextWithAllowance.getText();
			String text = minutesAlertMessage2WithAllowance.getText();
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(text, expectedMinutesAlertMessage2WithAllowance);
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Minutes with allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the other Alert message for Minutes");
		}
		return this;
	}

	/**
	 * Verify the display of alert message1 for Minutes when current allowance
	 * entered is 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage1WhenCurrentAllowanceIsZeroForMinutes() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer.parseInt(currentAllowanceEnteredValue.getAttribute("value"));
			if (currentAllowanceValue == 0) {
				String s1 = minutesAlert1TextWithAllowance.getText();
				String text = minutesAlertMessage1WithAllowance.getAttribute("innerHTML");
				Assert.assertEquals(s1, "!Alert :");
				Assert.assertEquals(text, expectedMinutesAlertMessage1WithAllowance);
				Reporter.log(
						"Successfully verified the Alert message '!Alert: This will disable calls for this line until you set the limit above 0. Always Allowed numbers can still be reached.' for Minutes with allowance");
			} else {
				Reporter.log("Alert message is not displayed, as entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the first Alert message for Minutes");
		}
		return this;
	}

	/**
	 * Verify the display of alert message2 for Minutes when current allowance
	 * entered is 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage2WhenCurrentAllowanceIsZeroForMinutes() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer.parseInt(currentAllowanceEnteredValue.getAttribute("value"));
			if (currentAllowanceValue == 0) {
				String s1 = minutesAlert2TextWithAllowance.getText();
				String text = minutesAlertMessage2WithAllowance.getAttribute("innerHTML");
				Assert.assertEquals(s1, "!Alert :");
				Assert.assertEquals(text, expectedMinutesAlertMessage2WithAllowance);
				Reporter.log(
						"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Minutes with allowance");
			} else {
				Reporter.log("Alert message is not displayed, as entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the other Alert message for Minutes");
		}
		return this;
	}

	/**
	 * Verify the alert messages are not displayed for Minutes when current
	 * allowance entered is greater than 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyNoAlertMessagesWhenCurrentAllowanceIsGreaterThanZeroForMinutes() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer.parseInt(currentAllowanceEnteredValue.getAttribute("value"));
			if (currentAllowanceValue > 0) {
				Assert.assertTrue(!isElementDisplayed(minutesAlertMessage1WithAllowance));
				Assert.assertTrue(!isElementDisplayed(minutesAlertMessage2WithAllowance));
				Reporter.log(
						"Successfully verified that the Alert messages are not displayed, when entered current allowance value is greater than 0");
			} else {
				Reporter.log("Alert messages are displayed, when entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Alert messages are displayed, when entered current allowance value is greater than 0");
		}
		return this;
	}

	/**
	 * Verify the display of Tool tip Icon.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertTooltipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(minutesAlertTooltipIcon));
			Reporter.log("Tooltip icon is displayed beside the Alert");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip icon beside the Alert");
		}
		return this;
	}

	/**
	 * Verify the display of Minutes Tool tip message.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertTooltipMessage() {
		try {
			checkPageIsReady();
			Actions Tooltip = new Actions(getDriver());
			Tooltip.clickAndHold(minutesAlertTooltipIcon).perform();
			Assert.assertTrue(isElementDisplayed(minutesAlertTooltipMessage));
			Reporter.log("Tooltip message is displayed successfully beside the Alert");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip message beside the Alert");
		}
		return this;
	}

	/**
	 * Verify the display of Current allowance, Used, Remaining fields
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinutesFieldLabelsDisplay() {
		try {
			checkPageIsReady();
			Assert.assertTrue(currentAllowanceLabel.isDisplayed(), "Current allowance label is not displayed");
			Reporter.log("Current allowance field label is displayed");
			Assert.assertTrue(usedLabel.isDisplayed(), "Used label is not displayed");
			Reporter.log("Used field label is displayed");
			Assert.assertTrue(remainingLabel.isDisplayed(), "Remaining label is not displayed");
			Reporter.log("Remaining field label is displayed");
			Reporter.log("Field labels are verified successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display the field labels");
		}
		return this;
	}

	/**
	 * Verify the field values Current allowance, Used, Remaining fields
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinutesFieldValuesWithoutAllowance() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(currentAllowanceDefaultValueWithoutAllowance),
					"No allowance default value is not displayed");
			Reporter.log("'No allowance' value is displayed in Current allowance field, when there is no allowance");
			String usedFieldValue = usedValue.getText();
			Assert.assertTrue(!usedFieldValue.isEmpty(), "Used field value is null");
			Reporter.log("'" + usedFieldValue + "'"
					+ " value is displayed in Used field as count of minutes used, when there is no allowance");
			Assert.assertTrue(isElementDisplayed(remainingDefaultValueWithoutAllowance),
					"No allowance set default value is not displayed");
			Reporter.log("'No allowance set' value is displayed in Remaining field, when there is no allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the field default values for Minutes without allowance");
		}
		return this;
	}

	/**
	 * Verify field validation error message when current allowance is blank
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceFieldValidationErrorMessageForMinutes() {
		try {
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Actions actions = new Actions(getDriver());
				actions.moveToElement(minutesCurrentAllowanceTextField).click().perform();
				Reporter.log("Clicked on Minutes Current allowance text field");
				minutesCurrentAllowanceTextField.clear();
				minutesCurrentAllowanceTextField.sendKeys(Keys.ENTER);
				Reporter.log("Cleared Minutes Current allowance text field");
				Assert.assertTrue(isElementDisplayed(fieldValidationErrorMessage));
				Reporter.log("'Please enter limit' field validation error message is displayed successfully");
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				elementClick(lineDashBoardPhoneStatusToggle);
				Actions actions = new Actions(getDriver());
				actions.moveToElement(minutesCurrentAllowanceTextField).click().perform();
				Reporter.log("Clicked on Minutes Current allowance text field");
				minutesCurrentAllowanceTextField.clear();
				minutesCurrentAllowanceTextField.sendKeys(Keys.ENTER);
				Reporter.log("Cleared Minutes Current allowance text field");
				Assert.assertTrue(isElementDisplayed(fieldValidationErrorMessage));
				Reporter.log("'Please enter limit' field validation error message is displayed successfully");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the validation error message for current allowance field");
		}
		return this;
	}

	/**
	 * Verify default Remaining field value when current allowance is blank
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyRemainingFieldDefaultValueWhenCurrentAllowanceIsBlank() {
		try {
			checkPageIsReady();
			int remainingValue = Integer.parseInt(minutesRemainingValueWithAllowance.getText());
			Assert.assertEquals(remainingValue, 0);
			Reporter.log("'" + remainingValue + "'"
					+ " default value is set for Remaining field, when current allowance is blank");
		} catch (Exception e) {
			Assert.fail("Failed to display the default value for Remaining field, when current allowance is blank");
		}
		return this;
	}

	/**
	 * Verify the length of current allowance field
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinAndMaxLengthOfCurrentAllowance() {
		checkPageIsReady();
		try {
			int max_length = Integer.parseInt(minutesCurrentAllowanceTextField.getAttribute("maxlength"));
			Assert.assertEquals(max_length, 6);
			Reporter.log(
					"Verified, Maximum length allowed in the Current allowance field is " + max_length + " digits");
			int min_length = Integer.parseInt(minutesCurrentAllowanceTextField.getAttribute("minlength"));
			Assert.assertEquals(min_length, 1);
			Reporter.log("Verified, Minimum length allowed in the Current allowance field is " + min_length + " digit");
		} catch (Exception e) {
			Assert.fail("Failed to verify the minimum and maximum length of Current allowance field");
		}
		return this;
	}

	/**
	 * Verify Calculated Remaining value for Minutes
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyCalculatedRemainingValueForMinutes() {
		checkPageIsReady();
		try {
			int currentAllowanceValue = Integer.parseInt(currentAllowanceEnteredValue.getAttribute("value"));
			int usedFieldValue = Integer.parseInt(usedValue.getText());
			int actualRemainingValue = Integer.parseInt(minutesRemainingValueWithAllowance.getText());
			int expectedRemainingValue = currentAllowanceValue - usedFieldValue;
			Reporter.log("Calculated Remaining value is: " + actualRemainingValue);
			Assert.assertEquals(actualRemainingValue, expectedRemainingValue);
			Reporter.log("Remaining value has been successfully verified ");
		} catch (Exception e) {
			Assert.fail("Failed to calculate and display the Remaining value for Minutes with allowance applied");
		}
		return this;
	}

	/**
	 * verify the display of saved allowance in Current allowance field under
	 * Minutes blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifySavedCurrentAllowanceDisplayedUnderMinutes() {
		try {
			checkPageIsReady();
			int currentAllowanceBeforeSave = Integer
					.parseInt(minutesCurrentAllowanceEnteredValue.getAttribute("value"));
			int usedFieldValueBeforeSave = Integer.parseInt(minutesUsedValue.getText());
			int expectedRemainingValueBeforeSave = currentAllowanceBeforeSave - usedFieldValueBeforeSave;
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickOnMinutesBlade();
			int currentAllowanceAfterSave = Integer.parseInt(minutesCurrentAllowanceEnteredValue.getAttribute("value"));
			int remainingValueAfterSave = Integer.parseInt(minutesRemainingValueWithAllowance.getText());
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
				Reporter.log("current allowance is same before and after when  phone status is in 'ON' status");
				Assert.assertEquals(expectedRemainingValueBeforeSave, remainingValueAfterSave);
				Reporter.log("remaining is same before and after when  phone status is in 'ON' status");
				verifyCalculatedRemainingValueForMinutes();
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
				Reporter.log("current allowance is same before and after when  phone status is in 'OFF' status");
				Assert.assertEquals(0, remainingValueAfterSave);
				Reporter.log("remaining value is zero as phone status is in 'OFF' status");
			} else {
				Assert.fail("Unable to validate values before and after save");
			}
		} catch (Exception e) {
			Assert.fail("Unable to validate allowance and remaining value before and after save");
		}
		return this;
	}

	/**
	 * Check or uncheck No allowance Check box for Minutes
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkOrUncheckMinutesNoAllowanceCheckbox(String expectedCheckState) {
		checkPageIsReady();
		try {
			String actualCheckedState = checkOrUncheckMinutesNoAllowanceCheckBox.getAttribute("value").toString();
			if (actualCheckedState.contains(expectedCheckState)) {
				Reporter.log("No allowance check box is already set to " + expectedCheckState);
			} else {
				Reporter.log("No allowance check box is  set to " + actualCheckedState);
				elementClick(minutesNoAllowanceCheckbox);
				Reporter.log("Clicked on No allowance checkbox");
				Reporter.log("No allowance check box is now set to " + expectedCheckState);
			}
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for No allowance check box");
		}
		return this;
	}

	/**
	 * Enter the data into current allowance
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage enterDataIntoMinutesCurrentAllowance(String currentAllowanceValue) {
		checkPageIsReady();
		try {
			sendTextData(minutesCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + currentAllowanceValue + " into the text box");
		}
		return this;
	}

	/**
	 * verify the Discard Changes does not apply Current allowance field under
	 * Minutes blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithDiscardChangesForMinutes(String currentAllowanceValue) {
		try {
			checkPageIsReady();
			String currentAllowanceExistingValue = minutesCurrentAllowanceTextField.getAttribute("value");
			minutesCurrentAllowanceTextField.clear();
			Reporter.log("Cleared the current allowance text field under Minutes");
			sendTextData(minutesCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
			verifyAndClickOnDiscardChangesButton();
			clickOnMinutesBlade();
			String actualCheckedState = checkOrUncheckMinutesNoAllowanceCheckBox.getAttribute("value").toString();
			if (actualCheckedState.contains("false")) {
				String currentAllowanceValueAfterDiscardChanges = minutesCurrentAllowanceTextField
						.getAttribute("value");
				Assert.assertEquals(currentAllowanceExistingValue, currentAllowanceValueAfterDiscardChanges);
				Reporter.log(
						"Current allowance field is not updated with the entered value after Discard changes, thus verified Discard changes functionality");
			} else {
				Assert.assertTrue(actualCheckedState.contains("true"), "No allowance check box is already set to true");
				Reporter.log("No allowance check box is set to true as before");
			}
		} catch (Exception e) {
			Assert.fail("Current allowance field is updated with the entered value after Discard changes");
		}
		return this;
	}

	/**
	 * verify the allowance is refreshed and overridden with existing value upon
	 * check and uncheck, under Minutes blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithCheckAndUncheckNoAllowanceUnderMinutes(
			String currentAllowanceValue) {
		try {
			checkPageIsReady();

			if (checkOrUncheckMinutesNoAllowanceCheckBox.getAttribute("value").equals("true")) {
				minutesNoAllowanceCheckbox.click();
			}
			String currentAllowanceExistingValue = minutesCurrentAllowanceTextField.getAttribute("value");
			minutesCurrentAllowanceTextField.clear();
			Reporter.log("Cleared the current allowance text field under Minutes");

			sendTextData(minutesCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");

			elementClick(minutesNoAllowanceCheckbox);
			Reporter.log("Checked No allowance check box");

			elementClick(minutesNoAllowanceCheckbox);
			Reporter.log("Unchecked again No allowance check box");

			String currentAllowanceAfterUncheckAndCheckNoAllowance = minutesCurrentAllowanceTextField
					.getAttribute("value");
			Assert.assertEquals(currentAllowanceExistingValue, currentAllowanceAfterUncheckAndCheckNoAllowance);
			Reporter.log(
					"Allowance value is refreshed and the previously saved value is displayed, when we uncheck and check No allowance check box");
		} catch (Exception e) {
			Assert.fail(
					"Able to see the entered allowance in Current allowance field when we uncheck and check No allowance check box");
		}
		return this;
	}

	/**
	 * Verify the line holder name on home page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardLineHolderName() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardLineHolderName));
			Reporter.log("Line Holder Name is Displayed");
		} catch (Exception e) {
			Assert.fail("Line Holder Name not Displayed");
		}
		return this;
	}

	/**
	 * Verify the Body Text under line holder name
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardBodyText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardBodyText));
			Reporter.log("Body text is Displayed");
		} catch (Exception e) {
			Assert.fail("Body text is not Displayed");
		}
		return this;
	}

	/**
	 * Verify and Click the Help link
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardHelp() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardHelp));
			Reporter.log("'Help' text is Displayed");
			clickElement(lineDashBoardHelp);
			checkPageIsReady();
			ArrayList<String> familyAllowanceWindows = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(familyAllowanceWindows.get(1));
			verifyCurrentPageURL("Help Tab", helpTab);
			Reporter.log("'Help' tab URL  is validated");
			getDriver().close();
			getDriver().switchTo().window(familyAllowanceWindows.get(0));
		} catch (Exception e) {
			Assert.fail("'Help' text is not Displayed");
		}
		return this;
	}

	/**
	 * Verify the Billing cycle Text
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardBillingCycle() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardBillingCycle));
			Reporter.log("BillingCycle is Displayed");
		} catch (Exception e) {
			Assert.fail("BillingCycle is not Displayed");
		}
		return this;
	}

	/**
	 * Verify the Line Selector
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardLineSelectorLink() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardLineSelector));
			Reporter.log("Line selector is Displayed");
		} catch (Exception e) {
			Assert.fail("Line selector is not Displayed");
		}
		return this;
	}

	/**
	 * Verify the Line Selector
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardLineSelectorFieldIsEditable() {
		checkPageIsReady();
		try {
			Assert.assertTrue(lineDashBoardLineSelector.isEnabled());
			Reporter.log("Line selector Field is editable");
		} catch (Exception e) {
			Assert.fail("Line selector Field is not editable");
		}
		return this;
	}

	/**
	 * Verify the add new number field for always allowed field
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAllowanceNumbersAlwaysAllowedFieldIsEditable() {
		checkPageIsReady();
		try {
			addNewNumberForAlwaysAllowed.sendKeys("1234567890");
			Assert.assertNotNull(addNewNumberForAlwaysAllowed.getText());
			Reporter.log("add new number field is editable");
		} catch (Exception e) {
			Assert.fail("add new number Field is not editable");
		}
		return this;
	}

	/**
	 * Verify the add new number field for never allowed field
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAllowanceNumbersNeverAllowedFieldIsEditable() {
		checkPageIsReady();
		try {
			addNewNumberForNeverAllowed.sendKeys("1234567890");
			Assert.assertNotNull(addNewNumberForNeverAllowed.getText());
			Reporter.log("add new number field for never allowed field is editable");
		} catch (Exception e) {
			Assert.fail("add new number Field for never allowed is not editable");
		}
		return this;
	}

	/**
	 * Verify phone status toggle is readable or not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardPhoneStatusToggleIsReadable() {
		checkPageIsReady();
		try {
			String actualPhoneStatusState = lineDashBoardCurrentPhoneStatus.getText().toString();
			clickElement(lineDashBoardPhoneStatusToggle);
			String PhoneStatusStateAfterClick = lineDashBoardCurrentPhoneStatus.getText().toString();
			Assert.assertEquals(PhoneStatusStateAfterClick, actualPhoneStatusState);
			Reporter.log("lineDashBoardPhoneStatusToggle field is readable");
		} catch (Exception e) {
			Assert.fail("failed to validate lineDashBoardPhoneStatusToggle field");
		}
		return this;
	}

	/**
	 * Verify Notification check box is readable or not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardNotificationCheckBoxIsReadable() {
		checkPageIsReady();
		try {
			String actualCheckBoxStatusBefore = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			clickElement(lineDashBoardNotificationCheckBox);
			String actualCheckBoxStatusAfter = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			Assert.assertEquals(actualCheckBoxStatusBefore, actualCheckBoxStatusAfter);
			Reporter.log("line DashBoard NotificationCheckBox field is readable");
		} catch (Exception e) {
			Assert.fail("failed to validate line DashBoard NotificationCheckBox field");
		}
		return this;
	}

	/**
	 * Verify the Line Selector
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardParentDropDownFieldIsEditable() {
		checkPageIsReady();
		try {
			Assert.assertTrue(lineDashBoardParentListDropDown.isEnabled());
			Reporter.log("Parent Drop down Field is editable");
		} catch (Exception e) {
			Assert.fail("Parent Drop down Field is not editable");
		}
		return this;
	}

	/**
	 * Verify Parentdrop down is hidden
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardParentDropDownFieldIsHidden() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!lineDashBoardParentListDropDown.isDisplayed());
			Reporter.log("Parent Drop down Field is hidden");
		} catch (Exception e) {
			Assert.fail("Parent Drop down Field is not validated");
		}
		return this;
	}

	/**
	 * Verify line selector down is hidden
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardLineSelectorFieldIsHidden() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!lineDashBoardLineSelector.isEnabled());
			Reporter.log("Line selector field is hidden");
		} catch (Exception e) {
			Reporter.log("Line selector field not hidden");
		}
		return this;
	}

	/**
	 * Verify history of lines field is hidden
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyHistoryOfLinesFieldIsHidden() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!changeHistoryViewListOfLines.isDisplayed());
			Reporter.log("history of lines Field is hidden");
		} catch (Exception e) {
			Reporter.log("history of lines Field is hidden");
		}
		return this;
	}

	/**
	 * Verify history of lines field is hidden
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyViewOfLinesFieldIsHidden() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!viewListOfLines.isDisplayed());
			Reporter.log("view list of lines Field is hidden");
		} catch (Exception e) {
			Reporter.log("view list of lines Field is hidden");
		}
		return this;
	}

	/**
	 * Change the phone status slider to ON if it is OFF and OFF if it is ON
	 * 
	 * @param expectedPhoneStatus
	 * @return
	 */
	public FamilyAllowancesPage lineDashBoardChangePhoneStatus() {
		checkPageIsReady();
		try {
			String actualPhoneStatusState = lineDashBoardCurrentPhoneStatus.getText().toString();
			if (actualPhoneStatusState.equals("ON")) {
				waitFor(ExpectedConditions.visibilityOf(lineDashBoardPhoneStatusToggle));
				elementClick(lineDashBoardPhoneStatusToggle);
				waitforSpinnerinProfilePage();
				lineDashBoardclickYes();
				waitforSpinnerinProfilePage();
				Reporter.log("Phone status is set to OFF");
			} else {
				waitFor(ExpectedConditions.visibilityOf(lineDashBoardPhoneStatusToggle));
				elementClick(lineDashBoardPhoneStatusToggle);
				waitforSpinnerinProfilePage();
				Reporter.log("Phone status is already set to ON");
			}
		} catch (Exception e) {
			Assert.fail("Unable to  state for phone status slider");
		}
		return this;

	}

	/**
	 * Verify apply allowance field is hidden
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyApplyAllowanceFieldIsHidden() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!applyAllowanceChangesToAllLinesCheckbox.isDisplayed());
			Reporter.log("apply allowance Field is hidden");
		} catch (Exception e) {
			Reporter.log("apply allowance Field is hidden");
		}
		return this;
	}

	/**
	 * Verify the Parent List drop down menu
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardParentListDropDown() {
		try {
			checkPageIsReady();
			Actions actions = new Actions(getDriver());
			actions.moveToElement(lineDashBoardParentListDropDown);
			Assert.assertTrue(isElementDisplayed(lineDashBoardParentListDropDown));
			Reporter.log("Parent Line drop down is Displayed");
		} catch (Exception e) {
			Assert.fail("Parent Line drop down is not Displayed");
		}
		return this;
	}

	/**
	 * Verify the phone status toggle
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardPhoneStatusToggle() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardPhoneStatusToggle));
			Reporter.log("Phone Status is Displayed");
		} catch (Exception e) {
			Assert.fail("Phone Status is not  Displayed");
		}
		return this;
	}

	/**
	 * Verify the Notification checkbox
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardNotificationCheckBox() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardNotificationCheckBox));
			Reporter.log("notification check box is displayed");
		} catch (Exception e) {
			Assert.fail("notification check box is displayed");
		}
		return this;
	}

	/**
	 * Verify the Tool tip icon
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardParentToolTipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardVerifyParentToolTopIcon));
			Reporter.log("Parent tool tip Icon is Displayed");
		} catch (Exception e) {
			Assert.fail("Parent tool tip Icon is not Displayed");
		}
		return this;
	}

	/**
	 * Validate the tool tip Text
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardParentToolTipText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineDashBoardVerifyParentToolTopText));
			Reporter.log("Parent tool tip Text is Displayed");
		} catch (Exception e) {
			Assert.fail("Parent tool tip Text is not Identified");
		}
		return this;
	}

	/**
	 * Change the phone status slider to ON or OFF
	 * 
	 * @param expectedPhoneStatus
	 * @return
	 */
	public FamilyAllowancesPage lineDashBoardChangeOnOrOffPhoneStatus(String expectedPhoneStatus) {
		checkPageIsReady();
		try {
			String actualPhoneStatusState = lineDashBoardCurrentPhoneStatus.getText().toString();
			if (!actualPhoneStatusState.equals(expectedPhoneStatus)) {
				waitFor(ExpectedConditions.visibilityOf(lineDashBoardPhoneStatusToggle));
				elementClick(lineDashBoardPhoneStatusToggle);
				waitforSpinnerinProfilePage();
				Reporter.log("Phone status is set to " + expectedPhoneStatus);
			} else {
				waitFor(ExpectedConditions.visibilityOf(lineDashBoardPhoneStatusToggle));
				elementClick(lineDashBoardPhoneStatusToggle);
				waitforSpinnerinProfilePage();
				lineDashBoardChangeOnOrOffPhoneStatus(expectedPhoneStatus);
				Reporter.log("Phone status is already set to " + expectedPhoneStatus);
			}
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for phone status slider");
		}
		return this;
	}

	/**
	 * cj the phone status slider to ON or OFF
	 * 
	 * @param expectedPhoneStatus
	 * @return
	 */
	public FamilyAllowancesPage lineDashBoardCheckOrUnCheckNotification(String expectedCheckBoxStatus) {
		checkPageIsReady();
		try {
			String actualCheckBoxStatus = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			if (!actualCheckBoxStatus.equals(expectedCheckBoxStatus)) {
				elementClick(lineDashBoardNotificationCheckBox);
				waitForSpinnerInvisibility();
				Reporter.log("Notification check box is changed to " + expectedCheckBoxStatus);
			} else {
				elementClick(lineDashBoardNotificationCheckBox);
				waitForSpinnerInvisibility();
			}
		} catch (Exception e) {
			Assert.fail("Unable to set Notification  check box state to" + expectedCheckBoxStatus);
		}
		return this;
	}

	/**
	 * It will click YES once the dialog box is displayed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage lineDashBoardclickYes() {
		checkPageIsReady();
		try {
			clickElement(lineDashBoardClickYes);
		} catch (Exception e) {
			Assert.fail("Unable to click yes");
		}
		return this;
	}

	/**
	 * It will click NO once the dialog box is displayed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage lineDashBoardclickNo() {
		checkPageIsReady();
		try {
			clickElement(lineDashBoardClickNo);
			Reporter.log("clicked No");
		} catch (Exception e) {
			Assert.fail("Unable to click No");
		}
		return this;
	}

	/**
	 * It will verify the existing phone status
	 * 
	 * @param existingPhoneStatus
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardPhoneToggleStatusOffOrOn(String existingPhoneStatus) {
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementText(lineDashBoardCurrentPhoneStatus, existingPhoneStatus));
			Reporter.log("status is set to expected state" + existingPhoneStatus);
		} catch (Exception e) {
			Assert.fail("Phone status is not set to " + existingPhoneStatus);
		}
		return this;
	}

	/**
	 * I will validate the line selector name and line holder name
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyLineDashBoardNameAndLineSelector() {
		checkPageIsReady();
		try {
			String lineSelectorName = lineDashBoardLineSelector.getAttribute("value");
			String[] lineSelector = lineSelectorName.split(",");
			String lineName = lineDashBoardLineHolderName.getText();
			if (lineName.contains(lineSelector[0])) {
				Reporter.log("Missdin name is same in line selecor and heading");
			} else {
				Reporter.log("Missdin name is not same in line selecor and heading");
				Assert.fail("Missdin name is not same in line selecor and heading");
			}
		} catch (Exception e) {
			Reporter.log("Missdin name is not same in line selecor and heading");
			Assert.fail("Missdin name is not same in line selecor and heading");
		}
		return this;
	}

	/**
	 * change the notification and phone status value and navigate to view list
	 * of lines and navigate back and compare before and after navigation
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyFieldStatesBeforeAndAfter() {
		checkPageIsReady();
		try {
			lineDashBoardChangePhoneStatus();
			lineDashBoardChangeNotificationCheckBoxStatus();
			String phoneStatusBefore = lineDashBoardCurrentPhoneStatus.getText();
			String notificationCheckBoxBefore = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			navigateToViewListOfLinesFromFamilyAllowance();
			navigateBack();
			verifyLineDashboardPage();
			String phoneStatusAfter = lineDashBoardCurrentPhoneStatus.getText();
			String notificationCheckBoxAfter = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			Assert.assertEquals(phoneStatusBefore, phoneStatusAfter);
			Reporter.log("phoneStatus is  same before and After navigating to view list of lines page");
			Assert.assertNotEquals(notificationCheckBoxBefore, notificationCheckBoxAfter);
			Reporter.log("notificationCheckBox is not same before and After");
		} catch (Exception e) {
			Reporter.log("states are not same before and After");
			Assert.fail("states are not same before and After");
		}
		return this;
	}

	/**
	 * change the notification value and navigate to view list of lines and
	 * navigate back and compare before and after navigation
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNotificationCheckBoxBeforeAndAfterNavigatonToViewListOfLines() {
		checkPageIsReady();
		try {
			lineDashBoardChangeNotificationCheckBoxStatus();
			String notificationCheckBoxBefore = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			navigateToViewListOfLinesFromFamilyAllowance();
			navigateBack();
			verifyLineDashboardPage();
			String notificationCheckBoxAfter = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			Assert.assertNotEquals(notificationCheckBoxBefore, notificationCheckBoxAfter);
			Reporter.log("notificationCheckBox is not same before and After");
		} catch (Exception e) {
			Reporter.log("states are not same before and After");
			Assert.fail("notification checkbox is  not same before and After navigation");
		}
		return this;
	}

	/**
	 * change the phone status and navigate to view list of lines and navigate
	 * back and compare before and after navigation
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyPhoneStatusBeforeAndAfterNavigatonToViewListOfLines() {
		checkPageIsReady();
		try {
			lineDashBoardChangePhoneStatus();
			String phoneStatusBefore = lineDashBoardCurrentPhoneStatus.getText();
			navigateToViewListOfLinesFromFamilyAllowance();
			navigateBack();
			verifyLineDashboardPage();
			String phoneStatusAfter = lineDashBoardCurrentPhoneStatus.getText();
			Assert.assertEquals(phoneStatusBefore, phoneStatusAfter);
			Reporter.log("phoneStatus is  same before and After navigating to view list of lines page");
		} catch (Exception e) {
			Assert.fail("failed to validate phone status before and after navigation to view list of lines");
		}
		return this;
	}

	/**
	 * Verify the display of Messages blade header.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesBladeHeader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(messagesHeader));
			Reporter.log("Messages blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Messages blade");
		}
		return this;
	}

	/**
	 * Click on Messages blade.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clickOnMessagesBlade() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			if (messagesToggleButton.getAttribute("Class").contains(toggleUpArrow)) {
				Reporter.log("Messages blade is already expanded");
			} else {
				elementClick(messagesHeader);
				Reporter.log("Messages blade is expanded successfully");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Messages blade");
		}
		return this;
	}

	/**
	 * Verify the display of Messages blade description.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesBladeDescription() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(messagesDescription));
			Reporter.log("Content below Messages blade header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Content below Messages blade header");
		}
		return this;
	}

	/**
	 * verify No allowance in Current allowance field after saving with No
	 * allowance under Minutes blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithSaveNoAllowanceUnderMinutes() {
		try {
			checkPageIsReady();
			String currentAllowanceBeforeSave = minutesCurrentAllowanceDefaultValueWithoutAllowance.getText();
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickOnMinutesBlade();
			String currentAllowanceAfterSave = minutesCurrentAllowanceDefaultValueWithoutAllowance.getText();
			Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
			Reporter.log("'No allowance' is displayed in Current allowance field after Saving with No allowance");
		} catch (Exception e) {
			Assert.fail("Unable to see the No allowance value in Current allowance field");
		}
		return this;
	}

	/***
	 * verify toggle down arrow is clickable for Messages
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesToggleArrow() {
		try {
			checkPageIsReady();
			Assert.assertTrue(messagesToggleButton.isDisplayed() && messagesToggleButton.isEnabled());
			Reporter.log("Verified Messages toggle arrow is displayed successfully and is clickable");
		} catch (Exception e) {
			Assert.fail("Unable to see the toggle arrow for Messages blade");
		}
		return this;
	}

	/***
	 * verify the display of No allowance text
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesNoAllowanceCheckBox() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(messagesNoAllowanceCheckboxText),
					"No Allowance text is not displayed");
			Reporter.log("No Allowance check box text is displayed");
			Assert.assertTrue(isElementDisplayed(messagesNoAllowanceCheckbox),
					"No Allowance check box is not displayed");
			Reporter.log("No Allowance check box is displayed");
			//System.out.println("messagesNoAllowanceCheckbox" + messagesNoAllowanceCheckbox.isSelected());
			//System.out.println("messagesNoAllowanceCheckbox" + messagesNoAllowanceCheckbox.isSelected());
			//System.out.println("messagesNoAllowanceCheckbox" + messagesNoAllowanceCheckbox.isSelected());
			//System.out.println(getDriver().findElement(By.xpath("//span[text()= 'Messages']/../following-sibling::div/div/span/label/input")).isSelected());
			getDriver()
					.findElement(By.xpath("//span[text()= 'Messages']/../following-sibling::div/div/span/label/input"))
					.isSelected();
			Assert.assertTrue(isElementEnabled(messagesNoAllowanceCheckbox), "No Allowance check box is not enabled");
			Reporter.log("No Allowance check box is enabled and is clickable");
		} catch (Exception e) {
			Assert.fail("No Allowance checkbox is not clickable");
		}
		return this;
	}

	/**
	 * Verify the display of Tool tip Icon for Messages.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesNoAllowanceTooltipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(messagesNoAllowanceTooltipIcon));
			Reporter.log("Tooltip icon is displayed beside No allowance check box");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip icon");
		}
		return this;
	}

	/**
	 * Verify the display of Messages Tool tip message.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesNoAllowanceTooltipMessage() {
		try {
			checkPageIsReady();
			Actions Tooltip = new Actions(getDriver());
			Tooltip.clickAndHold(messagesNoAllowanceTooltipIcon).perform();
			Assert.assertTrue(isElementDisplayed(messagesNoAllowanceTooltipMessage));
			Reporter.log(" Tooltip message is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip message for Messages");
		}
		return this;
	}

	/**
	 * Verify the display of alert message for Messages without allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWithoutAllowanceForMessages() {
		try {
			checkPageIsReady();
			String s1 = messagesNoAllowanceAlertText.getText();
			String s = messagesNoAllowanceAlertMessage.getText();
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(s,
					"This line has no allowance set, meaning that this line�s user can message without restrictions. If you do not have an Unlimited Plan, this could result in overage charges on your bill.");
			Reporter.log("Successfully verified the Alert message for Messages without allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the Alert message for Messages");
		}
		return this;
	}

	/**
	 * Clear current allowance text field for Messages with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clearCurrentAllowanceTextFieldUnderMessages() {
		try {
			checkPageIsReady();
			elementClick(messagesCurrentAllowanceTextField);
			Reporter.log("Clicked on Current allowance text field");
			messagesCurrentAllowanceTextField.clear();
			messagesCurrentAllowanceTextField.sendKeys(Keys.ENTER);
			Reporter.log("Cleared Current allowance text field");
		} catch (Exception e) {
			Assert.fail("Failed to clear the Current allowance text field Under Messages");
		}
		return this;
	}

	/**
	 * Verify the display of alert message1 for Messages with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage1WithAllowanceForMessages() {
		try {
			checkPageIsReady();
			String s1 = messagesAlert1TextWithAllowance.getText();
			String text = messagesAlertMessage1WithAllowance.getAttribute("innerHTML");
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(text, expectedMessagesAlertMessage1WithAllowance);
			Reporter.log(
					"Successfully verified the Alert message '!Alert: Messaging will be blocked for this line until you set the limit above 0. Always Allowed numbers can still be reached.' for Messages with allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the first Alert message for Messages");
		}
		return this;
	}

	/**
	 * Verify the display of alert message2 for Messages with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage2WithAllowanceForMessages() {
		try {
			checkPageIsReady();
			String s1 = messagesAlert2TextWithAllowance.getText();
			String text = messagesAlertMessage2WithAllowance.getAttribute("innerHTML");
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(text, expectedMessagesAlertMessage2WithAllowance);
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have exceeded your total message allowance.' for Messages with allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the other Alert message for Messages");
		}
		return this;
	}

	/**
	 * Verify the display of alert message1 for Messages when current allowance
	 * entered is 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage1WhenCurrentAllowanceIsZeroForMessages() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer.parseInt(messagesCurrentAllowanceEnteredValue.getAttribute("value"));
			if (currentAllowanceValue == 0) {
				String s1 = messagesAlert1TextWithAllowance.getText();
				String text = messagesAlertMessage1WithAllowance.getAttribute("innerHTML");
				Assert.assertEquals(s1, "!Alert :");
				Assert.assertEquals(text, expectedMessagesAlertMessage1WithAllowance);
				Reporter.log(
						"Successfully verified the Alert message '!Alert: Messaging will be blocked for this line until you set the limit above 0. Always Allowed numbers can still be reached.' for Messages with allowance");
			} else {
				Reporter.log("Alert message is not displayed, as entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the first Alert message for Messages");
		}
		return this;
	}

	/**
	 * Verify the display of alert message2 for Messages when current allowance
	 * entered is 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage2WhenCurrentAllowanceIsZeroForMessages() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer.parseInt(messagesCurrentAllowanceEnteredValue.getAttribute("value"));
			if (currentAllowanceValue == 0) {
				String s1 = messagesAlert2TextWithAllowance.getText();
				String text = messagesAlertMessage2WithAllowance.getAttribute("innerHTML");
				Assert.assertEquals(s1, "!Alert :");
				Assert.assertEquals(text, expectedMessagesAlertMessage2WithAllowance);
				Reporter.log(
						"Successfully verified the Alert message '!Alert: You have exceeded your total message allowance.' for Messages with allowance");
			} else {
				Reporter.log("Alert message is not displayed, as entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the other Alert message for Messages");
		}
		return this;
	}

	/**
	 * Verify the alert messages are not displayed for Messages when current
	 * allowance entered is greater than 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyNoAlertMessagesWhenCurrentAllowanceIsGreaterThanZeroForMessages() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer.parseInt(messagesCurrentAllowanceEnteredValue.getAttribute("value"));
			if (currentAllowanceValue > 0) {
				Assert.assertTrue(!isElementDisplayed(messagesAlertMessage1WithAllowance));
				Assert.assertTrue(!isElementDisplayed(messagesAlertMessage2WithAllowance));
				Reporter.log(
						"Successfully verified that the Alert messages are not displayed, when entered current allowance value is greater than 0");
			} else {
				Reporter.log("Alert messages are displayed, when entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Alert messages are displayed, when entered current allowance value is greater than 0");
		}
		return this;
	}

	/**
	 * Verify the display of Tool tip Icon for Messages.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesAlertTooltipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(messagesAlertTooltipIcon));
			Reporter.log("Tooltip icon is displayed beside the Alert");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip icon beside the Alert");
		}
		return this;
	}

	/**
	 * Verify the display of Messages Tool tip message.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesAlertTooltipMessage() {
		try {
			checkPageIsReady();
			Actions Tooltip = new Actions(getDriver());
			Tooltip.clickAndHold(messagesAlertTooltipIcon).perform();
			Assert.assertTrue(isElementDisplayed(messagesAlertTooltipMessage));
			Reporter.log("Tooltip message is displayed successfully beside the Alert");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip message beside the Alert");
		}
		return this;
	}

	/**
	 * Check or uncheck No allowance Check box for Messages
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkOrUncheckMessagesNoAllowanceCheckbox(String expectedCheckState) {
		checkPageIsReady();
		try {
			String actualCheckedState = checkOrUncheckMessagesNoAllowanceCheckBox.getAttribute("value").toString();
			if (actualCheckedState.contains(expectedCheckState)) {
				Reporter.log("No allowance check box is already set to " + expectedCheckState);
			} else {
				Reporter.log("No allowance check box is  set to " + actualCheckedState);
				elementClick(messagesNoAllowanceCheckbox);
				Reporter.log("Clicked on No allowance checkbox");
				Reporter.log("No allowance check box is now set to " + expectedCheckState);
			}
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for No allowance check box");
		}
		return this;
	}

	/**
	 * Verify the display of Current allowance, Used, Remaining fields in
	 * Messages blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */

	public FamilyAllowancesPage verifyMessagesFieldLabelsDisplay() {
		try {
			checkPageIsReady();
			Assert.assertTrue(messagesCurrentAllowanceLabel.isDisplayed(), "Current allowance label is not displayed");
			Reporter.log("Current allowance field label is displayed");
			Assert.assertTrue(messagesUsedLabel.isDisplayed(), "Used label is not displayed");
			Reporter.log("Used field label is displayed");
			Assert.assertTrue(messagesRemainingLabel.isDisplayed(), "Remaining label is not displayed");
			Reporter.log("Remaining field label is displayed");
			Reporter.log("Field labels are verified successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display the field labels");
		}
		return this;
	}

	/**
	 * Verify the field values Current allowance, Used, Remaining fields in
	 * Messages blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesFieldValuesWithoutAllowance() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(messagesCurrentAllowanceDefaultValueWithoutAllowance),
					"No allowance default value is not displayed");
			Reporter.log("'No allowance' value is displayed in Current allowance field, when there is no allowance");
			String usedFieldValue = messagesUsedValue.getText();
			Assert.assertTrue(!usedFieldValue.isEmpty(), "Used field value is null");
			Reporter.log("'" + usedFieldValue + "'"
					+ " value is displayed in Used field as count of messages used, when there is no allowance");
			Assert.assertTrue(isElementDisplayed(messagesRemainingDefaultValueWithoutAllowance),
					"No allowance set default value is not displayed");
			Reporter.log("'No allowance set' value is displayed in Remaining field, when there is no allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the field default values for Messages without allowance");
		}
		return this;
	}

	/**
	 * Verify field validation error message when current allowance is blank for
	 * Messages
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceFieldValidationErrorMessageForMessages() {
		try {
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Actions actions = new Actions(getDriver());
				actions.moveToElement(messagesCurrentAllowanceTextField).click().perform();
				Reporter.log("Clicked on Messages Current allowance text field");
				messagesCurrentAllowanceTextField.clear();
				messagesCurrentAllowanceTextField.sendKeys(Keys.ENTER);
				Reporter.log("Cleared Messages Current allowance text field");
				Assert.assertTrue(isElementDisplayed(messagesFieldValidationErrorMessage));
				Reporter.log("'Please enter limit' field validation error message is displayed successfully");
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				elementClick(lineDashBoardPhoneStatusToggle);
				Actions actions = new Actions(getDriver());
				actions.moveToElement(messagesCurrentAllowanceTextField).click().perform();
				Reporter.log("Clicked on Messages Current allowance text field");
				messagesCurrentAllowanceTextField.clear();
				messagesCurrentAllowanceTextField.sendKeys(Keys.ENTER);
				Reporter.log("Cleared Messages Current allowance text field");
				Assert.assertTrue(isElementDisplayed(messagesFieldValidationErrorMessage));
				Reporter.log("'Please enter limit' field validation error message is displayed successfully");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the validation error message for current allowance field");
		}
		return this;
	}

	/**
	 * Verify default Remaining field value when current allowance is blank in
	 * Messages blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMessagesRemainingFieldDefaultValueWhenCurrentAllowanceIsBlank() {
		try {
			checkPageIsReady();
			int remainingValue = Integer.parseInt(messagesRemainingValueWithAllowance.getText());
			Assert.assertEquals(remainingValue, 0);
			Reporter.log("'" + remainingValue + "'"
					+ " default value is set for Remaining field, when current allowance is blank");
		} catch (Exception e) {
			Assert.fail("Failed to display the default value for Remaining field, when current allowance is blank");
		}
		return this;
	}

	/**
	 * Verify the length of current allowance field under Messages blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinAndMaxLengthOfMessagesCurrentAllowance() {
		checkPageIsReady();
		try {
			int max_length = Integer.parseInt(messagesCurrentAllowanceTextField.getAttribute("maxlength"));
			Assert.assertEquals(max_length, 6);
			Reporter.log(
					"Verified, Maximum length allowed in the Current allowance field is " + max_length + " digits");
			int min_length = Integer.parseInt(messagesCurrentAllowanceTextField.getAttribute("minlength"));
			Assert.assertEquals(min_length, 1);
			Reporter.log("Verified, Minimum length allowed in the Current allowance field is " + min_length + " digit");
		} catch (Exception e) {
			Assert.fail("Failed to verify the minimum and maximum length of Current allowance field");
		}
		return this;
	}

	/**
	 * Enter the data into current allowance under Messages blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage enterDataIntoMessagesCurrentAllowance(String currentAllowanceValue) {
		checkPageIsReady();
		try {
			//System.out.println("currentAllowanceValue" + currentAllowanceValue);
			sendTextData(messagesCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + currentAllowanceValue + " into the text box");
		}
		return this;
	}

	/**
	 * Verify Calculated Remaining value for Messages
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyCalculatedRemainingValueForMessages() {
		checkPageIsReady();
		try {
			int currentAllowanceValue = Integer.parseInt(messagesCurrentAllowanceEnteredValue.getAttribute("value"));
			int usedFieldValue = Integer.parseInt(messagesUsedValue.getText());
			int actualRemainingValue = Integer.parseInt(messagesRemainingValueWithAllowance.getText());
			int expectedRemainingValue = currentAllowanceValue - usedFieldValue;
			Reporter.log("Calculated Remaining value is: " + actualRemainingValue);
			Assert.assertEquals(actualRemainingValue, expectedRemainingValue);
			Reporter.log("Remaining value has been successfully verified ");
		} catch (Exception e) {
			Assert.fail("Failed to calculate and display the Remaining value for Messages with allowance applied");
		}
		return this;
	}

	/**
	 * verify the display of saved allowance in Current allowance field under
	 * Messages blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifySavedCurrentAllowanceDisplayedUnderMessages() {
		try {
			checkPageIsReady();
			int currentAllowanceBeforeSave = Integer
					.parseInt(messagesCurrentAllowanceEnteredValue.getAttribute("value"));
			int usedFieldValueBeforeSave = Integer.parseInt(messagesUsedValue.getText());
			int expectedRemainingValueBeforeSave = currentAllowanceBeforeSave - usedFieldValueBeforeSave;
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickOnMessagesBlade();
			int currentAllowanceAfterSave = Integer
					.parseInt(messagesCurrentAllowanceEnteredValue.getAttribute("value"));
			int remainingValueAfterSave = Integer.parseInt(messagesRemainingValueWithAllowance.getText());
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
				Reporter.log("current allowance is same before and after when  phone status is in 'ON' status");
				Assert.assertEquals(expectedRemainingValueBeforeSave, remainingValueAfterSave);
				Reporter.log("remaining is same before and after when  phone status is in 'ON' status");
				verifyCalculatedRemainingValueForMessages();
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
				Reporter.log("current allowance is same before and after when  phone status is in 'OFF' status");
				Assert.assertEquals(0, remainingValueAfterSave);
				Reporter.log("remaining value is zero as phone status is in 'OFF' status");
			} else {
				Assert.fail("Unable to validate values before and after save");
			}
		} catch (Exception e) {
			Assert.fail("Unable to validate allowance and remaining value before and after save");
		}
		return this;
	}

	/**
	 * verify No allowance in Current allowance field after saving with No
	 * allowance under Messages blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithSaveNoAllowanceUnderMessages() {
		try {
			checkPageIsReady();
			String currentAllowanceBeforeSave = messagesCurrentAllowanceDefaultValueWithoutAllowance.getText();
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickOnMessagesBlade();
			String currentAllowanceAfterSave = messagesCurrentAllowanceDefaultValueWithoutAllowance.getText();
			Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
			Reporter.log("'No allowance' is displayed in Current allowance field after Saving with No allowance");
		} catch (Exception e) {
			Assert.fail("Unable to see the No allowance value in Current allowance field");
		}
		return this;
	}

	/**
	 * verify the Discard Changes does not apply Current allowance field under
	 * Messages blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithDiscardChangesForMessages(String currentAllowanceValue) {
		try {
			checkPageIsReady();
			String currentAllowanceExistingValue = messagesCurrentAllowanceTextField.getAttribute("value");
			messagesCurrentAllowanceTextField.clear();
			Reporter.log("Cleared the Current allowance text field under Messages");
			sendTextData(messagesCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
			verifyAndClickOnDiscardChangesButton();
			clickOnMessagesBlade();
			String actualCheckedState = checkOrUncheckMessagesNoAllowanceCheckBox.getAttribute("value").toString();
			if (actualCheckedState.contains("false")) {
				String currentAllowanceValueAfterDiscardChanges = messagesCurrentAllowanceTextField
						.getAttribute("value");
				Assert.assertEquals(currentAllowanceExistingValue, currentAllowanceValueAfterDiscardChanges);
				Reporter.log(
						"Current allowance field is not updated with the entered value after Discard changes, thus verified Discard changes functionality");
			} else {
				Assert.assertTrue(actualCheckedState.contains("true"), "No allowance check box is already set to true");
				Reporter.log("No allowance check box is set to true as before");
			}
		} catch (Exception e) {
			Assert.fail("Current allowance field is updated with the entered value after Discard changes");
		}
		return this;
	}

	/**
	 * verify the display of saved allowance in Current allowance field under
	 * Messages blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithCheckAndUncheckNoAllowanceUnderMessages(
			String currentAllowanceValue) {
		try {
			checkPageIsReady();
			String currentAllowanceExistingValue = messagesCurrentAllowanceTextField.getAttribute("value");
			sendTextData(messagesCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
			elementClick(messagesNoAllowanceCheckbox);
			Reporter.log("Checked No allowance check box");
			elementClick(messagesNoAllowanceCheckbox);
			Reporter.log("Unchecked again No allowance check box");
			String currentAllowanceAfterUncheckAndCheckNoAllowance = messagesCurrentAllowanceTextField
					.getAttribute("value");
			Assert.assertEquals(currentAllowanceExistingValue, currentAllowanceAfterUncheckAndCheckNoAllowance);
			Reporter.log(
					"Allowance value is refreshed and the previously saved value is displayed, when we uncheck and check No allowance check box");
		} catch (Exception e) {
			Assert.fail(
					"Able to see the entered allowance in Current allowance field when we uncheck and check No allowance check box");
		}
		return this;
	}

	/**
	 * Verify and Click on Save button
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndClickOnSaveButton() {
		try {
			moveToElement(saveButton);	
			Assert.assertTrue(saveButton.isDisplayed() && saveButton.isEnabled(), "Unable to see Save button");
			clickElement(saveButton);
			Reporter.log("Clicked on Save button");
		} catch (Exception e) {
			Assert.fail("Unable to click on Save button");
		}
		return this;
	}

	/**
	 * Verify and Click on Discard Changes button
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndClickOnDiscardChangesButton() {
		checkPageIsReady();
		try {
			Assert.assertTrue(discardChangesButton.isDisplayed() && discardChangesButton.isEnabled(),
					"Unable to see Discard Changes button");
			clickElement(discardChangesButton);
			Reporter.log("Clicked on Discard Changes button");
		} catch (Exception e) {
			Assert.fail("Unable to click on Discard Changes button");
		}
		return this;
	}

	/***
	 * verify and click on Apply Allowance Changes To All Lines check box
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndClickOnApplyAllowanceChangesToAllLinesCheckbox() {
		checkPageIsReady();
		try {
			Assert.assertTrue(applyAllowanceChangesToAllLinesCheckboxText.isDisplayed(),
					"Apply Allowance Changes to all lines text is not displayed");
			Reporter.log("Apply Allowance Changes to all lines text is displayed");
			Assert.assertTrue(applyAllowanceChangesToAllLinesCheckbox.isEnabled(),
					"Apply Allowance Changes to all lines check box is not enabled");
			Reporter.log("Apply Allowance Changes to all lines check box is enabled");
			clickElementWithJavaScript(applyAllowanceChangesToAllLinesCheckbox);
			Reporter.log("Clicked on Apply Allowance Changes to all lines checkbox");
			Reporter.log("Apply Allowance Changes to all lines check box is now set to true");
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for Apply Allowance Changes to all lines check box");
		}
		return this;
	}

	/**
	 * It will click Change history Blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndClickChangeHistory() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(changeHistoryBlade));
			clickElement(changeHistoryBlade);
			waitforSpinnerinProfilePage();
			Reporter.log("Clicked change history successfully");
		} catch (Exception e) {
			Reporter.log("Unable to click Change History Blade");
			Assert.fail("Unable to click Change History Blade");
		}
		return this;
	}

	/**
	 * It will click Change history Blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyChangeHistoryBillingCycleDropDownIsEditable() {
		checkPageIsReady();
		try {
			Assert.assertTrue(changeHistoryBillingCycleDropDown.isEnabled());
			Reporter.log("Clicked change history drop down  successfully");
		} catch (Exception e) {
			Reporter.log("change history billing cycle drop dwon is not editable");
			Assert.fail("change history billing cycle drop dwon is not editable");
		}
		return this;
	}

	/**
	 * It will verify billing Cycle
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyChangeHistoryBillingCycle() {
		checkPageIsReady();
		try {
			Assert.assertNotNull(lineDashboardBillingcycle.getText());
			String[] billingCycle = lineDashboardBillingcycle.getText().split("-");
			Assert.assertTrue(validateBillingCycleDateFormate(billingCycle[0].trim()));
			Assert.assertTrue(validateBillingCycleDateFormate(billingCycle[1].trim()));
			Reporter.log("default billing cycle date is validated");
		} catch (Exception e) {
			Reporter.log("unable to validate default billing cycle");
			Assert.fail("unable to validate default billing cycle");
		}
		return this;
	}

	/**
	 * It will verify view list of lines CTA
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyChangeHistoryViewListOfLinesCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(changeHistoryViewListOfLines));			
			clickElement(changeHistoryViewListOfLines);
			waitforSpinnerinProfilePage();
			Assert.assertTrue(getDriver().getCurrentUrl().contains(lineHistory));
			Reporter.log("View List Of Lines CTA validated successfully");
		} catch (Exception e) {
			Reporter.log("unable to click View List Of Lines CTA");
			Assert.fail("unable to click View List Of Lines CTA");
		}
		return this;
	}

	/**
	 * It will verify recent 3 months bill cycle
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyChangeHistoryThreeMonthsBillcycle() {
		checkPageIsReady();
		try {
			if (changeHistoryPastBillingCycle.size() == 3) {
				Reporter.log("Past 3 months bill cycle is displayed");
			} else {
				Reporter.log("Past 3 months bill cycle is not displayed");
				Assert.fail("Past 3 months bill cycle is not displayed");
			}
		} catch (Exception e) {
			Reporter.log("Past 3 months bill cycle is not displayed");
			Assert.fail("Past 3 months bill cycle is not displayed");
		}
		return this;
	}

	/**
	 * It will click the Schedule blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickScheduleBlade() {
		checkPageIsReady();
		try {
			clickElement(schedule);
			Reporter.log("clicked schedule blade successfully");
		} catch (Exception e) {
			Reporter.log("unable to click Schedule");
			Assert.fail("unable to click Schedule");
		}

		return this;
	}

	/**
	 * It will Verify heading and description
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyScheduleHeadingDescription() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(scheduleHeading));
			Reporter.log("Schedule heading is displayed successfully");

		} catch (Exception e) {
			Reporter.log("Schedule heading is not displayed successfully");
			Assert.fail("Schedule heading is not displayed successfully");
		}

		return this;
	}

	/**
	 * It will Verify heading and description tool tip
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyScheduleHeadingToolTip() {
		checkPageIsReady();
		try {
			Assert.assertTrue(scheduleHeadingToolTip.isDisplayed());
			Reporter.log("Schedule heading tool tip is displayed successfully");
		} catch (Exception e) {
			Reporter.log("Schedule heading tool tip  is not displayed successfully");
			Assert.fail("Schedule heading tool tip is not displayed successfully");
		}
		return this;
	}

	/**
	 * It will Verify heading and description tool tip text
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyScheduleHeadingToolTipText() {
		checkPageIsReady();
		try {
			Assert.assertTrue(scheduleHeadingToolTipText.isDisplayed());
			Reporter.log("Tool Tip Text is validated successfully");
		} catch (Exception e) {
			Reporter.log("Tool Tip Text is not validated");
			Assert.fail("Tool Tip Text is not validated ");
		}
		return this;
	}

	/**
	 * It will verify whether Minutes accordion is in collapsed state or not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMinutesAccordianStatusCollapsed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(minutesAccordian.getAttribute("class").contains(accordionCollapseStatus));
			Reporter.log("Minutes accordian is in collapsed status");
		} catch (Exception e) {
			Reporter.log("Unable to Identify minutes accordian");
			Assert.fail("Unable to Identify minutes accordian");
		}
		return this;
	}

	/**
	 * It will verify whether Messages accordion is in collapsed state or not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesAccordianStatusCollapsed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messagesAccordian.getAttribute("class").contains(accordionCollapseStatus),
					"message accordian not collapsed");
			Reporter.log("Messages accordian is in collapsed status");
		} catch (Exception e) {
			Reporter.log("Unable to Identify Messages accordian");
			Assert.fail("Unable to Identify Messages accordian");
		}
		return this;
	}

	/**
	 * It will verify whether download accordion is in collapsed state or not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDownloadsAccordianStatusCollapsed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(downloadsAccordian.getAttribute("class").contains(accordionCollapseStatus));
			Reporter.log("Downloads accordian is in collapsed status");

		} catch (Exception e) {
			Reporter.log("Unable to Identify Downloads accordian");
			Assert.fail("Unable to Identify Downloads accordian");
		}
		return this;
	}

	/**
	 * It will verify whether Schedule accordion is in collapsed state or not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyScheduleAccordianStatusCollapsed() {
		checkPageIsReady();
		try {
			// String tmp=scheduleAccordian.getAttribute("class");
			Assert.assertTrue(scheduleAccordian.getAttribute("class").contains(accordionCollapseStatus));
			Reporter.log("Schedule accordian is in collapsed status");
		} catch (Exception e) {
			Reporter.log("Unable to Identify Schedule accordian");
			Assert.fail("Unable to Identify Schedule accordian");
		}
		return this;
	}

	/**
	 * It will verify whether change history accordion is in collapsed state or
	 * not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifychangeHistoryAccordianStatusCollapsed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(changeHistoryAccordian.getAttribute("class").contains(accordionCollapseStatus));
			Reporter.log("change history accordian is in collapsed status");

		} catch (Exception e) {
			Reporter.log("Unable to Identify change history accordian");
			Assert.fail("Unable to Identify change history accordian");
		}
		return this;
	}

	/**
	 * It will expand the minutes accordion by clicking
	 * 
	 * @return
	 */
	public FamilyAllowancesPage expandMinutesAccordian() {
		checkPageIsReady();
		try {
			clickElement(minutesAccordian);
			Reporter.log("minutes accordian is expanded successfully");
		} catch (Exception e) {
			Reporter.log("Unable to Identify minutes accordian");
			Assert.fail("Unable to Identify minutes accordian");
		}
		return this;
	}

	/**
	 * It will enter the value in select line text box
	 * 
	 * @param selectLine
	 * @return
	 */

	public FamilyAllowancesPage updateSelectLine(String selectLine) {
		checkPageIsReady();
		try {

			sendTextData(lineDashBoardLineSelector, selectLine);
			lineDashBoardLineSelector.sendKeys(Keys.ARROW_DOWN);
			suggestionListOfHistoryLineSelectorSearchBar.get(0).click();
			Reporter.log("updated select line");
		} catch (Exception e) {
			Reporter.log("Unable to update select line");
			Assert.fail("Unable to update select line");
		}

		return this;
	}

	/**
	 * 
	 * It will select the value for parent line
	 * 
	 * @param parentLine
	 * @return
	 */
	public FamilyAllowancesPage updateParentLine() {
		checkPageIsReady();
		try {
			clickElement(lineDashBoardDefaultParentLine);
			clickElement(lineDashBoardselectParentValue);
			Reporter.log("updated Parent line");
		} catch (Exception e) {
			Reporter.log("Unable to update parent line");
			Assert.fail("Unable to update parent line");
		}

		return this;
	}

	/**
	 * It will verify whether Schedule accordian is in expandeds state or not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyScheduleAccordianStatusExpanded() {
		checkPageIsReady();
		try {
			Assert.assertTrue(scheduleAccordian.getAttribute("class").contains(accordianExpandStatus));
			Reporter.log("Schedule accordian is in expanded status");

		} catch (Exception e) {
			Reporter.log("Unable to Identify Schedule accordian");
			Assert.fail("Unable to Identify Schedule accordian");
		}

		return this;
	}

	/**
	 * It will return line selector,parent line,phone status and check box
	 * values into a list
	 * 
	 * @return
	 */
	public ArrayList<String> fetchLineDashBoardValues() {
		checkPageIsReady();
		ArrayList<String> listOfLineDashBoardValues = new ArrayList<String>();
		listOfLineDashBoardValues.add(lineDashBoardLineSelector.getAttribute("value"));
		listOfLineDashBoardValues.add(lineDashBoardCurrentPhoneStatus.getText());
		listOfLineDashBoardValues.add(lineDastBoardCurrentCheckBoxStatus.getAttribute("value"));

		return listOfLineDashBoardValues;

	}

	/**
	 * It will edit the values in the dashboard
	 * 
	 * @return
	 */
	public FamilyAllowancesPage editDashBoardValues() {
		checkPageIsReady();
		try {
			updateSelectLine("a");
			lineDashBoardChangePhoneStatus();
			lineDashBoardChangeNotificationCheckBoxStatus();
		} catch (Exception e) {
			Assert.fail("unable to update values");
		}

		return this;
	}

	/**
	 * 
	 * It will validate the list of values before and after clicking save and
	 * discard button
	 * 
	 * @param valuesBefore
	 * @param valuesAfter
	 * @return
	 */
	public FamilyAllowancesPage validateLineDashBoardValuesBeforeAndAfter(ArrayList<String> valuesBefore,
			ArrayList<String> valuesAfter) {

		checkPageIsReady();
		try {
			Assert.assertEquals(valuesBefore.get(0), valuesAfter.get(0));
			Reporter.log("lineSelector is  same before and After save ");
			Assert.assertEquals(valuesBefore.get(1), valuesAfter.get(1));
			Reporter.log("phoneStatus is  same before and After save");
			Assert.assertEquals(valuesBefore.get(2), valuesAfter.get(2));
			Reporter.log("notificationCheckBox is same before and After save");
		} catch (Exception e) {
			Assert.fail("unable to Validate values");
		}

		return this;

	}

	/**
	 * 
	 * It will validate the list of values before and after clicking discard
	 * button
	 * 
	 * @param valuesBefore
	 * @param valuesAfter
	 * @return
	 */
	public FamilyAllowancesPage validateLineDashBoardValuesBeforeAndAfterDiscard(ArrayList<String> valuesAfterEdit,
			ArrayList<String> valuesAfterDiscard) {

		checkPageIsReady();
		try {

			Assert.assertEquals(valuesAfterEdit.get(0), valuesAfterDiscard.get(0));
			Reporter.log("lineSelector is  same after  edit  and After clicking discard");
			Assert.assertEquals(valuesAfterEdit.get(1), valuesAfterDiscard.get(1));
			Reporter.log("phoneStatus is  same before and After");
			Assert.assertNotEquals(valuesAfterEdit.get(2), valuesAfterDiscard.get(2));
			Reporter.log("notificationCheckBox is same before and After");
		} catch (Exception e) {
			Assert.fail("unable to Validate values");
		}

		return this;

	}

	/**
	 * Verify the display of Downloads blade header.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyDownloadsBladeHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(downloadsHeader));
			Assert.assertTrue(isElementDisplayed(downloadsHeader));
			Reporter.log("Downloads blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Downloads blade");
		}
		return this;
	}

	/**
	 * Click on Downloads blade.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clickOnDownloadsBlade() {
		try {
			waitFor(ExpectedConditions.visibilityOf(downloadsToggleButton));
			if (downloadsToggleButton.getAttribute("Class").contains(toggleUpArrow)) {
				Reporter.log("Downloads blade is already expanded");
			} else {
				waitFor(ExpectedConditions.visibilityOf(downloadsHeader));
				elementClick(downloadsHeader);
				Reporter.log("Downloads blade is expanded successfully");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Downloads blade");
		}
		return this;
	}

	/**
	 * Verify the display of Downloads blade description.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyDownloadsBladeDescription() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(downloadsDescription));
			Reporter.log("Content below Downloads blade header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Content below Downloads blade header");
		}
		return this;
	}

	/***
	 * verify toggle down arrow is clickable for Downloads
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDownloadsToggleArrow() {
		try {
			waitFor(ExpectedConditions.visibilityOf(downloadsToggleButton));
			Assert.assertTrue(downloadsToggleButton.isDisplayed() && downloadsToggleButton.isEnabled());
			Reporter.log("Verified Downloads toggle arrow is displayed successfully and is clickable");
		} catch (Exception e) {
			Assert.fail("Unable to see the toggle arrow for Downloads blade");
		}
		return this;
	}

	/***
	 * verify the display of Downloads No allowance text
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDownloadsNoAllowanceCheckBox() {
		try {
			moveToElement(downloadsNoAllowanceCheckboxText);
			Assert.assertTrue(isElementDisplayed(downloadsNoAllowanceCheckboxText),
					"No Allowance text is not displayed");
			Reporter.log("No Allowance check box text is displayed");
			waitFor(ExpectedConditions.visibilityOf(downloadsNoAllowanceCheckbox));
			Assert.assertTrue(isElementDisplayed(downloadsNoAllowanceCheckbox),
					"No Allowance check box is not displayed");
			Reporter.log("No Allowance check box is displayed");
			waitFor(ExpectedConditions.visibilityOf(downloadsNoAllowanceCheckbox));
			Assert.assertTrue(isElementEnabled(downloadsNoAllowanceCheckbox), "No Allowance check box is not enabled");
			Reporter.log("No Allowance check box is enabled and is clickable");
		} catch (Exception e) {
			Assert.fail("No Allowance checkbox is not clickable");
		}
		return this;
	}

	/**
	 * Verify the display of Downloads Tool tip Icon.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyDownloadsNoAllowanceTooltipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(downloadsNoAllowanceTooltipIcon));
			Reporter.log("Tooltip icon is displayed beside No allowance check box");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip icon");
		}
		return this;
	}

	/**
	 * Verify the display of Downloads Tool tip message.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyDownloadsNoAllowanceTooltipMessage() {
		try {
			checkPageIsReady();
			Actions Tooltip = new Actions(getDriver());
			Tooltip.clickAndHold(downloadsNoAllowanceTooltipIcon).perform();
			Assert.assertTrue(isElementDisplayed(downloadsNoAllowanceTooltipMessage));

			Reporter.log(" Tooltip message is displayed successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip message for Downloads");
		}
		return this;
	}

	/**
	 * Verify the display of alert message for Downloads without allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWithoutAllowanceForDownloads() {
		try {
			checkPageIsReady();
			String s1 = downloadsNoAllowanceAlertText.getText();
			String s = downloadsNoAllowanceAlertMessage.getText();
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(s,
					"This line has no allowance set, meaning that this line�s user can download without restrictions.");
			Reporter.log("Successfully verified the Alert message for Downloads without allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the Alert message for Downloads");
		}
		return this;
	}

	/**
	 * Clear current allowance text field for Downloads with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clearCurrentAllowanceTextFieldUnderDownloads() {
		try {
			checkPageIsReady();
			elementClick(downloadsCurrentAllowanceTextField);
			Reporter.log("Clicked on Current allowance text field");
			downloadsCurrentAllowanceTextField.clear();
			downloadsCurrentAllowanceTextField.sendKeys(Keys.ENTER);
			Reporter.log("Cleared Current allowance text field");
		} catch (Exception e) {
			Assert.fail("Failed to clear the Current allowance text field Under Downloads");
		}
		return this;
	}

	/**
	 * Verify the display of alert message for Downloads with allowance.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWithAllowanceForDownloads() {
		try {
			checkPageIsReady();
			String s1 = downloadsAlertTextWithAllowance.getText();
			String text = downloadsAlertMessageWithAllowance.getAttribute("innerHTML");
			Assert.assertEquals(s1, "!Alert :");
			Assert.assertEquals(text, expectedDownloadsAlertMessageWithAllowance);
			Reporter.log(
					"Successfully verified the Alert message '!Alert: This line will not be able to purchase any downloads until you set an allowance above 0.' for Downloads with allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the first Alert message for Downloads");
		}
		return this;
	}

	/**
	 * Verify the display of alert message for Downloads when current allowance
	 * entered is 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWhenCurrentAllowanceIsZeroForDownloads() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer
					.parseInt(downloadsCurrentAllowanceEnteredValue.getAttribute("value").substring(1));
			if (currentAllowanceValue == 0) {
				String s1 = downloadsAlertTextWithAllowance.getText();
				String text = downloadsAlertMessageWithAllowance.getAttribute("innerHTML");
				Assert.assertEquals(s1, "!Alert :");
				Assert.assertEquals(text, expectedDownloadsAlertMessageWithAllowance);
				Reporter.log(
						"Successfully verified the Alert message '!Alert: This line will not be able to purchase any downloads until you set an allowance above 0.' for Downloads with allowance");
			} else {
				Reporter.log("Alert message is not displayed, as entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the first Alert message for Downloads");
		}
		return this;
	}

	/**
	 * Verify the alert messages are not displayed for Downloads when current
	 * allowance entered is greater than 0.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyNoAlertMessagesWhenCurrentAllowanceIsGreaterThanZeroForDownloads() {
		try {
			checkPageIsReady();
			int currentAllowanceValue = Integer
					.parseInt(downloadsCurrentAllowanceEnteredValue.getAttribute("value").substring(1));
			if (currentAllowanceValue > 0) {
				Assert.assertTrue(!isElementDisplayed(downloadsAlertMessageWithAllowance));
				Reporter.log(
						"Successfully verified that the Alert messages are not displayed, when entered current allowance value is greater than 0");
			} else {
				Reporter.log("Alert messages are displayed, when entered current allowance value is greater than 0");
			}
		} catch (Exception e) {
			Assert.fail("Alert messages are displayed, when entered current allowance value is greater than 0");
		}
		return this;
	}

	/**
	 * Check or uncheck No allowance Check box for Downloads
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkOrUncheckDownloadsNoAllowanceCheckbox(String expectedCheckState) {
		try {
			waitforSpinnerinProfilePage();
			String actualCheckedState = checkOrUncheckDownloadsNoAllowanceCheckBox.getAttribute("value").toString();
			if (actualCheckedState.contains(expectedCheckState)) {
				Reporter.log("No allowance check box is already set to " + expectedCheckState);
			} else {
				Reporter.log("No allowance check box is  set to " + actualCheckedState);
				downloadsNoAllowanceCheckbox.click();
				Reporter.log("Clicked on No allowance checkbox");
				Reporter.log("No allowance check box is now set to " + expectedCheckState);
			}
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for No allowance check box");
		}
		return this;
	}

	/**
	 * Verify the display of Current allowance, Used, Remaining fields in
	 * Downloads blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */

	public FamilyAllowancesPage verifyDownloadsFieldLabelsDisplay() {
		try {
			waitFor(ExpectedConditions.visibilityOf(downloadsCurrentAllowanceLabel));
			Assert.assertTrue(isElementDisplayed(downloadsCurrentAllowanceLabel), "Current allowance label is not displayed");
			Reporter.log("Current allowance field label is displayed");
			waitFor(ExpectedConditions.visibilityOf(downloadsUsedLabel));
			Assert.assertTrue(isElementDisplayed(downloadsUsedLabel), "Used label is not displayed");
			Reporter.log("Used field label is displayed");
			waitFor(ExpectedConditions.visibilityOf(downloadsRemainingLabel));
			Assert.assertTrue(isElementDisplayed(downloadsRemainingLabel), "Remaining label is not displayed");
			Reporter.log("Remaining field label is displayed");
			Reporter.log("Field labels are verified successfully");
		} catch (Exception e) {
			Assert.fail("Failed to display the field labels");
		}
		return this;
	}

	/**
	 * Verify the field values Current allowance, Used, Remaining fields in
	 * Downloads blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyDownloadsFieldValuesWithoutAllowance() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(downloadsCurrentAllowanceDefaultValueWithoutAllowance),
					"No allowance default value is not displayed");
			Reporter.log("'No allowance' value is displayed in Current allowance field, when there is no allowance");
			String usedFieldValue = downloadsUsedValue.getText();
			Assert.assertTrue(!usedFieldValue.isEmpty(), "Used field value is null");
			Reporter.log("'" + usedFieldValue + "'"
					+ " value is displayed in Used field as count of dollars used, when there is no allowance");
			Assert.assertTrue(isElementDisplayed(downloadsRemainingDefaultValueWithoutAllowance),
					"No allowance set default value is not displayed");
			Reporter.log("'No allowance set' value is displayed in Remaining field, when there is no allowance");
		} catch (Exception e) {
			Assert.fail("Failed to display the field default values for Downloads without allowance");
		}
		return this;
	}

	/**
	 * Verify field validation error message when current allowance is blank in
	 * Downloads blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceFieldValidationErrorMessageForDownloads() {
		try {
			waitFor(ExpectedConditions.visibilityOf(lineDashBoardCurrentPhoneStatus));
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Actions actions = new Actions(getDriver());
				actions.moveToElement(downloadsCurrentAllowanceTextField).click().perform();
				Reporter.log("Clicked on Downloads Current allowance text field");
				downloadsCurrentAllowanceTextField.clear();
				downloadsCurrentAllowanceTextField.sendKeys(Keys.ENTER);
				Reporter.log("Cleared Downloads Current allowance text field");
				Assert.assertTrue(isElementDisplayed(downloadsFieldValidationErrorMessage));
				Reporter.log("'Please enter limit' field validation error message is displayed successfully");
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				elementClick(lineDashBoardPhoneStatusToggle);
				Actions actions = new Actions(getDriver());
				actions.moveToElement(downloadsCurrentAllowanceTextField).click().perform();
				Reporter.log("Clicked on Downloads Current allowance text field");
				downloadsCurrentAllowanceTextField.clear();
				downloadsCurrentAllowanceTextField.sendKeys(Keys.ENTER);
				Reporter.log("Cleared Downloads Current allowance text field");
				Assert.assertTrue(isElementDisplayed(downloadsFieldValidationErrorMessage));
				Reporter.log("'Please enter limit' field validation error message is displayed successfully");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display the validation error message for current allowance field");
		}
		return this;
	}

	/**
	 * Verify default Remaining field value when current allowance is blank in
	 * Downloads blade
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyDownloadsRemainingFieldDefaultValueWhenCurrentAllowanceIsBlank() {
		try {
			checkPageIsReady();
			String remainingValue = downloadsRemainingValueWithAllowance.getText();
			Assert.assertEquals(remainingValue, "$0");
			Reporter.log("'" + remainingValue + "'"
					+ " default value is set for Remaining field, when current allowance is blank");
		} catch (Exception e) {
			Assert.fail("Failed to display the default value for Remaining field, when current allowance is blank");
		}
		return this;
	}

	/**
	 * Verify the length of current allowance field
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinAndMaxLengthOfDownloadsCurrentAllowance() {
		checkPageIsReady();
		try {
			int max_length = Integer.parseInt(downloadsCurrentAllowanceTextField.getAttribute("maxlength")) - 1;
			Assert.assertEquals(max_length, 4);
			Reporter.log(
					"Verified, Maximum length allowed in the Current allowance field is " + max_length + " digits");
			int min_length = Integer.parseInt(downloadsCurrentAllowanceTextField.getAttribute("minlength"));
			Assert.assertEquals(min_length, 1);
			Reporter.log("Verified, Minimum length allowed in the Current allowance field is " + min_length + " digit");
		} catch (Exception e) {
			Assert.fail("Failed to verify the minimum and maximum length of Current allowance field");
		}
		return this;
	}

	/**
	 * Enter the data into current allowance
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage enterDataIntoDownloadsCurrentAllowance(String currentAllowanceValue) {
		checkPageIsReady();
		try {
			sendTextData(downloadsCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + currentAllowanceValue + " into the text box");
		}
		return this;
	}

	/**
	 * Verify Calculated Remaining value for Downloads
	 *
	 * @return the FamilyAllowances page class instance.
	 */

	public FamilyAllowancesPage verifyCalculatedRemainingValueForDownloads() {
		checkPageIsReady();
		try {
			int currentAllowanceValue = Integer
					.parseInt(downloadsCurrentAllowanceEnteredValue.getAttribute("value").substring(1));
			int usedFieldValue = Integer.parseInt(downloadsUsedValue.getText().substring(1));
			int actualRemainingValue = Integer.parseInt(downloadsRemainingValueWithAllowance.getText().substring(1));
			int expectedRemainingValue = currentAllowanceValue - usedFieldValue;
			Reporter.log("Calculated Remaining value is: " + actualRemainingValue);
			Assert.assertEquals(actualRemainingValue, expectedRemainingValue);
			Reporter.log("Remaining value has been successfully verified ");
		} catch (Exception e) {
			Assert.fail("Failed to calculate and display the Remaining value for Downloads with allowance applied");
		}
		return this;
	}

	/**
	 * verify the display of saved allowance in Current allowance field under
	 * Downloads blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifySavedCurrentAllowanceDisplayedUnderDownloads() {
		try {
			checkPageIsReady();
			int currentAllowanceBeforeSave = Integer
					.parseInt(downloadsCurrentAllowanceEnteredValue.getAttribute("value").substring(1));
			int usedFieldValueBeforeSave = Integer.parseInt(downloadsUsedValue.getText().substring(1));
			int expectedRemainingValueBeforeSave = currentAllowanceBeforeSave - usedFieldValueBeforeSave;
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickOnDownloadsBlade();
			int currentAllowanceAfterSave = Integer
					.parseInt(downloadsCurrentAllowanceEnteredValue.getAttribute("value").substring(1));
			int remainingValueAfterSave = Integer.parseInt(downloadsRemainingValueWithAllowance.getText().substring(1));
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
				Reporter.log("current allowance is same before and after when  phone status is in 'ON' status");
				Assert.assertEquals(expectedRemainingValueBeforeSave, remainingValueAfterSave);
				Reporter.log("remaining is same before and after when  phone status is in 'ON' status");
				verifyCalculatedRemainingValueForDownloads();
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
				Reporter.log("current allowance is same before and after when  phone status is in 'OFF' status");
				Assert.assertEquals(0, remainingValueAfterSave);
				Reporter.log("remaining value is zero as phone status is in 'OFF' status");
			} else {
				Assert.fail("Unable to validate values before and after save");
			}
		} catch (Exception e) {
			Assert.fail("Unable to validate allowance and remaining value before and after save");
		}
		return this;
	}

	/**
	 * verify No allowance in Current allowance field after saving with No
	 * allowance under Downloads blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithSaveNoAllowanceUnderDownloads() {
		try {
			checkPageIsReady();
			String currentAllowanceBeforeSave = downloadsCurrentAllowanceDefaultValueWithoutAllowance.getText();
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickOnDownloadsBlade();
			String currentAllowanceAfterSave = downloadsCurrentAllowanceDefaultValueWithoutAllowance.getText();
			Assert.assertEquals(currentAllowanceBeforeSave, currentAllowanceAfterSave);
			Reporter.log("'No allowance' is displayed in Current allowance field after Saving with without allowance");
		} catch (Exception e) {
			Assert.fail("Unable to see the No allowance value in Current allowance field");
		}
		return this;
	}

	/**
	 * verify the Discard Changes does not apply Current allowance field under
	 * Downloads blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithDiscardChangesForDownloads(String currentAllowanceValue) {
		try {
			checkPageIsReady();
			String currentAllowanceExistingValue = downloadsCurrentAllowanceTextField.getAttribute("value");
			downloadsCurrentAllowanceTextField.clear();
			Reporter.log("Cleared the Current allowance text field under Downloads");
			sendTextData(downloadsCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
			verifyAndClickOnDiscardChangesButton();
			clickOnDownloadsBlade();
			String actualCheckedState = checkOrUncheckDownloadsNoAllowanceCheckBox.getAttribute("value").toString();
			if (actualCheckedState.contains("false")) {
				String currentAllowanceValueAfterDiscardChanges = downloadsCurrentAllowanceTextField
						.getAttribute("value");
				Assert.assertEquals(currentAllowanceExistingValue, currentAllowanceValueAfterDiscardChanges);
				Reporter.log(
						"Current allowance field is not updated with the entered value after Discard changes, thus verified Discard changes functionality");
			} else {
				Assert.assertTrue(actualCheckedState.contains("true"), "No allowance check box is already set to true");
				Reporter.log("No allowance check box is set to true as before");
			}
		} catch (Exception e) {
			Assert.fail("Current allowance field is updated with the entered value after Discard changes");
		}
		return this;
	}

	/**
	 * verify the allowance is refreshed and overridden with existing value upon
	 * check and uncheck, under Downloads blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyCurrentAllowanceWithCheckAndUncheckNoAllowanceUnderDownloads(
			String currentAllowanceValue) {
		try {
			checkPageIsReady();
			String currentAllowanceExistingValue = downloadsCurrentAllowanceTextField.getAttribute("value");
			sendTextData(downloadsCurrentAllowanceTextField, currentAllowanceValue);
			Reporter.log("Entered value " + currentAllowanceValue + " in the Current allowance field");
			elementClick(downloadsNoAllowanceCheckbox);
			Reporter.log("Checked No allowance check box");
			elementClick(downloadsNoAllowanceCheckbox);
			Reporter.log("Unchecked again No allowance check box");
			String currentAllowanceAfterUncheckAndCheckNoAllowance = downloadsCurrentAllowanceTextField
					.getAttribute("value");
			Assert.assertEquals(currentAllowanceExistingValue, currentAllowanceAfterUncheckAndCheckNoAllowance);
			Reporter.log(
					"Allowance value is refreshed and the previously saved value is displayed, when we uncheck and check No allowance check box");
		} catch (Exception e) {
			Assert.fail(
					"Able to see the entered allowance in Current allowance field when we uncheck and check No allowance check box");
		}
		return this;
	}

	/**
	 * 
	 * This method will validate if the date format is valid or not ex: JAN 16TH
	 * 
	 * @param billingCycleDateRange
	 * @return
	 */
	public Boolean validateBillingCycleDateFormate(String billingCycleDateRange) {
		return billingCycleDateRange.matches("[A-Z]{3} [0-9]{2}[A-Z]{2}");
	}

	/**
	 * 
	 * This method will validate if the date format is valid or not ex: JAN 16TH
	 * 
	 * @param billingCycleDateRange
	 * @return
	 */
	public Boolean validateHistoryDateTimeFormat(String historyDateTimeRange) {
		return historyDateTimeRange.matches("[A-Z]{3} [0-9]{2}[A-Z]{2}");
	}

	/**
	 * 
	 * It will click Family Control Blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickFamilyControlBlade() {
		checkPageIsReady();
		try {
			clickElement(familyControlsBlade);
			Reporter.log("clicked familyControlsBlade");
		} catch (Exception e) {
			Assert.fail("Unable to click familyControlsBlade");
		}
		return this;
	}

	/**
	 * verify the MSDIN name selected in the Family controls page with the page
	 * load value in Family allowances page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage validateMISDNNameInControlsAndAllowancesPage(String expectedLine) {
		checkPageIsReady();
		try {
			String lineName = lineDashBoardLineHolderName.getText();
			if (lineName.contains(expectedLine)) {
				Reporter.log("MISDN name selected in Family Controls page is displayed in Family Allowances dashboard");
			} else {
				Reporter.log(
						"MISDN name selected in Family Controls page is not displayed in Family Allowances dashboard");
			}
		} catch (Exception e) {
			Assert.fail("MISDN name selected in Family Controls page is not displayed in Family Allowances dashboard");
		}
		return this;
	}

	/***
	 * Verify and click on View history of all lines in Change history
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndClickViewHistoryOfAllLines() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(viewHistoryOfAllLines));
			clickElement(viewHistoryOfAllLines);
            waitforSpinnerinProfilePage();
			Assert.assertTrue(isElementDisplayed(historyOfLinesPage));
			Reporter.log("navigated to  history of all lines page successfully");
		} catch (Exception e) {
			Reporter.log("Unable to click View history of all lines");
			Assert.fail("Unable to click View history of all lines");
		}
		return this;
	}

	/**
	 * Verify FamilyAllowances history of all lines Page.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyHistoryOfAllLinesPage() {
		try {
			checkPageIsReady();
			verifyCurrentPageURL("History of all lines", historyOfAllLinesPageUrl);
			Reporter.log("History of all lines page is displayed");
		} catch (Exception e) {
			Assert.fail("History of all lines page is not displayed");
		}
		return this;
	}

	/**
	 * Verify the header of history page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyHistoryOfAllLinesHeader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(historyOfAllLinesHeader));
			Reporter.log("History of all lines header display is verified");
		} catch (Exception e) {
			Assert.fail("Unable to display the history of all line header");
		}
		return this;
	}

	/**
	 * Validate the line selector name in history of lines page header with the
	 * line holder name in allowance page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage validateHistoryHeaderNameAndDashboardLineSelectorName(
			String dashboardlineSelectorName) {
		checkPageIsReady();
		try {

			String[] expectedLineSelectorName = dashboardlineSelectorName.split(",");
			String headingLineName = historyOfAllLinesHeader.getText();
			if (headingLineName.contains(expectedLineSelectorName[0])) {
				Reporter.log("Line holder name is same in allowances line selector and history of lines header");
			} else {
				Reporter.log("Line holder name is not same in allowances line selector and history of lines header");
				Assert.fail("Line holder name is not same in allowances line selector and history of lines header");
			}
		} catch (Exception e) {
			Reporter.log("Line holder name is not same in allowances line selector and history of lines header");
			Assert.fail("Line holder name is not same in allowances line selector and header");
		}
		return this;
	}

	/**
	 * Verify the display of History of all lines description.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyHistoryOfAllLinesDescription() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(historyOfAllLinesDescription));
			Reporter.log("Content below History of all lines header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the History of all lines description");
		}
		return this;
	}

	/**
	 * Verify and Click the Help link
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyHistoryOfLinesHelp() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(historyOfLinesHelp));
			Reporter.log("'Help' text is Displayed");
			clickElement(historyOfLinesHelp);
			Reporter.log("Clicked on Help Link");
			checkPageIsReady();
			ArrayList<String> familyAllowanceWindows = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(familyAllowanceWindows.get(1));
			verifyCurrentPageURL("Help Tab", helpTab);
			Reporter.log("'Help' tab URL  is validated");
			getDriver().close();
			getDriver().switchTo().window(familyAllowanceWindows.get(0));
		} catch (Exception e) {
			Assert.fail("'Help' text is not Displayed");
		}
		return this;
	}

	/***
	 * verifying the value displayed in the search field of history of lines
	 * page with the line holder name in allowance page
	 * 
	 * @param expectedLine
	 * @return
	 */
	public FamilyAllowancesPage validateDefaultLineValueForHistoryLineSelectorSearchTextBar(
			String dashboardlineSelectorName) {
		try {
			checkPageIsReady();
			String[] expectedLineSelectorName = dashboardlineSelectorName.split(",");
			String actualLineSelectorName = historyLineSelector.getAttribute("value");
			String[] historySearchLineSelector = actualLineSelectorName.split(",");
			if (historySearchLineSelector[0].contains(expectedLineSelectorName[0])) {
				Reporter.log(
						"Line holder name is same in search text in history of lines and allowances line selector");
			} else {
				Reporter.log(
						"Line holder name is not same in search text in history of lines and allowances line selector");
				Assert.fail(
						"Line holder name is not same in search text in history of lines and allowances line selector");
			}
		} catch (Exception e) {
			Assert.fail("failed to verify the default line populated in the search text of history line selector"
					+ e.toString());
		}
		return this;
	}

	/**
	 * Verify the Billing cycle Text below billing cycle date field in history
	 * of lines page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyBillingCycleLabelDisplay() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(historyOfLinesBillingCycleLabel));
			Reporter.log("BillingCycle label is verified in history of lines page");
		} catch (Exception e) {
			Assert.fail("BillingCycle label is not verified in history of lines page");
		}
		return this;
	}

	/**
	 * Verify the default billing cycle format display
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyBillingCycleFormat() {
		checkPageIsReady();
		try {
			Assert.assertNotNull(historyOfLinesBillingcycle.getText());
			String[] billingCycle = historyOfLinesBillingcycle.getText().split("-");
			Assert.assertTrue(validateBillingCycleDateFormate(billingCycle[0].trim()));
			Assert.assertTrue(validateBillingCycleDateFormate(billingCycle[1].trim()));
			Reporter.log("Default billing cycle date is validated");
		} catch (Exception e) {
			Reporter.log("Unable to validate default billing cycle");
			Assert.fail("Unable to validate default billing cycle");
		}
		return this;
	}

	/***
	 * verifying the value displayed in the history of lines page with the value
	 * in change history page
	 * 
	 * @param expectedLine
	 * @return
	 */
	public FamilyAllowancesPage validateDefaultBillingCycleDateFromChangeHistory(String changeHistoryBillingCycleDate) {
		try {
			checkPageIsReady();
			String historyOfLinesBillingcycleDate = historyOfLinesBillingcycle.getText();
			if (historyOfLinesBillingcycleDate.contains(changeHistoryBillingCycleDate)) {
				Reporter.log("Billing cycle is same in change history and history of all lines page");
			} else {
				Reporter.log("Billing cycle is not same in change history and history of all lines page");
				Assert.fail("Billing cycle is not same in change history and history of all lines page");
			}
		} catch (Exception e) {
			Assert.fail("failed to verify the default billing cycle populated on the history of all lines page"
					+ e.toString());
		}
		return this;
	}

	/**
	 * It will verify recent 3 months bill cycle in history of lines page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyHistoryOfAllLinesThreeMonthsBillcycle() {
		checkPageIsReady();
		try {
			if (historyOfLinesPastBillingCycle.size() == 3) {
				Reporter.log("Past 3 months bill cycle is displayed");
			} else {
				Reporter.log("Past 3 months bill cycle is not displayed");
				Assert.fail("Past 3 months bill cycle is not displayed");
			}
		} catch (Exception e) {
			Reporter.log("Past 3 months bill cycle is not displayed");
			Assert.fail("Past 3 months bill cycle is not displayed");
		}
		return this;
	}

	/***
	 * verifying the placeholder value displayed in history line selector
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyPlaceholderValueInHistoryLineSelectorSearchTextBar() {
		try {
			checkPageIsReady();
			Assert.assertEquals("Find a line", historyLineSelectorSearchTextBar.getAttribute("placeholder").trim());
			Reporter.log("Placeholder value is displayed in history line selector search text field");
		} catch (Exception e) {
			Assert.fail("failed to display the placeholder value in the history line selector search text field");
		}
		return this;
	}

	/***
	 * verifying the value of history line selector is null
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNoTextInHistoryLineSelectorSearchTextBar() {
		try {
			checkPageIsReady();
			Assert.assertEquals("", historyLineSelectorSearchTextBar.getAttribute("innerHTML").trim());
			Reporter.log("The line selector search value is cleared");
		} catch (Exception e) {
			Assert.fail(
					"failed to verify the value in the line selector field upon clicking clear icon" + e.toString());
		}
		return this;
	}

	/***
	 * verifying the presence of history line selector search text field
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifySearchIconOnHistoryLineSelectorSearchTextBar() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(searchIconOnHistoryLineSelectorSearchTextBar),
					"Search icon for line selector field is not displayed");
			Reporter.log("Search icon for line selector field is displayed in history page");
		} catch (Exception e) {
			Assert.fail("Search icon for line selector field is not displayed in history page" + e.toString());
		}
		return this;
	}

	/***
	 * enter text in the history line selector text field
	 * 
	 * @param value
	 * @return
	 */
	public FamilyAllowancesPage setTextForHistoryLineSelectorSearchTextBar(String value) {
		try {
			checkPageIsReady();
			sendTextData(historyLineSelectorSearchTextBar, value);
			Reporter.log("The line selector field is entered with the value " + value);
		} catch (Exception e) {
			Assert.fail("failed to set text in the line selector field" + e.toString());
		}
		return this;
	}

	/**
	 * It will retrieve the entered value from line selector text box
	 * 
	 * @return
	 */
	public String retrieveAllowancesLineSelectorValue() {
		return lineDashBoardLineSelector.getAttribute("value");
	}

	/**
	 * It will retrieve the Billing cycle date from Change history
	 * 
	 * @return
	 */
	public String retrieveChangeHistoryBillingcycleDate() {
		return changeHistoryBillingcycle.getAttribute("innerHTML");
	}

	/***
	 * verifying the presence of clear icon for history line selector field
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyClearIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(historyLineSelectorClearIon));
			Reporter.log("Clear search icon is displayed for the line selector search text field in history page");
		} catch (Exception e) {
			Assert.fail("Clear icon for line selector field is not displayed in history page" + e.toString());
		}
		return this;
	}

	/***
	 * click on the clear icon for the history line selector field
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickClearIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(historyLineSelectorClearIon));
			Reporter.log("Clicked on the Clear icon");
		} catch (Exception e) {
			Assert.fail("Clear icon for line selector field is not displayed" + e.toString());
		}
		return this;
	}

	/**
	 * It will verify List of all lines in History of all lines page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyListOfAllLines() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(viewListOfAllLines));
			Reporter.log("View list of all lines link displayed successfully in history of all lines page");
		} catch (Exception e) {
			Reporter.log("Failed to display view list of all lines link in history of all lines page");
			Assert.fail("Failed to display view list of all lines link in history of all lines page");
		}
		return this;
	}

	/**
	 * It will click on List of all lines in History of all lines page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickListOfAllLines() {
		checkPageIsReady();
		try {
			clickElement(viewListOfAllLines);
			Reporter.log("Clicked View list of all lines");
			checkPageIsReady();
			getDriver().navigate().back();
		} catch (Exception e) {
			Reporter.log("Unable to click View list of all lines");
			Assert.fail("Unable to click View list of all lines");
		}
		return this;
	}

	/***
	 * verify the auto complete functionality for history line selector search
	 * field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public FamilyAllowancesPage verifyAutoCompleteSearchResultsForHistoryLineSelectorField(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			for (WebElement ele : suggestionListOfHistoryLineSelectorSearchBar) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					actListOfValuesThatMatchedTheSearchValue++;
				}
			}
			Assert.assertEquals(suggestionListOfHistoryLineSelectorSearchBar.size(),
					actListOfValuesThatMatchedTheSearchValue);
			Reporter.log("Autocomplete search functionality works for line selector search field");
		} catch (Exception e) {
			Assert.fail("failed to verify the Autocomplete search functionality for line selector search field"
					+ e.toString());
		}
		return this;
	}

	/***
	 * verify the tab functionality for the history line selector search results
	 * 
	 * @param expLineDetails
	 * @return
	 */
	public FamilyAllowancesPage verifyTabOutFunctionalityForLineSelectorField(String expLineDetails) {
		try {
			checkPageIsReady();
			historyLineSelectorSearchTextBar.sendKeys(Keys.TAB);
			String actLineDetails = historyLineSelectorSearchTextBar.getAttribute("value").trim();
			Assert.assertEquals(expLineDetails, actLineDetails);
			Reporter.log("The line is reset to the last selected value upon tab out");
		} catch (Exception e) {
			Assert.fail("failed to verify the last selected value upon tab out" + e.toString());
		}
		return this;
	}

	/***
	 * verify History line selector search results for no matching search
	 * criteria
	 * 
	 * @param expectedValue
	 * @return
	 */
	public FamilyAllowancesPage verifyAutoCompleteSearchResultsForNoMathingSearchCriteria(String expectedValue) {
		int listOfValuesThatMatchedTheHistorySearchValue = 0;
		try {
			checkPageIsReady();
			for (WebElement ele : suggestionListOfHistoryLineSelectorSearchBar) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					listOfValuesThatMatchedTheHistorySearchValue++;
				}
			}
			Assert.assertEquals(suggestionListOfHistoryLineSelectorSearchBar.size(),
					listOfValuesThatMatchedTheHistorySearchValue);
			Reporter.log("History line selector does not return the list of lines for invalid search");
		} catch (Exception e) {
			Assert.fail(
					"failed to verify the Autocomplete search functionality for invalid line selector in History of lines"
							+ e.toString());
		}
		return this;
	}

	/***
	 * verify the columns display for showing the history of a line
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyHistoryColumnsDisplay() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(dateTimeLabel));
			Reporter.log("Date/Time Column label is displayed successfully");
			Assert.assertTrue(isElementDisplayed(whatWasChangedLabel));
			Reporter.log("What Was Changed: Column label is displayed successfully");
			Reporter.log("Columns showing the history are displayed successfully");
		} catch (Exception e) {
			Reporter.log("Unable to display the columns showing the history of a line");
			Assert.fail("Unable to display the columns showing the history of a line");
		}
		return this;
	}

	/**
	 * It will enter the value in Find a line text box
	 * 
	 * @param FindALine
	 * @return
	 */
	public FamilyAllowancesPage updateHistoryFindALine(String selectLine) {
		checkPageIsReady();
		try {
			historyLineSelector.click();
			sendTextData(historyLineSelector, selectLine);
			historyLineSelector.sendKeys(Keys.ARROW_DOWN);
			Reporter.log("Updated Find a line");
		} catch (Exception e) {
			Reporter.log("Unable to update Find a line");
			Assert.fail("Unable to update Find a line");
		}
		return this;
	}

	/*
	 * It will update the value in a Billing cycle drop down
	 * 
	 * @return
	 */
	public FamilyAllowancesPage updateHistoryBillingCycle() {
		checkPageIsReady();
		try {

			clickElement(historyOfLinesBillingcycle);
			Reporter.log("Clicked on Billing cycle");
			historyOfLinesPastBillingCycle.get(1).click();
			Reporter.log("Updated Billing Cycle by selecting second billing cycle from the drop down");
		} catch (Exception e) {
			Reporter.log("Unable to update Billing Cycle");
			Assert.fail("Unable to update Billing Cycle");
		}
		return this;
	}

	/*
	 * It will validate the data in Date/Time column under History displayed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage validateDataInDateTimeColumn() {
		checkPageIsReady();
		try {
			int dataInDateTimeColumn = 0;
			for (WebElement x : dateTimeValue) {
				if (!x.getText().isEmpty()) {
					dataInDateTimeColumn++;
				} else {
					Reporter.log("No data for the selected line and billing cycle date");
				}
			}
			Assert.assertEquals(dateTimeValue.size(), dataInDateTimeColumn);
			Reporter.log("Validated data successfully in Date/Time column");
		} catch (Exception e) {
			Reporter.log("Unable to validate data in Date/Time column");
			Assert.fail("Unable to validate data in Date/Time column");
		}
		return this;
	}

	/*
	 * It will validate the data in What was changed column under History
	 * displayed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage validateDataInWhatWasChangedColumn() {
		checkPageIsReady();
		try {
			int dataInWhatWasChangedColumn = 0;
			for (WebElement x : whatWasChangedValue) {
				if (!x.getText().isEmpty()) {
					dataInWhatWasChangedColumn++;
				}
			}
			Assert.assertEquals(whatWasChangedValue.size(), dataInWhatWasChangedColumn);
			Reporter.log("Validated data successfully in What was changed column");
		} catch (Exception e) {
			Reporter.log("Unable to validate data in What was changed column");
			Assert.fail("Unable to validate data in What was changed column");
		}
		return this;
	}

	/**
	 * Check or uncheck No allowance Check box for Schedule
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkOrUncheckScheduleMontoFriFieldForDiscard() {
		checkPageIsReady();
		try {
			changeScheduleFirstCheckboxStatus();
			String checkedStateBeforeDiscard = scheduleCheckBoxStatus.getAttribute("value").toString();
			elementClick(discardChangesButton);
			clickScheduleBlade();
			String stateAfterDiscard = scheduleCheckBoxStatus.getAttribute("value").toString();
			Assert.assertNotEquals(checkedStateBeforeDiscard, stateAfterDiscard);
			Reporter.log("discard is working as expected");
		} catch (Exception e) {
			Assert.fail("Unable to discard for discard");
		}
		return this;
	}

	/**
	 * Check or uncheck No allowance Check box for Schedule
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkOrUncheckScheduleMontoFriFieldForSave() {
		checkPageIsReady();
		try {
			changeScheduleFirstCheckboxStatus();
			String checkedStateBeforeSave = scheduleCheckBoxStatus.getAttribute("value").toString();
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickScheduleBlade();
			String stateAfterSave = scheduleCheckBoxStatus.getAttribute("value").toString();
			Assert.assertEquals(checkedStateBeforeSave, stateAfterSave);
			Reporter.log("save is working as expected");
		} catch (Exception e) {
			Assert.fail("Unable to check save");
		}
		return this;
	}

	/**
	 * change the schedule status to checked if it is unchecked and unchecked if
	 * it is in checked status
	 * 
	 * @param expectedPhoneStatus
	 * @return
	 */
	public FamilyAllowancesPage changeScheduleFirstCheckboxStatus() {
		checkPageIsReady();
		try {
			String actualCheckBoxStatus = scheduleCheckBoxStatus.getAttribute("value");
			if (actualCheckBoxStatus.equals("true")) {
				elementClick(scheduleCheckbox);
				Reporter.log("schedule first  check box is changed to unchecked status");
			} else {
				elementClick(scheduleCheckbox);
				Reporter.log("schedule first  check box is changed to checked status");
			}
		} catch (Exception e) {
			Assert.fail("Unable to set schedule  check box state to check or uncheck status");
		}
		return this;
	}

	/**
	 * Check or uncheck No allowance Check box for Minutes
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkOrUncheckScheduleFirstCheckbox(String expectedCheckState) {
		checkPageIsReady();
		try {
			String actualCheckedState = scheduleFieldMontoFri.getAttribute("value").toString();
			if (actualCheckedState.contains(expectedCheckState)) {
				Reporter.log("Schedule check box is already set to " + expectedCheckState);
			} else {
				Reporter.log("Schedule check box is  set to " + actualCheckedState);
				elementClick(scheduleCheckbox);
				Reporter.log("Clicked on Schedule checkbox");
				Reporter.log("Schedule check box is now set to " + expectedCheckState);
			}
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for schedule check box");
		}
		return this;
	}

	/**
	 * Check If uncheck No allowance Check box for Schedule and vice versa
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkIfUncheckScheduleFirstCheckboxAndViceversa() {
		checkPageIsReady();
		try {
			String actualCheckedState = scheduleFieldMontoFri.getAttribute("value").toString();
			if (actualCheckedState.contains("True")) {
				Reporter.log("Schedule check box is  set to " + actualCheckedState);
				elementClick(scheduleCheckbox);
				Reporter.log("Clicked on Schedule checkbox");
				Reporter.log("Schedule check box is now set to False");
			} else {
				Reporter.log("Schedule check box is  set to " + actualCheckedState);
				elementClick(scheduleCheckbox);
				Reporter.log("Clicked on Schedule checkbox");
				Reporter.log("Schedule check box is now set to True");
			}
		} catch (Exception e) {
			Assert.fail("Unable to set expected state for schedule check box");
		}
		return this;
	}

	/**
	 * Check or uncheck No allowance Check box for Schedule
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage checkOrUncheckScheduleMontoFriFieldForSave(String expectedCheckState) {
		checkPageIsReady();
		try {
			checkOrUncheckScheduleFirstCheckbox(expectedCheckState);
			String checkedStateBeforeSave = scheduleFieldMontoFri.getAttribute("value").toString();
			verifyAndClickOnSaveButton();
			checkPageIsReady();
			clickScheduleBlade();
			String stateAfterSave = scheduleFieldMontoFri.getAttribute("value").toString();
			Assert.assertEquals(checkedStateBeforeSave, stateAfterSave, "not same before after save functionality");
			Reporter.log("check box status is same after and before discard changes");

		} catch (Exception e) {
			Assert.fail("Unable to set expected state for Schedule check box");
		}
		return this;
	}

	/***
	 * verifying the auto complete functionality for line selector field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public FamilyAllowancesPage verifyAutoCompleteSearchResultsForListOfLinesField(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			for (WebElement ele : suggestionListOfLines) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					actListOfValuesThatMatchedTheSearchValue++;
				}
			}
			Assert.assertEquals(suggestionListOfLines.size(), actListOfValuesThatMatchedTheSearchValue);
			Reporter.log("Autocomplete search functionality works for line selector search field");
		} catch (Exception e) {
			Assert.fail("failed to verify the Autocomplete search functionality for line selector search field"
					+ e.toString());
		}
		return this;
	}

	/**
	 * this method will check if the search results are refined to the
	 * particular input
	 * 
	 * @param expectedLine
	 * @return
	 */
	public FamilyAllowancesPage validateListOfLinesFromLineDashBoard(String expectedLine) {
		checkPageIsReady();
		try {
			enterDataInListOfLines(expectedLine);
			checkPageIsReady();
			verifyElementByText(listOfLinesMssdn, expectedLine);
			Reporter.log("selected line is validated successfully");
			clickElement(listOfLinesMssdn);
			checkPageIsReady();
			Assert.assertTrue(lineDashBoardLineHolderName.getText().contains(expectedLine));
			Assert.assertTrue(getDriver().getCurrentUrl().contains(lineDashBoardPage));
			getDriver().navigate().back();
		} catch (Exception e) {
			Assert.fail("selected line is not  validated successfully");
		}
		return this;
	}

	/**
	 * it will enter data into list of lines
	 * 
	 * @param expectedLine
	 */
	private void enterDataInListOfLines(String expectedLine) {
		sendTextData(listOfLinesTextBox, expectedLine);
		listOfLinesTextBox.sendKeys(Keys.ARROW_DOWN);
		listOfLinesTextBox.sendKeys(Keys.ENTER);
		listOfLinesTextBox.sendKeys(Keys.TAB);
	}

	/**
	 * 
	 * it will validate list of line from family controls page
	 * 
	 * @param expectedLine
	 * @return
	 */
	public FamilyAllowancesPage validateListOfLinesFromFamilyControls(String expectedLine) {
		checkPageIsReady();
		try {
			enterDataInListOfLines(expectedLine);
			verifyElementByText(listOfLinesMssdn, expectedLine);
			Reporter.log("selected line is validated successfully");
			clickElement(listOfLinesMssdn);
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains(familyControlsPage));
			getDriver().navigate().back();
		} catch (Exception e) {
			Assert.fail("selected line is not  validated successfully");
		}
		return this;
	}

	/**
	 * Verify the add new number field for always allowed field is readable or
	 * not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAllowanceNumbersAlwaysAllowedFieldIsReadable() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!addNewNumberForAlwaysAllowed.isEnabled(), "always allow field is not readable");
			Reporter.log("add new number field is Readable as expected");
		} catch (Exception e) {
			Assert.fail("add new number Field is not validated");
		}
		return this;
	}

	/**
	 * It will verify if the add button for always allowed field is disabled or
	 * not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddButtonForAlwaysAllowedIsDisabled() {
		checkPageIsReady();
		try {
			Assert.assertTrue(addButtonForAlwaysAllowed.isDisplayed());
			Assert.assertTrue(!addButtonForAlwaysAllowed.isEnabled());
			Reporter.log("add button is displayed and disabled for always allowed field");
			clickElement(addButtonForAlwaysAllowed);
			Reporter.log("add button for always allowed field is not clickable");
		} catch (Exception e) {
			Assert.fail("Unable to validate addButtonForAlwaysAllowed");
		}
		return this;
	}

	/**
	 * Verify the add new number field for never allowed field is readable or
	 * not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAllowanceNumbersNeverAllowedFieldIsReadable() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!addNewNumberForNeverAllowed.isEnabled(), "never allow field is not readable");
			Reporter.log("add new number field is Readable as expected");
		} catch (Exception e) {
			Assert.fail("add new number Field is not validated");
		}
		return this;
	}

	/**
	 * It will verify if the add button for always allowed field is disabled or
	 * not
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddButtonForNeverAllowedIsDisabled() {
		checkPageIsReady();
		try {
			Assert.assertTrue(addButtonForNeverAllowed.isDisplayed());
			Assert.assertTrue(!addButtonForNeverAllowed.isEnabled());
			Reporter.log("add button is displayed and disabled for never allowed field");
			clickElement(addButtonForAlwaysAllowed);
			Reporter.log("add button for never allowed field is not clickable");
		} catch (Exception e) {
			Assert.fail("Unable to validate addButtonForneverAllowed");
		}
		return this;
	}

	/**
	 * It will edit the values in the dashboard
	 * 
	 * @return
	 */
	public FamilyAllowancesPage editSingleLineDashBoardValues() {
		checkPageIsReady();
		try {
			lineDashBoardChangePhoneStatus();
			lineDashBoardChangeNotificationCheckBoxStatus();
		} catch (Exception e) {
			Assert.fail("unable to update values");
		}
		return this;
	}

	/**
	 * change the notification status to checked if it is unchecked and
	 * unchecked if it is in checked status
	 * 
	 * @param expectedPhoneStatus
	 * @return
	 */
	public FamilyAllowancesPage lineDashBoardChangeNotificationCheckBoxStatus() {
		checkPageIsReady();
		try {
			String actualCheckBoxStatus = lineDastBoardCurrentCheckBoxStatus.getAttribute("value");
			if (actualCheckBoxStatus.equals("true")) {
				elementClick(lineDashBoardNotificationCheckBox);
				waitForSpinnerInvisibility();
				Reporter.log("Notification check box is changed to unchecked status");
			} else {
				elementClick(lineDashBoardNotificationCheckBox);
				waitForSpinnerInvisibility();
				Reporter.log("Notification check box is changed to checked status");
			}
		} catch (Exception e) {
			Assert.fail("Unable to set Notification  check box state to check or uncheck status");
		}
		return this;
	}

	/**
	 * It will return line selector,parent line,phone status and check box
	 * values into a list
	 * 
	 * @return
	 */
	public ArrayList<String> fetchSingleLineDashBoardValues() {
		checkPageIsReady();
		ArrayList<String> listOfLineDashBoardValues = new ArrayList<String>();
		listOfLineDashBoardValues.add(lineDashBoardCurrentPhoneStatus.getText());
		listOfLineDashBoardValues.add(lineDastBoardCurrentCheckBoxStatus.getAttribute("value"));
		return listOfLineDashBoardValues;
	}

	/**
	 * 
	 * It will validate the list of values before and after clicking discard
	 * button
	 * 
	 * @param valuesBefore
	 * @param valuesAfter
	 * @return
	 */
	public FamilyAllowancesPage validateSingleLineDashBoardValuesBeforeAndAfterDiscard(ArrayList<String> valuesBefore,
			ArrayList<String> valuesAfter) {
		checkPageIsReady();
		try {
			Assert.assertEquals(valuesBefore.get(0), valuesAfter.get(0));
			Reporter.log("Phone Status is   same  before and After discard ");
			Assert.assertEquals(valuesBefore.get(1), valuesAfter.get(1));
			Reporter.log("notification check box is  not same before and After discard");
		} catch (Exception e) {
			Assert.fail("unable to Validate values");
		}
		return this;
	}

	/**
	 * It will validate the list of values before and after clicking save and
	 * discard button
	 * 
	 * @param valuesBefore
	 * @param valuesAfter
	 * @return
	 */
	public FamilyAllowancesPage validateSingleLineDashBoardValuesBeforeAndAfterSave(ArrayList<String> valuesBefore,
			ArrayList<String> valuesAfter) {
		checkPageIsReady();
		try {
			Assert.assertEquals(valuesBefore.get(0), valuesAfter.get(0));
			Reporter.log("Phone Status is  same before and After save ");
			Assert.assertEquals(valuesBefore.get(1), valuesAfter.get(1));
			Reporter.log("notification check box is  same before and After save");
		} catch (Exception e) {
			Assert.fail("unable to Validate values");
		}
		return this;
	}

	/**
	 * Verify the display of Allowed numbers blade header.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAllowedNumbersBladeHeader() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(allowedNumbersHeader));
			Reporter.log("Allowed numbers blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Allowed numbers blade");
		}
		return this;
	}

	/***
	 * verify toggle down arrow is clickable
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAllowedNumbersToggleArrow() {
		try {
			checkPageIsReady();
			Assert.assertTrue(allowedNumbersAccordian.isDisplayed() && allowedNumbersAccordian.isEnabled());
			Reporter.log("Verified Allowed numbers toggle down arrow is displayed and is enabled");
		} catch (Exception e) {
			Assert.fail("Unable to see the toggle down arrow");
		}
		return this;
	}

	/**
	 * Click on Allowed numbers blade.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clickOnAllowedNumbers() {
		try {
			waitFor(ExpectedConditions.visibilityOf(allowedNumbersAccordian));
			if (allowedNumbersAccordian.getAttribute("Class").contains(toggleUpArrow)) {
				Reporter.log("Allowed numbers blade is already expanded");
			} else {
				elementClick(allowedNumbersAccordian);
				Reporter.log("Clicked on Allowed numbers  and Allowed numbers blade is expanded successfully");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Allowed numbers blade");
		}
		return this;
	}

	/**
	 * Verify the display of allowed numbers blade description.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAllowedNumbersBladeDescription() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(allowedNumbersDescription));
			Reporter.log("Content below Allowed Numbers header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Content below Allowed Numbers header");
		}
		return this;
	}

	/**
	 * Verify the display of Allowed numbers blade header.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlwaysAllowed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(alwaysAllowed));
			Reporter.log("Always Allowed header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Always Allowed header");
		}
		return this;
	}

	/**
	 * Verify the display of Always allowed description.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlwaysAllowedDescription() {
		checkPageIsReady();
		try {
			String alwaysAllowedDesc = alwaysAllowedDescription.getText();
			Assert.assertEquals(alwaysAllowedDesc,
					"Always Allowed numbers must be domestic ten-digit phone numbers. Toll-free T-Mobile Customer Care numbers (611 or 1-877-453-1304) are always allowed, so they can�t be entered into your list.");
			Reporter.log("Content below Always allowed header is displayed");
			Assert.assertTrue(isElementDisplayed(alwaysAllowedDescription2));
			Reporter.log("All the Content below Always allowed header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Content below Always allowed header");
		}
		return this;
	}

	/**
	 * Verify the display of ALERT Tool tip Icon for Always allowed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlwaysAllowedTooltipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(alwaysAllowedTooltipIcon));
			Reporter.log("Always Allowed Tooltip icon is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip icon for Always Allowed");
		}
		return this;
	}

	/**
	 * Verify the display of alert message for Always allowed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyTooltipMessageUnderAlwaysAllowed() {
		try {
			checkPageIsReady();
			Actions Tooltip = new Actions(getDriver());
			Tooltip.clickAndHold(alwaysAllowedTooltipIcon).perform();
			Assert.assertTrue(isElementDisplayed(alwaysAllowedTooltipMessage));
			Reporter.log("Successfully verified the Tooltip message for Always allowed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip message for Always allowed");
		}
		return this;
	}

	/***
	 * verify the columns display for Always allowed under Allowed numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAlwaysAllowedColumnsDisplay() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(alwaysAllowedNumberLabel));
			Reporter.log("Number Column label is displayed successfully");
			Assert.assertTrue(isElementDisplayed(alwaysAllowedDescriptionLabel));
			Reporter.log("Description Column label is displayed successfully");
			Assert.assertTrue(isElementDisplayed(alwaysAllowedRemoveLabel));
			Reporter.log("Remove Column label is displayed successfully");
			Reporter.log("Columns under Always allowed are displayed successfully");
		} catch (Exception e) {
			Reporter.log("Unable to display the columns under Always allowed");
			Assert.fail("Unable to display the columns under Always allowed");
		}
		return this;
	}

	/***
	 * verify the default numbers displayed under Always allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDefaultNumbersUnderAlwaysAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(emergencyServicesNumber));
			Reporter.log("Emergency services number is displayed successfully under Always allowed numbers");
			Assert.assertTrue(isElementDisplayed(tMobileCustomerCareNumber));
			Reporter.log("T-Mobile Customer Care number is displayed successfully under Always allowed numbers");
			Assert.assertTrue(isElementDisplayed(emergencyServicesNumberDescription));
			Reporter.log(
					"Emergency services number description is displayed successfully under Always allowed numbers");
			Assert.assertTrue(isElementDisplayed(tMobileCustomerCareNumberDescription));
			Reporter.log(
					"T-Mobile Customer Care number description is displayed successfully under Always allowed numbers");
			Reporter.log("Default numbers are shown under Always allowed successfully");
		} catch (Exception e) {
			Reporter.log("Unable to display the default numbers under Always allowed");
			Assert.fail("Unable to display the default numbers under Always allowed");
		}
		return this;
	}

	/***
	 * verify Remove button is not displayed for default numbers under Always
	 * allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNoRemoveOptionForDefaultNumbersUnderAlwaysAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!isElementDisplayed(noRemoveButtonForEmergencyServices));
			Reporter.log(
					"Remove button is not displayed for 911 Emergency services default number under Always allowed");
			Assert.assertTrue(!isElementDisplayed(noRemoveButtonForTMobileCustCare));
			Reporter.log(
					"Remove button is not displayed for T-Mobile Customer care default number under Always allowed");
		} catch (Exception e) {
			Reporter.log("Remove button gets displayed for default numbers under Always allowed");
			Assert.fail("Remove button gets displayed for default numbers under Always allowed");
		}
		return this;
	}

	/***
	 * verify Add new text box displayed under Always allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddNewTextFieldUnderAlwaysAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(addNewNumberForAlwaysAllowed));
			Reporter.log("Add new text field is displayed under Always allowed");
		} catch (Exception e) {
			Reporter.log("Add new text field is not displayed under Always allowed");
			Assert.fail("Add new text field is not displayed under Always allowed");
		}
		return this;
	}

	/**
	 * Verify the length of Add New text field under Always allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinAndMaxLengthOfAddNewTextFieldUnderAlwaysAllowed() {
		checkPageIsReady();
		try {
			int max_length = Integer.parseInt(addNewNumberForAlwaysAllowed.getAttribute("maxlength"));
			Assert.assertEquals(max_length, 14);
			Reporter.log("Verified, Maximum length allowed in the Add New text field under Always allowed is "
					+ max_length + " digits");
			int min_length = Integer.parseInt(addNewNumberForAlwaysAllowed.getAttribute("minlength"));
			Assert.assertEquals(min_length, 14);
			Reporter.log("Verified, Minimum length allowed in the Add New text field under Always allowed is "
					+ min_length + " digit");
		} catch (Exception e) {
			Assert.fail("Failed to verify the minimum and maximum length of Add New text field under Always allowed");
		}
		return this;
	}

	/**
	 * Enter the data into Add new text field under Always allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage enterDataIntoAddNewUnderAlwaysAllowed(String addNewValue) {
		try {
			waitFor(ExpectedConditions.visibilityOf(addNewNumberForAlwaysAllowed));
			sendTextData(addNewNumberForAlwaysAllowed, addNewValue);
			Reporter.log("Entered value " + addNewValue + " in the Add new text field under Always allowed");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + addNewValue + " into the text box");
		}
		return this;
	}

	/**
	 * Verify the add button display for Always allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddButtonForAlwaysAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(addButtonForAlwaysAllowed.isDisplayed());
			Reporter.log("add button is displayed for always allowed field");
		} catch (Exception e) {
			Assert.fail("Unable to display addButtonForAlwaysAllowed");
		}
		return this;
	}

	/**
	 * Click the add button for always allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickAddButtonForAlwaysAllowed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addButtonForAlwaysAllowed));
			Assert.assertTrue(addButtonForAlwaysAllowed.isEnabled());
			clickElement(addButtonForAlwaysAllowed);
			Reporter.log("add button for always allowed field is clickable");
		} catch (Exception e) {
			Assert.fail("Unable to validate addButtonForAlwaysAllowed");
		}
		return this;
	}

	/**
	 * verify the display of newly added number in Never allowed under Allowed
	 * numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddedNumberDisplayedUnderAlwaysAllowed() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addNewNumberForAlwaysAllowed));
			String numberEnteredToAddUnderAlwaysAllowed = addNewNumberForAlwaysAllowed.getText();
			if (newlyAddedNumberForAlwaysAllowed.toString().contains(numberEnteredToAddUnderAlwaysAllowed)) {
				Reporter.log("Newly added number is displayed under Always allowed numbers");
			} else {
				Reporter.log("Newly added number is not displayed under Always allowed numbers");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to see the newly added number under Always allowed numbers");
		}
		return this;
	}

	/**
	 * verify removing the newly added number in Always allowed under Allowed
	 * numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndRemovingNewNumberUnderAlwaysAllowed() {
		try {
			String numberEnteredToAddUnderAlwaysAllowed = addNewNumberForAlwaysAllowed.getText();
			if (newlyAddedNumberForAlwaysAllowed.toString().contains(numberEnteredToAddUnderAlwaysAllowed)) {
				int alwaysAllowedSize = removeIconForNewlyAddedNumberUnderAlwaysAllowed.size();
				if (removeIconForNewlyAddedNumberUnderAlwaysAllowed.get(alwaysAllowedSize - 1).isDisplayed()) {
					Reporter.log("Remove icon is displayed for newly added number under Always allowed");
					removeIconForNewlyAddedNumberUnderAlwaysAllowed.get(alwaysAllowedSize - 1).click();
					Reporter.log("Clicked on Remove icon");
					Reporter.log("Newly added number has been removed successfully under Always allowed number");
				} else {
					Reporter.log("Newly added number is not been removed successfully under Always allowed number");
				}
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to remove the newly added number under Always allowed numbers");
		}
		return this;
	}

	/**
	 * Verify the display of Foot note under Always allowed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyFootNoteUnderAlwaysAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(alwaysAllowedFootNote));
			Reporter.log("Foot note is displayed under Always allowed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Foot note under Always allowed");
		}
		return this;
	}

	/**
	 * Verify field validation error message, upon entering invalid number in
	 * Add new text field under Always allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyErrorMessageForInvalidNumberUnderAlwaysAllowed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(alwaysAllowedNumberFieldValidationErrorMessage));
			Reporter.log(
					"'ERROR: Please enter a valid phone number (no international numbers, 900 numbers, or 800 numbers - including 866, 877, and 888).' field validation error message is displayed successfully in Add new text field");
		} catch (Exception e) {
			Assert.fail("Failed to display the validation error message for Add new field");
		}
		return this;
	}

	/**
	 * verify the display of saved number under Always allowed of Allowed
	 * numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifySavedNumberUnderAlwaysAllowedForAllowedNumbers() {
		checkPageIsReady();
		try {
			String numberEnteredToAddUnderAlwaysAllowed = addNewNumberForAlwaysAllowed.getText();
			for (WebElement ele : newlyAddedNumberForAlwaysAllowed) {
				if (ele.getText().toString().equals(numberEnteredToAddUnderAlwaysAllowed)) {
					break;
				} else {
					Reporter.log("Newly added number is saved under Always allowed numbers");
				}
			}
			Reporter.log("Newly added number is saved under Always allowed numbers");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to save the newly added number under Always allowed numbers");
		}
		return this;
	}

	/**
	 * verify the Discard Changes does not apply for a number to be added under
	 * Always allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNumberWithDiscardChangesForAlwaysAllowed() {
		checkPageIsReady();
		try {
			String numberEnteredToAddUnderAlwaysAllowed = addNewNumberForAlwaysAllowed.getText();
			for (WebElement ele : newlyAddedNumberForAlwaysAllowed) {
				if (!ele.getText().toString().equals(numberEnteredToAddUnderAlwaysAllowed)) {
					Reporter.log(
							"After discard changes, Newly added number is not displayed under Always allowed numbers");
					break;
				} else {
					Reporter.log(
							"After discard changes, Newly added number is not displayed under Always allowed numbers");
				}
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to save the newly added number under Always allowed numbers, after discard changes");
		}
		return this;
	}

	/**
	 * Verify field validation error message, upon entering duplicate number in
	 * Add new text field under Always allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyErrorMessageForAddingDuplicateUnderAlwaysAllowed(String addNewValue) {
		try {
			Assert.assertTrue(isElementDisplayed(duplicateNumberErrorMessageUnderAlwaysAllowed));
			Reporter.log("Duplicate number cannot be added under Always allowed numbers");
			Reporter.log(
					"'This number already exists in your Always Allowed list' error message is displayed successfully");
		} catch (NoSuchElementException e) {
			Assert.fail("Duplicate number is getting added under Always allowed numbers");
			Assert.fail("Failed to display the error message while adding a duplicate number under Always allowed");
		}
		return this;
	}

	/**
	 * Verify the display of Never allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyNeverAllowed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(neverAllowed));
			Reporter.log("Never Allowed header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Never Allowed header");
		}
		return this;
	}

	/**
	 * Verify the display of allowed numbers blade description.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyNeverAllowedDescription() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(neverAllowedDescription));
			Reporter.log("Content below Never allowed header is displayed");
			Assert.assertTrue(isElementDisplayed(neverAllowedDescription));
			Reporter.log("All the Content below Never allowed header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Content below Never allowed header");
		}
		return this;
	}

	/**
	 * Verify the display of Tool tip Icon for Never allowed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyNeverAllowedTooltipIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(neverAllowedTooltipIcon));
			Reporter.log("Tooltip icon is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip icon");
		}
		return this;
	}

	/**
	 * Verify the display of alert message for Never allowed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageUnderNeverAllowed() {
		try {
			checkPageIsReady();
			Actions Tooltip = new Actions(getDriver());
			Tooltip.clickAndHold(neverAllowedTooltipIcon).perform();
			Assert.assertTrue(neverAllowedTooltipMessage.isDisplayed());
			Reporter.log("Successfully verified the Tooltip message for Never allowed");
		} catch (Exception e) {
			Assert.fail("Failed to display the Tooltip message for Never allowed");
		}
		return this;
	}

	/***
	 * verify the columns display under Never allowed under Allowed numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNeverAllowedColumnsDisplay() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(neverAllowedNumberLabel));
			Reporter.log("Number Column label is displayed successfully");
			Assert.assertTrue(isElementDisplayed(neverAllowedRemoveLabel));
			Reporter.log("Remove Column label is displayed successfully");
			Reporter.log("Columns are displayed successfully under Never allowed");
		} catch (Exception e) {
			Reporter.log("Unable to display the columns under Never allowed");
			Assert.fail("Unable to display the columns under Never allowed");
		}
		return this;
	}

	/***
	 * verify the default number 411 check box under Never allowed under Allowed
	 * numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verify411NumberCheckboxUnderNeverAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(neverAllowed411NumberCheckbox));
			Reporter.log("411 default number checkbox is displayed successfully under Never allowed");
			Assert.assertTrue(isElementDisplayed(neverAllowed411NumberCheckboxText));
			Reporter.log("411 default number checkbox text is displayed successfully under Never allowed");
		} catch (Exception e) {
			Reporter.log("Unable to display the 411DefaultNumberCheckbox under Never allowed");
			Assert.fail("Unable to display the 411DefaultNumberCheckbox under Never allowed");
		}
		return this;
	}

	/**
	 * Check or uncheck 411 number Check box for Never allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage add411NumberUnderNeverAllowed(String expectedCheckState) {
		checkPageIsReady();
		try {
			String actualCheckedState = neverAllowed411NumberAddCheckbox.getAttribute("value").toString();
			if (actualCheckedState.contains(expectedCheckState)) {
				Reporter.log("411 check box is already set to " + actualCheckedState);
			} else {
				Reporter.log("411 check box is set to " + expectedCheckState);
				elementClick(neverAllowed411NumberCheckbox);
				Reporter.log("Clicked on 411 number checkbox, and added to Never allowed numbers");
				Reporter.log("411 number check box is now set to " + expectedCheckState);
			}
		} catch (Exception e) {
			Assert.fail("Unable to add 411 number to never allowed numbers");
		}
		return this;
	}

	/***
	 * verify Remove button is not displayed for default numbers under Never
	 * allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddNewTextFieldUnderNeverAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(isElementDisplayed(addNewNumberForNeverAllowed));
			Reporter.log("Add new text field is displayed under Never allowed");
		} catch (Exception e) {
			Reporter.log("Add new text field is not displayed under Never allowed");
			Assert.fail("Add new text field is not displayed under Never allowed");
		}
		return this;
	}

	/**
	 * Verify the length of Add New text field under Never allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyMinAndMaxLengthOfAddNewTextFieldUnderNeverAllowed() {
		checkPageIsReady();
		try {
			int max_length = Integer.parseInt(addNewNumberForNeverAllowed.getAttribute("maxlength"));
			Assert.assertEquals(max_length, 14);
			Reporter.log("Verified, Maximum length allowed in the Add New text field under Never allowed is "
					+ max_length + " digits");
			int min_length = Integer.parseInt(addNewNumberForNeverAllowed.getAttribute("minlength"));
			Assert.assertEquals(min_length, 14);
			Reporter.log("Verified, Minimum length allowed in the Add New text field under Never allowed is "
					+ min_length + " digit");
		} catch (Exception e) {
			Assert.fail("Failed to verify the minimum and maximum length of Add New text field under Never Allowed");
		}
		return this;
	}

	/**
	 * Enter the data into Add new text field under Never allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage enterDataIntoAddNewUnderNeverAllowed(String addNewValue) {
		try {
			waitFor(ExpectedConditions.visibilityOf(addNewNumberForNeverAllowed));
			sendTextData(addNewNumberForNeverAllowed, addNewValue);
			Reporter.log("Entered value " + addNewValue + " in the Add new text field under Never allowed");
		} catch (Exception e) {
			Assert.fail("failed to enter value" + addNewValue + " into the text box");
		}
		return this;
	}

	/**
	 * It will verify if the add button for Never allowed field is displayed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddButtonForNeverAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(addButtonForNeverAllowed.isDisplayed());
			Reporter.log("add button is displayed for Never allowed field");
			clickElement(addButtonForNeverAllowed);
			Reporter.log("add button for Never allowed field is displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display addButtonForNeverAllowed");
		}
		return this;
	}

	/**
	 * It will verify if the add button for never allowed field
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickAddButtonForNeverAllowed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(addButtonForNeverAllowed.isDisplayed());
			Assert.assertTrue(addButtonForNeverAllowed.isEnabled());
			Reporter.log("add button is displayed and enabled for never allowed field");
			clickElement(addButtonForNeverAllowed);
			Reporter.log("add button for never allowed field is clickable");
		} catch (Exception e) {
			Assert.fail("Unable to validate addButtonForNeverAllowed");
		}
		return this;
	}

	/**
	 * verify the display of newly added number in Never allowed under Allowed
	 * numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAddedNumberDisplayedUnderNeverAllowed() {
		checkPageIsReady();
		try {
			String numberEnteredToAddUnderNeverAllowed = addNewNumberForNeverAllowed.getText();
			if (newlyAddedNumberForNeverAllowed.toString().equals(numberEnteredToAddUnderNeverAllowed)) {
				Reporter.log("Newly added number is displayed under Never allowed numbers");
			} else {
				Reporter.log("Newly added number is displayed under Never allowed numbers");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to see the newly added number under Never allowed numbers");
		}
		return this;
	}

	/**
	 * verify removing the newly added number in Never allowed under Allowed
	 * numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndRemovingNewNumberUnderNeverAllowed() {
		try {
			String numberEnteredToAddUnderNeverAllowed = addNewNumberForNeverAllowed.getText();
			if (newlyAddedNumberForNeverAllowed.toString().contains(numberEnteredToAddUnderNeverAllowed)) {
				int neverAllowedSize = removeIconForNewlyAddedNumberUnderNeverAllowed.size();
				if (removeIconForNewlyAddedNumberUnderNeverAllowed.get(neverAllowedSize - 1).isDisplayed()) {
					Reporter.log("Remove icon is displayed for newly added number under Never allowed");
					removeIconForNewlyAddedNumberUnderNeverAllowed.get(neverAllowedSize - 1).click();
					Reporter.log("Clicked on Remove icon");
					Reporter.log("Newly added number has been removed successfully under Never allowed number");
				} else {
					Reporter.log("Newly added number is not been removed successfully under Never allowed number");
				}
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to remove the newly added number under Never allowed numbers");
		}
		return this;
	}

	/**
	 * Verify field validation error message, upon entering invalid number in
	 * Add new text field under Never allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyErrorMessageForInvalidNumberUnderNeverAllowed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(neverAllowedNumberFieldValidationErrorMessage));
			Reporter.log(
					"'ERROR: Please enter a valid phone number (no international numbers, 900 numbers, or 800 numbers - including 866, 877, and 888).' field validation error message is displayed successfully in Add new text field");
		} catch (Exception e) {
			Assert.fail("Failed to display the validation error message for Add new field");
		}
		return this;
	}

	/**
	 * verify the display of saved number under Always allowed of Allowed
	 * numbers
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifySavedNumberUnderNeverAllowedForAllowedNumbers() {
		checkPageIsReady();
		try {
			String numberEnteredToAddUnderNeverAllowed = addNewNumberForNeverAllowed.getText();
			for (WebElement ele : newlyAddedNumberForNeverAllowed) {
				if (ele.getText().toString().equals(numberEnteredToAddUnderNeverAllowed)) {
					Reporter.log("Newly added number is saved under Never allowed numbers");
				} else {
					Reporter.log("Newly added number is saved under Never allowed numbers");
				}
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to save the newly added number under Never allowed numbers");
		}
		return this;
	}

	/**
	 * verify the Discard Changes does not apply for a number to be added under
	 * Never allowed
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNumberWithDiscardChangesForNeverAllowed() {
		checkPageIsReady();
		try {
			String numberEnteredToAddUnderNeverAllowed = addNewNumberForNeverAllowed.getText();
			for (WebElement ele : newlyAddedNumberForNeverAllowed) {
				if (!ele.getText().toString().equals(numberEnteredToAddUnderNeverAllowed)) {
					Reporter.log(
							"After discard changes, Newly added number is not displayed under Never allowed numbers");
					break;
				} else {
					Reporter.log(
							"After discard changes, Newly added number is not displayed under Never allowed numbers");
				}

			}
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to save the newly added number under Never allowed numbers, after discard changes");
		}
		return this;
	}

	/**
	 * Verify field validation error message, upon entering duplicate number in
	 * Add new text field under Never allowed
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyErrorMessageForAddingDuplicateUnderNeverAllowed(String addNewValue) {
		try {
			Assert.assertTrue(isElementDisplayed(duplicateNumberErrorMessageUnderNeverAllowed));
			Reporter.log("Duplicate number cannot be added under Never allowed numbers");
			Reporter.log(
					"'This number already exists in your Always Allowed list' error message is displayed successfully in Add new text field");
		} catch (NoSuchElementException e) {
			Assert.fail("Duplicate number is getting added under Always allowed numbers");
			Assert.fail("Failed to display the error message while adding a duplicate number under Never allowed");
		}
		return this;
	}

	/**
	 * Verify and enter the value in a line selector Text box
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyAndEditSelectOtherLine() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			historyLineSelectorClearIon.isEnabled();
			elementClick(historyLineSelectorClearIon);
			sendTextData(historyLineSelectorSearchTextBar, "Ram");
			historyLineSelectorSearchTextBar.sendKeys(Keys.ARROW_DOWN);
			historyLineSelectorSearchTextBar.sendKeys(Keys.ENTER);
			historyLineSelectorSearchTextBar.sendKeys(Keys.TAB);
			Reporter.log("Selected other line from Select line drop down");
		} catch (Exception e) {
			Assert.fail("Unable to validate family allowances select line");
		}
		return this;
	}

	/**
	 * It will return the calculated remaining value under Minutes blade
	 * 
	 * @return
	 */
	public String getRemainingValueForMinutes() {
		checkPageIsReady();
		String allowanceValue = minutesRemainingValueWithAllowance.getText();
		return allowanceValue;
	}

	/**
	 * It will return the calculated remaining value under Messages blade
	 * 
	 * @return
	 */
	public String getRemainingValueForMessages() {
		checkPageIsReady();
		String allowanceValue = messagesRemainingValueWithAllowance.getText();
		return allowanceValue;
	}

	/**
	 * It will return the calculated remaining value under Downloads blade
	 * 
	 * @return
	 */
	public String getRemainingValueForDownloads() {
		checkPageIsReady();
		String allowanceValue = downloadsRemainingValueWithAllowance.getText();
		return allowanceValue;
	}

	/**
	 * It will return schedule state
	 * 
	 * @return
	 */
	public String getScheduleState() {

		checkPageIsReady();
		String checkedState = scheduleCheckBoxStatus.getAttribute("value");
		return checkedState;
	}

	/**
	 * It will return number added under Always allowed
	 * 
	 * @return
	 */
	public String getNumberEnteredUnderAlwaysAllowed() {
		checkPageIsReady();
		String numberEnteredToAddUnderAlwaysAllowed = addNewNumberForAlwaysAllowed.getAttribute("value");
		return numberEnteredToAddUnderAlwaysAllowed;
	}

	/**
	 * It will return number added under Always allowed
	 * 
	 * @return
	 */
	public String getNumberUnderAlwaysAllowed() {
		checkPageIsReady();
		String numberAdded = new String();
		for (WebElement ele : newlyAddedNumberForAlwaysAllowed) {
			if (ele.getText().toString().equals(getNumberEnteredUnderAlwaysAllowed())) {
				numberAdded = ele.getText().toString();
				Reporter.log("Newly added number is displayed under Always allowed numbers");
			}
		}
		return numberAdded;
	}

	/**
	 * It will discard the number added under Always allowed, after clicking on
	 * discard
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNumberDiscardedNotSavedUnderAlwaysAllowed(String valueBeforeDiscard) {
		checkPageIsReady();
		try {
			for (WebElement ele : newlyAddedNumberForAlwaysAllowed) {
				if (!ele.getText().toString().equals(valueBeforeDiscard)) {
				} else {
					Reporter.log("Newly added number is saved under Always allowed numbers");
				}
			}
			Reporter.log("Newly added number is discarded, and is not saved under Always allowed numbers");
		} catch (NoSuchElementException e) {
			Assert.fail("Newly added number is saved under Always allowed numbers");
		}
		return this;
	}

	/**
	 * It will return number added under Never allowed
	 * 
	 * @return
	 */
	public String getNumberEnteredUnderNeverAllowed() {
		checkPageIsReady();
		String numberEnteredToAddUnderNeverAllowed = addNewNumberForNeverAllowed.getAttribute("value");
		return numberEnteredToAddUnderNeverAllowed;
	}

	/**
	 * It will return number saved under Never allowed
	 * 
	 * @return
	 */
	public String getNumberUnderNeverAllowed() {
		checkPageIsReady();
		String numberAdded = new String();
		for (WebElement ele : newlyAddedNumberForNeverAllowed) {
			if (ele.getText().toString().equals(getNumberEnteredUnderNeverAllowed())) {
				numberAdded = ele.getText().toString();
				Reporter.log("Newly added number is displayed under Never allowed numbers");
			}
		}
		return numberAdded;
	}

	/**
	 * It will discard the number added under Never allowed, after clicking on
	 * discard
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNumberDiscardedNotSavedUnderNeverAllowed(String valueBeforeDiscard) {
		checkPageIsReady();
		try {
			for (WebElement ele : newlyAddedNumberForNeverAllowed) {
				if (!ele.getText().toString().equals(valueBeforeDiscard)) {
				} else {
					Reporter.log("Newly added number is saved under Never allowed numbers");
				}
			}
			Reporter.log("Newly added number is discarded, and is not saved under Never allowed numbers");
		} catch (NoSuchElementException e) {
			Assert.fail("Newly added number is saved under Never allowed numbers");
		}
		return this;
	}

	/**
	 * 
	 * It will validate the list of values before and after clicking save button
	 * 
	 * @param valuesBefore
	 * @param valuesAfter
	 * @return
	 */
	public FamilyAllowancesPage validateSavedValueDisplayedForOtherMISDIN(String valueBeforeSave,
			String valueAfterSave) {
		checkPageIsReady();
		try {
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Assert.assertEquals(valueBeforeSave, valueAfterSave);
				Reporter.log(
						"Updated value is displayed for the other MISDIN, after Save, after Apply allowance changes set to all line is applied");
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				String valueAfter = valueAfterSave.replace("$", "");
				//System.out.println(valueAfter);
				Assert.assertEquals("0", valueAfterSave.replace("$", ""));
				Reporter.log("remaining value is zero as phone status is in 'OFF' status");
			}
		} catch (Exception e) {
			Assert.fail("Unable to see the saved value for the other MISDIN");
		}
		return this;
	}

	/**
	 * 
	 * It will validate the list of values before and after clicking save button
	 * under Downloads
	 * 
	 * @param valuesBefore
	 * @param valuesAfter
	 * @return
	 */
	public FamilyAllowancesPage validateSavedValueDisplayedForDownloadsForOtherMISDIN(String valueBeforeSave,
			String valueAfterSave) {
		checkPageIsReady();
		try {
			if (lineDashBoardCurrentPhoneStatus.getText().equals("ON")) {
				Assert.assertEquals(valueBeforeSave, valueAfterSave);
				Reporter.log(
						"Updated value is displayed for the other MISDIN, after Save, after Apply allowance changes set to all line is applied");
			} else if (lineDashBoardCurrentPhoneStatus.getText().equals("OFF")) {
				Assert.assertEquals("$0", valueAfterSave);
				Reporter.log("remaining value is zero as phone status is in 'OFF' status");
			}
		} catch (Exception e) {
			Assert.fail("Unable to see the saved value for the other MISDIN");
		}
		return this;
	}

	/**
	 * 
	 * It will validate the list of values before and after clicking Discard
	 * changes button
	 * 
	 * @param valuesBefore
	 * @param valuesAfter
	 * @return
	 */
	public FamilyAllowancesPage validateValueAfterDiscardChangesForOtherMISDIN(String valueBeforeDiscard,
			String valueAfterDiscard) {
		checkPageIsReady();
		try {
			Assert.assertNotEquals(valueBeforeDiscard, valueAfterDiscard);
			Reporter.log(
					"Updated value is not displayed for the other MISDIN, after Discard changes, even after Apply allowance changes set to all line is applied");
		} catch (Exception e) {
			Assert.fail("Unable to see the saved value for the other MISDIN");
		}
		return this;
	}

	/**
	 * verifies if modal message is displayed in dashboard page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyModalMessageOnDashBoardPage() {
		try {
			Assert.assertTrue(isElementDisplayed(modalMessage));
			Reporter.log("Modal message is displayed");
			clickElement(modalMessageOkButton);
			Reporter.log("clicked ok");
		} catch (Exception e) {
			Assert.fail("failed to validate modal message");
		}
		return this;
	}

	/**
	 * verifies if modal message is not displayed in dashboard page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyModalMessageOnDashBoardPageIsNotDisplayed() {
		try {
			Assert.assertTrue(!modalMessage.isDisplayed());
			Reporter.log("Modal message is not displayed ");
		} catch (Exception e) {
			Reporter.log("modal message is not displayed ");
		}
		return this;
	}

	/**
	 * verifies if note is displayed in minutes accordian
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMinutesNote() {
		checkPageIsReady();
		try {
			Assert.assertTrue(minutesNote.isDisplayed());
			Reporter.log("minutes note is displayed");
			Assert.assertTrue(minutesNoteText.isDisplayed());
			Reporter.log("minutes node text  is displayed");
		} catch (Exception e) {
			Assert.fail("failed to validate minutes note message");
		}
		return this;
	}

	/**
	 * verifies if note is not displayed in minutes accordian
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMinutesNoteIsNotDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!minutesNote.isDisplayed());
			Reporter.log("minutes note is not displayed");
		} catch (Exception e) {
			Reporter.log("minutes note is not displayed");
		}
		return this;
	}

	/**
	 * verifies if note is displayed in messages accordian
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesNote() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messagesNote.isDisplayed());
			Reporter.log("messages note is displayed");
			Assert.assertTrue(messagesNoteText.isDisplayed());
			Reporter.log("messages note text  is displayed");
		} catch (Exception e) {
			Assert.fail("failed to validate messages note message");
		}
		return this;
	}

	/**
	 * verifies if note is not displayed in messages accordian
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesNoteIsNotDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!messagesNote.isDisplayed());
			Reporter.log("messages note is not displayed");
		} catch (Exception e) {
			Reporter.log("messages note is not displayed");
		}
		return this;
	}

	/**
	 * verifies if note is displayed in downloads accordian
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDownloadsNote() {
		checkPageIsReady();
		try {
			Assert.assertTrue(downloadsNote.isDisplayed());
			Reporter.log("downloads note is displayed");
			Assert.assertTrue(downloadsNoteText.isDisplayed());
			Reporter.log("downloads note text  is displayed");
		} catch (Exception e) {
			Assert.fail("failed to minutes note message");
		}
		return this;
	}

	/**
	 * 
	 * verifies if note is not displayed in downloads accordian
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDownloadsNoteIsNotDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(!downloadsNote.isDisplayed());
			Reporter.log("downloads note is not displayed");
		} catch (Exception e) {
			Reporter.log("downloads note is not displayed");
		}
		return this;
	}

	/**
	 * navigate to view list of line page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage navigateToViewListOfLinesFromFamilyAllowance() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(viewListOfLinesFromLineDashBoard));
			clickElement(viewListOfLinesFromLineDashBoard);
			Assert.assertTrue(isElementDisplayed(viewListOfLinesPage));
			Reporter.log("navigated to view list of lines page from family allowance page");
		} catch (Exception e) {
			Assert.fail("unable to navigate to list of lines page from family allowance page");
		}
		return this;
	}

	/**
	 * Verify Family allowance page.
	 *
	 * @return the Family allowance page class instance.
	 */
	public FamilyAllowancesPage verifyFamilyAllowancePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(familyAllowancesPageUrl);
			Reporter.log("Family allowances page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Family allowances page not displayed");
		}
		return this;
	}

	/**
	 * It will validate the breadcrumb in fmaily allowance page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyBreadCrumbInFamilyAllowance() {
		try {
			checkPageIsReady();
			Assert.assertEquals(breadCrumbProfile.getText() + " > " + breadCrumbFamilyControls.getText() + " > "
					+ breadCrumbFamilyAllowance.getText(), expectedBreadCrumbInFamilyAllowance);
			Reporter.log("BreadCrumb in Family allowance is displayed as expected");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to validate BreadCrumb");
		}
		return this;
	}

	/**
	 * It will validate the breadcrumb in view list of all lines page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyBreadCrumbInViewListOfLines() {
		try {
			checkPageIsReady();
			Assert.assertEquals(breadCrumbProfile.getText() + " > " + "...." + " > "
					+ breadCrumbFamilyAllowance.getText() + " > " + breadCrumbViewListOfAllLines.getText(),
					expectedBreadCrumbInViewListOfAllLines);
			Reporter.log("BreadCrumb in view list of lines is displayed as expected");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to validate BreadCrumb");
		}
		return this;
	}

	/**
	 * It will validate the breadcrumb in history of all lines
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyBreadCrumbInHistoryOfAllLines() {
		try {
			checkPageIsReady();
			Assert.assertEquals(breadCrumbProfile.getText() + " > " + "...." + " > "
					+ breadCrumbFamilyAllowance.getText() + " > " + breadCrumbHistoryOfAllLines.getText(),
					expectedBreadCrumbInHistoryOfAllLines);
			Reporter.log("BreadCrumb in history of all lines is displayed as expected");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to validate BreadCrumb");
		}
		return this;
	}

	/**
	 * It will click on family allowances breadcrumb
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickFamilyAllowancesBreadCrumb() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			clickElement(breadCrumbFamilyAllowance);
		} catch (NoSuchElementException e) {
			Assert.fail("Click on family allowances bread crumb failed");
		}
		return this;
	}

	/**
	 * It will click on family controls breadcrumb
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickFamilyControlsBreadCrumb() {
		try {
			waitFor(ExpectedConditions.visibilityOf(breadCrumbFamilyControls));
			clickElement(breadCrumbFamilyControls);
			waitforSpinnerinProfilePage();
		} catch (NoSuchElementException e) {
			Assert.fail("Click on family controls breadcrumb failed");
		}
		return this;
	}

	/**
	 * click Change history Blade
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickChangeHistoryBlade() {
		try {
			waitforSpinnerinProfilePage();
			moveToElement(changeHistoryBlade);
			changeHistoryBlade.click();
			Reporter.log("Click on change history blade is success");
		} catch (Exception e) {
			Reporter.log("Click on change history blade failed");
			Assert.fail("Click on change history blade failed");
		}
		return this;
	}

	/**
	 * It will verify view list of lines CTA
	 * 
	 * @return
	 */
	public FamilyAllowancesPage ClickChangeHistoryViewListOfLinesCTA() {
		try {
			waitforSpinnerinProfilePage();
			clickElement(changeHistoryViewListOfLines);
		} catch (Exception e) {
			Reporter.log("Click on change history view list of lines failed");
			Assert.fail("Click on change history view list of lines failed");
		}
		return this;
	}

	/**
	 * click view list of lines cta
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickViewListOfLinesCTA() {
		checkPageIsReady();
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(viewListOfLinesFromLineDashBoard));
			viewListOfLinesFromLineDashBoard.click();
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(viewListOfLinesPage));
			Assert.assertTrue(viewListOfLinesPage.isDisplayed());
			Reporter.log("navigated to  view list of all lines page successfully");
		} catch (Exception e) {
			Reporter.log("Click on view list of lines failed");
			Assert.fail("Click on view list of lines failed");
		}
		return this;
	}

	public FamilyAllowancesPage integralServerErrorModalOnFailureOfSave() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(errorMessageOnSave));
			Assert.assertTrue(isElementDisplayed(errorMessageOnSave));
			waitFor(ExpectedConditions.visibilityOf(okButton));
			okButton.click();
			Reporter.log("Error message 'We're having some trouble'  is diplayed and  validated");
		} catch (Exception e) {
			Assert.fail("unable to validate error message");
		}
		return this;
	}

	public FamilyAllowancesPage clickContinue() {
		try {
			waitFor(ExpectedConditions.visibilityOf(continueButton));
			clickElement(continueButton);
		} catch (Exception e) {
			Assert.fail("unable to validate continue button");
		}
		return this;
	}

	/**
	 * Click on Parent List drop down menu
	 * 
	 * @return
	 */
	public FamilyAllowancesPage clickOnLineDashBoardParentListDropDown() {
		try {
			checkPageIsReady();
			lineDashBoardParentListDropDown.click();
			Reporter.log("Clicked on Parent Line drop down");
		} catch (Exception e) {
			Assert.fail("Unable to click on Parent Line drop down");
		}
		return this;
	}

	/**
	 * Verify the display of 'None' option in Parent List drop down menu
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNoneOptionInParentListDropDown() {
		try {
			checkPageIsReady();
			noneOption.isDisplayed();
			Reporter.log("None option is displayed in the list of Parent selector values");
		} catch (Exception e) {
			Assert.fail("Unable to see the None option in Parent Line drop down");
		}
		return this;
	}

	/**
	 * Verify the display of full access users in Parent List drop down menu
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyFullAccessUsersInParentListDropDown() {
		try {
			checkPageIsReady();
			if (parentLineSelectorValuesList.size() > 0)
				Reporter.log("Full access users are displayed in the list of Parent selector values");
		} catch (Exception e) {
			Assert.fail("Unable to see the Full access users in the list of Parent selector values");
		}
		return this;
	}

	/**
	 * Select 'None' option in Parent List drop down menu
	 * 
	 * @return
	 */
	public FamilyAllowancesPage selectNoneOptionInParentListDropDown() {
		try {
			checkPageIsReady();
			noneOption.click();
			Reporter.log("None option is selected from the list of Parent selector values");
		} catch (Exception e) {
			Assert.fail("Unable to select the None option from the list of Parent Line drop down");
		}
		return this;
	}

	public FamilyAllowancesPage clickOnSubModule(String moduleName) {
		try {
			clickElementBytext(listOfSubModules, moduleName);
		} catch (Exception e) {
			Assert.fail("unable to validate error message");
		}
		return this;
	}

	/**
	 * Minutes Flow Details verification
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMinutesFlowDetails(String allowance) {
		try {
			clickOnSubModule("Minutes");
			verifyMinutesBladeDescription();
			verifyMinutesFieldLabelsDisplay();
			checkOrUncheckMinutesNoAllowanceCheckbox("false");
			clearCurrentAllowanceTextFieldUnderMinutes();
			enterDataIntoMinutesCurrentAllowance(allowance);
			verifyCalculatedRemainingValueForMinutes();
			verifySavedCurrentAllowanceDisplayedUnderMinutes();
			Reporter.log("Minutes field validation successful");
		} catch (Exception e) {
			Assert.fail("Minutes field validation error");
		}
		return this;
	}

	/**
	 * Messages Flow Details verification
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesFlowDetails(String allowance) {
		try {
			clickOnSubModule("Messages");
			verifyMessagesBladeDescription();
			verifyMessagesFieldLabelsDisplay();
			checkOrUncheckMessagesNoAllowanceCheckbox("false");
			clearCurrentAllowanceTextFieldUnderMessages();
			enterDataIntoMessagesCurrentAllowance(allowance);
			verifyCalculatedRemainingValueForMessages();
			verifySavedCurrentAllowanceDisplayedUnderMessages();
			Reporter.log("Messages field validation successful");
		} catch (Exception e) {
			Assert.fail("Message field validation error");
		}
		return this;
	}

	/**
	 * Downloads Flow Details verification
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDownloadsFlowDetails(String allowance) {
		try {
			clickOnSubModule("Downloads");
			verifyDownloadsBladeDescription();
			verifyDownloadsFieldLabelsDisplay();
			checkOrUncheckDownloadsNoAllowanceCheckbox("false");
			clearCurrentAllowanceTextFieldUnderDownloads();
			enterDataIntoDownloadsCurrentAllowance(allowance);
			verifyCalculatedRemainingValueForDownloads();
			verifySavedCurrentAllowanceDisplayedUnderDownloads();
			Reporter.log("Downloads field validation successful");
		} catch (Exception e) {

			Assert.fail("Downloads field validation error");
		}
		return this;
	}

	/**
	 * Schedule Flow Details verification
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyScheduleFlowDetails() {
		try {
			clickOnSubModule("Schedule");
			verifyScheduleHeadingDescription();
			checkOrUncheckScheduleMontoFriFieldForSave();
			Reporter.log("Schedule field validation successful");
		} catch (Exception e) {
			Assert.fail("Schedule field validation error");
		}
		return this;
	}

	/**
	 * HEAD Verify display of 'None' activated for the parent after save
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyNoneOptionSavedForParentSelectorDropDown() {
		try {
			checkPageIsReady();
			noneOptionDisplayAfterSave.isDisplayed();
			Reporter.log("Parent is set to None for the selected line");
		} catch (Exception e) {
			Assert.fail("Unable to set the parent to None for the selected line");
		}
		return this;
	}

	/*
	 * Allowed numbers Flow Details verification**@return
	 */
	public FamilyAllowancesPage verifyAllowedNumbersFlowDetails(String number, String number1) {
		try {
			clickOnSubModule("Allowed numbers");
			enterDataIntoAddNewUnderAlwaysAllowed(number);
			clickAddButtonForAlwaysAllowed();
			verifyAddedNumberDisplayedUnderAlwaysAllowed();
			verifyAndRemovingNewNumberUnderAlwaysAllowed();
			enterDataIntoAddNewUnderNeverAllowed(number1);
			clickAddButtonForNeverAllowed();
			verifyAddedNumberDisplayedUnderNeverAllowed();
			verifyAndRemovingNewNumberUnderNeverAllowed();
			verifyAndClickOnSaveButton();
			Reporter.log("Allowed numbers field validation successful");
		} catch (Exception e) {
			Assert.fail("Allowed numbers field validation error");
		}
		return this;
	}

	/**
	 * It will verify alert when phone status is off
	 * 
	 * @param existingPhoneStatus
	 * @return
	 */
	public FamilyAllowancesPage verifyAlertWhenPhoneToggleStatusOff(String expectedAlertMsg) {
		checkPageIsReady();
		try {
			String actualAlertMsg = alertLabelWhenPhoneStatusChangingFromOnToOff.getText() + " "
					+ alertMsgWhenPhoneStatusChangingFromOnToOff.getText();
			Assert.assertEquals(actualAlertMsg, expectedAlertMsg);
			Reporter.log("Alert Message is displayed with text : " + expectedAlertMsg);
		} catch (Exception e) {
			Assert.fail("Alert Message is not displayed with text : " + expectedAlertMsg);
		}
		return this;
	}

	/**
	 * Verify the alert message2 for Minutes with allowance When phone status is
	 * off is not displayed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage2WithAllowanceForMinutesIsNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(!minutesAlertMessage2WithAllowance.isDisplayed());
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Minutes is not displayed");
		} catch (Exception e) {
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Messages is not displayed");
		}
		return this;
	}

	/**
	 * Verify the alert message2 for Messages with allowance When phone status
	 * is off is not displayed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage2WithAllowanceForMessagesIsNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(!messagesAlertMessage2WithAllowance.isDisplayed());
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Messages is not displayed");
		} catch (Exception e) {
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Messages is not displayed");
		}
		return this;
	}

	/**
	 * Verify the alert message2 for Downloads with allowance When phone status
	 * is off is not displayed.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessage2WithAllowanceForDownloadsIsNotDisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(!downloadsAlertMessage2WithAllowance.isDisplayed());
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Downloads is not displayed");
		} catch (Exception e) {
			Reporter.log(
					"Successfully verified the Alert message '!Alert: You have reached or exceeded your total allowance set.' for Downloads is not displayed");
		}
		return this;
	}

	/**
	 * Verify the alert message for Minutes with No Allowance when Phone Status
	 * is off.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWithNoAllowanceForMinutesWhenPhoneStatusOff() {
		try {
			checkPageIsReady();
			Assert.assertTrue(alertMsgWithNoAllowanceForMinutesWhenPhoneStatusIsOff.isDisplayed());
			Reporter.log(
					"Successfully verified the Alert message '!Alert: This line has no allowance set, meaning that this line�s user can talk without restrictions.  This could result in overage charges on your bill.' for Messages is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Alert message for Minutes");
		}
		return this;
	}

	/**
	 * Verify the alert message for Messages with No Allowance when Phone Status
	 * is off.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWithNoAllowanceForMessagesWhenPhoneStatusOff() {
		try {
			checkPageIsReady();
			Assert.assertTrue(alertMsgWithNoAllowanceForMessagesWhenPhoneStatusIsOff.isDisplayed());
			Reporter.log(
					"Successfully verified the Alert message '!Alert: This line has no allowance set, meaning that this line�s user can message without restrictions. If you do not have an Unlimited Plan, this could result in overage charges on your bill.' for Messages is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Alert message for Messages");
		}
		return this;
	}

	/**
	 * Verify the alert message for Downloads with No Allowance when Phone
	 * Status is off.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertMessageWithNoAllowanceForDownloadsWhenPhoneStatusOff() {
		try {
			checkPageIsReady();
			Assert.assertTrue(alertMsgWithNoAllowanceForDownloadsWhenPhoneStatusIsOff.isDisplayed());
			Reporter.log(
					"Successfully verified the Alert message '!Alert: This line has no allowance set, meaning that this line�s user can download without restrictions.' for Downloads is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Alert message for Downloads");
		}
		return this;
	}

	/**
	 * This will validate the error message
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyErrormessageInAllowedNumbers() {
		try {
			waitFor(ExpectedConditions.visibilityOf(errorMessageInAllowedNumbers));
			Assert.assertTrue(isElementDisplayed(errorMessageInAllowedNumbers));
			Reporter.log("Error message number already exists in always allowed field is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to validate  error message in allowed numbers");
		}
		return this;
	}

	/**
	 * This will navigate to Dashboard page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage navigateToBookMarkedDashBoardURL() {
		try {
			checkPageIsReady();
			getDriver().navigate().to(System.getProperty("environment") + dashBoardBookMarkUrl);
		} catch (Exception e) {
			Assert.fail("unable To Navigate to dashboard page");
		}
		return this;
	}

	/**
	 * This will navigate to History of all lines page
	 * 
	 * @return
	 */
	public FamilyAllowancesPage navigateToBookMarkedHistoryOfAllLinesURL() {
		try {
			checkPageIsReady();
			getDriver().navigate().to(System.getProperty("environment") + historyOfAllLinesBookMarkUrl);
		} catch (Exception e) {
			Assert.fail("unable To Navigate to history of all lines page");
		}
		return this;
	}

	/**
	 * Verify Missdn number in family controls page.
	 */
	public FamilyAllowancesPage verifyMissdnNumberInFamilyControlsPage(String phoneNumber) {
		try {
			String familyControlsPageMissdn = missdninFamilyControlsPage.getAttribute("value");
			Assert.assertEquals(familyControlsPageMissdn, phoneNumber,
					"Linked phone number not matched with logged missdn");
			Reporter.log("Linked phone number matched with selected  missdn");
		} catch (Exception e) {
			Assert.fail("Linked phone number not matched with selected missdn");
		}
		return this;
	}

	/**
	 * Verify Missdn number in profile page.
	 */
	public FamilyAllowancesPage verifyMissdnNumberInProfilePage(String phoneNumber) {
		checkPageIsReady();
		try {
			String profilePageMissdn = missdninProfilePage.getText();
			Assert.assertEquals(profilePageMissdn, phoneNumber, "Linked phone number not matched with logged missdn");
			Reporter.log("Linked phone number matched with selected missdn");
		} catch (Exception e) {
			Assert.fail("Linked phone number not matched with selected missdn");
		}
		return this;
	}

	/**
	 * Verify Missdn number in family controls page.
	 */
	public FamilyAllowancesPage verifyMissdnNumberInFamilyAllowancesPage(String phoneNumber) {
		try {
			String familyAllowancesPageMissdn = missdninFamilyAllowancesPage.getAttribute("value");
			Assert.assertEquals(familyAllowancesPageMissdn, phoneNumber,
					"Linked phone number not matched with logged missdn");
			Reporter.log("Linked phone number matched with selected  missdn");
		} catch (Exception e) {
			Assert.fail("Linked phone number not matched with selected missdn");
		}
		return this;
	}

	/**
	 * It will verify if all the missdns webelement in view list of lines page
	 * contains 'PID' attribute or not
	 */
	public FamilyAllowancesPage verifyPIDAttributeinMissdnWebElementinViewListOfLinesPage() {
		checkPageIsReady();
		try {
			int counterForAttribute = 0;
			int counterForAttributeValue = 0;
			for (WebElement missdn : listOfMissdns) {
				Boolean flag = isAttribtuePresent(missdn, missdnAttribute);
				if (missdn.getAttribute("PID").contains("cust_msisdn")) {
					counterForAttributeValue++;
				}
				if (flag == true) {
					counterForAttribute++;
				}
			}
			Assert.assertEquals(counterForAttribute, listOfMissdns.size(), "not all the missdns contain PID Attribute");
			Reporter.log("All the missds in view list of lines page contains PID attribute");
			Assert.assertEquals(counterForAttributeValue, listOfMissdns.size(),
					"not all the missdns contain PID value as cust_msisdn");
			Reporter.log("All the missds in view list of lines page contains PID value as cust_msisdn");

		} catch (Exception e) {
			Assert.fail("unable to validate missdn attribute in view list of lines page");
		}
		return this;
	}

	/**
	 * It will check if the given element contains given attribute or not
	 * 
	 * @param element
	 * @param attribute
	 * @return
	 */
	private boolean isAttribtuePresent(WebElement element, String attribute) {
		Boolean result = false;
		try {
			String value = element.getAttribute(attribute);
			if (value != null) {
				result = true;
			}
		} catch (Exception e) {
		}

		return result;
	}

	/**
	 * Verify PID attribute value is in sorted order
	 */
	public FamilyAllowancesPage verifyPIDAttributeValueOrderInMissdnWebElement() {
		checkPageIsReady();
		ArrayList<String> PidAttributeValue = new ArrayList<String>();
		try {
			for (WebElement missdn : listOfMissdns) {
				PidAttributeValue.add(missdn.getAttribute("PID").substring(missdn.getAttribute("PID").length() - 1));
			}
			Assert.assertTrue(Ordering.natural().isOrdered(PidAttributeValue));
			Reporter.log("Value in the all  PID attribute of missdns are in sorted order");
		} catch (Exception e) {
			Assert.fail("unable to validate missdn attribute in view list of lines page");
		}
		return this;
	}

	/**
	 * Click on Downloads blade checkbox.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clickOnDownloadsBladeCheckBox() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(downloadsToggleCheckBox);
			Reporter.log("Click on downloads blade check box is success");
		} catch (Exception e) {
			Assert.fail("Click on downloads blade check box failed");
		}
		return this;
	}

	/**
	 * Verify alert word under downloads blade divison.
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyAlertWordinDownloads() {
		try {
			checkPageIsReady();
			Assert.assertTrue(downloadsDivAlertText.getText().contains("Alert"));
			Reporter.log("'Alert' word under downloads alert is displayed");
		} catch (AssertionError e) {
			Assert.fail("'Alert' word under downloads alert not displayed");
		}
		return this;
	}

	/***
	 * verify the messages no allowance checkbox not selected
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyMessagesNoAllowanceCheckBoxNotSelected() {
		try {
			checkPageIsReady();
			Assert.assertFalse(messagesNoAllowanceCheckboxStatus.isSelected());
			Reporter.log("Messages no allowance check box is not selected");
		} catch (Exception e) {
			Reporter.log("Messages no allowance check box is selected");
			Assert.fail("Messages no allowance check box is selected");
		}
		return this;
	}

	/***
	 * verify the messages updated allowance value
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyUpdatedAllowanceValue(String enteredAllowanceValue) {
		try {
			checkPageIsReady();
			Assert.assertEquals(messagesCurrentAllowanceTextField.getText().trim(), enteredAllowanceValue.trim());
			Reporter.log("Updated messages allowance value matched with entered allowance value");
		} catch (Exception e) {
			Reporter.log("Updated messages allowance value not matched with entered allowance value");
			Assert.fail("Updated messages allowance value not matched with entered allowance value");
		}
		return this;
	}

	/**
	 * Verify message below parent drop down
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyTextBelowParentDropDown() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			Assert.assertTrue(textBelowParentDropDown.isDisplayed());
			Reporter.log("validated the message below parent drop down");
		} catch (Exception e) {
			Assert.fail("failed to validate the message below parent drop down");
		}
		return this;
	}

	/**
	 * Verify grant users CTA
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage verifyGrantUsersCTA() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			Assert.assertTrue(grantUserAccessCTA.isDisplayed());
			Reporter.log("grant users CTA is displayed");
		} catch (Exception e) {
			Assert.fail("failed to validate grant users CTA");
		}
		return this;
	}

	/**
	 * Verify click grant users CTA
	 *
	 * @return the FamilyAllowances page class instance.
	 */
	public FamilyAllowancesPage clickGrantUsersCTA() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			clickElement(grantUserAccessCTA);
			verifyCurrentPageURL("lineSettingsPage", lineSettingsUrl);
		} catch (Exception e) {
			Assert.fail("failed to validate grant users CTA");
		}
		return this;
	}

	/**
	 * verify dollar symbol before the download limit in change history
	 * 
	 * @return
	 */
	public FamilyAllowancesPage verifyDollarSymbolinChangeHistory() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			String downloadlimitValuesTo[] = downloadLimit.getText().toString().split("to");
			Assert.assertEquals(downloadlimitValuesTo[1].trim().charAt(0), '$');
			String downloadlimitValuesFrom[] = downloadlimitValuesTo[0].split("from");
			Assert.assertEquals(downloadlimitValuesFrom[1].trim().charAt(0), '$');
			Reporter.log("Download limit contains $ symbol");
		} catch (Exception e) {
			Assert.fail("failed to validate grant users CTA");
		}
		return this;
	}
}
