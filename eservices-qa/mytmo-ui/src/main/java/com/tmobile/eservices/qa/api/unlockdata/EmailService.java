package com.tmobile.eservices.qa.api.unlockdata;

import javax.mail.Message;
import javax.mail.MessagingException;

/**
 * @author blakshminarayana
 *
 */
public interface EmailService {

	Message[] readEmail(String username, String password) throws MessagingException;

	boolean deleteEmails(String username, String password) throws MessagingException;
}
