
package com.tmobile.eservices.qa.pages.tmng.functional;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.ArrayList;
import java.util.List;

public class SearchResults extends TmngCommonPage {

    /**
     * @param webDriver
     */
    public SearchResults(WebDriver webDriver) {
        super(webDriver);
    }

    /**
     * Page objects for Search results page
     */
    @FindBy(css = "#mat-input-0")
    private WebElement inlineSearch;

    @FindBy(css = "#mat-autocomplete-0")
    private WebElement searchSuggestions;

    @FindBy(css = "div.mat-form-field-suffix > span>i")
    private WebElement inLineSearchCloseButton;

    @FindBy(css = "#mat-autocomplete-0 mat-option")
    private List<WebElement> searchSuggestionData;

    @FindBy(css = "div.mat-tab-label > div > span")
    private List<WebElement> categoryTabs;

    @FindBy(css = " #mat-tab-label-1-0 > div > span")
    private WebElement devices;

    @FindBy(css = " #mat-tab-label-1-1 > div > span")
    private WebElement  accessories;

    @FindBy(css = " #mat-tab-label-1-2 > div > span")
    private WebElement  support;

    @FindBy(css = " #mat-tab-label-1-3 > div > span")
    private WebElement  newsRoom;

    @FindBy(css = "div.item-category> span")
    private List<WebElement> categoryNameInResults;

    @FindBy(css = "div.ng-star-inserted.show > mat-progress-spinner")
    private WebElement spinner;

    @FindBy(css = "span[aria-label='filters']")
    private WebElement filter;

    @FindBy(css = "[aria-label='Device Type']")
    private WebElement deviceType;

    @FindBy(css = "#Device-Shape-Name-Mobile-Internet")
    private WebElement mobInternet;

    @FindBy(css = "#Device-Shape-Name-Smartphone > label > span")
    private WebElement smartphone;

    @FindBy(css = "#Device-Shape-Name-SIM-card-only > label > span")
    private WebElement simCardOnly;


    @FindBy(css = "[aria-label='Brand']")
    private WebElement brand;

    @FindBy(css = "[aria-label='Price Range']")
    private WebElement priceRange;

    @FindBy(css = "[aria-label='Pay Today']")
    private WebElement payToday;

    @FindBy(css = "[aria-label='Monthly Payment']")
    private WebElement monthlyPayment;

    @FindBy(css = "[aria-label='Memory']")
    private WebElement memory;

    @FindBy(css = "[aria-label='Accessory Type']")
    private WebElement accessoryType;

    @FindBy(css = "div.results > span")
    private WebElement resultsLabel;

    @FindBy(css = " button.sort-menu-button > span > div > span:nth-child(1)")
    private WebElement sortLabel;

    @FindBy(css = "button.sort-menu-button > span > div > mat-icon")
    private WebElement sortDropdown;

    @FindBy(css = "div > button.mat-menu-item > span")
    private List<WebElement> sortOption;

    @FindBy(css = "div > button.mat-menu-item >mat-icon")
    private List<WebElement> sortCheckMark;

    @FindBy(css = "div.mat-paginator-range-label")
    private WebElement pageRange;

    @FindBy(css = "button.mat-paginator-navigation-first")
    private WebElement paginatorFirst;

    @FindBy(css = "button.mat-paginator-navigation-previous")
    private WebElement paginatorPrevious;

    @FindBy(css = "button.mat-paginator-navigation-next")
    private WebElement paginatorNext;

    @FindBy(css = "button.mat-paginator-navigation-last")
    private WebElement paginatorLast;

    @FindBy(css = "i.material-icons.cursor")
    private WebElement inlineSearchClearButton;

    @FindBy(css = "button.clear-button")
    private WebElement filterClearButton;

    @FindBy(css = "mat-accordion.mat-accordion >div")
    private List<WebElement> newsRoomFilters;

    @FindBy(css = "#digital-header-utility-3 > span.mdi-magnify")
    private WebElement unavSearchIcon;

    @FindBy(css = "#digital-header-utility-2 > span.mdi-magnify")
    private WebElement unavSearchIconMyTmo;

    @FindBy(css = "div.navbar__search.is-active > form > input")
    private WebElement unavSearchText;

    @FindBy(css = "button.searchButton.mat-icon-button")
    private WebElement inlineSearchIcon;

    @FindBy(css = "button.searchButton.mat-icon-button")
    private WebElement myTmoSearch;

    @FindBy(css = "tmo-search-shop-content > section")
    private List<WebElement> deviceAndAccessoriesContent;

    @FindBy(css = "tmo-search-support-content> div")
    private List<WebElement> supportContent;

    @FindBy(css = "button.searchButton.mat-icon-button")
    private List<WebElement> newsroomContent;

    @FindBy(css = "div.text-legal.text-gray60.margin-bottom-xxs>span")
    private List<WebElement> deviceOrAccessoriesPrice;

    @FindBy(css="img[alt='shop image']")
    private List<WebElement>imageResultsDevicesNAccessories;

    @FindBy(css="div.shop-data>a>div:nth-child(2)>div:nth-child(2)")
    private List<WebElement>titleResultsDevicesNAccessories;

    @FindBy(css="div.shop-data>a>div:nth-child(2)>div:nth-child(3)")
    private List<WebElement>monthlyPayResultsDevicesNAccessories;

    @FindBy(css="div.shop-data>a>div:nth-child(2)>div:nth-child(4)")
    private List<WebElement>noOfMonthsResultsDevicesNAccessories;

    @FindBy(css="div.shop-data>a>div:nth-child(2)>div:nth-child(5)")
    private List<WebElement>fullPriceResultsDevicesNAccessories;

    int searchKeywordSize;

    /**
     * Method to validate inline search text box
     * @return SearchResults class
     */
    public SearchResults isInlineSearchDisplayed(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(inlineSearch));
//            isElementDisplayed(inlineSearch);
            Reporter.log("Inline search text field has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Inline search text field has not been displayed in search results page");
        }
        return this;
    }

    /**
     * Method to enter search keyword in inline search text box
     * @return SearchResults class
     */
    public SearchResults enterSearchKeyword(String searchKeyword){
        try{
            checkPageIsReady();
            searchKeywordSize = searchKeyword.length();
//            Assert.assertTrue(isElementDisplayed(inlineSearch));
//            isElementDisplayed(inlineSearch);
//            clickElement(inlineSearch);
            sendTextData(inlineSearch,searchKeyword);
//            Assert.assertTrue(isElementDisplayed(searchSuggestions));
            Reporter.log("Inline search text field has displayed in search results page and able to enter search keyword");
        }
        catch (Exception e){
            Assert.fail("unable to enter Inline search text.\n Actual error: "+e);
        }
        return this;
    }

    /**
     * Method to enter search keyword in inline search text box
     * @return SearchResults class
     */
    public SearchResults hitEnterSearchKeyword(){
        try{
            inlineSearch.sendKeys(Keys.ENTER);
            Reporter.log("Inline search text has inputted and hit hit enter");
        }
        catch (Exception e){
            Assert.fail("unable to hit enter after Inline search text input");
        }
        return this;
    }


    /**
     * Method to validate number of inline search suggestions displayed
     * @return SearchResults class
     */
    public SearchResults verifySearchSuggestionsCount(int count){
        try{
            checkPageIsReady();
//            Assert.assertTrue(isElementDisplayed(searchSuggestions),"Inline search suggestions has not been displayed in search results page");
//            Reporter.log("Inline search suggestions displayed in search results page");
            Assert.assertEquals(searchSuggestionData.size(),count,"search suggestions count is not matching with actual.");
        }
        catch (Exception e){
            Assert.fail("Inline search suggestions has not been displayed in search results page");
        }
        return this;
    }

    /**
     * Method to validate inline search suggestions
     * @return SearchResults class
     */
    public SearchResults verifySearchSuggestionsData(String searchKeyword){
        try{
            checkPageIsReady();
            for(int i=0;i<searchSuggestionData.size();i++){
                Assert.assertTrue(searchSuggestionData.get(i).getText().contains(searchKeyword));
            }
        }
        catch (Exception e){
            Assert.fail("Inline search suggestions has not have the search keyword: "+e);
        }
        return this;
    }

    /**
     * Method to validate inline search close button and click on X
     * @return SearchResults class
     */
    public SearchResults verifyInlineSearchCloseButton(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(inLineSearchCloseButton));
            Reporter.log("Inline search suggestions displayed in search results page");
            clickElement(inLineSearchCloseButton);
        }
        catch (Exception e){
            Assert.fail("Inline search suggestions has not been displayed in search results page and not able to click");
        }
        return this;
    }

    /**
     * Method to verify Device category TAB has displayed
     * @return SearchResults class
     */
    public SearchResults isDeviceTabDisplayed(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(0)));
            Reporter.log("Device category TAB has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Device category TAB has not been displayed in search results page");
        }
        return this;
    }
    /**
     * Method to click Device category TAB
     * @return SearchResults class
     */
    public SearchResults clickDeviceTab(){
        try{
            checkPageIsReady();
            clickElement(devices);
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(0)));
            Reporter.log("User is click on Device category TAB in search results page");
        }
        catch (Exception e){
            Assert.fail("User is unable to click on Device category TAB in search results page");
        }
        return this;
    }
    /**
     * Method to verify Device category TAB has displayed and in focus
     * @return SearchResults class
     */
    public SearchResults isDeviceTabActive(){
        try{
            checkPageIsReady();
            Assert.assertEquals(devices.getAttribute("ng-reflect-active"),"true");
            Reporter.log("Device category TAB has displayed and focus in search results page");
        }
        catch (Exception e){
            Assert.fail("Device category TAB has not in focus in search results page");
        }
        return this;
    }
    /**
     * Method to validate category name displayed in search results page
     * @return SearchResults class
     */
    public SearchResults categoryNameInSearchResults(String categoryName){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")),60);
            Assert.assertEquals(categoryNameInResults.get(0).getText(),categoryName);
            Reporter.log(categoryName+" category name has displayed in search results UI");
        }
        catch (Exception e){
            Assert.fail(categoryName+" category name has not displayed in search results UI \nActual Error:"+e);
        }
        return this;
    }
    /**
     * Method to verify Accessory TAB has displayed
     * @return SearchResults class
     */
    public SearchResults isAccessoryTabDisplayed(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(1)));
            Reporter.log("Accessory category TAB has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Accessory category TAB has not been displayed in search results page");
        }
        return this;
    }
    /**
     * Method to click Accessory category TAB
     * @return SearchResults class
     */
    public SearchResults clickAccessoryTab(){
        try{
            clickElement(categoryTabs.get(1));
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(1)));
            Reporter.log("Accessory category TAB has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Accessory category TAB has not been displayed in search results page");
        }
        return this;
    }
    /**
     * Method to verify Accessory TAB has displayed and active
     * @return SearchResults class
     */
    public SearchResults isAccessoryTabActive(){
        try{
            checkPageIsReady();
            Assert.assertEquals(accessories.getAttribute("ng-reflect-active"),"true");
            Reporter.log("Accessory category TAB is in focus in search results page");
        }
        catch (Exception e){
            Assert.fail("Accessory category TAB has not in focus in search results page");
        }
        return this;
    }
    /**
     * Method to verify Support TAB has displayed
     * @return SearchResults class
     */
    public SearchResults isSupportTabDisplayed(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(2)));
            Reporter.log("Support category TAB has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Support category TAB has not been displayed in search results page");
        }
        return this;
    }
    /**
     * Method to click Support category TAB
     * @return SearchResults class
     */
    public SearchResults clickSupportTab(){
        try{
            clickElement(categoryTabs.get(2));
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(2)));
            Reporter.log("Support category TAB has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Support category TAB has not been displayed in search results page. \n Actual Error:"+e);
        }
        return this;
    }
    /**
     * Method to verify Support TAB has displayed and active
     * @return SearchResults class
     */
    public SearchResults isSupportTabActive(){
        try{
            checkPageIsReady();
            Assert.assertEquals(support.getAttribute("ng-reflect-active"),"true");
            Reporter.log("Accessory category TAB is in focus in search results page");
        }
        catch (Exception e){
            Assert.fail("Support category TAB has not in focus in search results page");
        }
        return this;
    }

    /**
     * Method to verify Newsroom TAB has displayed
     * @return SearchResults class
     */
    public SearchResults isNewsRoomTabDisplayed(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(3)));
            Reporter.log("Newsroom category TAB has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Newsroom category TAB has not been displayed in search results page");
        }
        return this;
    }
    /**
     * Method to click Newsroom category TAB
     * @return SearchResults class
     */
    public SearchResults clickNewsRoomTab(){
        try{
            clickElement(categoryTabs.get(3));
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(categoryTabs.get(3)));
            Reporter.log("Newsroom category TAB has displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Newsroom category TAB has not been displayed in search results page");
        }
        return this;
    }

    /**
     * Method to verify filter categories under Device TAB
     * @return SearchResults class
     */
    public SearchResults verifyFilterCategoriesDeviceTab(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(filter));
            Assert.assertTrue(isElementDisplayed(deviceType));
            Assert.assertTrue(isElementDisplayed(brand));
            Assert.assertTrue(isElementDisplayed(priceRange));
            Assert.assertTrue(isElementDisplayed(payToday));
            Assert.assertTrue(isElementDisplayed(monthlyPayment));
            Assert.assertTrue(isElementDisplayed(memory));
            Reporter.log("All filter categories has listed under DEVICE Tab in search results page");
        }
        catch (Exception e){
            Assert.fail("All filter categories has not listed under DEVICE Tab in search results page");
        }
        return this;
    }
    /**
     * Method to verify filter categories under Accessories TAB
     * @return SearchResults class
     */
    public SearchResults verifyFilterCategoriesAccessoriesTab(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(filter));
            Assert.assertTrue(isElementDisplayed(accessoryType));
            Assert.assertTrue(isElementDisplayed(brand));
            Assert.assertTrue(isElementDisplayed(priceRange));
            Assert.assertTrue(isElementDisplayed(payToday));
            Assert.assertTrue(isElementDisplayed(monthlyPayment));
            Reporter.log("All filter categories has listed under accessories Tab in search results page");
        }
        catch (Exception e){
            Assert.fail("All filter categories has not listed under accessories Tab in search results page \nActual Error:"+e);
        }
        return this;
    }

    /**
     * Method to verify filter categories under Newsroom TAB
     * @return SearchResults class
     */
    public SearchResults verifyFilterCategoriesNewsRoomTab(){
        try{
            checkPageIsReady();
            for(int i=0;i<newsRoomFilters.size();i++){
                Assert.assertTrue(isElementDisplayed(newsRoomFilters.get(i)));
            }
            Reporter.log("All filter categories has listed under Newsroom Tab in search results page");
        }
        catch (Exception e){
            Assert.fail("All filter categories has not listed under Newsroom Tab in search results page");
        }
        return this;
    }
    /**
     * Method to verify number of search results after applying filters
     * @return SearchResults class
     */
    /*public SearchResults getCountFromFilterOption(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(monthlyPayment));
            Reporter.log("All filter categories has listed under accessories Tab in search results page");
        }
        catch (Exception e){
            Assert.fail("All filter categories has not listed under accessories Tab in search results page");
        }
        return this;
    }*/

    /**
     * Method to verify Results label under Device and Accessories tab
     * @return SearchResults class
     */
    public SearchResults validateResultsLabel(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")),60);
            Assert.assertTrue(isElementDisplayed(resultsLabel));
            Reporter.log("Results label has been displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Results label is not displayed in search results page");
        }
        return this;
    }

    /**
     * Method to verify Sort By label under Device and Accessories tab
     * @return SearchResults class
     */
    public SearchResults validateSortBy(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")),60);
            Assert.assertTrue(isElementDisplayed(sortLabel));
            Reporter.log("Sort By label has been displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Sort By label is not displayed in search results page");
        }
        return this;
    }
    /**
     * Method to verify Sort By options under Device tab
     * @return SearchResults class
     */
    public SearchResults validateSortByOptionsDeviceTab(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")),60);
            List<String> sortOpt = new ArrayList<>();
            sortOpt.add("Relevance");
            sortOpt.add("Price (Low to High)");
            sortOpt.add("Price (High to Low)");
            sortOpt.add("Customer Rating");
            sortOpt.add("Newest");
            clickElement(sortDropdown);
            for(int i=0;i<sortOpt.size();i++){
                Assert.assertTrue(sortOption.get(i).getText().contains(sortOpt.get(i)));
            }
            Reporter.log("All Sort By options has been displayed under device TAB in search results page");
        }
        catch (Exception e){
            Assert.fail("All Sort By options are not displayed under device tab in search results page");
        }
        return this;
    }

    /**
     * Method to verify Sort By options under Device tab
     * @return SearchResults class
     */
    public SearchResults selectSortOption(String sortType){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")),60);
            clickElement(sortDropdown);
            switch (sortType){
                case "LowToHigh":
                    clickElement(sortOption.get(1));
                    break;
                case "HighToLow":
                    clickElement(sortOption.get(2));
                    break;
            }
            Reporter.log("Sort Option "+sortType+"has been selected successfully in the dropdown");
        }
        catch (Exception e){
            Assert.fail("Sort Option "+sortType+"has not been selected successfully in the dropdown");
        }
        return this;
    }

    /**
     * Method to verify Sort By options under Accessories tab
     * @return SearchResults class
     */
    public SearchResults validateSortByOptionsAccessoryTab(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")));
            List<String> sortOpt = new ArrayList<>();
            sortOpt.add("Relevance");
            sortOpt.add("Price (Low to High)");
            sortOpt.add("Price (High to Low)");
            sortOpt.add("Customer Rating");
            clickElement(sortDropdown);
            for(int i=0;i<sortOpt.size();i++){
                Assert.assertTrue(sortOption.get(i).getText().contains(sortOpt.get(i)));
            }
            Reporter.log("All Sort By options has been displayed under Accessory TAB in search results page");
        }
        catch (Exception e){
            Assert.fail("All Sort By options are not displayed under Accessory tab in search results page");
        }
        return this;
    }



    /**
     * Method to verify Sort By options under Newsroom tab
     * @return SearchResults class
     */
    public SearchResults validateSortByOptionsNewsroomTab(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")));
            List<String> sortOpt = new ArrayList<>();
            sortOpt.add("Relevance");
            sortOpt.add("Newest");
            sortOpt.add("Oldest");
            clickElement(sortDropdown);
            for(int i=0;i<sortOpt.size();i++){
                Assert.assertTrue(sortOption.get(i).getText().contains(sortOpt.get(i)));
            }
            Reporter.log("All Sort By options has been displayed under Newsroom TAB in search results page");
        }
        catch (Exception e){
            Assert.fail("All Sort By options are not displayed under Newsroom tab in search results page");
        }
        return this;
    }

    /**
     * Method to verify Pagination links
     * @return SearchResults class
     */
    public SearchResults validatePaginationLinks(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")));
            Assert.assertTrue(isElementDisplayed(pageRange));
            Assert.assertTrue(isElementDisplayed(paginatorFirst));
            Assert.assertTrue(isElementDisplayed(paginatorPrevious));
            Assert.assertTrue(isElementDisplayed(paginatorNext));
            Assert.assertTrue(isElementDisplayed(paginatorLast));
            Reporter.log("Pagination links has been displayed in search results page");
        }
        catch (Exception e){
            Assert.fail("Pagination links are not displayed in search results page");
        }
        return this;
    }

    /**
     * Method to apply filter in device TAB
     * @return SearchResults class
     */
    public SearchResults applyFilter(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")));
            clickElement(deviceType);
            clickElement(mobInternet);
//            String filterNum = mobInternet.getText().replaceAll("[^0-9]", " ");
            Reporter.log("Applied filter under device tab successfully");
        }
        catch (Exception e){
            Assert.fail("there is some issue while applying filter under Device tab: \nActual Error: " + e);
        }
        return this;
    }

    /**
     * Method to apply filter in device TAB and verify the results UI
     * @return SearchResults class
     */
    public SearchResults validateFilterResultsCount(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")));
            String filterNum = mobInternet.getText().replaceAll("[^0-9]", "");
            // System.out.println("Filter count: "+filterNum);
            //System.out.println("results count: "+resultsLabel.getText());
            Assert.assertTrue(resultsLabel.getText().contains(filterNum));
            Reporter.log("Applied filter under device tab successfully and validated the results UI");
        }
        catch (Exception e){
            Assert.fail("Applied filter under Device Tab and found that mismatching between actual filter results count  and displayed search results count.");
        }
        return this;
    }

    /**
     * Method to validate clear filter button in device TAB
     * @return SearchResults class
     */
    public SearchResults verifyFilterClearButton(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")));
            Assert.assertTrue(isElementDisplayed(filterClearButton));
            Reporter.log("User is able to view clear button after applying filter");
        }
        catch (Exception e){
            Assert.fail("User is not able to view clear button in filter section");
        }
        return this;
    }

    /**
     * Method to click inline search clear button and validate
     * @return SearchResults class
     */
    public SearchResults clickInlineSearchClear(){
        try{
            checkPageIsReady();
            waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ng-star-inserted.show > mat-progress-spinner")));
            clickElement(inlineSearchClearButton);
            Assert.assertTrue(!isElementDisplayed(searchSuggestions));
            Reporter.log("User is able to click clear button");
        }
        catch (Exception e){
            Assert.fail("User is not able to click clear button in inline search section");
        }
        return this;
    }

    /**
     * Method to click inline search clear button and validate
     * CDCDWR-433 - OFD | Search | Inline Search - Clear auto-population query (ATDD)
     * @return SearchResults class
     */
    public SearchResults clearAutoPopulation(){
        try{
            checkPageIsReady();
            inlineSearch.click();
//            inlineSearch.sendKeys(Keys.CONTROL + "a");
            for(int i=0;i<searchKeywordSize;i++) {
                inlineSearch.sendKeys(Keys.BACK_SPACE);
            }

            Assert.assertTrue(!isElementDisplayed(searchSuggestions));
            Reporter.log("User is able to click clear button");
        }
        catch (Exception e){
            Assert.fail("User is not able to click clear button in filter section");
        }
        return this;
    }

    /**
     * Method to click Unav search and input search text
     *@return SearchResults class
     */
    public SearchResults unavSearch(String searKeyword){
        try{
            checkPageIsReady();
            clickElementWithJavaScript(unavSearchIcon);
//            unavSearchIcon.click();
            sendTextData(unavSearchText,searKeyword);
//            inlineSearch.sendKeys(Keys.CONTROL + "a");
            unavSearchText.sendKeys(Keys.ENTER);
            Reporter.log("User is able to click search button in input search keyword");
        }
        catch (Exception e){
            Assert.fail("User is not able to click search button in input search keyword/ \n Actual Error: "+e);
        }
        return this;
    }

    /**
     * Method to click Unav search in MyTmo and input search text
     *@return SearchResults class
     */
    public SearchResults unavSearchMyTmo(String searKeyword){
        try{
            checkPageIsReady();
            clickElementWithJavaScript(unavSearchIconMyTmo);
//            unavSearchIcon.click();
            sendTextData(unavSearchText,searKeyword);
//            inlineSearch.sendKeys(Keys.CONTROL + "a");
            unavSearchText.sendKeys(Keys.ENTER);
            Reporter.log("User is able to click search button in input search keyword");
        }
        catch (Exception e){
            Assert.fail("User is not able to click search button in input search keyword/ \n Actual Error: "+e);
        }
        return this;
    }

    /**
     * Method to interactive Search Icon
     *@return SearchResults class
     */
    public SearchResults interactiveSearchIcon(){
        try{
            checkPageIsReady();
            inlineSearch.clear();
            clickElement(inlineSearchIcon);
            Assert.assertTrue(isElementDisplayed(resultsLabel));

            Reporter.log("User is able to click inline search icon when no input entered and able to see results");
        }
        catch (Exception e){
            Assert.fail("User is unable to click inline search icon when no input entered and unable to see results/ \n Actual Error: "+e);
        }
        return this;
    }

    /**
     * Method to validate device or accessories category results count per page
     *@return SearchResults class
     */
    public SearchResults deviceNAccessoriesTabResultsCountPerPage(){
        try{
            checkPageIsReady();
            if(paginatorNext.getAttribute("disabled") == "false")
                Assert.assertTrue(deviceAndAccessoriesContent.size()==10,"Search results under"+categoryNameInResults.get(0).getText()+" category has displayed 10 results as per the requirement");
            else
                Assert.assertTrue(deviceAndAccessoriesContent.size()<=10,"Search results under"+categoryNameInResults.get(0).getText()+" category has displayed <= 10 results as per the requirement");
            Reporter.log("Search results under"+categoryNameInResults.get(0).getText()+" category has displayed as per the requirement");
        }
        catch (Exception e){
            Assert.fail("Search results under"+categoryNameInResults.get(0).getText()+" category has not displayed as per the requirement"+e);
        }
        return this;
    }

    /**
     * Method to validate support category results count per page
     *@return SearchResults class
     */
    public SearchResults supportTabResultsCountPerPage(){
        try{
            checkPageIsReady();
            if(paginatorNext.getAttribute("disabled") == "false")
                Assert.assertTrue(supportContent.size()==10,"Search results under"+categoryNameInResults.get(0).getText()+" category has displayed 10 results as per the requirement");
            else
                Assert.assertTrue(supportContent.size()<=10,"Search results under"+categoryNameInResults.get(0).getText()+" category has displayed <= 10 results as per the requirement");
            Reporter.log("Search results under"+categoryNameInResults.get(0).getText()+" category has displayed as per the requirement");
        }
        catch (Exception e){
            Assert.fail("Search results under"+categoryNameInResults.get(0).getText()+" category has not displayed as per the requirement"+e);
        }
        return this;
    }

    /**
     * Method to validate newsroom category results count per page
     *@return SearchResults class
     */
    public SearchResults newsroomTabResultsCountPerPage(){
        try{
            checkPageIsReady();
            if(paginatorNext.getAttribute("disabled") == "false")
                Assert.assertTrue(newsroomContent.size()==10,"Search results under"+categoryNameInResults.get(0).getText()+" category has displayed 10 results as per the requirement");
            else
                Assert.assertTrue(newsroomContent.size()<=10,"Search results under"+categoryNameInResults.get(0).getText()+" category has displayed <= 10 results as per the requirement");
            Reporter.log("Search results under"+categoryNameInResults.get(0).getText()+" category has displayed as per the requirement");
        }
        catch (Exception e){
            Assert.fail("Search results under"+categoryNameInResults.get(0).getText()+" category has not displayed as per the requirement"+e);
        }
        return this;
    }

    /**
     * Method to validate Mandatory labels in search results under Device/Accessories Categories
     *@return SearchResults class
     */
    public SearchResults validateResultsTemplateUnderDevicesAccessories(){
        try{
            checkPageIsReady();
            Assert.assertTrue(isElementDisplayed(imageResultsDevicesNAccessories.get(0)));
            Assert.assertTrue(isElementDisplayed(titleResultsDevicesNAccessories.get(0)));
            Assert.assertTrue(isElementDisplayed(monthlyPayResultsDevicesNAccessories.get(0)));
            Assert.assertTrue(isElementDisplayed(noOfMonthsResultsDevicesNAccessories.get(0)));
            Assert.assertTrue(isElementDisplayed(fullPriceResultsDevicesNAccessories.get(0)));
            Reporter.log("Mandatory labels are getting displayed in search results under Device/Accessories Category ");
        }
        catch (Exception e){
            Assert.fail("Mandatory labels are not getting displayed in search results under Device/Accessories Category \nActual Error: "+e);
        }
        return this;
    }

    /**
     * Method to validate sort functionality when user selects "Low to High" or "High to Low"
     *@return SearchResults class
     */
    public SearchResults validateSortFunctionality(String sortType){
        checkPageIsReady();
        ArrayList<Float> priceList = new ArrayList<Float>();
        try{
            for(int i=0; i<deviceAndAccessoriesContent.size();i++){
                String priceStr = deviceOrAccessoriesPrice.get(i).getText().replace("Full price: $","");
                Float priceFloat = Float.parseFloat(priceStr);
                priceList.add(priceFloat);
            }
            switch (sortType){
                case "LowToHigh":
                    Assert.assertTrue(sortLowtoHigh(priceList),"Search results has been sorted with prices from Low to High");
                    break;
                case "HighToLow":
                    Assert.assertTrue(sortHighToLow(priceList),"Search results has been sorted with prices from High to Low");
                    break;
            }
            Reporter.log("Search results has been sorted from "+sortType);
        }
        catch (Exception e){
            Assert.fail("Search results has not been sorted from "+sortType+"\nActual Error: "+e);
        }
        return this;
    }

    public Boolean sortLowtoHigh(ArrayList<Float> data){
        for(int i=0;i< data.size()-1;i++){
            if(data.get(i) > data.get(i+1)){
                return false;
            }
        }
        return true;
    }

    public Boolean sortHighToLow(ArrayList<Float> data){
        for(int i=0;i< data.size()-1;i++){
            if(data.get(i) < data.get(i+1)){
                return false;
            }
        }
        return true;
    }

    /**
     * Method to click PDP links
     *@return SearchResults class
     */
    public SearchResults clickPdpLink(){
        try{
            checkPageIsReady();
            clickElement(titleResultsDevicesNAccessories.get(0));
            Reporter.log("User is able to click on PDP link");
        }
        catch (Exception e){
            Assert.fail("User is not able to click PDP link. \nActual Error: "+e);
        }
        return this;
    }

    /**
     * Method to validate PDP links
     *@return SearchResults class
     */
    public SearchResults validatePdpLink(String AppType, String PDPLink){
        try{
            /*checkPageIsReady();
            if(AppType == "TMO"){
                Assert.assertTrue(getDriver().getCurrentUrl().startsWith(PDPLink));
                Reporter.log("User is able to click PDP link from "+AppType+" and successfully navigated to TMO PDP page");
            }
            else if(AppType == "MyTMO"){
                Assert.assertTrue(getDriver().getCurrentUrl().startsWith(PDPLink));
                Reporter.log("User is able to click PDP link from "+AppType+" and successfully navigated to MyMO PDP page");
            }*/
            Assert.assertTrue(getDriver().getCurrentUrl().startsWith(PDPLink));
            Reporter.log("User is able to click PDP link from "+AppType+" and successfully navigated to "+AppType+"PDP page");
        }
        catch (Exception e){
            Assert.fail("User is able to click PDP link from "+AppType+" and user is not navigated to "+AppType+" PDP page \nActual Error: "+e);
        }
        return this;
    }

    /**
     * Method to click on browser back button
     *@return SearchResults class
     */
    public SearchResults clickBrowserbackButton(){
        try{
            checkPageIsReady();
            getDriver().navigate().back();
//            getDriver().navigate().back();
            Reporter.log("User clicks on browser back button");
        }
        catch (Exception e){
            Assert.fail("User is not able to click on back button  \nActual Error: "+e);
        }
        return this;
    }

}
