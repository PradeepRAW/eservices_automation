package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class IAMProfileV2 extends EOSCommonLib {

	public Response getResponseIAMProfileV2(String alist[]) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			RestService restService = new RestService("", "https://iam.msg.t-mobile.com");
			// RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");

			restService.addHeader("Authorization", "Bearer " + alist[1]);

			response = restService.callService("consumerProfile/v2/userinfo", RestCallType.GET);

		}

		return response;
	}

	public Map<Object, Object> getvaluesforEOSPAALP(Response response, String misdin) {
		Map<Object, Object> vals = new HashMap<Object, Object>();
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			/*
			 * String custtype=jsonPathEvaluator.get(
			 * "extendedLines[0].contracts.findAll{it.msisdn=='" + misdin +
			 * "'}[0].multiLineCustomerType").toString();
			 */
			String sn = jsonPathEvaluator.get("extendedLines[0].contracts.findAll{it.msisdn=='" + misdin + "'}[0].sn");
			String lastname = jsonPathEvaluator
					.get("extendedLines[0].contracts.findAll{it.msisdn=='" + misdin + "'}[0].billingLastname");

			String firstname = jsonPathEvaluator
					.get("extendedLines[0].contracts.findAll{it.msisdn=='" + misdin + "'}[0].billingFirstname");
			String nickname = jsonPathEvaluator
					.get("extendedLines[0].contracts.findAll{it.msisdn=='" + misdin + "'}[0].nickname");
			String lname = sn.isEmpty() ? lastname : sn;
			String fname = nickname.isEmpty() ? firstname : nickname;

			vals.put("name", fname.trim() + " " + lname.trim());
		}

		return vals;
	}

}
