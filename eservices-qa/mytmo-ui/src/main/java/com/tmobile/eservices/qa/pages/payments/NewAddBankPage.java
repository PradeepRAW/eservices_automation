package com.tmobile.eservices.qa.pages.payments;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class NewAddBankPage extends CommonPage {

	private Long accNumber = 0L;
	private int count = 0;

	@FindBy(id = "accountName")
	private WebElement accountName;

	@FindBy(id = "routingNum")
	private WebElement routingNum;

	@FindBy(id = "accountNum")
	private WebElement accountNum;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	private WebElement btnContinue;

	@FindBy(xpath = "//button[contains(text(),'Back')]")
	private WebElement btnBack;

	@FindBy(xpath = "//p[text()='Bank information']")
	private WebElement headerBankinfo;
	
	@FindBy(xpath="//input[@formcontrolname='saveBankToggle']")
	private WebElement chkboxsavepaymentmethod;	
	
	@FindBy(xpath = "//div[contains(@class,'dialog animation')]//button[contains(text(),'Continue')]")
	private WebElement dialogcontinue;	

	@FindBy(xpath="//label[@for='accountName']")
	private WebElement placeholderNameonAccount;	

	@FindBy(xpath="//label[@for='routingNum']")
	private WebElement placeholderRoutingnumber;	
	
	@FindBy(xpath="//label[@for='accountNum']")
	private WebElement placeholderAccountNumber;	
	
	@FindBy(id="routingHelpLink")
	private WebElement linkHowtofindroutandaccnumber;	
	
	@FindBy(xpath="//span[contains(@class,'padding-horizontal-xsmall body')]")
	private List<WebElement> txtalreadyhavepaymentmethod;	
	
	@FindBy(xpath="//span[contains(text(),'Save this payment method')]")
	private List<WebElement> txtsavethispaymentmethod;	
	
	@FindBy(css="span.tooltip-save")
	private List<WebElement> tooltipSaveaccountOptional;
	
	@FindBy(id="closeModal")
	private WebElement accnoroutnopopup;
	
	@FindBy(xpath="//p[contains(@class,'errormessage')]")
	private List<WebElement> errormessages;
	
	
	
	
	private final String pageUrl = "payments/addbank";

	/**
	 * Constructor
	 *
	 * @param webDriver
	 */
	public NewAddBankPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewAddBankPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewAddBankPage verifyBankPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			// waitFor(ExpectedConditions.visibilityOf(paymentProcessing));
			waitFor(ExpectedConditions.elementToBeClickable(btnBack));
			headerBankinfo.isDisplayed();
			Reporter.log("Bank page is loaded and header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page is  not loaded and Header is failed to verified");

		}
		return this;
	}

	
	
	/**
	 * Verify bank page header is displayed or not.
	 * 
	 */
	public NewAddBankPage verifyBankPageHeader() {
		try {

			headerBankinfo.isDisplayed();
			Assert.assertTrue(headerBankinfo.getText().equals("Bank information"));
			Reporter.log("Bank page header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page header is failed to verified");
		}
		return this;
	}

	public NewAddBankPage EnterNameonAccount(String nameOnAccount) {
		try {
			accountName.clear();
			accountName.sendKeys(nameOnAccount);
			Reporter.log("Name on account entered");
		} catch (Exception e) {
			Assert.fail("Account name is not located");
		}
		return this;
	}

	public NewAddBankPage EnterRoutingNumber(String routingNumber) {
		try {
			routingNum.clear();
			routingNum.sendKeys(routingNumber);
			Reporter.log("routingNumber entered");
		} catch (Exception e) {
			Assert.fail("routingNumber is not located");
		}
		return this;
	}

	public NewAddBankPage EnteraccountNumber(String accountnum) {
		try {
			accountNum.clear();
			accountNum.sendKeys(accountnum);
			Reporter.log("Account number entered");
		} catch (Exception e) {
			Assert.fail("Account number is not located");
		}
		return this;
	}

	public NewAddBankPage clickContinueCTA() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
			Thread.sleep(1000);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.circle")));
			Thread.sleep(1000);
			waitFor(ExpectedConditions.elementToBeClickable(btnContinue));
			// Thread.sleep(1000);
			btnContinue.click();
			Reporter.log("Clicked on Continue button");

		} catch (Exception e) {
			Assert.fail("Failed to click continue button");
		}
		return this;
	}

	public NewAddBankPage addNewBankDetails(Map<String, String> getBankinfo) {
		EnterNameonAccount(getBankinfo.get("nameonbank"));
		EnterRoutingNumber(getBankinfo.get("routingnumber"));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
		EnteraccountNumber(getBankinfo.get("bankaccountnumber"));
		return this;
	}
	
	
	

	public boolean ispaymentmethodChecked() {
		
		if(chkboxsavepaymentmethod.getAttribute("checked")!=null) return true;
		else return false;
	
	}
	
	public NewAddBankPage savepaymentmethodOFF() {
		try {
		if(ispaymentmethodChecked())chkboxsavepaymentmethod.click();
		Reporter.log("Save payment method un selected");
		
		}
		catch(Exception e) {
			Assert.fail("Check box is not located");
		}
		return this;
	
}
	
	public NewAddBankPage savepaymentmethodON() {
		try {
		if(!ispaymentmethodChecked())chkboxsavepaymentmethod.click();
		Reporter.log("Save payment method  selected");
		
		}
		catch(Exception e) {
			Assert.fail("Check box is not located");
		}
		return this;
	}
	

	public NewAddBankPage ClickContinueinDialog() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
			Thread.sleep(1000);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.circle")));
			Thread.sleep(1000);
			waitFor(ExpectedConditions.elementToBeClickable(dialogcontinue));
			dialogcontinue.click();
		Reporter.log("Continue button is clicked");
		
		}
		catch(Exception e) {
			Assert.fail("Continue button is not located");
		}
		return this;
	
}
	
	
	
	public NewAddBankPage addbankwithsaveoption(Map<String, String> getBankinfo) {
		addNewBankDetails(getBankinfo);
		savepaymentmethodON();
		clickContinueCTA();
		ClickContinueinDialog();
		return this;
		
	}
	
	
	public NewAddBankPage CheckNameonAccountPalceholder() {
		try {
			if(placeholderNameonAccount.getText().trim().equalsIgnoreCase("Name on Account")) Reporter.log("name on account is displayed");
			else Verify.fail("Name on account place holder is not Name on Account");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderNameonAccount is not located");
			}
			return this;
	}
	
	
	public NewAddBankPage CheckroutingNumberPalceholder() {
		try {
			if(placeholderRoutingnumber.getText().trim().equalsIgnoreCase("Routing Number")) Reporter.log("Routing Number is displayed");
			else Verify.fail("Routing Number place holder is not Name on Account");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderRoutingnumber is not located");
			}
			return this;
	}
	
	
	public NewAddBankPage CheckaccountNumberPalceholder() {
		try {
			if(placeholderAccountNumber.getText().trim().equalsIgnoreCase("Account Number")) Reporter.log("Account Number is displayed");
			else Verify.fail("Account Number place holder is not Name on Account");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderAccountNumber is not located");
			}
			return this;
	}
	
	
	
	
	
	public NewAddBankPage CheckHowtofindaccountnumber() {
		try {
			linkHowtofindroutandaccnumber.click(); 
			Reporter.log("How to find account number link is Clicked");
			
			
			}
			catch(Exception e) {
				Assert.fail("placeholderAccountNumber is not located");
			}
			return this;
	}
	
	
	public NewAddBankPage Clickbackbutton() {
		try {
			btnBack.click(); 
			Reporter.log("Back button is Clicked");
			
			
			}
			catch(Exception e) {
				Assert.fail("Back button is not located");
			}
			return this;
	}
	
	
	
	public NewAddBankPage CheckaccnoroutnopopupDsiplayed() {
		try {
			if(accnoroutnopopup.isDisplayed())Reporter.log("How to find account number pop up is displayed");
			else Verify.fail("How to find account number pop up is not displayed");
			
			
			}
			catch(Exception e) {
				Assert.fail("accnoroutnopopup is not located");
			}
			return this;
	}
	
	public NewAddBankPage Checksavepaymentmethodexist() {
		try {
			if(chkboxsavepaymentmethod.isDisplayed())Reporter.log("save payment method checkbox is displayed");
			else Verify.fail("save payment method checkbox is not displayed");
			
			
			}
			catch(Exception e) {
				Assert.fail("chkboxsavepaymentmethod is not located");
			}
			return this;
	}
	
	public NewAddBankPage CheckSavePaymentmethodtext(boolean val) {
		try {
			
			boolean res=txtsavethispaymentmethod.size()>0?true:false;
			if(val==res) Reporter.log("Save check box text is "+res);
			else Verify.fail("Save check box text expected is:"+val+" actual is:"+res);
			
			}
			catch(Exception e) {
				Assert.fail("txtsavethispaymentmethod is not located");
			}
			return this;
	}
	
	
	public NewAddBankPage CheckAlreadyhavepaymentmethodtext(boolean val) {
		try {
			
			boolean res=txtalreadyhavepaymentmethod.size()>0?true:false;
			if(val==res) Reporter.log("already have paymentmethod text is "+res);
			else Verify.fail("already have paymentmethod text expected is:"+val+" actual is:"+res);
			
			}
			catch(Exception e) {
				Assert.fail("txtalreadyhavepaymentmethod is not located");
			}
			return this;
	}
	
	public NewAddBankPage Checktooltipforsaveoptional(boolean val) {
		try {
			
			boolean res=tooltipSaveaccountOptional.size()>0?true:false;
			if(val==res) Reporter.log("tooltip for save account checkbox "+res);
			else Verify.fail("tooltip for save account checkbox expected is:"+val+" actual is:"+res);
			
			}
			catch(Exception e) {
				Assert.fail("txtalreadyhavepaymentmethod is not located");
			}
			return this;
	}
	
	
	
	
	public boolean verifyErrorTextMessage(String message) {
		checkPageIsReady();
		boolean display = false;
		try {
			for (WebElement error : errormessages) {
				if (error.isDisplayed() && error.getText().contains(message)) {
					display = true;
				}
			}
		} catch (Exception e) {

		}
		return display;
	}
	
	
	
	public NewAddBankPage checkmaximumcharbyfield(WebElement ele, int stringlength) {
		try {
			if (ele.getText().length() > stringlength)
				Verify.fail("Name on account is accepting more than " + stringlength + " characters");
		} catch (Exception e) {
			Assert.fail("Account Name field not found");
		}
		return this;
	}

	/**
	 * validate all bank page elements
	 */
	public NewAddBankPage validateAddBankAllFields(String addBankHeader) {
		try {
			// continueAddBank.click();
			/*
			 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid name."),
			 * "Blank Account name can be accepted");
			 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid routing number."),
			 * "Blank Routing Number can be accepted");
			 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid account number."),
			 * "Blank Account Number can be accepted");
			 */
			EnterNameonAccount("T");
			Thread.sleep(1000);
		
			Verify.assertTrue(verifyErrorTextMessage("Enter a valid name"),
					"Account name with one character can be accepted");
			

			EnterNameonAccount("characters limit for account name fieldis");
			Thread.sleep(1000);
			checkmaximumcharbyfield(accountName, 40);

			EnterRoutingNumber("99999999");
			Thread.sleep(1000);
			
			Verify.assertTrue(verifyErrorTextMessage("Enter a valid routing number"),
					"Routing Number with less then 9 characters can not be accepted");
			EnterRoutingNumber("125008547");
			Thread.sleep(1000);
			
			Verify.assertFalse(verifyErrorTextMessage("Enter a valid routing number."),
					"Routing Number with more then 9 characters can be accepted");
			EnteraccountNumber("55555");
			Thread.sleep(1000);
			
			Verify.assertTrue(verifyErrorTextMessage("Enter a valid account number"),
					"Account Number with less then 6 characters can not be accepted");
			EnteraccountNumber("999999999999999999");
			Thread.sleep(1000);
			
			checkmaximumcharbyfield(accountNum, 17);
		} catch (Exception e) {
			Verify.fail("Failed to verify all the bank validations");
		}
		return this;
	}

	
	
}
