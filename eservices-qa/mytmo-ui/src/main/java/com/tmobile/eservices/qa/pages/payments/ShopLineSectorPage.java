package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author speddis
 *
 */
public class ShopLineSectorPage extends CommonPage {

	@FindBy(css = "div.main-content p.Display2")
	private WebElement pageHeader;

	private final String pageUrl = "/lineselector";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public ShopLineSectorPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ShopLineSectorPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public ShopLineSectorPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("'backToShop' Transition page not found.");
		}
		return this;
	}

}
