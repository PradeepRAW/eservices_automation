package com.tmobile.eservices.qa.pages.payments;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class OneTimePaymentPage extends CommonPage {

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	// @FindBy(xpath = "//h4[contains(text(),'Pay your bill')]")

	@FindBy(css = "span.arrow-right")
	private WebElement arrowRight;

	@FindBy(css = "span[pid='cust_crd0']")
	private WebElement addPaymentMethod;

	@FindBy(css = "h4.h3-title.h4-title-mobile.text-black")
	private WebElement otpPageHeader;

	@FindAll({ @FindBy(css = "div[ng-click*='Amount']"), @FindBy(xpath = "//span[contains(text(),'Amount')]") })
	private WebElement amountIcon;

	@FindBy(css = "div[ng-click*='Amount'] p.inline")
	private WebElement amountOnBlade;

	@FindAll({ @FindBy(css = "div>Span[aria-label='change payment method active clickable blade']"),
			@FindBy(id = "paymentLable"), @FindBy(css = "div#addPayment div.body-copy-description-bold"),
			@FindBy(css = "span#paymentDefaultText") })
	private WebElement paymentMethodBlade;

	@FindBy(css = "span#paymentDefaultText")
	private WebElement noPaymentMethodOnBlade;

	@FindAll({ @FindBy(css = "span.padding-horizontal-xsmall.black"), @FindBy(css = "span[pid='cust_crd0']") })
	private WebElement paymentMethodOnBlade;

	@FindBy(css = "button.btn.btn-primary.PrimaryCTA")
	private WebElement agreeAndSubmitbtn;

	// @FindBy(id = "agreesubmit")
	@FindBy(css = "p.agree.ng-binding")
	private WebElement submitBtnmobile;

	@FindBy(xpath = "//span[contains(@class,'icon-echeckAccount')]/following-sibling::p")
	private WebElement storedPaymentCheckingNumber;

	@FindBy(xpath = "//span[contains(@class,'Card')]/following-sibling::p")
	private WebElement storedCardNumber;

	@FindBys(@FindBy(id = "continueOverPayment_id"))
	private List<WebElement> continueOverPaymentYes;

	@FindBy(css = "button.PrimaryCTA.glueButton")
	private WebElement continueBtnModal;

	@FindBy(css = "button.SecondaryCTA.glueButton")
	private WebElement cancelBtn;

	@FindBy(css = "#exitOTP")
	private WebElement continueExitOTPBtn;

	@FindBy(css = "#cancelExitOTP")
	private WebElement backExitOTPBtn;

	@FindBy(css = "div.fine-print-body.text-black")
	private List<WebElement> alertTexts;

	@FindBy(css = "i.fa-warning")
	private WebElement warningLogo;

	@FindBy(xpath = "//div[contains(text(),'Using my')]")
	private WebElement usngMyIcon;

	/*
	 * @FindBy(id = "PAAccept") private WebElement pAdialogBoxYesMobile;
	 */
	@FindBy(xpath = "//div[@id='PAAccept']/button")
	private WebElement pAdialogBoxYesMobile;

	@FindBy(css = "#PAAccept button")
	private WebElement paymentArrangementYesBtn;

	@FindBy(xpath = "//*[@id='parent_alert']/div[2]")
	private WebElement scheduledPaymentalerts;

	@FindBy(xpath = "//p[contains(@ng-bind-html,'ban')]")
	private WebElement banMessage;

	@FindBy(css = "#oKBanned")
	private WebElement okButton;

	@FindBy(css = "p.inline.text-error")
	private List<WebElement> inlineErrorMsgs;

	@FindBy(css = "p.inline.fine-print-body")
	private WebElement inlineErrorMsg;

	@FindBy(css = "div.row.no-margin.p-b-20 div p")
	private WebElement cancelMsg;

	// @FindBy(css =
	// "button.btn.btn-secondary.button-title.SecondaryCTA.width-md.ng-binding")
	@FindAll({ @FindBy(css = "button.btn.btn-secondary.button-title.SecondaryCTA.width-md.ng-binding"),
			@FindBy(css = "div#back button") })
	private WebElement backBtn;

	@FindAll({ @FindBy(xpath = "//h4[contains(text(),'Select Payment Method')]"), @FindBy(css = "div.Display3") })
	private WebElement paymentHeader;

	@FindBy(css = "div.padding-top-small")
	private WebElement overridePayment;

	// @FindBy(xpath = "//div[@class='col-12
	// padding-vertical-small']/span[contains(text(),'Date')]")
	@FindBy(xpath = "//div[contains(text(),'Date')]")
	private WebElement dateBlade;

	@FindBy(xpath = "//div[@id='dateLabel']/..//.//i")
	private WebElement dateChevron;

	@FindBy(xpath = "//div[contains(text(),'Amount')]")
	private WebElement amountBlade;

	@FindBy(css = "otp-payment-date p.button-title")
	private WebElement datePageHeader;

	@FindBy(css = "i[class='fa fa-circle'] span.text-black.p-l-5.p-b-5")
	private WebElement selectedDate;

	@FindBy(css = "i[class='fa fa-circle-o p-l-15 ng-scope'] span.text-black.p-l-5.p-b-5")
	private WebElement dueDate;

	@FindBy(css = ".row.overlay-text-height div p")
	private WebElement immediatePaymentsMsg;

	@FindBy(css = "div.body-copy-description-regular")
	private WebElement otpPageModalMessage;

	@FindBy(css = "div.body-copy-description-regular span+span")
	private WebElement otpPageModalMasking;

	@FindBy(css = ".recommendedDIV .xsmall.pull-right")
	private List<WebElement> viewDetailsLink;

	@FindBy(css = "div.row div.col-12.col-md-10 p")
	private List<WebElement> otpConfirmationAlerts;

	@FindBy(xpath = "//span[contains(text(),'Show details')]")
	private WebElement showDetails;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement agreeAndSubmitButton;

	@FindBy(linkText = "review previous payments.")
	private WebElement reviewPreviouspayment;

	@FindBy(xpath = "//span[contains(text(),'Total')]/following-sibling::span")
	private WebElement balanceDueAmount;

	@FindBy(css = "div.col-12.col-md-10 p")
	private WebElement avoidAccountSuspensionAlert;

	@FindBy(css = "otp-payment-date button.btn-secondary")
	private WebElement dateBackBtn;

	@FindBy(css = "div#payyourbilladdpay div p.body-bold")
	private WebElement totalInHeader;

	@FindAll({ @FindBy(css = "h4.h3-title"), @FindBy(css = "div span.Display3") })
	private WebElement payYourBillHeader;

	@FindAll({ @FindBy(css = "div.modal-white-background.otpModule.ng-scope"),
			@FindBy(css = "div.dialog.animation-out.modal_landscapeht.animation-in") })
	private WebElement otpPageModals;

	@FindBy(css = "#goToPA button")
	private WebElement reviewPAbtnOnModal;

	@FindBy(css = "#cancelOverpayPAModal button")
	private WebElement continueWithPaymentBtnOnModal;

	@FindAll({ @FindBy(css = "#cancelExitOTP button"), @FindBy(css = "button.SecondaryCTA-accent.blackCTA") })
	private WebElement backBtnCancelModal;

	@FindAll({ @FindBy(css = "#exitOTP button"), @FindBy(css = "button.PrimaryCTA-accent") })
	private WebElement continueBtnCancelModal;

	@FindAll({ @FindBy(css = "span.fine-print-body.text-black"),
			@FindBy(css = "p.col-12.col-md-10.offset-md-1.pull-left.body") })
	private List<WebElement> paymentArrangementText;

	@FindBy(css = "span.credit_card_Bank")
	private WebElement savedpaymentType;

	@FindBy(css = "div.margin-left-errormsg")
	private WebElement errorMsgOtherAmount;

	@FindBy(css = "div.fine-print-body.text-black.ng-binding.ng-scope")
	private WebElement alertMsgSedona;

	@FindBy(css = "div.col-12.col-md-10 app-dynamic-component")
	private WebElement autopayAlertNonSuspended;

	@FindAll({ @FindBy(xpath = "//span[contains(text(),'Amount')]/following-sibling::span/span"),
			@FindBy(xpath = "//div[contains(text(),'Amount')]/following-sibling::div/p") })
	private WebElement amountBladeText;

	@FindBy(css = "#otp-paAlerts div p")
	private WebElement warningtextDueAmount;

	@FindBy(css = "#otp-paAlerts div div.exclamation-circle")
	private WebElement highSeverityWarningLogo;

	@FindBy(css = "a[ng-click='vm.goToTnC()']")
	private WebElement termsAndConditionsLink;

	@FindBy(css = "div[class*='otpModule'] h4")
	private WebElement termsandConditionsHeadline;

	@FindBy(css = "terms-and-conditions button")
	private WebElement backTermsandConditionsButton;

	@FindAll({ @FindBy(xpath = "//span[contains(@class,'sprite')]/following-sibling::p"),
			@FindBy(css = "span[pid='cust_crd0']") })
	private WebElement storedPaymentCardNumber;

	@FindBy(css = "p.body-copy-description-regular.black")
	private WebElement storedPaymentNo;

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindBy(css = "div#addPayment div.body-copy-description-bold")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//p[text()='You must add a payment method']")
	private WebElement warningAddApyment;

	@FindBy(css = "div.fine-print-body.glueHyperLink >p")
	private WebElement paSecuredMessageNotification;

	@FindBy(css = "div.fine-print-body.glueHyperLink >p>a")
	private WebElement viewPaymentScheduleLink;

	@FindBy(css = "div.fine-print-body.glueHyperLink >a")
	private WebElement reviewPayemntsMadeLink;

	@FindBy(css = "[ng-if*='Pa'] div.fine-print-body.glueHyperLink")
	private WebElement paymentArrangementAlert;

	@FindBy(css = "p.black")
	private WebElement accountFourDigitonPaymentBlade;

	private final String pageUrl = "/onetimepayment";

	private final String newPageUrl = "/paymentblade";

	@FindBy(css = "div.dialogPaddingLCModal")
	private WebElement paInterruptModal;

	@FindBy(css = "div.dialogPaddingLCModal div.text-right span")
	private WebElement paInterruptModalAmount;

	@FindBy(css = "div.dialogPaddingLCModal h4.h4-title")
	private WebElement paInterruptModalHeader;

	@FindBy(css = "div.dialogPaddingLCModal span.body-copy-description")
	private WebElement paInterruptModalText;

	@FindBy(css = "div.dialogPaddingLCModal p.body-copy-description-bold")
	private WebElement paInterruptModal30DaysPDText;

	@FindBy(css = "button#dgPAEntryContinue")
	private WebElement continueBtnInterruptModal;

	@FindBy(css = "button#dgPAEntryCancel")
	private WebElement cancelBtnInterruptModal;

	@FindBy(css = "span.fine-print-body-bold")
	private WebElement otpVariantPageHeader;

	@FindBy(css = "div[ng-if='vm.isDigitalPA && vm.isOTPVariant'].fine-print-body-bold")
	private WebElement otpVariantPageSubHeader;

	@FindBy(css = "div[ng-click*='Amount'] p.legal")
	private WebElement textOnAmountBlade;

	@FindBy(css = "div.col-xs-11.col-md-9 p")
	private List<WebElement> digitalPAAlert;

	@FindBy(xpath = "//div[contains(text(),'Date')]//following-sibling::div/p")
	private WebElement dateBladeOTPVariantPage;

	@FindBy(css = "span.fine-print-body-bold")
	private WebElement totalDueAmount;

	@FindBy(css = "#closeModal i.fa.fa-close")
	private WebElement closeModelIcon;

	@FindBy(css = "div[ng-repeat='week in vm.monthArray'] span:not([class='day ng-binding ng-scope different-month']")
	private List<WebElement> selectdate;

	@FindBy(css = "button[ng-click='vm.updateDate()']")
	private WebElement updateBtn;

	@FindBy(css = "span.gluePull-left-sm.pa-alert-text.ng-binding")
	private WebElement pAEnhancementAlert;

	@FindBy(css = "div.clock.p-b-20.p-t-20")
	private WebElement clock;

	@FindAll({ @FindBy(css = "div.padding-top-medium span.fine-print-body a"),
			@FindBy(xpath = "//a[text()='Setup a payment arrangement']") })
	private WebElement paymentArrangementLink;

	@FindBy(xpath = "//div[contains(@class,'fine-print-body color-9B9B9B ng-binding')]")
	private WebElement paConvinenceFeeMsg;

	@FindBy(css = "h4.h4-title.text-black")
	private WebElement digitalPAModalHeader;

	@FindBy(css = "h4.h3-title.h4-title-mobile")
	private WebElement otpVariantPage;

	@FindBy(css = "p.inline.no-margin.fine-print-body")
	private WebElement textUnderTotalBal;

	@FindBy(css = "button#dgPAEntryContinue")
	private WebElement digitalPAContinueBtn;

	@FindBy(css = "span.fine-print-body.ng-binding")
	private WebElement lastPaymentText;
	
	@FindBy(xpath="//button[contains(text(),'Yes, leave')]")
	private WebElement yesLeaveCTA;
	


	@FindBy(xpath = "//div[contains(text(),'You have a payment of')]")
	private List<WebElement> txtalertschedulepayment;
	
	@FindBy(xpath = "//div[contains(text(),'AutoPay will process any remaining balance on')]")
	private List<WebElement> txtalertAutopay;
	
	
	@FindBy(id = "dateText")
	private WebElement selecteddate;

	/**
	 * page constructor
	 * 
	 * @param webDriver
	 */
	public OneTimePaymentPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public OneTimePaymentPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public OneTimePaymentPage verifyNewPageUrl() {
		waitFor(ExpectedConditions.urlContains(newPageUrl));
		checkPageIsReady();
		return this;
	}

	/**
	 * Verify arrow right is displayed or not.
	 */
	public OneTimePaymentPage verifyArrowRight() {
		try {
			arrowRight.isDisplayed();
			Reporter.log("arrow right is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify arrow right");
		}
		return this;
	}

	/**
	 * clcik on arrow right
	 */
	public OneTimePaymentPage clickArrowRight() {
		try {
			arrowRight.click();
			Reporter.log("clicked onarrow right");
		} catch (Exception e) {
			Assert.fail("failed to verify arrow right");
		}
		return this;
	}

	/**
	 * Verify payment blade header with out payment method is displayed or not.
	 */
	public OneTimePaymentPage verifyPaymentBaldeHeaderWithOutPaymentMethod() {
		try {
			addPaymentMethod.isDisplayed();
			Assert.assertTrue(addPaymentMethod.getText().equals("Add a payment method"));
			Reporter.log("payment method blade with out header is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify payment method blade with out header ");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public OneTimePaymentPage verifyPageLoaded() {
		try {
			Thread.sleep(5000);
			checkPageIsReady();
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				verifyPageUrl();
			} else {
				verifyPageUrl();
				otpPageHeader.isDisplayed();
			}
			Reporter.log("One time payment page is displayed");
		} catch (Exception e) {
			Assert.fail("One time payment page not displayed");
		}
		return this;
	}

	/**
	 * Click edit button for amount
	 */
	public OneTimePaymentPage clickamountIcon() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(amountIcon));
			clickElementWithJavaScript(amountIcon);
			Reporter.log("Clicked on Amount icon");
		} catch (Exception e) {
			Assert.fail("Amount icon not found");
		}
		return this;
	}

	/**
	 * Click add payment method blade
	 * 
	 * @return
	 */
	public OneTimePaymentPage clickPaymentMethodBlade() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(paymentMethodBlade));
			paymentMethodBlade.click();
			while (getDriver().getCurrentUrl().contains("blade")) {
				if (paymentMethodBlade.isDisplayed()) {
					paymentMethodBlade.click();
				}
			}
			Reporter.log("Clicked on payment method blade");
		} catch (Exception e) {
			Assert.fail("Payment method blade not found");
		}
		return this;
	}

	/**
	 * Verify Agree And Submit Button is displayed and enabled
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyAgreeAndSubmitButtonEnabledOTPPage() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.visibilityOf(agreeAndSubmitbtn));
			agreeAndSubmitbtn.isEnabled();
			Reporter.log("Verified Agree and submit button");
		} catch (Exception e) {
			Verify.fail("Agree and submit button not found");
		}
		return this;
	}

	/**
	 * get payment account number on blade
	 * 
	 * @return boolean
	 */
	public String getStoredcheckingaccountNumber() {
		String accountNumber = null;
		try {
			accountNumber = storedPaymentCheckingNumber.getText();
			Reporter.log("Checking Account number is retrieved");
		} catch (Exception e) {
			Assert.fail("Failed to retrieve Checking Account number");
		}
		return accountNumber;
	}

	/**
	 * get payment card number on blade
	 * 
	 * @return boolean
	 */
	public String getStoredCardNumber() {
		String storedCard = null;
		try {
			storedCard = storedCardNumber.getText();
			Reporter.log("Stored card number " + storedCard);
		} catch (Exception e) {
			Assert.fail("Stored card number not found");
		}
		return storedCard;
	}

	/**
	 * click continue button modal
	 */
	public OneTimePaymentPage clickContinueBtnModal() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(continueBtnModal));
			continueBtnModal.click();
			Reporter.log("Clicked on Continue button on modal");
		} catch (Exception e) {
			Verify.fail("Continue button on modal not found");
		}
		return this;
	}

	/**
	 * click cancel otp button
	 */
	public OneTimePaymentPage clickCancelBtn() {
		try {
			clickElementWithJavaScript(cancelBtn);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Cancel button");
		} catch (Exception e) {
			Verify.fail("Cancel button not found");
		}
		return this;
	}

	/***
	 * Verify OTP page alerts
	 * 
	 * @return
	 */

	public OneTimePaymentPage verifyOtpPageAlerts() {
		boolean isDisplayed = false;
		try {
			for (WebElement alert : alertTexts) {
				if (alert.isDisplayed() && alert.getText()
						.contains("If you make a one-time payment now, AutoPay will process any remaining balance")) {
					warningLogo.isDisplayed();
					Reporter.log("Alert found on OTP page");
					isDisplayed = true;
					break;
				}
			}
			Verify.assertTrue(isDisplayed, "Alert is not displayed on OTP page");
		} catch (Exception e) {
			Verify.fail("Alert not found on OTP page");
		}
		return this;
	}

	/**
	 * Click payment Arrangement Yes button
	 */
	public OneTimePaymentPage clickpaymentArrangementYesBtn() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				pAdialogBoxYesMobile.click();
			} else {
				waitFor(ExpectedConditions.elementToBeClickable(paymentArrangementYesBtn));
				paymentArrangementYesBtn.click();
			}
			Reporter.log("Clicked on Payment Arrangement 'Yes' button on modal");
		} catch (Exception e) {
			Assert.fail("Payment Arrangement 'Yes' button not found on modal");
		}
		return this;
	}

	/**
	 * Click payment Arrangement link
	 */
	public OneTimePaymentPage clickpaymentArrangement() {
		try {
			clickPaymentArrangementLink();
			clickpaymentArrangementYesBtn();
		} catch (Exception e) {
			Assert.fail("PaymentArrangement link not found or This MSDSIN  may not  eligible for Payment Arrangement");
		}
		return this;
	}

	/**
	 * Click payment Arrangement link
	 */
	public OneTimePaymentPage clickPaymentArrangementLink() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.visibilityOf(paymentArrangementLink));
			paymentArrangementLink.click();
			Reporter.log("Clicked on PaymentArrangement link");
		} catch (Exception e) {
			Assert.fail("PaymentArrangement link not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyPaymentArrangementLink() {
		boolean holderValue = false;
		try {
			holderValue = paymentArrangementLink.isDisplayed();
			if (holderValue == false) {
				paymentArrangementLink = getDriver().findElement(By.cssSelector("div.tnt18178GoToPaNewDiv"));
				holderValue = paymentArrangementLink.isDisplayed();
			}
			Verify.assertTrue(holderValue, "PA link not displayed");
		} catch (Exception e) {
			Reporter.log("PA link not found, trying alternate steps to navigate to PA page");
		}
		return this;
	}

	public boolean verifyPaymentArrangementLinkDisplayed() {
		boolean holderValue = false;
		try {
			holderValue = paymentArrangementLink.isDisplayed();
			if (holderValue == false) {
				paymentArrangementLink = getDriver().findElement(By.cssSelector("div.tnt18178GoToPaNewDiv"));
				holderValue = paymentArrangementLink.isDisplayed();
			}
			return holderValue;
		} catch (Exception e) {
			Reporter.log("PA link not displayed or This data may not eligible for Payment Arrangement");
		}
		return holderValue;
	}

	public OneTimePaymentPage clickTermsAndConditionsBackButton() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			backTermsandConditionsButton.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Terms and Conditions Link");
		} catch (Exception e) {
			Verify.fail("Terms and Conditions Link not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyTermsandConditions() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div[ng-hide='hideSpinner']")));
			Verify.assertTrue(termsandConditionsHeadline.isDisplayed());
			Reporter.log("Verified terms and conditions");
		} catch (Exception e) {
			Verify.fail("Terms and conditions not found");
		}
		return this;
	}

	public OneTimePaymentPage clickTermsAndConditions() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			termsAndConditionsLink.click();
			Reporter.log("Clicked on Terms and Conditions link");
		} catch (Exception e) {
			Verify.fail("Terms and Conditions link not found");
		}
		return this;
	}

	/**
	 * Verify Cancel Button is displayed
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyCancelButton() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div[ng-hide='hideSpinner']")));
			cancelBtn.isDisplayed();
			cancelBtn.isEnabled();
			Reporter.log("Verified cancel button");
		} catch (Exception e) {
			Assert.fail("Cancel button not found");
		}
		return this;
	}

	/**
	 * verify ban Message
	 * 
	 * @return string
	 */
	public OneTimePaymentPage verifyBanMessage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(banMessage));
			banMessage.getText().equals(
					"To make a payment on this account, please visit a T-mobile store or call Customer Care at 1-877-746-0909.");
			Reporter.log("Ban message verified");
		} catch (Exception e) {
			Verify.fail("Ban message component not found");
		}
		return this;
	}

	public OneTimePaymentPage ClickOKbutton() {
		try {
			okButton.click();
			Reporter.log("Clicked on OK button");
		} catch (Exception e) {
			Verify.fail("Ok button not found");
		}
		return this;
	}

	/**
	 * click agree and submit
	 */
	public OneTimePaymentPage agreeAndSubmit() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("screen-lock")));
			waitFor(ExpectedConditions.visibilityOf(agreeAndSubmitbtn));
			agreeAndSubmitbtn.click();
			// waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Agree N submit button");
		} catch (Exception e) {
			Verify.fail("Agree N submit button not found");
		}
		return this;
	}

	/**
	 * Verify Inline Error Msgs OTP page on click of Agg
	 * 
	 * @param errorMsg
	 */
	public OneTimePaymentPage verifyInlineErrorMsgs(String errorMsg) {
		try {
			for (WebElement inlineErr : inlineErrorMsgs) {
				inlineErr.isDisplayed();
				inlineErr.getText().equals(errorMsg);
			}
			Reporter.log("Verified inline error Messages");
		} catch (Exception e) {
			Verify.fail("Inline error message component missing");
		}
		return this;
	}

	/**
	 * verify if continue button is displayed
	 * 
	 * @return true/false
	 */
	public OneTimePaymentPage isContinueBtnDisplayedOnModal() {
		try {
			continueBtnModal.isDisplayed();
			Reporter.log("Coninue button displayed");
		} catch (Exception e) {
			Verify.fail("Continue button not found");
		}
		return this;
	}

	/**
	 * verify cancel message
	 * 
	 * @return String
	 */
	public OneTimePaymentPage verifyCancelMsg(String msg) {
		try {
			Verify.assertTrue(cancelMsg.getText().contains(msg));
			Reporter.log("cancel model popup  displayed");
		} catch (Exception e) {
			Verify.fail("Cancel button not found");
		}
		return this;
	}

	public OneTimePaymentPage clickBackBtn() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(backBtn));
			backBtn.click();
			Reporter.log("Clicked on AutoPay SignUP button");
		} catch (Exception e) {
			Verify.fail("Autopay signUP button not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyTermsAndConditions() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			termsandConditionsHeadline.getText().equals("Terms and Conditions");
			Reporter.log("Verified terms and condtions");
		} catch (Exception e) {
			Assert.fail("Terms and conditions not found");
		}
		return this;
	}

	/**
	 * Verify Payment header is displayed
	 * 
	 * @return true/false
	 */

	public OneTimePaymentPage verifyPaymentHeader() {
		try {
			paymentHeader.isDisplayed();
			Reporter.log("Payment spoke page is displayed");
		} catch (Exception e) {
			Reporter.log("Payment spoke pageis displayed");
		}
		return this;
	}

	/**
	 * verify payment method div
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyPaymentMethod() {
		try {
			paymentMethodBlade.isDisplayed();
			paymentMethodBlade.getText().equals("Add payment method");
			Reporter.log("Verified Payment method");
		} catch (Exception e) {
			Assert.fail("Payment method component missing");
		}
		return this;
	}

	public OneTimePaymentPage verifyDateBlade() {
		try {
			Verify.assertTrue(dateBlade.isDisplayed(), "Date Blade not displayed");
			Reporter.log("Date blade is displayed");
		} catch (Exception e) {
			Verify.fail("Date blade not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyPaymentMethodBlade() {
		try {
			Verify.assertTrue(paymentMethodBlade.isDisplayed(), "paymentMethodBlade  not displayed");
			// paymentMethodBlade.isDisplayed();
			Reporter.log("Payment blade displayed");
		} catch (Exception e) {
			Verify.fail("Payment blade not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyAmountBlade() {
		try {
			// amountBlade.isDisplayed();
			Verify.assertTrue(amountBlade.isDisplayed(), "Amount Blade not displayed");
			Reporter.log("Verified Amount blade");
		} catch (Exception e) {
			Verify.fail("Amount blade not found");
		}
		return this;
	}

	public OneTimePaymentPage clickDateIcon() {
		try {
			clickElementWithJavaScript(dateBlade);
			Reporter.log("Clicked on AutoPay SignUP button");
		} catch (Exception e) {
			Assert.fail("Autopay signUP button not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyCancelOTPModal() {
		try {
			checkPageIsReady();
			otpPageModalMessage.isDisplayed();
			Assert.assertTrue(otpPageModalMessage.getText().contains(Constants.OTP_CANCEL_MODAL_TEXT));
			backExitOTPBtn.isDisplayed();
			continueExitOTPBtn.isDisplayed();
			Reporter.log("Verified OTP Model");
		} catch (Exception e) {
			Assert.fail("Autopay signUP button not found");
		}
		return this;
	}

	public OneTimePaymentPage clickbackExitOTPBtn() {
		try {
			backExitOTPBtn.click();
			Reporter.log("Clicked on AutoPay SignUP button");
		} catch (Exception e) {
			Assert.fail("Autopay signUP button not found");
		}
		return this;
	}

	public OneTimePaymentPage clickViewDetails() {
		try {
			viewDetailsLink.get(0).click();
			Reporter.log("Clicked on AutoPay SignUP button");
		} catch (Exception e) {
			Assert.fail("Autopay signUP button not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyViewDetailsLink() {
		try {
			if (viewDetailsLink.size() > 1) {
				viewDetailsLink.get(0).isDisplayed();
				viewDetailsLink.get(1).isDisplayed();
			}
			viewDetailsLink.get(0).isDisplayed();
			Reporter.log("Verified view details link");
		} catch (Exception e) {
			Assert.fail("View details link missing");
		}
		return this;
	}

	/**
	 * verify spinner on OTP page
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifySpinnerpresentuntilOTPPageLoads() {
		try {
			waitFor(ExpectedConditions
					.invisibilityOfElementLocated(By.cssSelector("i.fa.fa-spin.fa-spinner.interceptor-spinner")));
			otpPageHeader.isDisplayed();
			Reporter.log("Verified Spinner");
		} catch (Exception e) {
			Assert.fail("Spinner not found");
		}
		return this;
	}

	public String getBalanceDueAmount() {
		String bal = "";
		try {
			if (balanceDueAmount.isDisplayed()) {
				bal = balanceDueAmount.getText().replace("$", "");
			}

		} catch (Exception balDueNotCaptured) {
			Assert.fail("Balance Due could NOT be captured. ");
			bal = "Error";
		}
		return bal;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyAlertForAmountLessThanMissedInstallement() {
		try {
			checkPageIsReady();
			avoidAccountSuspensionAlert.isDisplayed();
			Assert.assertTrue(avoidAccountSuspensionAlert.getText().replaceAll("[0-9,/,$,.]", "").contains(
					"You need to have paid  toward your Payment Arrangement by today to avoid suspension & fees Increase amount or review payments made"));
			Reporter.log("Verified amount less than missed installment");
		} catch (Exception e) {
			Assert.fail("Alert not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyAlertForAmountLessThanInstallementDueInsideBlackoutPeriod() {
		try {
			checkPageIsReady();
			avoidAccountSuspensionAlert.isDisplayed();
			Assert.assertTrue(avoidAccountSuspensionAlert.getText().replaceAll("[0-9,/,$,.]", "").contains(
					"You need to have paid  toward your Payment Arrangement by today to avoid suspension & fees Increase amount or review payments made"));
			Reporter.log("Verified alert for amount less than installment due");
		} catch (Exception e) {
			Assert.fail("Alert not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyAlertForAmountGreaterThanInstallementDueInsideBlackoutPeriod() {
		try {
			avoidAccountSuspensionAlert.isDisplayed();
			Assert.assertTrue(avoidAccountSuspensionAlert.getText().replaceAll("[0-9,/,$,.]", "")
					.contains("This payment covers your payment arrangement amount due today"));
			Reporter.log("Verified alert for amount greater than installment Due inside blackout period");
		} catch (Exception e) {
			Assert.fail("Alert not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyAlertForAmountGreaterThanMissedInstallement() {
		try {
			avoidAccountSuspensionAlert.isDisplayed();
			Assert.assertTrue(avoidAccountSuspensionAlert.getText().replaceAll("[0-9,/,$,.]", "")
					.contains("This payment covers your payment arrangement amount due today"));
			Reporter.log("Verified alert for amount greater than missed installment");
		} catch (Exception e) {
			Assert.fail("Alert not found");
		}
		return this;
	}

	/**
	 * Verify Review Payments Link
	 */

	public OneTimePaymentPage verifyReviewPaymentsLink() {
		try {
			reviewPreviouspayment.isDisplayed();
			Reporter.log("Review payment Link is displayed");
		} catch (Exception e) {
			Assert.fail("Review payment is not Found");
		}
		return this;
	}

	public OneTimePaymentPage clickReviewPreviouspayments() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(reviewPreviouspayment));
			clickElementWithJavaScript(reviewPreviouspayment);
			Reporter.log("Clicked on review previous payments");
		} catch (Exception e) {
			Assert.fail("Review payments not found");
		}
		return this;
	}

	public OneTimePaymentPage clickReviewPaymentsMade() {
		try {
			reviewPayemntsMadeLink.click();
			Reporter.log("Clicked on review payment made link");
		} catch (Exception e) {
			Assert.fail("Review payment made link not found");
		}
		return this;
	}

	/**
	 * click agree and submit button
	 */
	public OneTimePaymentPage clickAgreeAndSubmit() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(agreeAndSubmitButton));
			clickElementWithJavaScript(agreeAndSubmitButton);
			Reporter.log("Clicked on agree and submit button");
		} catch (Exception e) {
			Assert.fail("Agree and submit button not found");
		}
		return this;
	}

	public OneTimePaymentPage clickOnShowdetails() {
		try {
			showDetails.click();
			Reporter.log("Clicked on Show details button");
		} catch (Exception e) {
			Assert.fail("Show details button not found");
		}
		return this;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyAlertForAmountLessThanInstallementDueOutsideBlackoutPeriod() {
		try {
			checkPageIsReady();
			avoidAccountSuspensionAlert.isDisplayed();
			Assert.assertTrue(avoidAccountSuspensionAlert.getText().replaceAll("[0-9,/,$,.]", "").contains(
					"You need to have paid  toward your Payment Arrangement by  to avoid suspension & fees Increase amount or review payments made"));
			Reporter.log("Clicked on AutoPay SignUP button");
		} catch (Exception e) {
			Assert.fail("Autopay signUP button not found");
		}
		return this;
	}

	/**
	 * get payment card/account number on blade
	 */
	public String getStoredPaymentMethodNumber() {
		String cardNumber = null;
		try {
			waitFor(ExpectedConditions.visibilityOf(storedPaymentCardNumber));
			// wait(2,20);
			cardNumber = storedPaymentCardNumber.getText().replace(" ", "").replace("*", "").trim();
		} catch (Exception e) {
			Assert.fail("Payment method number not found");
		}
		return cardNumber;
	}

	/**
	 * verify card/account number displayed or not
	 */
	public OneTimePaymentPage verifyPaymentMethodCardOrAccountNumber() {

		try {
			storedPaymentCardNumber.isDisplayed();
		} catch (Exception e) {
			Assert.fail("failed to verify stored account number/ cerd number");
		}
		return this;
	}

	/**
	 * verify total balance amountis displayed in the header as per the Acceptance
	 * criteria
	 * 
	 * @return true/false
	 */
	public OneTimePaymentPage verifyTotalBalanceAmountInHeader() {
		try {
			String balAmount = totalInHeader.getText();
			balAmount = balAmount.substring(balAmount.indexOf('$'), balAmount.indexOf(" "));
			totalInHeader.isDisplayed();
			Reporter.log("Verified total balance amount header");
		} catch (Exception e) {
			Assert.fail("Balance Amount header not found");
		}
		return this;
	}

	/**
	 * Verify Agree And Submit Button is displayed and enabled
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyAgreeAndSubmitButton() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));

			agreeAndSubmitbtn.isDisplayed();
			Reporter.log("Verified agree and submit button");
		} catch (Exception e) {
			Assert.fail("Agree and submit button not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyCancelBtnOtpPage() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			cancelBtn.isDisplayed();
			Reporter.log("Verified Cancel Button in OTP page");
		} catch (Exception e) {
			Assert.fail("Cancel button not found in OTP page");
		}
		return this;
	}

	/*
	 * public OneTimePaymentPage verifyPaymentArrangementLink() { try {
	 * waitFor(ExpectedConditions.visibilityOf(paymentArrangementLink));
	 * paymentArrangementLink.isDisplayed(); Reporter.log("PA Link displayed"); }
	 * catch (Exception e) { Assert.fail(
	 * "Entry point to Payment arrangement is not displayed"); } return this;
	 * 
	 * }
	 */

	/**
	 * verify pay your bill header
	 * 
	 * @return true/false
	 */
	public OneTimePaymentPage verifyPayYourBillHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(payYourBillHeader));
			payYourBillHeader.isDisplayed();
			Reporter.log("Verified Pay your bill header");
		} catch (Exception e) {
			Assert.fail("Pay your bill header not found");
		}
		return this;
	}

	/**
	 * verify cancel modal on OTP page
	 * 
	 * @return true/false
	 */
	public OneTimePaymentPage verifyCancelModal() {
		try {
			otpPageModals.isDisplayed();
			Reporter.log("Verified Cancel model");
		} catch (Exception e) {
			Assert.fail("Cancel button not found in model");
		}
		return this;
	}

	/**
	 * click the BACK button on cancel modal
	 */
	public OneTimePaymentPage clickBackBtnCancelModal() {
		try {
			backBtnCancelModal.click();
			Reporter.log("Clicked on back button");
		} catch (Exception e) {
			Assert.fail("Back button not found");
		}
		return this;
	}

	public OneTimePaymentPage clickContinueBtnCancelModal() {
		try {
			continueBtnCancelModal.click();
			Reporter.log("Clicked on continue button on Cancel model");
		} catch (Exception e) {
			Assert.fail("Continue button not found in cancel button");
		}
		return this;
	}

	/**
	 * Verify Payment Arrangement Text
	 * 
	 * @return String
	 */
	public OneTimePaymentPage verifyPaymentArrangementText() {
		try {
			Boolean paTextDisplayed = false;
			for (WebElement paText : paymentArrangementText) {
				String paMsg = paText.getText();
				paTextDisplayed = paMsg.contains("Make a Payment Arrangement (pay in installments)") || paMsg.contains(
						"Make a payment arrangement online and it's free. Payments made over the phone may incur a $8 Payment Support Fee.");
				if (paTextDisplayed) {
					break;
				}
			}
			Assert.assertTrue(paTextDisplayed, "Payment arrangement text not found in OTP page.");
			Reporter.log("Verified payment arrangement text");
		} catch (Exception e) {
			Assert.fail("Payment arrangement text not found");
		}
		return this;
	}

	/**
	 * verify payment method blade and saved payment type is displayed on blade
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifySavedPaymentMethod() {
		try {
			savedpaymentType.isDisplayed();
			Reporter.log("Verified saved payment method");
		} catch (Exception e) {
			Assert.fail("Saved payment method component not found");
		}
		return this;
	}

	/**
	 * verify payment method blade for no 'Add a payment method' text
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyNoSavedPaymentMethod() {
		try {
			noPaymentMethodOnBlade.isDisplayed();
			Assert.assertTrue(noPaymentMethodOnBlade.getText().equals("Add a payment method"));
			Reporter.log("Verified 'Add a payment method' on blade");
		} catch (Exception e) {
			Assert.fail("'Add a payment method' component not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyPaymentAmountSedonaAlert(String warningText) {
		try {
			errorMsgOtherAmount.isDisplayed();
			errorMsgOtherAmount.getText().equals(warningText);
			Reporter.log("Verified payment amount in sedona alert");
		} catch (Exception e) {
			Assert.fail("Sedona alert not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyPaymentAmountSedonaAlertMsg(String message) {
		try {
			alertMsgSedona.isDisplayed();
			alertMsgSedona.getText().equals(message);
			Reporter.log("Verified Sedona alert message");
		} catch (Exception e) {
			Assert.fail("Sedona alert message not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyPaymentAmountSedonaAlertInRed(String message) {
		try {
			errorMsgOtherAmount.isDisplayed();
			errorMsgOtherAmount.getText().equals(message);
			Assert.assertTrue(errorMsgOtherAmount.getAttribute("class").contains("color-red"));
			Reporter.log("Verified payment amount Alert in Red");
		} catch (Exception e) {
			Assert.fail("Sedona alert not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyStoredCardReplacedWithNewCard(String cardNumber) {

		try {
			String storedPaymentNumber = storedPaymentCardNumber.getText().replace("*", "");
			Assert.assertTrue(cardNumber.contains(storedPaymentNumber),
					"stored Card number is not replaced with new card number");
			Reporter.log("stored Card number is replaced with new card number");
		} catch (NoSuchElementException elementNotFound) {
			Assert.fail("stored Card number is not replaced with new card number");
		}

		return this;
	}

	public void verifySavedBankAccount(String accNo) {
		try {
			storedPaymentCardNumber.isDisplayed();
			Assert.assertTrue(storedPaymentCardNumber.getText().contains(accNo.substring(accNo.length() - 4)),
					"Saved payment method not found");
		} catch (Exception e) {
			Assert.fail("Saved payment method on OTP is not displayed on payment method blade");
		}
	}

	/**
	 * verify amount due for current installment on amount blade
	 * 
	 * @param textToVerify
	 */
	public OneTimePaymentPage verifyAmountBladeText(String textToVerify) {
		try {
			textOnAmountBlade.isDisplayed();
			Assert.assertTrue(textOnAmountBlade.getText().equals(textToVerify));
			Reporter.log("Text on Amount blade displayed");
		} catch (Exception e) {
			Assert.fail("Text is not displayed on Amount blade - OTP page");
		}
		return this;
	}

	/**
	 * Verify the Past Due Balance in the Amount Blade
	 */
	public OneTimePaymentPage verifyAmountDueEqualsPastDueOnBlade(String balanceAmount) {
		try {
			Assert.assertTrue(amountOnBlade.isDisplayed(), "Due amount is not displayed on Amount blade");
			Assert.assertTrue(amountOnBlade.getText().equals(balanceAmount),
					"Past due amount does not eaquals amount displayed on amount blade");
			Reporter.log("Past due amount equals amount displayed on amount blade");
		} catch (Exception e) {
			Assert.fail("Due amount is not displayed on Amount blade - OTP page");
		}
		return this;
	}

	/**
	 * Verify the message below the amount blade
	 */
	public OneTimePaymentPage verifyInlineErrorMsg(String warningText) {
		try {
			Assert.assertTrue(inlineErrorMsg.isDisplayed(), "Warning text is not displayed on amount blade");
			Assert.assertTrue(inlineErrorMsg.getText().equals(warningText),
					"Warning text is not correct on amount blade");
			Reporter.log("Warning text is displayed on amount blade");
		} catch (Exception e) {
			Assert.fail("Warning text is not displayed on amount blade");
		}
		return this;
	}

	/**
	 * Verify the notification above terms and conditions
	 */
	public OneTimePaymentPage verifyOtpPageAlerts(String alertText) {
		boolean isDisplayed = false;
		try {
			for (WebElement alert : alertTexts) {
				if (alert.isDisplayed() && alert.getText().contains(alertText)) {
					warningLogo.isDisplayed();
					Reporter.log("Alert found on OTP page");
					isDisplayed = true;
					break;
				}
			}
			Assert.assertTrue(isDisplayed, "Alert Text on OTP page is not correct");
		} catch (Exception e) {
			Assert.fail("Alert not found on OTP page");
		}
		return this;
	}

	/**
	 * Verify InLine error message Under Amount is not displayed
	 */
	public OneTimePaymentPage verifyInlineErrorMsgNotDisplayed() {
		try {
			boolean isInlineErrorMsgDisplayed = isElementDisplayed(inlineErrorMsg);
			Assert.assertFalse(isInlineErrorMsgDisplayed,
					"Warning text is displayed on amount blade for amount less than Past due");
		} catch (Exception e) {
			Assert.fail("Warning text on amount blade for amount less than Past due is not found");
		}
		return this;
	}

	public void verifyPopUp() {
		// TODO Auto-generated method stub

	}

	public void verifyMessageOnPopUp() {
		// TODO Auto-generated method stub

	}

	public void verifyCTAsOnPopUp() {
		// TODO Auto-generated method stub

	}

	public void clickContinueWithPaymentCTA() {
		try {
			continueWithPaymentBtnOnModal.click();
			Reporter.log("clicked on 'continue with payment' button on modal");
		} catch (Exception e) {
			Assert.fail("Unable to click on 'continue with payment' button on modal");
		}
	}

	public void clickReviewPaymentArrangementCTA() {
		// TODO Auto-generated method stub
		try {
			reviewPAbtnOnModal.click();
			Reporter.log("clicked on 'Review Payment Arrangement' button on modal");
		} catch (Exception e) {
			Assert.fail("Unable to click on 'Review Payment Arrangement' button on modal");
		}

	}

	/***
	 * Verify OTP page alerts
	 * 
	 * @return
	 */

	public OneTimePaymentPage verifyNoAlertsOnOtpPage() {
		try {
			for (WebElement alert : alertTexts) {
				if (alert.isDisplayed() && !alert.getText().contains("By submitting, you agree to ")) {
					Assert.fail("Alerts found on OTP page");
					break;
				}
			}
			Reporter.log("Verified no Alerts displayed on OTP page");
		} catch (Exception e) {
			Assert.fail("Alert not found on OTP page");
		}
		return this;
	}

	public String verifyReviewPAModal() {
		String modalText = null;
		try {
			checkPageIsReady();
			otpPageModals.isDisplayed();
			otpPageModalMessage.isDisplayed();
			modalText = otpPageModalMessage.getText();
			continueWithPaymentBtnOnModal.isDisplayed();
			reviewPAbtnOnModal.isDisplayed();
			Reporter.log("Verified Review PA Modal on OTP page");
		} catch (Exception e) {
			Assert.fail("Review PA Modal on OTP page is not found");
		}
		return modalText;
	}

	public Boolean verifyReviewPAModalIsDisplayedOrNot() {
		boolean isDisplayed = false;
		try {
			checkPageIsReady();
			otpPageModals.isDisplayed();
			isDisplayed = true;
			Reporter.log("Review PA Modal on OTP page is found");
		} catch (Exception e) {
			Reporter.log("Verified Review PA Modal is not displayed on OTP page");
			return isDisplayed;
		}
		return isDisplayed;
	}

	public OneTimePaymentPage verifyaddPaymentWarning() {
		try {

			waitFor(ExpectedConditions.visibilityOf(warningAddApyment));
			Reporter.log("Add Payment method warning message is displayed");
		} catch (Exception e) {
			Assert.fail("Add Payment method message is not get displayed");
		}
		return this;
	}

	public void clickReviewPABtnOnModal() {
		try {
			reviewPAbtnOnModal.click();
			Reporter.log("Clicked Review PA button");
		} catch (Exception e) {
			Assert.fail("Review PA button not found");
		}
	}

	/**
	 * This method is to verify PII masking is done for all payment information
	 * 
	 * @param custPaymentPID
	 */
	public void verifyPIImasking(String custPaymentPID) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(storedPaymentNo, custPaymentPID));
			Reporter.log("Verified PII masking for Payment method blade");
		} catch (Exception e) {
			Assert.fail("Failed to verify: PII masking for Payment method blade");
		}
	}

	/**
	 * This method is to verify PII masking is done for all payment information
	 * 
	 * @param custPaymentPID
	 */
	public void verifyPAAlert(String paAlert) {
		try {
			paymentArrangementAlert.isDisplayed();
			Assert.assertTrue(paymentArrangementAlert.getText().equals(paAlert));
		} catch (Exception e) {
			Assert.fail("Payment Arrangement Notification on OTP is not as expected");
		}
	}

	/**
	 * This method is to verify PA Notification Message in OTP Payment page
	 * 
	 * @param Formatted Message
	 */

	public OneTimePaymentPage verifySecuredPAMessageNotification(String paNotificationMessage) {
		try {

			waitFor(ExpectedConditions.visibilityOf(paSecuredMessageNotification));
			Assert.assertTrue(paSecuredMessageNotification.getText().contains(paNotificationMessage));
			Reporter.log(paSecuredMessageNotification.getText()
					+ "PA Notification Message is displayed on OTP Payment Page");
		} catch (Exception e) {
			Assert.fail(paSecuredMessageNotification.getText()
					+ "PA Notification Message is not get displayed on OTP Payment Page");
		}
		return this;
	}

	/**
	 * This method is to verify for the display of View Payment Schedule Link
	 * 
	 * @param
	 */

	public OneTimePaymentPage verifyViewPaymentScheduleLink() {
		try {

			waitFor(ExpectedConditions.visibilityOf(viewPaymentScheduleLink));
			viewPaymentScheduleLink.isDisplayed();
			Reporter.log("View Payment Schedule Link is displayed on OTP Payment Page");
		} catch (Exception e) {
			Assert.fail("View Payment Schedule Link is not displayed on OTP Payment Page");
		}
		return this;
	}

	/**
	 * This method is to verify PII masking is done for all payment information on
	 * OTP/PA modal
	 * 
	 * @param custPaymentPID
	 */
	public void verifyPIImaskingOnModal(String custPaymentPID) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(otpPageModalMasking, custPaymentPID));
			Reporter.log("Verified PII masking for Payment details on OTP/PA modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify: PII masking for Payment details on OTP/PA modal");
		}
	}

	/**
	 * verify payment method existance in Paymentblade in OTP page
	 * 
	 * @return boolean
	 */

	public boolean verifyPaymethodInPaymentBalde() {
		boolean bFlag = false;
		try {
			waitFor(ExpectedConditions.visibilityOf(accountFourDigitonPaymentBlade), 15);
			bFlag = accountFourDigitonPaymentBlade.isDisplayed();
			Reporter.log("Payment method is already");
		} catch (Exception e) {
			Reporter.log("Payment method is not added");
		}
		return bFlag;
	}

	/**
	 * verify PA interrupt modal is displayed for 30 days past due PA eligible
	 * customer
	 */
	public void verifyPAInterruptModal() {
		try {
			checkPageIsReady();
			Assert.assertTrue(paInterruptModal.isDisplayed(), "PA Interrupt Modal is not found");
			Assert.assertTrue(paInterruptModalText.isDisplayed(), "PA Interrupt Modal Header is not found");
			Assert.assertTrue(continueBtnInterruptModal.isDisplayed(), "PA Interrupt Modal Continue CTA is not found");
			Assert.assertTrue(cancelBtnInterruptModal.isDisplayed(), "PA Interrupt Modal Cancel CTA is not found");
			Reporter.log("Verified Interrupt PA Modal on OTP page");
		} catch (Exception e) {
			Assert.fail("Interrupt PA Modal on OTP page is not found");
		}
	}

	/**
	 * verify text on PA interrupt modal
	 * 
	 * @param paText
	 * @param newOtpPageHeader
	 */
	public void verifyTextOnPAInterruptModal(String paText, String newOtpPageHeader) {
		try {

			Assert.assertTrue(paInterruptModalHeader.getText().equals(newOtpPageHeader),
					"Expected Header is not displayed on PA interrupt modal");
			Assert.assertTrue(paInterruptModalText.getText().equals(paText),
					"Expected text is not displayed on PA interrupt modal");
		} catch (Exception e) {
			Assert.fail("Expected text is not displayed on PA interrupt modal");
		}
	}

	/**
	 * verify 30 days past due text and amount
	 * 
	 * @param pa30DaysPastDueText
	 */
	public void verify30DayPastDueOnPAInterruptModal(String pa30DaysPastDueText) {
		try {
			paInterruptModal30DaysPDText.isDisplayed();
			Assert.assertTrue(paInterruptModal30DaysPDText.getText().equals(pa30DaysPastDueText),
					"'30+ Days past due' is not displayed on PA interrupt modal");
		} catch (Exception e) {
			Assert.fail("'30 Days past due' is not displayed on PA interrupt modal");
		}
	}

	/**
	 * veirfy Continue and Cancel CTA's on interrupt modal
	 */
	public void verifyCTAsOnPAInterruptModal() {
		try {
			Assert.assertTrue(continueBtnInterruptModal.isDisplayed(),
					"'Continue button' - Interrupt PA Modal is not displayed");
			Reporter.log("Verified 'Continue button' on Interrupt PA Modal on OTP page");
			Assert.assertTrue(cancelBtnInterruptModal.isDisplayed(),
					"'Cancel button' - Interrupt PA Modal is not displayed");
			Reporter.log("Verified 'Cancel button' on Interrupt PA Modal on OTP page");
		} catch (Exception e) {
			Assert.fail("'Continue/Cancel button' on Interrupt PA Modal is not found");
		}
	}

	/**
	 * click continue button on PA interrupt modal
	 */
	public void clickContinueOnPAInterruptModal() {
		try {
			continueBtnInterruptModal.click();
			Reporter.log("Clicked Continue btn on interrupt modal");
		} catch (Exception e) {
			Assert.fail("Error clicking Continue btn on interrupt modal");
		}

	}

	/**
	 * verify New OTP page for Digital PA
	 * 
	 * @param newOtpPageHeader
	 */
	public void verifyNewOTPPageLoaded() {
		try {
			checkPageIsReady();
			waitforSpinner();
			verifyNewPageUrl();
			Reporter.log("New One Time Payment page is displayed for Digital PA");
		} catch (Exception e) {
			Assert.fail("New OTP page is not found");
		}
	}

	/**
	 * verify date blade is disabled for Digital PA on new OTP page
	 */
	public void verifyDateBladeIsDisabled() {
		try {
			Assert.assertTrue(dateBladeOTPVariantPage.getAttribute("class").contains("date-disable"),
					"Date blade is not disabled");
			Reporter.log("Date blade is disabled");
		} catch (Exception e) {
			Assert.fail("Date blade is not found");
		}

	}

	/**
	 * verify date blade shows current date PST
	 */
	public void verifyDateOnDateBlade() {
		try {
			DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
			Calendar calobj = Calendar.getInstance();
			String sysDate = dateFormat.format(calobj.getTime());
			Assert.assertTrue(dateBladeOTPVariantPage.getText().equals(sysDate),
					"Date does not match with Today's date");
			Reporter.log("Verified Date matches with Today's date");
		} catch (Exception e) {
			Assert.fail("Date blade is not found");
		}
	}

	public void verifyPALinkIsSuppressed() {
		try {
			Assert.assertFalse(paymentArrangementLink.isDisplayed());
			Assert.fail("Payment arrangement link is not  suppressed");
		} catch (Exception e) {
			Reporter.log("Payment arrangement link is suppressed");
		}
	}

	/**
	 * get the 30 days past due amount from interrupt modal
	 * 
	 * @return value
	 */
	public String get30DaysPastDueAmount() {
		try {
			return paInterruptModalAmount.getText();
		} catch (Exception e) {
			Assert.fail("30 days past due amount is not found on interrupt modal");
			return null;
		}
	}

	public String getTotalDueAmount() {
		try {
			String headerText = totalDueAmount.getText();
			return headerText;
			// headerText.substring(headerText.indexOf('$'),
			// headerText.indexOf(" "));
		} catch (Exception e) {
			Assert.fail("Total due amount is not found");
			return null;
		}
	}

	/**
	 * verify Digital PA messaging
	 * 
	 * @param digitalPaMsg
	 */
	public void verifyDigitalPAAlert(String digitalPaMsg) {
		try {
			checkPageIsReady();
			verifyElementBytext(digitalPAAlert, digitalPaMsg);

		} catch (Exception e) {
			Assert.fail("DIgital PA Notification on OTP page is not displayed");
		}
	}

	public void verifyBalanceonOTPPageMatchwithHomePage(String Homebal) {
		String bal = "";
		try {
			bal = getTotalDueAmount().replace("$", "");
			Verify.assertTrue(Homebal.replace("$", "").equals(bal));
			Reporter.log("Balance Due  on OTP Page Matches with Homepage Balance due ");
		}

		catch (Exception balDueNotCaptured) {
			Reporter.log(
					"Balance Due  on OTP Page not Matches with Homepage Balance due " + balDueNotCaptured.getMessage());

		}

	}

	/**
	 * Click on Payment Close Model
	 */
	public OneTimePaymentPage clickOnCloseModel() {
		try {
			if (closeModelIcon.isDisplayed()) {
				closeModelIcon.click();
				Reporter.log("Click close model is clickable");
			}
		} catch (Exception e) {
			Reporter.log("close model is not available");
		}
		return this;
	}

	/**
	 * Click on Payment Close Model
	 */
	public OneTimePaymentPage verifyFutureDatePage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(datePageHeader.isDisplayed(), "Future date page not dispalyed");
			Reporter.log("Future date page  dispalyed");

		} catch (Exception e) {
			Assert.fail("Future date page not dispalyed");
		}
		return this;
	}

	/**
	 * Click on Payment Close Model
	 */
	public OneTimePaymentPage selectDate() {
		try {
			checkPageIsReady();
			for (WebElement element : selectdate) {
				if (element.isDisplayed() && !element.isSelected()) {
					element.click();
				}
			}

			Reporter.log("date is selected");

		} catch (Exception e) {
			Assert.fail("date is not selected");
		}
		return this;
	}

	/**
	 * Click on Payment Close Model
	 */
	public OneTimePaymentPage clickUpdateBtn() {
		try {
			checkPageIsReady();
			updateBtn.click();
			Reporter.log("clicked on update button");
		} catch (Exception e) {
			Assert.fail("update button not found");
		}
		return this;
	}
	
	/**
	 * click on yes,leave cta
	 */
	public OneTimePaymentPage clickOnYesLeaveCTA() {
		try {
			yesLeaveCTA.click();
			Reporter.log("clicked on yes, leave cta");
		} catch (Exception e) {
			Assert.fail("Failed to click on leave cta");
		}
		return this;
	}

	/**
	 * select future date
	 */
	public OneTimePaymentPage selectFutureDate() {
		try {
			clickDateIcon();
			verifyFutureDatePage();
			selectDate();
			clickUpdateBtn();
			Reporter.log("selected future date");
		} catch (Exception e) {
			Assert.fail("future date not selected");
		}
		return this;
	}

	public OneTimePaymentPage verifyPaymentArrangementLinkNotDisplayed() {
		checkPageIsReady();

		try {
			if (paymentArrangementLink.isDisplayed()) {
				Assert.fail("PA link  displayed-not expected");
			}

		} catch (Exception e) {

		}
		return this;
	}

	public OneTimePaymentPage verifyPaymentArrangementEntryPointDispalyed() {
		checkPageIsReady();
		try {
			Verify.assertTrue(pAEnhancementAlert.getText().contains("Do you need more time to pay your bill?"),
					"PA Enhancement Alert not dispalyed");
			Verify.assertTrue(clock.isDisplayed(), "PA Enhancement clock not dispalyed");
			Verify.assertTrue(paymentArrangementLink.isDisplayed(), "setup payment arrangement link not dispalyed");
			Reporter.log("PA Entry point is displayed");
		} catch (Exception e) {
			Assert.fail("PA Entry point is not dispalyed");

		}
		return this;
	}

	/**
	 * select future date
	 */
	public OneTimePaymentPage clickPAEntryPointBanner() {
		try {
			checkPageIsReady();
			pAEnhancementAlert.click();
			clickpaymentArrangementYesBtn();

		} catch (Exception e) {
			Assert.fail("PA banner not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyPAConvinenceFeeMessage() {
		checkPageIsReady();
		try {
			Assert.assertTrue(paConvinenceFeeMsg.getText().contains(
					"Pay in installments by setting up a payment arrangement online (it's free). Payments made over the phone may incur an $8 payment support fee"));

			Reporter.log("PA convinence Fee message is displayed");
		} catch (Exception e) {
			Assert.fail("PA convinence Fee message is not dispalyed");

		}
		return this;
	}

	public OneTimePaymentPage verifyDigitalPAModal() {
		checkPageIsReady();
		try {
			digitalPAModalHeader.isDisplayed();
			Assert.assertTrue(digitalPAModalHeader.getText().contains("Please pay the past-due amount"),
					"Digital PA modal header not displayed");
			Reporter.log("Digital PA modal header is displayed");
		} catch (Exception e) {
			Assert.fail("Digital PA modal header not displayed");

		}
		return this;
	}

	/**
	 * select future date
	 */
	public OneTimePaymentPage clickOnDigitalPAcontinueBtn() {
		try {
			digitalPAContinueBtn.click();

		} catch (Exception e) {
			Assert.fail("digital PA Continue Btn not found");
		}
		return this;
	}

	public OneTimePaymentPage verifyOTPVariantPage() {
		checkPageIsReady();
		try {
			otpVariantPage.isDisplayed();
			Assert.assertTrue(otpVariantPage.getText().contains("Please pay the past-due amount"),
					"OTP variant PAge header not displayed");
			Reporter.log("OTP variant PAge header is displayed");
		} catch (Exception e) {
			Assert.fail("OTP variant PAge header not displayed");

		}
		return this;
	}

	/***
	 * Verify the message below total Balance
	 * 
	 * @return
	 */
	public OneTimePaymentPage verifyTextUnderTotalBal() {
		try {
			Assert.assertTrue(textUnderTotalBal.isDisplayed(), "Message below Total balance is not Displayed");
			Assert.assertTrue(textUnderTotalBal.getText().contains("Total Balance"),
					"Message under Total Balance is not correct");
			Reporter.log("Verified inline Message under Total balance");
		} catch (Exception e) {
			Assert.fail("Inline message component missing under Total balance");
		}
		return this;
	}

	/**
	 * verify that chevron is not displayed on date blade
	 */
	public void verifyNoChevronOnDateBlade() {
		try {
			dateChevron.isDisplayed();
			Assert.fail("Date chevron is displayed");
		} catch (Exception e) {
			Reporter.log("Date chevron is not displayed");
		}
	}

	/**
	 * verify Balance Amount Suppressed in Balance text for Non Master user
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyBalanceAmountSuppressedinBalancetextforNonMasterUser(MyTmoData myTmoData) {
		try {
			totalDueAmount.getText().toLowerCase().startsWith("due");
			Reporter.log("Balance Amount Suppressed in Balance text  for" + myTmoData.getPayment().getUserType());
		} catch (Exception e) {
			Assert.fail("Balance Amount not Suppressed  in Balance text for" + myTmoData.getPayment().getUserType());
		}
		return this;
	}

	/**
	 * verify Balance Amount Suppressed in last payment text for Non Master user
	 * 
	 * @return boolean
	 */
	public OneTimePaymentPage verifyBalanceAmountSuppressedinLastpaymenttextforNonMasterUser(MyTmoData myTmoData) {
		/*
		 * try {
		 * 
		 * if(lastPaymentText.isDisplayed()) { String[] lastPaymenttext =
		 * lastPaymentText.getText().split("Last Payment");
		 * Assert.assertTrue(lastPaymenttext[1].startsWith("on"), ""); Reporter.log(
		 * "Balance Amount Suppressed in last payment text for Non Master user for"
		 * +myTmoData.getPayment().getUserType()); } } catch (Exception e) {
		 * 
		 * }
		 */

		try {

			if (lastPaymentText.isDisplayed()) {
				String[] lastPaymenttext = lastPaymentText.getText().split("Last Payment");
				Assert.assertTrue(lastPaymenttext[1].startsWith("on"), "");
				Reporter.log("Balance Amount Suppressed in last payment text for Non Master user for"
						+ myTmoData.getPayment().getUserType());
			} else {
				Assert.fail("Failed to verify Balance Amount Suppressed in last payment text for Non Master user for "
						+ myTmoData.getPayment().getUserType());
			}
		} catch (Exception e) {
			if (e.toString().contains("NoSuchElementException")) {
				Reporter.log("Verified Balance Amount Suppressed in last payment text for Non Master user for"
						+ myTmoData.getPayment().getUserType());
			} else {
				Assert.fail("Failed to verify Balance Amount Suppressed in last payment text for Non Master user for"
						+ myTmoData.getPayment().getUserType());
			}
		}
		return this;
	}
	
	
	

}