package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class ChargeAccountPage extends CommonPage {

	private final String pageUrl = "charges/line/charge/Account";

	@FindBy(css = ".d-none.d-lg-inline-block")
	private WebElement backToSummaryLink;

	@FindBy(css = "i.fa.fa-angle-left")
	private WebElement arrowLeft;

	@FindBy(css = "i.fa.fa-angle-right")
	private WebElement arrowRight;

	@FindBy(css = "bb-slider div.bb-slider-item-amount")
	private List<WebElement> amountBlock;

	@FindBy(css = "img.loader-icon")
	private WebElement pageSpinner;

	@FindBy(css = "span.bb-back")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//bb-slider//div[contains(text(),'Account')]")
	private WebElement accountTabInSlider;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public ChargeAccountPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ChargeAccountPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @throws InterruptedException
	 */
	public ChargeAccountPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Charge Account page is not present");
		}
		return this;
	}

	/**
	 * Verify page elements
	 *
	 * @throws InterruptedException
	 */
	public ChargeAccountPage verifyChargeAccountPageElements() {
		try {
			Assert.assertTrue(backToSummaryLink.isDisplayed(), "Back To Summary link is not displayed");
			Assert.assertTrue(accountTabInSlider.isDisplayed(), "Equipment header is not displayed");
			Assert.assertTrue(arrowLeft.isDisplayed(), "Arrow left is not displayed");
			Assert.assertTrue(arrowRight.isDisplayed(), "Arrow right is not displayed");
			for (WebElement amount : amountBlock) {
				Assert.assertTrue(amount.getText().contains("$"), "Dollar sign is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Fail to verify Charge Account page elements");
		}
		return this;
	}

	/**
	 * Click Back To Summary Link
	 * 
	 * @return
	 */
	public ChargeAccountPage clickBackToSummaryLink() {
		try {
			backToSummaryLink.click();
		} catch (Exception e) {
			Assert.fail("Back to summary link is not displayed");
		}
		return this;
	}
}
