package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class PlayStorePage extends CommonPage {

	private static final String pageUrl = "play";

	@FindBy(css = "p[class='Display6']")
	private List<WebElement> profilePageLinks;

	public PlayStorePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify PAH Registration page.
	 *
	 * @return the PlayStorePage class instance.
	 */
	public PlayStorePage verifyPlayStorePage() {
		try {
			verifyPageUrl(pageUrl);
			Reporter.log("Play store page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Play store page not displayed");
		}
		return this;
	}
}
