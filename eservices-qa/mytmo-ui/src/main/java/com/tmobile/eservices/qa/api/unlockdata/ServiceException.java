package com.tmobile.eservices.qa.api.unlockdata;

/**
 * This class has handled all soap service errors.. 
 * @author blakshminarayana
 *
 */
public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = -6989830089962017674L;
	 /** 
     * Initialize with message and cause.
     * @param message
     * @param cause
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Initialize with message alone.
     * @param message
     */
    public ServiceException(String message) {
        super(message);
    }
    
}
