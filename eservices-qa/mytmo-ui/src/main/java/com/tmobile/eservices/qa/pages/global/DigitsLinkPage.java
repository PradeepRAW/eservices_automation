/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class DigitsLinkPage extends CommonPage {

	public DigitsLinkPage(WebDriver webDriver) {
		super(webDriver);
	}

	private static final Logger logger = LoggerFactory.getLogger(DigitsLinkPage.class);
	
	@FindBy(css = "div.headerDetailsContents h1")
	private WebElement pageTitle;
	
	/**
	 * Verify SignalBoosterLink Page.
	 *
	 * @return the SignalBoosterLinkPage class instance.
	 */
	public DigitsLinkPage verifyDigitsLinkPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(pageTitle),Constants.VERIFY_MY_DIGITS_PAGE_ERROR);			
			Reporter.log("MyDigits page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MyDigits page not displayed");
		}
		return this;

	}
}
