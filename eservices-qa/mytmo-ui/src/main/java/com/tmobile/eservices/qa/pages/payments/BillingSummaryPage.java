package com.tmobile.eservices.qa.pages.payments;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
/**
 * @author gsrujana
 *
 */
public class BillingSummaryPage extends CommonPage {

	@FindBy(css = "span#paperlessBillPmtText")
	private WebElement paperLessBilling;

	@FindBy(css = "div#paperlessContent")
	private WebElement paperLessBillingMobile;

	@FindBy(linkText = "View bill details")
	private WebElement viewBillDetails;

	@FindBy(linkText = "View usage summary")
	private WebElement viewUsageDetails;

	@FindAll({ @FindBy(id = "img_DropDownIcon"),
			@FindBy(css = "span.pull-right.ui_mobile_primary_link.cdrpassdropdetail") })
	private WebElement datesDropDown;

	@FindAll({ @FindBy(css = "div#paperless_enroll_status"), @FindBy(id = "paperlessStatusLink") })
	private WebElement paperLessEnrolled;

	@FindAll({ @FindBy(css = "div.billing-table-head div.billing-lastmonth"), @FindBy(id = "amountHeading") })
	private WebElement tableHeaderChange;

	@FindAll({ @FindBy(css = "#bill-summary-container > div.co_new-charges > div > div.ui_mobile_caps_headline_black"),
			@FindBy(id = "currentChargesHeading") })
	private WebElement currentChargeHeader;

	@FindBy(css = "div.row-fluid div.bill-title span.ui_headline")
	private WebElement verifyBillSummaryPage;

	@FindBy(css = "[class*='pull-left mt10 bill-title'] span.ui_headline")
	private WebElement billingSummary;

	@FindBy(css = "div.ui_mobile_headline")
	private WebElement billingSummaryMobile;

	@FindAll({ @FindBy(id = "billHighlightsHeader"), @FindBy(id = "billhighlights_title") })
	private WebElement billingHighlight;

	@FindBy(css = "div#downloadButtonsDiv span a")
	private List<WebElement> downloadCurrentPdfIcon;

	@FindAll({ @FindBy(css = "div#billCycleSelector span img#img_DropDownIcon"),
			@FindBy(css = "span img.DropArwPadding") })
	private WebElement billSelector;

	@FindAll({ @FindBy(css = "ul#di_billCycleDropDown li a span"),
			@FindBy(css = "ul#di_billCycleDropDown li a div div") })
	private List<WebElement> billCycleElements;

	@FindAll({ @FindBy(css = "#installmentPlanDiv div span[class*='ui_darkbody_bold']"),
			@FindBy(css = "div[class*='co_eip_balance'] > div[class*='pt15']") })
	private WebElement eipHeaderText;

	@FindBy(css = ".billing-table-row.currentChargesRow")
	private List<WebElement> currentChargesRow;

	@FindBy(css = "div[id*='currentChargesRow'] > div div.ui_mobile_darkbody_bold")
	private List<WebElement> currentChargesRowMobile;

	@FindBy(css = "a#view_bill_graph span")
	private WebElement viewBillGraph;

	@FindAll({ @FindBy(id = "date_billcycle"), @FindBy(id = "selectedBillingCycle_graph") })
	private WebElement graphMonths;

	@FindAll({ @FindBy(css = "div#billCycleSelector span#di_CDRbillCycle a span#di_billCycle0"),
			@FindBy(xpath = "//*[@id='di_CDRbillCycle']") })
	private WebElement selectedBillCycle;

	@FindBy(css = "div.modal-dialog.modal-sm button[data-dismiss='modal']")
	private WebElement closeGraph;

	@FindBy(css = "div.ajax_loader")
	private List<WebElement> spinners;

	// @FindBy(css = "img.bb-loader-icon.hide-loader")
	@FindBy(css = "img.bb-loader-icon")
	private WebElement brightBillSpinner;

	// @FindBy(xpath = "//div[contains(text(),'Credits and adjustments')]")
	@FindBy(css = "div.commonCol.billing-credits")
	private WebElement creditsAndAdjustments;

	@FindBy(id = "billSummary_description2")
	private WebElement creditsAndAdjustment;

	@FindAll({ @FindBy(id = "creditsAdjustmentsHeading"), @FindBy(id = "crdtheader_id") })
	private WebElement creditsAndAdjustmentsHeading;

	@FindBy(css = "#creditsAdjustments .close")
	private WebElement creditsAdjustmentsClose;

	@FindBy(css = "div#choose-Bill span.ui_mobile_subhead")
	private WebElement chooseBillVersionHeaderMobile;

	@FindAll({ @FindBy(id = "di_downloadcurrentpdficon"), @FindBy(id = "downloadBillDiv") })
	private WebElement downloadPDFIcon;

	@FindBy(id = "billSummary_amount3")
	private WebElement currentChagesAmount;
	// private List<WebElement> currentChagesAmount;

	@FindAll({ @FindBy(id = "easypay_dueDate"), @FindBy(id = "paymentdateEasyPay") })
	private WebElement autoPayschedulesDatemessageBilling;

	@FindAll({ @FindBy(id = "payWith-cardType"), @FindBy(id = "cardImageEasyPay"),
			@FindBy(css = "span#easyPay_cardImage") })
	private WebElement autoPaycardIconBillingsummary;

	@FindBy(css = "div.easyPayInnerDiv.ui_payacc")
	private WebElement autoPaycardIconBillingsummaryMobile;

	@FindAll({@FindBy(id = "img_DropDownIcon"), @FindBy(id ="changedropdown") })
	private WebElement changeDropdownMobile;

	@FindAll({@FindBy(id = "billCycleElement0") , @FindBy(id="di_billCycle1")})
	private WebElement selectBillcycleMobile;

	// @FindAll({@FindBy(id="currentCharges_panel")})
	@FindAll({ @FindBy(css = "#currentCharges_panel") })
	private WebElement currentChargesPanel;

	@FindBy(css = "div#legalTextTotal")
	private WebElement legalText;

	@FindBy(css = "div.co_new_charges_header div")
	private List<WebElement> currentChargesPanelMobile;

	@FindBy(id = "summary_balanceDue")
	private WebElement totalAmount;

	/// ***************

	@FindBy(id = "installmentPlanLink")
	private WebElement InstallemntArrow;

	@FindBy(css = "div#installmentPlanDiv div#eipArrow")
	private WebElement InstallemntArrowMobile;

	// @FindBy(css = "a#easypayStatusLink")
	@FindAll({ @FindBy(css = "div#easypayStatusdeskDiv"), @FindBy(css = "div#autopayonnod span") })
	public WebElement autopayONAlert;

	@FindBy(css = "div#autopayonwithd")
	public WebElement autopayONAlertMobile;

	@FindAll({ @FindBy(css = "div#paTitle"), @FindBy(css = "div#pmtArrangementContent div span.ui_darkbody_bold_15") })
	public WebElement pAText;

	@FindBy(css = "div#installmentPlanDiv span.ui_darkbody_bold")
	public WebElement eipText;

	@FindBy(css = "div#installmentPlanDiv span.ui_darkbody_bold_15")
	public WebElement eipTextMobile;

	@FindBy(css = "span#installmentPlanLink")
	public WebElement eipSubAlert;

	@FindBy(css = "span#installmentPlanLink")
	public WebElement eipSubAlertMobile;

	@FindAll({ @FindBy(css = "div#paSubTitle"), @FindBy(css = "span#paSubhead1") })
	public WebElement pASubAlert;

	@FindBy(css = "div#autooffsubdesk")
	public WebElement AutopayOFFSubAlert;

	// @FindBy(css = "div#autopayoffsub span")
	@FindAll({ @FindBy(css = "div#autopayoffwithd"), @FindBy(css = "div#autopayoffsub span"),
			@FindBy(css = "div#easypayStatusDiv div.ui-header-rt-arrow") })
	public WebElement AutopayOFFSubAlertMobile;

	@FindBy(css = "span[id*='auto'][class*='show']")
	public WebElement AutopayOFFAlert;

	/*
	 * @FindBy(css = "div#autopayoffnod span") public WebElement
	 * AutopayOFFAlertMobile;
	 */

	@FindAll({ @FindBy(css = "div#autopayoffwithd"), @FindBy(css = "div#autopayoffnod") })
	public WebElement AutopayOFFAlertMobile;

	@FindAll({ @FindBy(css = "div#autopaystandardsub"), @FindBy(css = "span#autoscnctitledesk"),
			@FindBy(css = "div#autopaystandardsub span") })
	public WebElement restrictedUserAutopayAlert;

	@FindBy(css = "span#paperlessBillPmtText")
	private WebElement paperlessBillingText;

	@FindBy(css = "div#autopayArrow span.paymentarrow")
	private WebElement easyPayArrow;

	@FindBy(css = "div#billSettingChange")
	private WebElement paymnetsSettingstab;

	// @FindBy(css = "div#easypayStatusDiv div.ui-header-rt-arrow")
	@FindBy(xpath = "//span[contains(text(), 'Set up recurring bill payments')]")
	private WebElement easyPayArrowMobile;

	@FindAll({ @FindBy(css = "div#autoonsubdesk"), @FindBy(css = "span#autopaydate") })
	public WebElement autopayDueDateAlert;

	@FindAll({ @FindBy(css = "div#di_leasesummary div.ui_subhead"), @FindBy(css = "div#innerLease span") })
	public WebElement jumpOndemandHeader;

	@FindAll({ @FindBy(css = "div#leaseMsgId"), @FindBy(css = "div#activeLease div.ui_mobile_smalltext") })
	public WebElement leaseMsg;

	@FindAll({ @FindBy(css = "div#containerDetails"), @FindBy(css = "div#currentLeasesHeader div h6") })
	public WebElement jumponDemandLeasePage;

	@FindAll({ @FindBy(css = "div#viewleasedetails a"), @FindBy(css = "div#viewleasedetails a") })
	public WebElement viewleasedetails;

	@FindBy(id = "noDownloadPermission")
	public WebElement noDownloadPermission;

	@FindBy(id = "summaryBill")
	private WebElement downloadSummaryBill;

	@FindBy(id = "detailedBill")
	private WebElement downloaddetailedBill;

	@FindBy(css = "div#getBill_panel > div.home-alert-heading")
	public WebElement getyourBillHeader;

	@FindBy(css = "div#paymentHistoryBlade span.ui_darkbody_bold")
	private WebElement paymentHistoryText;

	@FindBy(css = "span#pmtHistoryLabel")
	private WebElement paymentHistoryTextMobile;

	@FindBy(css = "div#paymentArrangementDiv span.paymentarrow")
	private WebElement paArrow;

	@FindBy(css = "div#paperlessStatusLink")
	private WebElement paperLessBillStatus;

	@FindBy(css = "span#paperlessStatusLinked")
	private WebElement paperLessBillStatusMobile;

	@FindBy(css = "span#enrolledText")
	private WebElement paperLessBillONStatus;

	@FindBy(xpath = "//div[contains(text(),'Current charges')][contains(@id,'billSummary_description')]")
	private WebElement currentChargesMobile;

	@FindBy(xpath = "//div[contains(text(),'Current charges')][contains(@id,'billSummary_description')]")
	private WebElement currentChargesDesktop;

	@FindBy(xpath = "//div[contains(text(),'Recurring charges')][contains(@id,'billSummary_description')]")
	private WebElement recurringChargesMobile;

	@FindBy(xpath = "//div[contains(text(),'Recurring charges')][contains(@class,'ui_body')]")
	private WebElement recurringChargesDesktop;

	@FindBy(xpath = "//div[contains(text(),'Misc. charges')][contains(@id,'billSummary_description')]")
	private WebElement miscChargesMobile;

	@FindBy(xpath = "//div[contains(text(),'Misc. charges')][contains(@class,'ui_body')]")
	private WebElement miscChargesDesktop;

	@FindBy(css = "div.billing-amount[id*='totalCharge']")
	private WebElement totalChargeAmount;

	@FindBy(css = "div.billing-amount[id*='cell']")
	private List<WebElement> amountCells;

	// Gopi
	@FindBy(xpath = "//div[@class='billing-table-row currentChargesRow']/div/span[@class='ui_body']")
	private List<WebElement> lineCells;

	private final String pageUrlEBill = "billing";

	private final String pageUrlBrightBill = "billandpay";

	@FindBy(id = "accountNumber")
	private WebElement accountNo;

	@FindBy(css = "span[id*='subscriberName']")
	private List<WebElement> subscriberNames;

	@FindBy(css = "span[id*='subscriberMsisdn']")
	private List<WebElement> subscriberNos;

	@FindBy(id = "currentChargesDiv")
	private WebElement currentChargesDiv;

	@FindBy(css = "div#viewleasedetails>a")
	private WebElement viewLeaseDetails;

	@FindBy(css = "div.bb-shortcut-title")
	private List<WebElement> billingActionOptions;

	@FindBy(css = "div#myPromotionsDiv span.ui_darkbody_bold")
	private WebElement myPromotionBladeHeader;

	@FindBy(css = "div#myPromotionsDiv span.ui_darkbody_bold_15")
	private WebElement mobileMyPromotionBladeHeader;

	@FindBy(css = "div#myPromotionsDiv img.payment-hub-icon")
	private WebElement myPromotionBladeIcon;

	@FindBy(css = "div#myPromotionsDiv img.payment-hub-icon")
	private WebElement mobileMyPromotionBladeIcon;

	@FindBy(css = "div#myPromotionsDiv span.paymentarrow")
	private WebElement myPromotionBladeChevron;

	@FindBy(css = "div#myPromotionsDiv div#myPromotionsArrow")
	private WebElement mobileMyPromotionBladeChevron;

	@FindBy(css = "a.btn.btn-primary.pay-now-btn.pull-right")
	private WebElement payNow;
	
	@FindBy(xpath = "//span[contains(@pid,'cust_msisdn')]")
	private List<WebElement> viewMsisdinByCurrentCharges;


    @FindBy(id = "paymentdateEasyPay")
    private WebElement paymentdateEasyPay;
    
    @FindBy(id = "cardImageEasyPay")
    private WebElement cardImageEasyPay;
    
    @FindBy(id = "cardDetailEasyPay")
    private WebElement cardDetailEasyPay;


	public static final String billingPageUrl = "/bill";

	/**
	 * 
	 * @param webDriver
	 */
	public BillingSummaryPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Click On credits Adjustments Close
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			verifyDomRendered();
			checkPageIsReady();
			if (getDriver().getCurrentUrl().contains(pageUrlEBill)) {
				waitFor(ExpectedConditions.invisibilityOfAllElements(spinners));
			} else if (getDriver().getCurrentUrl().contains(pageUrlBrightBill)) {
				waitFor(ExpectedConditions.invisibilityOf(brightBillSpinner));
			}
			Reporter.log("Bill Summary page is displayed");
		} catch (Exception e) {
			Assert.fail("Bill Summary page is not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public BillingSummaryPage verifyPageUrl() {
		if (getDriver().getCurrentUrl().contains(pageUrlEBill)) {
			Reporter.log("EBill url is displayed");
		} else if (getDriver().getCurrentUrl().contains(pageUrlBrightBill)) {
			Reporter.log("BrightBill url is displayed");
		} else
			Assert.fail("Url not found or not in correct page");
		return this;
	}

	/**
	 * Click On credits Adjustments Close
	 * 
	 * @return
	 */
	public BillingSummaryPage clickOnCreditsAdjustmentsClose() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				getDriver().navigate().back();
				Reporter.log("Navigated back to Billing Summary page");
			} else {
				getDriver().navigate().back();
				// clickElementWithJavaScript(creditsAdjustmentsClose);
				Reporter.log("Clicked on Credits Adjustments close");
			}
		} catch (Exception e) {
			Assert.fail("Fail to click on credits Adjustments close");
		}
		return this;
	}

	/**
	 * Get Manage settings Text
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyCreditsAndAdjustmentsHeading() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				getDriver().getCurrentUrl().contains("credit");
			} else {
				creditsAndAdjustmentsHeading.isDisplayed();
			}
			Reporter.log("Credits and adjustments overlay is displaying ");
		} catch (Exception e) {
			Assert.fail("Fail to verify Credists and adjustments Header");
		}
		return this;
	}

	/**
	 * Click On Credits AndAdjustments
	 * 
	 * @return
	 */
	public BillingSummaryPage clickOnCreditsAndAdjustments() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				creditsAndAdjustment.click();
			} else {
				creditsAndAdjustments.click();
			}
			Reporter.log("Clicked on Credits and Adjustments");
		} catch (Exception e) {
			Assert.fail("Credits and adjustments not found");
		}
		return this;
	}

	/**
	 * Click View Bill Details
	 */
	public BillingSummaryPage clickViewBillDetails() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickCurrentChargesRow();
			} else {
				waitFor(ExpectedConditions.visibilityOf(viewBillDetails));
				viewBillDetails.click();
				Reporter.log("Clicked on View Bill Details");
			}
		} catch (Exception e) {
			Assert.fail("Fail to click on view bill details");
		}
		return this;
	}

	/**
	 * verify View Bill Details link
	 * 
	 * @return
	 */

	public BillingSummaryPage verifyViewBillDetailsLinkDisplayed() {
		try {

			viewBillDetails.isDisplayed();
			Reporter.log("View Bill Details displayed");

		} catch (Exception e) {
			Assert.fail("View Bill Details not displayed");
		}
		return this;
	}

	/**
	 * Click View Bill Details
	 */
	public BillingSummaryPage clickUsageDetails() {
		try {
			viewUsageDetails.click();
			Reporter.log("Clicked on View Usage Details");
		} catch (Exception e) {
			Assert.fail("Fail to click on view Usage details");
		}
		return this;
	}

	/**
	 * Get Table Header Change
	 * 
	 * @return
	 */
	public BillingSummaryPage getTableHeaderChange(String msg) {
		try {
			tableHeaderChange.getText().equalsIgnoreCase(msg);
			Reporter.log("Table header change from last month' is displayed");
		} catch (Exception e) {
			Verify.fail("Table header 'change from last month' is not displayed");
		}
		return this;
	}

	/**
	 * Verify Bill Summary Page Header
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyBillSummaryPageHeader() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				billingSummaryMobile.getText().equals("Bill summary");
			} else {
				verifyBillSummaryPage.getText().equals("Bill summary");
			}
			Reporter.log("Billing summary page header is loaded");
		} catch (Exception e) {
			Assert.fail("Fail to verify the Bill SUmmary Page Header");
		}
		return this;
	}

	/**
	 * Click View Details
	 */
	public BillingSummaryPage clickViewDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(eipHeaderText));
			moveToElement(eipHeaderText);
			clickElementWithJavaScript(eipHeaderText);
			Reporter.log("Clicked on View Details");
		} catch (Exception e) {
			Assert.fail("Fail to click vew details");
		}
		return this;
	}

	/**
	 * Verify View Details
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyViewDetails() {
		try {
			eipHeaderText.isDisplayed();
			Reporter.log("EIP section & View details section is displayed");
		} catch (Exception e) {
			Assert.fail(" View Details is not displayed");
		}
		return this;
	}

	/**
	 * verify if billing highlights section is displayed
	 * 
	 * @return boolean
	 */
	public BillingSummaryPage verifyBillingHighLightsSection() {
		try {
			checkPageIsReady();
			getDriver().manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
			billingHighlight.isDisplayed();
		} catch (Exception e) {
			Verify.fail("Fail to verify Billing Highlights Section");
		}
		return this;
	}

	/**
	 * Click Download PDF
	 */
	public BillingSummaryPage clickDownLoadPDF() {
		try {
			for (WebElement downloadPDF : downloadCurrentPdfIcon) {
				if (downloadPDF.isDisplayed()) {
					moveToElement(downloadPDF);
					clickElementWithJavaScript(downloadPDF);
					break;
				}
			}
			Thread.sleep(10000);
		} catch (Exception e) {
			Verify.fail("Download PDF not found");
		}
		return this;
	}

	/**
	 * Verify Paper Less Link
	 * 
	 * @return
	 */
	public BillingSummaryPage checkPaperLessLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paperLessEnrolled));
			paperLessEnrolled.isDisplayed();
			Reporter.log("paper less billing section is displayed");
		} catch (Exception e) {
			Verify.fail("Fail to check paper Less link");
		}
		return this;
	}

	/**
	 * @return billingSummary
	 */
	public BillingSummaryPage getBillingSummary() {
		if (getDriver() instanceof AppiumDriver) {
			waitFor(ExpectedConditions.visibilityOf(billingSummary));
		} else {
			waitFor(ExpectedConditions.visibilityOf(billingSummary));
		}
		return this;
	}

	/**
	 * Select Bill Cycle
	 * @return
	 */
	public BillingSummaryPage selectBillCycleDesktop() {
		try {
			checkPageIsReady();
			if (!currentChargesDiv.isDisplayed()) {
				waitFor(ExpectedConditions.visibilityOf(billSelector));
				billSelector.click();
				waitFor(ExpectedConditions.visibilityOfAllElements(billCycleElements));
				for (WebElement webElement : billCycleElements) {
					webElement.click();
					if (currentChargesDiv.isDisplayed()) {
						break;
					} else {
						billSelector.click();
					}
				}
				Reporter.log("Selected the bill cycle elements");
			}
		} catch (Exception e) {
			Assert.fail("Fail to get select bill cycle");
		}
		return this;
	}

	/**
	 * verify Pay now button disabled for previous cycles
	 */
	public BillingSummaryPage verifypayNowButtonDisabledForPreviousBillCycles() {
		try {
			checkPageIsReady();
			Thread.sleep(2000);
			for (int i = 1; i <= billCycleElements.size(); i++) {
				billSelector.click();
				checkPageIsReady();
				billCycleElements.get(i).click();
				// add code to payNow disabled
			}
		} catch (Exception e) {
			Assert.fail("Fail to get select bill cycle");
		}
		return this;
	}

	/**
	 * verify the eip header text which has Equipment Installment Plan (EIP)
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyEipHeaderText() {
		try {
			eipHeaderText.getText().equalsIgnoreCase("Installment Plan");
			Reporter.log("Equipment Installation Plan Section loaded");
		} catch (Exception e) {
			Assert.fail("Fail to verify the Equipment Installation Plan Section");
		}
		return this;
	}

	/**
	 * click on current charges row
	 */
	public BillingSummaryPage clickCurrentChargesRow() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement currentCharge : currentChargesRowMobile) {
					if (currentCharge.isDisplayed()) {
						currentCharge.click();
						break;
					}
				}
			} else {
				clickElement(currentChargesRow);
				for (WebElement webElement : currentChargesRow) {
					moveToElement(webElement);
					clickElementWithJavaScript(webElement);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Current charges row field not found");
		}
		return this;
	}

	/**
	 * Click Bill History Chart
	 */
	public BillingSummaryPage clickBillHistoryChart() {
		try {
			checkPageIsReady();
			viewBillGraph.click();
			Reporter.log("Clicked onView Bill History Chart graph");
		} catch (Exception e) {
			Assert.fail("Fail to click on Bill History Chart graph");
		}
		return this;
	}

	/**
	 * Click view Lease Details Link
	 */
	public BillingSummaryPage clickViewLeaseDeatils() {
		try {
			checkPageIsReady();
			for (int i = 0; i < billingActionOptions.size(); i++) {
				if (billingActionOptions.get(i).getText().toLowerCase().contains("on demand leases")) {
					clickElementWithJavaScript(billingActionOptions.get(i));
					Reporter.log("Clicked on 'JUMP! On Demand Lease'");
				}
			}
			Reporter.log("Clicked View Lease Details Link");
		} catch (Exception e) {
			Assert.fail("Fail to click on View Lease Details Link");
		}
		return this;
	}

	/**
	 * Verify Graph
	 * 
	 * @return
	 */
	public boolean verifyGraph() {
		boolean graph = false;
		try {
			String selectedMonth = selectedBillCycle.getText();
			selectedMonth = selectedMonth.replace(". ", " ");
			if (selectedMonth.contains(graphMonths.getText().replace(". ", " "))) {
				graph = true;
			}
			Reporter.log("Bill graph information is displayed");
		} catch (Exception e) {
			Assert.fail("Bill graph is not found");
		}
		return graph;
	}

	/**
	 * Click Close Graph
	 */
	public BillingSummaryPage clickCloseGraph() {
		try {
			checkPageIsReady();
			closeGraph.click();
			Reporter.log("Clicked on Close Graph");
		} catch (Exception e) {
			Assert.fail("Fail to click on close Graph");
		}
		return this;
	}

	/**
	 * this method is to wait for all the loading spinners invisibility
	 */
	public void waitForSpinners() {
		waitFor(ExpectedConditions.invisibilityOfAllElements(spinners));
	}

	/**
	 * click paperless billing signup link
	 */
	public BillingSummaryPage clickPaperLessBilling() {
		if (getDriver() instanceof AppiumDriver) {
			clickElementWithJavaScript(paperLessBillingMobile);
		} else {
			paperLessBilling.click();
		}
		return this;
	}

	/**
	 * Current Charges don't have Any amount
	 */
	public BillingSummaryPage verifyCurrentCharges() {
		try {
			String id;
			if (getDriver() instanceof AppiumDriver) {
				id = currentChargesMobile.getAttribute("id").replaceAll("[^0-9]", "");
			} else {
				id = currentChargesDesktop.getAttribute("id").replaceAll("[^0-9]", "");
			}
			WebElement currentChargesAmount = getDriver().findElement(By.id("billSummary_amount" + id));
			Assert.assertFalse(!currentChargesAmount.getText().isEmpty(), "Current charges row is not found");
			Reporter.log(
					"CurrentCharges Section in the BalanceForward does not have any price displayed in front of it.");
		} catch (Exception e) {
			Assert.fail("Current charges row is not found");
		}
		return this;
	}

	/**
	 * Verify Recurring Charges title and Recurring Charges amount
	 */
	public BillingSummaryPage verifyRecurringCharges() {// Recurring charges
		try {

			String id;
			if (getDriver() instanceof AppiumDriver) {
				// recurringChargesMobile
				id = recurringChargesMobile.getAttribute("id").replaceAll("[^0-9]", "");
			} else {
				id = recurringChargesDesktop.getAttribute("id").replaceAll("[^0-9]", "");
			}
			WebElement recurringChargesAmount = getDriver().findElement(By.id("billSummary_amount" + id));
			Assert.assertFalse(recurringChargesAmount.getText().isEmpty(), "Recurring charges row is not found");
			Reporter.log(
					"Under CurrentCharges Section title show's as Recurring Charges and it Show's Recurring Charges Amount is Displayed.");
		} catch (Exception e) {
			Assert.fail("Recurring charges row is not found");
		}
		return this;
	}

	/**
	 * Verify Misc Charges title and Misc Charges amount
	 */
	public BillingSummaryPage verifyMiscCharges() {// Misc. charges
		try {
			String id;
			if (getDriver() instanceof AppiumDriver) {
				id = miscChargesMobile.getAttribute("id").replaceAll("[^0-9]", "");
			} else {
				id = miscChargesDesktop.getAttribute("id").replaceAll("[^0-9]", "");
			}
			WebElement miscChargesAmount = getDriver().findElement(By.id("billSummary_amount" + id));
			Assert.assertFalse(miscChargesAmount.getText().isEmpty(), "Misc. charges row is not found");
			Reporter.log(
					"Under CurrentCharges Section title show's as Misc Charges and it Show's Misc charges Amount is Displayed.");
		} catch (Exception e) {
			Assert.fail("Misc. charges row is not found");
		}
		return this;
	}

	/**
	 * Verify Auto Pay schedules Date is displayed in Billing page
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyAutoPayscheduledDateBillingpage() {
		try {
			autoPayschedulesDatemessageBilling.isDisplayed();
			Reporter.log("Autopay Payment Scheduled Date message is displayed");
		} catch (Exception e) {
			Verify.fail("Fail to verify Auto Pay scheduled Date Billing page");
		}
		return this;
	}

	/**
	 * Verify Auto Pay Card Icon is displayed
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyAutopayCardiconBillingsummary() {
		try {

			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(autoPaycardIconBillingsummaryMobile));
				autoPaycardIconBillingsummaryMobile.isDisplayed();
			} else {

				waitFor(ExpectedConditions.visibilityOf(autoPaycardIconBillingsummary));
				autoPaycardIconBillingsummary.isDisplayed();
			}
		} catch (Exception e) {
			Assert.fail("Autopay icon Billing Summary");
		}
		return this;
	}

	/**
	 * Click Billing cycle dates Drop down
	 */
	public BillingSummaryPage clickDatesDropDown() {
		try {
			datesDropDown.click();
			Reporter.log("Clicked onDates dates drop down");
		} catch (Exception e) {
			Assert.fail("Fail to click on Dates drop Down");
		}
		return this;
	}

	/**
	 * Verify previous bill cycle from bill cycle drop down > 1
	 */
	public BillingSummaryPage verifyBillingCyclePastBills() {
		try {
			if (billCycleElements.size() > 1) {
				Reporter.log("Past bills exists");
			} else
				Assert.fail("Past bills doesn't exists");
		} catch (Exception e) {
			Reporter.log("Billing cycle Past bills component missing");
		}
		return this;
	}

	/**
	 * Select Bill Cycle
	 * @return
	 */
	public BillingSummaryPage selectBillcycleMobile() {
		try {
			checkPageIsReady();
			changeDropdownMobile.click();
			waitFor(ExpectedConditions.visibilityOf(selectBillcycleMobile),3);
			selectBillcycleMobile.click();
			Reporter.log("selected Bill cycle Mobile");
		} catch (Exception e) {
			Assert.fail("Fail to select Bill cycle Mobile");
		}
		return this;
	}

	/**
	 * verify Taxed and fees tab is not displayed in current charges
	 * 
	 * @param taxAndFees
	 * @return true/false
	 */
	public BillingSummaryPage verifyTaxesandFeesSection(String taxAndFees) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement headers : currentChargesPanelMobile) {
					if (headers.isDisplayed() && headers.getText().contains(taxAndFees)) {
						Reporter.log("Taxes and fees displayed");
					}
				}
			} else {
				if (currentChargesPanel.isDisplayed()) {
					if (!(legalText.getText().toUpperCase()).contains(taxAndFees.toUpperCase())) {
						Reporter.log("Verify Taxes and Fee column Not displayed");

					} else {
						Assert.fail("Verify Taxes and Fee column Not displayed");
					}
				}
			}
		} catch (Exception elemNotFound) {
			Assert.fail("Taxes and fees element missing");
		}
		return this;
	}

	public BillingSummaryPage verifyEipText() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("paymentSettingLoader")));
			if (getDriver() instanceof AppiumDriver) {
				scrollToElement(paymnetsSettingstab);
				waitFor(ExpectedConditions.visibilityOf(eipTextMobile));
				eipTextMobile.getText().contains("Installment Plan");
			} else {
				waitFor(ExpectedConditions.visibilityOf(eipText));
				eipText.getText().contains("Installment Plan");
			}
		} catch (Exception e) {
			Verify.fail("EIP text component not found");
		}
		return this;
	}

	public BillingSummaryPage verifyEipSubAlertText() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(eipSubAlertMobile));
				eipSubAlertMobile.getText().contains("Pay or view device balance");
				Reporter.log("Verify EIP sub alert");
			} else {
				waitFor(ExpectedConditions.visibilityOf(eipSubAlert));
				eipSubAlert.getText().contains("Pay or view device balance");
				Reporter.log("Verify EIP sub alert");
			}
		} catch (Exception e) {
			Verify.fail("EIP sub alert not found");
		}
		return this;
	}

	public BillingSummaryPage clickInstallemntArrow() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				scrollToElement(InstallemntArrowMobile);
				clickElementWithJavaScript(InstallemntArrowMobile);
			} else {
				// scrollToElement(InstallemntArrow);
				// moveToElement(InstallemntArrow);
				clickElement(InstallemntArrow);
			}
		} catch (Exception e) {
			Verify.fail("Installment arrow not found");
		}
		return this;
	}

	public BillingSummaryPage verifyAutopayAlertwhenOFf() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				scrollToElement(paymnetsSettingstab);
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf(AutopayOFFAlertMobile));
				AutopayOFFAlertMobile.getText().contains("AutoPay: Enroll");
				Reporter.log("Verified Auto pay alert");
			} else {
				waitFor(ExpectedConditions.visibilityOf(AutopayOFFAlert));
				AutopayOFFAlert.getText().contains("Enroll & Save");
				Reporter.log("Verified Auto pay alert");
			}

		} catch (Exception e) {
			Verify.fail("Autopay alert not found");
		}
		return this;
	}

	public BillingSummaryPage verifyAutopaySubAlertwhenOFf() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(AutopayOFFSubAlertMobile));
				AutopayOFFSubAlertMobile.getText().contains("Set up recurring bill payments");
			} else {
				waitFor(ExpectedConditions.visibilityOf(AutopayOFFSubAlert));
				AutopayOFFSubAlert.getText().contains("Set up recurring bill payments");
			}
			Reporter.log("Autopay Off alert is displayed");
		} catch (Exception e) {
			Verify.fail("Auto pay sub alert not found");
		}
		return this;
	}

	public void clickEasyPayArrow() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				scrollToElement(paymnetsSettingstab);
				checkPageIsReady();
				clickElement(easyPayArrowMobile);

				Reporter.log("Clicked on EasyPay arrow");
			} else {
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf((easyPayArrow)));
				easyPayArrow.click();
				Reporter.log("Clicked on EasyPay arrow");
			}
		} catch (Exception e) {
			Verify.fail("Easy pay arrow not found");
		}

	}

	public BillingSummaryPage verifyPaymentArrangementText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(pAText));
			pAText.getText().contains("Payment Arrangement");
			Reporter.log("Verified payment arrangement text");
		} catch (Exception e) {
			Assert.fail(
					"Payment arrangement text is not displayed or This data may not  eligible for Payment Arrangement");
		}
		return this;
	}

	public BillingSummaryPage verifyPASubAlert() {
		try {
			pASubAlert.isDisplayed();
			Reporter.log("PA sub alert is displayed.");
		} catch (Exception e) {
			Assert.fail("PA sub alert message is not displayed.");
		}
		return this;
	}

	public BillingSummaryPage clickPAArrow() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(pAText));
				pAText.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(paArrow));
				paArrow.click();
			}
			Reporter.log("Clicked on PA Alert link on Payment Hub");
		} catch (Exception e) {
			Assert.fail("PA arrow missing");
		}
		return this;
	}

	public BillingSummaryPage verifyPASubTitileForInEligibleUser() {
		try {
			waitFor(ExpectedConditions.visibilityOf(pASubAlert));
			pASubAlert.getText().contains("Your account is not eligible at this time");
			Reporter.log("Verified PA Sub title for ineligible user");
		} catch (Exception e) {
			Assert.fail("Payment Arrangement ineligibility alert is not displayed");
		}
		return this;
	}

	public BillingSummaryPage verifypaperlessBillingTextDisplayed(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(paperlessBillingText));
			paperlessBillingText.getText().contains(msg);
			Reporter.log("Verfied the Paperless Billing");
		} catch (Exception e) {
			Assert.fail("Fail to verify Paperless Billing");
		}

		return this;
	}

	public BillingSummaryPage verifyPaperLessBillingStatusON() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(paperlessBillingText));
				paperlessBillingText.getText().contains("enrolled");

			} else {
				waitFor(ExpectedConditions.visibilityOf(paperLessBillONStatus));
				paperLessBillONStatus.getText().contains("Enrolled");
				paperLessBillStatus.getText().contains("Receive your bill electronically");
			}
			Reporter.log("Verified paperless billing status");
		} catch (Exception e) {
			Assert.fail("Paperless billing status not found");
		}
		return this;
	}

	public BillingSummaryPage verifyRestrictedUserAlertForPaperLessBilling() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(paperLessBillStatusMobile));
				paperLessBillStatusMobile.getText().contains("Contact Primary Account Holder to modify");
			} else {
				waitFor(ExpectedConditions.visibilityOf(paperLessBillStatus));
				paperLessBillStatus.getText().contains("Contact Primary Account Holder to modify");
			}
			Reporter.log("Verified restricted user alert");
		} catch (Exception e) {
			Assert.fail("Restricted user alert element missing");
		}
		return this;
	}

	public BillingSummaryPage verifyPaymentHistoryText() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(paymentHistoryTextMobile));
				paymentHistoryTextMobile.getText().contains("Payment History");
			} else {
				waitFor(ExpectedConditions.visibilityOf(paymentHistoryText));
				paymentHistoryText.getText().contains("Payment History");
			}
			Reporter.log("Verified payment history text");
		} catch (Exception e) {
			Verify.fail("Payment history text field not found");
		}
		return this;
	}

	public BillingSummaryPage verifyRestrictedUserAutopayMessage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(restrictedUserAutopayAlert));
			System.out.println(restrictedUserAutopayAlert.getText());
			if (restrictedUserAutopayAlert.getText().contains("Contact Primary Account Holder to modify")
					|| restrictedUserAutopayAlert.getText().contains("Your account is not eligible for AutoPay")) {
				Reporter.log("restricted user AutoPay message displayed");
			}

		} catch (Exception e) {
			Assert.fail("Fail to verify the restricted user Autopay Message");
		}
		return this;
	}

	public BillingSummaryPage verifyAutopayAlertwhenON() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(autopayONAlertMobile));
				autopayONAlertMobile.getText().contains("AutoPay: Enrolled");
				Reporter.log("Verified Autopay alert when ON");

			} else {
				waitFor(ExpectedConditions.visibilityOf(autopayONAlert));
				if (autopayONAlert.getText().contains("ON") || autopayONAlert.getText().contains("Enrolled")) {
					Reporter.log("Verified Autopay alert when ON");
				}
			}
		} catch (Exception e) {
			Verify.fail("Auto pay alert component not found");
		}
		return this;
	}

	public BillingSummaryPage verifyGetYourBillHeader(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(getyourBillHeader));
			getyourBillHeader.isDisplayed();
			getyourBillHeader.getText().contains(msg);
			Reporter.log("Verified get bill header");
		} catch (Exception e) {
			Assert.fail("Fail to verify Get your Bill Header ");
		}
		return this;
	}

	public BillingSummaryPage clickDownLoadSummaryBill() {
		try {
			waitFor(ExpectedConditions.visibilityOf(downloadSummaryBill));
			downloadSummaryBill.click();
			Thread.sleep(10000);
			Reporter.log("Clicked on download bill summary");
		} catch (Exception e) {
			Assert.fail("Download summary bill not found");
		}
		return this;
	}

	public BillingSummaryPage clickDownLoadDetailedBill() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(downloaddetailedBill));
			downloaddetailedBill.click();
			Thread.sleep(10000);
			Reporter.log("Clicked on download detail bill");
		} catch (Exception e) {
			throw new FrameworkException("element not found{}", e);
		}
		return this;
	}

	public BillingSummaryPage verifyNoDownLoadPermissionFOrRestrictedUser(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(noDownloadPermission), 2);
			noDownloadPermission.isDisplayed();
			Assert.assertTrue(noDownloadPermission.getText().trim().equalsIgnoreCase(msg));
			Reporter.log("no download permission messgae is displayed");
		} catch (Exception e) {
			Assert.fail("Fail to verify the No download permission for restricted User");
		}
		return this;
	}

	public BillingSummaryPage verifyjumponDemandComponentOnBillingPage() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(jumpOndemandHeader));
			jumpOndemandHeader.isDisplayed();
			jumpOndemandHeader.getText().contains("JUMP! On Demand lease");
		} catch (Exception e) {
			Assert.fail("Jump on demand component ob bill page not found");
		}
		return this;
	}

	public BillingSummaryPage verifyjumponDemandLeaseMessage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(leaseMsg));
			leaseMsg.isDisplayed();
			leaseMsg.getText().contains(
					"Your lease balance may reflect more than one device. Select view details for additional information.");
		} catch (Exception e) {
			Assert.fail("Jump on demand lease message field not found");
		}
		return this;
	}

	public BillingSummaryPage clickviewleasedetails() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(viewleasedetails));
			viewleasedetails.click();
			Reporter.log("Clicked on view lease details");
		} catch (Exception e) {
			Assert.fail("Fail to click on view leased details");
		}
		return this;
	}

	public BillingSummaryPage verifyjumponDemandLeasepage() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(jumponDemandLeasePage));
			jumponDemandLeasePage.isDisplayed();
			Reporter.log("Verified the Jump on Demand Lease Page");
		} catch (Exception e) {
			Assert.fail("Fail to verify the Jump on Demand Lease Page");
		}

		return this;
	}

	public BillingSummaryPage verifyDueDatePastHeader() {
		try {
			waitFor(ExpectedConditions.visibilityOf(autopayDueDateAlert));
			if (autopayDueDateAlert.getText().replaceAll("[0-9]", "").contains("Payment scheduled for")
					|| autopayDueDateAlert.getText().replaceAll("[0-9]", "")
							.contains("Modify or cancel recurring payments")) {
				Reporter.log("Verified Due date past Header");
			}
		} catch (Exception e) {
			Verify.fail("Due date past header not found");
		}
		return this;
	}

	public BillingSummaryPage verifyDownLoadPDFMobile() {
		try {
			downloadPDFIcon.isDisplayed();
			Reporter.log(" DownloadPDFIcon is displayed");
		} catch (Exception e) {
			Assert.fail("Fail to verify Download PDF Mobile");
		}
		return this;
	}

	public BillingSummaryPage verifyChooseBillVersionHeaderMobile() {
		try {
			chooseBillVersionHeaderMobile.isDisplayed();
			Reporter.log("Choose Bill version header is displayed");
		} catch (Exception e) {
			Assert.fail("Choose Bill version header is not displayed");
		}
		return this;
	}

	public BillingSummaryPage verifyDownLoadSummaryBill() {
		try {
			downloadSummaryBill.isDisplayed();
			Reporter.log("Verify Download Summary Bill is displayed");
		} catch (Exception e) {
			Assert.fail("Fail to verify download Summary Bill");
		}
		return this;
	}

	public BillingSummaryPage verifyDownLoadDetailedBill() {
		try {
			downloaddetailedBill.isDisplayed();
			Reporter.log("Download detailed bill is displayed");
		} catch (Exception e) {
			Assert.fail("Fail to verify download detailed bill");
		}
		return this;
	}

	public BillingSummaryPage verifyAmountTotalInCurrentCharges() {
		try {
			Double amountSum = 0.0;
			Double negativeAmounts = 0.0;
			Double total;
			for (WebElement amount : amountCells) {
				String amountTxt = amount.getText();
				if (amountTxt.contains("(")) {
					negativeAmounts = negativeAmounts + Double.parseDouble(amountTxt.replaceAll("[^.0-9]", ""));
				} else {
					amountSum = amountSum + Double.parseDouble(amountTxt.replaceAll("[^.0-9]", ""));
				}
			}
			amountSum = amountSum - negativeAmounts;
			String totalAmount = totalChargeAmount.getText();
			if (totalAmount.contains("(")) {
				Assert.assertTrue(amountSum.toString().contains("-"));
			} else {
				Assert.assertFalse(amountSum.toString().contains("-"));
			}
			totalAmount = totalAmount.replaceAll("[^.0-9]", "");
			total = Double.parseDouble(totalAmount);
			Assert.assertEquals(Double.compare(
					new BigDecimal(amountSum).setScale(2, RoundingMode.HALF_UP).abs().doubleValue(), total), 0);
			Reporter.log("Amount math is correctly displayed to Total amount");
		} catch (Exception e) {
			Assert.fail("Fail: Amount math doesn't equals Total amount");
		}
		return this;
	}

	public void changeBillCycle() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			selectBillcycleMobile();
		} else {
			selectBillCycleDesktop();
		}
		verifyPageLoaded();
	}

	/**
	 * Verify Contact us page.
	 *
	 * @return the ContactUSPage class instance.
	 */
	public BillingSummaryPage verifyBillingSummaryPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(billingPageUrl);
			Reporter.log("Bill summary page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Bill summary page not displayed");
		}
		return this;
	}

	/**
	 * Get the Row Count in Current Charges Table
	 *
	 * @return Row Count.
	 */
	public int getNumberofLinesinCurrentCharges() {
		int numberOfLines = 0;
		try {
			numberOfLines = lineCells.size();
			Reporter.log("Lines are displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Lines are not displayed");
		}
		return numberOfLines;
	}

	/**
	 * Click and Get the Line Number
	 *
	 * @return LineNumber.
	 */
	public String clickandGetLineNumber(int i) {
		String lineNumber = null;
		try {
			lineNumber = lineCells.get(i).getText();
			lineCells.get(i).click();
			Reporter.log("Line is clicked: " + lineNumber);
		} catch (NoSuchElementException e) {
			Assert.fail("Lines are not displayed");
		}
		return lineNumber;
	}

	/**
	 * Verify PID for ban
	 * 
	 * @param custAccountPID
	 */
	public void verifyPIIMaskingForBan(String custAccountPID) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(accountNo, custAccountPID), "No PII masking for BAN");
			Reporter.log("Verified PII masking for Payment method blade");
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for BAN");
		}
	}

	/**
	 * Verify PID for First name
	 * 
	 * @param custFirstNamePID
	 */
	public void verifyPIIMaskingForFirsName(String custFirstNamePID) {
		try {
			for (WebElement subsName : subscriberNames) {
				Assert.assertTrue(checkElementisPIIMasked(subsName, custFirstNamePID),
						"No PII masking for Subscriber name");
				Reporter.log("Verified PII masking for FirstName: " + subsName);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for FirstName");
		}
	}

	/**
	 * Verify PID for Msisdn
	 * 
	 * @param custMsisdnPID
	 */
	public void verifyPIIMaskingForMsisdn(String custMsisdnPID) {
		try {
			for (WebElement subsNo : subscriberNos) {
				Assert.assertTrue(checkElementisPIIMasked(subsNo, custMsisdnPID),
						"No PII masking for Subscriber Number");
				Reporter.log("Verified PII masking for Msisdn: " + subsNo);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Msisdn");
		}
	}

	/**
	 * Verify My Promotions Blade
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyMyPromotionBlade() {
		try {
			if (getDriver() instanceof AppiumDriver) {

				mobileMyPromotionBladeHeader.isDisplayed();
				Assert.assertTrue(mobileMyPromotionBladeHeader.getText().equals("My Promotions"));

			} else {
				myPromotionBladeHeader.isDisplayed();
				Assert.assertTrue(myPromotionBladeHeader.getText().equals("My Promotions"));
			}

			Reporter.log(" My Promotions' blade is displayed");
		} catch (Exception e) {
			Assert.fail(" My Promotions' blade is not displayed");
		}
		return this;
	}

	/**
	 * Verify My Promotions Blade Icon
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyMyPromotionBladeIcon() {
		try {
			if (getDriver() instanceof AppiumDriver) {

				mobileMyPromotionBladeIcon.isDisplayed();

			} else {
				myPromotionBladeIcon.isDisplayed();
			}
			Reporter.log(" My Promotions' blade Icon is displayed");
		} catch (Exception e) {
			Assert.fail(" My Promotions' blade Icon is not displayed");
		}
		return this;
	}

	/**
	 * Verify My Promotions Blade Chevron
	 * 
	 * @return
	 */
	public BillingSummaryPage verifyMyPromotionBladeChevron() {
		try {
			if (getDriver() instanceof AppiumDriver) {

				mobileMyPromotionBladeChevron.isDisplayed();

			} else {
				myPromotionBladeChevron.isDisplayed();
			}
			Reporter.log("My Promotions blade Chevron is displayed");
		} catch (Exception e) {
			Assert.fail("My Promotions blade Chevron is not displayed");
		}

		return this;

	}

	public BillingSummaryPage clickAutopayShortcut() {
		try {
			easyPayArrow.click();
			Reporter.log("Clicked on Autopay Shortcut");
		} catch (Exception e) {
			Assert.fail("Failed to click on Auto pay Shortcut");
		}
		return this;
	}
	
	

	/**
	 * verify text in Current Charges for Signed in Msisdin
	 */
	public BillingSummaryPage verifySingleLineMsisdin(String msisdinNumber) {
		try {
			checkPageIsReady();
				for (WebElement webElement : viewMsisdinByCurrentCharges) {
					waitFor(ExpectedConditions.visibilityOf(webElement));
					if(webElement.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
							.equalsIgnoreCase(msisdinNumber)){
						Reporter.log("Verified that the displayed msisdin is matching w.r.to Signed in Msisdin : "+msisdinNumber);
					}
				}
			} catch (Exception e) {
				Verify.fail("Signed in Msisdin by Categorylines are not displayed");
			}
		return this;
	}

	public BillingSummaryPage checkpaymentdateEasyPay(){
		try {
			if(paymentdateEasyPay.isDisplayed())Reporter.log("Easy pay paymentdate element is displayed");
			else Verify.fail("Easy pay paymentdate element is not displayed");
			
		} catch (Exception e) {
			Assert.fail("paymentdateEasyPay is not located");
		}
		return this;
	}
	
	public BillingSummaryPage checkcardImageEasyPay(){
		try {
			if(cardImageEasyPay.isDisplayed())Reporter.log("Easy pay card image element is displayed");
			else Verify.fail("Easy pay card image element is not displayed");
			
		} catch (Exception e) {
			Assert.fail("cardImageEasyPay is not located");
		}
		return this;
	}
	public BillingSummaryPage checkcardDetailEasyPay(){
		try {
			
			if(cardDetailEasyPay.isDisplayed())Reporter.log("Easy pay card details element is displayed");
			else Verify.fail("Easy pay card details element is not displayed");
			
		} catch (Exception e) {
			Assert.fail("cardDetailEasyPay is not located");
		}
		return this;
	}
	





}
