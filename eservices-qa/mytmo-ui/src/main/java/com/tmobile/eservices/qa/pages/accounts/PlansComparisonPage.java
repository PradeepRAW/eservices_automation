package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PlansComparisonPage extends CommonPage {

	@FindBy(id = "changeData")
	private List<WebElement> changeData;

	@FindBy(id = "btnwarningContinue")
	private WebElement kickBackWarning;

	@FindBy(linkText = "Add Services")
	private WebElement addServices;

	@FindBy(css = "p.small.name")
	private WebElement additionalLineCost;

	@FindBys(@FindBy(css = "i.text-magenta.fa.fa-plus"))
	private List<WebElement> moreButtons;

	@FindBy(css = "i.text-magenta.fa.fa-minus")
	private WebElement minusButton;

	@FindBy(xpath = "(//*[@id='currentPlan' and not (attribute::disabled)])[2]")
	private WebElement selectPersonalizeData;

	@FindBys(@FindBy(css = "button#currentPlan"))
	private List<WebElement> plans;

	@FindBy(css = ".ui-plan-font.showeligibleplanservices")
	private WebElement viewMorePlans;

	@FindBy(css = "button[soc='NATTU']")
	private WebElement selectNATTUContinue;

	@FindBy(css = "button[soc='6FAMSC2']")
	private WebElement sixgbFamilySelect;

	@FindAll({ @FindBy(css = "div#trebekplanitem div div button#currentPlanBtn"),
			@FindBy(css = "button[soc='LTUNL']") })
	private WebElement tmobileOneUNL;

	@FindBy(css = "button[soc='NAUTTF']")
	private WebElement selectNAUTTFContinue;

	@FindBy(css = "button[soc='FRLTUNL']")
	private WebElement tMobileOneSelectContinue;

	@FindBy(id = "proceedConfigure")
	private WebElement planProceed;

	@FindBys(@FindBy(css = "li#tmoOneNewTmScNc"))
	private List<WebElement> withoutAutoPay;

	@FindBys(@FindBy(css = "li.cost.text-magenta.text-center.tmoOneNewTmScNc"))
	private List<WebElement> withAutoPay;

	@FindBys(@FindBy(css = "p#autoPayOffMsg"))
	private List<WebElement> signUpAutoPay;

	@FindBy(xpath = "//span[text()= 'Without AutoPay']")
	private WebElement withOutAutoPayText;

	@FindBy(xpath = "//span[text()= 'With Autopay']")
	private WebElement withAutoPayText;

	@FindBy(id = "dollarSignTm")
	private WebElement dollarSymbol;

	@FindBy(xpath = "//span[text()=' 4G LTE per line']")
	private WebElement lTE4GPerLineText;

	@FindBy(css = "addcard div h3")
	private WebElement cardHeader;

	@FindBy(css = "div#add_card_plansconfig div.pull-right")
	private WebElement addCard;

	@FindBy(id = "card-name")
	private WebElement cardName;

	@FindBy(id = "account-name")
	private WebElement accountName;

	@FindBy(id = "routing-number")
	private WebElement routingNumber;

	@FindBy(id = "account-number")
	private WebElement accountNumber;

	@FindBy(id = "cvv_find")
	private WebElement findMyAccountNumber;

	@FindBy(id = "card-number")
	private WebElement cardNumber;

	@FindBy(id = "Account")
	private WebElement bankAccountNumber;

	@FindBy(id = "mmyy")
	private WebElement expiryDate;

	@FindBy(id = "card-secu-code")
	private WebElement cvvCode;

	@FindBy(id = "card-zipcode")
	private WebElement zip;

	@FindBy(css = "button#add_card_method_back")
	private WebElement backButton;

	@FindBy(css = "a#cvv_crd")
	private WebElement findMyCvv;

	@FindBy(css = "addbank div h4")
	private WebElement bankHeader;

	@FindBy(css = "div#add_bank_plansconfig div.pull-right")
	private WebElement addBank;

	@FindBy(css = "input#addBankContinueBtn")
	private WebElement continueAddBank;

	@FindAll({ @FindBy(css = "div.ui_legal"), @FindBy(css = "p[class*='term-condition-text']"),
			@FindBy(css = "div[class*='term-condition-text']") })
	private WebElement rPFAndTRFApplies;

	@FindBy(id = "currentPlanBtn")
	private List<WebElement> plansList;

	@FindBy(xpath = "//p[contains(text(),'You are currently on our best Talk and Text plan')]")
	private List<WebElement> bestTalkAndTextPlan;

	@FindBy(id = "changeData")
	private List<WebElement> changeDataLink;

	@FindBy(id = "ConfigureRatePlanMessage")
	private WebElement reviewYourOrderText;

	@FindBy(css = "input[id*='optionsRadios1']")
	private List<WebElement> radoiBtn;

	@FindBy(id = "unlimitedoption_NotRestricted")
	private WebElement lookingForUnlimitedOptions;

	@FindBy(css = "p[id*='price-data']")
	private List<WebElement> includedPrice;

	@FindBy(css = "div#available_plan p#legalFCCVerbiage.legal.text-gray-dark")
	public WebElement leagalTexstInPlanComparisonPage;

	@FindBy(css = "div.f-20-26.text-gray-dark")
	public WebElement newPlanComparisonPageHeader;

	@FindAll({ @FindBy(css = "h5#changePlanTitle"), @FindBy(css = "div#changePlansText h4"),
			@FindBy(xpath = "//div[@id='viewPlansText']/h4") })
	public List<WebElement> oldPlanComparisonPageHeader;

	@FindBy(css = "ul#insideData0 li")
	public WebElement planAttributes;

	@FindBy(css = "ul#insideData0 li")
	public List<WebElement> planAttributesElements;

	@FindBy(css = "div.text-gray-dark")
	public WebElement yourCurrentPlanHeader;

	@FindBy(css = "div#mobile_header i.fa.fa-angle-left")
	public WebElement navigationBar;

	@FindBy(css = "span#nameOfPlan")
	public WebElement nameOfPlan;

	@FindBy(css = "div#availablePlans0 span#priceWithStrikeAvailPlan")
	public WebElement priceOfPlan;

	@FindBy(css = "i#choice")
	public WebElement carrot;

	@FindBy(css = "div#OpenPlansDiv")
	public WebElement planDetails;

	// @FindBy(css = "div#OpenPlansDiv a.cursor")
	// @FindBy(css = "a.cursor")
	@FindBy(id = "availablePlanViewDetails")
	public List<WebElement> viewDetailsLink;

	@FindBy(id = "currentPlanViewDetails")
	public WebElement viewDeatailLinkOnCurrentplan;

	@FindBy(css = "div#available_plan div.Rectangle-1 a.cursor")
	public WebElement viewDeatailLinkOnavailableplan;

	@FindBy(css = "div.row div.f-20-26.col-xs-12")
	public WebElement ourAvailablePlan;

	@FindBy(css = "div#availablePlans0 span#ribbonTagLine")
	public WebElement marketingTagLine;

	// @FindBy(css = "div#available_plan .BG.btn-primary.selectedPlan")
	@FindBy(css = "button#selectPlanBtn")
	public List<WebElement> selectPlanCTA;

	@FindBy(css = "button#currentPlanBtn")
	public List<WebElement> backBtn;

	@FindBy(css = "div#unCarrierBenefits ul#features")
	public WebElement benefitsSection;

	@FindBy(css = "div.row div.col-md-offset-1 p.term-condition-text")
	public WebElement legalTextinFooter;

	@FindBy(css = "div#availablePlans0 div#attributelegalFCCVerbiage")
	public WebElement legalTextinAvailablePlanSection;

	@FindAll({ @FindBy(css = "div#attributelegalFCC P"), @FindBy(css = "p#legalFCCVerbiage") })
	public List<WebElement> legalTextinCurrentPlan;

	@FindBy(css = "span#legalFCC")
	public List<WebElement> legalTextinAvailablePlan;

	@FindBy(css = "#di_data_message > h2")
	private WebElement verifyNonIOTPlanMessage;

	@FindBy(css = "div#availablePlans0 span#priceWithoutDiscountDollarAvailPlan.Display4.strikePrice")
	public WebElement planPriceWithoutDiscountOnAvalablePlanSection;

	@FindBy(css = "div#availablePlans0 span#priceWithDiscountDollarAvailPlan")
	public WebElement planPriceWithDiscountOnAvalablePlanSection;

	@FindBy(css = "div#availablePlans0 div.text-center.pl-15")
	public WebElement autoPayDiscountText;

	@FindAll({ @FindBy(css = "button[soc='1PLSALL']"),
			@FindBy(xpath = "//span[text()='Amp up T-Mobile ONE']//..//..//..//button") })
	public WebElement selectAndContinueUsingSOC;

	@FindAll({ @FindBy(css = "button[soc='ONE55TI2']"),
			@FindBy(xpath = "//div[text()='Plan for ages 55+']//..//..//..//..//button") })
	public WebElement selectAndContinueUsing55;

	@FindBy(id = "tagLineTm")
	public List<WebElement> titles;

	@FindAll({ @FindBy(css = "button[soc='1PLSALL']"),
			@FindBy(xpath = "//span[text()='With Netflix On Us.']//..//..//..//button") })
	public WebElement selectAndContinueUsingtmoSOC;

	@FindAll({ @FindBy(css = "button[soc='TMSERVE2']"),
			@FindBy(xpath = "//div[text()='For military & veterans']//..//..//..//..//button") })
	public WebElement selectAndContinueUsingtmoSOCMilitary;

	@FindBy(css = "button[soc='TM1TI']")
	public WebElement selectAndContinueUsingPRTI;

	@FindBy(css = "button[soc='PRLTTVU']")
	public WebElement selectAndContinueUsingPRTE;

	@FindAll({ @FindBy(css = "button[soc='1PLSALL9']"),
			@FindBy(xpath = "//span[text()='Amp up T-Mobile ONE']//..//..//..//button") })
	public WebElement selectAndContinueUsingmiltarySOC;

	@FindAll({ @FindBy(css = "button[soc='NCCONE2']"),
			@FindBy(xpath = "//span[text()='Amp up T-Mobile ONE']//..//..//..//button") })
	public WebElement selectAndContinueUsingNCCSOC;

	@FindBy(css = "ul#insideData0 li")
	public WebElement availablePlanheader;

	@FindBy(css = "div.modal-body")
	public WebElement legalTextDisclaimer;

	@FindBy(css = "div[id*='popover']")
	public WebElement tooltipText;

	@FindBy(xpath = "//span[contains(text(),'Family Allowance')]//..//i[contains(@class,'fa-info-circle')]")
	public WebElement featureTooltip;

	@FindAll({ @FindBy(css = "#unCarrierBenefits > div > div > div > a > p > i"),
			@FindBy(css = "div#unCarrierBenefits > div > i") })
	private WebElement expandCollapsePlanDetails;

	@FindAll({ @FindBy(id = "features"), @FindBy(css = "div#trebekplanitem div#unCarrierBenefits #features") })
	private WebElement planfeaturesDisplay;

	private By planAttributeToolTipMilitaryPlan = By.xpath(
			"//div[text()='For military & veterans']//..//..//..//..//..//div[contains(@class,'bg-white')]//..//span[contains(text(),'%s')]//..//i[contains(@class,'fa-info-circle')]");
	private By planAttributeToolTipages55Plan = By.xpath(
			"//div[text()='Plan for ages 55+']//..//..//..//..//..//div[contains(@class,'bg-white')]//..//span[contains(text(),'%s')]//..//i[contains(@class,'fa-info-circle')]");
	/*
	 * private By planAttribute = By.xpath(
	 * "//div[text()='For Military & Veterans']//..//..//..//..//..//div[contains(@class,'bg-white')]//..//span[contains(text(),'%s')]"
	 * );
	 */
	private By planAttribute = By.xpath(
			"//div[text()='%s']//..//..//..//..//..//div[contains(@class,'bg-white')]//..//span[contains(text(),'%s')]");

	@FindBy(linkText = "t-mobile.com/military/verification")
	public WebElement militaryVerificationLink;

	@FindBy(css = "button[soc='TMSERVE']")
	public WebElement getverifiedbutton;

	@FindBy(linkText = "www.netflix.com/termsofuse")
	public WebElement netFlixTermsOfUseLink;

	// @FindBy(xpath = "//a[contains(text(),'More plan options')]")
	@FindBy(css = "div#exploremore_btn a")
	public WebElement moreplanOptions;

	@FindBy(linkText = "www.t-mobile.com")
	public WebElement tmobileLink;

	@FindBy(xpath = "//div[text()='For military & veterans' and @id='tagLineTm']")
	public WebElement militaryVeteransTitle;

	@FindBy(xpath = "//span[text()='For military & veterans' and @id='ribbonTagLine']")
	public WebElement militaryVeteransTitleMobile;

	@FindBy(xpath = "//div[text()='With Netflix On Us.' and @id='tagLineTm']")
	public WebElement netFlixOnUSTitle;

	@FindBy(xpath = "//span[text()='With Netflix On Us.' and @id='ribbonTagLine']")
	public WebElement netFlixOnUSTitleMobile;

	@FindBy(xpath = "//div[text()='Plan for ages 55+' and @id='tagLineTm']")
	public WebElement ages55Title;

	@FindBy(xpath = "//span[text()='Plan for ages 55+' and @id='ribbonTagLine']")
	public WebElement ages55TitleMobile;

	@FindBy(xpath = "//div[text()='Unlimited for all' and @id='tagLineTm']")
	public WebElement pappyessential;

	@FindBy(xpath = "//span[text()='Unlimited for all' and @id='ribbonTagLine']")
	public WebElement pappyessentialMobile;

	@FindBy(xpath = "//div[text()='Amp up T-Mobile ONE' and @id='tagLineTm']")
	public WebElement pappyTI;

	@FindBy(xpath = "//span[text()='Amp up T-Mobile ONE' and @id='ribbonTagLine']")
	public WebElement pappyTIMobile;

	@FindAll({ @FindBy(xpath = "//button[contains(text(),'Contact Us')]"),
			@FindBy(xpath = "//button[contains(text(),'Find a store')]") })
	public WebElement contactUsButton;

	@FindBy(css = "a#openTermsModal")
	public WebElement seefulldetails;

	@FindBy(css = "div#termsModal")
	public WebElement legalDisclaimerPopupModal;

	@FindBy(xpath = "//div[text()='Amp up T-Mobile ONE']//..//..//..//..//i[contains(@class, 'text-green')]//..//i[contains(@class,'fa fa-info-circle')]")
	public WebElement tootTipOver;

	@FindBy(xpath = "//div[@role='tooltip']//div[@class='popover-content']")
	public WebElement toolTipPopoverContent;

	@FindBy(xpath = "//div[text()='Plan for ages 55+']//following::button[text()='Contact Us']")
	public WebElement planForAges55WithContactUsBtn;

	@FindBy(xpath = "//div[text()='For military & veterans']//following::button[text()='Contact Us']")
	public WebElement forMilitaryVeteransOntactUsBtn;

	@FindBy(xpath = "//button[contains(text(), 'Select and continue')]")
	public List<WebElement> selectAndContinue;

	private final String pageUrl = "/plans-comparison.html";

	/**
	 * 
	 * @param webDriver
	 */
	public PlansComparisonPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PlansComparisonPage verifyPlanComparisionPage() {
		try {
			checkPageIsReady();
			waitForImageSpinnerInvisibility();
			verifyPageUrl();
			Reporter.log("Plan-Comparision Page is dispalyed");
		} catch (Exception e) {
			Assert.fail("Plan-Comparision Page not Loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PlansComparisonPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * click on ChangeData Link of Best plan message
	 */

	public PlansComparisonPage clickChangeDataLink() {
		try {
			changeData.get(0).click();
			Reporter.log("Clicked on Change Data link");
		} catch (Exception e) {
			Assert.fail("Change Data link is not clicked");
		}
		return this;

	}

	public PlansComparisonPage verifyNewPlansComparisonPageHeader(String msg) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader")));
			waitFor(ExpectedConditions.visibilityOf(newPlanComparisonPageHeader));
			newPlanComparisonPageHeader.getText().contains(msg);
			Reporter.log("New Plans Comparison Page is displayed");
		} catch (Exception e) {
			Assert.fail("New plans Comparison page not displayed");
		}
		return this;

	}

	/**
	 * Click Select And Continue Button
	 * 
	 * @return
	 */
	// public PlansComparisonPage clickSelectandContinueButton() {
	// waitForPageLoad(getDriver());
	// checkPageIsReady();
	// waitforSpinner();
	//
	// try {
	// if (!(getDriver() instanceof AppiumDriver)) {
	// if (!changeData.isEmpty() && changeData.size() >= 1) {
	// waitFor(ExpectedConditions.visibilityOf(changeData.get(0)));
	// clickElementWithJavaScript(changeData.get(0));
	// } else {
	// waitFor(ExpectedConditions.visibilityOf(selectAndContinue));
	// clickElementWithJavaScript(selectAndContinue);
	// Reporter.log("Clicked on select And Continue button");
	// }
	// } else {
	// selectPlanButton.get(1).click();
	// Reporter.log("Clicked on select Plan button");
	// }
	//
	// } catch (Exception e) {
	// Assert.fail("select And Continue button is not clicked");
	// }
	// return this;
	// }

	public PlansComparisonPage clickSelectandContinueButton() {
		checkPageIsReady();
		waitforSpinner();
		try {
			clickElementWithJavaScript(selectAndContinueUsingtmoSOC);
			Reporter.log("Clicked on Select And Continue button");

		} catch (Exception e) {
			Assert.fail("Select And Continue button is not clicked");
		}
		return this;
	}

	public PlansComparisonPage clickSelectandContinueButtonUsingSOC() {
		checkPageIsReady();
		waitforSpinner();
		try {
			clickElementWithJavaScript(selectAndContinueUsingSOC);
			Reporter.log("Clicked on select And Continue button");

		} catch (Exception e) {
			Assert.fail("Select And Continue button is not clicked");
		}
		return this;
	}

	public boolean verifyPlansComparisonPageBestTalkAndTextPlan() {
		// return !CollectionUtils.isEmpty(bestTalkAndTextPlan) &&
		// !CollectionUtils.sizeIsEmpty(bestTalkAndTextPlan);
		checkPageIsReady();
		waitForImageSpinnerInvisibility();
		return !bestTalkAndTextPlan.isEmpty();
	}

	public PlansComparisonPage verifyToolTipForPlanAttributesMilitaryPlan(String plan, String attribute,
			String tooltip) {
		try {
			checkPageIsReady();
			WebElement moreFeatures = getDriver()
					.findElement(getNewLocator(planAttributeToolTipMilitaryPlan, attribute));
			scrollToElement(moreFeatures);
			moveToElement(moreFeatures);
			Assert.assertTrue(tooltipText.getText().contains(tooltip), "tool tip text not dispalyed");
			mouseOutTooltipIcons(plan, attribute);
			Reporter.log(tooltip + " Mesage is displayed");
		} catch (Exception e) {
			Assert.fail(tooltip + " Mesage is not displayed");
		}
		return this;
	}

	public void scrollToElement(WebElement element) {
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		checkPageIsReady();
	}

	public PlansComparisonPage mouseOutTooltipIcons(String plan, String attribute) {
		try {
			checkPageIsReady();
			WebElement moreFeatures = getDriver().findElement(getNewLocator(planAttribute, plan, attribute));
			scrollToElement(moreFeatures);
			moveToElement(moreFeatures);
			Reporter.log("clicked on tool tip");

		} catch (Exception e) {
			Assert.fail("tooltip not displayed");
		}
		return this;
	}

	public PlansComparisonPage clickMorePlanOptions() {
		try {
			checkPageIsReady();
			moveToElement(moreplanOptions);
			moreplanOptions.click();
			Reporter.log("Clicked on More Plan Options");
			checkPageIsReady();
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollTop = arguments[0].scrollHeight",
					oldPlanComparisonPageHeader);
			// ((JavascriptExecutor)
			// getDriver()).executeScript("scroll(0,-250);");
		} catch (Exception e) {
		}
		return this;

	}

	public boolean verifyMilitaryandVeteransPlanTitle() {
		checkPageIsReady();
		boolean display = false;
		try {
			if (getDriver() instanceof AppiumDriver) {
				scrollToElement(militaryVeteransTitleMobile);
				checkPageIsReady();
				Assert.assertTrue(militaryVeteransTitleMobile.isDisplayed(), "Ages 55+ Title is not displayed");
				Reporter.log("Ages 55+ Title is displayed");
				display = true;
			} else {
				scrollToElement(militaryVeteransTitle);
				checkPageIsReady();
				Assert.assertTrue(militaryVeteransTitle.isDisplayed(),
						"Military and Veterans PlanTitle is not displayed");
				Reporter.log("Military and Veterans PlanTitle is displayed");
				display = true;
			}
			// ((JavascriptExecutor)
			// getDriver()).executeScript("arguments[0].scrollTop =
			// arguments[0].scrollHeight", militaryVeteransTitle);

		} catch (Exception e) {
			Assert.fail("Military and Veterans PlanTitle is not displayed");
		}
		return display;
	}

	public boolean verifyages55PlanTitle() {
		checkPageIsReady();
		boolean display = false;
		try {
			if (getDriver() instanceof AppiumDriver) {
				scrollToElement(ages55TitleMobile);
				checkPageIsReady();
				Assert.assertTrue(ages55TitleMobile.isDisplayed());
				Reporter.log("ages 55+ Title is displayed");
				display = true;
			} else {
				scrollToElement(ages55Title);
				checkPageIsReady();
				Assert.assertTrue(ages55Title.isDisplayed());
				Reporter.log("ages 55+ Title is displayed");
				display = true;
			}

		} catch (Exception e) {
			Assert.fail("ages 55+ Title is not displayed");
		}
		return display;
	}

	public boolean verifyPappyTITitle() {
		checkPageIsReady();
		boolean display = false;
		try {
			if (getDriver() instanceof AppiumDriver) {
				scrollToElement(pappyTIMobile);
				checkPageIsReady();
				Assert.assertTrue(pappyTIMobile.isDisplayed());
				Reporter.log("Pappy TI Title is displayed");
				display = true;
			} else {
				moveToElement(pappyTI);
				checkPageIsReady();
				Assert.assertTrue(pappyTI.isDisplayed());
				Reporter.log("Pappy TI Title is displayed");
				display = true;
			}

		} catch (Exception e) {
			Assert.fail("ages 55+ Title is not displayed");
		}
		return display;
	}

	public PlansComparisonPage verifyContactUsButtonDispalyed() {
		try {
			checkPageIsReady();
			contactUsButton.isDisplayed();
			Reporter.log("contact us button or find a store button is displayed");
		} catch (Exception e) {

			Assert.fail("Neither contact us or Find a store is not displayed");
		}
		return this;
	}

	public PlansComparisonPage verifyToolTipForPlanAttributesages55Plan(String plan, String attribute, String tooltip) {
		try {
			checkPageIsReady();
			WebElement moreFeatures = getDriver().findElement(getNewLocator(planAttributeToolTipages55Plan, attribute));
			scrollToElement(moreFeatures);
			moveToElement(moreFeatures);
			Assert.assertTrue(tooltipText.getText().contains(tooltip), "tool tip text not dispalyed");
			mouseOutTooltipIcons(plan, attribute);
			Reporter.log(tooltip + " Mesage is displayed");
		} catch (Exception e) {
			Assert.fail(tooltip + " Mesage is not displayed");
		}
		return this;
	}

}