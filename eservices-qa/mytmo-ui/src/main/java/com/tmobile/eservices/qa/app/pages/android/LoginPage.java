package com.tmobile.eservices.qa.app.pages.android;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.tmobile.eqm.testfrwk.ui.core.mobile.MobilePage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSElement;

public class LoginPage extends MobilePage {

	public static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

	@FindBy(css = "")
	private IOSElement emailOrPhone;

	@FindBy(css = "")
	private IOSElement password;

	@FindBy(css = "")
	private IOSElement loginButton;

	@FindBy(css = "")
	private IOSElement securityQuestionsRadioButton;

	@FindBy(css = "")
	private IOSElement continueButton;

	/***
	 * @param driver
	 */
	public LoginPage(AppiumDriver<?> appiumdriver) {
		super(appiumdriver);
	}

	/**
	 * Verify Login Page
	 * 
	 * @return
	 */
	public boolean verifyLoginPage() {
		return getDriver().getCurrentUrl().contains("signin");
	}

	/**
	 * Re enter EmailOrPhone
	 */
	private LoginPage enterEmailOrPhone(String value) {
		try {
			emailOrPhone.sendKeys(value);
		} catch (Exception e) {
			Assert.fail("Entering email or phone value was unsuccessful.");
		}
		return this;
	}

	/**
	 * Re enter Password
	 */
	private LoginPage enterPassword(String value) {
		try {	
			password.sendKeys(value);
			waitFor(ExpectedConditions.visibilityOf(password), 3);	
		} catch (Exception e) {
			Assert.fail("Entering password value was unsuccessful.");
		}
		return this;
	}

	/**
	 * Click Submit Button
	 */
	public LoginPage clickSubmitButton() {
		try {
			loginButton.click();
		} catch (Exception e) {
			Assert.fail("Unable to click on Login button");
		}
		return this;
	}
	
	/**
	 * This method is to enter user name and password and click submit
	 * 
	 * @param username
	 * @param password
	 */
	public LoginPage performLoginAction(String username, String password) {
		try {
			enterEmailOrPhone(username);
			enterPassword(password);
			clickSubmitButton();
		} catch (Exception e) {
			Assert.fail("Unable to login with " + username + "/" + password + "");
		}
		return this;
	}

}
