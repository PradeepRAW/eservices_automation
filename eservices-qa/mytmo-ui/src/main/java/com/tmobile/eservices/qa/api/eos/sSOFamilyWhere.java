package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class sSOFamilyWhere extends ApiCommonLib{

	  public Response getSSOFamilyWhere(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildSSOFamilywhere(apiTestData, tokenMap);
	    	String resourceURL = "v1/subscriber/partnertoken?Source=mytmobile&Service=FamilyWhere";
			Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "",RestServiceCallType.GET);
			return response;
	    }
	  public Response getSSOFamilyWhereMigrateEricssonLink(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildSSOFamilywhereMigrateEricsson(apiTestData, tokenMap);
	    	String resourceURL = "v1/subscriber/partnertoken?Service=ericsson";
			Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "",RestServiceCallType.GET);
			return response;
	    }
	  public Response getSSOFamilyWhereMigrateCallerTunesLink(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildSSOFamilywhereMigrateEricsson(apiTestData, tokenMap);
	    	String resourceURL = "v1/subscriber/partnertoken?Service=CallerTunes";
			Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "",RestServiceCallType.GET);
			return response;
	    }
	  public Response getSSOFamilyWhereMigrateEBillLink(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildSSOFamilywhereMigrateEricsson(apiTestData, tokenMap);
	    	String resourceURL = "v1/subscriber/partnertoken?Service=eBill";
			Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "",RestServiceCallType.GET);
			return response;
	    }
	  public Response getSSOVistaCredit(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildSSOVistaCreditAutoRefillCreditPayment(apiTestData, tokenMap);
	    	String resourceURL = "v1/subscriber/partnertoken?Service=VestaCredit";
			Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "",RestServiceCallType.GET);
			return response;
	    }
	  public Response getSSOVistaAutoRefill(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildSSOVistaCreditAutoRefillCreditPayment(apiTestData, tokenMap);
	    	String resourceURL = "v1/subscriber/partnertoken?Service=VestaAutoRefill";
			Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "",RestServiceCallType.GET);
			return response;
	    }
	  public Response getSSOVistaManagePayment(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildSSOVistaCreditAutoRefillCreditPayment(apiTestData, tokenMap);
	    	String resourceURL = "v1/subscriber/partnertoken?Service=VestaManagePayment";
			Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "",RestServiceCallType.GET);
			return response;
	    }
	  private Map<String, String> buildSSOFamilywhere(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
	    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
			Map<String, String> headers = new HashMap<String, String>();				
			headers.put("Accept", "application/json");
			headers.put("Content-Type", "application/json");
			headers.put("X-B3-TraceId", "FlexAPITest123");
			headers.put("X-B3-SpanId", "FlexAPITest456");
			headers.put("channel_id", "abc");
			headers.put("Authorization", jwtTokens.get("jwtToken"));
			headers.put("access_token", jwtTokens.get("accessToken"));
			headers.put("application_id", "abc");
			headers.put("user-token", "{{user-token}}");
			headers.put("tmo-id", "{{tmo-id}}");
			headers.put("msisdn", apiTestData.getMsisdn());
			headers.put("ban", apiTestData.getBan());
			headers.put("usn", "abc");
			headers.put("application_client", "{{application_client}}");
			headers.put("application_version_code", "{{application_version_code}}");
			headers.put("device_os", "{{device_os}}");
			headers.put("os_version", "{{os_version}}");
			headers.put("os_language", "{{os_language}}");
			headers.put("device_manufacturer", "{{device_manufacturer}}");
			headers.put("device_model", "{{device_model}}");
			headers.put("exitState", "MYACCT_WEB_CONTACT_US");
			return headers;
		}
	  
	  private Map<String, String> buildSSOFamilywhereMigrateEricsson(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
	    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
			Map<String, String> headers = new HashMap<String, String>();				
			headers.put("Content-Type", "application/json");
			headers.put("X-B3-TraceId", "test");
			headers.put("X-B3-SpanId", "test");
			headers.put("channel_id", "DESKTOP");
			headers.put("Authorization", jwtTokens.get("jwtToken"));
			headers.put("access_token", jwtTokens.get("accessToken"));
			headers.put("application_id", "MYTMO");
			headers.put("cache-control", "no-cache");
			headers.put("msisdn", apiTestData.getMsisdn());
			headers.put("ban", apiTestData.getBan());
			headers.put("usn", "usn");
			return headers;
		}
	  private Map<String, String> buildSSOVistaCreditAutoRefillCreditPayment(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
	    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
			Map<String, String> headers = new HashMap<String, String>();				
			headers.put("Accept", "application/json");
			headers.put("Content-Type", "application/json");
			headers.put("X-B3-TraceId", "12196d585b1249479771c125ee2d7ad5");
			headers.put("X-B3-SpanId", "12196d585b1249479771c125ee2d7ad5");
			headers.put("Authorization", jwtTokens.get("jwtToken"));
			headers.put("access_token", jwtTokens.get("accessToken"));
			headers.put("application_id", "MYTMO");
			headers.put("channel_id", "Desktop");
			headers.put("usn", "6be5f53f4bf9d175");		
			headers.put("msisdn", apiTestData.getMsisdn());
			headers.put("ban", apiTestData.getBan());
			headers.put("usn", "usn");
			return headers;
		}
}
