package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Divya
 *
 */
public class ShipmentTrackerPage extends CommonPage {


	public ShipmentTrackerPage(WebDriver webDriver) {
		super(webDriver);
	}


	/**
	 * Verify Shipment Tracker Page
	 * 
	 * @return
	 */
	public ShipmentTrackerPage verifyShipmentTrackerPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			getDriver().getCurrentUrl().contains("shipmenttracker");
			Reporter.log("Shipment Tracker Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Shipment Tracker Page is not displayed");
		}
		return this;
	}

}

