package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class ManageEasyPayApiV2 extends ApiCommonLib {
	
	
	 private Map<String, String> buildManageEasyPayAPIHeader(ApiTestData apiTestData) throws Exception {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("senderid", "MYTMO");
			headers.put("channelid", "WEB");
			headers.put("applicationid", "ESERVICE");
			headers.put("Accept", "application/json");
			headers.put("Content-Type", "application/json");
			headers.put("serviceTransactionId", "tnvp4GJSDiRMs2NMurMfkUbHjPud");
			headers.put("workflowid", "manageEasyPay");
			headers.put("interactionid", "123456787");
			headers.put("storeId", "MEWEB");
			headers.put("dealerCode", "0000000");
			headers.put("applicationUserId", "1998");
			headers.put("sessionId", "232323");
			headers.put("activityId", "updateEasyPay");
			headers.put("traceId", "123456");
			headers.put("Authorization", checkAndGetAccessToken());
			headers.put("X-B3-TraceId", "PaymentsAPITest123");
			headers.put("X-B3-SpanId", "PaymentsAPITest456");
			headers.put("transactionid","PaymentsAPITest");
		
			return headers;
		}

	
	 public Response easypaywithNewcard(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildManageEasyPayAPIHeader(apiTestData);
	    	RestService service = new RestService(requestBody, eos_base_url);
	        RequestSpecBuilder reqSpec = service.getRequestbuilder();
	 		reqSpec.addHeaders(headers);
			service.setRequestSpecBuilder(reqSpec);
	         Response response = service.callService("v2/easypay/signupeasypay",RestCallType.POST);
	         System.out.println(response.asString());
	         return response;
	    }
	 
	 public Response easypaywithStoredcard(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildManageEasyPayAPIHeader(apiTestData);
	    	headers.put("serviceTransactionId", "WMMt5qwcSqkAABxXt98AAAAN");
	    	headers.put("storeId", "PAYHUB_MEWEB");
	    	headers.put("traceId", "123442");
	    	RestService service = new RestService(requestBody, eos_base_url);
	        RequestSpecBuilder reqSpec = service.getRequestbuilder();
	 		reqSpec.addHeaders(headers);
			service.setRequestSpecBuilder(reqSpec);
	         Response response = service.callService("v2/easypay/signupeasypay",RestCallType.POST);
	         System.out.println(response.asString());
	         return response;
	    }
	 
	    
	public Response cancelAutopay(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildManageEasyPayAPIHeader(apiTestData);
		headers.put("serviceTransactionId", "WMMt5qwcSqkAABxXt98AAAAN");
		headers.put("storeId", "PAYHUB_MEWEB");
		headers.put("traceId", "123442");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v2/easypay/voideasypay", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}
	

	 public Response easypaywithNewBankAccount(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildManageEasyPayAPIHeader(apiTestData);
	    	headers.put("serviceTransactionId", "WMMt5qwcSqkAABxXt98AAAAN");
	    	headers.put("storeId", "PAYHUB_MEWEB");
	    	headers.put("traceId", "12345");
	    	RestService service = new RestService(requestBody, eos_base_url);
	        RequestSpecBuilder reqSpec = service.getRequestbuilder();
	 		reqSpec.addHeaders(headers);
			service.setRequestSpecBuilder(reqSpec);
	         Response response = service.callService("v2/easypay/signupeasypay",RestCallType.POST);
	         System.out.println(response.asString());
	         return response;
	    }
	 
	 public Response easypaywithStoredBankAccount(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
	    	Map<String, String> headers = buildManageEasyPayAPIHeader(apiTestData);
	    	headers.put("serviceTransactionId", "WMMt5qwcSqkAABxXt98AAAAN");
	    	headers.put("storeId", "PAYHUB_MEWEB");
	    	headers.put("traceId", "12345");
	    	RestService service = new RestService(requestBody, eos_base_url);
	        RequestSpecBuilder reqSpec = service.getRequestbuilder();
	 		reqSpec.addHeaders(headers);
			service.setRequestSpecBuilder(reqSpec);
	         Response response = service.callService("v2/easypay/signupeasypay",RestCallType.POST);
	         System.out.println(response.asString());
	         return response;
	    }
	 
	 
	 
	
}
