package com.tmobile.eservices.qa.pages.payments.api;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class SedonaApiCollectionV1 extends ApiCommonLib {
	

	
	private Map<String, String> buildSedonaAPICollectionHeader(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json");
		headers.put("interactionId","543219876");
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid","PaymentsAPITest");
	
		return headers;
	}
	
	public Response generatequote_datapass(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildSedonaAPICollectionHeader(apiTestData,tokenMap);
		headers.put("workflowId","MANAGEDATAPASS");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/generatequote", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}
	
	
	public Response generatequote_PA(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildSedonaAPICollectionHeader(apiTestData,tokenMap);
		headers.put("workflowId","MANAGEPAYMENTARRANGEMENT");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/generatequote", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}
	
	

	public Response generatequote_OTP(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildSedonaAPICollectionHeader(apiTestData,tokenMap);
		headers.put("workflowId","CHANGEMSISDN");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/generatequote", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

}
