package com.tmobile.eservices.qa.api.unlockdata;

import static com.jayway.restassured.RestAssured.given;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
//import com.tmobile.eservice.api.common.RequestData;
//import com.tmobile.eservice.api.exception.ServiceException;
//import com.tmobile.eservice.api.utils.ProfileServiceRequestBuilder;

/**
 * @author blakshminarayana
 *
 */
public class RestServiceImpl implements RestService {
	private static Logger logger = LoggerFactory.getLogger(RestServiceImpl.class);

	private static final String PROFILE_TYPE = "TMO";
	private static final String TMOLINE_PROFILE = "tmoLineProfile";
	private static final String PINCODE = "pincode";
	private static final String TMBILLING_ZIP = "tmbillingzip";
	private static final String IAM_USER_PROFILE = "iamUserProfile";
	private static final String IAM_USER_PROFILE_PASS = "tempPassword";

	/**
	 * This method is usefull to get the access token form service
	 * 
	 * @param requestData
	 * @see com.tmobile.eservice.api.services.RestService#getAccessToken(java.lang.String,
	 *      java.lang.String[])
	 */
	@Override
	public String getAccessToken(RequestData requestData) {
		logger.info("getAccessToken method called in RestServiceImpl");
		String accessToken = null;
		try {
			RequestSpecification requestSpecification = ProfileServiceRequestBuilder
					.createAccessTokenRequest(requestData);
			Response response = given(requestSpecification).post();
			
			int statusCode = response.getStatusCode();
			if (statusCode != 200) {
				logger.error("Failed to get token from service{}", getMessage(response));
			} else {
				accessToken = response.getBody().jsonPath().getString("access_token");
			}
		} catch (Exception e) {
	
			logger.error("IAM Exception occurred :::: " + e.getMessage());
			System.out.println("Unable to retreive the access token for text message service");
		}
		 //System.out.println("Access Token :::: " + accessToken);
		return accessToken;
	}
	
	
	/**
	 * This method to return the OTP from service
	 * 
	 * @param requestData
	 * @return true/false
	 */
	@Override
	public boolean sendPinToMsisdn(RequestData requestData) {
		logger.info("sendPinToMsisdn method called in RestServiceImpl");
		Boolean sendPinToMsisdn = false;
		try {
			RequestSpecification msisdnRequest = ProfileServiceRequestBuilder.createPinToMsisdnRequest(requestData);
			Response response = given(msisdnRequest).get();
			int statusCode = response.getStatusCode();
			if (statusCode != 200) {
				logger.error("Failed to get token from service{}", getMessage(response));
			}
			sendPinToMsisdn = true;
		} catch (Exception e) {
			throw new ServiceException("Unable to retreive the sendPinToMsisdn form service{}", e);
		}
		return sendPinToMsisdn;
	}

	

	@Override
	public boolean unlockAccount(RequestData requestData) {
		logger.info("clearCounters method called in RestServiceImpl");
		boolean profileTypeBoolean = false;
		try {
			RequestSpecification requestSpecification = ProfileServiceRequestBuilder.clearCounterRequest(requestData);
			// System.out.println("Request in unlockAccount :::: " +
			// given(requestSpecification).log().all());
			Response response = given(requestSpecification).post();
			// System.out.println("Response Header in unlockAccount :::: " +
			// response.getHeaders().toString());
			// System.out.println("Response Body in unlockAccount :::: " +
			// response.getBody().toString());
			int statusCode = response.getStatusCode();
			if (statusCode != 200) {
				logger.error("Failed to Unable unlockAccount from service{}", getMessage(response));
			} else {
				String profileType = response.getBody().jsonPath().getString("profile_type");
				// System.out.println("Profile Type " +profileType);
				if (StringUtils.isNotEmpty(profileType)) {
					profileTypeBoolean = true;
				}
			}

		} catch (Exception e) {
			throw new ServiceException("Unable to unlockAccount{}", e);
		}
		//System.out.println("Unlock Account Return Status " +profileTypeBoolean);
		return profileTypeBoolean;
	}

	/**
	 * This method to return the OTP from profile
	 * 
	 * @param requestData
	 */
	@Override
	public com.tmobile.eservices.qa.api.unlockdata.Response getProfile(RequestData requestData) {
		logger.info("getProfile method called in RestServiceImpl");
		com.tmobile.eservices.qa.api.unlockdata.Response response = new com.tmobile.eservices.qa.api.unlockdata.Response();
		try {
			RequestSpecification profileRequest = ProfileServiceRequestBuilder.createProfileRequest(requestData);
			Response serviceResponse = given(profileRequest).get();
			int statusCode = serviceResponse.getStatusCode();
			if (statusCode != 200) {
				logger.error("Failed to get pincode from form profile service{}", getMessage(serviceResponse));
				throw new ServiceException("Unable to get profile  details from service{}");
			}
			JsonPath jsonPath = serviceResponse.getBody().jsonPath();
			Map<String, String> jsonMap;
			if (requestData.getProfileType().equalsIgnoreCase(PROFILE_TYPE)) {
				jsonMap = jsonPath.get(TMOLINE_PROFILE);
				response.setPinCode(jsonMap.get(PINCODE));
				response.setZipCode(jsonMap.get(TMBILLING_ZIP));

			} else {
				jsonMap = jsonPath.get(IAM_USER_PROFILE);
				response.setTempPassword(jsonMap.get(IAM_USER_PROFILE_PASS));
			}
			response.setStatusCode(statusCode);
		} catch (Exception e) {
			throw new ServiceException("Unable to get profile  details from service{}", e);
		}
		return response;
	}

	private String getMessage(Response response) {
		return String.format("%s,%s", response.getStatusLine(), response.getStatusCode());
	}

	/**
	 * This method is used to delete the profile in IAM/TMO based on profile
	 * type
	 */
	@Override
	public com.tmobile.eservices.qa.api.unlockdata.Response deleteProfile(RequestData requestData) {
		logger.info("deleteProfile method called in RestServiceImpl");
		com.tmobile.eservices.qa.api.unlockdata.Response response = new com.tmobile.eservices.qa.api.unlockdata.Response();
		try {
			RequestSpecification deleteRequest = ProfileServiceRequestBuilder.createDeleteProfileRequest(requestData);
			Response serviceResponse = given(deleteRequest).delete();
			int statusCode = serviceResponse.getStatusCode();
			response.setStatusCode(statusCode);
			if (statusCode != 200) {
				response.setFailure(new Exception(generateFailureMessage(serviceResponse)));
				logger.error("Unable to delete profile in IAM {}", getMessage(serviceResponse));
			} else {
				response.setStatus("Deleted profile successfully");
			}
		} catch (Exception e) {
			throw new ServiceException("Unable to delete profile in IAM", e);
		}
		return response;
	}

	private String generateFailureMessage(Response serviceResponse) {
		JsonPath jsonPath = serviceResponse.getBody().jsonPath();
		return String.format("%s,%s", jsonPath.get("error_description"), jsonPath.get("error_code"));
	}

	@Override
	public String getDecryption(RequestData requestData) {
		logger.info("getDecrypt method called in RestServiceImpl");
		String decryptedText = null;
		try {
			RequestSpecification decryptionRequest = ProfileServiceRequestBuilder.createDecryptRequest(requestData);
			Response response = given(decryptionRequest).post();
			int statusCode = response.getStatusCode();
			decryptedText = response.getBody().jsonPath().getString("decryptedText");
			if (statusCode != 200) {
				logger.error("Unable to get decrypt code from service {}", getMessage(response));
			}
		} catch (Exception e) {
			System.out.println("Unable to get decrypt code from service {}");
		}
		return decryptedText;
	}
	
	@Override
	public Client createRestClient() {
		logger.info("createRestClient method called in RestServiceImpl");
		Client client = null;
		try {
			TrustManagerAndVerify managerAndVerify = new TrustManagerAndVerify();
			client = ClientBuilder.newBuilder().sslContext(managerAndVerify.getContext())
					.hostnameVerifier(managerAndVerify.insecureHostNameVerifier()).build();
		} catch (Exception sexp) {
			throw new ServiceException("Not able to create the client", sexp);
		}
		return client;
	}

	@Override
	public boolean changePassword(RequestData requestData) {

		logger.info("changePassword method called in RestServiceImpl");
		boolean changePasswordSuccess = false;
		/*try {
			RequestSpecification requestSpecification = ProfileServiceRequestBuilder
					.createResetPasswordRequest(requestData);

			Response response = given(requestSpecification).post();

			int statusCode = response.getStatusCode();
			if (statusCode != 200) {
				logger.error("Failed to change password from service{}", getMessage(response));
			} else if (statusCode == 200) {
				String temp_password = response.getBody().jsonPath().getString("temp_password");
				// System.out.println("Profile Type " +profileType);
				if (StringUtils.isNotEmpty(temp_password)) {
					requestData.setTempPassword(temp_password);
					RequestSpecification requestChangePasswordSpecification = ProfileServiceRequestBuilder
							.createChangePasswordRequest(requestData);
					Response ChangePasswordResponse = given(requestChangePasswordSpecification).post();
					if (ChangePasswordResponse.getStatusCode() == 200) {
						changePasswordSuccess = true;
						logger.info("change password for mentioned misidn is success " + requestData.getMsisdn());
					}
				}
			}

		} catch (Exception e) {
			logger.info("change password for mentioned misidn is failed " + requestData.getMsisdn());
			throw new ServiceException("Unable to changePassword{}", e);
		}
*/		return changePasswordSuccess;
	}

	public String generateTempPin(RequestData requestData) {
		String tempPin = null;
		try {
			RequestSpecification msisdnRequest = ProfileServiceRequestBuilder.createTempPinRequest(requestData);
			Response response = given(msisdnRequest).post();
			int statusCode = response.getStatusCode();
						
			if (statusCode != 200) {
				logger.error("Failed to get token from service{}", getMessage(response));
			} else {
				requestData.setTempPassword(response.getBody().jsonPath().getString("tempPin"));
				tempPin = getDecryption(requestData);
			}
		} catch (Exception e) {
			throw new ServiceException("Unable to retreive the sendPinToMsisdn form service{}", e);
		}
		return tempPin;
	}	
	
	 /**
     * This method is used to sendPinToMsisdn
     *
     * @param requestData
     * @throws Exception
     */
  /*  public String getTempPassword() throws Exception {
        //logger.info("getTempPassword method called in RestServiceHelper");
        
        String decryptedCode = null;
        //requestData.setTempPassword(null);
       RestServiceImpl restService = new RestServiceImpl();
        boolean sendPinToMsisdn = sendPinToMsisdn(requestData);
        if (sendPinToMsisdn) {
            String token = requestData.getToken();
            String[] splitToken = token.split(" ");
            requestData.setProfileType(IAM_PROFILE_TYPE);
            requestData.setToken(splitToken[1]);
            Response response = restService.getProfile(requestData);
            if (response.getStatusCode() == 200) {
                //requestData.setTempPassword(null);
                String tempPassword = null;
                Thread.sleep(2000);
                        tempPassword = response.getTempPassword();
                response = null;        
                if (StringUtils.isNotEmpty(tempPassword)) {
                    requestData.setTempPassword(tempPassword);
                    decryptedCode = restService.getDecryption(requestData);
                    
                    return decryptedCode;
                }
            }
        }
        System.out.println(decryptedCode);
        return null;
    }*/

}
