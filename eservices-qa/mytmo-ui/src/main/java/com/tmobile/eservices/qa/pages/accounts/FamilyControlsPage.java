/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class FamilyControlsPage extends CommonPage {

	public FamilyControlsPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(css = "input[name='inputText']")
	private WebElement familyControlsLineSelector;

	@FindBy(xpath = "//span[contains(text(),'having some trouble')]")
	private WebElement sSOErrorMessage;

	@FindBy(xpath = "//span[contains(text(),'View list of all lines')]")
	private WebElement familyControlsViewListOfLines;

	@FindBy(xpath = "//p[contains(text(),'Controls')]")
	private WebElement familyControls;

	@FindBy(css = "input[id='username']")
	private WebElement username;

	@FindBy(css = "input[id='password']")
	private WebElement pasword;

	@FindBy(css = "input[type='submit']")
	private WebElement login;
	@FindBy(css = "span[class='gh-arrow-down-gray gh-float-right']")
	private WebElement regDropDown;

	@FindBy(css = "a[aria-label='Profile']")
	private WebElement profile;

	@FindBy(css = "p.Display3")
	private WebElement pageHeader;

	@FindBy(xpath = "//p[contains(text(),'Web Guard')]")
	private WebElement webGuard;

	@FindBy(xpath = "//p[contains(text(),'Where')]")
	private WebElement familyWhere;

	@FindBy(xpath = "//p[contains(text(),'Allowances')]")
	private WebElement familyAllowances;

	@FindBy(xpath = "//a[contains(text(),'Manage')]")
	private WebElement manageLink;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement saveButton;

	@FindBy(css = "span.body.brand-magenta.cursor")
	private WebElement clickonfilterdetails;

	@FindBy(xpath = "//label[text()='Child']/../input")
	private WebElement childRadioBtn;

	@FindBy(xpath = "//label[text()='Teen']/../input")
	private WebElement teenRadioBtn;

	@FindBy(xpath = "//label[text()='Teen']")
	private WebElement teenBtn;

	@FindBy(xpath = "//label[text()='Child']")
	private WebElement childBtn;

	@FindBy(xpath = "//span[text()='Show Filter Details']")
	private WebElement showFilterBtn;

	@FindBy(xpath = "//span[text()='Hide Filter Details']")
	private WebElement hideFilterBtn;

	@FindBys(@FindBy(css = "div.row.padding-bottom-xl"))
	private List<WebElement> permissionDetailsTxt;

	@FindBys(@FindBy(css = "div.body.radio-container input"))
	private List<WebElement> webGuardPermissionradioBtns;

	@FindBy(css = "div.body.radio-container label")
	private List<WebElement> webGuardPermissionlabels;

	@FindBy(xpath = "//span[contains(text(),'T-Mobile FamilyMode')]")
	private WebElement bearModeDiv;

	@FindBy(css = "img.Appstore-icon")
	private WebElement appStoreIcon;

	@FindBy(css = "img.playstore-icon")
	private WebElement playStoreIcon;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement lineHeader;

	@FindBys(@FindBy(css = "div#test ul li"))
	private List<WebElement> lineList;

	@FindBy(css = "i[class*='search-icon']")
	private WebElement searchIconOnLineSelectorSearchTextBar;

	@FindBy(css = "#inputText")
	private WebElement lineSelectorSearchTextBar;

	@FindBy(css = "#inputText")
	private List<WebElement> lineSelectorSearchTextBarList;

	@FindBy(css = "div.padding-top-small")
	private List<WebElement> recentHistoryOfLineSelectorSearchBar;

	@FindBy(css = "div[class*='suggestions'] ul li")
	private List<WebElement> suggestionListOfLineSelectorSearchBar;

	@FindBy(css = "i[class*='clear-icon']")
	private WebElement lineSelectorClearIon;

	@FindBy(xpath = "//span[contains(text(),'View list of all lines')]")
	private WebElement ViewListOfAllLinesLinkInFamilyControls;

	@FindBy(xpath = "//div[div[p[contains(text(),'Allowances')]/..]/..][not(contains(@class,'active'))]")
	private WebElement familyAllowancesActivelink;

	@FindBy(xpath = "//div[div[p[contains(text(),'Allowances')]/..]/..][(contains(@class,'active'))]")
	private WebElement familyAllowancesInActivelink;

	@FindBy(xpath = "//*[contains(text(),'This line is currently suspended')]")
	private WebElement suspendedLineMessage;

	@FindBy(xpath = "(//div[contains(@tmo-layout,'grid')]/div[contains(@class,'padding-top-small')]/a)[1]")
	private WebElement firstMissdninViewListOfLines;

	@FindBy(css = "p.Display3")
	private WebElement profilePageHeader;

	@FindBy(xpath = "(//div[contains(@tmo-layout,'grid')]/div[contains(@class,'padding-top-small')]/a)[2]")
	private WebElement missdninViewListOfLines;

	@FindBy(xpath = "//p[contains(text(),'Allowances')]")
	private WebElement familyAllowancesLink;

	@FindBy(xpath = "//p[contains(text(),'Web Guard')]/../p[3]")
	private WebElement updatedWebGuardPermisson;

	/**
	 * select first missdn in view list of lines from family controls page
	 * 
	 * @return
	 */
	public String selectAndRetrieveMissdninViewListOfLine() {
		String selectedMissd = null;
		try {
			checkPageIsReady();
			selectedMissd = missdninViewListOfLines.getText();
			clickElement(missdninViewListOfLines);
			waitforSpinnerinProfilePage();
		} catch (Exception e) {
			Assert.fail("unable to click  missdn in view list of lines page");
		}
		return selectedMissd;
	}

	/**
	 * /** Verify page Header.
	 */
	public FamilyControlsPage verifyFamilyControlsPage() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			pageHeader.isDisplayed();
			Reporter.log("Family controls page displayed");
		} catch (Exception e) {
			Assert.fail("Family controls page not displayed");
		}
		return this;
	}

	/**
	 * /** Verify page Header.
	 */
	public FamilyControlsPage verifyProfilePage() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(profilePageHeader));
			pageHeader.isDisplayed();
			Reporter.log("Profile page dispalyed");
		} catch (Exception e) {
			Assert.fail("Profile page not displayed");
		}
		return this;
	}

	/**
	 * select first missdn in view list of lines from family controls page
	 * 
	 * @return
	 */
	public FamilyControlsPage clickFirstMissdninViewListOfLines() {
		try {
			checkPageIsReady();
			clickElement(firstMissdninViewListOfLines);
			Reporter.log("Clicked first missdn successfully");
		} catch (Exception e) {
			Assert.fail("unable to click first missdn in view list of lines page");
		}
		return this;
	}

	/**
	 * click on webguard
	 */
	public FamilyControlsPage clickWebGuard() {
		try {
			webGuard.click();
			Reporter.log("Clicked on webguard");
		} catch (NoSuchElementException e) {
			Assert.fail("Click on webguard failed" + e.getMessage());
		}
		return this;
	}

	/**
	 * click on webguard
	 */
	public String selectWebGuardPermissions() {
		String permissionSelected = null;
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			for (int i = 0; i < webGuardPermissionradioBtns.size(); i++) {
				if (!webGuardPermissionradioBtns.get(i).isSelected()) {
					clickElementWithJavaScript(webGuardPermissionlabels.get(i));
					permissionSelected = webGuardPermissionlabels.get(i).getText();
					break;
				}
			}
			Reporter.log("Changing permission level operation is success");
		} catch (NoSuchElementException e) {
			Assert.fail("Changing permission level operation is failed");
		}
		return permissionSelected;
	}

	/**
	 * click on save button
	 */
	public FamilyControlsPage clickSaveBtn() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(saveButton);
			Reporter.log("Clicked on save button");
		} catch (Exception e) {
			Assert.fail("Click on save button failed");
		}
		return this;
	}

	/**
	 * click Child RadioButton.
	 */
	public FamilyControlsPage clickChildRadioBtn() {
		try {
			if (!childRadioBtn.isSelected()) {
				childBtn.click();
			} else {
				teenBtn.click();
			}
			Reporter.log(" Child radio button selected");
		} catch (Exception e) {
			Assert.fail("Click on RadioBtn checkbox failed");
		}
		return this;
	}

	/**
	 * click SaveChanges btn.
	 */
	public FamilyControlsPage clickSaveChangesBtn() {
		waitforSpinnerinProfilePage();
		try {
			if (saveButton.isEnabled()) {
				Reporter.log("Save changes button is enabled");
				saveButton.click();
			} else {
				Assert.fail("Save changes button is in disabled state");
			}
		} catch (Exception e) {
			Assert.fail("Click on save changes button failed");
		}
		return this;
	}

	/**
	 * click ShowFilter btn.
	 */
	public FamilyControlsPage clickShowFilterBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (showFilterBtn.isEnabled()) {
				Reporter.log("Show filter button is enabled");
				showFilterBtn.click();
			} else {
				Assert.fail("Show filter button is in disabled state");
			}
		} catch (Exception e) {
			Assert.fail("Click on show filter button failed");
		}
		return this;
	}

	/**
	 * click hideFilter btn.
	 */
	public FamilyControlsPage clickHideFilterBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (hideFilterBtn.isEnabled()) {
				Reporter.log("Hide filter button is enabled");
				hideFilterBtn.click();
			} else {
				Assert.fail("Hide filter button is in disabled state");
			}
		} catch (Exception e) {
			Assert.fail("Click on Hide filter button failed");
		}
		return this;
	}

	/**
	 * Verify Permission text.
	 */
	public FamilyControlsPage verifyPermissionText() {
		try {
			permissionDetailsTxt.get(0).isDisplayed();
			Reporter.log("Permission details displayed");

		} catch (Exception e) {
			Assert.fail("Permission details not displayed");
		}
		return this;
	}

	/**
	 * click on Family Where
	 */
	public FamilyControlsPage clickFamilyWhere() {
		try {
			familyWhere.click();
			Reporter.log("Clicked on family where link");
		} catch (NoSuchElementException e) {
			Assert.fail("Family where link not Found");
		}
		return this;
	}

	/**
	 * click on Family Allowances
	 */
	public FamilyControlsPage clickFamilyAllowances() {
		try {
			waitFor(ExpectedConditions.visibilityOf(familyAllowances));
			familyAllowances.click();
		} catch (NoSuchElementException e) {
			Reporter.log("Family allowance link not found");
			Assert.fail("Family allowance link not Found");
		}
		return this;
	}

	/**
	 * click on Manage link
	 */
	public FamilyControlsPage clickManageLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(manageLink));
			manageLink.click();
			Reporter.log("Clicked on manage link");
		} catch (NoSuchElementException e) {
			Assert.fail("Manage link not found");
		}
		return this;
	}

	/**
	 * verify bear mode div
	 */
	public FamilyControlsPage verifyBearModeDiv() {
		try {
			waitFor(ExpectedConditions.visibilityOf(bearModeDiv));
			bearModeDiv.isDisplayed();
			Reporter.log("Bear mode div is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Bear mode div not displayed");
		}
		return this;
	}

	/**
	 * verify appstore icon
	 */
	public FamilyControlsPage verifyAppleStoreIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(appStoreIcon));
			appStoreIcon.isDisplayed();
			Reporter.log("Apple store icon displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Apple store icon not displayed");
		}
		return this;
	}

	/**
	 * verify playstore icon
	 */
	public FamilyControlsPage verifyPlayStoreIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(playStoreIcon));
			playStoreIcon.isDisplayed();
			Reporter.log("Play store icon displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Play store icon not displayed");
		}
		return this;
	}

	/**
	 * click on Manage link
	 */
	public FamilyControlsPage clickAppleStoreIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(appStoreIcon));
			appStoreIcon.click();
			Reporter.log("Clicked on appStore icon");
		} catch (NoSuchElementException e) {
			Assert.fail("Appstore link not found");
		}
		return this;
	}

	/**
	 * click on Manage link
	 */
	public FamilyControlsPage clickPlayStoreIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(playStoreIcon));
			playStoreIcon.click();
			Reporter.log("Clicked on playStore icon");
		} catch (NoSuchElementException e) {
			Assert.fail("Playstore link not found");
		}
		return this;
	}

	/**
	 * verify save button enabled
	 */
	public FamilyControlsPage verifySaveBtnEnabled() {
		try {
			saveButton.isEnabled();
			Reporter.log("save button is enabled");
		} catch (NoSuchElementException e) {
			Assert.fail("save button not enabled");
		}
		return this;
	}

	/***
	 * click on the clear icon for the line selector field
	 * 
	 * @return
	 */
	public FamilyControlsPage verifyClearIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(lineSelectorClearIon));
			Reporter.log("Clear search icon is displayed for the line selector search text field");
		} catch (Exception e) {
			Assert.fail("Clear icon for line selector field is not displayed");
		}
		return this;
	}

	/***
	 * verifying the presence of clear icon for line selector field
	 * 
	 * @return
	 */
	public FamilyControlsPage clickClearIcon() {
		try {
			checkPageIsReady();
			clickElement(lineSelectorClearIon);
			Reporter.log("Clicked on the Clear icon");
		} catch (Exception e) {
			Assert.fail("Clear icon for line selector field is not displayed");
		}
		return this;
	}

	/***
	 * verifying the presence of line selector search text field
	 * 
	 * @return
	 */
	public FamilyControlsPage verifySearchIconOnLineSelectorSearchTextBar() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(searchIconOnLineSelectorSearchTextBar),
					"Search icon for line selector field is not displayed");
			Reporter.log("Search icon for line selector field is displayed");
		} catch (Exception e) {
			Assert.fail("Search icon for line selector field is not displayed");
		}
		return this;
	}

	/***
	 * verifying the value of line selector is null
	 * 
	 * @return
	 */
	public FamilyControlsPage verifyNoTextInLineSelectorSearchTextBar() {
		try {
			checkPageIsReady();
			Assert.assertEquals("", lineSelectorSearchTextBar.getAttribute("value").trim());
			Reporter.log("The line selector search value is cleared");
		} catch (Exception e) {
			Assert.fail("failed to verify the value in the line selector field upon clicking clear icon");
		}
		return this;
	}

	/***
	 * verifying the value selected in the profile page with the page load value in
	 * family controls page
	 * 
	 * @param expectedLine
	 * @return
	 */
	public FamilyControlsPage verifyDefaultLineValueForLineSelectorSearchTextBar(String expectedLine) {
		try {
			checkPageIsReady();
			Assert.assertEquals(expectedLine, lineSelectorSearchTextBar.getAttribute("value").trim());
			Reporter.log("The line selector value loaded by default is the same as the line selected on profile page");
		} catch (Exception e) {
			Assert.fail("failed to verify the default line populated on the line selector field");
		}
		return this;
	}

	/***
	 * verify absence of line selector field for single line account
	 * 
	 * @return
	 */
	public FamilyControlsPage verifyLineSelector() {
		try {
			checkPageIsReady();
			Assert.assertEquals(lineSelectorSearchTextBarList.size(), 0);
			Reporter.log("Line selector field is not displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify the presence of line selector field");
		}
		return this;
	}

	/***
	 * verifying the auto complete functionality for line selector field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public FamilyControlsPage verifyAutoCompleteSearchResultsForLineSelectorField(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			for (WebElement ele : suggestionListOfLineSelectorSearchBar) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					actListOfValuesThatMatchedTheSearchValue++;
				}
			}
			Assert.assertEquals(suggestionListOfLineSelectorSearchBar.size(), actListOfValuesThatMatchedTheSearchValue);
			Reporter.log("Autocomplete search functionality works for line selector search field");
		} catch (Exception e) {
			Assert.fail("failed to verify the Autocomplete search functionality for line selector search field");
		}
		return this;
	}

	/***
	 * verification of line selector search results for no matching search criterion
	 * 
	 * @param expectedValue
	 * @return
	 */
	public FamilyControlsPage verifyAutoCompleteSearchResultsForNoMathingSearchCriteria(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 1;
		try {
			checkPageIsReady();
			for (WebElement ele : recentHistoryOfLineSelectorSearchBar) {
				if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
					actListOfValuesThatMatchedTheSearchValue++;
				}
			}
			Assert.assertEquals(recentHistoryOfLineSelectorSearchBar.size(), actListOfValuesThatMatchedTheSearchValue);
			Reporter.log("Line selector does not return the list of lines for invalid search");
		} catch (Exception e) {
			Assert.fail("failed to verify the Autocomplete search functionality for invalid line selector");
		}
		return this;
	}

	/***
	 * verifying hover functionality for line selector search results
	 * 
	 * @return
	 */
	public FamilyControlsPage verifyHoverFunctionalityInSearchResultsForLineSelectorField() {
		try {
			checkPageIsReady();
			Random randomNum = new Random();
			int randomLineToSelect = 1 + randomNum.nextInt(suggestionListOfLineSelectorSearchBar.size());
			String expLineDetails = suggestionListOfLineSelectorSearchBar.get(randomLineToSelect).getText().trim();
			Actions action = new Actions(getDriver());
			action.moveToElement(suggestionListOfLineSelectorSearchBar.get(randomLineToSelect)).build().perform();
			String actLineDetails = lineSelectorSearchTextBar.getAttribute("value").trim();
			Assert.assertEquals(expLineDetails, actLineDetails);
			Reporter.log("The line hovered from the search result list appeared in selector search field");
		} catch (Exception e) {
			Assert.fail("failed to verify the hover functionality for line selector search field");
		}
		return this;
	}

	/***
	 * verifying the tab functionality for the line selector search results
	 * 
	 * @param expLineDetails
	 * @return
	 */
	public FamilyControlsPage verifyTabOutFunctionalityForLineSelectorField(String expLineDetails) {
		try {
			checkPageIsReady();
			lineSelectorSearchTextBar.sendKeys(Keys.TAB);
			String actLineDetails = lineSelectorSearchTextBar.getAttribute("value").trim();
			Assert.assertEquals(expLineDetails, actLineDetails);
			Reporter.log("The line is reset to the last selected value upon tab out");
		} catch (Exception e) {
			Assert.fail("failed to verify the last selected value upon tab out");
		}
		return this;
	}

	/***
	 * enter text in the line selector text field
	 * 
	 * @param value
	 * @return
	 */
	public FamilyControlsPage setTextForLineSelectorSearchTextBar(String value) {
		try {
			checkPageIsReady();
			sendTextData(lineSelectorSearchTextBar, value);
			Reporter.log("The line selector field is entered with the value " + value);
		} catch (Exception e) {
			Assert.fail("failed to set text in the line selector field");
		}
		return this;
	}

	/***
	 * verify the cleared history value for line selector
	 * 
	 * @param expectedValue
	 * @return
	 */
	public FamilyControlsPage verifyClearedHistoryValue(String expectedValue) {
		boolean isLineDisplayedInHistory = false;
		try {
			checkPageIsReady();
			for (WebElement ele : recentHistoryOfLineSelectorSearchBar) {
				if (ele.getText().trim().equals(expectedValue)) {
					isLineDisplayedInHistory = true;
					break;
				}
			}
			Assert.assertTrue(isLineDisplayedInHistory);
			Reporter.log("The line cleared appears in the recent search history");
		} catch (Exception e) {
			Assert.fail("failed to verify the default line populated on the line selector field");
		}
		return this;
	}

	/**
	 * Verify and validate the List of lines link
	 * 
	 * @return
	 */
	public FamilyControlsPage verifyAndClickViewListOfLinesLinkInFamilyControlsPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(ViewListOfAllLinesLinkInFamilyControls));
			ViewListOfAllLinesLinkInFamilyControls.click();
		} catch (Exception e) {
			Assert.fail("'View list of lines' text is not Displayed");
		}
		return this;
	}

	/**
	 * Navigate to Profile page from login page
	 * 
	 * @return
	 */
	public FamilyControlsPage navigateToFamilyControlsPage() {
		checkPageIsReady();
		try {
			clickElement(familyControls);
			checkPageIsReady();
			Reporter.log("Clicked on family allowance");
		} catch (NoSuchElementException e) {
			Assert.fail("Family allowance link not Found");
		}
		return this;
	}

	/**
	 * Navigate to Profile page from login page
	 * 
	 * @return
	 */
	public FamilyControlsPage navigateToProfilePage() {
		checkPageIsReady();
		try {
			clickElement(regDropDown);
			checkPageIsReady();
			clickElement(profile);
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("selected line is not  validated successfully");
		}
		return this;
	}

	/**
	 * Log in to tmobile website
	 * 
	 * @param mssdn
	 * @param password
	 * @return
	 */
	public FamilyControlsPage login(String mssdn, String password) {
		try {
			checkPageIsReady();
			username.sendKeys(mssdn);
			pasword.sendKeys(password);
			clickElement(login);
			Reporter.log("logged in successfully with missdn and password");
		} catch (Exception e) {
			Assert.fail("failed to log into tmbile website");
		}
		return this;
	}

	/**
	 * Verify family allowance link
	 */
	public FamilyControlsPage verifyFamilyAllowancesLink() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(familyAllowancesLink));
			Reporter.log("Family allowances link  not displayed");
		} catch (Exception e) {
			Assert.fail("Family allowances link displayed");
		}
		return this;
	}

	/**
	 * Verify updated webguard permisson.
	 */
	public FamilyControlsPage verifyUpdatedWebGuardPermsission(String selectedWebGuardPermisson) {
		String updatedPermsission = null;
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			updatedPermsission = updatedWebGuardPermisson.getText();
			Assert.assertTrue(updatedPermsission.contains(selectedWebGuardPermisson),
					"WebGuard permission not updated successfully");
			Reporter.log("WebGuard permission update is success");
		} catch (AssertionError e) {
			Assert.fail("WebGuard permission not updated successfully");
			Reporter.log("WebGuard permission not updated successfully");
		}
		return this;
	}

	/**
	 * Verify view list of lines not displayed
	 * 
	 * @return
	 */
	public FamilyControlsPage verifyViewListOfLinesNotDisplayed() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			Assert.assertFalse(isElementDisplayed(ViewListOfAllLinesLinkInFamilyControls));
			Reporter.log("View list of lines link not displayed");
		} catch (AssertionError e) {
			Reporter.log("View list of lines link displayed");
			Assert.fail("View list of lines link displayed");
		}
		return this;
	}

	/**
	 * Verify line selector dropdown not displayed
	 * 
	 * @return
	 */
	public FamilyControlsPage verifyLineSelectorDropDownNotDisplayed() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			Assert.assertFalse(isElementDisplayed(lineHeader));
			Reporter.log("Line selector dropdown not displayed");
		} catch (AssertionError e) {
			Reporter.log("Line selector dropdown not displayed");
			Assert.fail("Line selector dropdown not displayed");
		}
		return this;
	}

}
