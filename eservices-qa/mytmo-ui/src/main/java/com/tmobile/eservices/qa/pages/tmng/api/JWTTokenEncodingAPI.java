package com.tmobile.eservices.qa.pages.tmng.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class JWTTokenEncodingAPI extends ApiCommonLib {

	/***
	 * This is the API will give decoded access JWT Token
	 * 
	 * @return
	 * @throws Exception
	 */

	public Response accesscodeEncode(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildAccesscodeEncodeHeader(apiTestData);
		RestService service = new RestService(requestBody, "https://tos-shop-qa1.dd-stg.kube.t-mobile.com");
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("api/oauth", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildAccesscodeEncodeHeader(ApiTestData apiTestData) throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-type", "application/json");

		return headers;
	}

}
