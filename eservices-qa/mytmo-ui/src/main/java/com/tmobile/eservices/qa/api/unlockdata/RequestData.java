package com.tmobile.eservices.qa.api.unlockdata;

import java.io.Serializable;

/**
 * @author blakshminarayana
 *
 */
@SuppressWarnings("serial")
public class RequestData implements Serializable {
	private String token;
	private String profileType;
	private String environment;
	private String banNumber;
	private String msisdn;
	private String tempPassword;
	private String userId;
	private String notification;
	

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token
	 *            the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the profileType
	 */
	public String getProfileType() {
		return profileType;
	}

	/**
	 * @param profileType
	 *            the profileType to set
	 */
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}

	/**
	 * @param environment
	 *            the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	/**
	 * @return the banNumber
	 */
	public String getBanNumber() {
		return banNumber;
	}

	/**
	 * @param banNumber
	 *            the banNumber to set
	 */
	public void setBanNumber(String banNumber) {
		this.banNumber = banNumber;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn
	 *            the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@Override
	public String toString() {
		return "RequestData [token=" + token + ", profileType=" + profileType + ", environment=" + environment
				+ ", banNumber=" + banNumber + ", msisdn=" + msisdn + ", tempPassword=" + tempPassword + ", userId="
				+ userId + ", notification=" + notification + "]";
	}

	/**
	 * @return the tempPassword
	 */
	public String getTempPassword() {
		return tempPassword;
	}

	/**
	 * @param tempPassword
	 *            the tempPassword to set
	 */
	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}
	
	
}
