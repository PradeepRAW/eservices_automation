package com.tmobile.eservices.qa.pages.payments;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * 
 * @author sam
 *
 */
public class UsagePage extends CommonPage {

	@FindBy(css = "a[_ngcontent-c2]")
	private WebElement downloadUsageRecords;

	@FindBy(css = ".col-12.Display3")
	private WebElement usageOverviewHeader;

	@FindBy(css = "main.d-table.d-md-flex")
	private WebElement usageOverviewPage;

	@FindBy(css = ".col-12.Display3")
	private WebElement billingCycleDate;

	@FindBy(css = "div.tab-content div p.black.Display4.float-left.text-left")
	private List<WebElement> viewByCategoryOptions;

	@FindBy(css = "div.accordion-content.content-open div.row.body div.row div.active")
	private List<WebElement> viewByCategoryLines;

	@FindBy(id = "content11")
	private WebElement viewByLineAccounts;

	@FindBy(css = "div.chart-inline.no-padding a")
	private List<WebElement> billCycles;

	@FindBy(xpath = "//label[contains(text(),'View by Line')]")
	private WebElement viewByLineTab;

	@FindBy(xpath = "//div[@id='content00']//div[@class='row no-margin']")
	private List<WebElement> itemsviewByCatagory;

	@FindBy(xpath = "//div[@class='row no-margin']")
	private List<WebElement> itemsviewByLine;

	@FindBy(xpath = "//label[contains(text(),'View by Line')]/parent::div")
	private WebElement viewByLine;

	@FindBy(xpath = "//label[contains(text(),'View by Category')]/parent::div")
	private WebElement viewByCategory;

	@FindBy(css = "div#Chart")
	private WebElement charticon;

	@FindBy(css = "div[class='col-11 no-padding']")
	private WebElement lineRow;

	@FindBy(xpath = "//p[contains(text(),'Data')]")
	private WebElement categoryData;

	@FindBy(xpath = "//p[contains(text(),'Mobile Hotspot')]")
	private WebElement hotSpotData;

	private final String pageUrl = "usage";

	@FindBy(css = "div.accordion-content.content-open div.text-right p.body")
	private List<WebElement> textOnDataBlade;


	@FindBy(xpath = "//p[contains(@pid,'cust_msisdn')]")
	private List<WebElement> viewMsisdinByCategoryLines;
	
	@FindBy(xpath = "//p[contains(text(),'Worried about experiencing slower speeds?')]")
	private WebElement lowersppedstext;

	@FindBy(xpath = "//button[contains(text(),'Manage Data')]")
	private WebElement buttonManageData;

	private By landingpageSpinner = By.cssSelector("div.circle");

	/**
	 * 
	 * @param webDriver
	 */
	public UsagePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public UsagePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	public UsagePage waitforchartload() {
		waitFor(ExpectedConditions.visibilityOf(charticon));

		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @return
	 */
	public UsagePage verifyPageLoaded() {
		try {
			Thread.sleep(3000);
			checkPageIsReady();
			verifyPageUrl();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(usageOverviewPage), 7);
			}
			Reporter.log("Usage page is loaded");
		} catch (Exception e) {
			Assert.fail("Failed to load Usage page");
		}
		return this;
	}

	/**
	 * Verify Download Usage Records is present on the page
	 */
	public UsagePage downloadUsageDetails() {
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			waitFor(ExpectedConditions.visibilityOf(downloadUsageRecords));
			Assert.assertTrue(downloadUsageRecords.isDisplayed(), "Download Usage Records is not present");
			Reporter.log("Verified download records");
		} catch (Exception e) {
			Assert.fail("Download usage records not found");
		}
		return this;
	}

	/**
	 * click view by line selector
	 */
	public UsagePage clickViewByLineSelector() {
		try {
			viewByLineTab.click();
			Reporter.log("Clicked on view By Line Tab");
		} catch (Exception e) {
			Assert.fail("Fail to click on view By Line Tab");
		}
		return this;
	}

	/**
	 * Verify All elements on Usage page
	 */
	public UsagePage verifyAllPageElements() {
		try {
			Assert.assertTrue(usageOverviewHeader.isDisplayed(), "Usage Overview header is not present");
			Assert.assertTrue(billingCycleDate.isDisplayed(), "Billing cycle dates is not present");
			Reporter.log("Verified all the page elements");
		} catch (Exception e) {
			Assert.fail("Failed to verify the page elements");
		}
		return this;
	}

	/**
	 * Click View By Category Option
	 * 
	 * @param option
	 * @return
	 */
	public UsagePage clickOptionViewByCategory(String option) {
		try {
			for (WebElement webElement : viewByCategoryOptions) {
				waitFor(ExpectedConditions.visibilityOf(webElement));
				if (webElement.getText().equalsIgnoreCase(option)) {
					webElement.click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("View by category options are not displayed");
		}
		return this;
	}

	/**
	 * Click View By Category [Line]
	 * 
	 * @return
	 */
	public UsagePage clickLineViewByCategory() {
		try {
			for (WebElement webElement : viewByCategoryLines) {
				waitFor(ExpectedConditions.visibilityOf(webElement));
				clickElement(webElement);

				break;
			}
		} catch (Exception e) {
			Assert.fail("View by category lines are not displayed");
		}
		return this;
	}

	/**
	 * Click on Usage options (Data/Calls/Messages) in View By Line
	 * 
	 * @param option
	 * @return
	 */
	public UsagePage clickOnUsageOptionsViewByLine(String option) {
		try {
			waitFor(ExpectedConditions.visibilityOf(viewByLineAccounts));
			viewByLineAccounts.click();
			waitFor(ExpectedConditions.visibilityOfAllElements(viewByCategoryLines));
			for (WebElement webElement : viewByCategoryLines) {
				if (webElement.getText().contains(option)) {
					clickElement(webElement);

					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// Assert.fail("Line Numbers/Usage Options not displayed");
		}
		return this;
	}

	/**
	 * Select Bill Cycle
	 * 
	 * @param option
	 * @return
	 */
	public String selectBillCycle(String option) {
		String billCycleMonth = null;
		try {
			for (WebElement webElement : billCycles) {
				if (webElement.getText().equalsIgnoreCase(option)) {
					billCycleMonth = webElement.getText();

					clickElement(webElement);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Bill cycle is not displayed");
		}
		return billCycleMonth;
	}

	public UsagePage selectrequiredmonthforcheck(String text) {
		try {
			boolean res = false;

			for (WebElement webElement : billCycles) {
				clickElement(webElement);

				Thread.sleep(1000);

				for (WebElement we : viewByCategoryOptions) {
					if (we.getText().trim().equalsIgnoreCase(text)) {
						res = true;
						break;
					}

				}
				if (res == true)
					break;
			}
			if (res = false)
				Assert.fail("Data for " + text + " is not present for given misdin");
		} catch (Exception e) {
			Assert.fail("No billcycles or viewby catogory options");
		}
		return this;
	}

	public WebElement getitemsviewByCatagory(String Catogory) {
		checkPageIsReady();
		waitforSpinner();
		WebElement a = null;
		try {
			for (WebElement ele : itemsviewByCatagory) {
				if (ele.findElement(By.xpath(".//p[contains(@class,'black Display')]")).getText().trim()
						.equalsIgnoreCase(Catogory)) {
					return ele;
				}
			}
		} catch (Exception e) {
			Assert.fail("itemsbyview catogory table not existing");

		}
		return a;
	}

	public UsagePage expandcatagory(WebElement ele, String Catagory) {
		try {
			checkPageIsReady();
			if (ele.findElements(By.xpath(".//span[@class='pull-right cursor arrow-up arrowup_rotation']"))
					.size() == 0) {

				retryingFindClick(
						ele.findElement(By.xpath(".//span[@class='pull-right cursor arrow-down arrowdown_rotation']")));
				Reporter.log("Blade got expanded for :" + Catagory);
			}
		} catch (Exception e) {
			Assert.fail(Catagory + " expand arrow object not identified");
		}
		return this;
	}

	public boolean checkvalidnodewithdata(WebElement ele) {
		try {
			boolean eleexist = false;
			int cnt = 0;
			while (!eleexist) {
				int elecount = ele.findElements(By.xpath(".//div[contains(@class,'Display3')]")).size();
				if (elecount == 0) {
					scrollToElement(ele);
					cnt++;
				} else {
					eleexist = true;
				}

				if (cnt == 20)
					Assert.fail("scroll is not working");
			}

			String gettext = ele.findElement(By.xpath(".//div[contains(@class,'Display3')]")).getText().toString()
					.trim();
			if (gettext.equalsIgnoreCase("0") || gettext.equalsIgnoreCase("0.00")) {
				return false;
			}
		} catch (Exception e) {
			Assert.fail("data element is not identified");
		}
		return true;
	}

	public List<WebElement> getItemsViewByCatagoryAccounts(WebElement ele) {
		checkPageIsReady();
		try {
			return ele.findElements(By.xpath(
					".//ancestor::div[@class='TMO-USAGE-BY-CATEGORY']//div[@class='accordion-content content-open']//div[contains(@class,'align-items-center')]"));
		} catch (Exception e) {
			Assert.fail("itemsbyview catogory table not existing");

		}
		return null;
	}

	public boolean clickOnNodeWithDataExist(WebElement ele) {
		try {
			checkPageIsReady();
			List<WebElement> cheles = ele.findElements(By.xpath(
					".//ancestor::div[@class='TMO-USAGE-BY-CATEGORY']//div[@class='accordion-content content-open']//div[contains(@class,'col-8')]//p[@class='Display6 padding-bottom-xsmall ng-star-inserted']"));
			for (WebElement chele : cheles) {
				String reqtext = chele.getText().toString().trim();
				reqtext = reqtext.split(" ")[0];
				if (!reqtext.equalsIgnoreCase("0") && !reqtext.equalsIgnoreCase("0.00")) {
					retryingFindClick(chele);
					return true;
				}
			}
		} catch (Exception e) {
			Assert.fail("sub nodes are not identified");

		}
		return false;
	}

	public UsagePage clickOnCatogryAccountBlade(List<WebElement> ele, int index) {
		try {
			waitFor(ExpectedConditions.visibilityOf(ele.get(index)), 3);
			retryingFindClick(ele.get(index));
			Reporter.log("Blade is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to identify account specific blades");
		}
		return this;
	}

	public List<WebElement> getbillingCycles() {
		try {
			return billCycles;
		} catch (Exception e) {
			Assert.fail("Month links are not existing");

		}
		return null;
	}

	/**
	 * Click Months
	 * 
	 * @param ele
	 * @param index
	 * @return
	 */
	public UsagePage clickMonths(List<WebElement> ele, int index) {
		try {
			checkPageIsReady();
			retryingFindClick(ele.get(index));
		} catch (Exception e) {
			Assert.fail("Month link is not existing");
		}
		return this;
	}

	public UsagePage clickViewByLine() {
		try {
			clickElement(viewByLine);
		} catch (Exception e) {
			Assert.fail("Viewby line lable is not present");
		}
		return this;
	}

	public UsagePage clickViewByCategory() {
		try {
			clickElement(viewByCategory);
		} catch (Exception e) {
			Assert.fail("Viewby Category lable is not present");
		}
		return this;
	}

	public UsagePage expandlineitemarrows(List<WebElement> ele, int index) {
		try {
			if (ele.get(index).findElements(By.xpath(".//span[@class='pull-right cursor arrow-up arrowup_rotation']"))
					.size() == 0) {
				retryingFindClick(ele.get(index)
						.findElement(By.xpath(".//span[@class='pull-right cursor arrow-down arrowdown_rotation']")));
			}
		} catch (Exception e) {
			Assert.fail("Viewby lines items downarrow is not existing" + index);
		}
		return this;
	}

	public List<WebElement> getcountviewbyLineitems() {
		try {
			return itemsviewByLine;
		} catch (Exception e) {
			Assert.fail("Viewby lines items are not eligible");
		}
		return null;
	}

	public UsagePage clickOnItemByLineItems(String Catogiry, List<WebElement> ele, int index) {
		try {
			checkPageIsReady();
			boolean eleexist = false;
			int cnt = 0;
			while (!eleexist) {
				int elecount = ele.get(index)
						.findElements(By
								.xpath(".//ancestor::div[@class='col-12 ng-star-inserted']//div[@class='accordion-content content-open']//p[text()='"
										+ Catogiry + "']/../.."))
						.size();
				if (elecount == 0) {
					moveToElement(ele.get(index));
					cnt++;
				} else {
					eleexist = true;
				}
				if (cnt == 20)
					Assert.fail("scroll is not working");
			}
			waitFor(ExpectedConditions.visibilityOf(ele.get(index)
					.findElement(By
							.xpath(".//ancestor::div[@class='col-12 ng-star-inserted']//div[@class='accordion-content content-open']//p[text()='"
									+ Catogiry + "']/../.."))));
			Thread.sleep(1000);
			retryingFindClick(ele.get(index)
					.findElement(By
							.xpath(".//ancestor::div[@class='col-12 ng-star-inserted']//div[@class='accordion-content content-open']//p[text()='"
									+ Catogiry + "']/../..")));

		} catch (Exception e) {
			Assert.fail("Viewby lines" + Catogiry + " is not existing");
		}
		return this;
	}

	public UsagePage contrastlineitemarrows(List<WebElement> ele, int index) {
		try {

			if (ele.get(index).findElements(By.xpath(".//span[@class='pull-right cursor arrow-up arrowup_rotation']"))
					.size() == 1) {
				retryingFindClick(ele.get(index)
						.findElement(By.xpath(".//span[@class='pull-right cursor arrow-up arrowup_rotation']")));
				// clickElement(ele.get(index).findElement(By.xpath(".//span[@class='pull-right
				// cursor arrow-up arrowup_rotation']")));
			}
		} catch (Exception e) {
			Assert.fail("Viewby lines items uparrow is not existing");
		}

		return this;
	}

	/**
	 * Click View By Category Option
	 * 
	 * @param option
	 * @return
	 */
	public UsagePage verifyViewByCategoryOptionSuppressed(String option) {
		try {
			for (WebElement webElement : viewByCategoryOptions) {
				waitFor(ExpectedConditions.visibilityOf(webElement));
				if (webElement.getText().contains(option)) {
					Assert.fail("View by category options " + option + " are not supressed");
				}
			}
		} catch (Exception e) {

		}
		return this;
	}

	public UsagePage verifyViewByLineOptionSuppressed(String option) {
		try {
			waitFor(ExpectedConditions.visibilityOf(viewByLineAccounts));
			viewByLineAccounts.click();
			waitFor(ExpectedConditions.visibilityOfAllElements(viewByCategoryLines));
			for (WebElement webElement : viewByCategoryLines) {
				if (webElement.getText().contains(option)) {
					Assert.fail("View by line options " + option + " are not supressed");

					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return this;
	}

	/**
	 * click on view by Line first row
	 * 
	 * @return
	 */
	public UsagePage clickonFirstRow() {
		try {
			lineRow.isDisplayed();
			lineRow.click();
		} catch (Exception e) {
			Assert.fail("Failed to click on Line Row");
		}
		return this;
	}

	/**
	 * verify text displayed on Data blade
	 * 
	 * @param dataTextToVerify
	 */
	public void verifyTextOnDataBlade(String dataTextToVerify) {
		try {
			for (WebElement text : textOnDataBlade) {
				if (text.isDisplayed() && text.getText().equals(dataTextToVerify)) {
					Reporter.log("Verified that text displayed on data category is matching: " + dataTextToVerify);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify text on Line Row");
		}
	}

	/**
	 * click on Category: Data row
	 * 
	 * @return
	 */
	public UsagePage clickonDataRow() {
		try {
			categoryData.isDisplayed();
			categoryData.click();
			Reporter.log("Category: Data row is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Data category");
		}
		return this;
	}

	/**
	 * Verify the for the Msisdin for each line and entity
	 * @return
	 */
	public UsagePage verifySingleLineMsisdin(String msisdinNumber) {
		try {
			for (WebElement webElement : viewMsisdinByCategoryLines) {
				waitFor(ExpectedConditions.visibilityOf(webElement));
				if(webElement.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
						.equalsIgnoreCase(msisdinNumber)){
					Reporter.log("Verified that the displayed msisdin is matching w.r.to Signed in Msisdin : "+msisdinNumber);
				}
			}
		} catch (Exception e) {
			Verify.fail("Signed in Msisdin by Categorylines are not displayed");
		}
		return this;
	}
	
	/**
	 * click on Category: Data row
	 * 
	 * @return
	 */
	public UsagePage clickonHotspotRow() {
		try {
			hotSpotData.isDisplayed();
			hotSpotData.click();
			Reporter.log("Category: Hotspot row is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Hotspot category");
		}
		return this;
	}

	public UsagePage checklowersppedstext() {
		try {
			if (lowersppedstext.isDisplayed())
				Reporter.log("LOwer speeds text displayed");
			else
				Verify.fail("Lower sppeds text not displayed");

		} catch (Exception e) {
			Assert.fail("lowersppedstext element is not located ");
		}
		return this;
	}

	public UsagePage checkManageDatabuttonfunctionality() {
		try {
			buttonManageData.click();

		} catch (Exception e) {
			Assert.fail("lowersppedstext element is not located ");
		}
		return this;

}
}
