package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

/**
 * @author nsuvvada
 *
 */
public class Address {

	@Data(name = "Zipcode")
	private String zipcode;
	@Data(name = "AddressLine1")
	private String addressLine1;
	@Data(name = "AddressLine2")
	private String addressLine2;
	@Data(name = "CityState")
	private String cityState;
	@Data(name = "StateCode")
	private String stateCode;
	@Data(name = "State")
	private String state;
	@Data(name = "BillingAddress")
	private String billingAddress;
	
	@Data(name = "HomePhone")
	private String homePhone;

	@Data(name = "IDType")
	private String idType;
	@Data(name = "IDNumber")
	private String idNumber;

	@Data(name = "IdExpirationDate")
	private String idExpirationDate;

	/**
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @return the cityState
	 */
	public String getCityState() {
		return cityState;
	}

	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress() {
		return billingAddress;
	}

	public String getIdType() {
		return idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public String getIdExpirationDate() {
		return idExpirationDate;
	}

	/**
	 * @return the homePhone
	 */
	public String getHomePhone() {
		return homePhone;
	}

	@Override
	public String toString() {
		return "Address [" + (zipcode != null ? "zipcode=" + zipcode + ", " : "")
				+ (addressLine1 != null ? "addressLine1=" + addressLine1 + ", " : "")
				+ (addressLine2 != null ? "addressLine2=" + addressLine2 + ", " : "")
				+ (cityState != null ? "cityState=" + cityState + ", " : "")
				+ (stateCode != null ? "stateCode=" + stateCode + ", " : "")
				+ (state != null ? "state=" + state + ", " : "")
				+ (billingAddress != null ? "billingAddress=" + billingAddress + ", " : "")
				+ (homePhone != null ? "homePhone=" + homePhone + ", " : "")
				+ (idType != null ? "idType=" + idType + ", " : "")
				+ (idNumber != null ? "idNumber=" + idNumber + ", " : "")
				+ (idExpirationDate != null ? "idExpirationDate=" + idExpirationDate : "") + "]";
	}
	
	
}
