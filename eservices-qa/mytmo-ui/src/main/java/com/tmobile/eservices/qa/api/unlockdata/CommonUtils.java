package com.tmobile.eservices.qa.api.unlockdata;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class has all common methods which can be used across applications
 * 
 * @author blakshminarayana
 *
 */
public final class CommonUtils {
	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);
	private static final String FILE_DATA_FORMATE = "MM-dd-yyyy";

	private CommonUtils() {
	}

	/**
	 * This method is used to get the authorization file
	 * 
	 * @return authorization stream
	 */
	public static InputStream getAuthorizationFile() {
		logger.info("called getAuthorizationFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.AUTHORIZATION_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the Exchange Warranty And fulfillment file
	 * 
	 * @return authorization stream
	 */
	public static InputStream getExchangeWarrantyAndFullfillmentFile() {
		logger.info("called getExchangeWarrantyAndFullfillmentFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.WARRANTY_EXCHANGE_FULFILLMENT_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the Exchange Warranty And fulfillment file
	 * 
	 * @return authorization stream
	 */
	public static InputStream getExchangeWarrantyInitiationFile() {
		logger.info("called getExchangeWarrantyInitiationFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.WARRANTY_EXCHANGE_INITIATION_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the AgencyModelFile
	 * 
	 * @return stream
	 */
	public static InputStream getAgencyModelFile() {
		logger.info("called getAgencyModelFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.AGENCYMODEL_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the CoverageDashBoardFile
	 * 
	 * @return stream
	 */
	public static InputStream getCoverageDashBoardFile() {
		logger.info("called getCoverageDashBoardFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.COVERAGEDEVICEDASHBOARD_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the CoverageSelectionFile
	 * 
	 * @return srtream
	 */
	public static InputStream getCoverageSelectionFile() {
		logger.info("called getCoverageSelectionFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.COVERAGE_SELECTION_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the streaam by file name
	 * 
	 * @param pathname
	 * @return
	 */
	private static InputStream getStreamByFileName(StringBuilder pathname) {
		ClassLoader classLoader = CommonUtils.class.getClassLoader();
		return classLoader.getResourceAsStream(pathname.toString());
	}

	/**
	 * This method is used to get the InstalmentAccountFile
	 * 
	 * @return stream
	 */
	public static InputStream getInstalmentAccountFile() {
		logger.info("called getInstalmentAccountFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.INSTALLMENTACCOUNTPAYMENT_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the InstalmentHistoryFile
	 * 
	 * @return stream
	 */

	public static InputStream getInstalmentHistoryFile() {
		logger.info("called getInstalmentHistoryFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.INSTALLMENTACCOUNTPAYMENT_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the nonAgencyModelFile
	 * 
	 * @return stream
	 */
	public static InputStream getNonAgencyModelFile() {
		logger.info("called getNonAgencyModelFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.NONAGENCYMODEL_FILE);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is load the property file from resources
	 * 
	 * @return property file
	 */
	private static Properties loadPropertyFile() {
		logger.info("called loadPropertyFile method in CommonUtils");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = CommonUtils.class.getClassLoader();
			InputStream inputStream = classLoader.getResourceAsStream(Constants.APPLICATION_PROPERTY_FILE);
			properties.load(inputStream);
		} catch (IOException e) {
			logger.error("Fail to load property file{}", e);
			throw new ServiceException("Fail to load property file{}", e);
		}
		return properties;
	}

	/**
	 * This Method is used to get the token end point URL
	 * 
	 * @param environment
	 * @return token endpointURL
	 */
	public static String getTokenEndPointUrl(String environment) {
		Properties propertyFile = loadPropertyFile();
		StringBuilder envBuilder = new StringBuilder();
		envBuilder.append(environment).append(Constants.AUTHORIZATION_TOKEN_END_POINT_URL);
		return propertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This Method is used to get the InstallmentHistoryEndPointUrl
	 * 
	 * @param environment
	 * @return endpointURL
	 */
	public static String getInstallmentHistoryEndPointUrl(String environment) {
		Properties propertyFile = loadPropertyFile();
		StringBuilder envBuilder = new StringBuilder();
		envBuilder.append(environment).append(Constants.INSTALLMENT_HISTORY_END_POINT_URL);
		return propertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * this method is used to get the PaymentAccountEndPointUrl
	 * 
	 * @param environment
	 * @return endpointURL
	 */
	public static String getPaymentAccountEndPointUrl(String environment) {
		Properties propertyFile = loadPropertyFile();
		StringBuilder envBuilder = new StringBuilder();
		envBuilder.append(environment).append(Constants.INSTALLMENT_ACCOUNT_END_POINT_URL);
		return propertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method is used to get the jdbcurl from property file
	 * 
	 * @param environment
	 * @return jbdcurl
	 */

	public static String getJdbcURL(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties databaseProperties = loadPropertyFile();
		envBuilder.append(environment).append(Constants.JDBC_DRIVER_URL);
		return databaseProperties.getProperty(envBuilder.toString());
	}

	/**
	 * This method return jdbc user id form properties file
	 * 
	 * @param environment
	 * @return
	 */
	public static String getUserID(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties databaseProperties = loadPropertyFile();
		envBuilder.append(environment).append(Constants.JDBC_USER_ID);
		return databaseProperties.getProperty(envBuilder.toString());
	}

	/**
	 * This method returns jdbc password from properties file
	 * 
	 * @param environment
	 * @return
	 */
	public static String getPassword(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties databaseProperties = loadPropertyFile();
		envBuilder.append(environment).append(Constants.JDBC_USER_PAASWORD);
		return databaseProperties.getProperty(envBuilder.toString());
	}

	/**
	 * This method to get local pdf directory
	 * 
	 * @return path
	 */
	public static String getPdfLocalDirectory() {
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.PDFHANDLER_DIR);
		return pathname.toString();
	}

	/**
	 * This method is used to get the saucelab directory Path
	 * 
	 * @return path
	 */
	public static String getSauceLabPDFDirectory() {
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR);
		return pathname.toString();
	}

	/**
	 * This method return access tokenURL from properties files
	 * 
	 * @param environment
	 * @return accessTokenUrl
	 */
	public static String getAccessTokenEndPointUrl(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.GET_ACCESS_TOKEN);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method return getPinToMsisdnEndPointUrl from properties files
	 * 
	 * @param environment
	 * @return accessTokenUrl
	 */
	public static String getPinToMsisdnEndPointUrl(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.SENT_PIN_TO_MSISDN);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method return getEncryptEndPointUrl from properties files
	 * 
	 * @param environment
	 * @return accessTokenUrl
	 */
	public static String getEncryptEndPointUrl(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.ENCRYPTION);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method return getDecryptEndPointUrl from properties files
	 * 
	 * @param environment
	 * @return accessTokenUrl
	 */
	public static String getDecryptEndPointUrl(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.DECRYPTION);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method return authorization from properties files
	 * 
	 * @param environment
	 * @return accessTokenUrl
	 */
	public static String getAuthorization(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.AUTHORIZATION);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method return profile end point url from properties files
	 * 
	 * @param environment
	 * @return accessTokenUrl
	 */
	public static String getProfileEndPointURL(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.PROFILE_END_POINT);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	public static String getClearCounterEndPointURL(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.GET_CLEAR_COUNTERS);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	public static String getResetPasswordEndPointURL(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.RESET_PROFILE_PASSWORD);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	public static String getChangePasswordEndPointURL(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.CHANGE_PROFILE_PASSWORD);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method is able to return the ASOP APP processQuote EndPoint URL
	 * 
	 * @param environment
	 * @return
	 */
	public static String getProcessQuoteEndPointURL(String environment) {
		//logger.info("called getProcessQuotePointURL method in CommonUtils");
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.ASOP_PROCESSQUOTE_ENDPOINT_URL);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method is able to return the ASOP APP syncQuote EndPoint URL
	 * 
	 * @param environment
	 * @return
	 */
	public static String getSyncQuoteEndPointURL(String environment) {
		//logger.info("called getsyncQuotePointURL method in CommonUtils");
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.ASOP_SYNCQUOTE_ENDPOINT_URL);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method is used to get the ASOPJsonFile
	 * 
	 * @return json String
	 */
	public static File getProcessQuoteJsonFile() {
		//logger.info("called getProcessQuoteJsonFile method in CommonUtils");
		StringBuilder filePath = new StringBuilder();
		filePath.append(Constants.ASOP_JSON_FILE_DIR).append(Constants.ASOP_STANDARD_ORDER_JSON_FILE);
		ClassLoader classLoader = CommonUtils.class.getClassLoader();
		return new File(classLoader.getResource(filePath.toString()).getFile());

	}

	/**
	 * This method to convert the date to date string
	 * 
	 * @return
	 */
	public static String convertDateToString() {
		return getDateFormate(new Date());
	}

	/**
	 * This method to return the previous date from calendar
	 * 
	 * @return
	 */
	public static String getPrevousDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		Date date = calendar.getTime();
		return getDateFormate(date);
	}

	private static String getDateFormate(Date date) {
		return new SimpleDateFormat(FILE_DATA_FORMATE, Locale.getDefault()).format(date);

	}

	/**
	 * get directory from resource
	 * 
	 * @return
	 * @throws URISyntaxException
	 */
	public static URI getURI() throws URISyntaxException {
		ClassLoader classLoader = CommonUtils.class.getClassLoader();
		URL resource = classLoader.getResource(Constants.ASOP_JSON_FILE_DIR);
		return resource.toURI();

	}

	/**
	 * This method to return the jump rest end point url
	 * 
	 * @param environment
	 * @return
	 */
	public static String getJumpSeviceEndPointURL(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.JUMP_SERVICE_SERVICE);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	/**
	 * This method is used to get the getProfileBetaFile
	 * 
	 * @return stream
	 */
	public static InputStream getProfileBetaFile() {
		//logger.info("called getProfileBetaFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.MANAGE_PROFILE_BETA);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the getProfileBetaFile
	 * 
	 * @return stream
	 */
	public static InputStream getProfilePartialFile() {
		//logger.info("called getProfilePartialFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.MANAGE_PROFILE_PARTIAL);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is used to get the getProfileBetaFile
	 * 
	 * @return stream
	 */
	public static InputStream getProfileFullFile() {
		//logger.info("called getProfileFullFile method in CommonUtils");
		StringBuilder pathname = new StringBuilder();
		pathname.append(Constants.SOAPHANDLER_SOAPDIR).append(Constants.MANAGE_PROFILE_FULL);
		return getStreamByFileName(pathname);

	}

	/**
	 * This method is able to return the getSubscriberProfileEndPointURL
	 * 
	 * @param environment
	 * @return
	 */
	public static String getSubscriberProfileEndPointURL(String environment) {
		//logger.info("called getSubscriberProfileEndPointURL method in CommonUtils");
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.MANAGE_PROFILE_END_POINT_URL);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	// EIP Database

	/**
	 * This method is used to get the EIP URL from property file
	 * 
	 * @param environment
	 * @return eip url
	 */

	public static String getEIPURL(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties databaseProperties = loadPropertyFile();
		envBuilder.append(environment).append(Constants.EIP_URL);
		return databaseProperties.getProperty(envBuilder.toString());
	}

	/**
	 * This method return EIP user id form properties file
	 * 
	 * @param environment
	 * @return
	 */
	public static String getEIPUserID(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties databaseProperties = loadPropertyFile();
		envBuilder.append(environment).append(Constants.EIP_USER_ID);
		return databaseProperties.getProperty(envBuilder.toString());
	}

	/**
	 * This method returns EIP password from properties file
	 * 
	 * @param environment
	 * @return
	 */
	public static String getEIPPassword(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties databaseProperties = loadPropertyFile();
		envBuilder.append(environment).append(Constants.EIP_USER_PAASWORD);
		return databaseProperties.getProperty(envBuilder.toString());
	}

	public static String getNewProfilePassword(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.PROFILE_NEW_PASSWORD);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

	public static String getTempPinEndPointUrl(String environment) {
		StringBuilder envBuilder = new StringBuilder();
		Properties loadPropertyFile = loadPropertyFile();
		envBuilder.append(environment).append(Constants.SECOND_FACTOR_AUTH);
		return loadPropertyFile.getProperty(envBuilder.toString());
	}

}
