package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class ActivitiesApiV1 extends ApiCommonLib {
	

	
	 private Map<String, String> buildActivitiesAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/json");
			headers.put("userRole", "PAH");
			headers.put("ban", apiTestData.getBan());
			headers.put("Authorization", "Bearer "+checkAndGetAccessToken());
			headers.put("X-B3-TraceId", "PaymentsAPITest123");
			headers.put("X-B3-SpanId", "PaymentsAPITest456");
			headers.put("transactionid","PaymentsAPITest");
			return headers;
		}
	 

	public Response getAccountSummary(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildActivitiesAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		reqSpec.addQueryParam("category", "all");
		reqSpec.addQueryParam("fromDate", "2016-03-10");
		reqSpec.addQueryParam("toDate", "2018-03-10");

		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/accountactivity/all", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	    

	   
	public Response getPdfDocument(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildActivitiesAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		reqSpec.addQueryParam("documentId", "7312311459449028");
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v1/accountactivity/document", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}
	    
	public Response getEipHistory(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildActivitiesAPIHeader(apiTestData,tokenMap);
    	RestService service = new RestService("", eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			reqSpec.addQueryParam("paymentId", "1767597938");		
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v1/accountactivity/eip",RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
}
