package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class AccessoriesApi extends ApiCommonLib{
	
	/***
	 * Gets all the accessories
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
    public Response getAccessoryList(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildGetAccessoryHeader1(apiTestData,tokenMap);
    	String resourceURL = "v3/accessory/list";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         System.out.println(response.asString());
         return response;
    	/*Map<String, String> headers = buildGetAccessoryHeader1(apiTestData,tokenMap);
        RestService service = new RestService(requestBody, eos_base_url);
        RequestSpecBuilder reqSpec = service.getRequestbuilder();
		 reqSpec.addHeaders(headers);
		 service.setRequestSpecBuilder(reqSpec);
        Response response = service.callService("v3/accessory/list", RestCallType.POST);
        System.out.println(response.asString());
        return response;*/
    }
    public Response getAccessoryList1(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	
    	Map<String, String> headers = buildGetAccessoryHeader1(apiTestData,tokenMap);
        RestService service = new RestService(requestBody, eos_base_url);
        RequestSpecBuilder reqSpec = service.getRequestbuilder();
		 reqSpec.addHeaders(headers);
		 service.setRequestSpecBuilder(reqSpec);
        Response response = service.callService("v3/accessory/list", RestCallType.POST);
        System.out.println(response.asString());
        return response;
    }
    
    
    /***
     * Gets all the accessories description
     * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
     * @return
     * @throws Exception 
     */
    public Response getAccessoryDetails(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception {
    	Map<String, String> headers = buildGetAccessoryHeader(apiTestData, tokenMap);
    	String resourceURL = "v3/accessory/details";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
        System.out.println("getAccessoryDetails endpoint = "+eos_base_url+"/v3/accessory/details");
        return response;
    }
    
    /***
     * summary: Gets the list for dropdowns
     * description: Gets the list of categories, manufacturers and device names
     * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
     * @return
     * @throws Exception 
     */
    public Response getAccessoryFilter(ApiTestData apiTestData,String requestBody,String filter, Map<String, String> tokenMap) throws Exception {
    	Map<String, String> headers = buildGetAccessoryHeader(apiTestData, tokenMap);
    	String resourceURL = "v3/accessory/filter?type="+filter;
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
	    System.out.println("getAccessoryFilter endpoint = "+eos_base_url+"/v3/accessory/filter?type="+filter);
        return response;
    }
    
    private Map<String, String> buildGetAccessoryHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", checkAndGetJWTToken(apiTestData, tokenMap).get("jwtToken"));
		headers.put("transactionId", "9b9fca35-b898-409d-9ede-16c7400993f8");
		headers.put("correlationId", "49978fd6-e17c-4068-aacb-dba657dbcfa3");
		headers.put("applicationId", "MYTMO");
		headers.put("channelId", "MYTMO");
		headers.put("clientId", "e-servicesUI");
		headers.put("transactionBusinessKey", apiTestData.getBan());
		headers.put("transactionBusinessKeyType", "missdn");
		headers.put("transactionType", "ACCESSORIES");
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}
    
    private Map<String, String> buildGetAccessoryHeader1(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		//headers.put("Authorization", checkAndGetJWTToken(apiTestData, tokenMap).get("jwtToken"));
		headers.put("Authorization",  getJWTDecodeToken(apiTestData,tokenMap));
		headers.put("transactionId", "06eb25a3-5cbd-41f4-a0de-3a298a94d6cd");
		headers.put("correlationId", "accessoryList990");
		headers.put("applicationId", "MYTMO");
		headers.put("channelId", "TMO");
		headers.put("clientId", "e-servicesUI");
		headers.put("transactionBusinessKey", "BAN");
		headers.put("transactionBusinessKeyType", apiTestData.getBan());
		headers.put("transactionType", "ACCESSORIES");
		//headers.put("X-B3-TraceId", "ShopAPITest123");
		//headers.put("X-B3-SpanId", "ShopAPITest456");
		//headers.put("transactionid","ShopAPITest");
		headers.put("cache-control","no-cache");
		headers.put("content-type","application/json");
		headers.put("interactionid", "IID");
		headers.put("usn", "testUSN");
		return headers;
	}

}
