
package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class SyncupPage extends TmngCommonPage {

	private final String pageUrl = "/offers/syncup";

	@FindBy(xpath = "//select[@id='compatibility-lead-gen-vehicle-year']")
	private WebElement vehicleYear;

	@FindBy(xpath = "//select[@id='compatibility-lead-gen-vehicle-make']")
	private WebElement vehicleMake;

	@FindBy(xpath = "//select[@id='compatibility-lead-gen-vehicle-model']")
	private WebElement vehicleModel;

	@FindBy(xpath = "//select[@id='compatibility-lead-gen-vehicle-engine']")
	private WebElement vehicleEngine;

	@FindBy(xpath = "//div[@class='sync-up-success-header']//h2[@class='ng-binding']")
	private WebElement compatibleMessage;

	public SyncupPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the SyncupPage class instance.
	 */
	public SyncupPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Syncup page loaded");

		} catch (Exception ex) {
			Assert.fail("Syncup Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the SyncupPage class instance.
	 */
	public SyncupPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl), 60);
		return this;
	}

	/**
	 * Test 1 - Select Vehicle
	 */
	public SyncupPage selectVehicle1() {
		try {
			if (vehicleYear.isEnabled()) {
				selectElementFromDropDown(vehicleYear, "Text", "2019");
				selectElementFromDropDown(vehicleMake, "Text", "Audi");
				selectElementFromDropDown(vehicleModel, "Text", "A3");
				selectElementFromDropDown(vehicleEngine, "Text", "2.0L 4-cyl. Turbo 7-speed Automated Manual");
				Reporter.log("vehicle 2019, Audi Options are selected.");
			} else {
				Assert.fail("vehicleYear 2019 Options are not selected.");
			}
		} catch (Exception e) {
			Assert.fail("vehicleYear 2019 Options are not selected." + e.getMessage());
		}
		return this;
	}

	/**
	 * Test 1 - Verify Message
	 */
	public SyncupPage verifyMessage1() {
		try {
			waitFor(ExpectedConditions.visibilityOf(compatibleMessage), 3);
			Assert.assertTrue(compatibleMessage.isDisplayed(),
					"Congratulations! SyncUP DRIVE is compatible with your vehicle. Please enjoy your connected car experience.");
			Reporter.log(
					"Congratulations! SyncUP DRIVE is compatible with your vehicle. Please enjoy your connected car experience. displayed");
		} catch (Exception e) {
			Assert.fail("Message not displayed");
		}
		return this;
	}

	/**
	 * Test 2 - Select Vehicle
	 */
	public SyncupPage selectVehicle2() {
		try {
			if (vehicleYear.isEnabled()) {
				selectElementFromDropDown(vehicleYear, "Text", "2004");
				selectElementFromDropDown(vehicleMake, "Text", "Chevrolet");
				selectElementFromDropDown(vehicleModel, "Text", "Express Cargo");
				selectElementFromDropDown(vehicleEngine, "Text", "6.0L V8 4-speed Automatic");
				Reporter.log("vehicle 2004, Chevrolet Options are selected.");
			} else {
				Assert.fail("vehicle Options are not selected.");
			}
		} catch (Exception e) {
			Assert.fail("vehicle Options is not selected." + e.getMessage());
		}
		return this;
	}

	/**
	 * Test 2 - Verify Message
	 */
	public SyncupPage verifyMessage2() {
		try {
			waitFor(ExpectedConditions.visibilityOf(compatibleMessage), 3);
			Assert.assertTrue(compatibleMessage.isDisplayed(),
					"Good news! SyncUP DRIVE is compatible with your vehicle. Some fuel readings may not be available for this particular vehicle.");
			Reporter.log(
					"Good news! SyncUP DRIVE is compatible with your vehicle. Some fuel readings may not be available for this particular vehicle displayed");
		} catch (Exception e) {
			Assert.fail("Good news! Message not displayed");
		}
		return this;
	}

	/**
	 * Test 3 - Select Vehicle
	 */
	public SyncupPage selectVehicle3() {
		try {
			if (vehicleYear.isEnabled()) {
				selectElementFromDropDown(vehicleYear, "Text", "2001");
				selectElementFromDropDown(vehicleMake, "Text", "Nissan");
				selectElementFromDropDown(vehicleModel, "Text", "Altima");
				selectElementFromDropDown(vehicleEngine, "Text", "2.4L 4-cyl. 5-speed Manual");
				Reporter.log("vehicle 2001, Nissan Options are selected.");
			} else {
				Assert.fail("vehicle Options are not selected.");
			}
		} catch (Exception e) {
			Assert.fail("vehicle Options is not selected." + e.getMessage());
		}
		return this;
	}

	/**
	 * Test 3 - Verify Message
	 */
	public SyncupPage verifyMessage3() {
		try {
			waitFor(ExpectedConditions.visibilityOf(compatibleMessage), 3);
			Assert.assertTrue(compatibleMessage.isDisplayed(),
					"Stop! SyncUP DRIVE is not compatible with your vehicle. Please remove SyncUP DRIVE from this vehicle.");
			Reporter.log(
					"Stop! SyncUP DRIVE is not compatible with your vehicle. Please remove SyncUP DRIVE from this vehicle. displayed");
		} catch (Exception e) {
			Assert.fail("Stop! Message not displayed");
		}
		return this;
	}

	/**
	 * Test 4 - Select Vehicle
	 */
	public SyncupPage selectVehicle4() {
		try {
			if (vehicleYear.isEnabled()) {
				selectElementFromDropDown(vehicleYear, "Text", "2009");
				selectElementFromDropDown(vehicleMake, "Text", "Ferrari");
				selectElementFromDropDown(vehicleModel, "Text", "California");
				selectElementFromDropDown(vehicleEngine, "Text", "4.3L V8 7-speed Automated Manual");
				Reporter.log("vehicle 2009, Ferrari Options are selected.");
			} else {
				Assert.fail("vehicle Options are not selected.");
			}
		} catch (Exception e) {
			Assert.fail("vehicle Options is not selected." + e.getMessage());
		}
		return this;
	}

	/**
	 * Test 4 - Verify Message
	 */
	public SyncupPage verifyMessage4() {
		try {
			waitFor(ExpectedConditions.visibilityOf(compatibleMessage), 3);
			Assert.assertTrue(compatibleMessage.isDisplayed(),
					"Welcome! SyncUP DRIVE has not been tested with your vehicle. It’s possible some data features may not be available for this particular vehicle.");
			Reporter.log(
					"Welcome! SyncUP DRIVE has not been tested with your vehicle. It’s possible some data features may not be available for this particular vehicle displayed");
		} catch (Exception e) {
			Assert.fail("Welcome! Message not displayed");
		}
		return this;
	}

	/**
	 * Test 5 - Select Vehicle
	 */
	public SyncupPage selectVehicle5() {
		try {
			if (vehicleYear.isEnabled()) {
				selectElementFromDropDown(vehicleYear, "Text", "2012");
				selectElementFromDropDown(vehicleMake, "Text", "Dodge");
				selectElementFromDropDown(vehicleModel, "Text", "Charger");
				selectElementFromDropDown(vehicleEngine, "Text", "6.4L V8 5-speed Automatic");
				Reporter.log("vehicle 2012 Options are selected.");
			} else {
				Assert.fail("vehicle Options are not selected.");
			}
		} catch (Exception e) {
			Assert.fail("vehicle Options is not selected." + e.getMessage());
		}
		return this;
	}

	/**
	 * Test 5 - Verify Message
	 */
	public SyncupPage verifyMessage5() {
		try {
			waitFor(ExpectedConditions.visibilityOf(compatibleMessage), 3);
			Assert.assertTrue(compatibleMessage.isDisplayed(),
					"Be advised! SyncUP DRIVE is not compatible with your vehicle.");
			Reporter.log("Be advised! SyncUP DRIVE is not compatible with your vehicle displayed");
		} catch (Exception e) {
			Assert.fail("Be advised! Message not displayed");
		}
		return this;
	}
}
