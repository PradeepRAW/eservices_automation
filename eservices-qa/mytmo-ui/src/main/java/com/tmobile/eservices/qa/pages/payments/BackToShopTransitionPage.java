package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author speddis
 *
 */
public class BackToShopTransitionPage extends CommonPage {
	
	@FindBy(css = "div.main-content p.Display2")
	private WebElement pageHeader;

	private final String pageUrl = "/backtoshop";

	@FindBy(linkText = "Continue")//If linktext doesn't work, use this CSS "div a.glueButton-JUMP"
	private WebElement continueButton;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public BackToShopTransitionPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
     * Verify that current page URL matches the expected URL.
     */
    public BackToShopTransitionPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public BackToShopTransitionPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
	    	waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
    	}catch(Exception e){
    		Assert.fail("'backToShop' Transition page not found.");
    	}
        return this;
    }

    /**
     * Click 'Continue' button 
     */
	public void clickContinueButton() {
		try {
			continueButton.click();
			Reporter.log("Continue Button is clicked");
		} catch (Exception e) {
			Assert.fail("Continue Button is not clicked");
		}
	}
	
	
	 /**
     * Verify 'Continue' button 
     */
	public void VerifyContinueButton() {
		try {
			continueButton.isDisplayed();
			Reporter.log("Continue Button is displayed");
		} catch (Exception e) {
			Assert.fail("Continue Button is not displayed");
		}
	}

}
