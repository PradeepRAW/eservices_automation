/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author csudheer
 *
 */

public class AddonsLineSelectorPage extends CommonPage {

	@FindBy(xpath = "")
	private WebElement selectDifferentLineLink;

	@FindBy(xpath = "//*[@id=\"lineSelector-title\"]/p/span")
	private WebElement headerOfLineSelectorPage;

	@FindBy(xpath = "")
	private WebElement backNavArrowOfLineSelectorPage;

	@FindBy(css = "div[class*='col-12 col-md-6 col-lg-6 no-padding-xs full-bleed-divider-top-xs p-t-24-lg p-l-0-lg p-l-0-md']")
	private WebElement selectLine;

	@FindBy(xpath = "(//*[@class='padding-bottom-xsmall Display6']/span)")
	private List<WebElement> listOfMSISDNs;

	public AddonsLineSelectorPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Line details page.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public AddonsLineSelectorPage verifyLineSelectorPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyElementText(headerOfLineSelectorPage, "Select a line to Manage"),
					"Line Selector page is loaded");
			Reporter.log("Line Selector page is  displayed.");
		} catch (Exception e) {
			Assert.fail("Line Selector page not displayed");
		}
		return this;
	}

	// Click on Back Nav Arrow
	public AddonsLineSelectorPage clickOnBackNavArrowOnLineSelectorPage() {

		try {
			backNavArrowOfLineSelectorPage.isDisplayed();
			backNavArrowOfLineSelectorPage.click();
			Reporter.log("Back Nav arrow on line selector page is clicked");
		} catch (Exception e) {
			Reporter.log("Back Nav arrow on line selector page is not displayed");
		}
		return this;
	}

	// Click on Back Nav Arrow
	public AddonsLineSelectorPage selectTheLine() {

		try {
			selectLine.isDisplayed();
			selectLine.click();
			Reporter.log("Selected the line");
		} catch (Exception e) {
			Reporter.log("Not able to select a line");
		}
		return this;
	}

	// Click on Back Nav Arrow
	public AddonsLineSelectorPage verifyHeaderOnLineSelectorPage() {

		try {
			headerOfLineSelectorPage.isDisplayed();
			Assert.assertEquals(headerOfLineSelectorPage.getText().trim(), "Select a line to manage",
					"Header on Line Selector page is mismatched");
			Reporter.log("Header on Line Selector page is matched");
		} catch (Exception e) {
			Reporter.log("Header on Line selector page is not displayed.");
		}
		return this;
	}

	// verify lines on Lines selector page
	public AddonsLineSelectorPage verifyLinesOnLineSelectorPage() {
		String expectedMSISDN[] = { "4049602370", "4049602371", "4049969302", "8028299224", "4049969551" };
		String actualMSISDN[] = null;
		// String futureDatedService = null;
		try {
			for (int i = 0; i <= listOfMSISDNs.size(); i++) {
				actualMSISDN[i] = listOfMSISDNs.get(i).getText();
				;
				System.out.println("MSISDNs displayed on Line selector page is " + actualMSISDN);
			}
		} catch (Exception e) {
			Assert.fail("MSISDNs are not displayed on Line Selector page");
		}

		for (int i = 0; i <= actualMSISDN.length; i++) {
			if (!(actualMSISDN[i].equals(expectedMSISDN[i])))
				Assert.assertEquals(actualMSISDN[i], expectedMSISDN[i], "MSISDN mismatch on Line selector page");
			else
				Reporter.log("MSISDN matched on Line selector page");
		}
		return this;
	}

}
