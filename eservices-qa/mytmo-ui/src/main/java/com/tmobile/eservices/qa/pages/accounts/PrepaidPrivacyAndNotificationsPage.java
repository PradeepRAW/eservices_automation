package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 * 
 */
public class PrepaidPrivacyAndNotificationsPage extends CommonPage {

	private static final String privacyAndNotificationsPage = "/account/profile/prepaid/privacy_notifications";

	public PrepaidPrivacyAndNotificationsPage(WebDriver webDriver) {
		super(webDriver);
	}

	// Verify Billing & Payments page
	public PrepaidPrivacyAndNotificationsPage verifyPrivacyAndNotificationsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(privacyAndNotificationsPage);
			Reporter.log("Privacy and Notifications page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Privacy and Notifications page not displayed");
		}
		return this;
	}

}
