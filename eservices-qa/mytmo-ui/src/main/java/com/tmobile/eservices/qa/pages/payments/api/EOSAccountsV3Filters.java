package com.tmobile.eservices.qa.pages.payments.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSAccountsV3Filters extends EOSCommonLib {

	/*********************************************************************************************
	 * EoS-Publickey v3
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 */

	public Response getResponsePublickKey(String alist[]) throws Exception {
		Response response = null;

		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("channel_id", "TSL");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("ban", alist[3]);
			restService.addHeader("application_id", "MYTMO");
			response = restService.callService("accounts/v3/publickey", RestCallType.GET);

		}

		return response;
	}

	public String getpublicKey(Response response) {
		String publickey = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("publicKey") != null) {
				String jsonpublickey = jsonPathEvaluator.get("publicKey").toString();
				JsonPath jsonPathEvaluator1 = JsonPath.from(jsonpublickey);
				if (jsonPathEvaluator1.get("n") != null)
					return jsonPathEvaluator1.get("n").toString();

			}
		}

		return publickey;
	}

	/*********************************************************************************************
	 * EoS-Search paymentmethods v3
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 **********************************************************************************************/

	public Response getResponseSearchpayments(String alist[]) throws Exception {
		Response response = null;

		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("channel_id", "TSL");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("interactionId", "123456787");
			restService.addHeader("ban", alist[3]);
			restService.addHeader("eosWalletCapacity", "3");
			restService.addHeader("isLimitedWalletCapacity", "false");
			response = restService.callService("accounts/v3/" + alist[3], RestCallType.GET);

		}

		return response;
	}

	public List<String> getstoredbankalias(Response response) {
		List<String> bank = new ArrayList<String>();
		;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("paymentInstrumentList.bankAccount.accountNumber") != null) {
				return jsonPathEvaluator.get("paymentInstrumentList.bankAccount.accountNumber");
			} else {
				return bank;
			}

		}
		return null;
	}

	public List<String> getstoredcardalias(Response response) {
		List<String> card = new ArrayList<String>();

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("paymentInstrumentList.creditCard.cardNumber") != null) {
				return jsonPathEvaluator.get("paymentInstrumentList.creditCard.cardNumber");
			} else {
				return card;
			}
		}
		return null;
	}

	public List<String> getstoreddebitcardalias(Response response) {
		List<String> card = new ArrayList<String>();

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("paymentInstrumentList.debitCard.cardNumber") != null) {
				return jsonPathEvaluator.get("paymentInstrumentList.debitCard.cardNumber");
			} else {
				return card;
			}
		}
		return null;
	}

	public String geteligibilityforbankpayment(Response response) {
		String bankpayment = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("eligibleForBankPayment") != null) {
				return jsonPathEvaluator.get("eligibleForBankPayment").toString();
			}
		}

		return bankpayment;
	}

	/*********************************************************************************************
	 * EoS-Negativefileban
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 **********************************************************************************************/

	public Response getResponseNegativefileban(String alist[]) throws Exception {
		Response response = null;

		if (alist != null) {

			RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");

			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("interactionId", "123456787");
			restService.addHeader("ban", alist[3]);

			response = restService.callService("accounts/v3/negativeFileCheck/" + alist[3], RestCallType.GET);

		}

		return response;
	}

	/*********************************************************************************************
	 * EoS-ValidateBank
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 **********************************************************************************************/
	public Response getResponsevalidatebank(String alist[], Map<Object, Object> optpara) throws Exception {
		Response response = null;

		if (alist != null) {
			JSONObject rootval = new JSONObject();
			JSONArray paymentInstrumentList = new JSONArray();
			JSONObject requestparam = new JSONObject();
			JSONObject bankAccount = new JSONObject();
			JSONObject billingAddress = new JSONObject();

			JSONObject bussinessDetails = new JSONObject();

			rootval.put("code", "CHECK");
			rootval.put("eligibleForCardPayment", true);
			rootval.put("eligibleForBankPayment", true);
			rootval.put("eligibleForStoredCardPayment", true);
			rootval.put("eligibleForStoredBankPayment", true);
			rootval.put("paymentInstrumentList", paymentInstrumentList);
			paymentInstrumentList.put(requestparam);
			requestparam.put("bankAccount", bankAccount);
			bankAccount.put("status", "VALID");
			bankAccount.put("bankAccountAlias", "");
			bankAccount.put("accountNumber", optpara.get("accountNumber"));
			bankAccount.put("routingNumber", optpara.get("routingNumber"));
			bankAccount.put("cardHolderName", "string");
			bankAccount.put("accountHolderFirstName", "test");
			bankAccount.put("accountHolderLastName", "test");
			bankAccount.put("expirationMonthYear", "Enter expirationyear");
			bankAccount.put("billingAddress", billingAddress);
			billingAddress.put("line1", "string");
			billingAddress.put("line2", "string");
			billingAddress.put("city", "string");
			billingAddress.put("state", "string");
			billingAddress.put("zip", "string");
			billingAddress.put("countryCode", "string");
			billingAddress.put("stateCode", "string");
			bankAccount.put("accountType", "Checking");
			bankAccount.put("nickName", "nickName");
			bankAccount.put("expirationStatus", "string");
			bankAccount.put("storePaymentMethodIndicator", false);
			bankAccount.put("defaultPaymentMethodIndicator", false);
			bankAccount.put("paymentMethodCode", "CHECK");
			rootval.put("sourceId", "string");
			rootval.put("businessTransactionType", "string");
			rootval.put("termsAgreementIndicator", true);
			rootval.put("bussinessDetails", bussinessDetails);
			bussinessDetails.put("billerCode", "string");
			bussinessDetails.put("businessUnit", "string");
			bussinessDetails.put("programCode", "string");
			bussinessDetails.put("businessSegment", "string");
			bussinessDetails.put("productGroup", "HardGoods");
			bussinessDetails.put("orderTypes", "MANAGERATEPLAN");
			bussinessDetails.put("preferedPaymentdevice", "string");
			bussinessDetails.put("clientId", "string");
			bussinessDetails.put("applicationId", "string");

			RestService restService = new RestService(rootval.toString(), eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("ban", alist[3]);
			restService.addHeader("msisdn", alist[0]);
			restService.addHeader("channel_id", "TSL");
			restService.addHeader("usn", "1000021111");
			restService.addHeader("interactionId", "123123123");

			response = restService.callService("accounts/v3/validation/" + alist[3], RestCallType.POST);

		}

		return response;
	}

	public String getbankalias(Response response) {
		String bankalias = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("bankAccountAlias") != null) {
				return jsonPathEvaluator.get("bankAccountAlias").toString();
			}

		}
		return bankalias;
	}

	/*********************************************************************************************
	 * EoS-Addbank
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 **********************************************************************************************/
	public Response getResponseaddbank(String alist[], Map<Object, Object> optpara) throws Exception {
		Response response = null;

		if (alist != null) {
			JSONObject rootval = new JSONObject();
			JSONArray paymentInstrumentList = new JSONArray();
			JSONObject requestparam = new JSONObject();
			JSONObject bankAccount = new JSONObject();
			JSONObject billingAddress = new JSONObject();

			JSONObject bussinessDetails = new JSONObject();

			rootval.put("code", "CHECK");
			rootval.put("eligibleForCardPayment", true);
			rootval.put("eligibleForBankPayment", true);
			rootval.put("eligibleForStoredCardPayment", true);
			rootval.put("eligibleForStoredBankPayment", true);
			rootval.put("paymentInstrumentList", paymentInstrumentList);
			paymentInstrumentList.put(requestparam);
			requestparam.put("bankAccount", bankAccount);
			bankAccount.put("status", "VALID");
			bankAccount.put("bankAccountAlias", optpara.get("bankalias"));
			bankAccount.put("accountNumber", optpara.get("bankalias"));
			bankAccount.put("routingNumber", optpara.get("routingNumber"));
			bankAccount.put("cardHolderName", "string");
			bankAccount.put("accountHolderFirstName", "test");
			bankAccount.put("accountHolderLastName", "test");
			bankAccount.put("expirationMonthYear", "Enter expirationyear");
			bankAccount.put("billingAddress", billingAddress);
			billingAddress.put("line1", "string");
			billingAddress.put("line2", "string");
			billingAddress.put("city", "string");
			billingAddress.put("state", "string");
			billingAddress.put("zip", "string");
			billingAddress.put("countryCode", "string");
			billingAddress.put("stateCode", "string");
			bankAccount.put("accountType", "Check");
			bankAccount.put("nickName", "nickName");
			bankAccount.put("expirationStatus", "string");
			bankAccount.put("storePaymentMethodIndicator", true);
			bankAccount.put("defaultPaymentMethodIndicator", true);
			bankAccount.put("paymentMethodCode", "CHECK");
			rootval.put("sourceId", "string");
			rootval.put("businessTransactionType", "string");
			rootval.put("termsAgreementIndicator", true);
			rootval.put("bussinessDetails", bussinessDetails);
			bussinessDetails.put("billerCode", "string");
			bussinessDetails.put("businessUnit", "string");
			bussinessDetails.put("programCode", "string");
			bussinessDetails.put("businessSegment", "string");
			bussinessDetails.put("productGroup", "HardGoods");
			bussinessDetails.put("orderTypes", "MANAGERATEPLAN");
			bussinessDetails.put("preferedPaymentdevice", "string");
			bussinessDetails.put("clientId", "string");
			bussinessDetails.put("applicationId", "string");

			RestService restService = new RestService(rootval.toString(), eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("ban", alist[3]);
			restService.addHeader("msisdn", alist[0]);
			restService.addHeader("channel_id", "TSL");
			restService.addHeader("usn", "1000021111");
			restService.addHeader("interactionId", "123123123");

			response = restService.callService("accounts/v3/" + alist[3], RestCallType.POST);

		}

		return response;
	}

	public String addbankstatus(Response response) {
		String addbank = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("status") != null) {
				return jsonPathEvaluator.get("status").toString();
			}

		}
		return addbank;
	}

	/*********************************************************************************************
	 * EoS-Deletebank
	 * 
	 * @param alist
	 * @return
	 * @throws Exception
	 **********************************************************************************************/
	public Response getResponsedeletebank(String alist[], Map<Object, Object> optpara) throws Exception {
		Response response = null;

		if (alist != null) {
			JSONObject rootval = new JSONObject();
			JSONArray paymentInstrumentList = new JSONArray();
			JSONObject requestparam = new JSONObject();
			JSONObject bankAccount = new JSONObject();

			JSONObject bussinessDetails = new JSONObject();

			rootval.put("code", "CHECK");

			rootval.put("paymentInstrumentList", paymentInstrumentList);
			paymentInstrumentList.put(requestparam);
			requestparam.put("bankAccount", bankAccount);
			bankAccount.put("status", "VALID");
			bankAccount.put("bankAccountAlias", optpara.get("bankalias"));
			bankAccount.put("accountNumber", optpara.get("bankalias"));
			bankAccount.put("routingNumber", optpara.get("routingNumber"));
			bankAccount.put("cardHolderName", "string");
			bankAccount.put("accountHolderFirstName", "test");
			bankAccount.put("accountHolderLastName", "test");
			bankAccount.put("expirationMonthYear", "Enter expirationyear");

			rootval.put("termsAgreementIndicator", true);
			rootval.put("bussinessDetails", bussinessDetails);
			bussinessDetails.put("billerCode", "SAMSON");
			bussinessDetails.put("businessUnit", "TMOBILE");
			bussinessDetails.put("programCode", "U1POSTLEGACY");
			bussinessDetails.put("businessSegment", "POSTPAID");
			bussinessDetails.put("productGroup", "SOFTGOODS");
			bussinessDetails.put("orderTypes", "MANAGERATEPLAN");
			bussinessDetails.put("operationType", "SALES");

			RestService restService = new RestService(rootval.toString(), eep.get(environment));
			restService.setContentType("application/json");
			restService.addHeader("Authorization", alist[2]);
			restService.addHeader("ban", alist[3]);
			restService.addHeader("msisdn", alist[0]);
			restService.addHeader("channel_id", "TSL");
			restService.addHeader("usn", "1000021111");
			restService.addHeader("interactionId", "123123123");

			response = restService.callService("accounts/v3/delete/" + alist[3], RestCallType.POST);

		}

		return response;
	}

	public String deletebankstatus(Response response) {
		String deletebank = null;

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get("status") != null) {
				return jsonPathEvaluator.get("status").toString();
			}

		}
		return deletebank;
	}

}
