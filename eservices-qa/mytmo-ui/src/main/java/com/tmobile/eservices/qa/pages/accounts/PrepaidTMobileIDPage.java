package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author agarpawr
 * 
 */
public class PrepaidTMobileIDPage extends CommonPage {

	private static final String tmobileIDPage = "prepaid/tmobile_id";

	@FindBy(xpath = "//*[contains(text(),'Billing & Payments')]")
	private WebElement BillingandPayments;

	@FindBy(css = "p.Display3.text-center")
	private WebElement billingandPaymentsPageHeader;

	@FindBy(css = "p.body.text-center")
	private WebElement billingAndPaymentsPageSubHeader;

	@FindBy(xpath = "//p[contains(text(),'Billing Address')]")
	private WebElement billingAddressTab;

	@FindBy(xpath = "(//p[contains(text(),'Billing Address')]//../p)[2]")
	private WebElement addressOnBillingAddressTab;

	@FindBy(xpath = "(//p[contains(text(),'Billing Address')]//../p)[3]")
	private WebElement cityAndZipCodeOnBillingAddressTab;

	@FindBy(xpath = "//p[contains(text(),'Paperless Billing')]")
	private WebElement paperlessBillingTab;

	@FindBy(xpath = "//p[contains(text(),'You will be notified')]")
	private WebElement subtextOnpaperlessBillingTab;

	public PrepaidTMobileIDPage(WebDriver webDriver) {
		super(webDriver);
	}

	// Verify Billing & Payments page
	public PrepaidTMobileIDPage verifyTMobileIdPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(tmobileIDPage);
			Reporter.log("TMobile ID page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("TMobile ID page not displayed");
		}
		return this;
	}

}
