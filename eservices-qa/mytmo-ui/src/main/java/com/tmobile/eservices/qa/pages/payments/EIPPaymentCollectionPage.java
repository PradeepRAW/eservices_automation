package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author gsrujana
 *
 */
public class EIPPaymentCollectionPage extends CommonPage {

	@FindBy(css = "div.Display3 div")
	private List<WebElement> pageHeader;

	private final String pageUrl = "/payments/paymentcollection";

	@FindBy(css = "span.pading-blade-top-xxs.pull-left.Display6.black")
	private WebElement addCard;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public EIPPaymentCollectionPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EIPPaymentCollectionPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public EIPPaymentCollectionPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitforSpinner();
			verifyPageUrl();

		} catch (Exception e) {
			Assert.fail("paymentcollection - page not found.");
		}
		return this;
	}

	/**
	 * * click on add card blade
	 */
	public EIPPaymentCollectionPage clickAddaCardBlade() {
		try {
			checkPageIsReady();
			addCard.click();
			Reporter.log("Add card button clicked");
		} catch (Exception e) {
			Assert.fail("Add card not found");
		}
		return this;
	}

}