package com.tmobile.eservices.qa.pages.payments.api;

import java.io.IOException;
import java.io.StringReader;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.response.Response;

public class TIBCOSubscriber extends EOSCommonLib {

	public Response geResponseTibco_Getsubscriberdetails(Map<Object, Object> tokenMap) throws Exception {
		Response response = null;
		String fileName = System.getProperty("user.dir")
				+ "/src/test/resources/reqtemplates/pub/Getsubscriberinfomiddleware.txt";
		String requestBody = new String(Files.readAllBytes(Paths.get(fileName)));
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		RestService restService = new RestService(updatedRequest, "https://tugs-tmo.internal.t-mobile.com");
		restService.setContentType("text/xml");
		restService.addHeader("soapaction", "gs.Customer.getSubscriberInfo");
		response = restService.callService("genericesp/soap", RestCallType.POST);

		return response;
	}

	public Map<Object, Object> getcreditclass(Response response)
			throws ParserConfigurationException, SAXException, IOException {
		Map<Object, Object> getsubscriber = new HashMap<Object, Object>();
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(response.asString()));

			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			// XPath xpath = XPathFactory.newInstance().newXPath();

			String easypay = "";
			NodeList nl = doc.getElementsByTagName("ns:indicator");
			for (int nde = 0; nde < nl.getLength(); nde++) {
				if (nl.item(nde).getAttributes().getNamedItem("name").toString().contains("EASY_PAY_INDICATOR")) {
					easypay = nl.item(nde).getTextContent();
					break;
				}
			}

			String Creditclass = doc.getElementsByTagName("ns:creditClass").item(0).getTextContent().trim();
			String Address1 = doc.getElementsByTagName("ns:addressLine1").item(0).getTextContent().trim();
			String city = doc.getElementsByTagName("ns:cityName").item(0).getTextContent().trim();
			String state = doc.getElementsByTagName("ns:stateCode").item(0).getTextContent().trim();
			String postalcode = doc.getElementsByTagName("ns:postalCode").item(0).getTextContent().trim();
			String statuscode = doc.getElementsByTagName("ns:statusCode").item(0).getTextContent().trim();
			String pastdue = doc.getElementsByTagName("ns:pastDueAmount").item(0).getTextContent().trim();
			String balancedue = doc.getElementsByTagName("ns:balanceDue").item(0).getTextContent().trim();
			String duedate = doc.getElementsByTagName("ns:dueDate").item(0).getTextContent().trim();

			DecimalFormat df = new DecimalFormat("#.0#");
			df.setRoundingMode(RoundingMode.CEILING);

			Double baldue = Double.parseDouble(balancedue);
			Double pasdue = Double.parseDouble(pastdue);
			balancedue = df.format(baldue);
			pastdue = df.format(pasdue);

			if (baldue != 0)
				getsubscriber.put("balancedue", balancedue);
			else
				getsubscriber.put("balancedue", "0.0");

			if (pasdue != 0)
				getsubscriber.put("pastdue", pastdue);
			else
				getsubscriber.put("pastdue", "0.0");

			if (pasdue > 0)
				getsubscriber.put("haspastdue", "true");
			else
				getsubscriber.put("haspastdue", "false");

			/*
			 * DecimalFormat df = new DecimalFormat("0.0");
			 * 
			 * Double baldue=Double.parseDouble(balancedue); Double
			 * pasdue=Double.parseDouble(pastdue);
			 * 
			 * balancedue =df.format(baldue); pastdue =df.format(pasdue);
			 * 
			 * if(pasdue>0)getsubscriber.put("haspastdue","true") ; else
			 * getsubscriber.put("haspastdue","false") ;
			 */

			getsubscriber.put("Creditclass", Creditclass);
			getsubscriber.put("Address1", Address1);
			getsubscriber.put("city", city);
			getsubscriber.put("state", state);
			getsubscriber.put("postalcode", postalcode);
			getsubscriber.put("statuscode", statuscode);

			duedate = duedate.substring(0, 10);
			getsubscriber.put("easypay", easypay);

			LocalDate date = LocalDate.parse(duedate.toString());

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM. dd, yyyy");
			String reqdate = formatter.format(date).toString();
			getsubscriber.put("duedate", reqdate);
			Map<Object, Object> autopayParameters = getdateparameters(balancedue, date, easypay);

			getsubscriber.put("insideblackoutperiod", autopayParameters.get("insideblackoutperiod"));
			getsubscriber.put("autopaydate", autopayParameters.get("autopaydate"));
			getsubscriber.put("aprecuringdate", autopayParameters.get("aprecuringdate"));

			/*
			 * LocalDate today=LocalDate.now(); if(Period.between(today,
			 * date).getDays()<0)date=date.plusMonths(1);
			 */

		}

		return getsubscriber;
	}

	public Map<Object, Object> getdateparameters(String balancedue, LocalDate duedate, String easypay) {
		Map<Object, Object> dateparameters = new HashMap<Object, Object>();
		LocalDate Autopaydate;
		Double duebalance = Double.parseDouble(balancedue);
		LocalDate Today = LocalDate.now();
		Period diff = Period.between(Today, duedate);
		String isinsideblackoutperiod = "true";

		if (diff.getDays() > 2 || diff.getDays() < 0)
			isinsideblackoutperiod = "false";
		if (diff.getDays() == 0) {
			Autopaydate = duedate.plusMonths(1).minusDays(2);
		}

		else if (isinsideblackoutperiod.equalsIgnoreCase("true") && duebalance > 0
				&& easypay.equalsIgnoreCase("false")) {
			Autopaydate = Today.plusDays(1);
		} else if (duebalance > 0 && diff.getDays() > 2) {
			Autopaydate = duedate.minusDays(2);
		} else {
			Autopaydate = duedate.plusMonths(1).minusDays(2);
		}

		DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("d");
		String aprecdate = formatter1.format(Autopaydate).toString();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy");
		String reqdate = formatter.format(Autopaydate).toString();
		if (diff.getDays() != 0 && easypay.equalsIgnoreCase("false"))
			isinsideblackoutperiod = "false";
		dateparameters.put("insideblackoutperiod", isinsideblackoutperiod);
		dateparameters.put("autopaydate", reqdate);
		dateparameters.put("aprecuringdate", aprecdate);

		return dateparameters;

	}

}