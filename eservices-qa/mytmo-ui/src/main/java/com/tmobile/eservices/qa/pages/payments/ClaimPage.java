package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class ClaimPage extends CommonPage {
	private final String pageUrl = "/claimflow";
	private final String pageUrlAngular = "/claimPaymentMethod";

	@FindAll({ @FindBy(css = "p>span[role=heading]"),
			@FindBy(css = "div>span[aria-label=' Please confirm payment method']") })
	private WebElement pageHeader;

	@FindAll({ @FindBy(css = "p>span"), @FindBy(css = "div.TMO-CLAIM-FLOW div.col-12 span") })
	private List<WebElement> claimPageSecurityMessages;

	@FindAll({ @FindBy(css = "button.PrimaryCTA.btn-primary"), @FindBy(css = "button.PrimaryCTA") })
	private WebElement continueButton;

	@FindAll({ @FindBy(css = "div.textfield>input"), @FindBy(css = "div#test>input") })
	private WebElement cardorBbankNumberField;

	@FindAll({ @FindBy(xpath = "//label[contains(text(),'Enter your card number')]"),
			@FindBy(xpath = "//span[contains(text(),'Enter your Credit card number')]") })
	private WebElement cardNumberFieldLabel;

	@FindAll({ @FindBy(xpath = "//label[contains(text(),'Enter your bank account number')]"),
			@FindBy(xpath = "//span[contains(text(),'Enter your Bank account number')]") })
	private WebElement bankNumberFieldLabel;

	@FindAll({ @FindBy(css = "button.SecondaryCTA.btn-secondary"), @FindBy(css = "button.SecondaryCTA") })
	private WebElement backButton;

	@FindAll({ @FindBy(css = "span#confirmModalButton"),
			@FindBy(xpath = "//span[contains(text(),'Remove from My Wallet')]") })
	private WebElement removeFromMyWallet;

	@FindBy(xpath = "//button[contains(text(),'Yes, Delete')]")
	private WebElement removeYesButton;

	@FindAll({ @FindBy(css = "button.btn-secondary.btn-black"), @FindBy(css = "button#deleteModalCancelBtn") })
	private WebElement removeCancelButton;

	@FindAll({ @FindBy(css = "div.modal-body-font>span.ng-binding"), @FindBy(css = "div#deleteModal div.body span") })
	private WebElement alertTitile;

	@FindAll({@FindBy(css = "div.pull-left.p-t-10"),@FindBy(css="div>p.errormessage")})
	private WebElement errorMessage;

	public ClaimPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ClaimPage verifyPageUrl() {
		waitFor(ExpectedConditions.or(ExpectedConditions.urlContains(pageUrlAngular), ExpectedConditions.urlContains(pageUrl)));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public ClaimPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Cliams Page is loaded.");
		} catch (Exception e) {
			Assert.fail("Cliams page is not loaded");
		}
		return this;
	}

	/**
	 * Click Continue Button
	 * 
	 */
	public void clickContinueButton() {
		try {
			continueButton.click();
			Reporter.log("Continue Button is Clicked");
		} catch (Exception e) {
			Assert.fail("ContinueButton is not Found");
		}
	}

	/**
	 * Click RemoveFromwallet Button
	 * 
	 */
	public void clickRemoveFromWalletButton() {
		try {
			removeFromMyWallet.click();
			Reporter.log("RemoveFromMyWallet Button is Clicked");
		} catch (Exception e) {
			Assert.fail("RemoveFromMyWallet is not Found");
		}
	}

	/**
	 * Click Back Button
	 * 
	 */
	public void clickBackButton() {
		try {
			backButton.click();
			Reporter.log("Back Button is Clicked");
		} catch (Exception e) {
			Assert.fail("Back is not Found");
		}
	}

	/**
	 * EnterCard Number
	 * 
	 */
	public void enterCardorBankAcctNo(String AccountNumber) {
		try {
			waitFor(ExpectedConditions.visibilityOf(continueButton));
			cardorBbankNumberField.sendKeys(AccountNumber);
			Reporter.log("Card/Bank Number Entered");
		} catch (Exception e) {
			Assert.fail("Card/Bank Number field not Found");
		}
	}

	/**
	 * Verfy Claims Page title text
	 * 
	 */
	public void verifyCliamsPageTitleText(String Title) {
		try {
			String SecurityText = "";
			for (int i = 0; i <= claimPageSecurityMessages.size() - 1; i++) {
				SecurityText = SecurityText + claimPageSecurityMessages.get(i).getText();
			}
			Assert.assertTrue(SecurityText.contains(Title.trim()));
			Reporter.log("ClaimPage Security Message Verified");
		} catch (Exception e) {
			Assert.fail("ClaimPage Security Message not Verified");
		}
	}

	/**
	 * Click on Yes for Remove Alert Button
	 * 
	 */
	public void clickYesButton() {
		try {
			removeYesButton.click();
			Reporter.log("Yes Button is Clicked");
		} catch (Exception e) {
			Assert.fail("Yes Button is not Found");
		}
	}

	/**
	 * Click on Cancel for Remove Alert Button
	 * 
	 */
	public void clickCancelButton() {
		try {
			removeCancelButton.click();
			Reporter.log("Cancel Button is Clicked");
		} catch (Exception e) {
			Assert.fail("Cancel Button is not Found");
		}

	}

	/**
	 * Verify Alert title text
	 * 
	 */
	public void verifyAlertTitleText(String Title) {
		try {
			Assert.assertTrue(alertTitile.getText().contains(Title));
			Reporter.log("Alert Title text verfied");
		} catch (Exception e) {
			Assert.fail("Alert Title  not Found");
		}
	}

	/**
	 * Verify Error Message
	 * 
	 */
	public void verifyErrorMessage(String Title) {
		try {
			waitFor(ExpectedConditions.visibilityOf(errorMessage));
			Assert.assertTrue(errorMessage.getText().trim().contains(Title));
			Reporter.log("Error Message verfied: " +Title);
		} catch (Exception e) {
			Assert.fail("Error element  not Found");
		}
	}

	/**
	 * Verify Label for Bank Field
	 * 
	 */
	public void verifyBankLabel() {
		try {
			bankNumberFieldLabel.isDisplayed();
			Reporter.log("Bank Label is displayed");
		} catch (Exception e) {
			Assert.fail("Bank Label is not Found");
		}

	}

	/**
	 * Verify Label for Card Field
	 * 
	 */
	public void verifyCardLabel() {
		try {
			cardNumberFieldLabel.isDisplayed();
			Reporter.log("Card label is displayed");
		} catch (Exception e) {
			Assert.fail("Card Label is not Found");
		}

	}

	public void verifyPiiMasking(String custPaymentPID) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(cardorBbankNumberField, custPaymentPID),
					"Card/Bank Number is not PII masked");
			Reporter.log("Verified PII masking for Claim Page");
		} catch (Exception e) {
			Assert.fail("Failed to Verify: PII masking for Claim page");
		}
	}

}