package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class EIPPaymentPage extends CommonPage {

	private final String pageUrl = "/equipment-payments-landing";

	@FindBy(css = "span.Display3")
	private WebElement pageHeader;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement cancelBtn;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement submitBtn;

	@FindBy(css = "div.animation-in span.Display5")
	private WebElement cancelModal;

	@FindBy(css = "div.animation-in button.SecondaryCTA-accent")
	private WebElement noBtncancelModal;

	@FindBy(css = "div.animation-in button.PrimaryCTA-accent")
	private WebElement yesBtncancelModal;

	@FindBy(css = "div.animation-in div.col-12.body")
	private WebElement cancelModalText;

	@FindBy(linkText = "Terms and Conditions")
	private WebElement termsAndConditionsLink;

	@FindBy(css = "div.TMO-EPP-TERMS-AND-CONDITION p.Display3")
	private WebElement termsAndConditionsHeader;

	@FindBy(css = "div.TMO-EPP-TERMS-AND-CONDITION p.body")
	private WebElement termsAndConditionspagelegaltext;

	@FindBy(css = "div.TMO-EPP-TERMS-AND-CONDITION button.PrimaryCTA")
	private WebElement tncBackBtn;

	@FindBy(css = "div.pull-right.arrow-margin")
	private WebElement eipBlade;

	@FindBy(css = "p span.Display6")
	private WebElement friendlyName;

	@FindBy(css = "span.pull-left.legal.black.p-t-8.m-t-4-xs")
	private WebElement msisdn;
	
	@FindBy(css = "span#paymentNumber")
	private WebElement paymentMethod;

	@FindBy(css = "span.pull-right-xs.body.black.m-t-27-xs")
	private WebElement deviceName;

	@FindBy(css = "div.padding-bottom-xsmall span.pull-right")
	private WebElement remainingBal;

	@FindBy(xpath = "//span[contains(text(),'EIP (Installment Plan) ID:')]/following-sibling::a")
	private WebElement eipPlanId;
	
	@FindBy(css = "span.pading-blade-top-xxs.body-bold.black.pull-left") 
	private WebElement amountLabel;
	
	@FindBy(css = "span.pading-blade-top-xxs.body-bold+span span.black")
	private WebElement extraDeviceamount;

	@FindBy(css="button.PrimaryCTA")
	private WebElement agreeAndSubmitBtn;
	
	@FindBy(css="span[aria-label='change amount active clickable blade']")
	private WebElement amountCheveron;
	
	@FindBy(css="span#paymentDefaultText")
	private WebElement addPaymentMethodsBlade;
	
	

	public EIPPaymentPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EIPPaymentPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public EIPPaymentPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Payment landing Page is loaded.");

		} catch (Exception e) {
			Assert.fail("payment landing page is not loaded");
		}
		return this;
	}

	public void clickCancelBtn() {
		try {
			cancelBtn.click();
			Reporter.log("Clicked on Cancel Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Cancel button");
		}

	}

	/**
	 * verify cancel modal is displayed
	 */
	public void verifyCancelModal() {
		try {
			cancelModal.isDisplayed();
			cancelModalText.isDisplayed();
			Reporter.log("Cancel modal is displayed");
		} catch (Exception e) {
			Assert.fail("Cancel modal not found");
		}

	}

	/**
	 * click No button on cancel modal
	 */
	public void clickNoBtnOnCancelModal() {
		try {
			noBtncancelModal.isDisplayed();
			noBtncancelModal.click();
			Reporter.log("Clicked on No button on Cancel Modal");
		} catch (Exception e) {
			Assert.fail("No button on Cancel Modal is not found");
		}
	}

	/**
	 * click Yes button on cancel modal
	 */
	public void clickYesBtnOnCancelModal() {
		try {
			yesBtncancelModal.isDisplayed();
			yesBtncancelModal.click();
			Reporter.log("Clicked on Yes button on Cancel Modal");
		} catch (Exception e) {
			Assert.fail("Yes button on Cancel Modal is not found");
		}
	}

	/**
	 * click on submit button
	 */
	public void verifySubmitBtn() {
		try {
			submitBtn.isDisplayed();
			Reporter.log("Clicked on Submit Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Submit button");
		}
	}

	/**
	 * verify cancel button is displayed
	 */
	public void verifyCancelBtn() {
		try {
			cancelBtn.isDisplayed();
			Reporter.log("Cancel Button is displayed");
		} catch (Exception e) {
			Assert.fail("Cancel button is not found");
		}
	}

	/**
	 * verify terms and conditions link
	 */
	public void verifyTermsAndConditionsLink() {
		try {
			termsAndConditionsLink.isDisplayed();
			Reporter.log("Terms and Conditions link is verified");
		} catch (Exception e) {
			Assert.fail("Terms and Conditions Link is not found");
		}
	}

	/**
	 * click Terms and conditions link
	 */
	public void clickTermsAndConditionsLink() {
		try {
			termsAndConditionsLink.click();
			Reporter.log("Terms and Conditions link is clicked");
		} catch (Exception e) {
			Assert.fail("Terms and Conditions Link is not found");
		}
	}

	/**
	 * verify Terms and conditions page
	 */
	public void verifyTermsandConditionsPage() {
		try {
			termsAndConditionsHeader.isDisplayed();
			Assert.assertTrue(termsAndConditionsHeader.getText().equals("Terms & conditions"));
			termsAndConditionspagelegaltext.isDisplayed();
			tncBackBtn.isDisplayed();
			Reporter.log("Verified Terms and conditions page");
		} catch (Exception e) {
			Assert.fail("Terms and conditions page is not found");
		}
	}

	/**
	 * click back button on TnC page
	 */
	public void clickTandCbackBtn() {
		try {
			tncBackBtn.click();
			Reporter.log("CLicked Back button on T n C page");
		} catch (Exception e) {
			Assert.fail("Back button not found on T n C page");
		}
	}

	/**
	 * click eip blade
	 */
	public void clickEipBlade() {
		try {
			eipBlade.isDisplayed();
			eipBlade.click();
			Reporter.log("Eip Blade is clicked");
		} catch (Exception e) {
			Assert.fail("Eip Blade is not found");
		}
	}

	/**
	 * get eip plan ID
	 * 
	 * @return
	 */
	public String getEipPlanId() {
		try {
			return eipPlanId.getText();
		} catch (Exception e) {
			Assert.fail("Failed to get EIP Plan ID");
		}
		return null;
	}

	/**
	 * get Friendly Name
	 * 
	 * @return
	 */
	public String getFriendlyName() {
		try {
			return friendlyName.getText();
		} catch (Exception e) {
			Assert.fail("Failed to get Friendly Name");
		}
		return null;
	}

	/**
	 * get MSISDN
	 * 
	 * @return
	 */
	public String getMsisdn() {
		try {
			return msisdn.getText();
		} catch (Exception e) {
			Assert.fail("Failed to get MSISDN");
		}
		return null;
	}

	/**
	 * get device balance
	 * 
	 * @return
	 */
	public String getRemainingBal() {
		try {
			return remainingBal.getText();
		} catch (Exception e) {
			Assert.fail("Failed to get Device Balance");
		}
		return null;
	}

	/**
	 * get device name
	 * 
	 * @return
	 */
	public String getDeviceName() {
		try {
			return deviceName.getText();
		} catch (Exception e) {
			Assert.fail("Failed to get device name");
		}
		return null;
	}
	
	/**
	 * get Extra Device Amount
	 * @return
	 */
	public String getExtraDeviceAmount() {
		try {
			return extraDeviceamount.getText();
		} catch (Exception e) {
			Assert.fail("Failed to get Extra Device Amount");
		}
		return null;
	}
	
	/**
	 * click amount blade
	 */
	public void clickAmountBlade() {
		try {
			amountLabel.isDisplayed();
			amountLabel.click();
			Reporter.log("Amount Blade is clicked");
		} catch (Exception e) {
			Assert.fail("Amount Blade is not found");
		}
	}

	/**
	 * verify pii masking for name
	 * @param piiCustomerCustfirstnamePid
	 */
	public void verifyNamePiiMasking(String piiCustomerCustfirstnamePid) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(friendlyName, piiCustomerCustfirstnamePid));
			Reporter.log("Friendly Name is PII masked");
		} catch (Exception e) {
			Assert.fail("Friendly Name is not PII masked");
		}
	}

	/**
	 * verify pii masking for msisdn
	 * @param piiCustomerMsisdnPid
	 */
	public void verifyMsisdnPiiMasking(String piiCustomerMsisdnPid) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(msisdn, piiCustomerMsisdnPid));
			Reporter.log("MSISDN is PII masked");
		} catch (Exception e) {
			Assert.fail("MSISDN is not PII masked");
		}		
	}

	/**
	 * verify pii masking for payment method
	 * @param piiCustomerPaymentinfoPid
	 */
	public void verifyPaymentMethodPiiMasking(String piiCustomerPaymentinfoPid) {
		try {
			checkPageIsReady();
			Assert.assertTrue(checkElementisPIIMasked(paymentMethod, piiCustomerPaymentinfoPid));
			Reporter.log("Payment method is PII masked");
		} catch (Exception e) {
			Assert.fail("Payment method is not PII masked");
		}		
	}

	/**
	 * click payment method blade
	 */
	public void clickPaymentMethodBlade() {
		try {
			paymentMethod.click();
			Reporter.log("Clicked on payment method blade");
		} catch (Exception e) {
			Assert.fail("Failed to click on payment method blade");
		}
		
	}

	/**
	 * click on agree and submit button
	 */
	public void clickAgreeandSubmitBtn() {
		try {
			agreeAndSubmitBtn.click();
			Reporter.log("Clicked on Agree and Submit button");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Agree and Submit button");
		}
	}
	
	
	/**
	 * verify No Payment Amount Chevron
	 */
	public void verifyNoPaymentAmountChevron() {
		try {
			if(amountCheveron.isDisplayed()){
				Assert.fail("PaymentAmount Chevron is displayed");
			}
			
		} catch (Exception e) {
			Reporter.log("PaymentAmount Chevron is not Found");
		}
	}
	
	
	
	/**
	 * verify Payment Amount Blade is enabled
	 */
	public boolean verifyPaymentAmountEnabled() {			
			boolean isEnabled = false;
			try {
				isEnabled = amountLabel.isEnabled();
			} catch (Exception e) {
				Assert.fail("PaymentAmountBlade  is enabled");
			}
			return isEnabled;	
	}
	
	
	
	/**
	 * verify EIP installment Plan ID is Clickable or not
	 */
	public void verifyEipInstallmentPlanclick() {
		try {
			if(eipPlanId.isEnabled()){
				eipPlanId.click();
				Reporter.log("EIP installment PlanId  is clicked");
			}
			
		} catch (Exception e) {
			Assert.fail("EIP installment PlanId  is not Found");
		}
	}
	
	
	/**
	 * get Cancel Modal Body Text
	 * 
	 * @return
	 */
	public String getModelDialogText() {
		try {
			return cancelModalText.getText();
		} catch (Exception e) {
			Assert.fail("Failed to get cancelModalText");
		}
		return null;
	}
		
	}
