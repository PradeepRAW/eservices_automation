package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class EbillStagingApi extends ApiCommonLib {
	
	

	 private Map<String, String> buildFEbillStagingAPIHeader(ApiTestData apiTestData) throws Exception {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", getAuthHeader());
			headers.put("Accept", "application/json");
			//headers.put("ban",apiTestData.getBan());
			//headers.put("msisdn",apiTestData.getMsisdn());

			return headers;
		}
	 

	
	public Response getListbills(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildFEbillStagingAPIHeader(apiTestData);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
	
		Response response = service.callService("services/rest/listbills/"+apiTestData.getBan() , RestCallType.GET);
		System.out.println(response.body().asString());
		return response;
	}
	
	public Response getPrintBillSummary(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildFEbillStagingAPIHeader(apiTestData);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
	
		Response response = service.callService("services/rest/printsummary/9212816412/473114962/MSISDN/"+apiTestData.getMsisdn() , RestCallType.GET);
		System.out.println(response.body().asString());
		return response;
	}

	public Response getPrintBill(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildFEbillStagingAPIHeader(apiTestData);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
	
		Response response = service.callService("services/rest/printbill/9787984403/960820110/BAN/"+apiTestData.getBan() , RestCallType.GET);
		System.out.println(response.body().asString());
		return response;
	}
}
