package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author rprakash
 *
 */
public class EIPPaymentAmountPage extends CommonPage{
	
	private final String pageUrl = "/eip-jod-poip/equipment-payments-landing";
	

	@FindBy(css = "p.Display3")
	private WebElement pageHeader;
	
	@FindBy(css = "input#extraAmount")
	private WebElement extraDevicePaymentRadio;
	
	@FindBy(css = "label[for='extraAmount']")
	private WebElement extraDevicePaymentRadioLabel;
	
	@FindBy(css = "label[for='fullAmount']")
	private WebElement pipFullRadioLabel;
		
	@FindBy(css = "input#extraPaymentID")
	private WebElement extraPayment;
	
	@FindBy(css = "span.right.body")
	private WebElement outStandingEIP;
	
	@FindBy(css = "span.legal.color-red.text-left.padding-top-xsmall.errormessage.mb-0.pl-0.p-r-10-xs")
	private WebElement errorMessage;
	
	@FindBy(css="button.PrimaryCTA")
	private WebElement continueBtn;
	
	
	public EIPPaymentAmountPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EIPPaymentAmountPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}
	
	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public EIPPaymentAmountPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Payment amount Page is loaded.");

		} catch (Exception e) {
			Assert.fail("payment aount page is not loaded");
		}
		return this;
	}
	
	public EIPPaymentAmountPage verifyEditAmount() {
		extraDevicePaymentRadio.isSelected();
		extraPayment.click();
		extraPayment.sendKeys(String.valueOf(Float.valueOf(outStandingEIP.getText().substring(1)) + 1));
		String msg1 = errorMessage.getText();
		Assert.assertTrue("Amount can not exceed plan balance.".equals(msg1), "Amount can not exceed plan balance.");
		extraPayment.clear();
		extraPayment.sendKeys("1");
		String msg2 = errorMessage.getText();
		Assert.assertTrue("Payment amount must be more than $1.00".equals(msg2), "Amount can not exceed plan balance.");
		pipFullRadioLabel.click();
		Assert.assertTrue("".equals(extraPayment.getText()), "Extra device payment field is cleared.");
		Assert.assertTrue(!extraPayment.isEnabled(), "Extra device payment field is disabled.");
		extraDevicePaymentRadioLabel.click();
		extraPayment.clear();
		extraPayment.sendKeys("2");
		continueBtn.click();
		return this;
	}

	/**
	 * set amount in extra amount field
	 * @param amount 
	 */
	public void setAmount(Double amount) {
		try {
			extraPayment.click();
			extraPayment.sendKeys(amount.toString());
			Reporter.log("Entered Extra amount in Edit amount page");
		} catch (Exception e) {
			Assert.fail("Failed to enter amount");
		}
	}

	/**
	 * click continue button
	 */
	public void clickContinueBtn() {
		try {
			continueBtn.click();
			Reporter.log("Clicked continue button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Continue button");
		}
	}
		
}
