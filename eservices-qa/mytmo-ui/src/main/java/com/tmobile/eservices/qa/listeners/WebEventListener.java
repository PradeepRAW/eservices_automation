package com.tmobile.eservices.qa.listeners;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import io.appium.java_client.AppiumDriver;

/**
 * @author blakshminarayana
 *
 */
public class WebEventListener extends AbstractWebDriverEventListener {

	private WebDriver webDriver;

	public WebEventListener(WebDriver driver) {
		this.webDriver = driver;
	}

	@FindAll({ @FindBy(css = "div.acsActionShell a.acsDeclineButton"), @FindBy(css = "img.LPMcloseButton") })
	private List<WebElement> declineButton;

	@FindBy(css = "div#acsFullScreenContainer div a.acsCloseButton--link.acsCloseButton.acsDeclineButton")
	private List<WebElement> appiumDeclineButton;

	@Override
	public void beforeClickOn(WebElement element, WebDriver driver) {
		handlePopUp(webDriver);
		super.beforeClickOn(element, webDriver);
	}

	@Override
	public void beforeFindBy(By by, WebElement element, WebDriver driver) {
		handlePopUp(webDriver);
		super.beforeFindBy(by, element, webDriver);
	}

	// @Override
	// public void beforeChangeValueOf(WebElement element, WebDriver driver,) {
	// handlePopUp(webDriver);
	// super.beforeChangeValueOf(element, webDriver);
	// }

	@Override
	public void beforeNavigateBack(WebDriver driver) {
		handlePopUp(webDriver);
		super.beforeNavigateBack(webDriver);
	}

	@Override
	public void beforeScript(String script, WebDriver driver) {
		handlePopUp(webDriver);
		super.beforeScript(script, webDriver);
	}

	@Override
	public void beforeNavigateRefresh(WebDriver driver) {
		handlePopUp(webDriver);
		super.beforeNavigateRefresh(webDriver);
	}

	@Override
	public void onException(Throwable throwable, WebDriver driver) {
		handlePopUp(webDriver);
		super.onException(throwable, webDriver);
	}

	@Override
	public void beforeNavigateForward(WebDriver driver) {
		handlePopUp(webDriver);
		super.beforeNavigateForward(webDriver);
	}

	@Override
	public void beforeNavigateTo(String url, WebDriver driver) {
		handlePopUp(webDriver);
		super.beforeNavigateTo(url, webDriver);
	}

	@Override
	public void afterNavigateTo(String paramString, WebDriver driver) {
		handlePopUp(webDriver);
		super.afterNavigateTo(paramString, webDriver);

	}

	@Override
	public void afterNavigateBack(WebDriver driver) {
		handlePopUp(webDriver);
		super.afterNavigateBack(webDriver);
	}

	@Override
	public void afterNavigateForward(WebDriver driver) {
		handlePopUp(webDriver);
		super.afterNavigateForward(webDriver);

	}

	@Override
	public void afterNavigateRefresh(WebDriver driver) {
		handlePopUp(webDriver);
		super.afterNavigateRefresh(webDriver);

	}

	@Override
	public void afterFindBy(By paramBy, WebElement paramWebElement, WebDriver driver) {
		handlePopUp(webDriver);
		super.afterFindBy(paramBy, paramWebElement, webDriver);
	}

	@Override
	public void afterClickOn(WebElement paramWebElement, WebDriver driver) {
		handlePopUp(webDriver);
		super.afterClickOn(paramWebElement, webDriver);

	}

	// @Override
	// public void afterChangeValueOf(WebElement paramWebElement, WebDriver
	// driver) {
	// handlePopUp(webDriver);
	// super.afterChangeValueOf(paramWebElement, webDriver);
	// }

	/*
	 * private void handlePopUp(WebDriver webDriver) { List<WebElement>
	 * findElements; if (webDriver instanceof AppiumDriver) { findElements =
	 * webDriver.findElements(By.cssSelector(
	 * "div#acsFullScreenContainer div a.acsCloseButton--link.acsCloseButton.acsDeclineButton"
	 * )); } else { findElements = webDriver.findElements(By.cssSelector(
	 * "div.acsActionShell a.acsDeclineButton")); } if
	 * (CollectionUtils.isNotEmpty(findElements) &&
	 * !CollectionUtils.sizeIsEmpty(findElements)) { for (WebElement webElement
	 * : findElements) { if (webElement != null && webElement.isDisplayed()) {
	 * webElement.click(); break; } } } }
	 */

	private void handlePopUp(WebDriver webDriver) {
		List<WebElement> findElements;
		if (webDriver instanceof AppiumDriver) {
			findElements = appiumDeclineButton;
		} else {
			findElements = declineButton;
		}
		if (CollectionUtils.isNotEmpty(findElements) && !CollectionUtils.sizeIsEmpty(findElements)) {
			for (WebElement webElement : findElements) {
				if (webElement != null && webElement.isDisplayed()) {
					webElement.click();
					break;
				}
			}
		}
	}

}
