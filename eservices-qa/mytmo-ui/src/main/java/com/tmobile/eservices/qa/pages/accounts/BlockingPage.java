package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class BlockingPage extends CommonPage {

	private static final String pageUrl = "profile/blocking";

	@FindBy(xpath = "//p[contains(text(),'Block Charged International Roaming') or contains(text(),'Block charged international roaming')]/../../div/span/label")
	private WebElement blockChargeToggle;

	@FindBy(xpath = "//p[contains(text(),'Block charged international roaming ') or contains(text(),'Block Charged International Roaming ')]//..//..//input")
	private WebElement blockChargeToggleIP;

	@FindBy(xpath = "//p[contains(text(),'Block International Roaming') or contains(text(),'Block international roaming')]/../../div/span/label")
	private WebElement blockInternationalToggle;

	@FindBy(xpath = "//p[contains(text(),'Block International Roaming') or contains(text(),'Block international roaming')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement blockInternationalToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Block International Roaming') or contains(text(),'Block international roaming')]/../../div/span/label[@class='switch mt-1']")
	private WebElement blockInternationalToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Block International Roaming') or contains(text(),'Block international roaming')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement blockInternationalToggleDisableStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Charged International Roaming') or contains(text(),'Block charged international roaming')]/../../div/span/label[@class='switch mt-1']")
	private WebElement blockChargeToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Charged International Roaming') or contains(text(),'Block charged international roaming')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement blockChargeToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Charged International Roaming') or contains(text(),'Block charged international roaming')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement blockChargeToggleDisableStatus;

	@FindBy(xpath = "//p[contains(text(),'Block international roaming') or contains(text(),'Block International Roaming')]//..//..//input")
	private WebElement blockInternationalRoamingIP;

	@FindBy(xpath = "//p[contains(text(),'Block Charged International Roaming') or contains(text(),'Block charged international roaming')]")
	private WebElement blockChargeHeader;

	@FindBy(xpath = "//p[contains(text(),'Block International Roaming') or contains(text(),'Block international roaming')]")
	private WebElement blockInternational;

	@FindBy(xpath = "//p[contains(text(),'Block Charged International Roaming') or contains(text(),'Block charged international roaming')]/../../div//a")
	private WebElement blockChargeMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Block Charged International Roaming') or contains(text(),'Block charged international roaming')]/../../div/p[2]")
	private WebElement blockChargedInternationRoamingDisableMsg;

	@FindBy(xpath = "//p[contains(text(),'Block International Roaming') or contains(text(),'Block international roaming')]//..//a")
	private WebElement blockInternationalMorebtn;

	@FindBy(css = ".body.d-inline-block")
	private WebElement showMoreTxt;

	@FindBy(css = ".Display5")
	private List<WebElement> updateOperationFailedDialog;

	@FindBy(css = ".PrimaryCTA-accent")
	private WebElement updateOperationFailedDialogOkBtn;

	@FindBy(xpath = "//p[contains(text(),'Block All Messages') or contains(text(),'Block all messages')]/../../div/span/label")
	private WebElement blockAllMessagesToggle;

	@FindBy(xpath = "//p[contains(text(),'Block All Messages') or contains(text(),'Block all messages')]/../../div/span/label[@class='switch mt-1']")
	private WebElement blockAllMessagesToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Block All Messages') or contains(text(),'Block all messages')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement blockAllMessagesToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Block All Messages') or contains(text(),'Block all messages')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement blockAllMessagesToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Block All Messages') or contains(text(),'Block all messages')]/../../div/span/p")
	private WebElement blockAllMessagesMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Block All Messages') or contains(text(),'Block all messages')]")
	private WebElement blockAllMessagesHeader;

	@FindBy(xpath = "//p[contains(text(),'Block Content Downloads') or contains(text(),'Block content downloads')]/../../div/span/label")
	private WebElement blockContentDownloadsToggle;

	@FindBy(xpath = "//p[contains(text(),'Block Content Downloads') or contains(text(),'Block content downloads')]/../../div/span/label[@class='switch mt-1']")
	private WebElement blockContentDownloadsToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Content Downloads') or contains(text(),'Block content downloads')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement blockContentDownloadsToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Content Downloads') or contains(text(),'Block content downloads')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement blockContentDownloadsToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Content Downloads') or contains(text(),'Block content downloads')]/../../div/span/p")
	private WebElement blockContentDownloadsMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Block Content Downloads') or contains(text(),'Block content downloads')]")
	private WebElement blockContentDownloadsHeader;

	@FindBy(xpath = "//p[contains(text(),'Block Instant Message sent via SMS') or contains(text(),'Block instant message sent via sms')]/../../div/span/label[@class='switch mt-1']")
	private WebElement blockInstantMessageToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Instant Message sent via SMS') or contains(text(),'Block instant message sent via sms')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement blockInstantMessageToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Instant Message sent via SMS') or contains(text(),'Block instant message sent via sms')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement blockInstantMessageToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Instant Message sent via SMS') or contains(text(),'Block instant message sent via sms')]/../../div/span/label")
	private WebElement blockInstantMessageToggle;

	@FindBy(xpath = "//p[contains(text(),'Block Instant Message sent via SMS') or contains(text(),'Block instant message sent via sms')]/../../div/span/p")
	private WebElement blockInstantMessageMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Block Instant Message sent via SMS') or contains(text(),'Block instant message sent via sms')]/../../div/p[2]")
	private WebElement blockInstantMessageDisableMessage;

	@FindBy(xpath = "//p[contains(text(),'Block Instant Message sent via SMS') or contains(text(),'Block instant message sent via sms')]")
	private WebElement blockInstantMessageHeader;

	@FindBy(xpath = "//p[contains(text(),'Block TMOmail.net email') or contains(text(),'Block tmomail.net email')]/../../div/span/label")
	private WebElement blockTMOmailToggle;

	@FindBy(xpath = "//p[contains(text(),'Block TMOmail.net email') or contains(text(),'Block tmomail.net email')]/../../div/span/label[@class='switch mt-1']")
	private WebElement blockTMOmailToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Block TMOmail.net email') or contains(text(),'Block tmomail.net email')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement blockTMOmailToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Block TMOmail.net email') or contains(text(),'Block tmomail.net email')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement blockTMOmailToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Block TMOmail.net email') or contains(text(),'Block tmomail.net email')]/../../div/span/p")
	private WebElement blockTMOmailMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Block TMOmail.net email') or contains(text(),'Block tmomail.net email')]/../../div/p[2]")
	private WebElement blockTMOmailDisableMessage;

	@FindBy(xpath = "//p[contains(text(),'Block TMOmail.net email') or contains(text(),'Block tmomail.net email')]")
	private WebElement blockTMOmailHeader;

	@FindBy(xpath = "//p[contains(text(),'Block Text and Picture Messages') or contains(text(),'Block text and picture messages')]/../../div/span/label")
	private WebElement blockTextMessagesToggle;

	@FindBy(xpath = "//p[contains(text(),'Block Text and Picture Messages') or contains(text(),'Block text and picture messages')]/../../div/span/label[@class='switch mt-1']")
	private WebElement blockTextMessagesToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Text and Picture Messages') or contains(text(),'Block text and picture messages')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement blockTextMessagesToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Text and Picture Messages') or contains(text(),'Block text and picture messages')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement blockTextMessagesToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Block Text and Picture Messages') or contains(text(),'Block text and picture messages')]/../../div/p[2]")
	private WebElement blockTextMessagesDisableMessage;

	@FindBy(xpath = "//p[contains(text(),'Block Text and Picture Messages') or contains(text(),'Block text and picture messages')]/../../div/span/p")
	private WebElement blockTextMessagesMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Block Text and Picture Messages') or contains(text(),'Block Text and Picture Messages')]")
	private WebElement blockTextMessagesHeader;

	@FindBy(xpath = "//p[contains(text(),'Device Block') or contains(text(),'Device block')]/../../div/span/label")
	private WebElement deviceBlockToggle;

	@FindBy(xpath = "//p[contains(text(),'Device Block') or contains(text(),'Device block')]/../../div/span/label[@class='switch mt-1']")
	private WebElement deviceBlockToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Device Block') or contains(text(),'Device block')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement deviceBlockToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Device Block') or contains(text(),'Device block')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement deviceBlockToggleEnabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Device Block') or contains(text(),'Device block')]/../../div/span/p")
	private WebElement deviceBlockMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Device Block') or contains(text(),'Device block')]")
	private WebElement deviceBlockHeader;

	@FindBy(xpath = "//p[contains(text(),'Scam Block') or contains(text(),'Scam block')]/../../div/span/label")
	private WebElement scamBlockToggle;

	@FindBy(xpath = "//p[contains(text(),'Scam Block') or contains(text(),'Scam block')]/../../div/span/label[@class='switch mt-1']")
	private WebElement scamBlockToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Scam Block') or contains(text(),'Scam block')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement scamBlockToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Scam Block') or contains(text(),'Scam block')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement scamBlockToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Scam Block') or contains(text(),'Scam block')]/../../div/span/p")
	private WebElement scamBlockMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Scam Block') or contains(text(),'Scam block')]")
	private WebElement scamBlockHeader;

	@FindBy(xpath = "//p[contains(text(),'Scam ID') or contains(text(),'Scam id')]/../../div/span/label")
	private WebElement scamIDToggle;

	@FindBy(xpath = "//p[contains(text(),'Scam ID') or contains(text(),'Scam id')]/../../div/span/label[@class='switch mt-1']")
	private WebElement scamIDToggleOffStatus;

	@FindBy(xpath = "//p[contains(text(),'Scam ID') or contains(text(),'Scam id')]/../../div/span/label[contains(@class,'Enabled')]")
	private WebElement scamIDToggleOnStatus;

	@FindBy(xpath = "//p[contains(text(),'Scam ID') or contains(text(),'Scam id')]/../../div/span/label[contains(@class,'Disabled')]")
	private WebElement scamIDToggleDisabledStatus;

	@FindBy(xpath = "//p[contains(text(),'Scam ID') or contains(text(),'Scam id')]/../../div/span/p")
	private WebElement scamIDMorebtn;

	@FindBy(xpath = "//p[contains(text(),'Scam ID') or contains(text(),'Scam id')]")
	private WebElement scamIDHeader;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement lineHeader;

	@FindBys(@FindBy(css = "div#test ul li"))
	private List<WebElement> lineList;

	@FindBy(css = "p.red")
	private WebElement swapLineMessage;

	public BlockingPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Blocking page.
	 *
	 * @return the BlockingPage class instance.
	 */
	public BlockingPage verifyBlockingPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Blocking page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Blocking page not displayed");
		}
		return this;

	}

	/**
	 * Verify Block international header.
	 */
	public BlockingPage verifyBlockInternationalHeader() {
		try {
			waitforSpinnerinProfilePage();
			blockInternational.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Block international header not displayed");
		}
		return this;
	}

	/**
	 * Verify Block charge header.
	 */
	public BlockingPage verifyBlockChargeHeader() {
		try {
			waitforSpinnerinProfilePage();
			blockChargeHeader.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Block charge header not displayed");
		}
		return this;
	}

	/**
	 * Click Block Charge toggle.
	 */
	public BlockingPage clickBlockChargerToggle() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(blockChargeToggle));
			blockChargeToggle.click();
			Reporter.log("Clicked on Block charge International roaming button");
		} catch (Exception e) {
			Assert.fail("Click on blockCharge toggle failed");
		}
		return this;
	}

	/**
	 * Click BlockInternational toggle.
	 */
	public BlockingPage clickBlockInternationalToggle() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(blockInternationalToggle));
			blockInternationalToggle.click();
			Reporter.log("Clicked on block international toggle");
		} catch (Exception e) {
			Assert.fail("Click on block international toggle failed");
		}
		return this;
	}

	/**
	 * Click blockInternationalMore button.
	 */
	public BlockingPage clickBlockInternationalMorebtn() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(blockInternationalMorebtn));
			blockInternationalMorebtn.click();
			showMoreTxt.isDisplayed();
			blockInternationalMorebtn.click();
			Reporter.log("Clicked on blockInternationalMore button");
		} catch (Exception e) {
			Assert.fail("Click on blockInternationalMore button failed");
		}
		return this;
	}

	/**
	 * Click BlockChargeMore button.
	 */
	public BlockingPage clickBlockChargeMorebtn() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(blockChargeMorebtn));
			blockChargeMorebtn.click();
			showMoreTxt.isDisplayed();
			blockChargeMorebtn.click();
			Reporter.log("Clicked on blockChargeMore button");
		} catch (Exception e) {
			Assert.fail("Click on blockChargeMore button failed");
		}
		return this;
	}

	/**
	 * Verify block all messages header.
	 */
	public BlockingPage verifyBlockAllMessagesHeader() {
		try {
			waitforSpinnerinProfilePage();
			blockAllMessagesHeader.isDisplayed();
			Reporter.log("Block all messages header displayed");
		} catch (Exception e) {
			Assert.fail("Block all messages header not displayed");
		}
		return this;
	}

	/**
	 * Verify block all messages toggle.
	 */
	public BlockingPage verifyBlockAllMessagesToggle() {
		try {
			waitforSpinnerinProfilePage();
			blockAllMessagesToggle.isDisplayed();
			Reporter.log("Block all messages toggle displayed");
		} catch (Exception e) {
			Assert.fail("Block all messages toggle not displayed");
		}
		return this;
	}

	/**
	 * Verify block content downloads header.
	 */
	public BlockingPage verifyBlockContentDownloadsHeader() {
		try {
			waitforSpinnerinProfilePage();
			blockContentDownloadsHeader.isDisplayed();
			Reporter.log("Block block content downloads header displayed");
		} catch (Exception e) {
			Assert.fail("Block block content downloads header not displayed");
		}
		return this;
	}

	/**
	 * Verify block all messages toggle.
	 */
	public BlockingPage verifyBlockContentDownloadsToggle() {
		try {
			waitforSpinnerinProfilePage();
			blockContentDownloadsToggle.isDisplayed();
			Reporter.log("Block block content downloads toggle displayed");
		} catch (Exception e) {
			Assert.fail("Block block content downloads not displayed");
		}
		return this;
	}

	/**
	 * Verify block instant message header.
	 */
	public BlockingPage verifyBlockInstantMessageHeader() {
		try {
			waitforSpinnerinProfilePage();
			blockInstantMessageHeader.isDisplayed();
			Reporter.log("Block InstantMessage header displayed");
		} catch (Exception e) {
			Assert.fail("Block InstantMessage header not displayed");
		}
		return this;
	}

	/**
	 * Verify block instant message toggle.
	 */
	public BlockingPage verifyBlockInstantMessageToggle() {
		try {
			waitforSpinnerinProfilePage();
			blockInstantMessageToggle.isDisplayed();
			Reporter.log("Block instant message toggle displayed");
		} catch (Exception e) {
			Assert.fail("Block instant message not displayed");
		}
		return this;
	}

	/**
	 * Verify TMO mail header.
	 */
	public BlockingPage verifyTMOmailHeader() {
		try {
			waitforSpinnerinProfilePage();
			blockTMOmailHeader.isDisplayed();
			Reporter.log("TMO mail header displayed");
		} catch (Exception e) {
			Assert.fail("TMO mail header not displayed");
		}
		return this;
	}

	/**
	 * Verify TMO mail toggle.
	 */
	public BlockingPage verifyTMOmailToggle() {
		try {
			waitforSpinnerinProfilePage();
			blockTMOmailToggle.isDisplayed();
			Reporter.log("TMO mail toggle displayed");
		} catch (Exception e) {
			Assert.fail("TMO mail not displayed");
		}
		return this;
	}

	/**
	 * Verify block text messages header.
	 */
	public BlockingPage verifyBlockTextMessagesHeader() {
		try {
			waitforSpinnerinProfilePage();
			blockTextMessagesHeader.isDisplayed();
			Reporter.log("Block text messages header displayed");
		} catch (Exception e) {
			Assert.fail("Block text messages header not displayed");
		}
		return this;
	}

	/**
	 * Verify block text messages toggle.
	 */
	public BlockingPage verifyBlockTextMessagesToggle() {
		try {
			waitforSpinnerinProfilePage();
			blockTextMessagesToggle.isDisplayed();
			Reporter.log("Block text messages toggle displayed");
		} catch (Exception e) {
			Assert.fail("Block text messagesnot displayed");
		}
		return this;
	}

	/**
	 * Verify device block header.
	 */
	public BlockingPage verifyDeviceBlocksHeader() {
		try {
			waitforSpinnerinProfilePage();
			deviceBlockHeader.isDisplayed();
			Reporter.log("Device block header displayed");
		} catch (Exception e) {
			Assert.fail("Device block header not displayed");
		}
		return this;
	}

	/**
	 * Verify device block toggle.
	 */
	public BlockingPage verifyDeviceBlockToggleToggle() {
		try {
			waitforSpinnerinProfilePage();
			deviceBlockToggle.isDisplayed();
			Reporter.log("Device block toggle displayed");
		} catch (Exception e) {
			Assert.fail("Device block toggle not displayed");
		}
		return this;
	}

	/**
	 * Verify scam block header.
	 */
	public BlockingPage verifyScamBlocksHeader() {
		try {
			waitforSpinnerinProfilePage();
			scamBlockHeader.isDisplayed();
			Reporter.log("Scam block header displayed");
		} catch (Exception e) {
			Assert.fail("Scam block header not displayed");
		}
		return this;
	}

	/**
	 * Verify scam block toggle.
	 */
	public BlockingPage verifyScamBlockToggle() {
		try {
			waitforSpinnerinProfilePage();
			scamBlockToggle.isDisplayed();
			Reporter.log("Scam block toggle displayed");
		} catch (Exception e) {
			Assert.fail("Scam block toggle not displayed");
		}
		return this;
	}

	/**
	 * Verify scam id header.
	 */
	public BlockingPage verifyScamIDHeader() {
		try {
			waitforSpinnerinProfilePage();
			scamIDHeader.isDisplayed();
			Reporter.log("Scam id header displayed");
		} catch (Exception e) {
			Assert.fail("Scam id header not displayed");
		}
		return this;
	}

	/**
	 * Verify scam id toggle.
	 */
	public BlockingPage verifyScamIDToggle() {
		try {
			waitforSpinnerinProfilePage();
			scamIDToggle.isDisplayed();
			Reporter.log("Scam id toggle displayed");
		} catch (Exception e) {
			Assert.fail("Scam id toggle not displayed");
		}
		return this;
	}

	/**
	 * Verify block charged international roaming disable message.
	 */
	public BlockingPage verifyBlockChargedInternationRoamingDisableMsg() {
		try {
			waitforSpinnerinProfilePage();
			blockChargedInternationRoamingDisableMsg.isDisplayed();
			Reporter.log("International roaming disable message is displayed");
		} catch (Exception e) {
			Assert.fail("International roaming disable message not  displayed");
		}
		return this;
	}

	/**
	 * Click block all messages toggle.
	 */
	public BlockingPage clickBlockAllMessagesToggle() {
		if (!isElementDisplayed(blockAllMessagesToggleOnStatus)) {
			try {
				waitforSpinnerinProfilePage();
				blockAllMessagesToggle.click();
				Reporter.log("Click on block all messages toggle is success");
			} catch (Exception e) {
				Assert.fail("Click on block all messages toggle failed");
			}
		}
		Reporter.log("Click on block all messages toggle is success");
		return this;
	}

	/**
	 * Verify block text and pictures disable message.
	 */
	public BlockingPage verifyblockTextMessagesDisableMsg() {
		try {
			waitforSpinnerinProfilePage();
			blockTextMessagesDisableMessage.isDisplayed();
			Reporter.log("Block text and pictures disable message is displayed");
		} catch (Exception e) {
			Assert.fail("Block text and pictures disable message not  displayed");
		}
		return this;
	}

	/**
	 * Verify block tmo mail disable message.
	 */
	public BlockingPage verifyblockTMOmailDisableMsg() {
		try {
			waitforSpinnerinProfilePage();
			blockTMOmailDisableMessage.isDisplayed();
			Reporter.log("Block tmo mail disable message is displayed");
		} catch (Exception e) {
			Reporter.log("Block tmo mail disable message not displayed");
			Assert.fail("Block tmo mail disable message not displayed");
		}
		return this;
	}

	/**
	 * Verify block instant message sent via sms disable message.
	 */
	public BlockingPage verifyblockInstantMessageDisableMsg() {
		try {
			waitforSpinnerinProfilePage();
			blockInstantMessageDisableMessage.isDisplayed();
			Reporter.log("Block instant message sent via sms disable message is displayed");
		} catch (Exception e) {
			Reporter.log("Block instant message sent via sms disable message not  displayed");
			Assert.fail("Block instant message sent via sms disable message not  displayed");
		}
		return this;
	}

	/**
	 * Change current line to suspended line.
	 */
	public BlockingPage changeToSuspendedLine() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(lineHeader));
			lineHeader.click();
			moveToElement(lineList.get(3));
			lineList.get(3).click();
			Reporter.log("Change to suspend line is success");
		} catch (Exception e) {
			Assert.fail("Change to suspend line is failed");
		}
		return this;
	}

	/**
	 * Verify suspend line message.
	 */
	public BlockingPage verifySuspendLineMessage() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(isElementDisplayed(swapLineMessage), "Suspend line message not displayed");
			Reporter.log("Suspend line message displayed");
		} catch (Exception e) {
			Assert.fail("Suspend line message not displayed");
		}
		return this;
	}

	/**
	 * Verify block charged international roaming toggle disable status.
	 */
	public BlockingPage verifyBlockChargedInternationRoamingToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			blockChargeToggleDisableStatus.isDisplayed();
			Reporter.log("Block charged international roaming toogle is in disabled state");
		} catch (Exception e) {
			Reporter.log("Block charged international roaming toogle is in enabled state");
			Assert.fail("Block charged international roaming toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify block international roaming toggle disable status.
	 */
	public BlockingPage verifyBlockInternationRoamingToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			blockInternationalToggleDisableStatus.isDisplayed();
			Reporter.log("Block charged international roaming toogle is in disabled state");
		} catch (Exception e) {
			Assert.fail("Block charged international roaming toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify block charged international roaming toggle disable status.
	 */
	public BlockingPage verifyBlockInstantMessageToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			blockInstantMessageToggleDisabledStatus.isDisplayed();
			Reporter.log("Block instant message toogle is in disabled state");
		} catch (Exception e) {
			Assert.fail("Block  instant message toogle is enabled");
		}
		return this;
	}

	/**
	 * Verify block charged international roaming toggle disable status.
	 */
	public BlockingPage verifyBlockTextPictureMessageToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			blockTextMessagesToggleDisabledStatus.isDisplayed();
			Reporter.log("Block text and picture messages toogle is in disabled state");
		} catch (Exception e) {
			Reporter.log("Block text and picture messages toggle is enabled");
			Assert.fail("Block text and picture messages toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify block charged international roaming toggle disable status.
	 */
	public BlockingPage verifyBlockTMOmailToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			blockTMOmailToggleDisabledStatus.isDisplayed();
			Reporter.log("Block TMO mail toogle is in disabled state");
		} catch (Exception e) {
			Reporter.log("Block TMO mail toggle is enabled");
			Assert.fail("Block TMO mail toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify device block toggle disable status.
	 */
	public BlockingPage verifyBlockDeviceToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			deviceBlockToggleDisabledStatus.isDisplayed();
			Reporter.log("Device block toggle is in disabled state");
		} catch (Exception e) {
			Assert.fail("Device block toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify scam id toggle disable status.
	 */
	public BlockingPage verifyscamIDToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			scamIDToggleDisabledStatus.isDisplayed();
			Reporter.log("Scam id toggle is in disabled state");
		} catch (Exception e) {
			Assert.fail("Scam id toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify scam block toggle disable status.
	 */
	public BlockingPage verifyscamBlockToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			scamBlockToggleDisabledStatus.isDisplayed();
			Reporter.log("Scam block toggle is in disabled state");
		} catch (Exception e) {
			Assert.fail("Scam block toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify block content downloads toggle disable status.
	 */
	public BlockingPage verifyblockContentDownloadsToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			blockContentDownloadsToggleDisabledStatus.isDisplayed();
			Reporter.log("Block content downloads toggle is in disabled state");
		} catch (Exception e) {
			Assert.fail("Block content downloads toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify block all messagesToggle toggle disable status.
	 */
	public BlockingPage verifyblockAllMessagesToggleDisableStatus() {
		try {
			waitforSpinnerinProfilePage();
			blockAllMessagesToggleDisabledStatus.isDisplayed();
			Reporter.log("Block all messages toggle is in disabled state");
		} catch (Exception e) {
			Assert.fail("Block all messages toggle is enabled");
		}
		return this;
	}

	/**
	 * Verify block international roaming toggle operations.
	 */
	public String verifyBlockInternationalRoamingToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(blockInternationalToggleOnStatus, blockInternationalToggleOffStatus,
					blockInternationalToggleDisableStatus);
			verifyToggleOpeartions(blockInternationalToggleOnStatus, blockInternationalToggleOffStatus,
					blockInternationalToggleDisableStatus, toggleStatus, blockInternationalToggle);
			return toggleStatus;
		} catch (Exception e) {
			Reporter.log("Block international roaming toggle status update failed");
			Assert.fail("Block international roaming toggle status update failed ");
			return null;
		}

	}

	/**
	 * Verify scam ID toggle operations.
	 */
	public BlockingPage verifyScamIDToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(scamIDToggleOnStatus, scamIDToggleOffStatus,
					scamIDToggleDisabledStatus);
			verifyToggleOpeartions(scamIDToggleOnStatus, scamIDToggleOffStatus, scamIDToggleDisabledStatus,
					toggleStatus, scamIDToggle);
		} catch (Exception e) {
			Reporter.log("Scam ID toggle update failed");
			Assert.fail("Scam ID toggle update failed ");
		}
		return this;
	}

	/**
	 * Verify scam block toggle operations.
	 */
	public BlockingPage verifyScamBlockToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(scamBlockToggleOnStatus, scamBlockToggleOffStatus,
					scamBlockToggleDisabledStatus);
			verifyToggleOpeartions(scamBlockToggleOnStatus, scamBlockToggleOffStatus, scamBlockToggleDisabledStatus,
					toggleStatus, scamBlockToggle);
		} catch (Exception e) {
			Reporter.log("Scam block toggle update failed");
			Assert.fail("Scam block toggle update failed ");
		}
		return this;
	}

	/**
	 * Verify block content downloads toggle operations.
	 */
	public BlockingPage verifyBlockContentDownloadsOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(blockContentDownloadsToggleOnStatus,
					blockContentDownloadsToggleOffStatus, blockContentDownloadsToggleDisabledStatus);
			verifyToggleOpeartions(blockContentDownloadsToggleOnStatus, blockContentDownloadsToggleOffStatus,
					blockContentDownloadsToggleDisabledStatus, toggleStatus, blockContentDownloadsToggle);
		} catch (Exception e) {
			Reporter.log("Block content downloads toggle update failed");
			Assert.fail("Block content downloads toggle update failed ");
		}
		return this;
	}

	/**
	 * Verify block text and picture messages toggle operations.
	 */
	public BlockingPage verifyBlockTextAndPictureMessagesToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(blockTextMessagesToggleOnStatus, blockTextMessagesToggleOffStatus,
					blockTextMessagesToggleDisabledStatus);
			verifyToggleOpeartions(blockTextMessagesToggleOnStatus, blockTextMessagesToggleOffStatus,
					blockTextMessagesToggleDisabledStatus, toggleStatus, blockTextMessagesToggle);
		} catch (Exception e) {
			Reporter.log("Block text and picture messages toggle update failed");
			Assert.fail("Block text and picture messages toggle update failed ");
		}
		return this;
	}

	/**
	 * Verify block tmo mail toggle operations.
	 */
	public BlockingPage verifyBlockTMOmailToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(blockTMOmailToggleOnStatus, blockTMOmailToggleOffStatus,
					blockTMOmailToggleDisabledStatus);
			verifyToggleOpeartions(blockTMOmailToggleOnStatus, blockTMOmailToggleOffStatus,
					blockTMOmailToggleDisabledStatus, toggleStatus, blockTMOmailToggle);
		} catch (Exception e) {
			Reporter.log("Block tmo mail toggle update failed");
			Assert.fail("Block tmo mail toggle update failed ");
		}
		return this;
	}

	/**
	 * Verify block instant messages toggle operations.
	 */
	public BlockingPage verifyBlockInstantMessageToggleOperations() {
		try {
			waitforSpinnerinProfilePage();
			String toggleStatus = getToggleStatus(blockInstantMessageToggleOnStatus, blockInstantMessageToggleOffStatus,
					blockInstantMessageToggleDisabledStatus);
			verifyToggleOpeartions(blockInstantMessageToggleOnStatus, blockInstantMessageToggleOffStatus,
					blockInstantMessageToggleDisabledStatus, toggleStatus, blockInstantMessageToggle);
		} catch (Exception e) {
			Reporter.log("Block instant messages toggle update failed");
			Assert.fail("Block instant messages toggle update failed ");
		}
		return this;
	}
}