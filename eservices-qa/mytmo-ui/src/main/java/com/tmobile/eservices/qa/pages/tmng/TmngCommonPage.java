package com.tmobile.eservices.qa.pages.tmng;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.listeners.WebEventListener;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

/**
 * @author ksrinivas
 *
 */
/**
 * @author prokarma
 *
 */
public class TmngCommonPage extends CommonPage {

	public static final Logger logger = LoggerFactory.getLogger(TmngCommonPage.class);
	private List<String> listDropdown = new ArrayList<>();
	Actions action;
	Select select;
	static JavascriptExecutor javaScript;
	EventFiringWebDriver eventDriver = null;
	WebEventListener eventListener = null;

	@FindBy(id = "overridelink")
	private WebElement btnCertificateButton;

	@FindBy(id = "android:id/button1")
	private WebElement securitycontinuebtn;

	@FindBy(id = "overridelink")
	private List<WebElement> continueToWebsiteButton;

	@FindBy(css = "i[class*='fa-phn-touch-area']")
	private WebElement contactUsImg;

	@FindBy(id = "logout-Ok")
	private WebElement logOutBtn;

	@FindBy(css = "div.TMO-HEADER-COMPONENT span")
	private WebElement backBtn;

	@FindBy(css = "img[aria-label='T-Mobile-Logo'][align='left']")
	private WebElement tmobileIcon;

	/**
	 * @param webDriver
	 */
	public TmngCommonPage(WebDriver webDriver) {
		super(webDriver);
		eventDriver = new EventFiringWebDriver(getDriver());
		eventListener = new WebEventListener(getDriver());
		eventDriver.register(eventListener);
	}

	/**
	 * 
	 * @param element
	 */
	public void elementClick(WebElement element) {
		try {
			eventListener.beforeClickOn(element, eventDriver);
			element.click();
		} catch (Exception e) {
			e.getMessage().contains("NoSuchElementException");
			Reporter.log(">>FAILED::Not found" + "'" + element.getText() + "'" + " Can not proceed further");
			Reporter.log(e.getMessage().trim());
		}
	}

	/**
	 * 
	 * @param element
	 */
	public void selectCheckbox(WebElement element) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element), 60);
			if (!element.isSelected()) {
				waitFor(ExpectedConditions.elementToBeClickable(element), 30);
				elementClick(element);
				Reporter.log("Select check box is checked");
			}
		} catch (Exception e) {
			Reporter.log("Select check box not displayed");
		}
	}

	/**
	 * Send Text Data
	 * 
	 * @param element
	 * @param data
	 */
	public void sendTextData(WebElement element, String data) {
		waitFor(ExpectedConditions.visibilityOf(element), 30);
		element.clear();
		element.sendKeys(data);
	}

	/**
	 * To Verify the Certificate of the Browser
	 */
	public void verifyCertificate() {
		waitFor(ExpectedConditions.visibilityOf(btnCertificateButton), 60);
		if (btnCertificateButton.isDisplayed()) {
			elementClick(btnCertificateButton);
		}
	}

	/**
	 * To Select the values from the drop down
	 * 
	 * @param topicListDropdown
	 * @return
	 */
	public List<String> getAllOptionsInDropdown(WebElement topicListDropdown) {
		select = new Select(topicListDropdown);
		List<WebElement> optionsList = select.getOptions();
		for (WebElement optionList : optionsList) {
			listDropdown.add(optionList.getText());
		}
		return listDropdown;
	}

	/**
	 * Select Element From The Drop Down
	 * 
	 * @param element [WebElement Of Select]
	 * @param param   [Value or Text or Index]
	 * @param data    [Element To Be Selected]
	 */
	public void selectElementFromDropDown(WebElement element, String param, String data) {
		waitFor(ExpectedConditions.visibilityOf(element), 60);
		select = new Select(element);
		switch (param) {
		case Constants.SELECT_BY_VALUE:
			select.selectByValue(data);
			break;
		case Constants.SELECT_BY_TEXT:
			select.selectByVisibleText(data);
			break;
		case Constants.SELECT_BY_INDEX:
			select.selectByIndex(Integer.parseInt(data));
			break;
		default:
			break;
		}
	}

	/**
	 * Get Selected Element
	 * 
	 * @param element
	 * @return
	 */
	public String getSelectedOption(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element), 60);
		select = new Select(element);
		return select.getFirstSelectedOption().getText();
	}

	/**
	 * @param tabName
	 * @param webElementList
	 */
	public void clickMenuLinks(final String tabName, final List<WebElement> webElementList) {
		for (WebElement link : webElementList) {
			if (tabName.equalsIgnoreCase(link.getText())) {
				elementClick(link);
				break;
			}
		}
	}

	/**
	 * @param tabName
	 * @param webElementList
	 * @return
	 */
	public boolean verifyLinksAreDisplaying(final String tabName, final List<WebElement> webElementList) {
		boolean linkText = false;
		for (WebElement link : webElementList) {
			if (tabName.equalsIgnoreCase(link.getText())) {
				link.isDisplayed();
				linkText = true;
				break;
			}
		}
		return linkText;
	}

	/**
	 * Gets the URL for current Page.
	 * 
	 * @return String
	 */
	public String currentUrl() {
		return getDriver().getCurrentUrl();
	}

	/**
	 * Switch To Window
	 */
	public void switchToWindow() {
		checkPageIsReady();
		String currentWindow = getDriver().getWindowHandle();
		for (String winHandle : getDriver().getWindowHandles()) {
			if (!winHandle.equalsIgnoreCase(currentWindow)) {
				getDriver().switchTo().window(winHandle);
				break;
			}
		}
	}

	/**
	 * Wait For Spinner Invisible
	 */
	public void waitForSpinnerInvisibility() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".fa.fa-spinner.fa-pulse")), 60);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[class*='spinner']")), 60);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("spinner")), 60);
			waitFor(ExpectedConditions
					.invisibilityOfElementLocated(By.cssSelector(".screen-lock.spinner.fa.fa-spinner.fa-pulse")), 60);
		} catch (Exception ex) {
			Assert.fail("Failed to verify that spinner is inavailable");
		}
	}

	/**
	 * Scroll the web page to specific element
	 */
	public void scrollDownToelement(WebElement Element) {
		try {
			javaScript = (JavascriptExecutor) getDriver();
			javaScript.executeScript("arguments[0].scrollIntoView();", Element);
		} catch (Exception e) {
			Assert.fail("Failed to scroll to element: " + Element);
		}
	}

	/**
	 * Scroll the web page to specific element in mobile
	 */
	public void scrollDownToelementMobile(WebElement Element) {
		try {
			javaScript = (JavascriptExecutor) getDriver();
			javaScript.executeScript("mobile:scroll", Element);
		} catch (Exception e) {
			Assert.fail("Failed to scroll to element: " + Element);
		}
	}

	/**
	 * Clicking On An Element With JavaScript
	 * 
	 * @param element
	 */
	public void clickElementWithJavaScript(WebElement element) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element), 3);
			javaScript = (JavascriptExecutor) getDriver();
			javaScript.executeScript("arguments[0].click();", element);
		} catch (TimeoutException e) {
			Assert.fail(element + "- Element not found to perform click operation ");
		}

	}

	/**
	 * This common method is used Delete all cookies in browser
	 */
	public void deleteCookies() {
		getDriver().manage().deleteAllCookies();
	}

	/**
	 * MoveToElement With Actions Class
	 * 
	 * @param element
	 */
	public void moveToElement(WebElement element) {
		try {
			action = new Actions(getDriver());
			action.moveToElement(element).build().perform();
		} catch (Exception e) {
			Assert.fail("Failed to move to web element");
		}
	}

	/**
	 * swipe down functionality with no webElement as reference
	 */

	public void scrollDownMobileIOS() {
		/*
		 * Dimension size=getDriver().manage().window().getSize(); int startX=0; int
		 * endX=0; int startY=0; int endY=0; endY=(int)(size.height*0.70);
		 * startY=(int)(size.height*0.30); startX=(size.width/2); TouchAction action =
		 * new TouchAction((io.appium.java_client.android.AndroidDriver)getDriver())
		 * .press(startX,startY) .waitAction(Duration.ofMillis(3000))
		 * .moveTo(startX,endY) .release() .perform();
		 * 
		 * Double screenHeightStart = size.getHeight() * 0.5;
		 * 
		 * int scrollStart = screenHeightStart.intValue(); Double screenHeightEnd =
		 * size.getHeight() * 0.2; int scrollEnd = screenHeightEnd.intValue();
		 * getDriver().swipe(0, scrollStart, 0, scrollEnd, 1000);
		 */
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		js.executeScript("mobile: scroll", scrollObject);

	}

	/**
	 * swipe UP functionality with no webElement as reference
	 */

	public void scrollUpMobileIOS() {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "up");
		js.executeScript("mobile: scroll", scrollObject);

	}

	/**
	 * Get Element Attribute
	 * 
	 * @param element
	 * @param attribute
	 * @return
	 */
	public String getAttribute(WebElement element, String attribute) {
		waitFor(ExpectedConditions.visibilityOf(element), 30);
		return element.getAttribute(attribute);
	}

	/**
	 * We are using this method to verifying the page load status Check Page Is
	 * Ready
	 */
	public void checkPageIsReady() {
		try {
			javaScript = (JavascriptExecutor) getDriver();
			for (int i = 0; i < 30; i++) {
				Thread.sleep(1000);
				if ("complete".equalsIgnoreCase(javaScript.executeScript("return document.readyState").toString())) {
					Thread.sleep(3000);
					break;
				}
			}
		} catch (Exception ex) {
			Reporter.log("Page is not loaded");
		}
	}

	/**
	 * Click Element By Text
	 * 
	 * @param elements
	 * @param link
	 */
	public void clickElementBytext(List<WebElement> elements, String link) {
		for (WebElement webElement : elements) {

			if (link.equalsIgnoreCase(webElement.getText())) {
				try {
					clickElementWithJavaScript(webElement);
				} catch (Exception e) {
					e.getMessage().contains("NoSuchElementException");
					Reporter.log(">>FAILED::Not found " + webElement.getText() + " can not proceed further");
					Reporter.log(e.getMessage().trim());
				}
				break;
			}
		}
	}

	/**
	 * Click Element [List Of elements]
	 * 
	 * @param elements
	 */
	public void clickElement(List<WebElement> elements) {
		waitFor(ExpectedConditions.visibilityOfAllElements(elements), 30);
		for (WebElement webElement : elements) {
			if (webElement.isDisplayed()) {
				try {
					elementClick(webElement);
				} catch (Exception e) {
					e.getMessage().contains("NoSuchElementException");
					Reporter.log(">>FAILED::Not found " + webElement.getText() + " Can not proceed further");
					Reporter.log(e.getMessage().trim());
				}
				break;
			}
		}
	}

	public boolean verifyElementBytext(List<WebElement> elements, String text) {
		boolean isElementDisplayed = false;
		for (WebElement webElement : elements) {
			if (webElement.getText().contains(text)) {
				isElementDisplayed = true;
				break;
			}
		}
		return isElementDisplayed;
	}

	public boolean verifyExactContentOfElementBytext(List<WebElement> elements, String text) {
		boolean isElementDisplayed = false;
		for (WebElement webElement : elements) {
			if (webElement.getText().equals(text)) {
				isElementDisplayed = true;
				break;
			}
		}
		return isElementDisplayed;
	}

	/**
	 * Switch To Frame [WebElement]
	 * 
	 * @param element
	 */
	public void switchToFrame(WebElement element) {
		if (getDriver() instanceof AppiumDriver) {
			getDriver().get(getAttribute(element, "src"));
		} else {
			getDriver().switchTo().frame(element);
		}

	}

	/**
	 * Switch To Default Content
	 */
	public void switchToDefaultContent() {
		getDriver().switchTo().defaultContent();
	}

	/**
	 * Navigate Back
	 */
	public void navigateBack() {
		checkPageIsReady();
		getDriver().navigate().back();
	}

	/**
	 * Click On Security Alert
	 */
	@SuppressWarnings("rawtypes")
	public void clickOnSecurityAlertAccept() {
		if (getDriver() instanceof AndroidDriver) {
			String currentContext = ((AppiumDriver) getDriver()).getContext();
			try {
				((AppiumDriver) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOf(securitycontinuebtn), 30);
				securitycontinuebtn.click();
				((AppiumDriver) getDriver()).context(currentContext);
			} catch (Exception e) {
				((AppiumDriver) getDriver()).context(currentContext);
				logger.info("Security certificate is not appeared in Android");
			}
		} else if (getDriver() instanceof AppiumDriver) {
			String currentIosContext = ((AppiumDriver) getDriver()).getContext();
			try {
				((AppiumDriver) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@label='Continue']")), 30);
				((AppiumDriver) getDriver()).findElement(By.xpath("//*[@label='Continue']")).click();
				((AppiumDriver) getDriver()).context(currentIosContext);
			} catch (Exception e) {
				((AppiumDriver) getDriver()).context(currentIosContext);
				logger.info("Security certificate is not appeared in IOS");
			}
		}
		Capabilities caps = ((RemoteWebDriver) getDriver()).getCapabilities();
		String browserName = caps.getBrowserName();
		if ("internet explorer".equalsIgnoreCase(browserName))
			checkPageIsReady();
		if (CollectionUtils.isNotEmpty(continueToWebsiteButton)
				&& !CollectionUtils.sizeIsEmpty(continueToWebsiteButton)) {
			clickElement(continueToWebsiteButton);
		}
	}

	/**
	 * Refresh A page
	 * 
	 * 
	 */
	public void refreshPage() {
		getDriver().navigate().refresh();
		Reporter.log("Page have been refreshed");
	}

	public void scrollToElement(WebElement element) {
		try {
			javaScript.executeScript(
					"arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e) {
			Assert.fail("Failed to scroll to element: " + element);
		}
	}

	public void doubleClick(WebElement element) {
		try {
			action = new Actions(getDriver()).doubleClick(element);
			action.build().perform();
		} catch (Exception e) {
			e.getMessage().contains("NoSuchElementException");
			Reporter.log(">>FAILED::Not found " + element.getText());

		}
	}

	public void acceptCertificateIE() {
		try {
			if ((getDriver() instanceof RemoteWebDriver)) {
				checkPageIsReady();
				if ("Certificate Error: Navigation Blocked".equalsIgnoreCase(getDriver().getTitle())
						&& btnCertificateButton.isDisplayed()) {
					waitFor(ExpectedConditions.visibilityOf(btnCertificateButton), 30);
					btnCertificateButton.click();
				}
			}
		} catch (Exception e) {
		}
	}

	public boolean verifyElementByText(WebElement element, String message) {
		boolean elementText = false;
		waitFor(ExpectedConditions.visibilityOf(element), 30);
		String elementTxt = element.getText();
		// highLightElement(element);
		if (elementTxt.contains(message)) {
			elementText = true;
		}
		return elementText;
	}

	/**
	 * verify Element By Text
	 * 
	 * @param elements
	 * @param link
	 * @return
	 */
	public boolean verifyListOfElementsBytext(List<WebElement> elements, String text) {

		boolean isElementDisplayed = false;
		for (WebElement webElement : elements) {
			if (webElement.getText().contains(text)) {
				isElementDisplayed = true;
				break;
			}
		}
		return isElementDisplayed;
	}

	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd");
		Calendar calobj = Calendar.getInstance();
		calobj.add(Calendar.DATE, 1);
		return dateFormat.format(calobj.getTime());

	}

	public void waitforSpinner() {
		waitFor(ExpectedConditions
				.invisibilityOfElementLocated(By.cssSelector("i.fa.fa-spin.fa-spinner.interceptor-spinner")), 30);
	}

	/**
	 * Generating random number
	 * 
	 * @return value
	 * @param elements
	 */
	public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	/**
	 * Get Text from Element [List Of elements]
	 * 
	 * @return value
	 * @param elements
	 */
	public String getTextUponDisplay(List<WebElement> elements) {
		for (WebElement webElement : elements) {
			if (webElement.isDisplayed()) {
				return webElement.getText();
			}
		}
		return StringUtils.EMPTY;
	}

	public void verifyCurrentPageURL(String pageName, String urlTextToVerify) {
		try {
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains(urlTextToVerify), pageName + " is not loaded");
			Reporter.log(pageName + " is loaded");

		} catch (Exception e) {
			Reporter.log(e.getMessage().trim());
			Reporter.log(pageName + " is not loaded");
		}
	}

	/**
	 * Click On Security Alert
	 */
	@SuppressWarnings("rawtypes")
	public void siteAttemptingPopUpAlertAccept() {
		if (getDriver() instanceof AppiumDriver) {
			String currentIosContext = ((AppiumDriver) getDriver()).getContext();
			try {
				((AppiumDriver) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@label='Allow']")), 60);
				((AppiumDriver) getDriver()).findElement(By.xpath("//*[@label='Allow']")).click();
				((AppiumDriver) getDriver()).context(currentIosContext);
			} catch (Exception e) {
				((AppiumDriver) getDriver()).context(currentIosContext);
				logger.info("Pop up is not appeared in IOS");
			}
		}
	}

	public boolean verifyElementText(WebElement element, String message) {
		boolean text = false;
		waitFor(ExpectedConditions.visibilityOf(element), 60);
		String Txt = element.getText();
		if (Txt.equalsIgnoreCase(message)) {
			text = true;
		}
		return text;
	}

	/**
	 * Click On Contact us Img
	 * 
	 * @return
	 */
	public TmngCommonPage clickOnContactUsImg() {
		checkPageIsReady();
		try {
			contactUsImg.click();
			Reporter.log("Clicked on Contact US icon");
		} catch (Exception e) {
			Reporter.log("Contact US icon is not clicked");
		}
		return this;
	}

	/**
	 * Click on back button
	 * 
	 * @return
	 */
	public TmngCommonPage clickOnBackBtn() {
		try {
			backBtn.click();
			Reporter.log("Back arrow is displayed on Review Page");
		} catch (Exception e) {
			Assert.fail("Back arrow is not displayed on Review Page");
		}
		return this;
	}

	/**
	 * Click On T-Mobile Icon
	 */
	public TmngCommonPage clickOnTMobileIcon() {
		try {
			waitFor(ExpectedConditions.visibilityOf(tmobileIcon), 60);
			tmobileIcon.click();
			Reporter.log("Clicked on T-Mobile Icon Cart page");
		} catch (Exception e) {
			Assert.fail("T-Mobile Icon is not found");
		}
		return this;
	}

	/**
	 * Check Element Is Displayed Or Not
	 * 
	 * @param element
	 * @return
	 */
	public boolean checkIsElementDisplayed(WebElement element) {
		try {
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Managing store closing time
	 * 
	 * @param element
	 * @return
	 */
	public String manageStoreClosingTime(String endTime) {
		if (endTime.contains(":")) {
			String hourStr = endTime.split(":")[0];
			int strHours = Integer.parseInt(hourStr);
			strHours = strHours + 12;
			endTime = strHours + endTime.split(":")[1];
		} else {
			int strHours = Integer.parseInt(endTime);
			strHours = strHours + 12;
			endTime = strHours + "00";
		}
		return endTime;
	}

	/**
	 * Managing store start time
	 * 
	 * @param element
	 * @return
	 */

	public String manageStoreStartTime(String startTime) {

		if (startTime.contains(":")) {
			startTime = startTime.replaceAll("[^0-9?!\\.]", "");
		} else {
			startTime = startTime + "00";
		}

		return startTime;
	}

	public boolean waitforSpinnerinMarketingCommunications() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector(".spinner-loader"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean waitforSpinnerinProfilePage() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector(".circle"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
			return true;
		} catch (Exception e) {
			Reporter.log("Spinner reached to inifinite state");
			Assert.fail("Spinner reached to inifinite state " + e.getMessage());
			return false;
		}

	}

	public boolean waitforSpinnerinTMobileIDPage() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector("img.spinner"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Click On Element
	 * 
	 * @param element
	 */
	public void clickElement(WebElement element) {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(element), 60);
			elementClick(element);
		} catch (Exception e) {
			if (e.getMessage().contains("NoSuchElementException"))
				Reporter.log(">>FAILED::Not found " + "'" + element.getText() + "'");
		}
	}

	/**
	 * Check Element Is Displayed Or Not
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementDisplayed(WebElement element) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element), 30);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String changeDateFormate(String date) {
		String mdy = null;
		try {
			SimpleDateFormat outFormat = new SimpleDateFormat("MMM d, yyyy");
			SimpleDateFormat inFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date dateString = inFormat.parse(date);
			// System.out.println(date);
			mdy = outFormat.format(dateString);
			// System.out.println("mdy" + mdy);
		} catch (Exception ex) {
		}
		return mdy;
	}

	/**
	 * Wait For Spinner
	 * 
	 * @return
	 */
	public boolean waitForNewSpinner() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector("div.circle-clipper.right div.circle"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * verify login page title
	 * 
	 * @return boolean
	 */
	public TmngCommonPage verifyCurrentPageTitle(String title) {
		checkPageIsReady();
		Assert.assertTrue(getDriver().getTitle().contains(title), " " + title + " is not displayed");
		Reporter.log("" + title + "is displayed");
		return this;
	}

	public TmngCommonPage clickOnlogOutBtn() {
		logOutBtn.click();
		Reporter.log("Clicked on logOut button");
		return this;
	}

	/**
	 * Navigate to previous page.
	 */
	public TmngCommonPage navigateToPreviousPage() {
		waitforSpinnerinProfilePage();
		try {
			getDriver().navigate().back();
			Reporter.log("Navigation to previous page is success");
		} catch (Exception e) {
			Assert.fail("Navigation to previous page is success" + e.toString());
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TmngCommonPage verifyPageUrl(String pageUrl) {
		try {
			waitFor(ExpectedConditions.urlContains(pageUrl), 60);
		} catch (Exception e) {
			Reporter.log("URL containing " + pageUrl + " page not loaded " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify toggle status.
	 */
	public String getToggleStatus(WebElement element1, WebElement element2, WebElement element3) {
		waitforSpinnerinProfilePage();
		if (isElementDisplayed(element1)) {
			return "ON";
		}
		if (isElementDisplayed(element2)) {
			return "OFF";
		}
		if (isElementDisplayed(element3)) {
			return "DISABLED";
		}
		return null;
	}

	/**
	 * Verify toggle operations.
	 */
	public void verifyToggleOpeartions(WebElement element1, WebElement element2, WebElement element3,
			String toggleStatus, WebElement toggle) {
		switch (toggleStatus) {
		case "ON":
			toggle.click();
			waitforSpinnerinProfilePage();
			String toggleStatus2 = getToggleStatus(element1, element2, element3);
			Assert.assertNotEquals(toggleStatus, toggleStatus2, "Status not updated");
			Reporter.log("Toggle is turned OFF and status is updated successfully");
			break;
		case "OFF":
			toggle.click();
			waitforSpinnerinProfilePage();
			String toggleStatus3 = getToggleStatus(element1, element2, element3);
			Assert.assertNotEquals(toggleStatus, toggleStatus3, "Status not updated");
			Reporter.log("Toggle is turned ON and status is updated successfully");
			break;
		case "DISABLED":
			Reporter.log("Toggle is in disabled state");
		}
	}

	/**
	 * Compare the pii Name and its Id Sequence Number
	 * 
	 * @param pid        list Element
	 * @param pii        Masking Element
	 * @param true/false
	 */
	public boolean comparePidandIndex(WebElement pidAttribute, String pidName) {
		boolean bFlag = false;
		try {

			if (pidAttribute.getAttribute("pid").contains(pidName) && Character
					.isDigit(pidAttribute.getAttribute("pid").charAt(pidAttribute.getAttribute("pid").length() - 1))) {
				bFlag = true;
			}
		} catch (Exception e) {
			Assert.fail("PII element verfitcaion fail to validate");
		}
		return bFlag;
	}

	/**
	 * Check PII Masking for all personal and payment information
	 * 
	 * @param pidElement
	 * @param piiMasking
	 * @return true/false
	 */
	public boolean checkElementisPIIMasked(WebElement pidElement, String piiMasking) {
		boolean isMasked = false;
		try {
			String pidAttr = pidElement.getAttribute("pid");
			if (!pidAttr.isEmpty() && pidAttr.contains(piiMasking) && pidAttr.matches(piiMasking + "\\d+")) {
				isMasked = true;
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking");
		}
		return isMasked;
	}

	/**
	 * Waits for specific amount of time, until the element is visible
	 * 
	 * @param element          WebElement is passed as argument
	 * @param timeOutInSeconds Specific time can be passed to this method
	 */

	public void explicitWait(WebElement element, int timeOutInSeconds) {
		try {
			new WebDriverWait(getDriver(), timeOutInSeconds).until((ExpectedConditions.visibilityOf(element)));
		} catch (Exception e) {
			Reporter.log("Page Loading is not completed");
		}
	}

	/***
	 * get price value
	 * 
	 */
	public String getPriceValue(WebElement element) {
		String priceValue = null;
		String tradeInHeaderText = element.getText();

		String[] trimmedText = tradeInHeaderText.split(" ");

		for (String value : trimmedText) {
			if (value.contains("$")) {
				priceValue = value;
				break;
			}
		}
		return priceValue.trim();
	}

	/**
	 * Element Focus
	 * 
	 * @param element
	 */
	public boolean elementFocus(WebElement element) {
		boolean Flag = false;
		try {
			action.contextClick(element);
			Flag = true;
			Reporter.log("Cursor focus on the Field");
		} catch (Exception e) {
			e.getMessage().contains("NoSuchElementException");
			Reporter.log(">>FAILED::Not found " + element.getText());
			Flag = false;
			Reporter.log("Cursor not focus on the Field");
		}
		return Flag;
	}

	public void visualMapping(WebElement elem, String name) {
		try {
			String s = elem.toString();
			String s1 = s.substring(s.lastIndexOf(":") + 2, s.length() - 1);
//			System.out.println(elem);
//			System.out.println(s1);
			javaScript = (JavascriptExecutor) getDriver();
			javaScript.executeScript("function labelElement(selector, text){\r\n"
					+ "    const $el = document.querySelectorAll(selector)[0];\r\n"
					+ "    $el.style.cssText = \"border: 2px solid #ff0000; position: relative;\";\r\n"
					+ "    const $textNode =  document.createElement('sdet');\r\n"
					+ "    $textNode.innerHTML = text;\r\n"
					+ "    $textNode.style.cssText = \"border: 1px solid #000; font-size: 12px; position: absolute; bottom: 0; right: 0; color: #000; background-color: #ffff00\";\r\n"
					+ "    $el.appendChild($textNode);\r\n" + "}\r\n" + " labelElement('" + s1 + "', '" + name + "');");
		} catch (Exception e) {
			Assert.fail("Not able to map element");
		}
	}

	/**
	 * Set Geo Location in cartPage
	 *
	 */

	public void setLocation() {
		if (getDriver() instanceof AppiumDriver) {
			Reporter.log("Location for Mobile device is disabled");
		} else {
			JavascriptExecutor js = (JavascriptExecutor) getDriver();
			js.executeScript(
					"window.navigator.geolocation.getCurrentPosition = function(success){ var position = {coords : {  latitude : 47.672049,  longitude : -122.1706612 }  };  success(position);}");
			/*
			 * DesiredCapabilities caps = new DesiredCapabilities(); ChromeOptions options =
			 * new ChromeOptions(); HashMap<String, Object> prefs = new HashMap<String,
			 * Object>(); prefs.put("profile.default_content_setting_values.geolocation",
			 * 1); // 1:allow; // 2:block options.setExperimentalOption("prefs", prefs);
			 * caps.setCapability(ChromeOptions.CAPABILITY, options); ((LocationContext) new
			 * Augmenter().augment(getDriver())) .setLocation(new Location(47.672049,
			 * -122.1706612, 0));
			 */
			Reporter.log("Location is set");
		}
	}

	/**
	 * Send Keys with small wait in case if some logic happens on UI and small wait
	 * needed
	 */
	public void sendKeysWithWait(WebElement inputField, String string) {
		try {
			for (int i = 0; i < string.length(); i++) {
				Thread.sleep(800);
				char c = string.charAt(i);
				String s = new StringBuilder().append(c).toString();
				inputField.sendKeys(s);
			}
		} catch (Exception e) {
			Assert.fail("Failed to send chars with wait");
		}
	}

	/**
	 * Verify that element is displayed on the page
	 * 
	 * @param w
	 * @return
	 */
	public WebElement getelement(WebElement w) {
		try {
			w.isDisplayed();
		} catch (Exception e) {
			return null;
		}
		return w;
	}

}