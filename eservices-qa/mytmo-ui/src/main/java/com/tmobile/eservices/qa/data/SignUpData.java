package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

/**
 * 
 * @author pshiva
 *
 */
public class SignUpData {

	@Data(name = "FirstName")
	private String firstName;

	@Data(name = "LastName")
	private String lastName;

	@Data(name = "PhoneNumber")
	private String phoneNumber;

	@Data(name = "Email")
	private String emailId;

	@Data(name = "Password")
	private String password;

	@Data(name = "SecurityQuestionAns1")
	private String securityQuestionAns1;

	@Data(name = "SecurityQuestionAns2")
	private String securityQuestionAns2;

	@Data(name = "SecurityQuestionAns3")
	private String securityQuestionAns3;

	@Data(name = "AccountZipCode")
	private String zipCode;


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the securityQuestionAns1
	 */
	public String getSecurityQuestionAns1() {
		return securityQuestionAns1;
	}

	/**
	 * @return the securityQuestionAns2
	 */
	public String getSecurityQuestionAns2() {
		return securityQuestionAns2;
	}

	/**
	 * @return the securityQuestionAns3
	 */
	public String getSecurityQuestionAns3() {
		return securityQuestionAns3;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	@Override
	public String toString() {
		return "SignUpData [" + (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (lastName != null ? "lastName=" + lastName + ", " : "")
				+ (phoneNumber != null ? "phoneNumber=" + phoneNumber + ", " : "")
				+ (emailId != null ? "emailId=" + emailId + ", " : "")
				+ (password != null ? "password=" + password + ", " : "")
				+ (securityQuestionAns1 != null ? "securityQuestionAns1=" + securityQuestionAns1 + ", " : "")
				+ (securityQuestionAns2 != null ? "securityQuestionAns2=" + securityQuestionAns2 + ", " : "")
				+ (securityQuestionAns3 != null ? "securityQuestionAns3=" + securityQuestionAns3 + ", " : "")
				+ (zipCode != null ? "zipCode=" + zipCode : "") + "]";
	}
}
