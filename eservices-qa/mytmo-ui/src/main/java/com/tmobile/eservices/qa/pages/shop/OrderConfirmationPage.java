package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class OrderConfirmationPage extends CommonPage {

	@FindBy(xpath = "//button[contains(text(), 'Accept and continue')]")
	private WebElement orderConfirmationPage;

	@FindBy(xpath = "//button[contains(text(), 'Accept and continue')]")
	private WebElement learnMoreLink;

	@FindBy(css = "div.row.padding-bottom-large.text-center h4")
	private WebElement orderConfirmationHeading;

	@FindBy(css = "p.no-padding.button-title-acacac")
	private WebElement orderNumberOld;

	@FindBy(css = "span[ng-bind*='contentData.shipDateTxt']")
	private WebElement estimatedShipDate;

	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement drpLegalText;

	@FindBy(css = "span.col-xs-12.body-copy-highlight")
	private WebElement monthlyCost;

	@FindBy(css = "span.col-xs-12.body-copy-highlight")
	private WebElement emailNotification;

	@FindBy(css = "Button.TrackYourOrder")
	private WebElement trackYourOrderButton;

	@FindBy(xpath = "//p[contains(text(),'ORDER DETAILS')]")
	private WebElement orderDetails;

	@FindBy(css = "HomeButton")
	private WebElement homeButton;
	
	@FindBy(css = "a[href*='accounthistory']")
	private WebElement accountHistoryLink;
	
	@FindBy(css = "[ng-click*='$ctrl.trackClick()']")
	private WebElement orderStatusButton;
	
	@FindBy(css ="h2.h3-title")
	private WebElement thankYouText;
	
	@FindBy(css = "div[class*='col-xs-12 col-md-12'] img")
	private WebElement thankYouImage;
	
	@FindBy(css = "div.text-transform-none div:not(.wordWrap) p.body-copy-highlight")
	private WebElement orderNumber;
	
	@FindBy(css = "button[ng-click*='ctrl.shopForAccessoriesCTA()']")
	private WebElement shopAccessoryButton;
	
	@FindBy(css = "button[ng-click*='ctrl.continueShoppingCTA()']")
	private WebElement continueShoppingButton;
	
	@FindBy(css = ".pii_mask_data")
	private WebElement email;
	
	@FindBy(xpath = "//p[contains(text(),'Transfer your number')]")
	private WebElement transferNumberHeader;
	
	@FindBy(xpath = "//p[contains(text(),'Transfer your number')]/../div/img")
	private WebElement transferNumberimage;
	
	@FindBy(xpath = "//p[contains(text(),'Transfer your number')]/../div/p")
	private WebElement transferNumberText;
	
	@FindBy(css = "p[ng-if*='ctrl.estimatedShipDateTo']")
	private WebElement estdShipping;

	@FindBy(css = "[ng-if*='ctrl.isTradeInSectionEnabled()'] p[class*='text-center']")
	private WebElement tradeInText;
	
	@FindBy(xpath = "//p[contains(text(),'receive an email')]")
	private WebElement receiveEmailOption;
	
	@FindBy(xpath = "//p[contains(text(),'receive an email')]")
	private WebElement rememberToSendOption;
	
	@FindBy(xpath = "//p[contains(text(),'receive an email')]")
	private WebElement forgetToTurnOffOption;
	
	@FindBy(css = "a[ng-click*=\"$ctrl.setPdlEventObj('Home')\"]")
	private WebElement tmobileIcon;
	
	public OrderConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Order Confirmation Page
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyOrderConfirmationPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("Order Confirmation Page is not Dispalyed");
		}

		return this;

	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public OrderConfirmationPage verifyPageUrl() {
		waitforSpinner();
		checkPageIsReady();
		try {
			getDriver().getCurrentUrl().contains("confirmation");
			Reporter.log("Order confirmation page URL is displyed");
		} catch (Exception e) {
			Assert.fail("Order conformation page URL is not displyed");
		}
		return this;
	}
	
	
	/**
	 * Verify Account History Hyperlink Not Present with FRP
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyAccountHistoryLinkNotPresent() {
		try {
			Assert.assertEquals(0, getDriver().findElements(By.cssSelector("a[href*='accounthistory']")).size());
			Reporter.log("Account History Hyperlink is not displayed");
		} catch (Exception e) {
			Assert.fail("Account History Hyperlink is displayed");
		}
		return this;
	}
	
	
	/**
	 * Verify Account History Hyperlink Present with EIP Without Trade In
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyAccountHistoryLinkPresent() {
		try {
			Assert.assertTrue(accountHistoryLink.isDisplayed(), "Account History Hyperlink is not displayed");
			Reporter.log("Account History Hyperlink is displayed");
		} catch (Exception e) {
			Assert.fail("Account History Hyperlink is not displayed");
		}
		return this;
	}
	
	
	
	/**
	 * Click Account History Hyperlink Present with EIP Without Trade In
	 * 
	 * @return
	 */
	public OrderConfirmationPage clickAccountHistoryLink() {
		try {
			accountHistoryLink.click();
			Reporter.log("Account History Hyperlink is Clicked");
		} catch (Exception e) {
			Assert.fail("Account History Hyperlink is not Clicked");
		}
		return this;
	}

	/**
	 * Click Order Status Button
	 * 
	 * @return
	 */
	public OrderConfirmationPage clickOrderStatus() {
		try {
			clickElementWithJavaScript(orderStatusButton);
			Reporter.log("Order Status Button is Clicked");
		} catch (Exception e) {
			Assert.fail("Order Status Button is not Clicked");
		}
		return this;
	}
	
	/**
	 * Verify Thank you text 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyThankYouText() {
		waitforSpinner();
		try {
			Assert.assertTrue(isElementDisplayed(thankYouText), "Thank you text is not displayed");
			Reporter.log("Thank you text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Thank you text");
		}
		return this;
	}
	
	/**
	 * Verify Thank you Image 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyThankYouImage() {
		try {
			Assert.assertTrue(isElementDisplayed(thankYouImage), "Thank you image is not displayed");
			Reporter.log("Thank you image is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Thank you image");
		}
		return this;
	}
	
	/**
	 * Verify Order details section 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyOrderDetailsSection() {
		try {
			Assert.assertTrue(isElementDisplayed(orderDetails), "Order details section is not displayed");
			Reporter.log("Order details section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Order details section");
		}
		return this;
	}

	
	/**
	 * Verify Transfer number section 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyTarnsferNumberSection() {
		try {
			Assert.assertTrue(isElementDisplayed(transferNumberHeader), "Transfer number section is not displayed");
			Reporter.log("Transfer number section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Transfer number section");
		}
		return this;
	}
	
	/**
	 * Verify order number section 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyOrderNumber() {
		try {
			waitFor(ExpectedConditions.visibilityOf(orderNumber),30);
			Assert.assertTrue(isElementDisplayed(orderNumber), "Order number section is not displayed");
			Reporter.log("order number section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display order number section");
		}
		return this;
	}
	
	/**
	 * Verify order number section 
	 * 
	 * @return
	 */
	public OrderConfirmationPage getOrderNumber() {
		try {
			String order = orderNumber.getText();
			Reporter.log("order number =" + order );
		} catch (Exception e) {
			Assert.fail("Failed to display order number section");
		}
		return this;
	}
	
	
	/**
	 * Verify order email section 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyOrderEmail() {
		try {
			Assert.assertTrue(isElementDisplayed(email), "Order email section is not displayed");
			Reporter.log("order email section is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display order email section");
		}
		return this;
	}
	
	/**
	 * Verify Transfer Image section 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyTransferImage() {
		try {
			Assert.assertTrue(isElementDisplayed(transferNumberimage), "Transfer Image is not displayed");
			Reporter.log("Transfer Image is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Transfer Image");
		}
		return this;
	}
	
	/**
	 * Verify Transfer Image section 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyTransferText() {
		try {
			Assert.assertTrue(isElementDisplayed(transferNumberText), "Transfer Image is not displayed");
			Reporter.log("Transfer Image is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Transfer Image");
		}
		return this;
	}
	
	/**
	 * Verify Shop For Accessories CTA 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyShopForAccessory() {
		try {
			Assert.assertTrue(isElementDisplayed(shopAccessoryButton), "Shop For Accessories CTA is not displayed");
			Reporter.log("Shop For Accessories CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Shop For Accessories CTA");
		}
		return this;
	}
	
	/**
	 * Verify Continue to shopping CTA 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyContinueToShopping() {
		try {
			Assert.assertTrue(isElementDisplayed(continueShoppingButton), "Continue to shopping CTA is not displayed");
			Reporter.log("Continue to shopping CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Continue to shopping CTA");
		}
		return this;
	}
	
	/**
	 * Click Continue to shopping CTA 
	 * 
	 * @return
	 */
	public OrderConfirmationPage clickContinueToShopping() {
		waitforSpinner();
		try {
			moveToElement(continueShoppingButton);
			clickElementWithJavaScript(continueShoppingButton);
			Reporter.log("Continue to shopping CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Continue to shopping CTA");
		}
		return this;
	}
	
	/**
	 * Click Shop For Accessories CTA 
	 * 
	 * @return
	 */
	public OrderConfirmationPage clickShopAccessoriesCTA() {
		try {
			clickElementWithJavaScript(shopAccessoryButton);
			Reporter.log("Shop For Accessories CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Shop For Accessories CTA");
		}
		return this;
	}
	
	/**
	 * Verify Continue to shopping CTA 
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyShippingDate() {
		try {
			Assert.assertTrue(isElementDisplayed(estdShipping), "Continue to shipping date is not displayed");
			Reporter.log("Continue toshipping date  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Continue to shipping date ");
		}
		return this;
	}
	
	/**
	 * Verify Trade in text
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyTradeInText() {
		try {
			Assert.assertTrue(isElementDisplayed(tradeInText), "Trade in text is not displayed");
			Reporter.log("Trade in text  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Trade in text");
		}
		return this;
	}
	
	/**
	 * Verify Receive Email Option
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyReceiveEmailOption() {
		try {
			Assert.assertTrue(isElementDisplayed(receiveEmailOption), "Receive Email Option is not displayed");
			Reporter.log("Receive Email Option  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Receive Email Option");
		}
		return this;
	}
	
	/**
	 * Verify Remember To send Option
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyRememberToSend() {
		try {
			Assert.assertTrue(isElementDisplayed(rememberToSendOption), "Remember To send Option is not displayed");
			Reporter.log("Remember To send Option  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Remember To send Option");
		}
		return this;
	}
	
	/**
	 * Verify Don't forget to turn Off Option
	 * 
	 * @return
	 */
	public OrderConfirmationPage verifyforgetToTurnOffOption() {
		try {
			Assert.assertTrue(isElementDisplayed(forgetToTurnOffOption), "Don't forget to turn Off Option is not displayed");
			Reporter.log("Don't forget to turn Off Option  is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Don't forget to turn Off Option");
		}
		return this;
	}
	
	
	/**
	 * Click on tmobile Icon 
	 * 
	 * @return
	 */
	public OrderConfirmationPage clickTmobileIcon() {
		try {
			clickElementWithJavaScript(tmobileIcon);
			Reporter.log("tmobile Icon is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click tmobile Icon");
		}
		return this;
	}
	
	/**
	 * Verify PID for EMail
	 * 
	 * @param custMailPID
	 */
	public void verifyPIIMaskingForEMail(String custMailPID) {
		try {
				Assert.assertTrue(checkElementisPIIMasked(email, custMailPID),
						"No PII masking for Customer Email");
				Reporter.log("Verified PII masking for Email: " + email);
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Email");
		}
	}
	
}
