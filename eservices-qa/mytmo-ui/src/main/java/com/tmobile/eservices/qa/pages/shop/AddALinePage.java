package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class AddALinePage extends CommonPage {

	public static final String ADD_A_LINE_URL = "add-a-line";

	@FindBy(css = "[onclick='customModalClose2()']")
	private WebElement aalModalWindowCloseBtn;

	public AddALinePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Add A Line Page
	 * 
	 * @return
	 */
	public AddALinePage verifyAddALinePage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyDuplicatedElements(this.getClass());
			verifyPageUrl();
			Reporter.log("Add a line page displayed");
		} catch (Exception e) {
			Assert.fail("Add a line page not displayed");

		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AddALinePage verifyPageUrl() {
		try{
			getDriver().getCurrentUrl().contains("add-a-line");
		}catch(Exception e){
			Assert.fail("Add-a-line page is not displayed");
		}
		return this;
	}
	
	/**
	 * Click modal window close btn.
	 */
	public AddALinePage clickModalWindowCloseBtn() {
		try {
			waitforSpinner();
			if(isElementDisplayed(aalModalWindowCloseBtn))
			{
				aalModalWindowCloseBtn.click();
			}
		} catch (Exception e) {
			Assert.fail("Click on modall window close btn failed");
		}
		return this;
	}
	

	
	
	
	
}