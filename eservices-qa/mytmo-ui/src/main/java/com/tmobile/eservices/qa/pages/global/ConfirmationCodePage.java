package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author pshiva
 *
 */
public class ConfirmationCodePage extends CommonPage {

	public static final String confirmationCodePageUrl = "";
	public static final String confirmationCodePageLoadText = "";

	@FindBy(id = "confirmtext")
	private WebElement confirmtext;

	@FindBy(css = "input[type='submit']")
	private WebElement nextButton;

	@FindBy(linkText = "Resend code")
	private WebElement resendCode;

	@FindBy(css = ".ui_subhead.ui_darkbody_bold.noWrap.ng-binding")
	private WebElement verifyBan;

	/**
	 * 
	 * @param webDriver
	 */
	public ConfirmationCodePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * To Enter the Confirmation Text
	 * 
	 * @param value
	 */
	public ConfirmationCodePage enterConfirmationPin(String value) {
		try {
			confirmtext.sendKeys(value);
			Reporter.log("Successfully sent Confirmation Code.");
		} catch (NoSuchElementException e) {
			Assert.fail("Sending Confirmation Code was unsuccessful.");
		}
		return this;
	}

	/**
	 * click resend code link
	 */
	public ConfirmationCodePage clickResendCodeLink() {
		try {
			resendCode.click();
			Reporter.log("Clicked on resend code link");
		} catch (Exception e) {
			Reporter.log("Click on resend code link failed");
		}
		resendCode.click();
		return this;
	}

	/**
	 * To Click Next button
	 */
	public ConfirmationCodePage clickNextButton() {
		try {
			nextButton.click();
			Reporter.log("Clicked on next button");
		} catch (Exception e) {
			Reporter.log("Click on next button failed");
		}
		return this;
	}

	/**
	 * This method is verify the given input and same in verification page
	 * 
	 * @return
	 */
	public ConfirmationCodePage verifyBan() {
		try {
			verifyBan.getText().replaceAll("[^0-9]", "");
			Reporter.log("Successfully Retrieved text from ban");
		} catch (Exception e) {
			Reporter.log("Retrieving text from ban was unsuccessfull");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Confirmation Code Page class instance.
	 */

	/**
	 * Verify ConfirmationCode Page.
	 *
	 * @return the ConfirmationCode Page class instance.
	 */
	public ConfirmationCodePage verifyConfirmationCodePage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl();
			Reporter.log("Confirmation code page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Confirmation code page not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Confirmation Code page class instance.
	 */
	public ConfirmationCodePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(confirmationCodePageUrl));
		return this;
	}
}
