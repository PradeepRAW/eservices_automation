package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class YextApi extends ApiCommonLib {
	String version="20180910";
	String accountId="650894";
	String pageToken="ADhxRyewhNR6bZ6ioUzCcwA6YiMFk2wcXrAK69kIuqImn1NTJAI5fVnk-jOjos-M6sHW1YmmryoNhHZLlW4J7QiYiQPTzqUGESW3NpQnlfTH_W5oiFqoEsJe5tIScwgn";
	String AccessToken="7qd7GJmJZiC5y7mP8Puq1WxLmDcM";
	String apiKey="ee058dd717fedb19132c289c91a9fdc7";
	/***
	 * Gets the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response listStores(String storeId,String limit) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader();
		RestService service = new RestService(" ", yext_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		reqSpec.addQueryParam("v", version);
		reqSpec.addQueryParam("api_key", apiKey);
		reqSpec.addQueryParam("limit", limit);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v2/accounts/"+accountId+"/locations/"+storeId, RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	/***
	 * Gets the Stores entitiesList:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response entitiesList(String addressFilter) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader();
		RestService service = new RestService("", yext_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		reqSpec.addQueryParam("v", version);
		reqSpec.addQueryParam("resolvePlaceholders", true);
		//reqSpec.addQueryParam("pageToken", pageToken);
		reqSpec.addQueryParam("filter", addressFilter);
		reqSpec.addQueryParam("api_key", apiKey);
		//reqSpec.addQueryParam("limit", "50");
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v2/accounts/"+accountId+"/entities", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}
	private Map<String, String> buildGetStoreLocatorHeader() throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		return headers;
	}

}
