package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;
/**
 * 
 * @author rprakash
 *
 */
public class StoreLocator extends CommonPage {

	private final String pageUrl = "store-locator";

	@FindBy(css = "div.logo.t-mobile")
	private WebElement pageHeader;
	
	@FindBy(css = "input#storeSearchField")
	private WebElement findAStore;
	

	public StoreLocator(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public StoreLocator verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public StoreLocator verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Store Locator Page is loaded.");
		} catch (Exception e) {
			Assert.fail("Store Locator page is not loaded");
		}
		return this;
	}
	
	
	
	/**
	 *
	 * Verify that the Search Text Box is loaded completely.
	 */
	public void verifySearchTextBox() {
		try {
			findAStore.isDisplayed();
			Reporter.log("Search Text Box displayed in Store Locator Page");
		} catch (Exception e) {
			Assert.fail("Search Text Box is not found in Store Locator Page");
		}
		
	}

}
