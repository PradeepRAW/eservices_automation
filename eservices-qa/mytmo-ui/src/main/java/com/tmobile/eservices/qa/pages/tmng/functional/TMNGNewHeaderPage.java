package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.UNAVCommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * This page contains all TMNG New UNAV Header methods
 * 
 * @author sputti2
 *
 */
public class TMNGNewHeaderPage extends UNAVCommonPage {

	/**
	 * 
	 * @param webDriver
	 */
	public TMNGNewHeaderPage(WebDriver webDriver) {
		super(webDriver);
	}

	// Verify TMNG New UNAV Header
	/**
	 * Click tmobile icon.
	 */
	public TMNGNewHeaderPage clickTMobileIcon() {
		try {
			tMobileIcon.click();
		} catch (Exception e) {
			Reporter.log("Click on tmobile icon failed");
			Assert.fail("Click on tmobile icon failed");
		}
		return this;
	}

	/**
	 * Verify Plans Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyHeaderPlansLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink0));
			Assert.assertTrue(navLink0.getText().equalsIgnoreCase(Constants.UNAV_HEADER_PLANS));
			Reporter.log(navLink0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Plans Link
	 */
	public TMNGNewHeaderPage clickHeaderPlansLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink0), 60);
			clickElement(navLink0);
			Reporter.log("Clicked on Plans link");
		} catch (Exception e) {
			Reporter.log("Click on Plansl link failed");
			Assert.fail("Plans link not found");
		}
		return this;
	}

	/**
	 * Verify Plans page.
	 *
	 * @return the PlansPage class instance.
	 */
	public TMNGNewHeaderPage verifyHeaderPlansPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_PLANS_LINK);
			Reporter.log("Plans page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Plans page not displayed");
		}
		return this;
	}

	/**
	 * Verify Phones Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyPhonesLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink1));
			Reporter.log("Phone tab is displayed");
		} catch (Exception e) {
			Assert.fail("Phone tab not displayed");
		}
		return this;
	}

	/**
	 * Click Phones Link
	 */
	public TMNGNewHeaderPage clickPhonesLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink1), 60);
			navLink1.click();
			Reporter.log("Clicked on Phones menu");
		} catch (Exception e) {
			Reporter.log("Click on Phones menu failed");
			Assert.fail("Click on Phones menu failed");
		}
		return this;
	}

	/**
	 * Verify Phones page.
	 *
	 * @return the PhonesPage class instance.
	 */
	public TMNGNewHeaderPage verifyPhonesPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_PHONES_LINK);
			Reporter.log("Phones page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Phones page not displayed");
		}
		return this;
	}

	/**
	 * Verify Deals Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyDealsLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink2));
			Assert.assertTrue(navLink2.getText().equalsIgnoreCase(Constants.UNAV_HEADER_DEALS));
			Reporter.log(navLink2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Deals Link
	 */
	public TMNGNewHeaderPage clickDealsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2), 60);
			navLink2.click();
			Reporter.log("Clicked on Deals menu");
		} catch (Exception e) {
			Reporter.log("Click on Deals menu failed");
			Assert.fail("Click on Deals menu failed");
		}
		return this;
	}

	/**
	 * Verify Deals page.
	 *
	 * @return the Deals class instance.
	 */
	public TMNGNewHeaderPage verifyDealsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_DEALS_LINK);
			Reporter.log("Account page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Account page not displayed");
		}
		return this;
	}

	/**
	 * Verify Coverage Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyCoverageLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink3));
			Assert.assertTrue(navLink3.getText().equalsIgnoreCase(Constants.UNAV_HEADER_COVERAGE));
			Reporter.log(navLink3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Coverage link
	 */
	public TMNGNewHeaderPage clickCoverageLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink3), 60);
			clickElement(navLink3);
			Reporter.log("Clicked on Coverage menu");
		} catch (Exception e) {
			Reporter.log("Click on Coverage menu failed");
			Assert.fail("Click on Coverage menu failed");
		}
		return this;
	}

	/**
	 * Verify Coverage page.
	 *
	 * @return the Coverage Page class instance.
	 */
	public TMNGNewHeaderPage verifyCoveragePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_COVERAGE_LINK);
			Reporter.log("Coverage page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Coverage page not displayed");
		}
		return this;
	}

	/**
	 * Verify Benefits & More Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyBenefitsAndMoreLabel() {
		try {
			Assert.assertTrue(isElementDisplayed(navLink4));
			Assert.assertTrue(navLink4.getText().equalsIgnoreCase(Constants.UNAV_HEADER_BENEFITS));
			Reporter.log(navLink4.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navLink4.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Benefits & More link
	 */
	public TMNGNewHeaderPage clickBenefitsAndMoreLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink4), 60);
			clickElement(navLink4);
			Reporter.log("Clicked on Benefits & More menu");
		} catch (Exception e) {
			Reporter.log("Click on Benefits & More menu failed");
			Assert.fail("Click on Benefits & More menu failed");
		}
		return this;
	}

	/**
	 * Verify Benefits & More page.
	 *
	 * @return the Benefits & More Page class instance.
	 */
	public TMNGNewHeaderPage verifyBenefitsAndMorePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_BENEFITS_LINK);
			Reporter.log("Benefits & More page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Benefits & More page not displayed");
		}
		return this;
	}

	// Verify Plans Menu Options
	/**
	 * Move to plans menu and verify Magenta Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyMagentaLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink0), 60);
			moveToElement(navLink0);
			Assert.assertTrue(isElementDisplayed(navItem_0_0));
			Assert.assertTrue(navItem_0_0.getText().equalsIgnoreCase(Constants.UNAV_HEADER_MAGENTA));
			Reporter.log(navItem_0_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_0_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Magenta link
	 */
	public TMNGNewHeaderPage clickMagentalink() {
		try {
			clickElement(navItem_0_0);
			Reporter.log("Clicked on Magenta Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Magenta Link Option failed");
			Assert.fail("Click on Magenta Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Magenta page.
	 *
	 * @return the Magenta Page class instance.
	 */
	public TMNGNewHeaderPage verifyMagentaPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_MAGENTA_LINK);
			Reporter.log("Magenta Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Magenta Page not displayed");
		}
		return this;
	}

	/**
	 * Move to plans menu and verify Magenta Plus Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyMagentaPlusLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink0), 60);
			moveToElement(navLink0);
			Assert.assertTrue(isElementDisplayed(navItem_0_1));
			Assert.assertTrue(navItem_0_1.getText().equalsIgnoreCase(Constants.UNAV_HEADER_MAGENTA_PLUS));
			Reporter.log(navItem_0_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_0_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify Magenta Plus Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickMagentaPluslink() {
		try {
			clickElement(navItem_0_1);
			Reporter.log("Clicked on Magenta Plus Option");
		} catch (Exception e) {
			Reporter.log("Click on Magenta Plus Option failed");
			Assert.fail("Click on Magenta Plus Option failed");
		}
		return this;
	}

	/**
	 * Verify Magenta Plus page.
	 *
	 * @return the Magenta Plus class instance.
	 */
	public TMNGNewHeaderPage verifyMagentaPlusPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_MAGENTA_PLUS_LINK);
			Reporter.log("Magenta Plus Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Magenta Plus Page not displayed");
		}
		return this;
	}

	/**
	 * Move to plans menu and verify Essentials Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyEssentialsLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink0), 60);
			moveToElement(navLink0);
			Assert.assertTrue(isElementDisplayed(navItem_0_2));
			Assert.assertTrue(navItem_0_2.getText().equalsIgnoreCase(Constants.UNAV_HEADER_ESSENT));
			Reporter.log(navItem_0_2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_0_2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify Essentials link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickEssentialslink() {
		try {
			clickElement(navItem_0_2);
			Reporter.log("Clicked on Essentials Option");
		} catch (Exception e) {
			Reporter.log("Click on Essentials Option failed");
			Assert.fail("Click on Essentials Option failed");
		}
		return this;
	}

	/**
	 * Verify Essentials page.
	 *
	 * @return the Essentials Page class instance.
	 */
	public TMNGNewHeaderPage verifyEssentialsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_ESSENT_LINK);
			Reporter.log("Essentials Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Essentials Page not displayed");
		}
		return this;
	}

	/**
	 * Move to plans menu and verify Unlimited 55 Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyUnlimited55Label() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink0), 60);
			moveToElement(navLink0);
			Assert.assertTrue(isElementDisplayed(navItem_0_3));
			Assert.assertTrue(navItem_0_3.getText().equalsIgnoreCase(Constants.UNAV_HEADER_ULTI55));
			Reporter.log(navItem_0_3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_0_3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Unlimited 55 link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickUnlimited55link() {
		try {
			clickElement(navItem_0_3);
			Reporter.log("Clicked on Unlimited 55 Option");
		} catch (Exception e) {
			Reporter.log("Click on Unlimited 55 failed");
			Assert.fail("Click on Unlimited 55 failed");
		}
		return this;
	}

	/**
	 * Verify Unlimited 55 page.
	 *
	 * @return the Unlimited 55 Page class instance.
	 */
	public TMNGNewHeaderPage verifyUnlimited55Page() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_ULTI55_LINK);
			Reporter.log("Unlimited 55 Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Unlimited 55 Page not displayed");
		}
		return this;
	}

	/**
	 * Move to plans menu and verify Military Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyHeaderMilitaryLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink0), 60);
			moveToElement(navLink0);
			Assert.assertTrue(isElementDisplayed(navItem_0_4));
			Assert.assertTrue(navItem_0_4.getText().equalsIgnoreCase(Constants.UNAV_HEADER_MILITARY));
			Reporter.log(navItem_0_4.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_0_4.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify Military link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickHeaderMilitarylink() {
		try {
			clickElement(navItem_0_4);
			Reporter.log("Clicked on Military Option");
		} catch (Exception e) {
			Reporter.log("Click on Military Option failed");
			Assert.fail("Click on Military Option failed");
		}
		return this;
	}

	/**
	 * Verify Military page.
	 *
	 * @return the Military Page class instance.
	 */
	public TMNGNewHeaderPage verifyHeaderMilitaryPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_MILITARY_LINK);
			Reporter.log("Military Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Military Page not displayed");
		}
		return this;
	}

	// Verify Phones Menu Options
	/**
	 * Move to Phones menu and verify Cell Phones Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyCellPhonesLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink1), 60);
			moveToElement(navLink1);
			Assert.assertTrue(isElementDisplayed(navItem_1_0));
			Assert.assertTrue(navItem_1_0.getText().equalsIgnoreCase(Constants.UNAV_HEADER_CELLPHONES));
			Reporter.log(navItem_1_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_1_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify CellPhones link
	 */
	public TMNGNewHeaderPage clickCellPhoneslink() {
		try {
			clickElement(navItem_1_0);
			Reporter.log("Clicked on Cell Phones Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Cell Phones Link Option failed");
			Assert.fail("Click on Cell Phones Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Cell Phones page.
	 *
	 * @return the Cell Phones Page class instance.
	 */
	public TMNGNewHeaderPage verifyCellPhonesPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_CELLPHONES_LINK);
			Reporter.log("Cell Phones Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Cell Phones Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Phones menu and verify Tablets Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyHeaderTabletsLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink1), 60);
			moveToElement(navLink1);
			Assert.assertTrue(isElementDisplayed(navItem_1_1));
			Assert.assertTrue(navItem_1_1.getText().equalsIgnoreCase(Constants.UNAV_HEADER_TABLETS));
			Reporter.log(navItem_1_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_1_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Tablets link
	 */
	public TMNGNewHeaderPage clickHeaderTabletslink() {
		try {
			clickElement(navItem_1_1);
			Reporter.log("Clicked on Tablets Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Tablets Link Option failed");
			Assert.fail("Click on Tablets Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Tablets page.
	 *
	 * @return the Tablets Page class instance.
	 */
	public TMNGNewHeaderPage verifyHeaderTabletsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_TABLETS_LINK);
			Reporter.log("Tablets Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Tablets Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Phones menu and verify Smart Watches Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyHeaderSmartWatchesLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink1), 60);
			moveToElement(navLink1);
			Assert.assertTrue(isElementDisplayed(navItem_1_2));
			Assert.assertTrue(navItem_1_2.getText().equalsIgnoreCase(Constants.UNAV_HEADER_SMARTWATCHES));
			Reporter.log(navItem_1_2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_1_2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Smart Watches link
	 */
	public TMNGNewHeaderPage clickHeaderSmartWatcheslink() {
		try {
			clickElement(navItem_1_2);
			Reporter.log("Clicked on Smart Watches Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Smart Watches Link Option failed");
			Assert.fail("Click on Smart Watches Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Smart Watches page.
	 *
	 * @return the Smart Watches Page class instance.
	 */
	public TMNGNewHeaderPage verifyHeaderSmartWatchesPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_SMARTWATCHES_LINK);
			Reporter.log("Smart Watches Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Smart Watches Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Phones menu and verify Accessories Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyHeaderAccessoriesLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink1), 60);
			moveToElement(navLink1);
			Assert.assertTrue(isElementDisplayed(navItem_1_3));
			Assert.assertTrue(navItem_1_3.getText().equalsIgnoreCase(Constants.UNAV_HEADER_ACCESSORIES));
			Reporter.log(navItem_1_3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_1_3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Accessories link
	 */
	public TMNGNewHeaderPage clickHeaderAccessorieslink() {
		try {
			clickElement(navItem_1_3);
			Reporter.log("Clicked on Accessories Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Accessories Link Option failed");
			Assert.fail("Click on Accessories Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Accessories page.
	 *
	 * @return the Accessories Page class instance.
	 */
	public TMNGNewHeaderPage verifyHeaderAccessoriesPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_ACCESSORIES_LINK);
			Reporter.log("Accessories Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Accessories Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Phones menu and verify Bring your own device Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyBringYourDeviceLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink1), 60);
			moveToElement(navLink1);
			Assert.assertTrue(isElementDisplayed(navItem_1_4));
			Assert.assertTrue(navItem_1_4.getText().equalsIgnoreCase(Constants.UNAV_HEADER_BRING_YOUR_OWNDEVICE));
			Reporter.log(navItem_1_4.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_1_4.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Bring your own device link
	 */
	public TMNGNewHeaderPage clickBringYourDevicelink() {
		try {
			clickElement(navItem_1_4);
			Reporter.log("Clicked on Bring your own device Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Bring your own device Link Option failed");
			Assert.fail("Click on Bring your own device Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Bring your own device page.
	 *
	 * @return the Bring your own device Page class instance.
	 */
	public TMNGNewHeaderPage verifyBringYourDevicePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_BRING_YOUR_OWNDEVICE_LINK);
			Reporter.log("Bring your own device Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Bring your own device Page not displayed");
		}
		return this;
	}

	// Verify Deals Menu Options
	/**
	 * Move to Deals menu and verify Apple Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyAppleLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2), 60);
			moveToElement(navLink2);
			Assert.assertTrue(isElementDisplayed(navItem_2_0));
			Assert.assertTrue(navItem_2_0.getText().equalsIgnoreCase(Constants.UNAV_HEADER_APPLE));
			Reporter.log(navItem_2_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_2_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Apple link
	 */
	public TMNGNewHeaderPage clickApplelink() {
		try {
			clickElement(navItem_2_0);
			Reporter.log("Clicked on Apple Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Apple Link Option failed");
			Assert.fail("Click on Apple Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Apple page.
	 *
	 * @return the Apple Page class instance.
	 */
	public TMNGNewHeaderPage verifyApplePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_APPLE_LINK);
			Reporter.log("Apple Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Apple Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Deals menu and verify Samsung Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifySamsungLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2), 60);
			moveToElement(navLink2);
			Assert.assertTrue(isElementDisplayed(navItem_2_1));
			Assert.assertTrue(navItem_2_1.getText().equalsIgnoreCase(Constants.UNAV_HEADER_SAMSUNG));
			Reporter.log(navItem_2_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_2_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Samsung link
	 */
	public TMNGNewHeaderPage clickSamsunglink() {
		try {
			clickElement(navItem_2_1);
			Reporter.log("Clicked on Samsung Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Samsung Link Option failed");
			Assert.fail("Click on Samsung Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Samsung page.
	 *
	 * @return the Samsung Page class instance.
	 */
	public TMNGNewHeaderPage verifySamsungPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_SAMSUNG_LINK);
			Reporter.log("Samsung Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Samsung Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Deals menu and verify Google Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyGoogleLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2), 60);
			moveToElement(navLink2);
			Assert.assertTrue(isElementDisplayed(navItem_2_2));
			Assert.assertTrue(navItem_2_2.getText().equalsIgnoreCase(Constants.UNAV_HEADER_GOOGLE));
			Reporter.log(navItem_2_2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_2_2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Google link
	 */
	public TMNGNewHeaderPage clickGooglelink() {
		try {
			clickElement(navItem_2_2);
			Reporter.log("Clicked on Google Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Google Link Option failed");
			Assert.fail("Click on Google Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Google page.
	 *
	 * @return the Google Page class instance.
	 */
	public TMNGNewHeaderPage verifyGooglePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_GOOGLE_LINK);
			Reporter.log("Google Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Google Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Deals menu and verify LG Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyLGLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink2), 60);
			moveToElement(navLink2);
			Assert.assertTrue(isElementDisplayed(navItem_2_3));
			Assert.assertTrue(navItem_2_3.getText().equalsIgnoreCase(Constants.UNAV_HEADER_LG));
			Reporter.log(navItem_2_3.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_2_3.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify LG link
	 */
	public TMNGNewHeaderPage clickLGlink() {
		try {
			clickElement(navItem_2_3);
			Reporter.log("Clicked on LG Link Option");
		} catch (Exception e) {
			Reporter.log("Click on LG Link Option failed");
			Assert.fail("Click on LG Link Option failed");
		}
		return this;
	}

	/**
	 * Verify LG page.
	 *
	 * @return the LG Page class instance.
	 */
	public TMNGNewHeaderPage verifyLGPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_LG_LINK);
			Reporter.log("LG Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("LG Page not displayed");
		}
		return this;
	}

	// Verify Benefits & More Menu Options
	/**
	 * Move to Benefits & More menu and verify Benefits Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyHeaderBenefitsLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink4), 60);
			moveToElement(navLink4);
			Assert.assertTrue(isElementDisplayed(navItem_4_0));
			Assert.assertTrue(navItem_4_0.getText().equalsIgnoreCase(Constants.UNAV_HEADER_BENEFITS1));
			Reporter.log(navItem_4_0.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_4_0.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Benefits link
	 */
	public TMNGNewHeaderPage clickHeaderBenefitslink() {
		try {
			clickElement(navItem_4_0);
			Reporter.log("Clicked on Benefits Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Benefits Link Option failed");
			Assert.fail("Click on Benefits Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Benefits page.
	 *
	 * @return the Benefits Page class instance.
	 */
	public TMNGNewHeaderPage verifyHeaderBenefitsPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_BENEFITS1_LINK);
			Reporter.log("Benefits Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Benefits Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Benefits & more menu and verify Why T-Mobile Vision Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyWhyTMobileLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink4), 60);
			moveToElement(navLink4);
			Assert.assertTrue(isElementDisplayed(navItem_4_1));
			Assert.assertTrue(navItem_4_1.getText().equalsIgnoreCase(Constants.UNAV_HEADER_WHYTMOBILE));
			Reporter.log(navItem_4_1.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_4_1.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Why T-Mobile Vision link
	 */
	public TMNGNewHeaderPage clickWhyTMobilelink() {
		try {
			clickElement(navItem_4_1);
			Reporter.log("Clicked on 5G Vision Link Option");
		} catch (Exception e) {
			Reporter.log("Click on 5G Vision Link Option failed");
			Assert.fail("Click on 5G Vision Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Why T-Mobile Vision page.
	 *
	 * @return the Why T-Mobile Vision Page class instance.
	 */
	public TMNGNewHeaderPage verifyWhyTMobilePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_WHYTMOBILE_LINK);
			Reporter.log("5G Vision Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("5G Vision Page not displayed");
		}
		return this;
	}

	/**
	 * Move to Benefits & More menu and verify 5G Vision Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verify5GVisionLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(navLink4), 60);
			moveToElement(navLink4);
			Assert.assertTrue(isElementDisplayed(navItem_4_2));
			Assert.assertTrue(navItem_4_2.getText().equalsIgnoreCase(Constants.UNAV_HEADER_5G));
			Reporter.log(navItem_4_2.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(navItem_4_2.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify 5G Vision link
	 */
	public TMNGNewHeaderPage click5GVisionlink() {
		try {
			clickElement(navItem_4_2);
			Reporter.log("Clicked on 5G Vision Link Option");
		} catch (Exception e) {
			Reporter.log("Click on 5G Vision Link Option failed");
			Assert.fail("Click on 5G Vision Link Option failed");
		}
		return this;
	}

	/**
	 * Verify 5G Vision page.
	 *
	 * @return the 5G Vision Page class instance.
	 */
	public TMNGNewHeaderPage verify5GVisionPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_5G_LINK);
			Reporter.log("5G Vision Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("5G Vision Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Find a store Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyFindAStoreLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(utility_0_Label), 60);
			Assert.assertTrue(isElementDisplayed(utility_0_Label));
			Assert.assertTrue(utility_0_Label.getText().equalsIgnoreCase(Constants.UNAV_HEADER_FIND_A_STORE));
			Reporter.log(utility_0_Label.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(utility_0_Label.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * verify FindAStore link
	 */
	public TMNGNewHeaderPage clickFindAStorelink() {
		try {
			clickElement(utility_0);
			Reporter.log("Clicked on FindAStore Link Option");
		} catch (Exception e) {
			Reporter.log("Click on FindAStore Link Option failed");
			Assert.fail("Click on FindAStore Link Option failed");
		}
		return this;
	}

	/**
	 * Verify FindAStore page.
	 *
	 * @return the FindAStore Page class instance.
	 */
	public TMNGNewHeaderPage verifyFindAStorePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_FIND_A_STORE_LINK);
			Reporter.log("FindAStore Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("FindAStore Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Let's Talk Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyLetsTalkLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(utility_1), 60);
			Assert.assertTrue(isElementDisplayed(utility_1));
			// Assert.assertTrue(utility_1.getText().equalsIgnoreCase(Constants.UNAV_HEADER_LETS_TALK));
			Reporter.log("Let's Talk is displayed");
		} catch (Exception e) {
			Assert.fail("Let's Talk is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify Cart Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyCartLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cart), 60);
			Assert.assertTrue(isElementDisplayed(cart));
			// Assert.assertTrue(cart.getText().equalsIgnoreCase(Constants.UNAV_HEADER_CART));
			Reporter.log("cart is displayed");
		} catch (Exception e) {
			Assert.fail("cart is not dispalyed");
		}
		return this;
	}

	/**
	 * verify Cart link
	 */
	public TMNGNewHeaderPage clickCartlink() {
		try {
			clickElement(cart);
			Reporter.log("Clicked on Cart Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Cart Link Option failed");
			Assert.fail("Click on Cart Link Option failed");
		}
		return this;
	}

	/**
	 * Verify Cart page.
	 *
	 * @return the Cart Page class instance.
	 */
	public TMNGNewHeaderPage verifyCartPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_CART_LINK);
			Reporter.log("Cart Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Cart Page not displayed");
		}
		return this;
	}

	// Verify My Account
	/**
	 * Verify My Account Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyMyAccountLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(myAccount_desk), 60);
			Assert.assertTrue(isElementDisplayed(myAccount_desk));
			Assert.assertTrue(myAccount_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_MYACCOUNT_WIRELESS));
			Reporter.log(myAccount_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(myAccount_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click My Account link
	 */
	public TMNGNewHeaderPage clickMyAccountlink() {
		try {
			clickElement(myAccount_desk);
			Reporter.log("Clicked on My Account Link Option");
		} catch (Exception e) {
			Reporter.log("Click on My Account Link Option failed");
			Assert.fail("Click on My Account Link Option failed");
		}
		return this;
	}

	/**
	 * Verify My Account - Login Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyLoginLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(logIn_myAcc_desk), 60);
			Assert.assertTrue(isElementDisplayed(logIn_myAcc_desk));
			Assert.assertTrue(logIn_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_LOGIN_WIRELESS));
			Reporter.log(logIn_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(logIn_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click Log In link
	 */
	public TMNGNewHeaderPage clickLoginlink() {
		try {
			clickElement(logIn_myAcc_desk);
			Reporter.log("Clicked on Log In Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Log In Link Option failed");
			Assert.fail("Click on Log In Link Option failed");
		}
		return this;
	}

	/**
	 * Verify MyAccount - Login page.
	 *
	 * @return the MyAccount - Login Page class instance.
	 */
	public TMNGNewHeaderPage verifyLoginPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_LOGIN_LINK);
			Reporter.log("MyAccount Login Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MyAccount Login Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Quick Actions Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyQuickActionsLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(quickActions_myAcc_desk), 60);
			Assert.assertTrue(isElementDisplayed(quickActions_myAcc_desk));
			Assert.assertTrue(quickActions_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_QUICK_ACTIONS));
			Reporter.log(quickActions_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(quickActions_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify My Account - Bill Pay Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyBillPayLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(billPay_myAcc_desk), 60);
			Assert.assertTrue(isElementDisplayed(billPay_myAcc_desk));
			Assert.assertTrue(billPay_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_BILL_PAY));
			Reporter.log(billPay_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(billPay_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click Bill Pay link
	 */
	public TMNGNewHeaderPage clickBillPaylink() {
		try {
			clickElement(billPay_myAcc_desk);
			Reporter.log("Clicked on Bill Pay Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Bill Pay Link Option failed");
			Assert.fail("Click on Bill Pay Link Option failed");
		}
		return this;
	}

	/**
	 * Verify MyAccount - Bill Pay page.
	 *
	 * @return the MyAccount - Bill Pay Page class instance.
	 */
	public TMNGNewHeaderPage verifyBillPayPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_BILL_PAY_LINK);
			Reporter.log("MyAccount Bill Pay Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MyAccount Bill Pay Page not displayed");
		}
		return this;
	}

	/**
	 * Verify Add A Device Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyAddDeviceLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addDevice_myAcc_desk));
			Assert.assertTrue(isElementDisplayed(addDevice_myAcc_desk));
			Assert.assertTrue(addDevice_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_ADD_DEVICE));
			Reporter.log(addDevice_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(addDevice_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify Upgrade Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyUpgradeLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(upgrade_myAcc_desk), 60);
			Assert.assertTrue(isElementDisplayed(upgrade_myAcc_desk));
			Assert.assertTrue(upgrade_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_UPGRADE));
			Reporter.log(upgrade_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(upgrade_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * Verify My Account - Check Order Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyCheckOrderLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(checkOrderStatus_myAcc_desk), 60);
			Assert.assertTrue(isElementDisplayed(checkOrderStatus_myAcc_desk));
			Assert.assertTrue(
					checkOrderStatus_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_CHECK_ORDER));
			Reporter.log(checkOrderStatus_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(checkOrderStatus_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click Check Order link
	 */
	public TMNGNewHeaderPage clickCheckOrderlink() {
		try {
			clickElement(checkOrderStatus_myAcc_desk);
			Reporter.log("Clicked on Check Order Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Check Order Link Option failed");
			Assert.fail("Click on Check Order Link Option failed");
		}
		return this;
	}

	/**
	 * Verify MyAccount - Check Order page.
	 *
	 * @return the MyAccount - Check Order Page class instance.
	 */
	public TMNGNewHeaderPage verifyCheckOrderPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_CHECK_ORDER_LINK);
			Reporter.log("MyAccount Check Order Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MyAccount Check Order Page not displayed");
		}
		return this;
	}

	/**
	 * Verify My Account - Support Label
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifySupportLabel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(support_myAcc_desk), 60);
			Assert.assertTrue(isElementDisplayed(support_myAcc_desk));
			Assert.assertTrue(support_myAcc_desk.getText().equalsIgnoreCase(Constants.UNAV_HEADER_SUPPORT));
			Reporter.log(support_myAcc_desk.getText() + " is displayed");
		} catch (Exception e) {
			Assert.fail(support_myAcc_desk.getText() + " is not dispalyed");
		}
		return this;
	}

	/**
	 * click Support link
	 */
	public TMNGNewHeaderPage clickSupportlink() {
		try {
			clickElement(support_myAcc_desk);
			Reporter.log("Clicked on Support Link Option");
		} catch (Exception e) {
			Reporter.log("Click on Support Link Option failed");
			Assert.fail("Click on Support Link Option failed");
		}
		return this;
	}

	/**
	 * Verify MyAccount - Support page.
	 *
	 * @return the MyAccount - Support Page class instance.
	 */
	public TMNGNewHeaderPage verifySupportPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_HEADER_ABOUTUS_SUPPORT_LINK);
			Reporter.log("MyAccount Support Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("MyAccount Support Page not displayed");
		}
		return this;
	}

	// Mobile Clicks on Wireless Header Page
	/**
	 * Click on Menu Expand link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickMenuExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(header_menu);
				header_menu.click();
				Reporter.log("Clicked on Menu Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Menu Expnad Button failed");
			Assert.fail("Click on Menu Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Plans Plus Expand Expand link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickPlansPlusExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(plusExpand_minusMinimize_mob_0);
				plusExpand_minusMinimize_mob_0.click();
				Reporter.log("Clicked on Plans Plus Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Plans Plus Expnad Button failed");
			Assert.fail("Click on Plans Plus Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Phones Plus Expand Expand link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickPhonesPlusExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(plusExpand_minusMinimize_mob_1);
				plusExpand_minusMinimize_mob_1.click();
				Reporter.log("Clicked on Phones Plus Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Phones Plus Expnad Button failed");
			Assert.fail("Click on Phones Plus Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Deals Plus Expand Expand link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickDealsPlusExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(plusExpand_minusMinimize_mob_2);
				plusExpand_minusMinimize_mob_2.click();
				Reporter.log("Clicked on Deals Plus Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Deals Plus Expnad Button failed");
			Assert.fail("Click on deals Plus Expnad Button failed");
		}
		return this;
	}

	/**
	 * Click on Benefits Plus Expand Expand link
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickBenefitsPlusExpandButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(plusExpand_minusMinimize_mob_4);
				plusExpand_minusMinimize_mob_4.click();
				Reporter.log("Clicked on Benefits Plus Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on Benefits Plus Expnad Button failed");
			Assert.fail("Click on Benefits Plus Expnad Button failed");
		}
		return this;
	}

	/**
	 * Verify My Account icon
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyMyAccountIcon() {
		try {
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(header_wireless_myAccount_icon));
			Reporter.log("My Account Icon link is displayed in UNAV header");
		} catch (Exception e) {
			Assert.fail("My Account Icon link is not dispalyed");
		}
		return this;
	}

	/**
	 * Click on My Account Right Expand Arrow
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickMyAccountRightArrow() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				moveToElement(utility_4_rightArrow_mob);
				clickElement(utility_4_rightArrow_mob);
				Reporter.log("Clicked on My Account Right Arrow Expnad Button");
			}
		} catch (Exception e) {
			Reporter.log("Click on My Account Right Arrow Expnad Button failed");
			Assert.fail("Click on My Account Right Arrow Expnad Button failed");
		}
		return this;
	}

	/**
	 * Verify Cart count in Home page Header
	 */
	public TMNGNewHeaderPage verifyCountOnCart(int count) {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			Assert.assertEquals(cartCounter, count);
			Reporter.log("Cart #" + count + " is displayed after adding the items ");
		} catch (Exception e) {
			Assert.fail("Failed to display " + count + " on Cart after adding the items ");
		}
		return this;
	}

	/**
	 * Click on Let's Talk menu
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage moveToletsTalkMenu() {
		try {
			moveToElement(letsTalkMenu);
			Reporter.log("Moved foucs on Let's Talk Menu");
		} catch (Exception e) {
			Assert.fail("Failed to move focus on Let's Talk menu ");
		}
		return this;
	}

	/**
	 * Click on Message US option
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage clickMessageUsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(messageUsLink), 60);
			messageUsLink.click();
			Reporter.log("Clicked on Let's Talk Menu");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Let's Talk menu ");
		}
		return this;
	}

	/**
	 * Verify LP chat box after clicking on Message US
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifyLPChatBox() {
		try {
			Thread.sleep(15000);
			waitFor(ExpectedConditions.visibilityOf(lpmessageUsHeader));
			Assert.assertTrue(lpmessageUsHeader.getText().equalsIgnoreCase("Message Us"),
					"Message Us not displayed after clicking on Let's Talk");
			Reporter.log("Verified LP chat box after clicking on Message US");
		} catch (Exception e) {
			Assert.fail("Failed to LP chat box after clicking on Message US");
		}
		return this;
	}

	/**
	 * Verify Selected Menu is highlighted
	 * 
	 * @return
	 */
	public TMNGNewHeaderPage verifySelectedMenuisHighlighted(String selectedMenu) {
		try {
			Assert.assertTrue(selectedMenuOnUNAVTMO.getAttribute("data-analytics-value").equalsIgnoreCase(selectedMenu),
					"Selected Menu is not highlighted:" + selectedMenu);
			Reporter.log("Selected menu item is highlighted in UNAV: " + selectedMenu);
		} catch (Exception e) {
			Assert.fail("Failed to verify Selected Menu is highlighted");
		}
		return this;
	}

}