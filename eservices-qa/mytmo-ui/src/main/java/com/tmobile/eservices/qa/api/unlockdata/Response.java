package com.tmobile.eservices.qa.api.unlockdata;

import java.util.List;

public class Response {

	private List<String> orders = null;
	private String status = null;
	private Exception failure = null;
	private int statusCode = 0;
	private String orderURL;
	private List<String> messagesList = null;
	private String pinCode;
	private String zipCode;
	private String tempPassword;
	private String errorcode;
	private String errordescription;


	public List<String> getOrders() {
		return orders;
	}

	public void setOrders(List<String> orders) {
		this.orders = orders;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Exception getFailure() {
		return failure;
	}

	public void setFailure(Exception failure) {
		this.failure = failure;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getOrderURL() {
		return orderURL;
	}

	public void setOrderURL(String orderURL) {
		this.orderURL = orderURL;
	}

	public List<String> getMessagesList() {
		return messagesList;
	}

	public void setMessagesList(List<String> messagesList) {
		this.messagesList = messagesList;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getErrordescription() {
		return errordescription;
	}

	public void setErrordescription(String errordescription) {
		this.errordescription = errordescription;
	}

	@Override
	public String toString() {
		return "Response [orders=" + orders + ", status=" + status + ", failure=" + failure + ", statusCode="
				+ statusCode + ", orderURL=" + orderURL + ", messagesList=" + messagesList + ", pinCode=" + pinCode
				+ ", zipCode=" + zipCode + ", tempPassword=" + tempPassword + ", errorcode=" + errorcode
				+ ", errordescription=" + errordescription + "]";
	}

}
