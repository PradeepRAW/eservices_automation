package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 * 
 */
public class LineSettingsPage extends CommonPage {

	private static final String pageUrl = "profile/line_settings";

	@FindBy(css = "p[class*='H5-heading']")
	private WebElement permissionChangesTextMessage;

	@FindBy(xpath = "//p[text()='Nickname']")
	private WebElement nickNameLink;

	@FindBy(xpath = "//p[text()='Paperless Billing']")
	private WebElement paperlessBillingAdressLink;

	@FindBy(xpath = "#city")
	private WebElement city;

	@FindBy(css = "#line1")
	private WebElement addressLine1;

	@FindBy(css = "#line2")
	private WebElement addressLine2;

	@FindBy(css = "[name='e911_address']")
	private WebElement e911Address;

	@FindBy(xpath = "//input[@name='e911_address']/../div/span")
	private WebElement e911AddressChkBox;

	@FindBy(xpath = "//input[@name='usgAddress']/../div/span")
	private WebElement usageAddressChkBox;

	@FindBy(css = "[name='usgAddress']")
	private WebElement usageAddress;

	@FindBy(css = ".SecondaryCTA")
	private WebElement cancelBtn;

	@FindBy(css = ".PrimaryCTA")
	private WebElement saveChangesBtn;

	@FindBy(css = "label[for='1']")
	private WebElement paperLessBillRadioBtn;

	@FindBy(css = "label[for='email'] .check1")
	private WebElement emailRadioBtn;

	@FindBy(id = "firstName")
	private WebElement firstNameTxtField;

	@FindBy(id = "lastName")
	private WebElement lastNameTxtField;

	@FindBy(css = ".btn-primary")
	private WebElement saveBtn;

	@FindBy(id = "logout-Ok")
	private WebElement logoutButton;

	@FindBy(xpath = "//p[contains(text(),'911 Address')]")
	private WebElement e911AdressLink;

	@FindBy(xpath = "//p[text()='Usage Address']")
	private WebElement usageAdressLink;

	@FindBy(xpath = "//p[text()='Permissions']")
	private WebElement permissionsHeader;

	@FindBy(css = "div#model span.body.black")
	private WebElement selectedLine;

	@FindBys(@FindBy(css = "div#test ul li"))
	private List<WebElement> nextBan;

	@FindBys(@FindBy(css = "div#test ul li a"))
	private List<WebElement> nextBanMissdn;

	@FindBy(css = "input#firstName")
	private WebElement nickName;

	@FindBy(css = "div#model")
	private WebElement selectLine;

	@FindBy(css = "div.dropdown-toggle span[class*='arrow-down']")
	private WebElement downArrowImg;

	@FindBy(css = "ul[class*='dropdown-menu'] li")
	private List<WebElement> listOfLines;

	@FindBy(xpath = "//p[contains(text(),'mission')]/following-sibling::p")
	private WebElement permissionsHolder;

	@FindBy(xpath = "//p[contains(text(),'KickBack')]")
	private WebElement kickBack;

	@FindBy(css = "div#model")
	private WebElement lineHeader;

	@FindBys(@FindBy(css = "div#test ul li"))
	private List<WebElement> lineList;

	@FindBy(xpath = "//label[text()='Standard Access']/..//input")
	private WebElement standardAccessBtn;

	@FindBy(xpath = "//label[text()='Full Access']/..//input")
	private WebElement fullAccessBtn;

	@FindBy(xpath = "//label[text()='Full Access']")
	private WebElement fullAccess;

	@FindBy(xpath = "//label[text()='Standard Access']")
	private WebElement standardAccess;

	// @FindBys(@FindBy(css = "p.body.d-inline-block"))
	@FindBy(xpath = "//p[contains(text(),'Access')]")
	private WebElement permissionTxt;

	@FindBy(css = ".SecondaryCTA")
	private WebElement backBtn;

	@FindBy(xpath = "//p[contains(text(),'Primary')]")
	private List<WebElement> permissionsPrimaryHolder;

	@FindBy(xpath = "//p[text()='Permissions']/../p[contains(@class,'body')]")
	private WebElement permissionLevel;

	@FindBy(xpath = "//label[text()='No Access']")
	private WebElement noAccess;

	@FindBy(xpath = "//label[text()='Restricted Access']")
	private WebElement restrictedAccess;

	private String nickNameText = "test test test test ";

	@FindBy(css = "#user-links-dropdown")
	private WebElement hamburgerMenuFirstName;

	@FindBy(css = "#user-links-dropdown-mobile .d-inline")
	private WebElement hamburgerMenuFirstNameForMobile;

	@FindBy(css = "[for='GMP_NM_P']")
	private WebElement permitAllRadioBtn;

	@FindBy(css = "#GMP_NM_P")
	private WebElement permitAllRadioBtnDiv;

	@FindBy(css = "#GMP_NM_NP")
	private WebElement restrictAllRadioBtnDiv;

	@FindBy(css = "[for='GMP_NM_NP']")
	private WebElement restrictAllRadioBtn;

	@FindBy(css = "p.Display6")
	private WebElement govAccountPermissionContent;

	@FindBy(css = "[aria-label='menu']")
	private WebElement hamburgerMenu;

	@FindBy(css = "span[class='legal check1']")
	private WebElement iAgreeToTheseTermsCheckBox;

	@FindAll({ @FindBy(css = "p.Display6") })
	private List<WebElement> autTexts;

	public LineSettingsPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify LineSettings page.
	 *
	 * @return the LineSettingsPage class instance.
	 */
	public LineSettingsPage verifyLineSettingsPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("LineSettings page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("LineSettings page not displayed");
		}
		return this;

	}

	/**
	 * Click nick name link.
	 */
	public LineSettingsPage clickNickNameLink() {
		try {
			waitforSpinnerinProfilePage();
			nickNameLink.click();
			Reporter.log("Clicked on nick name link");
		} catch (Exception e) {
			Assert.fail("Click on nick name link is failed ");
		}
		return this;
	}

	/**
	 * Click nick name link.
	 */
	public LineSettingsPage selectLineFromDropDown() {
		try {
			waitforSpinnerinProfilePage();
			Select sel = new Select(selectLine);
			sel.selectByIndex(1);
			Reporter.log("selected other line");
		} catch (Exception e) {
			Assert.fail("Selecting other line failed ");
		}
		return this;
	}

	/**
	 * Click nick name link.
	 */
	public LineSettingsPage selectLineFromDropDownList() {
		try {
			waitforSpinnerinProfilePage();
			downArrowImg.click();
			listOfLines.get(1).click();
			Reporter.log("selected other line");
		} catch (Exception e) {
			Assert.fail("Selecting other line failed ");
		}
		return this;
	}

	/**
	 * Update e911BillingAddress.
	 */
	public LineSettingsPage updatee911BillingAddress() {
		try {
			waitforSpinnerinProfilePage();
			sendTextData(addressLine1, "2626 228TH ST SE");
			sendTextData(addressLine2, "test");
		} catch (Exception e) {
			Assert.fail("Updating billing address failed");
		}
		return this;
	}

	/**
	 * Update usageBillingAddress.
	 */
	public LineSettingsPage updateUsageBillingAddress() {
		try {
			waitforSpinnerinProfilePage();
			sendTextData(addressLine1, "2626 228TH ST SE");
			sendTextData(addressLine2, "test");
		} catch (Exception e) {
			Assert.fail("Updating billing address failed");
		}
		return this;
	}

	/**
	 * Verify e911Address.
	 */
	public LineSettingsPage verifye911Address() {
		try {
			waitforSpinnerinProfilePage();
			scrollToElement(e911Address);
			if (!e911Address.isSelected()) {
				clickElement(e911AddressChkBox);
			}
		} catch (Exception e) {
			Assert.fail("Click on e911Address checkbox failed");
		}
		return this;
	}

	/**
	 * Verify UsageAddress.
	 */
	public LineSettingsPage verifyUsageAddress() {
		try {
			waitforSpinnerinProfilePage();
			scrollToElement(usageAddress);
			if (!usageAddress.isSelected()) {
				clickElement(usageAddressChkBox);
			}
		} catch (Exception e) {
			Assert.fail("Click on usageAddress checkbox failed");
		}
		return this;
	}

	/**
	 * Verify cancel button CTA.
	 */
	public LineSettingsPage verifyCancelButtonCTA() {
		try {
			waitforSpinnerinProfilePage();
			cancelBtn.click();
			Reporter.log("Clicked on cancel button");
		} catch (Exception e) {
			Assert.fail("Click on cancel button failed");
		}
		return this;
	}

	/**
	 * click SaveChanges btn.
	 */
	public LineSettingsPage clickSaveChangesBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (saveChangesBtn.isEnabled()) {
				Reporter.log("Save changes button is enabled");
				saveChangesBtn.click();
			} else {
				Assert.fail("Save changes button is in disabled state");
			}
		} catch (Exception e) {
			Assert.fail("Click on save changes button failed");
		}
		return this;
	}

	/**
	 * Verify e911Address.
	 */
	public LineSettingsPage clickPaperLessBillRadioBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (!paperLessBillRadioBtn.isSelected()) {
				clickElement(paperLessBillRadioBtn);
				Thread.sleep(10000);
			}
		} catch (Exception e) {
			Assert.fail("Click on paperLessBillRadioBtn checkbox failed");
		}
		return this;
	}

	/**
	 * Click BillingAddress.
	 */
	public LineSettingsPage clickEmailRadioBtn() {
		try {
			waitforSpinnerinProfilePage();
			scrollToElement(emailRadioBtn);
			if (!emailRadioBtn.isSelected()) {
				emailRadioBtn.click();
				System.out.println("jkaghdasdjksgakjdg");
				Thread.sleep(10000);
			}
		} catch (Exception e) {
			Assert.fail("Click on emailRadioBtn checkbox failed");
		}

		return this;
	}

	/**
	 * Update nickname and click save.
	 */
	public LineSettingsPage UpdateNickNameAndClickSave() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			sendTextData(firstNameTxtField, "test");
			sendTextData(lastNameTxtField, "test");
			clickElement(saveBtn);
			waitforSpinnerinProfilePage();
			Reporter.log("NickName succesfully updated");
		} catch (Exception e) {
			Assert.fail("Updating NickName failed" + e.toString());

		}
		return this;
	}

	/**
	 */
	public LineSettingsPage clickLogOutBtn() {
		waitforSpinnerinProfilePage();
		checkPageIsReady();
		clickElement(logoutButton);
		return this;
	}

	/**
	 * Click E911Address.
	 */
	public LineSettingsPage clickE911Address() {
		try {
			waitforSpinnerinProfilePage();
			e911AdressLink.click();
			Reporter.log("Clicked on e911AdressLink");
		} catch (Exception e) {
			Assert.fail("Click on e911AdressLink failed");
		}
		return this;
	}

	/**
	 * Click usageAddress.
	 */
	public LineSettingsPage clickUsageAddress() {
		try {
			waitforSpinnerinProfilePage();
			usageAdressLink.click();
			Reporter.log("Clicked on usageAdress link");
		} catch (Exception e) {
			Assert.fail("Click on usageAdresslink failed");
		}
		return this;
	}

	/**
	 * Click permissions link.
	 */
	public LineSettingsPage clickPermissions() {
		try {
			waitforSpinnerinProfilePage();
			permissionsHeader.click();
			Reporter.log("Clicked on permisiions link");
		} catch (Exception e) {
			Assert.fail("Click on permissions link failed");
		}
		return this;
	}

	/**
	 * Click on select line.
	 */
	public LineSettingsPage changeToNextLine() {
		try {
			checkPageIsReady();
			lineHeader.click();
			for (WebElement element : nextBan) {
				if (!element.getAttribute("class").contains("active")) {
					clickElementWithJavaScript(element);
					break;
				}
			}
			Reporter.log("Change to next line is success");
		} catch (Exception e) {
			Assert.fail("Change to next line failed ");
		}
		return this;
	}

	/**
	 * Enter Caller Id FirstName
	 */
	public LineSettingsPage enterCallerIdFirstName() {
		try {
			checkPageIsReady();
			lineHeader.click();
			for (WebElement element : nextBan) {
				if (!element.getAttribute("class").contains("active")) {
					clickElementWithJavaScript(element);
					break;
				}
			}
			Reporter.log("Change to next line is success");
		} catch (Exception e) {
			Assert.fail("Change to next line failed ");
		}
		return this;
	}

	/**
	 * Verify the authorable text.
	 */

	public LineSettingsPage verifyAuthorabletext(String str, WebElement element) {
		waitforSpinner();
		try {
			Assert.assertTrue(element.getText().equalsIgnoreCase(str), "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + element.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + element.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + element.getText());
		}
		return null;

	}

	/**
	 * Verify the authorable text.
	 */

	public LineSettingsPage verifyNclickLineSettingsAuthorableLinks(String str) {
		checkPageIsReady();
		for (int i = 0; i < autTexts.size(); i++) {
			try {
				if (autTexts.get(i).getText().contains(str)) {
					Assert.assertTrue(autTexts.get(i).getText().equalsIgnoreCase(str),
							"Expected result: Authorable text should appear " + str);
					autTexts.get(i).click();
				}
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + autTexts.get(i).getText());
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + autTexts.get(i).getText());
			}

		}
		return this;

	}

	/**
	 * Verify the authorable text.
	 */

	public LineSettingsPage getCallerIdName() {
		checkPageIsReady();
		try {
			String callerIdName = hamburgerMenu.getText();
			if (callerIdName.length() > 0) {
				Reporter.log("Caller id Name is " + callerIdName);
			}
		} catch (Exception e) {
			Assert.fail("Unable to get caller id name");
		}

		return this;

	}

	/**
	 * Click On Second Ban Number
	 */
	public LineSettingsPage verifyNickNameUpdated() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			selectedLine.getText().contains(nickName.getText());
			Reporter.log("Nick name updated");
		} catch (Exception e) {
			Assert.fail("Nick name not updated");
		}
		return this;
	}

	/**
	 * Verify KickBack does not appear for simple choice plan
	 */
	public LineSettingsPage verifyKickBackNotAppear() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertFalse(isElementDisplayed(kickBack));
			Reporter.log("KickBack not displayed for restricted");
		} catch (AssertionError e) {
			Reporter.log("KickBack displayed for restricted");
		}
		return this;
	}

	/**
	 * Get text for Permissions Holder
	 */
	public LineSettingsPage verifyPermissionsHolderText(String textValue) {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(verifyElementByText(permissionsHolder, textValue));
			Reporter.log(textValue + " text displayed");
		} catch (AssertionError e) {
			Reporter.log(textValue + " text not displayed");
			Assert.fail(" " + textValue + "is not displayed");
		}
		return this;
	}

	/**
	 * Verify permissions header.
	 */
	public LineSettingsPage verifyPermissionsHeader() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(isElementDisplayed(permissionsHeader));
			Reporter.log("Permission header displayed");
		} catch (AssertionError e) {
			Reporter.log("Permission header not displayed");
			Assert.fail("Permission header not displayed");
		}
		return this;
	}

	/**
	 * Verify standard access permissions.
	 */
	public LineSettingsPage verifyStandardAccessPermission() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(lineHeader));
			lineHeader.click();
			waitFor(ExpectedConditions.visibilityOfAllElements(lineList));
			lineList.get(1).click();
			permissionsHeader.click();
			waitFor(ExpectedConditions.visibilityOf(standardAccess));
			if (!standardAccessBtn.isSelected()) {
				standardAccess.click();
				saveChangesBtn.click();
			} else {
				backBtn.click();
			}
			waitforSpinnerinProfilePage();
			Assert.assertTrue(permissionTxt.getText().contains("Standard Access"),
					"Standard Access permission not updated");
			Reporter.log("Standard access permission updated");
		} catch (Exception e) {
			Assert.fail("Standard access permission not updated");
		}
		return this;
	}

	/**
	 * Verify full access permissions.
	 */
	public LineSettingsPage verifyFullAccessPermission() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(lineHeader));
			lineHeader.click();
			lineList.get(1).click();
			permissionsHeader.click();
			waitFor(ExpectedConditions.visibilityOf(fullAccess));
			if (!fullAccessBtn.isSelected()) {
				fullAccess.click();
				saveChangesBtn.click();
			} else {
				backBtn.click();
			}
			waitforSpinnerinProfilePage();
			Assert.assertTrue(permissionTxt.getText().contains("Full Access"), "Full access permission not updated");
			Reporter.log("Full access permission updated");
		} catch (Exception e) {
			Assert.fail("Full access permission not updated");
		}
		return this;
	}

	/**
	 * Click on select line.
	 */
	public LineSettingsPage clickOnStandardAccessLine(String missdn) {
		try {
			checkPageIsReady();
			lineHeader.click();
			for (WebElement element : nextBanMissdn) {
				if (element.getText().contains(missdn)) {
					clickElementWithJavaScript(element);
					break;
				}
			}
			Reporter.log("Clicked on standard access line");
		} catch (Exception e) {
			Reporter.log("Click on standard access line failed");
			Assert.fail("Click on standard access line failed ");
		}
		return this;
	}

	/**
	 * Click on select line.
	 */
	public LineSettingsPage clickOnCallerIdBlade() {
		try {
			checkPageIsReady();
			hamburgerMenu.click();
			Reporter.log("Clicked on callerId Blade");
		} catch (Exception e) {
			Reporter.log("Click on callerId Blade failed");
			Assert.fail("Click on callerId Blade failed");
		}
		return this;
	}

	/**
	 * Verify the callerId Name Page.
	 */

	public LineSettingsPage verifyCallerIDNamePage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("/callerId"));
		} catch (Exception e) {
			Assert.fail("/callerid");
		}
		return this;
	}

	/**
	 * Verify full access permissions.
	 */
	public LineSettingsPage verifyFullAccess() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(permissionLevel.getText().contains("Full Access"), "Full access permission not updated");
			Reporter.log("Full access permission updated");
		} catch (Exception e) {
			Assert.fail("Full access permission not updated");
		}
		return this;
	}

	/**
	 * Verify standard access permissions.
	 */
	public LineSettingsPage verifyStandardAccess() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(permissionLevel.getText().contains("Standard Access"),
					"Standard Access permission not updated");
			Reporter.log("Standard access permission updated");
		} catch (Exception e) {
			Assert.fail("Standard access permission not updated");
		}
		return this;
	}

	/**
	 * Click permission header
	 */
	public LineSettingsPage clickPermissionHeader() {
		try {
			waitforSpinnerinProfilePage();
			permissionsHeader.click();
			Reporter.log("Click on permission header should be suucess");
		} catch (Exception e) {
			Reporter.log("Click on permission header failed");
			Assert.fail("Click on permission header failed");
		}
		return this;
	}

	/**
	 * Click full access radio button.
	 */
	public LineSettingsPage clickFullAccessRadioButton() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(fullAccess));
			if (!fullAccessBtn.isSelected()) {
				fullAccess.click();
				saveChangesBtn.click();
			} else {
				backBtn.click();
			}
			Reporter.log("Full access radio button clicked");
		} catch (Exception e) {
			Assert.fail("Click on full access radio button failed ");
		}
		return this;
	}

	/**
	 * Verify full access radio button.
	 */
	public LineSettingsPage clickStandardAccessRadioButton() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(standardAccess));
			if (!standardAccessBtn.isSelected()) {
				standardAccess.click();
				saveChangesBtn.click();
			} else {
				backBtn.click();
			}
			Reporter.log("Standard access radio button clicked");
		} catch (Exception e) {
			Assert.fail("Click on standard access radio button failed ");
		}
		return this;
	}

	/**
	 * Click no access radio button.
	 */
	public LineSettingsPage clickNoAccessRadioButton() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(noAccess));
			if (!noAccess.isSelected()) {
				noAccess.click();
				saveChangesBtn.click();
			} else {
				noAccess.click();
			}
			Reporter.log("Clicked on no access radio button");
		} catch (Exception e) {
			Assert.fail("Click on no access radio button failed ");
		}
		return this;
	}

	/**
	 * Click no access radio button.
	 */
	public LineSettingsPage clickRestrictedAccessRadioButton() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(restrictedAccess));
			if (!restrictedAccess.isSelected()) {
				restrictedAccess.click();
				saveChangesBtn.click();
			} else {
				restrictedAccess.click();
			}
			Reporter.log("Clicked on restricted access radio button");
		} catch (Exception e) {
			Assert.fail("Click on restricted access radio button failed ");
		}
		return this;
	}

	/**
	 * Verify no access permissions.
	 */
	public LineSettingsPage verifyNoAccess() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(permissionLevel.getText().contains("No Access"), "No Access permission not updated");
			Reporter.log("No access permission updated");
		} catch (Exception e) {
			Assert.fail("No access permission not updated");
		}
		return this;
	}

	/**
	 * Verify restricted access permissions.
	 */
	public LineSettingsPage verifyRestrictedAccess() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(permissionLevel.getText().contains("Restricted Access"),
					"Restricted Access permission not updated");
			Reporter.log("Restricted access permission updated");
		} catch (Exception e) {
			Assert.fail("Restricted access permission not updated");
		}
		return this;
	}

	/**
	 * Update first name to 20 characters and click save.
	 */
	public LineSettingsPage updateFirstNameTo20CharactersAndClickSave() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			firstNameTxtField.clear();
			sendTextData(firstNameTxtField, nickNameText);
			clickElement(saveChangesBtn);
			waitforSpinnerinProfilePage();
			Reporter.log("Updating first name to 20 characters is success");
		} catch (Exception e) {
			Assert.fail("Updating first name to 20 characters failed" + e.toString());

		}
		return this;
	}

	/**
	 * Verify first name in hamburger menu.
	 */
	public LineSettingsPage verifyFirstNameInHamburgerMenu() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenu));
				hamburgerMenu.click();
				waitFor(ExpectedConditions.visibilityOf(hamburgerMenuFirstNameForMobile));
				Assert.assertTrue(hamburgerMenuFirstNameForMobile.getText().contains(nickNameText.trim()));
			} else {
				waitforSpinnerinProfilePage();
				Assert.assertEquals(hamburgerMenuFirstName.getText().trim(), nickNameText.trim());
			}
			Reporter.log("First name updated successfully in hamburger menu");
		} catch (AssertionError e) {
			Assert.fail("Updated first name not reflected in hamburger menu");
		}
		return this;
	}

	/**
	 * Click permit all permission.
	 */
	public LineSettingsPage clickPermitAllRadioButton() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(permitAllRadioBtn));
			if (!permitAllRadioBtnDiv.isSelected()) {
				permitAllRadioBtn.click();
				saveChangesBtn.click();
			} else {
				backBtn.click();
			}
			Reporter.log("Click on permit all radio button is success");
		} catch (Exception e) {
			Reporter.log("Click on permit all radio button failed");
			Assert.fail("Click on permit all radio button failed");
		}
		return this;
	}

	/**
	 * Verify permit all radio button is selected
	 */
	public LineSettingsPage verifyPermitAllRadioButtonSelected() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(permitAllRadioBtn));
			Assert.assertTrue(permitAllRadioBtnDiv.isSelected());
			backBtn.click();
			Reporter.log("Permit all radio button is in selected state");
		} catch (Exception e) {
			Reporter.log("Permit all radio button is not in selected state");
			Assert.fail("Permit all radio button is not in selected state");
		}
		return this;
	}

	/**
	 * Click restrict all permission.
	 */
	public LineSettingsPage clickRestrictAllRadioButton() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(restrictAllRadioBtn));
			if (!restrictAllRadioBtnDiv.isSelected()) {
				restrictAllRadioBtn.click();
				saveChangesBtn.click();
			} else {
				backBtn.click();
			}
			Reporter.log("Click on restrict all radio button is success");
		} catch (Exception e) {
			Reporter.log("Click on restrict all radio button failed");
			Assert.fail("Click on restrict all radio button failed");
		}
		return this;
	}

	/**
	 * Verify restrict all radio button is selected
	 */
	public LineSettingsPage verifyRestrictAllRadioButtonSelected() {
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(restrictAllRadioBtn));
			Assert.assertTrue(restrictAllRadioBtnDiv.isSelected());
			backBtn.click();
			Reporter.log("Restrict all radio button is in selected state");
		} catch (Exception e) {
			Reporter.log("Restrict all radio button is not in selected state");
			Assert.fail("Restrict all radio button is not in selected state");
		}
		return this;
	}

	/**
	 * Verify gov account permissions content
	 */
	public LineSettingsPage verifyGovAccountPermissionsContent() {
		try {
			waitforSpinnerinProfilePage();
			Assert.assertTrue(isElementDisplayed(govAccountPermissionContent));
			Reporter.log("Gov account permission content displayed");
		} catch (Exception e) {
			Reporter.log("Gov account permission content not displayed");
			Assert.fail("Gov account permission content not displayed");
		}
		return this;
	}

	/**
	 * verify account Access
	 */
	public String checkAccountAccessAndSelect() {
		String accessIs = "";
		try {
			waitforSpinnerinProfilePage();
			waitFor(ExpectedConditions.visibilityOf(saveChangesBtn));		
			if (fullAccessBtn.isSelected()) {
				standardAccess.click();
				Reporter.log("Standard Access radio buttion is selected");
				accessIs = "standard";
			} else if (standardAccessBtn.isSelected()) {
				fullAccess.click();
				Reporter.log("Full Access radio buttion is selected");
				accessIs = "fullAccess";
				saveChangesBtn.click();
				waitFor(ExpectedConditions.visibilityOf(iAgreeToTheseTermsCheckBox));
				iAgreeToTheseTermsCheckBox.click();
			}
		} catch (Exception e) {
			Assert.fail(accessIs + " radio buttion is not displaying");
		}
		return accessIs;
	}

	/**
	 * Verify full access permissions.
	 */
	public LineSettingsPage verifyFullAccessOrStandardAccess(String accessName) {
		try {
			waitforSpinner();
			checkPageIsReady();
			if (accessName.contains("standard")) {
				Assert.assertTrue(standardAccessBtn.isSelected(), "Standard Access permission not updated");
				Reporter.log("Standard access permission updated");
			} else {
				Assert.assertTrue(fullAccessBtn.isSelected(), "Full access permission not updated");
				Reporter.log("Full access permission updated");
			}

		} catch (Exception e) {
			Assert.fail(accessName + " access permission not updated");
		}
		return this;
	}

	/**
	 * Click On I Agree To These Terms Check Box
	 */
	public LineSettingsPage clickOnIAgreeToTheseTermsCheckBox() {
		try {
			iAgreeToTheseTermsCheckBox.click();
			Reporter.log("Clicked on I agree to these terms check box");
		} catch (Exception e) {
			Assert.fail("I agree to these terms check box is not displayed");
		}
		return this;
	}

	/**
	 * Verify permission Changes Text Message
	 */
	public LineSettingsPage verifyPermissionChangesTextMessage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(permissionChangesTextMessage.isDisplayed(),
					"Your permission changes have been saved message is not displaying");
			Reporter.log("Your permission changes have been saved message is displaying");
		} catch (Exception e) {
			Assert.fail("Your permission changes have been saved message is not displaying");
		}
		return this;
	}
}
