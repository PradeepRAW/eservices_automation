/**
 * 
 */
package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class HistoricBillsPage extends CommonPage {

	@FindBy(css = "div.bb-past-bills-title")
	private WebElement pastbiltitle;

	@FindBy(xpath = "//div[@class='container bb-charge-summary d-none d-md-block d-lg-block']")
	private List<WebElement> pastbillstable;

	@FindBy(css = "div.bb-charge-summary div.no-padding.text-left button.past-bill-link.d-lg-block")
	private List<WebElement> androidpastbillstable;

	@FindBy(css = "div.bb-past-bill-row")
	private List<WebElement> androidPassBillsRows;

	@FindBy(xpath = "//button[contains(text(),'Back to your bill')]")
	private WebElement backtobill;

	private final String pageUrl = "/historical";

	/**
	 * 
	 * @param webDriver
	 */
	public HistoricBillsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public HistoricBillsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	public HistoricBillsPage verifyPageLoaded() {
		try {
			checkPageIsReady();

			waitFor(ExpectedConditions.visibilityOf(pastbiltitle));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ajax_loader")));
			verifyPageUrl();
			Reporter.log("EIP details page is loaded");
		} catch (Exception e) {
			Assert.fail("Fail to load EIP details page");
		}
		return this;
	}

	public HistoricBillsPage checkheadertext(String header) {
		try {
			if (pastbiltitle.getText().trim().equalsIgnoreCase(header))
				Reporter.log("Historic bills header is verified and header is:" + header);
			else
				Verify.fail("Hostoric bills page header is not:" + header);

		} catch (Exception e) {
			Verify.fail("Fail to load Historic bills page");
		}
		return this;
	}

	public HistoricBillsPage checkallpreviousbillscontent() {
		try {
			boolean elseloop = false;
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : androidPassBillsRows) {
					String summaryPDF = webElement
							.findElement(By
									.xpath("//div[contains(@class, 'bb-charge-summary')]//div[contains(@class, 'no-padding')]/button[contains(@class,'past-bill-link no-padding ng-star-inserted')]/span"))
							.getText();
					String detailedPDF = webElement
							.findElement(By
									.xpath("//div[contains(@class, 'bb-charge-summary')]//div[contains(@class, 'no-padding')]//button[@class='past-bill-link ng-star-inserted']//span[text()='Detailed PDF']"))
							.getText();
					if (!summaryPDF.equalsIgnoreCase("Summary PDF") || !detailedPDF.equalsIgnoreCase("Detailed PDF")) {
						elseloop = true;
					}
				}
				Assert.assertFalse(elseloop, "Summary PDF or Details PDF is not displayed in rows");
			} else {
				for (WebElement row : pastbillstable) {
					String summarypdf = row
							.findElement(
									By.xpath("//div[@class='bb-download-past-summary-bill d-lg-block d-xs-none ng-star-inserted']//span"))
							.getText().trim();
					String detailedpdf = row.findElement(By.xpath("//div[@class='offset-1 d-lg-block d-xs-none ng-star-inserted']//span"))
							.getText().trim();
					String viewbill = row.findElement(By.xpath("//div[contains(@class,'bb-view-bill')]/button[@class='past-bill-link d-lg-block']"))
							.getText().trim();

					if (!summarypdf.equalsIgnoreCase("Summary PDF") || !detailedpdf.equalsIgnoreCase("Detailed PDF")
							|| !viewbill.equalsIgnoreCase("View bill")) {
						elseloop = true;
						break;
					}
				}
				Assert.assertFalse(elseloop, "Summary PDF or Details PDF is not displayed in rows");
			}
		} catch (Exception e) {
			Assert.fail("Fail to load Historic bills page past bills");
		}
		return this;
	}

	public HistoricBillsPage clickBacktobill() {
		try {
			scrollToElement(backtobill);
			backtobill.click();
			Reporter.log("back to bill button is clicked");
		} catch (Exception e) {
			Assert.fail("Back to bill button is not displayed");
		}
		return this;
	}

	/**
	 * Click view Bill
	 * 
	 * @return
	 */
	public HistoricBillsPage clickviewbill() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				androidpastbillstable.get(0).click();
			} else {
				pastbillstable.get(0).findElement(By.xpath(".//button[@class='past-bill-link d-lg-block']")).click();
			}
			Reporter.log("viewbill link is clicked");
		} catch (Exception e) {
			Assert.fail("view bill link is not exsting");
		}
		return this;
	}

	public HistoricBillsPage Clickongivenbillcycle(String billDate) {
		try {
			if(getDriver() instanceof AppiumDriver)
			{
			getDriver().findElement(By.xpath(".//div[contains(@class,'no-padding text-left')]/button[@class='past-bill-link d-lg-block' and contains(@aria-label,'"+billDate+ "')]")).click();
			}
			else
				getDriver().findElement(By.xpath("//div[contains(@class,'bb-view-bill')]/button[@class='past-bill-link d-lg-block' and contains(@aria-label,'"+billDate+ "')]")).click();	
			Reporter.log("view pastbill link clicked");
		} catch (Exception e) {
			Assert.fail("Fail to locate pastbill link");
		}
		return this;
	}

	public HistoricBillsPage clickonviewbill() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				androidpastbillstable.get(0).click();
			} else {
				//
				int count = pastbillstable.size();
				pastbillstable.get(count - 1).findElement(By.xpath(".//button[@class='past-bill-link d-lg-block']"))
						.click();
			}
			Reporter.log("viewbill link is clicked");
		} catch (Exception e) {
			Assert.fail("view bill link is not exsting");
		}
		return this;
	}

}
