package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class AutoPayCancelConfirmationPage extends CommonPage {
	
	@FindBy(css = "button[ng-click*='redirectToHome']")
	private WebElement returnToHome;

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;
	
	@FindBy(css = "h4.h3-title")
	private WebElement pageLoadElement;

	@FindAll({ @FindBy(css = "#cancel-confirmation-page .ui_mobile_headline"), @FindBy(css = "h4.h3-title.text-center.ng-binding") })
	private WebElement apCancelHeaderMobile;
	
	@FindAll({ @FindBy(css = "h4.h4-title"), @FindBy(css = "cancel-autopay-confirmation h4") })
	private WebElement apCancelHeader;

	@FindBy(css = "div.p-l-10.body-copy-description.text-black")
	private List<WebElement> cancelApText;

	private final String pageUrl = "/autopay/cancelConfirmation";

	/**
	 * Constructor
	 *
	 * @param webDriver
	 */
	public AutoPayCancelConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AutoPayCancelConfirmationPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public AutoPayCancelConfirmationPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			//waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
			Reporter.log("Autopay cancel confirmation page is loaded");
		} catch (Exception e) {
			Assert.fail("Autopay cancel confirmation page is not loaded");
		}
		return this;
	}

	/**
	 * click tmobile home icon to return to home page
	 */
	public AutoPayCancelConfirmationPage clickreturnToHome() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(returnToHome));
			returnToHome.click();
			Reporter.log("Clicked on return to Home");
		} catch (Exception e) {
			Assert.fail("Failed to click return to Home");
		}
		return this;
	}

	/**
	 * verify Cancel AutoPay confirmation page elements
	 */
	public AutoPayCancelConfirmationPage verifyCancelAutoPayConfirmationPageElements() {
		try {
			verifyAutopayCancelled();
			verifyAutoPayCancelMessage();
			verifyReturnToHomeBtn();
		} catch (Exception e) {
			Assert.fail("Fail to verify the Cancel Auto pay Confirmation Page elements");
		}
		return this;
	}

	/**
	 * verify auto pay cancelled header is displayed
	 *
	 * @return boolean
	 */
	public AutoPayCancelConfirmationPage verifyAutopayCancelled() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				apCancelHeaderMobile.isDisplayed();
				apCancelHeaderMobile.getText().contains("AutoPay Cancelled");
				Reporter.log("Verified auto pay cancelled");
			} else {
				apCancelHeader.isDisplayed();
				getDriver().getCurrentUrl().endsWith("cancelConfirmation");
				apCancelHeader.getText().equals("AUTOPAY CANCELED!");
				Reporter.log("Verified auto pay cancelled");
			}
		} catch (Exception e) {
			Assert.fail("Autopay cancel page not found");
		}
		return this;
	}

	/**
	 * verify auto pay status on confirmation page
	 *
	 * @return boolean
	 */
	public AutoPayCancelConfirmationPage verifyAutoPayCancelMessage() {
		try {
			for (WebElement msg : cancelApText) {
				if (msg.isDisplayed() && msg.getText().contains("You will no longer receive the AutoPay discount.")) {
					Reporter.log("Verified auto pay message");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Auto pay message component missing");
		}

		return this;
	}

	/**
	 * verify return to home button is displayed
	 *
	 * @return boolean
	 */
	public AutoPayCancelConfirmationPage verifyReturnToHomeBtn() {
		try {
			returnToHome.isDisplayed();
			Reporter.log("Return to Home Button is verified");
		} catch (Exception e) {
			Assert.fail("Fail to verify the Return to Home button ");
		}
		return this;
	}

}
