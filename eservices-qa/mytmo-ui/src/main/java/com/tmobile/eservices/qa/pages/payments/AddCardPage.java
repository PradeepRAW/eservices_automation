package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author pshiva
 *
 */
public class AddCardPage extends CommonPage {

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	private By waitSpinner = By.cssSelector("div.circle");

	@FindBy(css = "div.Display5")
	private WebElement replaceModelheader;

	@FindBy(css = "span.p-l-10")
	private WebElement replaceModelCardNumber;

	@FindBy(css = "div.col-9")
	private WebElement CardReplaceMessage;

	@FindBy(css = "div.black")
	private WebElement replaceModelSubHeader;

	@FindBy(css = "div.btns-row  button.PrimaryCTA-accent")
	private WebElement replaceModelContinueBtn;

	@FindBy(css = "div.btns-row  button.SecondaryCTA-accent")
	private WebElement replaceModelcancelBtn;

	@FindBy(css = "span[tooltip-position='top']")
	private WebElement toolTipText;

	@FindBy(css = "span.mt-lg-1")
	private WebElement SaveThispaymentErrorMsg;

	@FindBy(linkText = "What is this?")
	private WebElement whatIsThisCvvlink;

	@FindBy(css = "i.tooltip-icon")
	private WebElement toolTip;

	@FindBy(className = "cvv_visaImg")
	private WebElement cvvModel;

	@FindBy(css = "h3.no-margin.button-title")
	private WebElement pageHeader;

	@FindAll({ @FindBy(id = "name_on_card"), @FindBy(id = "cardName") })
	private WebElement cardName;

	@FindAll({ @FindBy(id = "card_number"), @FindBy(id = "creditCardNumber") })
	private WebElement cardNumber;

	@FindBy(css = "div[ng-if*='ctrl.paymentMethodBladeHelperService.negativeCardError']")
	private WebElement cardErrorMessage;

	@FindAll({@FindBy(id = "expirationMonthYear"), @FindBy(id="expirationDate")})
	private WebElement expiryMonth;

	@FindBy(css = "div[ng-if*='ctrl.paymentMethodBladeHelperService.isExpirationMonthValid'] div.errormsg")
	private WebElement expiryMonthErrorMessage;

	@FindBy(id = "expiration_year")
	private WebElement expiryYear;

	@FindAll({@FindBy(id = "expirationMonthYear"), @FindBy(id="expirationDate")})
	private WebElement expiryYearAndMonth;

	@FindAll({ @FindBy(id = "CVV"), @FindBy(id = "cvvNumber") })
	private WebElement cvvCode;

	@FindBy(css = "div[ng-if*='addcardForm.CVV.$invalid'] div.errormsg")
	private WebElement cvvCodeErrorMessage;

	@FindAll({ @FindBy(id = "Zip"), @FindBy(id = "zipCode") })
	private WebElement zip;

	@FindAll({ @FindBy(css = "div.card-information-title h3"), @FindBy(css = ".Display3"),
			@FindBy(css = "div.card-information-title span") })
	private WebElement cardHeader;

	@FindBy(css = "div.no-margin.title-payment span.tab-pressed.ng-binding")
	private WebElement cardHeaderMobile;

	@FindAll({ @FindBy(css = "div#continue button"), @FindBy(css = "button.PrimaryCTA") })
	private WebElement continueAddCard;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement cancelBtn;

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button.SecondaryCTA") })
	private WebElement backBtn;

	@FindAll({ @FindBy(css = "label[for='name_on_card']"), @FindBy(css = "label[for='cardName']") })
	private WebElement nameOnCardTitle;

	@FindAll({ @FindBy(css = "label[for='card_number']"), @FindBy(css = "label[for='creditCardNumber']") })
	private WebElement creditCardNoTitle;

	@FindAll({ @FindBy(css = "label[for='expirationMonthYear']"), @FindBy(css = "label[for='expirationDate']") })
	private WebElement expDateTitle;

	@FindAll({ @FindBy(css = "label[for='CVV']"), @FindBy(css = "label[for='cvvNumber']") })
	private WebElement cvvTitle;

	@FindAll({ @FindBy(css = "label[for='ZIP']"), @FindBy(css = "label[for='zipCode']"),@FindBy(css = "label[for='Zip']") })
	private WebElement zipCodeTitleSoftGoods;

	@FindAll({ @FindBy(css = "label[for='ZIP']"), @FindBy(css = "label[for='zipCode']") })
	private WebElement zipCodeTitle;

	@FindBy(css = "label[for='zip']")
	private WebElement zipCodeTitleHardGoods;

	@FindBy(id = "Zip")
	private WebElement zipCode;

	@FindBy(css = "div[ng-if*='addcardForm.Zip.$invalid'] div.errormsg")
	private WebElement zipCodeErrorMessage;

	@FindAll({ @FindBy(css = "#hardGoodFlow span.check1"), @FindBy(css = "span.legal") })
	private WebElement useThisBillingAddressCheckbox;

	@FindAll({ @FindBy(css = "#hardGoodFlow p.body-copy-description-regular.black.ng-binding"),
			@FindBy(css = "span.lineheight") })
	private WebElement useThisBillingAddressHeader;

	@FindAll({ @FindBy(css = "#hardGoodFlow div.ng-binding"), @FindBy(css = "div[tabindex='10']") })
	private WebElement useThisBillingAddressLines;

	@FindAll({ @FindBy(css = "label[for='address_line1']"), @FindBy(css = "label[for='address1']") })
	private WebElement addressLine1Title;

	@FindAll({ @FindBy(css = "label[for='address_line2']"), @FindBy(css = "label[for='address2']") })
	private WebElement addressLine2Title;

	@FindAll({ @FindBy(id = "address_line1"), @FindBy(css = "input[name='address1']") })
	private WebElement addressLine1;

	@FindAll({ @FindBy(id = "address_line2"), @FindBy(css = "input[name='address2']") })
	private WebElement addressLine2;

	@FindBy(id = "city")
	private WebElement city;

	@FindAll({ @FindBy(id = "state"), @FindBy(id = "model"), @FindBy(css = "mat-select[role=listbox]") })
	private WebElement state;

	@FindBy(css = "label[for='city']")
	private WebElement cityTitle;

	@FindAll({ @FindBy(css = "label[for='state']"), @FindBy(css = "p.pb-1") })
	private WebElement stateTitle;

	@FindBy(css = "#Zip")
	private WebElement zipPlaceholderSoftGoods;

	@FindAll({ @FindBy(id = "zipHardGoods"), @FindBy(css = "input[name='zip']") })
	private WebElement zipHardGoods;

	@FindBy(css = "button[ng-click*= 'saveCardCheck']")
	private WebElement creditCardOverWriteDialogueBtn;

	@FindAll({ @FindBy(css = "label.switch.mt-1"), @FindBy(css = "label[for ='checkbox2']>div>span"),
		@FindBy(css="#saveCardToggleLabel div span")})
	private WebElement savePaymentCheckBox;

	@FindAll({ @FindBy(css = "input[formcontrolname='saveCardToggle']"),
			@FindBy(css = "label#savePaymentMethodLabel input") })
	private WebElement savePaymentCheckboxInput;

	/*
	 * @FindBy(css = "label.switch.mt-1") private WebElement
	 * savePaymentCheckBox;
	 */

	@FindAll({ @FindBy(css = "span[ng-show='!$ctrl.paymentMethodBladeHelperService.isCreditCardNumberValid'] div"),
			@FindBy(id = "chk_card_no_error"), @FindBy(id = "vcard"),
			@FindBy(xpath = "//div[contains(@ng-if,'paymentMethodBladeHelperService.negativeCardError')]") })
	private WebElement ccError;

	@FindBy(css = "span.padding-top-xsmall.invalid.text-error")
	private List<WebElement> errorTexts;

	@FindAll({ @FindBy(css = "span.invalid.text-error"), @FindBy(css = "p.errormessage") })
	private List<WebElement> errorMsgs;

	@FindBy(css = "#pageContainer")
	private WebElement pageSpinner;

	@FindAll({ @FindBy(css = "div[class='d-flex'] i+span"), @FindBy(css = "div.fine-print-body.ng-binding") })
	private WebElement errWalletsizereached;

	private By cardhidepage = By.xpath("//div[@class='screen-lock']");

	@FindBy(css="label>input.saveCheckbox")
	private WebElement savePaymentValue;

	@FindAll({@FindBy(css = "div>input#cardNickname"),@FindBy(css = "input#nickname")})
	private WebElement nickName;

	@FindAll({ @FindBy(css = "label[for='nickName']"), @FindBy(css = "label[for='cardNickname']"),
		@FindBy(css="label[for*='nickname']")})
	private WebElement nickNameLabel;

	@FindAll({ @FindBy(css = "label[for = 'defaultCheckbox']>div>span"),
			@FindBy(css = "label[aria-label='Set as Default']"),@FindBy(css="input#defaultCheckbox"),
			@FindBy(css="input[name='defaultPaymentMethodIndicator']")})
	private WebElement setAsDefaultCheckbox;
	
	@FindBy(css = "input[name='defaultPaymentMethodIndicator']")
	private WebElement setAsDefaultInput;

	@FindAll({ @FindBy(xpath = "//p/span[contains(text(),'Set as default')]"),
			@FindBy(css = "label[aria-label='Set as Default']+span") })
	private WebElement setAsDefaultCheckboxLabel;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_name')]"), @FindBy(css = "label[for='accountName']") })
	private WebElement nameOnAccountHeader;
	
	@FindBy(css = "span.check1")
	private List<WebElement> savepaymentcheckboxList;
	
	
	
	private final String pageUrl = "addcard";

	@FindBy(css = "button.PrimaryCTA")
	private WebElement saveChangesBtn;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public AddCardPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AddCardPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * verify card information header
	 * 
	 * @param msg
	 * @return
	 */
	public AddCardPage verifyCardPageLoaded(String msg) {
		try {
			checkPageIsReady();
			verifyPageUrl();
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(cardHeaderMobile.getText().trim().equalsIgnoreCase(msg));
			} else {
				Assert.assertTrue(cardHeader.getText().trim().equalsIgnoreCase(msg));
			}
			Reporter.log("Verified the Card info Header details");
		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	public AddCardPage clickBackBtn() {
		try {
			backBtn.click();
		} catch (Exception e) {
			Assert.fail("Failed to click back button");
		}
		return this;
	}

	/**
	 * click continue button for add Card
	 */
	public AddCardPage clickContinueAddCard() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(continueAddCard));
			clickElementWithJavaScript(continueAddCard);
			Reporter.log("Clicked on Continue button");

		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	/**
	 * verify use this billing address checkbox is check state or not
	 */
	public AddCardPage verifyUseThisBillingAddressCheckboxIsSelected() {
		try {
			useThisBillingAddressCheckbox.isSelected();
			Reporter.log("Billing Address Checkbox is in check state");
		} catch (Exception e) {
			Assert.fail(" Billing Address Checkbox is not in check state");
		}
		return this;
	}

	/**
	 * verify use this billing address checkbox is found or not
	 */
	public AddCardPage verifyUseThisBillingAddressCheckbox() {
		try {
			useThisBillingAddressCheckbox.isDisplayed();
			Reporter.log(" verifed Billing Address Checkbox is displayed");
		} catch (Exception e) {
			Assert.fail(" Billing Address Checkbox is not not found");
		}
		return this;
	}

	/**
	 * verify use this billing address checkbox is found or not
	 */
	public AddCardPage verifyUseThisBillingAddressCheckboxIsSuppressed() {
		try {
			Assert.assertTrue(!useThisBillingAddressCheckbox.isDisplayed());
			Reporter.log(" verifed Billing Address Checkbox is displayed");
		} catch (Exception e) {
			Assert.fail(" Billing Address Checkbox is not not found");
		}
		return this;
	}

	/**
	 * click on Use billing address for payment info - Hard Goods
	 */
	public AddCardPage clickUseThisBillingAddressCheckbox() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
			useThisBillingAddressCheckbox.click();
			Reporter.log("Clicked on Use this Billing Address Checkbox");
		} catch (Exception e) {
			Verify.fail("Failed to click on Use this Billing Address Checkbox");
		}
		return this;
	}

	/**
	 * Fill all the card details and click on continue
	 *
	 * @param payment
	 */
	public AddCardPage fillCardDetails(Payment payment) {
		try {
			cardName.sendKeys(payment.getNameOnCard());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			cardNumber.sendKeys(payment.getCardNumber());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			expiryYearAndMonth.sendKeys(payment.getExpiryMonthYear());
			
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			zip.sendKeys(payment.getZipCode());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			cvvCode.sendKeys(payment.getCvv());
			Reporter.log("Filled the Card Info details");
			if (payment.getNickName() != null) {
				nickName.sendKeys(payment.getNickName());
			}
		} catch (Exception e) {
			Assert.fail("Failed to fill the card info details for Payment");
		}
		return this;
	}

	/**
	 * Fill all the card details and click on continue
	 *
	 * @param payment
	 */
	public AddCardPage fillCardInfo(Payment payment) {
		try {
			cardName.clear();
			cardName.sendKeys(payment.getNameOnCard());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			cardNumber.sendKeys(payment.getCardNumber());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			expiryMonth.sendKeys(payment.getExpiryMonthYear());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			zip.sendKeys(payment.getZipCode());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			cvvCode.clear();
			cvvCode.sendKeys(payment.getCvv());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			if (payment.getNickName() != null) {
				nickName.sendKeys(payment.getNickName());
			}
			Reporter.log("Filled the Card Info details");
		} catch (Exception e) {
			Assert.fail("Failed to fill the card info details for Payment");
		}
		return this;
	}

	/**
	 * Fill invalid card payment details
	 *
	 * @param payment
	 */
	public AddCardPage fillinvalidCardDetails(Payment payment) {
		try {
			cardName.sendKeys(payment.getNameOnCard());
			cardNumber.sendKeys(payment.getCardNumber());
			cvvCode.sendKeys(payment.getCvv());
			Reporter.log("Filled the invalid card details");
		} catch (Exception e) {
			Assert.fail("Failed to fill the invalid Card Details");
		}
		return this;
	}

	/**
	 * Verify Card Number place Holder
	 * 
	 * @return true/false
	 */
	public AddCardPage verifyCardNumberplaceHolder(String msg) {
		try {
			getplaceHolderValue(cardNumber, msg);
			Reporter.log("Verified card number place holder");
		} catch (Exception e) {
			Verify.fail("Failed to verify card Number place holder");
		}

		return this;
	}

	/**
	 * Verify Card Name place Holder
	 * 
	 * @return true/false
	 */
	public AddCardPage verifyCardNameplaceHolder(String msg) {
		try {
			getplaceHolderValue(cardName, msg);
			Reporter.log("Verified the card Name place holder");
		} catch (Exception e) {
			Verify.fail("Failed to verify Card Name place holder");
		}
		return this;
	}

	/**
	 * Verify Card Expiry date place Holder
	 * 
	 * @return true/false
	 */
	public AddCardPage verifyCardExpDateplaceHolder(String msg) {
		try {
			getplaceHolderValue(expiryMonth, msg);
			Reporter.log("Verified the card Exp Date place holder value");
		} catch (Exception e) {
			Verify.fail("Failed to verify Card Exp Date place Holder");
		}
		return this;
	}

	/**
	 * Verify Card expire Year place Holder
	 * 
	 * @return true/false
	 */
	public AddCardPage verifyCardExpiryYearplaceHolder(String msg) {
		try {
			getplaceHolderValue(expiryYear, msg);
			Reporter.log("Verified the card Expiry Year place holder value");
		} catch (Exception e) {
			Verify.fail("Failed to verify Card Expiry year place holder");
		}
		return this;
	}

	/**
	 * Verify Card Cvv Code place Holder
	 * 
	 * @return true/false
	 */
	public AddCardPage verifyCardCVVCodeplaceHolder(String msg) {
		try {
			getplaceHolderValue(cvvCode, msg);
			Reporter.log("CVV code place holder verified");
		} catch (Exception e) {
			Verify.fail("Failed to verify card CVV code place holder");
		}
		return this;
	}

	/**
	 * Verify Card ZipCode place Holder
	 * 
	 * @return true/false
	 */
	public AddCardPage verifyCardZipCodeplaceHolder(String msg) {
		try {
			getplaceHolderValue(zip, msg);
			Reporter.log("Verified the card zip code place holder value");
		} catch (Exception e) {
			Verify.fail("Failed to verify card zip code place holder value");
		}
		return this;
	}

	public void getplaceHolderValue(WebElement element, String placeHolder) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element));
			//element.getAttribute("placeholder").contains(placeHolder);
			if(element.findElement( By.xpath("./parent::div/label")).getText().contains(placeHolder))Reporter.log("Place holder is:"+placeHolder);
			else Verify.fail("Place holder is not :"+placeHolder);
		} catch (Exception e) {
			Verify.fail(placeHolder + " Place holder not found");
		}
	}

	/**
	 * verify Zip code(Soft goods) title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyZipCodeSoftGoods() {
		try {
			zipCodeTitleSoftGoods.isDisplayed();
			zipPlaceholderSoftGoods.isDisplayed();
			Reporter.log("Verified the zip code soft goods");
		} catch (Exception e) {
			Verify.fail("Failed to verify zip code soft goods");
		}
		return this;
	}

	/**
	 * verify Zip code(Hard goods) title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyZipCodeHardGoods() {
		try {
			zipCodeTitleHardGoods.isDisplayed();
			zipHardGoods.isDisplayed();
			Reporter.log("Verified the zip code hard goods");
		} catch (Exception e) {
			Verify.fail("Failed to verify the zip code hard goods");
		}

		return this;
	}

	/**
	 * verify Use Billing Address (Hard goods) field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyUseThisBillingAddress() {
		try {
			useThisBillingAddressCheckbox.isDisplayed();
			useThisBillingAddressHeader.isDisplayed();
			useThisBillingAddressLines.isDisplayed();
			Reporter.log("Verified the Use this Billing Address");
		} catch (Exception e) {
			Verify.fail("Failed to verify the Use this Billing Address");
		}

		return this;
	}

	/**
	 * verify Use Billing Address text are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyUseThisBillingAddressText() {
		try {

			useThisBillingAddressHeader.isDisplayed();
			Assert.assertTrue(useThisBillingAddressHeader.getText().equals("Use billing address for payment info"),
					"use this billing address is not found");
			Reporter.log("Verified the Use this Billing Address text");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Use this Billing Address text");
		}

		return this;
	}

	public AddCardPage verifyUseThisBillingAddressTextIsSuppressed() {
		try {

			Assert.assertTrue(!useThisBillingAddressHeader.isDisplayed(), "use this billing address is  found");
			Reporter.log("Verified the Use this Billing Address text");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Use this Billing Address text");
		}

		return this;
	}

	/**
	 * verify Use Billing Address (Hard goods) is not displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyUseThisBillingAddressIsNotDisplayed() {
		try {
			Assert.assertTrue(!useThisBillingAddressLines.isDisplayed(), "address lines are displayed");
			Reporter.log("Verified the Use this Billing Address lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Use this Billing Address lines");
		}

		return this;
	}

	/**
	 * verify Addressline1(Hard goods) title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyAddressLine1Header() {
		try {
			addressLine1Title.isDisplayed();
			addressLine1.isDisplayed();
			Reporter.log("Verified the Addres Line1 Header Value");
		} catch (Exception e) {
			Verify.fail("Failed to verify the Address Line Header value");
		}

		return this;
	}

	/**
	 * verify Addressline2(Hard goods) title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyAddressLine2Header() {
		try {
			addressLine2Title.isDisplayed();
			addressLine2.isDisplayed();
			Reporter.log("Verified the Address Line 2 Header value");
		} catch (Exception e) {
			Verify.fail("Failed to verify the Address line 2 Header");
		}

		return this;
	}

	/**
	 * verify use this address fields are suppressed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyAddressFieldsAreSuppressed() {
		try {
			Assert.assertTrue(!addressLine1.isDisplayed(), "address line1 field is displayed");
			Assert.assertTrue(!addressLine2.isDisplayed(), "address line2 field is displayed");
			Assert.assertTrue(!city.isDisplayed(), "city field is displayed");
			Assert.assertTrue(!state.isDisplayed(), "state field is displayed");
			Assert.assertTrue(!zipHardGoods.isDisplayed(), "address line2 field is displayed");
			Reporter.log("Verified address fields are suppressed ");
		} catch (Exception e) {
			Assert.fail("Failed to Verified address fields are suppressed");
		}

		return this;
	}

	/**
	 * verify City(Hard goods) title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyCityHeader() {
		try {
			cityTitle.isDisplayed();
			city.isDisplayed();
			Reporter.log("Verified the City Header Value");
		} catch (Exception e) {
			Verify.fail("Failed to verify the City Header value");
		}
		return this;
	}

	/**
	 * verify State(Hard goods) title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyStateHeader() {
		try {
			stateTitle.isDisplayed();
			state.isDisplayed();
			Reporter.log("Verified the State Header Value");
		} catch (Exception e) {
			Verify.fail("Failed to verify the State Header Value");
		}
		return this;
	}

	/**
	 * verify Name on card title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyNameOnCardHeader() {
		try {
			nameOnCardTitle.isDisplayed();
			cardName.isDisplayed();
			Reporter.log("Verified the Name on Card Header");
		} catch (Exception e) {
			Verify.fail("Failed to verify the Name on Card Header");
		}
		return this;
	}

	/**
	 * verify credit card number title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyCreditCardNoHeader() {
		try {
			creditCardNoTitle.isDisplayed();
			cardNumber.isDisplayed();
			Reporter.log("Verified the Credit Card No. Header");
		} catch (Exception e) {
			Verify.fail("Fail to verify the Credit Card No. Header");
		}

		return this;
	}

	/**
	 * verify Exp date title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyExpDateHeader() {
		try {
			expDateTitle.isDisplayed();
			Reporter.log("Verified the Exp Date Header");
		} catch (Exception e) {
			Verify.fail("Failed to verify the Exp Date Header");
		}
		return this;
	}

	/**
	 * verify CVV title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyCVVHeader() {
		try {
			cvvTitle.isDisplayed();
			cvvCode.isDisplayed();
			Reporter.log("Verified the CVV Header");
		} catch (Exception e) {
			Verify.fail("Fail to verify the CVV Header");
		}
		return this;
	}

	/**
	 * verify Exp date title and field are displayed
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyZipeHeader() {
		try {
			zipCodeTitle.isDisplayed();
			zipCode.isDisplayed();

			Reporter.log("Verified the Exp Date Header");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Exp Date Header");
		}
		return this;
	}

	public AddCardPage acceptCreditCardOverWriteDialogue() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("di_modal_message_loading")));
			waitFor(ExpectedConditions.elementToBeClickable(creditCardOverWriteDialogueBtn));
			clickElementWithJavaScript(creditCardOverWriteDialogueBtn);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("di_modal_message_loading")));
			Reporter.log("Verified the Credit Card Over Write Dialogue");
		} catch (Exception e) {
			Assert.fail("Failed to verify the accept Credit Card Over Write Dialogue");
		}
		return this;
	}

	/**
	 * Verify that Save Payment checkbox is present
	 */
	public AddCardPage verifySavePaymentMethodCheckBox() {
		try {
			savePaymentCheckBox.isDisplayed();
			Reporter.log("Save Payment Checkbox is present");
		} catch (Exception e) {
			Verify.fail("Failed to verify Save Payement CheckBox. Checkbox should be present");
		}
		return this;
	}

	/**
	 * Verify that Save Payment checkbox is NOT present
	 */
	public AddCardPage verifyNoSavePaymentMethodCheckBox() {
		try {
			waitFor(ExpectedConditions.visibilityOf(backBtn));
			if (!savePaymentCheckBox.isDisplayed()) {
				Reporter.log("Verified Save Payment Method checkbox is not displayed.");
			} else {
				Assert.fail("Failed to verify Save Payement CheckBox. Checkbox should NOT be present");
			}
		} catch (Exception e) {
			if (e.toString().contains("NoSuchElementException")) {
				Reporter.log("Verified Save Payment Method checkbox is not displayed.");
			} else {
				Assert.fail("Failed to verify Save Payement CheckBox");
			}
		}
		return this;
	}

	/**
	 * Soft/Hard Goods Verify all fields in Add Card page
	 */
	public AddCardPage verifyAddCardAllFieldsSoftOrHardGoods() {
		try {
			verifyNameOnCardHeader();
			Reporter.log("Name on Card Title and Field are displayed");
			verifyCreditCardNoHeader();
			Reporter.log("Credit Card Number Title and Field are displayed");
			verifyExpDateHeader();
			Reporter.log("Exp Date Title and Field are displayed");
			verifyCVVHeader();
			Reporter.log("CVV Title and Field are displayed");
			Verify.assertTrue(continueAddCard.isDisplayed(), "Continue button is not displayed");
			Reporter.log("Continue CTA is displayed");
			Verify.assertTrue(backBtn.isDisplayed(), "Back button is not displayed");
			Reporter.log("Back CTA is displayed");
			if (zipCodeTitleSoftGoods.isDisplayed()) {
				verifyZipCodeSoftGoods();
				Reporter.log("Zip code Title and Field are displayed");
				Reporter.log("Soft Goods page is displayed");
			} else {
				verifyUseThisBillingAddress();
				Reporter.log("Use billing address for payment info elements are displayed");
				clickUseThisBillingAddressCheckbox();
				verifyAddressLine1Header();
				Reporter.log("AddressLine1 Title and Field are displayed");
				verifyAddressLine2Header();
				Reporter.log("AddressLine2 Title and Field are displayed");
				verifyCityHeader();
				Reporter.log("City Title and Field are displayed");
				verifyStateHeader();
				Reporter.log("State Title and Field are displayed");
				verifyZipCodeHardGoods();
				Reporter.log("Zip code(Hard Goods) Title and Field are displayed");
				Reporter.log("Hard Goods page is displayed");
			}
		} catch (Exception e) {
			Verify.fail("Exception occured while verifying Card page Hard/Soft goods elements");
		}
		return this;
	}

	/**
	 * verify invalid cc error message
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyInvalidCCErrorMsg() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(cardhidepage));
			ccError.isDisplayed();
			Reporter.log("Invalid error message displayed");
		} catch (Exception e) {
			Assert.fail("Error message component missing");
		}
		return this;
	}

	/**
	 * verify invalid cc error message
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyMCBINCCErrorMsg() {
		try {
			ccError.isDisplayed();
			Assert.assertTrue(ccError.getText().equals("Enter a valid card number."),
					"Error message is not displayed correctly.");
			Reporter.log("Invalid error message displayed");
		} catch (Exception e) {
			Assert.fail("Error message component missing");
		}
		return this;
	}

	/**
	 * verify error messages for empty fields
	 * 
	 * @param message
	 * @return boolean
	 */
	public boolean verifyErrorTextMessage(String message) {
		try {
			for (WebElement error : errorTexts) {
				if (error.isDisplayed() && error.getText().contains(message)) {
					return true;
				}
			}
		} catch (Exception e) {

			Reporter.log("Error message" + message + "component missing");
			Verify.fail("Error message" + message + "component missing");
		}
		return false;
	}

	public AddCardPage verifyAndSelectSavePaymentOption() {
		try {
			savePaymentCheckBox.isDisplayed();
			if (!savePaymentValue.isSelected()) {
				savePaymentCheckBox.click();
			}
			Reporter.log("Save payment method option is selected");
		} catch (Exception e) {
			Reporter.log("Save payment method option is not displayed");
		}
		return this;
	}

	public AddCardPage verifyAndSelectSavePaymentCheckbox() {
		try {
			savePaymentCheckBox.isDisplayed();
			savePaymentCheckBox.click();

			Reporter.log("Save payment method option is selected");
		} catch (Exception e) {
			Reporter.log("Save payment method option is not displayed");
		}
		return this;
	}

	public AddCardPage Checkdisabilityofsavepayment() {
		try {

			if (savePaymentValue.isEnabled())
				Assert.fail("Save payment mehod is enble");
			else
				Reporter.log("Save payment method check box is disable");

		} catch (Exception e) {
			Assert.fail("Save payment method check box is not displayed ");
		}
		return this;
	}

	public AddCardPage verifyWalletsizereachedtext(String reqText) {
		try {

			if (errWalletsizereached.getText().contains(reqText))
				Reporter.log("Wallet size reached text is:" + reqText);
			else
				Assert.fail("Wallet size reached text is:" + errWalletsizereached.getText());
		} catch (Exception e) {
			Assert.fail("Wallet size reached text is not displayed ");
		}
		return this;
	}

	/**
	 * Verify Expiration date Error Msg
	 * 
	 * @return true/false
	 */
	public AddCardPage verifyExpirationdateErrorMsg(String errorMsg) {
		try {
			for(WebElement errorMessage :errorMsgs)
			{
				if(errorMessage.getText().contains(errorMsg))
				{
					Assert.assertTrue(true);
					Reporter.log("Error message exists");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Expiration error message component not found");
		}
		return this;
	}

	/**
	 * verify Page header is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyPageHeader(String pageHeaderText) {
		try {
			pageHeader.isDisplayed();
			Assert.assertTrue(pageHeader.getText().equals(pageHeaderText), "page header is not found");
			Reporter.log("Page header is displayed");
		} catch (Exception e) {
			Assert.fail("page header is not found");
		}
		return this;
	}

	/**
	 * verify continue button is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyContinueBtn() {
		try {
			Assert.assertTrue(continueAddCard.isDisplayed(),"Continue button in not displayed");
			Reporter.log("continue button is displayed");
		} catch (Exception e) {
			Assert.fail("continue button is not found");
		}
		return this;
	}

	/**
	 * verify cancel button is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyCancelBtn() {
		try {
			cancelBtn.isDisplayed();

			Reporter.log("cancel button is displayed");
		} catch (Exception e) {
			Assert.fail("cancel button is not found");
		}
		return this;
	}

	/**
	 * verify use this billing address fields is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyBillingAddressFields() {
		try {
			verifyAddressLine1Header();
			verifyAddressLine2Header();
			verifyCityHeader();
			verifyStateHeader();
			verifyZipCodeHardGoods();

			Reporter.log("use this filling fields is displayed");
		} catch (Exception e) {
			Assert.fail("use this filling fields is not found");
		}
		return this;
	}

	/**
	 * click what is this? link for cvv model
	 */
	public AddCardPage clickWhatIsThisCvvLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(whatIsThisCvvlink));
			whatIsThisCvvlink.isDisplayed();
			whatIsThisCvvlink.click();
			Reporter.log("Clicked on what is this? link");

		} catch (Exception e) {
			Assert.fail("what is this? link is not found");
		}
		return this;
	}

	/**
	 * verify cvv model is displayed or not
	 */
	public AddCardPage verifyCvvModel() {
		try {

			cvvModel.isDisplayed();

			Reporter.log("cvv model is displayed");

		} catch (Exception e) {
			Assert.fail("cvv model is not displayed");
		}
		return this;
	}

	public AddCardPage addCardpageElements(String savePaymentErrorMsg, String toolTipMsg) {
		try {

			verifyAddCardFieldLabels();
			verifyContinueBtn();
			verifyCancelBtn();
			clickWhatIsThisCvvLink();
			verifyCvvModel();
			clickOutModel();
			verifySavePaymentMethodCheckBox();
			verifySaveThisPaymentMethodCheckBoxIsUncheck();
			verifyAndSelectSavePaymentCheckbox();
			verifySaveThisPaymentErrorMessage(savePaymentErrorMsg);
			mouseHoverOnToolTip();
			verifyTextOnToolTip(toolTipMsg);

			Reporter.log("cvv model is displayed");

		} catch (Exception e) {
			Assert.fail("cvv model is not displayed");
		}
		return this;
	}

	public AddCardPage clickOutModel() {

		try {
			Actions builder = new Actions(getDriver());
			// builder.moveToElement(outOfModel).doubleClick().build().perform();

			builder.moveByOffset(150, 200).doubleClick().build().perform();

			Reporter.log("clicked on out of model");

		} catch (Exception e) {
			Assert.fail("failed to click out of model");
		}
		return this;

	}

	/**
	 * verify save this Payment method toggle is displayed or not
	 */
	public AddCardPage verifySaveThisPaymentMethodToggle() {
		try {

			savePaymentCheckBox.isDisplayed();

			Reporter.log("save this payment method toggle is displayed");

		} catch (Exception e) {
			Assert.fail("save this payment menthod toggle  is not displayed");
		}
		return this;
	}

	/**
	 * verify save this Payment method toggle butoon is uncheck or not
	 */
	public AddCardPage verifySaveThisPaymentMethodCheckBoxIsUncheck() {
		try {

			Assert.assertTrue(!savePaymentCheckBox.isSelected(), "save this payment toggle is checked state");

			Reporter.log("save this payment method toggle is uncheck state");

		} catch (Exception e) {
			Assert.fail("save this payment menthod toggle  is checked state");
		}
		return this;
	}

	/**
	 * verify save this Payment method toggle butoon is check or not
	 */
	public AddCardPage verifySaveThisPaymentMethodCheckBoxIscheck() {
		try {

			Assert.assertTrue(savePaymentCheckBox.findElement(By.cssSelector("input")).isSelected(),
					"save this payment toggle is unchecked state");

			Reporter.log("save this payment method toggle is check state");

		} catch (Exception e) {
			Assert.fail("save this payment menthod toggle  is uncheck state");
		}
		return this;
	}

	public AddCardPage mouseHoverOnToolTip() {
		try {
			Actions builder = new Actions(getDriver());
			Action mouseOverHome = builder.moveToElement(toolTip).build();
			mouseOverHome.perform();
			Reporter.log("mouse hover on tool tip");
		} catch (Exception e) {
			Assert.fail("tool tip is not found");
		}
		return this;
	}

	/**
	 * verify save this payment check box error message is display or not
	 */
	public AddCardPage verifySaveThisPaymentErrorMessage(String msg) {
		try {
			SaveThispaymentErrorMsg.isDisplayed();
			Assert.assertTrue(SaveThispaymentErrorMsg.getText().equals(msg));
			Reporter.log("save this error message is verified");
		} catch (Exception e) {
			Assert.fail("failed to verify save this error message");
		}
		return this;

	}

	/**
	 * verify text on tool tip
	 */
	public AddCardPage verifyTextOnToolTip(String toolTiptest) {
		try {

			toolTipText.isDisplayed();

			Assert.assertTrue(toolTipText.getAttribute("tooltip-text").equals(toolTiptest));

			Reporter.log("text on tool tip is verified");

		} catch (Exception e) {
			Assert.fail("text on tool tip not found");
		}
		return this;
	}

	public AddCardPage verifyReplaceModelElements(String cardNumber) {
		try {
			verifyReplaceModelHeader();
			verifyReplaceModelSubHeader();
			verifyReplaceModelCardNumber(cardNumber);
			verifyReplaceModelCardReplaceMessage();
			verifyReplaceModelContinueButton();
			verifyReplaceModelCancelButton();
			Reporter.log("replace model elements is verified");

		} catch (Exception e) {
			Assert.fail("failed to verify replace model elements is verified");
		}
		return this;
	}

	/**
	 * verify replace model is displayed or not
	 */
	public AddCardPage verifyReplaceModelHeader() {
		try {

			replaceModelheader.isDisplayed();
			Reporter.log("replace model is didplayed");

		} catch (Exception e) {
			Assert.fail("replace model is not displayed");
		}
		return this;
	}

	public AddCardPage verifyReplaceModelCardNumber(String cardNumber) {

		try {
			replaceModelCardNumber.isDisplayed();
			/*
			 * String storedPaymentNumber =
			 * replaceModelCardNumber.getText().replace("*", "");
			 * Assert.assertTrue(cardNumber.contains(storedPaymentNumber),
			 * "card number is not found");
			 */
			Reporter.log("card number is verified");
		} catch (NoSuchElementException elementNotFound) {
			Assert.fail("failed to verify card number");
		}

		return this;
	}

	public AddCardPage verifyReplaceModelCardReplaceMessage() {

		try {
			CardReplaceMessage.isDisplayed();
			Assert.assertTrue(
					CardReplaceMessage.getText().equals("This card will replace your current Visa card on file."),
					"card replace message is not found");
			Reporter.log("card replace message is verified");
		} catch (NoSuchElementException elementNotFound) {
			Assert.fail("failed to verify card replace message ");
		}

		return this;
	}

	/**
	 * click on replace model continue button
	 */
	public AddCardPage clickReplaceModelContinueButton() {
		try {

			replaceModelContinueBtn.isDisplayed();

			replaceModelContinueBtn.click();

			Reporter.log("clicked on replace model cuntinue button");

		} catch (Exception e) {
			Assert.fail("replace model continue button is not displayed");
		}
		return this;
	}

	/**
	 * click on replace model cancel button
	 */
	public AddCardPage clickReplaceModelCancelButton() {
		try {

			replaceModelcancelBtn.isDisplayed();

			replaceModelcancelBtn.click();

			Reporter.log("clicked on replace model cancel button");

		} catch (Exception e) {
			Assert.fail("replace model cancel button is not displayed");
		}
		return this;
	}

	/**
	 * verify replace model cancel button is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyReplaceModelCancelButton() {

		try {

			replaceModelcancelBtn.isDisplayed();

			Reporter.log("cancel button is displayed");

		} catch (Exception e) {
			Assert.fail("replace model cancel button is not found");
		}
		return this;

	}

	/**
	 * verify replace model continue button is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyReplaceModelContinueButton() {

		try {

			replaceModelContinueBtn.isDisplayed();

			Reporter.log("continue button is displayed");

		} catch (Exception e) {
			Assert.fail("replace model continue button is not found");
		}
		return this;

	}

	/**
	 * verify replace model sub header is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyReplaceModelSubHeader() {

		try {

			replaceModelSubHeader.isDisplayed();

			Assert.assertTrue(replaceModelSubHeader.getText().equals("Are you sure?"),
					"replace model sub header is not found");

			Reporter.log("replace model sub header is displayed");

		} catch (Exception e) {
			Assert.fail("replace model sub header is not found");
		}
		return this;

	}

	/**
	 * verify field label is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyAddCardFieldLabels() {
		try {
			verifyNameOnCardHeader();
			Assert.assertTrue(nameOnCardTitle.getText().equals("Name on card"), "Name on card is not found");
			verifyCreditCardNoHeader();
			Assert.assertTrue(creditCardNoTitle.getText().equals("Credit card number"),
					"Credit card number is not found");
			verifyExpDateHeader();
			Assert.assertTrue(expDateTitle.getText().equals("Expiration date"), "Expiration date is not found");
			verifyCVVHeader();
			Assert.assertTrue(cvvTitle.getText().equals("CVV"), "CVV is not found");
			verifyZipeHeader();
			Assert.assertTrue(zipCodeTitle.getText().equals("Zip"), "ZIP is not found");
			/*
			 * verifyNickNameHeader();
			 * Assert.assertTrue(nickNameLabel.getText().equals("NickName"),
			 * "NickName is not found");
			 */
			Reporter.log("Field labels is displayed");
		} catch (Exception e) {
			Assert.fail("Field labels  is not found");
		}
		return this;
	}

	/**
	 * Verify Card Error Message
	 * 
	 * @return
	 */
	public AddCardPage verifyCardErrorMessage() {
		try {
			cardNumber.sendKeys("12");
			cardNumber.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(cardErrorMessage));
			Assert.assertTrue(cardErrorMessage.getText().contentEquals("Enter a valid card number."),
					"Card error message is not displayed");
		} catch (Exception e) {
			Assert.fail("Card error message is not displayed");
		}
		return this;
	}

	/**
	 * Verify Expiration Date Error Message
	 * 
	 * @return
	 */
	public AddCardPage verifyExpirationDateErrorMessage() {
		try {
			expiryMonth.sendKeys(Keys.TAB);
			expiryMonth.sendKeys("12");
			expiryMonth.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(expiryMonth));
			Assert.assertTrue(expiryMonthErrorMessage.getText().contentEquals("Enter a valid expiration date."),
					"Expiration date error message is not displayed");
		} catch (Exception e) {
			Assert.fail("Expiry month error message is not displayed");
		}
		return this;
	}

	/**
	 * Verify CVV Error Message
	 * @return
	 */
	public AddCardPage verifyCVVErrorMessage() {
		try {
			cvvCode.sendKeys("12");
			cvvCode.sendKeys(Keys.TAB);
			waitFor(ExpectedConditions.visibilityOf(cvvCodeErrorMessage));
			Assert.assertTrue(cvvCodeErrorMessage.getText().contentEquals("Enter a valid CVV."),
					"CVV error message is not displayed");
		} catch (Exception e) {
			Assert.fail("CVV Error message is not displayed");
		}
		return this;
	}

	/**
	 * Verify ZIP Error Message
	 * @return
	 */
	public AddCardPage verifyZIPErrorMessage() {
		try {
			zipCode.sendKeys("12");
			waitFor(ExpectedConditions.visibilityOf(zipCodeErrorMessage));
			Assert.assertTrue(zipCodeErrorMessage.getText().contentEquals("Enter a valid Zip Code."),
					"Zip code error message is not displayed");
		} catch (Exception e) {
			Assert.fail("ZIP Code error message is not displayed");
		}
		return this;
	}

	public void verifyInSessionFieldValues(Payment payment) {
		try {
			Assert.assertTrue(cardName.getAttribute("value").equals(payment.getNameOnCard()), "Name is not matching");
			Assert.assertTrue(cardNumber.getAttribute("value").replaceAll("-", "").equals(payment.getCardNumber()),
					"Card Number is not matching");
			Assert.assertTrue(zipCode.getAttribute("value").equals(payment.getZipCode()), "ZipCode is not matching");
			Assert.assertTrue(expiryYearAndMonth.getAttribute("value").replaceAll("[^\\d.]", "")
					.equals(payment.getExpiryMonthYear()), "Expiry Month is not matching");
		} catch (Exception e) {
			Assert.fail("Failed verifying In session Field values for Card");
		}
	}

	public void enterCardNumber(String cardNum) {
		try {
			while (!cardNumber.getAttribute("value").isEmpty()) {
				cardNumber.clear();
			}
			cardNumber.sendKeys(cardNum);
		} catch (Exception e) {
			Assert.fail("Failed entering Card number");
		}
	}

	public void verifyAutoFillForStateDropdown(Payment payment) {
		try {
			moveToElement(state);
			waitFor(ExpectedConditions.visibilityOf(state));
			state.sendKeys(Keys.ENTER);
			state.sendKeys(payment.getState());
			state.sendKeys(Keys.ENTER);
			waitFor(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(state, "Select")));
			Assert.assertTrue(state.getText().startsWith(payment.getState()), "Selected state is not matching");
		} catch (Exception e) {
			Assert.fail("Failed to autofill the state dropdown");
		}
	}

	public void enterCVVCode(String cvv) {
		try {
			cvvCode.sendKeys(cvv);
		} catch (Exception e) {
			Assert.fail("Failed entering CVV code");
		}
	}

	/**
	 * Hard Goods Verify all fields in Add Card page
	 */
	/*
	 * public void verifyAddCardAllFieldsHardGoods() {
	 * Assert.assertTrue(verifyNameOnCardHeader(),
	 * "Name on Card Title and Field are not displayed"); Reporter.log(
	 * "Name on Card Title and Field are displayed");
	 * Assert.assertTrue(verifyCreditCardNoHeader(),
	 * "Credit Card Number Title and Field are not displayed"); Reporter.log(
	 * "Credit Card Number Title and Field are displayed");
	 * Assert.assertTrue(verifyExpDateHeader(),
	 * "Exp Date Title and Field are not displayed"); Reporter.log(
	 * "Exp Date Title and Field are displayed");
	 * Assert.assertTrue(verifyUseThisBillingAddress(),
	 * "Use billing address for payment info elements are not displayed");
	 * Reporter.log(
	 * "Use billing address for payment info elements are displayed");
	 * clickUseThisBillingAddressCheckbox();
	 * Assert.assertTrue(verifyAddressLine1Header(),
	 * "AddressLine1 Title and Field are not displayed"); Reporter.log(
	 * "AddressLine1 Title and Field are displayed");
	 * Assert.assertTrue(verifyAddressLine2Header(),
	 * "AddressLine2 Title and Field are not displayed"); Reporter.log(
	 * "AddressLine2 Title and Field are displayed");
	 * Assert.assertTrue(verifyCityHeader(),
	 * "City Title and Field are not displayed"); Reporter.log(
	 * "City Title and Field are displayed");
	 * Assert.assertTrue(verifyStateHeader(),
	 * "State Title and Field are not displayed"); Reporter.log(
	 * "State Title and Field are displayed");
	 * Assert.assertTrue(verifyZipCodeHardGoods(),
	 * "Zip code(Hard Goods) Title and Field are not displayed"); Reporter.log(
	 * "Zip code(Hard Goods) Title and Field are displayed");
	 * Assert.assertTrue(verifyCVVHeader(),
	 * "CVV Title and Field are not displayed"); Reporter.log(
	 * "CVV Title and Field are displayed");
	 * Assert.assertTrue(verifyContinueCTA(), "Continue button is not displayed"
	 * ); Reporter.log("Continue CTA is displayed");
	 * Assert.assertTrue(verifyBackCTA(), "Back button is not displayed");
	 * Reporter.log("Back CTA is displayed"); }
	 */

	/**
	 * verify NickName Field is displayed or not
	 * 
	 *
	 */
	public AddCardPage verifyNickNameHeader() {
		try {
			nickNameLabel.isDisplayed();
			nickName.isDisplayed();
			Reporter.log("nickName field is displayed");
		} catch (Exception e) {
			Verify.fail("Failed to Verify the nickName Header ");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public AddCardPage verifyNickNameDisplay() {
		try {
			if (nickName.isDisplayed()) {
				Reporter.log("nickName field is displayed");
			} else {
				Reporter.log("nickName field is Surpressed");
			}

		} catch (Exception e) {
			Assert.fail("nickName Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field is displayed or not
	 * 
	 *
	 */
	public AddCardPage verifySetAsDefaultCheckBox() {
		try {
			if (setAsDefaultCheckbox.isDisplayed()) {
				Reporter.log("Default Check Box field is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field is not Displayed");
			}

		} catch (Exception e) {
			Verify.fail("Default checkbox Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field label is displayed or not
	 * 
	 *
	 */
	public AddCardPage verifySetAsDefaultCheckBoxLabel() {
		try {
			if (setAsDefaultCheckboxLabel.isDisplayed()) {
				Reporter.log("Default Check Box field Label is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field Label is not Displayed");
			}

		} catch (Exception e) {
			Verify.fail("Default checkbox Field Label is not Found ");
		}
		return this;
	}

	/**
	 * Click Set As Default check box Field
	 *
	 */
	public AddCardPage clickSetAsDefaultCheckBox() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".circle")));
			waitFor(ExpectedConditions.elementToBeClickable(setAsDefaultCheckbox));
			if(!setAsDefaultCheckbox.isSelected()) {
				setAsDefaultCheckbox.click();
			}
			Reporter.log("Set as default Check box is Clicked");
		} catch (Exception e) {
			Verify.fail("Failed to click Set as Default CheckBox"+e.getMessage());
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public AddCardPage verifyNoNickNameandDefaultCheckBoxDisplay() {
		try {
			if (nickName.isDisplayed() && setAsDefaultCheckboxLabel.isDisplayed()
					&& setAsDefaultCheckbox.isDisplayed()) {
				Verify.fail("nickName Field,Default Check Box and Default CheckBox Label is Displayed");
			}

		} catch (Exception e) {
			Reporter.log("nickName Field,Default Check Box and Default CheckBox Label is not found");
		}
		return this;
	}

	public AddCardPage verifyNameOnAccountHeader() {
		try {
			Verify.assertTrue(nameOnAccountHeader.isDisplayed(), "Name on Account header is not displayed");
			Verify.assertTrue(nameOnAccountHeader.getText().equals("Name on Account"),
					"Name on Account header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Name on Account header missing");
		}
		return this;
	}

	public AddCardPage getPlaceholder(WebElement element, String msg) {
		try {
			Verify.assertTrue(element.getAttribute("placeholder").equals(msg),
					"Placeholder text is incorrect for " + element.getAttribute("placeholder"));
		} catch (Exception e) {
			Verify.fail("Place holder not found");
		}
		return this;
	}

	/**
	 * verify NickName Field Label, Header, Field
	 */
	public AddCardPage verifyNickNameFieldElements() {
		try {
			//verifyNameOnAccountHeader();
			getPlaceholder(nickNameLabel, "NickName");

		} catch (Exception e) {
			Verify.fail("Failed to verify all NickName elements");

		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public AddCardPage verifyNickNameSurpess() {
		try {
			if (nickName.isEnabled()) {
				Reporter.log("nickName field is Enabled");
			} else {
				Reporter.log("nickName field is Surpressed");
			}

		} catch (Exception e) {
			if (e.toString().contains("NoSuchElementException")) {
				Reporter.log("NickName Field is not displayed for");
			} else {
				Assert.fail("NickName Field is not Preseneted");
			}
		}
		return this;
	}

	public AddCardPage selectSavePaymentOptionCheckBox() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cardName));
			savePaymentCheckBox.isDisplayed();
			if (!savePaymentCheckBox.isSelected()) {
				savePaymentCheckBox.click();
			}

			Reporter.log("Save payment method option is selected");
		} catch (Exception e) {
			if (e.toString().contains("NoSuchElementException")) {
				Reporter.log("Verified Save Payment Method checkbox is not displayed for");
			} else {
				Assert.fail("Failed to verify Save Payement CheckBox");
			}
		}
		return this;
	}

	/**
	 * Enter Nick Name in card Page
	 *
	 * @param payment
	 */
	public void enterNickName(String nicName) {
		try {
			checkPageIsReady();
			nickName.clear();
			nickName.sendKeys(nicName);
			Reporter.log("Nick Name is Enterd successfully");
		} catch (Exception e) {
			Assert.fail("Failed to fill the Nick Name");
		}

	}

	/**
	 * Verify that Save Payment checkbox is suppressed
	 */
	public AddCardPage verifySavePaymentMethodCheckBoxSuppressed(MyTmoData myTmoData) {
		try {
			waitFor(ExpectedConditions.visibilityOf(backBtn));
			if (!savePaymentCheckBox.isDisplayed()) {
				Reporter.log("Verified Save Payment Method checkbox is not displayed for"+ myTmoData.getPayment().getUserType());
			} else {
				Assert.fail("Failed to verify Save Payement CheckBox. Checkbox should NOT be present for "+ myTmoData.getPayment().getUserType());
			}
		} catch (Exception e) {
			if (e.toString().contains("NoSuchElementException")) {
				Reporter.log("Verified Save Payment Method checkbox is not displayed for"
						+ myTmoData.getPayment().getUserType());
			} else {
				Assert.fail("Failed to verify Save Payement CheckBox for for" + myTmoData.getPayment().getUserType());
			}
		}
		return this;
	}

	/**
	 * verify that the save payment method checkbox is disabled for wallet full
	 */
	public void verifySavePaymentCheckboxDisabled() {
		try {
			Assert.assertFalse(savePaymentCheckboxInput.isEnabled(), "Save payment checkbox is not disabled");
			Reporter.log("Save payment checkbox is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save payment checkbox");
		}
	}

	public void verifyPiiMasking(String custNamePID, String custPaymentPID, String custZipPID) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(cardName, custPaymentPID), "Card holder name is not PII masked");
			Assert.assertTrue(checkElementisPIIMasked(cardNumber, custPaymentPID), "Card Number is not PII masked");
			Assert.assertTrue(checkElementisPIIMasked(zip, custPaymentPID), "Zip code is not PII masked");
			Reporter.log("Verified PII masking for Add card Page");
		} catch (Exception e) {
			Assert.fail("Failed to Verify: PII masking for Add card page");
		}
	}

	/**
	 * verify save to my wallet button
	 */
	public void verifySavetoMyWalletCTA() {
		try {
			saveChangesBtn.isDisplayed();
			Reporter.log("Verified Save to my wallet button is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify the Save to my wallet button");
		}
	}

	/**
	 * click save to my wallet button
	 */
	public void clickSaveToMyWalletCTA() {
		try {
			saveChangesBtn.click();
			Reporter.log("Clicked on Save to my wallet button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Save to my wallet button");
		}
	}
	
	public AddCardPage CheckNoSavepaymentoption() {
		try {
			if(savepaymentcheckboxList.size()==0) Reporter.log("No save payment option");
			else Assert.fail("Save PaymentOPtion is displayed");
		} catch (Exception e) {
			Assert.fail("SavepaymentcheckboxList is not located");
		}
		return this;
	}
	
	/**
	 * verify continue button is displayed or not
	 * 
	 * @return boolean
	 */
	public AddCardPage verifyContinueBtnEnabled() {
		try {
			Assert.assertFalse(continueAddCard.isEnabled(),"Continue button is enabled");
			Reporter.log("continue button is disabled");
		} catch (Exception e) {
			Assert.fail("continue button is enabled");
		}
		return this;
	}
	

}