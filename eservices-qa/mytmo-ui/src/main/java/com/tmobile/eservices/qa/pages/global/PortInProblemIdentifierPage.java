package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;
/**
 * @author RNallam1
 *
 */
public class PortInProblemIdentifierPage extends CommonPage {
		
	@FindBy(css = "img[aria-label='fixed icon']")
	private WebElement imgIcon;

	@FindBy(xpath = "//*[contains(text(),'Thanks for the help')]")
	private WebElement helpHeader;
	
	@FindBy(xpath = "//*[contains(text(),'resolved')]")
	private WebElement resovedMsg;

	@FindBy(css = ".secondary-cta")
	private WebElement allDoneBtn;
	
	@FindBy(xpath = "//div[@class='body text-container']")
	private WebElement inputSubHeader;

	@FindBy(xpath = "//input[@id='mat-input-0']")
	private WebElement inputPhoneNo;

	@FindBy(xpath = "//strong[contains(text(),'Type a unique phoneNumber.')]")
	private WebElement inputHelperText;

	@FindBy(xpath = "//div[@class='padding-bottom-xl padding-top-small']")
	private WebElement inputSubmitButton;
	
	@FindBy(xpath = "//span[contains(text(),'Error: Please enter number in correct format')]")
	private WebElement inputPhoneError;

	/**
	 * @param webDriver
	 */
	public PortInProblemIdentifierPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	
	/**
	 * Verify port in problem identifier page
	 */
	public PortInProblemIdentifierPage verifyPortInProblemIdentifierPage() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			Assert.assertTrue(getDriver().getCurrentUrl().contains("port-in/problemIdentifier"));
			Reporter.log("Portin problem identifier page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Portin problem identifier page not displayed");
		}
		return this;
	}
}