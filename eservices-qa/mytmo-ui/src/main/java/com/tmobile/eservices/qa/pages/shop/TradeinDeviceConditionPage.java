package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class TradeinDeviceConditionPage extends CommonPage {

	private static final String pageUrl = "tradeInDeviceCondition";

	public static final String deviceGood = "itsGood";
	public static final String deviceBad = "itsGotIssues";

	@FindBy(xpath = "//p[contains(text(),'in good condition')]") // p[contains(text(),'Good
																	// condition')]")
	private WebElement itsGood;

	@FindBy(xpath = "//p[contains(text(),'It has issues')]") // p[contains(text(),'Bad
																// condition')]")
	private WebElement itsGotIsues;

	@FindBy(css = "div#good div p:first-child")
	private WebElement goodConditiontext;

	@FindBy(css = ".btn.PrimaryCTA-Normal")
	private WebElement ContinueButton;

	@FindBy(css = "checkbox")
	private WebElement checkbox;

	@FindBys(@FindBy(css = "i[ng-click*='ctrl.questionCheckboxClick($index,question);']"))
	private List<WebElement> deviceConditionCheckboxes;

	@FindBy(css = "p.h3-title.p-b-30.text-black.ng-binding")
	private WebElement tradeInPageHeading;

	@FindBy(css = "div.p-t-25.p-b-25.p-l-28-sm")
	private WebElement checkAllApplyText;

	@FindBy(css = "div.row.no-margin div:nth-child(2) i")
	private WebElement deviceAcceptableLCDDisplay;

	@FindBy(css = "div.row.no-margin div:nth-child(4) i")
	private WebElement devicePowerOn;

	public TradeinDeviceConditionPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the TradeInDeviceCondition class instance.
	 */
	public TradeinDeviceConditionPage verifyTradeinDeviceConditionPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains(pageUrl),
					"Trade In Device Condition is not displaying");
			// verifyTradeInPageHeading();
			// verifycheckAllApplyText();
			Reporter.log("Navigated to Trade In Device Condition Page");
		} catch (Exception e) {
			Assert.fail("Trade In Device Condition page url not loaded");
		}
		return this;
	}

	/**
	 * Choose Device Condition
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage selectDeviceCondition(String option) {
		verifyTradeinDeviceConditionPage();
		if (option.equalsIgnoreCase(deviceGood)) {
			clickContinueButton();
		} else if (option.equalsIgnoreCase(deviceBad)) {
			// Click Radio Button
			clickContinueButton();
		}
		return this;
	}

	/**
	 * click Continue Button
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage clickContinueButton() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(ContinueButton));
			clickElementWithJavaScript(ContinueButton);
		} catch (Exception e) {
			Assert.fail("Continue Button is not Clickable");
		}
		return this;
	}

	/**
	 * Click CheckBox
	 */
	public TradeinDeviceConditionPage clickCheckBox() {
		try {
			checkbox.click();
		} catch (Exception e) {
			Assert.fail("Check Box is not Clickable");
		}
		return this;
	}

	/**
	 * Click Device has Acceptable LCD checkbox
	 */
	public TradeinDeviceConditionPage clickDeviceAcceptableLCD() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(ContinueButton));
			clickElement(deviceConditionCheckboxes.get(0));
			Reporter.log("Device has Acceptable LCD checkbox is clickable");
		} catch (Exception e) {
			Assert.fail("Device has Acceptable LCD checkbox is not clickable");
		}
		return this;
	}

	/**
	 * Click Device power on checkbox
	 */
	public TradeinDeviceConditionPage clickDevicePowerOn() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(ContinueButton));
			deviceConditionCheckboxes.get(2).click();
			Reporter.log("Device power on checkbox is clickable");
		} catch (Exception e) {
			Assert.fail("Device power on checkbox is not clickable");
		}
		return this;
	}

	/**
	 * Click Disable Find my IPhone power on checkbox
	 */
	public TradeinDeviceConditionPage clickDisableFindMyIPhone() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(ContinueButton));
			deviceConditionCheckboxes.get(0).click();
			Reporter.log("Disable my Iphone on checkbox is clickable");
		} catch (Exception e) {
			Assert.fail("Disable my Iphone on checkbox is not clickable");
		}
		return this;
	}

	/**
	 * Verify TradeIn Page Header
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage verifyTradeInPageHeading() {
		try {
			waitforSpinner();
			Assert.assertTrue(tradeInPageHeading.isDisplayed(), "TradeIn Page header is not displayed");
			Reporter.log("TradeIn Page header is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display trade in page header");
		}
		return this;
	}

	/**
	 * Verify Check All Apply Text
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage verifycheckAllApplyText() {
		try {
			waitforSpinner();
			Assert.assertTrue(checkAllApplyText.isDisplayed(), "Check all apply text is not displayed");
			Reporter.log("Check all apply text is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Check all apply text");
		}
		return this;
	}

	/**
	 * Click Device has Acceptable LCD and display checkbox
	 */
	public TradeinDeviceConditionPage clickDeviceAcceptableLCDAndDisplay() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(ContinueButton));
			deviceConditionCheckboxes.get(3).click();
			Reporter.log("Device has Acceptable LCD and Display checkbox is clicked");
		} catch (Exception e) {
			Assert.fail("Device has Acceptable LCD and Display checkbox is not clicked");
		}
		return this;
	}

	/**
	 * Click Device has Acceptable LCD and display checkbox
	 */
	public TradeinDeviceConditionPage clickDoesDevicePowerOn() {
		try {
			waitforSpinner();
			devicePowerOn.click();
			Reporter.log("Does device power on checkbox is clicked");
		} catch (Exception e) {
			Assert.fail("Does device power on checkbox is not clicked");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the TradeInDeviceCondition class instance.
	 */
	public TradeinDeviceConditionPage verifyOldTradeinDeviceConditionPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains(pageUrl),
					"Trade In Device Condition is not displaying");
			Reporter.log("Navigated to Trade In Device Condition Page");
		} catch (Exception e) {
			Assert.fail("Trade In Device Condition page url not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TradeinDeviceConditionPage verifyPageUrl() {
		try{
			getDriver().getCurrentUrl().contains(pageUrl);
		}catch(Exception e){
			Assert.fail("TradeIn device condition page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Trade in device condition
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage verifyTradeInDeviceCondition() {

		try {
			checkPageIsReady();
			waitforSpinner();
			verifyPageUrl();
			Reporter.log("Trade in Device Condition Page is loaded");
		} catch (Exception e) {
			Assert.fail("Trade in Device Condition Page is Not Loaded");
		}
		return this;

	}

	/**
	 * Verify Device Condition [Its Good]
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage verifyDeviceConditionAsItsGood() {
		try {
			Assert.assertTrue(goodConditiontext.getText().toLowerCase().contains("good condition"),
					"It's in good condition tile is not displayed.");
			Reporter.log("It's good section is displayed.");
		} catch (Exception e) {
			Assert.fail("It's good section is not displayed.");
		}

		return this;
	}

	/**
	 * Verify Device Condition [Its Got Issues]
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage verifyDeviceConditionAsItsBad() {
		try {
			Assert.assertTrue(itsGotIsues.isDisplayed(), "It's Got Issues section is not displayed");
			Reporter.log("It's Got Issues section is displayed.");
		} catch (Exception e) {
			Assert.fail("It's Got Issues section is not displayed");
		}
		return this;
	}

	/**
	 * Verify Device Condition [Its Got Issues]
	 * 
	 * @return
	 */
	public TradeinDeviceConditionPage clickDeviceConditionAsItsBad() {
		try {
			clickElementWithJavaScript(itsGotIsues);
			Reporter.log("It's Got Issues section is clickable");
		} catch (Exception e) {
			Assert.fail("t's Got Issues section is not clickable");
		}
		return this;
	}

	
	/**
	 * Click on Good condition
	 */
	public TradeinDeviceConditionPage clickOnGoodCondition() {
		try {
			waitforSpinner();
			Assert.assertTrue(goodConditiontext.isDisplayed(), "Good condition divison not displayed");
			goodConditiontext.click();
		} catch (Exception e) {
			Assert.fail("Click on good condition divison failed " + e.getMessage());
		}
		return this;
	}
}
