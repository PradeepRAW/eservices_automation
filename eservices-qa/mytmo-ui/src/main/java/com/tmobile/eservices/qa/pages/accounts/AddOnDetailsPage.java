package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class AddOnDetailsPage extends CommonPage {

	public AddOnDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	// Headers
	@FindBy(css = "div.Display4")
	private WebElement addOnDetailsHeader;

	@FindBy(css = "div.Display5")
	private WebElement verifySharedAddonName;

	@FindBy(xpath = "//span[text()='Active Add-ons']//following::div[contains(@class,'H6-heading')]")
	private List<WebElement> lineSharedAddons;

	@FindBy(xpath = "//div[contains(text(),'Add-on Details')]")
	private WebElement verifyAddon;

	@FindBy(xpath = "//span[contains(text(),'Active Add-ons')]")
	private WebElement verifyActiveAddon;

	// Buttons
	@FindBy(css = "button[class='SecondaryCTA blackCTA full-btn-width']")
	private List<WebElement> changePlan;

	@FindBy(css = "button[class*='blackCTA']")
	private WebElement backCTA;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement mangeOrRemoveCTA;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement returnCTA;

	@FindBy(css = "p.Display6.device-name-style.min-w-ls")
	private List<WebElement> lineName;

	@FindBy(css = "div.float-left.H6-heading.w-a")
	private List<WebElement> activeAddonsList;

	// Others

	@FindBy(xpath = "//p[contains(text(),'T-Mobile ONE +')]")
	private WebElement verifyActiveAddonName;

	@FindBy(css = "span.body-bold.black")
	private WebElement sharedAddonPrice;

	@FindBy(xpath = "//span[contains(text(),'Plus taxes and fees')]")
	private WebElement taxesandfee;

	@FindBy(xpath = "//div[contains(text(),'Plan Details')]")
	private WebElement planDetails;

	// Discount
	@FindBy(xpath = "//p[contains(text(),'Includes $10 of discounts')]")
	private WebElement sharedaddonDiscount;

	@FindBy(xpath = "//p[contains(text(),'Includes $0.52 of discounts')]")
	private WebElement activeaddonDiscount;

	@FindBy(css = "p[class*='body'] span")
	private List<WebElement> addonDiscript;

	@FindBy(xpath = "//span[contains(text(),'GSM Line - SOC3')]")
	private WebElement palnName;

	@FindBy(css = "span.body-bold.black")
	private WebElement verifyOnus;

	/**
	 * Verify Add On Details Page
	 */
	public AddOnDetailsPage verifyAddOnDetailsPage() {
		try {
			checkPageIsReady();
			addOnDetailsHeader.isDisplayed();
			Reporter.log("Add On Details Header is loaded");
		} catch (Exception e) {
			Assert.fail("Add On Details Header is not loaded");
		}
		return this;
	}

	/**
	 * Verify Netflix Standared Add on Over ride Price
	 */
	public AddOnDetailsPage verifyNetflixStandaredAddonOverridePrice() {
		try {
			checkPageIsReady();
			if (activeAddonsList.get(1).getText().contains("Netflix Standared")) {
				Assert.assertTrue(verifyOnus.getText().contains("On Us"));
				Reporter.log("On Us is displayed instead of Price along with Netflix Standard addon");
				verifyOnus.click();
			}
		} catch (Exception e) {
			Assert.fail("Netflix Standard Addon is not available on Line Details Page");
		}
		return this;
	}

	/**
	 * Verify OnUs text
	 */
	public AddOnDetailsPage verifyOnUs() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyOnus.getText().contains("On Us"));
			Reporter.log("Verify On Us available insteadOf price");
		} catch (Exception e) {
			Assert.fail("Verify On Us is not available insteadOf price");
		}
		return this;
	}

	/**
	 * Verify Manage CTA
	 */
	public AddOnDetailsPage verifyCTAName(String name) {
		try {
			checkPageIsReady();
			Assert.assertTrue(mangeOrRemoveCTA.getText().contains(name));
			Reporter.log("Clciked on " + name + " CTA and Navigated to ODF Page");
		} catch (Exception e) {
			Assert.fail(name + " CTA is not available");
		}
		return this;
	}

	/**
	 * Verify Addon Description
	 */
	public AddOnDetailsPage verifyAddonDescription(String name) {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyElementBytext(addonDiscript, name), "Add on Description is not available");
			Reporter.log("Add on Description is available");
		} catch (Exception e) {
			Assert.fail("Add on Description is not available");
		}
		return this;
	}

	/**
	 * Verify Active Addon Discount
	 */
	public AddOnDetailsPage verifyActiveAddonDiscount() {
		try {
			checkPageIsReady();
			activeaddonDiscount.isDisplayed();
			Reporter.log("addonDiscount is available");
		} catch (Exception e) {
			Assert.fail("addonDiscount is not available");
		}
		return this;
	}

	/**
	 * Verify Shared Addon Price
	 */
	public AddOnDetailsPage verifySharedAddonPrice() {
		try {
			checkPageIsReady();
			sharedAddonPrice.isDisplayed();
			Reporter.log("Price is Available for selectedAddon");
		} catch (Exception e) {
			Assert.fail("Price is not Available for selectedAddon");
		}
		return this;
	}

	/**
	 * Click Back CTA
	 */
	public AddOnDetailsPage clickOnBackCTA() {
		try {
			checkPageIsReady();
			backCTA.click();
			Reporter.log("Clicked on BACK CTA and navigate back to PreviousPage");
		} catch (Exception e) {
			Assert.fail("BACK CTA is not available to click");
		}
		return this;
	}

	/**
	 * Click On Active Addon Associated line
	 */
	public AddOnDetailsPage clickOnActiveAddonAssociatedline() {
		try {
			checkPageIsReady();
			if (!(activeAddonsList.isEmpty())) {
				activeAddonsList.get(0).click();
				Reporter.log("Active Add ons are available for selected line");
			} else {
				Assert.fail("Active Add ons are not available to click");
			}
		} catch (Exception e) {
			Assert.fail("Associated line not available to click");
		}
		return this;
	}

	/**
	 * Verify Active Addon Associated line for Service Plan
	 */
	public AddOnDetailsPage verifyActiveAddonAssociatedlineforServicePlan() {
		try {
			checkPageIsReady();
			int count = activeAddonsList.size();
			if (count >= 0) {
				Reporter.log("ActiveAddon are available for selected line");

				for (int i = 0; i <= count; i++) {

					String value = activeAddonsList.get(i).getAttribute("Value");
					if (value.equals("McAfee® Security for T-Mobile")) {
						activeAddonsList.get(i).click();
						Reporter.log("Clicked on ServicePlan and Navigated to AddonDetailsPage");

					} else {
						Reporter.log("ServicePlan is not available to clcik");
					}
				}
			}

		} catch (Exception e) {
			Assert.fail("Associatedline not available to click");
		}
		return this;
	}

	/**
	 * Verify Active Addon Feature
	 */
	public AddOnDetailsPage verifyActiveAddonFeature() {

		try {
			checkPageIsReady();
			activeAddonsList.get(0).click();
			verifyAddOnDetailsPage();
			clickOnBackCTA();
			Reporter.log("Active Add ons are available for selected line");

		} catch (Exception e) {
			Assert.fail("Associated line not available to click");
		}
		return this;
	}

	/**
	 * Click On Active AddOn By Name
	 */
	public AddOnDetailsPage clickOnActiveAddOnByName(String name) {

		try {
			checkPageIsReady();
			clickElementBytext(activeAddonsList, name);
			verifyAddOnDetailsPage();

			Reporter.log("Clicked on Active Add on by name");
		} catch (Exception e) {
			Assert.fail("Associated line not available to click");
		}
		return this;
	}

	/**
	 * Verify Active Addon Associated line for Data Plan
	 */
	public AddOnDetailsPage verifyActiveAddonAssociatedlineforDataPlan() {
		try {
			checkPageIsReady();
			int count = activeAddonsList.size();
			if (count >= 0) {
				Reporter.log("ActiveAddon are available for selected line");

				for (int i = 0; i <= count; i++) {
					String value = activeAddonsList.get(i).getAttribute("Value");
					if (activeAddOnsList().contains(value)) {
						activeAddonsList.get(i).click();
						Reporter.log("Clicked on DataPassPlan and Navigated to AddonDetailsPage");
					} else {
						Reporter.log("DataPassPlan is not available to clcik");
					}
				}
			}

		} catch (Exception e) {
			Assert.fail("Associatedline not available to click");
		}
		return this;
	}

	public List<String> activeAddOnsList() {

		List<String> list = new ArrayList<>();
		list.add("T-Mobile Essentials");
		list.add("T-Mobile ONE");
		list.add("Scam ID");
		return list;

	}

	/**
	 * Verify Active Addon Component
	 */
	public AddOnDetailsPage verifyActiveAddonComponent() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyActiveAddon.isDisplayed();
			Reporter.log("Activeaddon Componenet is available ");
		} catch (Exception e) {
			Assert.fail("Activeaddon Componenet is not available");
		}
		return this;
	}

	/**
	 * Verify Addon Heading
	 */
	public AddOnDetailsPage verifyAddonHeading() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyAddon.isDisplayed(), "AddOnPage Heading is not Availale");
			Reporter.log("AddOnPage Heading is Availale");
		} catch (Exception e) {
			Assert.fail("AddOn Page Heading is not Availale");
		}
		return this;
	}

	/**
	 * Verify Addon Page
	 */
	public AddOnDetailsPage verifyAddonPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyAddon.isDisplayed(), "Add On Page is not loaded");
			Reporter.log("Clciked on Active Add and loaded AddOnPage");
		} catch (Exception e) {
			Assert.fail("Add On Page is not loaded");
		}
		return this;
	}

	/**
	 * Click On Shared Addon Associated line
	 */
	public AddOnDetailsPage clickOnSharedAddonAssociatedline() {
		try {
			checkPageIsReady();
			lineSharedAddons.get(0).click();
			Reporter.log("Clicked on Associated line Shared Addon and navigated to AddOn Page");
		} catch (Exception e) {
			Assert.fail("Associated line Shared Addon is not displayed in plan Details Page");
		}
		return this;
	}
}
