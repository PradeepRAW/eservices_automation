package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * @author blakshminarayana
 *
 */
public class ShopPlansPage extends CommonPage {

	@FindBy(css = "div.step-header h2")
	private WebElement selectPlanHeaderText;
	
	@FindBy(id = "android:id/button1")
	private WebElement securitycontinuebtn;
	
	/**
	 * 
	 * @param webDriver
	 */
	public ShopPlansPage(WebDriver webDriver) {
		super(webDriver);
	}


	/**
	 * click on Security alert accept
	 * 
	 */

	@Override
	public void clickOnSecurityAlertAccept() {

		if (getDriver() instanceof AndroidDriver) {
			String currentContext = ((AppiumDriver<?>) getDriver()).getContext();
			try {
				((AppiumDriver<?>) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOf(securitycontinuebtn));
				securitycontinuebtn.click();
				((AppiumDriver<?>) getDriver()).context(currentContext);
			} catch (Exception e) {
				((AppiumDriver<?>) getDriver()).context(currentContext);
				logger.info("Security certificate is not appeared in Android");
			}
		} else if (getDriver() instanceof IOSDriver) {
			String currentIosContext = ((AppiumDriver<?>) getDriver()).getContext();
			try {
				((AppiumDriver<?>) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@label='Continue']")));
				((AppiumDriver<?>) getDriver()).findElement(By.xpath("//*[@label='Continue']")).click();
				((AppiumDriver<?>) getDriver()).context(currentIosContext);
			} catch (Exception e) {
				((AppiumDriver<?>) getDriver()).context(currentIosContext);
				logger.info("Security certificate is not appeared in IOS");
			}
		}
	}

	/**
	 * Verify Plans Page
	 * 
	 * @return
	 */
	public ShopPlansPage verifyPlansPage() {
		try {
			checkPageIsReady();
			getDriver().getCurrentUrl().contains("Plan");
			selectPlanHeaderText.isDisplayed();
			Reporter.log("Navigated to Shop Plans page");
		} catch (Exception e) {
			Assert.fail("Shop Plans page is not displayed");
		}return this;

	}

}
