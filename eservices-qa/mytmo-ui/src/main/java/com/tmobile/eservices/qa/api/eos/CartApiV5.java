package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class CartApiV5 extends ApiCommonLib {
	public static String accessToken;
	Map<String, String> headers;

	/***
	 * This is the API for the EOS Cart API parameters: - $ref:
	 * '#/parameters/oAuth' - $ref: '#/parameters/transactionId' - $ref:
	 * '#/parameters/correlationId' - $ref: '#/parameters/applicationId' - $ref:
	 * '#/parameters/channelId' - $ref: '#/parameters/clientId' - $ref:
	 * '#/parameters/transactionBusinessKey' - $ref:
	 * '#/parameters/transactionBusinessKeyType' - $ref:
	 * '#/parameters/transactionType'
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getCartLines(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		//accessToken = createPlattoken(apiTestData);
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/cart/" + tokenMap.get("cartId");
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);	
		System.out.println(response.asString());
		return response;
	}

	public Response getCart(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/cart/" + tokenMap.get("cartId");
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);	
		System.out.println(response.asString());
		return response;
	}

	public Response deleteCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		//accessToken = createPlattoken(apiTestData);
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);		
		String resourceURL = "v5/cart/" + tokenMap.get("cartId");
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.DELETE);		
		System.out.println(response.asString());
		return response;
	}

	public Response addToCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		//accessToken = createPlattoken(apiTestData);
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/cart/" + tokenMap.get("cartId");
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);		
		System.out.println(response.asString());
		return response;
	}

	
	public Response removeFromCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		//accessToken = createPlattoken(apiTestData);
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/cart/" + tokenMap.get("cartId");
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);	
		System.out.println(response.asString());
		return response;
	}

	public Response updateCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/cart/" + tokenMap.get("cartId");
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);		
		System.out.println(response.asString());
		return response;
	}

	public Response createCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v5/cart/";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);		
		System.out.println(response.asString());
			
		return response;
	}

	private Map<String, String> buildCartAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		tokenMap.put("version", "v1");
		String transactionType = "UPGRADE";
		Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		Map<String, String> headers = new HashMap<String, String>();
		if (StringUtils.isNoneEmpty(tokenMap.get("addaline"))) {
			transactionType = "ADDALINE";
		}
		headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("applicationid", "MYTMO");
		headers.put("cache-control", "no-cache");
		headers.put("channelid", "WEB");
		headers.put("content-type", "application/json");
		headers.put("correlationid", checkAndGetPlattokenJWT(apiTestData, jwtTokens.get("jwtToken")));
		headers.put("transactionbusinesskeytype", apiTestData.getBan());
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("transactionType", transactionType);
		headers.put("usn", "testUSN");
		headers.put("interactionid", "api-testing");
		headers.put("X-Auth-Originator", jwtTokens.get("jwtToken"));
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid", "ShopAPITest");
		return headers;
	}

}
