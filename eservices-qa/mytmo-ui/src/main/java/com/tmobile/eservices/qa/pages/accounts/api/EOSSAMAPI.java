package com.tmobile.eservices.qa.pages.accounts.api;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSSAMAPI extends EOSCommonLib {






public Response getResponseSAMAPI(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	//requestbody
	   
	    
	    JSONObject requestParams = new JSONObject();
	   // requestParams.put("", "");
		RestService restService = new RestService("", "https://api.t-mobile.com");
		
	//requestheaders	
		
		String Authorization = "Bearer"+" "+alist[2];
		
		    restService.setContentType("application/json");
		    restService.addHeader("Authorization",Authorization);	
		    restService.addHeader("X-Auth-Originator",alist[2]);
		    restService.addHeader("Accept", "application/json");
		    restService.addHeader("activity-id", "321321312");
		    restService.addHeader("X-B3-TraceId", "222");
		    restService.addHeader("X-B3-SpanId", "222");
		    restService.addHeader("application-id", "222");
		    restService.addHeader("channel-id", "222");
		    restService.addHeader("dealercode", "222");
		    restService.addHeader("Postman-Token", "eef93768-ed14-4076-afe5-9cbbf947a8f2");
		
		    String base64encodedString = Base64.getEncoder().encodeToString( alist[3].getBytes("utf-8"));
	         String url = "billing-experience/v3/recurring-items/"+base64encodedString;
	         
	    response = restService.callService(url,RestCallType.GET);
	}

	    return response;
}

public String getPendingChanges(Response response) {
	String pendingChanges=null;
	
	System.out.println(response.body().asString());
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
	//	response.jsonPath().getList("productsPendingChange.lines").size();ß
		response.jsonPath().getString("productsPendingChange.account.ratePlan.code");
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.getString("productsPendingChange.account.ratePlan.code")!=null) {
        	return jsonPathEvaluator.getString("productsPendingChange.account.ratePlan.code");
	}}
		
	
	return pendingChanges;
}  
	
public List<String> getPlanName(Response response) {
	List<String> getplanname=null;
	
List<String> avoidnulls = new ArrayList<String>();
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("ratePlans.name")!=null) {
			List<String> getplanname1= jsonPathEvaluator.get("ratePlans.name");
			for (String a : getplanname1){
			if (a!= null)	avoidnulls.add(a);
			
			}
        	return avoidnulls;
	}}
		
	
	return getplanname;
}    

public String getstatus(Response response) {
	String Accountstatus=null;
	

	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("accountStatus.statusCode")!=null) {
        	return jsonPathEvaluator.getString("accountStatus.statusCode");
	}}
		
	
	return Accountstatus;
}

public Integer getRatePlanName(Response response) {
	int getplanname=0;
	

	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("ratePlans.name")!=null) {
        	return jsonPathEvaluator.getList("ratePlans.name").size();
	}}
		
	
	return getplanname;
}

public List<Boolean> getTaxType(Response response) {
	List<Boolean> getTaxType=null;
	

	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("ratePlans.taxExclusive")!=null) {
        	return jsonPathEvaluator.getList("ratePlans.taxExclusive");
	}}
		
	
	return getTaxType;
}  

public List<String> ratePlantype(Response response) {
	List<String> ratePlantype=null;
	

	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("ratePlans.type")!=null) {
        	return jsonPathEvaluator.getList("ratePlans.type");
	}}
		
	
	return ratePlantype;
}

public Integer  noOflines(Response response) {
	int noofLines=0;
	
	
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("lines.msisdn")!=null) {
        	return jsonPathEvaluator.getList("lines.msisdn").size();
	}}
		
	
	return noofLines;
}

public String getProtectionFromAddOnCategory(Response response, String mainMsisdn) {
	
	String isprotection="false";
	
if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("lines.findAll{it.msisdn=='"+mainMsisdn+"'}[0].addOns.addOnCategory")!=null) {
			List<String> allpretectios=jsonPathEvaluator.get("lines.findAll{it.msisdn=='"+mainMsisdn+"'}[0].addOns.addOnCategory");
			for(String protection:allpretectios) {
				if(protection.toLowerCase().contains("protection")) return "true";
			}
		}
		
	
	}
        return isprotection;
}
	

	
	
/*	
	
	
	String protectionVal = "false";
	
	System.out.println(response.body().asString());
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		
		try {
			jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode() && jsonNode.has("lines")) {
				JsonNode linesPathNode = jsonNode.path("lines");
				if (!linesPathNode.isMissingNode() && linesPathNode.isArray()) {
					for (int i = 0; i < linesPathNode.size(); i++) {
						JsonNode dataNode = linesPathNode.get(i);
						if (dataNode.isObject()) {
							String msisdn = dataNode.path("msisdn").asText();
							if(mainMsisdn.equals(msisdn)){
							JsonNode addOnsPath = dataNode.path("addOns");
							if (!addOnsPath.isMissingNode() && addOnsPath.isArray()) {
								for (int j = 0; j < addOnsPath.size(); j++) {
									JsonNode addOnDataNode = addOnsPath.get(i);
									if (addOnDataNode.isObject()) {
										JsonNode addOnCategoryNode = addOnDataNode.path("addOnCategory");
										if(!addOnCategoryNode.isMissingNode()){
											String addOnCategoryNodeValue = addOnCategoryNode.asText();
											if(StringUtils.isNoneEmpty(addOnCategoryNodeValue) && addOnCategoryNodeValue.toLowerCase().contains("protection")){
												protectionVal="true";
												break;
											}
										}
										}
									}
								}
							}	
							}
						}
							
					}
						
				}
					
			}
		
			catch (JsonProcessingException e) {
			Reporter.log(" <b>JsonProcessingException Response :</b> ");
			Reporter.log(" " + e);
		}catch (IOException e) {
			Reporter.log(" <b>IOException Response :</b> ");
			Reporter.log(" " + e);
			// e.printStackTrace();
		}catch (Exception e) {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
			// e.printStackTrace();
		}
	}
	return protectionVal;
}
*/
}
