package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class ServicesApiV2 extends ApiCommonLib {
	public static String accessToken;
	Map<String, String> headers;
	/***
	 * Summary: Retreive the Service details (SOC details) for sale and conflicts
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getServicesForSale(ApiTestData apiTestData,String requestBody) throws Exception {
		Map<String, String> headers = buildGetCatalogHeader(apiTestData);
		accessToken = getAccessToken();
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v2/service/", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}


	private Map<String, String> buildGetCatalogHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("correlationId",accessToken);
		headers.put("applicationId","MyTMO");
		headers.put("usn","usn");
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("channelId","MyTMO");
		headers.put("clientId","ESERVICE");
		headers.put("transactionBusinessKey","BAN");
		headers.put("transactionBusinesskeyType",apiTestData.getBan());
		headers.put("transactionId","a6a609f9-186c-4da3-8d2b-ea42665026c3");
		headers.put("transactionType","UPGRADE");
		headers.put("Authorization",accessToken);
		return headers;
	}

}


