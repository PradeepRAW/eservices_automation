package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class PurchaseOptionInstallmentPlanPage extends CommonPage {
	private final String pageUrl = "poipPayment";

	@FindBy(css = "p.Display4")
	private WebElement pageHeader;

	@FindBy(css = "img.phone-image")
	private WebElement phoneImage;

	@FindBy(css = "p.Display6")
	private WebElement friendlyName;

	@FindBy(css = "p.legal.padding-top-xsmall")
	private WebElement msisdn;

	@FindBy(css = "p.body.padding-top-xsmall-xs")
	private WebElement deviceName;

	@FindBy(css = "div.d-inline-block.pr-3.body-bold.black>span.Display5")
	private WebElement dueAmount;

	@FindBy(css = "span.H5-heading")
	private WebElement monthlyPaymentAmount;

	@FindAll({ @FindBy(css = "div>span.Display6.black"), @FindBy(css = "span#paymentLable") })
	private WebElement paymentMethod;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement agreeContinueButton;

	public PurchaseOptionInstallmentPlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PurchaseOptionInstallmentPlanPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PurchaseOptionInstallmentPlanPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Purchase Option Installment Page is loaded.");
		} catch (Exception e) {
			Assert.fail("Purchase Option Installment page is not loaded");
		}
		return this;
	}

	/**
	 * verify phone image is displayed
	 * 
	 * @param phoneImage
	 */
	public void verifyPhoneImage(WebElement phoneImage) {
		try {
			phoneImage.isDisplayed();
			Reporter.log("Phone Image is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Phone Image is not found in JOD Ending Soon Options Page");
		}
	}

	/**
	 * verify friendlyName is displayed
	 * 
	 * @param friendlyName
	 */
	public void verifyFriendlyName(WebElement friendlyName) {
		try {
			friendlyName.isDisplayed();
			Reporter.log("Friendly Name is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Friendly Name is not found in JOD Ending Soon Options Page");
		}
	}

	/**
	 * verify MSISDN is displayed
	 * 
	 * @param MSISDN
	 */
	public void verifyMSISDN(WebElement msisdn) {
		try {
			msisdn.isDisplayed();
			Reporter.log("MSISDN is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("MSISDN is not found in JOD Ending Soon Options Page");
		}
	}

	/**
	 * verify Device Name is displayed
	 * 
	 * @param Device
	 *            Name
	 */
	public void verifyDeviceName() {
		try {
			deviceName.isDisplayed();
			Reporter.log("Device Name is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Device Name is not found in JOD Ending Soon Options Page");
		}
	}

	/**
	 * verify Due Amount is displayed
	 * 
	 * @param Due
	 *            Amount
	 */
	public void verifyDueAmount(WebElement dueAmount) {
		try {
			dueAmount.isDisplayed();
			Reporter.log("Due Amount is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Due Amount is not found in JOD Ending Soon Options Page");
		}
	}

	/**
	 * verify Monthly Payment is displayed
	 * 
	 * @param monthly
	 *            Payment
	 */
	public void verifyMonthlyPaymentAmount(WebElement monthlyPaymentAmount) {
		try {
			monthlyPaymentAmount.isDisplayed();
			Reporter.log("Monthly Payment Amount is displayed in JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("Monthly Payment Amount is not found in JOD Ending Soon Options Page");
		}
	}

	/**
	 * verify setup installment plan details
	 * 
	 * @param deviceName
	 */
	public void verifySetupInstallmentPlanDetails(Object deviceName) {
		verifyPhoneImage(phoneImage);
		verifyFriendlyName(friendlyName);
		verifyMSISDN(msisdn);
		verifyDeviceName();
		verifyDueAmount(dueAmount);
		verifyMonthlyPaymentAmount(monthlyPaymentAmount);
	}

	/**
	 * verify phone image is displayed
	 * 
	 * @param phoneImage
	 */
	public void verifyPaymentMethodBlade() {
		try {
			paymentMethod.isDisplayed();
			Reporter.log("paymentMethod Blade is displayed JOD Ending Soon Options Page");
		} catch (Exception e) {
			Assert.fail("paymentMethod Blade is not displayed  JOD Ending Soon Options Page");
		}
	}

	/**
	 * click payment method blade
	 */
	public void clickPaymentMethodBlade() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.elementToBeClickable(paymentMethod));
			paymentMethod.click();
			Reporter.log("Clicked on payment method blade");
		} catch (Exception e) {
			Assert.fail("Failed to click on payment method blade");
		}

	}

	/**
	 * click payment method blade
	 */
	public void clickCTAButton() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.elementToBeClickable(agreeContinueButton));
			agreeContinueButton.click();
			Reporter.log("Clicked on Agree and Continue Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Agree and Continue Button");
		}

	}

}
