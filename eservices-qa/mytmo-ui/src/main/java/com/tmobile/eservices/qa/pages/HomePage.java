package com.tmobile.eservices.qa.pages;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;

import io.appium.java_client.AppiumDriver;

/**
 * This page contains all the Home page elements and methods
 * 
 * @author pshiva
 *
 */
public class HomePage extends CommonPage {

	@FindBy(xpath = "//*[@id='digital-header-nav-link-1']")
	private WebElement usage1;

	@FindBy(id = "digital-header-nav-link-1")
	private List<WebElement> usage2;

	@FindBy(css = "div[id*='success'] span[class='bindeOnAdjustment']")
	private WebElement bingeOnText;

	@FindBy(css = "#digital-header-nav-link-1")
	private List<WebElement> usage3;

	@FindBy(id = "gbl-localization-id")
	private WebElement changeLangIcon;

	@FindAll({ @FindBy(linkText = "Change my plan"), @FindBy(css = "a[data-href*='change-plan']") })
	private WebElement changeMyPlanLink;

	@FindBy(id = "usageLink")
	private WebElement legacyViewAllAccountUsage;

	@FindBy(css = "#usageLink")
	private WebElement legacyViewAllAccount;

	@FindBy(id = "item1")
	private WebElement billingTab;

	@FindBy(css = "#gbl-localization-id")
	private WebElement changeLang;

	@FindBy(xpath = "//span[contains(text(),'DuplicateLine')]")
	private WebElement duplicateLine;

	@FindAll({ @FindBy(id = "DuplicateLine^5055449753_chckURL") })
	private WebElement bingeOn;

	@FindBy(linkText = "Sign Up for Netflix")
	private WebElement registerForNetflixLink;

	protected By usageModalLoader = By.id("di_modal_message_loading");
	protected By homeUsageLoader = By.id("homeUsage_waitcursor");
	protected By alertsLoader = By.id("alertsLoader");
	protected By homeSpinner = By.id("homeUsage_waitcursor");
	protected By callMespinner = By.className("up_cnt");

	@FindBy(className = "usage")
	private WebElement usageImgInLeagcyHomePage;

	@FindBy(id = "myCurrentUsage")
	private WebElement verifyHome;

	@FindBy(id = "loggedInUserName")
	private WebElement verifyTmoUser;

	@FindBy(css = "div.gbl-top-content.pull-right span a")
	private List<WebElement> rightHeaderNavigationLinks;

	@FindBy(css = "div.ui_third_tier_headline.ui-payments")
	private WebElement welcomeText;

	@FindBy(css = "#showLabel")
	private WebElement cloudHomePage;

	@FindBy(id = "id_username_checklist")
	private WebElement userName;

	@FindBy(id = "checklistFocusFirst")
	private WebElement closeButton;

	@FindBy(css = "#topnavsearch")
	private WebElement searchTextBox;

	@FindBy(css = "span.search-icon")
	private WebElement mobileSearchButton;

	@FindBy(linkText = "View account activity")
	private WebElement viewaccountactivity;

	@FindBy(linkText = "View bill")
	private WebElement viewBill;

	@FindAll({ @FindBy(linkText = "View details"),
			@FindBy(xpath = "//div[@class='plan-section show-content']/a[@class='viewPlanDetails']") })
	private WebElement viewPlanDetails;

	@FindBy(linkText = "Check usage details")
	private WebElement checkUsageDetails;

	@FindBy(linkText = "Manage Multi-Line Settings")
	private WebElement manageMutlilineSetting;

	@FindBy(css = ".profileAccordian[id]~.profileAccordian[id] #manageSharingLink")
	private List<WebElement> manageMultiLineSettings;

	@FindBy(linkText = "Turn On/Off")
	private WebElement turnOnOrOff;

	@FindBy(linkText = "Add high-speed data")
	private WebElement addhighspeeddata;

	@FindBy(css = "button[data-href*='onetimepayment']")
	private WebElement payBillButton;

	@FindBy(xpath = "//span[contains(text(),'Set up a Payment Arrangement')]")
	private WebElement setupPAlink;

	@FindBy(id = "di_balDueAmount_withRestoration")
	private WebElement balanceDueAmountRestoration;

	@FindBy(css = "div#di_errorbalancedue")
	private WebElement homePageBalanceError;

	@FindBy(id = "pay_now_ap")
	private WebElement payBillApButton;

	@FindBy(id = "autoPayLegacyDiv")
	private WebElement apOnPayBill;

	@FindBy(id = "autoPayNewDiv")
	private WebElement apOffPayBill;

	@FindBy(id = "co_odm_banner_image_img")
	private WebElement showNowButton;

	@FindAll({ @FindBy(css = "span#epflag"), @FindBy(xpath = "//a[contains(text(),'AutoPay')]") })
	private WebElement autoPayLink;

	@FindBy(css = "#epflag, span[style='color: rgb(0, 0, 0); cursor: auto;']")
	private WebElement autoPayLinkGreyedOut;

	@FindBy(css = "div.footer-pict-nav ul li a")
	private WebElement footerNavigationLinks;

	@FindBy(xpath = "//div[@class='gh-header']//div[contains(@class, 'gh-justify-content-end')]//p//a")
	private List<WebElement> navigationLinks;

	@FindBy(linkText = "Bill")
	private WebElement billingLink;

	/*
	 * @FindBy(xpath = "//a[contains(text(), 'Bill')]") private WebElement
	 * mobileBillingLink;
	 */

	@FindAll({ @FindBy(xpath = "//a[contains(text(), 'Bill')]"), @FindBy(xpath = "//a[@u1st-aria-label='Bill']"),
			@FindBy(linkText = "Bill") })
	private WebElement mobileBillingLink;

	@FindBy(linkText = "Usage")
	private WebElement usageLink;

	@FindBy(css = "a[aria-label='Usage']")
	private WebElement mobileUsageLink;

	@FindBy(linkText = "Turn On/Off")
	private WebElement turnONandOFF;

	@FindBy(linkText = "Change data")
	private WebElement changeData;

	@FindBy(css = "[href*='shop']")
	private WebElement shopLink;

	@FindBy(linkText = "COMPRAR")
	private WebElement shopLinkPuertoCustomer;

	@FindBy(css = "img[title='Shop Holiday Deals']")
	private WebElement shopDeals;

	@FindBy(id = "showLabel")
	private WebElement welcomeHeaderText;

	@FindAll({ @FindBy(id = "setUpCallModal"), @FindBy(id = "setupNewCallScreen") })
	private WebElement callModalHeaderText;

	@FindAll({ @FindBy(css = "#category_dd .none_display"), @FindBy(xpath = "//select[@id='category_dd']//option[1]") })
	private WebElement defaultTopicOptDropdown;

	@FindBy(id = "category_dd")
	private WebElement topicListDropdown;

	@FindBy(css = "select#category_dd option")
	private List<WebElement> dropDownOptions;

	@FindBy(css = "div.no-topic-selec~div.above-input")
	private WebElement numberToCallText;

	@FindBy(id = "callBackMsisdn")
	private WebElement enterNumber;

	@FindAll({ @FindBy(id = "confirmCallButton"), @FindBy(css = "#confirmCallButton div.call-me") })
	private WebElement callMeButton;

	@FindBy(xpath = "//div[contains(text(), 'set up a call')]")
	private WebElement setUpCallPopUp;

	@FindBy(id = "callMeButton")
	private WebElement callMelink;

	@FindBy(id = "scheduleCallBackButton")
	private WebElement scheduleCallMeButton;

	@FindBy(css = "button[class*='schedulecallback_disabled']")
	private WebElement callMeButtonDisabled;

	@FindBy(css = ".gbl-bottom-navigation .gbl-menu ul li a")
	private List<WebElement> leftMainMenuLinks;

	@FindBy(css = ".gbl-logo")
	private WebElement tMobileHomeLink;

	@FindBy(css = "//h5[contains(text(), 'Connect with us')]")
	private WebElement contactUsHeader;

	@FindBy(css = ".logo")
	private WebElement tmobileHeader;

	@FindBy(css = "a#footer_tmobilelink")
	private WebElement tmobileLinkMobile;

	@FindBy(css = ".logo.t-mobile")
	private WebElement tMobileLogo;

	@FindBy(id = "contactModalLink")
	private WebElement contactUsLink;

	@FindBy(id = "footer_coveragelink")
	private WebElement coverageLink;

	@FindBy(xpath = "//a[contains(text(), 'Check coverage')]")
	private WebElement verifyCoverage;

	@FindBy(css = ".mobileViewList-store a")
	private WebElement storeLocatorLink;

	@FindBy(id = "footer_tmobilelink")
	private WebElement tMobileLink;

	@FindBy(css = "span.icon-setting-sm-benefits")
	private WebElement benefitsLink;

	@FindAll({ @FindBy(id = "continueBtnNav"), @FindBy(css = "div.continue-btn") })
	private WebElement benefitsIconContinue;

	private String accessoriesText = " ";

	private String deviceText = " ";

	@FindBy(id = "topnavgo")
	private WebElement searchButton;

	@FindBy(css = "#Support")
	private WebElement supportTab;

	@FindBy(css = "#Devices")
	private WebElement devicesTab;

	@FindBy(css = "li.devices.active")
	private WebElement devicesTabActive;

	@FindBy(css = "li.accessories a")
	private WebElement accessoriesTab;

	@FindBy(css = "#support .span3.span3m ul li")
	private WebElement yourSelectionTextSupport;

	@FindBy(css = ".leftlinkcont ul li")
	private List<WebElement> mobileYourSelectionTextSupport;

	@FindBy(css = "#devices .span3.span3m ul li")
	private WebElement yourSelectionTextDevices;

	@FindBy(css = ".ui_third_tier_headline strong")
	private List<WebElement> mobileSelectionTextDevices;

	@FindBy(css = "#accessories .ui_third_tier_headline strong")
	private List<WebElement> mobileSelectionTextAccessories;

	@FindBy(css = "#accessories .span3.span3m ul li")
	private WebElement yourSelectionTextAccessories;

	@FindAll({ @FindBy(css = ".active .searchpagination"), @FindBy(css = "div#support div.line-usage-pagination") })
	private WebElement supportPagination;

	@FindAll({ @FindBy(css = "#devices .searchpagination.clearfix"),
			@FindBy(css = "div#devices div.line-usage-pagination") })
	private WebElement devicePagination;

	@FindAll({ @FindBy(css = "#accessories .searchpagination.clear"),
			@FindBy(css = "div#accessories div.line-usage-pagination") })
	private WebElement accessoriesPagination;

	@FindBy(css = ".tab-pane.support.active .span9.span9m.pl25 .searchsupportdetail.mb30")
	private List<WebElement> noOfSearchContentsInSupport;

	@FindBy(css = "#devices .span9.span9m .span4.spacbot4.span4d")
	private List<WebElement> noOfSearchContentsInDevices;

	@FindBy(css = "#accessories .span9.acce-col.span9m .span4.spacbot4.span4accessories")
	private List<WebElement> noOfSearchContentsInAccessories;

	@FindBy(css = "#footer_supportlink")
	private WebElement footerSupportLink;

	@FindBy(xpath = "//a[contains(text(),'store locator')]")
	private WebElement footerStoreLocatorLink;

	@FindBy(css = "#contactModalLink")
	private WebElement footerContactUsLink;

	@FindBy(css = "#failuersearch_error .noresultwrap .ui_third_tier_headline")
	private WebElement noSearchFound;

	@FindBy(css = "#failuersearch .noresultwrap .ui_third_tier_headline")
	private WebElement noSearchFoundNoError;

	@FindBy(css = "a[u1st-aria-label='My phone']")
	private WebElement phoneTab;

	@FindBy(xpath = "//a[contains(text(),'My phone')]")
	private WebElement mobilePhoneLink;

	@FindBy(linkText = "Privacy Policy")
	private WebElement privacyPolicyLink;

	@FindBy(linkText = "See payment history")
	private WebElement seePaymentHistory;

	@FindBy(linkText = "Change plan or services")
	private WebElement changePlanOrServices;

	@FindBy(linkText = "Change plans")
	private WebElement changePlans;

	@FindBy(css = "a[data-analytics-value='Account']")
	private WebElement planIcon;

	@FindBy(css = "div.FL.home-txt4 div a span")
	private WebElement payMyBillButton;

	@FindBy(css = "li#tmnav_Manage ul.secondlvl li a span")
	private List<WebElement> manageLinks;

	@FindBy(id = "tmnav_Manage")
	private WebElement manageTab;

	@FindBy(id = "phonebrands")
	private WebElement selectPhoneBrand;

	@FindBy(css = "div#deviceLoading li div")
	private List<WebElement> selectBrand;

	@FindBy(css = ".ui_headline.mb15")
	private WebElement welcomeHeader;

	@FindAll({ @FindBy(id = "logout-Ok"), @FindBy(id = "mobile-logout-Ok") })
	private WebElement logoutButton;

	@FindBy(css = "span[pid='cust_nicknameN']")
	private WebElement logoutMenu;

	@FindBy(linkText = "Logout")
	private WebElement logoutLink;

	@FindBy(css = "a#logout-Ok")
	private WebElement logoutMobileButton;

	@FindBy(id = "failuersearch_error")
	private WebElement searchError;

	@FindAll({ @FindBy(css = "#headacc-margin > div > span.firstName"),
			@FindBy(css = "th.ui_mobile_subhead span.firstName") })
	private WebElement msisdnUsername;

	@FindAll({ @FindBy(css = "#headacc-margin > div > span.msisdn.ui_body"),
			@FindBy(css = "th.ui_mobile_subhead span.msisdn.ui_mobile_phone") })
	private WebElement msisdnPhonenumber;

	@FindBy(id = "closeButton")
	private WebElement clossIcon;

	@FindBy(id = "category_dd")
	private WebElement selectCategory;

	@FindBy(css = "#slot0")
	private WebElement selectTimeslot;

	@FindAll({ @FindBy(id = "scheduleModalConfirmButton"), @FindBy(id = "availableSlotConfirm") })
	private WebElement scheduleModalConfirmBtn;

	@FindAll({ @FindBy(id = "CallConfirmDoneButton"), @FindBy(id = "doneButton") })
	private WebElement callConfirmDoneBtn;

	@FindAll({ @FindBy(css = "div#confirmCallModal div.ui_headline"),
			@FindBy(css = "div#alreadyScheduledScreen div.ui_mobile_caps_headline.font12") })
	private WebElement callBackscheduledheader;

	@FindAll({ @FindBy(id = "cancelCallBack"), @FindBy(id = "cancelCallbackSchedule") })
	private WebElement cancelCallbackBtn;

	@FindAll({ @FindBy(id = "makeachange_btn"), @FindBy(id = "makeAChangeSchedule") })
	private WebElement makeaChangebtn;

	@FindAll({ @FindBy(id = "date_dd"), @FindBy(xpath = "(//select[@class='ui_mobile_subtext'])[2]") })
	private WebElement dateDrowndownBoxcallMe;

	@FindAll({ @FindBy(id = "time_dd"), @FindBy(xpath = "(//select[@class='ui_mobile_subtext'])[3]") })
	private WebElement timeDrowndownBoxcallMe;

	@FindAll({ @FindBy(id = "select_zone_dd"), @FindBy(xpath = "(//select[@class='ui_mobile_subtext'])[4]") })
	private WebElement zoneDrowndownBoxcallMe;

	@FindAll({ @FindBy(id = "findATime"), @FindBy(css = "#findATimeButton button") })
	private WebElement findTimebtn;

	@FindAll({ @FindBy(css = "div#di_balancedue div div.ui_caps_headline"),
			@FindBy(css = "div#di_balancedue div span.ui_mobile_caps_headline") })
	private WebElement balanceDue;

	@FindBy(css = "#di_billdueDateText")
	private WebElement dueDate;

	@FindBy(css = "div.profileDummy table.table.billing-sec-heading tr")
	private List<WebElement> allLines;

	@FindBy(css = "div[class*='accordion-contain row-fluid'][style*='block'] div[class*='talk-minutes']")
	private WebElement talk;

	@FindBy(css = "div[class*='accordion-contain row-fluid'][style*='block'] div[class*='text-msgs']")
	private WebElement text;

	@FindBy(css = "div[class*='accordion-contain row-fluid'][style*='block'] div[class*='dataSpeedExist']")
	private WebElement data;

	@FindBy(id = "di_balDueAmount")
	private WebElement balanceDueAmount;

	@FindBy(css = "#devices div.span9 div.ui_third_tier_headline")
	private List<WebElement> devices;

	@FindBy(css = "#devices div.span9")
	private WebElement devicesDiv;

	@FindBy(id = "jump_retail")
	private WebElement popupDiv;

	@FindBy(id = "storeLocatorURL")
	private WebElement storeLocatorButton;

	@FindBy(css = ".profileAccordian[id]")
	private List<WebElement> plansDivs;

	@FindBy(xpath = "//div[@class='accordion-heading2 activeTab']//..//span[@class='planName']")
	private WebElement activePlanDesc;

	@FindBy(xpath = "//div[@class='accordion-heading2 activeTab']//..//span[@class='planName']")
	private WebElement activePlanDescMobile;

	@FindBy(css = "div.accordion-heading2.activeTab")
	private WebElement activeplanTab;

	@FindBy(xpath = "//div[@class='accordion-heading2 activeTab']//..//div[@id='sharingWithField']")
	private List<WebElement> pairedWith;

	@FindBy(css = ".profileAccordian.isClicked[id] div.ui_third_tier_headline span.planName")
	private WebElement plansDesc;

	@FindAll({ @FindBy(linkText = "View bill"), @FindBy(id = "comparebillhome") })
	private WebElement viewbillLink;

	@FindBy(css = "#di_showmore > div > b > a")
	private WebElement viewallAlertandActivitylink;

	@FindBy(id = "addHighSpeedDataLink")
	private WebElement addHighspeedDatalink;

	@FindBy(css = "div.ui_caps_headline.mt35")
	private WebElement currentPlantxt;

	@FindBy(css = "div.pull-left.mr30.talk-minutes.dataTalkMinutesExist.hide > div.ui_caps_headline.mb10")
	private WebElement talkText;

	@FindBy(css = "div.pull-left.mr25.text-msgs.dataTextMsgsExist.hide > div.ui_caps_headline.mb10")
	private WebElement textCurrentplan;

	@FindBy(css = "div.pull-left.mr20.mr35.data-speed.dataSpeedExist > div.ui_caps_headline.mb10")
	private WebElement dataCurrentplan;

	@FindBy(id = "di_balancedue")
	private WebElement balanceDuelabel;

	@FindBy(id = "di_duedated")
	private WebElement dueDatelabel;

	@FindBy(css = "body > div.checkorder > div.container > div:nth-child(3) > div:nth-child(1) > div > div.pagetitle_wrap.co_pagetitle > div > div.span12 > div > span")
	private WebElement orderStatusheader;

	@FindBy(css = "div#header_title")
	private WebElement verifyVoiceMail;

	@FindBy(css = "#callMeButton .call-icon")
	private WebElement phoneIcon;

	@FindBy(linkText = "Return Policy")
	private WebElement returnPolicyLink;

	@FindBy(linkText = "Contact Us")
	private WebElement legacyContactUsLink;

	@FindBy(linkText = "Coverage")
	private WebElement legacyCoverageLink;

	@FindBy(linkText = "Return Policy")
	private WebElement legacyReturnPolicyLink;

	@FindBy(css = "#utilityCoverage>a>span")
	private WebElement legacyHeaderCoverageLink;

	@FindBy(linkText = "Support")
	private WebElement legacySupportLink;

	@FindBy(css = "#utilityStorelocator a span")
	private WebElement legacyStoreLocatorLink;

	@FindBy(css = "div#logo a#A1 img")
	private WebElement legacyTmobileLink;

	@FindBy(css = "div.logo")
	private WebElement tmobileLinkLogo;

	@FindBy(xpath = "//h1[contains(text(),'Log in')]")
	private WebElement LoginTextLogo;

	@FindBy(css = ".span6 #footer_tnclink")
	private WebElement termsAndConditions;

	@FindBy(linkText = "Terms & Conditions")
	private WebElement termsAndConditionsDesktop;

	@FindBy(id = "msisdn0")
	private WebElement selectAccount;

	@FindAll({ @FindBy(css = ".profileAccordian[id]~.profileAccordian[id] span.planName"),
			@FindBy(css = ".profileAccordian[id]~.profileAccordian[id] div.planName") })
	private WebElement virtualText;

	@FindAll({ @FindBy(css = "#accordion > div.profileDummy >div:nth-child(2) >div>span"),
			@FindBy(css = ".profileAccordian:nth-child(2) .accordion-heading2") })
	private WebElement virtualLineacct;

	@FindBy(css = ".profileAccordian:nth-child(2) #manageSharingLink")
	private WebElement virtualLineManageMultilineSetting;

	@FindAll({ @FindBy(css = ".profileAccordian:nth-child(2) .ui_primary_link.checkUsageForward"),
			@FindBy(css = ".profileAccordian:nth-child(2) .ui_secondary_link.checkUsageForward") })
	private WebElement virtualLineCheckUsageDetails;

	@FindBy(id = "accountInformation")
	private WebElement dotNetHomePage;

	@FindBy(css = "div#footer div div ul li a")
	private List<WebElement> dotNetfooterNavigationLinks;

	@FindBy(css = "img#logo")
	private WebElement pageHeaderLogo;

	@FindBy(css = "#myUsageHeader > div > a")
	private WebElement dotNetHomePagePlans;

	// @FindBy(id = "di_alrttitle")
	// @FindBy(css="span.ui_subhead")
	@FindAll({ @FindBy(css = "span.ui_subhead"), @FindBy(css = "div.alert-heading span") })
	private WebElement alertSection;

	@FindBy(css = "div.tatCust18009Alert")
	private List<WebElement> alerts;

	@FindBy(id = "loggedInUserName")
	private List<WebElement> userNameText;

	@FindBy(id = "loggedInUserName")
	private WebElement loggedInUser;

	@FindBy(css = "div.co-importantmsgs p")
	private List<WebElement> importantMessage;

	// mobile elements

	@FindBy(css = "ul[class*='ntm-navbar'] a")
	private List<WebElement> mobileMenuLinks;

	@FindBy(css = "span.gbl-icon-mobile.icon-menu-icon")
	private WebElement mobileLogoutMenu;

	@FindBy(css = "#digital-header-menu-button")
	private WebElement mobileNavMenu;

	@FindBy(css = "#user-links-dropdown")
	private WebElement profileDropDown;

	@FindBy(linkText = "Profile")
	private WebElement profileLink;

	@FindBy(xpath = "//a[contains(text(),'Account History') or contains(text(), 'Account history')]")
	private WebElement accountHistoryLink;

	@FindBy(linkText = "Account history")
	private WebElement mobileAccountHistoryLink;

	@FindBy(css = "a#user-links-dropdown-mobile")
	private WebElement mobileMyAccountMenu;

	@FindBy(id = "benefitsPageUpdate")
	private WebElement mobileBenefitsLink;

	@FindBy(id = "item3")
	private WebElement mobileProfileLink;

	@FindBy(css = "div [class='accordion-heading2']")
	private WebElement virtualTextSecond;

	@FindBy(css = ".profileAccordian:nth-child(2) .viewPlanDetails")
	private WebElement virtualLineacctViewPlanDetails;

	@FindBy(id = "banner_cross1")
	private WebElement appBannerCross;

	@FindBy(css = "div.row-fluid.talk-text-details.usage-section.show-content div.dataSpeedExist span.totalNetworktxt")
	private WebElement dataUsage;

	@FindBy(css = "div.profileDummy span.totalNetworktxt.hide")
	private WebElement dataIOS;

	@FindBy(css = "span#di_alrtmssg span")
	private WebElement viewLeaseDetails;

	@FindBy(id = "setup_autopay")
	private WebElement setupAutopayBtn;

	@FindBy(css = "div span[class*='body-copy black']")
	private List<WebElement> currentPlanName;

	@FindBy(css = "a.ui_secondary_link.changeProfile[id*='chckURL']")
	private WebElement bingeOnOffLink;

	@FindBy(css = "a.ui_secondary_link.changeProfile[id*='chckURL']")
	private WebElement homepageduedate;

	@FindBy(linkText = "About T-Mobile USA")
	private WebElement aboutTmobileLink;

	@FindBy(css = "div.body-copy.text-center span")
	private WebElement billDuedate;

	@FindBy(css = "div.align-items-center span")
	private WebElement remainingDays;

	@FindBy(css = "div.text-center div.legal")
	private WebElement autoPayschedulesDatemessage;

	@FindBy(id = "id_checklist_footer")
	private WebElement checkListForFooter;

	@FindBy(css = "div.gbl-navigation > div.gbl-bottom-navigation > div > a")
	private WebElement tMobileicon;

	@FindBy(id = "switch_account")
	private WebElement switchAccount;

	@FindBy(css = "div.autopay-messagePadding")
	private WebElement autoPayMSgHomePage;

	@FindBy(css = "span#easyPayMsgOffTxt")
	private WebElement autoPayMsgOffMsg;

	@FindBy(css = "div#easyPayMessageON")
	private WebElement autoPayMsgOnMsg;

	// @FindBy(css = "div#di_dueDate")
	@FindAll({ @FindBy(css = "div#di_dueDate"), @FindBy(css = "span#di_dueDate") })
	private WebElement billingDueDate;

	@FindBy(linkText = "Sign Up for Netflix")
	private WebElement signUpForNetflix;

	@FindBy(linkText = "Sign Up for Netflix")
	private List<WebElement> signUpForNetflixLink;

	@FindBy(css = "a[id='addHighSpeedDataLink']")
	private List<WebElement> changeDataLink;

	@FindBy(id = "footer_internet_ads")
	private WebElement interestBasedAdsLink;

	@FindBy(id = "footer_privacyresourcelink")
	private WebElement privacyResourcesLink;

	@FindBy(linkText = "Privacy Resources")
	private WebElement privacyResourcesLinkDesktop;

	@FindBy(id = "footer_internetpolicylink")
	private WebElement openInternetPolicyLink;

	@FindBy(linkText = "© 2002 - 2018 T-Mobile USA, Inc")
	private WebElement copyrightsLink;

	@FindBy(css = "div.display4.text-center span")
	private WebElement pastDueAmount;

	@FindBy(css = "a[href*='//www.t-mobile.com/cell-phones']")
	private WebElement phones;

	@FindBy(css = ".gbl-profile")
	private WebElement profileMenuLink;

	@FindBy(css = ".gh-menu-white")
	private WebElement menuIcon;

	@FindBy(css = "[data-href*='profile']")
	private WebElement profileMenu;

	@FindAll({ @FindBy(className = "co_balance"), @FindBy(css = "div.co_billing-main-page") })
	private WebElement pageLoadElement;

	@FindBy(id = "homeUsage_waitcursor")
	private WebElement pageSpinner;

	@FindBy(xpath = "//*[contains(text(),'(360)  685-6773')]")
	private WebElement miPlanMobileNumber;

	@FindBy(css = "div[id='3606856773_plan'] span.planName")
	private WebElement miPlanHeader;

	@FindBy(css = "div[id='3606856773_plan'] div.planName")
	private WebElement miPlanHeaderForIOS;

	@FindBy(id = "militaryStatus")
	private WebElement militaryStatuslink;

	@FindBy(css = "span#di_alrttitle.ui_darkbody_bold")
	private WebElement primaryAccountHolderMissinglink;

	@FindBy(css = ".msisdn")
	private WebElement linkedMobileNumber;

	@FindBy(css = "#di_alertblade")
	private WebElement primaryAccountMissingAlertDiv;

	@FindBy(css = ".ui_headline.mb30")
	private WebElement searchResultsHeadLine;

	@FindBy(css = "#resultCount")
	private WebElement seachResultCount;

	@FindBy(css = "#searchKeyword")
	private WebElement searchKeyword;

	@FindBy(css = "#goBtn")
	private WebElement goCTA;

	@FindBy(css = "#topnavsearch")
	private WebElement navSearchPlaceHolder;

	@FindBy(css = "#showIosId [id*='banner_cross']")
	private WebElement tMobileAppIOSPopUp;

	private final String pageUrl = "home";

	@FindBy(xpath = "//*[contains(text(),'Protecting your account')]")
	private WebElement pinChangingAlertDiv;

	@FindBy(css = "a[id='addHighSpeedDataLink']")
	private List<WebElement> searchResults;

	@FindBy(xpath = "//h2[contains(text(),'Lifeline')]")
	private WebElement PuertoRicoCustomerHeader;

	@FindBy(xpath = "//h2[contains(text(),'VIP')]")
	private WebElement vipCustomerHeader;

	@FindBy(xpath = "//h2[contains(text(),'Government')]")
	private WebElement governmentCustomerHeader;

	@FindBy(xpath = "//*[contains(text(),'We need help transferring your number')]")
	private WebElement portInLink;

	@FindBy(xpath = "//a[contains(.,'Shop')]")
	private WebElement shopLinkMenu;

	@FindBy(xpath = "//button[contains(text(), 'Upgrade')]")
	private WebElement upgradeLink;

	@FindBy(css = "")
	private WebElement managePaymentsLink;

	@FindBy(linkText = "Add a person or device to my account")
	private WebElement addalinelink;

	@FindBy(css = "a[data-analytics-value='Account']")
	private WebElement accountIcon;

	@FindBy(css = "#doneButton")
	private List<WebElement> announcementModalWindow;
	
	@FindBy(css = "li.navbar__item div.nav__link-container a[href*='cell-phones']")
	private List<WebElement> phonesLink;
	
	@FindBy(id= "digital-header-menu-button")
	private WebElement mobileMenuLink;

	/**
	 * 
	 * @param webDriver
	 */
	public HomePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public HomePage verifyPageUrl() {
		// waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify Home Page
	 * 
	 * @return
	 */
	public HomePage verifyPageLoaded() {
		checkPageIsReady();
		waitforSpinner();
		try {
			if (!announcementModalWindow.isEmpty()) {
				announcementModalWindow.get(0).click();
			}
			waitFor(ExpectedConditions.urlContains(pageUrl));
			Reporter.log("Home page is displayed");
		} catch (Exception e) {
			Assert.fail("Home page is not displayed");
		}
		return this;
	}

	/**
	 * verify current plan name
	 */
	public HomePage verifyCurrentPlanName(String planName) {
		try {
			Assert.assertTrue(verifyElementBytext(currentPlanName, planName));
			Reporter.log(planName + "Plan name displayed");
		} catch (Exception e) {
			Assert.fail(planName + " not displayed");
		}
		return this;
	}

	public String homepageduedate() {
		String due = null;
		try {
			due = homepageduedate.getText();
			Reporter.log("home page due date is stored in string");
			return due;
		} catch (Exception e) {
			Assert.fail("could not find home page due date");
		}
		return due;
	}

	/**
	 * Select the value from the Drop down
	 */
	public HomePage selectCategory() {
		try {
			waitforSpinner();
			checkPageIsReady();
			selectElementFromDropDown(selectCategory, "Index", "1");
			Reporter.log("Call category 'Billing&payment' selected");
		} catch (Exception e) {
			Assert.fail("Call category dropdown not found");
		}
		return this;
	}

	/**
	 * Click About T Mobile Link
	 */
	public HomePage clickAboutTmobileLink() {
		try {
			aboutTmobileLink.click();
			Reporter.log("Clicked on 'About T-mobile link'");
		} catch (Exception e) {
			Assert.fail("About T-mobile link not found");
		}
		return this;
	}

	/**
	 * Verify Alert Section
	 * 
	 * @return
	 */
	public HomePage verifyAlertSection() {
		try {
			Assert.assertEquals(alertSection.getText(), "Alerts");
			Reporter.log("Alerts header displayed");
			if (alerts != null) {
				for (WebElement alert : alerts)
					Reporter.log("Alerts:" + alert);
			} else {
				Assert.fail("No alerts displayed");
			}
		} catch (Exception e) {
			Assert.fail("Alerts Section is not Displayed on the home page");
		}
		return this;
	}

	/**
	 * Click Cancel Call button
	 */
	public HomePage clickCancelcallBackbtn() {
		try {
			cancelCallbackBtn.click();
			Reporter.log("Clicked on Cancel Callback button");
		} catch (Exception e) {
			Assert.fail("Cancel Callback button not found");
		}
		return this;
	}

	/**
	 * Click Virtual Line tab
	 */
	public HomePage clickVirtuallineAcct() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,500)", "");
				virtualLineacct.click();
				Reporter.log("Clicked on Virtual line account");
			} else {
				virtualLineacct.click();
				Reporter.log("Clicked on Virtual line account");
			}
		} catch (Exception e) {
			Assert.fail("Virtual line account not displayed");
		}
		return this;
	}

	/**
	 * Select Date,Time and Country Value from the Call me Option
	 */
	public HomePage selectDatetimeCountrycallMeoption() {
		try {
			selectElementFromDropDown(dateDrowndownBoxcallMe, "Index", "1");
			selectElementFromDropDown(timeDrowndownBoxcallMe, "Index", "2");
			selectElementFromDropDown(zoneDrowndownBoxcallMe, "Index", "2");
			findTimebtn.click();
			Reporter.log("Call Schedule date/time/zone entered and clicked on FindTime button");
		} catch (Exception e) {
			Assert.fail("Call Schedule has some issue");
		}
		return this;
	}

	/**
	 * Verify view bill displayed or not
	 * 
	 * @return
	 */
	public HomePage isViewbillDisplayed() {
		try {
			viewbillLink.isDisplayed();
			Reporter.log("View Bill link exists");
		} catch (Exception e) {
			Assert.fail("View Bill Link not found");
		}
		return this;
	}

	/**
	 * Verify Check usange details displayed or not
	 * 
	 * @return
	 */
	public HomePage isCheckusageDetailsdisplayed() {
		try {
			checkUsageDetails.isDisplayed();
			Reporter.log("Check usage details link exists");
		} catch (Exception e) {
			Assert.fail("Check usage details link not found");
		}
		return this;
	}

	/**
	 * Verify View plan details displayed or not
	 * 
	 * @return
	 */
	public HomePage isViewplanDetailsdisplayed() {
		try {
			viewPlanDetails.isDisplayed();
			Reporter.log("View plan details displayed");
		} catch (Exception e) {
			Assert.fail("View Plan Details link not found");
		}
		return this;
	}

	/**
	 * Verify Manage Multi line settings displayed or not
	 * 
	 * @return
	 */
	public HomePage isManageMultiLinesSettingsdisplayed() {
		try {
			for (WebElement mutliLineSettingsLink : manageMultiLineSettings) {
				mutliLineSettingsLink.isDisplayed();
				Reporter.log("Manage Multiline Settings displayed");
			}
		} catch (Exception e) {
			Assert.fail("Multi Line Setting link not displayed");
		}
		return this;
	}

	/**
	 * Verify Pay bill button is displayed
	 * 
	 * @return
	 */
	public HomePage verifyPaybillBtn() {
		try {
			payBillButton.isDisplayed();
			Reporter.log("Pay bill button displayed");
		} catch (Exception e) {
			Assert.fail("Pay bill button not found");
		}
		return this;
	}

	/**
	 * Click Schedule Call me Button
	 */
	public HomePage clickScheduleCallbackbtn() {
		try {
			clickElementWithJavaScript(scheduleCallMeButton);
			Reporter.log("Clicked on Schedule Call me Button");
		} catch (Exception e) {
			Reporter.log("Schedule call me button not found");
		}
		return this;
	}

	/**
	 * Click Time slot
	 */
	public HomePage clickTimeslot() {
		checkPageIsReady();
		try {
			clickElementWithJavaScript(selectTimeslot);
			Reporter.log("Clicked on select time slot");
		} catch (Exception e) {
			Assert.fail("Select Time Slot not found");
		}
		return this;
	}

	/**
	 * Click Schedule modal Confirm Button
	 */
	public HomePage clickSchedulemodalConfirmbtn() {
		try {
			scheduleModalConfirmBtn.click();
			Reporter.log("Clicked on confirm button");
		} catch (Exception e) {
			Assert.fail("Confirm button not found in Schedule call model");
		}
		return this;
	}

	/**
	 * Click Call Confirm Done Button
	 */
	public HomePage clickCallConfirmDoneBtn() {
		try {
			waitforSpinner();
			checkPageIsReady();
			callConfirmDoneBtn.click();
			Reporter.log("Clicked on Call confirm done button");
		} catch (Exception e) {
			Verify.fail("Call confirm done button not found");
		}
		return this;
	}

	/**
	 * Get a Text from the Call back scheduled header
	 * 
	 * @return
	 */
	public HomePage getTextcallBackscheduleHeader(String text) {
		try {
			Assert.assertEquals(callBackscheduledheader.getText(), "You have a callback scheduled");
			Reporter.log("Call back scheduled message displayed");
		} catch (Exception e) {
			Reporter.log("Call back scheduled message not displayed");
		}
		return this;
	}

	/**
	 * Verify Cancel Call me Button is displayed
	 * 
	 * @return
	 */
	public HomePage verifyCancelcallBackbtnDisplayed() {
		try {
			cancelCallbackBtn.isDisplayed();
			Reporter.log("Cancel Callback Button Displayed");
		} catch (Exception e) {
			Assert.fail("Cancel call back button not displayed");
		}
		return this;
	}

	/**
	 * Verify make a change button is displayed
	 * 
	 * @return
	 */
	public HomePage verifyMakeachangeBtndisplayed() {
		try {
			makeaChangebtn.isDisplayed();
			Reporter.log("Make a change button displayed");
		} catch (Exception e) {
			Assert.fail("Make a change button not found");
		}
		return this;
	}

	/**
	 * Click Make a change Button
	 */
	public HomePage clickMakechangeBtn() {
		try {
			makeaChangebtn.click();
			Reporter.log("Clicked on make a change button");
		} catch (Exception e) {
			Assert.fail("Make a change button not found");
		}
		return this;
	}

	/**
	 * Click Call me Link
	 */
	public HomePage clickCallmeLink() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(callMelink);
			Reporter.log("Clicked on Call me Link");
		} catch (Exception e) {
			Assert.fail("Call me link not found");
		}
		return this;
	}

	/**
	 * Verify Home Page
	 * 
	 * @return
	 */
	public HomePage verifyHomePage() {
		checkPageIsReady();
		waitforSpinner();
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			waitFor(ExpectedConditions.titleContains("Home"));
		} else {
			verifyPageLoaded();
		}
		Reporter.log("Home page is displayed");
		return this;
	}

	/**
	 * Verify Home Page
	 * 
	 * @return
	 */
	public HomePage verifyPRHomePage() {
		try {
			waitFor(ExpectedConditions.titleContains("Inicio"));
			Reporter.log("Navigated to PR home page");
		} catch (Exception e) {
			Assert.fail("PortRicho homepage is not displayed");
		}

		return this;
	}

	/**
	 * Get text of MSISDN username
	 * 
	 * @return
	 */
	public HomePage verifyMsisdnuserName() {
		try {
			msisdnUsername.isDisplayed();
			Reporter.log("User name displayed" + msisdnUsername.getText());
		} catch (Exception e) {
			Assert.fail("User name element not found");
		}
		return this;
	}

	/**
	 * Get Text for MSISDN Phone number
	 * 
	 * @return
	 */
	public HomePage verifyMsidnphoneNumber() {
		try {
			msisdnPhonenumber.isDisplayed();
			Reporter.log("Missdn number displayed" + msisdnUsername.getText());
		} catch (Exception e) {
			Assert.fail("Missdn number element not found");
		}
		return this;
	}

	/**
	 * Click User Profile Links
	 * 
	 * @param linkName
	 */
	public HomePage clickUserProfileLinks(String linkName) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
				mobileProfileLink.click();
				Reporter.log("Clicked on Profile");
			} else {
				clickElementBytext(rightHeaderNavigationLinks, linkName);
				Reporter.log("Clicked on Profile");
			}
		} catch (Exception e) {
			Assert.fail("Profile link is not displayed");
		}
		return this;
	}

	/**
	 * Validate Drop Down Options
	 * 
	 * @param option
	 * @return
	 */
	public HomePage validateDropDownOptions(String option) {
		try {
			for (WebElement options : dropDownOptions) {
				if (option.equalsIgnoreCase(options.getText().trim()))
					Reporter.log(option + " Exists");
				else
					Reporter.log("Dropdown item '" + option + "' Not found");
			}
		} catch (Exception e) {
			Assert.fail("Dropdown not found");
		}
		return this;
	}

	/**
	 * click dropdown
	 */
	public HomePage clickTopicListDropDown() {
		try {
			topicListDropdown.click();
			Reporter.log("Clicked on Topic list Dropdown");
		} catch (Exception e) {
			Assert.fail("Topic list dropdown not found");
		}
		return this;
	}

	/**
	 * Enter the number of phone number
	 * 
	 * @param phoneNumber
	 */
	public HomePage enterCallMeNumber(String phoneNumber) {
		try {
			sendTextData(enterNumber, phoneNumber);
			Reporter.log("Entered call me number as" + phoneNumber);
		} catch (Exception e) {
			Assert.fail("Call me number enter field not found");
		}
		return this;
	}

	/**
	 * Get a schedule Call me Button
	 * 
	 * @return
	 */
	public HomePage scheduledCallMeDisabled() {
		try {
			scheduleCallMeButton.isSelected();
			Reporter.log("Scheduled Call Me disabled");
		} catch (Exception e) {
			Assert.fail("Scheduled Call Me element not found");
		}
		return this;
	}

	/**
	 * Get a schedule Call me Button webelement
	 * 
	 * @return
	 */
	public HomePage scheduledCallMeDisplay() {
		try {
			callMeButtonDisabled.isDisplayed();
			Reporter.log("Scheduled call me button displayed");
		} catch (Exception e) {
			Assert.fail("Scheduled call me button element not found");
		}
		return this;
	}

	/**
	 * To verify search field-Insert data and search
	 * 
	 * @param dataToEnter
	 */
	public HomePage enterSearchData(String dataToEnter) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				if (isElementDisplayed(tMobileAppIOSPopUp)) {
					clickElementWithJavaScript(tMobileAppIOSPopUp);
				}
				mobileSearchButton.click();
				sendTextData(searchTextBox, dataToEnter);
				searchTextBox.sendKeys(Keys.ENTER);
				Reporter.log("Entered data in search box");
			} else {
				sendTextData(searchTextBox, dataToEnter);
				Reporter.log("Entered data in search box");
				searchButton.click();
				Reporter.log("Clicked on search button");
			}
		} catch (Exception e) {
			Assert.fail("Search component not found");
		}
		return this;
	}

	/**
	 * Verify Search Data
	 * 
	 * @param searchData
	 * @return
	 */
	public HomePage getYourSelectionTextSupport(String searchData) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement webElement : mobileYourSelectionTextSupport) {
					if (webElement.getText().contains(searchData)) {
						break;
					}
				}
			} else {
				yourSelectionTextSupport.getText().contains(searchData);
			}
		} catch (Exception e) {
			Assert.fail("Search box not found");
		}
		return this;
	}

	/**
	 * Get Your Selection Text Devices
	 * 
	 * @return getYourSelectionTextDevices
	 */
	public HomePage getYourSelectionTextDevices() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				deviceText = mobileSelectionTextDevices.get(0).getText();
			} else {
				deviceText = yourSelectionTextDevices.getText().substring(0,
						yourSelectionTextDevices.getText().length() - 6);
				Assert.assertEquals(deviceText, "Apple");
			}
		} catch (Exception e) {
			Assert.fail("Selection Text device not found");
		}
		return this;
	}

	/**
	 * Get Your Selection Text Accessories
	 * 
	 * @return accessoriesText
	 */
	public HomePage getYourSelectionTextAccessories() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				accessoriesText = mobileSelectionTextAccessories.get(0).getText();
			} else {
				accessoriesText = yourSelectionTextAccessories.getText().substring(0,
						yourSelectionTextAccessories.getText().length() - 6);
				Assert.assertEquals(accessoriesText, "Apple");
			}
		} catch (Exception e) {
			Assert.fail("Your Selection Text Accessories not found");
		}
		return this;
	}

	/**
	 * Click the contactUS link
	 */
	public HomePage clickContactUSlink() {
		try {
			clickElementWithJavaScript(contactUsLink);
			Reporter.log("Clicked on contact us link");
		} catch (Exception e) {
			Verify.fail("Click on contact us link failed ");
		}
		return this;
	}

	/**
	 * click coverage link
	 */

	public HomePage clickCoverageLink() {
		try {
			coverageLink.click();
			Reporter.log("Clicked on Coverage link");
			if (getDriver() instanceof AppiumDriver) {
				acceptIOSAlert();
			}
		} catch (Exception e) {
			Assert.fail("Coverage link not found");
		}
		return this;
	}

	/**
	 * Click Benefits link
	 */
	public HomePage clickBenefitsLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
				mobileBenefitsLink.click();
				benefitsIconContinue.click();
				Reporter.log("Clicked on benefits link and continue button");
			} else {
				benefitsLink.click();
				benefitsIconContinue.click();
				Reporter.log("Clicked on benefits link and continue button");
			}
		} catch (Exception e) {
			Assert.fail("Benifits link not found");
		}
		return this;
	}

	/**
	 * Click Footer support link
	 */
	public HomePage clickFooterSupportlink() {
		try {
			footerSupportLink.click();
			Reporter.log("Clicked on Footer Support link");
			if (getDriver() instanceof AppiumDriver) {
				acceptIOSAlert();
			}
		} catch (Exception e) {
			Assert.fail("Footer support link not found");
		}
		return this;
	}

	/**
	 * Click Footer support link
	 */
	public HomePage clickFooterStoreLocatorlink() {
		try {
			footerStoreLocatorLink.click();
			Reporter.log("Clicked on footer store locator link");
		} catch (Exception e) {
			Assert.fail("Footer Store locator link");
		}
		return this;
	}

	/**
	 * Click Footer Contact US link
	 */
	public HomePage clickFooterContactUSlink() {
		try {
			moveToElement(contactUsLink);
			footerContactUsLink.click();
			Reporter.log("Cliked on footer Contact us link");
		} catch (Exception e) {
			Assert.fail("Contact Us link not found at footer");
		}
		return this;
	}

	/**
	 * Get Support Tab
	 * 
	 * @return getSupportTab
	 */
	public HomePage getSupportTab() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#ajax-spinner")));
			supportTab.isDisplayed();
			Reporter.log("Support link displayed");
		} catch (Exception e) {
			Assert.fail("Support link not found");
		}
		return this;
	}

	/**
	 * Get Devices Tab
	 * 
	 * @return getDevicesTab
	 */
	public HomePage getDevicesTab() {
		try {
			devicesTab.isDisplayed();
			Reporter.log("Devices link displayed");
		} catch (Exception e) {
			Assert.fail("Devices link not found");
		}
		return this;
	}

	/**
	 * Click Devices Tab
	 */
	public HomePage clickDevicesTab() {
		try {
			devicesTab.click();
			Reporter.log("Clikced on devices link");
		} catch (Exception e) {
			Assert.fail("Devices link not found");
		}
		return this;
	}

	/**
	 * Get Accessories Tab
	 * 
	 * @return getAccessoriesTab
	 */
	public HomePage getAccessoriesTab() {
		try {
			accessoriesTab.isDisplayed();
			Reporter.log("Get accessaries link displayed");
		} catch (Exception e) {
			Assert.fail("Accessaries link not found");
		}
		return this;
	}

	/**
	 * Click Accessories Tab
	 * 
	 * @return getAccessoriesTab
	 */
	public HomePage clickAccessoriesTab() {
		try {
			accessoriesTab.click();
			Reporter.log("Clicked on accessaries link");
		} catch (Exception e) {
			Assert.fail("Accessaries link not found");
		}
		return this;
	}

	/**
	 * Get Support Pagination
	 * 
	 * @return getSupportPagination
	 */
	public HomePage getSupportPagination() {
		try {
			moveToElement(supportPagination);
			supportPagination.isDisplayed();
			Reporter.log("Support pagination displayed");
		} catch (Exception e) {
			Assert.fail("Support pagination not found");
		}
		return this;
	}

	/**
	 * Get Device Pagination
	 * 
	 * @return getDevicePagination
	 */
	public boolean getDevicePagination() {
		return devicePagination.isDisplayed();
	}

	/**
	 * Get Accessories Pagination
	 * 
	 * @return getAccessoriesPagination
	 */
	public HomePage getAccessoriesPagination() {
		try {
			accessoriesPagination.isDisplayed();
			Reporter.log("Accesaries pagination displayed");
		} catch (Exception e) {
			Assert.fail("Accesseries pagination not found");
		}
		return this;
	}

	/**
	 * Get a Text of Call Modal Header
	 * 
	 * @return
	 */
	public HomePage getTextCallModalHeader() {
		try {
			checkPageIsReady();
			if (!callModalHeaderText.isDisplayed()) {
				cancelCallbackBtn.click();
				clickCallmeLink();
				callModalHeaderText.isDisplayed();
			}
		} catch (Exception e) {
			Assert.fail("Text Call Modal Header not found");
		}
		return this;
	}

	/**
	 * Get a text default Topic drop down
	 * 
	 * @return
	 */
	public HomePage getTextdefaultTopicOptDropdown(String text) {
		try {
			Assert.assertEquals(defaultTopicOptDropdown.getText(), "option");
			Reporter.log("Default topic text: " + defaultTopicOptDropdown.getText());
		} catch (Exception e) {
			Assert.fail("Topic option dropdown not found");
		}
		return this;
	}

	/**
	 * Get Text Number to Call
	 * 
	 * @return the numberToCallText
	 */
	public HomePage getTextNumbertoCall(String number) {
		try {
			if (!(getDriver() instanceof AppiumDriver)) {
				Assert.assertEquals(numberToCallText.getText(), "number");
				Reporter.log(number + " Exists");
			}
		} catch (Exception e) {
			Assert.fail("Number to call element not found");
		}
		return this;
	}

	/**
	 * Click Pay bill button
	 */
	public HomePage clickPayBillbutton() {
		try {
			moveToElement(payBillButton);
			payBillButton.click();
			Reporter.log("Clicked on PAY BILL button on Home page");
		} catch (Exception e) {
			Assert.fail("Pay bill button not found");
		}
		return this;
	}

	/**
	 * Verify View Bill Link
	 * 
	 * @return
	 */
	public HomePage verifyViewBill() {
		try {
			viewBill.isDisplayed();
			Reporter.log("View bill displayed");
		} catch (Exception e) {
			Assert.fail("View bill element not found");
		}
		return this;
	}

	/**
	 * Click Easy pay On
	 */
	public HomePage clickEasyPay() {
		try {
			if (setupAutopayBtn.isDisplayed()) {
				setupAutopayBtn.click();
			} else {
				autoPayLink.click();
			}
			Reporter.log("Clicked on easy pay button");
		} catch (Exception e) {
			Assert.fail("Autopay button is not displayed on Home page - Step failed");
		}
		return this;
	}

	/**
	 * Click Privacy Policy Link
	 */
	public HomePage clickPrivacyPolicyLink() {
		try {
			privacyPolicyLink.click();
			Reporter.log("Clicked on privacy policy link");
		} catch (Exception e) {
			Reporter.log("Privacy policy link not found");
		}
		return this;
	}

	/**
	 * Click Internet Based Ads Link
	 */
	public HomePage clickInterestBasedAdsLink() {
		try {
			interestBasedAdsLink.click();
			Reporter.log("Clicked on Interest Based Adds link");
		} catch (Exception e) {
			Assert.fail("Interest Base Ads Link not found");
		}
		return this;
	}

	/**
	 * Click Privacy Resources Link
	 */
	public HomePage clickPrivacyResourcesLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				privacyResourcesLink.click();
				acceptIOSAlert();
			} else {
				clickElementWithJavaScript(privacyResourcesLinkDesktop);
			}
		} catch (Exception e) {
			Assert.fail("Privacy resource link not found");
		}
		return this;
	}

	/**
	 * Click Open Internet Policy Link
	 */
	public HomePage clickOpenInternetPolicyLink() {
		try {
			openInternetPolicyLink.click();
			Reporter.log("clicked on Open internet policy link");
		} catch (Exception e) {
			Assert.fail("Open Internet Plocy link not found");
		}
		return this;
	}

	/**
	 * Click Open Internet Policy Link
	 */
	public HomePage clickCopyrightsLink() {
		try {
			clickElementWithJavaScript(copyrightsLink);
			Reporter.log("Clicked on copyrights link");
		} catch (Exception e) {
			Assert.fail("Copyrights link not found");
		}
		return this;
	}

	/**
	 * Verify TMO Home Page
	 * 
	 * @return
	 */
	public List<String> verifyTMOUser() {
		return getElementsByText(userNameText);
	}

	/**
	 * Click Check Usage Details
	 */
	public HomePage clickCheckUsageDetails() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(usageModalLoader));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(homeUsageLoader));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(alertsLoader));
			checkUsageDetails.click();
		} catch (Exception e) {
			Assert.fail("Usage details link not found");
		}
		return this;
	}

	/**
	 * Click Billing Link
	 * 
	 * @return
	 */
	public HomePage clickBillingLink() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(mobileNavMenu));
				mobileNavMenu.click();
				waitFor(ExpectedConditions.visibilityOf(mobileBillingLink), 10);

				waitFor(ExpectedConditions.elementToBeClickable(mobileBillingLink), 10);
				mobileBillingLink.click();
			} else {
				clickElementWithJavaScript(billingLink);
			}
			Reporter.log("Clicked on Billing link");
		} catch (Exception e) {
			Assert.fail("Billing link not found");
		}
		return this;
	}

	/**
	 * Click Usage Link
	 * 
	 * @return
	 */
	public HomePage clickUsageLink() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf(mobileUsageLink), 5);
				clickElementWithJavaScript(mobileUsageLink);
			} else {
				waitFor(ExpectedConditions.visibilityOf(usageLink));
				usageLink.click();
			}
			Reporter.log("Clicked on Usage link");
		} catch (Exception e) {
			Assert.fail("Usage link not found");
		}
		return this;
	}

	/**
	 * Click Billing Link
	 */
	public HomePage clickTurnONandOFFLink() {
		try {
			checkPageIsReady();
			turnONandOFF.click();
			Reporter.log("Clicked on Turn ON and OFF Link");
		} catch (Exception e) {
			Assert.fail("Turn ON and OFF Link not found");
		}
		return this;
	}

	/**
	 * Click Billing Link
	 */
	public HomePage clickChangeDataLink() {
		try {
			checkPageIsReady();
			changeData.click();
			Reporter.log("Clicked on change data Link");
		} catch (Exception e) {
			Assert.fail("change data Link not found");
		}
		return this;
	}

	/**
	 * Click Shop Tab
	 * 
	 * @return
	 */
	public HomePage clickShoplink() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickMobileNavMenu();
				clickShoplinkOnMenu();
			} else {
				shopLink.click();
				Reporter.log("Clicked on shop page link");
			}
		} catch (Exception e) {
			Assert.fail("Shop link not found");
		}
		return this;
	}

	/**
	 * Click Shop Tab for Puerto Customer
	 * 
	 * @return
	 */
	public HomePage clickShopLinkForPuertoCustomer() {
		checkPageIsReady();
		try {
			shopLinkPuertoCustomer.click();
			Reporter.log("Clicked on shop page link");
		} catch (Exception e) {
			Assert.fail("Shop link not found");
		}
		return this;
	}

	/**
	 * Clicks on Plan Link
	 * 
	 * @return
	 */
	public HomePage clickPlanLink() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
			}
			waitFor(ExpectedConditions.visibilityOf(planIcon));
			planIcon.click();
			Reporter.log("Clicked on Account link");
		} catch (Exception e) {
			Assert.fail("Plan link not found");
		}
		return this;
	}

	/**
	 * Click On Phone Link
	 * 
	 * @return
	 */
	public HomePage clickPhoneLink() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
				waitFor(ExpectedConditions.visibilityOf(mobileMenuLinks.get(1)));
				clickElementBytext(mobileMenuLinks, "My phone");
			} else {
				waitFor(ExpectedConditions.visibilityOf(phoneTab));
				clickElementWithJavaScript(phoneTab);
			}
			Reporter.log("Clicked on Phone link");
		} catch (Exception e) {
			Assert.fail("Phone link not found");
		}
		return this;
	}

	/**
	 * Click Plans And Services
	 */
	public HomePage clickViewPlanDetailsLink() {
		try {
			viewPlanDetails.click();
			Reporter.log("Clicked on view plan details link");
		} catch (Exception e) {
			Assert.fail("View Plan details link not found");
		}
		return this;
	}

	/**
	 * Verify Balance Due
	 * 
	 * @return
	 */
	public HomePage verifyBalanceDue() {
		try {
			balanceDue.isDisplayed();
			Reporter.log("Balance Due displayed");
		} catch (Exception e) {
			Assert.fail("Balance due not found");
		}
		return this;
	}

	/**
	 * Verify Due Date
	 * 
	 * @return
	 */
	public HomePage verifyDueDate() {
		try {
			dueDate.isDisplayed();
			Reporter.log("Due Date displayed");
		} catch (Exception e) {
			Assert.fail("Due date not found");
		}
		return this;
	}

	/**
	 * Verify Balance Due Amount
	 * 
	 * @return
	 */
	public HomePage verifyBalanceDueAmount() {
		try {
			balanceDueAmount.getText().contains("$");
			Reporter.log("Due amount exists");
		} catch (Exception e) {
			Assert.fail("Balance due not found");
		}
		return this;
	}

	/**
	 * Click Logout button
	 */
	public HomePage clickLogoutButton() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				clickMobileNavMenu();
			} else {
				waitFor(ExpectedConditions.visibilityOf(logoutMenu));
				logoutMenu.click();
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf(logoutLink), 5);
				clickElementWithJavaScript(logoutLink);
				waitFor(ExpectedConditions.visibilityOf(LoginTextLogo), 5);
				Reporter.log("Clicked on Logout ");
			}
		} catch (Exception e) {
			Assert.fail("Log out not working");
		}
		return this;
	}

	/**
	 * select any device from searched results
	 */
	public HomePage selectAnyDevice() {
		try {
			waitFor(ExpectedConditions.visibilityOf(devicesDiv));
			for (WebElement device : devices) {
				device.click();
				break;
			}
			if (popupDiv.isDisplayed()) {
				storeLocatorButton.click();
			}
		} catch (Exception e) {
			Assert.fail("Select any device not found");
		}
		return this;
	}

	/**
	 * verify virtual plan lines
	 * 
	 * @return
	 */
	public HomePage verifyVirtualPlanLines() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(usageModalLoader));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(homeUsageLoader));
			waitFor(ExpectedConditions.visibilityOfAllElements(plansDivs));
			virtualTextSecond.click();
			waitFor(ExpectedConditions.visibilityOf(virtualText));
			if (virtualText.getText().contains("Virtual")) {
				moveToElement(virtualText);
				isViewplanDetailsdisplayed();
				isCheckusageDetailsdisplayed();
				isManageMultiLinesSettingsdisplayed();
			}
		} catch (Exception e) {
			Assert.fail("Virtual plan lines not found");
		}
		return this;
	}

	/**
	 * Click TMobile Logo Link
	 */
	public HomePage clickTMobileLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				try {
					tmobileLinkMobile.click();
				} catch (Exception e) {
					getDriver().navigate().back();
				}
			} else {
				tMobileLink.click();
			}
		} catch (Exception e) {
			Assert.fail("Tmobile link not found");
		}
		return this;
	}

	/**
	 * Click Terms And Conditions Link
	 */
	public HomePage clickTermsAndConditionsLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				termsAndConditions.click();
				acceptIOSAlert();
			} else {
				clickElementWithJavaScript(termsAndConditionsDesktop);
				Reporter.log("Clicked on Terms and conditions link");
			}
		} catch (Exception e) {
			Assert.fail("Terms and conditions link not found");
		}
		return this;
	}

	/**
	 * Verify Tmobile Page
	 * 
	 * @return
	 */
	public HomePage verifyCloudTmobilePage() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				tMobileLogo.isDisplayed();
			} else {
				checkPageIsReady();
				tmobileLinkLogo.isDisplayed();
			}
		} catch (Exception e) {
			Assert.fail("Tmobile logo link not found");
		}
		return this;
	}

	/**
	 * Click Cloud Return Policy Link
	 */
	public HomePage clickCloudReturnPolicyLink() {
		try {
			returnPolicyLink.click();
			Reporter.log("Clicked on return policy link");
			if (getDriver() instanceof AppiumDriver) {
				Alert alert = getDriver().switchTo().alert();
				alert.accept();
			}
		} catch (Exception e) {
			Assert.fail("Return policy link not found");
		}
		return this;
	}

	/**
	 * Switch To TMobile Support Window
	 */
	public void switchToTMobileSupportWindow() {
		switchToWindow();
	}

	/**
	 * click on top right menu - mobile browser
	 * 
	 * @return
	 */

	public HomePage clickMobileNavMenu() {
		try {
			checkPageIsReady();
			mobileNavMenu.click();
			// mobileNavMenu.click();
			Reporter.log("Clicked on Mobile nav menu");
		} catch (Exception e) {
			Assert.fail("Mobile nav menu is not clicked");
		}
		return this;
	}

	/**
	 * Click Account History Link
	 * 
	 * @return
	 */
	public HomePage clickOnAccountHistoryLink() {
		checkPageIsReady();
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
				waitFor(ExpectedConditions.visibilityOf(mobileMyAccountMenu), 3);
				mobileMyAccountMenu.click();
				waitFor(ExpectedConditions.visibilityOf(mobileAccountHistoryLink), 3);
				mobileAccountHistoryLink.click();
				Reporter.log("Clicked on account History link");
			} else {
				profileDropDown.click();
				waitFor(ExpectedConditions.visibilityOf(accountHistoryLink), 5);
				accountHistoryLink.click();
			}

		} catch (Exception e) {
			Assert.fail("Failed to Click on account History link");
		}
		return this;
	}

	/**
	 * Click Shop Deals Link
	 */
	public HomePage clickShopDealsLink() {
		try {
			shopDeals.click();
			Reporter.log("Clicked on Shop deals link");
		} catch (Exception e) {
			Assert.fail("Shop deals link not found");
		}
		return this;
	}

	public HomePage getAutoPayText() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(autoPayLink));
			autoPayLink.getText().trim();
		} catch (Exception e) {
			Assert.fail("Auto pay text not found");
		}
		return this;
	}

	/**
	 * Verify View plan details displayed or not for Virtual line
	 * 
	 * @return
	 */
	public HomePage isVirtualLineViewplanDetailsdisplayed() {
		try {
			virtualLineacctViewPlanDetails.isDisplayed();
			Reporter.log("Virtual Plan details displayed");
		} catch (Exception e) {
			Assert.fail("Virtual line view plan details not found");
		}
		return this;
	}

	/**
	 * Verify Check usage details displayed or not for virtual line
	 * 
	 * @return
	 */
	public HomePage isVirtualLineCheckusageDetailsdisplayed() {
		try {
			virtualLineCheckUsageDetails.isDisplayed();
			Reporter.log("Virtual line usage details displayed");
		} catch (Exception e) {
			Assert.fail("Virtual line usage details not found");
		}
		return this;
	}

	/**
	 * Verify Manage Multi line settings displayed or not for Virtual line
	 * 
	 * @return
	 */
	public HomePage isVirtualLineManageMultiLinesSettingsDisplayed() {
		try {
			virtualLineManageMultilineSetting.isDisplayed();
			Reporter.log("Virtual line manage multi settings displayed");
		} catch (Exception e) {
			Assert.fail("Virtual line manage multiline settings not found");
		}
		return this;
	}

	/**
	 * Verify Data Information
	 * 
	 * @return
	 */
	public HomePage verifyDataInformation() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				dataIOS.getText();
				Reporter.log("Data information exists");
			} else {
				Assert.assertEquals(dataUsage.getText(), "Total on-network data");
				Reporter.log("Data information exists");
			}
		} catch (Exception e) {
			Assert.fail("Data information not found");
		}
		return this;
	}

	/**
	 * verify Important Message On home page
	 * 
	 * @return
	 */
	public HomePage verifyImportantMessage(String message) {
		try {
			checkPageIsReady();
			verifyElementBytext(importantMessage, message);
			Reporter.log("Verified important message section contains register for netflix");
		} catch (Exception e) {
			Assert.fail("Important message component not found");
		}
		return this;
	}

	/**
	 * Verify Auto Pay On Or Off
	 * 
	 * @param onOrOff
	 * @return
	 */
	public boolean verifyAutoPay(String onOrOff) {
		boolean isAutoPayOnOrOff = false;
		if ((getDriver() instanceof AppiumDriver) && onOrOff.equals("OFF")) {
			return (setupAutopayBtn.isDisplayed() || autoPayLink.isDisplayed());
		}
		if (autoPayLink.isDisplayed() && onOrOff.equalsIgnoreCase(autoPayLink.getText().trim())) {
			isAutoPayOnOrOff = true;
		}
		return isAutoPayOnOrOff;
	}

	/**
	 * Compare AutoPay conditions
	 */
	public void verifyAutoPayMatch(String onOrOff) {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ajax_loader")));
		waitFor(ExpectedConditions.visibilityOf(autoPayLink));
		;
		Verify.assertTrue(verifyAutoPay(onOrOff),
				"AutoPay condition is " + autoPayLink.getText().trim() + ", but should be - " + onOrOff);
		Reporter.log("AutoPay condition is matching to provided");
	}

	/**
	 * click ob Binge ON/OFF link
	 */
	public HomePage clickBingeOnOffLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(bingeOnOffLink));
			bingeOnOffLink.click();
		} catch (Exception e) {
			Assert.fail("Binge ON/OFF link not found");
		}
		return this;
	}

	/**
	 * Verify Bill due date displayed
	 * 
	 * @return
	 */
	public HomePage verifyBilldueDatedisplayed() {
		try {
			billDuedate.isDisplayed();
			Reporter.log("Bill Due date displayed");
		} catch (Exception e) {
			Assert.fail("Bill due date component not found");
		}
		return this;
	}

	/**
	 * Verify Remaining Days displayed *
	 * 
	 * @return
	 */
	public HomePage verifyRemainingdaysDisplayed() {
		try {
			remainingDays.isDisplayed();
			Reporter.log("Remaining days message is displayed");
		} catch (Exception e) {
			Assert.fail("Remaining days component not found");
		}
		return this;
	}

	/**
	 * Click View bill link
	 */
	public HomePage clickViewbillLink() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ajax_loader")));
			viewbillLink.click();
		} catch (Exception e) {
			Assert.fail("View bill link not found");
		}
		return this;
	}

	/**
	 * Verify AutoPay link is present
	 * 
	 * @return
	 */
	public HomePage verifyAutopayLinkGreyedOut() {
		try {
			Verify.assertTrue(autoPayLink.isDisplayed(), "AutoPay link is not visible");
			Verify.assertTrue(autoPayLinkGreyedOut.isDisplayed(), "AutoPay link is not visible");
		} catch (Exception e) {
			Verify.fail("AutoPay Link was NOT displayed");
		}
		return this;
	}

	/**
	 * Verify AutoPay link is not present
	 * 
	 * @return
	 */
	public HomePage verifyAutopayLinkIsNotPresent() {
		try {
			Assert.assertFalse(autoPayLink.isDisplayed(), "AutoPay link is visible. But should not be");
		} catch (Exception e) {
			Assert.fail("AutoPay Link was displayed. But should not be");
		}
		return this;
	}

	/**
	 * Verify AutoPay AutoPay ON link disabled
	 * 
	 * @return
	 */
	public HomePage verifyAutopayONLinkDisabled() {
		try {
			checkPageIsReady();
			autoPayLink.getText().contains("ON");

			Assert.assertTrue(autoPayLink.getAttribute("class").contains("off-color"), "AutoPay link is not disabled");
		} catch (Exception e) {
			Assert.fail("AutoPay link is not disabled");
		}
		return this;
	}

	/**
	 * Verify Autopay Schedule Date is displayed in the Home page
	 * 
	 * @return
	 */
	public HomePage verifyAutoPayscheduledDateHomepage() {
		try {
			autoPayschedulesDatemessage.isDisplayed();
			Reporter.log("AutoPay schedules date message displayed");
		} catch (Exception e) {
			Assert.fail("Auto Pay Schedule date component not found");
		}
		return this;
	}

	/**
	 * Verify On-boarding checkList For Footer
	 * 
	 * @return
	 */
	public HomePage verifyOnBoardingFooterCheckList() {
		try {
			checkListForFooter.isDisplayed();
			Reporter.log("Checklist for footer displayed");
		} catch (Exception e) {
			Assert.fail("Checklist for footer not found");
		}
		return this;
	}

	/**
	 * verify Duplicate plans for all lines
	 * 
	 * @return boolean
	 */
	public String verifyDuplicatePlanLines() {
		String message = "Expected result";
		boolean value;
		waitFor(ExpectedConditions.invisibilityOfElementLocated(usageModalLoader));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(homeUsageLoader));
		waitFor(ExpectedConditions.visibilityOfAllElements(plansDivs));
		for (WebElement planDiv : plansDivs) {
			moveToElement(planDiv);
			if (!planDiv.getAttribute("class").contains("isClicked")) {
				planDiv.click();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ajax_loader")));

				if (getDriver() instanceof AppiumDriver) {
					value = activePlanDescMobile.isDisplayed() && activePlanDescMobile.getText().contains("DIGITS");
				} else {
					value = activePlanDesc.isDisplayed() && activePlanDesc.getText().contains("DIGITS");
				}
				if (value) {
					for (WebElement paired : pairedWith) {
						value = paired.isDisplayed() && paired.getText().contains("Paired with:");
						if (value) {
							break;
						}
					}
					boolean isLinkUpdated = false;
					for (WebElement digitsLInk : manageMultiLineSettings) {
						isLinkUpdated = digitsLInk.isDisplayed()
								&& digitsLInk.getText().contains("Manage DIGITS settings");
						if (isLinkUpdated) {
							break;
						}
					}
					if (value && isLinkUpdated) {
						message = "Expected result";
						break;
					} else {
						message = "'Duplicate line' is not replaced with 'Paired with' / 'Manage Multi Line settings' is not updated to 'Manage DIGITS settings'";
					}
				}
			} else {
				message = "DIGITS Beta Duplicate Line is not displayed";
			}
			activeplanTab.click();
		}
		return message;
	}

	/***
	 * Click action for Switch Account
	 * 
	 * @return
	 */

	public HomePage clickSwitchAccount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(switchAccount));
			switchAccount.click();
		} catch (Exception e) {
			Assert.fail("Switch account component not found");
		}
		return this;
	}

	/**
	 * click on Change Language icon (English -> Spanish)
	 */
	public HomePage changeLanguage() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
			}
			changeLangIcon.click();
			Reporter.log("Clicked on Language Icon");
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("Language Icon not found");
		}
		return this;
	}

	public HomePage clickDuplicateLine() {
		try {
			waitFor(ExpectedConditions.visibilityOf(duplicateLine));
			duplicateLine.click();
			Reporter.log("Clicked on Duplicate line");
		} catch (Exception e) {
			Assert.fail("Duplicate line not found");
		}
		return this;
	}

	public HomePage verifybingeOnDisplayedOnHomePage() {
		try {
			if (bingeOn.isDisplayed()) {
				Reporter.log("Binge on is displayed");
			}
		} catch (Exception e) {
			Assert.fail("BingeOn not found");
		}
		return this;
	}

	public HomePage verifyBingeOnText() {
		try {
			bingeOnText.isDisplayed();
			Reporter.log("Binge on text is displayed");
		} catch (Exception e) {
			Assert.fail("Binge on text component not found");
		}
		return this;
	}

	public HomePage clickOnRegisterForNetflixLink() {
		try {
			registerForNetflixLink.click();
			Reporter.log("Register for netflix link clicked");
		} catch (Exception e) {
			Assert.fail("Register for netflix link not found");
		}
		return this;
	}

	public String getBalanceDueAmount() {
		String bal = "";
		try {
			if (balanceDueAmount.isDisplayed()) {
				bal = balanceDueAmount.getText();
			} else {
				bal = balanceDueAmountRestoration.getText();
			}
			Reporter.log("Balance Due displayed on Home Page :: " + bal);
			if (!(getDriver() instanceof AppiumDriver) && (bal.isEmpty() || homePageBalanceError.isDisplayed())) {
				Reporter.log("Check your test data....!!!!");
				Reporter.log(homePageBalanceError.getText());
			}
		} catch (Exception balDueNotCaptured) {
			Reporter.log("Balance Due on Home Page could NOT be captured. ");
			bal = "Error";
		}
		return bal;
	}

	public String getDueDate() {
		waitforSpinner();
		String dueDate = "";
		try {
			dueDate = billingDueDate.getText();
			Reporter.log("Due Date displayed on Home Page :: " + dueDate);
			if (!(getDriver() instanceof AppiumDriver) && (dueDate.isEmpty() || homePageBalanceError.isDisplayed())) {
				Reporter.log("Check your test data....!!!!");
				Reporter.log(homePageBalanceError.getText());
			}
		} catch (Exception dueDateExp) {
			Reporter.log("Due Date on Home Page could NOT be captured. ");
		}
		return dueDate;
	}

	/**
	 * verify setup auto pay button is displayed or not
	 * 
	 * @return boolean
	 */
	public HomePage verifySetupAutopayBtn() {
		try {
			waitFor(ExpectedConditions.invisibilityOf(setupAutopayBtn));
			// waitFor(ExpectedConditions.visibilityOf(setupAutopayBtn));
			// setupAutopayBtn.isDisplayed();
			Reporter.log("Setup autopay is displayed");
		} catch (Exception e) {
			Verify.fail("Setup autopay button not found");
		}
		return this;
	}

	public HomePage clickOnSignUpForNetflix() {
		try {
			checkPageIsReady();
			Thread.sleep(3000);
			moveToElement(signUpForNetflix);
			signUpForNetflix.click();
		} catch (Exception e) {
			Assert.fail("Signup for netflix component not found");
		}
		return this;
	}

	public HomePage verifySignUpForNetflix() {
		try {
			if (!signUpForNetflixLink.isEmpty()) {
				Reporter.log("SignUP for netflix displayed");
			} else {
				Assert.fail("Signup for netflix not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Signup for netflix not valid");
		}
		return this;
	}

	public HomePage verifySignUpForNetflixNotPresent() {
		try {
			signUpForNetflixLink.isEmpty();
			Reporter.log("SignUP for netflix not displayed for registeres customer");

		} catch (Exception e) {

			Assert.fail("Signup for netflix not valid");
		}
		return this;
	}

	public HomePage verifyChangeDataLinkforMBBLine() {
		try {
			if (changeDataLink.size() > 1) {
				Reporter.log("Change data link is not displayed for mbb line");
			} else {
				Assert.fail("Change data link is displayed for mbb line");
			}

		} catch (Exception e) {
			Assert.fail("Change data link is displayed for mbb line");
		}
		return this;
	}

	/**
	 * get balance due amount displayed
	 * 
	 * @return value
	 */
	public String getpastDueAmount() {
		checkPageIsReady();
		String bal = "";
		try {
			bal = pastDueAmount.getText();
			/*
			 * Reporter.log("Past Due displayed on Home Page :: " + pastDueAmount); if
			 * (!(getDriver() instanceof AppiumDriver) && (bal.isEmpty() ||
			 * homePageBalanceError.isDisplayed())) {
			 * Reporter.log("Check your test data....!!!!");
			 * Reporter.log(homePageBalanceError.getText()); }
			 */
		} catch (Exception balDueNotCaptured) {
			Reporter.log("Past Due on Home Page could NOT be captured. " + balDueNotCaptured.getMessage());
			bal = "Error";
		}
		return bal;
	}

	/**
	 * Click profile menu
	 */
	public HomePage clickProfileMenu() {
		if (getDriver() instanceof AppiumDriver) {
			waitFor(ExpectedConditions.visibilityOf(menuIcon));
			menuIcon.click();
			moveToElement(profileMenu);
			profileMenu.click();
		} else {
			waitforSpinner();
			checkPageIsReady();
			profileDropDown.click();
			waitFor(ExpectedConditions.visibilityOf(profileLink), 3);
			profileLink.click();
		}
		return this;
	}

	public void verifyBalanceAfterPayment(Double balance) {
		try {
			String bal = getBalanceDueAmount();
			Assert.assertEquals(bal.replace("$", ""),
					new BigDecimal(balance).setScale(2, RoundingMode.HALF_UP).toString());
			Reporter.log("Balance amount is updated after payment");
		} catch (Exception e) {
			Assert.fail("Balance amount is not updated after payment");
		}
	}

	/**
	 * Verify balance due amount displayed
	 * 
	 * @return value
	 */
	public String verifyPastDueAmount() {
		String bal = "";
		try {
			if (pastDueAmount.isDisplayed()) {
				bal = pastDueAmount.getText();
				bal = bal.substring(1, bal.length() - 10);
			} else {
				bal = balanceDueAmount.getText();
			}
			Reporter.log("Past Due amount displayed on Home Page :: " + bal);
			if (!(getDriver() instanceof AppiumDriver) && (bal.isEmpty() || homePageBalanceError.isDisplayed())) {
				Reporter.log("Check your test data....!!!!");
				Reporter.log(homePageBalanceError.getText());
			}
		} catch (Exception balDueNotCaptured) {
			Reporter.log("Balance Due on Home Page could NOT be captured. " + balDueNotCaptured.getMessage());
			bal = "Error";
		}
		return bal;
	}

	/**
	 * Click on Phones Link.
	 *
	 * @return the HomePage class instance.
	 */
	public HomePage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click miplan line.
	 */
	public HomePage clickMIPlanLine() {
		try {
			miPlanMobileNumber.click();
			Reporter.log("Clicked on miplan line");
		} catch (Exception e) {
			Assert.fail("Click on miplan line failed" + e.toString());
		}
		return this;
	}

	/**
	 * Verify miplan line.
	 */
	public HomePage verifyMIPlanLine() {
		if (getDriver() instanceof AppiumDriver) {
			try {
				Assert.assertTrue(verifyElementByText(miPlanHeaderForIOS, "Mobile"),
						"MI plan not displayed with the selected line");
				Reporter.log("MI plan displayed with the selected line ");
			} catch (Exception e) {
				Reporter.log("MI plan not displayed with the selected line ");
				Assert.fail("MI plan not displayed with the selected line " + e.toString());
			}
		} else {
			try {
				Assert.assertTrue(verifyElementByText(miPlanHeader, "Mobile"),
						"MI plan not displayed with the selected line");
				Reporter.log("MI plan displayed with the selected line ");
			} catch (Exception e) {
				Reporter.log("MI plan not displayed with the selected line ");
				Assert.fail("MI plan not displayed with the selected line " + e.toString());
			}
		}
		return this;
	}

	/**
	 * Click tmobile logo
	 */
	public HomePage clickTMobileLogo() {
		try {
			tMobileHomeLink.click();
			Reporter.log("Clicked on tmobile logo");
		} catch (Exception e) {
			Assert.fail("Click on tmobile logo failed" + e.toString());
		}
		return this;
	}

	/**
	 * Click verify military status link
	 */
	public HomePage clickMilitaryStatusLink() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(militaryStatuslink);
		} catch (Exception e) {
			Assert.fail("Military status link not found");
		}
		return this;
	}

	/**
	 * Click verify military status link
	 */
	public HomePage clickPrimaryAccountHolderMissingLink() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(primaryAccountHolderMissinglink);
		} catch (Exception e) {
			Assert.fail("Primary account holder missing link not found");
		}
		return this;
	}

	/***
	 * Verify switch account header menu
	 * 
	 * @return
	 */

	public HomePage verifySwitchAccountHeaderMenu() {
		try {
			waitFor(ExpectedConditions.visibilityOf(switchAccount));
			Assert.assertFalse(isElementDisplayed(switchAccount), "Switch account header menu displayed");
			Reporter.log("Switch account header menu not displayed");
		} catch (AssertionError e) {
			Assert.fail("Switch account header menu not displayed ");
		}
		return this;
	}

	/**
	 * Verify Missdn number.
	 */
	public HomePage verifyMissdnNumber(String phoneNumber) {
		try {
			String loggedInPhoneNumber = phoneNumber.replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			String s = linkedMobileNumber.getText().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
					.trim();
			Assert.assertEquals(s, loggedInPhoneNumber, "Linked phone number not matched with selected account missdn");
			Reporter.log("Linked phone number matched with selected account missdn");
		} catch (Exception e) {
			Assert.fail("Linked phone number not matched with selected account missdn" + e.toString());
		}
		return this;
	}

	/**
	 * Verify primary account holder missing alert divison
	 */
	public HomePage verifyPrimaryAccountMissingAlertDiv() {
		try {
			waitforSpinner();
			checkPageIsReady();
			primaryAccountMissingAlertDiv.isDisplayed();
			Reporter.log("Primary account holder missing alert div displayed");
		} catch (Exception e) {
			Reporter.log("Primary account holder missing alert div not displayed");
			Assert.fail("Primary account holder missing alert div not displayed ");
		}
		return this;
	}

	/**
	 * Verify search results head line
	 */
	public HomePage verifySearchResultsHeadLine() {
		try {
			waitforSpinner();
			checkPageIsReady();
			searchResultsHeadLine.isDisplayed();
			Reporter.log("Search results head line displayed");
		} catch (Exception e) {
			Reporter.log("Search results head line not displayed");
			Assert.fail("Search results head line not displayed ");
		}
		return this;
	}

	/**
	 * Verify search results count
	 */
	public HomePage verifySeachResultCount() {
		try {
			waitforSpinner();
			checkPageIsReady();
			seachResultCount.isDisplayed();
			Reporter.log("Search results count displayed");
		} catch (Exception e) {
			Reporter.log("Search results count not displayed");
			Assert.fail("Search results count not displayed ");
		}
		return this;
	}

	/**
	 * Verify search keyword
	 */
	public HomePage verifyTextInSearchResult() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertEquals(searchKeyword.getText().trim(), "iPhone".trim(),
					"Search keyword not matched with the keyword in search result");
			Reporter.log("Search keyword matched with the keyword in search result");
		} catch (AssertionError e) {
			Reporter.log("Search keyword not matched with the keyword in search result");
			Assert.fail("Search keyword not matched with the keyword in search result ");
		}
		return this;
	}

	/**
	 * Verify go cta
	 */
	public HomePage verifyGoCTA() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(goCTA), "Go cta displayed");
			Reporter.log("Go cta not displayed");
		} catch (Exception e) {
			Reporter.log("Go cta not displayed");
			Assert.fail("Go cta not displayed ");
		}
		return this;
	}

	/**
	 * Verify search text in search field in unav
	 */
	public HomePage verifyNavigationSearchPlaceHolderText() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertEquals(navSearchPlaceHolder.getAttribute("value").trim(), "iPhone".trim(),
					"Search text not displayed in search field in unav");
			Reporter.log("Search text displayed in search field in unav");
		} catch (AssertionError e) {
			Reporter.log("Search text not displayed in search field in unav");
			Assert.fail("Search text not displayed in search field in unav ");
		}
		return this;
	}

	/**
	 * Verify pin changing alert divison
	 */
	public HomePage verifyPinChangingAlertDiv() {
		try {
			waitforSpinner();
			checkPageIsReady();
			pinChangingAlertDiv.isDisplayed();
			Reporter.log("Pin change alert divison displayed");
		} catch (Exception e) {
			Reporter.log("Pin change alert divison not displayed");
			Assert.fail("Pin change alert divison not displayed ");
		}
		return this;
	}

	/***
	 * verifying the auto complete functionality for line selector field
	 * 
	 * @param expectedValue
	 * @return
	 */
	public HomePage verifyAutoCompleteSearchResultsForHomePage(String expectedValue) {
		int actListOfValuesThatMatchedTheSearchValue = 0;
		try {
			checkPageIsReady();
			if (isElementDisplayed(searchResults.get(0))) {
				for (WebElement ele : searchResults) {
					if (ele.getText().trim().toLowerCase().contains(expectedValue.toLowerCase())) {
						actListOfValuesThatMatchedTheSearchValue++;
					}
				}
				Assert.assertEquals(searchResults.size(), actListOfValuesThatMatchedTheSearchValue);
				Reporter.log("Autocomplete search functionality works for search field");
			} else {
				Reporter.log("No results found");
			}
		} catch (Exception e) {
			Assert.fail("failed to verify the Autocomplete search functionality for search field");
		}
		return this;
	}

	/**
	 * Verify PuertoRico customer
	 * 
	 * @return
	 */
	public HomePage verifyPuertoRicoCustomerHeader() {
		try {
			Assert.assertTrue(isElementDisplayed(PuertoRicoCustomerHeader));
			Reporter.log("PuertoRico customer header displayed");
		} catch (AssertionError e) {
			Assert.fail("PuertoRico customer header not displayed");
		}
		return this;
	}

	/**
	 * Verify VIP customer header
	 * 
	 * @return
	 */
	public HomePage verifyVIPCustomerHeader() {
		try {
			Assert.assertTrue(isElementDisplayed(vipCustomerHeader));
			Reporter.log("VIP customer header displayed");
		} catch (AssertionError e) {
			Assert.fail("VIP customer header not displayed");
		}
		return this;
	}

	/**
	 * Verify Government customer header
	 * 
	 * @return
	 */
	public HomePage verifyGovernmentCustomerHeader() {
		try {
			Assert.assertTrue(isElementDisplayed(governmentCustomerHeader));
			Reporter.log("Government customer header displayed");
		} catch (AssertionError e) {
			Assert.fail("Government customer header not displayed");
		}
		return this;
	}

	/**
	 * Verify CSRF token for all services
	 * 
	 * @return
	 */
	public HomePage verifyCSRFToken(WebDriver driver) {
		try {
			List<String> t_values = new ArrayList<String>();
			List<LogEntry> entries = driver.manage().logs().get(LogType.PERFORMANCE).getAll();
			for (LogEntry entry : entries) {
				String service_String = entry.getMessage();
				if (service_String.contains("eservice/servlet")) {
					JSONObject service_JsonObj = new JSONObject(service_String);
					JSONObject message = service_JsonObj.getJSONObject("message");
					JSONObject param = message.getJSONObject("params");
					Iterator<?> x = param.keys();
					JSONArray array = new JSONArray();
					String k = null;
					while (x.hasNext()) {
						String key = (String) x.next();
						if (key.equals("request") || (key.equals("response"))) {
							k = key;
							array.put(param.get(key));
							break;
						}
					}
					Iterator<?> i1;
					JSONObject head;
					String token_Value;
					for (int i = 0; i < array.length(); i++) {
						JSONObject objects = array.getJSONObject(i);
						if (k.equals("request")) {
							head = objects.getJSONObject("headers");
							i1 = head.keys();
							while (i1.hasNext()) {
								String key = (String) i1.next();
								if (key.equals("x-xsrf-token")) {
									token_Value = head.getString(key);
									t_values.add(token_Value);
									break;
								}
							}
						} else if (k.equals("response")) {
							head = objects.getJSONObject("requestHeaders");
							i1 = head.keys();
							while (i1.hasNext()) {
								String key = (String) i1.next();
								if (key.equals("x-xsrf-token")) {
									token_Value = head.getString(key);
									t_values.add(token_Value);
									break;
								}
							}
						}
					}
					int j = t_values.size();
					while (j > 1) {
						if (!((t_values.get(j - 1)).equals(t_values.get(j - 2)))) {
							Reporter.log("CSRF token is not same for all the services");
						}
						j--;
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("CSRF token is not same for all the services ");
		}
		return this;
	}

	/**
	 * Verify portin link
	 * 
	 * @return
	 */
	public HomePage verifyPortInLink() {
		try {
			Assert.assertTrue(isElementDisplayed(portInLink));
			Reporter.log("Portin link displayed");
		} catch (AssertionError e) {
			Reporter.log("Portin link not displayed");
			Assert.fail("Portin link not displayed");
		}
		return this;
	}

	/**
	 * Click portin link
	 * 
	 * @return
	 */
	public HomePage clickPortInLink() {
		try {
			portInLink.click();
			Reporter.log("Clicked on portin link");
		} catch (Exception e) {
			Assert.fail("Click failed on portin link");
		}
		return this;
	}

	/**
	 * Verify Autopay is scheduled
	 * 
	 * @return
	 */
	public boolean verifyAutopayisscheduled() {
		boolean display = false;
		try {
			if (autoPayLink.isDisplayed()) {
				display = true;
			}
			Reporter.log("Autopay is scheduled");
		} catch (AssertionError e) {
			Reporter.log("Autopay is not scheduled");
		}
		return display;
	}

	/**
	 * clcik on autopay link
	 * 
	 * @return
	 */
	public HomePage clickonAutopayLink() {

		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(autoPayLink));
			moveToElement(autoPayLink);
			autoPayLink.click();
			Reporter.log("AutopayLink is clicked");
		} catch (AssertionError e) {
			Reporter.log("AutopayLink is not clicked");
		}
		return this;
	}

	public HomePage URLredirecttoAutopay() {

		try {
			checkPageIsReady();
			getDriver().get(System.getProperty("environment") + "/payments/autopay");
			Reporter.log("Page redirected to Autopay");
		} catch (AssertionError e) {
			Reporter.log("Failed in page redirection to autopay page");
		}
		return this;
	}

	/**
	 * Click On Shop link On Menu
	 * 
	 * @return
	 */
	public HomePage clickShoplinkOnMenu() {
		checkPageIsReady();
		try {
			shopLinkMenu.click();
			Reporter.log("Clicked on shop page link On Menu");
		} catch (Exception e) {
			Assert.fail("Shop link on Menu not found");
		}
		return this;
	}

	/**
	 * Click setupPAlink link
	 */
	public boolean clickSetupPaymentArrangementLink() {
		boolean isDisplayed = false;
		try {
			if (setupPAlink.isDisplayed()) {
				isDisplayed = true;
			}
			setupPAlink.click();
			Reporter.log("Clicked on setupPAlink on Home page");
		} catch (Exception e) {
			Reporter.log("setupPAlink not found");
		}
		return isDisplayed;
	}

	/**
	 * verify manage payments link is displayed
	 */
	public void verifyManagePaymentsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(managePaymentsLink));
			Assert.assertTrue(managePaymentsLink.isDisplayed());
			Reporter.log("Manage payments link is displayed");
		} catch (Exception e) {
			Assert.fail("Manage payments link not found");
		}
	}

	/**
	 * click on manage payments link
	 */
	public void clickManagePaymentsLink() {
		try {
			managePaymentsLink.click();
			Reporter.log("clicked on Manage payments link");
		} catch (Exception e) {
			Assert.fail("Manage payments link not found");
		}
	}

	/**
	 * clcik on upgrade link
	 * 
	 * @return
	 */
	public HomePage clickUpgradeLink() {

		try {
			checkPageIsReady();
			upgradeLink.click();
			Reporter.log("upgrade Link is clicked");
		} catch (AssertionError e) {
			Reporter.log("Failed to click upgrade Link");
		}
		return this;
	}

	/**
	 * Verify Add a line link is displayed or not displayed
	 * 
	 * @return
	 */
	public boolean verifyAddaLineLink() {
		boolean display = false;
		try {
			if (addalinelink.isDisplayed()) {
				display = true;
				Reporter.log("Add a line link is displayed");
			} else {
				Reporter.log("Add a line link is not  displayed");
			}

		} catch (Exception e) {
			Reporter.log("Add a line link is not found");
		}
		return display;
	}

	/**
	 * Click Addaline link
	 */
	public HomePage clickAddaLineLink() {
		try {
			addalinelink.click();
			Reporter.log("Clicked on addalinelink on Home page");
		} catch (Exception e) {
			Assert.fail("addalinelink not found");
		}
		return this;
	}

	/**
	 * Clicks on Plan Link
	 */

	public HomePage clickAccountLink() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				mobileNavMenu.click();
				Reporter.log("Clicked on Mobile Menu link");
			}
			waitFor(ExpectedConditions.visibilityOf(accountIcon));
			accountIcon.click();
			Reporter.log("Clicked on Account link");
		} catch (Exception e) {
			Assert.fail("Account link not found");
		}
		return this;
	}

	/**
	 * Clicks on change My PlanLink
	 */

	public HomePage clickOnChangeMyPlanLink() {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickElementWithJavaScript(changeMyPlanLink);
			Reporter.log("Clicked on change My Plan Link");
		} catch (Exception e) {
			Assert.fail("change My Plan link not found");
		}
		return this;
	}

	/**
	 * ConfirmDone Button Display
	 */

	public boolean verifyCallConfirmDoneBtn() {
		try {
			// waitFor(ExpectedConditions.visibilityOf(callConfirmDoneBtn));
			if (callConfirmDoneBtn.isDisplayed()) {
				return callConfirmDoneBtn.isDisplayed();
			}
		} catch (Exception e) {
			return false;
		}
		return false;

	}

	public void verifyPageElements() {
		try {
			Class<?> objClass = this.getClass();
			Map<WebElement, String> elementList = new HashMap<WebElement, String>();
			Field[] fields = objClass.getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				String elementName = field.getName();

				if (field.getGenericType().toString().contains("WebElement")) {
					if (field.getType().equals(List.class)) {
						Reporter.log("---------------------------------------------------------");
						Reporter.log("Its list format: " + elementName);
						Reporter.log("xpath: " + field.getAnnotation(FindBy.class).xpath());
						Reporter.log("css: " + field.getAnnotation(FindBy.class).css());
						Reporter.log("linkText: " + field.getAnnotation(FindBy.class).linkText());
						Reporter.log("Id: " + field.getAnnotation(FindBy.class).id());
						Reporter.log("Name: " + field.getAnnotation(FindBy.class).name());
						Reporter.log("PartialLinkText: " + field.getAnnotation(FindBy.class).partialLinkText());
						Reporter.log("Class: " + field.getAnnotation(FindBy.class).className());

						// Reporter.log("xpath: "
						// +field.getAnnotation(FindAll.class).);

					} else {
						WebElement pageElement = getelement((WebElement) field.get(this));
						if (pageElement != null) {
							if (!elementList.containsKey(pageElement)) {
								elementList.put(pageElement, elementName);
							} else {
								Reporter.log("---------------------------------------------------------");
								Reporter.log("duplicates are :" + elementName + " &  " + elementList.get(pageElement));
							}
						}
					}
				} else {
					Reporter.log("---------------------------------------------------------");
					Reporter.log("Element Name: " + elementName + " & Element type: " + field.getType());
				}
			}

		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}

	}

	public WebElement getelement(WebElement pageElement) {
		try {
			pageElement.isDisplayed();
		} catch (Exception e) {
			return null;
		}
		return pageElement;
	}
	
	/**
	 * Click On Phones Link on UNAV tab
	 */
	public HomePage clickOnPhonesLink() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				mobileMenuLink.click();
				clickElementWithJavaScript(phonesLink.get(0));
				Reporter.log("Clicked on Phones & devices link");
			} else {
				clickElementWithJavaScript(phonesLink.get(0));
				Reporter.log("Clicked on Phones & devices link");
			}
			
		} catch (Exception e) {
			Assert.fail("Failed to disply Phones & devices link");
		}
		return this;
	}
	
	

}