package com.tmobile.eservices.qa.pages.payments;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
//import com.google.common.base.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class UsageDetailsPage extends CommonPage {

	@FindBy(css = "button.PrimaryCTA")
	private WebElement downloadUsageRecordsCTA;

	@FindBy(css = "span.arrow-back")
	private WebElement backButton;

	@FindBy(css = "ul.pagination.justify-content-center")
	private WebElement pagiNation;

	@FindBy(css = "div#dropdownId")
	private WebElement billCycle;

	@FindBy(css = "div#dropdownId span[ng-model='selectedVal']")
	private WebElement defaultBillCycleValue;

	@FindBy(css = "div#dropdownId ul.dropdown-menu li.dropdown-option")
	private List<WebElement> billCycleSelectorOptions;

	@FindBy(css = "div.TMO_USAGE_DETAILS div.blade-background-color")
	private WebElement usageDetailsPage;

	@FindBy(xpath = "//button[contains(text(),'Filter')]")
	private WebElement filterButton;

	@FindBy(css = "div.container div.row.body.d-flex.padding-bottom-medium-lg")
	private List<WebElement> data;

	@FindBy(css = "div.container div.row.body.d-flex.padding-bottom-medium-lg div.col-md-2.col-12.d-inline-block")
	private List<WebElement> dataList;

	@FindBy(css = "div.col-2.d-inline-block i")
	private List<WebElement> sortingArrow;

	@FindBy(css = "p.Display3.text-center")
	private WebElement header;

	@FindBy(css = "i.clear-icon")
	private WebElement clearicon;

	@FindBy(css = "span.ml-2")
	private List<WebElement> filtertext;

	@FindBy(xpath = "//div[@role='link']")
	private List<WebElement> datalinks;

	@FindBy(xpath = "//a[@aria-label='Go to Next Page']")
	private List<WebElement> nextpagelink;

	@FindBy(xpath = "//a[@aria-label='Go to Previous Page']")
	private List<WebElement> previouspagelink;

	@FindBy(xpath = "//a[@aria-label='Go to Page1']")
	private List<WebElement> page1;

	@FindBy(xpath = "//li[contains(@class,'dropdown-option')]")
	private List<WebElement> dropdownprevious;

	@FindBy(xpath = "//div[@class='row body d-flex padding-vertical-xsmall']/div")
	private List<WebElement> tableheaders;

	@FindBy(css = "ul.dropdown-menu.show")
	private List<WebElement> dropdownmenushow;

	@FindBy(css = "li.dropdown-option.mx-1.mt-1")
	private List<WebElement> alloptionsofdropdown;

	@FindBy(xpath = "//a[@aria-label='Go to Next Page' and @class='page-lin p-l-10']")
	private List<WebElement> nextbutton;

	@FindBy(css = "div.preloader-wrapper.active")
	private List<WebElement> spinner;

	@FindBy(css = "p.pull-left")
	private WebElement Totaltext;

	@FindBy(css = "p.pull-right")
	private WebElement Totalchargestext;

	@FindBy(css = "p.text-center.eyebrow.black")
	private WebElement name;

	@FindBy(xpath = "//div[@role='rowgroup']/div[contains(@class,'row body d-flex')]/div[3]/span[contains(@class,'float-left')]")
	private WebElement msisdn;

	@FindBy(xpath = "//div[@role='rowgroup']/div[contains(@class,'row body d-flex')]/div[2]")
	private WebElement address;

	@FindBy(css = "div.dropdown-toggle span[ng-model='selectedVal']")
	private WebElement selectedBillCycle;

	private final String pageUrl = "details";

	public UsageDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public UsageDetailsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @return
	 */
	public UsageDetailsPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(usageDetailsPage), 5);
			}
			Reporter.log("Usage details page is loaded");
		} catch (Exception e) {
			Assert.fail("Failed to load Usage details page");
		}
		return this;
	}

	/**
	 * Verify Usage Details Page
	 * 
	 * @return
	 */
	public UsageDetailsPage verifyUsageDetailsPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(pageUrl));
			waitFor(ExpectedConditions.visibilityOf(usageDetailsPage));
			Assert.assertTrue(usageDetailsPage.isDisplayed());
			Reporter.log("Usage details page is displayed");
		} catch (Exception e) {
			Assert.fail("Usage details page is not displayed");
		}
		return this;
	}

	/**
	 * Verify Download Usage Records CTA
	 * 
	 * @return
	 */
	public UsageDetailsPage verifyDownloadUsageRecordsCTA() {
		try {
			checkPageIsReady();
			Verify.assertTrue(downloadUsageRecordsCTA.isDisplayed(), "Download usage records CTA is not displayed");

			// Assert.assertTrue(downloadUsageRecordsCTA.isDisplayed(),
			// "Download usage records CTA is not displayed");
		} catch (Exception e) {
			Verify.fail("Download button is not Existing");
		}
		return this;
	}

	/**
	 * Click Back Button
	 * 
	 * @return
	 */
	public UsageDetailsPage clickBackButton() {
		checkPageIsReady();
		try {
			Assert.assertTrue(backButton.isDisplayed(), "Back button is not displayed");
			backButton.click();
		} catch (Exception e) {
			Assert.fail("Back button is not displayed");
		}
		return this;
	}

	/**
	 * Verify Pagination
	 * 
	 * @return
	 */
	public UsageDetailsPage verifyPagination() {
		try {
			Assert.assertTrue(pagiNation.isDisplayed(), "Pagination is not displayed");
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * Select Bill Cycle Selector
	 * 
	 * @return
	 */
	public UsageDetailsPage selectBillCycleSelector() {
		try {
			billCycle.click();
			for (WebElement webElement : billCycleSelectorOptions) {
				waitFor(ExpectedConditions.visibilityOf(webElement));
				webElement.click();
				break;
			}
		} catch (Exception e) {
			Assert.fail("Bill cycle selector is not displayed");
		}
		return this;
	}

	/**
	 * Verify Usage Bill Cycles
	 * 
	 * @return
	 */
	public UsageDetailsPage verifyUsageBillCycles() {
		try {
			int size = billCycleSelectorOptions.size();
			Assert.assertEquals(size, 19);
		} catch (Exception e) {
			Assert.fail("Bill cycle is not displayed");
		}
		return this;
	}

	/**
	 * Verify Default Bill Cycle Value
	 * 
	 * @param option
	 * @return
	 */
	public UsageDetailsPage verifyDefaultBillCycleValue(String option) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(defaultBillCycleValue), 5000);
			Assert.assertTrue(defaultBillCycleValue.getText().contains(option));
		} catch (Exception e) {
			Assert.fail("Default bill cycle value is not displayed");
		}
		return this;
	}

	/**
	 * Click On Filter
	 * 
	 * @return
	 */
	public UsageDetailsPage clickOnFilter() {
		try {
			filterButton.click();
		} catch (Exception e) {
			Assert.fail("Filter button is not displayed");

		}
		return null;
	}

	/**
	 * Select Data
	 * 
	 * @return
	 */
	public UsageDetailsPage selectData() {
		try {
			for (WebElement webElement : data) {
				webElement.click();
				break;
			}
		} catch (Exception e) {
			Assert.fail("Unable to select the data");
		}
		return null;
	}

	/**
	 * Click Sorting Arrow
	 * 
	 * @return
	 */
	public UsageDetailsPage clickSortingArrow() {
		try {
			for (WebElement webElement : sortingArrow) {
				webElement.click();
				break;
			}
		} catch (Exception e) {
			Assert.fail("Sorting arrow 'Type' is not cliked");
		}
		return null;
	}

	/**
	 * Verify Sorting
	 * 
	 * @return
	 */
	public boolean verifySorting() {
		boolean sorted = true;
		try {
			List<String> list = new ArrayList<String>();
			for (WebElement webElement : dataList) {
				list.add(webElement.getText());
			}
			for (int i = 1; i < list.size(); i++) {
				if (list.get(i - 1).compareTo(list.get(i)) > 0)
					sorted = false;
			}
			Assert.assertTrue(sorted, "Data is not in asending order");
			Reporter.log("Data is in asending order");
		} catch (Exception e) {
			Assert.fail("Data is not in asending order");
		}
		return sorted;
	}

	public UsageDetailsPage checkHeader(String strheader) {
		try {
			if (!header.getText().trim().equalsIgnoreCase(strheader))
				Assert.fail("Header is not" + strheader);
			else
				Reporter.log("header is " + strheader);

		} catch (Exception e) {
			Assert.fail("Filter button is not displayed");
		}
		return null;
	}

	public UsageDetailsPage verifyUsageDetailsPage(String Header) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(pageUrl));
			waitFor(ExpectedConditions.visibilityOf(usageDetailsPage));
			Assert.assertTrue(usageDetailsPage.isDisplayed());
			checkHeader(Header);
			Reporter.log("Usage details page is displayed");
		} catch (Exception e) {
			Assert.fail("Usage details page is not displayed");
		}
		return this;
	}

	public int getheadercolumn(String strheader) {
		for (int i = 0; i < tableheaders.size(); i++) {
			if (tableheaders.get(i).getText().trim().equalsIgnoreCase(strheader))
				return i;
		}
		return -1;
	}

	public UsageDetailsPage checkfiltertext(String textfilter) {
		try {
			checkPageIsReady();

			int count = 1;
			while (count < 20) {
				if (filtertext.size() > 0) {
					// if(textfilter.toLowerCase().contains(filtertext.get(0).getText().toLowerCase()))

					if (filtertext.get(0).getAttribute("aria-label").toLowerCase().replaceAll("\\s+", "")
							.contains(textfilter.toLowerCase().replaceAll("\\s+", "")))

					{
						Reporter.log("filer is checked " + textfilter);
					} else {
						Assert.fail("filter value is not matching");
					}
					return this;
				} else {
					Thread.sleep(1000);
				}

			}

			Assert.fail("filter text is not displayed");

		} catch (Exception e) {
			Assert.fail("given element description is not correct");
		}

		return this;
	}

	public UsageDetailsPage checkdagetlinksonfilter(boolean isfilteron) {
		try {
			Thread.sleep(3000);
			List<WebElement> datalinks1 = getDriver().findElements(By.xpath("//div[@role='link']"));
			int dlinks = datalinks1.size();

			if ((isfilteron && dlinks > 0) || (!isfilteron && dlinks < 1)) {
				// Reporter.log("filter is working good");
			} else {
				Assert.fail("filter is not working good");
			}

		} catch (Exception e) {
			Assert.fail("links of data is not existing");
		}
		return null;
	}

	public List<WebElement> getallrowdatfromgivencolumn(int cloumn, String headerval) {
		List<WebElement> vals;
		if (headerval.equalsIgnoreCase("Number")) {
			vals = getDriver()
					.findElements(By.xpath("//div[@role='rowgroup']/div[contains(@class,'row body d-flex')]/div["
							+ cloumn + "]/span[contains(@class,'float-left')]"));
		} else {
			vals = getDriver().findElements(
					By.xpath("//div[@role='rowgroup']/div[contains(@class,'row body d-flex')]/div[" + cloumn + "]"));
		}

		return vals;
	}

	public UsageDetailsPage addelementtolist(List<WebElement> values, List<String> strs) {
		try {
			for (WebElement el : values) {
				strs.add(el.getText().trim());
			}
		} catch (Exception e) {
			Assert.fail("data in table is not identified");

		}
		return this;
	}

	public String clickongivenelemt(WebElement tag) {
		String[] tagval = tag.getText().trim().split(",");
		checkPageIsReady();
		clickElementWithJavaScript(tag);
		// tag.click();
		return tagval[0];
	}

	public UsageDetailsPage checkfilteredvalues(List<WebElement> vals, String textval) {
		try {
			checkPageIsReady();
			for (WebElement val : vals) {
				String[] valtext = val.getText().trim().split(",");
				if (textval.contains(valtext[0])) {
					// if(valtext[0].contains(textval)) {
					// if(val.getText().trim().equalsIgnoreCase(textval))

					Reporter.log("filter is woking good for" + valtext[0]);

				} else {
					Assert.fail("not matching");

				}

			}
		} catch (Exception e) {
			Assert.fail("no elements find in table");
		}
		return this;
	}

	public UsageDetailsPage closefilter() {
		try {
			clearicon.click();
		} catch (Exception e) {
			Reporter.log("clear icon button was not existing");
		}
		return this;
	}

	public boolean checknextenable() {
		if (nextpagelink.size() > 0) {
			if (nextpagelink.get(0).getAttribute("class").equalsIgnoreCase("page-lin p-l-10"))
				return true;
			else
				return false;
		} else {
			return false;
		}
	}

	public UsageDetailsPage clicknexpage() {
		try {
			nextpagelink.get(0).click();
		} catch (Exception e) {
			Reporter.log("next page link  not existing");
		}
		return this;
	}

	public boolean checkpreviousenable() {
		if (previouspagelink.size() > 0) {
			if (previouspagelink.get(0).getAttribute("class").equalsIgnoreCase("page-lin p-l-10"))
				return true;
			else
				return false;
		} else {
			return false;
		}
	}

	public UsageDetailsPage clickpreviouspage() {
		try {
			previouspagelink.get(0).click();
		} catch (Exception e) {
			Reporter.log("previous page link  not existing");
		}
		return this;
	}

	public UsageDetailsPage checkappisinfirstpage() {
		try {
			/*
			 * boolean irun=true; while(irun) { irun=checkpreviousenable();
			 * if(irun)clickpreviouspage(); Thread.sleep(1000); }
			 */
			if (page1.size() > 0)
				page1.get(0).click();
			Thread.sleep(1000);

		} catch (Exception e) {
			Reporter.log("previous page link  not existing");
		}
		return this;
	}

	public UsageDetailsPage keepcolumndscorder(int elementindex) {
		try {

			while (tableheaders.get(elementindex)
					.findElements(By.xpath(".//i[@class='arrow-down ml-2 ng-star-inserted']")).size() < 1) {
				tableheaders.get(elementindex).click();
				Thread.sleep(1000);
			}
		} catch (Exception e) {

		}

		return this;
	}

	public List<String> getalldatainarray(int headercol) {
		List<String> reqarray = new ArrayList<String>();
		boolean irun = true;
		while (irun) {
			List<WebElement> vals = getallrowdatfromgivencolumn(headercol + 1, "");
			addelementtolist(vals, reqarray);
			irun = checknextenable();
			if (irun)
				clicknexpage();

		}
		return reqarray;
	}

	public UsageDetailsPage keepcolumnascorder(int elementindex) {
		try {

			while (tableheaders.get(elementindex)
					.findElements(By.xpath(".//i[@class='arrow-up ml-2 ng-star-inserted']")).size() < 1) {
				tableheaders.get(elementindex).click();
				Thread.sleep(1000);
			}
		} catch (Exception e) {

		}

		return this;
	}

	public UsageDetailsPage clickdropdown() {
		try {
			checkPageIsReady();
			int cntloop = 0;
			while (cntloop < 10) {
				billCycle.click();
				if (dropdownmenushow.size() > 0)
					break;
				else
					Thread.sleep(1000);
			}
			if (cntloop == 10)
				Verify.fail("Drop down is not opening");

		} catch (Exception e) {

			Verify.fail("Drop down is not existing");
		}

		return this;
	}

	public UsageDetailsPage checkprevioususagedropdowns() {
		try {
			if (dropdownprevious.size() > 0)
				Reporter.log("Previuos usage dropdown");
			else
				Assert.fail("Previuos usage dropdown not exist");

		} catch (Exception e) {
			Verify.fail("Drop down is not existing");
		}

		return this;
	}

	/**
	 * Select Bill Cycle Selector
	 * 
	 * @return
	 */
	public UsageDetailsPage selectBillCyclewithData() {
		try {
			billCycle.click();
			checkPageIsReady();

			for (WebElement webElement : billCycleSelectorOptions) {
				waitFor(ExpectedConditions.visibilityOf(webElement));
				webElement.click();
				checkPageIsReady();
				Thread.sleep(3000);

				if (!data.isEmpty()) {
					break;
				}

			}
		} catch (Exception e) {
			Assert.fail(" data is not available for all cycles");
		}
		return this;
	}

	public UsageDetailsPage checkMsdsinFormat(List<WebElement> vals) {
		try {
			checkPageIsReady();
			Thread.sleep(1000);
			for (WebElement val : vals) {

				String[] valtext = val.getText().split("(");

				if (valtext[1].charAt(0) == 1) {
					Assert.fail("MSDSIN started with 1-failed");

				}

			}

		} catch (Exception e) {

		}
		return this;
	}

	public List<WebElement> getallusagecycles() {
		try {
			return alloptionsofdropdown;
		} catch (Exception e) {
			Assert.fail("Month links are not existing");

		}
		return null;
	}

	public boolean checkhtenextbuttonenable() {
		try {
			return nextbutton.size() > 0 ? true : false;
		} catch (Exception e) {
			Assert.fail("nextbutton elements are not present");

		}
		return false;
	}

	public UsageDetailsPage clicknextbutton() {
		try {
			nextbutton.get(0).click();

		} catch (Exception e) {
			Verify.fail("nextbutton is not present");
		}

		return this;
	}

	public UsageDetailsPage waittillspinnerdisapper() {
		try {
			checkPageIsReady();
			int sizespinner = 1;
			int vartime = 0;
			while (sizespinner > 0) {
				Thread.sleep(1000);
				vartime++;
				sizespinner = spinner.size();
				if (vartime == 30)
					Assert.fail("end less spinner");
			}

		} catch (Exception e) {
			Verify.fail("nextbutton is not present");
		}

		return this;
	}

	public String checkmobilenumber(WebElement ele, int colindex) {
		try {

			String mbnumber = ele
					.findElement(By.xpath(".//../div[" + colindex + "]/span[contains(@class,'float-left')]")).getText()
					.trim();
			mbnumber = mbnumber.replace(" ", "");
			if (mbnumber.length() == 13 && mbnumber.charAt(0) == '(') {
				Pattern pattern = Pattern.compile("\\([2-9]{1}[0-9]{2}\\)\\d{3}-?\\d{4}");
				Matcher matcher = pattern.matcher(mbnumber);
				if (matcher.matches()) {
					return "valid";
				} else {
					return "invalid";
				}

			}
		} catch (Exception e) {

		}

		return null;
	}

	public UsageDetailsPage checkFilterbuttonExists() {
		try {
			if (filterButton.isEnabled())
				Reporter.log("Filter button is existing");
			else
				Verify.fail("Filter button is not existing");

		} catch (Exception e) {
			Verify.fail("Filter button is not exited");
		}

		return this;
	}

	public UsageDetailsPage checkTotalsmessage(String val) {
		try {
			if (Totaltext.isDisplayed())
				Reporter.log("text printing " + Totaltext.getText());
			else
				Verify.fail("Total  text for " + val + " on Left side is not printing");

		} catch (Exception e) {
			Verify.fail("Total " + val + " test is not exiting");
		}

		return this;
	}

	public UsageDetailsPage checkTotalcharges(String val) {
		try {
			if (Totalchargestext.isDisplayed())
				Reporter.log("text printing " + Totalchargestext.getText());
			else
				Verify.fail("Total charges text for " + val + " on right side is not printing");

		} catch (Exception e) {
			Verify.fail("Total charges " + val + " test is not exiting");
		}

		return this;
	}

	public String getmessagecountfromheader() {

		try {
			if (Totaltext.isDisplayed()) {

				String[] al = Totaltext.getText().trim().split(":");

				return al[1].trim().split(" ")[0];

			}

		} catch (Exception e) {
			Verify.fail("Totaltest is not exiting");
		}

		return null;
	}

	public UsageDetailsPage checkwhetherpaginationimplemented(String colname) {
		try {

			String reqtext = getmessagecountfromheader();
			int vals = Integer.parseInt(reqtext);
			if (vals > 50) {
				if (checkhtenextbuttonenable())
					Reporter.log("Pagination implemented for records more than 50");
				else
					Verify.fail("Pagination is not implemented for records more than 50");
			}

		} catch (Exception e) {
			Verify.fail("Usage details has no rows");
		}

		return this;
	}

	public UsageDetailsPage checkrowbasedonmessagetext(String colname) {
		try {
			String reqtext = getmessagecountfromheader();

			if (!reqtext.trim().equalsIgnoreCase("0") && !reqtext.trim().equalsIgnoreCase("0.00")) {
				verifyDownloadUsageRecordsCTA();
				int clon = getheadercolumn(colname);
				if (clon != 0) {
					List<WebElement> vals = getallrowdatfromgivencolumn(clon + 1, colname);
					if (vals.size() > 0)
						Reporter.log("recordes are printing ");
					else
						Verify.fail("recordes are printing zero which is wrong");

				}
			}

		} catch (Exception e) {
			Verify.fail("Usage details has no rows");
		}

		return this;
	}

	public UsageDetailsPage verifyDateSelectorDropdownCollapsed() {
		try {
			checkPageIsReady();

			Assert.assertFalse(dropdownmenushow.size() > 0, "Drop down is not collapsing");

		} catch (Exception e) {

			Assert.fail("Drop down is not collapsing");
		}

		return this;
	}

	/**
	 * verify pii masking for name
	 * 
	 * @param piiCustomerCustfirstnamePid
	 */
	public void verifyUserNamePiiMasking() {
		try {
			checkPageIsReady();
			scrollToElement(name);
			Assert.assertTrue(chkElementisPIIMasked(name, "cust_name"));
			Reporter.log("Customer Name is PII masked");
		} catch (Exception e) {
			Assert.fail("Customer Name is not PII masked");
		}
	}

	/**
	 * verify pii masking for
	 * 
	 * @param
	 */
	public void verifyPiiMasking(WebElement element, String headerval) {
		try {
			if (headerval.trim().equalsIgnoreCase("Destination")) {

				Assert.assertTrue(chkElementisPIIMasked(element, "cust_city"));
				Reporter.log("cust_city is PII masked");
			} else if (headerval.trim().equalsIgnoreCase("Number")) {
				Assert.assertTrue(chkElementisPIIMasked(element, "cust_msisdn"));
				Reporter.log("cust_msisdn is PII masked");
			}
			// Assert.assertTrue(chkElementisPIIMasked(element, Pid));

		} catch (Exception e) {
			Assert.fail("Pid is not PII masked");
		}
	}

	/*	*//**
			 * verify pii masking for msisdn
			 * 
			 * @param piiCustomerMsisdnPid
			 */

	/*
	 * public void verifyMsisdnPiiMasking(String CustomerMsisdnPid,WebElement
	 * element) { try { Assert.assertTrue(chkElementisPIIMasked(element,
	 * CustomerMsisdnPid)); Reporter.log("MSISDN is PII masked"); } catch
	 * (Exception e) { Assert.fail("MSISDN is not PII masked"); } }
	 * 
	 *//**
		 * verify pii masking for Address
		 * 
		 * @param
		 *//*
		 * public void verifyAddressPiiMasking(String CustomerMsisdnPid) { try {
		 * Assert.assertTrue(chkElementisPIIMasked(address, CustomerMsisdnPid));
		 * Reporter.log("Address is PII masked"); } catch (Exception e) {
		 * Assert.fail("Address is not PII masked"); } }
		 */

	public UsageDetailsPage verifySelectedBillCycleDisplayed(String billcycle) {
		try {
			checkPageIsReady();

			Assert.assertTrue(selectedBillCycle.getText().contains(billcycle), "selected bill cycle not displayed");

		} catch (Exception e) {

			Assert.fail("selected bill cycle not displayed");
		}

		return this;
	}

	/**
	 * Check PII Masking for all personal and payment information
	 * 
	 * @param pidElement
	 * @param piiMasking
	 * @return true/false
	 */
	public boolean chkElementisPIIMasked(WebElement pidElement, String piiMasking) {
		boolean isMasked = false;
		try {
			String pidAttr = pidElement.getAttribute("pid");
			if (!pidAttr.isEmpty() && pidAttr.contains(piiMasking)) {
				isMasked = true;
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking");
		}
		return isMasked;
	}

}
