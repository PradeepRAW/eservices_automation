package com.tmobile.eservices.qa.pages.payments;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author pshiva
 *
 */
public class AutoPayPage extends CommonPage {

	private int i = 0;
	private Long accNumber = 0L;

	@FindBy(css = "span.pointer.glueSpan.ng-binding")
	private WebElement cancelAutoPayLink;

	/*@FindBy(linkText = "See FAQs")
	private WebElement autoPayFAQsLink;
*/
	@FindBy(xpath = "//p[contains(@class, 'padding-top-small')]//span[@class='legal-link cursor']")
	private WebElement autoPayFAQsLink;
	
	@FindBy(css = "h4.h4-title.text-center.text-bold.ng-binding")
	public WebElement autoPayLandingPageHeader;

	@FindBy(css = "p.Display3")
	public WebElement autopayStatus;

	@FindBy(css = "button.btn.btn-primary.padding-bottom-large")
	private WebElement yesCancelAutopay;

	@FindBy(css = "button#acceptButton")
	private WebElement agreeAndSubmitbtn;

	@FindBy(css = "button.btn-secondary.SecondaryCTA")
	private WebElement cancelButton;

	@FindBy(css = "div.modal-content button.btn.btn-primary.padding-bottom-large.PrimaryCTA")
	private WebElement autopayExitModelYesButton;

	@FindBy(css = "div.modal-content button.btn.btn-secondary.SecondaryCTA")
	private WebElement autopayExitModelNoButton;

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	// div#addPayment a
	@FindAll({@FindBy(css = "div#addPayment div.body-copy-description-bold"), @FindBy(css = "#paymentLable")})
	private WebElement paymentMethodBlade;

	@FindBy(css = "div.container.tnc-modal")
	private WebElement tNcModal;

	@FindBy(css = "autopay-faq")
	private WebElement faqModal;

	@FindBy(css = ".h3-title.h4-title-mobile.text-bold")
	private WebElement tNcFaQHeader;

	@FindBy(css = "autopay-faq h4")
	private WebElement faQHeader;

	@FindBy(css = "p.body-bold.black.padding-bottom-xsmall")
	private WebElement FirstPaymentDateOnAutopayLandingPage;

	@FindBy(css = "p.legal.padding-bottom-small")
	private List<WebElement> billDUetDateOnAutopayLandingPage;

	@FindBy(css = "div.col-xs-12.col-sm-12.padding-top-medium p.fine-print-body.ng-scope")
	private WebElement paymentProcessingDateOnAutopayLandingPage;

	@FindBy(linkText = "terms and conditions")
	public WebElement termsNconditions;

	@FindBy(css = "[ng-click='vm.openAutopayExitModal()']")
	public WebElement cancelAutoPay;

	@FindBy(css = "div.tnc-modal button")
	public WebElement backBtn;

	@FindBy(css = "autopay-faq button")
	public WebElement faqBackBtn;

	@FindAll({@FindBy(css = "div.Display4"), @FindBy(css = "div.Display4")})
	private WebElement spokePageHeader;

	@FindAll({@FindBy(xpath = "//p[contains(text(),'Add Card')]"), @FindBy(xpath = "//span[contains(text(),'Add a card')]")})
	private WebElement addCardIcon;

	@FindAll({@FindBy(xpath = "//p[contains(text(),'Add Bank')]"), @FindBy(xpath = "//span[contains(text(),'Add a bank')]")})
	private WebElement addBankIcon;

	@FindAll({@FindBy(css = "div.card-information-title"), @FindBy(css="p.Display3")})
	private WebElement cardHeader;

	@FindAll({ @FindBy(css = "addbank div h4"), @FindBy(css = "p.Display3"), @FindBy(css = "addbank div.bank-heading span") })
	private WebElement bankHeader;

	@FindAll({ @FindBy(css = "#account_name"), @FindBy(css = "#accountName"), @FindBy(css = "#nameonCard") })
	private WebElement accountName;

	@FindAll({@FindBy(id = "name_on_card"), @FindBy(id="cardName")})
	private WebElement cardName;

	@FindAll({@FindBy(id = "card_number"), @FindBy(id="creditCardNumber")})
	private WebElement cardNumber;

	@FindAll({ @FindBy(css = "#routing_number"), @FindBy(css = "#routingNum"), @FindBy(css = "#routingNumber") })
	private WebElement routingNum;

	@FindAll({ @FindBy(css = "#account_number"), @FindBy(css = "#accountNum"), @FindBy(css = "#accountNumber") })
	private WebElement bankAccountNumber;

	@FindBy(id = "expiration_month")
	private WebElement expiryMonth;

	@FindBy(id = "expiration_year")
	private WebElement expiryYear;

	@FindBy(id="expirationDate")
	private WebElement expiryDate;
	
	@FindAll({@FindBy(id = "CVV"), @FindBy(id="cvvNumber")})
	private WebElement cvvCode;

	@FindAll({@FindBy(id = "Zip"), @FindBy(id="zipCode")})
	private WebElement zip;

	@FindAll({@FindBy(css = "div#continue button"), @FindBy(id="acceptButton")})
	private WebElement continueAddCard;

	@FindBy(css = "div h3.text-bold")
	private WebElement cancalAPModal;

	@FindBy(css = "div.inline p.inline")
	private WebElement inlinePaymentMethod;

	@FindBy(css = "#addPayment div.inline.black")
	private WebElement storedPaymentType;

	@FindBy(css = "h4.h4-title.text-center.text-bold")
	public WebElement autoPayLandingPageHeaderNew;

	@FindBy(css = ".text-bold.text-center.m-r-45-md")
	private WebElement autoPayCancelModalHeader;

	@FindBy(css = "button[ng-click='vm.cancel()']")
	private WebElement autoPayCancelModalNoBtn;

	@FindAll({ @FindBy(css = "button.btn.btn-primary.padding-bottom-large.primaryCTA.no-padding.width-md.ng-binding"),
		@FindBy(css = "button.PrimaryCTA") })
	private WebElement continueAddBank;

	@FindBy(css = "span.invalid.text-error.ng-scope")
	private WebElement errorMsg;

	@FindAll({@FindBy(css = "div.body-copy-description-bold"), @FindBy(css="p.Display3")})
	private WebElement pageLoadElement;

	@FindBy(css = "button.glueButtonSecondary")
	private WebElement returnHome;

	private final String pageUrl = "/autopay";
	
	@FindBy(css="span.sprite+p")
	private WebElement storedPaymentNo;
	
	@FindBy(css = "div.body-copy-description-regular p")
	private List<WebElement> autopaynotEligibleModal;
	
	@FindBy(css = "div.col-xs-12.col-sm-12 p")
	private List<WebElement> electronicAgrementsText;
	
	@FindBy(css = "span.align-baseline.ng-scope")
	private WebElement autopayScheduledStatus;
	
	@FindBy(css = "button.btn.btn-primary.padding-bottom-large.PrimaryCTA")
	private WebElement goTOHome;
	
	@FindAll({@FindBy(xpath = "//span[contains(@class,'sprite')]/following-sibling::p"),@FindBy(css="span[pid='cust_crd0']")})
	private WebElement storedPaymentCardNumber;
	
	@FindBy(css="div.glueHyperLink span")
	private WebElement spriteIcon;
	
	
	
	@FindBy(css="div.row.no-margin")
	private WebElement autopayDisallowedModal;
	
	@FindBy(css="div.row.no-margin p")
	private List<WebElement> autopayDisallowedModalText;

	@FindBy(css= "button[ng-click='vm.goToHome()']")
	private WebElement gotoHomePageButton;
	
	@FindBy(css=".Display6.d-flex span")
	private List<WebElement> savedPaymentMethodBlades;

	/**
	 * 
	 * @param webDriver
	 */
	public AutoPayPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AutoPayPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public AutoPayPage verifyPageLoaded() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				getDriver().navigate().refresh();
				Thread.sleep(2000);
				verifyPageUrl();
			} else {
				checkPageIsReady();
				waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
				verifyPageUrl();
				Reporter.log("Autopay page displayed");
			}
		} catch (Exception e) {
			Assert.fail("Autopay page not displayed");
		}
		return this;
	}

	public AutoPayPage clickAutopayExitModelYesButton() {
		try {
			autopayExitModelYesButton.click();
			Reporter.log("Clicked on Auto pay Exit Model Yes Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Auto pay Exit Model Yes Button");
		}
		return this;
	}

	/**
	 * return autopay status
	 * 
	 * @return
	 */
	public boolean verifyAutoPayStatusIsOFF() {
		try {
			waitFor(ExpectedConditions.visibilityOf(autopayStatus));
			return autopayStatus.getText().contains("OFF");
		} catch (Exception e) {
			Assert.fail("Auto pay status field not found");
		}
		return false;
	}

	/**
	 * click cancel auto pay link
	 */
	public AutoPayPage clickCancelAutoPayLink() {
		try {
			checkPageIsReady();
			cancelAutoPayLink.click();
			Reporter.log("Clicked on CancelAuto pay Link");
		} catch (Exception e) {
			Verify.fail("Failed to click cancel Auto pay Link");
		}
		return this;
	}

	/**
	 * click auto pay FAQ's link
	 */
	public AutoPayPage clickAutoPayFAQsLink() {
		try {
			clickElementWithJavaScript(autoPayFAQsLink);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Auto pay FAQs link");
		} catch (Exception e) {
			Assert.fail("Fail to click Auto Pay FAQs Link");
		}
		return this;
	}

	/**
	 * click auto pay cancel modal No button
	 */
	public AutoPayPage clickAutoPayCancelNoBtn() {
		try {
			clickElementWithJavaScript(autoPayCancelModalNoBtn);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Auto pay Cancel No button ");
		} catch (Exception e) {
			Verify.fail("Fail to click on Auto pay Cancel No Btn");
		}
		return this;
	}

	/**
	 * click yes button for cancel auto pay
	 */
	public AutoPayPage clickYesCancelAutoPay() {
		try {
			yesCancelAutopay.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Yes Cancel Auto Pay");
		} catch (Exception e) {
			Assert.fail("Fail to click on Yes Cancel Auto pay");
		}
		return this;

	}

	/**
	 * verify cancel autopay button is not displayed
	 * 
	 * @return true/false
	 */
	public AutoPayPage verifyCancelAutoPayLink() {
		try {
			cancelAutoPayLink.isDisplayed();
		} catch (Exception e) {
			Verify.fail("Fail to Verify Cancel Auto Pay Link");
		}
		return this;
	}

	/**
	 * verify Agree And Submit button is displayed
	 * 
	 * @return true/false
	 */
	public AutoPayPage verifyAgreeAndSubmitBtnEnabled() {
		try {
			checkPageIsReady();
			agreeAndSubmitbtn.isDisplayed();
			agreeAndSubmitbtn.getText().equals("Agree and Submit");
			Reporter.log("Agree and Submit button Enabled");
		} catch (Exception e) {
			Assert.fail("Fail to verify Agree and Submit button enabled");
		}
		return this;
	}

	/**
	 * verify Agree And Submit button is displayed
	 * 
	 * @return true/false
	 */
	public boolean verifyAgreeAndSubmitBtn() {
		checkPageIsReady();
		try {
			if (agreeAndSubmitbtn.isDisplayed() && agreeAndSubmitbtn.getText().equals("Agree & Submit")) {
				return true;
			}
		} catch (NoSuchElementException elementNotFound) {
			Reporter.log("Agree And Submit Button verification step Failed.");
		}
		return false;

	}

	/**
	 * verify cancel button is displayed
	 * 
	 * @return true/false
	 */
	public AutoPayPage verifyCancelBtn() {
		try {
			cancelButton.isDisplayed();
			cancelButton.getText().equals("Cancel");
		} catch (Exception e) {
			Assert.fail("Cancel button not found");
		}
		return this;
	}

	public AutoPayPage clickAddPaymentMethod() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("screen-lock")));
			waitFor(ExpectedConditions.elementToBeClickable(paymentMethodBlade));
			clickElementWithJavaScript(paymentMethodBlade);
			Reporter.log("Clicked on Add Payment Method");
		} catch (Exception e) {
			Assert.fail("Failed to click on Add payment Method");
		}
		return this;
	}

	public AutoPayPage clicktermsNconditions() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(termsNconditions));
			clickElementWithJavaScript(termsNconditions);
			Reporter.log("Clicked on Terms and Conditions");
		} catch (Exception e) {
			Verify.fail("Fail to click on terms and Conditions");
		}
		return this;
	}

	public AutoPayPage clickCancelAutoPayButton() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(cancelAutoPay));
			clickElementWithJavaScript(cancelAutoPay);
			Reporter.log("Clicked on Cancel Auto Pay button");
		} catch (Exception e) {
			Assert.fail("Fail to click Cancel Auto Pay Button");
		}
		return this;
	}

	/**
	 * validate Setup AutoPay page
	 */

	public AutoPayPage verifyAutoPayLandingPageFields() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("autopay"));
			checkPageIsReady();
			if (verifyAutoPayStatusIsOFF()) {
				verifyFirstPaymentDateContent("First Autopay payment:");
			} else {
				verifyNextPaymentDateContent("First Autopay payment: ");
				verifyCancelAutoPayLink();
			}
			verifyBillDueDateContent("Bill due");
			verifyPaymentMethodBlade();
			//verifyPaymentProcessingDateContent();
			autoPayFAQsLink.isDisplayed();
			verifyTnC();
			verifyAgreeAndSubmitBtn();
			//verifyCancelBtn();
			Reporter.log("Autopay landing page validation succesfully finished");
		} catch (Exception e) {
			Assert.fail("Auto Pay Landing page did not load or not found all elements");
		}
		return this;
	}

	/**
	 * validate Autopay Terms and Conditions modal
	 */
	public AutoPayPage validateTnCModal() {
		try {
			checkPageIsReady();
			Assert.assertTrue(tNcModal.isDisplayed(), "Auto Pay Terms and Conditions modal is not displayed");
			Reporter.log("Auto Pay Terms and Conditions modal is not displayed");
			Assert.assertTrue(tNcFaQHeader.getText().equals("Terms and Conditions"),
					"Auto Pay Terms and Conditions header is not displayed");
			Reporter.log("Auto Pay Terms and Conditions header is not displayed");
			Assert.assertTrue(backBtn.isDisplayed(), "Back button is not displayed on TnC modal");
			Reporter.log("Back button is displayed on TnC modal");
			Reporter.log("Autopay Terms & Conditions page is displayed");
		} catch (Exception e) {
			Assert.fail("Fail to validate Terms and Conditions Model");
		}
		return this;
	}

	public AutoPayPage verifyFirstPaymentDateContent(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(FirstPaymentDateOnAutopayLandingPage));
			Verify.assertTrue(FirstPaymentDateOnAutopayLandingPage.getText().contains(msg),
					"Next Payment Date Content is incorrect");
		} catch (Exception e) {
			Verify.fail("Fail to verify the First Payment Date Content");
		}
		return this;
	}

	public AutoPayPage verifyNextPaymentDateContent(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(FirstPaymentDateOnAutopayLandingPage));
			Verify.assertTrue(FirstPaymentDateOnAutopayLandingPage.getText().contains(msg),
					"Next Payment Date Content is incorrect");
		} catch (Exception e) {
			Verify.fail("Fail to verify the Next Payment Date Content");
		}
		return this;
	}

	public AutoPayPage verifyBillDueDateContent(String msg) {
		try {
			waitFor(ExpectedConditions.visibilityOf(billDUetDateOnAutopayLandingPage.get(0)));
			Verify.assertTrue(billDUetDateOnAutopayLandingPage.get(0).getText().contains(msg),
					"Bill Due Date Content is incorrect");
		} catch (Exception e) {
			Verify.fail("Fail to verify Bill Due Date Content");
		}
		return this;
	}

	/***
	 * Verify payment processing date content
	 * 
	 * @return
	 */

	public AutoPayPage verifyPaymentProcessingDateContent() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paymentProcessingDateOnAutopayLandingPage));
			Verify.assertTrue(paymentProcessingDateOnAutopayLandingPage.getText().replace("[0-9]", "")
					.contains("Payments will process on the"), "Payment Processing Date Content is incorrect");
		} catch (Exception e) {
			Verify.fail("Payment processing date content field not found");
		}
		return this;
	}

	/**
	 * Verify Auto Pay Spoke Page
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifyAutoPaySpokePage() {
		try {
			spokePageHeader.isDisplayed();
			Reporter.log("The Auto Pay Spoke Page is displayed");
		} catch (Exception E) {
			Assert.fail("AutoPay Spoke page is NOT displayed");
		}
		return this;
	}

	/**
	 * click add card icon
	 */
	public AutoPayPage clickAddCardIcon() {
		try {
			addCardIcon.click();
			Reporter.log("Clicked on Add Card Icon");
		} catch (Exception e) {
			Assert.fail("Fail to Click Add card Icon");
		}
		return this;

	}

	/**
	 * click add bank icon
	 */
	public AutoPayPage clickAddBankIcon() {
		try {
			checkPageIsReady();
			addBankIcon.click();
			Reporter.log("Clicked on Add Bank Icon");
		} catch (Exception e) {
			Assert.fail("Fail to Click Add Bank Icon");
		}
		return this;
	}

	/**
	 * verify card information header
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifyCardInfoHeader() {
		try {
			cardHeader.isDisplayed();
			Reporter.log("Card Information page header is verified displayed");

		} catch (NoSuchElementException elementNotFound) {
			Assert.fail("Card Information page header verification step Failed");
		}
		return this;
	}

	/**
	 * verify bank information header
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifyBankInfoHeader() {
		try {
			bankHeader.isDisplayed();
			Reporter.log("Bank info header is displayed");
		} catch (Exception e) {
			Assert.fail("Fail to verify Bank Info header");
		}
		return this;
	}

	/**
	 * Fill all the card details
	 *
	 * @param payment
	 */
	public AutoPayPage fillCardInfo(Payment payment) {
		try {
			sendTextData(cardName, payment.getNameOnCard());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(cardNumber));
			sendTextData(cardNumber, payment.getCardNumber());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			sendTextData(expiryMonth, payment.getExpiryMonth());
			sendTextData(expiryYear, payment.getExpiryYear());
			sendTextData(cvvCode, payment.getCvv());
			sendTextData(zip, payment.getZipCode());
			Reporter.log("Filled the Card info details");
		} catch (Exception e) {
			Assert.fail("Failed to fill the Card info details");
		}
		return this;

	}

	/**
	 * click continue button for add Card
	 */
	public AutoPayPage clickContinueButton() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(continueAddCard));
			clickElementWithJavaScript(continueAddCard);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on Continue Button");
		} catch (Exception e) {
			Assert.fail("Fail to click on continue Button");
		}
		return this;
	}

	/**
	 * addCard
	 */
	public AutoPayPage addCardToPaymentMethod(MyTmoData myTmoData) {
		try {
			clickAddPaymentMethod();
			verifyAutoPaySpokePage();
			clickAddCardIcon();
			verifyCardInfoHeader();
			Reporter.log("Card Information page is displayed");
			fillCardInfo(myTmoData.getPayment());
			clickContinueButton();
			verifyAutoPayLandingPageFields();
		} catch (Exception e) {
			Assert.fail("Failed to Add Card to Payment Method");
		}
		return this;
	}

	/**
	 * add payment card if there are no stored payment methods
	 * 
	 * @param myTmoData
	 * @return
	 * @return boolean
	 */
	public String verifyStoredPaymentMethodOrAddBank(MyTmoData myTmoData) {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
		if (!paymentMethodBlade.getText().equals("Using my") || (paymentMethodBlade.getText().equals("Using my") && !storedPaymentType.getAttribute("class").contains("checkAccount"))) {
			addBankToPaymentMethod(myTmoData);
			return "Bank";
		} else if (storedPaymentType.getAttribute("class").contains("checkAccount")) {
			return "Bank";
		} else {
			return "Card";
		}
	}

	/**
	 * verify cancel auto pay modal is displayed
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifycancelAutoPayModalHeader() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
		try {
			waitFor(ExpectedConditions.visibilityOf(cancalAPModal));
			cancalAPModal.isDisplayed();
		} catch (NoSuchElementException autoPayLandingPageNotFound) {
			Verify.fail("AutoPay Cancel modal is NOT displayed");
		}
		return this;
	}

	/**
	 * verify cancel auto pay modal is displayed
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifycancelAutoPayModal() {
		try {
			verifycancelAutoPayModalHeader();
			verifyYesButtonOnAPExitModal();
			verifyNoButtonOnAPExitModal();
			Reporter.log("Autopay Cancel modal validation succesfully finished");
		} catch (Exception e) {
			Verify.fail("Fail to Verify Cancel Auto Pay Modal");
		}
		return this;
	}

	/**
	 * 
	 * @param cardorAccountNumber
	 * @param paymentType
	 * @return
	 */
	public AutoPayPage verifyNewPaymentDetailsUpdated(String cardorAccountNumber, String paymentType) {
		try {
			if ("Card".equals(paymentType)) {
				inlinePaymentMethod.isDisplayed();
				inlinePaymentMethod.getText().contains(cardorAccountNumber.substring(12));
				Reporter.log("New Card details are updated");
			} else {
				inlinePaymentMethod.isDisplayed();
				inlinePaymentMethod.getText().contains(cardorAccountNumber.substring(cardorAccountNumber.length() - 4));
				Reporter.log("New Bank details are updated");
			}
		} catch (Exception e) {
			Assert.fail("New payment details update field not found");
		}
		return this;
	}

	/***
	 * verify payment method blade
	 * 
	 * @return
	 */

	public AutoPayPage verifyPaymentMethodBlade() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paymentMethodBlade));
			paymentMethodBlade.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Fail to verify the Payment Method Blade");
		}
		return this;
	}

	/***
	 * verify Terms and conditions
	 * 
	 * @return
	 */

	public AutoPayPage verifyTnC() {
		try {
			autoPayFAQsLink.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Fail to Verify Terms AND Conditions");
		}
		return this;
	}

	public AutoPayPage verifyYesButtonOnAPExitModal() {
		try {
			autopayExitModelYesButton.isDisplayed();
		} catch (Exception e) {
			Verify.fail("Fail to verify yes Button on Exit Model");
		}
		return this;
	}

	public AutoPayPage verifyNoButtonOnAPExitModal() {
		try {
			autopayExitModelNoButton.isDisplayed();
		} catch (Exception e) {
			Verify.fail("Fail to vrify the AUto pay Exit Modal No button");
		}
		return this;
	}

	public AutoPayPage clickNoExitApModal() {
		try {
			autopayExitModelNoButton.click();
			Reporter.log("Clicked on No Exit AP Modal");
		} catch (Exception e) {
			Assert.fail("Failed to Click on No Exit AP Modal");
		}
		return this;
	}

	/*
	 * public void highLighterMethod(WebDriver driver, WebElement element){
	 * JavascriptExecutor js = (JavascriptExecutor) driver; js.executeScript(
	 * "arguments[0].setAttribute('style', 'background: yellow; border: 2px red;');"
	 * , element); }
	 */

	public AutoPayPage verifyNewAutoPayLandingPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(autoPayLandingPageHeaderNew));
			autoPayLandingPageHeaderNew.isDisplayed();
			Reporter.log("AutoPay Settings landing page displayed.");
		} catch (NoSuchElementException autoPayLandingPageNotFound) {
			Assert.fail("AutoPay Settings landing page NOT displayed");
		}
		return this;
	}

	/**
	 * verify is saved payment method is displayed on blade
	 * 
	 * @return boolean
	 */
	public boolean verifyIfPaymentMethodIsSaved() {
		try {
			return storedPaymentType.isDisplayed() && paymentMethodBlade.getText().contains("Using my");
		} catch (Exception e) {
			Assert.fail("Payment method saved indication field not found");
		}
		return false;
	}

	/**
	 * verify AP cancel modal header
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifyAutoPayCancelModalHeader() {
		try {
			autoPayCancelModalHeader.isDisplayed();
			autoPayCancelModalHeader.getText().equalsIgnoreCase("Are you sure you want to cancel AutoPay?");
		} catch (Exception e) {
			Verify.fail("Autopay Cancel model not found");
		}
		return this;
	}

	public void clickAgreeAndSubmitBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(agreeAndSubmitbtn));
			if (agreeAndSubmitbtn.isDisplayed()) {
				agreeAndSubmitbtn.click();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
				Reporter.log("Clicked on 'Agree and Submit' button on AutoPay Settings page.");
				while (getDriver().getCurrentUrl().endsWith("autopay") && i <= 3) {
					agreeAndSubmitbtn.click();
					waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
					i++;
				}
			}
		} catch (NoSuchElementException agreeAndSubmitBtnNotFound) {
			Assert.fail("AutoPay Settings landing page - 'Agree and Submit' button NOT displayed ");
		}

	}

	/**
	 * Fill all the bank details and click on continue
	 *
	 * @param payment
	 */
	public AutoPayPage fillBankInfo(Payment payment) {
		try {
			accountName.sendKeys(payment.getCheckingName());
			routingNum.sendKeys(payment.getRoutingNumber());
			bankAccountNumber.sendKeys(payment.getAccountNumber());
			//accNumber = Long.parseLong(payment.getAccountNumber());
			Reporter.log("Bank information filled");
		} catch (Exception e) {
			Assert.fail("Fail to fill Bank info details");
		}
		return this;
	}

	/***
	 * Click FAQ button
	 * 
	 * @return
	 */

	public AutoPayPage clickFAQBackButton() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(faqBackBtn));
			clickElementWithJavaScript(faqBackBtn);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			Reporter.log("Clicked on FQA Back Button");
		} catch (Exception e) {
			Assert.fail("Fail to Click FAQ Back Button");
		}
		return this;
	}

	/**
	 * addbank
	 * 
	 * @param myTmoData
	 * @return
	 */
	public String addBankToPaymentMethod(MyTmoData myTmoData) {
		String accNum = "";
		try {
			clickAddPaymentMethod();
			verifyAutoPaySpokePage();
			clickAddBankIcon();
			verifyBankInfoHeader();
			fillBankInfo(myTmoData.getPayment());
			accNum = clickContinueAddBank();
		//	clickAgreeAndSubmitBtn();
			verifyAutoPayLandingPageFields();
			Reporter.log("Added bank to payment method");
		} catch (Exception e) {
			Assert.fail("Fail to Add Bank to Payment Method");
		}
		return accNum;
	}

	public String clickContinueAddBank() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("i.fa-spinner")));
			waitFor(ExpectedConditions.elementToBeClickable(continueAddBank));
			continueAddBank.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("i.fa-spinner")));
			checkPageIsReady();

			if (errorMsg.isDisplayed()) {
				accNumber = generateRandomAccNumber() + accNumber;
				bankAccountNumber.sendKeys(accNumber.toString());
				clickContinueAddBank();
			}
			Reporter.log("Clicked on Continue Add Bank");
		} catch (Exception e) {
			Reporter.log("Bank Account number is not accepted");
		}
		return accNumber.toString();
	}

	/**
	 * generate a unique amount value
	 * 
	 * @return double - amount
	 */
	private int generateRandomAccNumber() {
		Integer min = 1000;
		Integer max = 9999;
		Random r = new Random();
		int randomNumber = r.nextInt() * (max - min) + min;
		return randomNumber;
	}
	
	public AutoPayPage clickReturnHome(){
		try{
			if(returnHome.isDisplayed()){
				returnHome.click();
				checkPageIsReady();
				Reporter.log("clicked on return home");
			}
		}catch (Exception e){
			Reporter.log(" return home button not found");
		}
	
	return this ;
	}

	/**
	 * This method is to verify PII masking is done for all payment information
	 * @param custMsisdnPID
	 */
	public void verifyPIIMasking(String custMsisdnPID) {
		try {
			String pidAttr = storedPaymentNo.getAttribute("pid");
			if (!pidAttr.isEmpty() && pidAttr.contains(custMsisdnPID)
					&& pidAttr.matches(custMsisdnPID+"\\d+")) {
				Reporter.log("Verified PII masking for Payment method blade");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Payment method blade");
		}
	}
	
	
	/**
	 * verify returnHome button
	 * 
	 * @return boolean
	 *//*
	public AutoPayPage verifyReturnHomeButton() {
		try {
			returnHome.isDisplayed();
		} catch (Exception e) {
			Verify.fail("returnHome not found");
		}
		return this;
	}
	
	*/
	/**
	 * verify Autopay setup not eligible for PA scheduled user
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifyAutopaynotEligibleModalforPAScheduledUser() {
		try {
			Assert.assertTrue(autopaynotEligibleModal.get(0).getText().contains("Oops—we can’t set up AutoPay right now."),"Autopay not eligible modal not displayed");
			Assert.assertTrue(autopaynotEligibleModal.get(1).getText().contains("You have an active Payment Arrangement. Please come back to set up AutoPay when your Payment Arrangement is complete."),"Autopay not eligible modal not displayed");
			
		} catch (Exception e) {
			Verify.fail("returnHome not found");
		}
		return this;
	}
	
	/**
	 * verify electronic Agrements Text not displayed
	 * 
	 * @return boolean
	 */
	public AutoPayPage verifyElectronicAgreementTextRemoved() {
		
		try {
			for(WebElement element : electronicAgrementsText) {
				element.isDisplayed();
				Assert.assertFalse(element.getText().contains("history, and electronic agreements related to your account."), "electronic agreements  is  dispalyed");		
			}
		
		} catch (Exception e) {
			Verify.fail("electronic Agrements Text displayed-failed");
		}
		return this;
	}
	
	/**
	 * Verify Autopay is scheduled
	 * 
	 * @return
	 */
	public boolean verifyAutopayisscheduled() {
		checkPageIsReady();
		boolean display = false;
		try {
			Assert.assertTrue(autopayScheduledStatus.getText().contains("ON"));
			display = true;
			Reporter.log("AutopayLink is scheduled");
		} catch (AssertionError e) {
			Reporter.log("AutopayLink is not scheduled");
		}
		return display;
	}
	
	public AutoPayPage clickGoToHome(){
		try{
			if(goTOHome.isDisplayed()){
				goTOHome.click();
				checkPageIsReady();
				Reporter.log("clicked on goto  home");
			}
		}catch (Exception e){
			Reporter.log(" goto home button not found");
		}
	
	return this ;
	}
	
	
	
	public AutoPayPage verifySpriteIconDisplayed() {
		try {

			checkPageIsReady();
			Assert.assertTrue(spriteIcon.isDisplayed());
			Assert.assertTrue(spriteIcon.getAttribute("class").contains("masterCard"), "sprite icon not dispalyed");
			Reporter.log("sprite icon is dispalyed");

		} catch (Exception e) {
			Reporter.log("sprite icon not dispalyed");
		}

		return this;
	}
	
	public AutoPayPage verifyStoredCardReplacedWithNewCard(String cardNumber) {

		try {
			String storedPaymentNumber = storedPaymentCardNumber.getText().replace("*", "");
			Assert.assertTrue(cardNumber.contains(storedPaymentNumber),
					"stored Card number is not replaced with new card number");
			Reporter.log("stored Card number is replaced with new card number");
		} catch (NoSuchElementException elementNotFound) {
			Assert.fail("stored Card number is not replaced with new card number");
		}

		return this;
	}
	
	public AutoPayPage verifyAutopayDisAllowedforNonMasterUser(MyTmoData myTmoData) {
		try {

			checkPageIsReady();
			autopayDisallowedModal.isDisplayed();
			Assert.assertTrue(verifyElementBytext(autopayDisallowedModalText, "Oops!"), "autopay Disallowed Modal text not dispalyed");
			Assert.assertTrue(verifyElementBytext(autopayDisallowedModalText, "Please contact the Primary Account Holder to set up AutoPay"), "autopay Disallowed Modal text not dispalyed");
			Reporter.log("autopay Disallowed Modal  is dispalyed for "+myTmoData.getPayment().getUserType());
			System.out.println("autopay Disallowed Modal  is dispalyed for "+myTmoData.getPayment().getUserType());
		} catch (Exception e) {
			Reporter.log("autopay Disallowed Modal  not dispalyed for"+myTmoData.getPayment().getUserType());
		}

		return this;
	}
	
	
	public AutoPayPage clickGotoHomeButton() {
		try {
			checkPageIsReady();
			goTOHome.click();
		}catch(Exception e) {
			Assert.fail("Goto home page button not found");
		}
		return this;
	}

	/**
	 * add payment card if there are no stored payment methods
	 * 
	 * @param myTmoData
	 * @return
	 * @return boolean
	 */
	public String verifyStoredPaymentMethodOrAddCard(MyTmoData myTmoData) {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
		if (!paymentMethodBlade.getText().equals("Using my") || (paymentMethodBlade.getText().equals("Using my") )) {
			addCardToPaymentMethod(myTmoData);
			return "Bank";
		} 
		return "Card";
	}

	/**
	 * verify that the In Use payment method is displayed on top of saved payment method lists
	 */
	public void verifyInUsePaymentMethod() {
		try {
			for (int i = 0; i < savedPaymentMethodBlades.size(); i++) {
				if (i==0 && savedPaymentMethodBlades.get(i).isDisplayed() && savedPaymentMethodBlades.get(i).getText().contains("In use")) {
					Reporter.log("Verified that the In Use payment method is displayed on top of saved payment method lists");
				}
				else if (i==1 && savedPaymentMethodBlades.get(i).isDisplayed() && savedPaymentMethodBlades.get(i).getText().contains("In use")) {
					Reporter.log("Verified that the In Use payment method is displayed on top of saved payment method lists");
				}
				else {
					Reporter.log("In Use payment method is not displayed on top of saved payment method lists");
				}
			}
		} catch (Exception e) {
			Reporter.log("Failed to verify In use payment method");
		}
	}
	
	
}
