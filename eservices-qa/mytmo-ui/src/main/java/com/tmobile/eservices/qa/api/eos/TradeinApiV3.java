package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class TradeinApiV3 extends ApiCommonLib {

	/***
	 * Summary: This API is used do the trade in  through 
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	
	public Response getTradeInApi(ApiTestData apiTestData,Map<String,String> tokenMap,String jwtToken) throws Exception {
		Map<String, String> headers = buildTradeInApiHeaderTradeIn(apiTestData,jwtToken);
		String resourceURL = "v3/tradein/Questions?carrierName="+apiTestData.getCarrierName()+"&makeName="+apiTestData.getMakeName()+"&modelName="+apiTestData.getModelName();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
	}
	
	private Map<String, String> buildTradeInApiHeaderTradeIn(ApiTestData apiTestData,String jwtToken) throws Exception {
		//clearCounters(apiTestData);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("channelId","WEB");
		headers.put("Authorization",jwtToken);
		headers.put("X-Auth-Originator", jwtToken);
		headers.put("cache-control", "no-cache");
		headers.put("applicationId", "MYTMO");
		headers.put("correlationId","qlab02-19042019_62");
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("transactionBusinessKey", "ban");
		headers.put("transactionBusinessKeyType",apiTestData.getBan());
		headers.put("transactionId","get-tradeinquotes-devices-200");
		headers.put("transactionType", "Tradein");
		headers.put("usn", "12345");
		
      return headers;
	}	
	
}
