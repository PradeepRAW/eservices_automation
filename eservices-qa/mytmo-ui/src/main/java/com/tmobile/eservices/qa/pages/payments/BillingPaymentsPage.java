package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Bhavya
 *
 */
public class BillingPaymentsPage extends CommonPage {
	private static final String pageUrl = " profile/billing_payment";

	@FindBy(xpath = "//*[contains(text(),'Billing & Payments')]")
	private WebElement BillingandPaymnets;

	@FindBy(xpath = "//p[text()='Payment Methods']")
	private WebElement PaymnetMethod;

	@FindBy(xpath = "//p[text()='Payment Methods']//following-sibling::p")
	private WebElement paymentbody;

	@FindBy(xpath = "//p[text()='Payment Methods']//parent::div//following-sibling::div/span")
	private WebElement chevron;

	@FindBy(css = "p[class^='Display6']>span")
	private WebElement savedPaymentMethod;

	/**
	 * 
	 * @param webDriver
	 */
	public BillingPaymentsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public BillingPaymentsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Wait For Spinner
	 * 
	 * @return
	 */
	public boolean waitForNewSpinner() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector("div.circle-clipper.right div.circle"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */

	public BillingPaymentsPage verifyPageLoaded() {

		try {
			waitForNewSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("BillingPaymnets  page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Billing page is  not displayed");
		}
		return this;
	}

	public BillingPaymentsPage checkverifypaymentmethoddisplayed() {
		try {
			if (PaymnetMethod.isDisplayed())
				Reporter.log("Verify payment method is displayed");
			else
				Assert.fail("Verify payment method is not displayed");
		} catch (Exception e) {
			Assert.fail("Verifypaymentmethod element is not existed");
		}
		return this;
	}

	public BillingPaymentsPage checkverifypaymentbodydisplayed() {
		try {
			if (paymentbody.isDisplayed())
				Reporter.log("Verify payment method body  is displayed");
			else
				Assert.fail("Verify payment method body is not displayed");
		} catch (Exception e) {
			Assert.fail("Verifypaymentmethodbody element is not existed");
		}
		return this;
	}

	public BillingPaymentsPage checkverifypaymentchevrondisplayed() {
		try {
			if (chevron.isDisplayed())
				Reporter.log("Verify payment method chevron  is displayed");
			else
				Assert.fail("Verify payment method chevron is not displayed");
		} catch (Exception e) {
			Assert.fail("Verifypaymentmethodbody chevron is not existed");
		}
		return this;
	}

	/**
	 * Click Payment method.
	 */
	public BillingPaymentsPage clickPaymentmethod() {
		try {

			PaymnetMethod.click();
			Reporter.log("clicked on Paymnet method");
		} catch (Exception e) {
			Assert.fail("Click on Payment method failed");
		}
		return this;
	}

	/**
	 * Verify Saved Payment Method Link.
	 */
	public BillingPaymentsPage verifySavedPaymentMethodLink() {
		try {
			waitforSpinnerinProfilePage();
			savedPaymentMethod.isDisplayed();
			Reporter.log("Saved Payment Method Link is displayed");
		} catch (Exception e) {
			Assert.fail("Saved Payment Method element not found");
		}
		return this;
	}

	/**
	 * Click Saved Payment Method Link.
	 */
	public BillingPaymentsPage clickSavedPaymentMethodLink() {
		try {
			waitforSpinnerinProfilePage();
			savedPaymentMethod.click();
			Reporter.log("Saved Payment Method Link is clicked");
		} catch (Exception e) {
			Assert.fail("Saved Payment Method Link not displayed");
		}
		return this;
	}

	/**
	 * Verify Saved Payment Method Count.
	 */
	public BillingPaymentsPage verifySavedPaymentMethodCount(int count) {
		try {
			if (count > 0 || count >= 9) {
				savedPaymentMethod.getText().contains("(0" + Integer.toString(count) + " saved)");
				Reporter.log("Saved Payment Method count is displayed as " + count);
			} else if (count == 10) {
				savedPaymentMethod.getText().contains("(" + Integer.toString(count) + " saved)");
				Reporter.log("Saved Payment Method count is displayed as " + count);
			} else {
				verifySavedPaymentMethodLink();
			}

		} catch (Exception e) {
			Assert.fail("Saved Payment Method Link not displayed");
		}
		return this;
	}

	/**
	 * Verify No Saved Payment Method Link.
	 */
	public BillingPaymentsPage verifyNoSavedPaymentMethodLink() {
		try {
			waitforSpinnerinProfilePage();
			if (savedPaymentMethod.isDisplayed()) {
				Assert.fail("Saved Payment Method element not found");
			} else {
				Reporter.log("Saved Payment Method Link is not displayed");
			}

		} catch (Exception e) {
			Reporter.log("Saved Payment Method Link element not Founf");
		}
		return this;
	}
}