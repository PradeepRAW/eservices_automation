package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class AALEligibilityCheckApiV1 extends ApiCommonLib{
	
	/***
	 * Summary: Check the AAL Eligibilty for the given BAN/MSISDN
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-phoneNumber,required=true
	 *  -Content-Type
	 *  -usn
	 *  -phoneNumber 
	 *  
	 * @return
	 * @throws Exception
	 */
    public Response eligibilityCheckAAL(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception{
    	
    	Map<String, String> headers = buildEligibilityCheckAALHeader(apiTestData,tokenMap);
    	String resourceURL = "aal/v1/eligibility";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
         System.out.println(response.asString());
         return response;
    }
    
   public Response getEligibleLines(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception{
    	
    	Map<String, String> headers = buildEligibilityCheckAALHeader(apiTestData,tokenMap);
    	String resourceURL = "aal/v1/eligible-lines";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.GET);
         System.out.println(response.asString());
         return response;
    }
   
   public Response getEligibleLinesForWearables(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception{
	   Map<String, String> headers = buildEligibilityCheckAALHeader(apiTestData,tokenMap);
	   RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		String s="aal/v1/eligible-lines?product-type="+apiTestData.getProductTypeForAAL()+"&product-subtype="+apiTestData.getProductSubTypeForAAL();
		Response response = service.callService(s, RestCallType.GET);
        return response;
   }
  
    private Map<String, String> buildEligibilityCheckAALHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		String transactionType = "UPGRADE";
		Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		Map<String, String> headers = new HashMap<String, String>();
		if (StringUtils.isNoneEmpty(tokenMap.get("addaline"))) {
			transactionType = "ADDALINE";
		}
		
		headers.put("Authorization",jwtTokens.get("accessToken"));
		headers.put("transactionType",transactionType);
		headers.put("applicationId","MYTMO");
		headers.put("cache-control", "no-cache");
		headers.put("channelId","WEB");
		headers.put("correlationId",checkAndGetPlattokenJWT(apiTestData, jwtTokens.get("jwtToken")));
		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
		headers.put("transactionBusinessKey","BAN");
		headers.put("transactionBusinessKeyType",apiTestData.getBan());
		headers.put("phoneNumber",apiTestData.getMsisdn());
		headers.put("Content-Type","application/json");
		headers.put("accessToken", jwtTokens.get("accessToken"));
		headers.put("usn","testUSN");
		headers.put("accept","application/json");
		headers.put("transactionid","IID");
		return headers;
		

	}
    
    
    public Response getEligiblePlansForWearables(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception{
 	   Map<String, String> headers = buildEligibilityCheckAALHeaderPlan(apiTestData,tokenMap);
 	   RestService service = new RestService("", eos_base_url);
 		RequestSpecBuilder reqSpec = service.getRequestbuilder();
 		reqSpec.addHeaders(headers);
 		service.setRequestSpecBuilder(reqSpec);
 		String s = "v3/service/"+tokenMap.get("cartId")+"/plans?msisdn="+apiTestData.getMsisdn()+"&planType=WEARABLE";
 		Response response = service.callService(s, RestCallType.GET);
         return response;
    }
   
     private Map<String, String> buildEligibilityCheckAALHeaderPlan(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

 		String transactionType = "UPGRADE";
 		Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
 		Map<String, String> headers = new HashMap<String, String>();
 		if (StringUtils.isNoneEmpty(tokenMap.get("addaline"))) {
 			transactionType = "ADDALINE";
 		}
 		
 		headers.put("Authorization",jwtTokens.get("jwtToken"));
 		headers.put("transactionType",transactionType);
 		headers.put("applicationId","MYTMO");
 		headers.put("cache-control", "no-cache");
 		headers.put("channelId","WEB");
 		headers.put("correlationId",checkAndGetPlattokenJWT(apiTestData, jwtTokens.get("jwtToken")));
 		headers.put("X-Auth-Originator",jwtTokens.get("jwtToken"));
 		headers.put("transactionBusinessKey","BAN");
 		headers.put("transactionBusinessKeyType",apiTestData.getBan());
 		headers.put("phoneNumber",apiTestData.getMsisdn());
 		headers.put("Content-Type","application/json");
 		headers.put("usn","testUSN");
 		headers.put("accept","application/json");
 		headers.put("transactionid","IID");
 		return headers;
 		

 	}


}
