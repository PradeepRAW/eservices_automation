/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class SignalBoosterLinkPage extends CommonPage {

	public SignalBoosterLinkPage(WebDriver webDriver) {
		super(webDriver);
	}

	private static final Logger logger = LoggerFactory.getLogger(SignalBoosterLinkPage.class);

	@FindBy(css = "h1.doc-title.page-title")
	private WebElement pageTitle;

	/**
	 * Verify SignalBoosterLink Page.
	 *
	 * @return the SignalBoosterLinkPage class instance.
	 */
	public SignalBoosterLinkPage verifySignalBoosterLinkPage() {
		try {
			// waitforSpinner();
			checkPageIsReady();
			pageTitle.isDisplayed();
			Reporter.log("SignalBoosterLink page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("SignalBoosterLink page not displayed");
		}
		return this;

	}
}
