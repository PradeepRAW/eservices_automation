package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 * 
 */
public class PrepaidLineSettingsPage extends CommonPage {

	private static final String familyControlPage = "prepaid/multiple_devices";

	@FindBy(xpath = "//*[contains(text(),'Billing & Payments')]")
	private WebElement BillingandPayments;

	public PrepaidLineSettingsPage(WebDriver webDriver) {
		super(webDriver);
	}

	// Verify Billing & Payments page
	public PrepaidLineSettingsPage verifyLineSettingsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(familyControlPage);
			Reporter.log("Multiple Devices page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Multiple Devices page not displayed");
		}
		return this;
	}

}
