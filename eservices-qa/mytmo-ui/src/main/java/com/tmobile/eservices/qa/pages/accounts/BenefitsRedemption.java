package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class BenefitsRedemption extends CommonPage {

	private final String netflixPageUrl = "/benefit-redemption/Netflix";

	@FindAll({ @FindBy(css = "div.col-9.no-padding.pull-left a  div span") })
	protected List<WebElement> cardAuthorable;

	@FindAll({ @FindBy(css = "p.padding-bottom-xsmall.legal.padding-top-xsmall") })
	protected List<WebElement> cardStreemingText;

	// List<WebElement> cardAuthorable =
	// getDriver().findElements(By.cssSelector("div.col-9.no-padding.pull-left a
	// div span"));

	@FindBy(css = "p.Display3")
	public WebElement oopsText;

	@FindBy(css = "div.col-12.col-md-6.padding-bottom-small.padding-bottom-small-xs.padding-bottom-small a")
	private WebElement btn2;

	@FindBy(css = "div.padding-top-large-xs")
	private WebElement icon;

	@FindAll({ @FindBy(css = "p.pl-2.body"), @FindBy(css = " p.Display3.text-center span"),
			@FindBy(css = "div.col-md-10.offset-md-1.col-sm-10.offset-sm-1.no-padding"), })
	public WebElement messages;

	@FindBy(css = "p.Display3.text-center span")
	private WebElement message1;

	@FindAll({ @FindBy(css = "p.mb-0.H5-heading"), @FindBy(css = " p.H6-heading.text-center span") })
	public WebElement message2;

	@FindBy(css = "p.pl-2.body")
	public WebElement message3;

	@FindAll({ @FindBy(css = "*.Display3.text-center span"), @FindBy(css = "div.row.padding-top-large div  p span") })
	public WebElement message4;

	@FindAll({ @FindBy(xpath = "//p[@class='mb-0 padding-vertical-medium']/span"),
			@FindBy(css = "p.body.text-center.padding-top-medium a") })
	public WebElement message5;

	@FindBy(xpath = "//p[@class='mb-0 padding-vertical-medium']/a")
	public WebElement message6;

	@FindBy(css = "*.H6-heading.text-center span")
	private WebElement message7;

	@FindAll({ @FindBy(css = "*.padding-top-medium p:nth-child(1)"),
			@FindBy(css = "div.row.no-margin.d-block.text-sm-center div span") })
	public WebElement message8;

	@FindBy(css = "p.Display3.padding-bottom-small")
	private WebElement txterrorMsg1;

	@FindBy(css = "p.body.padding-top-xsmall")
	private WebElement txterrorMsg2;

	@FindBy(linkText = "Recover your account")
	private WebElement linkRecoverYourAccount;

	@FindBy(css = "div.legal.padding-bottom-xl span")
	private WebElement message10;

	@FindBy(css = "div.legal.padding-bottom-xl a")
	private WebElement message11;

	@FindBy(css = "p.body.text-center.black.padding-top-small-xs span")
	private WebElement message12;

	@FindBy(css = "div.col-md-10.offset-md-1.col-sm-10.offset-sm-1.no-padding")
	private WebElement message13;

	@FindBy(css = "div.container.no-padding-xs.hidden-xs div p span")
	public WebElement pitchPageM1;

	@FindBy(css = "div.container-fluid.no-padding.banner-position div div div span")
	public WebElement pitchPageM2;

	@FindAll({ @FindBy(css = "p.Display4.gray85.padding-top-medium-sm.padding-top-medium-xs span") })
	public List<WebElement> pitchPageAuthHeaders;

	@FindAll({ @FindBy(css = "p.body.font-sf-pro.gray85.padding-top-small span") })
	public List<WebElement> pitchPageAuthMsgs;

	@FindAll({ @FindBy(css = "p.padding-top-xl a") })
	public List<WebElement> pitchPageAuthlinks;

	@FindBy(xpath = "//p[@class='pl-2 body gray60 text-center']//a")
	private WebElement btnCustomerCare;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement cancelBtn;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement explorePlansBtn;

	@FindAll({ @FindBy(css = "button.SecondaryCTA"),
			@FindBy(css = "div.row.padding-top-xxl-xs.padding-top-medium-sm.padding-vertical-xl.padding-bottom-xxl-xs.padding-bottom-large-sm div div button") })
	private WebElement cancelPlansBtn;

	@FindBy(css = "button.SecondaryCTA.blackCTA")
	private WebElement backToPlanDetailsBtn;

	@FindBy(css = "a.PrimaryCT")
	private WebElement btn3;

	@FindBy(css = "img.Appstore-icon")
	private WebElement btnAppStore;

	@FindBy(css = "img.playstore-icon")
	private WebElement btnplayStore;

	public BenefitsRedemption(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public BenefitsRedemption verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions
					.javaScriptThrowsNoExceptions("return (document.readyState == 'complete' && jQuery.active == 0)"));
		} catch (Exception e) {
			Assert.fail("Benefits page is not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public BenefitsRedemption verifyPageUrl() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(netflixPageUrl));
		} catch (Exception e) {
			Assert.fail("NETFLIX benefits redemption  page is not displayed");
		}
		return this;
	}

	// click N verify new tab
	public BenefitsRedemption clickBtnNVerifyNewTab(String btn, String ExpRes) {
		checkPageIsReady();
		try {
			Assert.assertTrue(btn2.getText().contains(btn),
					"Expected result:" + btn + " should appear. Actual result: " + btn2.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			String Pwndow = getDriver().getWindowHandle();
			Set<String> allwindows = getDriver().getWindowHandles();
			java.util.Iterator<String> it = allwindows.iterator();

			while (it.hasNext()) {
				String Childwnd = it.next();
				if (Pwndow.equals(Childwnd)) {
					Pwndow = Childwnd;
				} else {
					getDriver().switchTo().window(Childwnd);
					String sActRes = getDriver().getCurrentUrl();
					if (sActRes.contains(ExpRes)) {
						Reporter.log("Navigated to " + ExpRes);
						getDriver().close();
						getDriver().switchTo().defaultContent();
					} else {
						Assert.fail(ExpRes + "not opened in a new tab");
					}
				}
			}
		}

		catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	/**
	 * Verify the authorable text.
	 */

	public BenefitsRedemption verifyAuthorabletext(String str, WebElement element) {
		checkPageIsReady();
		try {
			Assert.assertTrue(element.getText().equalsIgnoreCase(str), "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + element.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + element.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + element.getText());
		}
		return this;
	}

	/**
	 * Verify the authorable text.
	 */

	public BenefitsRedemption verifyAllAuthorabletext(String str, List<WebElement> element) {
		checkPageIsReady();
		for (int i = 0; i < element.size(); i++) {
			try {
				Assert.assertTrue(element.get(i).getText().equalsIgnoreCase(str),
						"Expected result: Authorable text should appear " + str
								+ ".Actual result: Authorable text appearing is " + element.get(i).getText());
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + element.get(i).getText());
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + element.get(i).getText());
			}

		}
		return this;

	}

	/**
	 * Verify the authorable text.
	 */

	public BenefitsRedemption verifyNclickPitchPageAuthorableLinks(String str) {
		checkPageIsReady();
		for (int i = 0; i < pitchPageAuthlinks.size(); i++) {
			try {
				if (pitchPageAuthlinks.get(i).getText().contains(str)) {
					Assert.assertTrue(pitchPageAuthlinks.get(i).getText().equalsIgnoreCase(str),
							"Expected result: Authorable text should appear " + str);
					pitchPageAuthlinks.get(i).click();
				}
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + pitchPageAuthlinks.get(i).getText());
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + pitchPageAuthlinks.get(i).getText());
			}

		}
		return this;

	}

	/**
	 * Verify the authorable text 1.
	 */

	public BenefitsRedemption verifyAuthorabletext1(String str) {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(oopsText));
			Assert.assertTrue(oopsText.getText().contains(str), "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + icon.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + icon.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + message1.getText());
		}
		return this;
	}

	/**
	 * Verify the authorable text 2.
	 */

	public BenefitsRedemption verifyAuthorabletext2(String str) {
		try {
			Assert.assertTrue(message2.getText().equalsIgnoreCase(str),
					"Expected result: Authorable text should appear " + str
							+ ".Actual result: Authorable text appearing is " + message2.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message2.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + message2.getText());
		}
		return this;
	}

	/**
	 * Verify the authorable text 3.
	 */

	public BenefitsRedemption verifyAuthorabletext3(String str) {
		try {

			Assert.assertTrue(message3.getText().contains(str), "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message3.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message3.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + message3.getText());
		}
		return this;
	}

	/**
	 * Verify the authorable text 4.
	 */

	public BenefitsRedemption verifyAuthorabletext4(String str) {
		waitforSpinner();
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(message4));
			Assert.assertTrue(message4.getText().contains(str), str + " is displayed");
			Reporter.log(str + " is displayed");
		} catch (Exception e) {
			Assert.fail(str + " is not displayed");
		}
		return this;
	}

	/**
	 * Verify the authorable text 5.
	 */

	public BenefitsRedemption verifyAuthorabletext5(String str) {
		if (message5.isDisplayed()) {

			try {

				Assert.assertTrue(message5.getText().contains(str), "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message5.getText());
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + message5.getText());
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message5.getText());
			}
		} else if (message10.isDisplayed()) {

			try {

				Assert.assertTrue(message10.getText().contains(str), "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message10.getText());
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + message10.getText());
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message10.getText());
			}
		} else {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + message10.getText());
		}

		return this;
	}

	/**
	 * Verify the authorable text 6.
	 */

	public BenefitsRedemption verifyAuthorabletext6(String str) {

		if (message6.isDisplayed()) {
			try {

				Assert.assertTrue(message6.getText().contains(str), "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message6.getText());
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + message6.getText());
				message6.click();
				Reporter.log("Current url is" + getDriver().getCurrentUrl());
				getDriver().navigate().back();
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message6.getText());
			}
		} else if (message11.isDisplayed()) {
			try {

				Assert.assertTrue(message11.getText().contains(str), "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message11.getText());
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + message11.getText());
				message11.click();
				Reporter.log(" clicked on" + message11.getText());
				ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
				getDriver().switchTo().window(tabs2.get(1));
				// verifyLegalCopy();
				getDriver().close();
				getDriver().switchTo().window(tabs2.get(0));
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + message11.getText());
			}
		} else {
			Assert.fail("Unable to verify the authorable text ");
		}

		return this;
	}

	/**
	 * Verify the appstore icon.
	 */

	public BenefitsRedemption verifybtnAppStore() {
		try {
			waitFor(ExpectedConditions.visibilityOf(btnAppStore));
			btnAppStore.click();
			Reporter.log("clicked on appstore icon");
			checkPageIsReady();
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs2.get(1));
			verifyAppStoreURL();
			getDriver().close();
			getDriver().switchTo().window(tabs2.get(0));
		} catch (Exception e) {
			Assert.fail("Unable to click on appstore icon");
		}
		return this;
	}

	/**
	 * Verify the app store url.
	 */

	public BenefitsRedemption verifyAppStoreURL() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("/apps.apple.com"));
		} catch (Exception e) {
			Assert.fail("/apps.apple.com/us/app");
		}
		return this;
	}

	/**
	 * Verify the blocking url
	 */

	public BenefitsRedemption verifyBlockingPageURL() {
		try {
			waitFor(ExpectedConditions.urlContains("/profile/blocking"));
			Reporter.log("navigated to " + getDriver().getCurrentUrl());
			getDriver().navigate().back();
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("/profile/blocking url is not displayed");
		}
		return this;
	}

	/**
	 * Verify the nameid url
	 */

	public BenefitsRedemption verifyNameIdPageURL() {
		try {
			waitFor(ExpectedConditions.urlContains("/benefit-redemption/nameid"));
			Reporter.log("navigated to " + getDriver().getCurrentUrl());
			getDriver().navigate().back();
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("/benefit-redemption/nameid url is not displayed");
		}
		return this;
	}

	/**
	 * Verify the playstore icon.
	 */

	public BenefitsRedemption verifybtnPlayStore() {
		try {
			btnplayStore.click();
			Reporter.log("clicked on appstore icon");
			checkPageIsReady();
			ArrayList<String> tabs2 = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs2.get(1));
			verifyPlayStoreURL();
			getDriver().close();
			getDriver().switchTo().window(tabs2.get(0));
		} catch (Exception e) {
			Assert.fail("Unable to click on appstore icon");
		}
		return this;
	}

	/**
	 * Verify the app store url.
	 */

	public BenefitsRedemption verifyPlayStoreURL() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("play.google.com/store/apps"));
		} catch (Exception e) {
			Assert.fail("play.google.com/store/apps url is not displayed");
		}
		return this;
	}

	/**
	 * Verify the authorable text 7.
	 */

	public BenefitsRedemption verifyAuthorabletext7(String str) {
		try {
			Assert.assertTrue(message7.getText().contains(str), "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message7.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message7.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + message7.getText());
		}
		return this;
	}

	/**
	 * Verify the authorable text 7.
	 */

	public BenefitsRedemption verifyAuthorabletext12(String str) {
		try {
			Assert.assertTrue(message12.getText().equalsIgnoreCase(str),
					"Expected result: Authorable text should appear " + str
							+ ".Actual result: Authorable text appearing is " + message12.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message12.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + message12.getText());
		}
		return this;
	}

	/**
	 * Verify the authorable text 8.
	 */

	public BenefitsRedemption verifyAuthorabletext8(String str) {
		try {
			Assert.assertTrue(message8.getText().contains(str), "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message8.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + message8.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + message8.getText());
		}
		return this;
	}

	/**
	 * Verify the card authorable text .
	 */

	public BenefitsRedemption verifyCardAuthorabletext(String str) {
		checkPageIsReady();

		for (int i = 0; i < cardAuthorable.size(); i++) {
			try {

				if (cardAuthorable.get(i).getText().equalsIgnoreCase(str)) {
					Assert.assertTrue(cardAuthorable.get(i).getText().equalsIgnoreCase(str),
							"Expected result: Authorable text should appear " + str
									+ ".Actual result: Authorable text appearing is "
									+ cardAuthorable.get(i).getText());
					Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear "
							+ str + ".Actual result: Authorable text appearing is " + cardAuthorable.get(i).getText());

				}

			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + cardAuthorable.get(i).getText());
			}
		}
		return this;
	}

	/**
	 * Verify the card authorable text .
	 */

	public BenefitsRedemption verifyCardStreamingtext(String str) {
		checkPageIsReady();
		for (int i = 0; i < cardStreemingText.size(); i++) {
			try {

				if (cardStreemingText.get(i).getText().equalsIgnoreCase(str)) {
					Assert.assertTrue(cardStreemingText.get(i).getText().equalsIgnoreCase(str),
							"Expected result: Authorable text should appear " + str
									+ ".Actual result: Authorable text appearing is "
									+ cardStreemingText.get(i).getText());
					Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear "
							+ str + ".Actual result: Authorable text appearing is "
							+ cardStreemingText.get(i).getText());

				}

			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + cardStreemingText.get(i).getText());
			}
		}
		return this;
	}

	/**
	 * Verify the card authorable text .
	 */

	public BenefitsRedemption clickOnCardtextlink(String str) {
		checkPageIsReady();
		for (int i = 0; i < cardAuthorable.size(); i++) {
			try {

				if (cardAuthorable.get(i).getText().equalsIgnoreCase(str)) {
					cardAuthorable.get(i).click();
					Reporter.log("clicked on" + str + "link");
				}

			} catch (Exception e) {
				Assert.fail("Unable to click on" + str + "link");
			}
		}
		return this;
	}

	/**
	 * Verify the authorable text for error.
	 */

	public BenefitsRedemption verifyAuthorabletextForErrorPage(String str1, String str2) {
		try {
			Assert.assertTrue(txterrorMsg1.getText().contains(str1), "Expected result: Authorable text should appear "
					+ str1 + ".Actual result: Authorable text appearing is " + txterrorMsg1.getText());
			Reporter.log("Expected result: Authorable text should appear " + str1
					+ ".Actual result: Authorable text appearing is " + txterrorMsg1.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str1 + ".Actual result: Authorable text appearing is " + txterrorMsg1.getText());
		}

		try {
			Assert.assertTrue(txterrorMsg2.getText().contains(str2), "Expected result: Authorable text should appear "
					+ str2 + ".Actual result: Authorable text appearing is " + txterrorMsg2.getText());
			Reporter.log("Expected result: Authorable text should appear " + str2
					+ ".Actual result: Authorable text appearing is " + txterrorMsg2.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str2 + ".Actual result: Authorable text appearing is " + txterrorMsg2.getText());
		}
		return this;
	}

	/**
	 * Verify the authorable text 3 in greyColor.
	 */

	public BenefitsRedemption verifyAuthorabletext3InGreyColor(String str) {
		try {
			Assert.assertTrue(message3.getAttribute("class").contains("gray"),
					"The  Authorable text ' " + str + " ' should appear in grey color");
			Reporter.log(" Verified  The  Authorable text ' " + str + " ' should appear in grey color");
		} catch (Exception e) {
			Assert.fail("Unable to verify  the  Authorable text ' " + str + " ' in grey color");
		}
		return this;
	}

	/**
	 * Verify the customer care button in authorable text 3 .
	 */

	public BenefitsRedemption verifyCustomerCareBtnInAuthorabletext(String str) {
		try {
			Assert.assertTrue(btnCustomerCare.getText().contains(str), "Expected result:" + str
					+ "button is appearing. Actual result:" + btnCustomerCare.getText() + "is appearing.");
			Reporter.log(str + "button is appearing");
		} catch (Exception e) {
			Assert.fail(str + "button is not appearing");
		}

		try {
			Assert.assertTrue(btnCustomerCare.getAttribute("class").contains("magenta-link"),
					"Expected result:" + str + "should appear in magenta link. Actual result:"
							+ btnCustomerCare.getText() + "is not appearing in magenta link");
			Reporter.log(str + "should appear in magenta link.");
		} catch (Exception e) {
			Assert.fail(str + "is Not showing in magenta link.");
		}

		try {
			btnCustomerCare.click();
			Reporter.log("clicked on customer care button");
		} catch (Exception e) {
			Assert.fail("Unable to click on customer care button");
		}
		verifyContactUsPageDispalyed();
		return this;
	}

	/**
	 * Verify the recover your account link in authorable text 3 .
	 */

	public BenefitsRedemption verifyRecoverYourAccount(String str) {
		try {
			Assert.assertTrue(linkRecoverYourAccount.getText().contains(str),
					" " + str + "button is appearing. Actual result:");
			Reporter.log(str + "link is appearing");
		} catch (Exception e) {
			Assert.fail(str + "link is not appearing");
		}
		try {
			linkRecoverYourAccount.click();
			Reporter.log("Clicked on Recover your account link");
		} catch (Exception e) {
			Assert.fail("Unable to clicked on recover your account link");
		}
		verifyRegistrationURL();
		return this;
	}

	private BenefitsRedemption verifyContactUsPageDispalyed() {
		try {
			checkPageIsReady();
			getDriver().getCurrentUrl().contains("contact-us");
			Reporter.log("contact-us page is displayed");
		} catch (Exception e) {
			Assert.fail("contact-us page not displayed");
		}

		try {
			getDriver().navigate().back();
			Reporter.log("Navigating back");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Unable to navigate back from contact -us page");
		}
		return this;
	}

	public BenefitsRedemption verifyAndClickOnBtnCancel(String btn) {
		try {
			Assert.assertTrue(cancelBtn.getText().contains(btn), "Expected result:" + btn
					+ " should appear. Actual result: " + cancelBtn.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			cancelBtn.click();
			verifyPlanDetailsPayment();
			Reporter.log(btn + "is clicked");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	// Go to plan comparision page
	public BenefitsRedemption verifyAndClickOnBtn3(String btn) {
		try {
			Assert.assertTrue(explorePlansBtn.getText().contains(btn), "Expected result:" + btn
					+ " should appear. Actual result: " + explorePlansBtn.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			explorePlansBtn.click();
			// verifyPlansComparisionPage();
			Reporter.log(btn + "is clicked");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	// registration URL that's provided in EOS Partner Benefits for Hulu
	public BenefitsRedemption verifyAndClickOnBtn6(String btn) {
		try {
			Assert.assertTrue(explorePlansBtn.getText().contains(btn), "Expected result:" + btn
					+ " should appear. Actual result: " + explorePlansBtn.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			explorePlansBtn.click();
			verifyRegistrationURLForHulu();
			Reporter.log(btn + "is clicked");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	// manage addons url
	public BenefitsRedemption verifyAndClickOnBtn(String sbtn) {
		try {
			waitFor(ExpectedConditions.visibilityOf(backToPlanDetailsBtn));
			Assert.assertTrue(backToPlanDetailsBtn.getText().contains(sbtn), "Expected result:" + backToPlanDetailsBtn
					+ " should appear. Actual result: " + backToPlanDetailsBtn.getText() + "is appearing.");
			backToPlanDetailsBtn.click();
			Reporter.log(backToPlanDetailsBtn + "is clicked");
		} catch (Exception e) {
			Reporter.log(backToPlanDetailsBtn + "is NOT  displayed");
		}
		return this;
	}

	// manage addons url
	public BenefitsRedemption verifyAndClickOnBtn5(String btn) {
		try {
			Assert.assertTrue(explorePlansBtn.getText().contains(btn), "Expected result:" + btn
					+ " should appear. Actual result: " + explorePlansBtn.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			explorePlansBtn.click();
			verifyManageAddonsURL();
			Reporter.log(btn + "is clicked");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	// home Page
	public BenefitsRedemption verifyAndClickOnBtn7(String btn) {
		try {
			Assert.assertTrue(explorePlansBtn.getText().contains(btn), "Expected result:" + btn
					+ " should appear. Actual result: " + explorePlansBtn.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			explorePlansBtn.click();
			verifyHomePageUrl();
			Reporter.log(btn + "is clicked");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	public BenefitsRedemption verifyAndClickOnRestoreBtn(String btn) {
		try {
			checkPageIsReady();
			waitforSpinner();
			Assert.assertTrue(explorePlansBtn.getText().contains(btn), "Expected result:" + btn
					+ " should appear. Actual result: " + explorePlansBtn.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			explorePlansBtn.click();
			verifyLineDetailsPage();
			getDriver().navigate().back();
			Reporter.log(btn + "is clicked");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	public BenefitsRedemption verifyAndClickOnCancelBtn(String btn) {
		try {
			Assert.assertTrue(cancelPlansBtn.getText().contains(btn), "Expected result:" + btn
					+ " should appear. Actual result: " + cancelPlansBtn.getText() + "is appearing.");
			Reporter.log(btn + "is displayed");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  displayed");
		}
		try {
			cancelPlansBtn.click();
			verifyPlanDetailsPayment();
			getDriver().navigate().back();
			Reporter.log(btn + "is clicked");
		} catch (Exception e) {
			Reporter.log(btn + "is NOT  clicked");
		}
		return this;
	}

	public BenefitsRedemption verifyPlanDetailsPayment() {
		try {
			getDriver().getCurrentUrl().contains("account-overview");
			Reporter.log("account-overview page is displayed");
		} catch (Exception e) {
			Assert.fail("account-overview page is not displayed");
		}
		return this;
	}

	// registration URL that's provided in EOS Partner Benefits for Netflix
	public BenefitsRedemption verifyRegistrationURLForNetflix() {
		try {
			getDriver().getCurrentUrl().contains("www.netflix.com/partner/registerOrSignIn");
			Reporter.log("www.netflix.com/partner/registerOrSignIn is displayed");
		} catch (Exception e) {
			Assert.fail("www.netflix.com/partner/registerOrSignIn is not displayed");
		}

		try {
			switchToWindow();
			Reporter.log("Navigating back from Netflix registration url ");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Unable to navigate back from www.netflix.com/partner/registerOrSignIn page");
		}
		return this;
	}

	public BenefitsRedemption verifyRegistrationURL() {
		try {
			getDriver().getCurrentUrl().contains("www.netflix.com/partner/registerOrSignIn");
			Reporter.log("www.netflix.com/partner/registerOrSignIn is displayed");
		} catch (Exception e) {
			Assert.fail("www.netflix.com/partner/registerOrSignIn is not displayed");
		}
		try {
			getDriver().navigate().back();
			Reporter.log("Navigating back from Netflix registration url ");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Unable to navigate back from www.netflix.com/partner/registerOrSignIn page");
		}
		return this;
	}

	// registration URL that's provided in EOS Partner Benefits for Netflix
	public BenefitsRedemption verifyRegistrationURLForHulu() {
		try {
			getDriver().getCurrentUrl().contains("https://www.hulu.com/welcome?");
			Reporter.log("https://www.hulu.com/welcome? is displayed");
		} catch (Exception e) {
			Assert.fail("https://www.hulu.com/welcome? is not displayed");
		}

		try {
			getDriver().navigate().back();
			Reporter.log("Navigating back from Hulu registration url ");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Unable to navigate back from https://www.hulu.com/welcome? page");
		}
		return this;
	}

	// manage addons URL
	public BenefitsRedemption verifyManageAddonsURL() {
		try {
			getDriver().getCurrentUrl()
					.contains("/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP");
			Reporter.log("/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP is displayed");
		} catch (Exception e) {
			Assert.fail(
					"/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP is not displayed");
		}
		ManageAddOnsSelectionPage manageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsPage.verifymanageDataAndAddOnsPage();
		manageAddOnsPage.verifyIfListOfServiceContaingNetFlixOnly();

		try {
			getDriver().navigate().back();
			Reporter.log("Navigating back");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail(
					"Unable to navigate back from /odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP page");
		}

		return this;
	}

	// homePage
	public BenefitsRedemption verifyHomePageUrl() {
		try {
			getDriver().getCurrentUrl().contains("home");
			Reporter.log("home page is displayed");
		} catch (Exception e) {
			Assert.fail("home page is not displayed");
		}

		return this;
	}

	// manage addons URL
	public BenefitsRedemption verifyManageAddonsURL1() {
		try {
			getDriver().getCurrentUrl()
					.contains("/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP");
			Reporter.log("/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP is displayed");
		} catch (Exception e) {
			Assert.fail(
					"/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP is not displayed");
		}
		ManageAddOnsSelectionPage manageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsPage.verifymanageDataAndAddOnsPage();
		manageAddOnsPage.verifyIfListOfServiceContaingNetFlixOnly();
		return this;
	}

	// manage addons URL
	public BenefitsRedemption verifyManageAddonsURL2() {
		try {
			getDriver().getCurrentUrl().contains("/odf/DataService:ALL/Service:ALL/DataPass:ALL");
			Reporter.log("/odf/DataService:ALL/Service:ALL/DataPass:ALL is displayed");
		} catch (Exception e) {
			Assert.fail("/odf/DataService:ALL/Service:ALL/DataPass:ALL is not displayed");
		}

		return this;
	}

	// verify error url
	public BenefitsRedemption verifyErrorUrl() {
		try {
			getDriver().getCurrentUrl().contains("error");
			Reporter.log("error is displayed");
		} catch (Exception e) {
			Assert.fail("error is not displayed");
		}

		return this;
	}

	public BenefitsRedemption verifyOneTimePayment() {
		try {
			getDriver().getCurrentUrl().contains("onetimepayment");
			Reporter.log("one time payment page is displayed");
		} catch (Exception e) {
			Assert.fail("one time payment page is not displayed");
		}

		try {
			getDriver().navigate().back();
			Reporter.log("Navigating back");
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Unable to navigate back from one time patment page");
		}
		return this;
	}

	public BenefitsRedemption verifyLineDetailsPage() {
		try {
			getDriver().getCurrentUrl().contains("line-details");
			Reporter.log("line-details page is displayed");
		} catch (Exception e) {
			Assert.fail("line-detailspage is not displayed");
		}

		return this;
	}

}
