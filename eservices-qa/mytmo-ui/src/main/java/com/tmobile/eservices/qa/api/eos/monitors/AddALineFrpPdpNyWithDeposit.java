package com.tmobile.eservices.qa.api.eos.monitors;

import org.testng.Reporter;

import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

public class AddALineFrpPdpNyWithDeposit extends OrderProcess {

	/*
	 * 
	 * Steps For aal_FRP_PDP_NY_WithDeposit Flow, 1. getCatalogProducts 2.
	 * Create Cart 3. Update Cart For Plans 4. Update Cart For Services 5.
	 * Create Quote 6. Update Quote 7. Make Payment 8. Place Order
	 * 
	 */

	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "createCartAAL_NBYOD_FRP.txt");
			invokeCreateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void updateCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(
					ShopConstants.SHOP_MONITORS + "UpdateCart_AAL_For_Plans_And_Services.txt");
			invokeUpdateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue during Update Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void updateQuote(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_FRP_V5.txt");
			invokeUpdateQuote(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while invoking the Update Quote : " + e);
			throwServiceFailException();
		}

	}

}
