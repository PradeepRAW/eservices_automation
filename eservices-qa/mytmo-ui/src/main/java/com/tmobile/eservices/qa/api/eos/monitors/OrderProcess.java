package com.tmobile.eservices.qa.api.eos.monitors;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.CartApiV5;
import com.tmobile.eservices.qa.api.eos.CatalogApiV4;
import com.tmobile.eservices.qa.api.eos.DeviceTradeInApiV3;
import com.tmobile.eservices.qa.api.eos.OrderApi;
import com.tmobile.eservices.qa.api.eos.QuoteApiV5;
import com.tmobile.eservices.qa.api.eos.ServicesApiV3;
import com.tmobile.eservices.qa.api.exception.InvalidServiceResultException;
import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public abstract class OrderProcess extends ApiCommonLib {

	public Map<String, String> jwtAccessTokenMap = null;
	public Map<String, String> serviceResultsMap = null;

	public QuoteApiV5 quoteApi;
	public CatalogApiV4 catalogApi;
	public CartApiV5 cartApi;
	public ServicesApiV3 serviceApi;
	public OrderApi createOrder;
	public DeviceTradeInApiV3 deviceTradeInApi;

	public abstract void createCart(ApiTestData apiTestData) throws ServiceFailureException;

	public final Map<String, String> getAccessToken(ApiTestData apiTestData) {
		jwtAccessTokenMap = new HashMap<String, String>();
		try {

			jwtAccessTokenMap = getJwtToken(apiTestData, jwtAccessTokenMap);
			serviceResultsMap.put("jwtToken", jwtAccessTokenMap.get("jwtToken"));
			serviceResultsMap.put("accessToken", jwtAccessTokenMap.get("accessToken"));
			serviceResultsMap.put("email", tokenMapCommon.get("emailVal"));
			serviceResultsMap.put("userType", tokenMapCommon.get("userType"));
		} catch (Exception e) {
			Reporter.log("Exception while fetching JWT token : " + e);
			Assert.fail("Exception while fetching JWT token");
		}
		return jwtAccessTokenMap;
	}

	public void init(ApiTestData apiTestData) {
		serviceResultsMap = new HashMap<String, String>();
		serviceResultsMap.put("ban", apiTestData.getBan());
		serviceResultsMap.put("msisdn", apiTestData.getMsisdn());
		serviceResultsMap.put("sku", apiTestData.getSku());
		serviceResultsMap.put("correlationid", getValForCorrelation());

		catalogApi = new CatalogApiV4();
		quoteApi = new QuoteApiV5();
		cartApi = new CartApiV5();
		serviceApi = new ServicesApiV3();
		deviceTradeInApi = new DeviceTradeInApiV3();
		createOrder = new OrderApi();

	}

	public void getProducts(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(
					ShopConstants.SHOP_MONITORS + "familyIdProductPostAPI_EIP_v4.txt");
			logLargeRequest(requestBody, "ProductCatalog");
			Response response = catalogApi.retrieveGroupedProductsUsingPOST(apiTestData, requestBody,
					serviceResultsMap);
			checkAndReturnResponseStatusAndLogInfo(response, "ProductCatalog");		
			serviceResultsMap = findAvailableSkuInFamilyProducts(response, serviceResultsMap);
		} catch (Exception e) {
			Reporter.log("Service issue while fetching the Product Catalog : " + e);
			throwServiceFailException();			
		}
	}

	

	public void createQuote(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "CreateQuote_V5.txt");
			invokeCreateQuote(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while invoking the Create Quote : " + e);
			throwServiceFailException();
		}
	}

	public void updateQuote(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_EIP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while invoking the Update Quote : " + e);
			throwServiceFailException();
		}

	}

	public void doPayment(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String encryptedCardNum = getEncryptedCardNumber(apiTestData, serviceResultsMap);
			String lastFourDigits = apiTestData.getCardNumber().substring(apiTestData.getCardNumber().length() - 4);
			serviceResultsMap.put("encryptedCardNum", encryptedCardNum);
			serviceResultsMap.put("lastFourDigits", lastFourDigits);
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "updatePayment_req.txt");
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "UpdatePayment");
			Response response = quoteApi.updatePayment(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "UpdatePayment");
			if (isSuccessResponse) {
				// nothing to extract as of now, flow proceeds to place an
				// order.
			}

		} catch (Exception e) {
			throwServiceFailException();
		}

	}

	public void placeOrder(ApiTestData apiTestData) throws ServiceFailureException{
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_ORDER + "order_FRP_req.txt");
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "CreateOrder");

			Response response = createOrder.createOrder(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "CreateOrder");
			if (isSuccessResponse) {
				// nothing to extract as of now, flow ends with placing the
				// order.
			}
		} catch (Exception e) {
			throwServiceFailException();
		}
	}

	public final void processStandardUpgradeOrder(ApiTestData apiTestData) throws Exception {
		init(apiTestData);
		getAccessToken(apiTestData);
		getProducts(apiTestData);
		createCart(apiTestData);
		getSOCForSale(apiTestData);
		getAccessories(apiTestData);
		//getAccountDevicesEligibleForTradeIn(apiTestData);
		invokeTradeIn(apiTestData);
		updateCart(apiTestData);
		createQuote(apiTestData);
		updateQuote(apiTestData);
		updateAddress(apiTestData);
		// doPayment(apiTestData);
		// placeOrder(apiTestData);
	}

	public final void processAddALineOrder(ApiTestData apiTestData) throws Exception {
		init(apiTestData);
		getAccessToken(apiTestData);
		getProducts(apiTestData);
		createCart(apiTestData);
		getEligiblePlans(apiTestData);
		updateCart(apiTestData);
		createQuote(apiTestData);
		updateQuote(apiTestData);
		// doPayment(apiTestData);
		// placeOrder(apiTestData);
	}

	public Map<String, String> findAvailableSkuInFamilyProducts(Response response, Map<String, String> tokenMap)
			throws Exception {

		JsonNode rootJsonNode = getParentNodeFromResponse(response);
		JsonNode availableDeviceInfoNode = null;
		boolean tokenMapUpdated = false;
		String availabilityStatus = "AVAILABLE";
		int jVal = -1;
		if (!rootJsonNode.isMissingNode() && rootJsonNode.has("deviceFamilyInfo")) {
			JsonNode deviceFamilyInfoPathNode = rootJsonNode.path("deviceFamilyInfo");
			JsonNode parentNode = deviceFamilyInfoPathNode.path("data");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					JsonNode dataNode = parentNode.get(i);
					if (dataNode.isObject()) {
						JsonNode devicesInfoPath = dataNode.path("devicesInfo");
						if (!devicesInfoPath.isMissingNode() && devicesInfoPath.isArray()) {
							for (int j = 0; j < devicesInfoPath.size(); j++) {
								if (availabilityStatus
										.equalsIgnoreCase(devicesInfoPath.get(j).get("productStatus").asText())) {
									jVal = j;
									availableDeviceInfoNode = devicesInfoPath;
									tokenMap = prepareTokenMapWithAvailableDevice(availableDeviceInfoNode, tokenMap,
											jVal);
									tokenMapUpdated = true;
									return tokenMap;
								}
							}
						}
					}
				}
				if (!tokenMapUpdated) {
					String exceptionMsg = "No Valid SKU found in Products to proceed for Cart Creation";
					addExceptionMessagesToMapAndLog(response, exceptionMsg);
					throw new Exception(exceptionMsg);
				}
			}
		} else {
			String exceptionMsg = "No deviceFamilyInfo in Service Response";
			addExceptionMessagesToMapAndLog(response, exceptionMsg);
			throw new InvalidServiceResultException(exceptionMsg,
					new Exception(exceptionMsg));
		}

		return tokenMap;

	}

	public Map<String, String> prepareTokenMapWithAvailableDevice(JsonNode devicesInfoPath,
			Map<String, String> tokenMap, int jVal) {

		tokenMap.put("sku", getPathVal(devicesInfoPath.get(jVal), "skuNumber"));
		tokenMap.put("color", getPathVal(devicesInfoPath.get(jVal), "color"));
		tokenMap.put("deviceId", getPathVal(devicesInfoPath.get(jVal), "deviceId"));
		tokenMap.put("colorSwatch", getPathVal(devicesInfoPath.get(jVal), "swatch"));
		tokenMap.put("description", getPathVal(devicesInfoPath.get(jVal), "description"));
		tokenMap.put("memory", getPathVal(devicesInfoPath.get(jVal), "memory"));
		tokenMap.put("memoryUom", getPathVal(devicesInfoPath.get(jVal), "memoryUOM"));
		tokenMap.put("availabilityStatus",
				getPathVal(devicesInfoPath.get(jVal), "productAvailability.availabilityStatus"));
		tokenMap.put("estimatedShipDateTo",
				getPathVal(devicesInfoPath.get(jVal), "productAvailability.estimatedShipDateTo"));
		tokenMap.put("estimatedShipDateFrom",
				getPathVal(devicesInfoPath.get(jVal), "productAvailability.estimatedShipDateFrom"));
		tokenMap.put("fullRetailPrice", getPathVal(devicesInfoPath.get(jVal), "pricing.offerPrice"));
		tokenMap.put("imageUrl", getPathVal(devicesInfoPath.get(jVal), "images[0].url"));
		tokenMap.put("familyId", getPathVal(devicesInfoPath.get(jVal), "family"));
		tokenMap.put("modelName", getPathVal(devicesInfoPath.get(jVal), "modelName"));
		/*
		 * tokenMap.put("payNowAmount", getPathVal(devicesInfoPath.get(jVal),
		 * "pricing.loans.financeDetails[0].eipInfo.dueTodayAmount.value"));
		 * tokenMap.put("monthlyAmount", getPathVal(devicesInfoPath.get(jVal),
		 * "pricing.loans.financeDetails[0].eipInfo.financeAmount.value"));
		 * tokenMap.put("monthlynoOfMonths",
		 * getPathVal(devicesInfoPath.get(jVal),
		 * "pricing.loans.configuredTerm"));
		 */
		tokenMap.put("payNowAmount", "125.0");
		tokenMap.put("monthlyAmount", "31.25");
		tokenMap.put("monthlynoOfMonths", "24");
		return tokenMap;
	}

	public Map<String, String> getTokenMapForCreateCartAPI(Response response, Map<String, String> tokenMap) {

		JsonNode jsonNode = getParentNodeFromResponse(response);

		if (!jsonNode.isMissingNode()) {
			tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
		}

		return tokenMap;
	}

	public Map<String, String> getTokenMapForCreateQuoteAPI(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {

			tokenMap.put("orderId", getPathVal(jsonNode, "orderId"));
			tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));

			Double totaltax = Double.parseDouble(getPathVal(jsonNode, "taxCollection.totalTax"));
			if (StringUtils.isBlank(tokenMap.get("payNowAmount"))) {
				exceptionMsgs.put(ShopConstants.ERROR_MSG,
						"Seems like a Data issue. Please check Product catalog service Response");
				throw new IllegalArgumentException(
						"Cannot proceed due to Empty payNowAmount from product catalog service Response");
			}
			Double payNowAmount = Double.parseDouble(tokenMap.get("payNowAmount"));
			Double chargeAmount = totaltax + 6.99 + payNowAmount;
			DecimalFormat df = new DecimalFormat("###.##");

			tokenMap.put("chargeAmount", df.format(chargeAmount));

			tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
			tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			tokenMap.put("addressLine1", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressLine1"));
			tokenMap.put("cityName", getPathVal(jsonNode, "shippingDetails[0].shipTo.cityName"));
			tokenMap.put("stateCode", getPathVal(jsonNode, "shippingDetails[0].shipTo.stateCode"));
			tokenMap.put("zip", getPathVal(jsonNode, "shippingDetails[0].shipTo.zip"));
			tokenMap.put("countryCode", getPathVal(jsonNode, "shippingDetails[0].shipTo.countryCode"));
			tokenMap.put("tax", getPathVal(jsonNode, "taxCollection.totalTax"));
			tokenMap.put("firstName", getPathVal(jsonNode, "shippingDetails[0].shipTo.firstName"));
			tokenMap.put("familyName", getPathVal(jsonNode, "shippingDetails[0].shipTo.familyName"));
		}
		return tokenMap;
	}

	public boolean checkAndReturnResponseStatusAndLogInfo(Response response, String operationName) throws ServiceFailureException{
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			return true;
		} else {
			String exceptionMessage = operationName + "Service Failure with Status Code " + response.getStatusCode()
					+ ". Response : " + response.body().asString();
			addExceptionMessagesToMapAndLog(response, exceptionMessage);
			throw new ServiceFailureException(exceptionMessage, new Exception(exceptionMessage));
		}
		//return false;
	}

	public void invokeCreateCart(ApiTestData apiTestData, String requestBody) throws ServiceFailureException {
		try {
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "CreateCart");
			Response response = cartApi.createCart(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "createCart");
			if (isSuccessResponse) {
				serviceResultsMap = getTokenMapForCreateCartAPI(response, serviceResultsMap);
			}
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}

	public void invokeCreateQuote(ApiTestData apiTestData, String requestBody) throws ServiceFailureException {
		try {
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "createQuote");
			Response response = quoteApi.createQuote(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "createQuote");
			if (isSuccessResponse) {
				serviceResultsMap = getTokenMapForCreateQuoteAPI(response, serviceResultsMap);
			}
		} catch (Exception e) {
			Reporter.log("Service issue while invoking the Create Quote : " + e);
			throwServiceFailException();
		}

	}

	public void invokeUpdateQuote(ApiTestData apiTestData, String requestBody) throws ServiceFailureException {
		try {
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "UpdateQuote");
			Response response = quoteApi.updateQuote(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "UpdateQuote");
			if (isSuccessResponse) {
				// no info required from response as of now
			}
		} catch (Exception e) {
			Reporter.log("Issue while invoking the Update Quote : " + e);
			throwServiceFailException();
		}
	}

	public void invokeUpdateCart(ApiTestData apiTestData, String requestBody) throws ServiceFailureException {
		try {
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "UpdateCart");
			Response response = cartApi.updateCart(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "UpdateCart");
			if (isSuccessResponse) {
				// no info required from response as of now. Flow continues with
				// next step.
			}
		} catch (Exception e) {
			Reporter.log("Error updating Cart :" + e);
			throwServiceFailException();
		}

	}

	public void updateCart(ApiTestData apiTestData) throws ServiceFailureException{
		// to support classes that doesn't have this implementation. Flow
		// Continues with next step.
	}

	public void getAccessories(ApiTestData apiTestData) throws ServiceFailureException{
		// to support classes that doesn't have this implementation. Flow
		// Continues with next step.

	}

	public void updateAddress(ApiTestData apiTestData) throws ServiceFailureException {
		// to support classes that doesn't have this implementation. Flow
		// Continues with next step.
	}

	public void getSOCForSale(ApiTestData apiTestData) throws ServiceFailureException{
		// to support classes that doesn't have this implementation. Flow
		// Continues with next step.
	}

	public void getAccountDevicesEligibleForTradeIn(ApiTestData apiTestData) throws ServiceFailureException{
		// to support classes that doesn't have this implementation. Flow
		// Continues with next step.
	}

	public void invokeTradeIn(ApiTestData apiTestData) throws ServiceFailureException{
		// to support classes that doesn't have this implementation. Flow
		// Continues with next step.
	}

	public void getEligiblePlans(ApiTestData apiTestData) throws ServiceFailureException{
		// to support classes that doesn't have this implementation. Flow
		// Continues with next step.

	}
}
