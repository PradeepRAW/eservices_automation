package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * This page contains all the Home page elements and methods
 * 
 * @author rnallamilli
 *
 */
public class TMobileProd_HomePage extends CommonPage {

	@FindBy(css = ".logo")
	private WebElement tmobileHeader;

	@FindBy(css = "a.gbl-logo-mob")
	private WebElement tmobileLinkMobile;

	@FindBy(id = "footer_tmobilelink")
	private WebElement tMobileLink;

	@FindBy(css = "div.logo.t-mobile>a")
	private WebElement tMobileLogo;

	@FindBy(linkText = "Apple iPhone 8")
	private WebElement appleIPhone8;

	@FindBy(css = "a.ng-scope>span.text-magenta.ng-binding")
	private WebElement myTMobileLink;

	@FindBy(css = "button[ng-click*='vm.goToShop(vm.upgradeCTAText)'] ")
	private WebElement upgradeButton;

	@FindBy(css = ".btn.btn-primary.m-r-10.m-xs-b-15.m-md-b-15.ng-binding")
	private WebElement addALineButton;

	@FindBy(css = "i.undefined.fa.fa-headphones")
	private WebElement accessoriesIcon;

	@FindBy(linkText = "T-Mobile 4 ft. Lightning Cable - Blue")
	private WebElement accessoriesDeviceLink;
	
	@FindBy(css = "ul.lvl-one a[ng-href*='cell-phones']")
	private WebElement phoneTab;

	public TMobileProd_HomePage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify TMobileHomePage
	 * 
	 * @return
	 */
	public TMobileProd_HomePage verifyTMobile_HomePage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			Assert.assertTrue(tMobileLogo.isDisplayed(), "User is not navigated To T MOBILE Home Page.");
			Reporter.log("User is navigated To T MOBILE Home Page.");
		} catch (Exception e) {
			Reporter.log("User is not navigated To T MOBILE Home Page.");
		}
		return this;
	}
	
	/**
	 * Click Phone Tab
	 */
	public TMobileProd_HomePage clickPhoneTab() {
		waitforSpinner();
		checkPageIsReady();
		try
		{
			phoneTab.click();
		}
		catch(Exception e)
		{
			Assert.fail("Not able to click phone tab link"+e.getMessage());
		}
		return this;
	}

	/**
	 * Verify MyTMobileLink
	 * 
	 * @return
	 */
	public boolean verifyMyTMobileLink() {
		waitforSpinner();
		checkPageIsReady();
		return isElementDisplayed(myTMobileLink);
	}

	/**
	 * Verify TMobile-PLP Page
	 * 
	 * @return
	 */
	public boolean verifyTMobilePLPPage() {
		waitforSpinner();
		checkPageIsReady();
		boolean isElementDisplayed = false;
		isElementDisplayed = getDriver().getCurrentUrl().contains("cell-phones");
		return isElementDisplayed;
	}

	/**
	 * Click Device
	 */
	public TMobileProd_HomePage clickDevice() {
		waitforSpinner();
		checkPageIsReady();
		try
		{
			clickElementWithJavaScript(appleIPhone8);
		}
		catch(Exception e)
		{
			Assert.fail("Not able to click device"+e.getMessage());
		}
		return this;
	}

	/**
	 * Click MyTMobile Link
	 */
	public TMobileProd_HomePage clickMyTMobileLink() {
		waitforSpinner();
		checkPageIsReady();
		try
		{
			clickElementWithJavaScript(myTMobileLink);
		}
		catch(Exception e)
		{
			Assert.fail("Not able to click TMobile link"+e.getMessage());
		}
		return this;
	}

}
