package com.tmobile.eservices.qa.common;

public class ShopConstants {
	
	/**
	 * SHOP API Payload locations
	 */
	public static final String SHOP_AALELIGIBILITYCHECK = "shop/aalEligibilityCheck/";
	public static final String SHOP_ACCESSORIES = "shop/accessories/";
	public static final String SHOP_BENEFITSCL="shop/benefitsCL/";
	public static final String SHOP_CART="shop/cart/";
	public static final String SHOP_CATALOG="shop/catalog/";
	public static final String SHOP_DEVICETRADEIN="shop/deviceTradeIn/";
	public static final String SHOP_LOANAGREEMENT="shop/loanAgreement/";
	public static final String SHOP_ORDER="shop/order/";
	public static final String SHOP_QUOTE="shop/quote/";
	public static final String SHOP_SERVICE="shop/service/";
	public static final String SHOP_MONITORS="shop/monitors/";
	public static final String SHOP_NPI="shop/npi/";
	
	public static final String ERROR_MSG="errMsg";
	
}
