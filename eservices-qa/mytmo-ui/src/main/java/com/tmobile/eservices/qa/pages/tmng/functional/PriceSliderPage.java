package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author sputti
 *
 */
public class PriceSliderPage extends TmngCommonPage {

	private final String pageUrl = "/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510";

	// Locators for Plan Page
	@FindBy(xpath = "//div[@id='ln-magenta-card']//div[@class='ln-btn']//a[1]")
	private WebElement magentaPlanCTA;

	// Locators for Price Slider Page
	@FindBy(xpath = "//input[@type='range']")
	private WebElement slider;

	@FindBy(xpath = "//div[@id='price1']")
	private WebElement slider1;

	@FindBy(xpath = "//div[@id='price2']")
	private WebElement slider2;

	@FindBy(xpath = "//div[@id='price3']")
	private WebElement slider3;

	@FindBy(xpath = "//div[@id='price4']")
	private WebElement slider4;

	@FindBy(xpath = "//div[@id='price5']")
	private WebElement slider5;

	@FindBy(css = ".active .limited-time-offer")
	private WebElement offerText;

	@FindBy(css = ".active .price")
	private WebElement price;

	@FindBy(css = ".active .dollar-sign")
	private WebElement priceDollarSign;

	@FindBy(css = ".active .bubble")
	private WebElement bubbleText;

	@FindBy(css = ".active .deal-line-one")
	private WebElement priceDescription;

	@FindBy(css = ".active .section-legal")
	private WebElement legalText;

	@FindBy(xpath = "//a[@id='priceSlider_cta']")
	private WebElement primaryCTA;

	@FindBy(xpath = "//a[@id='callButton_cta']")
	private WebElement secondaryTFN;

	/**
	 * @param webDriver
	 */
	public PriceSliderPage(WebDriver webDriver) {
		super(webDriver);
	}

	// ------------------------------------------------------- Price Slider Page
	// Load Verification --------------------------------
	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the PlansPage class instance.
	 */
	public PriceSliderPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Plans page loaded");
		} catch (NoSuchElementException e) {
			Assert.fail("Plans Page not loaded");
		}

		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the PlansPage class instance.
	 */
	public PriceSliderPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

	/**
	 * Click on MagentaPlan CTA.
	 */
	public PriceSliderPage clickEssentialsCTA() {
		magentaPlanCTA.click();
		return this;
	}

	public PriceSliderPage verifyPriceSliderPageLoaded() {
		checkPageIsReady();
		waitForSpinnerInvisibility();
		try {
			if (getDriver().getCurrentUrl().contains(pageUrl)) {
				verifyPriceSliderPageUrl();
				Reporter.log("Price Slider page loaded");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Price Slider page not loaded");
		}
		return this;
	}

	public PriceSliderPage verifyPriceSliderPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		Reporter.log("PriceSlider page URL verified");
		return this;
	}

	// ------------------------------------------------------- Price Slider
	// Screen Verification --------------------------------
	/**
	 * Verify Price 1 Slider Tick Mark Should be displayed and clicked:
	 */
	public PriceSliderPage verifyPrice1Tick() {

		try {
			Assert.assertTrue(isElementDisplayed(slider));
			Assert.assertTrue(isElementDisplayed(slider1));
			slide(-500);
			Reporter.log("Price Slider displayed and clicked on 1 Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price Slider 1 Tick Mark not displayed and clicked in Price Slider Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Price 2 Slider Tick Mark Should be displayed and clicked:
	 */
	public PriceSliderPage verifyPrice2Tick() {

		try {
			Assert.assertTrue(isElementDisplayed(slider));
			Assert.assertTrue(isElementDisplayed(slider2));
			slide(-130);
			Reporter.log("Price Slider displayed and clicked on 2 Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price Slider 2 Tick Mark not displayed and clicked in Price Slider Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Price 3 Slider Tick Mark Should be displayed and clicked:
	 */
	public PriceSliderPage verifyPrice3Tick() {

		try {
			Assert.assertTrue(isElementDisplayed(slider));
			Assert.assertTrue(isElementDisplayed(slider3));
			slide(-65);
			Reporter.log("Price Slider displayed and clicked on 3 Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price Slider 3 Tick Mark not displayed and clicked in Price Slider Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Price 4 Slider Tick Mark Should be displayed and clicked:
	 */
	public PriceSliderPage verifyPrice4Tick() {

		try {
			Assert.assertTrue(isElementDisplayed(slider));
			Assert.assertTrue(isElementDisplayed(slider4));
			slide(130);
			Reporter.log("Price Slider displayed and clicked on 4 Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price Slider 4 Tick Mark not displayed and clicked in Price Slider Page" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Price 5+ Slider Tick Mark Should be displayed and clicked:
	 */
	public PriceSliderPage verifyPrice5PlusTick() {

		try {
			Assert.assertTrue(isElementDisplayed(slider));
			Assert.assertTrue(isElementDisplayed(slider5));
			slide(600);
			// slideUsingJQuery(100);
			Reporter.log("Price Slider displayed and clicked on 5+ Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price Slider 5 + Tick Mark not displayed and clicked in Price Slider Page" + e.getMessage());
		}
		return this;
	}

	public void slide(int value) {
		waitFor(ExpectedConditions.visibilityOf(slider), 2);
		int width = slider.getSize().getWidth();
		float min = 10;
		float max = 1000;
		float offsetX = width / (max - min) * value;
		new Actions(getDriver()).dragAndDropBy(slider, (int) offsetX, 10).perform();
	}

	public void slideUsingJQuery(int value) {
		((JavascriptExecutor) getDriver()).executeScript("$(arguments[0]).val(" + value + ").change()", slider);

	}

	/**
	 * Verify Price displayed by clicking on Slider Tick:
	 */
	public PriceSliderPage verifyOfferText(int val) {
		try {
			waitFor(ExpectedConditions.visibilityOf(offerText), 2);
			Assert.assertTrue(isElementDisplayed(offerText));
			Reporter.log("Offer Text Message  ' " + offerText.getText()
					+ " '  is displayed by clicking on Price Slider " + val + " Tick Mark");
		} catch (Exception e) {
			Assert.fail("Offer Text Message is not displayed by clicking on Price Slider Tick Mark" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Price displayed by clicking on Slider Tick:
	 */
	public PriceSliderPage verifyPrice(int val) {
		try {
			waitFor(ExpectedConditions.visibilityOf(price), 2);
			Assert.assertTrue(isElementDisplayed(price));
			Reporter.log("Price value  ' " + price.getText() + " '  is displayed by clicking on Price Slider " + val
					+ "  Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price value is not displayed by clicking on Price Slider Tick Mark" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify priceDollarSign displayed by clicking on Slider Tick:
	 */
	public PriceSliderPage verifyDollarSign(int val) {
		try {
			waitFor(ExpectedConditions.visibilityOf(priceDollarSign), 2);
			Assert.assertTrue(isElementDisplayed(priceDollarSign));
			Reporter.log("Price Dollar Sign  ' " + priceDollarSign.getText()
					+ " '  is displayed by clicking on Price Slider  " + val + " Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price Dollar Sign is not displayed by clicking on Price Slider Tick Mark" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify bubbleText displayed by clicking on Slider Tick:
	 */
	public PriceSliderPage verifyBubbleText(int val) {
		try {
			waitFor(ExpectedConditions.visibilityOf(bubbleText), 2);
			Assert.assertTrue(isElementDisplayed(bubbleText));
			Reporter.log("Callout Bubble Text message  ' " + bubbleText.getText()
					+ " '  is displayed by clicking on Price Slider  " + val + " Tick Mark");
		} catch (Exception e) {
			Assert.fail("Callout Bubble Text message is not displayed by clicking on Price Slider Tick Mark"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Price Description displayed by clicking on Slider Tick:
	 */
	public PriceSliderPage verifyPriceDescription(int val) {
		try {
			waitFor(ExpectedConditions.visibilityOf(priceDescription),2);
			Assert.assertTrue(isElementDisplayed(priceDescription));
			Reporter.log("Price Description  ' " + priceDescription.getText()
					+ " '  is displayed by clicking on Price Slider  " + val + "  Tick Mark");
		} catch (Exception e) {
			Assert.fail("Price Description is not displayed by clicking on Price Slider Tick Mark" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify legalText displayed by clicking on Slider Tick:
	 */
	public PriceSliderPage verifyLegalText(int val) {
		try {
			waitFor(ExpectedConditions.visibilityOf(legalText),2);
			Assert.assertTrue(isElementDisplayed(legalText));
			Reporter.log("Legal Copy message  ' " + legalText.getText()
					+ " '  is displayed by clicking on Price Slider  " + val + " Tick Mark");
		} catch (Exception e) {
			Assert.fail("Legal Copy message is not displayed by clicking on Price Slider Tick Mark" + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Primary and Secondary CTA displayed by clicking on Slider Tick:
	 */
	public PriceSliderPage verifyPrimaryCTA(int val) {
		try {
			waitFor(ExpectedConditions.visibilityOf(primaryCTA),2);
			Assert.assertTrue(isElementDisplayed(primaryCTA));
			Reporter.log("Primary CTA and Tool Free Number are displayed by clicking on Price Slider  " + val
					+ " Tick Mark");
		} catch (Exception e) {
			Assert.fail("Primary CTA and Tool Free Number are not displayed by clicking on Price Slider Tick Mark"
					+ e.getMessage());
		}
		return this;
	}

	/**
	 * Verify Primary CTA text alignment
	 */
	public PriceSliderPage verifyPrimaryCTAAlign() {
		try {
			waitFor(ExpectedConditions.visibilityOf(primaryCTA),2);
			Assert.assertTrue(isElementDisplayed(primaryCTA));
			Reporter.log("Primary CTA Text is ' " + primaryCTA.getText() + " '");
			Assert.assertEquals(primaryCTA.getCssValue("vertical-align"), "middle");
			Reporter.log("Vertical Alignment of Primary CTA is " + primaryCTA.getCssValue("vertical-align"));
			Assert.assertEquals(primaryCTA.getCssValue("text-align"), "center");
			Reporter.log("Text Alignment of Primary CTA is " + primaryCTA.getCssValue("text-align"));
		} catch (Exception e) {
			Assert.fail("Text Alignment of Primary CTA is not center" + e.getMessage());
		}
		return this;
	}
}