package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author pshiva
 *
 */
public class SignUpPage extends CommonPage {

	public static final String signUpPageUrl = "/oauth2/v1/signup";
	public static final String signUpPageLoadText = "Sign up for T-Mobile ID";
	private int timeout = 60;

	@FindBy(css = "div.ui_headline.mpl20.ng-binding")
	private WebElement singupText;

	@FindBy(id = "firstName")
	private WebElement firstName;

	@FindBy(id = "lastName")
	private WebElement lastName;

	@FindBy(id = "email_id")
	private WebElement emailId;

	@FindBy(id = "confirm_email_id")
	private WebElement confirmEmailId;

	@FindBy(id = "phone")
	private WebElement phoneNumber;

	@FindBy(id = "password")
	private WebElement password;

	@FindBy(id = "passwordforminput1")
	private WebElement confirmPassword;

	@FindBy(css = "a[ng-if*='signUpController']")
	private WebElement nextBtn;

	@FindBy(linkText = "Back ")
	private WebElement backBtn;

	/**
	 * 
	 * @param webDriver
	 */
	public SignUpPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * get sign up Text
	 * 
	 * @return value
	 */
	public SignUpPage getSignUpText() {
		try {
			singupText.getText();
			Reporter.log("Sign Up text is visible.");
		} catch (Exception e) {
			Assert.fail("SignUp text is not visible.");
		}

		return this;
	}

	/**
	 * This method is to enter FirstName
	 */
	private SignUpPage enterFirstName(String value) {
		try {
			sendTextData(firstName, value);
			firstName.sendKeys(Keys.TAB);
			Reporter.log("First name entered successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to to enter first name.");
		}
		return this;
	}

	/**
	 * This method is to enter lastName
	 */
	private SignUpPage enterLastName(String value) {
		try {
			sendTextData(lastName, value);
			lastName.sendKeys(Keys.TAB);
			Reporter.log("Last Name entered successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to enter last name.");
		}

		return this;
	}

	/**
	 * This method is to enter emailId
	 */
	private SignUpPage enterEmailId(String value) {

		try {
			emailId.sendKeys(value);
			emailId.sendKeys(Keys.TAB);
			Reporter.log("EmailId entered successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to enter email id.");
		}

		return this;
	}

	/**
	 * This method is to enter confirmEmailId
	 */
	private SignUpPage enterConfirmEmailId(String value) {
		try {
			confirmEmailId.sendKeys(value);
			confirmEmailId.sendKeys(Keys.TAB);
			Reporter.log("ConfirmEmailId entered successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to enter confirm email id.");
		}
		return this;
	}

	/**
	 * This method is to enter phoneNumber
	 */
	private SignUpPage enterPhoneNumber(String value) {
		try {
			sendTextData(phoneNumber, value);
			phoneNumber.sendKeys(Keys.TAB);
			Reporter.log("Phone number entered successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to enter phone number.");
		}

		return this;
	}

	/**
	 * This method is to enter Password
	 */
	private SignUpPage enterPassword(String value) {
		try {
			sendTextData(password, value);
			password.sendKeys(Keys.TAB);
			Reporter.log("Password entered successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to enter password.");
		}
		return this;
	}

	/**
	 * This method is to re enter Password
	 */
	private SignUpPage enterConfirmPassword(String value) {
		try {
			sendTextData(confirmPassword, value);
			confirmPassword.sendKeys(Keys.TAB);
			Reporter.log("Confirm Password entered successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to enter confirm password.");
		}
		return this;
	}

	/**
	 * This method is to click Next button
	 */
	private SignUpPage clickNextButton() {
		try {
			checkPageIsReady();
			nextBtn.click();
			Reporter.log("Next Button clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to click next button.");
		}
		return this;
	}

	/**
	 * This method is to click Back button
	 */
	public SignUpPage clickBackButton() {
		try {
			checkPageIsReady();
			backBtn.click();
			Reporter.log("Back Button clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to click back button.");
		}
		return this;
	}

	/**
	 * This method is to perform Sign Up action
	 * 
	 * @param myTmoData
	 */
	public SignUpPage enterValuesAndClickNext(MyTmoData myTmoData) {
		try {
			enterFirstName(myTmoData.getSignUpData().getFirstName());
			enterLastName(myTmoData.getSignUpData().getLastName());
			String email = myTmoData.getSignUpData().getEmailId();
			enterEmailId(myTmoData.getLoginEmailOrPhone().trim() + "@gmail.com");
			enterConfirmEmailId(myTmoData.getLoginEmailOrPhone().trim() + "@gmail.com");
			enterPhoneNumber(myTmoData.getLoginEmailOrPhone().trim());
			String pswd = myTmoData.getSignUpData().getPassword();
			enterPassword(pswd.trim());
			enterConfirmPassword(pswd.trim());
			clickNextButton();
			Reporter.log("Successfully registered Sign Up Data.");
		} catch (Exception e) {
			Assert.fail("Not able to register sign up data.");
		}
		return this;

	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the Reset Security Question class instance.
	 */
	public SignUpPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(signUpPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Reset Security Question class instance.
	 */
	public SignUpPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(signUpPageUrl);
		return this;
	}

}
