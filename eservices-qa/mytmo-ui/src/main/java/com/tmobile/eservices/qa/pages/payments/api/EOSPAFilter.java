package com.tmobile.eservices.qa.pages.payments.api;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSPAFilter extends  EOSCommonLib {

	


//aginghistory

/***********************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 ***********************************************************************************/

public Response getResponsePA(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization", "Bearer "+alist[2]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("application_id", "ESERVICE");
	    restService.addHeader("cache-control", "no-cache");
	    restService.addHeader("channel_id", "WEB");  
	    response = restService.callService("v1/paymentArrangement/paDetails",RestCallType.GET);
	    
	}

	    return response;
}


public String getaginghistory(Response response) {
	String aginghistory=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("agingHistory.pastDueAmountGreaterThan30days")!=null) return "morethan30";
		else return "lessthan30";		
	}
	
	return aginghistory;
}


}
