
/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * Sudheer Reddy Chilukuri
 *
 */
public class ChangePlansReviewPage extends CommonPage {

	// Elements by Names

	private By listOfWarningIconOnPlansBlade = By.xpath("(//*[contains(@class,'warning-error')])[%s]");

	@FindBy(css = "div.body div[class*='Display4']")
	private WebElement planName;

	@FindBy(css = "p[aria-label='PLAN']")
	private WebElement textPlansOnPlansBlade;

	@FindBy(css = "p[aria-label='ADDON']")
	private WebElement textAddonOnAddonsBlade;

	@FindBy(css = "div[class*='text-right'] p.H6-heading")
	private List<WebElement> costOnPlansBlade;

	@FindBy(xpath = "//p[contains(text(),'Plans')]")
	private WebElement plansTabOnPlansReviewPage;

	@FindBy(xpath = "//*[contains(text(),'Device')]")
	private WebElement deviceTabOnPlansReviewPage;

	@FindBy(xpath = "//*[contains(text(),'Add-ons')]")
	private WebElement addOnsTabOnPlansReviewPage;

	@FindBy(xpath = "//*[contains(text(),'New monthly total')]")
	private WebElement newMonthlyTotalBladeOnPlansReviewPage;

	@FindBy(xpath = "//*[contains(@class,'Display3 padding-top-small')]")
	private WebElement headerOnPlansReviewPage;

	@FindBy(css = "button[aria-label='Click to Cancel']")
	private WebElement cancelCTA;

	@FindBy(xpath = "//img[@class='image-width']")
	private WebElement newPlanImageOnPlansReviewPage;

	@FindBy(xpath = "//span[@class='legal']")
	private WebElement congestionRelatedTermsAndConditions;

	@FindBy(xpath = "(//*[@class='col-12 col-md-10 offset-md-1 legal black'])")
	private WebElement autoPayTermsAndConditionsText;

	@FindBy(xpath = "//*[contains(@class,'text-center padding-top-small')]")
	private WebElement subTextOnPlansReviewPage;

	@FindBy(xpath = "//span[contains(text(),'New plan')]")
	private WebElement newPlanTextOnPlansReviewPage;

	@FindBy(css = "button[aria-label='Click to Agree & Submit']")
	private WebElement agreeAndSubmitCTA;

	@FindBy(xpath = "(//*[@class='H6-heading']//p[1])[3]")
	private WebElement textDevicesOnDevicesBlade;

	@FindBy(xpath = "(//span[@class='body padding-top-xsmall'])")
	private WebElement textTapToReviewChangesOnPlansBlade;

	@FindBy(xpath = "(//*[@class='H6-heading']//p[2])[2]")
	private WebElement tapReviewChangesOnAddonsBlade;

	@FindAll({@FindBy(css = "p[aria-label='Taxes and fees included.']"),
	@FindBy(css = "p[aria-labe='Taxes and fees included.']")})
	private WebElement taxesAndFeesTextOnPlansBlade;

	@FindBy(xpath = "(//*[@class='H6-heading d-inline-block'])[4]")
	private WebElement costOnAddonsBlade;

	@FindBy(xpath = "(//*[@class='H6-heading d-inline-block'])[3]")
	private WebElement costOnDevicesBlade;

	@FindBy(css = "div[aria-label='New monthly total']")
	private WebElement textOnNewMonthlyTotalBlade;

	@FindBy(xpath = "(//p[@class='Display6'])")
	private WebElement costOnNewMonthlyTotalOnBlade;

	@FindBy(xpath = "(//span[@class='tele-hal p-l-5'])")
	private WebElement effectiveDate;

	@FindBy(xpath = "//accept-terms[contains(text(),'I agree that (a) my Agreement')]")
	private WebElement legalText;

	@FindBy(xpath = "(//*[contains(text(),'By signing up')])")
	private List<WebElement> autoPayTermsAndConditions;

	@FindBy(xpath = "//*[text()='Terms & Conditions']")
	private WebElement tAndClinkInAutoPayText;

	@FindBy(xpath = "(//span[@class='Display3'])")
	private WebElement headingOfTermsAndConditionsPage;

	@FindBy(xpath = "//div[@class='body-link cursor padding-horizontal-xsmall']")
	private WebElement printThisPageLink;

	@FindBy(xpath = "//div[contains(@class,'print-icon')]")
	private WebElement printIcon;

	@FindBy(css = ".PrimaryCTA.full-btn-width")
	private WebElement backCTAOnAutoPayTermsAndConditions;

	@FindBy(css = "p[aria-label*='You’re saving']")
	private WebElement autoPayMessageOnAutopayBlade;

	@FindBy(css = ".H6-heading.padding-bottom-xsmall")
	private WebElement autoPayLabelOnAutoPayBlade;

	@FindBy(css = "div.padding-top-medium.no-padding")
	private WebElement firstAutoPayDateAndMessage;

	@FindBy(css = "span[class='slider round']")
	private WebElement autopaySlider;

	@FindBy(css = ".col-12.padding-vertical-small.cursor")
	private WebElement paymentMethodBlade;

	@FindBy(css = "span[aria-label='Autopay toggle button is On']")
	private WebElement autoPayBlade;

	@FindBy(css = ".warning-regular-35.d-inline-block")
	private WebElement iconOnWarningModal;

	@FindBy(xpath = "//*[@class='Display5 padding-top-small text-center']/span")
	private WebElement warningModalHeader;

	@FindBy(css = ".body.padding-top-medium.padding-top-small-md.mb-0")
	private WebElement warningModalText;

	@FindBy(css = ".body.padding-top-small.padding-top-medium-md")
	private WebElement warningModalTextForContinue;

	@FindBy(xpath = "//button[text()='Yes']")
	private WebElement warningModalYesBtn;

	@FindBy(css = ".PrimaryCTA-accent.pull-left")
	private WebElement warningModalContinueCTA;

	@FindBy(css = "span[aria-label='Plan update']")
	private List<WebElement> warningModal;

	@FindBy(xpath = "(//*[contains(@class,'warning-error')])")
	private WebElement warningIcon;

	@FindBy(css = "span.float-left.arrow-size.arrow-back")
	private WebElement unavBackArrow;

	@FindBy(xpath = "//*[contains(@class,'padding-top-small padding-top-')]")
	private WebElement deltaCostValue;

	@FindBy(xpath = "//p[@class='H4-heading']")
	private WebElement deltaCostMessage;

	public ChangePlansReviewPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Click On UNAV Arrow
	 * 
	 * @param webDriver
	 */

	public ChangePlansReviewPage clickOnUNAVArrow() {
		checkPageIsReady();
		waitForDataPassSpinnerInvisibility();
		try {
			checkPageIsReady();
			Assert.assertTrue(unavBackArrow.isDisplayed(), "UNAV Arrow is available");
			unavBackArrow.click();
			Reporter.log("Clicked on Unav Arrow ");
		} catch (Exception e) {
			Assert.fail("UNAV Arrow is not available");
		}
		return this;
	}

	/**
	 * Verify Device Breakdown page is displayed
	 * 
	 * @return
	 */
	public ChangePlansReviewPage verifyChangePlansReviewPage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains("plan-review"));
			checkWarningModalInAllRegularScenarios();
			Reporter.log("Plans Review page is displayed");
		} catch (Exception e) {
			Assert.fail("Plans Review page is not displayed");
		}
		return this;
	}

	/**
	 * click on Plans tab
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnPlansBlade() {
		plansTabOnPlansReviewPage.click();
		Reporter.log("Clicked on Plans tab");

		return this;
	}

	/**
	 * click on Device tab
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnDevicesBlade() {
		deviceTabOnPlansReviewPage.click();
		return this;
	}

	/**
	 * click on Addons Blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnAddonsBlade() {
		addOnsTabOnPlansReviewPage.click();
		return this;
	}

	/**
	 * click on New Monthly Total Blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnNewMonthlyTotalBlade() {
		newMonthlyTotalBladeOnPlansReviewPage.click();
		return this;
	}

	/**
	 * Check Header on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkHeaderOnPlansReviewPage() {
		try {
			headerOnPlansReviewPage.isDisplayed();
			if (headerOnPlansReviewPage.getText().trim().equals("Review changes"))
				Reporter.log("Header of Review page is matched.");
			else
				Assert.fail("Header of Review page is mismatched.");
		} catch (Exception e) {
			Assert.fail("Header of Review page is not displayed.");
		}
		return this;
	}

	/**
	 * Check SubHeader on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkSubHeaderOnPlansReviewPage() {
		try {
			subTextOnPlansReviewPage.isDisplayed();
			if (subTextOnPlansReviewPage.getText().trim()
					.contains("Here's the cost breakdown for moving your current lines and services to a new plan."))
				Reporter.log("Sub Header of Review page is matched.");
			else
				Assert.fail("subHeader of Review page is mismatched.");
		} catch (Exception e) {
			Assert.fail("SubHeader of Review page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Promotional text on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkPromotionalTextOnPlansReviewPage() {
		try {
			subTextOnPlansReviewPage.isDisplayed();
			if (subTextOnPlansReviewPage.getText().trim().contains(
					"If you have a free line(s) promotion paid via bill credits or are on a promotional plan, you'll lose those promotional benefits if you change plans."))
				Reporter.log("Promotional text of Review page is matched.");
			else
				Assert.fail("Promotional text of Review page is mismatched.");
		} catch (Exception e) {
			Assert.fail("Promotional text of Review page is not displayed.");
		}
		return this;
	}

	/**
	 * Check text "New Plan" on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTextNewPlanOnPlansReviewPage() {
		try {
			newPlanTextOnPlansReviewPage.isDisplayed();
			if (newPlanTextOnPlansReviewPage.getText().trim().equals("New plan"))
				Reporter.log("New Plan text of Review page is matched.");
			else
				Assert.fail("New Plan text of Review page is mismatched.");
		} catch (Exception e) {
			Assert.fail("New Plan text of Review page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Image of New Plan on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkImageOfNewPlanOnPlansReviewPage() {
		try {
			newPlanImageOnPlansReviewPage.isDisplayed();
			Reporter.log("Image for New Plan on Review page is displayed.");
		} catch (Exception e) {
			Reporter.log("Image for New Plan on Review page is not displayed.");
		}
		return this;
	}

	/**
	 * Check New Plan name
	 * 
	 * @return
	 */
	public ChangePlansReviewPage verifyNewPlanName(String expectedPlanName) {
		try {
			String name = planName.getText().replaceAll("[^\\x00-\\x7f]", "").toString();
			Assert.assertTrue(name.contains(expectedPlanName), "Selected Plan on Comparison page is not displayed");
			Reporter.log("Selected Plan on Comparison page is matched with Review page.");
		} catch (Exception e) {
			Assert.fail("Selected Plan on Comparison page is not displayed.");
		}
		return this;
	}

	/**
	 * Check warning image on Plans blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkWarningImageOnPlansBladeOnPlansReviewPage() {
		try {
			// WebElement warningIcon =
			// getDriver().findElement(getNewLocator(listOfWarningIconOnPlansBlade,
			// "1"));
			warningIcon.isDisplayed();
			Reporter.log("Warning icon is displayed on Plans Blade on Plans Review page.");
		} catch (Exception e) {
			Assert.fail("Warning icon is not displayed on Plans Blade on Plans Review page.");
		}
		return this;
	}

	/**
	 * Check warning image on Plans blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage whenWarningImageNotDisplayedOnPlansBlade() {
		try {

			warningIcon.isDisplayed();
			Assert.fail(
					"Warning icon is displayed on Plans Blade on Plans Review page even when MI line is compatible during migration.");
		} catch (Exception e) {
			Reporter.log("Warning icon is not displayed on Plans Blade on Plans Review page.");
		}
		return this;
	}

	/**
	 * Check text "Plan" on Plans Blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTextPlansOnPlansBlade() {
		try {
			Assert.assertTrue(textPlansOnPlansBlade.isDisplayed(), " Plans Blade is not dispalying");
			Reporter.log("Plans text on Plans Blade is matched.");
		} catch (Exception e) {
			Assert.fail("Plans text on Plans blade is not displayed.");
		}
		return this;
	}

	/**
	 * Check text "Tap to Review changes" on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTapToReviewChangesTextOnPlansBlade() {
		try {
			textTapToReviewChangesOnPlansBlade.isDisplayed();
			if (textTapToReviewChangesOnPlansBlade.getText().trim().equals("Tap to review changes."))
				Reporter.log("Tap to Review Changes text on Plans Blade is matched.");
			else
				Assert.fail("Tap to Review Changes text on Plans Blade is not mismatched.");
		} catch (Exception e) {
			Assert.fail("Tap to Review Changes text on Plans Blade is not displayed.");
		}
		return this;
	}

	/**
	 * Check text "Tap to Review changes" on Plans Review page is not displayed
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTapToReviewChangesTextNotDisplayedOnPlansBlade() {
		try {
			textTapToReviewChangesOnPlansBlade.isDisplayed();
			Assert.fail(
					"Tap to Review Changes text on Plans Blade is displayed even when user has compatible MBB line.");
		} catch (Exception e) {
			Reporter.log(
					"Tap to Review Changes text on Plans Blade is displayed even when user has compatible MBB line.");
		}
		return this;
	}

	/**
	 * Check text "Taxes and fees included" on Plans blade.
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTaxesAndFeesTextOnPlansBlade() {
		try {
			taxesAndFeesTextOnPlansBlade.isDisplayed();
			if (taxesAndFeesTextOnPlansBlade.getText().equals("Taxes and fees included."))
				Reporter.log("Taxes and fees text on Plans Blade is matched.");
			else
				Assert.fail("Taxes and fees text on Plans Blade is not matched.");
		} catch (Exception e) {
			Assert.fail("Taxes and fees text on Plans Blade is not displayed.");
		}
		return this;
	}

	/**
	 * Read cost on Plans blade
	 * 
	 * @return
	 */
	public String getCostOnPlansBlade() {
		String costOnBlade = costOnPlansBlade.get(0).getText().trim();
		try {
			costOnBlade = costOnBlade.split("/")[0];
			Reporter.log("Actual cost on Plans blade is " + costOnBlade + "Displayed");
		} catch (Exception e) {
			Assert.fail("Not able to read cost on Plans blade.");
		}
		return costOnBlade;
	}

	/**
	 * Check warning image on Addons blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkWarningImageOnAddonsBladeOnPlansReviewPage() {
		try {
			WebElement warningIcon = getDriver().findElement(getNewLocator(listOfWarningIconOnPlansBlade, "2"));
			warningIcon.isDisplayed();
			Reporter.log("Warning icon is displayed on Addons Blade on Plans Review page.");
		} catch (Exception e) {
			Assert.fail("Warning icon is not displayed on Addons Blade on Plans Review page.");
		}
		return this;
	}

	/**
	 * Check text "Addons" on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTextAddonsOnAddonsBlade() {
		try {
			textAddonOnAddonsBlade.isDisplayed();
			if (textAddonOnAddonsBlade.getText().trim().equals("Add-ons"))
				Reporter.log("Addons text on Addons Blade is matched.");
			else
				Assert.fail("Addons text on Addons Blade is mismatched.");
		} catch (Exception e) {
			Assert.fail("Addons text on Addons blade is not displayed.");
		}
		return this;
	}

	/**
	 * Read cost on Addons blade
	 * 
	 * @return
	 */
	public String verifyCostOnAddonsBlade() {
		String costOnBlade = costOnAddonsBlade.getText().trim();
		try {
			// costOnBlade = costOnBlade.replace("$", "");
			costOnBlade = costOnBlade.split("/")[0];
		} catch (Exception e) {
			Assert.fail("Not able to calculate Total Monthly cost.");
		}
		return costOnBlade;
	}

	/**
	 * Check text "Devices" on Plans Review page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTextDevicesOnAddonsBlade() {
		try {
			textDevicesOnDevicesBlade.isDisplayed();
			if (textDevicesOnDevicesBlade.getText().trim().equals("Devices"))
				Reporter.log("Devices text on Devices Blade is matched.");
			else
				Assert.fail("Devices text on Devices Blade is mismatched.");
		} catch (Exception e) {
			Assert.fail("Devices text on Devices blade is not displayed.");
		}
		return this;
	}

	/**
	 * Read cost on Devices blade
	 * 
	 * @return
	 */
	public String verifyCostOnDevicesBlade() {
		String costOnBlade = costOnDevicesBlade.getText().trim();
		try {
			costOnBlade = costOnBlade.replace("$", "");
			costOnBlade = costOnBlade.split("/")[0];
		} catch (Exception e) {
			Assert.fail("Not able to calculate on Devices blade");
		}
		return costOnBlade;
	}

	/**
	 * Check text Effective Date
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkEffectiveDateText() {
		try {
			// effectiveDateText.isDisplayed();
			Reporter.log("Effective date text is displayed on Plans review page.");
		} catch (Exception e) {
			Assert.fail("Effective date text is not displayed on Plans review page.");
		}
		return this;
	}

	/**
	 * Check text Effective Date
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkEffectiveDate() {
		try {
			effectiveDate.isDisplayed();
			Reporter.log("Effective date is displayed on Plans review page.");
		} catch (Exception e) {
			Assert.fail("Effective date is not displayed on Plans review page.");
		}
		return this;
	}

	/**
	 * Check text "New Monthly Total" on New Monthly Total blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTextNewMonthlyTotalOnMonthlyTotalBlade() {
		try {
			textOnNewMonthlyTotalBlade.isDisplayed();
			String actualText = textOnNewMonthlyTotalBlade.getText().trim();
			Assert.assertEquals(actualText, "New monthly total",
					"New Monthly Total text on New Monthly Total Blade is mismatched.");
		} catch (Exception e) {
			Assert.fail("New Monthly Total text on New Monthly Total Blade is not displayed.");
		}
		return this;
	}

	/**
	 * Check text "Taxes and fees included" on New Monthly Total blade.
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkTaxesAndFeesTextOnNewMonthlyTotalBlade(String planName) {
		try {
			taxesAndFeesTextOnPlansBlade.isDisplayed();
			if (planName.equalsIgnoreCase("TMobile Essentials")) {
				if (taxesAndFeesTextOnPlansBlade.getText().equals("Taxes and fees additional."))
					Reporter.log(
							"Taxes and fees text on New Monthly Total Blade is matched when user selected Essentails plan.");
				else
					Assert.fail(
							"Taxes and fees text on New Monthly Total Blade is not matched when user selected Essentails plan.");
			} else {
				if (taxesAndFeesTextOnPlansBlade.getText().equals("Taxes and fees included."))
					Reporter.log("Taxes and fees text on New Monthly Total Blade is matched.");
				else
					Assert.fail("Taxes and fees text on New Monthly Total Blade is not matched.");
			}
		} catch (Exception e) {
			Assert.fail("Taxes and fees text on New Monthly Total Blade is not displayed.");
		}
		return this;
	}

	/**
	 * Read cost on New Monthly Total blade
	 * 
	 * @return
	 */
	public String verifyCostOnNewMonthlyTotalBlade() {
		String costOnBlade = null;
		try {
			costOnBlade = costOnNewMonthlyTotalOnBlade.getText().trim();
			if (costOnBlade.contains("/mo"))
				Reporter.log("New Monthly Total Cost on Blade displayed with '/mo'");
			else
				Assert.fail("New Monthly Total Cost on Blade is not displayed with '/mo'");
			costOnBlade = costOnBlade.split("/")[0];

		} catch (Exception e) {
			Assert.fail("Not able to calculate on cost on New Monthly Total blade");
		}
		return costOnBlade;
	}

	/**
	 * Click on Cancel CTA
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnCancelCTA() {
		try {
			cancelCTA.isDisplayed();
			cancelCTA.click();
			Reporter.log("Cancel CTA is clicked on Plans Review page.");
		} catch (Exception e) {
			Assert.fail("Cancel CTA is not displayed on Plans Review page.");
		}
		return this;
	}

	/**
	 * Verify Legal text
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkLegalTextOnPlansAndReviewPage() {
		String expectedLegalText = "By clicking \"Agree and submit\", I agree that (a) my Agreement with T-Mobile includes the Service Agreement, any terms specific to my Rate Plan, and T-Mobile’s Terms & Conditions; (b) T-Mobile requires Arbitration of Disputes unless I previously opted out per T-Mobile’s Terms & Conditions; and (c) I accept T-Mobile’s Electronic Signature Terms.";
		try {
			legalText.isDisplayed();
			String actualLegalText = legalText.getText().trim();
			Assert.assertEquals(actualLegalText, expectedLegalText, "Legal text is matched");
			Reporter.log("Legal text is matched");
		} catch (Exception e) {
			Assert.fail("Legal text is not displayed.");
		}
		return this;
	}

	/**
	 * Verify Autopay Legal Text On Plans And Review Page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkAutopayLegalTextOnPlansAndReviewPage() {
		String expectedLegalText = "By signing up for AutoPay, I agree to the Terms & Conditions.";
		try {
			Assert.assertTrue(verifyElementBytext(autoPayTermsAndConditions, expectedLegalText),"Autopay Legal text is not displayed.");
			Reporter.log("Autopay Legal text is matched");
		} catch (Exception e) {
			Assert.fail("Autopay Legal text is not displayed.");
		}
		return this;
	}

	/**
	 * Click on "Terms & Conditions" link on Autopay Legal text
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnTermsAndConditionsLinkOnAutopayLegalText() {
		try {
			tAndClinkInAutoPayText.click();
		} catch (Exception e) {
			Assert.fail("Not able to click on link in Autopay Legal text");
		}
		return this;
	}

	/**
	 * Verify Header on Autopay Terms and conditions page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkHeaderOnAutopayTermsAndConditionsPage() {
		try {
			checkPageIsReady();
			headingOfTermsAndConditionsPage.isDisplayed();
			String actualHeading = headingOfTermsAndConditionsPage.getText().trim();
			Assert.assertEquals(actualHeading, "Terms and Conditions",
					"Heading on Autopay Terms And Conditions page is mismatched");
			Reporter.log("Heading for Autopay Terms and Conditions page is matched");
		} catch (Exception e) {
			Assert.fail("Heading for Autopay Terms and Conditions page is not displayed.");
		}
		return this;
	}

	/**
	 * Verify Print icon and Print this page link on Autopay Terms and conditions
	 * page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkPrintIconAndPrintThisPageLink() {
		try {
			printIcon.isDisplayed();
			Reporter.log("Print Icon is displayed on Autopay Terms and Conditions page.");
		} catch (Exception e) {
			Assert.fail("Print icon link is not displayed on Autopay Terms and Conditions page.");
		}
		try {
			printThisPageLink.isDisplayed();
			Reporter.log("Print this page link is displayed on Autopay Terms and Conditions page.");
		} catch (Exception e) {
			Assert.fail("Print this page link is not displayed on Autopay Terms and Conditions page.");
		}
		return this;
	}

	/**
	 * Verify Autopay text
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkAutopayTextFromAutopayTermsAndConditionsPage() {
		String paymentTerms = "Payment Terms.By submitting a payment, you authorize T-Mobile to debit your bank account/debit card or charge your credit card for the total amount and date shown.";
		String autoPayTermsAndConditions = "AutoPay Terms and Conditions.Please read the following AutoPay (“AutoPay”) Terms and Conditions. They describe the terms you agree to with regard to recurring payments you make to T-Mobile. These AutoPay Terms and Conditions may be updated at any time, and changes become binding once posted to the T-Mobile website.";
		String autoPayTermsAndConditionsTexts = "If you have elected to enroll in AutoPay, you authorize T-Mobile to automatically debit your bank account/debit card or charge your credit card, on a recurring basis no earlier than 2 days before your statement due date until you terminate your authorization online at my.T-Mobile.com or by calling 1-877-453-1304. You authorize T-Mobile to store your payment method for future payments by you and any verified users on the account. The amount of each monthly recurring payment will be the full monthly price reflected on your monthly statement for wireless service, plus any additional services, equipment, taxes, fees and other charges applicable to your T-Mobile purchase(s). If you find a billing error and notify T-Mobile at least 4 days before your monthly statement is due, we will attempt to correct the error before the next recurring payment. Also, if you sign up for, cancel or make changes to AutoPay 2 days or less before the payment due date, the change may not take effect until the following payment cycle. Otherwise, we will automatically debit/charge the amount reflected on your monthly statement. After terminating your authorization, you will be responsible for scheduling payments for subsequent monthly charges. You also authorize T-Mobile to credit your bank account/card in the appropriate amount for any refunds or other billing adjustments.";
		String additionalPaymentTermsAndConditions = "Additional Payment Terms and Conditions. If you are signing on behalf of a corporate, organizational or governmental entity, you represent and warrant that (1) you are authorized to sign on behalf of such entity and (2) the credit card you are using was established for business purposes and that it is not a debit card.";
		String legalText = "T-MOBILE SHALL BEAR NO LIABILITY OR RESPONSIBILITY FOR ANY LOSSES OF ANY KIND THAT YOU MAY INCUR AS A RESULT OF AN ERRONEOUS STATEMENT, ANY DELAY IN THE ACTUAL DATE ON WHICH YOUR ACCOUNT IS DEBITED OR YOUR FAILURE TO PROVIDE ACCURATE AND/OR VALID PAYMENT INFORMATION.";
		String autoPayText = autoPayTermsAndConditionsText.getText().trim();
		if (autoPayText.contains(paymentTerms))
			Reporter.log("Payment Terms are displayed in Autopay Terms and Conditions page.");
		else
			Assert.fail("Payment Terms are displayed in Autopay Terms and Conditions page.");
		if (autoPayText.contains(autoPayTermsAndConditions))
			Reporter.log("Autopay Terms and Conditions are displayed in Autopay Terms and Conditions page.");
		else
			Assert.fail("Autopay Terms and Conditions are displayed in Autopay Terms and Conditions page.");
		if (autoPayText.contains(autoPayTermsAndConditionsTexts))
			Reporter.log("Autopay Terms and Conditions are displayed in Autopay Terms and Conditions page.");
		else
			Assert.fail("Autopay Terms and Conditions are displayed in Autopay Terms and Conditions page.");
		if (autoPayText.contains(additionalPaymentTermsAndConditions))
			Reporter.log("Additional Payment Terms and Conditions are displayed in Autopay Terms and Conditions page.");
		else
			Assert.fail("Additional Payment Terms and Conditions are displayed in Autopay Terms and Conditions page.");
		if (autoPayText.contains(legalText))
			Reporter.log("Legal text at the below on Autopay Terms and Conditions page is displayed.");
		else
			Assert.fail("Legal text at the below on Autopay Terms and Conditions page is displayed.");
		return this;
	}

	/**
	 * Verify Back CTA on Autopay Terms and conditions page
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkBackCTAOnAutopayTermsAndConditionsPage() {
		try {
			backCTAOnAutoPayTermsAndConditions.isDisplayed();
			Reporter.log("Back CTA is displayed on Autopay Terms and Conditions page.");
		} catch (Exception e) {
			Assert.fail("Back CTA is not displayed on Autopay Terms and Conditions page.");
		}
		return this;
	}

	/**
	 * Verify Congestion related T&C
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkCongestionRelatedTAndC() {
		String expectedCongestionText = "During congestion, customers using >50GB/mo. may notice reduced speeds; Essentials customers may notice speeds lower than other customers. T-Mobile ONE™ and Essentials video typically streams on smartphone/tablet at DVD quality (480p) unless you add an HD Day Pass or Plus; tethering at max 3G speeds. Simple Choice video typically streams at DVD quality (480p or better) with Binge On enabled; disable Binge On at any time.";
		try {
			Assert.assertTrue(congestionRelatedTermsAndConditions.isDisplayed(),
					"Congestion related Terms and Conditions are mismatched");
			if (expectedCongestionText.equals(congestionRelatedTermsAndConditions.getText()))
				Reporter.log("Congestion related Terms and Conditions are matched.");
			else
				Assert.fail("Congestion related Terms and Conditions are mismatched.");
		} catch (Exception e) {
			Assert.fail("Congestion related Terms and Conditions are not displayed.");
		}
		return this;
	}

	/**
	 * Verify Autopay message
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkAutoPaySavingMessageOnAutopayBlade() {
		try {
			ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
			changePlanPlansBreakdownPage.clickOnPlansBlade();
			changePlanPlansBreakdownPage.verifyPlansBreakdownPage();

			int totalLine = changePlanPlansBreakdownPage.getTotalLines();
			float totalDiscount = totalLine * 5;
			String discount = (String.format("%.2f", totalDiscount));
			String msg = "You’re saving $" + discount + " with AutoPay.";

			changePlanPlansBreakdownPage.clickOnBackToSummaryCTA();
			ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
			changePlansReviewPage.verifyChangePlansReviewPage();

			autoPayMessageOnAutopayBlade.isDisplayed();
			if (autoPayMessageOnAutopayBlade.getText().trim().equals(msg))
				Reporter.log("Autopay saving message matched");
			else
				Assert.fail("Autopay saving message mismatched on Autopay blade.");
		} catch (Exception e) {
			Assert.fail("Autopay saving message is not displayed on Autopay blade.");
		}
		return this;
	}

	/**
	 * Turn Autopay slider to OFF
	 * 
	 * @return
	 */
	public ChangePlansReviewPage turnAutoPayOFFToggle() {
		try {
			autopaySlider.isDisplayed();
			autopaySlider.click();
			Reporter.log("Able to turn off Autopay");
		} catch (Exception e) {
			Assert.fail("Autopay slider is not displayed");
		}
		return this;
	}

	/**
	 * When Autopay blade is not displayed
	 * 
	 * @return
	 */
	public ChangePlansReviewPage whenAutopayBladeNotDispalyed() {
		try {
			Assert.assertTrue(autoPayBlade.isDisplayed(), "Autopay blade is not displayed");
			Reporter.log("Autopay blade is displayed");
		} catch (Exception e) {
			Reporter.log("Autopay blade is not displayed when Autopay already ON.");
		}
		return this;
	}

	/**
	 * check when able to see Payment Method Blade
	 * 
	 * @return
	 */
	public ChangePlansReviewPage whenPaymentMethodBladeNotDisplayed() {
		try {
			Assert.assertTrue(paymentMethodBlade.isDisplayed(), "Payment method is not displayed");
		} catch (Exception e) {
			Reporter.log("Payment method is not displayed");
		}
		return this;
	}

	/**
	 * check First Autopay date and autopay message not displayed
	 * 
	 * @return
	 */
	public ChangePlansReviewPage whenFirstAutopayDateAndMessageNotDisplayed() {
		try {
			firstAutoPayDateAndMessage.isDisplayed();
			Assert.fail("First Autopay Date and Message is displayed");
		} catch (Exception e) {
			Reporter.log("First Autopay Date and Message is not displayed");
		}
		return this;
	}

	/**
	 * verify First Autopay date and autopay message
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkFirstAutopayDateAndMessage() {
		try {
			String firstAutopayMessage = "First AutoPay payment:";
			String autopayDiscountMessage = "Your AutoPay discount will take effect beginning with your next bill on:";

			firstAutoPayDateAndMessage.isDisplayed();

			if (firstAutoPayDateAndMessage.getText().trim().contains(firstAutopayMessage))
				Reporter.log("First Autopay Date and Message is displayed");
			else
				Assert.fail("First Autopay date message is not displayed.");
			if (firstAutoPayDateAndMessage.getText().trim().contains(autopayDiscountMessage))
				Reporter.log("Discount Message is displayed");
			else
				Assert.fail("Discount Message is not displayed");
		} catch (Exception e) {
			Assert.fail("First Autopay Date and Message is not displayed");
		}
		return this;
	}

	/**
	 * check when warning modal is displayed
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkWarningModalInAllRegularScenarios() {
		try {
			waitforSpinner();
			checkPageIsReady();
			if (!warningModal.isEmpty()) {
				warningModalContinueCTA.click();
				Reporter.log("Warning modal is displayed");
			}
		} catch (Exception e) {
			Reporter.log("Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check when warning modal not displayed
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkWhenWarningModalIsNotDisplayed() {
		try {
			warningModal.get(0).isDisplayed();
			Assert.fail("Warning modal is displayed");
		} catch (Exception e) {
			Reporter.log("Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check whether Icon displayed on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage iconDisplayedOnWarningModal() {
		try {
			iconOnWarningModal.isDisplayed();
			Reporter.log("Icon on Warning modal is displayed");
		} catch (Exception e) {
			Assert.fail("Icon on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check whether Icon displayed on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage iconNotDisplayedOnWarningModal() {
		try {
			iconOnWarningModal.isDisplayed();
			Assert.fail("Icon on Warning modal is displayed");
		} catch (Exception e) {
			Reporter.log("Icon on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check whether Heading displayed on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkHeaderOnWarningModal() {
		try {
			warningModalHeader.isDisplayed();
			Reporter.log("Header on Warning modal is displayed");
			if (warningModalHeader.getText().trim().equals("Plan update"))
				Reporter.log("Header on Warning modal is matched");
			else
				Assert.fail("Header on Warning modal is matched");
		} catch (Exception e) {
			Assert.fail("Header on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check MBB related Authorable copy on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkMBBRelatedAuthorableCopyOnWarningModal() {
		try {
			String actualMIRelatedText = null;
			String msg = "Your current Mobile Internet plan isn't compatible with this new plan, but we'll select the best available plan for you. Please review this change by tapping 'Plans' in the next step.";
			warningModalText.isDisplayed();
			actualMIRelatedText = warningModalText.getText().trim();
			Reporter.log("Text on Warning modal is displayed");
			Assert.assertEquals(actualMIRelatedText, msg, "MI related text mismatched on Warning modal");
		} catch (Exception e) {
			Assert.fail("MI related Text on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check Back CTA on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkBackCTAOnWarningModal() {
		try {
			warningModalYesBtn.isDisplayed();
			Reporter.log("Back CTA on Warning modal is displayed");
		} catch (Exception e) {
			Assert.fail("Back CTA on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check Back CTA on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnCancelYesButton() {
		try {
			warningModalYesBtn.click();
			Reporter.log("Back CTA on Warning modal is displayed");
		} catch (Exception e) {
			Assert.fail("Back CTA on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * check Continue CTA on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage checkContinueCTAOnWarningModal() {
		try {
			warningModalContinueCTA.isDisplayed();
			Reporter.log("Continue CTA on Warning modal is displayed");
		} catch (Exception e) {
			Assert.fail("Continue CTA on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * Click on Continue CTA on Warning modal or not
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnContinueCTAOnWarningModal() {
		try {
			warningModalContinueCTA.click();
			Reporter.log("Able to click on Continue CTA on Warning modal is displayed");
		} catch (Exception e) {
			Assert.fail("Not clicked on Warning modal is not displayed");
		}
		return this;
	}

	/**
	 * Click on Agree & Submit CTA
	 * 
	 * @return
	 */
	public ChangePlansReviewPage clickOnAgreeAndSubmitCTA() {
		try {
			agreeAndSubmitCTA.click();
			Reporter.log("Clicked on Agree and Submit CTA");
		} catch (Exception e) {
			Assert.fail("Not able to clicked on Agree and Submit CTA");
		}
		return this;
	}

	/**
	 * Verify Warning modal and click on Continue CTA
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public ChangePlansReviewPage checkWarningModalandPlanNameOnReviewPage(String planName) throws InterruptedException {
		verifyChangePlansReviewPage();
		verifyNewPlanName(planName);
		return this;
	}

	/**
	 * Verify Warning modal in case of MBB incompatible
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public ChangePlansReviewPage checkWarningModalWhenMBBIsIncompatibleAndGSMStartsChangePlan()
			throws InterruptedException {
		checkPageIsReady();
		// checkWhenWarningModalDisplayed();
		iconDisplayedOnWarningModal();
		checkHeaderOnWarningModal();
		checkMBBRelatedAuthorableCopyOnWarningModal();
		// checkTapTheAlertAuthorableCopyOnWarningModal();
		checkBackCTAOnWarningModal();
		checkContinueCTAOnWarningModal();
		clickOnContinueCTAOnWarningModal();
		Thread.sleep(5000);
		verifyChangePlansReviewPage();
		checkTapToReviewChangesTextOnPlansBlade();
		checkWarningImageOnPlansBladeOnPlansReviewPage();
		// checkNewPlanName(planName);
		return this;
	}

	/**
	 * Verify Warning modal in case of MBB compatible
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public ChangePlansReviewPage checkWarningModalWhenMBBIsCompatibleAndGSMStartsChangePlan()
			throws InterruptedException {
		checkWhenWarningModalIsNotDisplayed();
		verifyChangePlansReviewPage();
		checkTapToReviewChangesTextNotDisplayedOnPlansBlade();
		whenWarningImageNotDisplayedOnPlansBlade();

		// checkNewPlanName(planName);
		return this;
	}

	// get Total Cost on Plans Breakdown page
	public double navigateFromPlansReviewToPlansBreakdownPageAndGetTotalCost() {
		double totalMonthlyCostOnPlansBreakdownPage = 0.00;
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		try {
			changePlansReviewPage.verifyChangePlansReviewPage();
			changePlansReviewPage.clickOnPlansBlade();

			ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
			changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
			totalMonthlyCostOnPlansBreakdownPage = changePlanPlansBreakdownPage.getTotalMonthlyCostWithoutDollarSign();
			changePlanPlansBreakdownPage.clickOnBackToSummaryCTA();
			changePlansReviewPage.verifyChangePlansReviewPage();
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost on Plans breakdown page");
		}
		return totalMonthlyCostOnPlansBreakdownPage;
	}

	// get Total Cost on Addons Breakdown page
	public double navigateFromPlansReviewToAddonsBreakdownPageAndGetTotalCost() {
		double totalMonthlyCostOnAddonsBreakdownPage = 0.00;
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		try {
			changePlansReviewPage.clickOnAddonsBlade();

			ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new ChangePlanAddonsBreakdownPage(
					getDriver());
			changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();
			totalMonthlyCostOnAddonsBreakdownPage = changePlanAddonsBreakdownPage
					.getTotalMonthlyCostWithoutDollarSign();
			changePlanAddonsBreakdownPage.clickOnBackToSummaryCTA();
			changePlansReviewPage.verifyChangePlansReviewPage();
		} catch (Exception e1) {
			Assert.fail("Not able to calculate cost on Addons Breakdown page");
		}
		return totalMonthlyCostOnAddonsBreakdownPage;
	}

	// get Total Cost on New Monthly Total Breakdown page
	public double navigateFromPlansReviewToNewMonthlyTotalBreakdownPageAndGetTotalCost() {
		double totalMonthlyCostOnTotalBreakdownPage = 0.00;
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		try {
			changePlansReviewPage.clickOnNewMonthlyTotalBlade();

			MonthlyTotalBreakdownPage monthlyTotalBreakdownPage = new MonthlyTotalBreakdownPage(getDriver());
			monthlyTotalBreakdownPage.verifyNewMonthlyTotalBreakdownPage();
			// newMonthlyTotalBreakdownPage.checkAndVerifyTotalMonthlyLevelCostAgainstAccountLevelAndLineLevel();
			totalMonthlyCostOnTotalBreakdownPage = monthlyTotalBreakdownPage.getTotalMonthlyCostWithoutDollarSign();
			monthlyTotalBreakdownPage.clickOnBackToSummaryCTA();
			changePlansReviewPage.verifyChangePlansReviewPage();
		} catch (Exception e2) {
			Assert.fail("Not able to calculate cost on New Monthly Total Breakdown page");
		}
		return totalMonthlyCostOnTotalBreakdownPage;
	}

	public double verifyNewMonthlyTotalCostWithPlansAndAddonsBreakdownPages() {
		double totalMonthlyCostOnPlansBreakdownPage = 0.00;
		double totalMonthlyCostOnAddonsBreakdownPage = 0.00;
		double totalMonthlyCostOnTotalBreakdownPage = 0.00;
		double totalCostOfPlansAndAddonsPages;
		try {
			totalMonthlyCostOnPlansBreakdownPage = navigateFromPlansReviewToPlansBreakdownPageAndGetTotalCost();
			totalMonthlyCostOnAddonsBreakdownPage = navigateFromPlansReviewToAddonsBreakdownPageAndGetTotalCost();
			totalMonthlyCostOnTotalBreakdownPage = navigateFromPlansReviewToNewMonthlyTotalBreakdownPageAndGetTotalCost();

			totalCostOfPlansAndAddonsPages = totalMonthlyCostOnPlansBreakdownPage
					+ totalMonthlyCostOnAddonsBreakdownPage;

			if (totalCostOfPlansAndAddonsPages == totalMonthlyCostOnTotalBreakdownPage)
				Reporter.log("Total Cost on Plans and Addons breakdown pages and total page is matched");
			else
				Assert.fail("Total Cost on Plans and Addons breakdown pages and total page is mismatched");
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost");
		}
		/*
		 * try { changePlansReviewPage.clickOnAddonsBlade();
		 * 
		 * ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new
		 * ChangePlanAddonsBreakdownPage(getDriver());
		 * changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();
		 * totalMonthlyCostOnAddonsBreakdownPage =
		 * changePlanAddonsBreakdownPage.getTotalMonthlyCostWithoutDollarSign();
		 * changePlanAddonsBreakdownPage.clickOnBackToSummaryCTA();
		 * changePlansReviewPage.verifyChangePlansReviewPage(); } catch (Exception e1) {
		 * Assert.fail( "Not able to calculate cost on Addons Breakdown page"); }
		 * 
		 * try { changePlansReviewPage.clickOnNewMonthlyTotalBlade();
		 * 
		 * NewMonthlyTotalBreakdownPage newMonthlyTotalBreakdownPage = new
		 * com.tmobile.eservices.qa.pages.accounts.NewMonthlyTotalBreakdownPage(
		 * getDriver());
		 * newMonthlyTotalBreakdownPage.verifyNewMonthlyTotalBreakdownPage();
		 * //newMonthlyTotalBreakdownPage.
		 * checkAndVerifyTotalMonthlyLevelCostAgainstAccountLevelAndLineLevel();
		 * totalMonthlyCostOnTotalBreakdownPage =
		 * newMonthlyTotalBreakdownPage.getTotalMonthlyCostWithoutDollarSign();
		 * newMonthlyTotalBreakdownPage.clickOnBackToSummaryCTA();
		 * changePlansReviewPage.verifyChangePlansReviewPage(); }catch (Exception e2) {
		 * Assert.fail(
		 * "Not able to calculate cost on New Monthly Total Breakdown page"); }
		 */

		return totalMonthlyCostOnTotalBreakdownPage;

	}

	/**
	 * Verify Delta cost
	 * 
	 * @return
	 * 
	 */
	public double verifyDeltaCost() {
		double actualDeltaCostValue = 0.00;
		String deltaCost;
		try {
			deltaCostValue.isDisplayed();
			deltaCost = deltaCostValue.getText().trim();
			deltaCost = deltaCost.split("/")[0].replace("$", "");
			actualDeltaCostValue = Double.parseDouble(deltaCost);
		} catch (Exception e) {
			Assert.fail("Delta cost value is not displayed");
		}
		return actualDeltaCostValue;
	}

	/**
	 * Verify Delta cost
	 * 
	 * @return
	 * 
	 */
	public double verifyDeltaCostValueDifference(double currentMonthlyCost) {
		double totalCostForNewPlan = 0.00;
		double differenceBetweenPlans = 0.00;
		double deltaCost = 0.00;
		try {
			totalCostForNewPlan = verifyNewMonthlyTotalCostWithPlansAndAddonsBreakdownPages();
			differenceBetweenPlans = totalCostForNewPlan - currentMonthlyCost;
			deltaCost = verifyDeltaCost();
			Assert.assertEquals(differenceBetweenPlans, deltaCost, "Delta cost value mismatched");
		} catch (Exception e) {
			Assert.fail("Not able to calculate Delta cost value");
		}
		return differenceBetweenPlans;
	}

	/**
	 * Read cost on New Monthly Total blade
	 *
	 * @return
	 */
	public String getCostFromNewMonthlyTotalBladeWithDollarAndMonthSign() {
		String costOnBlade = null;
		try {
			costOnBlade = costOnNewMonthlyTotalOnBlade.getText().trim();
			if (costOnBlade.contains("/mo"))
				Reporter.log("New Monthly Total Cost on Blade displayed with '/mo'");
			else
				Assert.fail("New Monthly Total Cost on Blade is not displayed with '/mo'");
		} catch (Exception e) {
			Assert.fail("Not able to calculate on cost on New Monthly Total blade");
		}
		return costOnBlade;
	}
}