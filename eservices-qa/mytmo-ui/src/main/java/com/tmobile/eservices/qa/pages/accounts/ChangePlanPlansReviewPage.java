
/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class ChangePlanPlansReviewPage extends CommonPage {

	@FindBy(xpath = "//*[contains(text(),'Plans')]")
	private WebElement plansTabOnPlansReviewPage;

	@FindBy(xpath = "//*[contains(text(),'Device')]")
	private WebElement deviceTabOnPlansReviewPage;

	@FindBy(xpath = "//*[contains(text(),'Add-ons')]")
	private WebElement addOnsTabOnPlansReviewPage;

	/**
	 * 
	 * @param webDriver
	 */
	public ChangePlanPlansReviewPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify plans Breakdown page is displayed
	 * 
	 * @return
	 */
	public ChangePlanPlansReviewPage verifyChangePlansReviewPage() {
		checkPageIsReady();
		waitForDataPassSpinnerInvisibility();
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("plan-configure"));
			Reporter.log("Plans Review page is displayed");
		} catch (Exception e) {
			Assert.fail("Plans Review page is not displayed");
		}
		return this;
	}

	// click on Device tab
	public ChangePlanPlansReviewPage clickOnAddonsBlade() {
		addOnsTabOnPlansReviewPage.click();
		return this;
	}
}
