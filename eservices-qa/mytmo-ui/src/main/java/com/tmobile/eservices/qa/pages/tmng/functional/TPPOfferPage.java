package com.tmobile.eservices.qa.pages.tmng.functional;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

public class TPPOfferPage extends TmngCommonPage {

	private final String offerPageURL = "pre-screen/offer";
	
    @FindBy(css = "tmo-pre-screen-offer>div>h2")
    private WebElement tppOfferPageHeader;
    
    @FindBy(xpath = "//button[contains(.,'Got it')]")
    private WebElement gotItCTAOnOfferPage;
    
    @FindBy (css = ".pre-screen-container .optOutDisclaimerOne")
    private WebElement disclaimerTextShortCopy;
    
    @FindBy (css = ".pre-screen-container .optOutDisclaimerTwo")
    private WebElement disclaimerTextLongCopy;

    public TPPOfferPage(WebDriver webDriver) {
        super(webDriver);
    }


    /**
     * Verify TPP Offer page
     */
    public TPPOfferPage verifyTPPOfferPage() {
        try {
        	waitFor(ExpectedConditions.urlContains(offerPageURL),60);
            Assert.assertTrue(tppOfferPageHeader.isDisplayed(), "TPP Offer page is not loaded");
            Reporter.log("TPP offer page loaded successfully.");
        } catch (Exception e) {
            Assert.fail("Failed to Verify TPP Offer page");
        }
        return this;
    }

    /**
     * Click Got It CTA on Pre screen Offer Page
     */
    public TPPOfferPage clickGotItCTA() {
        try {
        	checkPageIsReady();
        //	clickElementWithJavaScript(gotItCTAOnOfferPage);
        	gotItCTAOnOfferPage.click();
            Reporter.log("Clicked on Got It CTA on Pre screen OfferPage");
        } catch (Exception e) {
            Assert.fail("Failed to Click Got It CTA on Pre screen OfferPage");
        }
        return this;
    }
    
    /**
     * Verify TPP Offer page
     */
    public TPPOfferPage verifyDisclaimerText() {
        try {
            Assert.assertTrue(disclaimerTextShortCopy.isDisplayed(), "Opt-out disclaimer Short Copy on Offer page is not displayed");
            Assert.assertTrue(disclaimerTextLongCopy.isDisplayed(), "Opt-out disclaimer Long Copy on Offer page is not displayed");
            Reporter.log("Opt-out disclaimer on Offer page is present");
        } catch (Exception e) {
            Assert.fail("Failed to verify Opt-out disclaimer on Offer page");
        }
        return this;
    }

}
