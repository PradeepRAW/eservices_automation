package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 *
 */
public class BillingAndPaymentsPage extends CommonPage {

	private static final String pageUrl = "profile/billing_payment";

	// Headers

	@FindBy(css = "div.TMO-PROFILE-BLADE-PAGE .Display6")
	private List<WebElement> listOfPageHeaders;

	@FindBy(xpath = "#city")
	private WebElement city;

	@FindBy(css = "#line1")
	private WebElement addressLine1;

	@FindBy(css = "#line2")
	private WebElement addressLine2;

	@FindBy(css = "[name='e911_address']")
	private WebElement e911Address;

	@FindBy(xpath = "//input[@name='e911_address']/../div/span")
	private WebElement e911AddressChkBox;

	@FindBy(xpath = "//input[@name='usgAddress']/../div/span")
	private WebElement usageAddressChkBox;

	@FindBy(css = "[name='usgAddress']")
	private WebElement usageAddress;

	@FindBy(css = ".SecondaryCTA")
	private WebElement cancelBtn;

	@FindBy(css = ".PrimaryCTA")
	private WebElement saveChangesBtn;

	@FindBy(css = "label[for='paperlessBill']")
	private WebElement paperLessBillRadioBtn;

	@FindBy(css = "#paperlessBill")
	private WebElement paperLessBillRadioBtnForIOS;

	@FindBy(xpath = "//input[@id='1']/../label")
	private WebElement paperLessBillRadioBtnLabel;

	@FindBy(css = "label[for='email'] span.legal.check1")
	private WebElement emailRadioBtn;

	@FindBy(css = "#email")
	private WebElement emailRadioBtnForIOS;

	@FindBy(css = "input[id='3']")
	private WebElement summaryBillRadioBtn;

	@FindBy(css = "#isSummaryBill")
	private WebElement summaryBillRadioBtnForIOS;

	@FindBy(css = "label[for='4']")
	private WebElement detailedBillRadioBtn;

	@FindBy(css = ".PrimaryCTA-accent")
	private WebElement detailedBillAgreeBtn;

	@FindBy(xpath = "//p[text()='AutoPay']")
	private WebElement autoPayLink;

	@FindBy(xpath = "//p[text()='AutoPay']/following-sibling::p")
	private WebElement textAutoPayLink;

	@FindBy(css = "label[for='paperbill']")
	private WebElement paperBillRadioBtn;

	@FindBy(css = "#paperbill")
	private WebElement paperBillRadioBtnForIOS;

	@FindBy(css = ".PrimaryCTA")
	private WebElement agreeAndSubmitBtn;

	@FindBy(css = ".PrimaryCTA-accent:not(.margin-left-xxsmall)")
	private WebElement detailedBillAgreeAndSubmitBtn;

	@FindBy(xpath = "//p[@class='Display6'and contains(text(),'Billing Address')]/../p[2]")
	private WebElement updatedAddressLine2;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement stateDropDownToggle;

	@FindBy(css = "ul.dropdown-menu.show")
	private WebElement stateDropDownMenu;

	public BillingAndPaymentsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Billing And Payments Page.
	 *
	 * @return the BillingAndPaymentsPage class instance.
	 */
	public BillingAndPaymentsPage verifyBillingAndPaymentsPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Billing and payments page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Billing and payments page not displayed");
		}
		return this;
	}

	/**
	 * Verify Header By Name.
	 */
	public BillingAndPaymentsPage clikHeaderByName(String headerName) {
		waitforSpinnerinProfilePage();
		waitFor(ExpectedConditions.visibilityOf(listOfPageHeaders.get(0)));
		try {
			for (WebElement webElement : listOfPageHeaders) {
				if (webElement.getText().contains(headerName)) {
					webElement.click();
					Reporter.log(headerName + "clicked");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail(headerName + " header is not found to click");
		}
		return this;
	}

	/**
	 * Verify e911Address.
	 */
	public BillingAndPaymentsPage verifye911Address() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			if (!e911AddressChkBox.isSelected()) {
				clickElementWithJavaScript(e911AddressChkBox);
			}
		} catch (Exception e) {
			Assert.fail("Click on e911Address checkbox failed");
		}
		return this;
	}

	/**
	 * Verify UsageAddress.
	 */
	public BillingAndPaymentsPage verifyUsageAddress() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			if (!usageAddressChkBox.isSelected()) {
				clickElementWithJavaScript(usageAddressChkBox);
			}
		} catch (Exception e) {
			Assert.fail("Click on usageAddress checkbox failed");
		}
		return this;
	}

	/**
	 * Verify cancel button CTA.
	 */
	public BillingAndPaymentsPage verifyCancelButtonCTA() {
		try {
			waitforSpinnerinProfilePage();
			cancelBtn.click();
			Reporter.log("Clicked on cancel button");
		} catch (Exception e) {
			Reporter.log("Click on cancel button failed");
			Assert.fail("Click on cancel button failed");
		}
		return this;
	}

	/**
	 * click SaveChanges btn.
	 */
	public BillingAndPaymentsPage clickSaveChangesBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (saveChangesBtn.isEnabled()) {
				saveChangesBtn.click();
				Reporter.log("Save changes button is clicked");
			} else {
				Assert.fail("Save changes button is in disabled state");
			}
		} catch (Exception e) {
			Assert.fail("Click on save changes button failed");
		}
		return this;
	}

	/**
	 * Verify e911Address.
	 */
	public BillingAndPaymentsPage clickPaperLessBillRadioBtn() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				if (!paperLessBillRadioBtnForIOS.isSelected()) {
					clickElementWithJavaScript(paperLessBillRadioBtnForIOS);
				}
			} else {
				if (!paperLessBillRadioBtn.isSelected()) {
					clickElementWithJavaScript(paperLessBillRadioBtn);
				}
			}
		} catch (Exception e) {
			Assert.fail("Click on paperLessBillRadioBtn checkbox failed");
		}
		return this;
	}

	/**
	 * Click email radio button.
	 */
	public BillingAndPaymentsPage clickEmailRadioBtn() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			if (getDriver() instanceof AppiumDriver) {
				if (!emailRadioBtnForIOS.isSelected()) {
					clickElementWithJavaScript(emailRadioBtnForIOS);
				}
			} else if (!emailRadioBtn.isSelected()) {
				clickElementWithJavaScript(emailRadioBtn);
			}
		} catch (Exception e) {
			Assert.fail("Click on emailRadioBtn checkbox failed");
		}

		return this;
	}

	/**
	 * Verify e911Address.
	 */
	public BillingAndPaymentsPage clickSummaryBillRadioBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (getDriver() instanceof AppiumDriver) {
				/*
				 * waitForElement(summaryBillRadioBtnForIOS);
				 * moveToElement(summaryBillRadioBtnForIOS);
				 */
				if (!summaryBillRadioBtnForIOS.isSelected()) {
					clickElementWithJavaScript(summaryBillRadioBtnForIOS);
				}
			} else if (!summaryBillRadioBtnForIOS.isSelected()) {
				summaryBillRadioBtnForIOS.click();
			}
			Reporter.log("Summary bill radio button is selected");
		} catch (Exception e) {
			Assert.fail("Click on RadioBtn checkbox failed");
		}
		return this;
	}

	/**
	 * Click BillingAddress.
	 */
	public BillingAndPaymentsPage clickAutoPayBtn() {
		try {
			waitforSpinnerinProfilePage();
			textAutoPayLink.click();
		} catch (Exception e) {
			Assert.fail("Click on autopay link failed");
		}

		return this;
	}

	/**
	 * Select Paper Billing Address
	 */
	public BillingAndPaymentsPage selectPaperBillingAddress() {
		try {
			waitforSpinnerinProfilePage();
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(paperBillRadioBtnForIOS);
			} else {
				clickElementWithJavaScript(paperBillRadioBtn);
			}
			Reporter.log("Clicked on paper billing address");
		} catch (Exception e) {
			Assert.fail("Click on paper billing address failed ");
		}
		return this;
	}

	/**
	 * Click Agree And Submit Button
	 */
	public BillingAndPaymentsPage clickAgreeAndSubmitBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (agreeAndSubmitBtn.isEnabled()) {
				Reporter.log("Agree and submit button is enabled");
				agreeAndSubmitBtn.click();
				Reporter.log("Clicked on Agree and submit button ");
			} else {
				Assert.fail("Agree and submit button is in disabled state");
			}
		} catch (Exception e) {
			Assert.fail("Click on Agree and submit button failed");
		}
		return this;
	}

	/**
	 * Click Detailed Bill Radio Button
	 */
	public BillingAndPaymentsPage clickDetailedBillRadioBtn() {
		try {
			waitforSpinnerinProfilePage();
			if (!detailedBillRadioBtn.isSelected()) {
				detailedBillRadioBtn.click();
			}
			Reporter.log("Detailed bill radio button is selected");
		} catch (Exception e) {
			Assert.fail("Click on detailed bill radio btn failed");
		}
		return this;
	}

	/**
	 * Click Detailed Bill Agree And Submit Button
	 */
	public BillingAndPaymentsPage clickDetailedBillAgreeAndSubmitBtn() {
		try {
			waitforSpinnerinProfilePage();
			detailedBillAgreeAndSubmitBtn.click();
		} catch (Exception e) {
			Assert.fail("Click on detailed bill agree and submit button failed");
		}
		return this;
	}

	/**
	 * Verify state dropdown toogle.
	 */
	public BillingAndPaymentsPage verifyStateDropDownToggle() {
		try {
			waitforSpinnerinProfilePage();
			stateDropDownToggle.isDisplayed();
			Reporter.log("State dropdown toggle is displayed");
		} catch (Exception e) {
			Assert.fail("State dropdown toggle not displayed");
		}
		return this;
	}

	/**
	 * Veriy state dropdown menu.
	 */
	public BillingAndPaymentsPage verifyStateDropDownMenu() {
		try {
			waitforSpinnerinProfilePage();
			stateDropDownToggle.click();
			Assert.assertTrue(isElementDisplayed(stateDropDownMenu));
			Reporter.log("State dropdown menu div expanded");
		} catch (Exception e) {
			Assert.fail("State dropdown menu div not expanded");
		}
		return this;
	}

	/**
	 * verify saved payment methods block
	 */
	public void verifySavedPaymentMethods() {
		try {

		} catch (Exception e) {
			Assert.fail("Failed to verify Saved payment methods");
		}
	}

	/**
	 * verify saved card expired/expiration soon message for autopay
	 */
	public void verifyPaymentMethodExpirationMsgsAutopay(String expirationMsg) {
		try {

		} catch (Exception e) {
			Assert.fail("Failed to verify Expiry msg for Autopay saved card.");
		}
	}

	/**
	 * verify saved card expired/expiring soon message
	 */
	public void verifyPaymentMethodNotifications(String expirationMsg) {
		try {

		} catch (Exception e) {
			Assert.fail("Failed to verify Expiry msg for saved card.");
		}
	}

	/**
	 * Verify Autopay blade.
	 */
	public BillingAndPaymentsPage verifyAutoPayBlade() {

		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(textAutoPayLink));
			Reporter.log("Autopay blade not displayed");
		} catch (AssertionError e) {
			Reporter.log("Autopay blade displayed");
			Assert.fail("Autopay blade displayed");
		}
		return this;
	}

	/**
	 * Verify Autopay blade.
	 */
	public BillingAndPaymentsPage isAutoPayBladePresent() {

		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(textAutoPayLink));
			Reporter.log("Autopay blade displayed");
		} catch (AssertionError e) {
			Reporter.log("Autopay blade not displayed");
			Assert.fail("Autopay blade not displayed");
		}
		return this;
	}
}
