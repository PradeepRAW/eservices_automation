package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class UnlimitedPlansPage extends TmngCommonPage {
	private int timeout = 30;

	private final String pageLoadedText = "On all T-Mobile plans, during congestion, the small fraction of customers using &gt;50GB/month may notice reduced speeds until next bill cycle due to data prioritization";
	private final String pageUrl = "/cell-phone-plans";

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	@CacheLookup
	private WebElement about;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/accessibility-policy']")
	@CacheLookup
	private WebElement accessibility;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	@CacheLookup
	private WebElement accessories;

	@FindBy(css = "a[href='//prepaid-phones.t-mobile.com/prepaid-activate']")
	@CacheLookup
	private WebElement activateYourPrepaidPhoneOrDevice;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	@CacheLookup
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	@CacheLookup
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	@CacheLookup
	private WebElement business2;

	@FindBy(css = "#c8c4ad5c1b4a424b08aff7c380c9deba6ab5eb77 div:nth-of-type(1) div:nth-of-type(2) div.cta. a:nth-of-type(2)")
	@CacheLookup
	private WebElement call1800tmobile1;

	@FindBy(css = "#6ff66dced29300d9f730c198e61a0262aafa40f0 div:nth-of-type(1) div:nth-of-type(2) div.cta. a:nth-of-type(3)")
	@CacheLookup
	private WebElement call1800tmobile2;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	@CacheLookup
	private WebElement callUs;

	@FindBy(css = "a[href='//www.t-mobile.com/careers']")
	@CacheLookup
	private WebElement careers;

	@FindBy(css = "a[href='/offers/t-mobile-one-unlimited-55?icid=WMM_TM_FIDELIS_W9A4FQCPKYG13113']")
	@CacheLookup
	private WebElement checkItOut1;

	@FindBy(css = "a[href='/offers/military-phone-plans?icid=WMM_TM_FIDELIS_3QO120AGKH13112']")
	@CacheLookup
	private WebElement checkItOut2;

	@FindBy(css = "a[href='//www.t-mobile.com/no-credit-check?icid=WMD_TMNG_Q118NCCLP_0NLV7J6CPFI12694']")
	@CacheLookup
	private WebElement checkItOut3;

	@FindBy(css = "a[href='http://www.t-mobile.com/orderstatus/default.aspx']")
	@CacheLookup
	private WebElement checkOrderStatus;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	@CacheLookup
	private WebElement checkOutTheCoverage;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling#check-rates']")
	@CacheLookup
	private WebElement clickHere;

	@FindBy(css = "#5139ea84ba84cf4a17290c04ec9a8d02b383da22 div:nth-of-type(2) button.close")
	@CacheLookup
	private WebElement closeVideo;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info']")
	@CacheLookup
	private WebElement consumerInformation;

	@FindBy(css = "a[href='http://www.t-mobile.com/contact-us.html']")
	@CacheLookup
	private WebElement contactInformation;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	@CacheLookup
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	@CacheLookup
	private WebElement deals2;

	@FindBy(css = "a[href='//www.telekom.com/']")
	@CacheLookup
	private WebElement deutscheTelekom;

	@FindBy(css = "a[href='//support.t-mobile.com/community/phones-tablets-devices']")
	@CacheLookup
	private WebElement deviceSupport;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	@CacheLookup
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	@CacheLookup
	private WebElement espaol;

	@FindBy(css = "a.lang-toggle-wrapper")
	@CacheLookup
	private WebElement espaolEspaol;

	@FindBy(css = "a[href='http://www.t-mobile.com/store-locator.html']")
	@CacheLookup
	private WebElement findAStore;

	@FindBy(css = "a[href='http://www.t-mobile.com/promotions']")
	@CacheLookup
	private WebElement getARebate;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div.panel-collapse.collapse.anchorID-650970 div.panel-body p a")
	@CacheLookup
	private WebElement here1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) div.panel-collapse.collapse.anchorID-650970 div.panel-body p a")
	@CacheLookup
	private WebElement here2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) button.accordion-toggle.collapsed.")
	@CacheLookup
	private WebElement howDoIGetNetflix1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement howDoIGetNetflix2;

	@FindBy(css = "#e2e62437acc588a82bf7ff23089ebbb7bb57fb22 div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(1)")
	@CacheLookup
	private WebElement httpswwwNetflixComtermsofuse;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx#choicesaboutadvertising']")
	@CacheLookup
	private WebElement interestbasedAds;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	@CacheLookup
	private WebElement internationalCalling;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global.html']")
	@CacheLookup
	private WebElement internationalRates;

	@FindBy(css = "a[href='http://iot.t-mobile.com/']")
	@CacheLookup
	private WebElement internetOfThings;

	@FindBy(css = "a[href='http://investor.t-mobile.com/CorporateProfile.aspx?iid=4091145']")
	@CacheLookup
	private WebElement investorRelations;

	@FindBy(css = "a.btn-primary.btn-brand.btn")
	@CacheLookup
	private WebElement joinUsNow;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	@CacheLookup
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	@CacheLookup
	private WebElement logIn1;

	@FindBy(css = "a[href='//my.t-mobile.com/plans.html?icid=WMM_TM_Q417PLANSP_FO240JD100611068']")
	@CacheLookup
	private WebElement logIn2;

	@FindBy(css = "a.btn-secondary.btn")
	@CacheLookup
	private WebElement makeTheSwitch;

	@FindBy(css = "button.hamburger.hamburger-border")
	@CacheLookup
	private WebElement menuMenu;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	@CacheLookup
	private WebElement myTmobile;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) button.accordion-toggle.collapsed.")
	@CacheLookup
	private WebElement needToStreamToMore1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement needToStreamToMore2;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/policies/internet-service']")
	@CacheLookup
	private WebElement openInternet;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	@CacheLookup
	private WebElement phones;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	@CacheLookup
	private WebElement plans;

	@FindBy(css = "a[href='//support.t-mobile.com/community/plans-services']")
	@CacheLookup
	private WebElement plansServices;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	@CacheLookup
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	@CacheLookup
	private WebElement prepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/news']")
	@CacheLookup
	private WebElement press;

	@FindBy(css = "input.slider-range.ng-untouched.ng-valid.ng-not-empty.ng-dirty.ng-valid-parse")
	@CacheLookup
	private WebElement priceSlider;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/privacy']")
	@CacheLookup
	private WebElement privacyCenter;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx']")
	@CacheLookup
	private WebElement privacyStatement;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/safety/9-1-1']")
	@CacheLookup
	private WebElement publicSafety911;

	@FindBy(css = "a[href='http://www.t-mobilepr.com/']")
	@CacheLookup
	private WebElement puertoRico;

	@FindBy(css = "a[href='//support.t-mobile.com/community/billing']")
	@CacheLookup
	private WebElement questionsAboutYourBill;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-refill-api-bridging']")
	@CacheLookup
	private WebElement refillYourPrepaidAccount;

	@FindBy(css = ".marketing-page.ng-scope div:nth-of-type(8) button")
	@CacheLookup
	private WebElement scrollToTop;

	@FindBy(id = "searchText")
	@CacheLookup
	private WebElement search1;

	@FindBy(id = "searchText")
	@CacheLookup
	private WebElement search2;

	@FindBy(css = "a[href='/offers/tmo_one_faqs']")
	@CacheLookup
	private WebElement seeFullFaqs;

	@FindBy(css = "#e2e62437acc588a82bf7ff23089ebbb7bb57fb22 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) p.section-legal.col-xs-12 span a.tmo-modal-link")
	@CacheLookup
	private WebElement seeFullTerms1;

	@FindBy(css = "#8766fefbb9b7114ea946b0b1e0dc00397e769cf7 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	@CacheLookup
	private WebElement seeFullTerms2;

	@FindBy(css = "a[href='javascript:void(0)']")
	@CacheLookup
	private WebElement seeFullTerms3;

	@FindBy(css = "#458c2dab5712d955a84dc48a50f007d6fd38cb22 div:nth-of-type(1) div.section-content div:nth-of-type(4) p.section-legal.col-xs-12 a.tmo-modal-link")
	@CacheLookup
	private WebElement seeFullTerms4;

	@FindBy(css = "#85ef3e61dc876f372ae50b07a250d041f40845c7 div:nth-of-type(1) div.section-content div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	@CacheLookup
	private WebElement seeFullTerms5;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	@CacheLookup
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	@CacheLookup
	private WebElement shopPhones1;

	@FindBy(css = "a[href='//www.t-mobile.com/cart?icid=WMM_TM_Q417PLANSP_A94C7S14M111072']")
	@CacheLookup
	private WebElement shopPhones2;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-plans?icid=WMD_TMNG_Q417TMONCC_BPOJNQV2YLQ11511']")
	@CacheLookup
	private WebElement shopPrepaidPlans;

	@FindBy(css = "a[href='#divfootermain']")
	@CacheLookup
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	@CacheLookup
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	@CacheLookup
	private WebElement smartDevices;

	@FindBy(css = "a[href='//www.t-mobile.com/cart?icid=WMM_TM_Q417PLANSP_X7IY89V2Q9U11067']")
	@CacheLookup
	private WebElement startShopping;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	@CacheLookup
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	@CacheLookup
	private WebElement stores;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/student-teacher-smartphone-tablet-discounts?icid=WMM_TM_CAMPUSEXCL_UHTZZ7GR1RT11028']")
	@CacheLookup
	private WebElement studentteacherDiscount;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	@CacheLookup
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.ng-scope header.global-header.light.static div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	@CacheLookup
	private WebElement submitSearch2;

	@FindBy(css = "a[href='//support.t-mobile.com/']")
	@CacheLookup
	private WebElement supportHome;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	@CacheLookup
	private WebElement termsConditions;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true']")
	@CacheLookup
	private WebElement termsOfUse;

	@FindBy(css = "a[href='tel:1-800-266-2453']")
	@CacheLookup
	private WebElement tmobile11800;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) div.panel-collapse.collapse.anchorID-639395 div.panel-body a.tmo_tfn_number")
	@CacheLookup
	private WebElement tmobile21800;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) div.panel-collapse.collapse.anchorID-639395 div.panel-body a.tmo_tfn_number")
	@CacheLookup
	private WebElement tmobile31800;

	@FindBy(css = "#e2e62437acc588a82bf7ff23089ebbb7bb57fb22 div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(2)")
	@CacheLookup
	private WebElement tmobileComopeninternet1;

	@FindBy(css = "#85ef3e61dc876f372ae50b07a250d041f40845c7 div:nth-of-type(1) div.section-content div:nth-of-type(4) p a")
	@CacheLookup
	private WebElement tmobileComopeninternet2;

	@FindBy(css = "a[href='https://www.t-mobile.com/company/company-info/consumer/internet-services.html']")
	@CacheLookup
	private WebElement tmobileComopeninternet3;

	@FindBy(css = "a[href='https://business.t-mobile.com']")
	@CacheLookup
	private WebElement tmobileForBusiness;

	@FindBy(css = "a[href='http://www.t-mobile.com/cell-phone-trade-in.html']")
	@CacheLookup
	private WebElement tradeInProgram;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	@CacheLookup
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	@CacheLookup
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	@CacheLookup
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "a[href='http://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_ReturnPolicy&print=true']")
	@CacheLookup
	private WebElement viewReturnPolicy;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement wantToLearnMoreAbout1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement wantToLearnMoreAbout2;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	@CacheLookup
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	@CacheLookup
	private WebElement wellHelpYouJoin;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button.accordion-toggle.collapsed.")
	@CacheLookup
	private WebElement whatIfIDontHave1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatIfIDontHave2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatIfINeedMore1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatIfINeedMore2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatIfIWantMore1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatIfIWantMore2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatIsNetflixOnUs1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatIsNetflixOnUs2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) button.accordion-toggle.collapsed.")
	@CacheLookup
	private WebElement whatVersionOfNetflixDo1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whatVersionOfNetflixDo2;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(5) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whoCanGetIt1;

	@FindBy(css = "#0521d32b0d51a3eb4630bf5d12aa390e6e38e311 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(5) button.accordion-toggle.collapsed")
	@CacheLookup
	private WebElement whoCanGetIt2;

	@FindBy(css = "#8766fefbb9b7114ea946b0b1e0dc00397e769cf7 div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(3) a")
	@CacheLookup
	private WebElement wwwNetflixComtermsofuse1;

	@FindBy(css = "#plans div:nth-of-type(3) p:nth-of-type(2) a")
	@CacheLookup
	private WebElement wwwNetflixComtermsofuse2;

	public UnlimitedPlansPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Click on About Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickAboutLink() {
		about.click();
		return this;
	}

	/**
	 * Click on Accessibility Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickAccessibilityLink() {
		accessibility.click();
		return this;
	}

	/**
	 * Click on Accessories Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickAccessoriesLink() {
		accessories.click();
		return this;
	}

	/**
	 * Click on Activate Your Prepaid Phone Or Device Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickActivateYourPrepaidPhoneOrDeviceLink() {
		activateYourPrepaidPhoneOrDevice.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on Call 1800tmobile Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCall1800tmobile1Link() {
		call1800tmobile1.click();
		return this;
	}

	/**
	 * Click on Call 1800tmobile Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCall1800tmobile2Link() {
		call1800tmobile2.click();
		return this;
	}

	/**
	 * Click on Call Us Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Careers Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCareersLink() {
		careers.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCheckItOut1Link() {
		checkItOut1.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCheckItOut2Link() {
		checkItOut2.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCheckItOut3Link() {
		checkItOut3.click();
		return this;
	}

	/**
	 * Click on Check Order Status Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCheckOrderStatusLink() {
		checkOrderStatus.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Click Here Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickClickHereLink() {
		clickHere.click();
		return this;
	}

	/**
	 * Click on Close Video Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickCloseVideoButton() {
		closeVideo.click();
		return this;
	}

	/**
	 * Click on Consumer Information Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickConsumerInformationLink() {
		consumerInformation.click();
		return this;
	}

	/**
	 * Click on Contact Information Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickContactInformationLink() {
		contactInformation.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Deutsche Telekom Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickDeutscheTelekomLink() {
		deutscheTelekom.click();
		return this;
	}

	/**
	 * Click on Device Support Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickDeviceSupportLink() {
		deviceSupport.click();
		return this;
	}

	/**
	 * Click on Espanol Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Espaol Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickEspaolEspaolLink() {
		espaolEspaol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickFindAStoreLink() {
		findAStore.click();
		return this;
	}

	/**
	 * Click on Get A Rebate Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickGetARebateLink() {
		getARebate.click();
		return this;
	}

	/**
	 * Click on Here Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickHere1Link() {
		here1.click();
		return this;
	}

	/**
	 * Click on Here Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickHere2Link() {
		here2.click();
		return this;
	}

	/**
	 * Click on How Do I Get Netflix At No Additional Charge How Does It Work
	 * Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickHowDoIGetNetflix1Button() {
		howDoIGetNetflix1.click();
		return this;
	}

	/**
	 * Click on How Do I Get Netflix At No Additional Charge How Does It Work
	 * Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickHowDoIGetNetflix2Button() {
		howDoIGetNetflix2.click();
		return this;
	}

	/**
	 * Click on Httpswww.netflix.comtermsofuse Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickHttpswwwNetflixComtermsofuseLink() {
		httpswwwNetflixComtermsofuse.click();
		return this;
	}

	/**
	 * Click on Interestbased Ads Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickInterestbasedAdsLink() {
		interestbasedAds.click();
		return this;
	}

	/**
	 * Click on International Calling Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on International Rates Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickInternationalRatesLink() {
		internationalRates.click();
		return this;
	}

	/**
	 * Click on Internet Of Things Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickInternetOfThingsLink() {
		internetOfThings.click();
		return this;
	}

	/**
	 * Click on Investor Relations Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickInvestorRelationsLink() {
		investorRelations.click();
		return this;
	}

	/**
	 * Click on Join Us Now Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickJoinUsNowLink() {
		joinUsNow.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickLogIn1Link() {
		logIn1.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickLogIn2Link() {
		logIn2.click();
		return this;
	}

	/**
	 * Click on Make The Switch Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickMakeTheSwitchLink() {
		makeTheSwitch.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickMyTmobileLink() {
		myTmobile.click();
		return this;
	}

	/**
	 * Click on Need To Stream To More Devices Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickNeedToStreamToMore1Button() {
		needToStreamToMore1.click();
		return this;
	}

	/**
	 * Click on Need To Stream To More Devices Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickNeedToStreamToMore2Button() {
		needToStreamToMore2.click();
		return this;
	}

	/**
	 * Click on Open Internet Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickOpenInternetLink() {
		openInternet.click();
		return this;
	}

	/**
	 * Click on Phones Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click on Plans Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Plans Services Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPlansServicesLink() {
		plansServices.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Press Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPressLink() {
		press.click();
		return this;
	}

	/**
	 * Click on Privacy Center Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPrivacyCenterLink() {
		privacyCenter.click();
		return this;
	}

	/**
	 * Click on Privacy Statement Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPrivacyStatementLink() {
		privacyStatement.click();
		return this;
	}

	/**
	 * Click on Public Safety911 Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPublicSafety911Link() {
		publicSafety911.click();
		return this;
	}

	/**
	 * Click on Puerto Rico Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickPuertoRicoLink() {
		puertoRico.click();
		return this;
	}

	/**
	 * Click on Questions About Your Bill Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickQuestionsAboutYourBillLink() {
		questionsAboutYourBill.click();
		return this;
	}

	/**
	 * Click on Refill Your Prepaid Account Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickRefillYourPrepaidAccountLink() {
		refillYourPrepaidAccount.click();
		return this;
	}

	/**
	 * Click on Scroll To Top Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickScrollToTopButton() {
		scrollToTop.click();
		return this;
	}

	/**
	 * Click on See Full Faqs Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSeeFullFaqsLink() {
		seeFullFaqs.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSeeFullTerms1Link() {
		seeFullTerms1.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSeeFullTerms2Link() {
		seeFullTerms2.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSeeFullTerms3Link() {
		seeFullTerms3.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSeeFullTerms4Link() {
		seeFullTerms4.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSeeFullTerms5Link() {
		seeFullTerms5.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickShopPhones1Link() {
		shopPhones1.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickShopPhones2Link() {
		shopPhones2.click();
		return this;
	}

	/**
	 * Click on Shop Prepaid Plans Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickShopPrepaidPlansLink() {
		shopPrepaidPlans.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Start Shopping Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickStartShoppingLink() {
		startShopping.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Studentteacher Discount Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickStudentteacherDiscountLink() {
		studentteacherDiscount.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Support Home Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickSupportHomeLink() {
		supportHome.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Terms Of Use Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTermsOfUseLink() {
		termsOfUse.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTmobile1Link1800() {
		tmobile11800.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTmobile2Link1800() {
		tmobile21800.click();
		return this;
	}

	/**
	 * Click on 1800tmobile Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTmobile3Link1800() {
		tmobile31800.click();
		return this;
	}

	/**
	 * Click on Tmobile.comopeninternet Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTmobileComopeninternet1Link() {
		tmobileComopeninternet1.click();
		return this;
	}

	/**
	 * Click on Tmobile.comopeninternet Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTmobileComopeninternet2Link() {
		tmobileComopeninternet2.click();
		return this;
	}

	/**
	 * Click on Tmobile.comopeninternet Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTmobileComopeninternet3Link() {
		tmobileComopeninternet3.click();
		return this;
	}

	/**
	 * Click on Tmobile For Business Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTmobileForBusinessLink() {
		tmobileForBusiness.click();
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTradeInProgramLink() {
		tradeInProgram.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on View Return Policy Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickViewReturnPolicyLink() {
		viewReturnPolicy.click();
		return this;
	}

	/**
	 * Click on Want To Learn More About Our Unlimited Talk Text Only Plan Go To The
	 * Link Below For Details. Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWantToLearnMoreAbout1Button() {
		wantToLearnMoreAbout1.click();
		return this;
	}

	/**
	 * Click on Want To Learn More About Our Unlimited Talk Text Only Plan Go To The
	 * Link Below For Details. Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWantToLearnMoreAbout2Button() {
		wantToLearnMoreAbout2.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Click on What If I Dont Have A Netflix Account Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIfIDontHave1Button() {
		whatIfIDontHave1.click();
		return this;
	}

	/**
	 * Click on What If I Dont Have A Netflix Account Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIfIDontHave2Button() {
		whatIfIDontHave2.click();
		return this;
	}

	/**
	 * Click on What If I Need More Than 12 Lines Can I Get More On The Same Account
	 * Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIfINeedMore1Button() {
		whatIfINeedMore1.click();
		return this;
	}

	/**
	 * Click on What If I Need More Than 12 Lines Can I Get More On The Same Account
	 * Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIfINeedMore2Button() {
		whatIfINeedMore2.click();
		return this;
	}

	/**
	 * Click on What If I Want More Than Four Voice Lines What Would The Price Be
	 * Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIfIWantMore1Button() {
		whatIfIWantMore1.click();
		return this;
	}

	/**
	 * Click on What If I Want More Than Four Voice Lines What Would The Price Be
	 * Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIfIWantMore2Button() {
		whatIfIWantMore2.click();
		return this;
	}

	/**
	 * Click on What Is Netflix On Us Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIsNetflixOnUs1Button() {
		whatIsNetflixOnUs1.click();
		return this;
	}

	/**
	 * Click on What Is Netflix On Us Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatIsNetflixOnUs2Button() {
		whatIsNetflixOnUs2.click();
		return this;
	}

	/**
	 * Click on What Version Of Netflix Do I Get Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatVersionOfNetflixDo1Button() {
		whatVersionOfNetflixDo1.click();
		return this;
	}

	/**
	 * Click on What Version Of Netflix Do I Get Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhatVersionOfNetflixDo2Button() {
		whatVersionOfNetflixDo2.click();
		return this;
	}

	/**
	 * Click on Who Can Get It Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhoCanGetIt1Button() {
		whoCanGetIt1.click();
		return this;
	}

	/**
	 * Click on Who Can Get It Button.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWhoCanGetIt2Button() {
		whoCanGetIt2.click();
		return this;
	}

	/**
	 * Click on Www.netflix.comtermsofuse Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWwwNetflixComtermsofuse1Link() {
		wwwNetflixComtermsofuse1.click();
		return this;
	}

	/**
	 * Click on Www.netflix.comtermsofuse Link.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage clickWwwNetflixComtermsofuse2Link() {
		wwwNetflixComtermsofuse2.click();
		return this;
	}

	/**
	 * Set Price Slider Range field.
	 */
	public UnlimitedPlansPage setPriceSliderRangeField() {
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Unlimited plans page loaded");

		} catch (Exception ex) {
			Assert.fail("Unlimited Plans Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the UnlimitedPlansPage class instance.
	 */
	public UnlimitedPlansPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

}
